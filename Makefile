benchmarks:
	@echo "+---------------------------------------------------------------------------"
	@echo "| Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr"
	@echo "| "
	@echo "| This program is free software: you can redistribute it and/or modify"
	@echo "| it under the terms of the GNU General Public License as published by"
	@echo "| the Free Software Foundation, either version 3 of the License, or"
	@echo "| (at your option) any later version."
	@echo "| "
	@echo "| This program is distributed in the hope that it will be useful,"
	@echo "| but WITHOUT ANY WARRANTY; without even the implied warranty of"
	@echo "| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the"
	@echo "| GNU General Public License for more details."
	@echo "| "
	@echo "| You should have received a copy of the GNU General Public License"
	@echo "| along with this program.  If not, see <http://www.gnu.org/licenses/>."
	@echo "+---------------------------------------------------------------------------"
	@echo "| See \"Makefile\" for other configuration possibilities"
	@echo "|"
	@echo "| Possible choices are: "
	@echo "|     Targets: $(BENCHMARKS)"
	@echo "|     Others: clean all all-mips all-patmos"
	@echo "|"
	@echo "| Variables: "
	@echo "|     TARGET=<empty>/host/patmos/mips"
	@echo "|     TARGET_SUFFIX=<empty>/-patmos/-mips"
	@echo "|     WCETTOOL=heptane/platin"
	@echo "|     CFLAGS,CPPFLAGS,LDFLAGS,LDLIBS"
	@echo "+---------------------------------------------------------------------------"

TARGET?=host
WCETTOOL?=

#WCETTOOL Heptane : https://gforge.inria.fr/projects/heptane
HEPTANE_HOME?=~/heptane

#TARGET Patmos: http://patmos.compute.dtu.dk/
TCREST_HOME?=~/t-crest-up

BUILD_DIR?=build
TARGET_DIR?=dist
SRC_DIR?=src

ADDITIONAL_HEADERFILES=$(SRC_DIR)/globals.h
ADDITIONAL_SRCFILES=$(SRC_DIR)/comm_shared_buffer.c $(SRC_DIR)/globals.c

CPPFLAGS+=-I $(SRC_DIR)

BENCHMARKS=

ifeq ($(TARGET), patmos)
    CC?=patmos-clang
    CFLAGS+=-O0 -Wfatal-errors -g -std=c11 -nostartfiles -nostdlib -Wno-int-conversion
    CFLAGS+= -DNOSTDLIB 
    LDFLAGS+=-L$(TCREST_HOME)/local/lib \
	     -L$(TCREST_HOME)/local/patmos-unknown-unknown-elf/lib 
    LDLIBS+=-lm #-lc -lnosys -lpatmos -lg -msoft-float
    TARGET_SUFFIX?=-patmos
else ifeq ($(TARGET), mips)
    CC?=mips-linux-gnu-gcc
    CFLAGS+=-O0 -Wfatal-errors -std=c11 -fomit-frame-pointer -fno-builtin -e main -static -mips1 -mfp32 -nostartfiles -nodefaultlibs
    CFLAGS+=-DNOSTDLIB -DNOMATHLIB 
    TARGET_SUFFIX?=-mips
else
    # Host compiler 
    CC?=gcc
    CFLAGS+=-Wfatal-errors -g -std=c11
    LDLIBS+=-lm
    TARGET_SUFFIX?=
endif

ifeq ($(WCETTOOL), heptane)
    #to include the "annot.h"
    CPPFLAGS+=-O0 -I $(HEPTANE_HOME)/benchmarks
    CFLAGS+= -DHEPTANE
else ifeq ($(WCETTOOL), platin)
    CFLAGS+= -mserialize=benchmark.pml 
endif

#####
# Template rule to easily add new benchmark
#  Create the target with the benchmark identifier, then add target for the binary 
####
define BENCH_TEMPLATE
BENCHMARKS+=$(strip $(1))

.PHONY: $(strip $(1))
$(strip $(1)) : $(TARGET_DIR)/$(strip $(1))$(TARGET_SUFFIX)
	
$(TARGET_DIR)/$(strip $(1))$(TARGET_SUFFIX) : $(strip $(2)) $(strip $(3)) $(ADDITIONAL_SRCFILES) $(ADDITIONAL_HEADERFILES) 
	$(CC) $(CFLAGS) $(strip $(4)) $(CPPFLAGS) -o $$@ $(strip $(2)) $(ADDITIONAL_SRCFILES) $(LDFLAGS) $(LDLIBS)
endef

where-am-i=$(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST))
THISSTR2RTSMAKEFILE:=$(call where-am-i)

BENCHSMAKEFILE:=$(shell find $(SRC_DIR) -name 'bench.mk')
include $(BENCHSMAKEFILE)

all: $(filter-out peg% hsdf%, $(BENCHMARKS))
	
all-withpegs: $(BENCHMARKS)
	
all-mips:
	@echo $(THISMAKEFILE)
	$(MAKE) -f $(THISSTR2RTSMAKEFILE) TARGET="mips" all -$(MAKEFLAGS) 

all-patmos:
	$(MAKE) -f $(THISSTR2RTSMAKEFILE) TARGET="patmos" all -$(MAKEFLAGS) 
	
clean:
	rm -f $(TARGET_DIR)/*
	rm -f $(BUILD_DIR)/*

