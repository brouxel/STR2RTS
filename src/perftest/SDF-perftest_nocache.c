#include "SDF-perftest_nocache.h"

buffer_float_t ComplexFIRFilter_58QuadratureDemod_59;
buffer_float_t ComplexFIRFilter_54QuadratureDemod_55;
buffer_float_t ComplexFIRFilter_66QuadratureDemod_67;
buffer_float_t ComplexFIRFilter_62QuadratureDemod_63;
buffer_float_t SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_join[4];
buffer_float_t SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_71NullSink_69;
buffer_float_t QuadratureDemod_59RealFIRFilter_60;
buffer_float_t QuadratureDemod_63RealFIRFilter_64;
buffer_float_t QuadratureDemod_67RealFIRFilter_68;
buffer_float_t TestSource_51WEIGHTED_ROUND_ROBIN_Splitter_70;
buffer_float_t QuadratureDemod_55RealFIRFilter_56;


TestSource_51_t TestSource_51_s;
ComplexFIRFilter_54_t ComplexFIRFilter_54_s;
RealFIRFilter_56_t RealFIRFilter_56_s;
ComplexFIRFilter_54_t ComplexFIRFilter_58_s;
RealFIRFilter_56_t RealFIRFilter_60_s;
ComplexFIRFilter_54_t ComplexFIRFilter_62_s;
RealFIRFilter_56_t RealFIRFilter_64_s;
ComplexFIRFilter_54_t ComplexFIRFilter_66_s;
RealFIRFilter_56_t RealFIRFilter_68_s;

void TestSource_51(){
	FOR(uint32_t, __iter_steady_, 0, <, 16500, __iter_steady_++) {
		push_float(&TestSource_51WEIGHTED_ROUND_ROBIN_Splitter_70, TestSource_51_s.i++) ; 
		if(TestSource_51_s.i == 10000) {
			TestSource_51_s.i = 0 ; 
		}
	}
	ENDFOR
}

void ComplexFIRFilter_54(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		float resultReal = 0.0;
		float resultImag = 0.0;
		FOR(int, i__conflict__1, 0,  < , 400, i__conflict__1++) {
			ComplexFIRFilter_54_s.inputArray[i__conflict__1] = pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[0]) ; 
		}
		ENDFOR
		FOR(int, i__conflict__0, 0,  < , 425, i__conflict__0++) {
			pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[0]) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 400, i++) {
			resultReal = (resultReal + (ComplexFIRFilter_54_s.tapsReal[i] * ComplexFIRFilter_54_s.inputArray[i])) ; 
			resultImag = (resultImag + ComplexFIRFilter_54_s.tapsImag[i]) ; 
		}
		ENDFOR
 {
		ComplexFIRFilter_54_s.phase_correctionReal = ((ComplexFIRFilter_54_s.phase_correctionReal * ComplexFIRFilter_54_s.phase_corr_incrReal) - (ComplexFIRFilter_54_s.phase_correctionImag * ComplexFIRFilter_54_s.phase_corr_incrImag)) ; 
		ComplexFIRFilter_54_s.phase_correctionImag = ((ComplexFIRFilter_54_s.phase_correctionReal * ComplexFIRFilter_54_s.phase_corr_incrImag) - (ComplexFIRFilter_54_s.phase_correctionImag * ComplexFIRFilter_54_s.phase_corr_incrReal)) ; 
		resultReal = ((resultReal * ComplexFIRFilter_54_s.phase_correctionReal) - (resultImag * ComplexFIRFilter_54_s.phase_correctionImag)) ; 
		resultImag = ((resultReal * ComplexFIRFilter_54_s.phase_correctionImag) - (resultImag * ComplexFIRFilter_54_s.phase_correctionReal)) ; 
	}
		push_float(&ComplexFIRFilter_54QuadratureDemod_55, resultReal) ; 
		push_float(&ComplexFIRFilter_54QuadratureDemod_55, resultImag) ; 
	}
	ENDFOR
}

void QuadratureDemod_55() {
	float lastValReal = 0.0;
	float productReal = 0.0;
	float valReal = 0.0;
	float lastValImag = 0.0;
	float productImag = 0.0;
	float valImag = 0.0;
	lastValReal = peek_float(&ComplexFIRFilter_54QuadratureDemod_55, 3) ; 
	lastValImag = peek_float(&ComplexFIRFilter_54QuadratureDemod_55, 2) ; 
	FOR(int, i, 5,  > , 0, i--) {
		valImag = pop_float(&ComplexFIRFilter_54QuadratureDemod_55) ; 
		valReal = pop_float(&ComplexFIRFilter_54QuadratureDemod_55) ; 
		productReal = ((valReal * lastValReal) - (valImag * lastValImag)) ; 
		productImag = ((valReal * -lastValImag) - (valImag * lastValReal)) ; 
		lastValReal = valReal ; 
		lastValImag = valImag ; 
		push_float(&QuadratureDemod_55RealFIRFilter_56, (1.0 * ((float) asin((productImag / ((float) sqrt((((float) pow(productReal, 2.0)) + ((float) pow(productImag, 2.0)))))))))) ; 
	}
	ENDFOR
}


void RealFIRFilter_56() {
	float sum = 0.0;
	FOR(int, i__conflict__0, 0,  < , 5, i__conflict__0++) {
		sum = (sum + (RealFIRFilter_56_s.taps[i__conflict__0] * pop_float(&QuadratureDemod_55RealFIRFilter_56))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 15, i++) {
		sum = (sum + (RealFIRFilter_56_s.taps[(i + 5)] + peek_float(&QuadratureDemod_55RealFIRFilter_56, i))) ; 
	}
	ENDFOR
	push_float(&SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_join[0], sum) ; 
}


void ComplexFIRFilter_58(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		float resultReal = 0.0;
		float resultImag = 0.0;
		FOR(int, i__conflict__1, 0,  < , 400, i__conflict__1++) {
			ComplexFIRFilter_58_s.inputArray[i__conflict__1] = pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[1]) ; 
		}
		ENDFOR
		FOR(int, i__conflict__0, 0,  < , 425, i__conflict__0++) {
			pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[1]) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 400, i++) {
			resultReal = (resultReal + (ComplexFIRFilter_58_s.tapsReal[i] * ComplexFIRFilter_58_s.inputArray[i])) ; 
			resultImag = (resultImag + ComplexFIRFilter_58_s.tapsImag[i]) ; 
		}
		ENDFOR
 {
		ComplexFIRFilter_58_s.phase_correctionReal = ((ComplexFIRFilter_58_s.phase_correctionReal * ComplexFIRFilter_58_s.phase_corr_incrReal) - (ComplexFIRFilter_58_s.phase_correctionImag * ComplexFIRFilter_58_s.phase_corr_incrImag)) ; 
		ComplexFIRFilter_58_s.phase_correctionImag = ((ComplexFIRFilter_58_s.phase_correctionReal * ComplexFIRFilter_58_s.phase_corr_incrImag) - (ComplexFIRFilter_58_s.phase_correctionImag * ComplexFIRFilter_58_s.phase_corr_incrReal)) ; 
		resultReal = ((resultReal * ComplexFIRFilter_58_s.phase_correctionReal) - (resultImag * ComplexFIRFilter_58_s.phase_correctionImag)) ; 
		resultImag = ((resultReal * ComplexFIRFilter_58_s.phase_correctionImag) - (resultImag * ComplexFIRFilter_58_s.phase_correctionReal)) ; 
	}
		push_float(&ComplexFIRFilter_58QuadratureDemod_59, resultReal) ; 
		push_float(&ComplexFIRFilter_58QuadratureDemod_59, resultImag) ; 
	}
	ENDFOR
}

void QuadratureDemod_59() {
	float lastValReal = 0.0;
	float productReal = 0.0;
	float valReal = 0.0;
	float lastValImag = 0.0;
	float productImag = 0.0;
	float valImag = 0.0;
	lastValReal = peek_float(&ComplexFIRFilter_58QuadratureDemod_59, 3) ; 
	lastValImag = peek_float(&ComplexFIRFilter_58QuadratureDemod_59, 2) ; 
	FOR(int, i, 5,  > , 0, i--) {
		valImag = pop_float(&ComplexFIRFilter_58QuadratureDemod_59) ; 
		valReal = pop_float(&ComplexFIRFilter_58QuadratureDemod_59) ; 
		productReal = ((valReal * lastValReal) - (valImag * lastValImag)) ; 
		productImag = ((valReal * -lastValImag) - (valImag * lastValReal)) ; 
		lastValReal = valReal ; 
		lastValImag = valImag ; 
		push_float(&QuadratureDemod_59RealFIRFilter_60, (1.0 * ((float) asin((productImag / ((float) sqrt((((float) pow(productReal, 2.0)) + ((float) pow(productImag, 2.0)))))))))) ; 
	}
	ENDFOR
}


void RealFIRFilter_60() {
	float sum = 0.0;
	FOR(int, i__conflict__0, 0,  < , 5, i__conflict__0++) {
		sum = (sum + (RealFIRFilter_60_s.taps[i__conflict__0] * pop_float(&QuadratureDemod_59RealFIRFilter_60))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 15, i++) {
		sum = (sum + (RealFIRFilter_60_s.taps[(i + 5)] + peek_float(&QuadratureDemod_59RealFIRFilter_60, i))) ; 
	}
	ENDFOR
	push_float(&SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_join[1], sum) ; 
}


void ComplexFIRFilter_62(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		float resultReal = 0.0;
		float resultImag = 0.0;
		FOR(int, i__conflict__1, 0,  < , 400, i__conflict__1++) {
			ComplexFIRFilter_62_s.inputArray[i__conflict__1] = pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[2]) ; 
		}
		ENDFOR
		FOR(int, i__conflict__0, 0,  < , 425, i__conflict__0++) {
			pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[2]) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 400, i++) {
			resultReal = (resultReal + (ComplexFIRFilter_62_s.tapsReal[i] * ComplexFIRFilter_62_s.inputArray[i])) ; 
			resultImag = (resultImag + ComplexFIRFilter_62_s.tapsImag[i]) ; 
		}
		ENDFOR
 {
		ComplexFIRFilter_62_s.phase_correctionReal = ((ComplexFIRFilter_62_s.phase_correctionReal * ComplexFIRFilter_62_s.phase_corr_incrReal) - (ComplexFIRFilter_62_s.phase_correctionImag * ComplexFIRFilter_62_s.phase_corr_incrImag)) ; 
		ComplexFIRFilter_62_s.phase_correctionImag = ((ComplexFIRFilter_62_s.phase_correctionReal * ComplexFIRFilter_62_s.phase_corr_incrImag) - (ComplexFIRFilter_62_s.phase_correctionImag * ComplexFIRFilter_62_s.phase_corr_incrReal)) ; 
		resultReal = ((resultReal * ComplexFIRFilter_62_s.phase_correctionReal) - (resultImag * ComplexFIRFilter_62_s.phase_correctionImag)) ; 
		resultImag = ((resultReal * ComplexFIRFilter_62_s.phase_correctionImag) - (resultImag * ComplexFIRFilter_62_s.phase_correctionReal)) ; 
	}
		push_float(&ComplexFIRFilter_62QuadratureDemod_63, resultReal) ; 
		push_float(&ComplexFIRFilter_62QuadratureDemod_63, resultImag) ; 
	}
	ENDFOR
}

void QuadratureDemod_63() {
	float lastValReal = 0.0;
	float productReal = 0.0;
	float valReal = 0.0;
	float lastValImag = 0.0;
	float productImag = 0.0;
	float valImag = 0.0;
	lastValReal = peek_float(&ComplexFIRFilter_62QuadratureDemod_63, 3) ; 
	lastValImag = peek_float(&ComplexFIRFilter_62QuadratureDemod_63, 2) ; 
	FOR(int, i, 5,  > , 0, i--) {
		valImag = pop_float(&ComplexFIRFilter_62QuadratureDemod_63) ; 
		valReal = pop_float(&ComplexFIRFilter_62QuadratureDemod_63) ; 
		productReal = ((valReal * lastValReal) - (valImag * lastValImag)) ; 
		productImag = ((valReal * -lastValImag) - (valImag * lastValReal)) ; 
		lastValReal = valReal ; 
		lastValImag = valImag ; 
		push_float(&QuadratureDemod_63RealFIRFilter_64, (1.0 * ((float) asin((productImag / ((float) sqrt((((float) pow(productReal, 2.0)) + ((float) pow(productImag, 2.0)))))))))) ; 
	}
	ENDFOR
}


void RealFIRFilter_64() {
	float sum = 0.0;
	FOR(int, i__conflict__0, 0,  < , 5, i__conflict__0++) {
		sum = (sum + (RealFIRFilter_64_s.taps[i__conflict__0] * pop_float(&QuadratureDemod_63RealFIRFilter_64))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 15, i++) {
		sum = (sum + (RealFIRFilter_64_s.taps[(i + 5)] + peek_float(&QuadratureDemod_63RealFIRFilter_64, i))) ; 
	}
	ENDFOR
	push_float(&SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_join[2], sum) ; 
}


void ComplexFIRFilter_66(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		float resultReal = 0.0;
		float resultImag = 0.0;
		FOR(int, i__conflict__1, 0,  < , 400, i__conflict__1++) {
			ComplexFIRFilter_66_s.inputArray[i__conflict__1] = pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[3]) ; 
		}
		ENDFOR
		FOR(int, i__conflict__0, 0,  < , 425, i__conflict__0++) {
			pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[3]) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 400, i++) {
			resultReal = (resultReal + (ComplexFIRFilter_66_s.tapsReal[i] * ComplexFIRFilter_66_s.inputArray[i])) ; 
			resultImag = (resultImag + ComplexFIRFilter_66_s.tapsImag[i]) ; 
		}
		ENDFOR
 {
		ComplexFIRFilter_66_s.phase_correctionReal = ((ComplexFIRFilter_66_s.phase_correctionReal * ComplexFIRFilter_66_s.phase_corr_incrReal) - (ComplexFIRFilter_66_s.phase_correctionImag * ComplexFIRFilter_66_s.phase_corr_incrImag)) ; 
		ComplexFIRFilter_66_s.phase_correctionImag = ((ComplexFIRFilter_66_s.phase_correctionReal * ComplexFIRFilter_66_s.phase_corr_incrImag) - (ComplexFIRFilter_66_s.phase_correctionImag * ComplexFIRFilter_66_s.phase_corr_incrReal)) ; 
		resultReal = ((resultReal * ComplexFIRFilter_66_s.phase_correctionReal) - (resultImag * ComplexFIRFilter_66_s.phase_correctionImag)) ; 
		resultImag = ((resultReal * ComplexFIRFilter_66_s.phase_correctionImag) - (resultImag * ComplexFIRFilter_66_s.phase_correctionReal)) ; 
	}
		push_float(&ComplexFIRFilter_66QuadratureDemod_67, resultReal) ; 
		push_float(&ComplexFIRFilter_66QuadratureDemod_67, resultImag) ; 
	}
	ENDFOR
}

void QuadratureDemod_67() {
	float lastValReal = 0.0;
	float productReal = 0.0;
	float valReal = 0.0;
	float lastValImag = 0.0;
	float productImag = 0.0;
	float valImag = 0.0;
	lastValReal = peek_float(&ComplexFIRFilter_66QuadratureDemod_67, 3) ; 
	lastValImag = peek_float(&ComplexFIRFilter_66QuadratureDemod_67, 2) ; 
	FOR(int, i, 5,  > , 0, i--) {
		valImag = pop_float(&ComplexFIRFilter_66QuadratureDemod_67) ; 
		valReal = pop_float(&ComplexFIRFilter_66QuadratureDemod_67) ; 
		productReal = ((valReal * lastValReal) - (valImag * lastValImag)) ; 
		productImag = ((valReal * -lastValImag) - (valImag * lastValReal)) ; 
		lastValReal = valReal ; 
		lastValImag = valImag ; 
		push_float(&QuadratureDemod_67RealFIRFilter_68, (1.0 * ((float) asin((productImag / ((float) sqrt((((float) pow(productReal, 2.0)) + ((float) pow(productImag, 2.0)))))))))) ; 
	}
	ENDFOR
}


void RealFIRFilter_68() {
	float sum = 0.0;
	FOR(int, i__conflict__0, 0,  < , 5, i__conflict__0++) {
		sum = (sum + (RealFIRFilter_68_s.taps[i__conflict__0] * pop_float(&QuadratureDemod_67RealFIRFilter_68))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 15, i++) {
		sum = (sum + (RealFIRFilter_68_s.taps[(i + 5)] + peek_float(&QuadratureDemod_67RealFIRFilter_68, i))) ; 
	}
	ENDFOR
	push_float(&SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_join[3], sum) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_70() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 825, __iter_tok_++)
				push_float(&SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[__iter_dec_], pop_float(&TestSource_51WEIGHTED_ROUND_ROBIN_Splitter_70));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_71() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_71NullSink_69, pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_join[__iter_]));
	ENDFOR
}

void NullSink_69(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		printf("%.10f", pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_71NullSink_69));
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&ComplexFIRFilter_58QuadratureDemod_59);
	init_buffer_float(&ComplexFIRFilter_54QuadratureDemod_55);
	init_buffer_float(&ComplexFIRFilter_66QuadratureDemod_67);
	init_buffer_float(&ComplexFIRFilter_62QuadratureDemod_63);
	FOR(int, __iter_init_0_, 0, <, 4, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 4, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_71NullSink_69);
	init_buffer_float(&QuadratureDemod_59RealFIRFilter_60);
	init_buffer_float(&QuadratureDemod_63RealFIRFilter_64);
	init_buffer_float(&QuadratureDemod_67RealFIRFilter_68);
	init_buffer_float(&TestSource_51WEIGHTED_ROUND_ROBIN_Splitter_70);
	init_buffer_float(&QuadratureDemod_55RealFIRFilter_56);
// --- init: TestSource_51
	 {
	TestSource_51_s.i = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 49500, __iter_init_++) {
		push_float(&TestSource_51WEIGHTED_ROUND_ROBIN_Splitter_70, TestSource_51_s.i++) ; 
		if(TestSource_51_s.i == 10000) {
			TestSource_51_s.i = 0 ; 
		}
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_70
	FOR(uint32_t, __iter_init_, 0, <, 15, __iter_init_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 825, __iter_tok_++)
				push_float(&SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[__iter_dec_], pop_float(&TestSource_51WEIGHTED_ROUND_ROBIN_Splitter_70));
			ENDFOR
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: ComplexFIRFilter_54
	 {
	ComplexFIRFilter_54_s.phase_corr_incrReal = 1.0 ; 
	ComplexFIRFilter_54_s.phase_corr_incrImag = 0.0 ; 
	ComplexFIRFilter_54_s.phase_correctionReal = 1.0 ; 
	ComplexFIRFilter_54_s.phase_correctionImag = 0.0 ; 
 {
	FOR(int, index, 0,  < , 400, index++) {
		ComplexFIRFilter_54_s.tapsReal[index] = (2.0 * ((float) cos(((1.9745196 * index) * (0.54 - (0.46 * ((float) cos(((6.2831855 * index) / 399.0))))))))) ; 
		ComplexFIRFilter_54_s.tapsImag[index] = ((-2.0 * ((float) sin((1.9745196 * index)))) * (0.54 - (0.46 * ((float) cos(((6.2831855 * index) / 399.0)))))) ; 
	}
	ENDFOR
	ComplexFIRFilter_54_s.phase_corr_incrReal = -0.062805444 ; 
	ComplexFIRFilter_54_s.phase_corr_incrImag = -0.9980258 ; 
}
}
	FOR(uint32_t, __iter_init_, 0, <, 15, __iter_init_++) {
		float resultReal = 0.0;
		float resultImag = 0.0;
		FOR(int, i__conflict__1, 0,  < , 400, i__conflict__1++) {
			ComplexFIRFilter_54_s.inputArray[i__conflict__1] = pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[0]) ; 
		}
		ENDFOR
		FOR(int, i__conflict__0, 0,  < , 425, i__conflict__0++) {
			pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[0]) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 400, i++) {
			resultReal = (resultReal + (ComplexFIRFilter_54_s.tapsReal[i] * ComplexFIRFilter_54_s.inputArray[i])) ; 
			resultImag = (resultImag + ComplexFIRFilter_54_s.tapsImag[i]) ; 
		}
		ENDFOR
 {
		ComplexFIRFilter_54_s.phase_correctionReal = ((ComplexFIRFilter_54_s.phase_correctionReal * ComplexFIRFilter_54_s.phase_corr_incrReal) - (ComplexFIRFilter_54_s.phase_correctionImag * ComplexFIRFilter_54_s.phase_corr_incrImag)) ; 
		ComplexFIRFilter_54_s.phase_correctionImag = ((ComplexFIRFilter_54_s.phase_correctionReal * ComplexFIRFilter_54_s.phase_corr_incrImag) - (ComplexFIRFilter_54_s.phase_correctionImag * ComplexFIRFilter_54_s.phase_corr_incrReal)) ; 
		resultReal = ((resultReal * ComplexFIRFilter_54_s.phase_correctionReal) - (resultImag * ComplexFIRFilter_54_s.phase_correctionImag)) ; 
		resultImag = ((resultReal * ComplexFIRFilter_54_s.phase_correctionImag) - (resultImag * ComplexFIRFilter_54_s.phase_correctionReal)) ; 
	}
		push_float(&ComplexFIRFilter_54QuadratureDemod_55, resultReal) ; 
		push_float(&ComplexFIRFilter_54QuadratureDemod_55, resultImag) ; 
	}
	ENDFOR
//--------------------------------
// --- init: QuadratureDemod_55
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		float lastValReal = 0.0;
		float productReal = 0.0;
		float valReal = 0.0;
		float lastValImag = 0.0;
		float productImag = 0.0;
		float valImag = 0.0;
		lastValReal = peek_float(&ComplexFIRFilter_54QuadratureDemod_55, 3) ; 
		lastValImag = peek_float(&ComplexFIRFilter_54QuadratureDemod_55, 2) ; 
		FOR(int, i, 5,  > , 0, i--) {
			valImag = pop_float(&ComplexFIRFilter_54QuadratureDemod_55) ; 
			valReal = pop_float(&ComplexFIRFilter_54QuadratureDemod_55) ; 
			productReal = ((valReal * lastValReal) - (valImag * lastValImag)) ; 
			productImag = ((valReal * -lastValImag) - (valImag * lastValReal)) ; 
			lastValReal = valReal ; 
			lastValImag = valImag ; 
			push_float(&QuadratureDemod_55RealFIRFilter_56, (1.0 * ((float) asin((productImag / ((float) sqrt((((float) pow(productReal, 2.0)) + ((float) pow(productImag, 2.0)))))))))) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: RealFIRFilter_56
	 {
 {
	FOR(int, index, 0,  < , 20, index++) {
		if((index - 9.5) != 0.0) {
			RealFIRFilter_56_s.taps[index] = (1.0 * (((((float) sin((3.1415927 * (index - 9.5)))) / 3.1415927) / (index - 9.5)) * (0.54 - (0.46 * ((float) cos(((6.2831855 * index) / 19.0))))))) ; 
		}
	}
	ENDFOR
}
}
//--------------------------------
// --- init: ComplexFIRFilter_58
	 {
	ComplexFIRFilter_58_s.phase_corr_incrReal = 1.0 ; 
	ComplexFIRFilter_58_s.phase_corr_incrImag = 0.0 ; 
	ComplexFIRFilter_58_s.phase_correctionReal = 1.0 ; 
	ComplexFIRFilter_58_s.phase_correctionImag = 0.0 ; 
 {
	FOR(int, index, 0,  < , 400, index++) {
		ComplexFIRFilter_58_s.tapsReal[index] = (2.0 * ((float) cos(((1.9716636 * index) * (0.54 - (0.46 * ((float) cos(((6.2831855 * index) / 399.0))))))))) ; 
		ComplexFIRFilter_58_s.tapsImag[index] = ((-2.0 * ((float) sin((1.9716636 * index)))) * (0.54 - (0.46 * ((float) cos(((6.2831855 * index) / 399.0)))))) ; 
	}
	ENDFOR
	ComplexFIRFilter_58_s.phase_corr_incrReal = 0.7501165 ; 
	ComplexFIRFilter_58_s.phase_corr_incrImag = 0.66130567 ; 
}
}
	FOR(uint32_t, __iter_init_, 0, <, 15, __iter_init_++) {
		float resultReal = 0.0;
		float resultImag = 0.0;
		FOR(int, i__conflict__1, 0,  < , 400, i__conflict__1++) {
			ComplexFIRFilter_58_s.inputArray[i__conflict__1] = pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[1]) ; 
		}
		ENDFOR
		FOR(int, i__conflict__0, 0,  < , 425, i__conflict__0++) {
			pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[1]) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 400, i++) {
			resultReal = (resultReal + (ComplexFIRFilter_58_s.tapsReal[i] * ComplexFIRFilter_58_s.inputArray[i])) ; 
			resultImag = (resultImag + ComplexFIRFilter_58_s.tapsImag[i]) ; 
		}
		ENDFOR
 {
		ComplexFIRFilter_58_s.phase_correctionReal = ((ComplexFIRFilter_58_s.phase_correctionReal * ComplexFIRFilter_58_s.phase_corr_incrReal) - (ComplexFIRFilter_58_s.phase_correctionImag * ComplexFIRFilter_58_s.phase_corr_incrImag)) ; 
		ComplexFIRFilter_58_s.phase_correctionImag = ((ComplexFIRFilter_58_s.phase_correctionReal * ComplexFIRFilter_58_s.phase_corr_incrImag) - (ComplexFIRFilter_58_s.phase_correctionImag * ComplexFIRFilter_58_s.phase_corr_incrReal)) ; 
		resultReal = ((resultReal * ComplexFIRFilter_58_s.phase_correctionReal) - (resultImag * ComplexFIRFilter_58_s.phase_correctionImag)) ; 
		resultImag = ((resultReal * ComplexFIRFilter_58_s.phase_correctionImag) - (resultImag * ComplexFIRFilter_58_s.phase_correctionReal)) ; 
	}
		push_float(&ComplexFIRFilter_58QuadratureDemod_59, resultReal) ; 
		push_float(&ComplexFIRFilter_58QuadratureDemod_59, resultImag) ; 
	}
	ENDFOR
//--------------------------------
// --- init: QuadratureDemod_59
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		float lastValReal = 0.0;
		float productReal = 0.0;
		float valReal = 0.0;
		float lastValImag = 0.0;
		float productImag = 0.0;
		float valImag = 0.0;
		lastValReal = peek_float(&ComplexFIRFilter_58QuadratureDemod_59, 3) ; 
		lastValImag = peek_float(&ComplexFIRFilter_58QuadratureDemod_59, 2) ; 
		FOR(int, i, 5,  > , 0, i--) {
			valImag = pop_float(&ComplexFIRFilter_58QuadratureDemod_59) ; 
			valReal = pop_float(&ComplexFIRFilter_58QuadratureDemod_59) ; 
			productReal = ((valReal * lastValReal) - (valImag * lastValImag)) ; 
			productImag = ((valReal * -lastValImag) - (valImag * lastValReal)) ; 
			lastValReal = valReal ; 
			lastValImag = valImag ; 
			push_float(&QuadratureDemod_59RealFIRFilter_60, (1.0 * ((float) asin((productImag / ((float) sqrt((((float) pow(productReal, 2.0)) + ((float) pow(productImag, 2.0)))))))))) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: RealFIRFilter_60
	 {
 {
	FOR(int, index, 0,  < , 20, index++) {
		if((index - 9.5) != 0.0) {
			RealFIRFilter_60_s.taps[index] = (1.0 * (((((float) sin((3.1415927 * (index - 9.5)))) / 3.1415927) / (index - 9.5)) * (0.54 - (0.46 * ((float) cos(((6.2831855 * index) / 19.0))))))) ; 
		}
	}
	ENDFOR
}
}
//--------------------------------
// --- init: ComplexFIRFilter_62
	 {
	ComplexFIRFilter_62_s.phase_corr_incrReal = 1.0 ; 
	ComplexFIRFilter_62_s.phase_corr_incrImag = 0.0 ; 
	ComplexFIRFilter_62_s.phase_correctionReal = 1.0 ; 
	ComplexFIRFilter_62_s.phase_correctionImag = 0.0 ; 
 {
	FOR(int, index, 0,  < , 400, index++) {
		ComplexFIRFilter_62_s.tapsReal[index] = (2.0 * ((float) cos(((1.9688076 * index) * (0.54 - (0.46 * ((float) cos(((6.2831855 * index) / 399.0))))))))) ; 
		ComplexFIRFilter_62_s.tapsImag[index] = ((-2.0 * ((float) sin((1.9688076 * index)))) * (0.54 - (0.46 * ((float) cos(((6.2831855 * index) / 399.0)))))) ; 
	}
	ENDFOR
	ComplexFIRFilter_62_s.phase_corr_incrReal = -0.9980266 ; 
	ComplexFIRFilter_62_s.phase_corr_incrImag = 0.06279211 ; 
}
}
	FOR(uint32_t, __iter_init_, 0, <, 15, __iter_init_++) {
		float resultReal = 0.0;
		float resultImag = 0.0;
		FOR(int, i__conflict__1, 0,  < , 400, i__conflict__1++) {
			ComplexFIRFilter_62_s.inputArray[i__conflict__1] = pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[2]) ; 
		}
		ENDFOR
		FOR(int, i__conflict__0, 0,  < , 425, i__conflict__0++) {
			pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[2]) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 400, i++) {
			resultReal = (resultReal + (ComplexFIRFilter_62_s.tapsReal[i] * ComplexFIRFilter_62_s.inputArray[i])) ; 
			resultImag = (resultImag + ComplexFIRFilter_62_s.tapsImag[i]) ; 
		}
		ENDFOR
 {
		ComplexFIRFilter_62_s.phase_correctionReal = ((ComplexFIRFilter_62_s.phase_correctionReal * ComplexFIRFilter_62_s.phase_corr_incrReal) - (ComplexFIRFilter_62_s.phase_correctionImag * ComplexFIRFilter_62_s.phase_corr_incrImag)) ; 
		ComplexFIRFilter_62_s.phase_correctionImag = ((ComplexFIRFilter_62_s.phase_correctionReal * ComplexFIRFilter_62_s.phase_corr_incrImag) - (ComplexFIRFilter_62_s.phase_correctionImag * ComplexFIRFilter_62_s.phase_corr_incrReal)) ; 
		resultReal = ((resultReal * ComplexFIRFilter_62_s.phase_correctionReal) - (resultImag * ComplexFIRFilter_62_s.phase_correctionImag)) ; 
		resultImag = ((resultReal * ComplexFIRFilter_62_s.phase_correctionImag) - (resultImag * ComplexFIRFilter_62_s.phase_correctionReal)) ; 
	}
		push_float(&ComplexFIRFilter_62QuadratureDemod_63, resultReal) ; 
		push_float(&ComplexFIRFilter_62QuadratureDemod_63, resultImag) ; 
	}
	ENDFOR
//--------------------------------
// --- init: QuadratureDemod_63
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		float lastValReal = 0.0;
		float productReal = 0.0;
		float valReal = 0.0;
		float lastValImag = 0.0;
		float productImag = 0.0;
		float valImag = 0.0;
		lastValReal = peek_float(&ComplexFIRFilter_62QuadratureDemod_63, 3) ; 
		lastValImag = peek_float(&ComplexFIRFilter_62QuadratureDemod_63, 2) ; 
		FOR(int, i, 5,  > , 0, i--) {
			valImag = pop_float(&ComplexFIRFilter_62QuadratureDemod_63) ; 
			valReal = pop_float(&ComplexFIRFilter_62QuadratureDemod_63) ; 
			productReal = ((valReal * lastValReal) - (valImag * lastValImag)) ; 
			productImag = ((valReal * -lastValImag) - (valImag * lastValReal)) ; 
			lastValReal = valReal ; 
			lastValImag = valImag ; 
			push_float(&QuadratureDemod_63RealFIRFilter_64, (1.0 * ((float) asin((productImag / ((float) sqrt((((float) pow(productReal, 2.0)) + ((float) pow(productImag, 2.0)))))))))) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: RealFIRFilter_64
	 {
 {
	FOR(int, index, 0,  < , 20, index++) {
		if((index - 9.5) != 0.0) {
			RealFIRFilter_64_s.taps[index] = (1.0 * (((((float) sin((3.1415927 * (index - 9.5)))) / 3.1415927) / (index - 9.5)) * (0.54 - (0.46 * ((float) cos(((6.2831855 * index) / 19.0))))))) ; 
		}
	}
	ENDFOR
}
}
//--------------------------------
// --- init: ComplexFIRFilter_66
	 {
	ComplexFIRFilter_66_s.phase_corr_incrReal = 1.0 ; 
	ComplexFIRFilter_66_s.phase_corr_incrImag = 0.0 ; 
	ComplexFIRFilter_66_s.phase_correctionReal = 1.0 ; 
	ComplexFIRFilter_66_s.phase_correctionImag = 0.0 ; 
 {
	FOR(int, index, 0,  < , 400, index++) {
		ComplexFIRFilter_66_s.tapsReal[index] = (2.0 * ((float) cos(((2.0868745 * index) * (0.54 - (0.46 * ((float) cos(((6.2831855 * index) / 399.0))))))))) ; 
		ComplexFIRFilter_66_s.tapsImag[index] = ((-2.0 * ((float) sin((2.0868745 * index)))) * (0.54 - (0.46 * ((float) cos(((6.2831855 * index) / 399.0)))))) ; 
	}
	ENDFOR
	ComplexFIRFilter_66_s.phase_corr_incrReal = 0.99690205 ; 
	ComplexFIRFilter_66_s.phase_corr_incrImag = -0.0786533 ; 
}
}
	FOR(uint32_t, __iter_init_, 0, <, 15, __iter_init_++) {
		float resultReal = 0.0;
		float resultImag = 0.0;
		FOR(int, i__conflict__1, 0,  < , 400, i__conflict__1++) {
			ComplexFIRFilter_66_s.inputArray[i__conflict__1] = pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[3]) ; 
		}
		ENDFOR
		FOR(int, i__conflict__0, 0,  < , 425, i__conflict__0++) {
			pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[3]) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 400, i++) {
			resultReal = (resultReal + (ComplexFIRFilter_66_s.tapsReal[i] * ComplexFIRFilter_66_s.inputArray[i])) ; 
			resultImag = (resultImag + ComplexFIRFilter_66_s.tapsImag[i]) ; 
		}
		ENDFOR
 {
		ComplexFIRFilter_66_s.phase_correctionReal = ((ComplexFIRFilter_66_s.phase_correctionReal * ComplexFIRFilter_66_s.phase_corr_incrReal) - (ComplexFIRFilter_66_s.phase_correctionImag * ComplexFIRFilter_66_s.phase_corr_incrImag)) ; 
		ComplexFIRFilter_66_s.phase_correctionImag = ((ComplexFIRFilter_66_s.phase_correctionReal * ComplexFIRFilter_66_s.phase_corr_incrImag) - (ComplexFIRFilter_66_s.phase_correctionImag * ComplexFIRFilter_66_s.phase_corr_incrReal)) ; 
		resultReal = ((resultReal * ComplexFIRFilter_66_s.phase_correctionReal) - (resultImag * ComplexFIRFilter_66_s.phase_correctionImag)) ; 
		resultImag = ((resultReal * ComplexFIRFilter_66_s.phase_correctionImag) - (resultImag * ComplexFIRFilter_66_s.phase_correctionReal)) ; 
	}
		push_float(&ComplexFIRFilter_66QuadratureDemod_67, resultReal) ; 
		push_float(&ComplexFIRFilter_66QuadratureDemod_67, resultImag) ; 
	}
	ENDFOR
//--------------------------------
// --- init: QuadratureDemod_67
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		float lastValReal = 0.0;
		float productReal = 0.0;
		float valReal = 0.0;
		float lastValImag = 0.0;
		float productImag = 0.0;
		float valImag = 0.0;
		lastValReal = peek_float(&ComplexFIRFilter_66QuadratureDemod_67, 3) ; 
		lastValImag = peek_float(&ComplexFIRFilter_66QuadratureDemod_67, 2) ; 
		FOR(int, i, 5,  > , 0, i--) {
			valImag = pop_float(&ComplexFIRFilter_66QuadratureDemod_67) ; 
			valReal = pop_float(&ComplexFIRFilter_66QuadratureDemod_67) ; 
			productReal = ((valReal * lastValReal) - (valImag * lastValImag)) ; 
			productImag = ((valReal * -lastValImag) - (valImag * lastValReal)) ; 
			lastValReal = valReal ; 
			lastValImag = valImag ; 
			push_float(&QuadratureDemod_67RealFIRFilter_68, (1.0 * ((float) asin((productImag / ((float) sqrt((((float) pow(productReal, 2.0)) + ((float) pow(productImag, 2.0)))))))))) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: RealFIRFilter_68
	 {
 {
	FOR(int, index, 0,  < , 20, index++) {
		if((index - 9.5) != 0.0) {
			RealFIRFilter_68_s.taps[index] = (1.0 * (((((float) sin((3.1415927 * (index - 9.5)))) / 3.1415927) / (index - 9.5)) * (0.54 - (0.46 * ((float) cos(((6.2831855 * index) / 19.0))))))) ; 
		}
	}
	ENDFOR
}
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		TestSource_51();
		WEIGHTED_ROUND_ROBIN_Splitter_70();
			ComplexFIRFilter_54();
			QuadratureDemod_55();
			RealFIRFilter_56();
			ComplexFIRFilter_58();
			QuadratureDemod_59();
			RealFIRFilter_60();
			ComplexFIRFilter_62();
			QuadratureDemod_63();
			RealFIRFilter_64();
			ComplexFIRFilter_66();
			QuadratureDemod_67();
			RealFIRFilter_68();
		WEIGHTED_ROUND_ROBIN_Joiner_71();
		NullSink_69();
	ENDFOR
	return EXIT_SUCCESS;
}
