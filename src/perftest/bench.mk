BENCH:=sdf_perftest_nocache
BENCHSRCFILE:=$(SRC_DIR)/perftest/SDF-perftest_nocache.c
BENCHHFILE:=$(SRC_DIR)/perftest/SDF-perftest_nocache.h
BENCHEXTRAFLAGS:=-DBUF_SIZEMAX=33000

$(eval $(call BENCH_TEMPLATE, $(BENCH), $(BENCHSRCFILE), $(BENCHHFILE), $(BENCHEXTRAFLAGS)))
