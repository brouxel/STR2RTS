#include "SDF-perftest.h"

buffer_float_t ComplexFIRFilter_58QuadratureDemod_59;
buffer_float_t ComplexFIRFilter_54QuadratureDemod_55;
buffer_float_t ComplexFIRFilter_66QuadratureDemod_67;
buffer_float_t ComplexFIRFilter_62QuadratureDemod_63;
buffer_float_t SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_join[4];
buffer_float_t SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_71NullSink_69;
buffer_float_t QuadratureDemod_59RealFIRFilter_60;
buffer_float_t QuadratureDemod_63RealFIRFilter_64;
buffer_float_t QuadratureDemod_67RealFIRFilter_68;
buffer_float_t TestSource_51WEIGHTED_ROUND_ROBIN_Splitter_70;
buffer_float_t QuadratureDemod_55RealFIRFilter_56;


TestSource_51_t TestSource_51_s;
ComplexFIRFilter_54_t ComplexFIRFilter_54_s;
RealFIRFilter_56_t RealFIRFilter_56_s;
ComplexFIRFilter_54_t ComplexFIRFilter_58_s;
RealFIRFilter_56_t RealFIRFilter_60_s;
ComplexFIRFilter_54_t ComplexFIRFilter_62_s;
RealFIRFilter_56_t RealFIRFilter_64_s;
ComplexFIRFilter_54_t ComplexFIRFilter_66_s;
RealFIRFilter_56_t RealFIRFilter_68_s;

void TestSource(buffer_float_t *chanout) {
		push_float(&(*chanout), TestSource_51_s.i++) ; 
		if(TestSource_51_s.i == 10000) {
			TestSource_51_s.i = 0 ; 
		}
	}


void TestSource_51() {
	FOR(uint32_t, __iter_steady_, 0, <, 16500, __iter_steady_++)
		TestSource(&(TestSource_51WEIGHTED_ROUND_ROBIN_Splitter_70));
	ENDFOR
}

void ComplexFIRFilter(buffer_float_t *chanin, buffer_float_t *chanout) {
		float resultReal = 0.0;
		float resultImag = 0.0;
		FOR(int, i__conflict__1, 0,  < , 400, i__conflict__1++) {
			ComplexFIRFilter_54_s.inputArray[i__conflict__1] = pop_float(&(*chanin)) ; 
		}
		ENDFOR
		FOR(int, i__conflict__0, 0,  < , 425, i__conflict__0++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 400, i++) {
			resultReal = (resultReal + (ComplexFIRFilter_54_s.tapsReal[i] * ComplexFIRFilter_54_s.inputArray[i])) ; 
			resultImag = (resultImag + ComplexFIRFilter_54_s.tapsImag[i]) ; 
		}
		ENDFOR
 {
		ComplexFIRFilter_54_s.phase_correctionReal = ((ComplexFIRFilter_54_s.phase_correctionReal * ComplexFIRFilter_54_s.phase_corr_incrReal) - (ComplexFIRFilter_54_s.phase_correctionImag * ComplexFIRFilter_54_s.phase_corr_incrImag)) ; 
		ComplexFIRFilter_54_s.phase_correctionImag = ((ComplexFIRFilter_54_s.phase_correctionReal * ComplexFIRFilter_54_s.phase_corr_incrImag) - (ComplexFIRFilter_54_s.phase_correctionImag * ComplexFIRFilter_54_s.phase_corr_incrReal)) ; 
		resultReal = ((resultReal * ComplexFIRFilter_54_s.phase_correctionReal) - (resultImag * ComplexFIRFilter_54_s.phase_correctionImag)) ; 
		resultImag = ((resultReal * ComplexFIRFilter_54_s.phase_correctionImag) - (resultImag * ComplexFIRFilter_54_s.phase_correctionReal)) ; 
	}
		push_float(&(*chanout), resultReal) ; 
		push_float(&(*chanout), resultImag) ; 
	}


void ComplexFIRFilter_54() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		ComplexFIRFilter(&(SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[0]), &(ComplexFIRFilter_54QuadratureDemod_55));
	ENDFOR
}

void QuadratureDemod(buffer_float_t *chanin, buffer_float_t *chanout) {
	float lastValReal = 0.0;
	float productReal = 0.0;
	float valReal = 0.0;
	float lastValImag = 0.0;
	float productImag = 0.0;
	float valImag = 0.0;
	lastValReal = peek_float(&(*chanin), 3) ; 
	lastValImag = peek_float(&(*chanin), 2) ; 
	FOR(int, i, 5,  > , 0, i--) {
		valImag = pop_float(&(*chanin)) ; 
		valReal = pop_float(&(*chanin)) ; 
		productReal = ((valReal * lastValReal) - (valImag * lastValImag)) ; 
		productImag = ((valReal * -lastValImag) - (valImag * lastValReal)) ; 
		lastValReal = valReal ; 
		lastValImag = valImag ; 
		push_float(&(*chanout), (1.0 * ((float) asin((productImag / ((float) sqrt((((float) pow(productReal, 2.0)) + ((float) pow(productImag, 2.0)))))))))) ; 
	}
	ENDFOR
}


void QuadratureDemod_55() {
	QuadratureDemod(&(ComplexFIRFilter_54QuadratureDemod_55), &(QuadratureDemod_55RealFIRFilter_56));
}

void RealFIRFilter(buffer_float_t *chanin, buffer_float_t *chanout) {
	float sum = 0.0;
	FOR(int, i__conflict__0, 0,  < , 5, i__conflict__0++) {
		sum = (sum + (RealFIRFilter_56_s.taps[i__conflict__0] * pop_float(&(*chanin)))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 15, i++) {
		sum = (sum + (RealFIRFilter_56_s.taps[(i + 5)] + peek_float(&(*chanin), i))) ; 
	}
	ENDFOR
	push_float(&(*chanout), sum) ; 
}


void RealFIRFilter_56() {
	RealFIRFilter(&(QuadratureDemod_55RealFIRFilter_56), &(SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_join[0]));
}

void ComplexFIRFilter_58() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		ComplexFIRFilter(&(SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[1]), &(ComplexFIRFilter_58QuadratureDemod_59));
	ENDFOR
}

void QuadratureDemod_59() {
	QuadratureDemod(&(ComplexFIRFilter_58QuadratureDemod_59), &(QuadratureDemod_59RealFIRFilter_60));
}

void RealFIRFilter_60() {
	RealFIRFilter(&(QuadratureDemod_59RealFIRFilter_60), &(SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_join[1]));
}

void ComplexFIRFilter_62() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		ComplexFIRFilter(&(SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[2]), &(ComplexFIRFilter_62QuadratureDemod_63));
	ENDFOR
}

void QuadratureDemod_63() {
	QuadratureDemod(&(ComplexFIRFilter_62QuadratureDemod_63), &(QuadratureDemod_63RealFIRFilter_64));
}

void RealFIRFilter_64() {
	RealFIRFilter(&(QuadratureDemod_63RealFIRFilter_64), &(SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_join[2]));
}

void ComplexFIRFilter_66() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		ComplexFIRFilter(&(SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[3]), &(ComplexFIRFilter_66QuadratureDemod_67));
	ENDFOR
}

void QuadratureDemod_67() {
	QuadratureDemod(&(ComplexFIRFilter_66QuadratureDemod_67), &(QuadratureDemod_67RealFIRFilter_68));
}

void RealFIRFilter_68() {
	RealFIRFilter(&(QuadratureDemod_67RealFIRFilter_68), &(SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_join[3]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_70() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 825, __iter_tok_++)
				push_float(&SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[__iter_dec_], pop_float(&TestSource_51WEIGHTED_ROUND_ROBIN_Splitter_70));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_71() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_71NullSink_69, pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_join[__iter_]));
	ENDFOR
}

void NullSink(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void NullSink_69() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		NullSink(&(WEIGHTED_ROUND_ROBIN_Joiner_71NullSink_69));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&ComplexFIRFilter_58QuadratureDemod_59);
	init_buffer_float(&ComplexFIRFilter_54QuadratureDemod_55);
	init_buffer_float(&ComplexFIRFilter_66QuadratureDemod_67);
	init_buffer_float(&ComplexFIRFilter_62QuadratureDemod_63);
	FOR(int, __iter_init_0_, 0, <, 4, __iter_init_0_++)
		init_buffer_float(&SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 4, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_71NullSink_69);
	init_buffer_float(&QuadratureDemod_59RealFIRFilter_60);
	init_buffer_float(&QuadratureDemod_63RealFIRFilter_64);
	init_buffer_float(&QuadratureDemod_67RealFIRFilter_68);
	init_buffer_float(&TestSource_51WEIGHTED_ROUND_ROBIN_Splitter_70);
	init_buffer_float(&QuadratureDemod_55RealFIRFilter_56);
// --- init: TestSource_51
	 {
	TestSource_51_s.i = 0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 49500, __iter_init_++)
		TestSource(&(TestSource_51WEIGHTED_ROUND_ROBIN_Splitter_70));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_70
	FOR(uint32_t, __iter_init_, 0, <, 15, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(TestSource_51WEIGHTED_ROUND_ROBIN_Splitter_70), &(SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: ComplexFIRFilter_54
	 {
	ComplexFIRFilter_54_s.phase_corr_incrReal = 1.0 ; 
	ComplexFIRFilter_54_s.phase_corr_incrImag = 0.0 ; 
	ComplexFIRFilter_54_s.phase_correctionReal = 1.0 ; 
	ComplexFIRFilter_54_s.phase_correctionImag = 0.0 ; 
 {
	FOR(int, index, 0,  < , 400, index++) {
		ComplexFIRFilter_54_s.tapsReal[index] = (2.0 * ((float) cos(((1.9745196 * index) * (0.54 - (0.46 * ((float) cos(((6.2831855 * index) / 399.0))))))))) ; 
		ComplexFIRFilter_54_s.tapsImag[index] = ((-2.0 * ((float) sin((1.9745196 * index)))) * (0.54 - (0.46 * ((float) cos(((6.2831855 * index) / 399.0)))))) ; 
	}
	ENDFOR
	ComplexFIRFilter_54_s.phase_corr_incrReal = -0.062805444 ; 
	ComplexFIRFilter_54_s.phase_corr_incrImag = -0.9980258 ; 
}
}
	FOR(uint32_t, __iter_init_, 0, <, 15, __iter_init_++)
		ComplexFIRFilter(&(SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[0]), &(ComplexFIRFilter_54QuadratureDemod_55));
	ENDFOR
//--------------------------------
// --- init: QuadratureDemod_55
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		QuadratureDemod(&(ComplexFIRFilter_54QuadratureDemod_55), &(QuadratureDemod_55RealFIRFilter_56));
	ENDFOR
//--------------------------------
// --- init: RealFIRFilter_56
	 {
 {
	FOR(int, index, 0,  < , 20, index++) {
		if((index - 9.5) != 0.0) {
			RealFIRFilter_56_s.taps[index] = (1.0 * (((((float) sin((3.1415927 * (index - 9.5)))) / 3.1415927) / (index - 9.5)) * (0.54 - (0.46 * ((float) cos(((6.2831855 * index) / 19.0))))))) ; 
		}
	}
	ENDFOR
}
}
//--------------------------------
// --- init: ComplexFIRFilter_58
	 {
 {
	FOR(int, index, 0,  < , 400, index++) {
		ComplexFIRFilter_58_s.tapsReal[index] = (2.0 * ((float) cos(((1.9716636 * index) * (0.54 - (0.46 * ((float) cos(((6.2831855 * index) / 399.0))))))))) ; 
		ComplexFIRFilter_58_s.tapsImag[index] = ((-2.0 * ((float) sin((1.9716636 * index)))) * (0.54 - (0.46 * ((float) cos(((6.2831855 * index) / 399.0)))))) ; 
	}
	ENDFOR
	ComplexFIRFilter_58_s.phase_corr_incrReal = 0.7501165 ; 
	ComplexFIRFilter_58_s.phase_corr_incrImag = 0.66130567 ; 
}
}
	FOR(uint32_t, __iter_init_, 0, <, 15, __iter_init_++)
		ComplexFIRFilter(&(SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[1]), &(ComplexFIRFilter_58QuadratureDemod_59));
	ENDFOR
//--------------------------------
// --- init: QuadratureDemod_59
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		QuadratureDemod(&(ComplexFIRFilter_58QuadratureDemod_59), &(QuadratureDemod_59RealFIRFilter_60));
	ENDFOR
//--------------------------------
// --- init: RealFIRFilter_60
	 {
 {
	FOR(int, index, 0,  < , 20, index++) {
		if((index - 9.5) != 0.0) {
			RealFIRFilter_60_s.taps[index] = (1.0 * (((((float) sin((3.1415927 * (index - 9.5)))) / 3.1415927) / (index - 9.5)) * (0.54 - (0.46 * ((float) cos(((6.2831855 * index) / 19.0))))))) ; 
		}
	}
	ENDFOR
}
}
//--------------------------------
// --- init: ComplexFIRFilter_62
	 {
 {
	FOR(int, index, 0,  < , 400, index++) {
		ComplexFIRFilter_62_s.tapsReal[index] = (2.0 * ((float) cos(((1.9688076 * index) * (0.54 - (0.46 * ((float) cos(((6.2831855 * index) / 399.0))))))))) ; 
		ComplexFIRFilter_62_s.tapsImag[index] = ((-2.0 * ((float) sin((1.9688076 * index)))) * (0.54 - (0.46 * ((float) cos(((6.2831855 * index) / 399.0)))))) ; 
	}
	ENDFOR
	ComplexFIRFilter_62_s.phase_corr_incrReal = -0.9980266 ; 
	ComplexFIRFilter_62_s.phase_corr_incrImag = 0.06279211 ; 
}
}
	FOR(uint32_t, __iter_init_, 0, <, 15, __iter_init_++)
		ComplexFIRFilter(&(SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[2]), &(ComplexFIRFilter_62QuadratureDemod_63));
	ENDFOR
//--------------------------------
// --- init: QuadratureDemod_63
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		QuadratureDemod(&(ComplexFIRFilter_62QuadratureDemod_63), &(QuadratureDemod_63RealFIRFilter_64));
	ENDFOR
//--------------------------------
// --- init: RealFIRFilter_64
	 {
 {
	FOR(int, index, 0,  < , 20, index++) {
		if((index - 9.5) != 0.0) {
			RealFIRFilter_64_s.taps[index] = (1.0 * (((((float) sin((3.1415927 * (index - 9.5)))) / 3.1415927) / (index - 9.5)) * (0.54 - (0.46 * ((float) cos(((6.2831855 * index) / 19.0))))))) ; 
		}
	}
	ENDFOR
}
}
//--------------------------------
// --- init: ComplexFIRFilter_66
	 {
 {
	FOR(int, index, 0,  < , 400, index++) {
		ComplexFIRFilter_66_s.tapsReal[index] = (2.0 * ((float) cos(((2.0868745 * index) * (0.54 - (0.46 * ((float) cos(((6.2831855 * index) / 399.0))))))))) ; 
		ComplexFIRFilter_66_s.tapsImag[index] = ((-2.0 * ((float) sin((2.0868745 * index)))) * (0.54 - (0.46 * ((float) cos(((6.2831855 * index) / 399.0)))))) ; 
	}
	ENDFOR
	ComplexFIRFilter_66_s.phase_corr_incrReal = 0.99690205 ; 
	ComplexFIRFilter_66_s.phase_corr_incrImag = -0.0786533 ; 
}
}
	FOR(uint32_t, __iter_init_, 0, <, 15, __iter_init_++)
		ComplexFIRFilter(&(SplitJoin0_SplitJoin0_SplitJoin0_PerftestSplitJoin_52_72_77_83_split[3]), &(ComplexFIRFilter_66QuadratureDemod_67));
	ENDFOR
//--------------------------------
// --- init: QuadratureDemod_67
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		QuadratureDemod(&(ComplexFIRFilter_66QuadratureDemod_67), &(QuadratureDemod_67RealFIRFilter_68));
	ENDFOR
//--------------------------------
// --- init: RealFIRFilter_68
	 {
 {
	FOR(int, index, 0,  < , 20, index++) {
		if((index - 9.5) != 0.0) {
			RealFIRFilter_68_s.taps[index] = (1.0 * (((((float) sin((3.1415927 * (index - 9.5)))) / 3.1415927) / (index - 9.5)) * (0.54 - (0.46 * ((float) cos(((6.2831855 * index) / 19.0))))))) ; 
		}
	}
	ENDFOR
}
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		TestSource_51();
		WEIGHTED_ROUND_ROBIN_Splitter_70();
			ComplexFIRFilter_54();
			QuadratureDemod_55();
			RealFIRFilter_56();
			ComplexFIRFilter_58();
			QuadratureDemod_59();
			RealFIRFilter_60();
			ComplexFIRFilter_62();
			QuadratureDemod_63();
			RealFIRFilter_64();
			ComplexFIRFilter_66();
			QuadratureDemod_67();
			RealFIRFilter_68();
		WEIGHTED_ROUND_ROBIN_Joiner_71();
		NullSink_69();
	ENDFOR
	return EXIT_SUCCESS;
}
