#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=33000 on the compile command line
#else
#if BUF_SIZEMAX < 33000
#error BUF_SIZEMAX too small, it must be at least 33000
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	int i;
} TestSource_51_t;

typedef struct {
	float tapsReal[400];
	float tapsImag[400];
	float inputArray[400];
	float phase_correctionReal;
	float phase_correctionImag;
	float phase_corr_incrReal;
	float phase_corr_incrImag;
} ComplexFIRFilter_54_t;

typedef struct {
	float taps[20];
} RealFIRFilter_56_t;
void TestSource(buffer_float_t *chanout);
void TestSource_51();
void WEIGHTED_ROUND_ROBIN_Splitter_70();
void ComplexFIRFilter(buffer_float_t *chanin, buffer_float_t *chanout);
void ComplexFIRFilter_54();
void QuadratureDemod(buffer_float_t *chanin, buffer_float_t *chanout);
void QuadratureDemod_55();
void RealFIRFilter(buffer_float_t *chanin, buffer_float_t *chanout);
void RealFIRFilter_56();
void ComplexFIRFilter_58();
void QuadratureDemod_59();
void RealFIRFilter_60();
void ComplexFIRFilter_62();
void QuadratureDemod_63();
void RealFIRFilter_64();
void ComplexFIRFilter_66();
void QuadratureDemod_67();
void RealFIRFilter_68();
void WEIGHTED_ROUND_ROBIN_Joiner_71();
void NullSink(buffer_float_t *chanin);
void NullSink_69();

#ifdef __cplusplus
}
#endif
#endif
