#include "PEG12-FFT5.h"

buffer_complex_t Pre_CollapsedDataParallel_1_4022butterfly_3925;
buffer_complex_t SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3861_4087_4215_4234_split[2];
buffer_complex_t SplitJoin18_SplitJoin12_SplitJoin12_split2_3842_4095_4173_4254_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4197WEIGHTED_ROUND_ROBIN_Splitter_4039;
buffer_complex_t SplitJoin88_SplitJoin65_SplitJoin65_AnonFilter_a0_3895_4137_4226_4243_split[2];
buffer_complex_t SplitJoin58_SplitJoin37_SplitJoin37_split2_3848_4115_4178_4258_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4040WEIGHTED_ROUND_ROBIN_Splitter_4183;
buffer_complex_t SplitJoin72_SplitJoin49_SplitJoin49_AnonFilter_a0_3873_4126_4222_4238_split[2];
buffer_complex_t SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3863_4088_4171_4235_split[2];
buffer_complex_t SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3861_4087_4215_4234_join[2];
buffer_complex_t SplitJoin98_SplitJoin75_SplitJoin75_AnonFilter_a0_3909_4144_4229_4246_split[2];
buffer_complex_t SplitJoin76_SplitJoin53_SplitJoin53_AnonFilter_a0_3879_4129_4223_4239_split[2];
buffer_complex_t SplitJoin0_source_Fiss_4214_4233_split[2];
buffer_complex_t SplitJoin10_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_Hier_4218_4249_join[2];
buffer_complex_t SplitJoin52_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child1_4182_4256_split[2];
buffer_complex_t butterfly_3926Post_CollapsedDataParallel_2_4026;
buffer_complex_t SplitJoin58_SplitJoin37_SplitJoin37_split2_3848_4115_4178_4258_split[2];
buffer_complex_t butterfly_3930Post_CollapsedDataParallel_2_4038;
buffer_complex_t SplitJoin16_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child0_4180_4253_join[2];
buffer_complex_t SplitJoin90_SplitJoin67_SplitJoin67_AnonFilter_a0_3897_4138_4227_4244_join[2];
buffer_complex_t butterfly_3927Post_CollapsedDataParallel_2_4029;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4188WEIGHTED_ROUND_ROBIN_Splitter_4189;
buffer_complex_t butterfly_3924Post_CollapsedDataParallel_2_4020;
buffer_complex_t butterfly_3925Post_CollapsedDataParallel_2_4023;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4082WEIGHTED_ROUND_ROBIN_Splitter_4200;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4201sink_3950;
buffer_complex_t SplitJoin10_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_Hier_4218_4249_split[2];
buffer_complex_t SplitJoin76_SplitJoin53_SplitJoin53_AnonFilter_a0_3879_4129_4223_4239_join[2];
buffer_complex_t butterfly_3923Post_CollapsedDataParallel_2_4017;
buffer_complex_t SplitJoin52_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child1_4182_4256_join[2];
buffer_complex_t SplitJoin20_SplitJoin14_SplitJoin14_split1_3853_4097_4220_4259_split[2];
buffer_complex_t SplitJoin22_SplitJoin16_SplitJoin16_split2_3855_4098_4179_4260_split[4];
buffer_complex_t SplitJoin94_SplitJoin71_SplitJoin71_AnonFilter_a0_3903_4141_4228_4245_join[2];
buffer_complex_t SplitJoin65_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child1_4177_4251_join[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4194WEIGHTED_ROUND_ROBIN_Splitter_4081;
buffer_complex_t SplitJoin16_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child0_4180_4253_split[2];
buffer_complex_t butterfly_3928Post_CollapsedDataParallel_2_4032;
buffer_complex_t Pre_CollapsedDataParallel_1_4025butterfly_3926;
buffer_complex_t Pre_CollapsedDataParallel_1_4019butterfly_3924;
buffer_complex_t SplitJoin18_SplitJoin12_SplitJoin12_split2_3842_4095_4173_4254_split[2];
buffer_complex_t Pre_CollapsedDataParallel_1_4031butterfly_3928;
buffer_complex_t SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_3867_4090_4217_4237_join[2];
buffer_complex_t SplitJoin48_SplitJoin29_SplitJoin29_split2_3844_4109_4175_4255_split[2];
buffer_complex_t SplitJoin72_SplitJoin49_SplitJoin49_AnonFilter_a0_3873_4126_4222_4238_join[2];
buffer_complex_t SplitJoin48_SplitJoin29_SplitJoin29_split2_3844_4109_4175_4255_join[2];
buffer_complex_t SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_3867_4090_4217_4237_split[2];
buffer_complex_t SplitJoin54_SplitJoin33_SplitJoin33_split2_3846_4112_4176_4257_join[2];
buffer_complex_t SplitJoin94_SplitJoin71_SplitJoin71_AnonFilter_a0_3903_4141_4228_4245_split[2];
buffer_complex_t SplitJoin14_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_Hier_4219_4252_join[2];
buffer_complex_t SplitJoin54_SplitJoin33_SplitJoin33_split2_3846_4112_4176_4257_split[2];
buffer_complex_t SplitJoin0_source_Fiss_4214_4233_join[2];
buffer_complex_t SplitJoin20_SplitJoin14_SplitJoin14_split1_3853_4097_4220_4259_join[2];
buffer_float_t SplitJoin24_magnitude_Fiss_4221_4262_join[12];
buffer_complex_t SplitJoin86_SplitJoin63_SplitJoin63_AnonFilter_a0_3893_4136_4172_4242_join[2];
buffer_complex_t SplitJoin98_SplitJoin75_SplitJoin75_AnonFilter_a0_3909_4144_4229_4246_join[2];
buffer_complex_t SplitJoin82_SplitJoin59_SplitJoin59_AnonFilter_a0_3887_4133_4225_4241_split[2];
buffer_complex_t SplitJoin86_SplitJoin63_SplitJoin63_AnonFilter_a0_3893_4136_4172_4242_split[2];
buffer_complex_t SplitJoin88_SplitJoin65_SplitJoin65_AnonFilter_a0_3895_4137_4226_4243_join[2];
buffer_complex_t SplitJoin41_SplitJoin22_SplitJoin22_split2_3857_4103_4181_4261_split[4];
buffer_complex_t SplitJoin104_SplitJoin81_SplitJoin81_AnonFilter_a0_3917_4148_4231_4248_split[2];
buffer_complex_t SplitJoin22_SplitJoin16_SplitJoin16_split2_3855_4098_4179_4260_join[4];
buffer_complex_t SplitJoin24_magnitude_Fiss_4221_4262_split[12];
buffer_complex_t Pre_CollapsedDataParallel_1_4016butterfly_3923;
buffer_complex_t SplitJoin78_SplitJoin55_SplitJoin55_AnonFilter_a0_3881_4130_4224_4240_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_4028butterfly_3927;
buffer_complex_t SplitJoin14_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_Hier_4219_4252_split[2];
buffer_complex_t SplitJoin65_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child1_4177_4251_split[4];
buffer_complex_t SplitJoin82_SplitJoin59_SplitJoin59_AnonFilter_a0_3887_4133_4225_4241_join[2];
buffer_complex_t SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3865_4089_4216_4236_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_4034butterfly_3929;
buffer_complex_t SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3863_4088_4171_4235_join[2];
buffer_complex_t SplitJoin12_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child0_4174_4250_split[4];
buffer_complex_t SplitJoin100_SplitJoin77_SplitJoin77_AnonFilter_a0_3911_4145_4230_4247_join[2];
buffer_complex_t SplitJoin12_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child0_4174_4250_join[4];
buffer_complex_t SplitJoin41_SplitJoin22_SplitJoin22_split2_3857_4103_4181_4261_join[4];
buffer_complex_t SplitJoin104_SplitJoin81_SplitJoin81_AnonFilter_a0_3917_4148_4231_4248_join[2];
buffer_complex_t SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3865_4089_4216_4236_split[2];
buffer_complex_t SplitJoin100_SplitJoin77_SplitJoin77_AnonFilter_a0_3911_4145_4230_4247_split[2];
buffer_complex_t SplitJoin78_SplitJoin55_SplitJoin55_AnonFilter_a0_3881_4130_4224_4240_split[2];
buffer_complex_t SplitJoin90_SplitJoin67_SplitJoin67_AnonFilter_a0_3897_4138_4227_4244_split[2];
buffer_complex_t butterfly_3929Post_CollapsedDataParallel_2_4035;
buffer_complex_t Pre_CollapsedDataParallel_1_4037butterfly_3930;



void source(buffer_void_t *chanin, buffer_complex_t *chanout) {
		complex_t t;
		t.imag = 0.0 ; 
		t.real = 0.9501 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.2311 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.6068 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.486 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.8913 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.7621 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.4565 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.0185 ; 
		push_complex(&(*chanout), t) ; 
	}


void source_4198() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		source(&(SplitJoin0_source_Fiss_4214_4233_split[0]), &(SplitJoin0_source_Fiss_4214_4233_join[0]));
	ENDFOR
}

void source_4199() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		source(&(SplitJoin0_source_Fiss_4214_4233_split[1]), &(SplitJoin0_source_Fiss_4214_4233_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4196() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_4197() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4197WEIGHTED_ROUND_ROBIN_Splitter_4039, pop_complex(&SplitJoin0_source_Fiss_4214_4233_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4197WEIGHTED_ROUND_ROBIN_Splitter_4039, pop_complex(&SplitJoin0_source_Fiss_4214_4233_join[1]));
		ENDFOR
	ENDFOR
}}

void Identity(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t __tmp1094 = pop_complex(&(*chanin));
		push_complex(&(*chanout), __tmp1094) ; 
	}


void Identity_3869() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_3867_4090_4217_4237_split[0]), &(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_3867_4090_4217_4237_join[0]));
	ENDFOR
}

void Identity_3871() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_3867_4090_4217_4237_split[1]), &(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_3867_4090_4217_4237_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4045() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_3867_4090_4217_4237_split[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3865_4089_4216_4236_split[0]));
		push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_3867_4090_4217_4237_split[1], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3865_4089_4216_4236_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4046() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3865_4089_4216_4236_join[0], pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_3867_4090_4217_4237_join[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3865_4089_4216_4236_join[0], pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_3867_4090_4217_4237_join[1]));
	ENDFOR
}}

void Identity_3875() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin72_SplitJoin49_SplitJoin49_AnonFilter_a0_3873_4126_4222_4238_split[0]), &(SplitJoin72_SplitJoin49_SplitJoin49_AnonFilter_a0_3873_4126_4222_4238_join[0]));
	ENDFOR
}

void Identity_3877() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin72_SplitJoin49_SplitJoin49_AnonFilter_a0_3873_4126_4222_4238_split[1]), &(SplitJoin72_SplitJoin49_SplitJoin49_AnonFilter_a0_3873_4126_4222_4238_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4047() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin72_SplitJoin49_SplitJoin49_AnonFilter_a0_3873_4126_4222_4238_split[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3865_4089_4216_4236_split[1]));
		push_complex(&SplitJoin72_SplitJoin49_SplitJoin49_AnonFilter_a0_3873_4126_4222_4238_split[1], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3865_4089_4216_4236_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4048() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3865_4089_4216_4236_join[1], pop_complex(&SplitJoin72_SplitJoin49_SplitJoin49_AnonFilter_a0_3873_4126_4222_4238_join[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3865_4089_4216_4236_join[1], pop_complex(&SplitJoin72_SplitJoin49_SplitJoin49_AnonFilter_a0_3873_4126_4222_4238_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_4043() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3865_4089_4216_4236_split[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3863_4088_4171_4235_split[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3865_4089_4216_4236_split[1], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3863_4088_4171_4235_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4044() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3863_4088_4171_4235_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3865_4089_4216_4236_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3863_4088_4171_4235_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3865_4089_4216_4236_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3863_4088_4171_4235_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3865_4089_4216_4236_join[1]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3863_4088_4171_4235_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3865_4089_4216_4236_join[1]));
	ENDFOR
}}

void Identity_3883() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin78_SplitJoin55_SplitJoin55_AnonFilter_a0_3881_4130_4224_4240_split[0]), &(SplitJoin78_SplitJoin55_SplitJoin55_AnonFilter_a0_3881_4130_4224_4240_join[0]));
	ENDFOR
}

void Identity_3885() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin78_SplitJoin55_SplitJoin55_AnonFilter_a0_3881_4130_4224_4240_split[1]), &(SplitJoin78_SplitJoin55_SplitJoin55_AnonFilter_a0_3881_4130_4224_4240_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4051() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin78_SplitJoin55_SplitJoin55_AnonFilter_a0_3881_4130_4224_4240_split[0], pop_complex(&SplitJoin76_SplitJoin53_SplitJoin53_AnonFilter_a0_3879_4129_4223_4239_split[0]));
		push_complex(&SplitJoin78_SplitJoin55_SplitJoin55_AnonFilter_a0_3881_4130_4224_4240_split[1], pop_complex(&SplitJoin76_SplitJoin53_SplitJoin53_AnonFilter_a0_3879_4129_4223_4239_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4052() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin76_SplitJoin53_SplitJoin53_AnonFilter_a0_3879_4129_4223_4239_join[0], pop_complex(&SplitJoin78_SplitJoin55_SplitJoin55_AnonFilter_a0_3881_4130_4224_4240_join[0]));
		push_complex(&SplitJoin76_SplitJoin53_SplitJoin53_AnonFilter_a0_3879_4129_4223_4239_join[0], pop_complex(&SplitJoin78_SplitJoin55_SplitJoin55_AnonFilter_a0_3881_4130_4224_4240_join[1]));
	ENDFOR
}}

void Identity_3889() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin82_SplitJoin59_SplitJoin59_AnonFilter_a0_3887_4133_4225_4241_split[0]), &(SplitJoin82_SplitJoin59_SplitJoin59_AnonFilter_a0_3887_4133_4225_4241_join[0]));
	ENDFOR
}

void Identity_3891() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin82_SplitJoin59_SplitJoin59_AnonFilter_a0_3887_4133_4225_4241_split[1]), &(SplitJoin82_SplitJoin59_SplitJoin59_AnonFilter_a0_3887_4133_4225_4241_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4053() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin82_SplitJoin59_SplitJoin59_AnonFilter_a0_3887_4133_4225_4241_split[0], pop_complex(&SplitJoin76_SplitJoin53_SplitJoin53_AnonFilter_a0_3879_4129_4223_4239_split[1]));
		push_complex(&SplitJoin82_SplitJoin59_SplitJoin59_AnonFilter_a0_3887_4133_4225_4241_split[1], pop_complex(&SplitJoin76_SplitJoin53_SplitJoin53_AnonFilter_a0_3879_4129_4223_4239_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4054() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin76_SplitJoin53_SplitJoin53_AnonFilter_a0_3879_4129_4223_4239_join[1], pop_complex(&SplitJoin82_SplitJoin59_SplitJoin59_AnonFilter_a0_3887_4133_4225_4241_join[0]));
		push_complex(&SplitJoin76_SplitJoin53_SplitJoin53_AnonFilter_a0_3879_4129_4223_4239_join[1], pop_complex(&SplitJoin82_SplitJoin59_SplitJoin59_AnonFilter_a0_3887_4133_4225_4241_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_4049() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin76_SplitJoin53_SplitJoin53_AnonFilter_a0_3879_4129_4223_4239_split[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3863_4088_4171_4235_split[1]));
		push_complex(&SplitJoin76_SplitJoin53_SplitJoin53_AnonFilter_a0_3879_4129_4223_4239_split[1], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3863_4088_4171_4235_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4050() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3863_4088_4171_4235_join[1], pop_complex(&SplitJoin76_SplitJoin53_SplitJoin53_AnonFilter_a0_3879_4129_4223_4239_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3863_4088_4171_4235_join[1], pop_complex(&SplitJoin76_SplitJoin53_SplitJoin53_AnonFilter_a0_3879_4129_4223_4239_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3863_4088_4171_4235_join[1], pop_complex(&SplitJoin76_SplitJoin53_SplitJoin53_AnonFilter_a0_3879_4129_4223_4239_join[1]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3863_4088_4171_4235_join[1], pop_complex(&SplitJoin76_SplitJoin53_SplitJoin53_AnonFilter_a0_3879_4129_4223_4239_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_4041() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 12, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3863_4088_4171_4235_split[0], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3861_4087_4215_4234_split[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3863_4088_4171_4235_split[1], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3861_4087_4215_4234_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4042() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3861_4087_4215_4234_join[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3863_4088_4171_4235_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3861_4087_4215_4234_join[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3863_4088_4171_4235_join[1]));
		ENDFOR
	ENDFOR
}}

void Identity_3899() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin90_SplitJoin67_SplitJoin67_AnonFilter_a0_3897_4138_4227_4244_split[0]), &(SplitJoin90_SplitJoin67_SplitJoin67_AnonFilter_a0_3897_4138_4227_4244_join[0]));
	ENDFOR
}

void Identity_3901() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin90_SplitJoin67_SplitJoin67_AnonFilter_a0_3897_4138_4227_4244_split[1]), &(SplitJoin90_SplitJoin67_SplitJoin67_AnonFilter_a0_3897_4138_4227_4244_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4059() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin90_SplitJoin67_SplitJoin67_AnonFilter_a0_3897_4138_4227_4244_split[0], pop_complex(&SplitJoin88_SplitJoin65_SplitJoin65_AnonFilter_a0_3895_4137_4226_4243_split[0]));
		push_complex(&SplitJoin90_SplitJoin67_SplitJoin67_AnonFilter_a0_3897_4138_4227_4244_split[1], pop_complex(&SplitJoin88_SplitJoin65_SplitJoin65_AnonFilter_a0_3895_4137_4226_4243_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4060() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin88_SplitJoin65_SplitJoin65_AnonFilter_a0_3895_4137_4226_4243_join[0], pop_complex(&SplitJoin90_SplitJoin67_SplitJoin67_AnonFilter_a0_3897_4138_4227_4244_join[0]));
		push_complex(&SplitJoin88_SplitJoin65_SplitJoin65_AnonFilter_a0_3895_4137_4226_4243_join[0], pop_complex(&SplitJoin90_SplitJoin67_SplitJoin67_AnonFilter_a0_3897_4138_4227_4244_join[1]));
	ENDFOR
}}

void Identity_3905() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin94_SplitJoin71_SplitJoin71_AnonFilter_a0_3903_4141_4228_4245_split[0]), &(SplitJoin94_SplitJoin71_SplitJoin71_AnonFilter_a0_3903_4141_4228_4245_join[0]));
	ENDFOR
}

void Identity_3907() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin94_SplitJoin71_SplitJoin71_AnonFilter_a0_3903_4141_4228_4245_split[1]), &(SplitJoin94_SplitJoin71_SplitJoin71_AnonFilter_a0_3903_4141_4228_4245_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4061() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin94_SplitJoin71_SplitJoin71_AnonFilter_a0_3903_4141_4228_4245_split[0], pop_complex(&SplitJoin88_SplitJoin65_SplitJoin65_AnonFilter_a0_3895_4137_4226_4243_split[1]));
		push_complex(&SplitJoin94_SplitJoin71_SplitJoin71_AnonFilter_a0_3903_4141_4228_4245_split[1], pop_complex(&SplitJoin88_SplitJoin65_SplitJoin65_AnonFilter_a0_3895_4137_4226_4243_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4062() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin88_SplitJoin65_SplitJoin65_AnonFilter_a0_3895_4137_4226_4243_join[1], pop_complex(&SplitJoin94_SplitJoin71_SplitJoin71_AnonFilter_a0_3903_4141_4228_4245_join[0]));
		push_complex(&SplitJoin88_SplitJoin65_SplitJoin65_AnonFilter_a0_3895_4137_4226_4243_join[1], pop_complex(&SplitJoin94_SplitJoin71_SplitJoin71_AnonFilter_a0_3903_4141_4228_4245_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_4057() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin88_SplitJoin65_SplitJoin65_AnonFilter_a0_3895_4137_4226_4243_split[0], pop_complex(&SplitJoin86_SplitJoin63_SplitJoin63_AnonFilter_a0_3893_4136_4172_4242_split[0]));
		push_complex(&SplitJoin88_SplitJoin65_SplitJoin65_AnonFilter_a0_3895_4137_4226_4243_split[1], pop_complex(&SplitJoin86_SplitJoin63_SplitJoin63_AnonFilter_a0_3893_4136_4172_4242_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4058() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin86_SplitJoin63_SplitJoin63_AnonFilter_a0_3893_4136_4172_4242_join[0], pop_complex(&SplitJoin88_SplitJoin65_SplitJoin65_AnonFilter_a0_3895_4137_4226_4243_join[0]));
		push_complex(&SplitJoin86_SplitJoin63_SplitJoin63_AnonFilter_a0_3893_4136_4172_4242_join[0], pop_complex(&SplitJoin88_SplitJoin65_SplitJoin65_AnonFilter_a0_3895_4137_4226_4243_join[0]));
		push_complex(&SplitJoin86_SplitJoin63_SplitJoin63_AnonFilter_a0_3893_4136_4172_4242_join[0], pop_complex(&SplitJoin88_SplitJoin65_SplitJoin65_AnonFilter_a0_3895_4137_4226_4243_join[1]));
		push_complex(&SplitJoin86_SplitJoin63_SplitJoin63_AnonFilter_a0_3893_4136_4172_4242_join[0], pop_complex(&SplitJoin88_SplitJoin65_SplitJoin65_AnonFilter_a0_3895_4137_4226_4243_join[1]));
	ENDFOR
}}

void Identity_3913() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin100_SplitJoin77_SplitJoin77_AnonFilter_a0_3911_4145_4230_4247_split[0]), &(SplitJoin100_SplitJoin77_SplitJoin77_AnonFilter_a0_3911_4145_4230_4247_join[0]));
	ENDFOR
}

void Identity_3915() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin100_SplitJoin77_SplitJoin77_AnonFilter_a0_3911_4145_4230_4247_split[1]), &(SplitJoin100_SplitJoin77_SplitJoin77_AnonFilter_a0_3911_4145_4230_4247_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4065() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin100_SplitJoin77_SplitJoin77_AnonFilter_a0_3911_4145_4230_4247_split[0], pop_complex(&SplitJoin98_SplitJoin75_SplitJoin75_AnonFilter_a0_3909_4144_4229_4246_split[0]));
		push_complex(&SplitJoin100_SplitJoin77_SplitJoin77_AnonFilter_a0_3911_4145_4230_4247_split[1], pop_complex(&SplitJoin98_SplitJoin75_SplitJoin75_AnonFilter_a0_3909_4144_4229_4246_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4066() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin98_SplitJoin75_SplitJoin75_AnonFilter_a0_3909_4144_4229_4246_join[0], pop_complex(&SplitJoin100_SplitJoin77_SplitJoin77_AnonFilter_a0_3911_4145_4230_4247_join[0]));
		push_complex(&SplitJoin98_SplitJoin75_SplitJoin75_AnonFilter_a0_3909_4144_4229_4246_join[0], pop_complex(&SplitJoin100_SplitJoin77_SplitJoin77_AnonFilter_a0_3911_4145_4230_4247_join[1]));
	ENDFOR
}}

void Identity_3919() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin104_SplitJoin81_SplitJoin81_AnonFilter_a0_3917_4148_4231_4248_split[0]), &(SplitJoin104_SplitJoin81_SplitJoin81_AnonFilter_a0_3917_4148_4231_4248_join[0]));
	ENDFOR
}

void Identity_3921() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin104_SplitJoin81_SplitJoin81_AnonFilter_a0_3917_4148_4231_4248_split[1]), &(SplitJoin104_SplitJoin81_SplitJoin81_AnonFilter_a0_3917_4148_4231_4248_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4067() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin104_SplitJoin81_SplitJoin81_AnonFilter_a0_3917_4148_4231_4248_split[0], pop_complex(&SplitJoin98_SplitJoin75_SplitJoin75_AnonFilter_a0_3909_4144_4229_4246_split[1]));
		push_complex(&SplitJoin104_SplitJoin81_SplitJoin81_AnonFilter_a0_3917_4148_4231_4248_split[1], pop_complex(&SplitJoin98_SplitJoin75_SplitJoin75_AnonFilter_a0_3909_4144_4229_4246_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4068() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin98_SplitJoin75_SplitJoin75_AnonFilter_a0_3909_4144_4229_4246_join[1], pop_complex(&SplitJoin104_SplitJoin81_SplitJoin81_AnonFilter_a0_3917_4148_4231_4248_join[0]));
		push_complex(&SplitJoin98_SplitJoin75_SplitJoin75_AnonFilter_a0_3909_4144_4229_4246_join[1], pop_complex(&SplitJoin104_SplitJoin81_SplitJoin81_AnonFilter_a0_3917_4148_4231_4248_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_4063() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin98_SplitJoin75_SplitJoin75_AnonFilter_a0_3909_4144_4229_4246_split[0], pop_complex(&SplitJoin86_SplitJoin63_SplitJoin63_AnonFilter_a0_3893_4136_4172_4242_split[1]));
		push_complex(&SplitJoin98_SplitJoin75_SplitJoin75_AnonFilter_a0_3909_4144_4229_4246_split[1], pop_complex(&SplitJoin86_SplitJoin63_SplitJoin63_AnonFilter_a0_3893_4136_4172_4242_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4064() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin86_SplitJoin63_SplitJoin63_AnonFilter_a0_3893_4136_4172_4242_join[1], pop_complex(&SplitJoin98_SplitJoin75_SplitJoin75_AnonFilter_a0_3909_4144_4229_4246_join[0]));
		push_complex(&SplitJoin86_SplitJoin63_SplitJoin63_AnonFilter_a0_3893_4136_4172_4242_join[1], pop_complex(&SplitJoin98_SplitJoin75_SplitJoin75_AnonFilter_a0_3909_4144_4229_4246_join[0]));
		push_complex(&SplitJoin86_SplitJoin63_SplitJoin63_AnonFilter_a0_3893_4136_4172_4242_join[1], pop_complex(&SplitJoin98_SplitJoin75_SplitJoin75_AnonFilter_a0_3909_4144_4229_4246_join[1]));
		push_complex(&SplitJoin86_SplitJoin63_SplitJoin63_AnonFilter_a0_3893_4136_4172_4242_join[1], pop_complex(&SplitJoin98_SplitJoin75_SplitJoin75_AnonFilter_a0_3909_4144_4229_4246_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_4055() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 12, __iter_steady_++)
		push_complex(&SplitJoin86_SplitJoin63_SplitJoin63_AnonFilter_a0_3893_4136_4172_4242_split[0], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3861_4087_4215_4234_split[1]));
		push_complex(&SplitJoin86_SplitJoin63_SplitJoin63_AnonFilter_a0_3893_4136_4172_4242_split[1], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3861_4087_4215_4234_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4056() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3861_4087_4215_4234_join[1], pop_complex(&SplitJoin86_SplitJoin63_SplitJoin63_AnonFilter_a0_3893_4136_4172_4242_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3861_4087_4215_4234_join[1], pop_complex(&SplitJoin86_SplitJoin63_SplitJoin63_AnonFilter_a0_3893_4136_4172_4242_join[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_4039() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 24, __iter_steady_++)
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3861_4087_4215_4234_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4197WEIGHTED_ROUND_ROBIN_Splitter_4039));
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3861_4087_4215_4234_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4197WEIGHTED_ROUND_ROBIN_Splitter_4039));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4040() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4040WEIGHTED_ROUND_ROBIN_Splitter_4183, pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3861_4087_4215_4234_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4040WEIGHTED_ROUND_ROBIN_Splitter_4183, pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3861_4087_4215_4234_join[1]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_complex_t *chanin, buffer_complex_t *chanout) {
 {
 {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
			push_complex(&(*chanout), peek_complex(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
		}
		ENDFOR
	}
	}
	}
		pop_complex(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_4016() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child0_4174_4250_split[0]), &(Pre_CollapsedDataParallel_1_4016butterfly_3923));
	ENDFOR
}

void butterfly(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&(*chanin)));
		complex_t two = ((complex_t) pop_complex(&(*chanin)));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&(*chanout), __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&(*chanout), __sa2) ; 
	}


void butterfly_3923() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_4016butterfly_3923), &(butterfly_3923Post_CollapsedDataParallel_2_4017));
	ENDFOR
}

void Post_CollapsedDataParallel_2(buffer_complex_t *chanin, buffer_complex_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 2, _k++) {
 {
			push_complex(&(*chanout), peek_complex(&(*chanin), (_k + 0))) ; 
		}
		}
		ENDFOR
	}
	}
		pop_complex(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_4017() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_3923Post_CollapsedDataParallel_2_4017), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child0_4174_4250_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_4019() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child0_4174_4250_split[1]), &(Pre_CollapsedDataParallel_1_4019butterfly_3924));
	ENDFOR
}

void butterfly_3924() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_4019butterfly_3924), &(butterfly_3924Post_CollapsedDataParallel_2_4020));
	ENDFOR
}

void Post_CollapsedDataParallel_2_4020() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_3924Post_CollapsedDataParallel_2_4020), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child0_4174_4250_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_4022() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child0_4174_4250_split[2]), &(Pre_CollapsedDataParallel_1_4022butterfly_3925));
	ENDFOR
}

void butterfly_3925() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_4022butterfly_3925), &(butterfly_3925Post_CollapsedDataParallel_2_4023));
	ENDFOR
}

void Post_CollapsedDataParallel_2_4023() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_3925Post_CollapsedDataParallel_2_4023), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child0_4174_4250_join[2]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_4025() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child0_4174_4250_split[3]), &(Pre_CollapsedDataParallel_1_4025butterfly_3926));
	ENDFOR
}

void butterfly_3926() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_4025butterfly_3926), &(butterfly_3926Post_CollapsedDataParallel_2_4026));
	ENDFOR
}

void Post_CollapsedDataParallel_2_4026() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_3926Post_CollapsedDataParallel_2_4026), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child0_4174_4250_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4184() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child0_4174_4250_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_Hier_4218_4249_split[0]));
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child0_4174_4250_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_Hier_4218_4249_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4185() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_Hier_4218_4249_join[0], pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child0_4174_4250_join[__iter_]));
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_Hier_4218_4249_join[0], pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child0_4174_4250_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_4028() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin65_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child1_4177_4251_split[0]), &(Pre_CollapsedDataParallel_1_4028butterfly_3927));
	ENDFOR
}

void butterfly_3927() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_4028butterfly_3927), &(butterfly_3927Post_CollapsedDataParallel_2_4029));
	ENDFOR
}

void Post_CollapsedDataParallel_2_4029() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_3927Post_CollapsedDataParallel_2_4029), &(SplitJoin65_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child1_4177_4251_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_4031() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin65_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child1_4177_4251_split[1]), &(Pre_CollapsedDataParallel_1_4031butterfly_3928));
	ENDFOR
}

void butterfly_3928() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_4031butterfly_3928), &(butterfly_3928Post_CollapsedDataParallel_2_4032));
	ENDFOR
}

void Post_CollapsedDataParallel_2_4032() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_3928Post_CollapsedDataParallel_2_4032), &(SplitJoin65_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child1_4177_4251_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_4034() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin65_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child1_4177_4251_split[2]), &(Pre_CollapsedDataParallel_1_4034butterfly_3929));
	ENDFOR
}

void butterfly_3929() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_4034butterfly_3929), &(butterfly_3929Post_CollapsedDataParallel_2_4035));
	ENDFOR
}

void Post_CollapsedDataParallel_2_4035() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_3929Post_CollapsedDataParallel_2_4035), &(SplitJoin65_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child1_4177_4251_join[2]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_4037() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin65_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child1_4177_4251_split[3]), &(Pre_CollapsedDataParallel_1_4037butterfly_3930));
	ENDFOR
}

void butterfly_3930() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_4037butterfly_3930), &(butterfly_3930Post_CollapsedDataParallel_2_4038));
	ENDFOR
}

void Post_CollapsedDataParallel_2_4038() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_3930Post_CollapsedDataParallel_2_4038), &(SplitJoin65_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child1_4177_4251_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4186() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin65_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child1_4177_4251_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_Hier_4218_4249_split[1]));
			push_complex(&SplitJoin65_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child1_4177_4251_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_Hier_4218_4249_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4187() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_Hier_4218_4249_join[1], pop_complex(&SplitJoin65_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child1_4177_4251_join[__iter_]));
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_Hier_4218_4249_join[1], pop_complex(&SplitJoin65_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child1_4177_4251_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_4183() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_Hier_4218_4249_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4040WEIGHTED_ROUND_ROBIN_Splitter_4183));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_Hier_4218_4249_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4040WEIGHTED_ROUND_ROBIN_Splitter_4183));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4188() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4188WEIGHTED_ROUND_ROBIN_Splitter_4189, pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_Hier_4218_4249_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4188WEIGHTED_ROUND_ROBIN_Splitter_4189, pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_Hier_4218_4249_join[1]));
		ENDFOR
	ENDFOR
}}

void butterfly_3932() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin18_SplitJoin12_SplitJoin12_split2_3842_4095_4173_4254_split[0]), &(SplitJoin18_SplitJoin12_SplitJoin12_split2_3842_4095_4173_4254_join[0]));
	ENDFOR
}

void butterfly_3933() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin18_SplitJoin12_SplitJoin12_split2_3842_4095_4173_4254_split[1]), &(SplitJoin18_SplitJoin12_SplitJoin12_split2_3842_4095_4173_4254_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4073() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_3842_4095_4173_4254_split[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child0_4180_4253_split[0]));
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_3842_4095_4173_4254_split[1], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child0_4180_4253_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4074() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child0_4180_4253_join[0], pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_3842_4095_4173_4254_join[0]));
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child0_4180_4253_join[0], pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_3842_4095_4173_4254_join[1]));
	ENDFOR
}}

void butterfly_3934() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin48_SplitJoin29_SplitJoin29_split2_3844_4109_4175_4255_split[0]), &(SplitJoin48_SplitJoin29_SplitJoin29_split2_3844_4109_4175_4255_join[0]));
	ENDFOR
}

void butterfly_3935() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin48_SplitJoin29_SplitJoin29_split2_3844_4109_4175_4255_split[1]), &(SplitJoin48_SplitJoin29_SplitJoin29_split2_3844_4109_4175_4255_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4075() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin48_SplitJoin29_SplitJoin29_split2_3844_4109_4175_4255_split[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child0_4180_4253_split[1]));
		push_complex(&SplitJoin48_SplitJoin29_SplitJoin29_split2_3844_4109_4175_4255_split[1], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child0_4180_4253_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4076() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child0_4180_4253_join[1], pop_complex(&SplitJoin48_SplitJoin29_SplitJoin29_split2_3844_4109_4175_4255_join[0]));
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child0_4180_4253_join[1], pop_complex(&SplitJoin48_SplitJoin29_SplitJoin29_split2_3844_4109_4175_4255_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_4190() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child0_4180_4253_split[0], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_Hier_4219_4252_split[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child0_4180_4253_split[1], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_Hier_4219_4252_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4191() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_Hier_4219_4252_join[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child0_4180_4253_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_Hier_4219_4252_join[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child0_4180_4253_join[1]));
		ENDFOR
	ENDFOR
}}

void butterfly_3936() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin54_SplitJoin33_SplitJoin33_split2_3846_4112_4176_4257_split[0]), &(SplitJoin54_SplitJoin33_SplitJoin33_split2_3846_4112_4176_4257_join[0]));
	ENDFOR
}

void butterfly_3937() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin54_SplitJoin33_SplitJoin33_split2_3846_4112_4176_4257_split[1]), &(SplitJoin54_SplitJoin33_SplitJoin33_split2_3846_4112_4176_4257_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4077() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin54_SplitJoin33_SplitJoin33_split2_3846_4112_4176_4257_split[0], pop_complex(&SplitJoin52_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child1_4182_4256_split[0]));
		push_complex(&SplitJoin54_SplitJoin33_SplitJoin33_split2_3846_4112_4176_4257_split[1], pop_complex(&SplitJoin52_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child1_4182_4256_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4078() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin52_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child1_4182_4256_join[0], pop_complex(&SplitJoin54_SplitJoin33_SplitJoin33_split2_3846_4112_4176_4257_join[0]));
		push_complex(&SplitJoin52_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child1_4182_4256_join[0], pop_complex(&SplitJoin54_SplitJoin33_SplitJoin33_split2_3846_4112_4176_4257_join[1]));
	ENDFOR
}}

void butterfly_3938() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin58_SplitJoin37_SplitJoin37_split2_3848_4115_4178_4258_split[0]), &(SplitJoin58_SplitJoin37_SplitJoin37_split2_3848_4115_4178_4258_join[0]));
	ENDFOR
}

void butterfly_3939() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin58_SplitJoin37_SplitJoin37_split2_3848_4115_4178_4258_split[1]), &(SplitJoin58_SplitJoin37_SplitJoin37_split2_3848_4115_4178_4258_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4079() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin58_SplitJoin37_SplitJoin37_split2_3848_4115_4178_4258_split[0], pop_complex(&SplitJoin52_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child1_4182_4256_split[1]));
		push_complex(&SplitJoin58_SplitJoin37_SplitJoin37_split2_3848_4115_4178_4258_split[1], pop_complex(&SplitJoin52_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child1_4182_4256_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4080() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin52_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child1_4182_4256_join[1], pop_complex(&SplitJoin58_SplitJoin37_SplitJoin37_split2_3848_4115_4178_4258_join[0]));
		push_complex(&SplitJoin52_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child1_4182_4256_join[1], pop_complex(&SplitJoin58_SplitJoin37_SplitJoin37_split2_3848_4115_4178_4258_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_4192() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin52_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child1_4182_4256_split[0], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_Hier_4219_4252_split[1]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin52_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child1_4182_4256_split[1], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_Hier_4219_4252_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4193() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_Hier_4219_4252_join[1], pop_complex(&SplitJoin52_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child1_4182_4256_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_Hier_4219_4252_join[1], pop_complex(&SplitJoin52_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child1_4182_4256_join[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_4189() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_Hier_4219_4252_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4188WEIGHTED_ROUND_ROBIN_Splitter_4189));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_Hier_4219_4252_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4188WEIGHTED_ROUND_ROBIN_Splitter_4189));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4194() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4194WEIGHTED_ROUND_ROBIN_Splitter_4081, pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_Hier_4219_4252_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4194WEIGHTED_ROUND_ROBIN_Splitter_4081, pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_Hier_4219_4252_join[1]));
		ENDFOR
	ENDFOR
}}

void butterfly_3941() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_3855_4098_4179_4260_split[0]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_3855_4098_4179_4260_join[0]));
	ENDFOR
}

void butterfly_3942() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_3855_4098_4179_4260_split[1]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_3855_4098_4179_4260_join[1]));
	ENDFOR
}

void butterfly_3943() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_3855_4098_4179_4260_split[2]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_3855_4098_4179_4260_join[2]));
	ENDFOR
}

void butterfly_3944() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_3855_4098_4179_4260_split[3]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_3855_4098_4179_4260_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4083() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_3855_4098_4179_4260_split[__iter_], pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_3853_4097_4220_4259_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4084() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_3853_4097_4220_4259_join[0], pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_3855_4098_4179_4260_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void butterfly_3945() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin41_SplitJoin22_SplitJoin22_split2_3857_4103_4181_4261_split[0]), &(SplitJoin41_SplitJoin22_SplitJoin22_split2_3857_4103_4181_4261_join[0]));
	ENDFOR
}

void butterfly_3946() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin41_SplitJoin22_SplitJoin22_split2_3857_4103_4181_4261_split[1]), &(SplitJoin41_SplitJoin22_SplitJoin22_split2_3857_4103_4181_4261_join[1]));
	ENDFOR
}

void butterfly_3947() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin41_SplitJoin22_SplitJoin22_split2_3857_4103_4181_4261_split[2]), &(SplitJoin41_SplitJoin22_SplitJoin22_split2_3857_4103_4181_4261_join[2]));
	ENDFOR
}

void butterfly_3948() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin41_SplitJoin22_SplitJoin22_split2_3857_4103_4181_4261_split[3]), &(SplitJoin41_SplitJoin22_SplitJoin22_split2_3857_4103_4181_4261_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4085() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin41_SplitJoin22_SplitJoin22_split2_3857_4103_4181_4261_split[__iter_], pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_3853_4097_4220_4259_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4086() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_3853_4097_4220_4259_join[1], pop_complex(&SplitJoin41_SplitJoin22_SplitJoin22_split2_3857_4103_4181_4261_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_4081() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_3853_4097_4220_4259_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4194WEIGHTED_ROUND_ROBIN_Splitter_4081));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_3853_4097_4220_4259_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4194WEIGHTED_ROUND_ROBIN_Splitter_4081));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4082() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4082WEIGHTED_ROUND_ROBIN_Splitter_4200, pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_3853_4097_4220_4259_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4082WEIGHTED_ROUND_ROBIN_Splitter_4200, pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_3853_4097_4220_4259_join[1]));
		ENDFOR
	ENDFOR
}}

void magnitude(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		push_float(&(*chanout), ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}


void magnitude_4202() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_4221_4262_split[0]), &(SplitJoin24_magnitude_Fiss_4221_4262_join[0]));
	ENDFOR
}

void magnitude_4203() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_4221_4262_split[1]), &(SplitJoin24_magnitude_Fiss_4221_4262_join[1]));
	ENDFOR
}

void magnitude_4204() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_4221_4262_split[2]), &(SplitJoin24_magnitude_Fiss_4221_4262_join[2]));
	ENDFOR
}

void magnitude_4205() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_4221_4262_split[3]), &(SplitJoin24_magnitude_Fiss_4221_4262_join[3]));
	ENDFOR
}

void magnitude_4206() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_4221_4262_split[4]), &(SplitJoin24_magnitude_Fiss_4221_4262_join[4]));
	ENDFOR
}

void magnitude_4207() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_4221_4262_split[5]), &(SplitJoin24_magnitude_Fiss_4221_4262_join[5]));
	ENDFOR
}

void magnitude_4208() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_4221_4262_split[6]), &(SplitJoin24_magnitude_Fiss_4221_4262_join[6]));
	ENDFOR
}

void magnitude_4209() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_4221_4262_split[7]), &(SplitJoin24_magnitude_Fiss_4221_4262_join[7]));
	ENDFOR
}

void magnitude_4210() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_4221_4262_split[8]), &(SplitJoin24_magnitude_Fiss_4221_4262_join[8]));
	ENDFOR
}

void magnitude_4211() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_4221_4262_split[9]), &(SplitJoin24_magnitude_Fiss_4221_4262_join[9]));
	ENDFOR
}

void magnitude_4212() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_4221_4262_split[10]), &(SplitJoin24_magnitude_Fiss_4221_4262_join[10]));
	ENDFOR
}

void magnitude_4213() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_4221_4262_split[11]), &(SplitJoin24_magnitude_Fiss_4221_4262_join[11]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4200() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 12, __iter_++)
			push_complex(&SplitJoin24_magnitude_Fiss_4221_4262_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4082WEIGHTED_ROUND_ROBIN_Splitter_4200));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4201() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 12, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4201sink_3950, pop_float(&SplitJoin24_magnitude_Fiss_4221_4262_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void sink(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void sink_3950() {
	FOR(uint32_t, __iter_steady_, 0, <, 48, __iter_steady_++)
		sink(&(WEIGHTED_ROUND_ROBIN_Joiner_4201sink_3950));
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&Pre_CollapsedDataParallel_1_4022butterfly_3925);
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3861_4087_4215_4234_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 2, __iter_init_1_++)
		init_buffer_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_3842_4095_4173_4254_join[__iter_init_1_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4197WEIGHTED_ROUND_ROBIN_Splitter_4039);
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_complex(&SplitJoin88_SplitJoin65_SplitJoin65_AnonFilter_a0_3895_4137_4226_4243_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 2, __iter_init_3_++)
		init_buffer_complex(&SplitJoin58_SplitJoin37_SplitJoin37_split2_3848_4115_4178_4258_join[__iter_init_3_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4040WEIGHTED_ROUND_ROBIN_Splitter_4183);
	FOR(int, __iter_init_4_, 0, <, 2, __iter_init_4_++)
		init_buffer_complex(&SplitJoin72_SplitJoin49_SplitJoin49_AnonFilter_a0_3873_4126_4222_4238_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 2, __iter_init_5_++)
		init_buffer_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3863_4088_4171_4235_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 2, __iter_init_6_++)
		init_buffer_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3861_4087_4215_4234_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 2, __iter_init_7_++)
		init_buffer_complex(&SplitJoin98_SplitJoin75_SplitJoin75_AnonFilter_a0_3909_4144_4229_4246_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_complex(&SplitJoin76_SplitJoin53_SplitJoin53_AnonFilter_a0_3879_4129_4223_4239_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 2, __iter_init_9_++)
		init_buffer_complex(&SplitJoin0_source_Fiss_4214_4233_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_Hier_4218_4249_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 2, __iter_init_11_++)
		init_buffer_complex(&SplitJoin52_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child1_4182_4256_split[__iter_init_11_]);
	ENDFOR
	init_buffer_complex(&butterfly_3926Post_CollapsedDataParallel_2_4026);
	FOR(int, __iter_init_12_, 0, <, 2, __iter_init_12_++)
		init_buffer_complex(&SplitJoin58_SplitJoin37_SplitJoin37_split2_3848_4115_4178_4258_split[__iter_init_12_]);
	ENDFOR
	init_buffer_complex(&butterfly_3930Post_CollapsedDataParallel_2_4038);
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child0_4180_4253_join[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_complex(&SplitJoin90_SplitJoin67_SplitJoin67_AnonFilter_a0_3897_4138_4227_4244_join[__iter_init_14_]);
	ENDFOR
	init_buffer_complex(&butterfly_3927Post_CollapsedDataParallel_2_4029);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4188WEIGHTED_ROUND_ROBIN_Splitter_4189);
	init_buffer_complex(&butterfly_3924Post_CollapsedDataParallel_2_4020);
	init_buffer_complex(&butterfly_3925Post_CollapsedDataParallel_2_4023);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4082WEIGHTED_ROUND_ROBIN_Splitter_4200);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4201sink_3950);
	FOR(int, __iter_init_15_, 0, <, 2, __iter_init_15_++)
		init_buffer_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_Hier_4218_4249_split[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_complex(&SplitJoin76_SplitJoin53_SplitJoin53_AnonFilter_a0_3879_4129_4223_4239_join[__iter_init_16_]);
	ENDFOR
	init_buffer_complex(&butterfly_3923Post_CollapsedDataParallel_2_4017);
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_complex(&SplitJoin52_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child1_4182_4256_join[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 2, __iter_init_18_++)
		init_buffer_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_3853_4097_4220_4259_split[__iter_init_18_]);
	ENDFOR
	FOR(int, __iter_init_19_, 0, <, 4, __iter_init_19_++)
		init_buffer_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_3855_4098_4179_4260_split[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 2, __iter_init_20_++)
		init_buffer_complex(&SplitJoin94_SplitJoin71_SplitJoin71_AnonFilter_a0_3903_4141_4228_4245_join[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 4, __iter_init_21_++)
		init_buffer_complex(&SplitJoin65_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child1_4177_4251_join[__iter_init_21_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4194WEIGHTED_ROUND_ROBIN_Splitter_4081);
	FOR(int, __iter_init_22_, 0, <, 2, __iter_init_22_++)
		init_buffer_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_child0_4180_4253_split[__iter_init_22_]);
	ENDFOR
	init_buffer_complex(&butterfly_3928Post_CollapsedDataParallel_2_4032);
	init_buffer_complex(&Pre_CollapsedDataParallel_1_4025butterfly_3926);
	init_buffer_complex(&Pre_CollapsedDataParallel_1_4019butterfly_3924);
	FOR(int, __iter_init_23_, 0, <, 2, __iter_init_23_++)
		init_buffer_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_3842_4095_4173_4254_split[__iter_init_23_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_4031butterfly_3928);
	FOR(int, __iter_init_24_, 0, <, 2, __iter_init_24_++)
		init_buffer_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_3867_4090_4217_4237_join[__iter_init_24_]);
	ENDFOR
	FOR(int, __iter_init_25_, 0, <, 2, __iter_init_25_++)
		init_buffer_complex(&SplitJoin48_SplitJoin29_SplitJoin29_split2_3844_4109_4175_4255_split[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 2, __iter_init_26_++)
		init_buffer_complex(&SplitJoin72_SplitJoin49_SplitJoin49_AnonFilter_a0_3873_4126_4222_4238_join[__iter_init_26_]);
	ENDFOR
	FOR(int, __iter_init_27_, 0, <, 2, __iter_init_27_++)
		init_buffer_complex(&SplitJoin48_SplitJoin29_SplitJoin29_split2_3844_4109_4175_4255_join[__iter_init_27_]);
	ENDFOR
	FOR(int, __iter_init_28_, 0, <, 2, __iter_init_28_++)
		init_buffer_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_3867_4090_4217_4237_split[__iter_init_28_]);
	ENDFOR
	FOR(int, __iter_init_29_, 0, <, 2, __iter_init_29_++)
		init_buffer_complex(&SplitJoin54_SplitJoin33_SplitJoin33_split2_3846_4112_4176_4257_join[__iter_init_29_]);
	ENDFOR
	FOR(int, __iter_init_30_, 0, <, 2, __iter_init_30_++)
		init_buffer_complex(&SplitJoin94_SplitJoin71_SplitJoin71_AnonFilter_a0_3903_4141_4228_4245_split[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 2, __iter_init_31_++)
		init_buffer_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_Hier_4219_4252_join[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 2, __iter_init_32_++)
		init_buffer_complex(&SplitJoin54_SplitJoin33_SplitJoin33_split2_3846_4112_4176_4257_split[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 2, __iter_init_33_++)
		init_buffer_complex(&SplitJoin0_source_Fiss_4214_4233_join[__iter_init_33_]);
	ENDFOR
	FOR(int, __iter_init_34_, 0, <, 2, __iter_init_34_++)
		init_buffer_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_3853_4097_4220_4259_join[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 12, __iter_init_35_++)
		init_buffer_float(&SplitJoin24_magnitude_Fiss_4221_4262_join[__iter_init_35_]);
	ENDFOR
	FOR(int, __iter_init_36_, 0, <, 2, __iter_init_36_++)
		init_buffer_complex(&SplitJoin86_SplitJoin63_SplitJoin63_AnonFilter_a0_3893_4136_4172_4242_join[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 2, __iter_init_37_++)
		init_buffer_complex(&SplitJoin98_SplitJoin75_SplitJoin75_AnonFilter_a0_3909_4144_4229_4246_join[__iter_init_37_]);
	ENDFOR
	FOR(int, __iter_init_38_, 0, <, 2, __iter_init_38_++)
		init_buffer_complex(&SplitJoin82_SplitJoin59_SplitJoin59_AnonFilter_a0_3887_4133_4225_4241_split[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 2, __iter_init_39_++)
		init_buffer_complex(&SplitJoin86_SplitJoin63_SplitJoin63_AnonFilter_a0_3893_4136_4172_4242_split[__iter_init_39_]);
	ENDFOR
	FOR(int, __iter_init_40_, 0, <, 2, __iter_init_40_++)
		init_buffer_complex(&SplitJoin88_SplitJoin65_SplitJoin65_AnonFilter_a0_3895_4137_4226_4243_join[__iter_init_40_]);
	ENDFOR
	FOR(int, __iter_init_41_, 0, <, 4, __iter_init_41_++)
		init_buffer_complex(&SplitJoin41_SplitJoin22_SplitJoin22_split2_3857_4103_4181_4261_split[__iter_init_41_]);
	ENDFOR
	FOR(int, __iter_init_42_, 0, <, 2, __iter_init_42_++)
		init_buffer_complex(&SplitJoin104_SplitJoin81_SplitJoin81_AnonFilter_a0_3917_4148_4231_4248_split[__iter_init_42_]);
	ENDFOR
	FOR(int, __iter_init_43_, 0, <, 4, __iter_init_43_++)
		init_buffer_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_3855_4098_4179_4260_join[__iter_init_43_]);
	ENDFOR
	FOR(int, __iter_init_44_, 0, <, 12, __iter_init_44_++)
		init_buffer_complex(&SplitJoin24_magnitude_Fiss_4221_4262_split[__iter_init_44_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_4016butterfly_3923);
	FOR(int, __iter_init_45_, 0, <, 2, __iter_init_45_++)
		init_buffer_complex(&SplitJoin78_SplitJoin55_SplitJoin55_AnonFilter_a0_3881_4130_4224_4240_join[__iter_init_45_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_4028butterfly_3927);
	FOR(int, __iter_init_46_, 0, <, 2, __iter_init_46_++)
		init_buffer_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_3840_4094_Hier_Hier_4219_4252_split[__iter_init_46_]);
	ENDFOR
	FOR(int, __iter_init_47_, 0, <, 4, __iter_init_47_++)
		init_buffer_complex(&SplitJoin65_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child1_4177_4251_split[__iter_init_47_]);
	ENDFOR
	FOR(int, __iter_init_48_, 0, <, 2, __iter_init_48_++)
		init_buffer_complex(&SplitJoin82_SplitJoin59_SplitJoin59_AnonFilter_a0_3887_4133_4225_4241_join[__iter_init_48_]);
	ENDFOR
	FOR(int, __iter_init_49_, 0, <, 2, __iter_init_49_++)
		init_buffer_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3865_4089_4216_4236_join[__iter_init_49_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_4034butterfly_3929);
	FOR(int, __iter_init_50_, 0, <, 2, __iter_init_50_++)
		init_buffer_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3863_4088_4171_4235_join[__iter_init_50_]);
	ENDFOR
	FOR(int, __iter_init_51_, 0, <, 4, __iter_init_51_++)
		init_buffer_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child0_4174_4250_split[__iter_init_51_]);
	ENDFOR
	FOR(int, __iter_init_52_, 0, <, 2, __iter_init_52_++)
		init_buffer_complex(&SplitJoin100_SplitJoin77_SplitJoin77_AnonFilter_a0_3911_4145_4230_4247_join[__iter_init_52_]);
	ENDFOR
	FOR(int, __iter_init_53_, 0, <, 4, __iter_init_53_++)
		init_buffer_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_3819_4092_Hier_child0_4174_4250_join[__iter_init_53_]);
	ENDFOR
	FOR(int, __iter_init_54_, 0, <, 4, __iter_init_54_++)
		init_buffer_complex(&SplitJoin41_SplitJoin22_SplitJoin22_split2_3857_4103_4181_4261_join[__iter_init_54_]);
	ENDFOR
	FOR(int, __iter_init_55_, 0, <, 2, __iter_init_55_++)
		init_buffer_complex(&SplitJoin104_SplitJoin81_SplitJoin81_AnonFilter_a0_3917_4148_4231_4248_join[__iter_init_55_]);
	ENDFOR
	FOR(int, __iter_init_56_, 0, <, 2, __iter_init_56_++)
		init_buffer_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3865_4089_4216_4236_split[__iter_init_56_]);
	ENDFOR
	FOR(int, __iter_init_57_, 0, <, 2, __iter_init_57_++)
		init_buffer_complex(&SplitJoin100_SplitJoin77_SplitJoin77_AnonFilter_a0_3911_4145_4230_4247_split[__iter_init_57_]);
	ENDFOR
	FOR(int, __iter_init_58_, 0, <, 2, __iter_init_58_++)
		init_buffer_complex(&SplitJoin78_SplitJoin55_SplitJoin55_AnonFilter_a0_3881_4130_4224_4240_split[__iter_init_58_]);
	ENDFOR
	FOR(int, __iter_init_59_, 0, <, 2, __iter_init_59_++)
		init_buffer_complex(&SplitJoin90_SplitJoin67_SplitJoin67_AnonFilter_a0_3897_4138_4227_4244_split[__iter_init_59_]);
	ENDFOR
	init_buffer_complex(&butterfly_3929Post_CollapsedDataParallel_2_4035);
	init_buffer_complex(&Pre_CollapsedDataParallel_1_4037butterfly_3930);
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_4196();
			source_4198();
			source_4199();
		WEIGHTED_ROUND_ROBIN_Joiner_4197();
		WEIGHTED_ROUND_ROBIN_Splitter_4039();
			WEIGHTED_ROUND_ROBIN_Splitter_4041();
				WEIGHTED_ROUND_ROBIN_Splitter_4043();
					WEIGHTED_ROUND_ROBIN_Splitter_4045();
						Identity_3869();
						Identity_3871();
					WEIGHTED_ROUND_ROBIN_Joiner_4046();
					WEIGHTED_ROUND_ROBIN_Splitter_4047();
						Identity_3875();
						Identity_3877();
					WEIGHTED_ROUND_ROBIN_Joiner_4048();
				WEIGHTED_ROUND_ROBIN_Joiner_4044();
				WEIGHTED_ROUND_ROBIN_Splitter_4049();
					WEIGHTED_ROUND_ROBIN_Splitter_4051();
						Identity_3883();
						Identity_3885();
					WEIGHTED_ROUND_ROBIN_Joiner_4052();
					WEIGHTED_ROUND_ROBIN_Splitter_4053();
						Identity_3889();
						Identity_3891();
					WEIGHTED_ROUND_ROBIN_Joiner_4054();
				WEIGHTED_ROUND_ROBIN_Joiner_4050();
			WEIGHTED_ROUND_ROBIN_Joiner_4042();
			WEIGHTED_ROUND_ROBIN_Splitter_4055();
				WEIGHTED_ROUND_ROBIN_Splitter_4057();
					WEIGHTED_ROUND_ROBIN_Splitter_4059();
						Identity_3899();
						Identity_3901();
					WEIGHTED_ROUND_ROBIN_Joiner_4060();
					WEIGHTED_ROUND_ROBIN_Splitter_4061();
						Identity_3905();
						Identity_3907();
					WEIGHTED_ROUND_ROBIN_Joiner_4062();
				WEIGHTED_ROUND_ROBIN_Joiner_4058();
				WEIGHTED_ROUND_ROBIN_Splitter_4063();
					WEIGHTED_ROUND_ROBIN_Splitter_4065();
						Identity_3913();
						Identity_3915();
					WEIGHTED_ROUND_ROBIN_Joiner_4066();
					WEIGHTED_ROUND_ROBIN_Splitter_4067();
						Identity_3919();
						Identity_3921();
					WEIGHTED_ROUND_ROBIN_Joiner_4068();
				WEIGHTED_ROUND_ROBIN_Joiner_4064();
			WEIGHTED_ROUND_ROBIN_Joiner_4056();
		WEIGHTED_ROUND_ROBIN_Joiner_4040();
		WEIGHTED_ROUND_ROBIN_Splitter_4183();
			WEIGHTED_ROUND_ROBIN_Splitter_4184();
				Pre_CollapsedDataParallel_1_4016();
				butterfly_3923();
				Post_CollapsedDataParallel_2_4017();
				Pre_CollapsedDataParallel_1_4019();
				butterfly_3924();
				Post_CollapsedDataParallel_2_4020();
				Pre_CollapsedDataParallel_1_4022();
				butterfly_3925();
				Post_CollapsedDataParallel_2_4023();
				Pre_CollapsedDataParallel_1_4025();
				butterfly_3926();
				Post_CollapsedDataParallel_2_4026();
			WEIGHTED_ROUND_ROBIN_Joiner_4185();
			WEIGHTED_ROUND_ROBIN_Splitter_4186();
				Pre_CollapsedDataParallel_1_4028();
				butterfly_3927();
				Post_CollapsedDataParallel_2_4029();
				Pre_CollapsedDataParallel_1_4031();
				butterfly_3928();
				Post_CollapsedDataParallel_2_4032();
				Pre_CollapsedDataParallel_1_4034();
				butterfly_3929();
				Post_CollapsedDataParallel_2_4035();
				Pre_CollapsedDataParallel_1_4037();
				butterfly_3930();
				Post_CollapsedDataParallel_2_4038();
			WEIGHTED_ROUND_ROBIN_Joiner_4187();
		WEIGHTED_ROUND_ROBIN_Joiner_4188();
		WEIGHTED_ROUND_ROBIN_Splitter_4189();
			WEIGHTED_ROUND_ROBIN_Splitter_4190();
				WEIGHTED_ROUND_ROBIN_Splitter_4073();
					butterfly_3932();
					butterfly_3933();
				WEIGHTED_ROUND_ROBIN_Joiner_4074();
				WEIGHTED_ROUND_ROBIN_Splitter_4075();
					butterfly_3934();
					butterfly_3935();
				WEIGHTED_ROUND_ROBIN_Joiner_4076();
			WEIGHTED_ROUND_ROBIN_Joiner_4191();
			WEIGHTED_ROUND_ROBIN_Splitter_4192();
				WEIGHTED_ROUND_ROBIN_Splitter_4077();
					butterfly_3936();
					butterfly_3937();
				WEIGHTED_ROUND_ROBIN_Joiner_4078();
				WEIGHTED_ROUND_ROBIN_Splitter_4079();
					butterfly_3938();
					butterfly_3939();
				WEIGHTED_ROUND_ROBIN_Joiner_4080();
			WEIGHTED_ROUND_ROBIN_Joiner_4193();
		WEIGHTED_ROUND_ROBIN_Joiner_4194();
		WEIGHTED_ROUND_ROBIN_Splitter_4081();
			WEIGHTED_ROUND_ROBIN_Splitter_4083();
				butterfly_3941();
				butterfly_3942();
				butterfly_3943();
				butterfly_3944();
			WEIGHTED_ROUND_ROBIN_Joiner_4084();
			WEIGHTED_ROUND_ROBIN_Splitter_4085();
				butterfly_3945();
				butterfly_3946();
				butterfly_3947();
				butterfly_3948();
			WEIGHTED_ROUND_ROBIN_Joiner_4086();
		WEIGHTED_ROUND_ROBIN_Joiner_4082();
		WEIGHTED_ROUND_ROBIN_Splitter_4200();
			magnitude_4202();
			magnitude_4203();
			magnitude_4204();
			magnitude_4205();
			magnitude_4206();
			magnitude_4207();
			magnitude_4208();
			magnitude_4209();
			magnitude_4210();
			magnitude_4211();
			magnitude_4212();
			magnitude_4213();
		WEIGHTED_ROUND_ROBIN_Joiner_4201();
		sink_3950();
	ENDFOR
	return EXIT_SUCCESS;
}
