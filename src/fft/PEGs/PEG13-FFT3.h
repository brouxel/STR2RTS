#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=1664 on the compile command line
#else
#if BUF_SIZEMAX < 1664
#error BUF_SIZEMAX too small, it must be at least 1664
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float A_re[32];
	float A_im[32];
	int idx;
} FloatSource_3680_t;
void FloatSource(buffer_float_t *chanout);
void FloatSource_3680();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_3981();
void WEIGHTED_ROUND_ROBIN_Splitter_4123();
void Butterfly(buffer_float_t *chanin, buffer_float_t *chanout);
void Butterfly_4125();
void Butterfly_4126();
void Butterfly_4127();
void Butterfly_4128();
void Butterfly_4129();
void Butterfly_4130();
void Butterfly_4131();
void Butterfly_4132();
void Butterfly_4133();
void Butterfly_4134();
void Butterfly_4135();
void Butterfly_4136();
void Butterfly_4137();
void WEIGHTED_ROUND_ROBIN_Joiner_4124();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_3982();
void WEIGHTED_ROUND_ROBIN_Splitter_4025();
void Pre_CollapsedDataParallel_1_3984();
void WEIGHTED_ROUND_ROBIN_Splitter_4138();
void Butterfly_4140();
void Butterfly_4141();
void Butterfly_4142();
void Butterfly_4143();
void Butterfly_4144();
void Butterfly_4145();
void Butterfly_4146();
void Butterfly_4147();
void WEIGHTED_ROUND_ROBIN_Joiner_4139();
void Post_CollapsedDataParallel_2_3985();
void WEIGHTED_ROUND_ROBIN_Splitter_4105();
void Pre_CollapsedDataParallel_1_3990();
void WEIGHTED_ROUND_ROBIN_Splitter_4148();
void Butterfly_4150();
void Butterfly_4151();
void Butterfly_4152();
void Butterfly_4153();
void WEIGHTED_ROUND_ROBIN_Joiner_4149();
void Post_CollapsedDataParallel_2_3991();
void Pre_CollapsedDataParallel_1_3993();
void WEIGHTED_ROUND_ROBIN_Splitter_4154();
void Butterfly_4156();
void Butterfly_4157();
void Butterfly_4158();
void Butterfly_4159();
void WEIGHTED_ROUND_ROBIN_Joiner_4155();
void Post_CollapsedDataParallel_2_3994();
void WEIGHTED_ROUND_ROBIN_Joiner_4106();
void Pre_CollapsedDataParallel_1_3987();
void WEIGHTED_ROUND_ROBIN_Splitter_4160();
void Butterfly_4162();
void Butterfly_4163();
void Butterfly_4164();
void Butterfly_4165();
void Butterfly_4166();
void Butterfly_4167();
void Butterfly_4168();
void Butterfly_4169();
void WEIGHTED_ROUND_ROBIN_Joiner_4161();
void Post_CollapsedDataParallel_2_3988();
void WEIGHTED_ROUND_ROBIN_Splitter_4107();
void Pre_CollapsedDataParallel_1_3996();
void WEIGHTED_ROUND_ROBIN_Splitter_4170();
void Butterfly_4172();
void Butterfly_4173();
void Butterfly_4174();
void Butterfly_4175();
void WEIGHTED_ROUND_ROBIN_Joiner_4171();
void Post_CollapsedDataParallel_2_3997();
void Pre_CollapsedDataParallel_1_3999();
void WEIGHTED_ROUND_ROBIN_Splitter_4176();
void Butterfly_4178();
void Butterfly_4179();
void Butterfly_4180();
void Butterfly_4181();
void WEIGHTED_ROUND_ROBIN_Joiner_4177();
void Post_CollapsedDataParallel_2_4000();
void WEIGHTED_ROUND_ROBIN_Joiner_4108();
void WEIGHTED_ROUND_ROBIN_Joiner_4109();
void WEIGHTED_ROUND_ROBIN_Splitter_4110();
void WEIGHTED_ROUND_ROBIN_Splitter_4111();
void Pre_CollapsedDataParallel_1_4002();
void WEIGHTED_ROUND_ROBIN_Splitter_4182();
void Butterfly_4184();
void Butterfly_4185();
void WEIGHTED_ROUND_ROBIN_Joiner_4183();
void Post_CollapsedDataParallel_2_4003();
void Pre_CollapsedDataParallel_1_4005();
void WEIGHTED_ROUND_ROBIN_Splitter_4186();
void Butterfly_4188();
void Butterfly_4189();
void WEIGHTED_ROUND_ROBIN_Joiner_4187();
void Post_CollapsedDataParallel_2_4006();
void Pre_CollapsedDataParallel_1_4008();
void WEIGHTED_ROUND_ROBIN_Splitter_4190();
void Butterfly_4192();
void Butterfly_4193();
void WEIGHTED_ROUND_ROBIN_Joiner_4191();
void Post_CollapsedDataParallel_2_4009();
void Pre_CollapsedDataParallel_1_4011();
void WEIGHTED_ROUND_ROBIN_Splitter_4194();
void Butterfly_4196();
void Butterfly_4197();
void WEIGHTED_ROUND_ROBIN_Joiner_4195();
void Post_CollapsedDataParallel_2_4012();
void WEIGHTED_ROUND_ROBIN_Joiner_4112();
void WEIGHTED_ROUND_ROBIN_Splitter_4113();
void Pre_CollapsedDataParallel_1_4014();
void WEIGHTED_ROUND_ROBIN_Splitter_4198();
void Butterfly_4200();
void Butterfly_4201();
void WEIGHTED_ROUND_ROBIN_Joiner_4199();
void Post_CollapsedDataParallel_2_4015();
void Pre_CollapsedDataParallel_1_4017();
void WEIGHTED_ROUND_ROBIN_Splitter_4202();
void Butterfly_4204();
void Butterfly_4205();
void WEIGHTED_ROUND_ROBIN_Joiner_4203();
void Post_CollapsedDataParallel_2_4018();
void Pre_CollapsedDataParallel_1_4020();
void WEIGHTED_ROUND_ROBIN_Splitter_4206();
void Butterfly_4208();
void Butterfly_4209();
void WEIGHTED_ROUND_ROBIN_Joiner_4207();
void Post_CollapsedDataParallel_2_4021();
void Pre_CollapsedDataParallel_1_4023();
void WEIGHTED_ROUND_ROBIN_Splitter_4210();
void Butterfly_4212();
void Butterfly_4213();
void WEIGHTED_ROUND_ROBIN_Joiner_4211();
void Post_CollapsedDataParallel_2_4024();
void WEIGHTED_ROUND_ROBIN_Joiner_4114();
void WEIGHTED_ROUND_ROBIN_Joiner_4115();
void WEIGHTED_ROUND_ROBIN_Splitter_4116();
void WEIGHTED_ROUND_ROBIN_Splitter_4117();
void Butterfly_3745();
void Butterfly_3746();
void Butterfly_3747();
void Butterfly_3748();
void Butterfly_3749();
void Butterfly_3750();
void Butterfly_3751();
void Butterfly_3752();
void WEIGHTED_ROUND_ROBIN_Joiner_4118();
void WEIGHTED_ROUND_ROBIN_Splitter_4119();
void Butterfly_3753();
void Butterfly_3754();
void Butterfly_3755();
void Butterfly_3756();
void Butterfly_3757();
void Butterfly_3758();
void Butterfly_3759();
void Butterfly_3760();
void WEIGHTED_ROUND_ROBIN_Joiner_4120();
void WEIGHTED_ROUND_ROBIN_Joiner_4121();
int BitReverse_3761_bitrev(int inp, int numbits);
void BitReverse(buffer_float_t *chanin, buffer_float_t *chanout);
void BitReverse_3761();
void FloatPrinter(buffer_float_t *chanin);
void FloatPrinter_3762();

#ifdef __cplusplus
}
#endif
#endif
