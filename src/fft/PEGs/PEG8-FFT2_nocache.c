#include "PEG8-FFT2_nocache.h"

buffer_float_t SplitJoin16_CombineDFT_Fiss_11003_11024_split[8];
buffer_float_t SplitJoin68_FFTReorderSimple_Fiss_11007_11028_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10896WEIGHTED_ROUND_ROBIN_Splitter_10905;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10876WEIGHTED_ROUND_ROBIN_Splitter_10885;
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10809_10845_10996_11017_join[2];
buffer_float_t SplitJoin20_CombineDFT_Fiss_11005_11026_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10986WEIGHTED_ROUND_ROBIN_Splitter_10991;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10866WEIGHTED_ROUND_ROBIN_Splitter_10875;
buffer_float_t SplitJoin78_CombineDFT_Fiss_11012_11033_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10856WEIGHTED_ROUND_ROBIN_Splitter_10859;
buffer_float_t SplitJoin16_CombineDFT_Fiss_11003_11024_join[8];
buffer_float_t FFTReorderSimple_10831WEIGHTED_ROUND_ROBIN_Splitter_10925;
buffer_float_t SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[8];
buffer_float_t FFTReorderSimple_10820WEIGHTED_ROUND_ROBIN_Splitter_10855;
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[8];
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[8];
buffer_float_t SplitJoin0_FFTTestSource_Fiss_10995_11016_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10886WEIGHTED_ROUND_ROBIN_Splitter_10895;
buffer_float_t SplitJoin76_CombineDFT_Fiss_11011_11032_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10956WEIGHTED_ROUND_ROBIN_Splitter_10965;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10926WEIGHTED_ROUND_ROBIN_Splitter_10929;
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_10998_11019_split[4];
buffer_float_t SplitJoin80_CombineDFT_Fiss_11013_11034_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10852WEIGHTED_ROUND_ROBIN_Splitter_10843;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10976WEIGHTED_ROUND_ROBIN_Splitter_10985;
buffer_float_t SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[8];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10809_10845_10996_11017_split[2];
buffer_float_t SplitJoin74_CombineDFT_Fiss_11010_11031_join[8];
buffer_float_t SplitJoin14_CombineDFT_Fiss_11002_11023_join[8];
buffer_float_t SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[8];
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_10997_11018_split[2];
buffer_float_t SplitJoin82_CombineDFT_Fiss_11014_11035_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10916WEIGHTED_ROUND_ROBIN_Splitter_10921;
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10906WEIGHTED_ROUND_ROBIN_Splitter_10915;
buffer_float_t SplitJoin76_CombineDFT_Fiss_11011_11032_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10992CombineDFT_10841;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10936WEIGHTED_ROUND_ROBIN_Splitter_10945;
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_10997_11018_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10930WEIGHTED_ROUND_ROBIN_Splitter_10935;
buffer_float_t SplitJoin14_CombineDFT_Fiss_11002_11023_split[8];
buffer_float_t SplitJoin12_CombineDFT_Fiss_11001_11022_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10922CombineDFT_10830;
buffer_float_t SplitJoin68_FFTReorderSimple_Fiss_11007_11028_join[4];
buffer_float_t SplitJoin0_FFTTestSource_Fiss_10995_11016_split[2];
buffer_float_t SplitJoin20_CombineDFT_Fiss_11005_11026_split[2];
buffer_float_t SplitJoin78_CombineDFT_Fiss_11012_11033_join[8];
buffer_float_t SplitJoin74_CombineDFT_Fiss_11010_11031_split[8];
buffer_float_t SplitJoin66_FFTReorderSimple_Fiss_11006_11027_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10844FloatPrinter_10842;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10946WEIGHTED_ROUND_ROBIN_Splitter_10955;
buffer_float_t SplitJoin18_CombineDFT_Fiss_11004_11025_split[4];
buffer_float_t SplitJoin80_CombineDFT_Fiss_11013_11034_join[4];
buffer_float_t SplitJoin82_CombineDFT_Fiss_11014_11035_split[2];
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[8];
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_10998_11019_join[4];
buffer_float_t SplitJoin12_CombineDFT_Fiss_11001_11022_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10966WEIGHTED_ROUND_ROBIN_Splitter_10975;
buffer_float_t SplitJoin18_CombineDFT_Fiss_11004_11025_join[4];
buffer_float_t SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[8];
buffer_float_t SplitJoin66_FFTReorderSimple_Fiss_11006_11027_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10860WEIGHTED_ROUND_ROBIN_Splitter_10865;


CombineDFT_10887_t CombineDFT_10887_s;
CombineDFT_10887_t CombineDFT_10888_s;
CombineDFT_10887_t CombineDFT_10889_s;
CombineDFT_10887_t CombineDFT_10890_s;
CombineDFT_10887_t CombineDFT_10891_s;
CombineDFT_10887_t CombineDFT_10892_s;
CombineDFT_10887_t CombineDFT_10893_s;
CombineDFT_10887_t CombineDFT_10894_s;
CombineDFT_10897_t CombineDFT_10897_s;
CombineDFT_10897_t CombineDFT_10898_s;
CombineDFT_10897_t CombineDFT_10899_s;
CombineDFT_10897_t CombineDFT_10900_s;
CombineDFT_10897_t CombineDFT_10901_s;
CombineDFT_10897_t CombineDFT_10902_s;
CombineDFT_10897_t CombineDFT_10903_s;
CombineDFT_10897_t CombineDFT_10904_s;
CombineDFT_10907_t CombineDFT_10907_s;
CombineDFT_10907_t CombineDFT_10908_s;
CombineDFT_10907_t CombineDFT_10909_s;
CombineDFT_10907_t CombineDFT_10910_s;
CombineDFT_10907_t CombineDFT_10911_s;
CombineDFT_10907_t CombineDFT_10912_s;
CombineDFT_10907_t CombineDFT_10913_s;
CombineDFT_10907_t CombineDFT_10914_s;
CombineDFT_10917_t CombineDFT_10917_s;
CombineDFT_10917_t CombineDFT_10918_s;
CombineDFT_10917_t CombineDFT_10919_s;
CombineDFT_10917_t CombineDFT_10920_s;
CombineDFT_10923_t CombineDFT_10923_s;
CombineDFT_10923_t CombineDFT_10924_s;
CombineDFT_10830_t CombineDFT_10830_s;
CombineDFT_10887_t CombineDFT_10957_s;
CombineDFT_10887_t CombineDFT_10958_s;
CombineDFT_10887_t CombineDFT_10959_s;
CombineDFT_10887_t CombineDFT_10960_s;
CombineDFT_10887_t CombineDFT_10961_s;
CombineDFT_10887_t CombineDFT_10962_s;
CombineDFT_10887_t CombineDFT_10963_s;
CombineDFT_10887_t CombineDFT_10964_s;
CombineDFT_10897_t CombineDFT_10967_s;
CombineDFT_10897_t CombineDFT_10968_s;
CombineDFT_10897_t CombineDFT_10969_s;
CombineDFT_10897_t CombineDFT_10970_s;
CombineDFT_10897_t CombineDFT_10971_s;
CombineDFT_10897_t CombineDFT_10972_s;
CombineDFT_10897_t CombineDFT_10973_s;
CombineDFT_10897_t CombineDFT_10974_s;
CombineDFT_10907_t CombineDFT_10977_s;
CombineDFT_10907_t CombineDFT_10978_s;
CombineDFT_10907_t CombineDFT_10979_s;
CombineDFT_10907_t CombineDFT_10980_s;
CombineDFT_10907_t CombineDFT_10981_s;
CombineDFT_10907_t CombineDFT_10982_s;
CombineDFT_10907_t CombineDFT_10983_s;
CombineDFT_10907_t CombineDFT_10984_s;
CombineDFT_10917_t CombineDFT_10987_s;
CombineDFT_10917_t CombineDFT_10988_s;
CombineDFT_10917_t CombineDFT_10989_s;
CombineDFT_10917_t CombineDFT_10990_s;
CombineDFT_10923_t CombineDFT_10993_s;
CombineDFT_10923_t CombineDFT_10994_s;
CombineDFT_10830_t CombineDFT_10841_s;

void FFTTestSource_10853() {
	push_float(&SplitJoin0_FFTTestSource_Fiss_10995_11016_join[0], 0.0) ; 
	push_float(&SplitJoin0_FFTTestSource_Fiss_10995_11016_join[0], 0.0) ; 
	push_float(&SplitJoin0_FFTTestSource_Fiss_10995_11016_join[0], 1.0) ; 
	push_float(&SplitJoin0_FFTTestSource_Fiss_10995_11016_join[0], 0.0) ; 
	FOR(int, i, 0,  < , 124, i++) {
		push_float(&SplitJoin0_FFTTestSource_Fiss_10995_11016_join[0], 0.0) ; 
	}
	ENDFOR
}


void FFTTestSource_10854() {
	push_float(&SplitJoin0_FFTTestSource_Fiss_10995_11016_join[1], 0.0) ; 
	push_float(&SplitJoin0_FFTTestSource_Fiss_10995_11016_join[1], 0.0) ; 
	push_float(&SplitJoin0_FFTTestSource_Fiss_10995_11016_join[1], 1.0) ; 
	push_float(&SplitJoin0_FFTTestSource_Fiss_10995_11016_join[1], 0.0) ; 
	FOR(int, i, 0,  < , 124, i++) {
		push_float(&SplitJoin0_FFTTestSource_Fiss_10995_11016_join[1], 0.0) ; 
	}
	ENDFOR
}


void WEIGHTED_ROUND_ROBIN_Splitter_10851() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_10852() {
	FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10852WEIGHTED_ROUND_ROBIN_Splitter_10843, pop_float(&SplitJoin0_FFTTestSource_Fiss_10995_11016_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10852WEIGHTED_ROUND_ROBIN_Splitter_10843, pop_float(&SplitJoin0_FFTTestSource_Fiss_10995_11016_join[1]));
	ENDFOR
}

void FFTReorderSimple_10820() {
	FOR(int, i, 0,  < , 128, i = (i + 4)) {
		push_float(&FFTReorderSimple_10820WEIGHTED_ROUND_ROBIN_Splitter_10855, peek_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10809_10845_10996_11017_split[0], i)) ; 
		push_float(&FFTReorderSimple_10820WEIGHTED_ROUND_ROBIN_Splitter_10855, peek_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10809_10845_10996_11017_split[0], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 128, i = (i + 4)) {
		push_float(&FFTReorderSimple_10820WEIGHTED_ROUND_ROBIN_Splitter_10855, peek_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10809_10845_10996_11017_split[0], i)) ; 
		push_float(&FFTReorderSimple_10820WEIGHTED_ROUND_ROBIN_Splitter_10855, peek_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10809_10845_10996_11017_split[0], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 64, i++) {
		pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10809_10845_10996_11017_split[0]) ; 
		pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10809_10845_10996_11017_split[0]) ; 
	}
	ENDFOR
}


void FFTReorderSimple_10857() {
	FOR(int, i, 0,  < , 64, i = (i + 4)) {
		push_float(&SplitJoin4_FFTReorderSimple_Fiss_10997_11018_join[0], peek_float(&SplitJoin4_FFTReorderSimple_Fiss_10997_11018_split[0], i)) ; 
		push_float(&SplitJoin4_FFTReorderSimple_Fiss_10997_11018_join[0], peek_float(&SplitJoin4_FFTReorderSimple_Fiss_10997_11018_split[0], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 64, i = (i + 4)) {
		push_float(&SplitJoin4_FFTReorderSimple_Fiss_10997_11018_join[0], peek_float(&SplitJoin4_FFTReorderSimple_Fiss_10997_11018_split[0], i)) ; 
		push_float(&SplitJoin4_FFTReorderSimple_Fiss_10997_11018_join[0], peek_float(&SplitJoin4_FFTReorderSimple_Fiss_10997_11018_split[0], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 32, i++) {
		pop_float(&SplitJoin4_FFTReorderSimple_Fiss_10997_11018_split[0]) ; 
		pop_float(&SplitJoin4_FFTReorderSimple_Fiss_10997_11018_split[0]) ; 
	}
	ENDFOR
}


void FFTReorderSimple_10858() {
	FOR(int, i, 0,  < , 64, i = (i + 4)) {
		push_float(&SplitJoin4_FFTReorderSimple_Fiss_10997_11018_join[1], peek_float(&SplitJoin4_FFTReorderSimple_Fiss_10997_11018_split[1], i)) ; 
		push_float(&SplitJoin4_FFTReorderSimple_Fiss_10997_11018_join[1], peek_float(&SplitJoin4_FFTReorderSimple_Fiss_10997_11018_split[1], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 64, i = (i + 4)) {
		push_float(&SplitJoin4_FFTReorderSimple_Fiss_10997_11018_join[1], peek_float(&SplitJoin4_FFTReorderSimple_Fiss_10997_11018_split[1], i)) ; 
		push_float(&SplitJoin4_FFTReorderSimple_Fiss_10997_11018_join[1], peek_float(&SplitJoin4_FFTReorderSimple_Fiss_10997_11018_split[1], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 32, i++) {
		pop_float(&SplitJoin4_FFTReorderSimple_Fiss_10997_11018_split[1]) ; 
		pop_float(&SplitJoin4_FFTReorderSimple_Fiss_10997_11018_split[1]) ; 
	}
	ENDFOR
}


void WEIGHTED_ROUND_ROBIN_Splitter_10855() {
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&SplitJoin4_FFTReorderSimple_Fiss_10997_11018_split[0], pop_float(&FFTReorderSimple_10820WEIGHTED_ROUND_ROBIN_Splitter_10855));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&SplitJoin4_FFTReorderSimple_Fiss_10997_11018_split[1], pop_float(&FFTReorderSimple_10820WEIGHTED_ROUND_ROBIN_Splitter_10855));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10856() {
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10856WEIGHTED_ROUND_ROBIN_Splitter_10859, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_10997_11018_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10856WEIGHTED_ROUND_ROBIN_Splitter_10859, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_10997_11018_join[1]));
	ENDFOR
}

void FFTReorderSimple_10861() {
	FOR(int, i, 0,  < , 32, i = (i + 4)) {
		push_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_join[0], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_split[0], i)) ; 
		push_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_join[0], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_split[0], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 32, i = (i + 4)) {
		push_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_join[0], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_split[0], i)) ; 
		push_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_join[0], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_split[0], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_split[0]) ; 
		pop_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_split[0]) ; 
	}
	ENDFOR
}


void FFTReorderSimple_10862() {
	FOR(int, i, 0,  < , 32, i = (i + 4)) {
		push_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_join[1], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_split[1], i)) ; 
		push_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_join[1], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_split[1], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 32, i = (i + 4)) {
		push_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_join[1], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_split[1], i)) ; 
		push_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_join[1], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_split[1], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_split[1]) ; 
		pop_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_split[1]) ; 
	}
	ENDFOR
}


void FFTReorderSimple_10863() {
	FOR(int, i, 0,  < , 32, i = (i + 4)) {
		push_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_join[2], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_split[2], i)) ; 
		push_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_join[2], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_split[2], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 32, i = (i + 4)) {
		push_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_join[2], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_split[2], i)) ; 
		push_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_join[2], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_split[2], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_split[2]) ; 
		pop_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_split[2]) ; 
	}
	ENDFOR
}


void FFTReorderSimple_10864() {
	FOR(int, i, 0,  < , 32, i = (i + 4)) {
		push_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_join[3], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_split[3], i)) ; 
		push_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_join[3], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_split[3], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 32, i = (i + 4)) {
		push_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_join[3], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_split[3], i)) ; 
		push_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_join[3], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_split[3], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_split[3]) ; 
		pop_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_split[3]) ; 
	}
	ENDFOR
}


void WEIGHTED_ROUND_ROBIN_Splitter_10859() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10856WEIGHTED_ROUND_ROBIN_Splitter_10859));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10860() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10860WEIGHTED_ROUND_ROBIN_Splitter_10865, pop_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void FFTReorderSimple_10867() {
	FOR(int, i, 0,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[0], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[0], i)) ; 
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[0], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[0], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[0], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[0], i)) ; 
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[0], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[0], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 8, i++) {
		pop_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[0]) ; 
		pop_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[0]) ; 
	}
	ENDFOR
}


void FFTReorderSimple_10868() {
	FOR(int, i, 0,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[1], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[1], i)) ; 
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[1], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[1], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[1], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[1], i)) ; 
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[1], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[1], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 8, i++) {
		pop_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[1]) ; 
		pop_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[1]) ; 
	}
	ENDFOR
}


void FFTReorderSimple_10869() {
	FOR(int, i, 0,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[2], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[2], i)) ; 
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[2], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[2], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[2], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[2], i)) ; 
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[2], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[2], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 8, i++) {
		pop_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[2]) ; 
		pop_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[2]) ; 
	}
	ENDFOR
}


void FFTReorderSimple_10870() {
	FOR(int, i, 0,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[3], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[3], i)) ; 
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[3], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[3], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[3], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[3], i)) ; 
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[3], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[3], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 8, i++) {
		pop_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[3]) ; 
		pop_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[3]) ; 
	}
	ENDFOR
}


void FFTReorderSimple_10871() {
	FOR(int, i, 0,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[4], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[4], i)) ; 
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[4], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[4], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[4], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[4], i)) ; 
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[4], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[4], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 8, i++) {
		pop_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[4]) ; 
		pop_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[4]) ; 
	}
	ENDFOR
}


void FFTReorderSimple_10872() {
	FOR(int, i, 0,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[5], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[5], i)) ; 
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[5], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[5], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[5], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[5], i)) ; 
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[5], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[5], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 8, i++) {
		pop_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[5]) ; 
		pop_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[5]) ; 
	}
	ENDFOR
}


void FFTReorderSimple_10873() {
	FOR(int, i, 0,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[6], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[6], i)) ; 
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[6], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[6], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[6], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[6], i)) ; 
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[6], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[6], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 8, i++) {
		pop_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[6]) ; 
		pop_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[6]) ; 
	}
	ENDFOR
}


void FFTReorderSimple_10874() {
	FOR(int, i, 0,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[7], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[7], i)) ; 
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[7], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[7], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[7], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[7], i)) ; 
		push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[7], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[7], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 8, i++) {
		pop_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[7]) ; 
		pop_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[7]) ; 
	}
	ENDFOR
}


void WEIGHTED_ROUND_ROBIN_Splitter_10865() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10860WEIGHTED_ROUND_ROBIN_Splitter_10865));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10866() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10866WEIGHTED_ROUND_ROBIN_Splitter_10875, pop_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void FFTReorderSimple_10877(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[0], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[0], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[0], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[0], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[0], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[0], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[0]) ; 
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_10878(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[1], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[1], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[1], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[1], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[1], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[1], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[1]) ; 
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_10879(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[2], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[2], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[2], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[2], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[2], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[2], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[2], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[2], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[2]) ; 
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_10880(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[3], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[3], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[3], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[3], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[3], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[3], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[3], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[3], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[3]) ; 
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_10881(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[4], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[4], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[4], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[4], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[4], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[4], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[4], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[4], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[4]) ; 
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_10882(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[5], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[5], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[5], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[5], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[5], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[5], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[5], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[5], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[5]) ; 
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_10883(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[6], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[6], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[6], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[6], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[6], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[6], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[6], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[6], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[6]) ; 
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_10884(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[7], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[7], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[7], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[7], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[7], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[7], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[7], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[7], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[7]) ; 
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10875() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10866WEIGHTED_ROUND_ROBIN_Splitter_10875));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10876() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10876WEIGHTED_ROUND_ROBIN_Splitter_10885, pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_10887(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[0], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[0], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[0], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[0], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10887_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10887_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[0]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_10888(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[1], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[1], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[1], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[1], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10888_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10888_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[1]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_10889(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[2], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[2], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[2], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[2], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10889_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10889_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[2]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_10890(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[3], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[3], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[3], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[3], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10890_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10890_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[3]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_10891(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[4], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[4], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[4], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[4], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10891_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10891_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[4]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_10892(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[5], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[5], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[5], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[5], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10892_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10892_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[5]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_10893(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[6], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[6], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[6], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[6], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10893_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10893_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[6]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_10894(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[7], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[7], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[7], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[7], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10894_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10894_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[7]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10885() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10876WEIGHTED_ROUND_ROBIN_Splitter_10885));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10886() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10886WEIGHTED_ROUND_ROBIN_Splitter_10895, pop_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_10897(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[0], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[0], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[0], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[0], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10897_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10897_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[0]) ; 
			push_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_10898(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[1], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[1], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[1], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[1], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10898_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10898_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[1]) ; 
			push_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_10899(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[2], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[2], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[2], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[2], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10899_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10899_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[2]) ; 
			push_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_10900(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[3], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[3], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[3], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[3], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10900_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10900_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[3]) ; 
			push_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_10901(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[4], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[4], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[4], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[4], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10901_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10901_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[4]) ; 
			push_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_10902(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[5], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[5], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[5], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[5], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10902_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10902_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[5]) ; 
			push_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_10903(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[6], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[6], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[6], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[6], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10903_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10903_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[6]) ; 
			push_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_10904(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[7], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[7], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[7], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[7], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10904_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10904_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[7]) ; 
			push_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10895() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10886WEIGHTED_ROUND_ROBIN_Splitter_10895));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10896() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10896WEIGHTED_ROUND_ROBIN_Splitter_10905, pop_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_10907() {
	float results[16];
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		int i_plus_1 = 0;
		float y0_r = 0.0;
		float y0_i = 0.0;
		float y1_r = 0.0;
		float y1_i = 0.0;
		float weight_real = 0.0;
		float weight_imag = 0.0;
		float y1w_r = 0.0;
		float y1w_i = 0.0;
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		i_plus_1 = (i + 1) ; 
		y0_r = 0.0 ; 
		y0_r = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[0], i) ; 
		y0_i = 0.0 ; 
		y0_i = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[0], i_plus_1) ; 
		y1_r = 0.0 ; 
		y1_r = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[0], (8 + i)) ; 
		y1_i = 0.0 ; 
		y1_i = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[0], (8 + i_plus_1)) ; 
		weight_real = 0.0 ; 
		weight_real = CombineDFT_10907_s.w[i] ; 
		weight_imag = 0.0 ; 
		weight_imag = CombineDFT_10907_s.w[i_plus_1] ; 
		y1w_r = 0.0 ; 
		y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
		y1w_i = 0.0 ; 
		y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
		results[i] = (y0_r + y1w_r) ; 
		results[(i + 1)] = (y0_i + y1w_i) ; 
		results[(8 + i)] = (y0_r - y1w_r) ; 
		results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[0]) ; 
		push_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_join[0], results[i]) ; 
	}
	ENDFOR
}


void CombineDFT_10908() {
	float results[16];
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		int i_plus_1 = 0;
		float y0_r = 0.0;
		float y0_i = 0.0;
		float y1_r = 0.0;
		float y1_i = 0.0;
		float weight_real = 0.0;
		float weight_imag = 0.0;
		float y1w_r = 0.0;
		float y1w_i = 0.0;
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		i_plus_1 = (i + 1) ; 
		y0_r = 0.0 ; 
		y0_r = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[1], i) ; 
		y0_i = 0.0 ; 
		y0_i = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[1], i_plus_1) ; 
		y1_r = 0.0 ; 
		y1_r = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[1], (8 + i)) ; 
		y1_i = 0.0 ; 
		y1_i = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[1], (8 + i_plus_1)) ; 
		weight_real = 0.0 ; 
		weight_real = CombineDFT_10908_s.w[i] ; 
		weight_imag = 0.0 ; 
		weight_imag = CombineDFT_10908_s.w[i_plus_1] ; 
		y1w_r = 0.0 ; 
		y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
		y1w_i = 0.0 ; 
		y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
		results[i] = (y0_r + y1w_r) ; 
		results[(i + 1)] = (y0_i + y1w_i) ; 
		results[(8 + i)] = (y0_r - y1w_r) ; 
		results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[1]) ; 
		push_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_join[1], results[i]) ; 
	}
	ENDFOR
}


void CombineDFT_10909() {
	float results[16];
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		int i_plus_1 = 0;
		float y0_r = 0.0;
		float y0_i = 0.0;
		float y1_r = 0.0;
		float y1_i = 0.0;
		float weight_real = 0.0;
		float weight_imag = 0.0;
		float y1w_r = 0.0;
		float y1w_i = 0.0;
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		i_plus_1 = (i + 1) ; 
		y0_r = 0.0 ; 
		y0_r = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[2], i) ; 
		y0_i = 0.0 ; 
		y0_i = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[2], i_plus_1) ; 
		y1_r = 0.0 ; 
		y1_r = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[2], (8 + i)) ; 
		y1_i = 0.0 ; 
		y1_i = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[2], (8 + i_plus_1)) ; 
		weight_real = 0.0 ; 
		weight_real = CombineDFT_10909_s.w[i] ; 
		weight_imag = 0.0 ; 
		weight_imag = CombineDFT_10909_s.w[i_plus_1] ; 
		y1w_r = 0.0 ; 
		y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
		y1w_i = 0.0 ; 
		y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
		results[i] = (y0_r + y1w_r) ; 
		results[(i + 1)] = (y0_i + y1w_i) ; 
		results[(8 + i)] = (y0_r - y1w_r) ; 
		results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[2]) ; 
		push_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_join[2], results[i]) ; 
	}
	ENDFOR
}


void CombineDFT_10910() {
	float results[16];
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		int i_plus_1 = 0;
		float y0_r = 0.0;
		float y0_i = 0.0;
		float y1_r = 0.0;
		float y1_i = 0.0;
		float weight_real = 0.0;
		float weight_imag = 0.0;
		float y1w_r = 0.0;
		float y1w_i = 0.0;
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		i_plus_1 = (i + 1) ; 
		y0_r = 0.0 ; 
		y0_r = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[3], i) ; 
		y0_i = 0.0 ; 
		y0_i = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[3], i_plus_1) ; 
		y1_r = 0.0 ; 
		y1_r = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[3], (8 + i)) ; 
		y1_i = 0.0 ; 
		y1_i = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[3], (8 + i_plus_1)) ; 
		weight_real = 0.0 ; 
		weight_real = CombineDFT_10910_s.w[i] ; 
		weight_imag = 0.0 ; 
		weight_imag = CombineDFT_10910_s.w[i_plus_1] ; 
		y1w_r = 0.0 ; 
		y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
		y1w_i = 0.0 ; 
		y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
		results[i] = (y0_r + y1w_r) ; 
		results[(i + 1)] = (y0_i + y1w_i) ; 
		results[(8 + i)] = (y0_r - y1w_r) ; 
		results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[3]) ; 
		push_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_join[3], results[i]) ; 
	}
	ENDFOR
}


void CombineDFT_10911() {
	float results[16];
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		int i_plus_1 = 0;
		float y0_r = 0.0;
		float y0_i = 0.0;
		float y1_r = 0.0;
		float y1_i = 0.0;
		float weight_real = 0.0;
		float weight_imag = 0.0;
		float y1w_r = 0.0;
		float y1w_i = 0.0;
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		i_plus_1 = (i + 1) ; 
		y0_r = 0.0 ; 
		y0_r = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[4], i) ; 
		y0_i = 0.0 ; 
		y0_i = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[4], i_plus_1) ; 
		y1_r = 0.0 ; 
		y1_r = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[4], (8 + i)) ; 
		y1_i = 0.0 ; 
		y1_i = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[4], (8 + i_plus_1)) ; 
		weight_real = 0.0 ; 
		weight_real = CombineDFT_10911_s.w[i] ; 
		weight_imag = 0.0 ; 
		weight_imag = CombineDFT_10911_s.w[i_plus_1] ; 
		y1w_r = 0.0 ; 
		y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
		y1w_i = 0.0 ; 
		y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
		results[i] = (y0_r + y1w_r) ; 
		results[(i + 1)] = (y0_i + y1w_i) ; 
		results[(8 + i)] = (y0_r - y1w_r) ; 
		results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[4]) ; 
		push_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_join[4], results[i]) ; 
	}
	ENDFOR
}


void CombineDFT_10912() {
	float results[16];
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		int i_plus_1 = 0;
		float y0_r = 0.0;
		float y0_i = 0.0;
		float y1_r = 0.0;
		float y1_i = 0.0;
		float weight_real = 0.0;
		float weight_imag = 0.0;
		float y1w_r = 0.0;
		float y1w_i = 0.0;
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		i_plus_1 = (i + 1) ; 
		y0_r = 0.0 ; 
		y0_r = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[5], i) ; 
		y0_i = 0.0 ; 
		y0_i = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[5], i_plus_1) ; 
		y1_r = 0.0 ; 
		y1_r = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[5], (8 + i)) ; 
		y1_i = 0.0 ; 
		y1_i = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[5], (8 + i_plus_1)) ; 
		weight_real = 0.0 ; 
		weight_real = CombineDFT_10912_s.w[i] ; 
		weight_imag = 0.0 ; 
		weight_imag = CombineDFT_10912_s.w[i_plus_1] ; 
		y1w_r = 0.0 ; 
		y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
		y1w_i = 0.0 ; 
		y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
		results[i] = (y0_r + y1w_r) ; 
		results[(i + 1)] = (y0_i + y1w_i) ; 
		results[(8 + i)] = (y0_r - y1w_r) ; 
		results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[5]) ; 
		push_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_join[5], results[i]) ; 
	}
	ENDFOR
}


void CombineDFT_10913() {
	float results[16];
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		int i_plus_1 = 0;
		float y0_r = 0.0;
		float y0_i = 0.0;
		float y1_r = 0.0;
		float y1_i = 0.0;
		float weight_real = 0.0;
		float weight_imag = 0.0;
		float y1w_r = 0.0;
		float y1w_i = 0.0;
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		i_plus_1 = (i + 1) ; 
		y0_r = 0.0 ; 
		y0_r = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[6], i) ; 
		y0_i = 0.0 ; 
		y0_i = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[6], i_plus_1) ; 
		y1_r = 0.0 ; 
		y1_r = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[6], (8 + i)) ; 
		y1_i = 0.0 ; 
		y1_i = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[6], (8 + i_plus_1)) ; 
		weight_real = 0.0 ; 
		weight_real = CombineDFT_10913_s.w[i] ; 
		weight_imag = 0.0 ; 
		weight_imag = CombineDFT_10913_s.w[i_plus_1] ; 
		y1w_r = 0.0 ; 
		y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
		y1w_i = 0.0 ; 
		y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
		results[i] = (y0_r + y1w_r) ; 
		results[(i + 1)] = (y0_i + y1w_i) ; 
		results[(8 + i)] = (y0_r - y1w_r) ; 
		results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[6]) ; 
		push_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_join[6], results[i]) ; 
	}
	ENDFOR
}


void CombineDFT_10914() {
	float results[16];
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		int i_plus_1 = 0;
		float y0_r = 0.0;
		float y0_i = 0.0;
		float y1_r = 0.0;
		float y1_i = 0.0;
		float weight_real = 0.0;
		float weight_imag = 0.0;
		float y1w_r = 0.0;
		float y1w_i = 0.0;
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		i_plus_1 = (i + 1) ; 
		y0_r = 0.0 ; 
		y0_r = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[7], i) ; 
		y0_i = 0.0 ; 
		y0_i = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[7], i_plus_1) ; 
		y1_r = 0.0 ; 
		y1_r = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[7], (8 + i)) ; 
		y1_i = 0.0 ; 
		y1_i = peek_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[7], (8 + i_plus_1)) ; 
		weight_real = 0.0 ; 
		weight_real = CombineDFT_10914_s.w[i] ; 
		weight_imag = 0.0 ; 
		weight_imag = CombineDFT_10914_s.w[i_plus_1] ; 
		y1w_r = 0.0 ; 
		y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
		y1w_i = 0.0 ; 
		y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
		results[i] = (y0_r + y1w_r) ; 
		results[(i + 1)] = (y0_i + y1w_i) ; 
		results[(8 + i)] = (y0_r - y1w_r) ; 
		results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[7]) ; 
		push_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_join[7], results[i]) ; 
	}
	ENDFOR
}


void WEIGHTED_ROUND_ROBIN_Splitter_10905() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
			push_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10896WEIGHTED_ROUND_ROBIN_Splitter_10905));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10906() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10906WEIGHTED_ROUND_ROBIN_Splitter_10915, pop_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void CombineDFT_10917() {
	float results[32];
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		int i_plus_1 = 0;
		float y0_r = 0.0;
		float y0_i = 0.0;
		float y1_r = 0.0;
		float y1_i = 0.0;
		float weight_real = 0.0;
		float weight_imag = 0.0;
		float y1w_r = 0.0;
		float y1w_i = 0.0;
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		i_plus_1 = (i + 1) ; 
		y0_r = 0.0 ; 
		y0_r = peek_float(&SplitJoin18_CombineDFT_Fiss_11004_11025_split[0], i) ; 
		y0_i = 0.0 ; 
		y0_i = peek_float(&SplitJoin18_CombineDFT_Fiss_11004_11025_split[0], i_plus_1) ; 
		y1_r = 0.0 ; 
		y1_r = peek_float(&SplitJoin18_CombineDFT_Fiss_11004_11025_split[0], (16 + i)) ; 
		y1_i = 0.0 ; 
		y1_i = peek_float(&SplitJoin18_CombineDFT_Fiss_11004_11025_split[0], (16 + i_plus_1)) ; 
		weight_real = 0.0 ; 
		weight_real = CombineDFT_10917_s.w[i] ; 
		weight_imag = 0.0 ; 
		weight_imag = CombineDFT_10917_s.w[i_plus_1] ; 
		y1w_r = 0.0 ; 
		y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
		y1w_i = 0.0 ; 
		y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
		results[i] = (y0_r + y1w_r) ; 
		results[(i + 1)] = (y0_i + y1w_i) ; 
		results[(16 + i)] = (y0_r - y1w_r) ; 
		results[((16 + i) + 1)] = (y0_i - y1w_i) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 32, i++) {
		pop_float(&SplitJoin18_CombineDFT_Fiss_11004_11025_split[0]) ; 
		push_float(&SplitJoin18_CombineDFT_Fiss_11004_11025_join[0], results[i]) ; 
	}
	ENDFOR
}


void CombineDFT_10918() {
	float results[32];
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		int i_plus_1 = 0;
		float y0_r = 0.0;
		float y0_i = 0.0;
		float y1_r = 0.0;
		float y1_i = 0.0;
		float weight_real = 0.0;
		float weight_imag = 0.0;
		float y1w_r = 0.0;
		float y1w_i = 0.0;
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		i_plus_1 = (i + 1) ; 
		y0_r = 0.0 ; 
		y0_r = peek_float(&SplitJoin18_CombineDFT_Fiss_11004_11025_split[1], i) ; 
		y0_i = 0.0 ; 
		y0_i = peek_float(&SplitJoin18_CombineDFT_Fiss_11004_11025_split[1], i_plus_1) ; 
		y1_r = 0.0 ; 
		y1_r = peek_float(&SplitJoin18_CombineDFT_Fiss_11004_11025_split[1], (16 + i)) ; 
		y1_i = 0.0 ; 
		y1_i = peek_float(&SplitJoin18_CombineDFT_Fiss_11004_11025_split[1], (16 + i_plus_1)) ; 
		weight_real = 0.0 ; 
		weight_real = CombineDFT_10918_s.w[i] ; 
		weight_imag = 0.0 ; 
		weight_imag = CombineDFT_10918_s.w[i_plus_1] ; 
		y1w_r = 0.0 ; 
		y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
		y1w_i = 0.0 ; 
		y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
		results[i] = (y0_r + y1w_r) ; 
		results[(i + 1)] = (y0_i + y1w_i) ; 
		results[(16 + i)] = (y0_r - y1w_r) ; 
		results[((16 + i) + 1)] = (y0_i - y1w_i) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 32, i++) {
		pop_float(&SplitJoin18_CombineDFT_Fiss_11004_11025_split[1]) ; 
		push_float(&SplitJoin18_CombineDFT_Fiss_11004_11025_join[1], results[i]) ; 
	}
	ENDFOR
}


void CombineDFT_10919() {
	float results[32];
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		int i_plus_1 = 0;
		float y0_r = 0.0;
		float y0_i = 0.0;
		float y1_r = 0.0;
		float y1_i = 0.0;
		float weight_real = 0.0;
		float weight_imag = 0.0;
		float y1w_r = 0.0;
		float y1w_i = 0.0;
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		i_plus_1 = (i + 1) ; 
		y0_r = 0.0 ; 
		y0_r = peek_float(&SplitJoin18_CombineDFT_Fiss_11004_11025_split[2], i) ; 
		y0_i = 0.0 ; 
		y0_i = peek_float(&SplitJoin18_CombineDFT_Fiss_11004_11025_split[2], i_plus_1) ; 
		y1_r = 0.0 ; 
		y1_r = peek_float(&SplitJoin18_CombineDFT_Fiss_11004_11025_split[2], (16 + i)) ; 
		y1_i = 0.0 ; 
		y1_i = peek_float(&SplitJoin18_CombineDFT_Fiss_11004_11025_split[2], (16 + i_plus_1)) ; 
		weight_real = 0.0 ; 
		weight_real = CombineDFT_10919_s.w[i] ; 
		weight_imag = 0.0 ; 
		weight_imag = CombineDFT_10919_s.w[i_plus_1] ; 
		y1w_r = 0.0 ; 
		y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
		y1w_i = 0.0 ; 
		y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
		results[i] = (y0_r + y1w_r) ; 
		results[(i + 1)] = (y0_i + y1w_i) ; 
		results[(16 + i)] = (y0_r - y1w_r) ; 
		results[((16 + i) + 1)] = (y0_i - y1w_i) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 32, i++) {
		pop_float(&SplitJoin18_CombineDFT_Fiss_11004_11025_split[2]) ; 
		push_float(&SplitJoin18_CombineDFT_Fiss_11004_11025_join[2], results[i]) ; 
	}
	ENDFOR
}


void CombineDFT_10920() {
	float results[32];
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		int i_plus_1 = 0;
		float y0_r = 0.0;
		float y0_i = 0.0;
		float y1_r = 0.0;
		float y1_i = 0.0;
		float weight_real = 0.0;
		float weight_imag = 0.0;
		float y1w_r = 0.0;
		float y1w_i = 0.0;
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		i_plus_1 = (i + 1) ; 
		y0_r = 0.0 ; 
		y0_r = peek_float(&SplitJoin18_CombineDFT_Fiss_11004_11025_split[3], i) ; 
		y0_i = 0.0 ; 
		y0_i = peek_float(&SplitJoin18_CombineDFT_Fiss_11004_11025_split[3], i_plus_1) ; 
		y1_r = 0.0 ; 
		y1_r = peek_float(&SplitJoin18_CombineDFT_Fiss_11004_11025_split[3], (16 + i)) ; 
		y1_i = 0.0 ; 
		y1_i = peek_float(&SplitJoin18_CombineDFT_Fiss_11004_11025_split[3], (16 + i_plus_1)) ; 
		weight_real = 0.0 ; 
		weight_real = CombineDFT_10920_s.w[i] ; 
		weight_imag = 0.0 ; 
		weight_imag = CombineDFT_10920_s.w[i_plus_1] ; 
		y1w_r = 0.0 ; 
		y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
		y1w_i = 0.0 ; 
		y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
		results[i] = (y0_r + y1w_r) ; 
		results[(i + 1)] = (y0_i + y1w_i) ; 
		results[(16 + i)] = (y0_r - y1w_r) ; 
		results[((16 + i) + 1)] = (y0_i - y1w_i) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 32, i++) {
		pop_float(&SplitJoin18_CombineDFT_Fiss_11004_11025_split[3]) ; 
		push_float(&SplitJoin18_CombineDFT_Fiss_11004_11025_join[3], results[i]) ; 
	}
	ENDFOR
}


void WEIGHTED_ROUND_ROBIN_Splitter_10915() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
			push_float(&SplitJoin18_CombineDFT_Fiss_11004_11025_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10906WEIGHTED_ROUND_ROBIN_Splitter_10915));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10916() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10916WEIGHTED_ROUND_ROBIN_Splitter_10921, pop_float(&SplitJoin18_CombineDFT_Fiss_11004_11025_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void CombineDFT_10923() {
	float results[64];
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		int i_plus_1 = 0;
		float y0_r = 0.0;
		float y0_i = 0.0;
		float y1_r = 0.0;
		float y1_i = 0.0;
		float weight_real = 0.0;
		float weight_imag = 0.0;
		float y1w_r = 0.0;
		float y1w_i = 0.0;
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		i_plus_1 = (i + 1) ; 
		y0_r = 0.0 ; 
		y0_r = peek_float(&SplitJoin20_CombineDFT_Fiss_11005_11026_split[0], i) ; 
		y0_i = 0.0 ; 
		y0_i = peek_float(&SplitJoin20_CombineDFT_Fiss_11005_11026_split[0], i_plus_1) ; 
		y1_r = 0.0 ; 
		y1_r = peek_float(&SplitJoin20_CombineDFT_Fiss_11005_11026_split[0], (32 + i)) ; 
		y1_i = 0.0 ; 
		y1_i = peek_float(&SplitJoin20_CombineDFT_Fiss_11005_11026_split[0], (32 + i_plus_1)) ; 
		weight_real = 0.0 ; 
		weight_real = CombineDFT_10923_s.w[i] ; 
		weight_imag = 0.0 ; 
		weight_imag = CombineDFT_10923_s.w[i_plus_1] ; 
		y1w_r = 0.0 ; 
		y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
		y1w_i = 0.0 ; 
		y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
		results[i] = (y0_r + y1w_r) ; 
		results[(i + 1)] = (y0_i + y1w_i) ; 
		results[(32 + i)] = (y0_r - y1w_r) ; 
		results[((32 + i) + 1)] = (y0_i - y1w_i) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 64, i++) {
		pop_float(&SplitJoin20_CombineDFT_Fiss_11005_11026_split[0]) ; 
		push_float(&SplitJoin20_CombineDFT_Fiss_11005_11026_join[0], results[i]) ; 
	}
	ENDFOR
}


void CombineDFT_10924() {
	float results[64];
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		int i_plus_1 = 0;
		float y0_r = 0.0;
		float y0_i = 0.0;
		float y1_r = 0.0;
		float y1_i = 0.0;
		float weight_real = 0.0;
		float weight_imag = 0.0;
		float y1w_r = 0.0;
		float y1w_i = 0.0;
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		i_plus_1 = (i + 1) ; 
		y0_r = 0.0 ; 
		y0_r = peek_float(&SplitJoin20_CombineDFT_Fiss_11005_11026_split[1], i) ; 
		y0_i = 0.0 ; 
		y0_i = peek_float(&SplitJoin20_CombineDFT_Fiss_11005_11026_split[1], i_plus_1) ; 
		y1_r = 0.0 ; 
		y1_r = peek_float(&SplitJoin20_CombineDFT_Fiss_11005_11026_split[1], (32 + i)) ; 
		y1_i = 0.0 ; 
		y1_i = peek_float(&SplitJoin20_CombineDFT_Fiss_11005_11026_split[1], (32 + i_plus_1)) ; 
		weight_real = 0.0 ; 
		weight_real = CombineDFT_10924_s.w[i] ; 
		weight_imag = 0.0 ; 
		weight_imag = CombineDFT_10924_s.w[i_plus_1] ; 
		y1w_r = 0.0 ; 
		y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
		y1w_i = 0.0 ; 
		y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
		results[i] = (y0_r + y1w_r) ; 
		results[(i + 1)] = (y0_i + y1w_i) ; 
		results[(32 + i)] = (y0_r - y1w_r) ; 
		results[((32 + i) + 1)] = (y0_i - y1w_i) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 64, i++) {
		pop_float(&SplitJoin20_CombineDFT_Fiss_11005_11026_split[1]) ; 
		push_float(&SplitJoin20_CombineDFT_Fiss_11005_11026_join[1], results[i]) ; 
	}
	ENDFOR
}


void WEIGHTED_ROUND_ROBIN_Splitter_10921() {
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&SplitJoin20_CombineDFT_Fiss_11005_11026_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10916WEIGHTED_ROUND_ROBIN_Splitter_10921));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&SplitJoin20_CombineDFT_Fiss_11005_11026_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10916WEIGHTED_ROUND_ROBIN_Splitter_10921));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10922() {
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10922CombineDFT_10830, pop_float(&SplitJoin20_CombineDFT_Fiss_11005_11026_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10922CombineDFT_10830, pop_float(&SplitJoin20_CombineDFT_Fiss_11005_11026_join[1]));
	ENDFOR
}

void CombineDFT_10830() {
	float results[128];
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		int i_plus_1 = 0;
		float y0_r = 0.0;
		float y0_i = 0.0;
		float y1_r = 0.0;
		float y1_i = 0.0;
		float weight_real = 0.0;
		float weight_imag = 0.0;
		float y1w_r = 0.0;
		float y1w_i = 0.0;
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		i_plus_1 = (i + 1) ; 
		y0_r = 0.0 ; 
		y0_r = peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_10922CombineDFT_10830, i) ; 
		y0_i = 0.0 ; 
		y0_i = peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_10922CombineDFT_10830, i_plus_1) ; 
		y1_r = 0.0 ; 
		y1_r = peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_10922CombineDFT_10830, (64 + i)) ; 
		y1_i = 0.0 ; 
		y1_i = peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_10922CombineDFT_10830, (64 + i_plus_1)) ; 
		weight_real = 0.0 ; 
		weight_real = CombineDFT_10830_s.w[i] ; 
		weight_imag = 0.0 ; 
		weight_imag = CombineDFT_10830_s.w[i_plus_1] ; 
		y1w_r = 0.0 ; 
		y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
		y1w_i = 0.0 ; 
		y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
		results[i] = (y0_r + y1w_r) ; 
		results[(i + 1)] = (y0_i + y1w_i) ; 
		results[(64 + i)] = (y0_r - y1w_r) ; 
		results[((64 + i) + 1)] = (y0_i - y1w_i) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 128, i++) {
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10922CombineDFT_10830) ; 
		push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10809_10845_10996_11017_join[0], results[i]) ; 
	}
	ENDFOR
}


void FFTReorderSimple_10831() {
	FOR(int, i, 0,  < , 128, i = (i + 4)) {
		push_float(&FFTReorderSimple_10831WEIGHTED_ROUND_ROBIN_Splitter_10925, peek_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10809_10845_10996_11017_split[1], i)) ; 
		push_float(&FFTReorderSimple_10831WEIGHTED_ROUND_ROBIN_Splitter_10925, peek_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10809_10845_10996_11017_split[1], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 128, i = (i + 4)) {
		push_float(&FFTReorderSimple_10831WEIGHTED_ROUND_ROBIN_Splitter_10925, peek_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10809_10845_10996_11017_split[1], i)) ; 
		push_float(&FFTReorderSimple_10831WEIGHTED_ROUND_ROBIN_Splitter_10925, peek_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10809_10845_10996_11017_split[1], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 64, i++) {
		pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10809_10845_10996_11017_split[1]) ; 
		pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10809_10845_10996_11017_split[1]) ; 
	}
	ENDFOR
}


void FFTReorderSimple_10927() {
	FOR(int, i, 0,  < , 64, i = (i + 4)) {
		push_float(&SplitJoin66_FFTReorderSimple_Fiss_11006_11027_join[0], peek_float(&SplitJoin66_FFTReorderSimple_Fiss_11006_11027_split[0], i)) ; 
		push_float(&SplitJoin66_FFTReorderSimple_Fiss_11006_11027_join[0], peek_float(&SplitJoin66_FFTReorderSimple_Fiss_11006_11027_split[0], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 64, i = (i + 4)) {
		push_float(&SplitJoin66_FFTReorderSimple_Fiss_11006_11027_join[0], peek_float(&SplitJoin66_FFTReorderSimple_Fiss_11006_11027_split[0], i)) ; 
		push_float(&SplitJoin66_FFTReorderSimple_Fiss_11006_11027_join[0], peek_float(&SplitJoin66_FFTReorderSimple_Fiss_11006_11027_split[0], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 32, i++) {
		pop_float(&SplitJoin66_FFTReorderSimple_Fiss_11006_11027_split[0]) ; 
		pop_float(&SplitJoin66_FFTReorderSimple_Fiss_11006_11027_split[0]) ; 
	}
	ENDFOR
}


void FFTReorderSimple_10928() {
	FOR(int, i, 0,  < , 64, i = (i + 4)) {
		push_float(&SplitJoin66_FFTReorderSimple_Fiss_11006_11027_join[1], peek_float(&SplitJoin66_FFTReorderSimple_Fiss_11006_11027_split[1], i)) ; 
		push_float(&SplitJoin66_FFTReorderSimple_Fiss_11006_11027_join[1], peek_float(&SplitJoin66_FFTReorderSimple_Fiss_11006_11027_split[1], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 64, i = (i + 4)) {
		push_float(&SplitJoin66_FFTReorderSimple_Fiss_11006_11027_join[1], peek_float(&SplitJoin66_FFTReorderSimple_Fiss_11006_11027_split[1], i)) ; 
		push_float(&SplitJoin66_FFTReorderSimple_Fiss_11006_11027_join[1], peek_float(&SplitJoin66_FFTReorderSimple_Fiss_11006_11027_split[1], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 32, i++) {
		pop_float(&SplitJoin66_FFTReorderSimple_Fiss_11006_11027_split[1]) ; 
		pop_float(&SplitJoin66_FFTReorderSimple_Fiss_11006_11027_split[1]) ; 
	}
	ENDFOR
}


void WEIGHTED_ROUND_ROBIN_Splitter_10925() {
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&SplitJoin66_FFTReorderSimple_Fiss_11006_11027_split[0], pop_float(&FFTReorderSimple_10831WEIGHTED_ROUND_ROBIN_Splitter_10925));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&SplitJoin66_FFTReorderSimple_Fiss_11006_11027_split[1], pop_float(&FFTReorderSimple_10831WEIGHTED_ROUND_ROBIN_Splitter_10925));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10926() {
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10926WEIGHTED_ROUND_ROBIN_Splitter_10929, pop_float(&SplitJoin66_FFTReorderSimple_Fiss_11006_11027_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10926WEIGHTED_ROUND_ROBIN_Splitter_10929, pop_float(&SplitJoin66_FFTReorderSimple_Fiss_11006_11027_join[1]));
	ENDFOR
}

void FFTReorderSimple_10931() {
	FOR(int, i, 0,  < , 32, i = (i + 4)) {
		push_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_join[0], peek_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_split[0], i)) ; 
		push_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_join[0], peek_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_split[0], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 32, i = (i + 4)) {
		push_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_join[0], peek_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_split[0], i)) ; 
		push_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_join[0], peek_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_split[0], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_split[0]) ; 
		pop_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_split[0]) ; 
	}
	ENDFOR
}


void FFTReorderSimple_10932() {
	FOR(int, i, 0,  < , 32, i = (i + 4)) {
		push_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_join[1], peek_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_split[1], i)) ; 
		push_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_join[1], peek_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_split[1], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 32, i = (i + 4)) {
		push_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_join[1], peek_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_split[1], i)) ; 
		push_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_join[1], peek_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_split[1], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_split[1]) ; 
		pop_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_split[1]) ; 
	}
	ENDFOR
}


void FFTReorderSimple_10933() {
	FOR(int, i, 0,  < , 32, i = (i + 4)) {
		push_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_join[2], peek_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_split[2], i)) ; 
		push_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_join[2], peek_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_split[2], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 32, i = (i + 4)) {
		push_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_join[2], peek_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_split[2], i)) ; 
		push_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_join[2], peek_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_split[2], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_split[2]) ; 
		pop_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_split[2]) ; 
	}
	ENDFOR
}


void FFTReorderSimple_10934() {
	FOR(int, i, 0,  < , 32, i = (i + 4)) {
		push_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_join[3], peek_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_split[3], i)) ; 
		push_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_join[3], peek_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_split[3], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 32, i = (i + 4)) {
		push_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_join[3], peek_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_split[3], i)) ; 
		push_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_join[3], peek_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_split[3], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_split[3]) ; 
		pop_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_split[3]) ; 
	}
	ENDFOR
}


void WEIGHTED_ROUND_ROBIN_Splitter_10929() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
			push_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10926WEIGHTED_ROUND_ROBIN_Splitter_10929));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10930() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10930WEIGHTED_ROUND_ROBIN_Splitter_10935, pop_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void FFTReorderSimple_10937() {
	FOR(int, i, 0,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[0], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[0], i)) ; 
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[0], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[0], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[0], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[0], i)) ; 
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[0], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[0], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 8, i++) {
		pop_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[0]) ; 
		pop_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[0]) ; 
	}
	ENDFOR
}


void FFTReorderSimple_10938() {
	FOR(int, i, 0,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[1], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[1], i)) ; 
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[1], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[1], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[1], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[1], i)) ; 
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[1], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[1], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 8, i++) {
		pop_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[1]) ; 
		pop_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[1]) ; 
	}
	ENDFOR
}


void FFTReorderSimple_10939() {
	FOR(int, i, 0,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[2], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[2], i)) ; 
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[2], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[2], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[2], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[2], i)) ; 
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[2], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[2], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 8, i++) {
		pop_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[2]) ; 
		pop_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[2]) ; 
	}
	ENDFOR
}


void FFTReorderSimple_10940() {
	FOR(int, i, 0,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[3], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[3], i)) ; 
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[3], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[3], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[3], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[3], i)) ; 
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[3], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[3], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 8, i++) {
		pop_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[3]) ; 
		pop_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[3]) ; 
	}
	ENDFOR
}


void FFTReorderSimple_10941() {
	FOR(int, i, 0,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[4], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[4], i)) ; 
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[4], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[4], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[4], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[4], i)) ; 
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[4], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[4], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 8, i++) {
		pop_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[4]) ; 
		pop_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[4]) ; 
	}
	ENDFOR
}


void FFTReorderSimple_10942() {
	FOR(int, i, 0,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[5], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[5], i)) ; 
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[5], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[5], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[5], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[5], i)) ; 
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[5], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[5], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 8, i++) {
		pop_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[5]) ; 
		pop_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[5]) ; 
	}
	ENDFOR
}


void FFTReorderSimple_10943() {
	FOR(int, i, 0,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[6], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[6], i)) ; 
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[6], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[6], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[6], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[6], i)) ; 
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[6], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[6], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 8, i++) {
		pop_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[6]) ; 
		pop_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[6]) ; 
	}
	ENDFOR
}


void FFTReorderSimple_10944() {
	FOR(int, i, 0,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[7], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[7], i)) ; 
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[7], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[7], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 16, i = (i + 4)) {
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[7], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[7], i)) ; 
		push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[7], peek_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[7], (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 8, i++) {
		pop_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[7]) ; 
		pop_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[7]) ; 
	}
	ENDFOR
}


void WEIGHTED_ROUND_ROBIN_Splitter_10935() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
			push_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10930WEIGHTED_ROUND_ROBIN_Splitter_10935));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10936() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10936WEIGHTED_ROUND_ROBIN_Splitter_10945, pop_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void FFTReorderSimple_10947(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[0], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[0], i)) ; 
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[0], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[0], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[0], i)) ; 
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[0], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[0]) ; 
			pop_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_10948(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[1], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[1], i)) ; 
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[1], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[1], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[1], i)) ; 
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[1], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[1]) ; 
			pop_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_10949(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[2], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[2], i)) ; 
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[2], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[2], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[2], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[2], i)) ; 
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[2], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[2], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[2]) ; 
			pop_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_10950(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[3], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[3], i)) ; 
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[3], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[3], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[3], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[3], i)) ; 
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[3], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[3], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[3]) ; 
			pop_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_10951(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[4], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[4], i)) ; 
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[4], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[4], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[4], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[4], i)) ; 
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[4], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[4], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[4]) ; 
			pop_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_10952(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[5], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[5], i)) ; 
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[5], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[5], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[5], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[5], i)) ; 
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[5], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[5], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[5]) ; 
			pop_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_10953(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[6], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[6], i)) ; 
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[6], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[6], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[6], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[6], i)) ; 
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[6], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[6], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[6]) ; 
			pop_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_10954(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[7], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[7], i)) ; 
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[7], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[7], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[7], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[7], i)) ; 
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[7], peek_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[7], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[7]) ; 
			pop_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10945() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10936WEIGHTED_ROUND_ROBIN_Splitter_10945));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10946() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10946WEIGHTED_ROUND_ROBIN_Splitter_10955, pop_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_10957(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[0], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[0], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[0], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[0], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10957_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10957_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[0]) ; 
			push_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_10958(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[1], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[1], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[1], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[1], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10958_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10958_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[1]) ; 
			push_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_10959(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[2], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[2], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[2], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[2], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10959_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10959_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[2]) ; 
			push_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_10960(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[3], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[3], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[3], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[3], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10960_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10960_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[3]) ; 
			push_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_10961(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[4], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[4], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[4], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[4], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10961_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10961_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[4]) ; 
			push_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_10962(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[5], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[5], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[5], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[5], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10962_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10962_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[5]) ; 
			push_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_10963(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[6], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[6], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[6], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[6], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10963_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10963_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[6]) ; 
			push_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_10964(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[7], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[7], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[7], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[7], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10964_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10964_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[7]) ; 
			push_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10955() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10946WEIGHTED_ROUND_ROBIN_Splitter_10955));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10956() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10956WEIGHTED_ROUND_ROBIN_Splitter_10965, pop_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_10967(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[0], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[0], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[0], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[0], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10967_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10967_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[0]) ; 
			push_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_10968(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[1], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[1], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[1], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[1], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10968_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10968_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[1]) ; 
			push_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_10969(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[2], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[2], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[2], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[2], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10969_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10969_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[2]) ; 
			push_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_10970(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[3], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[3], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[3], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[3], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10970_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10970_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[3]) ; 
			push_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_10971(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[4], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[4], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[4], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[4], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10971_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10971_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[4]) ; 
			push_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_10972(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[5], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[5], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[5], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[5], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10972_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10972_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[5]) ; 
			push_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_10973(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[6], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[6], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[6], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[6], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10973_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10973_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[6]) ; 
			push_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_10974(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[7], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[7], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[7], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[7], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10974_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10974_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[7]) ; 
			push_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10965() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10956WEIGHTED_ROUND_ROBIN_Splitter_10965));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10966() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10966WEIGHTED_ROUND_ROBIN_Splitter_10975, pop_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_10977() {
	float results[16];
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		int i_plus_1 = 0;
		float y0_r = 0.0;
		float y0_i = 0.0;
		float y1_r = 0.0;
		float y1_i = 0.0;
		float weight_real = 0.0;
		float weight_imag = 0.0;
		float y1w_r = 0.0;
		float y1w_i = 0.0;
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		i_plus_1 = (i + 1) ; 
		y0_r = 0.0 ; 
		y0_r = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[0], i) ; 
		y0_i = 0.0 ; 
		y0_i = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[0], i_plus_1) ; 
		y1_r = 0.0 ; 
		y1_r = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[0], (8 + i)) ; 
		y1_i = 0.0 ; 
		y1_i = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[0], (8 + i_plus_1)) ; 
		weight_real = 0.0 ; 
		weight_real = CombineDFT_10977_s.w[i] ; 
		weight_imag = 0.0 ; 
		weight_imag = CombineDFT_10977_s.w[i_plus_1] ; 
		y1w_r = 0.0 ; 
		y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
		y1w_i = 0.0 ; 
		y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
		results[i] = (y0_r + y1w_r) ; 
		results[(i + 1)] = (y0_i + y1w_i) ; 
		results[(8 + i)] = (y0_r - y1w_r) ; 
		results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[0]) ; 
		push_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_join[0], results[i]) ; 
	}
	ENDFOR
}


void CombineDFT_10978() {
	float results[16];
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		int i_plus_1 = 0;
		float y0_r = 0.0;
		float y0_i = 0.0;
		float y1_r = 0.0;
		float y1_i = 0.0;
		float weight_real = 0.0;
		float weight_imag = 0.0;
		float y1w_r = 0.0;
		float y1w_i = 0.0;
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		i_plus_1 = (i + 1) ; 
		y0_r = 0.0 ; 
		y0_r = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[1], i) ; 
		y0_i = 0.0 ; 
		y0_i = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[1], i_plus_1) ; 
		y1_r = 0.0 ; 
		y1_r = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[1], (8 + i)) ; 
		y1_i = 0.0 ; 
		y1_i = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[1], (8 + i_plus_1)) ; 
		weight_real = 0.0 ; 
		weight_real = CombineDFT_10978_s.w[i] ; 
		weight_imag = 0.0 ; 
		weight_imag = CombineDFT_10978_s.w[i_plus_1] ; 
		y1w_r = 0.0 ; 
		y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
		y1w_i = 0.0 ; 
		y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
		results[i] = (y0_r + y1w_r) ; 
		results[(i + 1)] = (y0_i + y1w_i) ; 
		results[(8 + i)] = (y0_r - y1w_r) ; 
		results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[1]) ; 
		push_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_join[1], results[i]) ; 
	}
	ENDFOR
}


void CombineDFT_10979() {
	float results[16];
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		int i_plus_1 = 0;
		float y0_r = 0.0;
		float y0_i = 0.0;
		float y1_r = 0.0;
		float y1_i = 0.0;
		float weight_real = 0.0;
		float weight_imag = 0.0;
		float y1w_r = 0.0;
		float y1w_i = 0.0;
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		i_plus_1 = (i + 1) ; 
		y0_r = 0.0 ; 
		y0_r = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[2], i) ; 
		y0_i = 0.0 ; 
		y0_i = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[2], i_plus_1) ; 
		y1_r = 0.0 ; 
		y1_r = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[2], (8 + i)) ; 
		y1_i = 0.0 ; 
		y1_i = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[2], (8 + i_plus_1)) ; 
		weight_real = 0.0 ; 
		weight_real = CombineDFT_10979_s.w[i] ; 
		weight_imag = 0.0 ; 
		weight_imag = CombineDFT_10979_s.w[i_plus_1] ; 
		y1w_r = 0.0 ; 
		y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
		y1w_i = 0.0 ; 
		y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
		results[i] = (y0_r + y1w_r) ; 
		results[(i + 1)] = (y0_i + y1w_i) ; 
		results[(8 + i)] = (y0_r - y1w_r) ; 
		results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[2]) ; 
		push_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_join[2], results[i]) ; 
	}
	ENDFOR
}


void CombineDFT_10980() {
	float results[16];
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		int i_plus_1 = 0;
		float y0_r = 0.0;
		float y0_i = 0.0;
		float y1_r = 0.0;
		float y1_i = 0.0;
		float weight_real = 0.0;
		float weight_imag = 0.0;
		float y1w_r = 0.0;
		float y1w_i = 0.0;
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		i_plus_1 = (i + 1) ; 
		y0_r = 0.0 ; 
		y0_r = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[3], i) ; 
		y0_i = 0.0 ; 
		y0_i = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[3], i_plus_1) ; 
		y1_r = 0.0 ; 
		y1_r = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[3], (8 + i)) ; 
		y1_i = 0.0 ; 
		y1_i = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[3], (8 + i_plus_1)) ; 
		weight_real = 0.0 ; 
		weight_real = CombineDFT_10980_s.w[i] ; 
		weight_imag = 0.0 ; 
		weight_imag = CombineDFT_10980_s.w[i_plus_1] ; 
		y1w_r = 0.0 ; 
		y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
		y1w_i = 0.0 ; 
		y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
		results[i] = (y0_r + y1w_r) ; 
		results[(i + 1)] = (y0_i + y1w_i) ; 
		results[(8 + i)] = (y0_r - y1w_r) ; 
		results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[3]) ; 
		push_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_join[3], results[i]) ; 
	}
	ENDFOR
}


void CombineDFT_10981() {
	float results[16];
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		int i_plus_1 = 0;
		float y0_r = 0.0;
		float y0_i = 0.0;
		float y1_r = 0.0;
		float y1_i = 0.0;
		float weight_real = 0.0;
		float weight_imag = 0.0;
		float y1w_r = 0.0;
		float y1w_i = 0.0;
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		i_plus_1 = (i + 1) ; 
		y0_r = 0.0 ; 
		y0_r = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[4], i) ; 
		y0_i = 0.0 ; 
		y0_i = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[4], i_plus_1) ; 
		y1_r = 0.0 ; 
		y1_r = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[4], (8 + i)) ; 
		y1_i = 0.0 ; 
		y1_i = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[4], (8 + i_plus_1)) ; 
		weight_real = 0.0 ; 
		weight_real = CombineDFT_10981_s.w[i] ; 
		weight_imag = 0.0 ; 
		weight_imag = CombineDFT_10981_s.w[i_plus_1] ; 
		y1w_r = 0.0 ; 
		y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
		y1w_i = 0.0 ; 
		y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
		results[i] = (y0_r + y1w_r) ; 
		results[(i + 1)] = (y0_i + y1w_i) ; 
		results[(8 + i)] = (y0_r - y1w_r) ; 
		results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[4]) ; 
		push_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_join[4], results[i]) ; 
	}
	ENDFOR
}


void CombineDFT_10982() {
	float results[16];
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		int i_plus_1 = 0;
		float y0_r = 0.0;
		float y0_i = 0.0;
		float y1_r = 0.0;
		float y1_i = 0.0;
		float weight_real = 0.0;
		float weight_imag = 0.0;
		float y1w_r = 0.0;
		float y1w_i = 0.0;
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		i_plus_1 = (i + 1) ; 
		y0_r = 0.0 ; 
		y0_r = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[5], i) ; 
		y0_i = 0.0 ; 
		y0_i = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[5], i_plus_1) ; 
		y1_r = 0.0 ; 
		y1_r = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[5], (8 + i)) ; 
		y1_i = 0.0 ; 
		y1_i = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[5], (8 + i_plus_1)) ; 
		weight_real = 0.0 ; 
		weight_real = CombineDFT_10982_s.w[i] ; 
		weight_imag = 0.0 ; 
		weight_imag = CombineDFT_10982_s.w[i_plus_1] ; 
		y1w_r = 0.0 ; 
		y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
		y1w_i = 0.0 ; 
		y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
		results[i] = (y0_r + y1w_r) ; 
		results[(i + 1)] = (y0_i + y1w_i) ; 
		results[(8 + i)] = (y0_r - y1w_r) ; 
		results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[5]) ; 
		push_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_join[5], results[i]) ; 
	}
	ENDFOR
}


void CombineDFT_10983() {
	float results[16];
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		int i_plus_1 = 0;
		float y0_r = 0.0;
		float y0_i = 0.0;
		float y1_r = 0.0;
		float y1_i = 0.0;
		float weight_real = 0.0;
		float weight_imag = 0.0;
		float y1w_r = 0.0;
		float y1w_i = 0.0;
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		i_plus_1 = (i + 1) ; 
		y0_r = 0.0 ; 
		y0_r = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[6], i) ; 
		y0_i = 0.0 ; 
		y0_i = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[6], i_plus_1) ; 
		y1_r = 0.0 ; 
		y1_r = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[6], (8 + i)) ; 
		y1_i = 0.0 ; 
		y1_i = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[6], (8 + i_plus_1)) ; 
		weight_real = 0.0 ; 
		weight_real = CombineDFT_10983_s.w[i] ; 
		weight_imag = 0.0 ; 
		weight_imag = CombineDFT_10983_s.w[i_plus_1] ; 
		y1w_r = 0.0 ; 
		y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
		y1w_i = 0.0 ; 
		y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
		results[i] = (y0_r + y1w_r) ; 
		results[(i + 1)] = (y0_i + y1w_i) ; 
		results[(8 + i)] = (y0_r - y1w_r) ; 
		results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[6]) ; 
		push_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_join[6], results[i]) ; 
	}
	ENDFOR
}


void CombineDFT_10984() {
	float results[16];
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		int i_plus_1 = 0;
		float y0_r = 0.0;
		float y0_i = 0.0;
		float y1_r = 0.0;
		float y1_i = 0.0;
		float weight_real = 0.0;
		float weight_imag = 0.0;
		float y1w_r = 0.0;
		float y1w_i = 0.0;
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		i_plus_1 = (i + 1) ; 
		y0_r = 0.0 ; 
		y0_r = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[7], i) ; 
		y0_i = 0.0 ; 
		y0_i = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[7], i_plus_1) ; 
		y1_r = 0.0 ; 
		y1_r = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[7], (8 + i)) ; 
		y1_i = 0.0 ; 
		y1_i = peek_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[7], (8 + i_plus_1)) ; 
		weight_real = 0.0 ; 
		weight_real = CombineDFT_10984_s.w[i] ; 
		weight_imag = 0.0 ; 
		weight_imag = CombineDFT_10984_s.w[i_plus_1] ; 
		y1w_r = 0.0 ; 
		y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
		y1w_i = 0.0 ; 
		y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
		results[i] = (y0_r + y1w_r) ; 
		results[(i + 1)] = (y0_i + y1w_i) ; 
		results[(8 + i)] = (y0_r - y1w_r) ; 
		results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[7]) ; 
		push_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_join[7], results[i]) ; 
	}
	ENDFOR
}


void WEIGHTED_ROUND_ROBIN_Splitter_10975() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
			push_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10966WEIGHTED_ROUND_ROBIN_Splitter_10975));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10976() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10976WEIGHTED_ROUND_ROBIN_Splitter_10985, pop_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void CombineDFT_10987() {
	float results[32];
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		int i_plus_1 = 0;
		float y0_r = 0.0;
		float y0_i = 0.0;
		float y1_r = 0.0;
		float y1_i = 0.0;
		float weight_real = 0.0;
		float weight_imag = 0.0;
		float y1w_r = 0.0;
		float y1w_i = 0.0;
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		i_plus_1 = (i + 1) ; 
		y0_r = 0.0 ; 
		y0_r = peek_float(&SplitJoin80_CombineDFT_Fiss_11013_11034_split[0], i) ; 
		y0_i = 0.0 ; 
		y0_i = peek_float(&SplitJoin80_CombineDFT_Fiss_11013_11034_split[0], i_plus_1) ; 
		y1_r = 0.0 ; 
		y1_r = peek_float(&SplitJoin80_CombineDFT_Fiss_11013_11034_split[0], (16 + i)) ; 
		y1_i = 0.0 ; 
		y1_i = peek_float(&SplitJoin80_CombineDFT_Fiss_11013_11034_split[0], (16 + i_plus_1)) ; 
		weight_real = 0.0 ; 
		weight_real = CombineDFT_10987_s.w[i] ; 
		weight_imag = 0.0 ; 
		weight_imag = CombineDFT_10987_s.w[i_plus_1] ; 
		y1w_r = 0.0 ; 
		y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
		y1w_i = 0.0 ; 
		y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
		results[i] = (y0_r + y1w_r) ; 
		results[(i + 1)] = (y0_i + y1w_i) ; 
		results[(16 + i)] = (y0_r - y1w_r) ; 
		results[((16 + i) + 1)] = (y0_i - y1w_i) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 32, i++) {
		pop_float(&SplitJoin80_CombineDFT_Fiss_11013_11034_split[0]) ; 
		push_float(&SplitJoin80_CombineDFT_Fiss_11013_11034_join[0], results[i]) ; 
	}
	ENDFOR
}


void CombineDFT_10988() {
	float results[32];
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		int i_plus_1 = 0;
		float y0_r = 0.0;
		float y0_i = 0.0;
		float y1_r = 0.0;
		float y1_i = 0.0;
		float weight_real = 0.0;
		float weight_imag = 0.0;
		float y1w_r = 0.0;
		float y1w_i = 0.0;
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		i_plus_1 = (i + 1) ; 
		y0_r = 0.0 ; 
		y0_r = peek_float(&SplitJoin80_CombineDFT_Fiss_11013_11034_split[1], i) ; 
		y0_i = 0.0 ; 
		y0_i = peek_float(&SplitJoin80_CombineDFT_Fiss_11013_11034_split[1], i_plus_1) ; 
		y1_r = 0.0 ; 
		y1_r = peek_float(&SplitJoin80_CombineDFT_Fiss_11013_11034_split[1], (16 + i)) ; 
		y1_i = 0.0 ; 
		y1_i = peek_float(&SplitJoin80_CombineDFT_Fiss_11013_11034_split[1], (16 + i_plus_1)) ; 
		weight_real = 0.0 ; 
		weight_real = CombineDFT_10988_s.w[i] ; 
		weight_imag = 0.0 ; 
		weight_imag = CombineDFT_10988_s.w[i_plus_1] ; 
		y1w_r = 0.0 ; 
		y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
		y1w_i = 0.0 ; 
		y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
		results[i] = (y0_r + y1w_r) ; 
		results[(i + 1)] = (y0_i + y1w_i) ; 
		results[(16 + i)] = (y0_r - y1w_r) ; 
		results[((16 + i) + 1)] = (y0_i - y1w_i) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 32, i++) {
		pop_float(&SplitJoin80_CombineDFT_Fiss_11013_11034_split[1]) ; 
		push_float(&SplitJoin80_CombineDFT_Fiss_11013_11034_join[1], results[i]) ; 
	}
	ENDFOR
}


void CombineDFT_10989() {
	float results[32];
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		int i_plus_1 = 0;
		float y0_r = 0.0;
		float y0_i = 0.0;
		float y1_r = 0.0;
		float y1_i = 0.0;
		float weight_real = 0.0;
		float weight_imag = 0.0;
		float y1w_r = 0.0;
		float y1w_i = 0.0;
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		i_plus_1 = (i + 1) ; 
		y0_r = 0.0 ; 
		y0_r = peek_float(&SplitJoin80_CombineDFT_Fiss_11013_11034_split[2], i) ; 
		y0_i = 0.0 ; 
		y0_i = peek_float(&SplitJoin80_CombineDFT_Fiss_11013_11034_split[2], i_plus_1) ; 
		y1_r = 0.0 ; 
		y1_r = peek_float(&SplitJoin80_CombineDFT_Fiss_11013_11034_split[2], (16 + i)) ; 
		y1_i = 0.0 ; 
		y1_i = peek_float(&SplitJoin80_CombineDFT_Fiss_11013_11034_split[2], (16 + i_plus_1)) ; 
		weight_real = 0.0 ; 
		weight_real = CombineDFT_10989_s.w[i] ; 
		weight_imag = 0.0 ; 
		weight_imag = CombineDFT_10989_s.w[i_plus_1] ; 
		y1w_r = 0.0 ; 
		y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
		y1w_i = 0.0 ; 
		y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
		results[i] = (y0_r + y1w_r) ; 
		results[(i + 1)] = (y0_i + y1w_i) ; 
		results[(16 + i)] = (y0_r - y1w_r) ; 
		results[((16 + i) + 1)] = (y0_i - y1w_i) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 32, i++) {
		pop_float(&SplitJoin80_CombineDFT_Fiss_11013_11034_split[2]) ; 
		push_float(&SplitJoin80_CombineDFT_Fiss_11013_11034_join[2], results[i]) ; 
	}
	ENDFOR
}


void CombineDFT_10990() {
	float results[32];
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		int i_plus_1 = 0;
		float y0_r = 0.0;
		float y0_i = 0.0;
		float y1_r = 0.0;
		float y1_i = 0.0;
		float weight_real = 0.0;
		float weight_imag = 0.0;
		float y1w_r = 0.0;
		float y1w_i = 0.0;
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		i_plus_1 = (i + 1) ; 
		y0_r = 0.0 ; 
		y0_r = peek_float(&SplitJoin80_CombineDFT_Fiss_11013_11034_split[3], i) ; 
		y0_i = 0.0 ; 
		y0_i = peek_float(&SplitJoin80_CombineDFT_Fiss_11013_11034_split[3], i_plus_1) ; 
		y1_r = 0.0 ; 
		y1_r = peek_float(&SplitJoin80_CombineDFT_Fiss_11013_11034_split[3], (16 + i)) ; 
		y1_i = 0.0 ; 
		y1_i = peek_float(&SplitJoin80_CombineDFT_Fiss_11013_11034_split[3], (16 + i_plus_1)) ; 
		weight_real = 0.0 ; 
		weight_real = CombineDFT_10990_s.w[i] ; 
		weight_imag = 0.0 ; 
		weight_imag = CombineDFT_10990_s.w[i_plus_1] ; 
		y1w_r = 0.0 ; 
		y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
		y1w_i = 0.0 ; 
		y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
		results[i] = (y0_r + y1w_r) ; 
		results[(i + 1)] = (y0_i + y1w_i) ; 
		results[(16 + i)] = (y0_r - y1w_r) ; 
		results[((16 + i) + 1)] = (y0_i - y1w_i) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 32, i++) {
		pop_float(&SplitJoin80_CombineDFT_Fiss_11013_11034_split[3]) ; 
		push_float(&SplitJoin80_CombineDFT_Fiss_11013_11034_join[3], results[i]) ; 
	}
	ENDFOR
}


void WEIGHTED_ROUND_ROBIN_Splitter_10985() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
			push_float(&SplitJoin80_CombineDFT_Fiss_11013_11034_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10976WEIGHTED_ROUND_ROBIN_Splitter_10985));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10986() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10986WEIGHTED_ROUND_ROBIN_Splitter_10991, pop_float(&SplitJoin80_CombineDFT_Fiss_11013_11034_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void CombineDFT_10993() {
	float results[64];
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		int i_plus_1 = 0;
		float y0_r = 0.0;
		float y0_i = 0.0;
		float y1_r = 0.0;
		float y1_i = 0.0;
		float weight_real = 0.0;
		float weight_imag = 0.0;
		float y1w_r = 0.0;
		float y1w_i = 0.0;
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		i_plus_1 = (i + 1) ; 
		y0_r = 0.0 ; 
		y0_r = peek_float(&SplitJoin82_CombineDFT_Fiss_11014_11035_split[0], i) ; 
		y0_i = 0.0 ; 
		y0_i = peek_float(&SplitJoin82_CombineDFT_Fiss_11014_11035_split[0], i_plus_1) ; 
		y1_r = 0.0 ; 
		y1_r = peek_float(&SplitJoin82_CombineDFT_Fiss_11014_11035_split[0], (32 + i)) ; 
		y1_i = 0.0 ; 
		y1_i = peek_float(&SplitJoin82_CombineDFT_Fiss_11014_11035_split[0], (32 + i_plus_1)) ; 
		weight_real = 0.0 ; 
		weight_real = CombineDFT_10993_s.w[i] ; 
		weight_imag = 0.0 ; 
		weight_imag = CombineDFT_10993_s.w[i_plus_1] ; 
		y1w_r = 0.0 ; 
		y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
		y1w_i = 0.0 ; 
		y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
		results[i] = (y0_r + y1w_r) ; 
		results[(i + 1)] = (y0_i + y1w_i) ; 
		results[(32 + i)] = (y0_r - y1w_r) ; 
		results[((32 + i) + 1)] = (y0_i - y1w_i) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 64, i++) {
		pop_float(&SplitJoin82_CombineDFT_Fiss_11014_11035_split[0]) ; 
		push_float(&SplitJoin82_CombineDFT_Fiss_11014_11035_join[0], results[i]) ; 
	}
	ENDFOR
}


void CombineDFT_10994() {
	float results[64];
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		int i_plus_1 = 0;
		float y0_r = 0.0;
		float y0_i = 0.0;
		float y1_r = 0.0;
		float y1_i = 0.0;
		float weight_real = 0.0;
		float weight_imag = 0.0;
		float y1w_r = 0.0;
		float y1w_i = 0.0;
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		i_plus_1 = (i + 1) ; 
		y0_r = 0.0 ; 
		y0_r = peek_float(&SplitJoin82_CombineDFT_Fiss_11014_11035_split[1], i) ; 
		y0_i = 0.0 ; 
		y0_i = peek_float(&SplitJoin82_CombineDFT_Fiss_11014_11035_split[1], i_plus_1) ; 
		y1_r = 0.0 ; 
		y1_r = peek_float(&SplitJoin82_CombineDFT_Fiss_11014_11035_split[1], (32 + i)) ; 
		y1_i = 0.0 ; 
		y1_i = peek_float(&SplitJoin82_CombineDFT_Fiss_11014_11035_split[1], (32 + i_plus_1)) ; 
		weight_real = 0.0 ; 
		weight_real = CombineDFT_10994_s.w[i] ; 
		weight_imag = 0.0 ; 
		weight_imag = CombineDFT_10994_s.w[i_plus_1] ; 
		y1w_r = 0.0 ; 
		y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
		y1w_i = 0.0 ; 
		y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
		results[i] = (y0_r + y1w_r) ; 
		results[(i + 1)] = (y0_i + y1w_i) ; 
		results[(32 + i)] = (y0_r - y1w_r) ; 
		results[((32 + i) + 1)] = (y0_i - y1w_i) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 64, i++) {
		pop_float(&SplitJoin82_CombineDFT_Fiss_11014_11035_split[1]) ; 
		push_float(&SplitJoin82_CombineDFT_Fiss_11014_11035_join[1], results[i]) ; 
	}
	ENDFOR
}


void WEIGHTED_ROUND_ROBIN_Splitter_10991() {
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&SplitJoin82_CombineDFT_Fiss_11014_11035_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10986WEIGHTED_ROUND_ROBIN_Splitter_10991));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&SplitJoin82_CombineDFT_Fiss_11014_11035_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10986WEIGHTED_ROUND_ROBIN_Splitter_10991));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10992() {
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10992CombineDFT_10841, pop_float(&SplitJoin82_CombineDFT_Fiss_11014_11035_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10992CombineDFT_10841, pop_float(&SplitJoin82_CombineDFT_Fiss_11014_11035_join[1]));
	ENDFOR
}

void CombineDFT_10841() {
	float results[128];
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		int i_plus_1 = 0;
		float y0_r = 0.0;
		float y0_i = 0.0;
		float y1_r = 0.0;
		float y1_i = 0.0;
		float weight_real = 0.0;
		float weight_imag = 0.0;
		float y1w_r = 0.0;
		float y1w_i = 0.0;
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		y0_r = 0.0 ; 
		y0_i = 0.0 ; 
		y1_r = 0.0 ; 
		y1_i = 0.0 ; 
		weight_real = 0.0 ; 
		weight_imag = 0.0 ; 
		y1w_r = 0.0 ; 
		y1w_i = 0.0 ; 
		i_plus_1 = 0 ; 
		i_plus_1 = (i + 1) ; 
		y0_r = 0.0 ; 
		y0_r = peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_10992CombineDFT_10841, i) ; 
		y0_i = 0.0 ; 
		y0_i = peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_10992CombineDFT_10841, i_plus_1) ; 
		y1_r = 0.0 ; 
		y1_r = peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_10992CombineDFT_10841, (64 + i)) ; 
		y1_i = 0.0 ; 
		y1_i = peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_10992CombineDFT_10841, (64 + i_plus_1)) ; 
		weight_real = 0.0 ; 
		weight_real = CombineDFT_10841_s.w[i] ; 
		weight_imag = 0.0 ; 
		weight_imag = CombineDFT_10841_s.w[i_plus_1] ; 
		y1w_r = 0.0 ; 
		y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
		y1w_i = 0.0 ; 
		y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
		results[i] = (y0_r + y1w_r) ; 
		results[(i + 1)] = (y0_i + y1w_i) ; 
		results[(64 + i)] = (y0_r - y1w_r) ; 
		results[((64 + i) + 1)] = (y0_i - y1w_i) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 128, i++) {
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10992CombineDFT_10841) ; 
		push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10809_10845_10996_11017_join[1], results[i]) ; 
	}
	ENDFOR
}


void WEIGHTED_ROUND_ROBIN_Splitter_10843() {
	FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
		push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10809_10845_10996_11017_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10852WEIGHTED_ROUND_ROBIN_Splitter_10843));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
		push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10809_10845_10996_11017_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10852WEIGHTED_ROUND_ROBIN_Splitter_10843));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10844() {
	FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10844FloatPrinter_10842, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10809_10845_10996_11017_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10844FloatPrinter_10842, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10809_10845_10996_11017_join[1]));
	ENDFOR
}

void FloatPrinter_10842(){
	FOR(uint32_t, __iter_steady_, 0, <, 256, __iter_steady_++) {
		printf("%.10f", pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10844FloatPrinter_10842));
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 4, __iter_init_1_++)
		init_buffer_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_split[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10896WEIGHTED_ROUND_ROBIN_Splitter_10905);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10876WEIGHTED_ROUND_ROBIN_Splitter_10885);
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10809_10845_10996_11017_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 2, __iter_init_3_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_11005_11026_join[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10986WEIGHTED_ROUND_ROBIN_Splitter_10991);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10866WEIGHTED_ROUND_ROBIN_Splitter_10875);
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_split[__iter_init_4_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10856WEIGHTED_ROUND_ROBIN_Splitter_10859);
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_11003_11024_join[__iter_init_5_]);
	ENDFOR
	init_buffer_float(&FFTReorderSimple_10831WEIGHTED_ROUND_ROBIN_Splitter_10925);
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_split[__iter_init_6_]);
	ENDFOR
	init_buffer_float(&FFTReorderSimple_10820WEIGHTED_ROUND_ROBIN_Splitter_10855);
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 2, __iter_init_9_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_10995_11016_join[__iter_init_9_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10886WEIGHTED_ROUND_ROBIN_Splitter_10895);
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_join[__iter_init_10_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10956WEIGHTED_ROUND_ROBIN_Splitter_10965);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10926WEIGHTED_ROUND_ROBIN_Splitter_10929);
	FOR(int, __iter_init_11_, 0, <, 4, __iter_init_11_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_split[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 4, __iter_init_12_++)
		init_buffer_float(&SplitJoin80_CombineDFT_Fiss_11013_11034_split[__iter_init_12_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10852WEIGHTED_ROUND_ROBIN_Splitter_10843);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10976WEIGHTED_ROUND_ROBIN_Splitter_10985);
	FOR(int, __iter_init_13_, 0, <, 8, __iter_init_13_++)
		init_buffer_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_join[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10809_10845_10996_11017_split[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 8, __iter_init_15_++)
		init_buffer_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_join[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 8, __iter_init_16_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_join[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 8, __iter_init_17_++)
		init_buffer_float(&SplitJoin72_FFTReorderSimple_Fiss_11009_11030_split[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 2, __iter_init_18_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_10997_11018_split[__iter_init_18_]);
	ENDFOR
	FOR(int, __iter_init_19_, 0, <, 2, __iter_init_19_++)
		init_buffer_float(&SplitJoin82_CombineDFT_Fiss_11014_11035_join[__iter_init_19_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10916WEIGHTED_ROUND_ROBIN_Splitter_10921);
	FOR(int, __iter_init_20_, 0, <, 8, __iter_init_20_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_10999_11020_join[__iter_init_20_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10906WEIGHTED_ROUND_ROBIN_Splitter_10915);
	FOR(int, __iter_init_21_, 0, <, 8, __iter_init_21_++)
		init_buffer_float(&SplitJoin76_CombineDFT_Fiss_11011_11032_split[__iter_init_21_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10992CombineDFT_10841);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10936WEIGHTED_ROUND_ROBIN_Splitter_10945);
	FOR(int, __iter_init_22_, 0, <, 2, __iter_init_22_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_10997_11018_join[__iter_init_22_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10930WEIGHTED_ROUND_ROBIN_Splitter_10935);
	FOR(int, __iter_init_23_, 0, <, 8, __iter_init_23_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_11002_11023_split[__iter_init_23_]);
	ENDFOR
	FOR(int, __iter_init_24_, 0, <, 8, __iter_init_24_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_join[__iter_init_24_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10922CombineDFT_10830);
	FOR(int, __iter_init_25_, 0, <, 4, __iter_init_25_++)
		init_buffer_float(&SplitJoin68_FFTReorderSimple_Fiss_11007_11028_join[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 2, __iter_init_26_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_10995_11016_split[__iter_init_26_]);
	ENDFOR
	FOR(int, __iter_init_27_, 0, <, 2, __iter_init_27_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_11005_11026_split[__iter_init_27_]);
	ENDFOR
	FOR(int, __iter_init_28_, 0, <, 8, __iter_init_28_++)
		init_buffer_float(&SplitJoin78_CombineDFT_Fiss_11012_11033_join[__iter_init_28_]);
	ENDFOR
	FOR(int, __iter_init_29_, 0, <, 8, __iter_init_29_++)
		init_buffer_float(&SplitJoin74_CombineDFT_Fiss_11010_11031_split[__iter_init_29_]);
	ENDFOR
	FOR(int, __iter_init_30_, 0, <, 2, __iter_init_30_++)
		init_buffer_float(&SplitJoin66_FFTReorderSimple_Fiss_11006_11027_split[__iter_init_30_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10844FloatPrinter_10842);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10946WEIGHTED_ROUND_ROBIN_Splitter_10955);
	FOR(int, __iter_init_31_, 0, <, 4, __iter_init_31_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_11004_11025_split[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 4, __iter_init_32_++)
		init_buffer_float(&SplitJoin80_CombineDFT_Fiss_11013_11034_join[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 2, __iter_init_33_++)
		init_buffer_float(&SplitJoin82_CombineDFT_Fiss_11014_11035_split[__iter_init_33_]);
	ENDFOR
	FOR(int, __iter_init_34_, 0, <, 8, __iter_init_34_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_11000_11021_join[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 4, __iter_init_35_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_10998_11019_join[__iter_init_35_]);
	ENDFOR
	FOR(int, __iter_init_36_, 0, <, 8, __iter_init_36_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_11001_11022_split[__iter_init_36_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10966WEIGHTED_ROUND_ROBIN_Splitter_10975);
	FOR(int, __iter_init_37_, 0, <, 4, __iter_init_37_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_11004_11025_join[__iter_init_37_]);
	ENDFOR
	FOR(int, __iter_init_38_, 0, <, 8, __iter_init_38_++)
		init_buffer_float(&SplitJoin70_FFTReorderSimple_Fiss_11008_11029_join[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 2, __iter_init_39_++)
		init_buffer_float(&SplitJoin66_FFTReorderSimple_Fiss_11006_11027_join[__iter_init_39_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10860WEIGHTED_ROUND_ROBIN_Splitter_10865);
// --- init: CombineDFT_10887
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10887_s.w[i] = real ; 
		CombineDFT_10887_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10888
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10888_s.w[i] = real ; 
		CombineDFT_10888_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10889
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10889_s.w[i] = real ; 
		CombineDFT_10889_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10890
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10890_s.w[i] = real ; 
		CombineDFT_10890_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10891
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10891_s.w[i] = real ; 
		CombineDFT_10891_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10892
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10892_s.w[i] = real ; 
		CombineDFT_10892_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10893
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10893_s.w[i] = real ; 
		CombineDFT_10893_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10894
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10894_s.w[i] = real ; 
		CombineDFT_10894_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10897
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10897_s.w[i] = real ; 
		CombineDFT_10897_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10898
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10898_s.w[i] = real ; 
		CombineDFT_10898_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10899
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10899_s.w[i] = real ; 
		CombineDFT_10899_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10900
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10900_s.w[i] = real ; 
		CombineDFT_10900_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10901
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10901_s.w[i] = real ; 
		CombineDFT_10901_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10902
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10902_s.w[i] = real ; 
		CombineDFT_10902_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10903
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10903_s.w[i] = real ; 
		CombineDFT_10903_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10904
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10904_s.w[i] = real ; 
		CombineDFT_10904_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10907
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10907_s.w[i] = real ; 
		CombineDFT_10907_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10908
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10908_s.w[i] = real ; 
		CombineDFT_10908_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10909
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10909_s.w[i] = real ; 
		CombineDFT_10909_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10910
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10910_s.w[i] = real ; 
		CombineDFT_10910_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10911
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10911_s.w[i] = real ; 
		CombineDFT_10911_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10912
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10912_s.w[i] = real ; 
		CombineDFT_10912_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10913
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10913_s.w[i] = real ; 
		CombineDFT_10913_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10914
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10914_s.w[i] = real ; 
		CombineDFT_10914_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10917
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_10917_s.w[i] = real ; 
		CombineDFT_10917_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10918
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_10918_s.w[i] = real ; 
		CombineDFT_10918_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10919
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_10919_s.w[i] = real ; 
		CombineDFT_10919_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10920
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_10920_s.w[i] = real ; 
		CombineDFT_10920_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10923
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_10923_s.w[i] = real ; 
		CombineDFT_10923_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10924
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_10924_s.w[i] = real ; 
		CombineDFT_10924_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10830
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_10830_s.w[i] = real ; 
		CombineDFT_10830_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10957
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10957_s.w[i] = real ; 
		CombineDFT_10957_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10958
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10958_s.w[i] = real ; 
		CombineDFT_10958_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10959
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10959_s.w[i] = real ; 
		CombineDFT_10959_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10960
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10960_s.w[i] = real ; 
		CombineDFT_10960_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10961
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10961_s.w[i] = real ; 
		CombineDFT_10961_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10962
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10962_s.w[i] = real ; 
		CombineDFT_10962_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10963
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10963_s.w[i] = real ; 
		CombineDFT_10963_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10964
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10964_s.w[i] = real ; 
		CombineDFT_10964_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10967
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10967_s.w[i] = real ; 
		CombineDFT_10967_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10968
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10968_s.w[i] = real ; 
		CombineDFT_10968_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10969
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10969_s.w[i] = real ; 
		CombineDFT_10969_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10970
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10970_s.w[i] = real ; 
		CombineDFT_10970_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10971
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10971_s.w[i] = real ; 
		CombineDFT_10971_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10972
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10972_s.w[i] = real ; 
		CombineDFT_10972_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10973
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10973_s.w[i] = real ; 
		CombineDFT_10973_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10974
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10974_s.w[i] = real ; 
		CombineDFT_10974_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10977
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10977_s.w[i] = real ; 
		CombineDFT_10977_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10978
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10978_s.w[i] = real ; 
		CombineDFT_10978_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10979
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10979_s.w[i] = real ; 
		CombineDFT_10979_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10980
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10980_s.w[i] = real ; 
		CombineDFT_10980_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10981
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10981_s.w[i] = real ; 
		CombineDFT_10981_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10982
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10982_s.w[i] = real ; 
		CombineDFT_10982_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10983
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10983_s.w[i] = real ; 
		CombineDFT_10983_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10984
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10984_s.w[i] = real ; 
		CombineDFT_10984_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10987
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_10987_s.w[i] = real ; 
		CombineDFT_10987_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10988
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_10988_s.w[i] = real ; 
		CombineDFT_10988_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10989
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_10989_s.w[i] = real ; 
		CombineDFT_10989_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10990
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_10990_s.w[i] = real ; 
		CombineDFT_10990_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10993
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_10993_s.w[i] = real ; 
		CombineDFT_10993_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10994
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_10994_s.w[i] = real ; 
		CombineDFT_10994_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10841
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_10841_s.w[i] = real ; 
		CombineDFT_10841_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_10851();
			FFTTestSource_10853();
			FFTTestSource_10854();
		WEIGHTED_ROUND_ROBIN_Joiner_10852();
		WEIGHTED_ROUND_ROBIN_Splitter_10843();
			FFTReorderSimple_10820();
			WEIGHTED_ROUND_ROBIN_Splitter_10855();
				FFTReorderSimple_10857();
				FFTReorderSimple_10858();
			WEIGHTED_ROUND_ROBIN_Joiner_10856();
			WEIGHTED_ROUND_ROBIN_Splitter_10859();
				FFTReorderSimple_10861();
				FFTReorderSimple_10862();
				FFTReorderSimple_10863();
				FFTReorderSimple_10864();
			WEIGHTED_ROUND_ROBIN_Joiner_10860();
			WEIGHTED_ROUND_ROBIN_Splitter_10865();
				FFTReorderSimple_10867();
				FFTReorderSimple_10868();
				FFTReorderSimple_10869();
				FFTReorderSimple_10870();
				FFTReorderSimple_10871();
				FFTReorderSimple_10872();
				FFTReorderSimple_10873();
				FFTReorderSimple_10874();
			WEIGHTED_ROUND_ROBIN_Joiner_10866();
			WEIGHTED_ROUND_ROBIN_Splitter_10875();
				FFTReorderSimple_10877();
				FFTReorderSimple_10878();
				FFTReorderSimple_10879();
				FFTReorderSimple_10880();
				FFTReorderSimple_10881();
				FFTReorderSimple_10882();
				FFTReorderSimple_10883();
				FFTReorderSimple_10884();
			WEIGHTED_ROUND_ROBIN_Joiner_10876();
			WEIGHTED_ROUND_ROBIN_Splitter_10885();
				CombineDFT_10887();
				CombineDFT_10888();
				CombineDFT_10889();
				CombineDFT_10890();
				CombineDFT_10891();
				CombineDFT_10892();
				CombineDFT_10893();
				CombineDFT_10894();
			WEIGHTED_ROUND_ROBIN_Joiner_10886();
			WEIGHTED_ROUND_ROBIN_Splitter_10895();
				CombineDFT_10897();
				CombineDFT_10898();
				CombineDFT_10899();
				CombineDFT_10900();
				CombineDFT_10901();
				CombineDFT_10902();
				CombineDFT_10903();
				CombineDFT_10904();
			WEIGHTED_ROUND_ROBIN_Joiner_10896();
			WEIGHTED_ROUND_ROBIN_Splitter_10905();
				CombineDFT_10907();
				CombineDFT_10908();
				CombineDFT_10909();
				CombineDFT_10910();
				CombineDFT_10911();
				CombineDFT_10912();
				CombineDFT_10913();
				CombineDFT_10914();
			WEIGHTED_ROUND_ROBIN_Joiner_10906();
			WEIGHTED_ROUND_ROBIN_Splitter_10915();
				CombineDFT_10917();
				CombineDFT_10918();
				CombineDFT_10919();
				CombineDFT_10920();
			WEIGHTED_ROUND_ROBIN_Joiner_10916();
			WEIGHTED_ROUND_ROBIN_Splitter_10921();
				CombineDFT_10923();
				CombineDFT_10924();
			WEIGHTED_ROUND_ROBIN_Joiner_10922();
			CombineDFT_10830();
			FFTReorderSimple_10831();
			WEIGHTED_ROUND_ROBIN_Splitter_10925();
				FFTReorderSimple_10927();
				FFTReorderSimple_10928();
			WEIGHTED_ROUND_ROBIN_Joiner_10926();
			WEIGHTED_ROUND_ROBIN_Splitter_10929();
				FFTReorderSimple_10931();
				FFTReorderSimple_10932();
				FFTReorderSimple_10933();
				FFTReorderSimple_10934();
			WEIGHTED_ROUND_ROBIN_Joiner_10930();
			WEIGHTED_ROUND_ROBIN_Splitter_10935();
				FFTReorderSimple_10937();
				FFTReorderSimple_10938();
				FFTReorderSimple_10939();
				FFTReorderSimple_10940();
				FFTReorderSimple_10941();
				FFTReorderSimple_10942();
				FFTReorderSimple_10943();
				FFTReorderSimple_10944();
			WEIGHTED_ROUND_ROBIN_Joiner_10936();
			WEIGHTED_ROUND_ROBIN_Splitter_10945();
				FFTReorderSimple_10947();
				FFTReorderSimple_10948();
				FFTReorderSimple_10949();
				FFTReorderSimple_10950();
				FFTReorderSimple_10951();
				FFTReorderSimple_10952();
				FFTReorderSimple_10953();
				FFTReorderSimple_10954();
			WEIGHTED_ROUND_ROBIN_Joiner_10946();
			WEIGHTED_ROUND_ROBIN_Splitter_10955();
				CombineDFT_10957();
				CombineDFT_10958();
				CombineDFT_10959();
				CombineDFT_10960();
				CombineDFT_10961();
				CombineDFT_10962();
				CombineDFT_10963();
				CombineDFT_10964();
			WEIGHTED_ROUND_ROBIN_Joiner_10956();
			WEIGHTED_ROUND_ROBIN_Splitter_10965();
				CombineDFT_10967();
				CombineDFT_10968();
				CombineDFT_10969();
				CombineDFT_10970();
				CombineDFT_10971();
				CombineDFT_10972();
				CombineDFT_10973();
				CombineDFT_10974();
			WEIGHTED_ROUND_ROBIN_Joiner_10966();
			WEIGHTED_ROUND_ROBIN_Splitter_10975();
				CombineDFT_10977();
				CombineDFT_10978();
				CombineDFT_10979();
				CombineDFT_10980();
				CombineDFT_10981();
				CombineDFT_10982();
				CombineDFT_10983();
				CombineDFT_10984();
			WEIGHTED_ROUND_ROBIN_Joiner_10976();
			WEIGHTED_ROUND_ROBIN_Splitter_10985();
				CombineDFT_10987();
				CombineDFT_10988();
				CombineDFT_10989();
				CombineDFT_10990();
			WEIGHTED_ROUND_ROBIN_Joiner_10986();
			WEIGHTED_ROUND_ROBIN_Splitter_10991();
				CombineDFT_10993();
				CombineDFT_10994();
			WEIGHTED_ROUND_ROBIN_Joiner_10992();
			CombineDFT_10841();
		WEIGHTED_ROUND_ROBIN_Joiner_10844();
		FloatPrinter_10842();
	ENDFOR
	return EXIT_SUCCESS;
}
