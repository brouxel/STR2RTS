#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=2560 on the compile command line
#else
#if BUF_SIZEMAX < 2560
#error BUF_SIZEMAX too small, it must be at least 2560
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float w[2];
} CombineDFT_10175_t;

typedef struct {
	float w[4];
} CombineDFT_10187_t;

typedef struct {
	float w[8];
} CombineDFT_10199_t;

typedef struct {
	float w[16];
} CombineDFT_10209_t;

typedef struct {
	float w[32];
} CombineDFT_10215_t;

typedef struct {
	float w[64];
} CombineDFT_10116_t;
void WEIGHTED_ROUND_ROBIN_Splitter_10137();
void FFTTestSource_10139();
void FFTTestSource_10140();
void WEIGHTED_ROUND_ROBIN_Joiner_10138();
void WEIGHTED_ROUND_ROBIN_Splitter_10129();
void FFTReorderSimple_10106();
void WEIGHTED_ROUND_ROBIN_Splitter_10141();
void FFTReorderSimple_10143();
void FFTReorderSimple_10144();
void WEIGHTED_ROUND_ROBIN_Joiner_10142();
void WEIGHTED_ROUND_ROBIN_Splitter_10145();
void FFTReorderSimple_10147();
void FFTReorderSimple_10148();
void FFTReorderSimple_10149();
void FFTReorderSimple_10150();
void WEIGHTED_ROUND_ROBIN_Joiner_10146();
void WEIGHTED_ROUND_ROBIN_Splitter_10151();
void FFTReorderSimple_10153();
void FFTReorderSimple_10154();
void FFTReorderSimple_10155();
void FFTReorderSimple_10156();
void FFTReorderSimple_10157();
void FFTReorderSimple_10158();
void FFTReorderSimple_10159();
void FFTReorderSimple_10160();
void WEIGHTED_ROUND_ROBIN_Joiner_10152();
void WEIGHTED_ROUND_ROBIN_Splitter_10161();
void FFTReorderSimple_10163();
void FFTReorderSimple_10164();
void FFTReorderSimple_10165();
void FFTReorderSimple_10166();
void FFTReorderSimple_10167();
void FFTReorderSimple_10168();
void FFTReorderSimple_10169();
void FFTReorderSimple_10170();
void FFTReorderSimple_10171();
void FFTReorderSimple_10172();
void WEIGHTED_ROUND_ROBIN_Joiner_10162();
void WEIGHTED_ROUND_ROBIN_Splitter_10173();
void CombineDFT_10175();
void CombineDFT_10176();
void CombineDFT_10177();
void CombineDFT_10178();
void CombineDFT_10179();
void CombineDFT_10180();
void CombineDFT_10181();
void CombineDFT_10182();
void CombineDFT_10183();
void CombineDFT_10184();
void WEIGHTED_ROUND_ROBIN_Joiner_10174();
void WEIGHTED_ROUND_ROBIN_Splitter_10185();
void CombineDFT_10187();
void CombineDFT_10188();
void CombineDFT_10189();
void CombineDFT_10190();
void CombineDFT_10191();
void CombineDFT_10192();
void CombineDFT_10193();
void CombineDFT_10194();
void CombineDFT_10195();
void CombineDFT_10196();
void WEIGHTED_ROUND_ROBIN_Joiner_10186();
void WEIGHTED_ROUND_ROBIN_Splitter_10197();
void CombineDFT_10199();
void CombineDFT_10200();
void CombineDFT_10201();
void CombineDFT_10202();
void CombineDFT_10203();
void CombineDFT_10204();
void CombineDFT_10205();
void CombineDFT_10206();
void WEIGHTED_ROUND_ROBIN_Joiner_10198();
void WEIGHTED_ROUND_ROBIN_Splitter_10207();
void CombineDFT_10209();
void CombineDFT_10210();
void CombineDFT_10211();
void CombineDFT_10212();
void WEIGHTED_ROUND_ROBIN_Joiner_10208();
void WEIGHTED_ROUND_ROBIN_Splitter_10213();
void CombineDFT_10215();
void CombineDFT_10216();
void WEIGHTED_ROUND_ROBIN_Joiner_10214();
void CombineDFT_10116();
void FFTReorderSimple_10117();
void WEIGHTED_ROUND_ROBIN_Splitter_10217();
void FFTReorderSimple_10219();
void FFTReorderSimple_10220();
void WEIGHTED_ROUND_ROBIN_Joiner_10218();
void WEIGHTED_ROUND_ROBIN_Splitter_10221();
void FFTReorderSimple_10223();
void FFTReorderSimple_10224();
void FFTReorderSimple_10225();
void FFTReorderSimple_10226();
void WEIGHTED_ROUND_ROBIN_Joiner_10222();
void WEIGHTED_ROUND_ROBIN_Splitter_10227();
void FFTReorderSimple_10229();
void FFTReorderSimple_10230();
void FFTReorderSimple_10231();
void FFTReorderSimple_10232();
void FFTReorderSimple_10233();
void FFTReorderSimple_10234();
void FFTReorderSimple_10235();
void FFTReorderSimple_10236();
void WEIGHTED_ROUND_ROBIN_Joiner_10228();
void WEIGHTED_ROUND_ROBIN_Splitter_10237();
void FFTReorderSimple_10239();
void FFTReorderSimple_10240();
void FFTReorderSimple_10241();
void FFTReorderSimple_10242();
void FFTReorderSimple_10243();
void FFTReorderSimple_10244();
void FFTReorderSimple_10245();
void FFTReorderSimple_10246();
void FFTReorderSimple_10247();
void FFTReorderSimple_10248();
void WEIGHTED_ROUND_ROBIN_Joiner_10238();
void WEIGHTED_ROUND_ROBIN_Splitter_10249();
void CombineDFT_10251();
void CombineDFT_10252();
void CombineDFT_10253();
void CombineDFT_10254();
void CombineDFT_10255();
void CombineDFT_10256();
void CombineDFT_10257();
void CombineDFT_10258();
void CombineDFT_10259();
void CombineDFT_10260();
void WEIGHTED_ROUND_ROBIN_Joiner_10250();
void WEIGHTED_ROUND_ROBIN_Splitter_10261();
void CombineDFT_10263();
void CombineDFT_10264();
void CombineDFT_10265();
void CombineDFT_10266();
void CombineDFT_10267();
void CombineDFT_10268();
void CombineDFT_10269();
void CombineDFT_10270();
void CombineDFT_10271();
void CombineDFT_10272();
void WEIGHTED_ROUND_ROBIN_Joiner_10262();
void WEIGHTED_ROUND_ROBIN_Splitter_10273();
void CombineDFT_10275();
void CombineDFT_10276();
void CombineDFT_10277();
void CombineDFT_10278();
void CombineDFT_10279();
void CombineDFT_10280();
void CombineDFT_10281();
void CombineDFT_10282();
void WEIGHTED_ROUND_ROBIN_Joiner_10274();
void WEIGHTED_ROUND_ROBIN_Splitter_10283();
void CombineDFT_10285();
void CombineDFT_10286();
void CombineDFT_10287();
void CombineDFT_10288();
void WEIGHTED_ROUND_ROBIN_Joiner_10284();
void WEIGHTED_ROUND_ROBIN_Splitter_10289();
void CombineDFT_10291();
void CombineDFT_10292();
void WEIGHTED_ROUND_ROBIN_Joiner_10290();
void CombineDFT_10127();
void WEIGHTED_ROUND_ROBIN_Joiner_10130();
void FloatPrinter_10128();

#ifdef __cplusplus
}
#endif
#endif
