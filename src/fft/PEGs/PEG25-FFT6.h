#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=3200 on the compile command line
#else
#if BUF_SIZEMAX < 3200
#error BUF_SIZEMAX too small, it must be at least 3200
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	complex_t wn;
} CombineDFT_1769_t;
void FFTTestSource(buffer_complex_t *chanout);
void FFTTestSource_1715();
void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout);
void FFTReorderSimple_1716();
void WEIGHTED_ROUND_ROBIN_Splitter_1729();
void FFTReorderSimple_1731();
void FFTReorderSimple_1732();
void WEIGHTED_ROUND_ROBIN_Joiner_1730();
void WEIGHTED_ROUND_ROBIN_Splitter_1733();
void FFTReorderSimple_1735();
void FFTReorderSimple_1736();
void FFTReorderSimple_1737();
void FFTReorderSimple_1738();
void WEIGHTED_ROUND_ROBIN_Joiner_1734();
void WEIGHTED_ROUND_ROBIN_Splitter_1739();
void FFTReorderSimple_1741();
void FFTReorderSimple_1742();
void FFTReorderSimple_1743();
void FFTReorderSimple_1744();
void FFTReorderSimple_1745();
void FFTReorderSimple_1746();
void FFTReorderSimple_1747();
void FFTReorderSimple_1748();
void WEIGHTED_ROUND_ROBIN_Joiner_1740();
void WEIGHTED_ROUND_ROBIN_Splitter_1749();
void FFTReorderSimple_1751();
void FFTReorderSimple_1752();
void FFTReorderSimple_1753();
void FFTReorderSimple_1754();
void FFTReorderSimple_1755();
void FFTReorderSimple_1756();
void FFTReorderSimple_1757();
void FFTReorderSimple_1758();
void FFTReorderSimple_1759();
void FFTReorderSimple_1760();
void FFTReorderSimple_1761();
void FFTReorderSimple_1762();
void FFTReorderSimple_1763();
void FFTReorderSimple_1764();
void FFTReorderSimple_1765();
void FFTReorderSimple_1766();
void WEIGHTED_ROUND_ROBIN_Joiner_1750();
void WEIGHTED_ROUND_ROBIN_Splitter_1767();
void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout);
void CombineDFT_1769();
void CombineDFT_1770();
void CombineDFT_1771();
void CombineDFT_1772();
void CombineDFT_1773();
void CombineDFT_1774();
void CombineDFT_1775();
void CombineDFT_1776();
void CombineDFT_1777();
void CombineDFT_1778();
void CombineDFT_1779();
void CombineDFT_1780();
void CombineDFT_1781();
void CombineDFT_1782();
void CombineDFT_1783();
void CombineDFT_1784();
void CombineDFT_1785();
void CombineDFT_1786();
void CombineDFT_1787();
void CombineDFT_1788();
void CombineDFT_1789();
void CombineDFT_1790();
void CombineDFT_1791();
void CombineDFT_1792();
void CombineDFT_1793();
void WEIGHTED_ROUND_ROBIN_Joiner_1768();
void WEIGHTED_ROUND_ROBIN_Splitter_1794();
void CombineDFT_1796();
void CombineDFT_1797();
void CombineDFT_1798();
void CombineDFT_1799();
void CombineDFT_1800();
void CombineDFT_1801();
void CombineDFT_1802();
void CombineDFT_1803();
void CombineDFT_1804();
void CombineDFT_1805();
void CombineDFT_1806();
void CombineDFT_1807();
void CombineDFT_1808();
void CombineDFT_1809();
void CombineDFT_1810();
void CombineDFT_1811();
void WEIGHTED_ROUND_ROBIN_Joiner_1795();
void WEIGHTED_ROUND_ROBIN_Splitter_1812();
void CombineDFT_1814();
void CombineDFT_1815();
void CombineDFT_1816();
void CombineDFT_1817();
void CombineDFT_1818();
void CombineDFT_1819();
void CombineDFT_1820();
void CombineDFT_1821();
void WEIGHTED_ROUND_ROBIN_Joiner_1813();
void WEIGHTED_ROUND_ROBIN_Splitter_1822();
void CombineDFT_1824();
void CombineDFT_1825();
void CombineDFT_1826();
void CombineDFT_1827();
void WEIGHTED_ROUND_ROBIN_Joiner_1823();
void WEIGHTED_ROUND_ROBIN_Splitter_1828();
void CombineDFT_1830();
void CombineDFT_1831();
void WEIGHTED_ROUND_ROBIN_Joiner_1829();
void CombineDFT_1726();
void CPrinter(buffer_complex_t *chanin);
void CPrinter_1727();

#ifdef __cplusplus
}
#endif
#endif
