#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=512 on the compile command line
#else
#if BUF_SIZEMAX < 512
#error BUF_SIZEMAX too small, it must be at least 512
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float w[2];
} CombineDFT_10887_t;

typedef struct {
	float w[4];
} CombineDFT_10897_t;

typedef struct {
	float w[8];
} CombineDFT_10907_t;

typedef struct {
	float w[16];
} CombineDFT_10917_t;

typedef struct {
	float w[32];
} CombineDFT_10923_t;

typedef struct {
	float w[64];
} CombineDFT_10830_t;
void WEIGHTED_ROUND_ROBIN_Splitter_10851();
void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout);
void FFTTestSource_10853();
void FFTTestSource_10854();
void WEIGHTED_ROUND_ROBIN_Joiner_10852();
void WEIGHTED_ROUND_ROBIN_Splitter_10843();
void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout);
void FFTReorderSimple_10820();
void WEIGHTED_ROUND_ROBIN_Splitter_10855();
void FFTReorderSimple_10857();
void FFTReorderSimple_10858();
void WEIGHTED_ROUND_ROBIN_Joiner_10856();
void WEIGHTED_ROUND_ROBIN_Splitter_10859();
void FFTReorderSimple_10861();
void FFTReorderSimple_10862();
void FFTReorderSimple_10863();
void FFTReorderSimple_10864();
void WEIGHTED_ROUND_ROBIN_Joiner_10860();
void WEIGHTED_ROUND_ROBIN_Splitter_10865();
void FFTReorderSimple_10867();
void FFTReorderSimple_10868();
void FFTReorderSimple_10869();
void FFTReorderSimple_10870();
void FFTReorderSimple_10871();
void FFTReorderSimple_10872();
void FFTReorderSimple_10873();
void FFTReorderSimple_10874();
void WEIGHTED_ROUND_ROBIN_Joiner_10866();
void WEIGHTED_ROUND_ROBIN_Splitter_10875();
void FFTReorderSimple_10877();
void FFTReorderSimple_10878();
void FFTReorderSimple_10879();
void FFTReorderSimple_10880();
void FFTReorderSimple_10881();
void FFTReorderSimple_10882();
void FFTReorderSimple_10883();
void FFTReorderSimple_10884();
void WEIGHTED_ROUND_ROBIN_Joiner_10876();
void WEIGHTED_ROUND_ROBIN_Splitter_10885();
void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout);
void CombineDFT_10887();
void CombineDFT_10888();
void CombineDFT_10889();
void CombineDFT_10890();
void CombineDFT_10891();
void CombineDFT_10892();
void CombineDFT_10893();
void CombineDFT_10894();
void WEIGHTED_ROUND_ROBIN_Joiner_10886();
void WEIGHTED_ROUND_ROBIN_Splitter_10895();
void CombineDFT_10897();
void CombineDFT_10898();
void CombineDFT_10899();
void CombineDFT_10900();
void CombineDFT_10901();
void CombineDFT_10902();
void CombineDFT_10903();
void CombineDFT_10904();
void WEIGHTED_ROUND_ROBIN_Joiner_10896();
void WEIGHTED_ROUND_ROBIN_Splitter_10905();
void CombineDFT_10907();
void CombineDFT_10908();
void CombineDFT_10909();
void CombineDFT_10910();
void CombineDFT_10911();
void CombineDFT_10912();
void CombineDFT_10913();
void CombineDFT_10914();
void WEIGHTED_ROUND_ROBIN_Joiner_10906();
void WEIGHTED_ROUND_ROBIN_Splitter_10915();
void CombineDFT_10917();
void CombineDFT_10918();
void CombineDFT_10919();
void CombineDFT_10920();
void WEIGHTED_ROUND_ROBIN_Joiner_10916();
void WEIGHTED_ROUND_ROBIN_Splitter_10921();
void CombineDFT_10923();
void CombineDFT_10924();
void WEIGHTED_ROUND_ROBIN_Joiner_10922();
void CombineDFT_10830();
void FFTReorderSimple_10831();
void WEIGHTED_ROUND_ROBIN_Splitter_10925();
void FFTReorderSimple_10927();
void FFTReorderSimple_10928();
void WEIGHTED_ROUND_ROBIN_Joiner_10926();
void WEIGHTED_ROUND_ROBIN_Splitter_10929();
void FFTReorderSimple_10931();
void FFTReorderSimple_10932();
void FFTReorderSimple_10933();
void FFTReorderSimple_10934();
void WEIGHTED_ROUND_ROBIN_Joiner_10930();
void WEIGHTED_ROUND_ROBIN_Splitter_10935();
void FFTReorderSimple_10937();
void FFTReorderSimple_10938();
void FFTReorderSimple_10939();
void FFTReorderSimple_10940();
void FFTReorderSimple_10941();
void FFTReorderSimple_10942();
void FFTReorderSimple_10943();
void FFTReorderSimple_10944();
void WEIGHTED_ROUND_ROBIN_Joiner_10936();
void WEIGHTED_ROUND_ROBIN_Splitter_10945();
void FFTReorderSimple_10947();
void FFTReorderSimple_10948();
void FFTReorderSimple_10949();
void FFTReorderSimple_10950();
void FFTReorderSimple_10951();
void FFTReorderSimple_10952();
void FFTReorderSimple_10953();
void FFTReorderSimple_10954();
void WEIGHTED_ROUND_ROBIN_Joiner_10946();
void WEIGHTED_ROUND_ROBIN_Splitter_10955();
void CombineDFT_10957();
void CombineDFT_10958();
void CombineDFT_10959();
void CombineDFT_10960();
void CombineDFT_10961();
void CombineDFT_10962();
void CombineDFT_10963();
void CombineDFT_10964();
void WEIGHTED_ROUND_ROBIN_Joiner_10956();
void WEIGHTED_ROUND_ROBIN_Splitter_10965();
void CombineDFT_10967();
void CombineDFT_10968();
void CombineDFT_10969();
void CombineDFT_10970();
void CombineDFT_10971();
void CombineDFT_10972();
void CombineDFT_10973();
void CombineDFT_10974();
void WEIGHTED_ROUND_ROBIN_Joiner_10966();
void WEIGHTED_ROUND_ROBIN_Splitter_10975();
void CombineDFT_10977();
void CombineDFT_10978();
void CombineDFT_10979();
void CombineDFT_10980();
void CombineDFT_10981();
void CombineDFT_10982();
void CombineDFT_10983();
void CombineDFT_10984();
void WEIGHTED_ROUND_ROBIN_Joiner_10976();
void WEIGHTED_ROUND_ROBIN_Splitter_10985();
void CombineDFT_10987();
void CombineDFT_10988();
void CombineDFT_10989();
void CombineDFT_10990();
void WEIGHTED_ROUND_ROBIN_Joiner_10986();
void WEIGHTED_ROUND_ROBIN_Splitter_10991();
void CombineDFT_10993();
void CombineDFT_10994();
void WEIGHTED_ROUND_ROBIN_Joiner_10992();
void CombineDFT_10841();
void WEIGHTED_ROUND_ROBIN_Joiner_10844();
void FloatPrinter(buffer_float_t *chanin);
void FloatPrinter_10842();

#ifdef __cplusplus
}
#endif
#endif
