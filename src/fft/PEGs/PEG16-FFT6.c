#include "PEG16-FFT6.h"

buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3712WEIGHTED_ROUND_ROBIN_Splitter_3729;
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_3788_3798_join[16];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3696WEIGHTED_ROUND_ROBIN_Splitter_3701;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3748WEIGHTED_ROUND_ROBIN_Splitter_3765;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_3789_3799_join[16];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_3792_3802_split[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3730WEIGHTED_ROUND_ROBIN_Splitter_3747;
buffer_complex_t CombineDFT_3688CPrinter_3689;
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_3786_3796_join[4];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_3790_3800_split[16];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_3792_3802_join[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3702WEIGHTED_ROUND_ROBIN_Splitter_3711;
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_3786_3796_split[4];
buffer_complex_t FFTReorderSimple_3678WEIGHTED_ROUND_ROBIN_Splitter_3691;
buffer_complex_t SplitJoin16_CombineDFT_Fiss_3793_3803_split[2];
buffer_complex_t FFTTestSource_3677FFTReorderSimple_3678;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_3789_3799_split[16];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3766WEIGHTED_ROUND_ROBIN_Splitter_3775;
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_3785_3795_split[2];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_3793_3803_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3776WEIGHTED_ROUND_ROBIN_Splitter_3781;
buffer_complex_t SplitJoin10_CombineDFT_Fiss_3790_3800_join[16];
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_3788_3798_split[16];
buffer_complex_t SplitJoin12_CombineDFT_Fiss_3791_3801_join[8];
buffer_complex_t SplitJoin12_CombineDFT_Fiss_3791_3801_split[8];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3782CombineDFT_3688;
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_3785_3795_join[2];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_3787_3797_split[8];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_3787_3797_join[8];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3692WEIGHTED_ROUND_ROBIN_Splitter_3695;


CombineDFT_3731_t CombineDFT_3731_s;
CombineDFT_3731_t CombineDFT_3732_s;
CombineDFT_3731_t CombineDFT_3733_s;
CombineDFT_3731_t CombineDFT_3734_s;
CombineDFT_3731_t CombineDFT_3735_s;
CombineDFT_3731_t CombineDFT_3736_s;
CombineDFT_3731_t CombineDFT_3737_s;
CombineDFT_3731_t CombineDFT_3738_s;
CombineDFT_3731_t CombineDFT_3739_s;
CombineDFT_3731_t CombineDFT_3740_s;
CombineDFT_3731_t CombineDFT_3741_s;
CombineDFT_3731_t CombineDFT_3742_s;
CombineDFT_3731_t CombineDFT_3743_s;
CombineDFT_3731_t CombineDFT_3744_s;
CombineDFT_3731_t CombineDFT_3745_s;
CombineDFT_3731_t CombineDFT_3746_s;
CombineDFT_3731_t CombineDFT_3749_s;
CombineDFT_3731_t CombineDFT_3750_s;
CombineDFT_3731_t CombineDFT_3751_s;
CombineDFT_3731_t CombineDFT_3752_s;
CombineDFT_3731_t CombineDFT_3753_s;
CombineDFT_3731_t CombineDFT_3754_s;
CombineDFT_3731_t CombineDFT_3755_s;
CombineDFT_3731_t CombineDFT_3756_s;
CombineDFT_3731_t CombineDFT_3757_s;
CombineDFT_3731_t CombineDFT_3758_s;
CombineDFT_3731_t CombineDFT_3759_s;
CombineDFT_3731_t CombineDFT_3760_s;
CombineDFT_3731_t CombineDFT_3761_s;
CombineDFT_3731_t CombineDFT_3762_s;
CombineDFT_3731_t CombineDFT_3763_s;
CombineDFT_3731_t CombineDFT_3764_s;
CombineDFT_3731_t CombineDFT_3767_s;
CombineDFT_3731_t CombineDFT_3768_s;
CombineDFT_3731_t CombineDFT_3769_s;
CombineDFT_3731_t CombineDFT_3770_s;
CombineDFT_3731_t CombineDFT_3771_s;
CombineDFT_3731_t CombineDFT_3772_s;
CombineDFT_3731_t CombineDFT_3773_s;
CombineDFT_3731_t CombineDFT_3774_s;
CombineDFT_3731_t CombineDFT_3777_s;
CombineDFT_3731_t CombineDFT_3778_s;
CombineDFT_3731_t CombineDFT_3779_s;
CombineDFT_3731_t CombineDFT_3780_s;
CombineDFT_3731_t CombineDFT_3783_s;
CombineDFT_3731_t CombineDFT_3784_s;
CombineDFT_3731_t CombineDFT_3688_s;

void FFTTestSource(buffer_complex_t *chanout) {
	complex_t c1;
	complex_t zero;
	c1.real = 1.0 ; 
	c1.imag = 0.0 ; 
	zero.real = 0.0 ; 
	zero.imag = 0.0 ; 
	push_complex(&(*chanout), zero) ; 
	push_complex(&(*chanout), c1) ; 
	FOR(int, i, 0,  < , 62, i++) {
		push_complex(&(*chanout), zero) ; 
	}
	ENDFOR
}


void FFTTestSource_3677() {
	FFTTestSource(&(FFTTestSource_3677FFTReorderSimple_3678));
}

void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout) {
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		complex_t __sa7 = {
			.real = 0,
			.imag = 0
		};
		__sa7 = ((complex_t) peek_complex(&(*chanin), i)) ; 
		push_complex(&(*chanout), __sa7) ; 
	}
	ENDFOR
	FOR(int, i, 1,  < , 64, i = (i + 2)) {
		complex_t __sa8 = {
			.real = 0,
			.imag = 0
		};
		__sa8 = ((complex_t) peek_complex(&(*chanin), i)) ; 
		push_complex(&(*chanout), __sa8) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 64, i++) {
		pop_complex(&(*chanin)) ; 
	}
	ENDFOR
}


void FFTReorderSimple_3678() {
	FFTReorderSimple(&(FFTTestSource_3677FFTReorderSimple_3678), &(FFTReorderSimple_3678WEIGHTED_ROUND_ROBIN_Splitter_3691));
}

void FFTReorderSimple_3693() {
	FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_3785_3795_split[0]), &(SplitJoin0_FFTReorderSimple_Fiss_3785_3795_join[0]));
}

void FFTReorderSimple_3694() {
	FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_3785_3795_split[1]), &(SplitJoin0_FFTReorderSimple_Fiss_3785_3795_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_3691() {
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&SplitJoin0_FFTReorderSimple_Fiss_3785_3795_split[0], pop_complex(&FFTReorderSimple_3678WEIGHTED_ROUND_ROBIN_Splitter_3691));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&SplitJoin0_FFTReorderSimple_Fiss_3785_3795_split[1], pop_complex(&FFTReorderSimple_3678WEIGHTED_ROUND_ROBIN_Splitter_3691));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_3692() {
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3692WEIGHTED_ROUND_ROBIN_Splitter_3695, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_3785_3795_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3692WEIGHTED_ROUND_ROBIN_Splitter_3695, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_3785_3795_join[1]));
	ENDFOR
}

void FFTReorderSimple_3697() {
	FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_3786_3796_split[0]), &(SplitJoin2_FFTReorderSimple_Fiss_3786_3796_join[0]));
}

void FFTReorderSimple_3698() {
	FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_3786_3796_split[1]), &(SplitJoin2_FFTReorderSimple_Fiss_3786_3796_join[1]));
}

void FFTReorderSimple_3699() {
	FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_3786_3796_split[2]), &(SplitJoin2_FFTReorderSimple_Fiss_3786_3796_join[2]));
}

void FFTReorderSimple_3700() {
	FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_3786_3796_split[3]), &(SplitJoin2_FFTReorderSimple_Fiss_3786_3796_join[3]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_3695() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_3786_3796_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3692WEIGHTED_ROUND_ROBIN_Splitter_3695));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_3696() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3696WEIGHTED_ROUND_ROBIN_Splitter_3701, pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_3786_3796_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void FFTReorderSimple_3703() {
	FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_3787_3797_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_3787_3797_join[0]));
}

void FFTReorderSimple_3704() {
	FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_3787_3797_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_3787_3797_join[1]));
}

void FFTReorderSimple_3705() {
	FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_3787_3797_split[2]), &(SplitJoin4_FFTReorderSimple_Fiss_3787_3797_join[2]));
}

void FFTReorderSimple_3706() {
	FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_3787_3797_split[3]), &(SplitJoin4_FFTReorderSimple_Fiss_3787_3797_join[3]));
}

void FFTReorderSimple_3707() {
	FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_3787_3797_split[4]), &(SplitJoin4_FFTReorderSimple_Fiss_3787_3797_join[4]));
}

void FFTReorderSimple_3708() {
	FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_3787_3797_split[5]), &(SplitJoin4_FFTReorderSimple_Fiss_3787_3797_join[5]));
}

void FFTReorderSimple_3709() {
	FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_3787_3797_split[6]), &(SplitJoin4_FFTReorderSimple_Fiss_3787_3797_join[6]));
}

void FFTReorderSimple_3710() {
	FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_3787_3797_split[7]), &(SplitJoin4_FFTReorderSimple_Fiss_3787_3797_join[7]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_3701() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3787_3797_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3696WEIGHTED_ROUND_ROBIN_Splitter_3701));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_3702() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3702WEIGHTED_ROUND_ROBIN_Splitter_3711, pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_3787_3797_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void FFTReorderSimple_3713() {
	FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_join[0]));
}

void FFTReorderSimple_3714() {
	FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_join[1]));
}

void FFTReorderSimple_3715() {
	FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_join[2]));
}

void FFTReorderSimple_3716() {
	FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_join[3]));
}

void FFTReorderSimple_3717() {
	FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_split[4]), &(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_join[4]));
}

void FFTReorderSimple_3718() {
	FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_split[5]), &(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_join[5]));
}

void FFTReorderSimple_3719() {
	FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_split[6]), &(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_join[6]));
}

void FFTReorderSimple_3720() {
	FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_split[7]), &(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_join[7]));
}

void FFTReorderSimple_3721() {
	FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_split[8]), &(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_join[8]));
}

void FFTReorderSimple_3722() {
	FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_split[9]), &(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_join[9]));
}

void FFTReorderSimple_3723() {
	FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_split[10]), &(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_join[10]));
}

void FFTReorderSimple_3724() {
	FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_split[11]), &(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_join[11]));
}

void FFTReorderSimple_3725() {
	FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_split[12]), &(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_join[12]));
}

void FFTReorderSimple_3726() {
	FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_split[13]), &(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_join[13]));
}

void FFTReorderSimple_3727() {
	FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_split[14]), &(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_join[14]));
}

void FFTReorderSimple_3728() {
	FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_split[15]), &(SplitJoin6_FFTReorderSimple_Fiss_3788_3798_join[15]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_3711() {
	FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3788_3798_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3702WEIGHTED_ROUND_ROBIN_Splitter_3711));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_3712() {
	FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3712WEIGHTED_ROUND_ROBIN_Splitter_3729, pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3788_3798_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&(*chanin), (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3731_s.wn.real) - (w.imag * CombineDFT_3731_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3731_s.wn.imag) + (w.imag * CombineDFT_3731_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&(*chanin)) ; 
			push_complex(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_3731() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3789_3799_split[0]), &(SplitJoin8_CombineDFT_Fiss_3789_3799_join[0]));
	ENDFOR
}

void CombineDFT_3732() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3789_3799_split[1]), &(SplitJoin8_CombineDFT_Fiss_3789_3799_join[1]));
	ENDFOR
}

void CombineDFT_3733() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3789_3799_split[2]), &(SplitJoin8_CombineDFT_Fiss_3789_3799_join[2]));
	ENDFOR
}

void CombineDFT_3734() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3789_3799_split[3]), &(SplitJoin8_CombineDFT_Fiss_3789_3799_join[3]));
	ENDFOR
}

void CombineDFT_3735() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3789_3799_split[4]), &(SplitJoin8_CombineDFT_Fiss_3789_3799_join[4]));
	ENDFOR
}

void CombineDFT_3736() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3789_3799_split[5]), &(SplitJoin8_CombineDFT_Fiss_3789_3799_join[5]));
	ENDFOR
}

void CombineDFT_3737() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3789_3799_split[6]), &(SplitJoin8_CombineDFT_Fiss_3789_3799_join[6]));
	ENDFOR
}

void CombineDFT_3738() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3789_3799_split[7]), &(SplitJoin8_CombineDFT_Fiss_3789_3799_join[7]));
	ENDFOR
}

void CombineDFT_3739() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3789_3799_split[8]), &(SplitJoin8_CombineDFT_Fiss_3789_3799_join[8]));
	ENDFOR
}

void CombineDFT_3740() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3789_3799_split[9]), &(SplitJoin8_CombineDFT_Fiss_3789_3799_join[9]));
	ENDFOR
}

void CombineDFT_3741() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3789_3799_split[10]), &(SplitJoin8_CombineDFT_Fiss_3789_3799_join[10]));
	ENDFOR
}

void CombineDFT_3742() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3789_3799_split[11]), &(SplitJoin8_CombineDFT_Fiss_3789_3799_join[11]));
	ENDFOR
}

void CombineDFT_3743() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3789_3799_split[12]), &(SplitJoin8_CombineDFT_Fiss_3789_3799_join[12]));
	ENDFOR
}

void CombineDFT_3744() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3789_3799_split[13]), &(SplitJoin8_CombineDFT_Fiss_3789_3799_join[13]));
	ENDFOR
}

void CombineDFT_3745() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3789_3799_split[14]), &(SplitJoin8_CombineDFT_Fiss_3789_3799_join[14]));
	ENDFOR
}

void CombineDFT_3746() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3789_3799_split[15]), &(SplitJoin8_CombineDFT_Fiss_3789_3799_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3729() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_complex(&SplitJoin8_CombineDFT_Fiss_3789_3799_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3712WEIGHTED_ROUND_ROBIN_Splitter_3729));
			push_complex(&SplitJoin8_CombineDFT_Fiss_3789_3799_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3712WEIGHTED_ROUND_ROBIN_Splitter_3729));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3730() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3730WEIGHTED_ROUND_ROBIN_Splitter_3747, pop_complex(&SplitJoin8_CombineDFT_Fiss_3789_3799_join[__iter_]));
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3730WEIGHTED_ROUND_ROBIN_Splitter_3747, pop_complex(&SplitJoin8_CombineDFT_Fiss_3789_3799_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_3749() {
	CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3790_3800_split[0]), &(SplitJoin10_CombineDFT_Fiss_3790_3800_join[0]));
}

void CombineDFT_3750() {
	CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3790_3800_split[1]), &(SplitJoin10_CombineDFT_Fiss_3790_3800_join[1]));
}

void CombineDFT_3751() {
	CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3790_3800_split[2]), &(SplitJoin10_CombineDFT_Fiss_3790_3800_join[2]));
}

void CombineDFT_3752() {
	CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3790_3800_split[3]), &(SplitJoin10_CombineDFT_Fiss_3790_3800_join[3]));
}

void CombineDFT_3753() {
	CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3790_3800_split[4]), &(SplitJoin10_CombineDFT_Fiss_3790_3800_join[4]));
}

void CombineDFT_3754() {
	CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3790_3800_split[5]), &(SplitJoin10_CombineDFT_Fiss_3790_3800_join[5]));
}

void CombineDFT_3755() {
	CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3790_3800_split[6]), &(SplitJoin10_CombineDFT_Fiss_3790_3800_join[6]));
}

void CombineDFT_3756() {
	CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3790_3800_split[7]), &(SplitJoin10_CombineDFT_Fiss_3790_3800_join[7]));
}

void CombineDFT_3757() {
	CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3790_3800_split[8]), &(SplitJoin10_CombineDFT_Fiss_3790_3800_join[8]));
}

void CombineDFT_3758() {
	CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3790_3800_split[9]), &(SplitJoin10_CombineDFT_Fiss_3790_3800_join[9]));
}

void CombineDFT_3759() {
	CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3790_3800_split[10]), &(SplitJoin10_CombineDFT_Fiss_3790_3800_join[10]));
}

void CombineDFT_3760() {
	CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3790_3800_split[11]), &(SplitJoin10_CombineDFT_Fiss_3790_3800_join[11]));
}

void CombineDFT_3761() {
	CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3790_3800_split[12]), &(SplitJoin10_CombineDFT_Fiss_3790_3800_join[12]));
}

void CombineDFT_3762() {
	CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3790_3800_split[13]), &(SplitJoin10_CombineDFT_Fiss_3790_3800_join[13]));
}

void CombineDFT_3763() {
	CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3790_3800_split[14]), &(SplitJoin10_CombineDFT_Fiss_3790_3800_join[14]));
}

void CombineDFT_3764() {
	CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3790_3800_split[15]), &(SplitJoin10_CombineDFT_Fiss_3790_3800_join[15]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_3747() {
	FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_complex(&SplitJoin10_CombineDFT_Fiss_3790_3800_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3730WEIGHTED_ROUND_ROBIN_Splitter_3747));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_3748() {
	FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3748WEIGHTED_ROUND_ROBIN_Splitter_3765, pop_complex(&SplitJoin10_CombineDFT_Fiss_3790_3800_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void CombineDFT_3767() {
	CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3791_3801_split[0]), &(SplitJoin12_CombineDFT_Fiss_3791_3801_join[0]));
}

void CombineDFT_3768() {
	CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3791_3801_split[1]), &(SplitJoin12_CombineDFT_Fiss_3791_3801_join[1]));
}

void CombineDFT_3769() {
	CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3791_3801_split[2]), &(SplitJoin12_CombineDFT_Fiss_3791_3801_join[2]));
}

void CombineDFT_3770() {
	CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3791_3801_split[3]), &(SplitJoin12_CombineDFT_Fiss_3791_3801_join[3]));
}

void CombineDFT_3771() {
	CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3791_3801_split[4]), &(SplitJoin12_CombineDFT_Fiss_3791_3801_join[4]));
}

void CombineDFT_3772() {
	CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3791_3801_split[5]), &(SplitJoin12_CombineDFT_Fiss_3791_3801_join[5]));
}

void CombineDFT_3773() {
	CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3791_3801_split[6]), &(SplitJoin12_CombineDFT_Fiss_3791_3801_join[6]));
}

void CombineDFT_3774() {
	CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3791_3801_split[7]), &(SplitJoin12_CombineDFT_Fiss_3791_3801_join[7]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_3765() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_complex(&SplitJoin12_CombineDFT_Fiss_3791_3801_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3748WEIGHTED_ROUND_ROBIN_Splitter_3765));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_3766() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3766WEIGHTED_ROUND_ROBIN_Splitter_3775, pop_complex(&SplitJoin12_CombineDFT_Fiss_3791_3801_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void CombineDFT_3777() {
	CombineDFT(&(SplitJoin14_CombineDFT_Fiss_3792_3802_split[0]), &(SplitJoin14_CombineDFT_Fiss_3792_3802_join[0]));
}

void CombineDFT_3778() {
	CombineDFT(&(SplitJoin14_CombineDFT_Fiss_3792_3802_split[1]), &(SplitJoin14_CombineDFT_Fiss_3792_3802_join[1]));
}

void CombineDFT_3779() {
	CombineDFT(&(SplitJoin14_CombineDFT_Fiss_3792_3802_split[2]), &(SplitJoin14_CombineDFT_Fiss_3792_3802_join[2]));
}

void CombineDFT_3780() {
	CombineDFT(&(SplitJoin14_CombineDFT_Fiss_3792_3802_split[3]), &(SplitJoin14_CombineDFT_Fiss_3792_3802_join[3]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_3775() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
			push_complex(&SplitJoin14_CombineDFT_Fiss_3792_3802_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3766WEIGHTED_ROUND_ROBIN_Splitter_3775));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_3776() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3776WEIGHTED_ROUND_ROBIN_Splitter_3781, pop_complex(&SplitJoin14_CombineDFT_Fiss_3792_3802_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void CombineDFT_3783() {
	CombineDFT(&(SplitJoin16_CombineDFT_Fiss_3793_3803_split[0]), &(SplitJoin16_CombineDFT_Fiss_3793_3803_join[0]));
}

void CombineDFT_3784() {
	CombineDFT(&(SplitJoin16_CombineDFT_Fiss_3793_3803_split[1]), &(SplitJoin16_CombineDFT_Fiss_3793_3803_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_3781() {
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&SplitJoin16_CombineDFT_Fiss_3793_3803_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3776WEIGHTED_ROUND_ROBIN_Splitter_3781));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&SplitJoin16_CombineDFT_Fiss_3793_3803_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3776WEIGHTED_ROUND_ROBIN_Splitter_3781));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_3782() {
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3782CombineDFT_3688, pop_complex(&SplitJoin16_CombineDFT_Fiss_3793_3803_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3782CombineDFT_3688, pop_complex(&SplitJoin16_CombineDFT_Fiss_3793_3803_join[1]));
	ENDFOR
}

void CombineDFT_3688() {
	CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_3782CombineDFT_3688), &(CombineDFT_3688CPrinter_3689));
}

void CPrinter(buffer_complex_t *chanin) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		printf("%.10f", c.real);
		printf("\n");
		printf("%.10f", c.imag);
		printf("\n");
	}


void CPrinter_3689() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		CPrinter(&(CombineDFT_3688CPrinter_3689));
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3712WEIGHTED_ROUND_ROBIN_Splitter_3729);
	FOR(int, __iter_init_0_, 0, <, 16, __iter_init_0_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_3788_3798_join[__iter_init_0_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3696WEIGHTED_ROUND_ROBIN_Splitter_3701);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3748WEIGHTED_ROUND_ROBIN_Splitter_3765);
	FOR(int, __iter_init_1_, 0, <, 16, __iter_init_1_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_3789_3799_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 4, __iter_init_2_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_3792_3802_split[__iter_init_2_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3730WEIGHTED_ROUND_ROBIN_Splitter_3747);
	init_buffer_complex(&CombineDFT_3688CPrinter_3689);
	FOR(int, __iter_init_3_, 0, <, 4, __iter_init_3_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_3786_3796_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 16, __iter_init_4_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_3790_3800_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 4, __iter_init_5_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_3792_3802_join[__iter_init_5_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3702WEIGHTED_ROUND_ROBIN_Splitter_3711);
	FOR(int, __iter_init_6_, 0, <, 4, __iter_init_6_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_3786_3796_split[__iter_init_6_]);
	ENDFOR
	init_buffer_complex(&FFTReorderSimple_3678WEIGHTED_ROUND_ROBIN_Splitter_3691);
	FOR(int, __iter_init_7_, 0, <, 2, __iter_init_7_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_3793_3803_split[__iter_init_7_]);
	ENDFOR
	init_buffer_complex(&FFTTestSource_3677FFTReorderSimple_3678);
	FOR(int, __iter_init_8_, 0, <, 16, __iter_init_8_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_3789_3799_split[__iter_init_8_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3766WEIGHTED_ROUND_ROBIN_Splitter_3775);
	FOR(int, __iter_init_9_, 0, <, 2, __iter_init_9_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_3785_3795_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_3793_3803_join[__iter_init_10_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3776WEIGHTED_ROUND_ROBIN_Splitter_3781);
	FOR(int, __iter_init_11_, 0, <, 16, __iter_init_11_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_3790_3800_join[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 16, __iter_init_12_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_3788_3798_split[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 8, __iter_init_13_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_3791_3801_join[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 8, __iter_init_14_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_3791_3801_split[__iter_init_14_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3782CombineDFT_3688);
	FOR(int, __iter_init_15_, 0, <, 2, __iter_init_15_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_3785_3795_join[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 8, __iter_init_16_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_3787_3797_split[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 8, __iter_init_17_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_3787_3797_join[__iter_init_17_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3692WEIGHTED_ROUND_ROBIN_Splitter_3695);
// --- init: CombineDFT_3731
	 {
	 ; 
	CombineDFT_3731_s.wn.real = -1.0 ; 
	CombineDFT_3731_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3732
	 {
	CombineDFT_3732_s.wn.real = -1.0 ; 
	CombineDFT_3732_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3733
	 {
	CombineDFT_3733_s.wn.real = -1.0 ; 
	CombineDFT_3733_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3734
	 {
	CombineDFT_3734_s.wn.real = -1.0 ; 
	CombineDFT_3734_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3735
	 {
	CombineDFT_3735_s.wn.real = -1.0 ; 
	CombineDFT_3735_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3736
	 {
	CombineDFT_3736_s.wn.real = -1.0 ; 
	CombineDFT_3736_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3737
	 {
	CombineDFT_3737_s.wn.real = -1.0 ; 
	CombineDFT_3737_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3738
	 {
	CombineDFT_3738_s.wn.real = -1.0 ; 
	CombineDFT_3738_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3739
	 {
	CombineDFT_3739_s.wn.real = -1.0 ; 
	CombineDFT_3739_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3740
	 {
	CombineDFT_3740_s.wn.real = -1.0 ; 
	CombineDFT_3740_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3741
	 {
	CombineDFT_3741_s.wn.real = -1.0 ; 
	CombineDFT_3741_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3742
	 {
	CombineDFT_3742_s.wn.real = -1.0 ; 
	CombineDFT_3742_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3743
	 {
	CombineDFT_3743_s.wn.real = -1.0 ; 
	CombineDFT_3743_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3744
	 {
	CombineDFT_3744_s.wn.real = -1.0 ; 
	CombineDFT_3744_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3745
	 {
	CombineDFT_3745_s.wn.real = -1.0 ; 
	CombineDFT_3745_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3746
	 {
	CombineDFT_3746_s.wn.real = -1.0 ; 
	CombineDFT_3746_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3749
	 {
	CombineDFT_3749_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3749_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3750
	 {
	CombineDFT_3750_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3750_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3751
	 {
	CombineDFT_3751_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3751_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3752
	 {
	CombineDFT_3752_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3752_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3753
	 {
	CombineDFT_3753_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3753_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3754
	 {
	CombineDFT_3754_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3754_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3755
	 {
	CombineDFT_3755_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3755_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3756
	 {
	CombineDFT_3756_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3756_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3757
	 {
	CombineDFT_3757_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3757_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3758
	 {
	CombineDFT_3758_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3758_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3759
	 {
	CombineDFT_3759_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3759_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3760
	 {
	CombineDFT_3760_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3760_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3761
	 {
	CombineDFT_3761_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3761_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3762
	 {
	CombineDFT_3762_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3762_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3763
	 {
	CombineDFT_3763_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3763_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3764
	 {
	CombineDFT_3764_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3764_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3767
	 {
	CombineDFT_3767_s.wn.real = 0.70710677 ; 
	CombineDFT_3767_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3768
	 {
	CombineDFT_3768_s.wn.real = 0.70710677 ; 
	CombineDFT_3768_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3769
	 {
	CombineDFT_3769_s.wn.real = 0.70710677 ; 
	CombineDFT_3769_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3770
	 {
	CombineDFT_3770_s.wn.real = 0.70710677 ; 
	CombineDFT_3770_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3771
	 {
	CombineDFT_3771_s.wn.real = 0.70710677 ; 
	CombineDFT_3771_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3772
	 {
	CombineDFT_3772_s.wn.real = 0.70710677 ; 
	CombineDFT_3772_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3773
	 {
	CombineDFT_3773_s.wn.real = 0.70710677 ; 
	CombineDFT_3773_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3774
	 {
	CombineDFT_3774_s.wn.real = 0.70710677 ; 
	CombineDFT_3774_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3777
	 {
	CombineDFT_3777_s.wn.real = 0.9238795 ; 
	CombineDFT_3777_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_3778
	 {
	CombineDFT_3778_s.wn.real = 0.9238795 ; 
	CombineDFT_3778_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_3779
	 {
	CombineDFT_3779_s.wn.real = 0.9238795 ; 
	CombineDFT_3779_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_3780
	 {
	CombineDFT_3780_s.wn.real = 0.9238795 ; 
	CombineDFT_3780_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_3783
	 {
	CombineDFT_3783_s.wn.real = 0.98078525 ; 
	CombineDFT_3783_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_3784
	 {
	CombineDFT_3784_s.wn.real = 0.98078525 ; 
	CombineDFT_3784_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_3688
	 {
	 ; 
	CombineDFT_3688_s.wn.real = 0.9951847 ; 
	CombineDFT_3688_s.wn.imag = -0.09801714 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FFTTestSource_3677();
		FFTReorderSimple_3678();
		WEIGHTED_ROUND_ROBIN_Splitter_3691();
			FFTReorderSimple_3693();
			FFTReorderSimple_3694();
		WEIGHTED_ROUND_ROBIN_Joiner_3692();
		WEIGHTED_ROUND_ROBIN_Splitter_3695();
			FFTReorderSimple_3697();
			FFTReorderSimple_3698();
			FFTReorderSimple_3699();
			FFTReorderSimple_3700();
		WEIGHTED_ROUND_ROBIN_Joiner_3696();
		WEIGHTED_ROUND_ROBIN_Splitter_3701();
			FFTReorderSimple_3703();
			FFTReorderSimple_3704();
			FFTReorderSimple_3705();
			FFTReorderSimple_3706();
			FFTReorderSimple_3707();
			FFTReorderSimple_3708();
			FFTReorderSimple_3709();
			FFTReorderSimple_3710();
		WEIGHTED_ROUND_ROBIN_Joiner_3702();
		WEIGHTED_ROUND_ROBIN_Splitter_3711();
			FFTReorderSimple_3713();
			FFTReorderSimple_3714();
			FFTReorderSimple_3715();
			FFTReorderSimple_3716();
			FFTReorderSimple_3717();
			FFTReorderSimple_3718();
			FFTReorderSimple_3719();
			FFTReorderSimple_3720();
			FFTReorderSimple_3721();
			FFTReorderSimple_3722();
			FFTReorderSimple_3723();
			FFTReorderSimple_3724();
			FFTReorderSimple_3725();
			FFTReorderSimple_3726();
			FFTReorderSimple_3727();
			FFTReorderSimple_3728();
		WEIGHTED_ROUND_ROBIN_Joiner_3712();
		WEIGHTED_ROUND_ROBIN_Splitter_3729();
			CombineDFT_3731();
			CombineDFT_3732();
			CombineDFT_3733();
			CombineDFT_3734();
			CombineDFT_3735();
			CombineDFT_3736();
			CombineDFT_3737();
			CombineDFT_3738();
			CombineDFT_3739();
			CombineDFT_3740();
			CombineDFT_3741();
			CombineDFT_3742();
			CombineDFT_3743();
			CombineDFT_3744();
			CombineDFT_3745();
			CombineDFT_3746();
		WEIGHTED_ROUND_ROBIN_Joiner_3730();
		WEIGHTED_ROUND_ROBIN_Splitter_3747();
			CombineDFT_3749();
			CombineDFT_3750();
			CombineDFT_3751();
			CombineDFT_3752();
			CombineDFT_3753();
			CombineDFT_3754();
			CombineDFT_3755();
			CombineDFT_3756();
			CombineDFT_3757();
			CombineDFT_3758();
			CombineDFT_3759();
			CombineDFT_3760();
			CombineDFT_3761();
			CombineDFT_3762();
			CombineDFT_3763();
			CombineDFT_3764();
		WEIGHTED_ROUND_ROBIN_Joiner_3748();
		WEIGHTED_ROUND_ROBIN_Splitter_3765();
			CombineDFT_3767();
			CombineDFT_3768();
			CombineDFT_3769();
			CombineDFT_3770();
			CombineDFT_3771();
			CombineDFT_3772();
			CombineDFT_3773();
			CombineDFT_3774();
		WEIGHTED_ROUND_ROBIN_Joiner_3766();
		WEIGHTED_ROUND_ROBIN_Splitter_3775();
			CombineDFT_3777();
			CombineDFT_3778();
			CombineDFT_3779();
			CombineDFT_3780();
		WEIGHTED_ROUND_ROBIN_Joiner_3776();
		WEIGHTED_ROUND_ROBIN_Splitter_3781();
			CombineDFT_3783();
			CombineDFT_3784();
		WEIGHTED_ROUND_ROBIN_Joiner_3782();
		CombineDFT_3688();
		CPrinter_3689();
	ENDFOR
	return EXIT_SUCCESS;
}
