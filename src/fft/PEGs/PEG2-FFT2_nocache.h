#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=512 on the compile command line
#else
#if BUF_SIZEMAX < 512
#error BUF_SIZEMAX too small, it must be at least 512
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float w[2];
} CombineDFT_12599_t;

typedef struct {
	float w[4];
} CombineDFT_12603_t;

typedef struct {
	float w[8];
} CombineDFT_12607_t;

typedef struct {
	float w[16];
} CombineDFT_12611_t;

typedef struct {
	float w[32];
} CombineDFT_12615_t;

typedef struct {
	float w[64];
} CombineDFT_12556_t;
void WEIGHTED_ROUND_ROBIN_Splitter_12577();
void FFTTestSource_12579();
void FFTTestSource_12580();
void WEIGHTED_ROUND_ROBIN_Joiner_12578();
void WEIGHTED_ROUND_ROBIN_Splitter_12569();
void FFTReorderSimple_12546();
void WEIGHTED_ROUND_ROBIN_Splitter_12581();
void FFTReorderSimple_12583();
void FFTReorderSimple_12584();
void WEIGHTED_ROUND_ROBIN_Joiner_12582();
void WEIGHTED_ROUND_ROBIN_Splitter_12585();
void FFTReorderSimple_12587();
void FFTReorderSimple_12588();
void WEIGHTED_ROUND_ROBIN_Joiner_12586();
void WEIGHTED_ROUND_ROBIN_Splitter_12589();
void FFTReorderSimple_12591();
void FFTReorderSimple_12592();
void WEIGHTED_ROUND_ROBIN_Joiner_12590();
void WEIGHTED_ROUND_ROBIN_Splitter_12593();
void FFTReorderSimple_12595();
void FFTReorderSimple_12596();
void WEIGHTED_ROUND_ROBIN_Joiner_12594();
void WEIGHTED_ROUND_ROBIN_Splitter_12597();
void CombineDFT_12599();
void CombineDFT_12600();
void WEIGHTED_ROUND_ROBIN_Joiner_12598();
void WEIGHTED_ROUND_ROBIN_Splitter_12601();
void CombineDFT_12603();
void CombineDFT_12604();
void WEIGHTED_ROUND_ROBIN_Joiner_12602();
void WEIGHTED_ROUND_ROBIN_Splitter_12605();
void CombineDFT_12607();
void CombineDFT_12608();
void WEIGHTED_ROUND_ROBIN_Joiner_12606();
void WEIGHTED_ROUND_ROBIN_Splitter_12609();
void CombineDFT_12611();
void CombineDFT_12612();
void WEIGHTED_ROUND_ROBIN_Joiner_12610();
void WEIGHTED_ROUND_ROBIN_Splitter_12613();
void CombineDFT_12615();
void CombineDFT_12616();
void WEIGHTED_ROUND_ROBIN_Joiner_12614();
void CombineDFT_12556();
void FFTReorderSimple_12557();
void WEIGHTED_ROUND_ROBIN_Splitter_12617();
void FFTReorderSimple_12619();
void FFTReorderSimple_12620();
void WEIGHTED_ROUND_ROBIN_Joiner_12618();
void WEIGHTED_ROUND_ROBIN_Splitter_12621();
void FFTReorderSimple_12623();
void FFTReorderSimple_12624();
void WEIGHTED_ROUND_ROBIN_Joiner_12622();
void WEIGHTED_ROUND_ROBIN_Splitter_12625();
void FFTReorderSimple_12627();
void FFTReorderSimple_12628();
void WEIGHTED_ROUND_ROBIN_Joiner_12626();
void WEIGHTED_ROUND_ROBIN_Splitter_12629();
void FFTReorderSimple_12631();
void FFTReorderSimple_12632();
void WEIGHTED_ROUND_ROBIN_Joiner_12630();
void WEIGHTED_ROUND_ROBIN_Splitter_12633();
void CombineDFT_12635();
void CombineDFT_12636();
void WEIGHTED_ROUND_ROBIN_Joiner_12634();
void WEIGHTED_ROUND_ROBIN_Splitter_12637();
void CombineDFT_12639();
void CombineDFT_12640();
void WEIGHTED_ROUND_ROBIN_Joiner_12638();
void WEIGHTED_ROUND_ROBIN_Splitter_12641();
void CombineDFT_12643();
void CombineDFT_12644();
void WEIGHTED_ROUND_ROBIN_Joiner_12642();
void WEIGHTED_ROUND_ROBIN_Splitter_12645();
void CombineDFT_12647();
void CombineDFT_12648();
void WEIGHTED_ROUND_ROBIN_Joiner_12646();
void WEIGHTED_ROUND_ROBIN_Splitter_12649();
void CombineDFT_12651();
void CombineDFT_12652();
void WEIGHTED_ROUND_ROBIN_Joiner_12650();
void CombineDFT_12567();
void WEIGHTED_ROUND_ROBIN_Joiner_12570();
void FloatPrinter_12568();

#ifdef __cplusplus
}
#endif
#endif
