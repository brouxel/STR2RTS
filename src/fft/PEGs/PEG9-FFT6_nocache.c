#include "PEG9-FFT6_nocache.h"

buffer_complex_t FFTTestSource_5007FFTReorderSimple_5008;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5053WEIGHTED_ROUND_ROBIN_Splitter_5063;
buffer_complex_t SplitJoin10_CombineDFT_Fiss_5099_5109_join[9];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5022WEIGHTED_ROUND_ROBIN_Splitter_5025;
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_5097_5107_split[9];
buffer_complex_t SplitJoin8_CombineDFT_Fiss_5098_5108_join[9];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5085WEIGHTED_ROUND_ROBIN_Splitter_5090;
buffer_complex_t CombineDFT_5018CPrinter_5019;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5075WEIGHTED_ROUND_ROBIN_Splitter_5084;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5091CombineDFT_5018;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_5098_5108_split[9];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_5096_5106_join[8];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_5094_5104_join[2];
buffer_complex_t SplitJoin12_CombineDFT_Fiss_5100_5110_split[8];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_5101_5111_join[4];
buffer_complex_t FFTReorderSimple_5008WEIGHTED_ROUND_ROBIN_Splitter_5021;
buffer_complex_t SplitJoin12_CombineDFT_Fiss_5100_5110_join[8];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_5101_5111_split[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5042WEIGHTED_ROUND_ROBIN_Splitter_5052;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5032WEIGHTED_ROUND_ROBIN_Splitter_5041;
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_5094_5104_split[2];
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_5095_5105_split[4];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_5099_5109_split[9];
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_5097_5107_join[9];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_5102_5112_split[2];
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_5095_5105_join[4];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_5096_5106_split[8];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_5102_5112_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5064WEIGHTED_ROUND_ROBIN_Splitter_5074;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5026WEIGHTED_ROUND_ROBIN_Splitter_5031;


CombineDFT_5054_t CombineDFT_5054_s;
CombineDFT_5054_t CombineDFT_5055_s;
CombineDFT_5054_t CombineDFT_5056_s;
CombineDFT_5054_t CombineDFT_5057_s;
CombineDFT_5054_t CombineDFT_5058_s;
CombineDFT_5054_t CombineDFT_5059_s;
CombineDFT_5054_t CombineDFT_5060_s;
CombineDFT_5054_t CombineDFT_5061_s;
CombineDFT_5054_t CombineDFT_5062_s;
CombineDFT_5054_t CombineDFT_5065_s;
CombineDFT_5054_t CombineDFT_5066_s;
CombineDFT_5054_t CombineDFT_5067_s;
CombineDFT_5054_t CombineDFT_5068_s;
CombineDFT_5054_t CombineDFT_5069_s;
CombineDFT_5054_t CombineDFT_5070_s;
CombineDFT_5054_t CombineDFT_5071_s;
CombineDFT_5054_t CombineDFT_5072_s;
CombineDFT_5054_t CombineDFT_5073_s;
CombineDFT_5054_t CombineDFT_5076_s;
CombineDFT_5054_t CombineDFT_5077_s;
CombineDFT_5054_t CombineDFT_5078_s;
CombineDFT_5054_t CombineDFT_5079_s;
CombineDFT_5054_t CombineDFT_5080_s;
CombineDFT_5054_t CombineDFT_5081_s;
CombineDFT_5054_t CombineDFT_5082_s;
CombineDFT_5054_t CombineDFT_5083_s;
CombineDFT_5054_t CombineDFT_5086_s;
CombineDFT_5054_t CombineDFT_5087_s;
CombineDFT_5054_t CombineDFT_5088_s;
CombineDFT_5054_t CombineDFT_5089_s;
CombineDFT_5054_t CombineDFT_5092_s;
CombineDFT_5054_t CombineDFT_5093_s;
CombineDFT_5054_t CombineDFT_5018_s;

void FFTTestSource_5007(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		complex_t c1;
		complex_t zero;
		c1.real = 1.0 ; 
		c1.imag = 0.0 ; 
		zero.real = 0.0 ; 
		zero.imag = 0.0 ; 
		push_complex(&FFTTestSource_5007FFTReorderSimple_5008, zero) ; 
		push_complex(&FFTTestSource_5007FFTReorderSimple_5008, c1) ; 
		FOR(int, i, 0,  < , 62, i++) {
			push_complex(&FFTTestSource_5007FFTReorderSimple_5008, zero) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5008(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&FFTTestSource_5007FFTReorderSimple_5008, i)) ; 
			push_complex(&FFTReorderSimple_5008WEIGHTED_ROUND_ROBIN_Splitter_5021, __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 64, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&FFTTestSource_5007FFTReorderSimple_5008, i)) ; 
			push_complex(&FFTReorderSimple_5008WEIGHTED_ROUND_ROBIN_Splitter_5021, __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&FFTTestSource_5007FFTReorderSimple_5008) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5023(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_5094_5104_split[0], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_5094_5104_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 32, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_5094_5104_split[0], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_5094_5104_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_5094_5104_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5024(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_5094_5104_split[1], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_5094_5104_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 32, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_5094_5104_split[1], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_5094_5104_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_5094_5104_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5021() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_5094_5104_split[0], pop_complex(&FFTReorderSimple_5008WEIGHTED_ROUND_ROBIN_Splitter_5021));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_5094_5104_split[1], pop_complex(&FFTReorderSimple_5008WEIGHTED_ROUND_ROBIN_Splitter_5021));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5022() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5022WEIGHTED_ROUND_ROBIN_Splitter_5025, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_5094_5104_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5022WEIGHTED_ROUND_ROBIN_Splitter_5025, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_5094_5104_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_5027(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_5095_5105_split[0], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5095_5105_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_5095_5105_split[0], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5095_5105_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_5095_5105_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5028(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_5095_5105_split[1], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5095_5105_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_5095_5105_split[1], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5095_5105_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_5095_5105_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5029(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_5095_5105_split[2], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5095_5105_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_5095_5105_split[2], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5095_5105_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_5095_5105_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5030(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_5095_5105_split[3], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5095_5105_join[3], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_5095_5105_split[3], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5095_5105_join[3], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_5095_5105_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5025() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5095_5105_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5022WEIGHTED_ROUND_ROBIN_Splitter_5025));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5026() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5026WEIGHTED_ROUND_ROBIN_Splitter_5031, pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_5095_5105_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_5033(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_split[0], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_split[0], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5034(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_split[1], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_split[1], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5035(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_split[2], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_split[2], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5036(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_split[3], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_join[3], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_split[3], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_join[3], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5037(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_split[4], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_join[4], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_split[4], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_join[4], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5038(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_split[5], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_join[5], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_split[5], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_join[5], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5039(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_split[6], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_join[6], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_split[6], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_join[6], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5040(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_split[7], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_join[7], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_split[7], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_join[7], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5031() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5026WEIGHTED_ROUND_ROBIN_Splitter_5031));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5032() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5032WEIGHTED_ROUND_ROBIN_Splitter_5041, pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_5043(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_split[0], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_split[0], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5044(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_split[1], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_split[1], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5045(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_split[2], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_split[2], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5046(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_split[3], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_join[3], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_split[3], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_join[3], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5047(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_split[4], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_join[4], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_split[4], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_join[4], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5048(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_split[5], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_join[5], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_split[5], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_join[5], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5049(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_split[6], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_join[6], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_split[6], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_join[6], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5050(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_split[7], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_join[7], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_split[7], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_join[7], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5051(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_split[8], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_join[8], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_split[8], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_join[8], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_split[8]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5041() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 9, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5032WEIGHTED_ROUND_ROBIN_Splitter_5041));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5042() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 9, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5042WEIGHTED_ROUND_ROBIN_Splitter_5052, pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5054(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_split[0], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5054_s.wn.real) - (w.imag * CombineDFT_5054_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5054_s.wn.imag) + (w.imag * CombineDFT_5054_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_split[0]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5055(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_split[1], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5055_s.wn.real) - (w.imag * CombineDFT_5055_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5055_s.wn.imag) + (w.imag * CombineDFT_5055_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_split[1]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5056(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_split[2], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5056_s.wn.real) - (w.imag * CombineDFT_5056_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5056_s.wn.imag) + (w.imag * CombineDFT_5056_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_split[2]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5057(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_split[3], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5057_s.wn.real) - (w.imag * CombineDFT_5057_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5057_s.wn.imag) + (w.imag * CombineDFT_5057_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_split[3]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5058(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_split[4], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_split[4], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5058_s.wn.real) - (w.imag * CombineDFT_5058_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5058_s.wn.imag) + (w.imag * CombineDFT_5058_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_split[4]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5059(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_split[5], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_split[5], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5059_s.wn.real) - (w.imag * CombineDFT_5059_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5059_s.wn.imag) + (w.imag * CombineDFT_5059_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_split[5]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5060(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_split[6], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_split[6], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5060_s.wn.real) - (w.imag * CombineDFT_5060_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5060_s.wn.imag) + (w.imag * CombineDFT_5060_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_split[6]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5061(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_split[7], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_split[7], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5061_s.wn.real) - (w.imag * CombineDFT_5061_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5061_s.wn.imag) + (w.imag * CombineDFT_5061_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_split[7]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5062(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_split[8], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_split[8], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5062_s.wn.real) - (w.imag * CombineDFT_5062_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5062_s.wn.imag) + (w.imag * CombineDFT_5062_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_split[8]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_join[8], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5052() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 9, __iter_++)
			push_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5042WEIGHTED_ROUND_ROBIN_Splitter_5052));
			push_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5042WEIGHTED_ROUND_ROBIN_Splitter_5052));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5053() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 9, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5053WEIGHTED_ROUND_ROBIN_Splitter_5063, pop_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_join[__iter_]));
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5053WEIGHTED_ROUND_ROBIN_Splitter_5063, pop_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_5065(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_split[0], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5065_s.wn.real) - (w.imag * CombineDFT_5065_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5065_s.wn.imag) + (w.imag * CombineDFT_5065_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_split[0]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5066(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_split[1], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5066_s.wn.real) - (w.imag * CombineDFT_5066_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5066_s.wn.imag) + (w.imag * CombineDFT_5066_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_split[1]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5067(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_split[2], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5067_s.wn.real) - (w.imag * CombineDFT_5067_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5067_s.wn.imag) + (w.imag * CombineDFT_5067_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_split[2]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5068(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_split[3], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5068_s.wn.real) - (w.imag * CombineDFT_5068_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5068_s.wn.imag) + (w.imag * CombineDFT_5068_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_split[3]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5069(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_split[4], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_split[4], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5069_s.wn.real) - (w.imag * CombineDFT_5069_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5069_s.wn.imag) + (w.imag * CombineDFT_5069_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_split[4]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5070(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_split[5], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_split[5], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5070_s.wn.real) - (w.imag * CombineDFT_5070_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5070_s.wn.imag) + (w.imag * CombineDFT_5070_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_split[5]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5071(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_split[6], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_split[6], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5071_s.wn.real) - (w.imag * CombineDFT_5071_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5071_s.wn.imag) + (w.imag * CombineDFT_5071_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_split[6]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5072(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_split[7], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_split[7], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5072_s.wn.real) - (w.imag * CombineDFT_5072_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5072_s.wn.imag) + (w.imag * CombineDFT_5072_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_split[7]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5073(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_split[8], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_split[8], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5073_s.wn.real) - (w.imag * CombineDFT_5073_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5073_s.wn.imag) + (w.imag * CombineDFT_5073_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_split[8]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_join[8], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5063() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 9, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5053WEIGHTED_ROUND_ROBIN_Splitter_5063));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5064() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 9, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5064WEIGHTED_ROUND_ROBIN_Splitter_5074, pop_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5076(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_split[0], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5076_s.wn.real) - (w.imag * CombineDFT_5076_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5076_s.wn.imag) + (w.imag * CombineDFT_5076_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_split[0]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5077(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_split[1], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5077_s.wn.real) - (w.imag * CombineDFT_5077_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5077_s.wn.imag) + (w.imag * CombineDFT_5077_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_split[1]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5078(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_split[2], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5078_s.wn.real) - (w.imag * CombineDFT_5078_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5078_s.wn.imag) + (w.imag * CombineDFT_5078_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_split[2]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5079(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_split[3], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5079_s.wn.real) - (w.imag * CombineDFT_5079_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5079_s.wn.imag) + (w.imag * CombineDFT_5079_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_split[3]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5080(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_split[4], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_split[4], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5080_s.wn.real) - (w.imag * CombineDFT_5080_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5080_s.wn.imag) + (w.imag * CombineDFT_5080_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_split[4]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5081(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_split[5], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_split[5], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5081_s.wn.real) - (w.imag * CombineDFT_5081_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5081_s.wn.imag) + (w.imag * CombineDFT_5081_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_split[5]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5082(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_split[6], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_split[6], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5082_s.wn.real) - (w.imag * CombineDFT_5082_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5082_s.wn.imag) + (w.imag * CombineDFT_5082_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_split[6]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5083(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_split[7], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_split[7], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5083_s.wn.real) - (w.imag * CombineDFT_5083_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5083_s.wn.imag) + (w.imag * CombineDFT_5083_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_split[7]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5074() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5064WEIGHTED_ROUND_ROBIN_Splitter_5074));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5075() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5075WEIGHTED_ROUND_ROBIN_Splitter_5084, pop_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5086(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_5101_5111_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_5101_5111_split[0], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5086_s.wn.real) - (w.imag * CombineDFT_5086_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5086_s.wn.imag) + (w.imag * CombineDFT_5086_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_5101_5111_split[0]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_5101_5111_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5087(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_5101_5111_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_5101_5111_split[1], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5087_s.wn.real) - (w.imag * CombineDFT_5087_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5087_s.wn.imag) + (w.imag * CombineDFT_5087_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_5101_5111_split[1]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_5101_5111_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5088(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_5101_5111_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_5101_5111_split[2], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5088_s.wn.real) - (w.imag * CombineDFT_5088_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5088_s.wn.imag) + (w.imag * CombineDFT_5088_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_5101_5111_split[2]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_5101_5111_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5089(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_5101_5111_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_5101_5111_split[3], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5089_s.wn.real) - (w.imag * CombineDFT_5089_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5089_s.wn.imag) + (w.imag * CombineDFT_5089_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_5101_5111_split[3]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_5101_5111_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5084() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin14_CombineDFT_Fiss_5101_5111_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5075WEIGHTED_ROUND_ROBIN_Splitter_5084));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5085() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5085WEIGHTED_ROUND_ROBIN_Splitter_5090, pop_complex(&SplitJoin14_CombineDFT_Fiss_5101_5111_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5092(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[32];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 16, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_5102_5112_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_5102_5112_split[0], (16 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(16 + i)].real = (y0.real - y1w.real) ; 
			results[(16 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5092_s.wn.real) - (w.imag * CombineDFT_5092_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5092_s.wn.imag) + (w.imag * CombineDFT_5092_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin16_CombineDFT_Fiss_5102_5112_split[0]) ; 
			push_complex(&SplitJoin16_CombineDFT_Fiss_5102_5112_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5093(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[32];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 16, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_5102_5112_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_5102_5112_split[1], (16 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(16 + i)].real = (y0.real - y1w.real) ; 
			results[(16 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5093_s.wn.real) - (w.imag * CombineDFT_5093_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5093_s.wn.imag) + (w.imag * CombineDFT_5093_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin16_CombineDFT_Fiss_5102_5112_split[1]) ; 
			push_complex(&SplitJoin16_CombineDFT_Fiss_5102_5112_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5090() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_5102_5112_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5085WEIGHTED_ROUND_ROBIN_Splitter_5090));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_5102_5112_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5085WEIGHTED_ROUND_ROBIN_Splitter_5090));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5091() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5091CombineDFT_5018, pop_complex(&SplitJoin16_CombineDFT_Fiss_5102_5112_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5091CombineDFT_5018, pop_complex(&SplitJoin16_CombineDFT_Fiss_5102_5112_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_5018(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[64];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5091CombineDFT_5018, i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5091CombineDFT_5018, (32 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(32 + i)].real = (y0.real - y1w.real) ; 
			results[(32 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5018_s.wn.real) - (w.imag * CombineDFT_5018_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5018_s.wn.imag) + (w.imag * CombineDFT_5018_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5091CombineDFT_5018) ; 
			push_complex(&CombineDFT_5018CPrinter_5019, results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CPrinter_5019(){
	FOR(uint32_t, __iter_steady_, 0, <, 576, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&CombineDFT_5018CPrinter_5019));
		printf("%.10f", c.real);
		printf("\n");
		printf("%.10f", c.imag);
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&FFTTestSource_5007FFTReorderSimple_5008);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5053WEIGHTED_ROUND_ROBIN_Splitter_5063);
	FOR(int, __iter_init_0_, 0, <, 9, __iter_init_0_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_join[__iter_init_0_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5022WEIGHTED_ROUND_ROBIN_Splitter_5025);
	FOR(int, __iter_init_1_, 0, <, 9, __iter_init_1_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 9, __iter_init_2_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_join[__iter_init_2_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5085WEIGHTED_ROUND_ROBIN_Splitter_5090);
	init_buffer_complex(&CombineDFT_5018CPrinter_5019);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5075WEIGHTED_ROUND_ROBIN_Splitter_5084);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5091CombineDFT_5018);
	FOR(int, __iter_init_3_, 0, <, 9, __iter_init_3_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_5098_5108_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 2, __iter_init_5_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_5094_5104_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 4, __iter_init_7_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_5101_5111_join[__iter_init_7_]);
	ENDFOR
	init_buffer_complex(&FFTReorderSimple_5008WEIGHTED_ROUND_ROBIN_Splitter_5021);
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_5100_5110_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 4, __iter_init_9_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_5101_5111_split[__iter_init_9_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5042WEIGHTED_ROUND_ROBIN_Splitter_5052);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5032WEIGHTED_ROUND_ROBIN_Splitter_5041);
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_5094_5104_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 4, __iter_init_11_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_5095_5105_split[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 9, __iter_init_12_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_5099_5109_split[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 9, __iter_init_13_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_5097_5107_join[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_5102_5112_split[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 4, __iter_init_15_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_5095_5105_join[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 8, __iter_init_16_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_5096_5106_split[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_5102_5112_join[__iter_init_17_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5064WEIGHTED_ROUND_ROBIN_Splitter_5074);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5026WEIGHTED_ROUND_ROBIN_Splitter_5031);
// --- init: CombineDFT_5054
	 {
	 ; 
	CombineDFT_5054_s.wn.real = -1.0 ; 
	CombineDFT_5054_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5055
	 {
	 ; 
	CombineDFT_5055_s.wn.real = -1.0 ; 
	CombineDFT_5055_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5056
	 {
	 ; 
	CombineDFT_5056_s.wn.real = -1.0 ; 
	CombineDFT_5056_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5057
	 {
	 ; 
	CombineDFT_5057_s.wn.real = -1.0 ; 
	CombineDFT_5057_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5058
	 {
	 ; 
	CombineDFT_5058_s.wn.real = -1.0 ; 
	CombineDFT_5058_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5059
	 {
	 ; 
	CombineDFT_5059_s.wn.real = -1.0 ; 
	CombineDFT_5059_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5060
	 {
	 ; 
	CombineDFT_5060_s.wn.real = -1.0 ; 
	CombineDFT_5060_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5061
	 {
	 ; 
	CombineDFT_5061_s.wn.real = -1.0 ; 
	CombineDFT_5061_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5062
	 {
	 ; 
	CombineDFT_5062_s.wn.real = -1.0 ; 
	CombineDFT_5062_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5065
	 {
	 ; 
	CombineDFT_5065_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5065_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5066
	 {
	 ; 
	CombineDFT_5066_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5066_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5067
	 {
	 ; 
	CombineDFT_5067_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5067_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5068
	 {
	 ; 
	CombineDFT_5068_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5068_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5069
	 {
	 ; 
	CombineDFT_5069_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5069_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5070
	 {
	 ; 
	CombineDFT_5070_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5070_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5071
	 {
	 ; 
	CombineDFT_5071_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5071_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5072
	 {
	 ; 
	CombineDFT_5072_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5072_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5073
	 {
	 ; 
	CombineDFT_5073_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5073_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5076
	 {
	 ; 
	CombineDFT_5076_s.wn.real = 0.70710677 ; 
	CombineDFT_5076_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5077
	 {
	 ; 
	CombineDFT_5077_s.wn.real = 0.70710677 ; 
	CombineDFT_5077_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5078
	 {
	 ; 
	CombineDFT_5078_s.wn.real = 0.70710677 ; 
	CombineDFT_5078_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5079
	 {
	 ; 
	CombineDFT_5079_s.wn.real = 0.70710677 ; 
	CombineDFT_5079_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5080
	 {
	 ; 
	CombineDFT_5080_s.wn.real = 0.70710677 ; 
	CombineDFT_5080_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5081
	 {
	 ; 
	CombineDFT_5081_s.wn.real = 0.70710677 ; 
	CombineDFT_5081_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5082
	 {
	 ; 
	CombineDFT_5082_s.wn.real = 0.70710677 ; 
	CombineDFT_5082_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5083
	 {
	 ; 
	CombineDFT_5083_s.wn.real = 0.70710677 ; 
	CombineDFT_5083_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5086
	 {
	 ; 
	CombineDFT_5086_s.wn.real = 0.9238795 ; 
	CombineDFT_5086_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_5087
	 {
	 ; 
	CombineDFT_5087_s.wn.real = 0.9238795 ; 
	CombineDFT_5087_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_5088
	 {
	 ; 
	CombineDFT_5088_s.wn.real = 0.9238795 ; 
	CombineDFT_5088_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_5089
	 {
	 ; 
	CombineDFT_5089_s.wn.real = 0.9238795 ; 
	CombineDFT_5089_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_5092
	 {
	 ; 
	CombineDFT_5092_s.wn.real = 0.98078525 ; 
	CombineDFT_5092_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_5093
	 {
	 ; 
	CombineDFT_5093_s.wn.real = 0.98078525 ; 
	CombineDFT_5093_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_5018
	 {
	 ; 
	CombineDFT_5018_s.wn.real = 0.9951847 ; 
	CombineDFT_5018_s.wn.imag = -0.09801714 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FFTTestSource_5007();
		FFTReorderSimple_5008();
		WEIGHTED_ROUND_ROBIN_Splitter_5021();
			FFTReorderSimple_5023();
			FFTReorderSimple_5024();
		WEIGHTED_ROUND_ROBIN_Joiner_5022();
		WEIGHTED_ROUND_ROBIN_Splitter_5025();
			FFTReorderSimple_5027();
			FFTReorderSimple_5028();
			FFTReorderSimple_5029();
			FFTReorderSimple_5030();
		WEIGHTED_ROUND_ROBIN_Joiner_5026();
		WEIGHTED_ROUND_ROBIN_Splitter_5031();
			FFTReorderSimple_5033();
			FFTReorderSimple_5034();
			FFTReorderSimple_5035();
			FFTReorderSimple_5036();
			FFTReorderSimple_5037();
			FFTReorderSimple_5038();
			FFTReorderSimple_5039();
			FFTReorderSimple_5040();
		WEIGHTED_ROUND_ROBIN_Joiner_5032();
		WEIGHTED_ROUND_ROBIN_Splitter_5041();
			FFTReorderSimple_5043();
			FFTReorderSimple_5044();
			FFTReorderSimple_5045();
			FFTReorderSimple_5046();
			FFTReorderSimple_5047();
			FFTReorderSimple_5048();
			FFTReorderSimple_5049();
			FFTReorderSimple_5050();
			FFTReorderSimple_5051();
		WEIGHTED_ROUND_ROBIN_Joiner_5042();
		WEIGHTED_ROUND_ROBIN_Splitter_5052();
			CombineDFT_5054();
			CombineDFT_5055();
			CombineDFT_5056();
			CombineDFT_5057();
			CombineDFT_5058();
			CombineDFT_5059();
			CombineDFT_5060();
			CombineDFT_5061();
			CombineDFT_5062();
		WEIGHTED_ROUND_ROBIN_Joiner_5053();
		WEIGHTED_ROUND_ROBIN_Splitter_5063();
			CombineDFT_5065();
			CombineDFT_5066();
			CombineDFT_5067();
			CombineDFT_5068();
			CombineDFT_5069();
			CombineDFT_5070();
			CombineDFT_5071();
			CombineDFT_5072();
			CombineDFT_5073();
		WEIGHTED_ROUND_ROBIN_Joiner_5064();
		WEIGHTED_ROUND_ROBIN_Splitter_5074();
			CombineDFT_5076();
			CombineDFT_5077();
			CombineDFT_5078();
			CombineDFT_5079();
			CombineDFT_5080();
			CombineDFT_5081();
			CombineDFT_5082();
			CombineDFT_5083();
		WEIGHTED_ROUND_ROBIN_Joiner_5075();
		WEIGHTED_ROUND_ROBIN_Splitter_5084();
			CombineDFT_5086();
			CombineDFT_5087();
			CombineDFT_5088();
			CombineDFT_5089();
		WEIGHTED_ROUND_ROBIN_Joiner_5085();
		WEIGHTED_ROUND_ROBIN_Splitter_5090();
			CombineDFT_5092();
			CombineDFT_5093();
		WEIGHTED_ROUND_ROBIN_Joiner_5091();
		CombineDFT_5018();
		CPrinter_5019();
	ENDFOR
	return EXIT_SUCCESS;
}
