#include "PEG22-FFT2_nocache.h"

buffer_float_t SplitJoin98_FFTReorderSimple_Fiss_5229_5250_join[4];
buffer_float_t SplitJoin108_CombineDFT_Fiss_5234_5255_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5208WEIGHTED_ROUND_ROBIN_Splitter_5213;
buffer_float_t FFTReorderSimple_4993WEIGHTED_ROUND_ROBIN_Splitter_5117;
buffer_float_t SplitJoin112_CombineDFT_Fiss_5236_5257_split[2];
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[16];
buffer_float_t SplitJoin18_CombineDFT_Fiss_5226_5247_join[4];
buffer_float_t SplitJoin0_FFTTestSource_Fiss_5217_5238_split[2];
buffer_float_t SplitJoin110_CombineDFT_Fiss_5235_5256_join[4];
buffer_float_t SplitJoin12_CombineDFT_Fiss_5223_5244_join[22];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5080WEIGHTED_ROUND_ROBIN_Splitter_5097;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5108WEIGHTED_ROUND_ROBIN_Splitter_5113;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5128WEIGHTED_ROUND_ROBIN_Splitter_5137;
buffer_float_t SplitJoin106_CombineDFT_Fiss_5233_5254_split[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5006FloatPrinter_5004;
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_5220_5241_split[4];
buffer_float_t SplitJoin12_CombineDFT_Fiss_5223_5244_split[22];
buffer_float_t SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5122WEIGHTED_ROUND_ROBIN_Splitter_5127;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5098WEIGHTED_ROUND_ROBIN_Splitter_5107;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5038WEIGHTED_ROUND_ROBIN_Splitter_5055;
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_5219_5240_split[2];
buffer_float_t SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5018WEIGHTED_ROUND_ROBIN_Splitter_5021;
buffer_float_t SplitJoin20_CombineDFT_Fiss_5227_5248_join[2];
buffer_float_t SplitJoin14_CombineDFT_Fiss_5224_5245_join[16];
buffer_float_t SplitJoin108_CombineDFT_Fiss_5234_5255_split[8];
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5138WEIGHTED_ROUND_ROBIN_Splitter_5155;
buffer_float_t SplitJoin0_FFTTestSource_Fiss_5217_5238_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5022WEIGHTED_ROUND_ROBIN_Splitter_5027;
buffer_float_t SplitJoin104_CombineDFT_Fiss_5232_5253_split[22];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_4971_5007_5218_5239_join[2];
buffer_float_t SplitJoin14_CombineDFT_Fiss_5224_5245_split[16];
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_5219_5240_join[2];
buffer_float_t SplitJoin16_CombineDFT_Fiss_5225_5246_join[8];
buffer_float_t SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5014WEIGHTED_ROUND_ROBIN_Splitter_5005;
buffer_float_t SplitJoin96_FFTReorderSimple_Fiss_5228_5249_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5198WEIGHTED_ROUND_ROBIN_Splitter_5207;
buffer_float_t SplitJoin20_CombineDFT_Fiss_5227_5248_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5114CombineDFT_4992;
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_5220_5241_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5118WEIGHTED_ROUND_ROBIN_Splitter_5121;
buffer_float_t SplitJoin104_CombineDFT_Fiss_5232_5253_join[22];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5056WEIGHTED_ROUND_ROBIN_Splitter_5079;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5180WEIGHTED_ROUND_ROBIN_Splitter_5197;
buffer_float_t SplitJoin96_FFTReorderSimple_Fiss_5228_5249_join[2];
buffer_float_t SplitJoin98_FFTReorderSimple_Fiss_5229_5250_split[4];
buffer_float_t SplitJoin106_CombineDFT_Fiss_5233_5254_join[16];
buffer_float_t SplitJoin16_CombineDFT_Fiss_5225_5246_split[8];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_4971_5007_5218_5239_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5214CombineDFT_5003;
buffer_float_t SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[8];
buffer_float_t FFTReorderSimple_4982WEIGHTED_ROUND_ROBIN_Splitter_5017;
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[16];
buffer_float_t SplitJoin18_CombineDFT_Fiss_5226_5247_split[4];
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[8];
buffer_float_t SplitJoin110_CombineDFT_Fiss_5235_5256_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5028WEIGHTED_ROUND_ROBIN_Splitter_5037;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5156WEIGHTED_ROUND_ROBIN_Splitter_5179;
buffer_float_t SplitJoin112_CombineDFT_Fiss_5236_5257_join[2];


CombineDFT_5057_t CombineDFT_5057_s;
CombineDFT_5057_t CombineDFT_5058_s;
CombineDFT_5057_t CombineDFT_5059_s;
CombineDFT_5057_t CombineDFT_5060_s;
CombineDFT_5057_t CombineDFT_5061_s;
CombineDFT_5057_t CombineDFT_5062_s;
CombineDFT_5057_t CombineDFT_5063_s;
CombineDFT_5057_t CombineDFT_5064_s;
CombineDFT_5057_t CombineDFT_5065_s;
CombineDFT_5057_t CombineDFT_5066_s;
CombineDFT_5057_t CombineDFT_5067_s;
CombineDFT_5057_t CombineDFT_5068_s;
CombineDFT_5057_t CombineDFT_5069_s;
CombineDFT_5057_t CombineDFT_5070_s;
CombineDFT_5057_t CombineDFT_5071_s;
CombineDFT_5057_t CombineDFT_5072_s;
CombineDFT_5057_t CombineDFT_5073_s;
CombineDFT_5057_t CombineDFT_5074_s;
CombineDFT_5057_t CombineDFT_5075_s;
CombineDFT_5057_t CombineDFT_5076_s;
CombineDFT_5057_t CombineDFT_5077_s;
CombineDFT_5057_t CombineDFT_5078_s;
CombineDFT_5081_t CombineDFT_5081_s;
CombineDFT_5081_t CombineDFT_5082_s;
CombineDFT_5081_t CombineDFT_5083_s;
CombineDFT_5081_t CombineDFT_5084_s;
CombineDFT_5081_t CombineDFT_5085_s;
CombineDFT_5081_t CombineDFT_5086_s;
CombineDFT_5081_t CombineDFT_5087_s;
CombineDFT_5081_t CombineDFT_5088_s;
CombineDFT_5081_t CombineDFT_5089_s;
CombineDFT_5081_t CombineDFT_5090_s;
CombineDFT_5081_t CombineDFT_5091_s;
CombineDFT_5081_t CombineDFT_5092_s;
CombineDFT_5081_t CombineDFT_5093_s;
CombineDFT_5081_t CombineDFT_5094_s;
CombineDFT_5081_t CombineDFT_5095_s;
CombineDFT_5081_t CombineDFT_5096_s;
CombineDFT_5099_t CombineDFT_5099_s;
CombineDFT_5099_t CombineDFT_5100_s;
CombineDFT_5099_t CombineDFT_5101_s;
CombineDFT_5099_t CombineDFT_5102_s;
CombineDFT_5099_t CombineDFT_5103_s;
CombineDFT_5099_t CombineDFT_5104_s;
CombineDFT_5099_t CombineDFT_5105_s;
CombineDFT_5099_t CombineDFT_5106_s;
CombineDFT_5109_t CombineDFT_5109_s;
CombineDFT_5109_t CombineDFT_5110_s;
CombineDFT_5109_t CombineDFT_5111_s;
CombineDFT_5109_t CombineDFT_5112_s;
CombineDFT_5115_t CombineDFT_5115_s;
CombineDFT_5115_t CombineDFT_5116_s;
CombineDFT_4992_t CombineDFT_4992_s;
CombineDFT_5057_t CombineDFT_5157_s;
CombineDFT_5057_t CombineDFT_5158_s;
CombineDFT_5057_t CombineDFT_5159_s;
CombineDFT_5057_t CombineDFT_5160_s;
CombineDFT_5057_t CombineDFT_5161_s;
CombineDFT_5057_t CombineDFT_5162_s;
CombineDFT_5057_t CombineDFT_5163_s;
CombineDFT_5057_t CombineDFT_5164_s;
CombineDFT_5057_t CombineDFT_5165_s;
CombineDFT_5057_t CombineDFT_5166_s;
CombineDFT_5057_t CombineDFT_5167_s;
CombineDFT_5057_t CombineDFT_5168_s;
CombineDFT_5057_t CombineDFT_5169_s;
CombineDFT_5057_t CombineDFT_5170_s;
CombineDFT_5057_t CombineDFT_5171_s;
CombineDFT_5057_t CombineDFT_5172_s;
CombineDFT_5057_t CombineDFT_5173_s;
CombineDFT_5057_t CombineDFT_5174_s;
CombineDFT_5057_t CombineDFT_5175_s;
CombineDFT_5057_t CombineDFT_5176_s;
CombineDFT_5057_t CombineDFT_5177_s;
CombineDFT_5057_t CombineDFT_5178_s;
CombineDFT_5081_t CombineDFT_5181_s;
CombineDFT_5081_t CombineDFT_5182_s;
CombineDFT_5081_t CombineDFT_5183_s;
CombineDFT_5081_t CombineDFT_5184_s;
CombineDFT_5081_t CombineDFT_5185_s;
CombineDFT_5081_t CombineDFT_5186_s;
CombineDFT_5081_t CombineDFT_5187_s;
CombineDFT_5081_t CombineDFT_5188_s;
CombineDFT_5081_t CombineDFT_5189_s;
CombineDFT_5081_t CombineDFT_5190_s;
CombineDFT_5081_t CombineDFT_5191_s;
CombineDFT_5081_t CombineDFT_5192_s;
CombineDFT_5081_t CombineDFT_5193_s;
CombineDFT_5081_t CombineDFT_5194_s;
CombineDFT_5081_t CombineDFT_5195_s;
CombineDFT_5081_t CombineDFT_5196_s;
CombineDFT_5099_t CombineDFT_5199_s;
CombineDFT_5099_t CombineDFT_5200_s;
CombineDFT_5099_t CombineDFT_5201_s;
CombineDFT_5099_t CombineDFT_5202_s;
CombineDFT_5099_t CombineDFT_5203_s;
CombineDFT_5099_t CombineDFT_5204_s;
CombineDFT_5099_t CombineDFT_5205_s;
CombineDFT_5099_t CombineDFT_5206_s;
CombineDFT_5109_t CombineDFT_5209_s;
CombineDFT_5109_t CombineDFT_5210_s;
CombineDFT_5109_t CombineDFT_5211_s;
CombineDFT_5109_t CombineDFT_5212_s;
CombineDFT_5115_t CombineDFT_5215_s;
CombineDFT_5115_t CombineDFT_5216_s;
CombineDFT_4992_t CombineDFT_5003_s;

void FFTTestSource_5015(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		push_float(&SplitJoin0_FFTTestSource_Fiss_5217_5238_join[0], 0.0) ; 
		push_float(&SplitJoin0_FFTTestSource_Fiss_5217_5238_join[0], 0.0) ; 
		push_float(&SplitJoin0_FFTTestSource_Fiss_5217_5238_join[0], 1.0) ; 
		push_float(&SplitJoin0_FFTTestSource_Fiss_5217_5238_join[0], 0.0) ; 
		FOR(int, i, 0,  < , 124, i++) {
			push_float(&SplitJoin0_FFTTestSource_Fiss_5217_5238_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTTestSource_5016(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		push_float(&SplitJoin0_FFTTestSource_Fiss_5217_5238_join[1], 0.0) ; 
		push_float(&SplitJoin0_FFTTestSource_Fiss_5217_5238_join[1], 0.0) ; 
		push_float(&SplitJoin0_FFTTestSource_Fiss_5217_5238_join[1], 1.0) ; 
		push_float(&SplitJoin0_FFTTestSource_Fiss_5217_5238_join[1], 0.0) ; 
		FOR(int, i, 0,  < , 124, i++) {
			push_float(&SplitJoin0_FFTTestSource_Fiss_5217_5238_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5013() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_5014() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5014WEIGHTED_ROUND_ROBIN_Splitter_5005, pop_float(&SplitJoin0_FFTTestSource_Fiss_5217_5238_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5014WEIGHTED_ROUND_ROBIN_Splitter_5005, pop_float(&SplitJoin0_FFTTestSource_Fiss_5217_5238_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_4982(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 128, i = (i + 4)) {
			push_float(&FFTReorderSimple_4982WEIGHTED_ROUND_ROBIN_Splitter_5017, peek_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_4971_5007_5218_5239_split[0], i)) ; 
			push_float(&FFTReorderSimple_4982WEIGHTED_ROUND_ROBIN_Splitter_5017, peek_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_4971_5007_5218_5239_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 128, i = (i + 4)) {
			push_float(&FFTReorderSimple_4982WEIGHTED_ROUND_ROBIN_Splitter_5017, peek_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_4971_5007_5218_5239_split[0], i)) ; 
			push_float(&FFTReorderSimple_4982WEIGHTED_ROUND_ROBIN_Splitter_5017, peek_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_4971_5007_5218_5239_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_4971_5007_5218_5239_split[0]) ; 
			pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_4971_5007_5218_5239_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5019(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i = (i + 4)) {
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_5219_5240_join[0], peek_float(&SplitJoin4_FFTReorderSimple_Fiss_5219_5240_split[0], i)) ; 
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_5219_5240_join[0], peek_float(&SplitJoin4_FFTReorderSimple_Fiss_5219_5240_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 64, i = (i + 4)) {
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_5219_5240_join[0], peek_float(&SplitJoin4_FFTReorderSimple_Fiss_5219_5240_split[0], i)) ; 
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_5219_5240_join[0], peek_float(&SplitJoin4_FFTReorderSimple_Fiss_5219_5240_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&SplitJoin4_FFTReorderSimple_Fiss_5219_5240_split[0]) ; 
			pop_float(&SplitJoin4_FFTReorderSimple_Fiss_5219_5240_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5020(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i = (i + 4)) {
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_5219_5240_join[1], peek_float(&SplitJoin4_FFTReorderSimple_Fiss_5219_5240_split[1], i)) ; 
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_5219_5240_join[1], peek_float(&SplitJoin4_FFTReorderSimple_Fiss_5219_5240_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 64, i = (i + 4)) {
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_5219_5240_join[1], peek_float(&SplitJoin4_FFTReorderSimple_Fiss_5219_5240_split[1], i)) ; 
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_5219_5240_join[1], peek_float(&SplitJoin4_FFTReorderSimple_Fiss_5219_5240_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&SplitJoin4_FFTReorderSimple_Fiss_5219_5240_split[1]) ; 
			pop_float(&SplitJoin4_FFTReorderSimple_Fiss_5219_5240_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5017() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_5219_5240_split[0], pop_float(&FFTReorderSimple_4982WEIGHTED_ROUND_ROBIN_Splitter_5017));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_5219_5240_split[1], pop_float(&FFTReorderSimple_4982WEIGHTED_ROUND_ROBIN_Splitter_5017));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5018() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5018WEIGHTED_ROUND_ROBIN_Splitter_5021, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_5219_5240_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5018WEIGHTED_ROUND_ROBIN_Splitter_5021, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_5219_5240_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_5023(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_join[0], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_split[0], i)) ; 
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_join[0], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_join[0], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_split[0], i)) ; 
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_join[0], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_split[0]) ; 
			pop_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5024(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_join[1], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_split[1], i)) ; 
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_join[1], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_join[1], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_split[1], i)) ; 
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_join[1], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_split[1]) ; 
			pop_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5025(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_join[2], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_split[2], i)) ; 
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_join[2], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_split[2], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_join[2], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_split[2], i)) ; 
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_join[2], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_split[2], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_split[2]) ; 
			pop_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5026(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_join[3], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_split[3], i)) ; 
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_join[3], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_split[3], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_join[3], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_split[3], i)) ; 
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_join[3], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_split[3], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_split[3]) ; 
			pop_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5021() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5018WEIGHTED_ROUND_ROBIN_Splitter_5021));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5022() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5022WEIGHTED_ROUND_ROBIN_Splitter_5027, pop_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_5029(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[0], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[0], i)) ; 
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[0], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[0], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[0], i)) ; 
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[0], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[0]) ; 
			pop_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5030(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[1], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[1], i)) ; 
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[1], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[1], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[1], i)) ; 
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[1], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[1]) ; 
			pop_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5031(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[2], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[2], i)) ; 
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[2], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[2], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[2], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[2], i)) ; 
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[2], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[2], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[2]) ; 
			pop_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5032(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[3], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[3], i)) ; 
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[3], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[3], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[3], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[3], i)) ; 
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[3], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[3], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[3]) ; 
			pop_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5033(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[4], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[4], i)) ; 
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[4], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[4], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[4], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[4], i)) ; 
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[4], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[4], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[4]) ; 
			pop_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5034(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[5], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[5], i)) ; 
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[5], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[5], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[5], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[5], i)) ; 
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[5], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[5], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[5]) ; 
			pop_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5035(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[6], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[6], i)) ; 
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[6], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[6], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[6], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[6], i)) ; 
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[6], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[6], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[6]) ; 
			pop_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5036(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[7], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[7], i)) ; 
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[7], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[7], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[7], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[7], i)) ; 
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[7], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[7], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[7]) ; 
			pop_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5027() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5022WEIGHTED_ROUND_ROBIN_Splitter_5027));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5028() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5028WEIGHTED_ROUND_ROBIN_Splitter_5037, pop_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_5039(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[0], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[0], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[0], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[0], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[0], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[0], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[0]) ; 
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5040(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[1], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[1], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[1], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[1], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[1], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[1], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[1]) ; 
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5041(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[2], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[2], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[2], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[2], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[2], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[2], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[2], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[2], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[2]) ; 
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5042(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[3], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[3], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[3], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[3], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[3], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[3], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[3], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[3], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[3]) ; 
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5043(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[4], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[4], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[4], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[4], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[4], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[4], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[4], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[4], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[4]) ; 
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5044(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[5], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[5], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[5], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[5], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[5], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[5], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[5], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[5], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[5]) ; 
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5045(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[6], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[6], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[6], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[6], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[6], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[6], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[6], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[6], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[6]) ; 
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5046(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[7], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[7], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[7], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[7], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[7], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[7], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[7], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[7], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[7]) ; 
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5047(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[8], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[8], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[8], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[8], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[8], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[8], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[8], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[8], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[8]) ; 
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[8]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5048(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[9], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[9], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[9], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[9], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[9], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[9], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[9], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[9], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[9]) ; 
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[9]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5049(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[10], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[10], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[10], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[10], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[10], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[10], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[10], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[10], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[10]) ; 
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[10]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5050(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[11], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[11], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[11], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[11], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[11], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[11], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[11], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[11], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[11]) ; 
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[11]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5051(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[12], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[12], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[12], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[12], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[12], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[12], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[12], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[12], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[12]) ; 
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[12]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5052(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[13], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[13], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[13], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[13], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[13], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[13], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[13], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[13], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[13]) ; 
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[13]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5053(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[14], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[14], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[14], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[14], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[14], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[14], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[14], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[14], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[14]) ; 
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[14]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5054(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[15], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[15], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[15], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[15], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[15], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[15], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[15], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[15], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[15]) ; 
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[15]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5037() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5028WEIGHTED_ROUND_ROBIN_Splitter_5037));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5038() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5038WEIGHTED_ROUND_ROBIN_Splitter_5055, pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5057(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[0], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[0], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[0], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[0], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5057_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5057_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[0]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5058(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[1], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[1], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[1], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[1], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5058_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5058_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[1]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5059(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[2], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[2], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[2], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[2], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5059_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5059_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[2]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5060(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[3], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[3], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[3], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[3], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5060_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5060_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[3]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5061(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[4], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[4], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[4], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[4], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5061_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5061_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[4]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5062(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[5], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[5], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[5], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[5], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5062_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5062_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[5]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5063(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[6], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[6], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[6], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[6], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5063_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5063_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[6]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5064(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[7], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[7], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[7], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[7], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5064_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5064_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[7]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5065(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[8], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[8], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[8], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[8], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5065_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5065_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[8]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_join[8], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5066(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[9], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[9], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[9], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[9], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5066_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5066_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[9]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_join[9], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5067(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[10], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[10], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[10], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[10], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5067_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5067_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[10]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_join[10], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5068(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[11], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[11], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[11], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[11], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5068_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5068_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[11]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_join[11], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5069(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[12], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[12], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[12], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[12], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5069_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5069_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[12]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_join[12], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5070(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[13], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[13], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[13], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[13], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5070_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5070_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[13]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_join[13], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5071(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[14], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[14], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[14], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[14], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5071_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5071_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[14]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_join[14], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5072(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[15], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[15], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[15], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[15], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5072_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5072_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[15]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_join[15], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5073(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[16], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[16], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[16], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[16], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5073_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5073_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[16]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_join[16], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5074(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[17], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[17], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[17], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[17], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5074_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5074_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[17]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_join[17], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5075(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[18], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[18], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[18], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[18], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5075_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5075_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[18]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_join[18], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5076(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[19], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[19], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[19], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[19], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5076_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5076_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[19]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_join[19], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5077(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[20], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[20], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[20], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[20], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5077_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5077_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[20]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_join[20], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5078(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[21], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[21], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[21], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[21], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5078_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5078_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[21]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_join[21], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5055() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 22, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5038WEIGHTED_ROUND_ROBIN_Splitter_5055));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5056() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 22, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5056WEIGHTED_ROUND_ROBIN_Splitter_5079, pop_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5081(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[0], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[0], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[0], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[0], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5081_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5081_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[0]) ; 
			push_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5082(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[1], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[1], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[1], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[1], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5082_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5082_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[1]) ; 
			push_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5083(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[2], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[2], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[2], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[2], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5083_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5083_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[2]) ; 
			push_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5084(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[3], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[3], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[3], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[3], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5084_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5084_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[3]) ; 
			push_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5085(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[4], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[4], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[4], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[4], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5085_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5085_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[4]) ; 
			push_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5086(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[5], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[5], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[5], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[5], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5086_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5086_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[5]) ; 
			push_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5087(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[6], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[6], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[6], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[6], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5087_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5087_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[6]) ; 
			push_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5088(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[7], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[7], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[7], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[7], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5088_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5088_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[7]) ; 
			push_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5089(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[8], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[8], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[8], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[8], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5089_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5089_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[8]) ; 
			push_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_join[8], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5090(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[9], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[9], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[9], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[9], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5090_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5090_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[9]) ; 
			push_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_join[9], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5091(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[10], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[10], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[10], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[10], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5091_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5091_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[10]) ; 
			push_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_join[10], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5092(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[11], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[11], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[11], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[11], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5092_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5092_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[11]) ; 
			push_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_join[11], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5093(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[12], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[12], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[12], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[12], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5093_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5093_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[12]) ; 
			push_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_join[12], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5094(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[13], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[13], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[13], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[13], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5094_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5094_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[13]) ; 
			push_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_join[13], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5095(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[14], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[14], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[14], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[14], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5095_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5095_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[14]) ; 
			push_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_join[14], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5096(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[15], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[15], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[15], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[15], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5096_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5096_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[15]) ; 
			push_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_join[15], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5079() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5056WEIGHTED_ROUND_ROBIN_Splitter_5079));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5080() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5080WEIGHTED_ROUND_ROBIN_Splitter_5097, pop_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5099(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[16];
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[0], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[0], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[0], (8 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[0], (8 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5099_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5099_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(8 + i)] = (y0_r - y1w_r) ; 
			results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[0]) ; 
			push_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5100(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[16];
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[1], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[1], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[1], (8 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[1], (8 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5100_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5100_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(8 + i)] = (y0_r - y1w_r) ; 
			results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[1]) ; 
			push_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5101(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[16];
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[2], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[2], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[2], (8 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[2], (8 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5101_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5101_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(8 + i)] = (y0_r - y1w_r) ; 
			results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[2]) ; 
			push_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5102(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[16];
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[3], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[3], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[3], (8 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[3], (8 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5102_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5102_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(8 + i)] = (y0_r - y1w_r) ; 
			results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[3]) ; 
			push_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5103(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[16];
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[4], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[4], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[4], (8 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[4], (8 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5103_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5103_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(8 + i)] = (y0_r - y1w_r) ; 
			results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[4]) ; 
			push_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5104(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[16];
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[5], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[5], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[5], (8 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[5], (8 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5104_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5104_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(8 + i)] = (y0_r - y1w_r) ; 
			results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[5]) ; 
			push_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5105(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[16];
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[6], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[6], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[6], (8 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[6], (8 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5105_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5105_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(8 + i)] = (y0_r - y1w_r) ; 
			results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[6]) ; 
			push_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5106(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[16];
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[7], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[7], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[7], (8 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[7], (8 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5106_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5106_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(8 + i)] = (y0_r - y1w_r) ; 
			results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[7]) ; 
			push_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5097() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5080WEIGHTED_ROUND_ROBIN_Splitter_5097));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5098() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5098WEIGHTED_ROUND_ROBIN_Splitter_5107, pop_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5109(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[32];
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin18_CombineDFT_Fiss_5226_5247_split[0], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin18_CombineDFT_Fiss_5226_5247_split[0], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin18_CombineDFT_Fiss_5226_5247_split[0], (16 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin18_CombineDFT_Fiss_5226_5247_split[0], (16 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5109_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5109_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(16 + i)] = (y0_r - y1w_r) ; 
			results[((16 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&SplitJoin18_CombineDFT_Fiss_5226_5247_split[0]) ; 
			push_float(&SplitJoin18_CombineDFT_Fiss_5226_5247_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5110(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[32];
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin18_CombineDFT_Fiss_5226_5247_split[1], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin18_CombineDFT_Fiss_5226_5247_split[1], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin18_CombineDFT_Fiss_5226_5247_split[1], (16 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin18_CombineDFT_Fiss_5226_5247_split[1], (16 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5110_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5110_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(16 + i)] = (y0_r - y1w_r) ; 
			results[((16 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&SplitJoin18_CombineDFT_Fiss_5226_5247_split[1]) ; 
			push_float(&SplitJoin18_CombineDFT_Fiss_5226_5247_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5111(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[32];
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin18_CombineDFT_Fiss_5226_5247_split[2], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin18_CombineDFT_Fiss_5226_5247_split[2], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin18_CombineDFT_Fiss_5226_5247_split[2], (16 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin18_CombineDFT_Fiss_5226_5247_split[2], (16 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5111_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5111_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(16 + i)] = (y0_r - y1w_r) ; 
			results[((16 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&SplitJoin18_CombineDFT_Fiss_5226_5247_split[2]) ; 
			push_float(&SplitJoin18_CombineDFT_Fiss_5226_5247_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5112(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[32];
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin18_CombineDFT_Fiss_5226_5247_split[3], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin18_CombineDFT_Fiss_5226_5247_split[3], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin18_CombineDFT_Fiss_5226_5247_split[3], (16 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin18_CombineDFT_Fiss_5226_5247_split[3], (16 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5112_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5112_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(16 + i)] = (y0_r - y1w_r) ; 
			results[((16 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&SplitJoin18_CombineDFT_Fiss_5226_5247_split[3]) ; 
			push_float(&SplitJoin18_CombineDFT_Fiss_5226_5247_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5107() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin18_CombineDFT_Fiss_5226_5247_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5098WEIGHTED_ROUND_ROBIN_Splitter_5107));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5108() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5108WEIGHTED_ROUND_ROBIN_Splitter_5113, pop_float(&SplitJoin18_CombineDFT_Fiss_5226_5247_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5115(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[64];
		FOR(int, i, 0,  < , 32, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin20_CombineDFT_Fiss_5227_5248_split[0], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin20_CombineDFT_Fiss_5227_5248_split[0], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin20_CombineDFT_Fiss_5227_5248_split[0], (32 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin20_CombineDFT_Fiss_5227_5248_split[0], (32 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5115_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5115_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(32 + i)] = (y0_r - y1w_r) ; 
			results[((32 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_float(&SplitJoin20_CombineDFT_Fiss_5227_5248_split[0]) ; 
			push_float(&SplitJoin20_CombineDFT_Fiss_5227_5248_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5116(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[64];
		FOR(int, i, 0,  < , 32, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin20_CombineDFT_Fiss_5227_5248_split[1], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin20_CombineDFT_Fiss_5227_5248_split[1], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin20_CombineDFT_Fiss_5227_5248_split[1], (32 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin20_CombineDFT_Fiss_5227_5248_split[1], (32 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5116_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5116_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(32 + i)] = (y0_r - y1w_r) ; 
			results[((32 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_float(&SplitJoin20_CombineDFT_Fiss_5227_5248_split[1]) ; 
			push_float(&SplitJoin20_CombineDFT_Fiss_5227_5248_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5113() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_5227_5248_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5108WEIGHTED_ROUND_ROBIN_Splitter_5113));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_5227_5248_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5108WEIGHTED_ROUND_ROBIN_Splitter_5113));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5114() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5114CombineDFT_4992, pop_float(&SplitJoin20_CombineDFT_Fiss_5227_5248_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5114CombineDFT_4992, pop_float(&SplitJoin20_CombineDFT_Fiss_5227_5248_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_4992(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[128];
		FOR(int, i, 0,  < , 64, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_5114CombineDFT_4992, i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_5114CombineDFT_4992, i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_5114CombineDFT_4992, (64 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_5114CombineDFT_4992, (64 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_4992_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_4992_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(64 + i)] = (y0_r - y1w_r) ; 
			results[((64 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 128, i++) {
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5114CombineDFT_4992) ; 
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_4971_5007_5218_5239_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4993(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 128, i = (i + 4)) {
			push_float(&FFTReorderSimple_4993WEIGHTED_ROUND_ROBIN_Splitter_5117, peek_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_4971_5007_5218_5239_split[1], i)) ; 
			push_float(&FFTReorderSimple_4993WEIGHTED_ROUND_ROBIN_Splitter_5117, peek_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_4971_5007_5218_5239_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 128, i = (i + 4)) {
			push_float(&FFTReorderSimple_4993WEIGHTED_ROUND_ROBIN_Splitter_5117, peek_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_4971_5007_5218_5239_split[1], i)) ; 
			push_float(&FFTReorderSimple_4993WEIGHTED_ROUND_ROBIN_Splitter_5117, peek_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_4971_5007_5218_5239_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_4971_5007_5218_5239_split[1]) ; 
			pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_4971_5007_5218_5239_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5119(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i = (i + 4)) {
			push_float(&SplitJoin96_FFTReorderSimple_Fiss_5228_5249_join[0], peek_float(&SplitJoin96_FFTReorderSimple_Fiss_5228_5249_split[0], i)) ; 
			push_float(&SplitJoin96_FFTReorderSimple_Fiss_5228_5249_join[0], peek_float(&SplitJoin96_FFTReorderSimple_Fiss_5228_5249_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 64, i = (i + 4)) {
			push_float(&SplitJoin96_FFTReorderSimple_Fiss_5228_5249_join[0], peek_float(&SplitJoin96_FFTReorderSimple_Fiss_5228_5249_split[0], i)) ; 
			push_float(&SplitJoin96_FFTReorderSimple_Fiss_5228_5249_join[0], peek_float(&SplitJoin96_FFTReorderSimple_Fiss_5228_5249_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&SplitJoin96_FFTReorderSimple_Fiss_5228_5249_split[0]) ; 
			pop_float(&SplitJoin96_FFTReorderSimple_Fiss_5228_5249_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5120(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i = (i + 4)) {
			push_float(&SplitJoin96_FFTReorderSimple_Fiss_5228_5249_join[1], peek_float(&SplitJoin96_FFTReorderSimple_Fiss_5228_5249_split[1], i)) ; 
			push_float(&SplitJoin96_FFTReorderSimple_Fiss_5228_5249_join[1], peek_float(&SplitJoin96_FFTReorderSimple_Fiss_5228_5249_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 64, i = (i + 4)) {
			push_float(&SplitJoin96_FFTReorderSimple_Fiss_5228_5249_join[1], peek_float(&SplitJoin96_FFTReorderSimple_Fiss_5228_5249_split[1], i)) ; 
			push_float(&SplitJoin96_FFTReorderSimple_Fiss_5228_5249_join[1], peek_float(&SplitJoin96_FFTReorderSimple_Fiss_5228_5249_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&SplitJoin96_FFTReorderSimple_Fiss_5228_5249_split[1]) ; 
			pop_float(&SplitJoin96_FFTReorderSimple_Fiss_5228_5249_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5117() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin96_FFTReorderSimple_Fiss_5228_5249_split[0], pop_float(&FFTReorderSimple_4993WEIGHTED_ROUND_ROBIN_Splitter_5117));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin96_FFTReorderSimple_Fiss_5228_5249_split[1], pop_float(&FFTReorderSimple_4993WEIGHTED_ROUND_ROBIN_Splitter_5117));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5118() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5118WEIGHTED_ROUND_ROBIN_Splitter_5121, pop_float(&SplitJoin96_FFTReorderSimple_Fiss_5228_5249_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5118WEIGHTED_ROUND_ROBIN_Splitter_5121, pop_float(&SplitJoin96_FFTReorderSimple_Fiss_5228_5249_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_5123(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_join[0], peek_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_split[0], i)) ; 
			push_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_join[0], peek_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_join[0], peek_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_split[0], i)) ; 
			push_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_join[0], peek_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_split[0]) ; 
			pop_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5124(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_join[1], peek_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_split[1], i)) ; 
			push_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_join[1], peek_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_join[1], peek_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_split[1], i)) ; 
			push_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_join[1], peek_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_split[1]) ; 
			pop_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5125(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_join[2], peek_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_split[2], i)) ; 
			push_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_join[2], peek_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_split[2], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_join[2], peek_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_split[2], i)) ; 
			push_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_join[2], peek_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_split[2], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_split[2]) ; 
			pop_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5126(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_join[3], peek_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_split[3], i)) ; 
			push_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_join[3], peek_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_split[3], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_join[3], peek_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_split[3], i)) ; 
			push_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_join[3], peek_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_split[3], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_split[3]) ; 
			pop_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5121() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5118WEIGHTED_ROUND_ROBIN_Splitter_5121));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5122() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5122WEIGHTED_ROUND_ROBIN_Splitter_5127, pop_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_5129(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[0], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[0], i)) ; 
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[0], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[0], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[0], i)) ; 
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[0], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[0]) ; 
			pop_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5130(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[1], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[1], i)) ; 
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[1], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[1], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[1], i)) ; 
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[1], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[1]) ; 
			pop_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5131(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[2], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[2], i)) ; 
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[2], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[2], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[2], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[2], i)) ; 
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[2], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[2], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[2]) ; 
			pop_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5132(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[3], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[3], i)) ; 
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[3], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[3], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[3], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[3], i)) ; 
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[3], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[3], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[3]) ; 
			pop_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5133(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[4], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[4], i)) ; 
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[4], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[4], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[4], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[4], i)) ; 
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[4], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[4], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[4]) ; 
			pop_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5134(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[5], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[5], i)) ; 
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[5], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[5], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[5], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[5], i)) ; 
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[5], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[5], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[5]) ; 
			pop_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5135(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[6], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[6], i)) ; 
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[6], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[6], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[6], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[6], i)) ; 
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[6], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[6], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[6]) ; 
			pop_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5136(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[7], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[7], i)) ; 
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[7], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[7], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[7], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[7], i)) ; 
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[7], peek_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[7], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[7]) ; 
			pop_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5127() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5122WEIGHTED_ROUND_ROBIN_Splitter_5127));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5128() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5128WEIGHTED_ROUND_ROBIN_Splitter_5137, pop_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_5139(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[0], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[0], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[0], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[0], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[0], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[0], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[0]) ; 
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5140(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[1], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[1], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[1], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[1], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[1], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[1], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[1]) ; 
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5141(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[2], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[2], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[2], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[2], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[2], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[2], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[2], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[2], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[2]) ; 
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5142(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[3], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[3], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[3], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[3], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[3], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[3], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[3], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[3], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[3]) ; 
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5143(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[4], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[4], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[4], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[4], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[4], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[4], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[4], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[4], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[4]) ; 
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5144(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[5], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[5], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[5], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[5], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[5], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[5], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[5], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[5], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[5]) ; 
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5145(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[6], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[6], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[6], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[6], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[6], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[6], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[6], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[6], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[6]) ; 
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5146(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[7], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[7], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[7], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[7], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[7], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[7], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[7], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[7], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[7]) ; 
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5147(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[8], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[8], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[8], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[8], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[8], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[8], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[8], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[8], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[8]) ; 
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[8]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5148(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[9], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[9], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[9], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[9], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[9], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[9], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[9], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[9], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[9]) ; 
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[9]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5149(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[10], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[10], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[10], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[10], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[10], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[10], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[10], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[10], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[10]) ; 
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[10]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5150(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[11], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[11], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[11], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[11], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[11], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[11], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[11], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[11], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[11]) ; 
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[11]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5151(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[12], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[12], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[12], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[12], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[12], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[12], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[12], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[12], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[12]) ; 
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[12]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5152(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[13], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[13], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[13], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[13], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[13], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[13], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[13], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[13], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[13]) ; 
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[13]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5153(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[14], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[14], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[14], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[14], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[14], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[14], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[14], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[14], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[14]) ; 
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[14]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5154(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[15], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[15], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[15], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[15], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[15], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[15], i)) ; 
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[15], peek_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[15], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[15]) ; 
			pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[15]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5137() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5128WEIGHTED_ROUND_ROBIN_Splitter_5137));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5138() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5138WEIGHTED_ROUND_ROBIN_Splitter_5155, pop_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5157(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[0], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[0], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[0], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[0], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5157_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5157_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[0]) ; 
			push_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5158(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[1], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[1], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[1], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[1], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5158_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5158_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[1]) ; 
			push_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5159(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[2], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[2], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[2], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[2], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5159_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5159_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[2]) ; 
			push_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5160(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[3], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[3], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[3], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[3], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5160_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5160_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[3]) ; 
			push_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5161(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[4], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[4], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[4], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[4], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5161_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5161_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[4]) ; 
			push_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5162(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[5], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[5], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[5], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[5], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5162_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5162_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[5]) ; 
			push_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5163(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[6], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[6], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[6], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[6], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5163_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5163_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[6]) ; 
			push_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5164(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[7], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[7], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[7], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[7], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5164_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5164_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[7]) ; 
			push_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5165(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[8], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[8], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[8], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[8], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5165_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5165_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[8]) ; 
			push_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_join[8], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5166(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[9], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[9], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[9], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[9], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5166_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5166_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[9]) ; 
			push_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_join[9], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5167(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[10], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[10], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[10], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[10], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5167_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5167_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[10]) ; 
			push_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_join[10], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5168(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[11], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[11], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[11], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[11], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5168_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5168_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[11]) ; 
			push_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_join[11], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5169(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[12], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[12], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[12], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[12], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5169_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5169_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[12]) ; 
			push_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_join[12], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5170(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[13], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[13], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[13], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[13], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5170_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5170_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[13]) ; 
			push_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_join[13], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5171(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[14], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[14], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[14], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[14], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5171_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5171_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[14]) ; 
			push_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_join[14], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5172(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[15], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[15], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[15], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[15], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5172_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5172_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[15]) ; 
			push_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_join[15], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5173(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[16], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[16], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[16], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[16], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5173_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5173_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[16]) ; 
			push_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_join[16], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5174(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[17], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[17], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[17], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[17], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5174_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5174_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[17]) ; 
			push_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_join[17], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5175(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[18], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[18], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[18], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[18], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5175_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5175_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[18]) ; 
			push_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_join[18], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5176(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[19], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[19], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[19], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[19], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5176_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5176_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[19]) ; 
			push_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_join[19], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5177(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[20], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[20], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[20], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[20], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5177_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5177_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[20]) ; 
			push_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_join[20], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5178(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[21], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[21], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[21], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[21], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5178_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5178_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[21]) ; 
			push_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_join[21], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5155() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 22, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5138WEIGHTED_ROUND_ROBIN_Splitter_5155));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5156() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 22, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5156WEIGHTED_ROUND_ROBIN_Splitter_5179, pop_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5181(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[0], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[0], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[0], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[0], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5181_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5181_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[0]) ; 
			push_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5182(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[1], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[1], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[1], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[1], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5182_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5182_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[1]) ; 
			push_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5183(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[2], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[2], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[2], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[2], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5183_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5183_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[2]) ; 
			push_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5184(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[3], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[3], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[3], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[3], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5184_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5184_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[3]) ; 
			push_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5185(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[4], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[4], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[4], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[4], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5185_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5185_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[4]) ; 
			push_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5186(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[5], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[5], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[5], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[5], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5186_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5186_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[5]) ; 
			push_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5187(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[6], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[6], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[6], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[6], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5187_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5187_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[6]) ; 
			push_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5188(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[7], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[7], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[7], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[7], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5188_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5188_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[7]) ; 
			push_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5189(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[8], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[8], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[8], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[8], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5189_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5189_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[8]) ; 
			push_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_join[8], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5190(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[9], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[9], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[9], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[9], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5190_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5190_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[9]) ; 
			push_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_join[9], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5191(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[10], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[10], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[10], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[10], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5191_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5191_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[10]) ; 
			push_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_join[10], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5192(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[11], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[11], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[11], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[11], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5192_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5192_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[11]) ; 
			push_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_join[11], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5193(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[12], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[12], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[12], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[12], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5193_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5193_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[12]) ; 
			push_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_join[12], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5194(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[13], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[13], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[13], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[13], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5194_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5194_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[13]) ; 
			push_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_join[13], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5195(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[14], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[14], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[14], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[14], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5195_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5195_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[14]) ; 
			push_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_join[14], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5196(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[15], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[15], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[15], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[15], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5196_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5196_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[15]) ; 
			push_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_join[15], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5179() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5156WEIGHTED_ROUND_ROBIN_Splitter_5179));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5180() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5180WEIGHTED_ROUND_ROBIN_Splitter_5197, pop_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5199(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[16];
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[0], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[0], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[0], (8 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[0], (8 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5199_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5199_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(8 + i)] = (y0_r - y1w_r) ; 
			results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[0]) ; 
			push_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5200(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[16];
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[1], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[1], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[1], (8 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[1], (8 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5200_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5200_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(8 + i)] = (y0_r - y1w_r) ; 
			results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[1]) ; 
			push_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5201(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[16];
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[2], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[2], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[2], (8 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[2], (8 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5201_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5201_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(8 + i)] = (y0_r - y1w_r) ; 
			results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[2]) ; 
			push_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5202(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[16];
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[3], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[3], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[3], (8 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[3], (8 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5202_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5202_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(8 + i)] = (y0_r - y1w_r) ; 
			results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[3]) ; 
			push_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5203(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[16];
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[4], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[4], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[4], (8 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[4], (8 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5203_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5203_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(8 + i)] = (y0_r - y1w_r) ; 
			results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[4]) ; 
			push_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5204(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[16];
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[5], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[5], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[5], (8 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[5], (8 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5204_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5204_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(8 + i)] = (y0_r - y1w_r) ; 
			results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[5]) ; 
			push_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5205(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[16];
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[6], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[6], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[6], (8 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[6], (8 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5205_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5205_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(8 + i)] = (y0_r - y1w_r) ; 
			results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[6]) ; 
			push_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5206(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[16];
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[7], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[7], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[7], (8 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[7], (8 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5206_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5206_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(8 + i)] = (y0_r - y1w_r) ; 
			results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[7]) ; 
			push_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5197() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5180WEIGHTED_ROUND_ROBIN_Splitter_5197));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5198() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5198WEIGHTED_ROUND_ROBIN_Splitter_5207, pop_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5209(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[32];
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin110_CombineDFT_Fiss_5235_5256_split[0], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin110_CombineDFT_Fiss_5235_5256_split[0], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin110_CombineDFT_Fiss_5235_5256_split[0], (16 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin110_CombineDFT_Fiss_5235_5256_split[0], (16 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5209_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5209_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(16 + i)] = (y0_r - y1w_r) ; 
			results[((16 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&SplitJoin110_CombineDFT_Fiss_5235_5256_split[0]) ; 
			push_float(&SplitJoin110_CombineDFT_Fiss_5235_5256_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5210(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[32];
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin110_CombineDFT_Fiss_5235_5256_split[1], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin110_CombineDFT_Fiss_5235_5256_split[1], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin110_CombineDFT_Fiss_5235_5256_split[1], (16 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin110_CombineDFT_Fiss_5235_5256_split[1], (16 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5210_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5210_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(16 + i)] = (y0_r - y1w_r) ; 
			results[((16 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&SplitJoin110_CombineDFT_Fiss_5235_5256_split[1]) ; 
			push_float(&SplitJoin110_CombineDFT_Fiss_5235_5256_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5211(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[32];
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin110_CombineDFT_Fiss_5235_5256_split[2], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin110_CombineDFT_Fiss_5235_5256_split[2], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin110_CombineDFT_Fiss_5235_5256_split[2], (16 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin110_CombineDFT_Fiss_5235_5256_split[2], (16 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5211_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5211_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(16 + i)] = (y0_r - y1w_r) ; 
			results[((16 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&SplitJoin110_CombineDFT_Fiss_5235_5256_split[2]) ; 
			push_float(&SplitJoin110_CombineDFT_Fiss_5235_5256_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5212(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[32];
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin110_CombineDFT_Fiss_5235_5256_split[3], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin110_CombineDFT_Fiss_5235_5256_split[3], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin110_CombineDFT_Fiss_5235_5256_split[3], (16 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin110_CombineDFT_Fiss_5235_5256_split[3], (16 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5212_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5212_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(16 + i)] = (y0_r - y1w_r) ; 
			results[((16 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&SplitJoin110_CombineDFT_Fiss_5235_5256_split[3]) ; 
			push_float(&SplitJoin110_CombineDFT_Fiss_5235_5256_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5207() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin110_CombineDFT_Fiss_5235_5256_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5198WEIGHTED_ROUND_ROBIN_Splitter_5207));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5208() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5208WEIGHTED_ROUND_ROBIN_Splitter_5213, pop_float(&SplitJoin110_CombineDFT_Fiss_5235_5256_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5215(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[64];
		FOR(int, i, 0,  < , 32, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin112_CombineDFT_Fiss_5236_5257_split[0], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin112_CombineDFT_Fiss_5236_5257_split[0], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin112_CombineDFT_Fiss_5236_5257_split[0], (32 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin112_CombineDFT_Fiss_5236_5257_split[0], (32 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5215_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5215_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(32 + i)] = (y0_r - y1w_r) ; 
			results[((32 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_float(&SplitJoin112_CombineDFT_Fiss_5236_5257_split[0]) ; 
			push_float(&SplitJoin112_CombineDFT_Fiss_5236_5257_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5216(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[64];
		FOR(int, i, 0,  < , 32, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin112_CombineDFT_Fiss_5236_5257_split[1], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin112_CombineDFT_Fiss_5236_5257_split[1], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin112_CombineDFT_Fiss_5236_5257_split[1], (32 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin112_CombineDFT_Fiss_5236_5257_split[1], (32 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5216_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5216_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(32 + i)] = (y0_r - y1w_r) ; 
			results[((32 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_float(&SplitJoin112_CombineDFT_Fiss_5236_5257_split[1]) ; 
			push_float(&SplitJoin112_CombineDFT_Fiss_5236_5257_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5213() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin112_CombineDFT_Fiss_5236_5257_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5208WEIGHTED_ROUND_ROBIN_Splitter_5213));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin112_CombineDFT_Fiss_5236_5257_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5208WEIGHTED_ROUND_ROBIN_Splitter_5213));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5214() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5214CombineDFT_5003, pop_float(&SplitJoin112_CombineDFT_Fiss_5236_5257_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5214CombineDFT_5003, pop_float(&SplitJoin112_CombineDFT_Fiss_5236_5257_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_5003(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		float results[128];
		FOR(int, i, 0,  < , 64, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_5214CombineDFT_5003, i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_5214CombineDFT_5003, i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_5214CombineDFT_5003, (64 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_5214CombineDFT_5003, (64 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5003_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5003_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(64 + i)] = (y0_r - y1w_r) ; 
			results[((64 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 128, i++) {
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5214CombineDFT_5003) ; 
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_4971_5007_5218_5239_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5005() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_4971_5007_5218_5239_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5014WEIGHTED_ROUND_ROBIN_Splitter_5005));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_4971_5007_5218_5239_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5014WEIGHTED_ROUND_ROBIN_Splitter_5005));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5006() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5006FloatPrinter_5004, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_4971_5007_5218_5239_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5006FloatPrinter_5004, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_4971_5007_5218_5239_join[1]));
		ENDFOR
	ENDFOR
}}

void FloatPrinter_5004(){
	FOR(uint32_t, __iter_steady_, 0, <, 2816, __iter_steady_++) {
		printf("%.10f", pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5006FloatPrinter_5004));
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 4, __iter_init_0_++)
		init_buffer_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_join[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5208WEIGHTED_ROUND_ROBIN_Splitter_5213);
	init_buffer_float(&FFTReorderSimple_4993WEIGHTED_ROUND_ROBIN_Splitter_5117);
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_float(&SplitJoin112_CombineDFT_Fiss_5236_5257_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 16, __iter_init_3_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 4, __iter_init_4_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_5226_5247_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 2, __iter_init_5_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_5217_5238_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 4, __iter_init_6_++)
		init_buffer_float(&SplitJoin110_CombineDFT_Fiss_5235_5256_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 22, __iter_init_7_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_join[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5080WEIGHTED_ROUND_ROBIN_Splitter_5097);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5108WEIGHTED_ROUND_ROBIN_Splitter_5113);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5128WEIGHTED_ROUND_ROBIN_Splitter_5137);
	FOR(int, __iter_init_8_, 0, <, 16, __iter_init_8_++)
		init_buffer_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_split[__iter_init_8_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5006FloatPrinter_5004);
	FOR(int, __iter_init_9_, 0, <, 4, __iter_init_9_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 22, __iter_init_10_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_5223_5244_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 16, __iter_init_11_++)
		init_buffer_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_split[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5122WEIGHTED_ROUND_ROBIN_Splitter_5127);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5098WEIGHTED_ROUND_ROBIN_Splitter_5107);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5038WEIGHTED_ROUND_ROBIN_Splitter_5055);
	FOR(int, __iter_init_12_, 0, <, 2, __iter_init_12_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_5219_5240_split[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 8, __iter_init_13_++)
		init_buffer_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_join[__iter_init_13_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5018WEIGHTED_ROUND_ROBIN_Splitter_5021);
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_5227_5248_join[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 16, __iter_init_15_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_join[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 8, __iter_init_16_++)
		init_buffer_float(&SplitJoin108_CombineDFT_Fiss_5234_5255_split[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 8, __iter_init_17_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_join[__iter_init_17_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5138WEIGHTED_ROUND_ROBIN_Splitter_5155);
	FOR(int, __iter_init_18_, 0, <, 2, __iter_init_18_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_5217_5238_join[__iter_init_18_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5022WEIGHTED_ROUND_ROBIN_Splitter_5027);
	FOR(int, __iter_init_19_, 0, <, 22, __iter_init_19_++)
		init_buffer_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_split[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 2, __iter_init_20_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_4971_5007_5218_5239_join[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 16, __iter_init_21_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_5224_5245_split[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 2, __iter_init_22_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_5219_5240_join[__iter_init_22_]);
	ENDFOR
	FOR(int, __iter_init_23_, 0, <, 8, __iter_init_23_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_join[__iter_init_23_]);
	ENDFOR
	FOR(int, __iter_init_24_, 0, <, 16, __iter_init_24_++)
		init_buffer_float(&SplitJoin102_FFTReorderSimple_Fiss_5231_5252_join[__iter_init_24_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5014WEIGHTED_ROUND_ROBIN_Splitter_5005);
	FOR(int, __iter_init_25_, 0, <, 2, __iter_init_25_++)
		init_buffer_float(&SplitJoin96_FFTReorderSimple_Fiss_5228_5249_split[__iter_init_25_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5198WEIGHTED_ROUND_ROBIN_Splitter_5207);
	FOR(int, __iter_init_26_, 0, <, 2, __iter_init_26_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_5227_5248_split[__iter_init_26_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5114CombineDFT_4992);
	FOR(int, __iter_init_27_, 0, <, 4, __iter_init_27_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_5220_5241_join[__iter_init_27_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5118WEIGHTED_ROUND_ROBIN_Splitter_5121);
	FOR(int, __iter_init_28_, 0, <, 22, __iter_init_28_++)
		init_buffer_float(&SplitJoin104_CombineDFT_Fiss_5232_5253_join[__iter_init_28_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5056WEIGHTED_ROUND_ROBIN_Splitter_5079);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5180WEIGHTED_ROUND_ROBIN_Splitter_5197);
	FOR(int, __iter_init_29_, 0, <, 2, __iter_init_29_++)
		init_buffer_float(&SplitJoin96_FFTReorderSimple_Fiss_5228_5249_join[__iter_init_29_]);
	ENDFOR
	FOR(int, __iter_init_30_, 0, <, 4, __iter_init_30_++)
		init_buffer_float(&SplitJoin98_FFTReorderSimple_Fiss_5229_5250_split[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 16, __iter_init_31_++)
		init_buffer_float(&SplitJoin106_CombineDFT_Fiss_5233_5254_join[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 8, __iter_init_32_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_5225_5246_split[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 2, __iter_init_33_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_4971_5007_5218_5239_split[__iter_init_33_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5214CombineDFT_5003);
	FOR(int, __iter_init_34_, 0, <, 8, __iter_init_34_++)
		init_buffer_float(&SplitJoin100_FFTReorderSimple_Fiss_5230_5251_split[__iter_init_34_]);
	ENDFOR
	init_buffer_float(&FFTReorderSimple_4982WEIGHTED_ROUND_ROBIN_Splitter_5017);
	FOR(int, __iter_init_35_, 0, <, 16, __iter_init_35_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_5222_5243_join[__iter_init_35_]);
	ENDFOR
	FOR(int, __iter_init_36_, 0, <, 4, __iter_init_36_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_5226_5247_split[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 8, __iter_init_37_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_5221_5242_split[__iter_init_37_]);
	ENDFOR
	FOR(int, __iter_init_38_, 0, <, 4, __iter_init_38_++)
		init_buffer_float(&SplitJoin110_CombineDFT_Fiss_5235_5256_split[__iter_init_38_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5028WEIGHTED_ROUND_ROBIN_Splitter_5037);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5156WEIGHTED_ROUND_ROBIN_Splitter_5179);
	FOR(int, __iter_init_39_, 0, <, 2, __iter_init_39_++)
		init_buffer_float(&SplitJoin112_CombineDFT_Fiss_5236_5257_join[__iter_init_39_]);
	ENDFOR
// --- init: CombineDFT_5057
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5057_s.w[i] = real ; 
		CombineDFT_5057_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5058
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5058_s.w[i] = real ; 
		CombineDFT_5058_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5059
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5059_s.w[i] = real ; 
		CombineDFT_5059_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5060
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5060_s.w[i] = real ; 
		CombineDFT_5060_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5061
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5061_s.w[i] = real ; 
		CombineDFT_5061_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5062
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5062_s.w[i] = real ; 
		CombineDFT_5062_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5063
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5063_s.w[i] = real ; 
		CombineDFT_5063_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5064
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5064_s.w[i] = real ; 
		CombineDFT_5064_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5065
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5065_s.w[i] = real ; 
		CombineDFT_5065_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5066
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5066_s.w[i] = real ; 
		CombineDFT_5066_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5067
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5067_s.w[i] = real ; 
		CombineDFT_5067_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5068
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5068_s.w[i] = real ; 
		CombineDFT_5068_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5069
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5069_s.w[i] = real ; 
		CombineDFT_5069_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5070
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5070_s.w[i] = real ; 
		CombineDFT_5070_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5071
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5071_s.w[i] = real ; 
		CombineDFT_5071_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5072
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5072_s.w[i] = real ; 
		CombineDFT_5072_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5073
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5073_s.w[i] = real ; 
		CombineDFT_5073_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5074
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5074_s.w[i] = real ; 
		CombineDFT_5074_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5075
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5075_s.w[i] = real ; 
		CombineDFT_5075_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5076
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5076_s.w[i] = real ; 
		CombineDFT_5076_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5077
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5077_s.w[i] = real ; 
		CombineDFT_5077_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5078
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5078_s.w[i] = real ; 
		CombineDFT_5078_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5081
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5081_s.w[i] = real ; 
		CombineDFT_5081_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5082
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5082_s.w[i] = real ; 
		CombineDFT_5082_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5083
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5083_s.w[i] = real ; 
		CombineDFT_5083_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5084
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5084_s.w[i] = real ; 
		CombineDFT_5084_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5085
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5085_s.w[i] = real ; 
		CombineDFT_5085_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5086
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5086_s.w[i] = real ; 
		CombineDFT_5086_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5087
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5087_s.w[i] = real ; 
		CombineDFT_5087_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5088
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5088_s.w[i] = real ; 
		CombineDFT_5088_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5089
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5089_s.w[i] = real ; 
		CombineDFT_5089_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5090
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5090_s.w[i] = real ; 
		CombineDFT_5090_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5091
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5091_s.w[i] = real ; 
		CombineDFT_5091_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5092
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5092_s.w[i] = real ; 
		CombineDFT_5092_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5093
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5093_s.w[i] = real ; 
		CombineDFT_5093_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5094
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5094_s.w[i] = real ; 
		CombineDFT_5094_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5095
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5095_s.w[i] = real ; 
		CombineDFT_5095_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5096
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5096_s.w[i] = real ; 
		CombineDFT_5096_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5099
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5099_s.w[i] = real ; 
		CombineDFT_5099_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5100
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5100_s.w[i] = real ; 
		CombineDFT_5100_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5101
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5101_s.w[i] = real ; 
		CombineDFT_5101_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5102
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5102_s.w[i] = real ; 
		CombineDFT_5102_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5103
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5103_s.w[i] = real ; 
		CombineDFT_5103_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5104
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5104_s.w[i] = real ; 
		CombineDFT_5104_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5105
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5105_s.w[i] = real ; 
		CombineDFT_5105_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5106
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5106_s.w[i] = real ; 
		CombineDFT_5106_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5109
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_5109_s.w[i] = real ; 
		CombineDFT_5109_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5110
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_5110_s.w[i] = real ; 
		CombineDFT_5110_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5111
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_5111_s.w[i] = real ; 
		CombineDFT_5111_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5112
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_5112_s.w[i] = real ; 
		CombineDFT_5112_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5115
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_5115_s.w[i] = real ; 
		CombineDFT_5115_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5116
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_5116_s.w[i] = real ; 
		CombineDFT_5116_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_4992
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_4992_s.w[i] = real ; 
		CombineDFT_4992_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5157
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5157_s.w[i] = real ; 
		CombineDFT_5157_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5158
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5158_s.w[i] = real ; 
		CombineDFT_5158_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5159
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5159_s.w[i] = real ; 
		CombineDFT_5159_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5160
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5160_s.w[i] = real ; 
		CombineDFT_5160_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5161
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5161_s.w[i] = real ; 
		CombineDFT_5161_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5162
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5162_s.w[i] = real ; 
		CombineDFT_5162_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5163
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5163_s.w[i] = real ; 
		CombineDFT_5163_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5164
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5164_s.w[i] = real ; 
		CombineDFT_5164_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5165
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5165_s.w[i] = real ; 
		CombineDFT_5165_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5166
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5166_s.w[i] = real ; 
		CombineDFT_5166_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5167
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5167_s.w[i] = real ; 
		CombineDFT_5167_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5168
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5168_s.w[i] = real ; 
		CombineDFT_5168_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5169
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5169_s.w[i] = real ; 
		CombineDFT_5169_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5170
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5170_s.w[i] = real ; 
		CombineDFT_5170_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5171
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5171_s.w[i] = real ; 
		CombineDFT_5171_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5172
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5172_s.w[i] = real ; 
		CombineDFT_5172_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5173
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5173_s.w[i] = real ; 
		CombineDFT_5173_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5174
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5174_s.w[i] = real ; 
		CombineDFT_5174_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5175
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5175_s.w[i] = real ; 
		CombineDFT_5175_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5176
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5176_s.w[i] = real ; 
		CombineDFT_5176_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5177
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5177_s.w[i] = real ; 
		CombineDFT_5177_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5178
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5178_s.w[i] = real ; 
		CombineDFT_5178_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5181
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5181_s.w[i] = real ; 
		CombineDFT_5181_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5182
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5182_s.w[i] = real ; 
		CombineDFT_5182_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5183
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5183_s.w[i] = real ; 
		CombineDFT_5183_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5184
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5184_s.w[i] = real ; 
		CombineDFT_5184_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5185
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5185_s.w[i] = real ; 
		CombineDFT_5185_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5186
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5186_s.w[i] = real ; 
		CombineDFT_5186_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5187
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5187_s.w[i] = real ; 
		CombineDFT_5187_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5188
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5188_s.w[i] = real ; 
		CombineDFT_5188_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5189
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5189_s.w[i] = real ; 
		CombineDFT_5189_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5190
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5190_s.w[i] = real ; 
		CombineDFT_5190_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5191
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5191_s.w[i] = real ; 
		CombineDFT_5191_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5192
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5192_s.w[i] = real ; 
		CombineDFT_5192_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5193
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5193_s.w[i] = real ; 
		CombineDFT_5193_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5194
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5194_s.w[i] = real ; 
		CombineDFT_5194_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5195
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5195_s.w[i] = real ; 
		CombineDFT_5195_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5196
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5196_s.w[i] = real ; 
		CombineDFT_5196_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5199
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5199_s.w[i] = real ; 
		CombineDFT_5199_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5200
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5200_s.w[i] = real ; 
		CombineDFT_5200_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5201
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5201_s.w[i] = real ; 
		CombineDFT_5201_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5202
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5202_s.w[i] = real ; 
		CombineDFT_5202_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5203
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5203_s.w[i] = real ; 
		CombineDFT_5203_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5204
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5204_s.w[i] = real ; 
		CombineDFT_5204_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5205
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5205_s.w[i] = real ; 
		CombineDFT_5205_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5206
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5206_s.w[i] = real ; 
		CombineDFT_5206_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5209
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_5209_s.w[i] = real ; 
		CombineDFT_5209_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5210
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_5210_s.w[i] = real ; 
		CombineDFT_5210_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5211
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_5211_s.w[i] = real ; 
		CombineDFT_5211_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5212
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_5212_s.w[i] = real ; 
		CombineDFT_5212_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5215
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_5215_s.w[i] = real ; 
		CombineDFT_5215_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5216
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_5216_s.w[i] = real ; 
		CombineDFT_5216_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5003
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_5003_s.w[i] = real ; 
		CombineDFT_5003_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_5013();
			FFTTestSource_5015();
			FFTTestSource_5016();
		WEIGHTED_ROUND_ROBIN_Joiner_5014();
		WEIGHTED_ROUND_ROBIN_Splitter_5005();
			FFTReorderSimple_4982();
			WEIGHTED_ROUND_ROBIN_Splitter_5017();
				FFTReorderSimple_5019();
				FFTReorderSimple_5020();
			WEIGHTED_ROUND_ROBIN_Joiner_5018();
			WEIGHTED_ROUND_ROBIN_Splitter_5021();
				FFTReorderSimple_5023();
				FFTReorderSimple_5024();
				FFTReorderSimple_5025();
				FFTReorderSimple_5026();
			WEIGHTED_ROUND_ROBIN_Joiner_5022();
			WEIGHTED_ROUND_ROBIN_Splitter_5027();
				FFTReorderSimple_5029();
				FFTReorderSimple_5030();
				FFTReorderSimple_5031();
				FFTReorderSimple_5032();
				FFTReorderSimple_5033();
				FFTReorderSimple_5034();
				FFTReorderSimple_5035();
				FFTReorderSimple_5036();
			WEIGHTED_ROUND_ROBIN_Joiner_5028();
			WEIGHTED_ROUND_ROBIN_Splitter_5037();
				FFTReorderSimple_5039();
				FFTReorderSimple_5040();
				FFTReorderSimple_5041();
				FFTReorderSimple_5042();
				FFTReorderSimple_5043();
				FFTReorderSimple_5044();
				FFTReorderSimple_5045();
				FFTReorderSimple_5046();
				FFTReorderSimple_5047();
				FFTReorderSimple_5048();
				FFTReorderSimple_5049();
				FFTReorderSimple_5050();
				FFTReorderSimple_5051();
				FFTReorderSimple_5052();
				FFTReorderSimple_5053();
				FFTReorderSimple_5054();
			WEIGHTED_ROUND_ROBIN_Joiner_5038();
			WEIGHTED_ROUND_ROBIN_Splitter_5055();
				CombineDFT_5057();
				CombineDFT_5058();
				CombineDFT_5059();
				CombineDFT_5060();
				CombineDFT_5061();
				CombineDFT_5062();
				CombineDFT_5063();
				CombineDFT_5064();
				CombineDFT_5065();
				CombineDFT_5066();
				CombineDFT_5067();
				CombineDFT_5068();
				CombineDFT_5069();
				CombineDFT_5070();
				CombineDFT_5071();
				CombineDFT_5072();
				CombineDFT_5073();
				CombineDFT_5074();
				CombineDFT_5075();
				CombineDFT_5076();
				CombineDFT_5077();
				CombineDFT_5078();
			WEIGHTED_ROUND_ROBIN_Joiner_5056();
			WEIGHTED_ROUND_ROBIN_Splitter_5079();
				CombineDFT_5081();
				CombineDFT_5082();
				CombineDFT_5083();
				CombineDFT_5084();
				CombineDFT_5085();
				CombineDFT_5086();
				CombineDFT_5087();
				CombineDFT_5088();
				CombineDFT_5089();
				CombineDFT_5090();
				CombineDFT_5091();
				CombineDFT_5092();
				CombineDFT_5093();
				CombineDFT_5094();
				CombineDFT_5095();
				CombineDFT_5096();
			WEIGHTED_ROUND_ROBIN_Joiner_5080();
			WEIGHTED_ROUND_ROBIN_Splitter_5097();
				CombineDFT_5099();
				CombineDFT_5100();
				CombineDFT_5101();
				CombineDFT_5102();
				CombineDFT_5103();
				CombineDFT_5104();
				CombineDFT_5105();
				CombineDFT_5106();
			WEIGHTED_ROUND_ROBIN_Joiner_5098();
			WEIGHTED_ROUND_ROBIN_Splitter_5107();
				CombineDFT_5109();
				CombineDFT_5110();
				CombineDFT_5111();
				CombineDFT_5112();
			WEIGHTED_ROUND_ROBIN_Joiner_5108();
			WEIGHTED_ROUND_ROBIN_Splitter_5113();
				CombineDFT_5115();
				CombineDFT_5116();
			WEIGHTED_ROUND_ROBIN_Joiner_5114();
			CombineDFT_4992();
			FFTReorderSimple_4993();
			WEIGHTED_ROUND_ROBIN_Splitter_5117();
				FFTReorderSimple_5119();
				FFTReorderSimple_5120();
			WEIGHTED_ROUND_ROBIN_Joiner_5118();
			WEIGHTED_ROUND_ROBIN_Splitter_5121();
				FFTReorderSimple_5123();
				FFTReorderSimple_5124();
				FFTReorderSimple_5125();
				FFTReorderSimple_5126();
			WEIGHTED_ROUND_ROBIN_Joiner_5122();
			WEIGHTED_ROUND_ROBIN_Splitter_5127();
				FFTReorderSimple_5129();
				FFTReorderSimple_5130();
				FFTReorderSimple_5131();
				FFTReorderSimple_5132();
				FFTReorderSimple_5133();
				FFTReorderSimple_5134();
				FFTReorderSimple_5135();
				FFTReorderSimple_5136();
			WEIGHTED_ROUND_ROBIN_Joiner_5128();
			WEIGHTED_ROUND_ROBIN_Splitter_5137();
				FFTReorderSimple_5139();
				FFTReorderSimple_5140();
				FFTReorderSimple_5141();
				FFTReorderSimple_5142();
				FFTReorderSimple_5143();
				FFTReorderSimple_5144();
				FFTReorderSimple_5145();
				FFTReorderSimple_5146();
				FFTReorderSimple_5147();
				FFTReorderSimple_5148();
				FFTReorderSimple_5149();
				FFTReorderSimple_5150();
				FFTReorderSimple_5151();
				FFTReorderSimple_5152();
				FFTReorderSimple_5153();
				FFTReorderSimple_5154();
			WEIGHTED_ROUND_ROBIN_Joiner_5138();
			WEIGHTED_ROUND_ROBIN_Splitter_5155();
				CombineDFT_5157();
				CombineDFT_5158();
				CombineDFT_5159();
				CombineDFT_5160();
				CombineDFT_5161();
				CombineDFT_5162();
				CombineDFT_5163();
				CombineDFT_5164();
				CombineDFT_5165();
				CombineDFT_5166();
				CombineDFT_5167();
				CombineDFT_5168();
				CombineDFT_5169();
				CombineDFT_5170();
				CombineDFT_5171();
				CombineDFT_5172();
				CombineDFT_5173();
				CombineDFT_5174();
				CombineDFT_5175();
				CombineDFT_5176();
				CombineDFT_5177();
				CombineDFT_5178();
			WEIGHTED_ROUND_ROBIN_Joiner_5156();
			WEIGHTED_ROUND_ROBIN_Splitter_5179();
				CombineDFT_5181();
				CombineDFT_5182();
				CombineDFT_5183();
				CombineDFT_5184();
				CombineDFT_5185();
				CombineDFT_5186();
				CombineDFT_5187();
				CombineDFT_5188();
				CombineDFT_5189();
				CombineDFT_5190();
				CombineDFT_5191();
				CombineDFT_5192();
				CombineDFT_5193();
				CombineDFT_5194();
				CombineDFT_5195();
				CombineDFT_5196();
			WEIGHTED_ROUND_ROBIN_Joiner_5180();
			WEIGHTED_ROUND_ROBIN_Splitter_5197();
				CombineDFT_5199();
				CombineDFT_5200();
				CombineDFT_5201();
				CombineDFT_5202();
				CombineDFT_5203();
				CombineDFT_5204();
				CombineDFT_5205();
				CombineDFT_5206();
			WEIGHTED_ROUND_ROBIN_Joiner_5198();
			WEIGHTED_ROUND_ROBIN_Splitter_5207();
				CombineDFT_5209();
				CombineDFT_5210();
				CombineDFT_5211();
				CombineDFT_5212();
			WEIGHTED_ROUND_ROBIN_Joiner_5208();
			WEIGHTED_ROUND_ROBIN_Splitter_5213();
				CombineDFT_5215();
				CombineDFT_5216();
			WEIGHTED_ROUND_ROBIN_Joiner_5214();
			CombineDFT_5003();
		WEIGHTED_ROUND_ROBIN_Joiner_5006();
		FloatPrinter_5004();
	ENDFOR
	return EXIT_SUCCESS;
}
