#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=7680 on the compile command line
#else
#if BUF_SIZEMAX < 7680
#error BUF_SIZEMAX too small, it must be at least 7680
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float w[2];
} CombineDFT_8185_t;

typedef struct {
	float w[4];
} CombineDFT_8202_t;

typedef struct {
	float w[8];
} CombineDFT_8219_t;

typedef struct {
	float w[16];
} CombineDFT_8229_t;

typedef struct {
	float w[32];
} CombineDFT_8235_t;

typedef struct {
	float w[64];
} CombineDFT_8121_t;
void WEIGHTED_ROUND_ROBIN_Splitter_8142();
void FFTTestSource_8144();
void FFTTestSource_8145();
void WEIGHTED_ROUND_ROBIN_Joiner_8143();
void WEIGHTED_ROUND_ROBIN_Splitter_8134();
void FFTReorderSimple_8111();
void WEIGHTED_ROUND_ROBIN_Splitter_8146();
void FFTReorderSimple_8148();
void FFTReorderSimple_8149();
void WEIGHTED_ROUND_ROBIN_Joiner_8147();
void WEIGHTED_ROUND_ROBIN_Splitter_8150();
void FFTReorderSimple_8152();
void FFTReorderSimple_8153();
void FFTReorderSimple_8154();
void FFTReorderSimple_8155();
void WEIGHTED_ROUND_ROBIN_Joiner_8151();
void WEIGHTED_ROUND_ROBIN_Splitter_8156();
void FFTReorderSimple_8158();
void FFTReorderSimple_8159();
void FFTReorderSimple_8160();
void FFTReorderSimple_8161();
void FFTReorderSimple_8162();
void FFTReorderSimple_8163();
void FFTReorderSimple_8164();
void FFTReorderSimple_8165();
void WEIGHTED_ROUND_ROBIN_Joiner_8157();
void WEIGHTED_ROUND_ROBIN_Splitter_8166();
void FFTReorderSimple_8168();
void FFTReorderSimple_8169();
void FFTReorderSimple_8170();
void FFTReorderSimple_8171();
void FFTReorderSimple_8172();
void FFTReorderSimple_8173();
void FFTReorderSimple_8174();
void FFTReorderSimple_8175();
void FFTReorderSimple_8176();
void FFTReorderSimple_8177();
void FFTReorderSimple_8178();
void FFTReorderSimple_8179();
void FFTReorderSimple_8180();
void FFTReorderSimple_8181();
void FFTReorderSimple_8182();
void WEIGHTED_ROUND_ROBIN_Joiner_8167();
void WEIGHTED_ROUND_ROBIN_Splitter_8183();
void CombineDFT_8185();
void CombineDFT_8186();
void CombineDFT_8187();
void CombineDFT_8188();
void CombineDFT_8189();
void CombineDFT_8190();
void CombineDFT_8191();
void CombineDFT_8192();
void CombineDFT_8193();
void CombineDFT_8194();
void CombineDFT_8195();
void CombineDFT_8196();
void CombineDFT_8197();
void CombineDFT_8198();
void CombineDFT_8199();
void WEIGHTED_ROUND_ROBIN_Joiner_8184();
void WEIGHTED_ROUND_ROBIN_Splitter_8200();
void CombineDFT_8202();
void CombineDFT_8203();
void CombineDFT_8204();
void CombineDFT_8205();
void CombineDFT_8206();
void CombineDFT_8207();
void CombineDFT_8208();
void CombineDFT_8209();
void CombineDFT_8210();
void CombineDFT_8211();
void CombineDFT_8212();
void CombineDFT_8213();
void CombineDFT_8214();
void CombineDFT_8215();
void CombineDFT_8216();
void WEIGHTED_ROUND_ROBIN_Joiner_8201();
void WEIGHTED_ROUND_ROBIN_Splitter_8217();
void CombineDFT_8219();
void CombineDFT_8220();
void CombineDFT_8221();
void CombineDFT_8222();
void CombineDFT_8223();
void CombineDFT_8224();
void CombineDFT_8225();
void CombineDFT_8226();
void WEIGHTED_ROUND_ROBIN_Joiner_8218();
void WEIGHTED_ROUND_ROBIN_Splitter_8227();
void CombineDFT_8229();
void CombineDFT_8230();
void CombineDFT_8231();
void CombineDFT_8232();
void WEIGHTED_ROUND_ROBIN_Joiner_8228();
void WEIGHTED_ROUND_ROBIN_Splitter_8233();
void CombineDFT_8235();
void CombineDFT_8236();
void WEIGHTED_ROUND_ROBIN_Joiner_8234();
void CombineDFT_8121();
void FFTReorderSimple_8122();
void WEIGHTED_ROUND_ROBIN_Splitter_8237();
void FFTReorderSimple_8239();
void FFTReorderSimple_8240();
void WEIGHTED_ROUND_ROBIN_Joiner_8238();
void WEIGHTED_ROUND_ROBIN_Splitter_8241();
void FFTReorderSimple_8243();
void FFTReorderSimple_8244();
void FFTReorderSimple_8245();
void FFTReorderSimple_8246();
void WEIGHTED_ROUND_ROBIN_Joiner_8242();
void WEIGHTED_ROUND_ROBIN_Splitter_8247();
void FFTReorderSimple_8249();
void FFTReorderSimple_8250();
void FFTReorderSimple_8251();
void FFTReorderSimple_8252();
void FFTReorderSimple_8253();
void FFTReorderSimple_8254();
void FFTReorderSimple_8255();
void FFTReorderSimple_8256();
void WEIGHTED_ROUND_ROBIN_Joiner_8248();
void WEIGHTED_ROUND_ROBIN_Splitter_8257();
void FFTReorderSimple_8259();
void FFTReorderSimple_8260();
void FFTReorderSimple_8261();
void FFTReorderSimple_8262();
void FFTReorderSimple_8263();
void FFTReorderSimple_8264();
void FFTReorderSimple_8265();
void FFTReorderSimple_8266();
void FFTReorderSimple_8267();
void FFTReorderSimple_8268();
void FFTReorderSimple_8269();
void FFTReorderSimple_8270();
void FFTReorderSimple_8271();
void FFTReorderSimple_8272();
void FFTReorderSimple_8273();
void WEIGHTED_ROUND_ROBIN_Joiner_8258();
void WEIGHTED_ROUND_ROBIN_Splitter_8274();
void CombineDFT_8276();
void CombineDFT_8277();
void CombineDFT_8278();
void CombineDFT_8279();
void CombineDFT_8280();
void CombineDFT_8281();
void CombineDFT_8282();
void CombineDFT_8283();
void CombineDFT_8284();
void CombineDFT_8285();
void CombineDFT_8286();
void CombineDFT_8287();
void CombineDFT_8288();
void CombineDFT_8289();
void CombineDFT_8290();
void WEIGHTED_ROUND_ROBIN_Joiner_8275();
void WEIGHTED_ROUND_ROBIN_Splitter_8291();
void CombineDFT_8293();
void CombineDFT_8294();
void CombineDFT_8295();
void CombineDFT_8296();
void CombineDFT_8297();
void CombineDFT_8298();
void CombineDFT_8299();
void CombineDFT_8300();
void CombineDFT_8301();
void CombineDFT_8302();
void CombineDFT_8303();
void CombineDFT_8304();
void CombineDFT_8305();
void CombineDFT_8306();
void CombineDFT_8307();
void WEIGHTED_ROUND_ROBIN_Joiner_8292();
void WEIGHTED_ROUND_ROBIN_Splitter_8308();
void CombineDFT_8310();
void CombineDFT_8311();
void CombineDFT_8312();
void CombineDFT_8313();
void CombineDFT_8314();
void CombineDFT_8315();
void CombineDFT_8316();
void CombineDFT_8317();
void WEIGHTED_ROUND_ROBIN_Joiner_8309();
void WEIGHTED_ROUND_ROBIN_Splitter_8318();
void CombineDFT_8320();
void CombineDFT_8321();
void CombineDFT_8322();
void CombineDFT_8323();
void WEIGHTED_ROUND_ROBIN_Joiner_8319();
void WEIGHTED_ROUND_ROBIN_Splitter_8324();
void CombineDFT_8326();
void CombineDFT_8327();
void WEIGHTED_ROUND_ROBIN_Joiner_8325();
void CombineDFT_8132();
void WEIGHTED_ROUND_ROBIN_Joiner_8135();
void FloatPrinter_8133();

#ifdef __cplusplus
}
#endif
#endif
