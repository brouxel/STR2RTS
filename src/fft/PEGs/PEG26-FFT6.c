#include "PEG26-FFT6.h"

buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1568WEIGHTED_ROUND_ROBIN_Splitter_1585;
buffer_complex_t SplitJoin10_CombineDFT_Fiss_1610_1620_join[16];
buffer_complex_t SplitJoin8_CombineDFT_Fiss_1609_1619_join[26];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_1613_1623_split[2];
buffer_complex_t SplitJoin12_CombineDFT_Fiss_1611_1621_split[8];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1522WEIGHTED_ROUND_ROBIN_Splitter_1539;
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_1608_1618_join[16];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1540WEIGHTED_ROUND_ROBIN_Splitter_1567;
buffer_complex_t FFTTestSource_1487FFTReorderSimple_1488;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_1609_1619_split[26];
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_1606_1616_join[4];
buffer_complex_t CombineDFT_1498CPrinter_1499;
buffer_complex_t SplitJoin10_CombineDFT_Fiss_1610_1620_split[16];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1512WEIGHTED_ROUND_ROBIN_Splitter_1521;
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_1606_1616_split[4];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_1612_1622_join[4];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_1605_1615_split[2];
buffer_complex_t FFTReorderSimple_1488WEIGHTED_ROUND_ROBIN_Splitter_1501;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1602CombineDFT_1498;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1502WEIGHTED_ROUND_ROBIN_Splitter_1505;
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_1607_1617_split[8];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1506WEIGHTED_ROUND_ROBIN_Splitter_1511;
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_1608_1618_split[16];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_1605_1615_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1586WEIGHTED_ROUND_ROBIN_Splitter_1595;
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_1607_1617_join[8];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_1613_1623_join[2];
buffer_complex_t SplitJoin12_CombineDFT_Fiss_1611_1621_join[8];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_1612_1622_split[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1596WEIGHTED_ROUND_ROBIN_Splitter_1601;


CombineDFT_1541_t CombineDFT_1541_s;
CombineDFT_1541_t CombineDFT_1542_s;
CombineDFT_1541_t CombineDFT_1543_s;
CombineDFT_1541_t CombineDFT_1544_s;
CombineDFT_1541_t CombineDFT_1545_s;
CombineDFT_1541_t CombineDFT_1546_s;
CombineDFT_1541_t CombineDFT_1547_s;
CombineDFT_1541_t CombineDFT_1548_s;
CombineDFT_1541_t CombineDFT_1549_s;
CombineDFT_1541_t CombineDFT_1550_s;
CombineDFT_1541_t CombineDFT_1551_s;
CombineDFT_1541_t CombineDFT_1552_s;
CombineDFT_1541_t CombineDFT_1553_s;
CombineDFT_1541_t CombineDFT_1554_s;
CombineDFT_1541_t CombineDFT_1555_s;
CombineDFT_1541_t CombineDFT_1556_s;
CombineDFT_1541_t CombineDFT_1557_s;
CombineDFT_1541_t CombineDFT_1558_s;
CombineDFT_1541_t CombineDFT_1559_s;
CombineDFT_1541_t CombineDFT_1560_s;
CombineDFT_1541_t CombineDFT_1561_s;
CombineDFT_1541_t CombineDFT_1562_s;
CombineDFT_1541_t CombineDFT_1563_s;
CombineDFT_1541_t CombineDFT_1564_s;
CombineDFT_1541_t CombineDFT_1565_s;
CombineDFT_1541_t CombineDFT_1566_s;
CombineDFT_1541_t CombineDFT_1569_s;
CombineDFT_1541_t CombineDFT_1570_s;
CombineDFT_1541_t CombineDFT_1571_s;
CombineDFT_1541_t CombineDFT_1572_s;
CombineDFT_1541_t CombineDFT_1573_s;
CombineDFT_1541_t CombineDFT_1574_s;
CombineDFT_1541_t CombineDFT_1575_s;
CombineDFT_1541_t CombineDFT_1576_s;
CombineDFT_1541_t CombineDFT_1577_s;
CombineDFT_1541_t CombineDFT_1578_s;
CombineDFT_1541_t CombineDFT_1579_s;
CombineDFT_1541_t CombineDFT_1580_s;
CombineDFT_1541_t CombineDFT_1581_s;
CombineDFT_1541_t CombineDFT_1582_s;
CombineDFT_1541_t CombineDFT_1583_s;
CombineDFT_1541_t CombineDFT_1584_s;
CombineDFT_1541_t CombineDFT_1587_s;
CombineDFT_1541_t CombineDFT_1588_s;
CombineDFT_1541_t CombineDFT_1589_s;
CombineDFT_1541_t CombineDFT_1590_s;
CombineDFT_1541_t CombineDFT_1591_s;
CombineDFT_1541_t CombineDFT_1592_s;
CombineDFT_1541_t CombineDFT_1593_s;
CombineDFT_1541_t CombineDFT_1594_s;
CombineDFT_1541_t CombineDFT_1597_s;
CombineDFT_1541_t CombineDFT_1598_s;
CombineDFT_1541_t CombineDFT_1599_s;
CombineDFT_1541_t CombineDFT_1600_s;
CombineDFT_1541_t CombineDFT_1603_s;
CombineDFT_1541_t CombineDFT_1604_s;
CombineDFT_1541_t CombineDFT_1498_s;

void FFTTestSource(buffer_complex_t *chanout) {
		complex_t c1;
		complex_t zero;
		c1.real = 1.0 ; 
		c1.imag = 0.0 ; 
		zero.real = 0.0 ; 
		zero.imag = 0.0 ; 
		push_complex(&(*chanout), zero) ; 
		push_complex(&(*chanout), c1) ; 
		FOR(int, i, 0,  < , 62, i++) {
			push_complex(&(*chanout), zero) ; 
		}
		ENDFOR
	}


void FFTTestSource_1487() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTTestSource(&(FFTTestSource_1487FFTReorderSimple_1488));
	ENDFOR
}

void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 64, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&(*chanin)) ; 
		}
		ENDFOR
	}


void FFTReorderSimple_1488() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(FFTTestSource_1487FFTReorderSimple_1488), &(FFTReorderSimple_1488WEIGHTED_ROUND_ROBIN_Splitter_1501));
	ENDFOR
}

void FFTReorderSimple_1503() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_1605_1615_split[0]), &(SplitJoin0_FFTReorderSimple_Fiss_1605_1615_join[0]));
	ENDFOR
}

void FFTReorderSimple_1504() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_1605_1615_split[1]), &(SplitJoin0_FFTReorderSimple_Fiss_1605_1615_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1501() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_1605_1615_split[0], pop_complex(&FFTReorderSimple_1488WEIGHTED_ROUND_ROBIN_Splitter_1501));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_1605_1615_split[1], pop_complex(&FFTReorderSimple_1488WEIGHTED_ROUND_ROBIN_Splitter_1501));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1502() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1502WEIGHTED_ROUND_ROBIN_Splitter_1505, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_1605_1615_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1502WEIGHTED_ROUND_ROBIN_Splitter_1505, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_1605_1615_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_1507() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_1606_1616_split[0]), &(SplitJoin2_FFTReorderSimple_Fiss_1606_1616_join[0]));
	ENDFOR
}

void FFTReorderSimple_1508() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_1606_1616_split[1]), &(SplitJoin2_FFTReorderSimple_Fiss_1606_1616_join[1]));
	ENDFOR
}

void FFTReorderSimple_1509() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_1606_1616_split[2]), &(SplitJoin2_FFTReorderSimple_Fiss_1606_1616_join[2]));
	ENDFOR
}

void FFTReorderSimple_1510() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_1606_1616_split[3]), &(SplitJoin2_FFTReorderSimple_Fiss_1606_1616_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1505() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin2_FFTReorderSimple_Fiss_1606_1616_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1502WEIGHTED_ROUND_ROBIN_Splitter_1505));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1506() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1506WEIGHTED_ROUND_ROBIN_Splitter_1511, pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_1606_1616_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_1513() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_1607_1617_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_1607_1617_join[0]));
	ENDFOR
}

void FFTReorderSimple_1514() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_1607_1617_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_1607_1617_join[1]));
	ENDFOR
}

void FFTReorderSimple_1515() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_1607_1617_split[2]), &(SplitJoin4_FFTReorderSimple_Fiss_1607_1617_join[2]));
	ENDFOR
}

void FFTReorderSimple_1516() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_1607_1617_split[3]), &(SplitJoin4_FFTReorderSimple_Fiss_1607_1617_join[3]));
	ENDFOR
}

void FFTReorderSimple_1517() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_1607_1617_split[4]), &(SplitJoin4_FFTReorderSimple_Fiss_1607_1617_join[4]));
	ENDFOR
}

void FFTReorderSimple_1518() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_1607_1617_split[5]), &(SplitJoin4_FFTReorderSimple_Fiss_1607_1617_join[5]));
	ENDFOR
}

void FFTReorderSimple_1519() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_1607_1617_split[6]), &(SplitJoin4_FFTReorderSimple_Fiss_1607_1617_join[6]));
	ENDFOR
}

void FFTReorderSimple_1520() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_1607_1617_split[7]), &(SplitJoin4_FFTReorderSimple_Fiss_1607_1617_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1511() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin4_FFTReorderSimple_Fiss_1607_1617_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1506WEIGHTED_ROUND_ROBIN_Splitter_1511));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1512() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1512WEIGHTED_ROUND_ROBIN_Splitter_1521, pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_1607_1617_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_1523() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_join[0]));
	ENDFOR
}

void FFTReorderSimple_1524() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_join[1]));
	ENDFOR
}

void FFTReorderSimple_1525() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_join[2]));
	ENDFOR
}

void FFTReorderSimple_1526() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_join[3]));
	ENDFOR
}

void FFTReorderSimple_1527() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_split[4]), &(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_join[4]));
	ENDFOR
}

void FFTReorderSimple_1528() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_split[5]), &(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_join[5]));
	ENDFOR
}

void FFTReorderSimple_1529() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_split[6]), &(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_join[6]));
	ENDFOR
}

void FFTReorderSimple_1530() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_split[7]), &(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_join[7]));
	ENDFOR
}

void FFTReorderSimple_1531() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_split[8]), &(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_join[8]));
	ENDFOR
}

void FFTReorderSimple_1532() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_split[9]), &(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_join[9]));
	ENDFOR
}

void FFTReorderSimple_1533() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_split[10]), &(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_join[10]));
	ENDFOR
}

void FFTReorderSimple_1534() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_split[11]), &(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_join[11]));
	ENDFOR
}

void FFTReorderSimple_1535() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_split[12]), &(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_join[12]));
	ENDFOR
}

void FFTReorderSimple_1536() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_split[13]), &(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_join[13]));
	ENDFOR
}

void FFTReorderSimple_1537() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_split[14]), &(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_join[14]));
	ENDFOR
}

void FFTReorderSimple_1538() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_split[15]), &(SplitJoin6_FFTReorderSimple_Fiss_1608_1618_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1521() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1608_1618_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1512WEIGHTED_ROUND_ROBIN_Splitter_1521));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1522() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1522WEIGHTED_ROUND_ROBIN_Splitter_1539, pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_1608_1618_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&(*chanin), (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1541_s.wn.real) - (w.imag * CombineDFT_1541_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1541_s.wn.imag) + (w.imag * CombineDFT_1541_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&(*chanin)) ; 
			push_complex(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_1541() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1609_1619_split[0]), &(SplitJoin8_CombineDFT_Fiss_1609_1619_join[0]));
	ENDFOR
}

void CombineDFT_1542() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1609_1619_split[1]), &(SplitJoin8_CombineDFT_Fiss_1609_1619_join[1]));
	ENDFOR
}

void CombineDFT_1543() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1609_1619_split[2]), &(SplitJoin8_CombineDFT_Fiss_1609_1619_join[2]));
	ENDFOR
}

void CombineDFT_1544() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1609_1619_split[3]), &(SplitJoin8_CombineDFT_Fiss_1609_1619_join[3]));
	ENDFOR
}

void CombineDFT_1545() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1609_1619_split[4]), &(SplitJoin8_CombineDFT_Fiss_1609_1619_join[4]));
	ENDFOR
}

void CombineDFT_1546() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1609_1619_split[5]), &(SplitJoin8_CombineDFT_Fiss_1609_1619_join[5]));
	ENDFOR
}

void CombineDFT_1547() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1609_1619_split[6]), &(SplitJoin8_CombineDFT_Fiss_1609_1619_join[6]));
	ENDFOR
}

void CombineDFT_1548() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1609_1619_split[7]), &(SplitJoin8_CombineDFT_Fiss_1609_1619_join[7]));
	ENDFOR
}

void CombineDFT_1549() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1609_1619_split[8]), &(SplitJoin8_CombineDFT_Fiss_1609_1619_join[8]));
	ENDFOR
}

void CombineDFT_1550() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1609_1619_split[9]), &(SplitJoin8_CombineDFT_Fiss_1609_1619_join[9]));
	ENDFOR
}

void CombineDFT_1551() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1609_1619_split[10]), &(SplitJoin8_CombineDFT_Fiss_1609_1619_join[10]));
	ENDFOR
}

void CombineDFT_1552() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1609_1619_split[11]), &(SplitJoin8_CombineDFT_Fiss_1609_1619_join[11]));
	ENDFOR
}

void CombineDFT_1553() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1609_1619_split[12]), &(SplitJoin8_CombineDFT_Fiss_1609_1619_join[12]));
	ENDFOR
}

void CombineDFT_1554() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1609_1619_split[13]), &(SplitJoin8_CombineDFT_Fiss_1609_1619_join[13]));
	ENDFOR
}

void CombineDFT_1555() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1609_1619_split[14]), &(SplitJoin8_CombineDFT_Fiss_1609_1619_join[14]));
	ENDFOR
}

void CombineDFT_1556() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1609_1619_split[15]), &(SplitJoin8_CombineDFT_Fiss_1609_1619_join[15]));
	ENDFOR
}

void CombineDFT_1557() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1609_1619_split[16]), &(SplitJoin8_CombineDFT_Fiss_1609_1619_join[16]));
	ENDFOR
}

void CombineDFT_1558() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1609_1619_split[17]), &(SplitJoin8_CombineDFT_Fiss_1609_1619_join[17]));
	ENDFOR
}

void CombineDFT_1559() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1609_1619_split[18]), &(SplitJoin8_CombineDFT_Fiss_1609_1619_join[18]));
	ENDFOR
}

void CombineDFT_1560() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1609_1619_split[19]), &(SplitJoin8_CombineDFT_Fiss_1609_1619_join[19]));
	ENDFOR
}

void CombineDFT_1561() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1609_1619_split[20]), &(SplitJoin8_CombineDFT_Fiss_1609_1619_join[20]));
	ENDFOR
}

void CombineDFT_1562() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1609_1619_split[21]), &(SplitJoin8_CombineDFT_Fiss_1609_1619_join[21]));
	ENDFOR
}

void CombineDFT_1563() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1609_1619_split[22]), &(SplitJoin8_CombineDFT_Fiss_1609_1619_join[22]));
	ENDFOR
}

void CombineDFT_1564() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1609_1619_split[23]), &(SplitJoin8_CombineDFT_Fiss_1609_1619_join[23]));
	ENDFOR
}

void CombineDFT_1565() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1609_1619_split[24]), &(SplitJoin8_CombineDFT_Fiss_1609_1619_join[24]));
	ENDFOR
}

void CombineDFT_1566() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1609_1619_split[25]), &(SplitJoin8_CombineDFT_Fiss_1609_1619_join[25]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1539() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 26, __iter_++)
			push_complex(&SplitJoin8_CombineDFT_Fiss_1609_1619_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1522WEIGHTED_ROUND_ROBIN_Splitter_1539));
			push_complex(&SplitJoin8_CombineDFT_Fiss_1609_1619_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1522WEIGHTED_ROUND_ROBIN_Splitter_1539));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1540() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 26, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1540WEIGHTED_ROUND_ROBIN_Splitter_1567, pop_complex(&SplitJoin8_CombineDFT_Fiss_1609_1619_join[__iter_]));
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1540WEIGHTED_ROUND_ROBIN_Splitter_1567, pop_complex(&SplitJoin8_CombineDFT_Fiss_1609_1619_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_1569() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1610_1620_split[0]), &(SplitJoin10_CombineDFT_Fiss_1610_1620_join[0]));
	ENDFOR
}

void CombineDFT_1570() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1610_1620_split[1]), &(SplitJoin10_CombineDFT_Fiss_1610_1620_join[1]));
	ENDFOR
}

void CombineDFT_1571() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1610_1620_split[2]), &(SplitJoin10_CombineDFT_Fiss_1610_1620_join[2]));
	ENDFOR
}

void CombineDFT_1572() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1610_1620_split[3]), &(SplitJoin10_CombineDFT_Fiss_1610_1620_join[3]));
	ENDFOR
}

void CombineDFT_1573() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1610_1620_split[4]), &(SplitJoin10_CombineDFT_Fiss_1610_1620_join[4]));
	ENDFOR
}

void CombineDFT_1574() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1610_1620_split[5]), &(SplitJoin10_CombineDFT_Fiss_1610_1620_join[5]));
	ENDFOR
}

void CombineDFT_1575() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1610_1620_split[6]), &(SplitJoin10_CombineDFT_Fiss_1610_1620_join[6]));
	ENDFOR
}

void CombineDFT_1576() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1610_1620_split[7]), &(SplitJoin10_CombineDFT_Fiss_1610_1620_join[7]));
	ENDFOR
}

void CombineDFT_1577() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1610_1620_split[8]), &(SplitJoin10_CombineDFT_Fiss_1610_1620_join[8]));
	ENDFOR
}

void CombineDFT_1578() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1610_1620_split[9]), &(SplitJoin10_CombineDFT_Fiss_1610_1620_join[9]));
	ENDFOR
}

void CombineDFT_1579() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1610_1620_split[10]), &(SplitJoin10_CombineDFT_Fiss_1610_1620_join[10]));
	ENDFOR
}

void CombineDFT_1580() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1610_1620_split[11]), &(SplitJoin10_CombineDFT_Fiss_1610_1620_join[11]));
	ENDFOR
}

void CombineDFT_1581() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1610_1620_split[12]), &(SplitJoin10_CombineDFT_Fiss_1610_1620_join[12]));
	ENDFOR
}

void CombineDFT_1582() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1610_1620_split[13]), &(SplitJoin10_CombineDFT_Fiss_1610_1620_join[13]));
	ENDFOR
}

void CombineDFT_1583() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1610_1620_split[14]), &(SplitJoin10_CombineDFT_Fiss_1610_1620_join[14]));
	ENDFOR
}

void CombineDFT_1584() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1610_1620_split[15]), &(SplitJoin10_CombineDFT_Fiss_1610_1620_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1567() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin10_CombineDFT_Fiss_1610_1620_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1540WEIGHTED_ROUND_ROBIN_Splitter_1567));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1568() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1568WEIGHTED_ROUND_ROBIN_Splitter_1585, pop_complex(&SplitJoin10_CombineDFT_Fiss_1610_1620_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_1587() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1611_1621_split[0]), &(SplitJoin12_CombineDFT_Fiss_1611_1621_join[0]));
	ENDFOR
}

void CombineDFT_1588() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1611_1621_split[1]), &(SplitJoin12_CombineDFT_Fiss_1611_1621_join[1]));
	ENDFOR
}

void CombineDFT_1589() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1611_1621_split[2]), &(SplitJoin12_CombineDFT_Fiss_1611_1621_join[2]));
	ENDFOR
}

void CombineDFT_1590() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1611_1621_split[3]), &(SplitJoin12_CombineDFT_Fiss_1611_1621_join[3]));
	ENDFOR
}

void CombineDFT_1591() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1611_1621_split[4]), &(SplitJoin12_CombineDFT_Fiss_1611_1621_join[4]));
	ENDFOR
}

void CombineDFT_1592() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1611_1621_split[5]), &(SplitJoin12_CombineDFT_Fiss_1611_1621_join[5]));
	ENDFOR
}

void CombineDFT_1593() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1611_1621_split[6]), &(SplitJoin12_CombineDFT_Fiss_1611_1621_join[6]));
	ENDFOR
}

void CombineDFT_1594() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1611_1621_split[7]), &(SplitJoin12_CombineDFT_Fiss_1611_1621_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1585() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin12_CombineDFT_Fiss_1611_1621_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1568WEIGHTED_ROUND_ROBIN_Splitter_1585));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1586() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1586WEIGHTED_ROUND_ROBIN_Splitter_1595, pop_complex(&SplitJoin12_CombineDFT_Fiss_1611_1621_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_1597() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_1612_1622_split[0]), &(SplitJoin14_CombineDFT_Fiss_1612_1622_join[0]));
	ENDFOR
}

void CombineDFT_1598() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_1612_1622_split[1]), &(SplitJoin14_CombineDFT_Fiss_1612_1622_join[1]));
	ENDFOR
}

void CombineDFT_1599() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_1612_1622_split[2]), &(SplitJoin14_CombineDFT_Fiss_1612_1622_join[2]));
	ENDFOR
}

void CombineDFT_1600() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_1612_1622_split[3]), &(SplitJoin14_CombineDFT_Fiss_1612_1622_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1595() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin14_CombineDFT_Fiss_1612_1622_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1586WEIGHTED_ROUND_ROBIN_Splitter_1595));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1596() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1596WEIGHTED_ROUND_ROBIN_Splitter_1601, pop_complex(&SplitJoin14_CombineDFT_Fiss_1612_1622_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_1603() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_1613_1623_split[0]), &(SplitJoin16_CombineDFT_Fiss_1613_1623_join[0]));
	ENDFOR
}

void CombineDFT_1604() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_1613_1623_split[1]), &(SplitJoin16_CombineDFT_Fiss_1613_1623_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1601() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_1613_1623_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1596WEIGHTED_ROUND_ROBIN_Splitter_1601));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_1613_1623_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1596WEIGHTED_ROUND_ROBIN_Splitter_1601));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1602() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1602CombineDFT_1498, pop_complex(&SplitJoin16_CombineDFT_Fiss_1613_1623_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1602CombineDFT_1498, pop_complex(&SplitJoin16_CombineDFT_Fiss_1613_1623_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_1498() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_1602CombineDFT_1498), &(CombineDFT_1498CPrinter_1499));
	ENDFOR
}

void CPrinter(buffer_complex_t *chanin) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		printf("%.10f", c.real);
		printf("\n");
		printf("%.10f", c.imag);
		printf("\n");
	}


void CPrinter_1499() {
	FOR(uint32_t, __iter_steady_, 0, <, 832, __iter_steady_++)
		CPrinter(&(CombineDFT_1498CPrinter_1499));
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1568WEIGHTED_ROUND_ROBIN_Splitter_1585);
	FOR(int, __iter_init_0_, 0, <, 16, __iter_init_0_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_1610_1620_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 26, __iter_init_1_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_1609_1619_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_1613_1623_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_1611_1621_split[__iter_init_3_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1522WEIGHTED_ROUND_ROBIN_Splitter_1539);
	FOR(int, __iter_init_4_, 0, <, 16, __iter_init_4_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_1608_1618_join[__iter_init_4_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1540WEIGHTED_ROUND_ROBIN_Splitter_1567);
	init_buffer_complex(&FFTTestSource_1487FFTReorderSimple_1488);
	FOR(int, __iter_init_5_, 0, <, 26, __iter_init_5_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_1609_1619_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 4, __iter_init_6_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_1606_1616_join[__iter_init_6_]);
	ENDFOR
	init_buffer_complex(&CombineDFT_1498CPrinter_1499);
	FOR(int, __iter_init_7_, 0, <, 16, __iter_init_7_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_1610_1620_split[__iter_init_7_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1512WEIGHTED_ROUND_ROBIN_Splitter_1521);
	FOR(int, __iter_init_8_, 0, <, 4, __iter_init_8_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_1606_1616_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 4, __iter_init_9_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_1612_1622_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_1605_1615_split[__iter_init_10_]);
	ENDFOR
	init_buffer_complex(&FFTReorderSimple_1488WEIGHTED_ROUND_ROBIN_Splitter_1501);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1602CombineDFT_1498);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1502WEIGHTED_ROUND_ROBIN_Splitter_1505);
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_1607_1617_split[__iter_init_11_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1506WEIGHTED_ROUND_ROBIN_Splitter_1511);
	FOR(int, __iter_init_12_, 0, <, 16, __iter_init_12_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_1608_1618_split[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_1605_1615_join[__iter_init_13_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1586WEIGHTED_ROUND_ROBIN_Splitter_1595);
	FOR(int, __iter_init_14_, 0, <, 8, __iter_init_14_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_1607_1617_join[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 2, __iter_init_15_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_1613_1623_join[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 8, __iter_init_16_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_1611_1621_join[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 4, __iter_init_17_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_1612_1622_split[__iter_init_17_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1596WEIGHTED_ROUND_ROBIN_Splitter_1601);
// --- init: CombineDFT_1541
	 {
	 ; 
	CombineDFT_1541_s.wn.real = -1.0 ; 
	CombineDFT_1541_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1542
	 {
	CombineDFT_1542_s.wn.real = -1.0 ; 
	CombineDFT_1542_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1543
	 {
	CombineDFT_1543_s.wn.real = -1.0 ; 
	CombineDFT_1543_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1544
	 {
	CombineDFT_1544_s.wn.real = -1.0 ; 
	CombineDFT_1544_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1545
	 {
	CombineDFT_1545_s.wn.real = -1.0 ; 
	CombineDFT_1545_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1546
	 {
	CombineDFT_1546_s.wn.real = -1.0 ; 
	CombineDFT_1546_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1547
	 {
	CombineDFT_1547_s.wn.real = -1.0 ; 
	CombineDFT_1547_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1548
	 {
	CombineDFT_1548_s.wn.real = -1.0 ; 
	CombineDFT_1548_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1549
	 {
	CombineDFT_1549_s.wn.real = -1.0 ; 
	CombineDFT_1549_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1550
	 {
	CombineDFT_1550_s.wn.real = -1.0 ; 
	CombineDFT_1550_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1551
	 {
	CombineDFT_1551_s.wn.real = -1.0 ; 
	CombineDFT_1551_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1552
	 {
	CombineDFT_1552_s.wn.real = -1.0 ; 
	CombineDFT_1552_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1553
	 {
	CombineDFT_1553_s.wn.real = -1.0 ; 
	CombineDFT_1553_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1554
	 {
	CombineDFT_1554_s.wn.real = -1.0 ; 
	CombineDFT_1554_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1555
	 {
	CombineDFT_1555_s.wn.real = -1.0 ; 
	CombineDFT_1555_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1556
	 {
	CombineDFT_1556_s.wn.real = -1.0 ; 
	CombineDFT_1556_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1557
	 {
	CombineDFT_1557_s.wn.real = -1.0 ; 
	CombineDFT_1557_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1558
	 {
	CombineDFT_1558_s.wn.real = -1.0 ; 
	CombineDFT_1558_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1559
	 {
	CombineDFT_1559_s.wn.real = -1.0 ; 
	CombineDFT_1559_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1560
	 {
	CombineDFT_1560_s.wn.real = -1.0 ; 
	CombineDFT_1560_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1561
	 {
	CombineDFT_1561_s.wn.real = -1.0 ; 
	CombineDFT_1561_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1562
	 {
	CombineDFT_1562_s.wn.real = -1.0 ; 
	CombineDFT_1562_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1563
	 {
	CombineDFT_1563_s.wn.real = -1.0 ; 
	CombineDFT_1563_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1564
	 {
	CombineDFT_1564_s.wn.real = -1.0 ; 
	CombineDFT_1564_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1565
	 {
	CombineDFT_1565_s.wn.real = -1.0 ; 
	CombineDFT_1565_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1566
	 {
	CombineDFT_1566_s.wn.real = -1.0 ; 
	CombineDFT_1566_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1569
	 {
	CombineDFT_1569_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1569_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1570
	 {
	CombineDFT_1570_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1570_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1571
	 {
	CombineDFT_1571_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1571_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1572
	 {
	CombineDFT_1572_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1572_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1573
	 {
	CombineDFT_1573_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1573_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1574
	 {
	CombineDFT_1574_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1574_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1575
	 {
	CombineDFT_1575_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1575_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1576
	 {
	CombineDFT_1576_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1576_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1577
	 {
	CombineDFT_1577_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1577_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1578
	 {
	CombineDFT_1578_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1578_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1579
	 {
	CombineDFT_1579_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1579_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1580
	 {
	CombineDFT_1580_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1580_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1581
	 {
	CombineDFT_1581_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1581_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1582
	 {
	CombineDFT_1582_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1582_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1583
	 {
	CombineDFT_1583_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1583_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1584
	 {
	CombineDFT_1584_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1584_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1587
	 {
	CombineDFT_1587_s.wn.real = 0.70710677 ; 
	CombineDFT_1587_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1588
	 {
	CombineDFT_1588_s.wn.real = 0.70710677 ; 
	CombineDFT_1588_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1589
	 {
	CombineDFT_1589_s.wn.real = 0.70710677 ; 
	CombineDFT_1589_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1590
	 {
	CombineDFT_1590_s.wn.real = 0.70710677 ; 
	CombineDFT_1590_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1591
	 {
	CombineDFT_1591_s.wn.real = 0.70710677 ; 
	CombineDFT_1591_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1592
	 {
	CombineDFT_1592_s.wn.real = 0.70710677 ; 
	CombineDFT_1592_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1593
	 {
	CombineDFT_1593_s.wn.real = 0.70710677 ; 
	CombineDFT_1593_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1594
	 {
	CombineDFT_1594_s.wn.real = 0.70710677 ; 
	CombineDFT_1594_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1597
	 {
	CombineDFT_1597_s.wn.real = 0.9238795 ; 
	CombineDFT_1597_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_1598
	 {
	CombineDFT_1598_s.wn.real = 0.9238795 ; 
	CombineDFT_1598_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_1599
	 {
	CombineDFT_1599_s.wn.real = 0.9238795 ; 
	CombineDFT_1599_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_1600
	 {
	CombineDFT_1600_s.wn.real = 0.9238795 ; 
	CombineDFT_1600_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_1603
	 {
	CombineDFT_1603_s.wn.real = 0.98078525 ; 
	CombineDFT_1603_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_1604
	 {
	CombineDFT_1604_s.wn.real = 0.98078525 ; 
	CombineDFT_1604_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_1498
	 {
	 ; 
	CombineDFT_1498_s.wn.real = 0.9951847 ; 
	CombineDFT_1498_s.wn.imag = -0.09801714 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FFTTestSource_1487();
		FFTReorderSimple_1488();
		WEIGHTED_ROUND_ROBIN_Splitter_1501();
			FFTReorderSimple_1503();
			FFTReorderSimple_1504();
		WEIGHTED_ROUND_ROBIN_Joiner_1502();
		WEIGHTED_ROUND_ROBIN_Splitter_1505();
			FFTReorderSimple_1507();
			FFTReorderSimple_1508();
			FFTReorderSimple_1509();
			FFTReorderSimple_1510();
		WEIGHTED_ROUND_ROBIN_Joiner_1506();
		WEIGHTED_ROUND_ROBIN_Splitter_1511();
			FFTReorderSimple_1513();
			FFTReorderSimple_1514();
			FFTReorderSimple_1515();
			FFTReorderSimple_1516();
			FFTReorderSimple_1517();
			FFTReorderSimple_1518();
			FFTReorderSimple_1519();
			FFTReorderSimple_1520();
		WEIGHTED_ROUND_ROBIN_Joiner_1512();
		WEIGHTED_ROUND_ROBIN_Splitter_1521();
			FFTReorderSimple_1523();
			FFTReorderSimple_1524();
			FFTReorderSimple_1525();
			FFTReorderSimple_1526();
			FFTReorderSimple_1527();
			FFTReorderSimple_1528();
			FFTReorderSimple_1529();
			FFTReorderSimple_1530();
			FFTReorderSimple_1531();
			FFTReorderSimple_1532();
			FFTReorderSimple_1533();
			FFTReorderSimple_1534();
			FFTReorderSimple_1535();
			FFTReorderSimple_1536();
			FFTReorderSimple_1537();
			FFTReorderSimple_1538();
		WEIGHTED_ROUND_ROBIN_Joiner_1522();
		WEIGHTED_ROUND_ROBIN_Splitter_1539();
			CombineDFT_1541();
			CombineDFT_1542();
			CombineDFT_1543();
			CombineDFT_1544();
			CombineDFT_1545();
			CombineDFT_1546();
			CombineDFT_1547();
			CombineDFT_1548();
			CombineDFT_1549();
			CombineDFT_1550();
			CombineDFT_1551();
			CombineDFT_1552();
			CombineDFT_1553();
			CombineDFT_1554();
			CombineDFT_1555();
			CombineDFT_1556();
			CombineDFT_1557();
			CombineDFT_1558();
			CombineDFT_1559();
			CombineDFT_1560();
			CombineDFT_1561();
			CombineDFT_1562();
			CombineDFT_1563();
			CombineDFT_1564();
			CombineDFT_1565();
			CombineDFT_1566();
		WEIGHTED_ROUND_ROBIN_Joiner_1540();
		WEIGHTED_ROUND_ROBIN_Splitter_1567();
			CombineDFT_1569();
			CombineDFT_1570();
			CombineDFT_1571();
			CombineDFT_1572();
			CombineDFT_1573();
			CombineDFT_1574();
			CombineDFT_1575();
			CombineDFT_1576();
			CombineDFT_1577();
			CombineDFT_1578();
			CombineDFT_1579();
			CombineDFT_1580();
			CombineDFT_1581();
			CombineDFT_1582();
			CombineDFT_1583();
			CombineDFT_1584();
		WEIGHTED_ROUND_ROBIN_Joiner_1568();
		WEIGHTED_ROUND_ROBIN_Splitter_1585();
			CombineDFT_1587();
			CombineDFT_1588();
			CombineDFT_1589();
			CombineDFT_1590();
			CombineDFT_1591();
			CombineDFT_1592();
			CombineDFT_1593();
			CombineDFT_1594();
		WEIGHTED_ROUND_ROBIN_Joiner_1586();
		WEIGHTED_ROUND_ROBIN_Splitter_1595();
			CombineDFT_1597();
			CombineDFT_1598();
			CombineDFT_1599();
			CombineDFT_1600();
		WEIGHTED_ROUND_ROBIN_Joiner_1596();
		WEIGHTED_ROUND_ROBIN_Splitter_1601();
			CombineDFT_1603();
			CombineDFT_1604();
		WEIGHTED_ROUND_ROBIN_Joiner_1602();
		CombineDFT_1498();
		CPrinter_1499();
	ENDFOR
	return EXIT_SUCCESS;
}
