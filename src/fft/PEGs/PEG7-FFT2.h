#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=3584 on the compile command line
#else
#if BUF_SIZEMAX < 3584
#error BUF_SIZEMAX too small, it must be at least 3584
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float w[2];
} CombineDFT_11224_t;

typedef struct {
	float w[4];
} CombineDFT_11233_t;

typedef struct {
	float w[8];
} CombineDFT_11242_t;

typedef struct {
	float w[16];
} CombineDFT_11251_t;

typedef struct {
	float w[32];
} CombineDFT_11257_t;

typedef struct {
	float w[64];
} CombineDFT_11169_t;
void WEIGHTED_ROUND_ROBIN_Splitter_11190();
void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout);
void FFTTestSource_11192();
void FFTTestSource_11193();
void WEIGHTED_ROUND_ROBIN_Joiner_11191();
void WEIGHTED_ROUND_ROBIN_Splitter_11182();
void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout);
void FFTReorderSimple_11159();
void WEIGHTED_ROUND_ROBIN_Splitter_11194();
void FFTReorderSimple_11196();
void FFTReorderSimple_11197();
void WEIGHTED_ROUND_ROBIN_Joiner_11195();
void WEIGHTED_ROUND_ROBIN_Splitter_11198();
void FFTReorderSimple_11200();
void FFTReorderSimple_11201();
void FFTReorderSimple_11202();
void FFTReorderSimple_11203();
void WEIGHTED_ROUND_ROBIN_Joiner_11199();
void WEIGHTED_ROUND_ROBIN_Splitter_11204();
void FFTReorderSimple_11206();
void FFTReorderSimple_11207();
void FFTReorderSimple_11208();
void FFTReorderSimple_11209();
void FFTReorderSimple_11210();
void FFTReorderSimple_11211();
void FFTReorderSimple_11212();
void WEIGHTED_ROUND_ROBIN_Joiner_11205();
void WEIGHTED_ROUND_ROBIN_Splitter_11213();
void FFTReorderSimple_11215();
void FFTReorderSimple_11216();
void FFTReorderSimple_11217();
void FFTReorderSimple_11218();
void FFTReorderSimple_11219();
void FFTReorderSimple_11220();
void FFTReorderSimple_11221();
void WEIGHTED_ROUND_ROBIN_Joiner_11214();
void WEIGHTED_ROUND_ROBIN_Splitter_11222();
void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout);
void CombineDFT_11224();
void CombineDFT_11225();
void CombineDFT_11226();
void CombineDFT_11227();
void CombineDFT_11228();
void CombineDFT_11229();
void CombineDFT_11230();
void WEIGHTED_ROUND_ROBIN_Joiner_11223();
void WEIGHTED_ROUND_ROBIN_Splitter_11231();
void CombineDFT_11233();
void CombineDFT_11234();
void CombineDFT_11235();
void CombineDFT_11236();
void CombineDFT_11237();
void CombineDFT_11238();
void CombineDFT_11239();
void WEIGHTED_ROUND_ROBIN_Joiner_11232();
void WEIGHTED_ROUND_ROBIN_Splitter_11240();
void CombineDFT_11242();
void CombineDFT_11243();
void CombineDFT_11244();
void CombineDFT_11245();
void CombineDFT_11246();
void CombineDFT_11247();
void CombineDFT_11248();
void WEIGHTED_ROUND_ROBIN_Joiner_11241();
void WEIGHTED_ROUND_ROBIN_Splitter_11249();
void CombineDFT_11251();
void CombineDFT_11252();
void CombineDFT_11253();
void CombineDFT_11254();
void WEIGHTED_ROUND_ROBIN_Joiner_11250();
void WEIGHTED_ROUND_ROBIN_Splitter_11255();
void CombineDFT_11257();
void CombineDFT_11258();
void WEIGHTED_ROUND_ROBIN_Joiner_11256();
void CombineDFT_11169();
void FFTReorderSimple_11170();
void WEIGHTED_ROUND_ROBIN_Splitter_11259();
void FFTReorderSimple_11261();
void FFTReorderSimple_11262();
void WEIGHTED_ROUND_ROBIN_Joiner_11260();
void WEIGHTED_ROUND_ROBIN_Splitter_11263();
void FFTReorderSimple_11265();
void FFTReorderSimple_11266();
void FFTReorderSimple_11267();
void FFTReorderSimple_11268();
void WEIGHTED_ROUND_ROBIN_Joiner_11264();
void WEIGHTED_ROUND_ROBIN_Splitter_11269();
void FFTReorderSimple_11271();
void FFTReorderSimple_11272();
void FFTReorderSimple_11273();
void FFTReorderSimple_11274();
void FFTReorderSimple_11275();
void FFTReorderSimple_11276();
void FFTReorderSimple_11277();
void WEIGHTED_ROUND_ROBIN_Joiner_11270();
void WEIGHTED_ROUND_ROBIN_Splitter_11278();
void FFTReorderSimple_11280();
void FFTReorderSimple_11281();
void FFTReorderSimple_11282();
void FFTReorderSimple_11283();
void FFTReorderSimple_11284();
void FFTReorderSimple_11285();
void FFTReorderSimple_11286();
void WEIGHTED_ROUND_ROBIN_Joiner_11279();
void WEIGHTED_ROUND_ROBIN_Splitter_11287();
void CombineDFT_11289();
void CombineDFT_11290();
void CombineDFT_11291();
void CombineDFT_11292();
void CombineDFT_11293();
void CombineDFT_11294();
void CombineDFT_11295();
void WEIGHTED_ROUND_ROBIN_Joiner_11288();
void WEIGHTED_ROUND_ROBIN_Splitter_11296();
void CombineDFT_11298();
void CombineDFT_11299();
void CombineDFT_11300();
void CombineDFT_11301();
void CombineDFT_11302();
void CombineDFT_11303();
void CombineDFT_11304();
void WEIGHTED_ROUND_ROBIN_Joiner_11297();
void WEIGHTED_ROUND_ROBIN_Splitter_11305();
void CombineDFT_11307();
void CombineDFT_11308();
void CombineDFT_11309();
void CombineDFT_11310();
void CombineDFT_11311();
void CombineDFT_11312();
void CombineDFT_11313();
void WEIGHTED_ROUND_ROBIN_Joiner_11306();
void WEIGHTED_ROUND_ROBIN_Splitter_11314();
void CombineDFT_11316();
void CombineDFT_11317();
void CombineDFT_11318();
void CombineDFT_11319();
void WEIGHTED_ROUND_ROBIN_Joiner_11315();
void WEIGHTED_ROUND_ROBIN_Splitter_11320();
void CombineDFT_11322();
void CombineDFT_11323();
void WEIGHTED_ROUND_ROBIN_Joiner_11321();
void CombineDFT_11180();
void WEIGHTED_ROUND_ROBIN_Joiner_11183();
void FloatPrinter(buffer_float_t *chanin);
void FloatPrinter_11181();

#ifdef __cplusplus
}
#endif
#endif
