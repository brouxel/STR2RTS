#include "PEG15-FFT5.h"

buffer_complex_t SplitJoin89_SplitJoin63_SplitJoin63_AnonFilter_a0_2090_2333_2369_2442_split[2];
buffer_complex_t butterfly_2126Post_CollapsedDataParallel_2_2232;
buffer_complex_t SplitJoin81_SplitJoin55_SplitJoin55_AnonFilter_a0_2078_2327_2424_2440_join[2];
buffer_complex_t SplitJoin93_SplitJoin67_SplitJoin67_AnonFilter_a0_2094_2335_2427_2444_join[2];
buffer_complex_t SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2062_2286_2416_2436_split[2];
buffer_complex_t SplitJoin75_SplitJoin49_SplitJoin49_AnonFilter_a0_2070_2323_2422_2438_split[2];
buffer_complex_t SplitJoin101_SplitJoin75_SplitJoin75_AnonFilter_a0_2106_2341_2429_2446_join[2];
buffer_complex_t SplitJoin103_SplitJoin77_SplitJoin77_AnonFilter_a0_2108_2342_2430_2447_join[2];
buffer_complex_t butterfly_2127Post_CollapsedDataParallel_2_2235;
buffer_complex_t SplitJoin16_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child0_2377_2453_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_2216butterfly_2121;
buffer_complex_t Pre_CollapsedDataParallel_1_2228butterfly_2125;
buffer_complex_t SplitJoin91_SplitJoin65_SplitJoin65_AnonFilter_a0_2092_2334_2426_2443_join[2];
buffer_complex_t SplitJoin61_SplitJoin37_SplitJoin37_split2_2045_2312_2375_2458_join[2];
buffer_complex_t SplitJoin55_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child1_2379_2456_split[2];
buffer_complex_t SplitJoin103_SplitJoin77_SplitJoin77_AnonFilter_a0_2108_2342_2430_2447_split[2];
buffer_complex_t SplitJoin0_source_Fiss_2414_2433_split[2];
buffer_complex_t SplitJoin89_SplitJoin63_SplitJoin63_AnonFilter_a0_2090_2333_2369_2442_join[2];
buffer_complex_t SplitJoin101_SplitJoin75_SplitJoin75_AnonFilter_a0_2106_2341_2429_2446_split[2];
buffer_complex_t SplitJoin16_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child0_2377_2453_split[2];
buffer_complex_t SplitJoin44_SplitJoin22_SplitJoin22_split2_2054_2300_2378_2461_join[4];
buffer_complex_t SplitJoin107_SplitJoin81_SplitJoin81_AnonFilter_a0_2114_2345_2431_2448_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_2225butterfly_2124;
buffer_complex_t Pre_CollapsedDataParallel_1_2219butterfly_2122;
buffer_complex_t butterfly_2122Post_CollapsedDataParallel_2_2220;
buffer_complex_t butterfly_2124Post_CollapsedDataParallel_2_2226;
buffer_complex_t SplitJoin55_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child1_2379_2456_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2237WEIGHTED_ROUND_ROBIN_Splitter_2380;
buffer_complex_t SplitJoin12_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child0_2371_2450_join[4];
buffer_complex_t SplitJoin51_SplitJoin29_SplitJoin29_split2_2041_2306_2372_2455_join[2];
buffer_complex_t SplitJoin85_SplitJoin59_SplitJoin59_AnonFilter_a0_2084_2330_2425_2441_split[2];
buffer_complex_t SplitJoin97_SplitJoin71_SplitJoin71_AnonFilter_a0_2100_2338_2428_2445_join[2];
buffer_complex_t SplitJoin85_SplitJoin59_SplitJoin59_AnonFilter_a0_2084_2330_2425_2441_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_2231butterfly_2126;
buffer_complex_t SplitJoin61_SplitJoin37_SplitJoin37_split2_2045_2312_2375_2458_split[2];
buffer_complex_t SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2058_2284_2415_2434_split[2];
buffer_complex_t SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2058_2284_2415_2434_join[2];
buffer_complex_t SplitJoin79_SplitJoin53_SplitJoin53_AnonFilter_a0_2076_2326_2423_2439_split[2];
buffer_complex_t Pre_CollapsedDataParallel_1_2213butterfly_2120;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2385WEIGHTED_ROUND_ROBIN_Splitter_2386;
buffer_complex_t SplitJoin51_SplitJoin29_SplitJoin29_split2_2041_2306_2372_2455_split[2];
buffer_complex_t SplitJoin18_SplitJoin12_SplitJoin12_split2_2039_2292_2370_2454_split[2];
buffer_complex_t SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2062_2286_2416_2436_join[2];
buffer_complex_t SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_2064_2287_2417_2437_split[2];
buffer_complex_t butterfly_2121Post_CollapsedDataParallel_2_2217;
buffer_complex_t SplitJoin18_SplitJoin12_SplitJoin12_split2_2039_2292_2370_2454_join[2];
buffer_complex_t SplitJoin14_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_Hier_2419_2452_split[2];
buffer_complex_t SplitJoin0_source_Fiss_2414_2433_join[2];
buffer_complex_t SplitJoin93_SplitJoin67_SplitJoin67_AnonFilter_a0_2094_2335_2427_2444_split[2];
buffer_complex_t butterfly_2120Post_CollapsedDataParallel_2_2214;
buffer_float_t SplitJoin24_magnitude_Fiss_2421_2462_join[15];
buffer_complex_t SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2060_2285_2368_2435_join[2];
buffer_complex_t SplitJoin81_SplitJoin55_SplitJoin55_AnonFilter_a0_2078_2327_2424_2440_split[2];
buffer_complex_t SplitJoin22_SplitJoin16_SplitJoin16_split2_2052_2295_2376_2460_join[4];
buffer_complex_t SplitJoin44_SplitJoin22_SplitJoin22_split2_2054_2300_2378_2461_split[4];
buffer_complex_t SplitJoin68_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child1_2374_2451_join[4];
buffer_complex_t SplitJoin24_magnitude_Fiss_2421_2462_split[15];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2398sink_2147;
buffer_complex_t Pre_CollapsedDataParallel_1_2234butterfly_2127;
buffer_complex_t SplitJoin22_SplitJoin16_SplitJoin16_split2_2052_2295_2376_2460_split[4];
buffer_complex_t SplitJoin57_SplitJoin33_SplitJoin33_split2_2043_2309_2373_2457_split[2];
buffer_complex_t SplitJoin10_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_Hier_2418_2449_split[2];
buffer_complex_t SplitJoin12_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child0_2371_2450_split[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2391WEIGHTED_ROUND_ROBIN_Splitter_2278;
buffer_complex_t SplitJoin91_SplitJoin65_SplitJoin65_AnonFilter_a0_2092_2334_2426_2443_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2394WEIGHTED_ROUND_ROBIN_Splitter_2236;
buffer_complex_t SplitJoin107_SplitJoin81_SplitJoin81_AnonFilter_a0_2114_2345_2431_2448_split[2];
buffer_complex_t butterfly_2123Post_CollapsedDataParallel_2_2223;
buffer_complex_t SplitJoin14_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_Hier_2419_2452_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_2222butterfly_2123;
buffer_complex_t SplitJoin68_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child1_2374_2451_split[4];
buffer_complex_t butterfly_2125Post_CollapsedDataParallel_2_2229;
buffer_complex_t SplitJoin10_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_Hier_2418_2449_join[2];
buffer_complex_t SplitJoin97_SplitJoin71_SplitJoin71_AnonFilter_a0_2100_2338_2428_2445_split[2];
buffer_complex_t SplitJoin57_SplitJoin33_SplitJoin33_split2_2043_2309_2373_2457_join[2];
buffer_complex_t SplitJoin75_SplitJoin49_SplitJoin49_AnonFilter_a0_2070_2323_2422_2438_join[2];
buffer_complex_t SplitJoin79_SplitJoin53_SplitJoin53_AnonFilter_a0_2076_2326_2423_2439_join[2];
buffer_complex_t SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_2064_2287_2417_2437_join[2];
buffer_complex_t SplitJoin20_SplitJoin14_SplitJoin14_split1_2050_2294_2420_2459_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2279WEIGHTED_ROUND_ROBIN_Splitter_2397;
buffer_complex_t SplitJoin20_SplitJoin14_SplitJoin14_split1_2050_2294_2420_2459_split[2];
buffer_complex_t SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2060_2285_2368_2435_split[2];



void source(buffer_void_t *chanin, buffer_complex_t *chanout) {
		complex_t t;
		t.imag = 0.0 ; 
		t.real = 0.9501 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.2311 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.6068 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.486 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.8913 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.7621 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.4565 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.0185 ; 
		push_complex(&(*chanout), t) ; 
	}


void source_2395() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		source(&(SplitJoin0_source_Fiss_2414_2433_split[0]), &(SplitJoin0_source_Fiss_2414_2433_join[0]));
	ENDFOR
}

void source_2396() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		source(&(SplitJoin0_source_Fiss_2414_2433_split[1]), &(SplitJoin0_source_Fiss_2414_2433_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2393() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_2394() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2394WEIGHTED_ROUND_ROBIN_Splitter_2236, pop_complex(&SplitJoin0_source_Fiss_2414_2433_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2394WEIGHTED_ROUND_ROBIN_Splitter_2236, pop_complex(&SplitJoin0_source_Fiss_2414_2433_join[1]));
		ENDFOR
	ENDFOR
}}

void Identity(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t __tmp638 = pop_complex(&(*chanin));
		push_complex(&(*chanout), __tmp638) ; 
	}


void Identity_2066() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Identity(&(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_2064_2287_2417_2437_split[0]), &(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_2064_2287_2417_2437_join[0]));
	ENDFOR
}

void Identity_2068() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Identity(&(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_2064_2287_2417_2437_split[1]), &(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_2064_2287_2417_2437_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2242() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_2064_2287_2417_2437_split[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2062_2286_2416_2436_split[0]));
		push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_2064_2287_2417_2437_split[1], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2062_2286_2416_2436_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2243() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2062_2286_2416_2436_join[0], pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_2064_2287_2417_2437_join[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2062_2286_2416_2436_join[0], pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_2064_2287_2417_2437_join[1]));
	ENDFOR
}}

void Identity_2072() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Identity(&(SplitJoin75_SplitJoin49_SplitJoin49_AnonFilter_a0_2070_2323_2422_2438_split[0]), &(SplitJoin75_SplitJoin49_SplitJoin49_AnonFilter_a0_2070_2323_2422_2438_join[0]));
	ENDFOR
}

void Identity_2074() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Identity(&(SplitJoin75_SplitJoin49_SplitJoin49_AnonFilter_a0_2070_2323_2422_2438_split[1]), &(SplitJoin75_SplitJoin49_SplitJoin49_AnonFilter_a0_2070_2323_2422_2438_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2244() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		push_complex(&SplitJoin75_SplitJoin49_SplitJoin49_AnonFilter_a0_2070_2323_2422_2438_split[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2062_2286_2416_2436_split[1]));
		push_complex(&SplitJoin75_SplitJoin49_SplitJoin49_AnonFilter_a0_2070_2323_2422_2438_split[1], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2062_2286_2416_2436_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2245() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2062_2286_2416_2436_join[1], pop_complex(&SplitJoin75_SplitJoin49_SplitJoin49_AnonFilter_a0_2070_2323_2422_2438_join[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2062_2286_2416_2436_join[1], pop_complex(&SplitJoin75_SplitJoin49_SplitJoin49_AnonFilter_a0_2070_2323_2422_2438_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_2240() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 30, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2062_2286_2416_2436_split[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2060_2285_2368_2435_split[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2062_2286_2416_2436_split[1], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2060_2285_2368_2435_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2241() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2060_2285_2368_2435_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2062_2286_2416_2436_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2060_2285_2368_2435_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2062_2286_2416_2436_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2060_2285_2368_2435_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2062_2286_2416_2436_join[1]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2060_2285_2368_2435_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2062_2286_2416_2436_join[1]));
	ENDFOR
}}

void Identity_2080() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Identity(&(SplitJoin81_SplitJoin55_SplitJoin55_AnonFilter_a0_2078_2327_2424_2440_split[0]), &(SplitJoin81_SplitJoin55_SplitJoin55_AnonFilter_a0_2078_2327_2424_2440_join[0]));
	ENDFOR
}

void Identity_2082() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Identity(&(SplitJoin81_SplitJoin55_SplitJoin55_AnonFilter_a0_2078_2327_2424_2440_split[1]), &(SplitJoin81_SplitJoin55_SplitJoin55_AnonFilter_a0_2078_2327_2424_2440_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2248() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		push_complex(&SplitJoin81_SplitJoin55_SplitJoin55_AnonFilter_a0_2078_2327_2424_2440_split[0], pop_complex(&SplitJoin79_SplitJoin53_SplitJoin53_AnonFilter_a0_2076_2326_2423_2439_split[0]));
		push_complex(&SplitJoin81_SplitJoin55_SplitJoin55_AnonFilter_a0_2078_2327_2424_2440_split[1], pop_complex(&SplitJoin79_SplitJoin53_SplitJoin53_AnonFilter_a0_2076_2326_2423_2439_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2249() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		push_complex(&SplitJoin79_SplitJoin53_SplitJoin53_AnonFilter_a0_2076_2326_2423_2439_join[0], pop_complex(&SplitJoin81_SplitJoin55_SplitJoin55_AnonFilter_a0_2078_2327_2424_2440_join[0]));
		push_complex(&SplitJoin79_SplitJoin53_SplitJoin53_AnonFilter_a0_2076_2326_2423_2439_join[0], pop_complex(&SplitJoin81_SplitJoin55_SplitJoin55_AnonFilter_a0_2078_2327_2424_2440_join[1]));
	ENDFOR
}}

void Identity_2086() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Identity(&(SplitJoin85_SplitJoin59_SplitJoin59_AnonFilter_a0_2084_2330_2425_2441_split[0]), &(SplitJoin85_SplitJoin59_SplitJoin59_AnonFilter_a0_2084_2330_2425_2441_join[0]));
	ENDFOR
}

void Identity_2088() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Identity(&(SplitJoin85_SplitJoin59_SplitJoin59_AnonFilter_a0_2084_2330_2425_2441_split[1]), &(SplitJoin85_SplitJoin59_SplitJoin59_AnonFilter_a0_2084_2330_2425_2441_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2250() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		push_complex(&SplitJoin85_SplitJoin59_SplitJoin59_AnonFilter_a0_2084_2330_2425_2441_split[0], pop_complex(&SplitJoin79_SplitJoin53_SplitJoin53_AnonFilter_a0_2076_2326_2423_2439_split[1]));
		push_complex(&SplitJoin85_SplitJoin59_SplitJoin59_AnonFilter_a0_2084_2330_2425_2441_split[1], pop_complex(&SplitJoin79_SplitJoin53_SplitJoin53_AnonFilter_a0_2076_2326_2423_2439_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2251() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		push_complex(&SplitJoin79_SplitJoin53_SplitJoin53_AnonFilter_a0_2076_2326_2423_2439_join[1], pop_complex(&SplitJoin85_SplitJoin59_SplitJoin59_AnonFilter_a0_2084_2330_2425_2441_join[0]));
		push_complex(&SplitJoin79_SplitJoin53_SplitJoin53_AnonFilter_a0_2076_2326_2423_2439_join[1], pop_complex(&SplitJoin85_SplitJoin59_SplitJoin59_AnonFilter_a0_2084_2330_2425_2441_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_2246() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 30, __iter_steady_++)
		push_complex(&SplitJoin79_SplitJoin53_SplitJoin53_AnonFilter_a0_2076_2326_2423_2439_split[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2060_2285_2368_2435_split[1]));
		push_complex(&SplitJoin79_SplitJoin53_SplitJoin53_AnonFilter_a0_2076_2326_2423_2439_split[1], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2060_2285_2368_2435_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2247() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2060_2285_2368_2435_join[1], pop_complex(&SplitJoin79_SplitJoin53_SplitJoin53_AnonFilter_a0_2076_2326_2423_2439_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2060_2285_2368_2435_join[1], pop_complex(&SplitJoin79_SplitJoin53_SplitJoin53_AnonFilter_a0_2076_2326_2423_2439_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2060_2285_2368_2435_join[1], pop_complex(&SplitJoin79_SplitJoin53_SplitJoin53_AnonFilter_a0_2076_2326_2423_2439_join[1]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2060_2285_2368_2435_join[1], pop_complex(&SplitJoin79_SplitJoin53_SplitJoin53_AnonFilter_a0_2076_2326_2423_2439_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_2238() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 60, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2060_2285_2368_2435_split[0], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2058_2284_2415_2434_split[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2060_2285_2368_2435_split[1], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2058_2284_2415_2434_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2239() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2058_2284_2415_2434_join[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2060_2285_2368_2435_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2058_2284_2415_2434_join[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2060_2285_2368_2435_join[1]));
		ENDFOR
	ENDFOR
}}

void Identity_2096() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Identity(&(SplitJoin93_SplitJoin67_SplitJoin67_AnonFilter_a0_2094_2335_2427_2444_split[0]), &(SplitJoin93_SplitJoin67_SplitJoin67_AnonFilter_a0_2094_2335_2427_2444_join[0]));
	ENDFOR
}

void Identity_2098() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Identity(&(SplitJoin93_SplitJoin67_SplitJoin67_AnonFilter_a0_2094_2335_2427_2444_split[1]), &(SplitJoin93_SplitJoin67_SplitJoin67_AnonFilter_a0_2094_2335_2427_2444_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2256() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		push_complex(&SplitJoin93_SplitJoin67_SplitJoin67_AnonFilter_a0_2094_2335_2427_2444_split[0], pop_complex(&SplitJoin91_SplitJoin65_SplitJoin65_AnonFilter_a0_2092_2334_2426_2443_split[0]));
		push_complex(&SplitJoin93_SplitJoin67_SplitJoin67_AnonFilter_a0_2094_2335_2427_2444_split[1], pop_complex(&SplitJoin91_SplitJoin65_SplitJoin65_AnonFilter_a0_2092_2334_2426_2443_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2257() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		push_complex(&SplitJoin91_SplitJoin65_SplitJoin65_AnonFilter_a0_2092_2334_2426_2443_join[0], pop_complex(&SplitJoin93_SplitJoin67_SplitJoin67_AnonFilter_a0_2094_2335_2427_2444_join[0]));
		push_complex(&SplitJoin91_SplitJoin65_SplitJoin65_AnonFilter_a0_2092_2334_2426_2443_join[0], pop_complex(&SplitJoin93_SplitJoin67_SplitJoin67_AnonFilter_a0_2094_2335_2427_2444_join[1]));
	ENDFOR
}}

void Identity_2102() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Identity(&(SplitJoin97_SplitJoin71_SplitJoin71_AnonFilter_a0_2100_2338_2428_2445_split[0]), &(SplitJoin97_SplitJoin71_SplitJoin71_AnonFilter_a0_2100_2338_2428_2445_join[0]));
	ENDFOR
}

void Identity_2104() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Identity(&(SplitJoin97_SplitJoin71_SplitJoin71_AnonFilter_a0_2100_2338_2428_2445_split[1]), &(SplitJoin97_SplitJoin71_SplitJoin71_AnonFilter_a0_2100_2338_2428_2445_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2258() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		push_complex(&SplitJoin97_SplitJoin71_SplitJoin71_AnonFilter_a0_2100_2338_2428_2445_split[0], pop_complex(&SplitJoin91_SplitJoin65_SplitJoin65_AnonFilter_a0_2092_2334_2426_2443_split[1]));
		push_complex(&SplitJoin97_SplitJoin71_SplitJoin71_AnonFilter_a0_2100_2338_2428_2445_split[1], pop_complex(&SplitJoin91_SplitJoin65_SplitJoin65_AnonFilter_a0_2092_2334_2426_2443_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2259() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		push_complex(&SplitJoin91_SplitJoin65_SplitJoin65_AnonFilter_a0_2092_2334_2426_2443_join[1], pop_complex(&SplitJoin97_SplitJoin71_SplitJoin71_AnonFilter_a0_2100_2338_2428_2445_join[0]));
		push_complex(&SplitJoin91_SplitJoin65_SplitJoin65_AnonFilter_a0_2092_2334_2426_2443_join[1], pop_complex(&SplitJoin97_SplitJoin71_SplitJoin71_AnonFilter_a0_2100_2338_2428_2445_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_2254() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 30, __iter_steady_++)
		push_complex(&SplitJoin91_SplitJoin65_SplitJoin65_AnonFilter_a0_2092_2334_2426_2443_split[0], pop_complex(&SplitJoin89_SplitJoin63_SplitJoin63_AnonFilter_a0_2090_2333_2369_2442_split[0]));
		push_complex(&SplitJoin91_SplitJoin65_SplitJoin65_AnonFilter_a0_2092_2334_2426_2443_split[1], pop_complex(&SplitJoin89_SplitJoin63_SplitJoin63_AnonFilter_a0_2090_2333_2369_2442_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2255() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		push_complex(&SplitJoin89_SplitJoin63_SplitJoin63_AnonFilter_a0_2090_2333_2369_2442_join[0], pop_complex(&SplitJoin91_SplitJoin65_SplitJoin65_AnonFilter_a0_2092_2334_2426_2443_join[0]));
		push_complex(&SplitJoin89_SplitJoin63_SplitJoin63_AnonFilter_a0_2090_2333_2369_2442_join[0], pop_complex(&SplitJoin91_SplitJoin65_SplitJoin65_AnonFilter_a0_2092_2334_2426_2443_join[0]));
		push_complex(&SplitJoin89_SplitJoin63_SplitJoin63_AnonFilter_a0_2090_2333_2369_2442_join[0], pop_complex(&SplitJoin91_SplitJoin65_SplitJoin65_AnonFilter_a0_2092_2334_2426_2443_join[1]));
		push_complex(&SplitJoin89_SplitJoin63_SplitJoin63_AnonFilter_a0_2090_2333_2369_2442_join[0], pop_complex(&SplitJoin91_SplitJoin65_SplitJoin65_AnonFilter_a0_2092_2334_2426_2443_join[1]));
	ENDFOR
}}

void Identity_2110() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Identity(&(SplitJoin103_SplitJoin77_SplitJoin77_AnonFilter_a0_2108_2342_2430_2447_split[0]), &(SplitJoin103_SplitJoin77_SplitJoin77_AnonFilter_a0_2108_2342_2430_2447_join[0]));
	ENDFOR
}

void Identity_2112() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Identity(&(SplitJoin103_SplitJoin77_SplitJoin77_AnonFilter_a0_2108_2342_2430_2447_split[1]), &(SplitJoin103_SplitJoin77_SplitJoin77_AnonFilter_a0_2108_2342_2430_2447_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2262() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		push_complex(&SplitJoin103_SplitJoin77_SplitJoin77_AnonFilter_a0_2108_2342_2430_2447_split[0], pop_complex(&SplitJoin101_SplitJoin75_SplitJoin75_AnonFilter_a0_2106_2341_2429_2446_split[0]));
		push_complex(&SplitJoin103_SplitJoin77_SplitJoin77_AnonFilter_a0_2108_2342_2430_2447_split[1], pop_complex(&SplitJoin101_SplitJoin75_SplitJoin75_AnonFilter_a0_2106_2341_2429_2446_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2263() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		push_complex(&SplitJoin101_SplitJoin75_SplitJoin75_AnonFilter_a0_2106_2341_2429_2446_join[0], pop_complex(&SplitJoin103_SplitJoin77_SplitJoin77_AnonFilter_a0_2108_2342_2430_2447_join[0]));
		push_complex(&SplitJoin101_SplitJoin75_SplitJoin75_AnonFilter_a0_2106_2341_2429_2446_join[0], pop_complex(&SplitJoin103_SplitJoin77_SplitJoin77_AnonFilter_a0_2108_2342_2430_2447_join[1]));
	ENDFOR
}}

void Identity_2116() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Identity(&(SplitJoin107_SplitJoin81_SplitJoin81_AnonFilter_a0_2114_2345_2431_2448_split[0]), &(SplitJoin107_SplitJoin81_SplitJoin81_AnonFilter_a0_2114_2345_2431_2448_join[0]));
	ENDFOR
}

void Identity_2118() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Identity(&(SplitJoin107_SplitJoin81_SplitJoin81_AnonFilter_a0_2114_2345_2431_2448_split[1]), &(SplitJoin107_SplitJoin81_SplitJoin81_AnonFilter_a0_2114_2345_2431_2448_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2264() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		push_complex(&SplitJoin107_SplitJoin81_SplitJoin81_AnonFilter_a0_2114_2345_2431_2448_split[0], pop_complex(&SplitJoin101_SplitJoin75_SplitJoin75_AnonFilter_a0_2106_2341_2429_2446_split[1]));
		push_complex(&SplitJoin107_SplitJoin81_SplitJoin81_AnonFilter_a0_2114_2345_2431_2448_split[1], pop_complex(&SplitJoin101_SplitJoin75_SplitJoin75_AnonFilter_a0_2106_2341_2429_2446_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2265() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		push_complex(&SplitJoin101_SplitJoin75_SplitJoin75_AnonFilter_a0_2106_2341_2429_2446_join[1], pop_complex(&SplitJoin107_SplitJoin81_SplitJoin81_AnonFilter_a0_2114_2345_2431_2448_join[0]));
		push_complex(&SplitJoin101_SplitJoin75_SplitJoin75_AnonFilter_a0_2106_2341_2429_2446_join[1], pop_complex(&SplitJoin107_SplitJoin81_SplitJoin81_AnonFilter_a0_2114_2345_2431_2448_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_2260() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 30, __iter_steady_++)
		push_complex(&SplitJoin101_SplitJoin75_SplitJoin75_AnonFilter_a0_2106_2341_2429_2446_split[0], pop_complex(&SplitJoin89_SplitJoin63_SplitJoin63_AnonFilter_a0_2090_2333_2369_2442_split[1]));
		push_complex(&SplitJoin101_SplitJoin75_SplitJoin75_AnonFilter_a0_2106_2341_2429_2446_split[1], pop_complex(&SplitJoin89_SplitJoin63_SplitJoin63_AnonFilter_a0_2090_2333_2369_2442_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2261() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		push_complex(&SplitJoin89_SplitJoin63_SplitJoin63_AnonFilter_a0_2090_2333_2369_2442_join[1], pop_complex(&SplitJoin101_SplitJoin75_SplitJoin75_AnonFilter_a0_2106_2341_2429_2446_join[0]));
		push_complex(&SplitJoin89_SplitJoin63_SplitJoin63_AnonFilter_a0_2090_2333_2369_2442_join[1], pop_complex(&SplitJoin101_SplitJoin75_SplitJoin75_AnonFilter_a0_2106_2341_2429_2446_join[0]));
		push_complex(&SplitJoin89_SplitJoin63_SplitJoin63_AnonFilter_a0_2090_2333_2369_2442_join[1], pop_complex(&SplitJoin101_SplitJoin75_SplitJoin75_AnonFilter_a0_2106_2341_2429_2446_join[1]));
		push_complex(&SplitJoin89_SplitJoin63_SplitJoin63_AnonFilter_a0_2090_2333_2369_2442_join[1], pop_complex(&SplitJoin101_SplitJoin75_SplitJoin75_AnonFilter_a0_2106_2341_2429_2446_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_2252() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 60, __iter_steady_++)
		push_complex(&SplitJoin89_SplitJoin63_SplitJoin63_AnonFilter_a0_2090_2333_2369_2442_split[0], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2058_2284_2415_2434_split[1]));
		push_complex(&SplitJoin89_SplitJoin63_SplitJoin63_AnonFilter_a0_2090_2333_2369_2442_split[1], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2058_2284_2415_2434_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2253() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2058_2284_2415_2434_join[1], pop_complex(&SplitJoin89_SplitJoin63_SplitJoin63_AnonFilter_a0_2090_2333_2369_2442_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2058_2284_2415_2434_join[1], pop_complex(&SplitJoin89_SplitJoin63_SplitJoin63_AnonFilter_a0_2090_2333_2369_2442_join[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_2236() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 120, __iter_steady_++)
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2058_2284_2415_2434_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2394WEIGHTED_ROUND_ROBIN_Splitter_2236));
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2058_2284_2415_2434_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2394WEIGHTED_ROUND_ROBIN_Splitter_2236));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2237() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2237WEIGHTED_ROUND_ROBIN_Splitter_2380, pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2058_2284_2415_2434_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2237WEIGHTED_ROUND_ROBIN_Splitter_2380, pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2058_2284_2415_2434_join[1]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_complex_t *chanin, buffer_complex_t *chanout) {
 {
 {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
			push_complex(&(*chanout), peek_complex(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
		}
		ENDFOR
	}
	}
	}
		pop_complex(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_2213() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child0_2371_2450_split[0]), &(Pre_CollapsedDataParallel_1_2213butterfly_2120));
	ENDFOR
}

void butterfly(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&(*chanin)));
		complex_t two = ((complex_t) pop_complex(&(*chanin)));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&(*chanout), __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&(*chanout), __sa2) ; 
	}


void butterfly_2120() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_2213butterfly_2120), &(butterfly_2120Post_CollapsedDataParallel_2_2214));
	ENDFOR
}

void Post_CollapsedDataParallel_2(buffer_complex_t *chanin, buffer_complex_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 2, _k++) {
 {
			push_complex(&(*chanout), peek_complex(&(*chanin), (_k + 0))) ; 
		}
		}
		ENDFOR
	}
	}
		pop_complex(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_2214() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_2120Post_CollapsedDataParallel_2_2214), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child0_2371_2450_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_2216() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child0_2371_2450_split[1]), &(Pre_CollapsedDataParallel_1_2216butterfly_2121));
	ENDFOR
}

void butterfly_2121() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_2216butterfly_2121), &(butterfly_2121Post_CollapsedDataParallel_2_2217));
	ENDFOR
}

void Post_CollapsedDataParallel_2_2217() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_2121Post_CollapsedDataParallel_2_2217), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child0_2371_2450_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_2219() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child0_2371_2450_split[2]), &(Pre_CollapsedDataParallel_1_2219butterfly_2122));
	ENDFOR
}

void butterfly_2122() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_2219butterfly_2122), &(butterfly_2122Post_CollapsedDataParallel_2_2220));
	ENDFOR
}

void Post_CollapsedDataParallel_2_2220() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_2122Post_CollapsedDataParallel_2_2220), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child0_2371_2450_join[2]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_2222() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child0_2371_2450_split[3]), &(Pre_CollapsedDataParallel_1_2222butterfly_2123));
	ENDFOR
}

void butterfly_2123() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_2222butterfly_2123), &(butterfly_2123Post_CollapsedDataParallel_2_2223));
	ENDFOR
}

void Post_CollapsedDataParallel_2_2223() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_2123Post_CollapsedDataParallel_2_2223), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child0_2371_2450_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2381() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child0_2371_2450_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_Hier_2418_2449_split[0]));
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child0_2371_2450_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_Hier_2418_2449_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2382() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_Hier_2418_2449_join[0], pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child0_2371_2450_join[__iter_]));
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_Hier_2418_2449_join[0], pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child0_2371_2450_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_2225() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin68_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child1_2374_2451_split[0]), &(Pre_CollapsedDataParallel_1_2225butterfly_2124));
	ENDFOR
}

void butterfly_2124() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_2225butterfly_2124), &(butterfly_2124Post_CollapsedDataParallel_2_2226));
	ENDFOR
}

void Post_CollapsedDataParallel_2_2226() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_2124Post_CollapsedDataParallel_2_2226), &(SplitJoin68_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child1_2374_2451_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_2228() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin68_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child1_2374_2451_split[1]), &(Pre_CollapsedDataParallel_1_2228butterfly_2125));
	ENDFOR
}

void butterfly_2125() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_2228butterfly_2125), &(butterfly_2125Post_CollapsedDataParallel_2_2229));
	ENDFOR
}

void Post_CollapsedDataParallel_2_2229() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_2125Post_CollapsedDataParallel_2_2229), &(SplitJoin68_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child1_2374_2451_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_2231() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin68_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child1_2374_2451_split[2]), &(Pre_CollapsedDataParallel_1_2231butterfly_2126));
	ENDFOR
}

void butterfly_2126() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_2231butterfly_2126), &(butterfly_2126Post_CollapsedDataParallel_2_2232));
	ENDFOR
}

void Post_CollapsedDataParallel_2_2232() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_2126Post_CollapsedDataParallel_2_2232), &(SplitJoin68_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child1_2374_2451_join[2]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_2234() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin68_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child1_2374_2451_split[3]), &(Pre_CollapsedDataParallel_1_2234butterfly_2127));
	ENDFOR
}

void butterfly_2127() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_2234butterfly_2127), &(butterfly_2127Post_CollapsedDataParallel_2_2235));
	ENDFOR
}

void Post_CollapsedDataParallel_2_2235() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_2127Post_CollapsedDataParallel_2_2235), &(SplitJoin68_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child1_2374_2451_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2383() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin68_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child1_2374_2451_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_Hier_2418_2449_split[1]));
			push_complex(&SplitJoin68_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child1_2374_2451_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_Hier_2418_2449_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2384() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_Hier_2418_2449_join[1], pop_complex(&SplitJoin68_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child1_2374_2451_join[__iter_]));
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_Hier_2418_2449_join[1], pop_complex(&SplitJoin68_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child1_2374_2451_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_2380() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_Hier_2418_2449_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2237WEIGHTED_ROUND_ROBIN_Splitter_2380));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_Hier_2418_2449_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2237WEIGHTED_ROUND_ROBIN_Splitter_2380));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2385() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2385WEIGHTED_ROUND_ROBIN_Splitter_2386, pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_Hier_2418_2449_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2385WEIGHTED_ROUND_ROBIN_Splitter_2386, pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_Hier_2418_2449_join[1]));
		ENDFOR
	ENDFOR
}}

void butterfly_2129() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		butterfly(&(SplitJoin18_SplitJoin12_SplitJoin12_split2_2039_2292_2370_2454_split[0]), &(SplitJoin18_SplitJoin12_SplitJoin12_split2_2039_2292_2370_2454_join[0]));
	ENDFOR
}

void butterfly_2130() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		butterfly(&(SplitJoin18_SplitJoin12_SplitJoin12_split2_2039_2292_2370_2454_split[1]), &(SplitJoin18_SplitJoin12_SplitJoin12_split2_2039_2292_2370_2454_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2270() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 30, __iter_steady_++)
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_2039_2292_2370_2454_split[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child0_2377_2453_split[0]));
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_2039_2292_2370_2454_split[1], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child0_2377_2453_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2271() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 30, __iter_steady_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child0_2377_2453_join[0], pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_2039_2292_2370_2454_join[0]));
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child0_2377_2453_join[0], pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_2039_2292_2370_2454_join[1]));
	ENDFOR
}}

void butterfly_2131() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		butterfly(&(SplitJoin51_SplitJoin29_SplitJoin29_split2_2041_2306_2372_2455_split[0]), &(SplitJoin51_SplitJoin29_SplitJoin29_split2_2041_2306_2372_2455_join[0]));
	ENDFOR
}

void butterfly_2132() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		butterfly(&(SplitJoin51_SplitJoin29_SplitJoin29_split2_2041_2306_2372_2455_split[1]), &(SplitJoin51_SplitJoin29_SplitJoin29_split2_2041_2306_2372_2455_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2272() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 30, __iter_steady_++)
		push_complex(&SplitJoin51_SplitJoin29_SplitJoin29_split2_2041_2306_2372_2455_split[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child0_2377_2453_split[1]));
		push_complex(&SplitJoin51_SplitJoin29_SplitJoin29_split2_2041_2306_2372_2455_split[1], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child0_2377_2453_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2273() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 30, __iter_steady_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child0_2377_2453_join[1], pop_complex(&SplitJoin51_SplitJoin29_SplitJoin29_split2_2041_2306_2372_2455_join[0]));
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child0_2377_2453_join[1], pop_complex(&SplitJoin51_SplitJoin29_SplitJoin29_split2_2041_2306_2372_2455_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_2387() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child0_2377_2453_split[0], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_Hier_2419_2452_split[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child0_2377_2453_split[1], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_Hier_2419_2452_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2388() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_Hier_2419_2452_join[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child0_2377_2453_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_Hier_2419_2452_join[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child0_2377_2453_join[1]));
		ENDFOR
	ENDFOR
}}

void butterfly_2133() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		butterfly(&(SplitJoin57_SplitJoin33_SplitJoin33_split2_2043_2309_2373_2457_split[0]), &(SplitJoin57_SplitJoin33_SplitJoin33_split2_2043_2309_2373_2457_join[0]));
	ENDFOR
}

void butterfly_2134() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		butterfly(&(SplitJoin57_SplitJoin33_SplitJoin33_split2_2043_2309_2373_2457_split[1]), &(SplitJoin57_SplitJoin33_SplitJoin33_split2_2043_2309_2373_2457_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2274() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 30, __iter_steady_++)
		push_complex(&SplitJoin57_SplitJoin33_SplitJoin33_split2_2043_2309_2373_2457_split[0], pop_complex(&SplitJoin55_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child1_2379_2456_split[0]));
		push_complex(&SplitJoin57_SplitJoin33_SplitJoin33_split2_2043_2309_2373_2457_split[1], pop_complex(&SplitJoin55_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child1_2379_2456_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2275() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 30, __iter_steady_++)
		push_complex(&SplitJoin55_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child1_2379_2456_join[0], pop_complex(&SplitJoin57_SplitJoin33_SplitJoin33_split2_2043_2309_2373_2457_join[0]));
		push_complex(&SplitJoin55_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child1_2379_2456_join[0], pop_complex(&SplitJoin57_SplitJoin33_SplitJoin33_split2_2043_2309_2373_2457_join[1]));
	ENDFOR
}}

void butterfly_2135() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		butterfly(&(SplitJoin61_SplitJoin37_SplitJoin37_split2_2045_2312_2375_2458_split[0]), &(SplitJoin61_SplitJoin37_SplitJoin37_split2_2045_2312_2375_2458_join[0]));
	ENDFOR
}

void butterfly_2136() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		butterfly(&(SplitJoin61_SplitJoin37_SplitJoin37_split2_2045_2312_2375_2458_split[1]), &(SplitJoin61_SplitJoin37_SplitJoin37_split2_2045_2312_2375_2458_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2276() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 30, __iter_steady_++)
		push_complex(&SplitJoin61_SplitJoin37_SplitJoin37_split2_2045_2312_2375_2458_split[0], pop_complex(&SplitJoin55_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child1_2379_2456_split[1]));
		push_complex(&SplitJoin61_SplitJoin37_SplitJoin37_split2_2045_2312_2375_2458_split[1], pop_complex(&SplitJoin55_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child1_2379_2456_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2277() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 30, __iter_steady_++)
		push_complex(&SplitJoin55_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child1_2379_2456_join[1], pop_complex(&SplitJoin61_SplitJoin37_SplitJoin37_split2_2045_2312_2375_2458_join[0]));
		push_complex(&SplitJoin55_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child1_2379_2456_join[1], pop_complex(&SplitJoin61_SplitJoin37_SplitJoin37_split2_2045_2312_2375_2458_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_2389() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin55_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child1_2379_2456_split[0], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_Hier_2419_2452_split[1]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin55_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child1_2379_2456_split[1], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_Hier_2419_2452_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2390() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_Hier_2419_2452_join[1], pop_complex(&SplitJoin55_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child1_2379_2456_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_Hier_2419_2452_join[1], pop_complex(&SplitJoin55_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child1_2379_2456_join[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_2386() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_Hier_2419_2452_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2385WEIGHTED_ROUND_ROBIN_Splitter_2386));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_Hier_2419_2452_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2385WEIGHTED_ROUND_ROBIN_Splitter_2386));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2391() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2391WEIGHTED_ROUND_ROBIN_Splitter_2278, pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_Hier_2419_2452_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2391WEIGHTED_ROUND_ROBIN_Splitter_2278, pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_Hier_2419_2452_join[1]));
		ENDFOR
	ENDFOR
}}

void butterfly_2138() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_2052_2295_2376_2460_split[0]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_2052_2295_2376_2460_join[0]));
	ENDFOR
}

void butterfly_2139() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_2052_2295_2376_2460_split[1]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_2052_2295_2376_2460_join[1]));
	ENDFOR
}

void butterfly_2140() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_2052_2295_2376_2460_split[2]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_2052_2295_2376_2460_join[2]));
	ENDFOR
}

void butterfly_2141() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_2052_2295_2376_2460_split[3]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_2052_2295_2376_2460_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2280() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 30, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_2052_2295_2376_2460_split[__iter_], pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_2050_2294_2420_2459_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2281() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 30, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_2050_2294_2420_2459_join[0], pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_2052_2295_2376_2460_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void butterfly_2142() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		butterfly(&(SplitJoin44_SplitJoin22_SplitJoin22_split2_2054_2300_2378_2461_split[0]), &(SplitJoin44_SplitJoin22_SplitJoin22_split2_2054_2300_2378_2461_join[0]));
	ENDFOR
}

void butterfly_2143() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		butterfly(&(SplitJoin44_SplitJoin22_SplitJoin22_split2_2054_2300_2378_2461_split[1]), &(SplitJoin44_SplitJoin22_SplitJoin22_split2_2054_2300_2378_2461_join[1]));
	ENDFOR
}

void butterfly_2144() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		butterfly(&(SplitJoin44_SplitJoin22_SplitJoin22_split2_2054_2300_2378_2461_split[2]), &(SplitJoin44_SplitJoin22_SplitJoin22_split2_2054_2300_2378_2461_join[2]));
	ENDFOR
}

void butterfly_2145() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		butterfly(&(SplitJoin44_SplitJoin22_SplitJoin22_split2_2054_2300_2378_2461_split[3]), &(SplitJoin44_SplitJoin22_SplitJoin22_split2_2054_2300_2378_2461_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2282() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 30, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin44_SplitJoin22_SplitJoin22_split2_2054_2300_2378_2461_split[__iter_], pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_2050_2294_2420_2459_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2283() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 30, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_2050_2294_2420_2459_join[1], pop_complex(&SplitJoin44_SplitJoin22_SplitJoin22_split2_2054_2300_2378_2461_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_2278() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_2050_2294_2420_2459_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2391WEIGHTED_ROUND_ROBIN_Splitter_2278));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_2050_2294_2420_2459_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2391WEIGHTED_ROUND_ROBIN_Splitter_2278));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2279() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2279WEIGHTED_ROUND_ROBIN_Splitter_2397, pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_2050_2294_2420_2459_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2279WEIGHTED_ROUND_ROBIN_Splitter_2397, pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_2050_2294_2420_2459_join[1]));
		ENDFOR
	ENDFOR
}}

void magnitude(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		push_float(&(*chanout), ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}


void magnitude_2399() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_2421_2462_split[0]), &(SplitJoin24_magnitude_Fiss_2421_2462_join[0]));
	ENDFOR
}

void magnitude_2400() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_2421_2462_split[1]), &(SplitJoin24_magnitude_Fiss_2421_2462_join[1]));
	ENDFOR
}

void magnitude_2401() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_2421_2462_split[2]), &(SplitJoin24_magnitude_Fiss_2421_2462_join[2]));
	ENDFOR
}

void magnitude_2402() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_2421_2462_split[3]), &(SplitJoin24_magnitude_Fiss_2421_2462_join[3]));
	ENDFOR
}

void magnitude_2403() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_2421_2462_split[4]), &(SplitJoin24_magnitude_Fiss_2421_2462_join[4]));
	ENDFOR
}

void magnitude_2404() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_2421_2462_split[5]), &(SplitJoin24_magnitude_Fiss_2421_2462_join[5]));
	ENDFOR
}

void magnitude_2405() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_2421_2462_split[6]), &(SplitJoin24_magnitude_Fiss_2421_2462_join[6]));
	ENDFOR
}

void magnitude_2406() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_2421_2462_split[7]), &(SplitJoin24_magnitude_Fiss_2421_2462_join[7]));
	ENDFOR
}

void magnitude_2407() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_2421_2462_split[8]), &(SplitJoin24_magnitude_Fiss_2421_2462_join[8]));
	ENDFOR
}

void magnitude_2408() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_2421_2462_split[9]), &(SplitJoin24_magnitude_Fiss_2421_2462_join[9]));
	ENDFOR
}

void magnitude_2409() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_2421_2462_split[10]), &(SplitJoin24_magnitude_Fiss_2421_2462_join[10]));
	ENDFOR
}

void magnitude_2410() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_2421_2462_split[11]), &(SplitJoin24_magnitude_Fiss_2421_2462_join[11]));
	ENDFOR
}

void magnitude_2411() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_2421_2462_split[12]), &(SplitJoin24_magnitude_Fiss_2421_2462_join[12]));
	ENDFOR
}

void magnitude_2412() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_2421_2462_split[13]), &(SplitJoin24_magnitude_Fiss_2421_2462_join[13]));
	ENDFOR
}

void magnitude_2413() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_2421_2462_split[14]), &(SplitJoin24_magnitude_Fiss_2421_2462_join[14]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2397() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 15, __iter_++)
			push_complex(&SplitJoin24_magnitude_Fiss_2421_2462_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2279WEIGHTED_ROUND_ROBIN_Splitter_2397));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2398() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 15, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2398sink_2147, pop_float(&SplitJoin24_magnitude_Fiss_2421_2462_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void sink(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void sink_2147() {
	FOR(uint32_t, __iter_steady_, 0, <, 240, __iter_steady_++)
		sink(&(WEIGHTED_ROUND_ROBIN_Joiner_2398sink_2147));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_complex(&SplitJoin89_SplitJoin63_SplitJoin63_AnonFilter_a0_2090_2333_2369_2442_split[__iter_init_0_]);
	ENDFOR
	init_buffer_complex(&butterfly_2126Post_CollapsedDataParallel_2_2232);
	FOR(int, __iter_init_1_, 0, <, 2, __iter_init_1_++)
		init_buffer_complex(&SplitJoin81_SplitJoin55_SplitJoin55_AnonFilter_a0_2078_2327_2424_2440_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_complex(&SplitJoin93_SplitJoin67_SplitJoin67_AnonFilter_a0_2094_2335_2427_2444_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 2, __iter_init_3_++)
		init_buffer_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2062_2286_2416_2436_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 2, __iter_init_4_++)
		init_buffer_complex(&SplitJoin75_SplitJoin49_SplitJoin49_AnonFilter_a0_2070_2323_2422_2438_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 2, __iter_init_5_++)
		init_buffer_complex(&SplitJoin101_SplitJoin75_SplitJoin75_AnonFilter_a0_2106_2341_2429_2446_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 2, __iter_init_6_++)
		init_buffer_complex(&SplitJoin103_SplitJoin77_SplitJoin77_AnonFilter_a0_2108_2342_2430_2447_join[__iter_init_6_]);
	ENDFOR
	init_buffer_complex(&butterfly_2127Post_CollapsedDataParallel_2_2235);
	FOR(int, __iter_init_7_, 0, <, 2, __iter_init_7_++)
		init_buffer_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child0_2377_2453_join[__iter_init_7_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_2216butterfly_2121);
	init_buffer_complex(&Pre_CollapsedDataParallel_1_2228butterfly_2125);
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_complex(&SplitJoin91_SplitJoin65_SplitJoin65_AnonFilter_a0_2092_2334_2426_2443_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 2, __iter_init_9_++)
		init_buffer_complex(&SplitJoin61_SplitJoin37_SplitJoin37_split2_2045_2312_2375_2458_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_complex(&SplitJoin55_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child1_2379_2456_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 2, __iter_init_11_++)
		init_buffer_complex(&SplitJoin103_SplitJoin77_SplitJoin77_AnonFilter_a0_2108_2342_2430_2447_split[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 2, __iter_init_12_++)
		init_buffer_complex(&SplitJoin0_source_Fiss_2414_2433_split[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_complex(&SplitJoin89_SplitJoin63_SplitJoin63_AnonFilter_a0_2090_2333_2369_2442_join[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_complex(&SplitJoin101_SplitJoin75_SplitJoin75_AnonFilter_a0_2106_2341_2429_2446_split[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 2, __iter_init_15_++)
		init_buffer_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child0_2377_2453_split[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 4, __iter_init_16_++)
		init_buffer_complex(&SplitJoin44_SplitJoin22_SplitJoin22_split2_2054_2300_2378_2461_join[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_complex(&SplitJoin107_SplitJoin81_SplitJoin81_AnonFilter_a0_2114_2345_2431_2448_join[__iter_init_17_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_2225butterfly_2124);
	init_buffer_complex(&Pre_CollapsedDataParallel_1_2219butterfly_2122);
	init_buffer_complex(&butterfly_2122Post_CollapsedDataParallel_2_2220);
	init_buffer_complex(&butterfly_2124Post_CollapsedDataParallel_2_2226);
	FOR(int, __iter_init_18_, 0, <, 2, __iter_init_18_++)
		init_buffer_complex(&SplitJoin55_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_child1_2379_2456_join[__iter_init_18_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2237WEIGHTED_ROUND_ROBIN_Splitter_2380);
	FOR(int, __iter_init_19_, 0, <, 4, __iter_init_19_++)
		init_buffer_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child0_2371_2450_join[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 2, __iter_init_20_++)
		init_buffer_complex(&SplitJoin51_SplitJoin29_SplitJoin29_split2_2041_2306_2372_2455_join[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 2, __iter_init_21_++)
		init_buffer_complex(&SplitJoin85_SplitJoin59_SplitJoin59_AnonFilter_a0_2084_2330_2425_2441_split[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 2, __iter_init_22_++)
		init_buffer_complex(&SplitJoin97_SplitJoin71_SplitJoin71_AnonFilter_a0_2100_2338_2428_2445_join[__iter_init_22_]);
	ENDFOR
	FOR(int, __iter_init_23_, 0, <, 2, __iter_init_23_++)
		init_buffer_complex(&SplitJoin85_SplitJoin59_SplitJoin59_AnonFilter_a0_2084_2330_2425_2441_join[__iter_init_23_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_2231butterfly_2126);
	FOR(int, __iter_init_24_, 0, <, 2, __iter_init_24_++)
		init_buffer_complex(&SplitJoin61_SplitJoin37_SplitJoin37_split2_2045_2312_2375_2458_split[__iter_init_24_]);
	ENDFOR
	FOR(int, __iter_init_25_, 0, <, 2, __iter_init_25_++)
		init_buffer_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2058_2284_2415_2434_split[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 2, __iter_init_26_++)
		init_buffer_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2058_2284_2415_2434_join[__iter_init_26_]);
	ENDFOR
	FOR(int, __iter_init_27_, 0, <, 2, __iter_init_27_++)
		init_buffer_complex(&SplitJoin79_SplitJoin53_SplitJoin53_AnonFilter_a0_2076_2326_2423_2439_split[__iter_init_27_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_2213butterfly_2120);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2385WEIGHTED_ROUND_ROBIN_Splitter_2386);
	FOR(int, __iter_init_28_, 0, <, 2, __iter_init_28_++)
		init_buffer_complex(&SplitJoin51_SplitJoin29_SplitJoin29_split2_2041_2306_2372_2455_split[__iter_init_28_]);
	ENDFOR
	FOR(int, __iter_init_29_, 0, <, 2, __iter_init_29_++)
		init_buffer_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_2039_2292_2370_2454_split[__iter_init_29_]);
	ENDFOR
	FOR(int, __iter_init_30_, 0, <, 2, __iter_init_30_++)
		init_buffer_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2062_2286_2416_2436_join[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 2, __iter_init_31_++)
		init_buffer_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_2064_2287_2417_2437_split[__iter_init_31_]);
	ENDFOR
	init_buffer_complex(&butterfly_2121Post_CollapsedDataParallel_2_2217);
	FOR(int, __iter_init_32_, 0, <, 2, __iter_init_32_++)
		init_buffer_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_2039_2292_2370_2454_join[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 2, __iter_init_33_++)
		init_buffer_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_Hier_2419_2452_split[__iter_init_33_]);
	ENDFOR
	FOR(int, __iter_init_34_, 0, <, 2, __iter_init_34_++)
		init_buffer_complex(&SplitJoin0_source_Fiss_2414_2433_join[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 2, __iter_init_35_++)
		init_buffer_complex(&SplitJoin93_SplitJoin67_SplitJoin67_AnonFilter_a0_2094_2335_2427_2444_split[__iter_init_35_]);
	ENDFOR
	init_buffer_complex(&butterfly_2120Post_CollapsedDataParallel_2_2214);
	FOR(int, __iter_init_36_, 0, <, 15, __iter_init_36_++)
		init_buffer_float(&SplitJoin24_magnitude_Fiss_2421_2462_join[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 2, __iter_init_37_++)
		init_buffer_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2060_2285_2368_2435_join[__iter_init_37_]);
	ENDFOR
	FOR(int, __iter_init_38_, 0, <, 2, __iter_init_38_++)
		init_buffer_complex(&SplitJoin81_SplitJoin55_SplitJoin55_AnonFilter_a0_2078_2327_2424_2440_split[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 4, __iter_init_39_++)
		init_buffer_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_2052_2295_2376_2460_join[__iter_init_39_]);
	ENDFOR
	FOR(int, __iter_init_40_, 0, <, 4, __iter_init_40_++)
		init_buffer_complex(&SplitJoin44_SplitJoin22_SplitJoin22_split2_2054_2300_2378_2461_split[__iter_init_40_]);
	ENDFOR
	FOR(int, __iter_init_41_, 0, <, 4, __iter_init_41_++)
		init_buffer_complex(&SplitJoin68_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child1_2374_2451_join[__iter_init_41_]);
	ENDFOR
	FOR(int, __iter_init_42_, 0, <, 15, __iter_init_42_++)
		init_buffer_complex(&SplitJoin24_magnitude_Fiss_2421_2462_split[__iter_init_42_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2398sink_2147);
	init_buffer_complex(&Pre_CollapsedDataParallel_1_2234butterfly_2127);
	FOR(int, __iter_init_43_, 0, <, 4, __iter_init_43_++)
		init_buffer_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_2052_2295_2376_2460_split[__iter_init_43_]);
	ENDFOR
	FOR(int, __iter_init_44_, 0, <, 2, __iter_init_44_++)
		init_buffer_complex(&SplitJoin57_SplitJoin33_SplitJoin33_split2_2043_2309_2373_2457_split[__iter_init_44_]);
	ENDFOR
	FOR(int, __iter_init_45_, 0, <, 2, __iter_init_45_++)
		init_buffer_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_Hier_2418_2449_split[__iter_init_45_]);
	ENDFOR
	FOR(int, __iter_init_46_, 0, <, 4, __iter_init_46_++)
		init_buffer_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child0_2371_2450_split[__iter_init_46_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2391WEIGHTED_ROUND_ROBIN_Splitter_2278);
	FOR(int, __iter_init_47_, 0, <, 2, __iter_init_47_++)
		init_buffer_complex(&SplitJoin91_SplitJoin65_SplitJoin65_AnonFilter_a0_2092_2334_2426_2443_split[__iter_init_47_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2394WEIGHTED_ROUND_ROBIN_Splitter_2236);
	FOR(int, __iter_init_48_, 0, <, 2, __iter_init_48_++)
		init_buffer_complex(&SplitJoin107_SplitJoin81_SplitJoin81_AnonFilter_a0_2114_2345_2431_2448_split[__iter_init_48_]);
	ENDFOR
	init_buffer_complex(&butterfly_2123Post_CollapsedDataParallel_2_2223);
	FOR(int, __iter_init_49_, 0, <, 2, __iter_init_49_++)
		init_buffer_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_2037_2291_Hier_Hier_2419_2452_join[__iter_init_49_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_2222butterfly_2123);
	FOR(int, __iter_init_50_, 0, <, 4, __iter_init_50_++)
		init_buffer_complex(&SplitJoin68_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_child1_2374_2451_split[__iter_init_50_]);
	ENDFOR
	init_buffer_complex(&butterfly_2125Post_CollapsedDataParallel_2_2229);
	FOR(int, __iter_init_51_, 0, <, 2, __iter_init_51_++)
		init_buffer_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_2016_2289_Hier_Hier_2418_2449_join[__iter_init_51_]);
	ENDFOR
	FOR(int, __iter_init_52_, 0, <, 2, __iter_init_52_++)
		init_buffer_complex(&SplitJoin97_SplitJoin71_SplitJoin71_AnonFilter_a0_2100_2338_2428_2445_split[__iter_init_52_]);
	ENDFOR
	FOR(int, __iter_init_53_, 0, <, 2, __iter_init_53_++)
		init_buffer_complex(&SplitJoin57_SplitJoin33_SplitJoin33_split2_2043_2309_2373_2457_join[__iter_init_53_]);
	ENDFOR
	FOR(int, __iter_init_54_, 0, <, 2, __iter_init_54_++)
		init_buffer_complex(&SplitJoin75_SplitJoin49_SplitJoin49_AnonFilter_a0_2070_2323_2422_2438_join[__iter_init_54_]);
	ENDFOR
	FOR(int, __iter_init_55_, 0, <, 2, __iter_init_55_++)
		init_buffer_complex(&SplitJoin79_SplitJoin53_SplitJoin53_AnonFilter_a0_2076_2326_2423_2439_join[__iter_init_55_]);
	ENDFOR
	FOR(int, __iter_init_56_, 0, <, 2, __iter_init_56_++)
		init_buffer_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_2064_2287_2417_2437_join[__iter_init_56_]);
	ENDFOR
	FOR(int, __iter_init_57_, 0, <, 2, __iter_init_57_++)
		init_buffer_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_2050_2294_2420_2459_join[__iter_init_57_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2279WEIGHTED_ROUND_ROBIN_Splitter_2397);
	FOR(int, __iter_init_58_, 0, <, 2, __iter_init_58_++)
		init_buffer_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_2050_2294_2420_2459_split[__iter_init_58_]);
	ENDFOR
	FOR(int, __iter_init_59_, 0, <, 2, __iter_init_59_++)
		init_buffer_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2060_2285_2368_2435_split[__iter_init_59_]);
	ENDFOR
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_2393();
			source_2395();
			source_2396();
		WEIGHTED_ROUND_ROBIN_Joiner_2394();
		WEIGHTED_ROUND_ROBIN_Splitter_2236();
			WEIGHTED_ROUND_ROBIN_Splitter_2238();
				WEIGHTED_ROUND_ROBIN_Splitter_2240();
					WEIGHTED_ROUND_ROBIN_Splitter_2242();
						Identity_2066();
						Identity_2068();
					WEIGHTED_ROUND_ROBIN_Joiner_2243();
					WEIGHTED_ROUND_ROBIN_Splitter_2244();
						Identity_2072();
						Identity_2074();
					WEIGHTED_ROUND_ROBIN_Joiner_2245();
				WEIGHTED_ROUND_ROBIN_Joiner_2241();
				WEIGHTED_ROUND_ROBIN_Splitter_2246();
					WEIGHTED_ROUND_ROBIN_Splitter_2248();
						Identity_2080();
						Identity_2082();
					WEIGHTED_ROUND_ROBIN_Joiner_2249();
					WEIGHTED_ROUND_ROBIN_Splitter_2250();
						Identity_2086();
						Identity_2088();
					WEIGHTED_ROUND_ROBIN_Joiner_2251();
				WEIGHTED_ROUND_ROBIN_Joiner_2247();
			WEIGHTED_ROUND_ROBIN_Joiner_2239();
			WEIGHTED_ROUND_ROBIN_Splitter_2252();
				WEIGHTED_ROUND_ROBIN_Splitter_2254();
					WEIGHTED_ROUND_ROBIN_Splitter_2256();
						Identity_2096();
						Identity_2098();
					WEIGHTED_ROUND_ROBIN_Joiner_2257();
					WEIGHTED_ROUND_ROBIN_Splitter_2258();
						Identity_2102();
						Identity_2104();
					WEIGHTED_ROUND_ROBIN_Joiner_2259();
				WEIGHTED_ROUND_ROBIN_Joiner_2255();
				WEIGHTED_ROUND_ROBIN_Splitter_2260();
					WEIGHTED_ROUND_ROBIN_Splitter_2262();
						Identity_2110();
						Identity_2112();
					WEIGHTED_ROUND_ROBIN_Joiner_2263();
					WEIGHTED_ROUND_ROBIN_Splitter_2264();
						Identity_2116();
						Identity_2118();
					WEIGHTED_ROUND_ROBIN_Joiner_2265();
				WEIGHTED_ROUND_ROBIN_Joiner_2261();
			WEIGHTED_ROUND_ROBIN_Joiner_2253();
		WEIGHTED_ROUND_ROBIN_Joiner_2237();
		WEIGHTED_ROUND_ROBIN_Splitter_2380();
			WEIGHTED_ROUND_ROBIN_Splitter_2381();
				Pre_CollapsedDataParallel_1_2213();
				butterfly_2120();
				Post_CollapsedDataParallel_2_2214();
				Pre_CollapsedDataParallel_1_2216();
				butterfly_2121();
				Post_CollapsedDataParallel_2_2217();
				Pre_CollapsedDataParallel_1_2219();
				butterfly_2122();
				Post_CollapsedDataParallel_2_2220();
				Pre_CollapsedDataParallel_1_2222();
				butterfly_2123();
				Post_CollapsedDataParallel_2_2223();
			WEIGHTED_ROUND_ROBIN_Joiner_2382();
			WEIGHTED_ROUND_ROBIN_Splitter_2383();
				Pre_CollapsedDataParallel_1_2225();
				butterfly_2124();
				Post_CollapsedDataParallel_2_2226();
				Pre_CollapsedDataParallel_1_2228();
				butterfly_2125();
				Post_CollapsedDataParallel_2_2229();
				Pre_CollapsedDataParallel_1_2231();
				butterfly_2126();
				Post_CollapsedDataParallel_2_2232();
				Pre_CollapsedDataParallel_1_2234();
				butterfly_2127();
				Post_CollapsedDataParallel_2_2235();
			WEIGHTED_ROUND_ROBIN_Joiner_2384();
		WEIGHTED_ROUND_ROBIN_Joiner_2385();
		WEIGHTED_ROUND_ROBIN_Splitter_2386();
			WEIGHTED_ROUND_ROBIN_Splitter_2387();
				WEIGHTED_ROUND_ROBIN_Splitter_2270();
					butterfly_2129();
					butterfly_2130();
				WEIGHTED_ROUND_ROBIN_Joiner_2271();
				WEIGHTED_ROUND_ROBIN_Splitter_2272();
					butterfly_2131();
					butterfly_2132();
				WEIGHTED_ROUND_ROBIN_Joiner_2273();
			WEIGHTED_ROUND_ROBIN_Joiner_2388();
			WEIGHTED_ROUND_ROBIN_Splitter_2389();
				WEIGHTED_ROUND_ROBIN_Splitter_2274();
					butterfly_2133();
					butterfly_2134();
				WEIGHTED_ROUND_ROBIN_Joiner_2275();
				WEIGHTED_ROUND_ROBIN_Splitter_2276();
					butterfly_2135();
					butterfly_2136();
				WEIGHTED_ROUND_ROBIN_Joiner_2277();
			WEIGHTED_ROUND_ROBIN_Joiner_2390();
		WEIGHTED_ROUND_ROBIN_Joiner_2391();
		WEIGHTED_ROUND_ROBIN_Splitter_2278();
			WEIGHTED_ROUND_ROBIN_Splitter_2280();
				butterfly_2138();
				butterfly_2139();
				butterfly_2140();
				butterfly_2141();
			WEIGHTED_ROUND_ROBIN_Joiner_2281();
			WEIGHTED_ROUND_ROBIN_Splitter_2282();
				butterfly_2142();
				butterfly_2143();
				butterfly_2144();
				butterfly_2145();
			WEIGHTED_ROUND_ROBIN_Joiner_2283();
		WEIGHTED_ROUND_ROBIN_Joiner_2279();
		WEIGHTED_ROUND_ROBIN_Splitter_2397();
			magnitude_2399();
			magnitude_2400();
			magnitude_2401();
			magnitude_2402();
			magnitude_2403();
			magnitude_2404();
			magnitude_2405();
			magnitude_2406();
			magnitude_2407();
			magnitude_2408();
			magnitude_2409();
			magnitude_2410();
			magnitude_2411();
			magnitude_2412();
			magnitude_2413();
		WEIGHTED_ROUND_ROBIN_Joiner_2398();
		sink_2147();
	ENDFOR
	return EXIT_SUCCESS;
}
