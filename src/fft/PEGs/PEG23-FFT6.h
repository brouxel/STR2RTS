#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=2944 on the compile command line
#else
#if BUF_SIZEMAX < 2944
#error BUF_SIZEMAX too small, it must be at least 2944
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	complex_t wn;
} CombineDFT_2219_t;
void FFTTestSource(buffer_complex_t *chanout);
void FFTTestSource_2165();
void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout);
void FFTReorderSimple_2166();
void WEIGHTED_ROUND_ROBIN_Splitter_2179();
void FFTReorderSimple_2181();
void FFTReorderSimple_2182();
void WEIGHTED_ROUND_ROBIN_Joiner_2180();
void WEIGHTED_ROUND_ROBIN_Splitter_2183();
void FFTReorderSimple_2185();
void FFTReorderSimple_2186();
void FFTReorderSimple_2187();
void FFTReorderSimple_2188();
void WEIGHTED_ROUND_ROBIN_Joiner_2184();
void WEIGHTED_ROUND_ROBIN_Splitter_2189();
void FFTReorderSimple_2191();
void FFTReorderSimple_2192();
void FFTReorderSimple_2193();
void FFTReorderSimple_2194();
void FFTReorderSimple_2195();
void FFTReorderSimple_2196();
void FFTReorderSimple_2197();
void FFTReorderSimple_2198();
void WEIGHTED_ROUND_ROBIN_Joiner_2190();
void WEIGHTED_ROUND_ROBIN_Splitter_2199();
void FFTReorderSimple_2201();
void FFTReorderSimple_2202();
void FFTReorderSimple_2203();
void FFTReorderSimple_2204();
void FFTReorderSimple_2205();
void FFTReorderSimple_2206();
void FFTReorderSimple_2207();
void FFTReorderSimple_2208();
void FFTReorderSimple_2209();
void FFTReorderSimple_2210();
void FFTReorderSimple_2211();
void FFTReorderSimple_2212();
void FFTReorderSimple_2213();
void FFTReorderSimple_2214();
void FFTReorderSimple_2215();
void FFTReorderSimple_2216();
void WEIGHTED_ROUND_ROBIN_Joiner_2200();
void WEIGHTED_ROUND_ROBIN_Splitter_2217();
void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout);
void CombineDFT_2219();
void CombineDFT_2220();
void CombineDFT_2221();
void CombineDFT_2222();
void CombineDFT_2223();
void CombineDFT_2224();
void CombineDFT_2225();
void CombineDFT_2226();
void CombineDFT_2227();
void CombineDFT_2228();
void CombineDFT_2229();
void CombineDFT_2230();
void CombineDFT_2231();
void CombineDFT_2232();
void CombineDFT_2233();
void CombineDFT_2234();
void CombineDFT_2235();
void CombineDFT_2236();
void CombineDFT_2237();
void CombineDFT_2238();
void CombineDFT_2239();
void CombineDFT_2240();
void CombineDFT_2241();
void WEIGHTED_ROUND_ROBIN_Joiner_2218();
void WEIGHTED_ROUND_ROBIN_Splitter_2242();
void CombineDFT_2244();
void CombineDFT_2245();
void CombineDFT_2246();
void CombineDFT_2247();
void CombineDFT_2248();
void CombineDFT_2249();
void CombineDFT_2250();
void CombineDFT_2251();
void CombineDFT_2252();
void CombineDFT_2253();
void CombineDFT_2254();
void CombineDFT_2255();
void CombineDFT_2256();
void CombineDFT_2257();
void CombineDFT_2258();
void CombineDFT_2259();
void WEIGHTED_ROUND_ROBIN_Joiner_2243();
void WEIGHTED_ROUND_ROBIN_Splitter_2260();
void CombineDFT_2262();
void CombineDFT_2263();
void CombineDFT_2264();
void CombineDFT_2265();
void CombineDFT_2266();
void CombineDFT_2267();
void CombineDFT_2268();
void CombineDFT_2269();
void WEIGHTED_ROUND_ROBIN_Joiner_2261();
void WEIGHTED_ROUND_ROBIN_Splitter_2270();
void CombineDFT_2272();
void CombineDFT_2273();
void CombineDFT_2274();
void CombineDFT_2275();
void WEIGHTED_ROUND_ROBIN_Joiner_2271();
void WEIGHTED_ROUND_ROBIN_Splitter_2276();
void CombineDFT_2278();
void CombineDFT_2279();
void WEIGHTED_ROUND_ROBIN_Joiner_2277();
void CombineDFT_2176();
void CPrinter(buffer_complex_t *chanin);
void CPrinter_2177();

#ifdef __cplusplus
}
#endif
#endif
