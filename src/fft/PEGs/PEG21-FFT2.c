#include "PEG21-FFT2.h"

buffer_float_t SplitJoin12_CombineDFT_Fiss_5680_5701_split[21];
buffer_float_t SplitJoin111_CombineDFT_Fiss_5693_5714_split[2];
buffer_float_t SplitJoin95_FFTReorderSimple_Fiss_5685_5706_split[2];
buffer_float_t SplitJoin0_FFTTestSource_Fiss_5674_5695_join[2];
buffer_float_t SplitJoin101_FFTReorderSimple_Fiss_5688_5709_join[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5477WEIGHTED_ROUND_ROBIN_Splitter_5480;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5655WEIGHTED_ROUND_ROBIN_Splitter_5664;
buffer_float_t SplitJoin97_FFTReorderSimple_Fiss_5686_5707_split[4];
buffer_float_t SplitJoin107_CombineDFT_Fiss_5691_5712_join[8];
buffer_float_t SplitJoin14_CombineDFT_Fiss_5681_5702_join[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5572CombineDFT_5451;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5515WEIGHTED_ROUND_ROBIN_Splitter_5537;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5637WEIGHTED_ROUND_ROBIN_Splitter_5654;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5596WEIGHTED_ROUND_ROBIN_Splitter_5613;
buffer_float_t SplitJoin95_FFTReorderSimple_Fiss_5685_5706_join[2];
buffer_float_t SplitJoin107_CombineDFT_Fiss_5691_5712_split[8];
buffer_float_t FFTReorderSimple_5441WEIGHTED_ROUND_ROBIN_Splitter_5476;
buffer_float_t SplitJoin109_CombineDFT_Fiss_5692_5713_join[4];
buffer_float_t SplitJoin16_CombineDFT_Fiss_5682_5703_split[8];
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_5679_5700_split[16];
buffer_float_t SplitJoin18_CombineDFT_Fiss_5683_5704_split[4];
buffer_float_t FFTReorderSimple_5452WEIGHTED_ROUND_ROBIN_Splitter_5575;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5481WEIGHTED_ROUND_ROBIN_Splitter_5486;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5586WEIGHTED_ROUND_ROBIN_Splitter_5595;
buffer_float_t SplitJoin105_CombineDFT_Fiss_5690_5711_split[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5671CombineDFT_5462;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5566WEIGHTED_ROUND_ROBIN_Splitter_5571;
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_5678_5699_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5465FloatPrinter_5463;
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_5430_5466_5675_5696_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5556WEIGHTED_ROUND_ROBIN_Splitter_5565;
buffer_float_t SplitJoin109_CombineDFT_Fiss_5692_5713_split[4];
buffer_float_t SplitJoin101_FFTReorderSimple_Fiss_5688_5709_split[16];
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_5678_5699_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5487WEIGHTED_ROUND_ROBIN_Splitter_5496;
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_5676_5697_split[2];
buffer_float_t SplitJoin12_CombineDFT_Fiss_5680_5701_join[21];
buffer_float_t SplitJoin97_FFTReorderSimple_Fiss_5686_5707_join[4];
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_5677_5698_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5497WEIGHTED_ROUND_ROBIN_Splitter_5514;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5538WEIGHTED_ROUND_ROBIN_Splitter_5555;
buffer_float_t SplitJoin103_CombineDFT_Fiss_5689_5710_join[21];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5614WEIGHTED_ROUND_ROBIN_Splitter_5636;
buffer_float_t SplitJoin18_CombineDFT_Fiss_5683_5704_join[4];
buffer_float_t SplitJoin0_FFTTestSource_Fiss_5674_5695_split[2];
buffer_float_t SplitJoin20_CombineDFT_Fiss_5684_5705_split[2];
buffer_float_t SplitJoin14_CombineDFT_Fiss_5681_5702_split[16];
buffer_float_t SplitJoin111_CombineDFT_Fiss_5693_5714_join[2];
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_5677_5698_join[4];
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_5679_5700_join[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5576WEIGHTED_ROUND_ROBIN_Splitter_5579;
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_5676_5697_join[2];
buffer_float_t SplitJoin105_CombineDFT_Fiss_5690_5711_join[16];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_5430_5466_5675_5696_split[2];
buffer_float_t SplitJoin99_FFTReorderSimple_Fiss_5687_5708_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5665WEIGHTED_ROUND_ROBIN_Splitter_5670;
buffer_float_t SplitJoin20_CombineDFT_Fiss_5684_5705_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5473WEIGHTED_ROUND_ROBIN_Splitter_5464;
buffer_float_t SplitJoin99_FFTReorderSimple_Fiss_5687_5708_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5580WEIGHTED_ROUND_ROBIN_Splitter_5585;
buffer_float_t SplitJoin103_CombineDFT_Fiss_5689_5710_split[21];
buffer_float_t SplitJoin16_CombineDFT_Fiss_5682_5703_join[8];


CombineDFT_5516_t CombineDFT_5516_s;
CombineDFT_5516_t CombineDFT_5517_s;
CombineDFT_5516_t CombineDFT_5518_s;
CombineDFT_5516_t CombineDFT_5519_s;
CombineDFT_5516_t CombineDFT_5520_s;
CombineDFT_5516_t CombineDFT_5521_s;
CombineDFT_5516_t CombineDFT_5522_s;
CombineDFT_5516_t CombineDFT_5523_s;
CombineDFT_5516_t CombineDFT_5524_s;
CombineDFT_5516_t CombineDFT_5525_s;
CombineDFT_5516_t CombineDFT_5526_s;
CombineDFT_5516_t CombineDFT_5527_s;
CombineDFT_5516_t CombineDFT_5528_s;
CombineDFT_5516_t CombineDFT_5529_s;
CombineDFT_5516_t CombineDFT_5530_s;
CombineDFT_5516_t CombineDFT_5531_s;
CombineDFT_5516_t CombineDFT_5532_s;
CombineDFT_5516_t CombineDFT_5533_s;
CombineDFT_5516_t CombineDFT_5534_s;
CombineDFT_5516_t CombineDFT_5535_s;
CombineDFT_5516_t CombineDFT_5536_s;
CombineDFT_5539_t CombineDFT_5539_s;
CombineDFT_5539_t CombineDFT_5540_s;
CombineDFT_5539_t CombineDFT_5541_s;
CombineDFT_5539_t CombineDFT_5542_s;
CombineDFT_5539_t CombineDFT_5543_s;
CombineDFT_5539_t CombineDFT_5544_s;
CombineDFT_5539_t CombineDFT_5545_s;
CombineDFT_5539_t CombineDFT_5546_s;
CombineDFT_5539_t CombineDFT_5547_s;
CombineDFT_5539_t CombineDFT_5548_s;
CombineDFT_5539_t CombineDFT_5549_s;
CombineDFT_5539_t CombineDFT_5550_s;
CombineDFT_5539_t CombineDFT_5551_s;
CombineDFT_5539_t CombineDFT_5552_s;
CombineDFT_5539_t CombineDFT_5553_s;
CombineDFT_5539_t CombineDFT_5554_s;
CombineDFT_5557_t CombineDFT_5557_s;
CombineDFT_5557_t CombineDFT_5558_s;
CombineDFT_5557_t CombineDFT_5559_s;
CombineDFT_5557_t CombineDFT_5560_s;
CombineDFT_5557_t CombineDFT_5561_s;
CombineDFT_5557_t CombineDFT_5562_s;
CombineDFT_5557_t CombineDFT_5563_s;
CombineDFT_5557_t CombineDFT_5564_s;
CombineDFT_5567_t CombineDFT_5567_s;
CombineDFT_5567_t CombineDFT_5568_s;
CombineDFT_5567_t CombineDFT_5569_s;
CombineDFT_5567_t CombineDFT_5570_s;
CombineDFT_5573_t CombineDFT_5573_s;
CombineDFT_5573_t CombineDFT_5574_s;
CombineDFT_5451_t CombineDFT_5451_s;
CombineDFT_5516_t CombineDFT_5615_s;
CombineDFT_5516_t CombineDFT_5616_s;
CombineDFT_5516_t CombineDFT_5617_s;
CombineDFT_5516_t CombineDFT_5618_s;
CombineDFT_5516_t CombineDFT_5619_s;
CombineDFT_5516_t CombineDFT_5620_s;
CombineDFT_5516_t CombineDFT_5621_s;
CombineDFT_5516_t CombineDFT_5622_s;
CombineDFT_5516_t CombineDFT_5623_s;
CombineDFT_5516_t CombineDFT_5624_s;
CombineDFT_5516_t CombineDFT_5625_s;
CombineDFT_5516_t CombineDFT_5626_s;
CombineDFT_5516_t CombineDFT_5627_s;
CombineDFT_5516_t CombineDFT_5628_s;
CombineDFT_5516_t CombineDFT_5629_s;
CombineDFT_5516_t CombineDFT_5630_s;
CombineDFT_5516_t CombineDFT_5631_s;
CombineDFT_5516_t CombineDFT_5632_s;
CombineDFT_5516_t CombineDFT_5633_s;
CombineDFT_5516_t CombineDFT_5634_s;
CombineDFT_5516_t CombineDFT_5635_s;
CombineDFT_5539_t CombineDFT_5638_s;
CombineDFT_5539_t CombineDFT_5639_s;
CombineDFT_5539_t CombineDFT_5640_s;
CombineDFT_5539_t CombineDFT_5641_s;
CombineDFT_5539_t CombineDFT_5642_s;
CombineDFT_5539_t CombineDFT_5643_s;
CombineDFT_5539_t CombineDFT_5644_s;
CombineDFT_5539_t CombineDFT_5645_s;
CombineDFT_5539_t CombineDFT_5646_s;
CombineDFT_5539_t CombineDFT_5647_s;
CombineDFT_5539_t CombineDFT_5648_s;
CombineDFT_5539_t CombineDFT_5649_s;
CombineDFT_5539_t CombineDFT_5650_s;
CombineDFT_5539_t CombineDFT_5651_s;
CombineDFT_5539_t CombineDFT_5652_s;
CombineDFT_5539_t CombineDFT_5653_s;
CombineDFT_5557_t CombineDFT_5656_s;
CombineDFT_5557_t CombineDFT_5657_s;
CombineDFT_5557_t CombineDFT_5658_s;
CombineDFT_5557_t CombineDFT_5659_s;
CombineDFT_5557_t CombineDFT_5660_s;
CombineDFT_5557_t CombineDFT_5661_s;
CombineDFT_5557_t CombineDFT_5662_s;
CombineDFT_5557_t CombineDFT_5663_s;
CombineDFT_5567_t CombineDFT_5666_s;
CombineDFT_5567_t CombineDFT_5667_s;
CombineDFT_5567_t CombineDFT_5668_s;
CombineDFT_5567_t CombineDFT_5669_s;
CombineDFT_5573_t CombineDFT_5672_s;
CombineDFT_5573_t CombineDFT_5673_s;
CombineDFT_5451_t CombineDFT_5462_s;

void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), 0.0) ; 
		push_float(&(*chanout), 0.0) ; 
		push_float(&(*chanout), 1.0) ; 
		push_float(&(*chanout), 0.0) ; 
		FOR(int, i, 0,  < , 124, i++) {
			push_float(&(*chanout), 0.0) ; 
		}
		ENDFOR
	}


void FFTTestSource_5474() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_5674_5695_split[0]), &(SplitJoin0_FFTTestSource_Fiss_5674_5695_join[0]));
	ENDFOR
}

void FFTTestSource_5475() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_5674_5695_split[1]), &(SplitJoin0_FFTTestSource_Fiss_5674_5695_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5472() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_5473() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5473WEIGHTED_ROUND_ROBIN_Splitter_5464, pop_float(&SplitJoin0_FFTTestSource_Fiss_5674_5695_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5473WEIGHTED_ROUND_ROBIN_Splitter_5464, pop_float(&SplitJoin0_FFTTestSource_Fiss_5674_5695_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, i, 0,  < , 128, i = (i + 4)) {
			push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 128, i = (i + 4)) {
			push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_float(&(*chanin)) ; 
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void FFTReorderSimple_5441() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_5430_5466_5675_5696_split[0]), &(FFTReorderSimple_5441WEIGHTED_ROUND_ROBIN_Splitter_5476));
	ENDFOR
}

void FFTReorderSimple_5478() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_5676_5697_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_5676_5697_join[0]));
	ENDFOR
}

void FFTReorderSimple_5479() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_5676_5697_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_5676_5697_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5476() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_5676_5697_split[0], pop_float(&FFTReorderSimple_5441WEIGHTED_ROUND_ROBIN_Splitter_5476));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_5676_5697_split[1], pop_float(&FFTReorderSimple_5441WEIGHTED_ROUND_ROBIN_Splitter_5476));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5477() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5477WEIGHTED_ROUND_ROBIN_Splitter_5480, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_5676_5697_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5477WEIGHTED_ROUND_ROBIN_Splitter_5480, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_5676_5697_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_5482() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_5677_5698_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_5677_5698_join[0]));
	ENDFOR
}

void FFTReorderSimple_5483() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_5677_5698_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_5677_5698_join[1]));
	ENDFOR
}

void FFTReorderSimple_5484() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_5677_5698_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_5677_5698_join[2]));
	ENDFOR
}

void FFTReorderSimple_5485() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_5677_5698_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_5677_5698_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5480() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin6_FFTReorderSimple_Fiss_5677_5698_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5477WEIGHTED_ROUND_ROBIN_Splitter_5480));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5481() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5481WEIGHTED_ROUND_ROBIN_Splitter_5486, pop_float(&SplitJoin6_FFTReorderSimple_Fiss_5677_5698_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_5488() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_5678_5699_split[0]), &(SplitJoin8_FFTReorderSimple_Fiss_5678_5699_join[0]));
	ENDFOR
}

void FFTReorderSimple_5489() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_5678_5699_split[1]), &(SplitJoin8_FFTReorderSimple_Fiss_5678_5699_join[1]));
	ENDFOR
}

void FFTReorderSimple_5490() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_5678_5699_split[2]), &(SplitJoin8_FFTReorderSimple_Fiss_5678_5699_join[2]));
	ENDFOR
}

void FFTReorderSimple_5491() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_5678_5699_split[3]), &(SplitJoin8_FFTReorderSimple_Fiss_5678_5699_join[3]));
	ENDFOR
}

void FFTReorderSimple_5492() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_5678_5699_split[4]), &(SplitJoin8_FFTReorderSimple_Fiss_5678_5699_join[4]));
	ENDFOR
}

void FFTReorderSimple_5493() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_5678_5699_split[5]), &(SplitJoin8_FFTReorderSimple_Fiss_5678_5699_join[5]));
	ENDFOR
}

void FFTReorderSimple_5494() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_5678_5699_split[6]), &(SplitJoin8_FFTReorderSimple_Fiss_5678_5699_join[6]));
	ENDFOR
}

void FFTReorderSimple_5495() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_5678_5699_split[7]), &(SplitJoin8_FFTReorderSimple_Fiss_5678_5699_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5486() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin8_FFTReorderSimple_Fiss_5678_5699_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5481WEIGHTED_ROUND_ROBIN_Splitter_5486));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5487() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5487WEIGHTED_ROUND_ROBIN_Splitter_5496, pop_float(&SplitJoin8_FFTReorderSimple_Fiss_5678_5699_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_5498() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_split[0]), &(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_join[0]));
	ENDFOR
}

void FFTReorderSimple_5499() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_split[1]), &(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_join[1]));
	ENDFOR
}

void FFTReorderSimple_5500() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_split[2]), &(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_join[2]));
	ENDFOR
}

void FFTReorderSimple_5501() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_split[3]), &(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_join[3]));
	ENDFOR
}

void FFTReorderSimple_5502() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_split[4]), &(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_join[4]));
	ENDFOR
}

void FFTReorderSimple_5503() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_split[5]), &(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_join[5]));
	ENDFOR
}

void FFTReorderSimple_5504() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_split[6]), &(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_join[6]));
	ENDFOR
}

void FFTReorderSimple_5505() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_split[7]), &(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_join[7]));
	ENDFOR
}

void FFTReorderSimple_5506() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_split[8]), &(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_join[8]));
	ENDFOR
}

void FFTReorderSimple_5507() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_split[9]), &(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_join[9]));
	ENDFOR
}

void FFTReorderSimple_5508() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_split[10]), &(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_join[10]));
	ENDFOR
}

void FFTReorderSimple_5509() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_split[11]), &(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_join[11]));
	ENDFOR
}

void FFTReorderSimple_5510() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_split[12]), &(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_join[12]));
	ENDFOR
}

void FFTReorderSimple_5511() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_split[13]), &(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_join[13]));
	ENDFOR
}

void FFTReorderSimple_5512() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_split[14]), &(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_join[14]));
	ENDFOR
}

void FFTReorderSimple_5513() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_split[15]), &(SplitJoin10_FFTReorderSimple_Fiss_5679_5700_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5496() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_FFTReorderSimple_Fiss_5679_5700_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5487WEIGHTED_ROUND_ROBIN_Splitter_5496));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5497() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5497WEIGHTED_ROUND_ROBIN_Splitter_5514, pop_float(&SplitJoin10_FFTReorderSimple_Fiss_5679_5700_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&(*chanin), i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&(*chanin), i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&(*chanin), (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&(*chanin), (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5516_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5516_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&(*chanin)) ; 
			push_float(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_5516() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5680_5701_split[0]), &(SplitJoin12_CombineDFT_Fiss_5680_5701_join[0]));
	ENDFOR
}

void CombineDFT_5517() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5680_5701_split[1]), &(SplitJoin12_CombineDFT_Fiss_5680_5701_join[1]));
	ENDFOR
}

void CombineDFT_5518() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5680_5701_split[2]), &(SplitJoin12_CombineDFT_Fiss_5680_5701_join[2]));
	ENDFOR
}

void CombineDFT_5519() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5680_5701_split[3]), &(SplitJoin12_CombineDFT_Fiss_5680_5701_join[3]));
	ENDFOR
}

void CombineDFT_5520() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5680_5701_split[4]), &(SplitJoin12_CombineDFT_Fiss_5680_5701_join[4]));
	ENDFOR
}

void CombineDFT_5521() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5680_5701_split[5]), &(SplitJoin12_CombineDFT_Fiss_5680_5701_join[5]));
	ENDFOR
}

void CombineDFT_5522() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5680_5701_split[6]), &(SplitJoin12_CombineDFT_Fiss_5680_5701_join[6]));
	ENDFOR
}

void CombineDFT_5523() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5680_5701_split[7]), &(SplitJoin12_CombineDFT_Fiss_5680_5701_join[7]));
	ENDFOR
}

void CombineDFT_5524() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5680_5701_split[8]), &(SplitJoin12_CombineDFT_Fiss_5680_5701_join[8]));
	ENDFOR
}

void CombineDFT_5525() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5680_5701_split[9]), &(SplitJoin12_CombineDFT_Fiss_5680_5701_join[9]));
	ENDFOR
}

void CombineDFT_5526() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5680_5701_split[10]), &(SplitJoin12_CombineDFT_Fiss_5680_5701_join[10]));
	ENDFOR
}

void CombineDFT_5527() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5680_5701_split[11]), &(SplitJoin12_CombineDFT_Fiss_5680_5701_join[11]));
	ENDFOR
}

void CombineDFT_5528() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5680_5701_split[12]), &(SplitJoin12_CombineDFT_Fiss_5680_5701_join[12]));
	ENDFOR
}

void CombineDFT_5529() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5680_5701_split[13]), &(SplitJoin12_CombineDFT_Fiss_5680_5701_join[13]));
	ENDFOR
}

void CombineDFT_5530() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5680_5701_split[14]), &(SplitJoin12_CombineDFT_Fiss_5680_5701_join[14]));
	ENDFOR
}

void CombineDFT_5531() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5680_5701_split[15]), &(SplitJoin12_CombineDFT_Fiss_5680_5701_join[15]));
	ENDFOR
}

void CombineDFT_5532() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5680_5701_split[16]), &(SplitJoin12_CombineDFT_Fiss_5680_5701_join[16]));
	ENDFOR
}

void CombineDFT_5533() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5680_5701_split[17]), &(SplitJoin12_CombineDFT_Fiss_5680_5701_join[17]));
	ENDFOR
}

void CombineDFT_5534() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5680_5701_split[18]), &(SplitJoin12_CombineDFT_Fiss_5680_5701_join[18]));
	ENDFOR
}

void CombineDFT_5535() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5680_5701_split[19]), &(SplitJoin12_CombineDFT_Fiss_5680_5701_join[19]));
	ENDFOR
}

void CombineDFT_5536() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5680_5701_split[20]), &(SplitJoin12_CombineDFT_Fiss_5680_5701_join[20]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5514() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 21, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin12_CombineDFT_Fiss_5680_5701_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5497WEIGHTED_ROUND_ROBIN_Splitter_5514));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5515() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 21, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5515WEIGHTED_ROUND_ROBIN_Splitter_5537, pop_float(&SplitJoin12_CombineDFT_Fiss_5680_5701_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5539() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_5681_5702_split[0]), &(SplitJoin14_CombineDFT_Fiss_5681_5702_join[0]));
	ENDFOR
}

void CombineDFT_5540() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_5681_5702_split[1]), &(SplitJoin14_CombineDFT_Fiss_5681_5702_join[1]));
	ENDFOR
}

void CombineDFT_5541() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_5681_5702_split[2]), &(SplitJoin14_CombineDFT_Fiss_5681_5702_join[2]));
	ENDFOR
}

void CombineDFT_5542() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_5681_5702_split[3]), &(SplitJoin14_CombineDFT_Fiss_5681_5702_join[3]));
	ENDFOR
}

void CombineDFT_5543() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_5681_5702_split[4]), &(SplitJoin14_CombineDFT_Fiss_5681_5702_join[4]));
	ENDFOR
}

void CombineDFT_5544() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_5681_5702_split[5]), &(SplitJoin14_CombineDFT_Fiss_5681_5702_join[5]));
	ENDFOR
}

void CombineDFT_5545() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_5681_5702_split[6]), &(SplitJoin14_CombineDFT_Fiss_5681_5702_join[6]));
	ENDFOR
}

void CombineDFT_5546() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_5681_5702_split[7]), &(SplitJoin14_CombineDFT_Fiss_5681_5702_join[7]));
	ENDFOR
}

void CombineDFT_5547() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_5681_5702_split[8]), &(SplitJoin14_CombineDFT_Fiss_5681_5702_join[8]));
	ENDFOR
}

void CombineDFT_5548() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_5681_5702_split[9]), &(SplitJoin14_CombineDFT_Fiss_5681_5702_join[9]));
	ENDFOR
}

void CombineDFT_5549() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_5681_5702_split[10]), &(SplitJoin14_CombineDFT_Fiss_5681_5702_join[10]));
	ENDFOR
}

void CombineDFT_5550() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_5681_5702_split[11]), &(SplitJoin14_CombineDFT_Fiss_5681_5702_join[11]));
	ENDFOR
}

void CombineDFT_5551() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_5681_5702_split[12]), &(SplitJoin14_CombineDFT_Fiss_5681_5702_join[12]));
	ENDFOR
}

void CombineDFT_5552() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_5681_5702_split[13]), &(SplitJoin14_CombineDFT_Fiss_5681_5702_join[13]));
	ENDFOR
}

void CombineDFT_5553() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_5681_5702_split[14]), &(SplitJoin14_CombineDFT_Fiss_5681_5702_join[14]));
	ENDFOR
}

void CombineDFT_5554() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_5681_5702_split[15]), &(SplitJoin14_CombineDFT_Fiss_5681_5702_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5537() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin14_CombineDFT_Fiss_5681_5702_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5515WEIGHTED_ROUND_ROBIN_Splitter_5537));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5538() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5538WEIGHTED_ROUND_ROBIN_Splitter_5555, pop_float(&SplitJoin14_CombineDFT_Fiss_5681_5702_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5557() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_5682_5703_split[0]), &(SplitJoin16_CombineDFT_Fiss_5682_5703_join[0]));
	ENDFOR
}

void CombineDFT_5558() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_5682_5703_split[1]), &(SplitJoin16_CombineDFT_Fiss_5682_5703_join[1]));
	ENDFOR
}

void CombineDFT_5559() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_5682_5703_split[2]), &(SplitJoin16_CombineDFT_Fiss_5682_5703_join[2]));
	ENDFOR
}

void CombineDFT_5560() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_5682_5703_split[3]), &(SplitJoin16_CombineDFT_Fiss_5682_5703_join[3]));
	ENDFOR
}

void CombineDFT_5561() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_5682_5703_split[4]), &(SplitJoin16_CombineDFT_Fiss_5682_5703_join[4]));
	ENDFOR
}

void CombineDFT_5562() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_5682_5703_split[5]), &(SplitJoin16_CombineDFT_Fiss_5682_5703_join[5]));
	ENDFOR
}

void CombineDFT_5563() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_5682_5703_split[6]), &(SplitJoin16_CombineDFT_Fiss_5682_5703_join[6]));
	ENDFOR
}

void CombineDFT_5564() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_5682_5703_split[7]), &(SplitJoin16_CombineDFT_Fiss_5682_5703_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5555() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin16_CombineDFT_Fiss_5682_5703_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5538WEIGHTED_ROUND_ROBIN_Splitter_5555));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5556() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5556WEIGHTED_ROUND_ROBIN_Splitter_5565, pop_float(&SplitJoin16_CombineDFT_Fiss_5682_5703_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5567() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_5683_5704_split[0]), &(SplitJoin18_CombineDFT_Fiss_5683_5704_join[0]));
	ENDFOR
}

void CombineDFT_5568() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_5683_5704_split[1]), &(SplitJoin18_CombineDFT_Fiss_5683_5704_join[1]));
	ENDFOR
}

void CombineDFT_5569() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_5683_5704_split[2]), &(SplitJoin18_CombineDFT_Fiss_5683_5704_join[2]));
	ENDFOR
}

void CombineDFT_5570() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_5683_5704_split[3]), &(SplitJoin18_CombineDFT_Fiss_5683_5704_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5565() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin18_CombineDFT_Fiss_5683_5704_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5556WEIGHTED_ROUND_ROBIN_Splitter_5565));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5566() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5566WEIGHTED_ROUND_ROBIN_Splitter_5571, pop_float(&SplitJoin18_CombineDFT_Fiss_5683_5704_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5573() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin20_CombineDFT_Fiss_5684_5705_split[0]), &(SplitJoin20_CombineDFT_Fiss_5684_5705_join[0]));
	ENDFOR
}

void CombineDFT_5574() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin20_CombineDFT_Fiss_5684_5705_split[1]), &(SplitJoin20_CombineDFT_Fiss_5684_5705_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5571() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_5684_5705_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5566WEIGHTED_ROUND_ROBIN_Splitter_5571));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_5684_5705_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5566WEIGHTED_ROUND_ROBIN_Splitter_5571));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5572() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5572CombineDFT_5451, pop_float(&SplitJoin20_CombineDFT_Fiss_5684_5705_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5572CombineDFT_5451, pop_float(&SplitJoin20_CombineDFT_Fiss_5684_5705_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_5451() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_5572CombineDFT_5451), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_5430_5466_5675_5696_join[0]));
	ENDFOR
}

void FFTReorderSimple_5452() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_5430_5466_5675_5696_split[1]), &(FFTReorderSimple_5452WEIGHTED_ROUND_ROBIN_Splitter_5575));
	ENDFOR
}

void FFTReorderSimple_5577() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin95_FFTReorderSimple_Fiss_5685_5706_split[0]), &(SplitJoin95_FFTReorderSimple_Fiss_5685_5706_join[0]));
	ENDFOR
}

void FFTReorderSimple_5578() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin95_FFTReorderSimple_Fiss_5685_5706_split[1]), &(SplitJoin95_FFTReorderSimple_Fiss_5685_5706_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5575() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin95_FFTReorderSimple_Fiss_5685_5706_split[0], pop_float(&FFTReorderSimple_5452WEIGHTED_ROUND_ROBIN_Splitter_5575));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin95_FFTReorderSimple_Fiss_5685_5706_split[1], pop_float(&FFTReorderSimple_5452WEIGHTED_ROUND_ROBIN_Splitter_5575));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5576() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5576WEIGHTED_ROUND_ROBIN_Splitter_5579, pop_float(&SplitJoin95_FFTReorderSimple_Fiss_5685_5706_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5576WEIGHTED_ROUND_ROBIN_Splitter_5579, pop_float(&SplitJoin95_FFTReorderSimple_Fiss_5685_5706_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_5581() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin97_FFTReorderSimple_Fiss_5686_5707_split[0]), &(SplitJoin97_FFTReorderSimple_Fiss_5686_5707_join[0]));
	ENDFOR
}

void FFTReorderSimple_5582() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin97_FFTReorderSimple_Fiss_5686_5707_split[1]), &(SplitJoin97_FFTReorderSimple_Fiss_5686_5707_join[1]));
	ENDFOR
}

void FFTReorderSimple_5583() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin97_FFTReorderSimple_Fiss_5686_5707_split[2]), &(SplitJoin97_FFTReorderSimple_Fiss_5686_5707_join[2]));
	ENDFOR
}

void FFTReorderSimple_5584() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin97_FFTReorderSimple_Fiss_5686_5707_split[3]), &(SplitJoin97_FFTReorderSimple_Fiss_5686_5707_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5579() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin97_FFTReorderSimple_Fiss_5686_5707_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5576WEIGHTED_ROUND_ROBIN_Splitter_5579));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5580() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5580WEIGHTED_ROUND_ROBIN_Splitter_5585, pop_float(&SplitJoin97_FFTReorderSimple_Fiss_5686_5707_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_5587() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin99_FFTReorderSimple_Fiss_5687_5708_split[0]), &(SplitJoin99_FFTReorderSimple_Fiss_5687_5708_join[0]));
	ENDFOR
}

void FFTReorderSimple_5588() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin99_FFTReorderSimple_Fiss_5687_5708_split[1]), &(SplitJoin99_FFTReorderSimple_Fiss_5687_5708_join[1]));
	ENDFOR
}

void FFTReorderSimple_5589() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin99_FFTReorderSimple_Fiss_5687_5708_split[2]), &(SplitJoin99_FFTReorderSimple_Fiss_5687_5708_join[2]));
	ENDFOR
}

void FFTReorderSimple_5590() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin99_FFTReorderSimple_Fiss_5687_5708_split[3]), &(SplitJoin99_FFTReorderSimple_Fiss_5687_5708_join[3]));
	ENDFOR
}

void FFTReorderSimple_5591() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin99_FFTReorderSimple_Fiss_5687_5708_split[4]), &(SplitJoin99_FFTReorderSimple_Fiss_5687_5708_join[4]));
	ENDFOR
}

void FFTReorderSimple_5592() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin99_FFTReorderSimple_Fiss_5687_5708_split[5]), &(SplitJoin99_FFTReorderSimple_Fiss_5687_5708_join[5]));
	ENDFOR
}

void FFTReorderSimple_5593() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin99_FFTReorderSimple_Fiss_5687_5708_split[6]), &(SplitJoin99_FFTReorderSimple_Fiss_5687_5708_join[6]));
	ENDFOR
}

void FFTReorderSimple_5594() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin99_FFTReorderSimple_Fiss_5687_5708_split[7]), &(SplitJoin99_FFTReorderSimple_Fiss_5687_5708_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5585() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin99_FFTReorderSimple_Fiss_5687_5708_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5580WEIGHTED_ROUND_ROBIN_Splitter_5585));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5586() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5586WEIGHTED_ROUND_ROBIN_Splitter_5595, pop_float(&SplitJoin99_FFTReorderSimple_Fiss_5687_5708_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_5597() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_split[0]), &(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_join[0]));
	ENDFOR
}

void FFTReorderSimple_5598() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_split[1]), &(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_join[1]));
	ENDFOR
}

void FFTReorderSimple_5599() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_split[2]), &(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_join[2]));
	ENDFOR
}

void FFTReorderSimple_5600() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_split[3]), &(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_join[3]));
	ENDFOR
}

void FFTReorderSimple_5601() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_split[4]), &(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_join[4]));
	ENDFOR
}

void FFTReorderSimple_5602() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_split[5]), &(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_join[5]));
	ENDFOR
}

void FFTReorderSimple_5603() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_split[6]), &(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_join[6]));
	ENDFOR
}

void FFTReorderSimple_5604() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_split[7]), &(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_join[7]));
	ENDFOR
}

void FFTReorderSimple_5605() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_split[8]), &(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_join[8]));
	ENDFOR
}

void FFTReorderSimple_5606() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_split[9]), &(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_join[9]));
	ENDFOR
}

void FFTReorderSimple_5607() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_split[10]), &(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_join[10]));
	ENDFOR
}

void FFTReorderSimple_5608() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_split[11]), &(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_join[11]));
	ENDFOR
}

void FFTReorderSimple_5609() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_split[12]), &(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_join[12]));
	ENDFOR
}

void FFTReorderSimple_5610() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_split[13]), &(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_join[13]));
	ENDFOR
}

void FFTReorderSimple_5611() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_split[14]), &(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_join[14]));
	ENDFOR
}

void FFTReorderSimple_5612() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_split[15]), &(SplitJoin101_FFTReorderSimple_Fiss_5688_5709_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5595() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin101_FFTReorderSimple_Fiss_5688_5709_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5586WEIGHTED_ROUND_ROBIN_Splitter_5595));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5596() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5596WEIGHTED_ROUND_ROBIN_Splitter_5613, pop_float(&SplitJoin101_FFTReorderSimple_Fiss_5688_5709_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5615() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin103_CombineDFT_Fiss_5689_5710_split[0]), &(SplitJoin103_CombineDFT_Fiss_5689_5710_join[0]));
	ENDFOR
}

void CombineDFT_5616() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin103_CombineDFT_Fiss_5689_5710_split[1]), &(SplitJoin103_CombineDFT_Fiss_5689_5710_join[1]));
	ENDFOR
}

void CombineDFT_5617() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin103_CombineDFT_Fiss_5689_5710_split[2]), &(SplitJoin103_CombineDFT_Fiss_5689_5710_join[2]));
	ENDFOR
}

void CombineDFT_5618() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin103_CombineDFT_Fiss_5689_5710_split[3]), &(SplitJoin103_CombineDFT_Fiss_5689_5710_join[3]));
	ENDFOR
}

void CombineDFT_5619() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin103_CombineDFT_Fiss_5689_5710_split[4]), &(SplitJoin103_CombineDFT_Fiss_5689_5710_join[4]));
	ENDFOR
}

void CombineDFT_5620() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin103_CombineDFT_Fiss_5689_5710_split[5]), &(SplitJoin103_CombineDFT_Fiss_5689_5710_join[5]));
	ENDFOR
}

void CombineDFT_5621() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin103_CombineDFT_Fiss_5689_5710_split[6]), &(SplitJoin103_CombineDFT_Fiss_5689_5710_join[6]));
	ENDFOR
}

void CombineDFT_5622() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin103_CombineDFT_Fiss_5689_5710_split[7]), &(SplitJoin103_CombineDFT_Fiss_5689_5710_join[7]));
	ENDFOR
}

void CombineDFT_5623() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin103_CombineDFT_Fiss_5689_5710_split[8]), &(SplitJoin103_CombineDFT_Fiss_5689_5710_join[8]));
	ENDFOR
}

void CombineDFT_5624() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin103_CombineDFT_Fiss_5689_5710_split[9]), &(SplitJoin103_CombineDFT_Fiss_5689_5710_join[9]));
	ENDFOR
}

void CombineDFT_5625() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin103_CombineDFT_Fiss_5689_5710_split[10]), &(SplitJoin103_CombineDFT_Fiss_5689_5710_join[10]));
	ENDFOR
}

void CombineDFT_5626() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin103_CombineDFT_Fiss_5689_5710_split[11]), &(SplitJoin103_CombineDFT_Fiss_5689_5710_join[11]));
	ENDFOR
}

void CombineDFT_5627() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin103_CombineDFT_Fiss_5689_5710_split[12]), &(SplitJoin103_CombineDFT_Fiss_5689_5710_join[12]));
	ENDFOR
}

void CombineDFT_5628() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin103_CombineDFT_Fiss_5689_5710_split[13]), &(SplitJoin103_CombineDFT_Fiss_5689_5710_join[13]));
	ENDFOR
}

void CombineDFT_5629() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin103_CombineDFT_Fiss_5689_5710_split[14]), &(SplitJoin103_CombineDFT_Fiss_5689_5710_join[14]));
	ENDFOR
}

void CombineDFT_5630() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin103_CombineDFT_Fiss_5689_5710_split[15]), &(SplitJoin103_CombineDFT_Fiss_5689_5710_join[15]));
	ENDFOR
}

void CombineDFT_5631() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin103_CombineDFT_Fiss_5689_5710_split[16]), &(SplitJoin103_CombineDFT_Fiss_5689_5710_join[16]));
	ENDFOR
}

void CombineDFT_5632() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin103_CombineDFT_Fiss_5689_5710_split[17]), &(SplitJoin103_CombineDFT_Fiss_5689_5710_join[17]));
	ENDFOR
}

void CombineDFT_5633() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin103_CombineDFT_Fiss_5689_5710_split[18]), &(SplitJoin103_CombineDFT_Fiss_5689_5710_join[18]));
	ENDFOR
}

void CombineDFT_5634() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin103_CombineDFT_Fiss_5689_5710_split[19]), &(SplitJoin103_CombineDFT_Fiss_5689_5710_join[19]));
	ENDFOR
}

void CombineDFT_5635() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin103_CombineDFT_Fiss_5689_5710_split[20]), &(SplitJoin103_CombineDFT_Fiss_5689_5710_join[20]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5613() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 21, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin103_CombineDFT_Fiss_5689_5710_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5596WEIGHTED_ROUND_ROBIN_Splitter_5613));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5614() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 21, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5614WEIGHTED_ROUND_ROBIN_Splitter_5636, pop_float(&SplitJoin103_CombineDFT_Fiss_5689_5710_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5638() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin105_CombineDFT_Fiss_5690_5711_split[0]), &(SplitJoin105_CombineDFT_Fiss_5690_5711_join[0]));
	ENDFOR
}

void CombineDFT_5639() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin105_CombineDFT_Fiss_5690_5711_split[1]), &(SplitJoin105_CombineDFT_Fiss_5690_5711_join[1]));
	ENDFOR
}

void CombineDFT_5640() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin105_CombineDFT_Fiss_5690_5711_split[2]), &(SplitJoin105_CombineDFT_Fiss_5690_5711_join[2]));
	ENDFOR
}

void CombineDFT_5641() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin105_CombineDFT_Fiss_5690_5711_split[3]), &(SplitJoin105_CombineDFT_Fiss_5690_5711_join[3]));
	ENDFOR
}

void CombineDFT_5642() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin105_CombineDFT_Fiss_5690_5711_split[4]), &(SplitJoin105_CombineDFT_Fiss_5690_5711_join[4]));
	ENDFOR
}

void CombineDFT_5643() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin105_CombineDFT_Fiss_5690_5711_split[5]), &(SplitJoin105_CombineDFT_Fiss_5690_5711_join[5]));
	ENDFOR
}

void CombineDFT_5644() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin105_CombineDFT_Fiss_5690_5711_split[6]), &(SplitJoin105_CombineDFT_Fiss_5690_5711_join[6]));
	ENDFOR
}

void CombineDFT_5645() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin105_CombineDFT_Fiss_5690_5711_split[7]), &(SplitJoin105_CombineDFT_Fiss_5690_5711_join[7]));
	ENDFOR
}

void CombineDFT_5646() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin105_CombineDFT_Fiss_5690_5711_split[8]), &(SplitJoin105_CombineDFT_Fiss_5690_5711_join[8]));
	ENDFOR
}

void CombineDFT_5647() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin105_CombineDFT_Fiss_5690_5711_split[9]), &(SplitJoin105_CombineDFT_Fiss_5690_5711_join[9]));
	ENDFOR
}

void CombineDFT_5648() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin105_CombineDFT_Fiss_5690_5711_split[10]), &(SplitJoin105_CombineDFT_Fiss_5690_5711_join[10]));
	ENDFOR
}

void CombineDFT_5649() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin105_CombineDFT_Fiss_5690_5711_split[11]), &(SplitJoin105_CombineDFT_Fiss_5690_5711_join[11]));
	ENDFOR
}

void CombineDFT_5650() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin105_CombineDFT_Fiss_5690_5711_split[12]), &(SplitJoin105_CombineDFT_Fiss_5690_5711_join[12]));
	ENDFOR
}

void CombineDFT_5651() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin105_CombineDFT_Fiss_5690_5711_split[13]), &(SplitJoin105_CombineDFT_Fiss_5690_5711_join[13]));
	ENDFOR
}

void CombineDFT_5652() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin105_CombineDFT_Fiss_5690_5711_split[14]), &(SplitJoin105_CombineDFT_Fiss_5690_5711_join[14]));
	ENDFOR
}

void CombineDFT_5653() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin105_CombineDFT_Fiss_5690_5711_split[15]), &(SplitJoin105_CombineDFT_Fiss_5690_5711_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5636() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin105_CombineDFT_Fiss_5690_5711_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5614WEIGHTED_ROUND_ROBIN_Splitter_5636));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5637() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5637WEIGHTED_ROUND_ROBIN_Splitter_5654, pop_float(&SplitJoin105_CombineDFT_Fiss_5690_5711_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5656() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin107_CombineDFT_Fiss_5691_5712_split[0]), &(SplitJoin107_CombineDFT_Fiss_5691_5712_join[0]));
	ENDFOR
}

void CombineDFT_5657() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin107_CombineDFT_Fiss_5691_5712_split[1]), &(SplitJoin107_CombineDFT_Fiss_5691_5712_join[1]));
	ENDFOR
}

void CombineDFT_5658() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin107_CombineDFT_Fiss_5691_5712_split[2]), &(SplitJoin107_CombineDFT_Fiss_5691_5712_join[2]));
	ENDFOR
}

void CombineDFT_5659() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin107_CombineDFT_Fiss_5691_5712_split[3]), &(SplitJoin107_CombineDFT_Fiss_5691_5712_join[3]));
	ENDFOR
}

void CombineDFT_5660() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin107_CombineDFT_Fiss_5691_5712_split[4]), &(SplitJoin107_CombineDFT_Fiss_5691_5712_join[4]));
	ENDFOR
}

void CombineDFT_5661() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin107_CombineDFT_Fiss_5691_5712_split[5]), &(SplitJoin107_CombineDFT_Fiss_5691_5712_join[5]));
	ENDFOR
}

void CombineDFT_5662() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin107_CombineDFT_Fiss_5691_5712_split[6]), &(SplitJoin107_CombineDFT_Fiss_5691_5712_join[6]));
	ENDFOR
}

void CombineDFT_5663() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin107_CombineDFT_Fiss_5691_5712_split[7]), &(SplitJoin107_CombineDFT_Fiss_5691_5712_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5654() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin107_CombineDFT_Fiss_5691_5712_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5637WEIGHTED_ROUND_ROBIN_Splitter_5654));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5655() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5655WEIGHTED_ROUND_ROBIN_Splitter_5664, pop_float(&SplitJoin107_CombineDFT_Fiss_5691_5712_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5666() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin109_CombineDFT_Fiss_5692_5713_split[0]), &(SplitJoin109_CombineDFT_Fiss_5692_5713_join[0]));
	ENDFOR
}

void CombineDFT_5667() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin109_CombineDFT_Fiss_5692_5713_split[1]), &(SplitJoin109_CombineDFT_Fiss_5692_5713_join[1]));
	ENDFOR
}

void CombineDFT_5668() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin109_CombineDFT_Fiss_5692_5713_split[2]), &(SplitJoin109_CombineDFT_Fiss_5692_5713_join[2]));
	ENDFOR
}

void CombineDFT_5669() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin109_CombineDFT_Fiss_5692_5713_split[3]), &(SplitJoin109_CombineDFT_Fiss_5692_5713_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5664() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin109_CombineDFT_Fiss_5692_5713_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5655WEIGHTED_ROUND_ROBIN_Splitter_5664));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5665() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5665WEIGHTED_ROUND_ROBIN_Splitter_5670, pop_float(&SplitJoin109_CombineDFT_Fiss_5692_5713_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5672() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin111_CombineDFT_Fiss_5693_5714_split[0]), &(SplitJoin111_CombineDFT_Fiss_5693_5714_join[0]));
	ENDFOR
}

void CombineDFT_5673() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin111_CombineDFT_Fiss_5693_5714_split[1]), &(SplitJoin111_CombineDFT_Fiss_5693_5714_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5670() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin111_CombineDFT_Fiss_5693_5714_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5665WEIGHTED_ROUND_ROBIN_Splitter_5670));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin111_CombineDFT_Fiss_5693_5714_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5665WEIGHTED_ROUND_ROBIN_Splitter_5670));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5671() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5671CombineDFT_5462, pop_float(&SplitJoin111_CombineDFT_Fiss_5693_5714_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5671CombineDFT_5462, pop_float(&SplitJoin111_CombineDFT_Fiss_5693_5714_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_5462() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_5671CombineDFT_5462), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_5430_5466_5675_5696_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5464() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_5430_5466_5675_5696_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5473WEIGHTED_ROUND_ROBIN_Splitter_5464));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_5430_5466_5675_5696_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5473WEIGHTED_ROUND_ROBIN_Splitter_5464));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5465() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5465FloatPrinter_5463, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_5430_5466_5675_5696_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5465FloatPrinter_5463, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_5430_5466_5675_5696_join[1]));
		ENDFOR
	ENDFOR
}}

void FloatPrinter(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void FloatPrinter_5463() {
	FOR(uint32_t, __iter_steady_, 0, <, 5376, __iter_steady_++)
		FloatPrinter(&(WEIGHTED_ROUND_ROBIN_Joiner_5465FloatPrinter_5463));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 21, __iter_init_0_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_5680_5701_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 2, __iter_init_1_++)
		init_buffer_float(&SplitJoin111_CombineDFT_Fiss_5693_5714_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_float(&SplitJoin95_FFTReorderSimple_Fiss_5685_5706_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 2, __iter_init_3_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_5674_5695_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 16, __iter_init_4_++)
		init_buffer_float(&SplitJoin101_FFTReorderSimple_Fiss_5688_5709_join[__iter_init_4_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5477WEIGHTED_ROUND_ROBIN_Splitter_5480);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5655WEIGHTED_ROUND_ROBIN_Splitter_5664);
	FOR(int, __iter_init_5_, 0, <, 4, __iter_init_5_++)
		init_buffer_float(&SplitJoin97_FFTReorderSimple_Fiss_5686_5707_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_float(&SplitJoin107_CombineDFT_Fiss_5691_5712_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 16, __iter_init_7_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_5681_5702_join[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5572CombineDFT_5451);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5515WEIGHTED_ROUND_ROBIN_Splitter_5537);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5637WEIGHTED_ROUND_ROBIN_Splitter_5654);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5596WEIGHTED_ROUND_ROBIN_Splitter_5613);
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_float(&SplitJoin95_FFTReorderSimple_Fiss_5685_5706_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_float(&SplitJoin107_CombineDFT_Fiss_5691_5712_split[__iter_init_9_]);
	ENDFOR
	init_buffer_float(&FFTReorderSimple_5441WEIGHTED_ROUND_ROBIN_Splitter_5476);
	FOR(int, __iter_init_10_, 0, <, 4, __iter_init_10_++)
		init_buffer_float(&SplitJoin109_CombineDFT_Fiss_5692_5713_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_5682_5703_split[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 16, __iter_init_12_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_5679_5700_split[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 4, __iter_init_13_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_5683_5704_split[__iter_init_13_]);
	ENDFOR
	init_buffer_float(&FFTReorderSimple_5452WEIGHTED_ROUND_ROBIN_Splitter_5575);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5481WEIGHTED_ROUND_ROBIN_Splitter_5486);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5586WEIGHTED_ROUND_ROBIN_Splitter_5595);
	FOR(int, __iter_init_14_, 0, <, 16, __iter_init_14_++)
		init_buffer_float(&SplitJoin105_CombineDFT_Fiss_5690_5711_split[__iter_init_14_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5671CombineDFT_5462);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5566WEIGHTED_ROUND_ROBIN_Splitter_5571);
	FOR(int, __iter_init_15_, 0, <, 8, __iter_init_15_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_5678_5699_join[__iter_init_15_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5465FloatPrinter_5463);
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_5430_5466_5675_5696_join[__iter_init_16_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5556WEIGHTED_ROUND_ROBIN_Splitter_5565);
	FOR(int, __iter_init_17_, 0, <, 4, __iter_init_17_++)
		init_buffer_float(&SplitJoin109_CombineDFT_Fiss_5692_5713_split[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 16, __iter_init_18_++)
		init_buffer_float(&SplitJoin101_FFTReorderSimple_Fiss_5688_5709_split[__iter_init_18_]);
	ENDFOR
	FOR(int, __iter_init_19_, 0, <, 8, __iter_init_19_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_5678_5699_split[__iter_init_19_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5487WEIGHTED_ROUND_ROBIN_Splitter_5496);
	FOR(int, __iter_init_20_, 0, <, 2, __iter_init_20_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_5676_5697_split[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 21, __iter_init_21_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_5680_5701_join[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 4, __iter_init_22_++)
		init_buffer_float(&SplitJoin97_FFTReorderSimple_Fiss_5686_5707_join[__iter_init_22_]);
	ENDFOR
	FOR(int, __iter_init_23_, 0, <, 4, __iter_init_23_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_5677_5698_split[__iter_init_23_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5497WEIGHTED_ROUND_ROBIN_Splitter_5514);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5538WEIGHTED_ROUND_ROBIN_Splitter_5555);
	FOR(int, __iter_init_24_, 0, <, 21, __iter_init_24_++)
		init_buffer_float(&SplitJoin103_CombineDFT_Fiss_5689_5710_join[__iter_init_24_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5614WEIGHTED_ROUND_ROBIN_Splitter_5636);
	FOR(int, __iter_init_25_, 0, <, 4, __iter_init_25_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_5683_5704_join[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 2, __iter_init_26_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_5674_5695_split[__iter_init_26_]);
	ENDFOR
	FOR(int, __iter_init_27_, 0, <, 2, __iter_init_27_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_5684_5705_split[__iter_init_27_]);
	ENDFOR
	FOR(int, __iter_init_28_, 0, <, 16, __iter_init_28_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_5681_5702_split[__iter_init_28_]);
	ENDFOR
	FOR(int, __iter_init_29_, 0, <, 2, __iter_init_29_++)
		init_buffer_float(&SplitJoin111_CombineDFT_Fiss_5693_5714_join[__iter_init_29_]);
	ENDFOR
	FOR(int, __iter_init_30_, 0, <, 4, __iter_init_30_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_5677_5698_join[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 16, __iter_init_31_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_5679_5700_join[__iter_init_31_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5576WEIGHTED_ROUND_ROBIN_Splitter_5579);
	FOR(int, __iter_init_32_, 0, <, 2, __iter_init_32_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_5676_5697_join[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 16, __iter_init_33_++)
		init_buffer_float(&SplitJoin105_CombineDFT_Fiss_5690_5711_join[__iter_init_33_]);
	ENDFOR
	FOR(int, __iter_init_34_, 0, <, 2, __iter_init_34_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_5430_5466_5675_5696_split[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 8, __iter_init_35_++)
		init_buffer_float(&SplitJoin99_FFTReorderSimple_Fiss_5687_5708_join[__iter_init_35_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5665WEIGHTED_ROUND_ROBIN_Splitter_5670);
	FOR(int, __iter_init_36_, 0, <, 2, __iter_init_36_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_5684_5705_join[__iter_init_36_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5473WEIGHTED_ROUND_ROBIN_Splitter_5464);
	FOR(int, __iter_init_37_, 0, <, 8, __iter_init_37_++)
		init_buffer_float(&SplitJoin99_FFTReorderSimple_Fiss_5687_5708_split[__iter_init_37_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5580WEIGHTED_ROUND_ROBIN_Splitter_5585);
	FOR(int, __iter_init_38_, 0, <, 21, __iter_init_38_++)
		init_buffer_float(&SplitJoin103_CombineDFT_Fiss_5689_5710_split[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 8, __iter_init_39_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_5682_5703_join[__iter_init_39_]);
	ENDFOR
// --- init: CombineDFT_5516
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5516_s.w[i] = real ; 
		CombineDFT_5516_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5517
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5517_s.w[i] = real ; 
		CombineDFT_5517_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5518
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5518_s.w[i] = real ; 
		CombineDFT_5518_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5519
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5519_s.w[i] = real ; 
		CombineDFT_5519_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5520
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5520_s.w[i] = real ; 
		CombineDFT_5520_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5521
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5521_s.w[i] = real ; 
		CombineDFT_5521_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5522
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5522_s.w[i] = real ; 
		CombineDFT_5522_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5523
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5523_s.w[i] = real ; 
		CombineDFT_5523_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5524
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5524_s.w[i] = real ; 
		CombineDFT_5524_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5525
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5525_s.w[i] = real ; 
		CombineDFT_5525_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5526
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5526_s.w[i] = real ; 
		CombineDFT_5526_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5527
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5527_s.w[i] = real ; 
		CombineDFT_5527_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5528
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5528_s.w[i] = real ; 
		CombineDFT_5528_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5529
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5529_s.w[i] = real ; 
		CombineDFT_5529_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5530
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5530_s.w[i] = real ; 
		CombineDFT_5530_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5531
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5531_s.w[i] = real ; 
		CombineDFT_5531_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5532
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5532_s.w[i] = real ; 
		CombineDFT_5532_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5533
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5533_s.w[i] = real ; 
		CombineDFT_5533_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5534
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5534_s.w[i] = real ; 
		CombineDFT_5534_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5535
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5535_s.w[i] = real ; 
		CombineDFT_5535_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5536
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5536_s.w[i] = real ; 
		CombineDFT_5536_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5539
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5539_s.w[i] = real ; 
		CombineDFT_5539_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5540
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5540_s.w[i] = real ; 
		CombineDFT_5540_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5541
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5541_s.w[i] = real ; 
		CombineDFT_5541_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5542
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5542_s.w[i] = real ; 
		CombineDFT_5542_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5543
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5543_s.w[i] = real ; 
		CombineDFT_5543_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5544
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5544_s.w[i] = real ; 
		CombineDFT_5544_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5545
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5545_s.w[i] = real ; 
		CombineDFT_5545_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5546
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5546_s.w[i] = real ; 
		CombineDFT_5546_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5547
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5547_s.w[i] = real ; 
		CombineDFT_5547_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5548
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5548_s.w[i] = real ; 
		CombineDFT_5548_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5549
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5549_s.w[i] = real ; 
		CombineDFT_5549_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5550
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5550_s.w[i] = real ; 
		CombineDFT_5550_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5551
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5551_s.w[i] = real ; 
		CombineDFT_5551_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5552
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5552_s.w[i] = real ; 
		CombineDFT_5552_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5553
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5553_s.w[i] = real ; 
		CombineDFT_5553_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5554
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5554_s.w[i] = real ; 
		CombineDFT_5554_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5557
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5557_s.w[i] = real ; 
		CombineDFT_5557_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5558
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5558_s.w[i] = real ; 
		CombineDFT_5558_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5559
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5559_s.w[i] = real ; 
		CombineDFT_5559_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5560
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5560_s.w[i] = real ; 
		CombineDFT_5560_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5561
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5561_s.w[i] = real ; 
		CombineDFT_5561_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5562
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5562_s.w[i] = real ; 
		CombineDFT_5562_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5563
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5563_s.w[i] = real ; 
		CombineDFT_5563_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5564
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5564_s.w[i] = real ; 
		CombineDFT_5564_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5567
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_5567_s.w[i] = real ; 
		CombineDFT_5567_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5568
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_5568_s.w[i] = real ; 
		CombineDFT_5568_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5569
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_5569_s.w[i] = real ; 
		CombineDFT_5569_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5570
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_5570_s.w[i] = real ; 
		CombineDFT_5570_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5573
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_5573_s.w[i] = real ; 
		CombineDFT_5573_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5574
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_5574_s.w[i] = real ; 
		CombineDFT_5574_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5451
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_5451_s.w[i] = real ; 
		CombineDFT_5451_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5615
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5615_s.w[i] = real ; 
		CombineDFT_5615_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5616
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5616_s.w[i] = real ; 
		CombineDFT_5616_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5617
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5617_s.w[i] = real ; 
		CombineDFT_5617_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5618
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5618_s.w[i] = real ; 
		CombineDFT_5618_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5619
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5619_s.w[i] = real ; 
		CombineDFT_5619_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5620
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5620_s.w[i] = real ; 
		CombineDFT_5620_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5621
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5621_s.w[i] = real ; 
		CombineDFT_5621_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5622
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5622_s.w[i] = real ; 
		CombineDFT_5622_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5623
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5623_s.w[i] = real ; 
		CombineDFT_5623_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5624
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5624_s.w[i] = real ; 
		CombineDFT_5624_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5625
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5625_s.w[i] = real ; 
		CombineDFT_5625_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5626
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5626_s.w[i] = real ; 
		CombineDFT_5626_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5627
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5627_s.w[i] = real ; 
		CombineDFT_5627_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5628
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5628_s.w[i] = real ; 
		CombineDFT_5628_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5629
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5629_s.w[i] = real ; 
		CombineDFT_5629_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5630
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5630_s.w[i] = real ; 
		CombineDFT_5630_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5631
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5631_s.w[i] = real ; 
		CombineDFT_5631_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5632
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5632_s.w[i] = real ; 
		CombineDFT_5632_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5633
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5633_s.w[i] = real ; 
		CombineDFT_5633_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5634
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5634_s.w[i] = real ; 
		CombineDFT_5634_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5635
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5635_s.w[i] = real ; 
		CombineDFT_5635_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5638
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5638_s.w[i] = real ; 
		CombineDFT_5638_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5639
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5639_s.w[i] = real ; 
		CombineDFT_5639_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5640
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5640_s.w[i] = real ; 
		CombineDFT_5640_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5641
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5641_s.w[i] = real ; 
		CombineDFT_5641_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5642
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5642_s.w[i] = real ; 
		CombineDFT_5642_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5643
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5643_s.w[i] = real ; 
		CombineDFT_5643_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5644
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5644_s.w[i] = real ; 
		CombineDFT_5644_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5645
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5645_s.w[i] = real ; 
		CombineDFT_5645_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5646
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5646_s.w[i] = real ; 
		CombineDFT_5646_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5647
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5647_s.w[i] = real ; 
		CombineDFT_5647_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5648
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5648_s.w[i] = real ; 
		CombineDFT_5648_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5649
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5649_s.w[i] = real ; 
		CombineDFT_5649_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5650
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5650_s.w[i] = real ; 
		CombineDFT_5650_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5651
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5651_s.w[i] = real ; 
		CombineDFT_5651_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5652
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5652_s.w[i] = real ; 
		CombineDFT_5652_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5653
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5653_s.w[i] = real ; 
		CombineDFT_5653_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5656
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5656_s.w[i] = real ; 
		CombineDFT_5656_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5657
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5657_s.w[i] = real ; 
		CombineDFT_5657_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5658
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5658_s.w[i] = real ; 
		CombineDFT_5658_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5659
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5659_s.w[i] = real ; 
		CombineDFT_5659_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5660
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5660_s.w[i] = real ; 
		CombineDFT_5660_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5661
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5661_s.w[i] = real ; 
		CombineDFT_5661_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5662
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5662_s.w[i] = real ; 
		CombineDFT_5662_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5663
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_5663_s.w[i] = real ; 
		CombineDFT_5663_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5666
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_5666_s.w[i] = real ; 
		CombineDFT_5666_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5667
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_5667_s.w[i] = real ; 
		CombineDFT_5667_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5668
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_5668_s.w[i] = real ; 
		CombineDFT_5668_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5669
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_5669_s.w[i] = real ; 
		CombineDFT_5669_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5672
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_5672_s.w[i] = real ; 
		CombineDFT_5672_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5673
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_5673_s.w[i] = real ; 
		CombineDFT_5673_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5462
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_5462_s.w[i] = real ; 
		CombineDFT_5462_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_5472();
			FFTTestSource_5474();
			FFTTestSource_5475();
		WEIGHTED_ROUND_ROBIN_Joiner_5473();
		WEIGHTED_ROUND_ROBIN_Splitter_5464();
			FFTReorderSimple_5441();
			WEIGHTED_ROUND_ROBIN_Splitter_5476();
				FFTReorderSimple_5478();
				FFTReorderSimple_5479();
			WEIGHTED_ROUND_ROBIN_Joiner_5477();
			WEIGHTED_ROUND_ROBIN_Splitter_5480();
				FFTReorderSimple_5482();
				FFTReorderSimple_5483();
				FFTReorderSimple_5484();
				FFTReorderSimple_5485();
			WEIGHTED_ROUND_ROBIN_Joiner_5481();
			WEIGHTED_ROUND_ROBIN_Splitter_5486();
				FFTReorderSimple_5488();
				FFTReorderSimple_5489();
				FFTReorderSimple_5490();
				FFTReorderSimple_5491();
				FFTReorderSimple_5492();
				FFTReorderSimple_5493();
				FFTReorderSimple_5494();
				FFTReorderSimple_5495();
			WEIGHTED_ROUND_ROBIN_Joiner_5487();
			WEIGHTED_ROUND_ROBIN_Splitter_5496();
				FFTReorderSimple_5498();
				FFTReorderSimple_5499();
				FFTReorderSimple_5500();
				FFTReorderSimple_5501();
				FFTReorderSimple_5502();
				FFTReorderSimple_5503();
				FFTReorderSimple_5504();
				FFTReorderSimple_5505();
				FFTReorderSimple_5506();
				FFTReorderSimple_5507();
				FFTReorderSimple_5508();
				FFTReorderSimple_5509();
				FFTReorderSimple_5510();
				FFTReorderSimple_5511();
				FFTReorderSimple_5512();
				FFTReorderSimple_5513();
			WEIGHTED_ROUND_ROBIN_Joiner_5497();
			WEIGHTED_ROUND_ROBIN_Splitter_5514();
				CombineDFT_5516();
				CombineDFT_5517();
				CombineDFT_5518();
				CombineDFT_5519();
				CombineDFT_5520();
				CombineDFT_5521();
				CombineDFT_5522();
				CombineDFT_5523();
				CombineDFT_5524();
				CombineDFT_5525();
				CombineDFT_5526();
				CombineDFT_5527();
				CombineDFT_5528();
				CombineDFT_5529();
				CombineDFT_5530();
				CombineDFT_5531();
				CombineDFT_5532();
				CombineDFT_5533();
				CombineDFT_5534();
				CombineDFT_5535();
				CombineDFT_5536();
			WEIGHTED_ROUND_ROBIN_Joiner_5515();
			WEIGHTED_ROUND_ROBIN_Splitter_5537();
				CombineDFT_5539();
				CombineDFT_5540();
				CombineDFT_5541();
				CombineDFT_5542();
				CombineDFT_5543();
				CombineDFT_5544();
				CombineDFT_5545();
				CombineDFT_5546();
				CombineDFT_5547();
				CombineDFT_5548();
				CombineDFT_5549();
				CombineDFT_5550();
				CombineDFT_5551();
				CombineDFT_5552();
				CombineDFT_5553();
				CombineDFT_5554();
			WEIGHTED_ROUND_ROBIN_Joiner_5538();
			WEIGHTED_ROUND_ROBIN_Splitter_5555();
				CombineDFT_5557();
				CombineDFT_5558();
				CombineDFT_5559();
				CombineDFT_5560();
				CombineDFT_5561();
				CombineDFT_5562();
				CombineDFT_5563();
				CombineDFT_5564();
			WEIGHTED_ROUND_ROBIN_Joiner_5556();
			WEIGHTED_ROUND_ROBIN_Splitter_5565();
				CombineDFT_5567();
				CombineDFT_5568();
				CombineDFT_5569();
				CombineDFT_5570();
			WEIGHTED_ROUND_ROBIN_Joiner_5566();
			WEIGHTED_ROUND_ROBIN_Splitter_5571();
				CombineDFT_5573();
				CombineDFT_5574();
			WEIGHTED_ROUND_ROBIN_Joiner_5572();
			CombineDFT_5451();
			FFTReorderSimple_5452();
			WEIGHTED_ROUND_ROBIN_Splitter_5575();
				FFTReorderSimple_5577();
				FFTReorderSimple_5578();
			WEIGHTED_ROUND_ROBIN_Joiner_5576();
			WEIGHTED_ROUND_ROBIN_Splitter_5579();
				FFTReorderSimple_5581();
				FFTReorderSimple_5582();
				FFTReorderSimple_5583();
				FFTReorderSimple_5584();
			WEIGHTED_ROUND_ROBIN_Joiner_5580();
			WEIGHTED_ROUND_ROBIN_Splitter_5585();
				FFTReorderSimple_5587();
				FFTReorderSimple_5588();
				FFTReorderSimple_5589();
				FFTReorderSimple_5590();
				FFTReorderSimple_5591();
				FFTReorderSimple_5592();
				FFTReorderSimple_5593();
				FFTReorderSimple_5594();
			WEIGHTED_ROUND_ROBIN_Joiner_5586();
			WEIGHTED_ROUND_ROBIN_Splitter_5595();
				FFTReorderSimple_5597();
				FFTReorderSimple_5598();
				FFTReorderSimple_5599();
				FFTReorderSimple_5600();
				FFTReorderSimple_5601();
				FFTReorderSimple_5602();
				FFTReorderSimple_5603();
				FFTReorderSimple_5604();
				FFTReorderSimple_5605();
				FFTReorderSimple_5606();
				FFTReorderSimple_5607();
				FFTReorderSimple_5608();
				FFTReorderSimple_5609();
				FFTReorderSimple_5610();
				FFTReorderSimple_5611();
				FFTReorderSimple_5612();
			WEIGHTED_ROUND_ROBIN_Joiner_5596();
			WEIGHTED_ROUND_ROBIN_Splitter_5613();
				CombineDFT_5615();
				CombineDFT_5616();
				CombineDFT_5617();
				CombineDFT_5618();
				CombineDFT_5619();
				CombineDFT_5620();
				CombineDFT_5621();
				CombineDFT_5622();
				CombineDFT_5623();
				CombineDFT_5624();
				CombineDFT_5625();
				CombineDFT_5626();
				CombineDFT_5627();
				CombineDFT_5628();
				CombineDFT_5629();
				CombineDFT_5630();
				CombineDFT_5631();
				CombineDFT_5632();
				CombineDFT_5633();
				CombineDFT_5634();
				CombineDFT_5635();
			WEIGHTED_ROUND_ROBIN_Joiner_5614();
			WEIGHTED_ROUND_ROBIN_Splitter_5636();
				CombineDFT_5638();
				CombineDFT_5639();
				CombineDFT_5640();
				CombineDFT_5641();
				CombineDFT_5642();
				CombineDFT_5643();
				CombineDFT_5644();
				CombineDFT_5645();
				CombineDFT_5646();
				CombineDFT_5647();
				CombineDFT_5648();
				CombineDFT_5649();
				CombineDFT_5650();
				CombineDFT_5651();
				CombineDFT_5652();
				CombineDFT_5653();
			WEIGHTED_ROUND_ROBIN_Joiner_5637();
			WEIGHTED_ROUND_ROBIN_Splitter_5654();
				CombineDFT_5656();
				CombineDFT_5657();
				CombineDFT_5658();
				CombineDFT_5659();
				CombineDFT_5660();
				CombineDFT_5661();
				CombineDFT_5662();
				CombineDFT_5663();
			WEIGHTED_ROUND_ROBIN_Joiner_5655();
			WEIGHTED_ROUND_ROBIN_Splitter_5664();
				CombineDFT_5666();
				CombineDFT_5667();
				CombineDFT_5668();
				CombineDFT_5669();
			WEIGHTED_ROUND_ROBIN_Joiner_5665();
			WEIGHTED_ROUND_ROBIN_Splitter_5670();
				CombineDFT_5672();
				CombineDFT_5673();
			WEIGHTED_ROUND_ROBIN_Joiner_5671();
			CombineDFT_5462();
		WEIGHTED_ROUND_ROBIN_Joiner_5465();
		FloatPrinter_5463();
	ENDFOR
	return EXIT_SUCCESS;
}
