#include "PEG5-FFT5_nocache.h"

buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_8177WEIGHTED_ROUND_ROBIN_Splitter_8320;
buffer_complex_t SplitJoin18_SplitJoin12_SplitJoin12_split2_7979_8232_8310_8384_join[2];
buffer_complex_t SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8002_8226_8346_8366_split[2];
buffer_complex_t SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_8004_8227_8347_8367_join[2];
buffer_complex_t SplitJoin65_SplitJoin49_SplitJoin49_AnonFilter_a0_8010_8263_8352_8368_split[2];
buffer_complex_t SplitJoin20_SplitJoin14_SplitJoin14_split1_7990_8234_8350_8389_join[2];
buffer_complex_t SplitJoin41_SplitJoin29_SplitJoin29_split2_7981_8246_8312_8385_split[2];
buffer_complex_t SplitJoin58_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child1_8314_8381_join[4];
buffer_complex_t SplitJoin69_SplitJoin53_SplitJoin53_AnonFilter_a0_8016_8266_8353_8369_split[2];
buffer_complex_t SplitJoin58_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child1_8314_8381_split[4];
buffer_complex_t SplitJoin41_SplitJoin29_SplitJoin29_split2_7981_8246_8312_8385_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_8156butterfly_8061;
buffer_complex_t SplitJoin47_SplitJoin33_SplitJoin33_split2_7983_8249_8313_8387_join[2];
buffer_complex_t butterfly_8067Post_CollapsedDataParallel_2_8175;
buffer_complex_t SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8002_8226_8346_8366_join[2];
buffer_complex_t butterfly_8066Post_CollapsedDataParallel_2_8172;
buffer_complex_t SplitJoin34_SplitJoin22_SplitJoin22_split2_7994_8240_8318_8391_split[4];
buffer_complex_t SplitJoin91_SplitJoin75_SplitJoin75_AnonFilter_a0_8046_8281_8359_8376_split[2];
buffer_complex_t SplitJoin45_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child1_8319_8386_join[2];
buffer_complex_t SplitJoin51_SplitJoin37_SplitJoin37_split2_7985_8252_8315_8388_split[2];
buffer_complex_t SplitJoin18_SplitJoin12_SplitJoin12_split2_7979_8232_8310_8384_split[2];
buffer_complex_t SplitJoin79_SplitJoin63_SplitJoin63_AnonFilter_a0_8030_8273_8309_8372_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_8334WEIGHTED_ROUND_ROBIN_Splitter_8176;
buffer_complex_t Pre_CollapsedDataParallel_1_8174butterfly_8067;
buffer_complex_t SplitJoin14_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_Hier_8349_8382_split[2];
buffer_complex_t Pre_CollapsedDataParallel_1_8168butterfly_8065;
buffer_complex_t Pre_CollapsedDataParallel_1_8153butterfly_8060;
buffer_complex_t SplitJoin47_SplitJoin33_SplitJoin33_split2_7983_8249_8313_8387_split[2];
buffer_complex_t SplitJoin20_SplitJoin14_SplitJoin14_split1_7990_8234_8350_8389_split[2];
buffer_complex_t SplitJoin10_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_Hier_8348_8379_split[2];
buffer_complex_t butterfly_8063Post_CollapsedDataParallel_2_8163;
buffer_complex_t SplitJoin79_SplitJoin63_SplitJoin63_AnonFilter_a0_8030_8273_8309_8372_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8338sink_8087;
buffer_complex_t butterfly_8065Post_CollapsedDataParallel_2_8169;
buffer_complex_t SplitJoin81_SplitJoin65_SplitJoin65_AnonFilter_a0_8032_8274_8356_8373_split[2];
buffer_complex_t SplitJoin51_SplitJoin37_SplitJoin37_split2_7985_8252_8315_8388_join[2];
buffer_complex_t SplitJoin93_SplitJoin77_SplitJoin77_AnonFilter_a0_8048_8282_8360_8377_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_8325WEIGHTED_ROUND_ROBIN_Splitter_8326;
buffer_complex_t SplitJoin16_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child0_8317_8383_split[2];
buffer_complex_t SplitJoin16_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child0_8317_8383_join[2];
buffer_complex_t SplitJoin45_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child1_8319_8386_split[2];
buffer_complex_t SplitJoin12_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child0_8311_8380_join[4];
buffer_complex_t SplitJoin71_SplitJoin55_SplitJoin55_AnonFilter_a0_8018_8267_8354_8370_join[2];
buffer_complex_t SplitJoin83_SplitJoin67_SplitJoin67_AnonFilter_a0_8034_8275_8357_8374_join[2];
buffer_complex_t SplitJoin97_SplitJoin81_SplitJoin81_AnonFilter_a0_8054_8285_8361_8378_split[2];
buffer_complex_t Pre_CollapsedDataParallel_1_8159butterfly_8062;
buffer_complex_t SplitJoin69_SplitJoin53_SplitJoin53_AnonFilter_a0_8016_8266_8353_8369_join[2];
buffer_complex_t SplitJoin87_SplitJoin71_SplitJoin71_AnonFilter_a0_8040_8278_8358_8375_split[2];
buffer_complex_t SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8000_8225_8308_8365_join[2];
buffer_complex_t SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7998_8224_8345_8364_join[2];
buffer_complex_t SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_8004_8227_8347_8367_split[2];
buffer_complex_t SplitJoin65_SplitJoin49_SplitJoin49_AnonFilter_a0_8010_8263_8352_8368_join[2];
buffer_complex_t SplitJoin34_SplitJoin22_SplitJoin22_split2_7994_8240_8318_8391_join[4];
buffer_complex_t SplitJoin75_SplitJoin59_SplitJoin59_AnonFilter_a0_8024_8270_8355_8371_join[2];
buffer_complex_t SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8000_8225_8308_8365_split[2];
buffer_complex_t SplitJoin71_SplitJoin55_SplitJoin55_AnonFilter_a0_8018_8267_8354_8370_split[2];
buffer_complex_t SplitJoin81_SplitJoin65_SplitJoin65_AnonFilter_a0_8032_8274_8356_8373_join[2];
buffer_complex_t SplitJoin75_SplitJoin59_SplitJoin59_AnonFilter_a0_8024_8270_8355_8371_split[2];
buffer_complex_t SplitJoin97_SplitJoin81_SplitJoin81_AnonFilter_a0_8054_8285_8361_8378_join[2];
buffer_complex_t SplitJoin22_SplitJoin16_SplitJoin16_split2_7992_8235_8316_8390_split[4];
buffer_complex_t butterfly_8060Post_CollapsedDataParallel_2_8154;
buffer_complex_t SplitJoin93_SplitJoin77_SplitJoin77_AnonFilter_a0_8048_8282_8360_8377_split[2];
buffer_complex_t butterfly_8061Post_CollapsedDataParallel_2_8157;
buffer_complex_t SplitJoin83_SplitJoin67_SplitJoin67_AnonFilter_a0_8034_8275_8357_8374_split[2];
buffer_complex_t SplitJoin87_SplitJoin71_SplitJoin71_AnonFilter_a0_8040_8278_8358_8375_join[2];
buffer_complex_t SplitJoin91_SplitJoin75_SplitJoin75_AnonFilter_a0_8046_8281_8359_8376_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_8162butterfly_8063;
buffer_complex_t SplitJoin0_source_Fiss_8344_8363_join[2];
buffer_complex_t SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7998_8224_8345_8364_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_8331WEIGHTED_ROUND_ROBIN_Splitter_8218;
buffer_complex_t SplitJoin14_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_Hier_8349_8382_join[2];
buffer_complex_t SplitJoin10_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_Hier_8348_8379_join[2];
buffer_complex_t SplitJoin0_source_Fiss_8344_8363_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_8219WEIGHTED_ROUND_ROBIN_Splitter_8337;
buffer_complex_t butterfly_8064Post_CollapsedDataParallel_2_8166;
buffer_complex_t SplitJoin12_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child0_8311_8380_split[4];
buffer_complex_t SplitJoin22_SplitJoin16_SplitJoin16_split2_7992_8235_8316_8390_join[4];
buffer_complex_t Pre_CollapsedDataParallel_1_8165butterfly_8064;
buffer_complex_t Pre_CollapsedDataParallel_1_8171butterfly_8066;
buffer_complex_t butterfly_8062Post_CollapsedDataParallel_2_8160;
buffer_complex_t SplitJoin24_magnitude_Fiss_8351_8392_split[5];
buffer_float_t SplitJoin24_magnitude_Fiss_8351_8392_join[5];



void source_8335(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t t;
		t.imag = 0.0 ; 
		t.real = 0.9501 ; 
		push_complex(&SplitJoin0_source_Fiss_8344_8363_join[0], t) ; 
		t.real = 0.2311 ; 
		push_complex(&SplitJoin0_source_Fiss_8344_8363_join[0], t) ; 
		t.real = 0.6068 ; 
		push_complex(&SplitJoin0_source_Fiss_8344_8363_join[0], t) ; 
		t.real = 0.486 ; 
		push_complex(&SplitJoin0_source_Fiss_8344_8363_join[0], t) ; 
		t.real = 0.8913 ; 
		push_complex(&SplitJoin0_source_Fiss_8344_8363_join[0], t) ; 
		t.real = 0.7621 ; 
		push_complex(&SplitJoin0_source_Fiss_8344_8363_join[0], t) ; 
		t.real = 0.4565 ; 
		push_complex(&SplitJoin0_source_Fiss_8344_8363_join[0], t) ; 
		t.real = 0.0185 ; 
		push_complex(&SplitJoin0_source_Fiss_8344_8363_join[0], t) ; 
	}
	ENDFOR
}

void source_8336(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t t;
		t.imag = 0.0 ; 
		t.real = 0.9501 ; 
		push_complex(&SplitJoin0_source_Fiss_8344_8363_join[1], t) ; 
		t.real = 0.2311 ; 
		push_complex(&SplitJoin0_source_Fiss_8344_8363_join[1], t) ; 
		t.real = 0.6068 ; 
		push_complex(&SplitJoin0_source_Fiss_8344_8363_join[1], t) ; 
		t.real = 0.486 ; 
		push_complex(&SplitJoin0_source_Fiss_8344_8363_join[1], t) ; 
		t.real = 0.8913 ; 
		push_complex(&SplitJoin0_source_Fiss_8344_8363_join[1], t) ; 
		t.real = 0.7621 ; 
		push_complex(&SplitJoin0_source_Fiss_8344_8363_join[1], t) ; 
		t.real = 0.4565 ; 
		push_complex(&SplitJoin0_source_Fiss_8344_8363_join[1], t) ; 
		t.real = 0.0185 ; 
		push_complex(&SplitJoin0_source_Fiss_8344_8363_join[1], t) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8333() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_8334() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8334WEIGHTED_ROUND_ROBIN_Splitter_8176, pop_complex(&SplitJoin0_source_Fiss_8344_8363_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8334WEIGHTED_ROUND_ROBIN_Splitter_8176, pop_complex(&SplitJoin0_source_Fiss_8344_8363_join[1]));
		ENDFOR
	ENDFOR
}}

void Identity_8006(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t __tmp2158 = pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_8004_8227_8347_8367_split[0]);
		push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_8004_8227_8347_8367_join[0], __tmp2158) ; 
	}
	ENDFOR
}

void Identity_8008(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t __tmp2161 = pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_8004_8227_8347_8367_split[1]);
		push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_8004_8227_8347_8367_join[1], __tmp2161) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8182() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_8004_8227_8347_8367_split[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8002_8226_8346_8366_split[0]));
		push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_8004_8227_8347_8367_split[1], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8002_8226_8346_8366_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8183() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8002_8226_8346_8366_join[0], pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_8004_8227_8347_8367_join[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8002_8226_8346_8366_join[0], pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_8004_8227_8347_8367_join[1]));
	ENDFOR
}}

void Identity_8012(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t __tmp2169 = pop_complex(&SplitJoin65_SplitJoin49_SplitJoin49_AnonFilter_a0_8010_8263_8352_8368_split[0]);
		push_complex(&SplitJoin65_SplitJoin49_SplitJoin49_AnonFilter_a0_8010_8263_8352_8368_join[0], __tmp2169) ; 
	}
	ENDFOR
}

void Identity_8014(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t __tmp2172 = pop_complex(&SplitJoin65_SplitJoin49_SplitJoin49_AnonFilter_a0_8010_8263_8352_8368_split[1]);
		push_complex(&SplitJoin65_SplitJoin49_SplitJoin49_AnonFilter_a0_8010_8263_8352_8368_join[1], __tmp2172) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8184() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin65_SplitJoin49_SplitJoin49_AnonFilter_a0_8010_8263_8352_8368_split[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8002_8226_8346_8366_split[1]));
		push_complex(&SplitJoin65_SplitJoin49_SplitJoin49_AnonFilter_a0_8010_8263_8352_8368_split[1], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8002_8226_8346_8366_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8185() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8002_8226_8346_8366_join[1], pop_complex(&SplitJoin65_SplitJoin49_SplitJoin49_AnonFilter_a0_8010_8263_8352_8368_join[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8002_8226_8346_8366_join[1], pop_complex(&SplitJoin65_SplitJoin49_SplitJoin49_AnonFilter_a0_8010_8263_8352_8368_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_8180() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8002_8226_8346_8366_split[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8000_8225_8308_8365_split[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8002_8226_8346_8366_split[1], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8000_8225_8308_8365_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8181() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8000_8225_8308_8365_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8002_8226_8346_8366_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8000_8225_8308_8365_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8002_8226_8346_8366_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8000_8225_8308_8365_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8002_8226_8346_8366_join[1]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8000_8225_8308_8365_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8002_8226_8346_8366_join[1]));
	ENDFOR
}}

void Identity_8020(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t __tmp2185 = pop_complex(&SplitJoin71_SplitJoin55_SplitJoin55_AnonFilter_a0_8018_8267_8354_8370_split[0]);
		push_complex(&SplitJoin71_SplitJoin55_SplitJoin55_AnonFilter_a0_8018_8267_8354_8370_join[0], __tmp2185) ; 
	}
	ENDFOR
}

void Identity_8022(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t __tmp2188 = pop_complex(&SplitJoin71_SplitJoin55_SplitJoin55_AnonFilter_a0_8018_8267_8354_8370_split[1]);
		push_complex(&SplitJoin71_SplitJoin55_SplitJoin55_AnonFilter_a0_8018_8267_8354_8370_join[1], __tmp2188) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8188() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin71_SplitJoin55_SplitJoin55_AnonFilter_a0_8018_8267_8354_8370_split[0], pop_complex(&SplitJoin69_SplitJoin53_SplitJoin53_AnonFilter_a0_8016_8266_8353_8369_split[0]));
		push_complex(&SplitJoin71_SplitJoin55_SplitJoin55_AnonFilter_a0_8018_8267_8354_8370_split[1], pop_complex(&SplitJoin69_SplitJoin53_SplitJoin53_AnonFilter_a0_8016_8266_8353_8369_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8189() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin69_SplitJoin53_SplitJoin53_AnonFilter_a0_8016_8266_8353_8369_join[0], pop_complex(&SplitJoin71_SplitJoin55_SplitJoin55_AnonFilter_a0_8018_8267_8354_8370_join[0]));
		push_complex(&SplitJoin69_SplitJoin53_SplitJoin53_AnonFilter_a0_8016_8266_8353_8369_join[0], pop_complex(&SplitJoin71_SplitJoin55_SplitJoin55_AnonFilter_a0_8018_8267_8354_8370_join[1]));
	ENDFOR
}}

void Identity_8026(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t __tmp2196 = pop_complex(&SplitJoin75_SplitJoin59_SplitJoin59_AnonFilter_a0_8024_8270_8355_8371_split[0]);
		push_complex(&SplitJoin75_SplitJoin59_SplitJoin59_AnonFilter_a0_8024_8270_8355_8371_join[0], __tmp2196) ; 
	}
	ENDFOR
}

void Identity_8028(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t __tmp2199 = pop_complex(&SplitJoin75_SplitJoin59_SplitJoin59_AnonFilter_a0_8024_8270_8355_8371_split[1]);
		push_complex(&SplitJoin75_SplitJoin59_SplitJoin59_AnonFilter_a0_8024_8270_8355_8371_join[1], __tmp2199) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8190() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin75_SplitJoin59_SplitJoin59_AnonFilter_a0_8024_8270_8355_8371_split[0], pop_complex(&SplitJoin69_SplitJoin53_SplitJoin53_AnonFilter_a0_8016_8266_8353_8369_split[1]));
		push_complex(&SplitJoin75_SplitJoin59_SplitJoin59_AnonFilter_a0_8024_8270_8355_8371_split[1], pop_complex(&SplitJoin69_SplitJoin53_SplitJoin53_AnonFilter_a0_8016_8266_8353_8369_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8191() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin69_SplitJoin53_SplitJoin53_AnonFilter_a0_8016_8266_8353_8369_join[1], pop_complex(&SplitJoin75_SplitJoin59_SplitJoin59_AnonFilter_a0_8024_8270_8355_8371_join[0]));
		push_complex(&SplitJoin69_SplitJoin53_SplitJoin53_AnonFilter_a0_8016_8266_8353_8369_join[1], pop_complex(&SplitJoin75_SplitJoin59_SplitJoin59_AnonFilter_a0_8024_8270_8355_8371_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_8186() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		push_complex(&SplitJoin69_SplitJoin53_SplitJoin53_AnonFilter_a0_8016_8266_8353_8369_split[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8000_8225_8308_8365_split[1]));
		push_complex(&SplitJoin69_SplitJoin53_SplitJoin53_AnonFilter_a0_8016_8266_8353_8369_split[1], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8000_8225_8308_8365_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8187() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8000_8225_8308_8365_join[1], pop_complex(&SplitJoin69_SplitJoin53_SplitJoin53_AnonFilter_a0_8016_8266_8353_8369_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8000_8225_8308_8365_join[1], pop_complex(&SplitJoin69_SplitJoin53_SplitJoin53_AnonFilter_a0_8016_8266_8353_8369_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8000_8225_8308_8365_join[1], pop_complex(&SplitJoin69_SplitJoin53_SplitJoin53_AnonFilter_a0_8016_8266_8353_8369_join[1]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8000_8225_8308_8365_join[1], pop_complex(&SplitJoin69_SplitJoin53_SplitJoin53_AnonFilter_a0_8016_8266_8353_8369_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_8178() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 20, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8000_8225_8308_8365_split[0], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7998_8224_8345_8364_split[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8000_8225_8308_8365_split[1], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7998_8224_8345_8364_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8179() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7998_8224_8345_8364_join[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8000_8225_8308_8365_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7998_8224_8345_8364_join[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8000_8225_8308_8365_join[1]));
		ENDFOR
	ENDFOR
}}

void Identity_8036(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t __tmp2217 = pop_complex(&SplitJoin83_SplitJoin67_SplitJoin67_AnonFilter_a0_8034_8275_8357_8374_split[0]);
		push_complex(&SplitJoin83_SplitJoin67_SplitJoin67_AnonFilter_a0_8034_8275_8357_8374_join[0], __tmp2217) ; 
	}
	ENDFOR
}

void Identity_8038(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t __tmp2220 = pop_complex(&SplitJoin83_SplitJoin67_SplitJoin67_AnonFilter_a0_8034_8275_8357_8374_split[1]);
		push_complex(&SplitJoin83_SplitJoin67_SplitJoin67_AnonFilter_a0_8034_8275_8357_8374_join[1], __tmp2220) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8196() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin83_SplitJoin67_SplitJoin67_AnonFilter_a0_8034_8275_8357_8374_split[0], pop_complex(&SplitJoin81_SplitJoin65_SplitJoin65_AnonFilter_a0_8032_8274_8356_8373_split[0]));
		push_complex(&SplitJoin83_SplitJoin67_SplitJoin67_AnonFilter_a0_8034_8275_8357_8374_split[1], pop_complex(&SplitJoin81_SplitJoin65_SplitJoin65_AnonFilter_a0_8032_8274_8356_8373_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8197() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin81_SplitJoin65_SplitJoin65_AnonFilter_a0_8032_8274_8356_8373_join[0], pop_complex(&SplitJoin83_SplitJoin67_SplitJoin67_AnonFilter_a0_8034_8275_8357_8374_join[0]));
		push_complex(&SplitJoin81_SplitJoin65_SplitJoin65_AnonFilter_a0_8032_8274_8356_8373_join[0], pop_complex(&SplitJoin83_SplitJoin67_SplitJoin67_AnonFilter_a0_8034_8275_8357_8374_join[1]));
	ENDFOR
}}

void Identity_8042(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t __tmp2228 = pop_complex(&SplitJoin87_SplitJoin71_SplitJoin71_AnonFilter_a0_8040_8278_8358_8375_split[0]);
		push_complex(&SplitJoin87_SplitJoin71_SplitJoin71_AnonFilter_a0_8040_8278_8358_8375_join[0], __tmp2228) ; 
	}
	ENDFOR
}

void Identity_8044(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t __tmp2231 = pop_complex(&SplitJoin87_SplitJoin71_SplitJoin71_AnonFilter_a0_8040_8278_8358_8375_split[1]);
		push_complex(&SplitJoin87_SplitJoin71_SplitJoin71_AnonFilter_a0_8040_8278_8358_8375_join[1], __tmp2231) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8198() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin87_SplitJoin71_SplitJoin71_AnonFilter_a0_8040_8278_8358_8375_split[0], pop_complex(&SplitJoin81_SplitJoin65_SplitJoin65_AnonFilter_a0_8032_8274_8356_8373_split[1]));
		push_complex(&SplitJoin87_SplitJoin71_SplitJoin71_AnonFilter_a0_8040_8278_8358_8375_split[1], pop_complex(&SplitJoin81_SplitJoin65_SplitJoin65_AnonFilter_a0_8032_8274_8356_8373_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8199() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin81_SplitJoin65_SplitJoin65_AnonFilter_a0_8032_8274_8356_8373_join[1], pop_complex(&SplitJoin87_SplitJoin71_SplitJoin71_AnonFilter_a0_8040_8278_8358_8375_join[0]));
		push_complex(&SplitJoin81_SplitJoin65_SplitJoin65_AnonFilter_a0_8032_8274_8356_8373_join[1], pop_complex(&SplitJoin87_SplitJoin71_SplitJoin71_AnonFilter_a0_8040_8278_8358_8375_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_8194() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		push_complex(&SplitJoin81_SplitJoin65_SplitJoin65_AnonFilter_a0_8032_8274_8356_8373_split[0], pop_complex(&SplitJoin79_SplitJoin63_SplitJoin63_AnonFilter_a0_8030_8273_8309_8372_split[0]));
		push_complex(&SplitJoin81_SplitJoin65_SplitJoin65_AnonFilter_a0_8032_8274_8356_8373_split[1], pop_complex(&SplitJoin79_SplitJoin63_SplitJoin63_AnonFilter_a0_8030_8273_8309_8372_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8195() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin79_SplitJoin63_SplitJoin63_AnonFilter_a0_8030_8273_8309_8372_join[0], pop_complex(&SplitJoin81_SplitJoin65_SplitJoin65_AnonFilter_a0_8032_8274_8356_8373_join[0]));
		push_complex(&SplitJoin79_SplitJoin63_SplitJoin63_AnonFilter_a0_8030_8273_8309_8372_join[0], pop_complex(&SplitJoin81_SplitJoin65_SplitJoin65_AnonFilter_a0_8032_8274_8356_8373_join[0]));
		push_complex(&SplitJoin79_SplitJoin63_SplitJoin63_AnonFilter_a0_8030_8273_8309_8372_join[0], pop_complex(&SplitJoin81_SplitJoin65_SplitJoin65_AnonFilter_a0_8032_8274_8356_8373_join[1]));
		push_complex(&SplitJoin79_SplitJoin63_SplitJoin63_AnonFilter_a0_8030_8273_8309_8372_join[0], pop_complex(&SplitJoin81_SplitJoin65_SplitJoin65_AnonFilter_a0_8032_8274_8356_8373_join[1]));
	ENDFOR
}}

void Identity_8050(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t __tmp2244 = pop_complex(&SplitJoin93_SplitJoin77_SplitJoin77_AnonFilter_a0_8048_8282_8360_8377_split[0]);
		push_complex(&SplitJoin93_SplitJoin77_SplitJoin77_AnonFilter_a0_8048_8282_8360_8377_join[0], __tmp2244) ; 
	}
	ENDFOR
}

void Identity_8052(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t __tmp2247 = pop_complex(&SplitJoin93_SplitJoin77_SplitJoin77_AnonFilter_a0_8048_8282_8360_8377_split[1]);
		push_complex(&SplitJoin93_SplitJoin77_SplitJoin77_AnonFilter_a0_8048_8282_8360_8377_join[1], __tmp2247) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8202() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin93_SplitJoin77_SplitJoin77_AnonFilter_a0_8048_8282_8360_8377_split[0], pop_complex(&SplitJoin91_SplitJoin75_SplitJoin75_AnonFilter_a0_8046_8281_8359_8376_split[0]));
		push_complex(&SplitJoin93_SplitJoin77_SplitJoin77_AnonFilter_a0_8048_8282_8360_8377_split[1], pop_complex(&SplitJoin91_SplitJoin75_SplitJoin75_AnonFilter_a0_8046_8281_8359_8376_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8203() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin91_SplitJoin75_SplitJoin75_AnonFilter_a0_8046_8281_8359_8376_join[0], pop_complex(&SplitJoin93_SplitJoin77_SplitJoin77_AnonFilter_a0_8048_8282_8360_8377_join[0]));
		push_complex(&SplitJoin91_SplitJoin75_SplitJoin75_AnonFilter_a0_8046_8281_8359_8376_join[0], pop_complex(&SplitJoin93_SplitJoin77_SplitJoin77_AnonFilter_a0_8048_8282_8360_8377_join[1]));
	ENDFOR
}}

void Identity_8056(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t __tmp2255 = pop_complex(&SplitJoin97_SplitJoin81_SplitJoin81_AnonFilter_a0_8054_8285_8361_8378_split[0]);
		push_complex(&SplitJoin97_SplitJoin81_SplitJoin81_AnonFilter_a0_8054_8285_8361_8378_join[0], __tmp2255) ; 
	}
	ENDFOR
}

void Identity_8058(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t __tmp2258 = pop_complex(&SplitJoin97_SplitJoin81_SplitJoin81_AnonFilter_a0_8054_8285_8361_8378_split[1]);
		push_complex(&SplitJoin97_SplitJoin81_SplitJoin81_AnonFilter_a0_8054_8285_8361_8378_join[1], __tmp2258) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8204() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin97_SplitJoin81_SplitJoin81_AnonFilter_a0_8054_8285_8361_8378_split[0], pop_complex(&SplitJoin91_SplitJoin75_SplitJoin75_AnonFilter_a0_8046_8281_8359_8376_split[1]));
		push_complex(&SplitJoin97_SplitJoin81_SplitJoin81_AnonFilter_a0_8054_8285_8361_8378_split[1], pop_complex(&SplitJoin91_SplitJoin75_SplitJoin75_AnonFilter_a0_8046_8281_8359_8376_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8205() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin91_SplitJoin75_SplitJoin75_AnonFilter_a0_8046_8281_8359_8376_join[1], pop_complex(&SplitJoin97_SplitJoin81_SplitJoin81_AnonFilter_a0_8054_8285_8361_8378_join[0]));
		push_complex(&SplitJoin91_SplitJoin75_SplitJoin75_AnonFilter_a0_8046_8281_8359_8376_join[1], pop_complex(&SplitJoin97_SplitJoin81_SplitJoin81_AnonFilter_a0_8054_8285_8361_8378_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_8200() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		push_complex(&SplitJoin91_SplitJoin75_SplitJoin75_AnonFilter_a0_8046_8281_8359_8376_split[0], pop_complex(&SplitJoin79_SplitJoin63_SplitJoin63_AnonFilter_a0_8030_8273_8309_8372_split[1]));
		push_complex(&SplitJoin91_SplitJoin75_SplitJoin75_AnonFilter_a0_8046_8281_8359_8376_split[1], pop_complex(&SplitJoin79_SplitJoin63_SplitJoin63_AnonFilter_a0_8030_8273_8309_8372_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8201() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin79_SplitJoin63_SplitJoin63_AnonFilter_a0_8030_8273_8309_8372_join[1], pop_complex(&SplitJoin91_SplitJoin75_SplitJoin75_AnonFilter_a0_8046_8281_8359_8376_join[0]));
		push_complex(&SplitJoin79_SplitJoin63_SplitJoin63_AnonFilter_a0_8030_8273_8309_8372_join[1], pop_complex(&SplitJoin91_SplitJoin75_SplitJoin75_AnonFilter_a0_8046_8281_8359_8376_join[0]));
		push_complex(&SplitJoin79_SplitJoin63_SplitJoin63_AnonFilter_a0_8030_8273_8309_8372_join[1], pop_complex(&SplitJoin91_SplitJoin75_SplitJoin75_AnonFilter_a0_8046_8281_8359_8376_join[1]));
		push_complex(&SplitJoin79_SplitJoin63_SplitJoin63_AnonFilter_a0_8030_8273_8309_8372_join[1], pop_complex(&SplitJoin91_SplitJoin75_SplitJoin75_AnonFilter_a0_8046_8281_8359_8376_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_8192() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 20, __iter_steady_++)
		push_complex(&SplitJoin79_SplitJoin63_SplitJoin63_AnonFilter_a0_8030_8273_8309_8372_split[0], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7998_8224_8345_8364_split[1]));
		push_complex(&SplitJoin79_SplitJoin63_SplitJoin63_AnonFilter_a0_8030_8273_8309_8372_split[1], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7998_8224_8345_8364_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8193() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7998_8224_8345_8364_join[1], pop_complex(&SplitJoin79_SplitJoin63_SplitJoin63_AnonFilter_a0_8030_8273_8309_8372_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7998_8224_8345_8364_join[1], pop_complex(&SplitJoin79_SplitJoin63_SplitJoin63_AnonFilter_a0_8030_8273_8309_8372_join[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_8176() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 40, __iter_steady_++)
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7998_8224_8345_8364_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8334WEIGHTED_ROUND_ROBIN_Splitter_8176));
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7998_8224_8345_8364_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8334WEIGHTED_ROUND_ROBIN_Splitter_8176));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8177() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8177WEIGHTED_ROUND_ROBIN_Splitter_8320, pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7998_8224_8345_8364_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8177WEIGHTED_ROUND_ROBIN_Splitter_8320, pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7998_8224_8345_8364_join[1]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_8153(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
 {
 {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
			push_complex(&Pre_CollapsedDataParallel_1_8153butterfly_8060, peek_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child0_8311_8380_split[0], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
		}
		ENDFOR
	}
	}
	}
		pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child0_8311_8380_split[0]) ; 
	}
	ENDFOR
}

void butterfly_8060(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_8153butterfly_8060));
		complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_8153butterfly_8060));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&butterfly_8060Post_CollapsedDataParallel_2_8154, __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&butterfly_8060Post_CollapsedDataParallel_2_8154, __sa2) ; 
	}
	ENDFOR
}

void Post_CollapsedDataParallel_2_8154(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 2, _k++) {
 {
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child0_8311_8380_join[0], peek_complex(&butterfly_8060Post_CollapsedDataParallel_2_8154, (_k + 0))) ; 
		}
		}
		ENDFOR
	}
	}
		pop_complex(&butterfly_8060Post_CollapsedDataParallel_2_8154) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_8156(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
 {
 {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
			push_complex(&Pre_CollapsedDataParallel_1_8156butterfly_8061, peek_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child0_8311_8380_split[1], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
		}
		ENDFOR
	}
	}
	}
		pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child0_8311_8380_split[1]) ; 
	}
	ENDFOR
}

void butterfly_8061(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_8156butterfly_8061));
		complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_8156butterfly_8061));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&butterfly_8061Post_CollapsedDataParallel_2_8157, __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&butterfly_8061Post_CollapsedDataParallel_2_8157, __sa2) ; 
	}
	ENDFOR
}

void Post_CollapsedDataParallel_2_8157(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 2, _k++) {
 {
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child0_8311_8380_join[1], peek_complex(&butterfly_8061Post_CollapsedDataParallel_2_8157, (_k + 0))) ; 
		}
		}
		ENDFOR
	}
	}
		pop_complex(&butterfly_8061Post_CollapsedDataParallel_2_8157) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_8159(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
 {
 {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
			push_complex(&Pre_CollapsedDataParallel_1_8159butterfly_8062, peek_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child0_8311_8380_split[2], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
		}
		ENDFOR
	}
	}
	}
		pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child0_8311_8380_split[2]) ; 
	}
	ENDFOR
}

void butterfly_8062(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_8159butterfly_8062));
		complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_8159butterfly_8062));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&butterfly_8062Post_CollapsedDataParallel_2_8160, __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&butterfly_8062Post_CollapsedDataParallel_2_8160, __sa2) ; 
	}
	ENDFOR
}

void Post_CollapsedDataParallel_2_8160(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 2, _k++) {
 {
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child0_8311_8380_join[2], peek_complex(&butterfly_8062Post_CollapsedDataParallel_2_8160, (_k + 0))) ; 
		}
		}
		ENDFOR
	}
	}
		pop_complex(&butterfly_8062Post_CollapsedDataParallel_2_8160) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_8162(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
 {
 {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
			push_complex(&Pre_CollapsedDataParallel_1_8162butterfly_8063, peek_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child0_8311_8380_split[3], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
		}
		ENDFOR
	}
	}
	}
		pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child0_8311_8380_split[3]) ; 
	}
	ENDFOR
}

void butterfly_8063(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_8162butterfly_8063));
		complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_8162butterfly_8063));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&butterfly_8063Post_CollapsedDataParallel_2_8163, __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&butterfly_8063Post_CollapsedDataParallel_2_8163, __sa2) ; 
	}
	ENDFOR
}

void Post_CollapsedDataParallel_2_8163(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 2, _k++) {
 {
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child0_8311_8380_join[3], peek_complex(&butterfly_8063Post_CollapsedDataParallel_2_8163, (_k + 0))) ; 
		}
		}
		ENDFOR
	}
	}
		pop_complex(&butterfly_8063Post_CollapsedDataParallel_2_8163) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8321() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child0_8311_8380_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_Hier_8348_8379_split[0]));
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child0_8311_8380_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_Hier_8348_8379_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8322() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_Hier_8348_8379_join[0], pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child0_8311_8380_join[__iter_]));
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_Hier_8348_8379_join[0], pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child0_8311_8380_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_8165(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
 {
 {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
			push_complex(&Pre_CollapsedDataParallel_1_8165butterfly_8064, peek_complex(&SplitJoin58_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child1_8314_8381_split[0], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
		}
		ENDFOR
	}
	}
	}
		pop_complex(&SplitJoin58_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child1_8314_8381_split[0]) ; 
	}
	ENDFOR
}

void butterfly_8064(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_8165butterfly_8064));
		complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_8165butterfly_8064));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&butterfly_8064Post_CollapsedDataParallel_2_8166, __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&butterfly_8064Post_CollapsedDataParallel_2_8166, __sa2) ; 
	}
	ENDFOR
}

void Post_CollapsedDataParallel_2_8166(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 2, _k++) {
 {
			push_complex(&SplitJoin58_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child1_8314_8381_join[0], peek_complex(&butterfly_8064Post_CollapsedDataParallel_2_8166, (_k + 0))) ; 
		}
		}
		ENDFOR
	}
	}
		pop_complex(&butterfly_8064Post_CollapsedDataParallel_2_8166) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_8168(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
 {
 {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
			push_complex(&Pre_CollapsedDataParallel_1_8168butterfly_8065, peek_complex(&SplitJoin58_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child1_8314_8381_split[1], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
		}
		ENDFOR
	}
	}
	}
		pop_complex(&SplitJoin58_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child1_8314_8381_split[1]) ; 
	}
	ENDFOR
}

void butterfly_8065(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_8168butterfly_8065));
		complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_8168butterfly_8065));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&butterfly_8065Post_CollapsedDataParallel_2_8169, __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&butterfly_8065Post_CollapsedDataParallel_2_8169, __sa2) ; 
	}
	ENDFOR
}

void Post_CollapsedDataParallel_2_8169(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 2, _k++) {
 {
			push_complex(&SplitJoin58_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child1_8314_8381_join[1], peek_complex(&butterfly_8065Post_CollapsedDataParallel_2_8169, (_k + 0))) ; 
		}
		}
		ENDFOR
	}
	}
		pop_complex(&butterfly_8065Post_CollapsedDataParallel_2_8169) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_8171(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
 {
 {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
			push_complex(&Pre_CollapsedDataParallel_1_8171butterfly_8066, peek_complex(&SplitJoin58_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child1_8314_8381_split[2], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
		}
		ENDFOR
	}
	}
	}
		pop_complex(&SplitJoin58_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child1_8314_8381_split[2]) ; 
	}
	ENDFOR
}

void butterfly_8066(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_8171butterfly_8066));
		complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_8171butterfly_8066));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&butterfly_8066Post_CollapsedDataParallel_2_8172, __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&butterfly_8066Post_CollapsedDataParallel_2_8172, __sa2) ; 
	}
	ENDFOR
}

void Post_CollapsedDataParallel_2_8172(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 2, _k++) {
 {
			push_complex(&SplitJoin58_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child1_8314_8381_join[2], peek_complex(&butterfly_8066Post_CollapsedDataParallel_2_8172, (_k + 0))) ; 
		}
		}
		ENDFOR
	}
	}
		pop_complex(&butterfly_8066Post_CollapsedDataParallel_2_8172) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_8174(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
 {
 {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
			push_complex(&Pre_CollapsedDataParallel_1_8174butterfly_8067, peek_complex(&SplitJoin58_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child1_8314_8381_split[3], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
		}
		ENDFOR
	}
	}
	}
		pop_complex(&SplitJoin58_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child1_8314_8381_split[3]) ; 
	}
	ENDFOR
}

void butterfly_8067(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_8174butterfly_8067));
		complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_8174butterfly_8067));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&butterfly_8067Post_CollapsedDataParallel_2_8175, __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&butterfly_8067Post_CollapsedDataParallel_2_8175, __sa2) ; 
	}
	ENDFOR
}

void Post_CollapsedDataParallel_2_8175(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 2, _k++) {
 {
			push_complex(&SplitJoin58_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child1_8314_8381_join[3], peek_complex(&butterfly_8067Post_CollapsedDataParallel_2_8175, (_k + 0))) ; 
		}
		}
		ENDFOR
	}
	}
		pop_complex(&butterfly_8067Post_CollapsedDataParallel_2_8175) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8323() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin58_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child1_8314_8381_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_Hier_8348_8379_split[1]));
			push_complex(&SplitJoin58_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child1_8314_8381_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_Hier_8348_8379_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8324() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_Hier_8348_8379_join[1], pop_complex(&SplitJoin58_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child1_8314_8381_join[__iter_]));
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_Hier_8348_8379_join[1], pop_complex(&SplitJoin58_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child1_8314_8381_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_8320() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_Hier_8348_8379_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8177WEIGHTED_ROUND_ROBIN_Splitter_8320));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_Hier_8348_8379_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8177WEIGHTED_ROUND_ROBIN_Splitter_8320));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8325() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8325WEIGHTED_ROUND_ROBIN_Splitter_8326, pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_Hier_8348_8379_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8325WEIGHTED_ROUND_ROBIN_Splitter_8326, pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_Hier_8348_8379_join[1]));
		ENDFOR
	ENDFOR
}}

void butterfly_8069(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_7979_8232_8310_8384_split[0]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_7979_8232_8310_8384_split[0]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_7979_8232_8310_8384_join[0], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_7979_8232_8310_8384_join[0], __sa2) ; 
	}
	ENDFOR
}

void butterfly_8070(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_7979_8232_8310_8384_split[1]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_7979_8232_8310_8384_split[1]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = -4.371139E-8 ; 
		WN1.imag = -1.0 ; 
		WN2.real = 1.1924881E-8 ; 
		WN2.imag = 1.0 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_7979_8232_8310_8384_join[1], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_7979_8232_8310_8384_join[1], __sa2) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8210() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_7979_8232_8310_8384_split[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child0_8317_8383_split[0]));
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_7979_8232_8310_8384_split[1], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child0_8317_8383_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8211() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child0_8317_8383_join[0], pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_7979_8232_8310_8384_join[0]));
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child0_8317_8383_join[0], pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_7979_8232_8310_8384_join[1]));
	ENDFOR
}}

void butterfly_8071(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin41_SplitJoin29_SplitJoin29_split2_7981_8246_8312_8385_split[0]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin41_SplitJoin29_SplitJoin29_split2_7981_8246_8312_8385_split[0]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin41_SplitJoin29_SplitJoin29_split2_7981_8246_8312_8385_join[0], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin41_SplitJoin29_SplitJoin29_split2_7981_8246_8312_8385_join[0], __sa2) ; 
	}
	ENDFOR
}

void butterfly_8072(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin41_SplitJoin29_SplitJoin29_split2_7981_8246_8312_8385_split[1]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin41_SplitJoin29_SplitJoin29_split2_7981_8246_8312_8385_split[1]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = -4.371139E-8 ; 
		WN1.imag = -1.0 ; 
		WN2.real = 1.1924881E-8 ; 
		WN2.imag = 1.0 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin41_SplitJoin29_SplitJoin29_split2_7981_8246_8312_8385_join[1], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin41_SplitJoin29_SplitJoin29_split2_7981_8246_8312_8385_join[1], __sa2) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8212() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		push_complex(&SplitJoin41_SplitJoin29_SplitJoin29_split2_7981_8246_8312_8385_split[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child0_8317_8383_split[1]));
		push_complex(&SplitJoin41_SplitJoin29_SplitJoin29_split2_7981_8246_8312_8385_split[1], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child0_8317_8383_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8213() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child0_8317_8383_join[1], pop_complex(&SplitJoin41_SplitJoin29_SplitJoin29_split2_7981_8246_8312_8385_join[0]));
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child0_8317_8383_join[1], pop_complex(&SplitJoin41_SplitJoin29_SplitJoin29_split2_7981_8246_8312_8385_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_8327() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child0_8317_8383_split[0], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_Hier_8349_8382_split[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child0_8317_8383_split[1], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_Hier_8349_8382_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8328() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_Hier_8349_8382_join[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child0_8317_8383_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_Hier_8349_8382_join[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child0_8317_8383_join[1]));
		ENDFOR
	ENDFOR
}}

void butterfly_8073(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin47_SplitJoin33_SplitJoin33_split2_7983_8249_8313_8387_split[0]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin47_SplitJoin33_SplitJoin33_split2_7983_8249_8313_8387_split[0]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin47_SplitJoin33_SplitJoin33_split2_7983_8249_8313_8387_join[0], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin47_SplitJoin33_SplitJoin33_split2_7983_8249_8313_8387_join[0], __sa2) ; 
	}
	ENDFOR
}

void butterfly_8074(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin47_SplitJoin33_SplitJoin33_split2_7983_8249_8313_8387_split[1]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin47_SplitJoin33_SplitJoin33_split2_7983_8249_8313_8387_split[1]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = -4.371139E-8 ; 
		WN1.imag = -1.0 ; 
		WN2.real = 1.1924881E-8 ; 
		WN2.imag = 1.0 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin47_SplitJoin33_SplitJoin33_split2_7983_8249_8313_8387_join[1], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin47_SplitJoin33_SplitJoin33_split2_7983_8249_8313_8387_join[1], __sa2) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8214() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		push_complex(&SplitJoin47_SplitJoin33_SplitJoin33_split2_7983_8249_8313_8387_split[0], pop_complex(&SplitJoin45_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child1_8319_8386_split[0]));
		push_complex(&SplitJoin47_SplitJoin33_SplitJoin33_split2_7983_8249_8313_8387_split[1], pop_complex(&SplitJoin45_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child1_8319_8386_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8215() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		push_complex(&SplitJoin45_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child1_8319_8386_join[0], pop_complex(&SplitJoin47_SplitJoin33_SplitJoin33_split2_7983_8249_8313_8387_join[0]));
		push_complex(&SplitJoin45_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child1_8319_8386_join[0], pop_complex(&SplitJoin47_SplitJoin33_SplitJoin33_split2_7983_8249_8313_8387_join[1]));
	ENDFOR
}}

void butterfly_8075(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin51_SplitJoin37_SplitJoin37_split2_7985_8252_8315_8388_split[0]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin51_SplitJoin37_SplitJoin37_split2_7985_8252_8315_8388_split[0]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin51_SplitJoin37_SplitJoin37_split2_7985_8252_8315_8388_join[0], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin51_SplitJoin37_SplitJoin37_split2_7985_8252_8315_8388_join[0], __sa2) ; 
	}
	ENDFOR
}

void butterfly_8076(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin51_SplitJoin37_SplitJoin37_split2_7985_8252_8315_8388_split[1]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin51_SplitJoin37_SplitJoin37_split2_7985_8252_8315_8388_split[1]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = -4.371139E-8 ; 
		WN1.imag = -1.0 ; 
		WN2.real = 1.1924881E-8 ; 
		WN2.imag = 1.0 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin51_SplitJoin37_SplitJoin37_split2_7985_8252_8315_8388_join[1], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin51_SplitJoin37_SplitJoin37_split2_7985_8252_8315_8388_join[1], __sa2) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8216() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		push_complex(&SplitJoin51_SplitJoin37_SplitJoin37_split2_7985_8252_8315_8388_split[0], pop_complex(&SplitJoin45_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child1_8319_8386_split[1]));
		push_complex(&SplitJoin51_SplitJoin37_SplitJoin37_split2_7985_8252_8315_8388_split[1], pop_complex(&SplitJoin45_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child1_8319_8386_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8217() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		push_complex(&SplitJoin45_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child1_8319_8386_join[1], pop_complex(&SplitJoin51_SplitJoin37_SplitJoin37_split2_7985_8252_8315_8388_join[0]));
		push_complex(&SplitJoin45_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child1_8319_8386_join[1], pop_complex(&SplitJoin51_SplitJoin37_SplitJoin37_split2_7985_8252_8315_8388_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_8329() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin45_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child1_8319_8386_split[0], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_Hier_8349_8382_split[1]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin45_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child1_8319_8386_split[1], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_Hier_8349_8382_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8330() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_Hier_8349_8382_join[1], pop_complex(&SplitJoin45_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child1_8319_8386_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_Hier_8349_8382_join[1], pop_complex(&SplitJoin45_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child1_8319_8386_join[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_8326() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_Hier_8349_8382_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8325WEIGHTED_ROUND_ROBIN_Splitter_8326));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_Hier_8349_8382_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8325WEIGHTED_ROUND_ROBIN_Splitter_8326));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8331() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8331WEIGHTED_ROUND_ROBIN_Splitter_8218, pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_Hier_8349_8382_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8331WEIGHTED_ROUND_ROBIN_Splitter_8218, pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_Hier_8349_8382_join[1]));
		ENDFOR
	ENDFOR
}}

void butterfly_8078(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_7992_8235_8316_8390_split[0]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_7992_8235_8316_8390_split[0]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_7992_8235_8316_8390_join[0], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_7992_8235_8316_8390_join[0], __sa2) ; 
	}
	ENDFOR
}

void butterfly_8079(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_7992_8235_8316_8390_split[1]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_7992_8235_8316_8390_split[1]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 0.70710677 ; 
		WN1.imag = -0.70710677 ; 
		WN2.real = -0.70710665 ; 
		WN2.imag = 0.7071069 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_7992_8235_8316_8390_join[1], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_7992_8235_8316_8390_join[1], __sa2) ; 
	}
	ENDFOR
}

void butterfly_8080(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_7992_8235_8316_8390_split[2]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_7992_8235_8316_8390_split[2]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = -4.371139E-8 ; 
		WN1.imag = -1.0 ; 
		WN2.real = 1.1924881E-8 ; 
		WN2.imag = 1.0 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_7992_8235_8316_8390_join[2], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_7992_8235_8316_8390_join[2], __sa2) ; 
	}
	ENDFOR
}

void butterfly_8081(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_7992_8235_8316_8390_split[3]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_7992_8235_8316_8390_split[3]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = -0.70710677 ; 
		WN1.imag = -0.70710677 ; 
		WN2.real = 0.707107 ; 
		WN2.imag = 0.70710653 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_7992_8235_8316_8390_join[3], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_7992_8235_8316_8390_join[3], __sa2) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8220() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_7992_8235_8316_8390_split[__iter_], pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_7990_8234_8350_8389_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8221() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_7990_8234_8350_8389_join[0], pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_7992_8235_8316_8390_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void butterfly_8082(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin34_SplitJoin22_SplitJoin22_split2_7994_8240_8318_8391_split[0]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin34_SplitJoin22_SplitJoin22_split2_7994_8240_8318_8391_split[0]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin34_SplitJoin22_SplitJoin22_split2_7994_8240_8318_8391_join[0], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin34_SplitJoin22_SplitJoin22_split2_7994_8240_8318_8391_join[0], __sa2) ; 
	}
	ENDFOR
}

void butterfly_8083(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin34_SplitJoin22_SplitJoin22_split2_7994_8240_8318_8391_split[1]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin34_SplitJoin22_SplitJoin22_split2_7994_8240_8318_8391_split[1]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 0.70710677 ; 
		WN1.imag = -0.70710677 ; 
		WN2.real = -0.70710665 ; 
		WN2.imag = 0.7071069 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin34_SplitJoin22_SplitJoin22_split2_7994_8240_8318_8391_join[1], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin34_SplitJoin22_SplitJoin22_split2_7994_8240_8318_8391_join[1], __sa2) ; 
	}
	ENDFOR
}

void butterfly_8084(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin34_SplitJoin22_SplitJoin22_split2_7994_8240_8318_8391_split[2]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin34_SplitJoin22_SplitJoin22_split2_7994_8240_8318_8391_split[2]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = -4.371139E-8 ; 
		WN1.imag = -1.0 ; 
		WN2.real = 1.1924881E-8 ; 
		WN2.imag = 1.0 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin34_SplitJoin22_SplitJoin22_split2_7994_8240_8318_8391_join[2], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin34_SplitJoin22_SplitJoin22_split2_7994_8240_8318_8391_join[2], __sa2) ; 
	}
	ENDFOR
}

void butterfly_8085(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin34_SplitJoin22_SplitJoin22_split2_7994_8240_8318_8391_split[3]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin34_SplitJoin22_SplitJoin22_split2_7994_8240_8318_8391_split[3]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = -0.70710677 ; 
		WN1.imag = -0.70710677 ; 
		WN2.real = 0.707107 ; 
		WN2.imag = 0.70710653 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin34_SplitJoin22_SplitJoin22_split2_7994_8240_8318_8391_join[3], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin34_SplitJoin22_SplitJoin22_split2_7994_8240_8318_8391_join[3], __sa2) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8222() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin34_SplitJoin22_SplitJoin22_split2_7994_8240_8318_8391_split[__iter_], pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_7990_8234_8350_8389_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8223() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_7990_8234_8350_8389_join[1], pop_complex(&SplitJoin34_SplitJoin22_SplitJoin22_split2_7994_8240_8318_8391_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_8218() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_7990_8234_8350_8389_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8331WEIGHTED_ROUND_ROBIN_Splitter_8218));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_7990_8234_8350_8389_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8331WEIGHTED_ROUND_ROBIN_Splitter_8218));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8219() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8219WEIGHTED_ROUND_ROBIN_Splitter_8337, pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_7990_8234_8350_8389_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8219WEIGHTED_ROUND_ROBIN_Splitter_8337, pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_7990_8234_8350_8389_join[1]));
		ENDFOR
	ENDFOR
}}

void magnitude_8339(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_8351_8392_split[0]));
		push_float(&SplitJoin24_magnitude_Fiss_8351_8392_join[0], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_8340(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_8351_8392_split[1]));
		push_float(&SplitJoin24_magnitude_Fiss_8351_8392_join[1], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_8341(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_8351_8392_split[2]));
		push_float(&SplitJoin24_magnitude_Fiss_8351_8392_join[2], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_8342(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_8351_8392_split[3]));
		push_float(&SplitJoin24_magnitude_Fiss_8351_8392_join[3], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_8343(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_8351_8392_split[4]));
		push_float(&SplitJoin24_magnitude_Fiss_8351_8392_join[4], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8337() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 5, __iter_++)
			push_complex(&SplitJoin24_magnitude_Fiss_8351_8392_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8219WEIGHTED_ROUND_ROBIN_Splitter_8337));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8338() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 5, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8338sink_8087, pop_float(&SplitJoin24_magnitude_Fiss_8351_8392_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void sink_8087(){
	FOR(uint32_t, __iter_steady_, 0, <, 80, __iter_steady_++) {
		printf("%.10f", pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8338sink_8087));
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8177WEIGHTED_ROUND_ROBIN_Splitter_8320);
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_7979_8232_8310_8384_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 2, __iter_init_1_++)
		init_buffer_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8002_8226_8346_8366_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_8004_8227_8347_8367_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 2, __iter_init_3_++)
		init_buffer_complex(&SplitJoin65_SplitJoin49_SplitJoin49_AnonFilter_a0_8010_8263_8352_8368_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 2, __iter_init_4_++)
		init_buffer_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_7990_8234_8350_8389_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 2, __iter_init_5_++)
		init_buffer_complex(&SplitJoin41_SplitJoin29_SplitJoin29_split2_7981_8246_8312_8385_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 4, __iter_init_6_++)
		init_buffer_complex(&SplitJoin58_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child1_8314_8381_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 2, __iter_init_7_++)
		init_buffer_complex(&SplitJoin69_SplitJoin53_SplitJoin53_AnonFilter_a0_8016_8266_8353_8369_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 4, __iter_init_8_++)
		init_buffer_complex(&SplitJoin58_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child1_8314_8381_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 2, __iter_init_9_++)
		init_buffer_complex(&SplitJoin41_SplitJoin29_SplitJoin29_split2_7981_8246_8312_8385_join[__iter_init_9_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_8156butterfly_8061);
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_complex(&SplitJoin47_SplitJoin33_SplitJoin33_split2_7983_8249_8313_8387_join[__iter_init_10_]);
	ENDFOR
	init_buffer_complex(&butterfly_8067Post_CollapsedDataParallel_2_8175);
	FOR(int, __iter_init_11_, 0, <, 2, __iter_init_11_++)
		init_buffer_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8002_8226_8346_8366_join[__iter_init_11_]);
	ENDFOR
	init_buffer_complex(&butterfly_8066Post_CollapsedDataParallel_2_8172);
	FOR(int, __iter_init_12_, 0, <, 4, __iter_init_12_++)
		init_buffer_complex(&SplitJoin34_SplitJoin22_SplitJoin22_split2_7994_8240_8318_8391_split[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_complex(&SplitJoin91_SplitJoin75_SplitJoin75_AnonFilter_a0_8046_8281_8359_8376_split[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_complex(&SplitJoin45_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child1_8319_8386_join[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 2, __iter_init_15_++)
		init_buffer_complex(&SplitJoin51_SplitJoin37_SplitJoin37_split2_7985_8252_8315_8388_split[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_7979_8232_8310_8384_split[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_complex(&SplitJoin79_SplitJoin63_SplitJoin63_AnonFilter_a0_8030_8273_8309_8372_join[__iter_init_17_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8334WEIGHTED_ROUND_ROBIN_Splitter_8176);
	init_buffer_complex(&Pre_CollapsedDataParallel_1_8174butterfly_8067);
	FOR(int, __iter_init_18_, 0, <, 2, __iter_init_18_++)
		init_buffer_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_Hier_8349_8382_split[__iter_init_18_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_8168butterfly_8065);
	init_buffer_complex(&Pre_CollapsedDataParallel_1_8153butterfly_8060);
	FOR(int, __iter_init_19_, 0, <, 2, __iter_init_19_++)
		init_buffer_complex(&SplitJoin47_SplitJoin33_SplitJoin33_split2_7983_8249_8313_8387_split[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 2, __iter_init_20_++)
		init_buffer_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_7990_8234_8350_8389_split[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 2, __iter_init_21_++)
		init_buffer_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_Hier_8348_8379_split[__iter_init_21_]);
	ENDFOR
	init_buffer_complex(&butterfly_8063Post_CollapsedDataParallel_2_8163);
	FOR(int, __iter_init_22_, 0, <, 2, __iter_init_22_++)
		init_buffer_complex(&SplitJoin79_SplitJoin63_SplitJoin63_AnonFilter_a0_8030_8273_8309_8372_split[__iter_init_22_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8338sink_8087);
	init_buffer_complex(&butterfly_8065Post_CollapsedDataParallel_2_8169);
	FOR(int, __iter_init_23_, 0, <, 2, __iter_init_23_++)
		init_buffer_complex(&SplitJoin81_SplitJoin65_SplitJoin65_AnonFilter_a0_8032_8274_8356_8373_split[__iter_init_23_]);
	ENDFOR
	FOR(int, __iter_init_24_, 0, <, 2, __iter_init_24_++)
		init_buffer_complex(&SplitJoin51_SplitJoin37_SplitJoin37_split2_7985_8252_8315_8388_join[__iter_init_24_]);
	ENDFOR
	FOR(int, __iter_init_25_, 0, <, 2, __iter_init_25_++)
		init_buffer_complex(&SplitJoin93_SplitJoin77_SplitJoin77_AnonFilter_a0_8048_8282_8360_8377_join[__iter_init_25_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8325WEIGHTED_ROUND_ROBIN_Splitter_8326);
	FOR(int, __iter_init_26_, 0, <, 2, __iter_init_26_++)
		init_buffer_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child0_8317_8383_split[__iter_init_26_]);
	ENDFOR
	FOR(int, __iter_init_27_, 0, <, 2, __iter_init_27_++)
		init_buffer_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child0_8317_8383_join[__iter_init_27_]);
	ENDFOR
	FOR(int, __iter_init_28_, 0, <, 2, __iter_init_28_++)
		init_buffer_complex(&SplitJoin45_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_child1_8319_8386_split[__iter_init_28_]);
	ENDFOR
	FOR(int, __iter_init_29_, 0, <, 4, __iter_init_29_++)
		init_buffer_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child0_8311_8380_join[__iter_init_29_]);
	ENDFOR
	FOR(int, __iter_init_30_, 0, <, 2, __iter_init_30_++)
		init_buffer_complex(&SplitJoin71_SplitJoin55_SplitJoin55_AnonFilter_a0_8018_8267_8354_8370_join[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 2, __iter_init_31_++)
		init_buffer_complex(&SplitJoin83_SplitJoin67_SplitJoin67_AnonFilter_a0_8034_8275_8357_8374_join[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 2, __iter_init_32_++)
		init_buffer_complex(&SplitJoin97_SplitJoin81_SplitJoin81_AnonFilter_a0_8054_8285_8361_8378_split[__iter_init_32_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_8159butterfly_8062);
	FOR(int, __iter_init_33_, 0, <, 2, __iter_init_33_++)
		init_buffer_complex(&SplitJoin69_SplitJoin53_SplitJoin53_AnonFilter_a0_8016_8266_8353_8369_join[__iter_init_33_]);
	ENDFOR
	FOR(int, __iter_init_34_, 0, <, 2, __iter_init_34_++)
		init_buffer_complex(&SplitJoin87_SplitJoin71_SplitJoin71_AnonFilter_a0_8040_8278_8358_8375_split[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 2, __iter_init_35_++)
		init_buffer_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8000_8225_8308_8365_join[__iter_init_35_]);
	ENDFOR
	FOR(int, __iter_init_36_, 0, <, 2, __iter_init_36_++)
		init_buffer_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7998_8224_8345_8364_join[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 2, __iter_init_37_++)
		init_buffer_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_8004_8227_8347_8367_split[__iter_init_37_]);
	ENDFOR
	FOR(int, __iter_init_38_, 0, <, 2, __iter_init_38_++)
		init_buffer_complex(&SplitJoin65_SplitJoin49_SplitJoin49_AnonFilter_a0_8010_8263_8352_8368_join[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 4, __iter_init_39_++)
		init_buffer_complex(&SplitJoin34_SplitJoin22_SplitJoin22_split2_7994_8240_8318_8391_join[__iter_init_39_]);
	ENDFOR
	FOR(int, __iter_init_40_, 0, <, 2, __iter_init_40_++)
		init_buffer_complex(&SplitJoin75_SplitJoin59_SplitJoin59_AnonFilter_a0_8024_8270_8355_8371_join[__iter_init_40_]);
	ENDFOR
	FOR(int, __iter_init_41_, 0, <, 2, __iter_init_41_++)
		init_buffer_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8000_8225_8308_8365_split[__iter_init_41_]);
	ENDFOR
	FOR(int, __iter_init_42_, 0, <, 2, __iter_init_42_++)
		init_buffer_complex(&SplitJoin71_SplitJoin55_SplitJoin55_AnonFilter_a0_8018_8267_8354_8370_split[__iter_init_42_]);
	ENDFOR
	FOR(int, __iter_init_43_, 0, <, 2, __iter_init_43_++)
		init_buffer_complex(&SplitJoin81_SplitJoin65_SplitJoin65_AnonFilter_a0_8032_8274_8356_8373_join[__iter_init_43_]);
	ENDFOR
	FOR(int, __iter_init_44_, 0, <, 2, __iter_init_44_++)
		init_buffer_complex(&SplitJoin75_SplitJoin59_SplitJoin59_AnonFilter_a0_8024_8270_8355_8371_split[__iter_init_44_]);
	ENDFOR
	FOR(int, __iter_init_45_, 0, <, 2, __iter_init_45_++)
		init_buffer_complex(&SplitJoin97_SplitJoin81_SplitJoin81_AnonFilter_a0_8054_8285_8361_8378_join[__iter_init_45_]);
	ENDFOR
	FOR(int, __iter_init_46_, 0, <, 4, __iter_init_46_++)
		init_buffer_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_7992_8235_8316_8390_split[__iter_init_46_]);
	ENDFOR
	init_buffer_complex(&butterfly_8060Post_CollapsedDataParallel_2_8154);
	FOR(int, __iter_init_47_, 0, <, 2, __iter_init_47_++)
		init_buffer_complex(&SplitJoin93_SplitJoin77_SplitJoin77_AnonFilter_a0_8048_8282_8360_8377_split[__iter_init_47_]);
	ENDFOR
	init_buffer_complex(&butterfly_8061Post_CollapsedDataParallel_2_8157);
	FOR(int, __iter_init_48_, 0, <, 2, __iter_init_48_++)
		init_buffer_complex(&SplitJoin83_SplitJoin67_SplitJoin67_AnonFilter_a0_8034_8275_8357_8374_split[__iter_init_48_]);
	ENDFOR
	FOR(int, __iter_init_49_, 0, <, 2, __iter_init_49_++)
		init_buffer_complex(&SplitJoin87_SplitJoin71_SplitJoin71_AnonFilter_a0_8040_8278_8358_8375_join[__iter_init_49_]);
	ENDFOR
	FOR(int, __iter_init_50_, 0, <, 2, __iter_init_50_++)
		init_buffer_complex(&SplitJoin91_SplitJoin75_SplitJoin75_AnonFilter_a0_8046_8281_8359_8376_join[__iter_init_50_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_8162butterfly_8063);
	FOR(int, __iter_init_51_, 0, <, 2, __iter_init_51_++)
		init_buffer_complex(&SplitJoin0_source_Fiss_8344_8363_join[__iter_init_51_]);
	ENDFOR
	FOR(int, __iter_init_52_, 0, <, 2, __iter_init_52_++)
		init_buffer_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7998_8224_8345_8364_split[__iter_init_52_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8331WEIGHTED_ROUND_ROBIN_Splitter_8218);
	FOR(int, __iter_init_53_, 0, <, 2, __iter_init_53_++)
		init_buffer_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_7977_8231_Hier_Hier_8349_8382_join[__iter_init_53_]);
	ENDFOR
	FOR(int, __iter_init_54_, 0, <, 2, __iter_init_54_++)
		init_buffer_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_Hier_8348_8379_join[__iter_init_54_]);
	ENDFOR
	FOR(int, __iter_init_55_, 0, <, 2, __iter_init_55_++)
		init_buffer_complex(&SplitJoin0_source_Fiss_8344_8363_split[__iter_init_55_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8219WEIGHTED_ROUND_ROBIN_Splitter_8337);
	init_buffer_complex(&butterfly_8064Post_CollapsedDataParallel_2_8166);
	FOR(int, __iter_init_56_, 0, <, 4, __iter_init_56_++)
		init_buffer_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_7956_8229_Hier_child0_8311_8380_split[__iter_init_56_]);
	ENDFOR
	FOR(int, __iter_init_57_, 0, <, 4, __iter_init_57_++)
		init_buffer_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_7992_8235_8316_8390_join[__iter_init_57_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_8165butterfly_8064);
	init_buffer_complex(&Pre_CollapsedDataParallel_1_8171butterfly_8066);
	init_buffer_complex(&butterfly_8062Post_CollapsedDataParallel_2_8160);
	FOR(int, __iter_init_58_, 0, <, 5, __iter_init_58_++)
		init_buffer_complex(&SplitJoin24_magnitude_Fiss_8351_8392_split[__iter_init_58_]);
	ENDFOR
	FOR(int, __iter_init_59_, 0, <, 5, __iter_init_59_++)
		init_buffer_float(&SplitJoin24_magnitude_Fiss_8351_8392_join[__iter_init_59_]);
	ENDFOR
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_8333();
			source_8335();
			source_8336();
		WEIGHTED_ROUND_ROBIN_Joiner_8334();
		WEIGHTED_ROUND_ROBIN_Splitter_8176();
			WEIGHTED_ROUND_ROBIN_Splitter_8178();
				WEIGHTED_ROUND_ROBIN_Splitter_8180();
					WEIGHTED_ROUND_ROBIN_Splitter_8182();
						Identity_8006();
						Identity_8008();
					WEIGHTED_ROUND_ROBIN_Joiner_8183();
					WEIGHTED_ROUND_ROBIN_Splitter_8184();
						Identity_8012();
						Identity_8014();
					WEIGHTED_ROUND_ROBIN_Joiner_8185();
				WEIGHTED_ROUND_ROBIN_Joiner_8181();
				WEIGHTED_ROUND_ROBIN_Splitter_8186();
					WEIGHTED_ROUND_ROBIN_Splitter_8188();
						Identity_8020();
						Identity_8022();
					WEIGHTED_ROUND_ROBIN_Joiner_8189();
					WEIGHTED_ROUND_ROBIN_Splitter_8190();
						Identity_8026();
						Identity_8028();
					WEIGHTED_ROUND_ROBIN_Joiner_8191();
				WEIGHTED_ROUND_ROBIN_Joiner_8187();
			WEIGHTED_ROUND_ROBIN_Joiner_8179();
			WEIGHTED_ROUND_ROBIN_Splitter_8192();
				WEIGHTED_ROUND_ROBIN_Splitter_8194();
					WEIGHTED_ROUND_ROBIN_Splitter_8196();
						Identity_8036();
						Identity_8038();
					WEIGHTED_ROUND_ROBIN_Joiner_8197();
					WEIGHTED_ROUND_ROBIN_Splitter_8198();
						Identity_8042();
						Identity_8044();
					WEIGHTED_ROUND_ROBIN_Joiner_8199();
				WEIGHTED_ROUND_ROBIN_Joiner_8195();
				WEIGHTED_ROUND_ROBIN_Splitter_8200();
					WEIGHTED_ROUND_ROBIN_Splitter_8202();
						Identity_8050();
						Identity_8052();
					WEIGHTED_ROUND_ROBIN_Joiner_8203();
					WEIGHTED_ROUND_ROBIN_Splitter_8204();
						Identity_8056();
						Identity_8058();
					WEIGHTED_ROUND_ROBIN_Joiner_8205();
				WEIGHTED_ROUND_ROBIN_Joiner_8201();
			WEIGHTED_ROUND_ROBIN_Joiner_8193();
		WEIGHTED_ROUND_ROBIN_Joiner_8177();
		WEIGHTED_ROUND_ROBIN_Splitter_8320();
			WEIGHTED_ROUND_ROBIN_Splitter_8321();
				Pre_CollapsedDataParallel_1_8153();
				butterfly_8060();
				Post_CollapsedDataParallel_2_8154();
				Pre_CollapsedDataParallel_1_8156();
				butterfly_8061();
				Post_CollapsedDataParallel_2_8157();
				Pre_CollapsedDataParallel_1_8159();
				butterfly_8062();
				Post_CollapsedDataParallel_2_8160();
				Pre_CollapsedDataParallel_1_8162();
				butterfly_8063();
				Post_CollapsedDataParallel_2_8163();
			WEIGHTED_ROUND_ROBIN_Joiner_8322();
			WEIGHTED_ROUND_ROBIN_Splitter_8323();
				Pre_CollapsedDataParallel_1_8165();
				butterfly_8064();
				Post_CollapsedDataParallel_2_8166();
				Pre_CollapsedDataParallel_1_8168();
				butterfly_8065();
				Post_CollapsedDataParallel_2_8169();
				Pre_CollapsedDataParallel_1_8171();
				butterfly_8066();
				Post_CollapsedDataParallel_2_8172();
				Pre_CollapsedDataParallel_1_8174();
				butterfly_8067();
				Post_CollapsedDataParallel_2_8175();
			WEIGHTED_ROUND_ROBIN_Joiner_8324();
		WEIGHTED_ROUND_ROBIN_Joiner_8325();
		WEIGHTED_ROUND_ROBIN_Splitter_8326();
			WEIGHTED_ROUND_ROBIN_Splitter_8327();
				WEIGHTED_ROUND_ROBIN_Splitter_8210();
					butterfly_8069();
					butterfly_8070();
				WEIGHTED_ROUND_ROBIN_Joiner_8211();
				WEIGHTED_ROUND_ROBIN_Splitter_8212();
					butterfly_8071();
					butterfly_8072();
				WEIGHTED_ROUND_ROBIN_Joiner_8213();
			WEIGHTED_ROUND_ROBIN_Joiner_8328();
			WEIGHTED_ROUND_ROBIN_Splitter_8329();
				WEIGHTED_ROUND_ROBIN_Splitter_8214();
					butterfly_8073();
					butterfly_8074();
				WEIGHTED_ROUND_ROBIN_Joiner_8215();
				WEIGHTED_ROUND_ROBIN_Splitter_8216();
					butterfly_8075();
					butterfly_8076();
				WEIGHTED_ROUND_ROBIN_Joiner_8217();
			WEIGHTED_ROUND_ROBIN_Joiner_8330();
		WEIGHTED_ROUND_ROBIN_Joiner_8331();
		WEIGHTED_ROUND_ROBIN_Splitter_8218();
			WEIGHTED_ROUND_ROBIN_Splitter_8220();
				butterfly_8078();
				butterfly_8079();
				butterfly_8080();
				butterfly_8081();
			WEIGHTED_ROUND_ROBIN_Joiner_8221();
			WEIGHTED_ROUND_ROBIN_Splitter_8222();
				butterfly_8082();
				butterfly_8083();
				butterfly_8084();
				butterfly_8085();
			WEIGHTED_ROUND_ROBIN_Joiner_8223();
		WEIGHTED_ROUND_ROBIN_Joiner_8219();
		WEIGHTED_ROUND_ROBIN_Splitter_8337();
			magnitude_8339();
			magnitude_8340();
			magnitude_8341();
			magnitude_8342();
			magnitude_8343();
		WEIGHTED_ROUND_ROBIN_Joiner_8338();
		sink_8087();
	ENDFOR
	return EXIT_SUCCESS;
}
