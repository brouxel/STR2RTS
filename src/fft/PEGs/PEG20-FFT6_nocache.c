#include "PEG20-FFT6_nocache.h"

buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[16];
buffer_complex_t SplitJoin12_CombineDFT_Fiss_2943_2953_split[8];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_2937_2947_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2900WEIGHTED_ROUND_ROBIN_Splitter_2917;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2878WEIGHTED_ROUND_ROBIN_Splitter_2899;
buffer_complex_t SplitJoin12_CombineDFT_Fiss_2943_2953_join[8];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_2945_2955_split[2];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_2939_2949_split[8];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_2937_2947_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2840WEIGHTED_ROUND_ROBIN_Splitter_2843;
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_2939_2949_join[8];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_2945_2955_join[2];
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_2938_2948_split[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2850WEIGHTED_ROUND_ROBIN_Splitter_2859;
buffer_complex_t SplitJoin14_CombineDFT_Fiss_2944_2954_join[4];
buffer_complex_t CombineDFT_2836CPrinter_2837;
buffer_complex_t SplitJoin14_CombineDFT_Fiss_2944_2954_split[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2860WEIGHTED_ROUND_ROBIN_Splitter_2877;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_2941_2951_join[20];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2844WEIGHTED_ROUND_ROBIN_Splitter_2849;
buffer_complex_t SplitJoin10_CombineDFT_Fiss_2942_2952_join[16];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_2942_2952_split[16];
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[16];
buffer_complex_t FFTTestSource_2825FFTReorderSimple_2826;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_2941_2951_split[20];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2928WEIGHTED_ROUND_ROBIN_Splitter_2933;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2934CombineDFT_2836;
buffer_complex_t FFTReorderSimple_2826WEIGHTED_ROUND_ROBIN_Splitter_2839;
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_2938_2948_join[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2918WEIGHTED_ROUND_ROBIN_Splitter_2927;


CombineDFT_2879_t CombineDFT_2879_s;
CombineDFT_2879_t CombineDFT_2880_s;
CombineDFT_2879_t CombineDFT_2881_s;
CombineDFT_2879_t CombineDFT_2882_s;
CombineDFT_2879_t CombineDFT_2883_s;
CombineDFT_2879_t CombineDFT_2884_s;
CombineDFT_2879_t CombineDFT_2885_s;
CombineDFT_2879_t CombineDFT_2886_s;
CombineDFT_2879_t CombineDFT_2887_s;
CombineDFT_2879_t CombineDFT_2888_s;
CombineDFT_2879_t CombineDFT_2889_s;
CombineDFT_2879_t CombineDFT_2890_s;
CombineDFT_2879_t CombineDFT_2891_s;
CombineDFT_2879_t CombineDFT_2892_s;
CombineDFT_2879_t CombineDFT_2893_s;
CombineDFT_2879_t CombineDFT_2894_s;
CombineDFT_2879_t CombineDFT_2895_s;
CombineDFT_2879_t CombineDFT_2896_s;
CombineDFT_2879_t CombineDFT_2897_s;
CombineDFT_2879_t CombineDFT_2898_s;
CombineDFT_2879_t CombineDFT_2901_s;
CombineDFT_2879_t CombineDFT_2902_s;
CombineDFT_2879_t CombineDFT_2903_s;
CombineDFT_2879_t CombineDFT_2904_s;
CombineDFT_2879_t CombineDFT_2905_s;
CombineDFT_2879_t CombineDFT_2906_s;
CombineDFT_2879_t CombineDFT_2907_s;
CombineDFT_2879_t CombineDFT_2908_s;
CombineDFT_2879_t CombineDFT_2909_s;
CombineDFT_2879_t CombineDFT_2910_s;
CombineDFT_2879_t CombineDFT_2911_s;
CombineDFT_2879_t CombineDFT_2912_s;
CombineDFT_2879_t CombineDFT_2913_s;
CombineDFT_2879_t CombineDFT_2914_s;
CombineDFT_2879_t CombineDFT_2915_s;
CombineDFT_2879_t CombineDFT_2916_s;
CombineDFT_2879_t CombineDFT_2919_s;
CombineDFT_2879_t CombineDFT_2920_s;
CombineDFT_2879_t CombineDFT_2921_s;
CombineDFT_2879_t CombineDFT_2922_s;
CombineDFT_2879_t CombineDFT_2923_s;
CombineDFT_2879_t CombineDFT_2924_s;
CombineDFT_2879_t CombineDFT_2925_s;
CombineDFT_2879_t CombineDFT_2926_s;
CombineDFT_2879_t CombineDFT_2929_s;
CombineDFT_2879_t CombineDFT_2930_s;
CombineDFT_2879_t CombineDFT_2931_s;
CombineDFT_2879_t CombineDFT_2932_s;
CombineDFT_2879_t CombineDFT_2935_s;
CombineDFT_2879_t CombineDFT_2936_s;
CombineDFT_2879_t CombineDFT_2836_s;

void FFTTestSource_2825(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t c1;
		complex_t zero;
		c1.real = 1.0 ; 
		c1.imag = 0.0 ; 
		zero.real = 0.0 ; 
		zero.imag = 0.0 ; 
		push_complex(&FFTTestSource_2825FFTReorderSimple_2826, zero) ; 
		push_complex(&FFTTestSource_2825FFTReorderSimple_2826, c1) ; 
		FOR(int, i, 0,  < , 62, i++) {
			push_complex(&FFTTestSource_2825FFTReorderSimple_2826, zero) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2826(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&FFTTestSource_2825FFTReorderSimple_2826, i)) ; 
			push_complex(&FFTReorderSimple_2826WEIGHTED_ROUND_ROBIN_Splitter_2839, __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 64, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&FFTTestSource_2825FFTReorderSimple_2826, i)) ; 
			push_complex(&FFTReorderSimple_2826WEIGHTED_ROUND_ROBIN_Splitter_2839, __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&FFTTestSource_2825FFTReorderSimple_2826) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2841(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_2937_2947_split[0], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_2937_2947_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 32, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_2937_2947_split[0], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_2937_2947_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_2937_2947_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2842(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_2937_2947_split[1], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_2937_2947_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 32, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_2937_2947_split[1], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_2937_2947_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_2937_2947_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2839() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_2937_2947_split[0], pop_complex(&FFTReorderSimple_2826WEIGHTED_ROUND_ROBIN_Splitter_2839));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_2937_2947_split[1], pop_complex(&FFTReorderSimple_2826WEIGHTED_ROUND_ROBIN_Splitter_2839));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2840() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2840WEIGHTED_ROUND_ROBIN_Splitter_2843, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_2937_2947_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2840WEIGHTED_ROUND_ROBIN_Splitter_2843, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_2937_2947_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_2845(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_2938_2948_split[0], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_2938_2948_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_2938_2948_split[0], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_2938_2948_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_2938_2948_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2846(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_2938_2948_split[1], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_2938_2948_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_2938_2948_split[1], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_2938_2948_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_2938_2948_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2847(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_2938_2948_split[2], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_2938_2948_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_2938_2948_split[2], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_2938_2948_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_2938_2948_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2848(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_2938_2948_split[3], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_2938_2948_join[3], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_2938_2948_split[3], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_2938_2948_join[3], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_2938_2948_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2843() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin2_FFTReorderSimple_Fiss_2938_2948_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2840WEIGHTED_ROUND_ROBIN_Splitter_2843));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2844() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2844WEIGHTED_ROUND_ROBIN_Splitter_2849, pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_2938_2948_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_2851(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_split[0], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_split[0], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2852(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_split[1], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_split[1], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2853(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_split[2], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_split[2], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2854(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_split[3], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_join[3], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_split[3], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_join[3], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2855(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_split[4], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_join[4], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_split[4], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_join[4], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2856(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_split[5], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_join[5], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_split[5], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_join[5], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2857(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_split[6], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_join[6], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_split[6], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_join[6], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2858(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_split[7], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_join[7], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_split[7], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_join[7], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2849() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2844WEIGHTED_ROUND_ROBIN_Splitter_2849));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2850() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2850WEIGHTED_ROUND_ROBIN_Splitter_2859, pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_2861(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[0], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[0], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2862(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[1], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[1], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2863(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[2], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[2], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2864(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[3], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[3], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[3], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[3], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2865(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[4], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[4], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[4], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[4], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2866(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[5], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[5], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[5], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[5], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2867(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[6], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[6], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[6], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[6], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2868(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[7], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[7], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[7], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[7], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2869(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[8], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[8], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[8], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[8], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[8]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2870(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[9], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[9], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[9], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[9], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[9]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2871(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[10], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[10], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[10], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[10], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[10]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2872(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[11], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[11], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[11], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[11], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[11]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2873(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[12], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[12], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[12], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[12], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[12]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2874(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[13], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[13], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[13], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[13], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[13]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2875(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[14], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[14], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[14], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[14], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[14]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2876(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[15], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[15], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[15], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[15], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[15]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2859() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2850WEIGHTED_ROUND_ROBIN_Splitter_2859));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2860() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2860WEIGHTED_ROUND_ROBIN_Splitter_2877, pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2879(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[0], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2879_s.wn.real) - (w.imag * CombineDFT_2879_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2879_s.wn.imag) + (w.imag * CombineDFT_2879_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[0]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2880(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[1], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2880_s.wn.real) - (w.imag * CombineDFT_2880_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2880_s.wn.imag) + (w.imag * CombineDFT_2880_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[1]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2881(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[2], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2881_s.wn.real) - (w.imag * CombineDFT_2881_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2881_s.wn.imag) + (w.imag * CombineDFT_2881_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[2]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2882(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[3], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2882_s.wn.real) - (w.imag * CombineDFT_2882_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2882_s.wn.imag) + (w.imag * CombineDFT_2882_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[3]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2883(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[4], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[4], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2883_s.wn.real) - (w.imag * CombineDFT_2883_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2883_s.wn.imag) + (w.imag * CombineDFT_2883_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[4]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2884(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[5], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[5], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2884_s.wn.real) - (w.imag * CombineDFT_2884_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2884_s.wn.imag) + (w.imag * CombineDFT_2884_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[5]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2885(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[6], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[6], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2885_s.wn.real) - (w.imag * CombineDFT_2885_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2885_s.wn.imag) + (w.imag * CombineDFT_2885_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[6]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2886(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[7], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[7], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2886_s.wn.real) - (w.imag * CombineDFT_2886_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2886_s.wn.imag) + (w.imag * CombineDFT_2886_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[7]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2887(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[8], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[8], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2887_s.wn.real) - (w.imag * CombineDFT_2887_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2887_s.wn.imag) + (w.imag * CombineDFT_2887_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[8]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_join[8], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2888(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[9], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[9], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2888_s.wn.real) - (w.imag * CombineDFT_2888_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2888_s.wn.imag) + (w.imag * CombineDFT_2888_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[9]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_join[9], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2889(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[10], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[10], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2889_s.wn.real) - (w.imag * CombineDFT_2889_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2889_s.wn.imag) + (w.imag * CombineDFT_2889_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[10]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_join[10], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2890(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[11], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[11], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2890_s.wn.real) - (w.imag * CombineDFT_2890_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2890_s.wn.imag) + (w.imag * CombineDFT_2890_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[11]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_join[11], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2891(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[12], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[12], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2891_s.wn.real) - (w.imag * CombineDFT_2891_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2891_s.wn.imag) + (w.imag * CombineDFT_2891_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[12]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_join[12], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2892(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[13], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[13], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2892_s.wn.real) - (w.imag * CombineDFT_2892_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2892_s.wn.imag) + (w.imag * CombineDFT_2892_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[13]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_join[13], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2893(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[14], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[14], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2893_s.wn.real) - (w.imag * CombineDFT_2893_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2893_s.wn.imag) + (w.imag * CombineDFT_2893_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[14]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_join[14], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2894(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[15], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[15], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2894_s.wn.real) - (w.imag * CombineDFT_2894_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2894_s.wn.imag) + (w.imag * CombineDFT_2894_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[15]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_join[15], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2895(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[16], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[16], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2895_s.wn.real) - (w.imag * CombineDFT_2895_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2895_s.wn.imag) + (w.imag * CombineDFT_2895_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[16]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_join[16], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2896(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[17], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[17], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2896_s.wn.real) - (w.imag * CombineDFT_2896_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2896_s.wn.imag) + (w.imag * CombineDFT_2896_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[17]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_join[17], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2897(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[18], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[18], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2897_s.wn.real) - (w.imag * CombineDFT_2897_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2897_s.wn.imag) + (w.imag * CombineDFT_2897_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[18]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_join[18], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2898(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[19], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[19], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2898_s.wn.real) - (w.imag * CombineDFT_2898_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2898_s.wn.imag) + (w.imag * CombineDFT_2898_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[19]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_join[19], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2877() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 20, __iter_++)
			push_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2860WEIGHTED_ROUND_ROBIN_Splitter_2877));
			push_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2860WEIGHTED_ROUND_ROBIN_Splitter_2877));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2878() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 20, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2878WEIGHTED_ROUND_ROBIN_Splitter_2899, pop_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_join[__iter_]));
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2878WEIGHTED_ROUND_ROBIN_Splitter_2899, pop_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_2901(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[0], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2901_s.wn.real) - (w.imag * CombineDFT_2901_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2901_s.wn.imag) + (w.imag * CombineDFT_2901_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[0]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2902(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[1], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2902_s.wn.real) - (w.imag * CombineDFT_2902_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2902_s.wn.imag) + (w.imag * CombineDFT_2902_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[1]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2903(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[2], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2903_s.wn.real) - (w.imag * CombineDFT_2903_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2903_s.wn.imag) + (w.imag * CombineDFT_2903_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[2]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2904(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[3], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2904_s.wn.real) - (w.imag * CombineDFT_2904_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2904_s.wn.imag) + (w.imag * CombineDFT_2904_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[3]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2905(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[4], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[4], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2905_s.wn.real) - (w.imag * CombineDFT_2905_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2905_s.wn.imag) + (w.imag * CombineDFT_2905_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[4]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2906(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[5], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[5], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2906_s.wn.real) - (w.imag * CombineDFT_2906_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2906_s.wn.imag) + (w.imag * CombineDFT_2906_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[5]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2907(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[6], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[6], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2907_s.wn.real) - (w.imag * CombineDFT_2907_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2907_s.wn.imag) + (w.imag * CombineDFT_2907_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[6]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2908(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[7], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[7], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2908_s.wn.real) - (w.imag * CombineDFT_2908_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2908_s.wn.imag) + (w.imag * CombineDFT_2908_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[7]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2909(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[8], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[8], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2909_s.wn.real) - (w.imag * CombineDFT_2909_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2909_s.wn.imag) + (w.imag * CombineDFT_2909_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[8]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_join[8], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2910(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[9], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[9], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2910_s.wn.real) - (w.imag * CombineDFT_2910_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2910_s.wn.imag) + (w.imag * CombineDFT_2910_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[9]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_join[9], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2911(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[10], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[10], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2911_s.wn.real) - (w.imag * CombineDFT_2911_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2911_s.wn.imag) + (w.imag * CombineDFT_2911_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[10]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_join[10], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2912(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[11], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[11], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2912_s.wn.real) - (w.imag * CombineDFT_2912_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2912_s.wn.imag) + (w.imag * CombineDFT_2912_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[11]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_join[11], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2913(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[12], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[12], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2913_s.wn.real) - (w.imag * CombineDFT_2913_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2913_s.wn.imag) + (w.imag * CombineDFT_2913_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[12]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_join[12], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2914(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[13], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[13], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2914_s.wn.real) - (w.imag * CombineDFT_2914_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2914_s.wn.imag) + (w.imag * CombineDFT_2914_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[13]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_join[13], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2915(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[14], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[14], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2915_s.wn.real) - (w.imag * CombineDFT_2915_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2915_s.wn.imag) + (w.imag * CombineDFT_2915_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[14]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_join[14], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2916(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[15], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[15], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2916_s.wn.real) - (w.imag * CombineDFT_2916_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2916_s.wn.imag) + (w.imag * CombineDFT_2916_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[15]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_join[15], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2899() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2878WEIGHTED_ROUND_ROBIN_Splitter_2899));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2900() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2900WEIGHTED_ROUND_ROBIN_Splitter_2917, pop_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2919(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_split[0], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2919_s.wn.real) - (w.imag * CombineDFT_2919_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2919_s.wn.imag) + (w.imag * CombineDFT_2919_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_split[0]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2920(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_split[1], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2920_s.wn.real) - (w.imag * CombineDFT_2920_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2920_s.wn.imag) + (w.imag * CombineDFT_2920_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_split[1]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2921(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_split[2], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2921_s.wn.real) - (w.imag * CombineDFT_2921_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2921_s.wn.imag) + (w.imag * CombineDFT_2921_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_split[2]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2922(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_split[3], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2922_s.wn.real) - (w.imag * CombineDFT_2922_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2922_s.wn.imag) + (w.imag * CombineDFT_2922_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_split[3]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2923(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_split[4], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_split[4], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2923_s.wn.real) - (w.imag * CombineDFT_2923_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2923_s.wn.imag) + (w.imag * CombineDFT_2923_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_split[4]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2924(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_split[5], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_split[5], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2924_s.wn.real) - (w.imag * CombineDFT_2924_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2924_s.wn.imag) + (w.imag * CombineDFT_2924_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_split[5]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2925(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_split[6], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_split[6], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2925_s.wn.real) - (w.imag * CombineDFT_2925_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2925_s.wn.imag) + (w.imag * CombineDFT_2925_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_split[6]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2926(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_split[7], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_split[7], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2926_s.wn.real) - (w.imag * CombineDFT_2926_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2926_s.wn.imag) + (w.imag * CombineDFT_2926_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_split[7]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2917() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2900WEIGHTED_ROUND_ROBIN_Splitter_2917));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2918() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2918WEIGHTED_ROUND_ROBIN_Splitter_2927, pop_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2929(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_2944_2954_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_2944_2954_split[0], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2929_s.wn.real) - (w.imag * CombineDFT_2929_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2929_s.wn.imag) + (w.imag * CombineDFT_2929_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_2944_2954_split[0]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_2944_2954_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2930(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_2944_2954_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_2944_2954_split[1], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2930_s.wn.real) - (w.imag * CombineDFT_2930_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2930_s.wn.imag) + (w.imag * CombineDFT_2930_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_2944_2954_split[1]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_2944_2954_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2931(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_2944_2954_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_2944_2954_split[2], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2931_s.wn.real) - (w.imag * CombineDFT_2931_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2931_s.wn.imag) + (w.imag * CombineDFT_2931_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_2944_2954_split[2]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_2944_2954_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2932(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_2944_2954_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_2944_2954_split[3], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2932_s.wn.real) - (w.imag * CombineDFT_2932_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2932_s.wn.imag) + (w.imag * CombineDFT_2932_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_2944_2954_split[3]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_2944_2954_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2927() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin14_CombineDFT_Fiss_2944_2954_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2918WEIGHTED_ROUND_ROBIN_Splitter_2927));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2928() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2928WEIGHTED_ROUND_ROBIN_Splitter_2933, pop_complex(&SplitJoin14_CombineDFT_Fiss_2944_2954_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2935(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[32];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 16, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_2945_2955_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_2945_2955_split[0], (16 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(16 + i)].real = (y0.real - y1w.real) ; 
			results[(16 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2935_s.wn.real) - (w.imag * CombineDFT_2935_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2935_s.wn.imag) + (w.imag * CombineDFT_2935_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin16_CombineDFT_Fiss_2945_2955_split[0]) ; 
			push_complex(&SplitJoin16_CombineDFT_Fiss_2945_2955_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2936(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[32];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 16, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_2945_2955_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_2945_2955_split[1], (16 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(16 + i)].real = (y0.real - y1w.real) ; 
			results[(16 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2936_s.wn.real) - (w.imag * CombineDFT_2936_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2936_s.wn.imag) + (w.imag * CombineDFT_2936_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin16_CombineDFT_Fiss_2945_2955_split[1]) ; 
			push_complex(&SplitJoin16_CombineDFT_Fiss_2945_2955_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2933() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_2945_2955_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2928WEIGHTED_ROUND_ROBIN_Splitter_2933));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_2945_2955_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2928WEIGHTED_ROUND_ROBIN_Splitter_2933));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2934() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2934CombineDFT_2836, pop_complex(&SplitJoin16_CombineDFT_Fiss_2945_2955_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2934CombineDFT_2836, pop_complex(&SplitJoin16_CombineDFT_Fiss_2945_2955_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_2836(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[64];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2934CombineDFT_2836, i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2934CombineDFT_2836, (32 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(32 + i)].real = (y0.real - y1w.real) ; 
			results[(32 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2836_s.wn.real) - (w.imag * CombineDFT_2836_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2836_s.wn.imag) + (w.imag * CombineDFT_2836_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2934CombineDFT_2836) ; 
			push_complex(&CombineDFT_2836CPrinter_2837, results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CPrinter_2837(){
	FOR(uint32_t, __iter_steady_, 0, <, 320, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&CombineDFT_2836CPrinter_2837));
		printf("%.10f", c.real);
		printf("\n");
		printf("%.10f", c.imag);
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 16, __iter_init_0_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_2937_2947_split[__iter_init_2_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2900WEIGHTED_ROUND_ROBIN_Splitter_2917);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2878WEIGHTED_ROUND_ROBIN_Splitter_2899);
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_2943_2953_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 2, __iter_init_4_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_2945_2955_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 2, __iter_init_6_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_2937_2947_join[__iter_init_6_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2840WEIGHTED_ROUND_ROBIN_Splitter_2843);
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_2939_2949_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_2945_2955_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 4, __iter_init_9_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_2938_2948_split[__iter_init_9_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2850WEIGHTED_ROUND_ROBIN_Splitter_2859);
	FOR(int, __iter_init_10_, 0, <, 4, __iter_init_10_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_2944_2954_join[__iter_init_10_]);
	ENDFOR
	init_buffer_complex(&CombineDFT_2836CPrinter_2837);
	FOR(int, __iter_init_11_, 0, <, 4, __iter_init_11_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_2944_2954_split[__iter_init_11_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2860WEIGHTED_ROUND_ROBIN_Splitter_2877);
	FOR(int, __iter_init_12_, 0, <, 20, __iter_init_12_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_join[__iter_init_12_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2844WEIGHTED_ROUND_ROBIN_Splitter_2849);
	FOR(int, __iter_init_13_, 0, <, 16, __iter_init_13_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_join[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 16, __iter_init_14_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_2942_2952_split[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 16, __iter_init_15_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_2940_2950_join[__iter_init_15_]);
	ENDFOR
	init_buffer_complex(&FFTTestSource_2825FFTReorderSimple_2826);
	FOR(int, __iter_init_16_, 0, <, 20, __iter_init_16_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_2941_2951_split[__iter_init_16_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2928WEIGHTED_ROUND_ROBIN_Splitter_2933);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2934CombineDFT_2836);
	init_buffer_complex(&FFTReorderSimple_2826WEIGHTED_ROUND_ROBIN_Splitter_2839);
	FOR(int, __iter_init_17_, 0, <, 4, __iter_init_17_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_2938_2948_join[__iter_init_17_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2918WEIGHTED_ROUND_ROBIN_Splitter_2927);
// --- init: CombineDFT_2879
	 {
	 ; 
	CombineDFT_2879_s.wn.real = -1.0 ; 
	CombineDFT_2879_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2880
	 {
	 ; 
	CombineDFT_2880_s.wn.real = -1.0 ; 
	CombineDFT_2880_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2881
	 {
	 ; 
	CombineDFT_2881_s.wn.real = -1.0 ; 
	CombineDFT_2881_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2882
	 {
	 ; 
	CombineDFT_2882_s.wn.real = -1.0 ; 
	CombineDFT_2882_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2883
	 {
	 ; 
	CombineDFT_2883_s.wn.real = -1.0 ; 
	CombineDFT_2883_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2884
	 {
	 ; 
	CombineDFT_2884_s.wn.real = -1.0 ; 
	CombineDFT_2884_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2885
	 {
	 ; 
	CombineDFT_2885_s.wn.real = -1.0 ; 
	CombineDFT_2885_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2886
	 {
	 ; 
	CombineDFT_2886_s.wn.real = -1.0 ; 
	CombineDFT_2886_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2887
	 {
	 ; 
	CombineDFT_2887_s.wn.real = -1.0 ; 
	CombineDFT_2887_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2888
	 {
	 ; 
	CombineDFT_2888_s.wn.real = -1.0 ; 
	CombineDFT_2888_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2889
	 {
	 ; 
	CombineDFT_2889_s.wn.real = -1.0 ; 
	CombineDFT_2889_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2890
	 {
	 ; 
	CombineDFT_2890_s.wn.real = -1.0 ; 
	CombineDFT_2890_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2891
	 {
	 ; 
	CombineDFT_2891_s.wn.real = -1.0 ; 
	CombineDFT_2891_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2892
	 {
	 ; 
	CombineDFT_2892_s.wn.real = -1.0 ; 
	CombineDFT_2892_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2893
	 {
	 ; 
	CombineDFT_2893_s.wn.real = -1.0 ; 
	CombineDFT_2893_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2894
	 {
	 ; 
	CombineDFT_2894_s.wn.real = -1.0 ; 
	CombineDFT_2894_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2895
	 {
	 ; 
	CombineDFT_2895_s.wn.real = -1.0 ; 
	CombineDFT_2895_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2896
	 {
	 ; 
	CombineDFT_2896_s.wn.real = -1.0 ; 
	CombineDFT_2896_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2897
	 {
	 ; 
	CombineDFT_2897_s.wn.real = -1.0 ; 
	CombineDFT_2897_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2898
	 {
	 ; 
	CombineDFT_2898_s.wn.real = -1.0 ; 
	CombineDFT_2898_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2901
	 {
	 ; 
	CombineDFT_2901_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2901_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2902
	 {
	 ; 
	CombineDFT_2902_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2902_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2903
	 {
	 ; 
	CombineDFT_2903_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2903_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2904
	 {
	 ; 
	CombineDFT_2904_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2904_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2905
	 {
	 ; 
	CombineDFT_2905_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2905_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2906
	 {
	 ; 
	CombineDFT_2906_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2906_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2907
	 {
	 ; 
	CombineDFT_2907_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2907_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2908
	 {
	 ; 
	CombineDFT_2908_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2908_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2909
	 {
	 ; 
	CombineDFT_2909_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2909_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2910
	 {
	 ; 
	CombineDFT_2910_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2910_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2911
	 {
	 ; 
	CombineDFT_2911_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2911_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2912
	 {
	 ; 
	CombineDFT_2912_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2912_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2913
	 {
	 ; 
	CombineDFT_2913_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2913_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2914
	 {
	 ; 
	CombineDFT_2914_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2914_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2915
	 {
	 ; 
	CombineDFT_2915_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2915_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2916
	 {
	 ; 
	CombineDFT_2916_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2916_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2919
	 {
	 ; 
	CombineDFT_2919_s.wn.real = 0.70710677 ; 
	CombineDFT_2919_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2920
	 {
	 ; 
	CombineDFT_2920_s.wn.real = 0.70710677 ; 
	CombineDFT_2920_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2921
	 {
	 ; 
	CombineDFT_2921_s.wn.real = 0.70710677 ; 
	CombineDFT_2921_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2922
	 {
	 ; 
	CombineDFT_2922_s.wn.real = 0.70710677 ; 
	CombineDFT_2922_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2923
	 {
	 ; 
	CombineDFT_2923_s.wn.real = 0.70710677 ; 
	CombineDFT_2923_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2924
	 {
	 ; 
	CombineDFT_2924_s.wn.real = 0.70710677 ; 
	CombineDFT_2924_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2925
	 {
	 ; 
	CombineDFT_2925_s.wn.real = 0.70710677 ; 
	CombineDFT_2925_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2926
	 {
	 ; 
	CombineDFT_2926_s.wn.real = 0.70710677 ; 
	CombineDFT_2926_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2929
	 {
	 ; 
	CombineDFT_2929_s.wn.real = 0.9238795 ; 
	CombineDFT_2929_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_2930
	 {
	 ; 
	CombineDFT_2930_s.wn.real = 0.9238795 ; 
	CombineDFT_2930_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_2931
	 {
	 ; 
	CombineDFT_2931_s.wn.real = 0.9238795 ; 
	CombineDFT_2931_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_2932
	 {
	 ; 
	CombineDFT_2932_s.wn.real = 0.9238795 ; 
	CombineDFT_2932_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_2935
	 {
	 ; 
	CombineDFT_2935_s.wn.real = 0.98078525 ; 
	CombineDFT_2935_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_2936
	 {
	 ; 
	CombineDFT_2936_s.wn.real = 0.98078525 ; 
	CombineDFT_2936_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_2836
	 {
	 ; 
	CombineDFT_2836_s.wn.real = 0.9951847 ; 
	CombineDFT_2836_s.wn.imag = -0.09801714 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FFTTestSource_2825();
		FFTReorderSimple_2826();
		WEIGHTED_ROUND_ROBIN_Splitter_2839();
			FFTReorderSimple_2841();
			FFTReorderSimple_2842();
		WEIGHTED_ROUND_ROBIN_Joiner_2840();
		WEIGHTED_ROUND_ROBIN_Splitter_2843();
			FFTReorderSimple_2845();
			FFTReorderSimple_2846();
			FFTReorderSimple_2847();
			FFTReorderSimple_2848();
		WEIGHTED_ROUND_ROBIN_Joiner_2844();
		WEIGHTED_ROUND_ROBIN_Splitter_2849();
			FFTReorderSimple_2851();
			FFTReorderSimple_2852();
			FFTReorderSimple_2853();
			FFTReorderSimple_2854();
			FFTReorderSimple_2855();
			FFTReorderSimple_2856();
			FFTReorderSimple_2857();
			FFTReorderSimple_2858();
		WEIGHTED_ROUND_ROBIN_Joiner_2850();
		WEIGHTED_ROUND_ROBIN_Splitter_2859();
			FFTReorderSimple_2861();
			FFTReorderSimple_2862();
			FFTReorderSimple_2863();
			FFTReorderSimple_2864();
			FFTReorderSimple_2865();
			FFTReorderSimple_2866();
			FFTReorderSimple_2867();
			FFTReorderSimple_2868();
			FFTReorderSimple_2869();
			FFTReorderSimple_2870();
			FFTReorderSimple_2871();
			FFTReorderSimple_2872();
			FFTReorderSimple_2873();
			FFTReorderSimple_2874();
			FFTReorderSimple_2875();
			FFTReorderSimple_2876();
		WEIGHTED_ROUND_ROBIN_Joiner_2860();
		WEIGHTED_ROUND_ROBIN_Splitter_2877();
			CombineDFT_2879();
			CombineDFT_2880();
			CombineDFT_2881();
			CombineDFT_2882();
			CombineDFT_2883();
			CombineDFT_2884();
			CombineDFT_2885();
			CombineDFT_2886();
			CombineDFT_2887();
			CombineDFT_2888();
			CombineDFT_2889();
			CombineDFT_2890();
			CombineDFT_2891();
			CombineDFT_2892();
			CombineDFT_2893();
			CombineDFT_2894();
			CombineDFT_2895();
			CombineDFT_2896();
			CombineDFT_2897();
			CombineDFT_2898();
		WEIGHTED_ROUND_ROBIN_Joiner_2878();
		WEIGHTED_ROUND_ROBIN_Splitter_2899();
			CombineDFT_2901();
			CombineDFT_2902();
			CombineDFT_2903();
			CombineDFT_2904();
			CombineDFT_2905();
			CombineDFT_2906();
			CombineDFT_2907();
			CombineDFT_2908();
			CombineDFT_2909();
			CombineDFT_2910();
			CombineDFT_2911();
			CombineDFT_2912();
			CombineDFT_2913();
			CombineDFT_2914();
			CombineDFT_2915();
			CombineDFT_2916();
		WEIGHTED_ROUND_ROBIN_Joiner_2900();
		WEIGHTED_ROUND_ROBIN_Splitter_2917();
			CombineDFT_2919();
			CombineDFT_2920();
			CombineDFT_2921();
			CombineDFT_2922();
			CombineDFT_2923();
			CombineDFT_2924();
			CombineDFT_2925();
			CombineDFT_2926();
		WEIGHTED_ROUND_ROBIN_Joiner_2918();
		WEIGHTED_ROUND_ROBIN_Splitter_2927();
			CombineDFT_2929();
			CombineDFT_2930();
			CombineDFT_2931();
			CombineDFT_2932();
		WEIGHTED_ROUND_ROBIN_Joiner_2928();
		WEIGHTED_ROUND_ROBIN_Splitter_2933();
			CombineDFT_2935();
			CombineDFT_2936();
		WEIGHTED_ROUND_ROBIN_Joiner_2934();
		CombineDFT_2836();
		CPrinter_2837();
	ENDFOR
	return EXIT_SUCCESS;
}
