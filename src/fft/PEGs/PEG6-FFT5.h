#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=96 on the compile command line
#else
#if BUF_SIZEMAX < 96
#error BUF_SIZEMAX too small, it must be at least 96
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif


void WEIGHTED_ROUND_ROBIN_Splitter_7748();
void source(buffer_void_t *chanin, buffer_complex_t *chanout);
void source_7750();
void source_7751();
void WEIGHTED_ROUND_ROBIN_Joiner_7749();
void WEIGHTED_ROUND_ROBIN_Splitter_7591();
void WEIGHTED_ROUND_ROBIN_Splitter_7593();
void WEIGHTED_ROUND_ROBIN_Splitter_7595();
void WEIGHTED_ROUND_ROBIN_Splitter_7597();
void Identity(buffer_complex_t *chanin, buffer_complex_t *chanout);
void Identity_7421();
void Identity_7423();
void WEIGHTED_ROUND_ROBIN_Joiner_7598();
void WEIGHTED_ROUND_ROBIN_Splitter_7599();
void Identity_7427();
void Identity_7429();
void WEIGHTED_ROUND_ROBIN_Joiner_7600();
void WEIGHTED_ROUND_ROBIN_Joiner_7596();
void WEIGHTED_ROUND_ROBIN_Splitter_7601();
void WEIGHTED_ROUND_ROBIN_Splitter_7603();
void Identity_7435();
void Identity_7437();
void WEIGHTED_ROUND_ROBIN_Joiner_7604();
void WEIGHTED_ROUND_ROBIN_Splitter_7605();
void Identity_7441();
void Identity_7443();
void WEIGHTED_ROUND_ROBIN_Joiner_7606();
void WEIGHTED_ROUND_ROBIN_Joiner_7602();
void WEIGHTED_ROUND_ROBIN_Joiner_7594();
void WEIGHTED_ROUND_ROBIN_Splitter_7607();
void WEIGHTED_ROUND_ROBIN_Splitter_7609();
void WEIGHTED_ROUND_ROBIN_Splitter_7611();
void Identity_7451();
void Identity_7453();
void WEIGHTED_ROUND_ROBIN_Joiner_7612();
void WEIGHTED_ROUND_ROBIN_Splitter_7613();
void Identity_7457();
void Identity_7459();
void WEIGHTED_ROUND_ROBIN_Joiner_7614();
void WEIGHTED_ROUND_ROBIN_Joiner_7610();
void WEIGHTED_ROUND_ROBIN_Splitter_7615();
void WEIGHTED_ROUND_ROBIN_Splitter_7617();
void Identity_7465();
void Identity_7467();
void WEIGHTED_ROUND_ROBIN_Joiner_7618();
void WEIGHTED_ROUND_ROBIN_Splitter_7619();
void Identity_7471();
void Identity_7473();
void WEIGHTED_ROUND_ROBIN_Joiner_7620();
void WEIGHTED_ROUND_ROBIN_Joiner_7616();
void WEIGHTED_ROUND_ROBIN_Joiner_7608();
void WEIGHTED_ROUND_ROBIN_Joiner_7592();
void WEIGHTED_ROUND_ROBIN_Splitter_7735();
void WEIGHTED_ROUND_ROBIN_Splitter_7736();
void Pre_CollapsedDataParallel_1(buffer_complex_t *chanin, buffer_complex_t *chanout);
void Pre_CollapsedDataParallel_1_7568();
void butterfly(buffer_complex_t *chanin, buffer_complex_t *chanout);
void butterfly_7475();
void Post_CollapsedDataParallel_2(buffer_complex_t *chanin, buffer_complex_t *chanout);
void Post_CollapsedDataParallel_2_7569();
void Pre_CollapsedDataParallel_1_7571();
void butterfly_7476();
void Post_CollapsedDataParallel_2_7572();
void Pre_CollapsedDataParallel_1_7574();
void butterfly_7477();
void Post_CollapsedDataParallel_2_7575();
void Pre_CollapsedDataParallel_1_7577();
void butterfly_7478();
void Post_CollapsedDataParallel_2_7578();
void WEIGHTED_ROUND_ROBIN_Joiner_7737();
void WEIGHTED_ROUND_ROBIN_Splitter_7738();
void Pre_CollapsedDataParallel_1_7580();
void butterfly_7479();
void Post_CollapsedDataParallel_2_7581();
void Pre_CollapsedDataParallel_1_7583();
void butterfly_7480();
void Post_CollapsedDataParallel_2_7584();
void Pre_CollapsedDataParallel_1_7586();
void butterfly_7481();
void Post_CollapsedDataParallel_2_7587();
void Pre_CollapsedDataParallel_1_7589();
void butterfly_7482();
void Post_CollapsedDataParallel_2_7590();
void WEIGHTED_ROUND_ROBIN_Joiner_7739();
void WEIGHTED_ROUND_ROBIN_Joiner_7740();
void WEIGHTED_ROUND_ROBIN_Splitter_7741();
void WEIGHTED_ROUND_ROBIN_Splitter_7742();
void WEIGHTED_ROUND_ROBIN_Splitter_7625();
void butterfly_7484();
void butterfly_7485();
void WEIGHTED_ROUND_ROBIN_Joiner_7626();
void WEIGHTED_ROUND_ROBIN_Splitter_7627();
void butterfly_7486();
void butterfly_7487();
void WEIGHTED_ROUND_ROBIN_Joiner_7628();
void WEIGHTED_ROUND_ROBIN_Joiner_7743();
void WEIGHTED_ROUND_ROBIN_Splitter_7744();
void WEIGHTED_ROUND_ROBIN_Splitter_7629();
void butterfly_7488();
void butterfly_7489();
void WEIGHTED_ROUND_ROBIN_Joiner_7630();
void WEIGHTED_ROUND_ROBIN_Splitter_7631();
void butterfly_7490();
void butterfly_7491();
void WEIGHTED_ROUND_ROBIN_Joiner_7632();
void WEIGHTED_ROUND_ROBIN_Joiner_7745();
void WEIGHTED_ROUND_ROBIN_Joiner_7746();
void WEIGHTED_ROUND_ROBIN_Splitter_7633();
void WEIGHTED_ROUND_ROBIN_Splitter_7635();
void butterfly_7493();
void butterfly_7494();
void butterfly_7495();
void butterfly_7496();
void WEIGHTED_ROUND_ROBIN_Joiner_7636();
void WEIGHTED_ROUND_ROBIN_Splitter_7637();
void butterfly_7497();
void butterfly_7498();
void butterfly_7499();
void butterfly_7500();
void WEIGHTED_ROUND_ROBIN_Joiner_7638();
void WEIGHTED_ROUND_ROBIN_Joiner_7634();
void WEIGHTED_ROUND_ROBIN_Splitter_7752();
void magnitude(buffer_complex_t *chanin, buffer_float_t *chanout);
void magnitude_7754();
void magnitude_7755();
void magnitude_7756();
void magnitude_7757();
void magnitude_7758();
void magnitude_7759();
void WEIGHTED_ROUND_ROBIN_Joiner_7753();
void sink(buffer_float_t *chanin);
void sink_7502();

#ifdef __cplusplus
}
#endif
#endif
