#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=1408 on the compile command line
#else
#if BUF_SIZEMAX < 1408
#error BUF_SIZEMAX too small, it must be at least 1408
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	complex_t wn;
} CombineDFT_4706_t;
void FFTTestSource_4657();
void FFTReorderSimple_4658();
void WEIGHTED_ROUND_ROBIN_Splitter_4671();
void FFTReorderSimple_4673();
void FFTReorderSimple_4674();
void WEIGHTED_ROUND_ROBIN_Joiner_4672();
void WEIGHTED_ROUND_ROBIN_Splitter_4675();
void FFTReorderSimple_4677();
void FFTReorderSimple_4678();
void FFTReorderSimple_4679();
void FFTReorderSimple_4680();
void WEIGHTED_ROUND_ROBIN_Joiner_4676();
void WEIGHTED_ROUND_ROBIN_Splitter_4681();
void FFTReorderSimple_4683();
void FFTReorderSimple_4684();
void FFTReorderSimple_4685();
void FFTReorderSimple_4686();
void FFTReorderSimple_4687();
void FFTReorderSimple_4688();
void FFTReorderSimple_4689();
void FFTReorderSimple_4690();
void WEIGHTED_ROUND_ROBIN_Joiner_4682();
void WEIGHTED_ROUND_ROBIN_Splitter_4691();
void FFTReorderSimple_4693();
void FFTReorderSimple_4694();
void FFTReorderSimple_4695();
void FFTReorderSimple_4696();
void FFTReorderSimple_4697();
void FFTReorderSimple_4698();
void FFTReorderSimple_4699();
void FFTReorderSimple_4700();
void FFTReorderSimple_4701();
void FFTReorderSimple_4702();
void FFTReorderSimple_4703();
void WEIGHTED_ROUND_ROBIN_Joiner_4692();
void WEIGHTED_ROUND_ROBIN_Splitter_4704();
void CombineDFT_4706();
void CombineDFT_4707();
void CombineDFT_4708();
void CombineDFT_4709();
void CombineDFT_4710();
void CombineDFT_4711();
void CombineDFT_4712();
void CombineDFT_4713();
void CombineDFT_4714();
void CombineDFT_4715();
void CombineDFT_4716();
void WEIGHTED_ROUND_ROBIN_Joiner_4705();
void WEIGHTED_ROUND_ROBIN_Splitter_4717();
void CombineDFT_4719();
void CombineDFT_4720();
void CombineDFT_4721();
void CombineDFT_4722();
void CombineDFT_4723();
void CombineDFT_4724();
void CombineDFT_4725();
void CombineDFT_4726();
void CombineDFT_4727();
void CombineDFT_4728();
void CombineDFT_4729();
void WEIGHTED_ROUND_ROBIN_Joiner_4718();
void WEIGHTED_ROUND_ROBIN_Splitter_4730();
void CombineDFT_4732();
void CombineDFT_4733();
void CombineDFT_4734();
void CombineDFT_4735();
void CombineDFT_4736();
void CombineDFT_4737();
void CombineDFT_4738();
void CombineDFT_4739();
void WEIGHTED_ROUND_ROBIN_Joiner_4731();
void WEIGHTED_ROUND_ROBIN_Splitter_4740();
void CombineDFT_4742();
void CombineDFT_4743();
void CombineDFT_4744();
void CombineDFT_4745();
void WEIGHTED_ROUND_ROBIN_Joiner_4741();
void WEIGHTED_ROUND_ROBIN_Splitter_4746();
void CombineDFT_4748();
void CombineDFT_4749();
void WEIGHTED_ROUND_ROBIN_Joiner_4747();
void CombineDFT_4668();
void CPrinter_4669();

#ifdef __cplusplus
}
#endif
#endif
