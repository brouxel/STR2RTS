#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=384 on the compile command line
#else
#if BUF_SIZEMAX < 384
#error BUF_SIZEMAX too small, it must be at least 384
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	complex_t wn;
} CombineDFT_4523_t;
void FFTTestSource(buffer_complex_t *chanout);
void FFTTestSource_4473();
void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout);
void FFTReorderSimple_4474();
void WEIGHTED_ROUND_ROBIN_Splitter_4487();
void FFTReorderSimple_4489();
void FFTReorderSimple_4490();
void WEIGHTED_ROUND_ROBIN_Joiner_4488();
void WEIGHTED_ROUND_ROBIN_Splitter_4491();
void FFTReorderSimple_4493();
void FFTReorderSimple_4494();
void FFTReorderSimple_4495();
void FFTReorderSimple_4496();
void WEIGHTED_ROUND_ROBIN_Joiner_4492();
void WEIGHTED_ROUND_ROBIN_Splitter_4497();
void FFTReorderSimple_4499();
void FFTReorderSimple_4500();
void FFTReorderSimple_4501();
void FFTReorderSimple_4502();
void FFTReorderSimple_4503();
void FFTReorderSimple_4504();
void FFTReorderSimple_4505();
void FFTReorderSimple_4506();
void WEIGHTED_ROUND_ROBIN_Joiner_4498();
void WEIGHTED_ROUND_ROBIN_Splitter_4507();
void FFTReorderSimple_4509();
void FFTReorderSimple_4510();
void FFTReorderSimple_4511();
void FFTReorderSimple_4512();
void FFTReorderSimple_4513();
void FFTReorderSimple_4514();
void FFTReorderSimple_4515();
void FFTReorderSimple_4516();
void FFTReorderSimple_4517();
void FFTReorderSimple_4518();
void FFTReorderSimple_4519();
void FFTReorderSimple_4520();
void WEIGHTED_ROUND_ROBIN_Joiner_4508();
void WEIGHTED_ROUND_ROBIN_Splitter_4521();
void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout);
void CombineDFT_4523();
void CombineDFT_4524();
void CombineDFT_4525();
void CombineDFT_4526();
void CombineDFT_4527();
void CombineDFT_4528();
void CombineDFT_4529();
void CombineDFT_4530();
void CombineDFT_4531();
void CombineDFT_4532();
void CombineDFT_4533();
void CombineDFT_4534();
void WEIGHTED_ROUND_ROBIN_Joiner_4522();
void WEIGHTED_ROUND_ROBIN_Splitter_4535();
void CombineDFT_4537();
void CombineDFT_4538();
void CombineDFT_4539();
void CombineDFT_4540();
void CombineDFT_4541();
void CombineDFT_4542();
void CombineDFT_4543();
void CombineDFT_4544();
void CombineDFT_4545();
void CombineDFT_4546();
void CombineDFT_4547();
void CombineDFT_4548();
void WEIGHTED_ROUND_ROBIN_Joiner_4536();
void WEIGHTED_ROUND_ROBIN_Splitter_4549();
void CombineDFT_4551();
void CombineDFT_4552();
void CombineDFT_4553();
void CombineDFT_4554();
void CombineDFT_4555();
void CombineDFT_4556();
void CombineDFT_4557();
void CombineDFT_4558();
void WEIGHTED_ROUND_ROBIN_Joiner_4550();
void WEIGHTED_ROUND_ROBIN_Splitter_4559();
void CombineDFT_4561();
void CombineDFT_4562();
void CombineDFT_4563();
void CombineDFT_4564();
void WEIGHTED_ROUND_ROBIN_Joiner_4560();
void WEIGHTED_ROUND_ROBIN_Splitter_4565();
void CombineDFT_4567();
void CombineDFT_4568();
void WEIGHTED_ROUND_ROBIN_Joiner_4566();
void CombineDFT_4484();
void CPrinter(buffer_complex_t *chanin);
void CPrinter_4485();

#ifdef __cplusplus
}
#endif
#endif
