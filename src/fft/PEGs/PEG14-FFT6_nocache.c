#include "PEG14-FFT6_nocache.h"

buffer_complex_t SplitJoin10_CombineDFT_Fiss_4194_4204_split[14];
buffer_complex_t SplitJoin12_CombineDFT_Fiss_4195_4205_join[8];
buffer_complex_t SplitJoin8_CombineDFT_Fiss_4193_4203_split[14];
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_4190_4200_split[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4112WEIGHTED_ROUND_ROBIN_Splitter_4121;
buffer_complex_t FFTTestSource_4087FFTReorderSimple_4088;
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_4191_4201_split[8];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_4197_4207_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4102WEIGHTED_ROUND_ROBIN_Splitter_4105;
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_4192_4202_join[14];
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_4190_4200_join[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4106WEIGHTED_ROUND_ROBIN_Splitter_4111;
buffer_complex_t FFTReorderSimple_4088WEIGHTED_ROUND_ROBIN_Splitter_4101;
buffer_complex_t SplitJoin12_CombineDFT_Fiss_4195_4205_split[8];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_4189_4199_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4154WEIGHTED_ROUND_ROBIN_Splitter_4169;
buffer_complex_t SplitJoin10_CombineDFT_Fiss_4194_4204_join[14];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4170WEIGHTED_ROUND_ROBIN_Splitter_4179;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4138WEIGHTED_ROUND_ROBIN_Splitter_4153;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_4193_4203_join[14];
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[14];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_4196_4206_join[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4180WEIGHTED_ROUND_ROBIN_Splitter_4185;
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_4189_4199_join[2];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_4191_4201_join[8];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4122WEIGHTED_ROUND_ROBIN_Splitter_4137;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4186CombineDFT_4098;
buffer_complex_t SplitJoin14_CombineDFT_Fiss_4196_4206_split[4];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_4197_4207_join[2];
buffer_complex_t CombineDFT_4098CPrinter_4099;


CombineDFT_4139_t CombineDFT_4139_s;
CombineDFT_4139_t CombineDFT_4140_s;
CombineDFT_4139_t CombineDFT_4141_s;
CombineDFT_4139_t CombineDFT_4142_s;
CombineDFT_4139_t CombineDFT_4143_s;
CombineDFT_4139_t CombineDFT_4144_s;
CombineDFT_4139_t CombineDFT_4145_s;
CombineDFT_4139_t CombineDFT_4146_s;
CombineDFT_4139_t CombineDFT_4147_s;
CombineDFT_4139_t CombineDFT_4148_s;
CombineDFT_4139_t CombineDFT_4149_s;
CombineDFT_4139_t CombineDFT_4150_s;
CombineDFT_4139_t CombineDFT_4151_s;
CombineDFT_4139_t CombineDFT_4152_s;
CombineDFT_4139_t CombineDFT_4155_s;
CombineDFT_4139_t CombineDFT_4156_s;
CombineDFT_4139_t CombineDFT_4157_s;
CombineDFT_4139_t CombineDFT_4158_s;
CombineDFT_4139_t CombineDFT_4159_s;
CombineDFT_4139_t CombineDFT_4160_s;
CombineDFT_4139_t CombineDFT_4161_s;
CombineDFT_4139_t CombineDFT_4162_s;
CombineDFT_4139_t CombineDFT_4163_s;
CombineDFT_4139_t CombineDFT_4164_s;
CombineDFT_4139_t CombineDFT_4165_s;
CombineDFT_4139_t CombineDFT_4166_s;
CombineDFT_4139_t CombineDFT_4167_s;
CombineDFT_4139_t CombineDFT_4168_s;
CombineDFT_4139_t CombineDFT_4171_s;
CombineDFT_4139_t CombineDFT_4172_s;
CombineDFT_4139_t CombineDFT_4173_s;
CombineDFT_4139_t CombineDFT_4174_s;
CombineDFT_4139_t CombineDFT_4175_s;
CombineDFT_4139_t CombineDFT_4176_s;
CombineDFT_4139_t CombineDFT_4177_s;
CombineDFT_4139_t CombineDFT_4178_s;
CombineDFT_4139_t CombineDFT_4181_s;
CombineDFT_4139_t CombineDFT_4182_s;
CombineDFT_4139_t CombineDFT_4183_s;
CombineDFT_4139_t CombineDFT_4184_s;
CombineDFT_4139_t CombineDFT_4187_s;
CombineDFT_4139_t CombineDFT_4188_s;
CombineDFT_4139_t CombineDFT_4098_s;

void FFTTestSource_4087(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t c1;
		complex_t zero;
		c1.real = 1.0 ; 
		c1.imag = 0.0 ; 
		zero.real = 0.0 ; 
		zero.imag = 0.0 ; 
		push_complex(&FFTTestSource_4087FFTReorderSimple_4088, zero) ; 
		push_complex(&FFTTestSource_4087FFTReorderSimple_4088, c1) ; 
		FOR(int, i, 0,  < , 62, i++) {
			push_complex(&FFTTestSource_4087FFTReorderSimple_4088, zero) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4088(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&FFTTestSource_4087FFTReorderSimple_4088, i)) ; 
			push_complex(&FFTReorderSimple_4088WEIGHTED_ROUND_ROBIN_Splitter_4101, __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 64, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&FFTTestSource_4087FFTReorderSimple_4088, i)) ; 
			push_complex(&FFTReorderSimple_4088WEIGHTED_ROUND_ROBIN_Splitter_4101, __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&FFTTestSource_4087FFTReorderSimple_4088) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4103(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_4189_4199_split[0], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_4189_4199_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 32, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_4189_4199_split[0], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_4189_4199_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_4189_4199_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4104(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_4189_4199_split[1], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_4189_4199_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 32, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_4189_4199_split[1], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_4189_4199_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_4189_4199_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4101() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_4189_4199_split[0], pop_complex(&FFTReorderSimple_4088WEIGHTED_ROUND_ROBIN_Splitter_4101));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_4189_4199_split[1], pop_complex(&FFTReorderSimple_4088WEIGHTED_ROUND_ROBIN_Splitter_4101));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4102() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4102WEIGHTED_ROUND_ROBIN_Splitter_4105, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_4189_4199_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4102WEIGHTED_ROUND_ROBIN_Splitter_4105, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_4189_4199_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_4107(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_4190_4200_split[0], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_4190_4200_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_4190_4200_split[0], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_4190_4200_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_4190_4200_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4108(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_4190_4200_split[1], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_4190_4200_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_4190_4200_split[1], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_4190_4200_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_4190_4200_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4109(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_4190_4200_split[2], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_4190_4200_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_4190_4200_split[2], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_4190_4200_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_4190_4200_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4110(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_4190_4200_split[3], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_4190_4200_join[3], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_4190_4200_split[3], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_4190_4200_join[3], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_4190_4200_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4105() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin2_FFTReorderSimple_Fiss_4190_4200_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4102WEIGHTED_ROUND_ROBIN_Splitter_4105));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4106() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4106WEIGHTED_ROUND_ROBIN_Splitter_4111, pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_4190_4200_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_4113(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_split[0], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_split[0], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4114(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_split[1], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_split[1], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4115(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_split[2], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_split[2], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4116(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_split[3], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_join[3], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_split[3], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_join[3], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4117(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_split[4], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_join[4], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_split[4], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_join[4], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4118(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_split[5], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_join[5], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_split[5], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_join[5], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4119(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_split[6], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_join[6], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_split[6], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_join[6], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4120(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_split[7], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_join[7], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_split[7], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_join[7], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4111() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4106WEIGHTED_ROUND_ROBIN_Splitter_4111));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4112() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4112WEIGHTED_ROUND_ROBIN_Splitter_4121, pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_4123(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[0], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[0], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4124(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[1], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[1], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4125(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[2], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[2], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4126(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[3], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_join[3], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[3], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_join[3], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4127(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[4], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_join[4], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[4], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_join[4], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4128(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[5], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_join[5], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[5], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_join[5], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4129(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[6], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_join[6], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[6], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_join[6], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4130(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[7], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_join[7], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[7], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_join[7], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4131(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[8], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_join[8], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[8], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_join[8], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[8]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4132(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[9], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_join[9], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[9], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_join[9], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[9]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4133(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[10], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_join[10], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[10], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_join[10], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[10]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4134(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[11], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_join[11], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[11], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_join[11], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[11]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4135(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[12], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_join[12], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[12], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_join[12], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[12]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4136(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[13], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_join[13], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[13], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_join[13], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[13]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4121() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 14, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4112WEIGHTED_ROUND_ROBIN_Splitter_4121));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4122() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 14, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4122WEIGHTED_ROUND_ROBIN_Splitter_4137, pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_4139(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[0], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4139_s.wn.real) - (w.imag * CombineDFT_4139_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4139_s.wn.imag) + (w.imag * CombineDFT_4139_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[0]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4140(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[1], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4140_s.wn.real) - (w.imag * CombineDFT_4140_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4140_s.wn.imag) + (w.imag * CombineDFT_4140_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[1]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4141(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[2], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4141_s.wn.real) - (w.imag * CombineDFT_4141_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4141_s.wn.imag) + (w.imag * CombineDFT_4141_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[2]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4142(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[3], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4142_s.wn.real) - (w.imag * CombineDFT_4142_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4142_s.wn.imag) + (w.imag * CombineDFT_4142_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[3]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4143(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[4], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[4], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4143_s.wn.real) - (w.imag * CombineDFT_4143_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4143_s.wn.imag) + (w.imag * CombineDFT_4143_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[4]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4144(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[5], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[5], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4144_s.wn.real) - (w.imag * CombineDFT_4144_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4144_s.wn.imag) + (w.imag * CombineDFT_4144_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[5]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4145(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[6], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[6], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4145_s.wn.real) - (w.imag * CombineDFT_4145_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4145_s.wn.imag) + (w.imag * CombineDFT_4145_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[6]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4146(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[7], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[7], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4146_s.wn.real) - (w.imag * CombineDFT_4146_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4146_s.wn.imag) + (w.imag * CombineDFT_4146_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[7]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4147(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[8], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[8], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4147_s.wn.real) - (w.imag * CombineDFT_4147_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4147_s.wn.imag) + (w.imag * CombineDFT_4147_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[8]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_join[8], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4148(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[9], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[9], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4148_s.wn.real) - (w.imag * CombineDFT_4148_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4148_s.wn.imag) + (w.imag * CombineDFT_4148_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[9]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_join[9], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4149(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[10], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[10], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4149_s.wn.real) - (w.imag * CombineDFT_4149_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4149_s.wn.imag) + (w.imag * CombineDFT_4149_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[10]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_join[10], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4150(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[11], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[11], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4150_s.wn.real) - (w.imag * CombineDFT_4150_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4150_s.wn.imag) + (w.imag * CombineDFT_4150_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[11]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_join[11], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4151(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[12], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[12], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4151_s.wn.real) - (w.imag * CombineDFT_4151_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4151_s.wn.imag) + (w.imag * CombineDFT_4151_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[12]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_join[12], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4152(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[13], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[13], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4152_s.wn.real) - (w.imag * CombineDFT_4152_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4152_s.wn.imag) + (w.imag * CombineDFT_4152_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[13]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_join[13], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4137() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 14, __iter_++)
			push_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4122WEIGHTED_ROUND_ROBIN_Splitter_4137));
			push_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4122WEIGHTED_ROUND_ROBIN_Splitter_4137));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4138() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 14, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4138WEIGHTED_ROUND_ROBIN_Splitter_4153, pop_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_join[__iter_]));
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4138WEIGHTED_ROUND_ROBIN_Splitter_4153, pop_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_4155(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[0], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4155_s.wn.real) - (w.imag * CombineDFT_4155_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4155_s.wn.imag) + (w.imag * CombineDFT_4155_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[0]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4156(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[1], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4156_s.wn.real) - (w.imag * CombineDFT_4156_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4156_s.wn.imag) + (w.imag * CombineDFT_4156_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[1]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4157(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[2], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4157_s.wn.real) - (w.imag * CombineDFT_4157_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4157_s.wn.imag) + (w.imag * CombineDFT_4157_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[2]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4158(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[3], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4158_s.wn.real) - (w.imag * CombineDFT_4158_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4158_s.wn.imag) + (w.imag * CombineDFT_4158_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[3]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4159(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[4], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[4], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4159_s.wn.real) - (w.imag * CombineDFT_4159_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4159_s.wn.imag) + (w.imag * CombineDFT_4159_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[4]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4160(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[5], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[5], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4160_s.wn.real) - (w.imag * CombineDFT_4160_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4160_s.wn.imag) + (w.imag * CombineDFT_4160_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[5]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4161(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[6], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[6], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4161_s.wn.real) - (w.imag * CombineDFT_4161_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4161_s.wn.imag) + (w.imag * CombineDFT_4161_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[6]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4162(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[7], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[7], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4162_s.wn.real) - (w.imag * CombineDFT_4162_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4162_s.wn.imag) + (w.imag * CombineDFT_4162_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[7]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4163(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[8], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[8], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4163_s.wn.real) - (w.imag * CombineDFT_4163_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4163_s.wn.imag) + (w.imag * CombineDFT_4163_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[8]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_join[8], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4164(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[9], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[9], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4164_s.wn.real) - (w.imag * CombineDFT_4164_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4164_s.wn.imag) + (w.imag * CombineDFT_4164_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[9]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_join[9], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4165(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[10], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[10], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4165_s.wn.real) - (w.imag * CombineDFT_4165_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4165_s.wn.imag) + (w.imag * CombineDFT_4165_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[10]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_join[10], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4166(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[11], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[11], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4166_s.wn.real) - (w.imag * CombineDFT_4166_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4166_s.wn.imag) + (w.imag * CombineDFT_4166_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[11]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_join[11], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4167(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[12], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[12], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4167_s.wn.real) - (w.imag * CombineDFT_4167_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4167_s.wn.imag) + (w.imag * CombineDFT_4167_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[12]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_join[12], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4168(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[13], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[13], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4168_s.wn.real) - (w.imag * CombineDFT_4168_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4168_s.wn.imag) + (w.imag * CombineDFT_4168_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[13]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_join[13], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4153() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 14, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4138WEIGHTED_ROUND_ROBIN_Splitter_4153));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4154() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 14, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4154WEIGHTED_ROUND_ROBIN_Splitter_4169, pop_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_4171(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_split[0], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4171_s.wn.real) - (w.imag * CombineDFT_4171_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4171_s.wn.imag) + (w.imag * CombineDFT_4171_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_split[0]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4172(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_split[1], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4172_s.wn.real) - (w.imag * CombineDFT_4172_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4172_s.wn.imag) + (w.imag * CombineDFT_4172_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_split[1]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4173(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_split[2], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4173_s.wn.real) - (w.imag * CombineDFT_4173_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4173_s.wn.imag) + (w.imag * CombineDFT_4173_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_split[2]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4174(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_split[3], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4174_s.wn.real) - (w.imag * CombineDFT_4174_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4174_s.wn.imag) + (w.imag * CombineDFT_4174_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_split[3]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4175(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_split[4], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_split[4], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4175_s.wn.real) - (w.imag * CombineDFT_4175_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4175_s.wn.imag) + (w.imag * CombineDFT_4175_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_split[4]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4176(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_split[5], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_split[5], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4176_s.wn.real) - (w.imag * CombineDFT_4176_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4176_s.wn.imag) + (w.imag * CombineDFT_4176_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_split[5]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4177(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_split[6], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_split[6], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4177_s.wn.real) - (w.imag * CombineDFT_4177_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4177_s.wn.imag) + (w.imag * CombineDFT_4177_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_split[6]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4178(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_split[7], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_split[7], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4178_s.wn.real) - (w.imag * CombineDFT_4178_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4178_s.wn.imag) + (w.imag * CombineDFT_4178_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_split[7]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4169() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4154WEIGHTED_ROUND_ROBIN_Splitter_4169));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4170() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4170WEIGHTED_ROUND_ROBIN_Splitter_4179, pop_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_4181(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_4196_4206_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_4196_4206_split[0], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4181_s.wn.real) - (w.imag * CombineDFT_4181_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4181_s.wn.imag) + (w.imag * CombineDFT_4181_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_4196_4206_split[0]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_4196_4206_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4182(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_4196_4206_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_4196_4206_split[1], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4182_s.wn.real) - (w.imag * CombineDFT_4182_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4182_s.wn.imag) + (w.imag * CombineDFT_4182_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_4196_4206_split[1]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_4196_4206_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4183(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_4196_4206_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_4196_4206_split[2], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4183_s.wn.real) - (w.imag * CombineDFT_4183_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4183_s.wn.imag) + (w.imag * CombineDFT_4183_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_4196_4206_split[2]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_4196_4206_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4184(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_4196_4206_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_4196_4206_split[3], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4184_s.wn.real) - (w.imag * CombineDFT_4184_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4184_s.wn.imag) + (w.imag * CombineDFT_4184_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_4196_4206_split[3]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_4196_4206_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4179() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin14_CombineDFT_Fiss_4196_4206_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4170WEIGHTED_ROUND_ROBIN_Splitter_4179));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4180() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4180WEIGHTED_ROUND_ROBIN_Splitter_4185, pop_complex(&SplitJoin14_CombineDFT_Fiss_4196_4206_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_4187(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[32];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 16, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_4197_4207_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_4197_4207_split[0], (16 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(16 + i)].real = (y0.real - y1w.real) ; 
			results[(16 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4187_s.wn.real) - (w.imag * CombineDFT_4187_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4187_s.wn.imag) + (w.imag * CombineDFT_4187_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin16_CombineDFT_Fiss_4197_4207_split[0]) ; 
			push_complex(&SplitJoin16_CombineDFT_Fiss_4197_4207_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4188(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[32];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 16, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_4197_4207_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_4197_4207_split[1], (16 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(16 + i)].real = (y0.real - y1w.real) ; 
			results[(16 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4188_s.wn.real) - (w.imag * CombineDFT_4188_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4188_s.wn.imag) + (w.imag * CombineDFT_4188_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin16_CombineDFT_Fiss_4197_4207_split[1]) ; 
			push_complex(&SplitJoin16_CombineDFT_Fiss_4197_4207_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4185() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_4197_4207_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4180WEIGHTED_ROUND_ROBIN_Splitter_4185));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_4197_4207_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4180WEIGHTED_ROUND_ROBIN_Splitter_4185));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4186() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4186CombineDFT_4098, pop_complex(&SplitJoin16_CombineDFT_Fiss_4197_4207_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4186CombineDFT_4098, pop_complex(&SplitJoin16_CombineDFT_Fiss_4197_4207_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_4098(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[64];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4186CombineDFT_4098, i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4186CombineDFT_4098, (32 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(32 + i)].real = (y0.real - y1w.real) ; 
			results[(32 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4098_s.wn.real) - (w.imag * CombineDFT_4098_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4098_s.wn.imag) + (w.imag * CombineDFT_4098_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4186CombineDFT_4098) ; 
			push_complex(&CombineDFT_4098CPrinter_4099, results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CPrinter_4099(){
	FOR(uint32_t, __iter_steady_, 0, <, 448, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&CombineDFT_4098CPrinter_4099));
		printf("%.10f", c.real);
		printf("\n");
		printf("%.10f", c.imag);
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 14, __iter_init_0_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 14, __iter_init_2_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 4, __iter_init_3_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_4190_4200_split[__iter_init_3_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4112WEIGHTED_ROUND_ROBIN_Splitter_4121);
	init_buffer_complex(&FFTTestSource_4087FFTReorderSimple_4088);
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 2, __iter_init_5_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_4197_4207_split[__iter_init_5_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4102WEIGHTED_ROUND_ROBIN_Splitter_4105);
	FOR(int, __iter_init_6_, 0, <, 14, __iter_init_6_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 4, __iter_init_7_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_4190_4200_join[__iter_init_7_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4106WEIGHTED_ROUND_ROBIN_Splitter_4111);
	init_buffer_complex(&FFTReorderSimple_4088WEIGHTED_ROUND_ROBIN_Splitter_4101);
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_4195_4205_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 2, __iter_init_9_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_4189_4199_split[__iter_init_9_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4154WEIGHTED_ROUND_ROBIN_Splitter_4169);
	FOR(int, __iter_init_10_, 0, <, 14, __iter_init_10_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_4194_4204_join[__iter_init_10_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4170WEIGHTED_ROUND_ROBIN_Splitter_4179);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4138WEIGHTED_ROUND_ROBIN_Splitter_4153);
	FOR(int, __iter_init_11_, 0, <, 14, __iter_init_11_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_4193_4203_join[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 14, __iter_init_12_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_4192_4202_split[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 4, __iter_init_13_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_4196_4206_join[__iter_init_13_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4180WEIGHTED_ROUND_ROBIN_Splitter_4185);
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_4189_4199_join[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 8, __iter_init_15_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_4191_4201_join[__iter_init_15_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4122WEIGHTED_ROUND_ROBIN_Splitter_4137);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4186CombineDFT_4098);
	FOR(int, __iter_init_16_, 0, <, 4, __iter_init_16_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_4196_4206_split[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_4197_4207_join[__iter_init_17_]);
	ENDFOR
	init_buffer_complex(&CombineDFT_4098CPrinter_4099);
// --- init: CombineDFT_4139
	 {
	 ; 
	CombineDFT_4139_s.wn.real = -1.0 ; 
	CombineDFT_4139_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4140
	 {
	 ; 
	CombineDFT_4140_s.wn.real = -1.0 ; 
	CombineDFT_4140_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4141
	 {
	 ; 
	CombineDFT_4141_s.wn.real = -1.0 ; 
	CombineDFT_4141_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4142
	 {
	 ; 
	CombineDFT_4142_s.wn.real = -1.0 ; 
	CombineDFT_4142_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4143
	 {
	 ; 
	CombineDFT_4143_s.wn.real = -1.0 ; 
	CombineDFT_4143_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4144
	 {
	 ; 
	CombineDFT_4144_s.wn.real = -1.0 ; 
	CombineDFT_4144_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4145
	 {
	 ; 
	CombineDFT_4145_s.wn.real = -1.0 ; 
	CombineDFT_4145_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4146
	 {
	 ; 
	CombineDFT_4146_s.wn.real = -1.0 ; 
	CombineDFT_4146_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4147
	 {
	 ; 
	CombineDFT_4147_s.wn.real = -1.0 ; 
	CombineDFT_4147_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4148
	 {
	 ; 
	CombineDFT_4148_s.wn.real = -1.0 ; 
	CombineDFT_4148_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4149
	 {
	 ; 
	CombineDFT_4149_s.wn.real = -1.0 ; 
	CombineDFT_4149_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4150
	 {
	 ; 
	CombineDFT_4150_s.wn.real = -1.0 ; 
	CombineDFT_4150_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4151
	 {
	 ; 
	CombineDFT_4151_s.wn.real = -1.0 ; 
	CombineDFT_4151_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4152
	 {
	 ; 
	CombineDFT_4152_s.wn.real = -1.0 ; 
	CombineDFT_4152_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4155
	 {
	 ; 
	CombineDFT_4155_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4155_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4156
	 {
	 ; 
	CombineDFT_4156_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4156_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4157
	 {
	 ; 
	CombineDFT_4157_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4157_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4158
	 {
	 ; 
	CombineDFT_4158_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4158_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4159
	 {
	 ; 
	CombineDFT_4159_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4159_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4160
	 {
	 ; 
	CombineDFT_4160_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4160_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4161
	 {
	 ; 
	CombineDFT_4161_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4161_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4162
	 {
	 ; 
	CombineDFT_4162_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4162_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4163
	 {
	 ; 
	CombineDFT_4163_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4163_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4164
	 {
	 ; 
	CombineDFT_4164_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4164_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4165
	 {
	 ; 
	CombineDFT_4165_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4165_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4166
	 {
	 ; 
	CombineDFT_4166_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4166_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4167
	 {
	 ; 
	CombineDFT_4167_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4167_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4168
	 {
	 ; 
	CombineDFT_4168_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4168_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4171
	 {
	 ; 
	CombineDFT_4171_s.wn.real = 0.70710677 ; 
	CombineDFT_4171_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4172
	 {
	 ; 
	CombineDFT_4172_s.wn.real = 0.70710677 ; 
	CombineDFT_4172_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4173
	 {
	 ; 
	CombineDFT_4173_s.wn.real = 0.70710677 ; 
	CombineDFT_4173_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4174
	 {
	 ; 
	CombineDFT_4174_s.wn.real = 0.70710677 ; 
	CombineDFT_4174_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4175
	 {
	 ; 
	CombineDFT_4175_s.wn.real = 0.70710677 ; 
	CombineDFT_4175_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4176
	 {
	 ; 
	CombineDFT_4176_s.wn.real = 0.70710677 ; 
	CombineDFT_4176_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4177
	 {
	 ; 
	CombineDFT_4177_s.wn.real = 0.70710677 ; 
	CombineDFT_4177_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4178
	 {
	 ; 
	CombineDFT_4178_s.wn.real = 0.70710677 ; 
	CombineDFT_4178_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4181
	 {
	 ; 
	CombineDFT_4181_s.wn.real = 0.9238795 ; 
	CombineDFT_4181_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_4182
	 {
	 ; 
	CombineDFT_4182_s.wn.real = 0.9238795 ; 
	CombineDFT_4182_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_4183
	 {
	 ; 
	CombineDFT_4183_s.wn.real = 0.9238795 ; 
	CombineDFT_4183_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_4184
	 {
	 ; 
	CombineDFT_4184_s.wn.real = 0.9238795 ; 
	CombineDFT_4184_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_4187
	 {
	 ; 
	CombineDFT_4187_s.wn.real = 0.98078525 ; 
	CombineDFT_4187_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_4188
	 {
	 ; 
	CombineDFT_4188_s.wn.real = 0.98078525 ; 
	CombineDFT_4188_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_4098
	 {
	 ; 
	CombineDFT_4098_s.wn.real = 0.9951847 ; 
	CombineDFT_4098_s.wn.imag = -0.09801714 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FFTTestSource_4087();
		FFTReorderSimple_4088();
		WEIGHTED_ROUND_ROBIN_Splitter_4101();
			FFTReorderSimple_4103();
			FFTReorderSimple_4104();
		WEIGHTED_ROUND_ROBIN_Joiner_4102();
		WEIGHTED_ROUND_ROBIN_Splitter_4105();
			FFTReorderSimple_4107();
			FFTReorderSimple_4108();
			FFTReorderSimple_4109();
			FFTReorderSimple_4110();
		WEIGHTED_ROUND_ROBIN_Joiner_4106();
		WEIGHTED_ROUND_ROBIN_Splitter_4111();
			FFTReorderSimple_4113();
			FFTReorderSimple_4114();
			FFTReorderSimple_4115();
			FFTReorderSimple_4116();
			FFTReorderSimple_4117();
			FFTReorderSimple_4118();
			FFTReorderSimple_4119();
			FFTReorderSimple_4120();
		WEIGHTED_ROUND_ROBIN_Joiner_4112();
		WEIGHTED_ROUND_ROBIN_Splitter_4121();
			FFTReorderSimple_4123();
			FFTReorderSimple_4124();
			FFTReorderSimple_4125();
			FFTReorderSimple_4126();
			FFTReorderSimple_4127();
			FFTReorderSimple_4128();
			FFTReorderSimple_4129();
			FFTReorderSimple_4130();
			FFTReorderSimple_4131();
			FFTReorderSimple_4132();
			FFTReorderSimple_4133();
			FFTReorderSimple_4134();
			FFTReorderSimple_4135();
			FFTReorderSimple_4136();
		WEIGHTED_ROUND_ROBIN_Joiner_4122();
		WEIGHTED_ROUND_ROBIN_Splitter_4137();
			CombineDFT_4139();
			CombineDFT_4140();
			CombineDFT_4141();
			CombineDFT_4142();
			CombineDFT_4143();
			CombineDFT_4144();
			CombineDFT_4145();
			CombineDFT_4146();
			CombineDFT_4147();
			CombineDFT_4148();
			CombineDFT_4149();
			CombineDFT_4150();
			CombineDFT_4151();
			CombineDFT_4152();
		WEIGHTED_ROUND_ROBIN_Joiner_4138();
		WEIGHTED_ROUND_ROBIN_Splitter_4153();
			CombineDFT_4155();
			CombineDFT_4156();
			CombineDFT_4157();
			CombineDFT_4158();
			CombineDFT_4159();
			CombineDFT_4160();
			CombineDFT_4161();
			CombineDFT_4162();
			CombineDFT_4163();
			CombineDFT_4164();
			CombineDFT_4165();
			CombineDFT_4166();
			CombineDFT_4167();
			CombineDFT_4168();
		WEIGHTED_ROUND_ROBIN_Joiner_4154();
		WEIGHTED_ROUND_ROBIN_Splitter_4169();
			CombineDFT_4171();
			CombineDFT_4172();
			CombineDFT_4173();
			CombineDFT_4174();
			CombineDFT_4175();
			CombineDFT_4176();
			CombineDFT_4177();
			CombineDFT_4178();
		WEIGHTED_ROUND_ROBIN_Joiner_4170();
		WEIGHTED_ROUND_ROBIN_Splitter_4179();
			CombineDFT_4181();
			CombineDFT_4182();
			CombineDFT_4183();
			CombineDFT_4184();
		WEIGHTED_ROUND_ROBIN_Joiner_4180();
		WEIGHTED_ROUND_ROBIN_Splitter_4185();
			CombineDFT_4187();
			CombineDFT_4188();
		WEIGHTED_ROUND_ROBIN_Joiner_4186();
		CombineDFT_4098();
		CPrinter_4099();
	ENDFOR
	return EXIT_SUCCESS;
}
