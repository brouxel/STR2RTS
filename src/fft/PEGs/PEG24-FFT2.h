#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=1536 on the compile command line
#else
#if BUF_SIZEMAX < 1536
#error BUF_SIZEMAX too small, it must be at least 1536
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float w[2];
} CombineDFT_4127_t;

typedef struct {
	float w[4];
} CombineDFT_4153_t;

typedef struct {
	float w[8];
} CombineDFT_4171_t;

typedef struct {
	float w[16];
} CombineDFT_4181_t;

typedef struct {
	float w[32];
} CombineDFT_4187_t;

typedef struct {
	float w[64];
} CombineDFT_4062_t;
void WEIGHTED_ROUND_ROBIN_Splitter_4083();
void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout);
void FFTTestSource_4085();
void FFTTestSource_4086();
void WEIGHTED_ROUND_ROBIN_Joiner_4084();
void WEIGHTED_ROUND_ROBIN_Splitter_4075();
void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout);
void FFTReorderSimple_4052();
void WEIGHTED_ROUND_ROBIN_Splitter_4087();
void FFTReorderSimple_4089();
void FFTReorderSimple_4090();
void WEIGHTED_ROUND_ROBIN_Joiner_4088();
void WEIGHTED_ROUND_ROBIN_Splitter_4091();
void FFTReorderSimple_4093();
void FFTReorderSimple_4094();
void FFTReorderSimple_4095();
void FFTReorderSimple_4096();
void WEIGHTED_ROUND_ROBIN_Joiner_4092();
void WEIGHTED_ROUND_ROBIN_Splitter_4097();
void FFTReorderSimple_4099();
void FFTReorderSimple_4100();
void FFTReorderSimple_4101();
void FFTReorderSimple_4102();
void FFTReorderSimple_4103();
void FFTReorderSimple_4104();
void FFTReorderSimple_4105();
void FFTReorderSimple_4106();
void WEIGHTED_ROUND_ROBIN_Joiner_4098();
void WEIGHTED_ROUND_ROBIN_Splitter_4107();
void FFTReorderSimple_4109();
void FFTReorderSimple_4110();
void FFTReorderSimple_4111();
void FFTReorderSimple_4112();
void FFTReorderSimple_4113();
void FFTReorderSimple_4114();
void FFTReorderSimple_4115();
void FFTReorderSimple_4116();
void FFTReorderSimple_4117();
void FFTReorderSimple_4118();
void FFTReorderSimple_4119();
void FFTReorderSimple_4120();
void FFTReorderSimple_4121();
void FFTReorderSimple_4122();
void FFTReorderSimple_4123();
void FFTReorderSimple_4124();
void WEIGHTED_ROUND_ROBIN_Joiner_4108();
void WEIGHTED_ROUND_ROBIN_Splitter_4125();
void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout);
void CombineDFT_4127();
void CombineDFT_4128();
void CombineDFT_4129();
void CombineDFT_4130();
void CombineDFT_4131();
void CombineDFT_4132();
void CombineDFT_4133();
void CombineDFT_4134();
void CombineDFT_4135();
void CombineDFT_4136();
void CombineDFT_4137();
void CombineDFT_4138();
void CombineDFT_4139();
void CombineDFT_4140();
void CombineDFT_4141();
void CombineDFT_4142();
void CombineDFT_4143();
void CombineDFT_4144();
void CombineDFT_4145();
void CombineDFT_4146();
void CombineDFT_4147();
void CombineDFT_4148();
void CombineDFT_4149();
void CombineDFT_4150();
void WEIGHTED_ROUND_ROBIN_Joiner_4126();
void WEIGHTED_ROUND_ROBIN_Splitter_4151();
void CombineDFT_4153();
void CombineDFT_4154();
void CombineDFT_4155();
void CombineDFT_4156();
void CombineDFT_4157();
void CombineDFT_4158();
void CombineDFT_4159();
void CombineDFT_4160();
void CombineDFT_4161();
void CombineDFT_4162();
void CombineDFT_4163();
void CombineDFT_4164();
void CombineDFT_4165();
void CombineDFT_4166();
void CombineDFT_4167();
void CombineDFT_4168();
void WEIGHTED_ROUND_ROBIN_Joiner_4152();
void WEIGHTED_ROUND_ROBIN_Splitter_4169();
void CombineDFT_4171();
void CombineDFT_4172();
void CombineDFT_4173();
void CombineDFT_4174();
void CombineDFT_4175();
void CombineDFT_4176();
void CombineDFT_4177();
void CombineDFT_4178();
void WEIGHTED_ROUND_ROBIN_Joiner_4170();
void WEIGHTED_ROUND_ROBIN_Splitter_4179();
void CombineDFT_4181();
void CombineDFT_4182();
void CombineDFT_4183();
void CombineDFT_4184();
void WEIGHTED_ROUND_ROBIN_Joiner_4180();
void WEIGHTED_ROUND_ROBIN_Splitter_4185();
void CombineDFT_4187();
void CombineDFT_4188();
void WEIGHTED_ROUND_ROBIN_Joiner_4186();
void CombineDFT_4062();
void FFTReorderSimple_4063();
void WEIGHTED_ROUND_ROBIN_Splitter_4189();
void FFTReorderSimple_4191();
void FFTReorderSimple_4192();
void WEIGHTED_ROUND_ROBIN_Joiner_4190();
void WEIGHTED_ROUND_ROBIN_Splitter_4193();
void FFTReorderSimple_4195();
void FFTReorderSimple_4196();
void FFTReorderSimple_4197();
void FFTReorderSimple_4198();
void WEIGHTED_ROUND_ROBIN_Joiner_4194();
void WEIGHTED_ROUND_ROBIN_Splitter_4199();
void FFTReorderSimple_4201();
void FFTReorderSimple_4202();
void FFTReorderSimple_4203();
void FFTReorderSimple_4204();
void FFTReorderSimple_4205();
void FFTReorderSimple_4206();
void FFTReorderSimple_4207();
void FFTReorderSimple_4208();
void WEIGHTED_ROUND_ROBIN_Joiner_4200();
void WEIGHTED_ROUND_ROBIN_Splitter_4209();
void FFTReorderSimple_4211();
void FFTReorderSimple_4212();
void FFTReorderSimple_4213();
void FFTReorderSimple_4214();
void FFTReorderSimple_4215();
void FFTReorderSimple_4216();
void FFTReorderSimple_4217();
void FFTReorderSimple_4218();
void FFTReorderSimple_4219();
void FFTReorderSimple_4220();
void FFTReorderSimple_4221();
void FFTReorderSimple_4222();
void FFTReorderSimple_4223();
void FFTReorderSimple_4224();
void FFTReorderSimple_4225();
void FFTReorderSimple_4226();
void WEIGHTED_ROUND_ROBIN_Joiner_4210();
void WEIGHTED_ROUND_ROBIN_Splitter_4227();
void CombineDFT_4229();
void CombineDFT_4230();
void CombineDFT_4231();
void CombineDFT_4232();
void CombineDFT_4233();
void CombineDFT_4234();
void CombineDFT_4235();
void CombineDFT_4236();
void CombineDFT_4237();
void CombineDFT_4238();
void CombineDFT_4239();
void CombineDFT_4240();
void CombineDFT_4241();
void CombineDFT_4242();
void CombineDFT_4243();
void CombineDFT_4244();
void CombineDFT_4245();
void CombineDFT_4246();
void CombineDFT_4247();
void CombineDFT_4248();
void CombineDFT_4249();
void CombineDFT_4250();
void CombineDFT_4251();
void CombineDFT_4252();
void WEIGHTED_ROUND_ROBIN_Joiner_4228();
void WEIGHTED_ROUND_ROBIN_Splitter_4253();
void CombineDFT_4255();
void CombineDFT_4256();
void CombineDFT_4257();
void CombineDFT_4258();
void CombineDFT_4259();
void CombineDFT_4260();
void CombineDFT_4261();
void CombineDFT_4262();
void CombineDFT_4263();
void CombineDFT_4264();
void CombineDFT_4265();
void CombineDFT_4266();
void CombineDFT_4267();
void CombineDFT_4268();
void CombineDFT_4269();
void CombineDFT_4270();
void WEIGHTED_ROUND_ROBIN_Joiner_4254();
void WEIGHTED_ROUND_ROBIN_Splitter_4271();
void CombineDFT_4273();
void CombineDFT_4274();
void CombineDFT_4275();
void CombineDFT_4276();
void CombineDFT_4277();
void CombineDFT_4278();
void CombineDFT_4279();
void CombineDFT_4280();
void WEIGHTED_ROUND_ROBIN_Joiner_4272();
void WEIGHTED_ROUND_ROBIN_Splitter_4281();
void CombineDFT_4283();
void CombineDFT_4284();
void CombineDFT_4285();
void CombineDFT_4286();
void WEIGHTED_ROUND_ROBIN_Joiner_4282();
void WEIGHTED_ROUND_ROBIN_Splitter_4287();
void CombineDFT_4289();
void CombineDFT_4290();
void WEIGHTED_ROUND_ROBIN_Joiner_4288();
void CombineDFT_4073();
void WEIGHTED_ROUND_ROBIN_Joiner_4076();
void FloatPrinter(buffer_float_t *chanin);
void FloatPrinter_4074();

#ifdef __cplusplus
}
#endif
#endif
