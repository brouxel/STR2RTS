#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=3712 on the compile command line
#else
#if BUF_SIZEMAX < 3712
#error BUF_SIZEMAX too small, it must be at least 3712
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	complex_t wn;
} CombineDFT_845_t;
void FFTTestSource(buffer_complex_t *chanout);
void FFTTestSource_791();
void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout);
void FFTReorderSimple_792();
void WEIGHTED_ROUND_ROBIN_Splitter_805();
void FFTReorderSimple_807();
void FFTReorderSimple_808();
void WEIGHTED_ROUND_ROBIN_Joiner_806();
void WEIGHTED_ROUND_ROBIN_Splitter_809();
void FFTReorderSimple_811();
void FFTReorderSimple_812();
void FFTReorderSimple_813();
void FFTReorderSimple_814();
void WEIGHTED_ROUND_ROBIN_Joiner_810();
void WEIGHTED_ROUND_ROBIN_Splitter_815();
void FFTReorderSimple_817();
void FFTReorderSimple_818();
void FFTReorderSimple_819();
void FFTReorderSimple_820();
void FFTReorderSimple_821();
void FFTReorderSimple_822();
void FFTReorderSimple_823();
void FFTReorderSimple_824();
void WEIGHTED_ROUND_ROBIN_Joiner_816();
void WEIGHTED_ROUND_ROBIN_Splitter_825();
void FFTReorderSimple_827();
void FFTReorderSimple_828();
void FFTReorderSimple_829();
void FFTReorderSimple_830();
void FFTReorderSimple_831();
void FFTReorderSimple_832();
void FFTReorderSimple_833();
void FFTReorderSimple_834();
void FFTReorderSimple_835();
void FFTReorderSimple_836();
void FFTReorderSimple_837();
void FFTReorderSimple_838();
void FFTReorderSimple_839();
void FFTReorderSimple_840();
void FFTReorderSimple_841();
void FFTReorderSimple_842();
void WEIGHTED_ROUND_ROBIN_Joiner_826();
void WEIGHTED_ROUND_ROBIN_Splitter_843();
void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout);
void CombineDFT_845();
void CombineDFT_846();
void CombineDFT_847();
void CombineDFT_848();
void CombineDFT_849();
void CombineDFT_850();
void CombineDFT_851();
void CombineDFT_852();
void CombineDFT_853();
void CombineDFT_854();
void CombineDFT_855();
void CombineDFT_856();
void CombineDFT_857();
void CombineDFT_858();
void CombineDFT_859();
void CombineDFT_860();
void CombineDFT_861();
void CombineDFT_862();
void CombineDFT_863();
void CombineDFT_864();
void CombineDFT_865();
void CombineDFT_866();
void CombineDFT_867();
void CombineDFT_868();
void CombineDFT_869();
void CombineDFT_870();
void CombineDFT_871();
void CombineDFT_872();
void CombineDFT_873();
void WEIGHTED_ROUND_ROBIN_Joiner_844();
void WEIGHTED_ROUND_ROBIN_Splitter_874();
void CombineDFT_876();
void CombineDFT_877();
void CombineDFT_878();
void CombineDFT_879();
void CombineDFT_880();
void CombineDFT_881();
void CombineDFT_882();
void CombineDFT_883();
void CombineDFT_884();
void CombineDFT_885();
void CombineDFT_886();
void CombineDFT_887();
void CombineDFT_888();
void CombineDFT_889();
void CombineDFT_890();
void CombineDFT_891();
void WEIGHTED_ROUND_ROBIN_Joiner_875();
void WEIGHTED_ROUND_ROBIN_Splitter_892();
void CombineDFT_894();
void CombineDFT_895();
void CombineDFT_896();
void CombineDFT_897();
void CombineDFT_898();
void CombineDFT_899();
void CombineDFT_900();
void CombineDFT_901();
void WEIGHTED_ROUND_ROBIN_Joiner_893();
void WEIGHTED_ROUND_ROBIN_Splitter_902();
void CombineDFT_904();
void CombineDFT_905();
void CombineDFT_906();
void CombineDFT_907();
void WEIGHTED_ROUND_ROBIN_Joiner_903();
void WEIGHTED_ROUND_ROBIN_Splitter_908();
void CombineDFT_910();
void CombineDFT_911();
void WEIGHTED_ROUND_ROBIN_Joiner_909();
void CombineDFT_802();
void CPrinter(buffer_complex_t *chanin);
void CPrinter_803();

#ifdef __cplusplus
}
#endif
#endif
