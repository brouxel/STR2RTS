#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=128 on the compile command line
#else
#if BUF_SIZEMAX < 128
#error BUF_SIZEMAX too small, it must be at least 128
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	complex_t wn;
} CombineDFT_3731_t;
void FFTTestSource_3677();
void FFTReorderSimple_3678();
void WEIGHTED_ROUND_ROBIN_Splitter_3691();
void FFTReorderSimple_3693();
void FFTReorderSimple_3694();
void WEIGHTED_ROUND_ROBIN_Joiner_3692();
void WEIGHTED_ROUND_ROBIN_Splitter_3695();
void FFTReorderSimple_3697();
void FFTReorderSimple_3698();
void FFTReorderSimple_3699();
void FFTReorderSimple_3700();
void WEIGHTED_ROUND_ROBIN_Joiner_3696();
void WEIGHTED_ROUND_ROBIN_Splitter_3701();
void FFTReorderSimple_3703();
void FFTReorderSimple_3704();
void FFTReorderSimple_3705();
void FFTReorderSimple_3706();
void FFTReorderSimple_3707();
void FFTReorderSimple_3708();
void FFTReorderSimple_3709();
void FFTReorderSimple_3710();
void WEIGHTED_ROUND_ROBIN_Joiner_3702();
void WEIGHTED_ROUND_ROBIN_Splitter_3711();
void FFTReorderSimple_3713();
void FFTReorderSimple_3714();
void FFTReorderSimple_3715();
void FFTReorderSimple_3716();
void FFTReorderSimple_3717();
void FFTReorderSimple_3718();
void FFTReorderSimple_3719();
void FFTReorderSimple_3720();
void FFTReorderSimple_3721();
void FFTReorderSimple_3722();
void FFTReorderSimple_3723();
void FFTReorderSimple_3724();
void FFTReorderSimple_3725();
void FFTReorderSimple_3726();
void FFTReorderSimple_3727();
void FFTReorderSimple_3728();
void WEIGHTED_ROUND_ROBIN_Joiner_3712();
void WEIGHTED_ROUND_ROBIN_Splitter_3729();
void CombineDFT_3731();
void CombineDFT_3732();
void CombineDFT_3733();
void CombineDFT_3734();
void CombineDFT_3735();
void CombineDFT_3736();
void CombineDFT_3737();
void CombineDFT_3738();
void CombineDFT_3739();
void CombineDFT_3740();
void CombineDFT_3741();
void CombineDFT_3742();
void CombineDFT_3743();
void CombineDFT_3744();
void CombineDFT_3745();
void CombineDFT_3746();
void WEIGHTED_ROUND_ROBIN_Joiner_3730();
void WEIGHTED_ROUND_ROBIN_Splitter_3747();
void CombineDFT_3749();
void CombineDFT_3750();
void CombineDFT_3751();
void CombineDFT_3752();
void CombineDFT_3753();
void CombineDFT_3754();
void CombineDFT_3755();
void CombineDFT_3756();
void CombineDFT_3757();
void CombineDFT_3758();
void CombineDFT_3759();
void CombineDFT_3760();
void CombineDFT_3761();
void CombineDFT_3762();
void CombineDFT_3763();
void CombineDFT_3764();
void WEIGHTED_ROUND_ROBIN_Joiner_3748();
void WEIGHTED_ROUND_ROBIN_Splitter_3765();
void CombineDFT_3767();
void CombineDFT_3768();
void CombineDFT_3769();
void CombineDFT_3770();
void CombineDFT_3771();
void CombineDFT_3772();
void CombineDFT_3773();
void CombineDFT_3774();
void WEIGHTED_ROUND_ROBIN_Joiner_3766();
void WEIGHTED_ROUND_ROBIN_Splitter_3775();
void CombineDFT_3777();
void CombineDFT_3778();
void CombineDFT_3779();
void CombineDFT_3780();
void WEIGHTED_ROUND_ROBIN_Joiner_3776();
void WEIGHTED_ROUND_ROBIN_Splitter_3781();
void CombineDFT_3783();
void CombineDFT_3784();
void WEIGHTED_ROUND_ROBIN_Joiner_3782();
void CombineDFT_3688();
void CPrinter_3689();

#ifdef __cplusplus
}
#endif
#endif
