#include "PEG17-FFT6.h"

buffer_complex_t SplitJoin10_CombineDFT_Fiss_3581_3591_join[16];
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_3579_3589_split[16];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3539WEIGHTED_ROUND_ROBIN_Splitter_3556;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3520WEIGHTED_ROUND_ROBIN_Splitter_3538;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3486WEIGHTED_ROUND_ROBIN_Splitter_3491;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3482WEIGHTED_ROUND_ROBIN_Splitter_3485;
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_3578_3588_split[8];
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_3579_3589_join[16];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3492WEIGHTED_ROUND_ROBIN_Splitter_3501;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3502WEIGHTED_ROUND_ROBIN_Splitter_3519;
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_3577_3587_join[4];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_3583_3593_split[4];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_3583_3593_join[4];
buffer_complex_t FFTTestSource_3467FFTReorderSimple_3468;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_3580_3590_split[17];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_3584_3594_split[2];
buffer_complex_t SplitJoin12_CombineDFT_Fiss_3582_3592_join[8];
buffer_complex_t FFTReorderSimple_3468WEIGHTED_ROUND_ROBIN_Splitter_3481;
buffer_complex_t SplitJoin12_CombineDFT_Fiss_3582_3592_split[8];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_3576_3586_join[2];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_3578_3588_join[8];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3557WEIGHTED_ROUND_ROBIN_Splitter_3566;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3573CombineDFT_3478;
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_3577_3587_split[4];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_3584_3594_join[2];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_3581_3591_split[16];
buffer_complex_t CombineDFT_3478CPrinter_3479;
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_3576_3586_split[2];
buffer_complex_t SplitJoin8_CombineDFT_Fiss_3580_3590_join[17];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3567WEIGHTED_ROUND_ROBIN_Splitter_3572;


CombineDFT_3521_t CombineDFT_3521_s;
CombineDFT_3521_t CombineDFT_3522_s;
CombineDFT_3521_t CombineDFT_3523_s;
CombineDFT_3521_t CombineDFT_3524_s;
CombineDFT_3521_t CombineDFT_3525_s;
CombineDFT_3521_t CombineDFT_3526_s;
CombineDFT_3521_t CombineDFT_3527_s;
CombineDFT_3521_t CombineDFT_3528_s;
CombineDFT_3521_t CombineDFT_3529_s;
CombineDFT_3521_t CombineDFT_3530_s;
CombineDFT_3521_t CombineDFT_3531_s;
CombineDFT_3521_t CombineDFT_3532_s;
CombineDFT_3521_t CombineDFT_3533_s;
CombineDFT_3521_t CombineDFT_3534_s;
CombineDFT_3521_t CombineDFT_3535_s;
CombineDFT_3521_t CombineDFT_3536_s;
CombineDFT_3521_t CombineDFT_3537_s;
CombineDFT_3521_t CombineDFT_3540_s;
CombineDFT_3521_t CombineDFT_3541_s;
CombineDFT_3521_t CombineDFT_3542_s;
CombineDFT_3521_t CombineDFT_3543_s;
CombineDFT_3521_t CombineDFT_3544_s;
CombineDFT_3521_t CombineDFT_3545_s;
CombineDFT_3521_t CombineDFT_3546_s;
CombineDFT_3521_t CombineDFT_3547_s;
CombineDFT_3521_t CombineDFT_3548_s;
CombineDFT_3521_t CombineDFT_3549_s;
CombineDFT_3521_t CombineDFT_3550_s;
CombineDFT_3521_t CombineDFT_3551_s;
CombineDFT_3521_t CombineDFT_3552_s;
CombineDFT_3521_t CombineDFT_3553_s;
CombineDFT_3521_t CombineDFT_3554_s;
CombineDFT_3521_t CombineDFT_3555_s;
CombineDFT_3521_t CombineDFT_3558_s;
CombineDFT_3521_t CombineDFT_3559_s;
CombineDFT_3521_t CombineDFT_3560_s;
CombineDFT_3521_t CombineDFT_3561_s;
CombineDFT_3521_t CombineDFT_3562_s;
CombineDFT_3521_t CombineDFT_3563_s;
CombineDFT_3521_t CombineDFT_3564_s;
CombineDFT_3521_t CombineDFT_3565_s;
CombineDFT_3521_t CombineDFT_3568_s;
CombineDFT_3521_t CombineDFT_3569_s;
CombineDFT_3521_t CombineDFT_3570_s;
CombineDFT_3521_t CombineDFT_3571_s;
CombineDFT_3521_t CombineDFT_3574_s;
CombineDFT_3521_t CombineDFT_3575_s;
CombineDFT_3521_t CombineDFT_3478_s;

void FFTTestSource(buffer_complex_t *chanout) {
		complex_t c1;
		complex_t zero;
		c1.real = 1.0 ; 
		c1.imag = 0.0 ; 
		zero.real = 0.0 ; 
		zero.imag = 0.0 ; 
		push_complex(&(*chanout), zero) ; 
		push_complex(&(*chanout), c1) ; 
		FOR(int, i, 0,  < , 62, i++) {
			push_complex(&(*chanout), zero) ; 
		}
		ENDFOR
	}


void FFTTestSource_3467() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTTestSource(&(FFTTestSource_3467FFTReorderSimple_3468));
	ENDFOR
}

void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 64, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&(*chanin)) ; 
		}
		ENDFOR
	}


void FFTReorderSimple_3468() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(FFTTestSource_3467FFTReorderSimple_3468), &(FFTReorderSimple_3468WEIGHTED_ROUND_ROBIN_Splitter_3481));
	ENDFOR
}

void FFTReorderSimple_3483() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_3576_3586_split[0]), &(SplitJoin0_FFTReorderSimple_Fiss_3576_3586_join[0]));
	ENDFOR
}

void FFTReorderSimple_3484() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_3576_3586_split[1]), &(SplitJoin0_FFTReorderSimple_Fiss_3576_3586_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3481() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_3576_3586_split[0], pop_complex(&FFTReorderSimple_3468WEIGHTED_ROUND_ROBIN_Splitter_3481));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_3576_3586_split[1], pop_complex(&FFTReorderSimple_3468WEIGHTED_ROUND_ROBIN_Splitter_3481));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3482() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3482WEIGHTED_ROUND_ROBIN_Splitter_3485, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_3576_3586_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3482WEIGHTED_ROUND_ROBIN_Splitter_3485, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_3576_3586_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_3487() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_3577_3587_split[0]), &(SplitJoin2_FFTReorderSimple_Fiss_3577_3587_join[0]));
	ENDFOR
}

void FFTReorderSimple_3488() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_3577_3587_split[1]), &(SplitJoin2_FFTReorderSimple_Fiss_3577_3587_join[1]));
	ENDFOR
}

void FFTReorderSimple_3489() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_3577_3587_split[2]), &(SplitJoin2_FFTReorderSimple_Fiss_3577_3587_join[2]));
	ENDFOR
}

void FFTReorderSimple_3490() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_3577_3587_split[3]), &(SplitJoin2_FFTReorderSimple_Fiss_3577_3587_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3485() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin2_FFTReorderSimple_Fiss_3577_3587_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3482WEIGHTED_ROUND_ROBIN_Splitter_3485));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3486() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3486WEIGHTED_ROUND_ROBIN_Splitter_3491, pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_3577_3587_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_3493() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_3578_3588_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_3578_3588_join[0]));
	ENDFOR
}

void FFTReorderSimple_3494() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_3578_3588_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_3578_3588_join[1]));
	ENDFOR
}

void FFTReorderSimple_3495() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_3578_3588_split[2]), &(SplitJoin4_FFTReorderSimple_Fiss_3578_3588_join[2]));
	ENDFOR
}

void FFTReorderSimple_3496() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_3578_3588_split[3]), &(SplitJoin4_FFTReorderSimple_Fiss_3578_3588_join[3]));
	ENDFOR
}

void FFTReorderSimple_3497() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_3578_3588_split[4]), &(SplitJoin4_FFTReorderSimple_Fiss_3578_3588_join[4]));
	ENDFOR
}

void FFTReorderSimple_3498() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_3578_3588_split[5]), &(SplitJoin4_FFTReorderSimple_Fiss_3578_3588_join[5]));
	ENDFOR
}

void FFTReorderSimple_3499() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_3578_3588_split[6]), &(SplitJoin4_FFTReorderSimple_Fiss_3578_3588_join[6]));
	ENDFOR
}

void FFTReorderSimple_3500() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_3578_3588_split[7]), &(SplitJoin4_FFTReorderSimple_Fiss_3578_3588_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3491() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3578_3588_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3486WEIGHTED_ROUND_ROBIN_Splitter_3491));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3492() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3492WEIGHTED_ROUND_ROBIN_Splitter_3501, pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_3578_3588_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_3503() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_join[0]));
	ENDFOR
}

void FFTReorderSimple_3504() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_join[1]));
	ENDFOR
}

void FFTReorderSimple_3505() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_join[2]));
	ENDFOR
}

void FFTReorderSimple_3506() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_join[3]));
	ENDFOR
}

void FFTReorderSimple_3507() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_split[4]), &(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_join[4]));
	ENDFOR
}

void FFTReorderSimple_3508() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_split[5]), &(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_join[5]));
	ENDFOR
}

void FFTReorderSimple_3509() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_split[6]), &(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_join[6]));
	ENDFOR
}

void FFTReorderSimple_3510() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_split[7]), &(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_join[7]));
	ENDFOR
}

void FFTReorderSimple_3511() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_split[8]), &(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_join[8]));
	ENDFOR
}

void FFTReorderSimple_3512() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_split[9]), &(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_join[9]));
	ENDFOR
}

void FFTReorderSimple_3513() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_split[10]), &(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_join[10]));
	ENDFOR
}

void FFTReorderSimple_3514() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_split[11]), &(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_join[11]));
	ENDFOR
}

void FFTReorderSimple_3515() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_split[12]), &(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_join[12]));
	ENDFOR
}

void FFTReorderSimple_3516() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_split[13]), &(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_join[13]));
	ENDFOR
}

void FFTReorderSimple_3517() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_split[14]), &(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_join[14]));
	ENDFOR
}

void FFTReorderSimple_3518() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_split[15]), &(SplitJoin6_FFTReorderSimple_Fiss_3579_3589_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3501() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3579_3589_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3492WEIGHTED_ROUND_ROBIN_Splitter_3501));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3502() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3502WEIGHTED_ROUND_ROBIN_Splitter_3519, pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3579_3589_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&(*chanin), (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3521_s.wn.real) - (w.imag * CombineDFT_3521_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3521_s.wn.imag) + (w.imag * CombineDFT_3521_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&(*chanin)) ; 
			push_complex(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_3521() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3580_3590_split[0]), &(SplitJoin8_CombineDFT_Fiss_3580_3590_join[0]));
	ENDFOR
}

void CombineDFT_3522() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3580_3590_split[1]), &(SplitJoin8_CombineDFT_Fiss_3580_3590_join[1]));
	ENDFOR
}

void CombineDFT_3523() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3580_3590_split[2]), &(SplitJoin8_CombineDFT_Fiss_3580_3590_join[2]));
	ENDFOR
}

void CombineDFT_3524() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3580_3590_split[3]), &(SplitJoin8_CombineDFT_Fiss_3580_3590_join[3]));
	ENDFOR
}

void CombineDFT_3525() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3580_3590_split[4]), &(SplitJoin8_CombineDFT_Fiss_3580_3590_join[4]));
	ENDFOR
}

void CombineDFT_3526() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3580_3590_split[5]), &(SplitJoin8_CombineDFT_Fiss_3580_3590_join[5]));
	ENDFOR
}

void CombineDFT_3527() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3580_3590_split[6]), &(SplitJoin8_CombineDFT_Fiss_3580_3590_join[6]));
	ENDFOR
}

void CombineDFT_3528() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3580_3590_split[7]), &(SplitJoin8_CombineDFT_Fiss_3580_3590_join[7]));
	ENDFOR
}

void CombineDFT_3529() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3580_3590_split[8]), &(SplitJoin8_CombineDFT_Fiss_3580_3590_join[8]));
	ENDFOR
}

void CombineDFT_3530() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3580_3590_split[9]), &(SplitJoin8_CombineDFT_Fiss_3580_3590_join[9]));
	ENDFOR
}

void CombineDFT_3531() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3580_3590_split[10]), &(SplitJoin8_CombineDFT_Fiss_3580_3590_join[10]));
	ENDFOR
}

void CombineDFT_3532() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3580_3590_split[11]), &(SplitJoin8_CombineDFT_Fiss_3580_3590_join[11]));
	ENDFOR
}

void CombineDFT_3533() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3580_3590_split[12]), &(SplitJoin8_CombineDFT_Fiss_3580_3590_join[12]));
	ENDFOR
}

void CombineDFT_3534() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3580_3590_split[13]), &(SplitJoin8_CombineDFT_Fiss_3580_3590_join[13]));
	ENDFOR
}

void CombineDFT_3535() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3580_3590_split[14]), &(SplitJoin8_CombineDFT_Fiss_3580_3590_join[14]));
	ENDFOR
}

void CombineDFT_3536() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3580_3590_split[15]), &(SplitJoin8_CombineDFT_Fiss_3580_3590_join[15]));
	ENDFOR
}

void CombineDFT_3537() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3580_3590_split[16]), &(SplitJoin8_CombineDFT_Fiss_3580_3590_join[16]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3519() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 17, __iter_++)
			push_complex(&SplitJoin8_CombineDFT_Fiss_3580_3590_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3502WEIGHTED_ROUND_ROBIN_Splitter_3519));
			push_complex(&SplitJoin8_CombineDFT_Fiss_3580_3590_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3502WEIGHTED_ROUND_ROBIN_Splitter_3519));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3520() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 17, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3520WEIGHTED_ROUND_ROBIN_Splitter_3538, pop_complex(&SplitJoin8_CombineDFT_Fiss_3580_3590_join[__iter_]));
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3520WEIGHTED_ROUND_ROBIN_Splitter_3538, pop_complex(&SplitJoin8_CombineDFT_Fiss_3580_3590_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_3540() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3581_3591_split[0]), &(SplitJoin10_CombineDFT_Fiss_3581_3591_join[0]));
	ENDFOR
}

void CombineDFT_3541() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3581_3591_split[1]), &(SplitJoin10_CombineDFT_Fiss_3581_3591_join[1]));
	ENDFOR
}

void CombineDFT_3542() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3581_3591_split[2]), &(SplitJoin10_CombineDFT_Fiss_3581_3591_join[2]));
	ENDFOR
}

void CombineDFT_3543() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3581_3591_split[3]), &(SplitJoin10_CombineDFT_Fiss_3581_3591_join[3]));
	ENDFOR
}

void CombineDFT_3544() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3581_3591_split[4]), &(SplitJoin10_CombineDFT_Fiss_3581_3591_join[4]));
	ENDFOR
}

void CombineDFT_3545() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3581_3591_split[5]), &(SplitJoin10_CombineDFT_Fiss_3581_3591_join[5]));
	ENDFOR
}

void CombineDFT_3546() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3581_3591_split[6]), &(SplitJoin10_CombineDFT_Fiss_3581_3591_join[6]));
	ENDFOR
}

void CombineDFT_3547() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3581_3591_split[7]), &(SplitJoin10_CombineDFT_Fiss_3581_3591_join[7]));
	ENDFOR
}

void CombineDFT_3548() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3581_3591_split[8]), &(SplitJoin10_CombineDFT_Fiss_3581_3591_join[8]));
	ENDFOR
}

void CombineDFT_3549() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3581_3591_split[9]), &(SplitJoin10_CombineDFT_Fiss_3581_3591_join[9]));
	ENDFOR
}

void CombineDFT_3550() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3581_3591_split[10]), &(SplitJoin10_CombineDFT_Fiss_3581_3591_join[10]));
	ENDFOR
}

void CombineDFT_3551() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3581_3591_split[11]), &(SplitJoin10_CombineDFT_Fiss_3581_3591_join[11]));
	ENDFOR
}

void CombineDFT_3552() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3581_3591_split[12]), &(SplitJoin10_CombineDFT_Fiss_3581_3591_join[12]));
	ENDFOR
}

void CombineDFT_3553() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3581_3591_split[13]), &(SplitJoin10_CombineDFT_Fiss_3581_3591_join[13]));
	ENDFOR
}

void CombineDFT_3554() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3581_3591_split[14]), &(SplitJoin10_CombineDFT_Fiss_3581_3591_join[14]));
	ENDFOR
}

void CombineDFT_3555() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3581_3591_split[15]), &(SplitJoin10_CombineDFT_Fiss_3581_3591_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3538() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin10_CombineDFT_Fiss_3581_3591_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3520WEIGHTED_ROUND_ROBIN_Splitter_3538));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3539() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3539WEIGHTED_ROUND_ROBIN_Splitter_3556, pop_complex(&SplitJoin10_CombineDFT_Fiss_3581_3591_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_3558() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3582_3592_split[0]), &(SplitJoin12_CombineDFT_Fiss_3582_3592_join[0]));
	ENDFOR
}

void CombineDFT_3559() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3582_3592_split[1]), &(SplitJoin12_CombineDFT_Fiss_3582_3592_join[1]));
	ENDFOR
}

void CombineDFT_3560() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3582_3592_split[2]), &(SplitJoin12_CombineDFT_Fiss_3582_3592_join[2]));
	ENDFOR
}

void CombineDFT_3561() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3582_3592_split[3]), &(SplitJoin12_CombineDFT_Fiss_3582_3592_join[3]));
	ENDFOR
}

void CombineDFT_3562() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3582_3592_split[4]), &(SplitJoin12_CombineDFT_Fiss_3582_3592_join[4]));
	ENDFOR
}

void CombineDFT_3563() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3582_3592_split[5]), &(SplitJoin12_CombineDFT_Fiss_3582_3592_join[5]));
	ENDFOR
}

void CombineDFT_3564() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3582_3592_split[6]), &(SplitJoin12_CombineDFT_Fiss_3582_3592_join[6]));
	ENDFOR
}

void CombineDFT_3565() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3582_3592_split[7]), &(SplitJoin12_CombineDFT_Fiss_3582_3592_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3556() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin12_CombineDFT_Fiss_3582_3592_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3539WEIGHTED_ROUND_ROBIN_Splitter_3556));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3557() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3557WEIGHTED_ROUND_ROBIN_Splitter_3566, pop_complex(&SplitJoin12_CombineDFT_Fiss_3582_3592_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_3568() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_3583_3593_split[0]), &(SplitJoin14_CombineDFT_Fiss_3583_3593_join[0]));
	ENDFOR
}

void CombineDFT_3569() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_3583_3593_split[1]), &(SplitJoin14_CombineDFT_Fiss_3583_3593_join[1]));
	ENDFOR
}

void CombineDFT_3570() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_3583_3593_split[2]), &(SplitJoin14_CombineDFT_Fiss_3583_3593_join[2]));
	ENDFOR
}

void CombineDFT_3571() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_3583_3593_split[3]), &(SplitJoin14_CombineDFT_Fiss_3583_3593_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3566() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin14_CombineDFT_Fiss_3583_3593_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3557WEIGHTED_ROUND_ROBIN_Splitter_3566));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3567() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3567WEIGHTED_ROUND_ROBIN_Splitter_3572, pop_complex(&SplitJoin14_CombineDFT_Fiss_3583_3593_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_3574() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_3584_3594_split[0]), &(SplitJoin16_CombineDFT_Fiss_3584_3594_join[0]));
	ENDFOR
}

void CombineDFT_3575() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_3584_3594_split[1]), &(SplitJoin16_CombineDFT_Fiss_3584_3594_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3572() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_3584_3594_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3567WEIGHTED_ROUND_ROBIN_Splitter_3572));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_3584_3594_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3567WEIGHTED_ROUND_ROBIN_Splitter_3572));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3573() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3573CombineDFT_3478, pop_complex(&SplitJoin16_CombineDFT_Fiss_3584_3594_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3573CombineDFT_3478, pop_complex(&SplitJoin16_CombineDFT_Fiss_3584_3594_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_3478() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_3573CombineDFT_3478), &(CombineDFT_3478CPrinter_3479));
	ENDFOR
}

void CPrinter(buffer_complex_t *chanin) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		printf("%.10f", c.real);
		printf("\n");
		printf("%.10f", c.imag);
		printf("\n");
	}


void CPrinter_3479() {
	FOR(uint32_t, __iter_steady_, 0, <, 1088, __iter_steady_++)
		CPrinter(&(CombineDFT_3478CPrinter_3479));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 16, __iter_init_0_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_3581_3591_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 16, __iter_init_1_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_3579_3589_split[__iter_init_1_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3539WEIGHTED_ROUND_ROBIN_Splitter_3556);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3520WEIGHTED_ROUND_ROBIN_Splitter_3538);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3486WEIGHTED_ROUND_ROBIN_Splitter_3491);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3482WEIGHTED_ROUND_ROBIN_Splitter_3485);
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_3578_3588_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 16, __iter_init_3_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_3579_3589_join[__iter_init_3_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3492WEIGHTED_ROUND_ROBIN_Splitter_3501);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3502WEIGHTED_ROUND_ROBIN_Splitter_3519);
	FOR(int, __iter_init_4_, 0, <, 4, __iter_init_4_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_3577_3587_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 4, __iter_init_5_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_3583_3593_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 4, __iter_init_6_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_3583_3593_join[__iter_init_6_]);
	ENDFOR
	init_buffer_complex(&FFTTestSource_3467FFTReorderSimple_3468);
	FOR(int, __iter_init_7_, 0, <, 17, __iter_init_7_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_3580_3590_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_3584_3594_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_3582_3592_join[__iter_init_9_]);
	ENDFOR
	init_buffer_complex(&FFTReorderSimple_3468WEIGHTED_ROUND_ROBIN_Splitter_3481);
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_3582_3592_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 2, __iter_init_11_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_3576_3586_join[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 8, __iter_init_12_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_3578_3588_join[__iter_init_12_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3557WEIGHTED_ROUND_ROBIN_Splitter_3566);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3573CombineDFT_3478);
	FOR(int, __iter_init_13_, 0, <, 4, __iter_init_13_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_3577_3587_split[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_3584_3594_join[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 16, __iter_init_15_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_3581_3591_split[__iter_init_15_]);
	ENDFOR
	init_buffer_complex(&CombineDFT_3478CPrinter_3479);
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_3576_3586_split[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 17, __iter_init_17_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_3580_3590_join[__iter_init_17_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3567WEIGHTED_ROUND_ROBIN_Splitter_3572);
// --- init: CombineDFT_3521
	 {
	 ; 
	CombineDFT_3521_s.wn.real = -1.0 ; 
	CombineDFT_3521_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3522
	 {
	CombineDFT_3522_s.wn.real = -1.0 ; 
	CombineDFT_3522_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3523
	 {
	CombineDFT_3523_s.wn.real = -1.0 ; 
	CombineDFT_3523_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3524
	 {
	CombineDFT_3524_s.wn.real = -1.0 ; 
	CombineDFT_3524_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3525
	 {
	CombineDFT_3525_s.wn.real = -1.0 ; 
	CombineDFT_3525_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3526
	 {
	CombineDFT_3526_s.wn.real = -1.0 ; 
	CombineDFT_3526_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3527
	 {
	CombineDFT_3527_s.wn.real = -1.0 ; 
	CombineDFT_3527_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3528
	 {
	CombineDFT_3528_s.wn.real = -1.0 ; 
	CombineDFT_3528_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3529
	 {
	CombineDFT_3529_s.wn.real = -1.0 ; 
	CombineDFT_3529_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3530
	 {
	CombineDFT_3530_s.wn.real = -1.0 ; 
	CombineDFT_3530_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3531
	 {
	CombineDFT_3531_s.wn.real = -1.0 ; 
	CombineDFT_3531_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3532
	 {
	CombineDFT_3532_s.wn.real = -1.0 ; 
	CombineDFT_3532_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3533
	 {
	CombineDFT_3533_s.wn.real = -1.0 ; 
	CombineDFT_3533_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3534
	 {
	CombineDFT_3534_s.wn.real = -1.0 ; 
	CombineDFT_3534_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3535
	 {
	CombineDFT_3535_s.wn.real = -1.0 ; 
	CombineDFT_3535_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3536
	 {
	CombineDFT_3536_s.wn.real = -1.0 ; 
	CombineDFT_3536_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3537
	 {
	CombineDFT_3537_s.wn.real = -1.0 ; 
	CombineDFT_3537_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3540
	 {
	CombineDFT_3540_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3540_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3541
	 {
	CombineDFT_3541_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3541_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3542
	 {
	CombineDFT_3542_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3542_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3543
	 {
	CombineDFT_3543_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3543_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3544
	 {
	CombineDFT_3544_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3544_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3545
	 {
	CombineDFT_3545_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3545_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3546
	 {
	CombineDFT_3546_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3546_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3547
	 {
	CombineDFT_3547_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3547_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3548
	 {
	CombineDFT_3548_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3548_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3549
	 {
	CombineDFT_3549_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3549_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3550
	 {
	CombineDFT_3550_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3550_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3551
	 {
	CombineDFT_3551_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3551_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3552
	 {
	CombineDFT_3552_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3552_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3553
	 {
	CombineDFT_3553_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3553_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3554
	 {
	CombineDFT_3554_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3554_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3555
	 {
	CombineDFT_3555_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3555_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3558
	 {
	CombineDFT_3558_s.wn.real = 0.70710677 ; 
	CombineDFT_3558_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3559
	 {
	CombineDFT_3559_s.wn.real = 0.70710677 ; 
	CombineDFT_3559_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3560
	 {
	CombineDFT_3560_s.wn.real = 0.70710677 ; 
	CombineDFT_3560_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3561
	 {
	CombineDFT_3561_s.wn.real = 0.70710677 ; 
	CombineDFT_3561_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3562
	 {
	CombineDFT_3562_s.wn.real = 0.70710677 ; 
	CombineDFT_3562_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3563
	 {
	CombineDFT_3563_s.wn.real = 0.70710677 ; 
	CombineDFT_3563_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3564
	 {
	CombineDFT_3564_s.wn.real = 0.70710677 ; 
	CombineDFT_3564_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3565
	 {
	CombineDFT_3565_s.wn.real = 0.70710677 ; 
	CombineDFT_3565_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3568
	 {
	CombineDFT_3568_s.wn.real = 0.9238795 ; 
	CombineDFT_3568_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_3569
	 {
	CombineDFT_3569_s.wn.real = 0.9238795 ; 
	CombineDFT_3569_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_3570
	 {
	CombineDFT_3570_s.wn.real = 0.9238795 ; 
	CombineDFT_3570_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_3571
	 {
	CombineDFT_3571_s.wn.real = 0.9238795 ; 
	CombineDFT_3571_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_3574
	 {
	CombineDFT_3574_s.wn.real = 0.98078525 ; 
	CombineDFT_3574_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_3575
	 {
	CombineDFT_3575_s.wn.real = 0.98078525 ; 
	CombineDFT_3575_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_3478
	 {
	 ; 
	CombineDFT_3478_s.wn.real = 0.9951847 ; 
	CombineDFT_3478_s.wn.imag = -0.09801714 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FFTTestSource_3467();
		FFTReorderSimple_3468();
		WEIGHTED_ROUND_ROBIN_Splitter_3481();
			FFTReorderSimple_3483();
			FFTReorderSimple_3484();
		WEIGHTED_ROUND_ROBIN_Joiner_3482();
		WEIGHTED_ROUND_ROBIN_Splitter_3485();
			FFTReorderSimple_3487();
			FFTReorderSimple_3488();
			FFTReorderSimple_3489();
			FFTReorderSimple_3490();
		WEIGHTED_ROUND_ROBIN_Joiner_3486();
		WEIGHTED_ROUND_ROBIN_Splitter_3491();
			FFTReorderSimple_3493();
			FFTReorderSimple_3494();
			FFTReorderSimple_3495();
			FFTReorderSimple_3496();
			FFTReorderSimple_3497();
			FFTReorderSimple_3498();
			FFTReorderSimple_3499();
			FFTReorderSimple_3500();
		WEIGHTED_ROUND_ROBIN_Joiner_3492();
		WEIGHTED_ROUND_ROBIN_Splitter_3501();
			FFTReorderSimple_3503();
			FFTReorderSimple_3504();
			FFTReorderSimple_3505();
			FFTReorderSimple_3506();
			FFTReorderSimple_3507();
			FFTReorderSimple_3508();
			FFTReorderSimple_3509();
			FFTReorderSimple_3510();
			FFTReorderSimple_3511();
			FFTReorderSimple_3512();
			FFTReorderSimple_3513();
			FFTReorderSimple_3514();
			FFTReorderSimple_3515();
			FFTReorderSimple_3516();
			FFTReorderSimple_3517();
			FFTReorderSimple_3518();
		WEIGHTED_ROUND_ROBIN_Joiner_3502();
		WEIGHTED_ROUND_ROBIN_Splitter_3519();
			CombineDFT_3521();
			CombineDFT_3522();
			CombineDFT_3523();
			CombineDFT_3524();
			CombineDFT_3525();
			CombineDFT_3526();
			CombineDFT_3527();
			CombineDFT_3528();
			CombineDFT_3529();
			CombineDFT_3530();
			CombineDFT_3531();
			CombineDFT_3532();
			CombineDFT_3533();
			CombineDFT_3534();
			CombineDFT_3535();
			CombineDFT_3536();
			CombineDFT_3537();
		WEIGHTED_ROUND_ROBIN_Joiner_3520();
		WEIGHTED_ROUND_ROBIN_Splitter_3538();
			CombineDFT_3540();
			CombineDFT_3541();
			CombineDFT_3542();
			CombineDFT_3543();
			CombineDFT_3544();
			CombineDFT_3545();
			CombineDFT_3546();
			CombineDFT_3547();
			CombineDFT_3548();
			CombineDFT_3549();
			CombineDFT_3550();
			CombineDFT_3551();
			CombineDFT_3552();
			CombineDFT_3553();
			CombineDFT_3554();
			CombineDFT_3555();
		WEIGHTED_ROUND_ROBIN_Joiner_3539();
		WEIGHTED_ROUND_ROBIN_Splitter_3556();
			CombineDFT_3558();
			CombineDFT_3559();
			CombineDFT_3560();
			CombineDFT_3561();
			CombineDFT_3562();
			CombineDFT_3563();
			CombineDFT_3564();
			CombineDFT_3565();
		WEIGHTED_ROUND_ROBIN_Joiner_3557();
		WEIGHTED_ROUND_ROBIN_Splitter_3566();
			CombineDFT_3568();
			CombineDFT_3569();
			CombineDFT_3570();
			CombineDFT_3571();
		WEIGHTED_ROUND_ROBIN_Joiner_3567();
		WEIGHTED_ROUND_ROBIN_Splitter_3572();
			CombineDFT_3574();
			CombineDFT_3575();
		WEIGHTED_ROUND_ROBIN_Joiner_3573();
		CombineDFT_3478();
		CPrinter_3479();
	ENDFOR
	return EXIT_SUCCESS;
}
