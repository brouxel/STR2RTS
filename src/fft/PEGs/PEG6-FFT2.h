#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=1536 on the compile command line
#else
#if BUF_SIZEMAX < 1536
#error BUF_SIZEMAX too small, it must be at least 1536
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float w[2];
} CombineDFT_11541_t;

typedef struct {
	float w[4];
} CombineDFT_11549_t;

typedef struct {
	float w[8];
} CombineDFT_11557_t;

typedef struct {
	float w[16];
} CombineDFT_11565_t;

typedef struct {
	float w[32];
} CombineDFT_11571_t;

typedef struct {
	float w[64];
} CombineDFT_11488_t;
void WEIGHTED_ROUND_ROBIN_Splitter_11509();
void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout);
void FFTTestSource_11511();
void FFTTestSource_11512();
void WEIGHTED_ROUND_ROBIN_Joiner_11510();
void WEIGHTED_ROUND_ROBIN_Splitter_11501();
void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout);
void FFTReorderSimple_11478();
void WEIGHTED_ROUND_ROBIN_Splitter_11513();
void FFTReorderSimple_11515();
void FFTReorderSimple_11516();
void WEIGHTED_ROUND_ROBIN_Joiner_11514();
void WEIGHTED_ROUND_ROBIN_Splitter_11517();
void FFTReorderSimple_11519();
void FFTReorderSimple_11520();
void FFTReorderSimple_11521();
void FFTReorderSimple_11522();
void WEIGHTED_ROUND_ROBIN_Joiner_11518();
void WEIGHTED_ROUND_ROBIN_Splitter_11523();
void FFTReorderSimple_11525();
void FFTReorderSimple_11526();
void FFTReorderSimple_11527();
void FFTReorderSimple_11528();
void FFTReorderSimple_11529();
void FFTReorderSimple_11530();
void WEIGHTED_ROUND_ROBIN_Joiner_11524();
void WEIGHTED_ROUND_ROBIN_Splitter_11531();
void FFTReorderSimple_11533();
void FFTReorderSimple_11534();
void FFTReorderSimple_11535();
void FFTReorderSimple_11536();
void FFTReorderSimple_11537();
void FFTReorderSimple_11538();
void WEIGHTED_ROUND_ROBIN_Joiner_11532();
void WEIGHTED_ROUND_ROBIN_Splitter_11539();
void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout);
void CombineDFT_11541();
void CombineDFT_11542();
void CombineDFT_11543();
void CombineDFT_11544();
void CombineDFT_11545();
void CombineDFT_11546();
void WEIGHTED_ROUND_ROBIN_Joiner_11540();
void WEIGHTED_ROUND_ROBIN_Splitter_11547();
void CombineDFT_11549();
void CombineDFT_11550();
void CombineDFT_11551();
void CombineDFT_11552();
void CombineDFT_11553();
void CombineDFT_11554();
void WEIGHTED_ROUND_ROBIN_Joiner_11548();
void WEIGHTED_ROUND_ROBIN_Splitter_11555();
void CombineDFT_11557();
void CombineDFT_11558();
void CombineDFT_11559();
void CombineDFT_11560();
void CombineDFT_11561();
void CombineDFT_11562();
void WEIGHTED_ROUND_ROBIN_Joiner_11556();
void WEIGHTED_ROUND_ROBIN_Splitter_11563();
void CombineDFT_11565();
void CombineDFT_11566();
void CombineDFT_11567();
void CombineDFT_11568();
void WEIGHTED_ROUND_ROBIN_Joiner_11564();
void WEIGHTED_ROUND_ROBIN_Splitter_11569();
void CombineDFT_11571();
void CombineDFT_11572();
void WEIGHTED_ROUND_ROBIN_Joiner_11570();
void CombineDFT_11488();
void FFTReorderSimple_11489();
void WEIGHTED_ROUND_ROBIN_Splitter_11573();
void FFTReorderSimple_11575();
void FFTReorderSimple_11576();
void WEIGHTED_ROUND_ROBIN_Joiner_11574();
void WEIGHTED_ROUND_ROBIN_Splitter_11577();
void FFTReorderSimple_11579();
void FFTReorderSimple_11580();
void FFTReorderSimple_11581();
void FFTReorderSimple_11582();
void WEIGHTED_ROUND_ROBIN_Joiner_11578();
void WEIGHTED_ROUND_ROBIN_Splitter_11583();
void FFTReorderSimple_11585();
void FFTReorderSimple_11586();
void FFTReorderSimple_11587();
void FFTReorderSimple_11588();
void FFTReorderSimple_11589();
void FFTReorderSimple_11590();
void WEIGHTED_ROUND_ROBIN_Joiner_11584();
void WEIGHTED_ROUND_ROBIN_Splitter_11591();
void FFTReorderSimple_11593();
void FFTReorderSimple_11594();
void FFTReorderSimple_11595();
void FFTReorderSimple_11596();
void FFTReorderSimple_11597();
void FFTReorderSimple_11598();
void WEIGHTED_ROUND_ROBIN_Joiner_11592();
void WEIGHTED_ROUND_ROBIN_Splitter_11599();
void CombineDFT_11601();
void CombineDFT_11602();
void CombineDFT_11603();
void CombineDFT_11604();
void CombineDFT_11605();
void CombineDFT_11606();
void WEIGHTED_ROUND_ROBIN_Joiner_11600();
void WEIGHTED_ROUND_ROBIN_Splitter_11607();
void CombineDFT_11609();
void CombineDFT_11610();
void CombineDFT_11611();
void CombineDFT_11612();
void CombineDFT_11613();
void CombineDFT_11614();
void WEIGHTED_ROUND_ROBIN_Joiner_11608();
void WEIGHTED_ROUND_ROBIN_Splitter_11615();
void CombineDFT_11617();
void CombineDFT_11618();
void CombineDFT_11619();
void CombineDFT_11620();
void CombineDFT_11621();
void CombineDFT_11622();
void WEIGHTED_ROUND_ROBIN_Joiner_11616();
void WEIGHTED_ROUND_ROBIN_Splitter_11623();
void CombineDFT_11625();
void CombineDFT_11626();
void CombineDFT_11627();
void CombineDFT_11628();
void WEIGHTED_ROUND_ROBIN_Joiner_11624();
void WEIGHTED_ROUND_ROBIN_Splitter_11629();
void CombineDFT_11631();
void CombineDFT_11632();
void WEIGHTED_ROUND_ROBIN_Joiner_11630();
void CombineDFT_11499();
void WEIGHTED_ROUND_ROBIN_Joiner_11502();
void FloatPrinter(buffer_float_t *chanin);
void FloatPrinter_11500();

#ifdef __cplusplus
}
#endif
#endif
