#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=384 on the compile command line
#else
#if BUF_SIZEMAX < 384
#error BUF_SIZEMAX too small, it must be at least 384
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	complex_t wn;
} CombineDFT_1995_t;
void FFTTestSource(buffer_complex_t *chanout);
void FFTTestSource_1941();
void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout);
void FFTReorderSimple_1942();
void WEIGHTED_ROUND_ROBIN_Splitter_1955();
void FFTReorderSimple_1957();
void FFTReorderSimple_1958();
void WEIGHTED_ROUND_ROBIN_Joiner_1956();
void WEIGHTED_ROUND_ROBIN_Splitter_1959();
void FFTReorderSimple_1961();
void FFTReorderSimple_1962();
void FFTReorderSimple_1963();
void FFTReorderSimple_1964();
void WEIGHTED_ROUND_ROBIN_Joiner_1960();
void WEIGHTED_ROUND_ROBIN_Splitter_1965();
void FFTReorderSimple_1967();
void FFTReorderSimple_1968();
void FFTReorderSimple_1969();
void FFTReorderSimple_1970();
void FFTReorderSimple_1971();
void FFTReorderSimple_1972();
void FFTReorderSimple_1973();
void FFTReorderSimple_1974();
void WEIGHTED_ROUND_ROBIN_Joiner_1966();
void WEIGHTED_ROUND_ROBIN_Splitter_1975();
void FFTReorderSimple_1977();
void FFTReorderSimple_1978();
void FFTReorderSimple_1979();
void FFTReorderSimple_1980();
void FFTReorderSimple_1981();
void FFTReorderSimple_1982();
void FFTReorderSimple_1983();
void FFTReorderSimple_1984();
void FFTReorderSimple_1985();
void FFTReorderSimple_1986();
void FFTReorderSimple_1987();
void FFTReorderSimple_1988();
void FFTReorderSimple_1989();
void FFTReorderSimple_1990();
void FFTReorderSimple_1991();
void FFTReorderSimple_1992();
void WEIGHTED_ROUND_ROBIN_Joiner_1976();
void WEIGHTED_ROUND_ROBIN_Splitter_1993();
void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout);
void CombineDFT_1995();
void CombineDFT_1996();
void CombineDFT_1997();
void CombineDFT_1998();
void CombineDFT_1999();
void CombineDFT_2000();
void CombineDFT_2001();
void CombineDFT_2002();
void CombineDFT_2003();
void CombineDFT_2004();
void CombineDFT_2005();
void CombineDFT_2006();
void CombineDFT_2007();
void CombineDFT_2008();
void CombineDFT_2009();
void CombineDFT_2010();
void CombineDFT_2011();
void CombineDFT_2012();
void CombineDFT_2013();
void CombineDFT_2014();
void CombineDFT_2015();
void CombineDFT_2016();
void CombineDFT_2017();
void CombineDFT_2018();
void WEIGHTED_ROUND_ROBIN_Joiner_1994();
void WEIGHTED_ROUND_ROBIN_Splitter_2019();
void CombineDFT_2021();
void CombineDFT_2022();
void CombineDFT_2023();
void CombineDFT_2024();
void CombineDFT_2025();
void CombineDFT_2026();
void CombineDFT_2027();
void CombineDFT_2028();
void CombineDFT_2029();
void CombineDFT_2030();
void CombineDFT_2031();
void CombineDFT_2032();
void CombineDFT_2033();
void CombineDFT_2034();
void CombineDFT_2035();
void CombineDFT_2036();
void WEIGHTED_ROUND_ROBIN_Joiner_2020();
void WEIGHTED_ROUND_ROBIN_Splitter_2037();
void CombineDFT_2039();
void CombineDFT_2040();
void CombineDFT_2041();
void CombineDFT_2042();
void CombineDFT_2043();
void CombineDFT_2044();
void CombineDFT_2045();
void CombineDFT_2046();
void WEIGHTED_ROUND_ROBIN_Joiner_2038();
void WEIGHTED_ROUND_ROBIN_Splitter_2047();
void CombineDFT_2049();
void CombineDFT_2050();
void CombineDFT_2051();
void CombineDFT_2052();
void WEIGHTED_ROUND_ROBIN_Joiner_2048();
void WEIGHTED_ROUND_ROBIN_Splitter_2053();
void CombineDFT_2055();
void CombineDFT_2056();
void WEIGHTED_ROUND_ROBIN_Joiner_2054();
void CombineDFT_1952();
void CPrinter(buffer_complex_t *chanin);
void CPrinter_1953();

#ifdef __cplusplus
}
#endif
#endif
