#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=640 on the compile command line
#else
#if BUF_SIZEMAX < 640
#error BUF_SIZEMAX too small, it must be at least 640
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	complex_t wn;
} CombineDFT_5663_t;
void FFTTestSource_5623();
void FFTReorderSimple_5624();
void WEIGHTED_ROUND_ROBIN_Splitter_5637();
void FFTReorderSimple_5639();
void FFTReorderSimple_5640();
void WEIGHTED_ROUND_ROBIN_Joiner_5638();
void WEIGHTED_ROUND_ROBIN_Splitter_5641();
void FFTReorderSimple_5643();
void FFTReorderSimple_5644();
void FFTReorderSimple_5645();
void FFTReorderSimple_5646();
void WEIGHTED_ROUND_ROBIN_Joiner_5642();
void WEIGHTED_ROUND_ROBIN_Splitter_5647();
void FFTReorderSimple_5649();
void FFTReorderSimple_5650();
void FFTReorderSimple_5651();
void FFTReorderSimple_5652();
void FFTReorderSimple_5653();
void WEIGHTED_ROUND_ROBIN_Joiner_5648();
void WEIGHTED_ROUND_ROBIN_Splitter_5654();
void FFTReorderSimple_5656();
void FFTReorderSimple_5657();
void FFTReorderSimple_5658();
void FFTReorderSimple_5659();
void FFTReorderSimple_5660();
void WEIGHTED_ROUND_ROBIN_Joiner_5655();
void WEIGHTED_ROUND_ROBIN_Splitter_5661();
void CombineDFT_5663();
void CombineDFT_5664();
void CombineDFT_5665();
void CombineDFT_5666();
void CombineDFT_5667();
void WEIGHTED_ROUND_ROBIN_Joiner_5662();
void WEIGHTED_ROUND_ROBIN_Splitter_5668();
void CombineDFT_5670();
void CombineDFT_5671();
void CombineDFT_5672();
void CombineDFT_5673();
void CombineDFT_5674();
void WEIGHTED_ROUND_ROBIN_Joiner_5669();
void WEIGHTED_ROUND_ROBIN_Splitter_5675();
void CombineDFT_5677();
void CombineDFT_5678();
void CombineDFT_5679();
void CombineDFT_5680();
void CombineDFT_5681();
void WEIGHTED_ROUND_ROBIN_Joiner_5676();
void WEIGHTED_ROUND_ROBIN_Splitter_5682();
void CombineDFT_5684();
void CombineDFT_5685();
void CombineDFT_5686();
void CombineDFT_5687();
void WEIGHTED_ROUND_ROBIN_Joiner_5683();
void WEIGHTED_ROUND_ROBIN_Splitter_5688();
void CombineDFT_5690();
void CombineDFT_5691();
void WEIGHTED_ROUND_ROBIN_Joiner_5689();
void CombineDFT_5634();
void CPrinter_5635();

#ifdef __cplusplus
}
#endif
#endif
