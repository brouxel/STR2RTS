#include "PEG28-FFT2.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2180WEIGHTED_ROUND_ROBIN_Splitter_2183;
buffer_float_t FFTReorderSimple_2144WEIGHTED_ROUND_ROBIN_Splitter_2179;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2218WEIGHTED_ROUND_ROBIN_Splitter_2247;
buffer_float_t SplitJoin102_FFTReorderSimple_Fiss_2402_2423_join[2];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_2133_2169_2392_2413_split[2];
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_2394_2415_join[4];
buffer_float_t SplitJoin104_FFTReorderSimple_Fiss_2403_2424_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2382WEIGHTED_ROUND_ROBIN_Splitter_2387;
buffer_float_t SplitJoin102_FFTReorderSimple_Fiss_2402_2423_split[2];
buffer_float_t SplitJoin118_CombineDFT_Fiss_2410_2431_join[2];
buffer_float_t SplitJoin12_CombineDFT_Fiss_2397_2418_split[28];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_2133_2169_2392_2413_join[2];
buffer_float_t SplitJoin110_CombineDFT_Fiss_2406_2427_split[28];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2296WEIGHTED_ROUND_ROBIN_Splitter_2305;
buffer_float_t SplitJoin106_FFTReorderSimple_Fiss_2404_2425_split[8];
buffer_float_t SplitJoin14_CombineDFT_Fiss_2398_2419_join[16];
buffer_float_t SplitJoin108_FFTReorderSimple_Fiss_2405_2426_split[16];
buffer_float_t SplitJoin14_CombineDFT_Fiss_2398_2419_split[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2372WEIGHTED_ROUND_ROBIN_Splitter_2381;
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_2393_2414_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2184WEIGHTED_ROUND_ROBIN_Splitter_2189;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2276WEIGHTED_ROUND_ROBIN_Splitter_2281;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2282CombineDFT_2154;
buffer_float_t SplitJoin114_CombineDFT_Fiss_2408_2429_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2306WEIGHTED_ROUND_ROBIN_Splitter_2323;
buffer_float_t SplitJoin116_CombineDFT_Fiss_2409_2430_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2176WEIGHTED_ROUND_ROBIN_Splitter_2167;
buffer_float_t SplitJoin12_CombineDFT_Fiss_2397_2418_join[28];
buffer_float_t SplitJoin112_CombineDFT_Fiss_2407_2428_split[16];
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_2395_2416_split[8];
buffer_float_t SplitJoin20_CombineDFT_Fiss_2401_2422_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2388CombineDFT_2165;
buffer_float_t SplitJoin108_FFTReorderSimple_Fiss_2405_2426_join[16];
buffer_float_t SplitJoin20_CombineDFT_Fiss_2401_2422_join[2];
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_2393_2414_split[2];
buffer_float_t SplitJoin0_FFTTestSource_Fiss_2391_2412_split[2];
buffer_float_t SplitJoin116_CombineDFT_Fiss_2409_2430_join[4];
buffer_float_t SplitJoin0_FFTTestSource_Fiss_2391_2412_join[2];
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_2396_2417_join[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2324WEIGHTED_ROUND_ROBIN_Splitter_2353;
buffer_float_t SplitJoin114_CombineDFT_Fiss_2408_2429_split[8];
buffer_float_t SplitJoin112_CombineDFT_Fiss_2407_2428_join[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2190WEIGHTED_ROUND_ROBIN_Splitter_2199;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2200WEIGHTED_ROUND_ROBIN_Splitter_2217;
buffer_float_t SplitJoin16_CombineDFT_Fiss_2399_2420_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2354WEIGHTED_ROUND_ROBIN_Splitter_2371;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2266WEIGHTED_ROUND_ROBIN_Splitter_2275;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2168FloatPrinter_2166;
buffer_float_t SplitJoin18_CombineDFT_Fiss_2400_2421_join[4];
buffer_float_t SplitJoin106_FFTReorderSimple_Fiss_2404_2425_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2248WEIGHTED_ROUND_ROBIN_Splitter_2265;
buffer_float_t FFTReorderSimple_2155WEIGHTED_ROUND_ROBIN_Splitter_2285;
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_2395_2416_join[8];
buffer_float_t SplitJoin110_CombineDFT_Fiss_2406_2427_join[28];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2290WEIGHTED_ROUND_ROBIN_Splitter_2295;
buffer_float_t SplitJoin118_CombineDFT_Fiss_2410_2431_split[2];
buffer_float_t SplitJoin18_CombineDFT_Fiss_2400_2421_split[4];
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_2396_2417_split[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2286WEIGHTED_ROUND_ROBIN_Splitter_2289;
buffer_float_t SplitJoin16_CombineDFT_Fiss_2399_2420_split[8];
buffer_float_t SplitJoin104_FFTReorderSimple_Fiss_2403_2424_join[4];
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_2394_2415_split[4];


CombineDFT_2219_t CombineDFT_2219_s;
CombineDFT_2219_t CombineDFT_2220_s;
CombineDFT_2219_t CombineDFT_2221_s;
CombineDFT_2219_t CombineDFT_2222_s;
CombineDFT_2219_t CombineDFT_2223_s;
CombineDFT_2219_t CombineDFT_2224_s;
CombineDFT_2219_t CombineDFT_2225_s;
CombineDFT_2219_t CombineDFT_2226_s;
CombineDFT_2219_t CombineDFT_2227_s;
CombineDFT_2219_t CombineDFT_2228_s;
CombineDFT_2219_t CombineDFT_2229_s;
CombineDFT_2219_t CombineDFT_2230_s;
CombineDFT_2219_t CombineDFT_2231_s;
CombineDFT_2219_t CombineDFT_2232_s;
CombineDFT_2219_t CombineDFT_2233_s;
CombineDFT_2219_t CombineDFT_2234_s;
CombineDFT_2219_t CombineDFT_2235_s;
CombineDFT_2219_t CombineDFT_2236_s;
CombineDFT_2219_t CombineDFT_2237_s;
CombineDFT_2219_t CombineDFT_2238_s;
CombineDFT_2219_t CombineDFT_2239_s;
CombineDFT_2219_t CombineDFT_2240_s;
CombineDFT_2219_t CombineDFT_2241_s;
CombineDFT_2219_t CombineDFT_2242_s;
CombineDFT_2219_t CombineDFT_2243_s;
CombineDFT_2219_t CombineDFT_2244_s;
CombineDFT_2219_t CombineDFT_2245_s;
CombineDFT_2219_t CombineDFT_2246_s;
CombineDFT_2249_t CombineDFT_2249_s;
CombineDFT_2249_t CombineDFT_2250_s;
CombineDFT_2249_t CombineDFT_2251_s;
CombineDFT_2249_t CombineDFT_2252_s;
CombineDFT_2249_t CombineDFT_2253_s;
CombineDFT_2249_t CombineDFT_2254_s;
CombineDFT_2249_t CombineDFT_2255_s;
CombineDFT_2249_t CombineDFT_2256_s;
CombineDFT_2249_t CombineDFT_2257_s;
CombineDFT_2249_t CombineDFT_2258_s;
CombineDFT_2249_t CombineDFT_2259_s;
CombineDFT_2249_t CombineDFT_2260_s;
CombineDFT_2249_t CombineDFT_2261_s;
CombineDFT_2249_t CombineDFT_2262_s;
CombineDFT_2249_t CombineDFT_2263_s;
CombineDFT_2249_t CombineDFT_2264_s;
CombineDFT_2267_t CombineDFT_2267_s;
CombineDFT_2267_t CombineDFT_2268_s;
CombineDFT_2267_t CombineDFT_2269_s;
CombineDFT_2267_t CombineDFT_2270_s;
CombineDFT_2267_t CombineDFT_2271_s;
CombineDFT_2267_t CombineDFT_2272_s;
CombineDFT_2267_t CombineDFT_2273_s;
CombineDFT_2267_t CombineDFT_2274_s;
CombineDFT_2277_t CombineDFT_2277_s;
CombineDFT_2277_t CombineDFT_2278_s;
CombineDFT_2277_t CombineDFT_2279_s;
CombineDFT_2277_t CombineDFT_2280_s;
CombineDFT_2283_t CombineDFT_2283_s;
CombineDFT_2283_t CombineDFT_2284_s;
CombineDFT_2154_t CombineDFT_2154_s;
CombineDFT_2219_t CombineDFT_2325_s;
CombineDFT_2219_t CombineDFT_2326_s;
CombineDFT_2219_t CombineDFT_2327_s;
CombineDFT_2219_t CombineDFT_2328_s;
CombineDFT_2219_t CombineDFT_2329_s;
CombineDFT_2219_t CombineDFT_2330_s;
CombineDFT_2219_t CombineDFT_2331_s;
CombineDFT_2219_t CombineDFT_2332_s;
CombineDFT_2219_t CombineDFT_2333_s;
CombineDFT_2219_t CombineDFT_2334_s;
CombineDFT_2219_t CombineDFT_2335_s;
CombineDFT_2219_t CombineDFT_2336_s;
CombineDFT_2219_t CombineDFT_2337_s;
CombineDFT_2219_t CombineDFT_2338_s;
CombineDFT_2219_t CombineDFT_2339_s;
CombineDFT_2219_t CombineDFT_2340_s;
CombineDFT_2219_t CombineDFT_2341_s;
CombineDFT_2219_t CombineDFT_2342_s;
CombineDFT_2219_t CombineDFT_2343_s;
CombineDFT_2219_t CombineDFT_2344_s;
CombineDFT_2219_t CombineDFT_2345_s;
CombineDFT_2219_t CombineDFT_2346_s;
CombineDFT_2219_t CombineDFT_2347_s;
CombineDFT_2219_t CombineDFT_2348_s;
CombineDFT_2219_t CombineDFT_2349_s;
CombineDFT_2219_t CombineDFT_2350_s;
CombineDFT_2219_t CombineDFT_2351_s;
CombineDFT_2219_t CombineDFT_2352_s;
CombineDFT_2249_t CombineDFT_2355_s;
CombineDFT_2249_t CombineDFT_2356_s;
CombineDFT_2249_t CombineDFT_2357_s;
CombineDFT_2249_t CombineDFT_2358_s;
CombineDFT_2249_t CombineDFT_2359_s;
CombineDFT_2249_t CombineDFT_2360_s;
CombineDFT_2249_t CombineDFT_2361_s;
CombineDFT_2249_t CombineDFT_2362_s;
CombineDFT_2249_t CombineDFT_2363_s;
CombineDFT_2249_t CombineDFT_2364_s;
CombineDFT_2249_t CombineDFT_2365_s;
CombineDFT_2249_t CombineDFT_2366_s;
CombineDFT_2249_t CombineDFT_2367_s;
CombineDFT_2249_t CombineDFT_2368_s;
CombineDFT_2249_t CombineDFT_2369_s;
CombineDFT_2249_t CombineDFT_2370_s;
CombineDFT_2267_t CombineDFT_2373_s;
CombineDFT_2267_t CombineDFT_2374_s;
CombineDFT_2267_t CombineDFT_2375_s;
CombineDFT_2267_t CombineDFT_2376_s;
CombineDFT_2267_t CombineDFT_2377_s;
CombineDFT_2267_t CombineDFT_2378_s;
CombineDFT_2267_t CombineDFT_2379_s;
CombineDFT_2267_t CombineDFT_2380_s;
CombineDFT_2277_t CombineDFT_2383_s;
CombineDFT_2277_t CombineDFT_2384_s;
CombineDFT_2277_t CombineDFT_2385_s;
CombineDFT_2277_t CombineDFT_2386_s;
CombineDFT_2283_t CombineDFT_2389_s;
CombineDFT_2283_t CombineDFT_2390_s;
CombineDFT_2154_t CombineDFT_2165_s;

void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), 0.0) ; 
		push_float(&(*chanout), 0.0) ; 
		push_float(&(*chanout), 1.0) ; 
		push_float(&(*chanout), 0.0) ; 
		FOR(int, i, 0,  < , 124, i++) {
			push_float(&(*chanout), 0.0) ; 
		}
		ENDFOR
	}


void FFTTestSource_2177() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_2391_2412_split[0]), &(SplitJoin0_FFTTestSource_Fiss_2391_2412_join[0]));
	ENDFOR
}

void FFTTestSource_2178() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_2391_2412_split[1]), &(SplitJoin0_FFTTestSource_Fiss_2391_2412_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2175() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_2176() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2176WEIGHTED_ROUND_ROBIN_Splitter_2167, pop_float(&SplitJoin0_FFTTestSource_Fiss_2391_2412_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2176WEIGHTED_ROUND_ROBIN_Splitter_2167, pop_float(&SplitJoin0_FFTTestSource_Fiss_2391_2412_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, i, 0,  < , 128, i = (i + 4)) {
			push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 128, i = (i + 4)) {
			push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_float(&(*chanin)) ; 
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void FFTReorderSimple_2144() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_2133_2169_2392_2413_split[0]), &(FFTReorderSimple_2144WEIGHTED_ROUND_ROBIN_Splitter_2179));
	ENDFOR
}

void FFTReorderSimple_2181() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_2393_2414_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_2393_2414_join[0]));
	ENDFOR
}

void FFTReorderSimple_2182() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_2393_2414_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_2393_2414_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2179() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_2393_2414_split[0], pop_float(&FFTReorderSimple_2144WEIGHTED_ROUND_ROBIN_Splitter_2179));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_2393_2414_split[1], pop_float(&FFTReorderSimple_2144WEIGHTED_ROUND_ROBIN_Splitter_2179));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2180() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2180WEIGHTED_ROUND_ROBIN_Splitter_2183, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_2393_2414_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2180WEIGHTED_ROUND_ROBIN_Splitter_2183, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_2393_2414_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_2185() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2394_2415_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_2394_2415_join[0]));
	ENDFOR
}

void FFTReorderSimple_2186() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2394_2415_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_2394_2415_join[1]));
	ENDFOR
}

void FFTReorderSimple_2187() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2394_2415_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_2394_2415_join[2]));
	ENDFOR
}

void FFTReorderSimple_2188() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2394_2415_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_2394_2415_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2183() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin6_FFTReorderSimple_Fiss_2394_2415_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2180WEIGHTED_ROUND_ROBIN_Splitter_2183));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2184() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2184WEIGHTED_ROUND_ROBIN_Splitter_2189, pop_float(&SplitJoin6_FFTReorderSimple_Fiss_2394_2415_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_2191() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_2395_2416_split[0]), &(SplitJoin8_FFTReorderSimple_Fiss_2395_2416_join[0]));
	ENDFOR
}

void FFTReorderSimple_2192() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_2395_2416_split[1]), &(SplitJoin8_FFTReorderSimple_Fiss_2395_2416_join[1]));
	ENDFOR
}

void FFTReorderSimple_2193() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_2395_2416_split[2]), &(SplitJoin8_FFTReorderSimple_Fiss_2395_2416_join[2]));
	ENDFOR
}

void FFTReorderSimple_2194() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_2395_2416_split[3]), &(SplitJoin8_FFTReorderSimple_Fiss_2395_2416_join[3]));
	ENDFOR
}

void FFTReorderSimple_2195() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_2395_2416_split[4]), &(SplitJoin8_FFTReorderSimple_Fiss_2395_2416_join[4]));
	ENDFOR
}

void FFTReorderSimple_2196() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_2395_2416_split[5]), &(SplitJoin8_FFTReorderSimple_Fiss_2395_2416_join[5]));
	ENDFOR
}

void FFTReorderSimple_2197() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_2395_2416_split[6]), &(SplitJoin8_FFTReorderSimple_Fiss_2395_2416_join[6]));
	ENDFOR
}

void FFTReorderSimple_2198() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_2395_2416_split[7]), &(SplitJoin8_FFTReorderSimple_Fiss_2395_2416_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2189() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin8_FFTReorderSimple_Fiss_2395_2416_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2184WEIGHTED_ROUND_ROBIN_Splitter_2189));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2190() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2190WEIGHTED_ROUND_ROBIN_Splitter_2199, pop_float(&SplitJoin8_FFTReorderSimple_Fiss_2395_2416_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_2201() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_split[0]), &(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_join[0]));
	ENDFOR
}

void FFTReorderSimple_2202() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_split[1]), &(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_join[1]));
	ENDFOR
}

void FFTReorderSimple_2203() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_split[2]), &(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_join[2]));
	ENDFOR
}

void FFTReorderSimple_2204() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_split[3]), &(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_join[3]));
	ENDFOR
}

void FFTReorderSimple_2205() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_split[4]), &(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_join[4]));
	ENDFOR
}

void FFTReorderSimple_2206() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_split[5]), &(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_join[5]));
	ENDFOR
}

void FFTReorderSimple_2207() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_split[6]), &(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_join[6]));
	ENDFOR
}

void FFTReorderSimple_2208() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_split[7]), &(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_join[7]));
	ENDFOR
}

void FFTReorderSimple_2209() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_split[8]), &(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_join[8]));
	ENDFOR
}

void FFTReorderSimple_2210() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_split[9]), &(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_join[9]));
	ENDFOR
}

void FFTReorderSimple_2211() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_split[10]), &(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_join[10]));
	ENDFOR
}

void FFTReorderSimple_2212() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_split[11]), &(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_join[11]));
	ENDFOR
}

void FFTReorderSimple_2213() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_split[12]), &(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_join[12]));
	ENDFOR
}

void FFTReorderSimple_2214() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_split[13]), &(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_join[13]));
	ENDFOR
}

void FFTReorderSimple_2215() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_split[14]), &(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_join[14]));
	ENDFOR
}

void FFTReorderSimple_2216() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_split[15]), &(SplitJoin10_FFTReorderSimple_Fiss_2396_2417_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2199() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_FFTReorderSimple_Fiss_2396_2417_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2190WEIGHTED_ROUND_ROBIN_Splitter_2199));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2200() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2200WEIGHTED_ROUND_ROBIN_Splitter_2217, pop_float(&SplitJoin10_FFTReorderSimple_Fiss_2396_2417_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&(*chanin), i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&(*chanin), i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&(*chanin), (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&(*chanin), (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_2219_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_2219_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&(*chanin)) ; 
			push_float(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_2219() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2397_2418_split[0]), &(SplitJoin12_CombineDFT_Fiss_2397_2418_join[0]));
	ENDFOR
}

void CombineDFT_2220() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2397_2418_split[1]), &(SplitJoin12_CombineDFT_Fiss_2397_2418_join[1]));
	ENDFOR
}

void CombineDFT_2221() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2397_2418_split[2]), &(SplitJoin12_CombineDFT_Fiss_2397_2418_join[2]));
	ENDFOR
}

void CombineDFT_2222() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2397_2418_split[3]), &(SplitJoin12_CombineDFT_Fiss_2397_2418_join[3]));
	ENDFOR
}

void CombineDFT_2223() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2397_2418_split[4]), &(SplitJoin12_CombineDFT_Fiss_2397_2418_join[4]));
	ENDFOR
}

void CombineDFT_2224() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2397_2418_split[5]), &(SplitJoin12_CombineDFT_Fiss_2397_2418_join[5]));
	ENDFOR
}

void CombineDFT_2225() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2397_2418_split[6]), &(SplitJoin12_CombineDFT_Fiss_2397_2418_join[6]));
	ENDFOR
}

void CombineDFT_2226() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2397_2418_split[7]), &(SplitJoin12_CombineDFT_Fiss_2397_2418_join[7]));
	ENDFOR
}

void CombineDFT_2227() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2397_2418_split[8]), &(SplitJoin12_CombineDFT_Fiss_2397_2418_join[8]));
	ENDFOR
}

void CombineDFT_2228() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2397_2418_split[9]), &(SplitJoin12_CombineDFT_Fiss_2397_2418_join[9]));
	ENDFOR
}

void CombineDFT_2229() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2397_2418_split[10]), &(SplitJoin12_CombineDFT_Fiss_2397_2418_join[10]));
	ENDFOR
}

void CombineDFT_2230() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2397_2418_split[11]), &(SplitJoin12_CombineDFT_Fiss_2397_2418_join[11]));
	ENDFOR
}

void CombineDFT_2231() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2397_2418_split[12]), &(SplitJoin12_CombineDFT_Fiss_2397_2418_join[12]));
	ENDFOR
}

void CombineDFT_2232() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2397_2418_split[13]), &(SplitJoin12_CombineDFT_Fiss_2397_2418_join[13]));
	ENDFOR
}

void CombineDFT_2233() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2397_2418_split[14]), &(SplitJoin12_CombineDFT_Fiss_2397_2418_join[14]));
	ENDFOR
}

void CombineDFT_2234() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2397_2418_split[15]), &(SplitJoin12_CombineDFT_Fiss_2397_2418_join[15]));
	ENDFOR
}

void CombineDFT_2235() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2397_2418_split[16]), &(SplitJoin12_CombineDFT_Fiss_2397_2418_join[16]));
	ENDFOR
}

void CombineDFT_2236() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2397_2418_split[17]), &(SplitJoin12_CombineDFT_Fiss_2397_2418_join[17]));
	ENDFOR
}

void CombineDFT_2237() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2397_2418_split[18]), &(SplitJoin12_CombineDFT_Fiss_2397_2418_join[18]));
	ENDFOR
}

void CombineDFT_2238() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2397_2418_split[19]), &(SplitJoin12_CombineDFT_Fiss_2397_2418_join[19]));
	ENDFOR
}

void CombineDFT_2239() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2397_2418_split[20]), &(SplitJoin12_CombineDFT_Fiss_2397_2418_join[20]));
	ENDFOR
}

void CombineDFT_2240() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2397_2418_split[21]), &(SplitJoin12_CombineDFT_Fiss_2397_2418_join[21]));
	ENDFOR
}

void CombineDFT_2241() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2397_2418_split[22]), &(SplitJoin12_CombineDFT_Fiss_2397_2418_join[22]));
	ENDFOR
}

void CombineDFT_2242() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2397_2418_split[23]), &(SplitJoin12_CombineDFT_Fiss_2397_2418_join[23]));
	ENDFOR
}

void CombineDFT_2243() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2397_2418_split[24]), &(SplitJoin12_CombineDFT_Fiss_2397_2418_join[24]));
	ENDFOR
}

void CombineDFT_2244() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2397_2418_split[25]), &(SplitJoin12_CombineDFT_Fiss_2397_2418_join[25]));
	ENDFOR
}

void CombineDFT_2245() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2397_2418_split[26]), &(SplitJoin12_CombineDFT_Fiss_2397_2418_join[26]));
	ENDFOR
}

void CombineDFT_2246() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2397_2418_split[27]), &(SplitJoin12_CombineDFT_Fiss_2397_2418_join[27]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2217() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 28, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin12_CombineDFT_Fiss_2397_2418_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2200WEIGHTED_ROUND_ROBIN_Splitter_2217));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2218() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 28, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2218WEIGHTED_ROUND_ROBIN_Splitter_2247, pop_float(&SplitJoin12_CombineDFT_Fiss_2397_2418_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2249() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2398_2419_split[0]), &(SplitJoin14_CombineDFT_Fiss_2398_2419_join[0]));
	ENDFOR
}

void CombineDFT_2250() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2398_2419_split[1]), &(SplitJoin14_CombineDFT_Fiss_2398_2419_join[1]));
	ENDFOR
}

void CombineDFT_2251() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2398_2419_split[2]), &(SplitJoin14_CombineDFT_Fiss_2398_2419_join[2]));
	ENDFOR
}

void CombineDFT_2252() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2398_2419_split[3]), &(SplitJoin14_CombineDFT_Fiss_2398_2419_join[3]));
	ENDFOR
}

void CombineDFT_2253() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2398_2419_split[4]), &(SplitJoin14_CombineDFT_Fiss_2398_2419_join[4]));
	ENDFOR
}

void CombineDFT_2254() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2398_2419_split[5]), &(SplitJoin14_CombineDFT_Fiss_2398_2419_join[5]));
	ENDFOR
}

void CombineDFT_2255() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2398_2419_split[6]), &(SplitJoin14_CombineDFT_Fiss_2398_2419_join[6]));
	ENDFOR
}

void CombineDFT_2256() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2398_2419_split[7]), &(SplitJoin14_CombineDFT_Fiss_2398_2419_join[7]));
	ENDFOR
}

void CombineDFT_2257() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2398_2419_split[8]), &(SplitJoin14_CombineDFT_Fiss_2398_2419_join[8]));
	ENDFOR
}

void CombineDFT_2258() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2398_2419_split[9]), &(SplitJoin14_CombineDFT_Fiss_2398_2419_join[9]));
	ENDFOR
}

void CombineDFT_2259() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2398_2419_split[10]), &(SplitJoin14_CombineDFT_Fiss_2398_2419_join[10]));
	ENDFOR
}

void CombineDFT_2260() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2398_2419_split[11]), &(SplitJoin14_CombineDFT_Fiss_2398_2419_join[11]));
	ENDFOR
}

void CombineDFT_2261() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2398_2419_split[12]), &(SplitJoin14_CombineDFT_Fiss_2398_2419_join[12]));
	ENDFOR
}

void CombineDFT_2262() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2398_2419_split[13]), &(SplitJoin14_CombineDFT_Fiss_2398_2419_join[13]));
	ENDFOR
}

void CombineDFT_2263() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2398_2419_split[14]), &(SplitJoin14_CombineDFT_Fiss_2398_2419_join[14]));
	ENDFOR
}

void CombineDFT_2264() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2398_2419_split[15]), &(SplitJoin14_CombineDFT_Fiss_2398_2419_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2247() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin14_CombineDFT_Fiss_2398_2419_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2218WEIGHTED_ROUND_ROBIN_Splitter_2247));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2248() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2248WEIGHTED_ROUND_ROBIN_Splitter_2265, pop_float(&SplitJoin14_CombineDFT_Fiss_2398_2419_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2267() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_2399_2420_split[0]), &(SplitJoin16_CombineDFT_Fiss_2399_2420_join[0]));
	ENDFOR
}

void CombineDFT_2268() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_2399_2420_split[1]), &(SplitJoin16_CombineDFT_Fiss_2399_2420_join[1]));
	ENDFOR
}

void CombineDFT_2269() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_2399_2420_split[2]), &(SplitJoin16_CombineDFT_Fiss_2399_2420_join[2]));
	ENDFOR
}

void CombineDFT_2270() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_2399_2420_split[3]), &(SplitJoin16_CombineDFT_Fiss_2399_2420_join[3]));
	ENDFOR
}

void CombineDFT_2271() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_2399_2420_split[4]), &(SplitJoin16_CombineDFT_Fiss_2399_2420_join[4]));
	ENDFOR
}

void CombineDFT_2272() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_2399_2420_split[5]), &(SplitJoin16_CombineDFT_Fiss_2399_2420_join[5]));
	ENDFOR
}

void CombineDFT_2273() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_2399_2420_split[6]), &(SplitJoin16_CombineDFT_Fiss_2399_2420_join[6]));
	ENDFOR
}

void CombineDFT_2274() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_2399_2420_split[7]), &(SplitJoin16_CombineDFT_Fiss_2399_2420_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2265() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin16_CombineDFT_Fiss_2399_2420_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2248WEIGHTED_ROUND_ROBIN_Splitter_2265));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2266() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2266WEIGHTED_ROUND_ROBIN_Splitter_2275, pop_float(&SplitJoin16_CombineDFT_Fiss_2399_2420_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2277() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_2400_2421_split[0]), &(SplitJoin18_CombineDFT_Fiss_2400_2421_join[0]));
	ENDFOR
}

void CombineDFT_2278() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_2400_2421_split[1]), &(SplitJoin18_CombineDFT_Fiss_2400_2421_join[1]));
	ENDFOR
}

void CombineDFT_2279() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_2400_2421_split[2]), &(SplitJoin18_CombineDFT_Fiss_2400_2421_join[2]));
	ENDFOR
}

void CombineDFT_2280() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_2400_2421_split[3]), &(SplitJoin18_CombineDFT_Fiss_2400_2421_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2275() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin18_CombineDFT_Fiss_2400_2421_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2266WEIGHTED_ROUND_ROBIN_Splitter_2275));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2276() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2276WEIGHTED_ROUND_ROBIN_Splitter_2281, pop_float(&SplitJoin18_CombineDFT_Fiss_2400_2421_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2283() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin20_CombineDFT_Fiss_2401_2422_split[0]), &(SplitJoin20_CombineDFT_Fiss_2401_2422_join[0]));
	ENDFOR
}

void CombineDFT_2284() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin20_CombineDFT_Fiss_2401_2422_split[1]), &(SplitJoin20_CombineDFT_Fiss_2401_2422_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2281() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_2401_2422_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2276WEIGHTED_ROUND_ROBIN_Splitter_2281));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_2401_2422_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2276WEIGHTED_ROUND_ROBIN_Splitter_2281));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2282() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2282CombineDFT_2154, pop_float(&SplitJoin20_CombineDFT_Fiss_2401_2422_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2282CombineDFT_2154, pop_float(&SplitJoin20_CombineDFT_Fiss_2401_2422_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_2154() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_2282CombineDFT_2154), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_2133_2169_2392_2413_join[0]));
	ENDFOR
}

void FFTReorderSimple_2155() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_2133_2169_2392_2413_split[1]), &(FFTReorderSimple_2155WEIGHTED_ROUND_ROBIN_Splitter_2285));
	ENDFOR
}

void FFTReorderSimple_2287() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin102_FFTReorderSimple_Fiss_2402_2423_split[0]), &(SplitJoin102_FFTReorderSimple_Fiss_2402_2423_join[0]));
	ENDFOR
}

void FFTReorderSimple_2288() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin102_FFTReorderSimple_Fiss_2402_2423_split[1]), &(SplitJoin102_FFTReorderSimple_Fiss_2402_2423_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2285() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_2402_2423_split[0], pop_float(&FFTReorderSimple_2155WEIGHTED_ROUND_ROBIN_Splitter_2285));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin102_FFTReorderSimple_Fiss_2402_2423_split[1], pop_float(&FFTReorderSimple_2155WEIGHTED_ROUND_ROBIN_Splitter_2285));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2286() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2286WEIGHTED_ROUND_ROBIN_Splitter_2289, pop_float(&SplitJoin102_FFTReorderSimple_Fiss_2402_2423_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2286WEIGHTED_ROUND_ROBIN_Splitter_2289, pop_float(&SplitJoin102_FFTReorderSimple_Fiss_2402_2423_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_2291() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin104_FFTReorderSimple_Fiss_2403_2424_split[0]), &(SplitJoin104_FFTReorderSimple_Fiss_2403_2424_join[0]));
	ENDFOR
}

void FFTReorderSimple_2292() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin104_FFTReorderSimple_Fiss_2403_2424_split[1]), &(SplitJoin104_FFTReorderSimple_Fiss_2403_2424_join[1]));
	ENDFOR
}

void FFTReorderSimple_2293() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin104_FFTReorderSimple_Fiss_2403_2424_split[2]), &(SplitJoin104_FFTReorderSimple_Fiss_2403_2424_join[2]));
	ENDFOR
}

void FFTReorderSimple_2294() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin104_FFTReorderSimple_Fiss_2403_2424_split[3]), &(SplitJoin104_FFTReorderSimple_Fiss_2403_2424_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2289() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin104_FFTReorderSimple_Fiss_2403_2424_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2286WEIGHTED_ROUND_ROBIN_Splitter_2289));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2290() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2290WEIGHTED_ROUND_ROBIN_Splitter_2295, pop_float(&SplitJoin104_FFTReorderSimple_Fiss_2403_2424_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_2297() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin106_FFTReorderSimple_Fiss_2404_2425_split[0]), &(SplitJoin106_FFTReorderSimple_Fiss_2404_2425_join[0]));
	ENDFOR
}

void FFTReorderSimple_2298() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin106_FFTReorderSimple_Fiss_2404_2425_split[1]), &(SplitJoin106_FFTReorderSimple_Fiss_2404_2425_join[1]));
	ENDFOR
}

void FFTReorderSimple_2299() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin106_FFTReorderSimple_Fiss_2404_2425_split[2]), &(SplitJoin106_FFTReorderSimple_Fiss_2404_2425_join[2]));
	ENDFOR
}

void FFTReorderSimple_2300() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin106_FFTReorderSimple_Fiss_2404_2425_split[3]), &(SplitJoin106_FFTReorderSimple_Fiss_2404_2425_join[3]));
	ENDFOR
}

void FFTReorderSimple_2301() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin106_FFTReorderSimple_Fiss_2404_2425_split[4]), &(SplitJoin106_FFTReorderSimple_Fiss_2404_2425_join[4]));
	ENDFOR
}

void FFTReorderSimple_2302() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin106_FFTReorderSimple_Fiss_2404_2425_split[5]), &(SplitJoin106_FFTReorderSimple_Fiss_2404_2425_join[5]));
	ENDFOR
}

void FFTReorderSimple_2303() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin106_FFTReorderSimple_Fiss_2404_2425_split[6]), &(SplitJoin106_FFTReorderSimple_Fiss_2404_2425_join[6]));
	ENDFOR
}

void FFTReorderSimple_2304() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin106_FFTReorderSimple_Fiss_2404_2425_split[7]), &(SplitJoin106_FFTReorderSimple_Fiss_2404_2425_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2295() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin106_FFTReorderSimple_Fiss_2404_2425_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2290WEIGHTED_ROUND_ROBIN_Splitter_2295));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2296() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2296WEIGHTED_ROUND_ROBIN_Splitter_2305, pop_float(&SplitJoin106_FFTReorderSimple_Fiss_2404_2425_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_2307() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_split[0]), &(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_join[0]));
	ENDFOR
}

void FFTReorderSimple_2308() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_split[1]), &(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_join[1]));
	ENDFOR
}

void FFTReorderSimple_2309() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_split[2]), &(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_join[2]));
	ENDFOR
}

void FFTReorderSimple_2310() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_split[3]), &(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_join[3]));
	ENDFOR
}

void FFTReorderSimple_2311() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_split[4]), &(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_join[4]));
	ENDFOR
}

void FFTReorderSimple_2312() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_split[5]), &(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_join[5]));
	ENDFOR
}

void FFTReorderSimple_2313() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_split[6]), &(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_join[6]));
	ENDFOR
}

void FFTReorderSimple_2314() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_split[7]), &(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_join[7]));
	ENDFOR
}

void FFTReorderSimple_2315() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_split[8]), &(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_join[8]));
	ENDFOR
}

void FFTReorderSimple_2316() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_split[9]), &(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_join[9]));
	ENDFOR
}

void FFTReorderSimple_2317() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_split[10]), &(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_join[10]));
	ENDFOR
}

void FFTReorderSimple_2318() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_split[11]), &(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_join[11]));
	ENDFOR
}

void FFTReorderSimple_2319() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_split[12]), &(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_join[12]));
	ENDFOR
}

void FFTReorderSimple_2320() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_split[13]), &(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_join[13]));
	ENDFOR
}

void FFTReorderSimple_2321() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_split[14]), &(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_join[14]));
	ENDFOR
}

void FFTReorderSimple_2322() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_split[15]), &(SplitJoin108_FFTReorderSimple_Fiss_2405_2426_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2305() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin108_FFTReorderSimple_Fiss_2405_2426_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2296WEIGHTED_ROUND_ROBIN_Splitter_2305));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2306() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2306WEIGHTED_ROUND_ROBIN_Splitter_2323, pop_float(&SplitJoin108_FFTReorderSimple_Fiss_2405_2426_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2325() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_2406_2427_split[0]), &(SplitJoin110_CombineDFT_Fiss_2406_2427_join[0]));
	ENDFOR
}

void CombineDFT_2326() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_2406_2427_split[1]), &(SplitJoin110_CombineDFT_Fiss_2406_2427_join[1]));
	ENDFOR
}

void CombineDFT_2327() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_2406_2427_split[2]), &(SplitJoin110_CombineDFT_Fiss_2406_2427_join[2]));
	ENDFOR
}

void CombineDFT_2328() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_2406_2427_split[3]), &(SplitJoin110_CombineDFT_Fiss_2406_2427_join[3]));
	ENDFOR
}

void CombineDFT_2329() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_2406_2427_split[4]), &(SplitJoin110_CombineDFT_Fiss_2406_2427_join[4]));
	ENDFOR
}

void CombineDFT_2330() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_2406_2427_split[5]), &(SplitJoin110_CombineDFT_Fiss_2406_2427_join[5]));
	ENDFOR
}

void CombineDFT_2331() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_2406_2427_split[6]), &(SplitJoin110_CombineDFT_Fiss_2406_2427_join[6]));
	ENDFOR
}

void CombineDFT_2332() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_2406_2427_split[7]), &(SplitJoin110_CombineDFT_Fiss_2406_2427_join[7]));
	ENDFOR
}

void CombineDFT_2333() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_2406_2427_split[8]), &(SplitJoin110_CombineDFT_Fiss_2406_2427_join[8]));
	ENDFOR
}

void CombineDFT_2334() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_2406_2427_split[9]), &(SplitJoin110_CombineDFT_Fiss_2406_2427_join[9]));
	ENDFOR
}

void CombineDFT_2335() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_2406_2427_split[10]), &(SplitJoin110_CombineDFT_Fiss_2406_2427_join[10]));
	ENDFOR
}

void CombineDFT_2336() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_2406_2427_split[11]), &(SplitJoin110_CombineDFT_Fiss_2406_2427_join[11]));
	ENDFOR
}

void CombineDFT_2337() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_2406_2427_split[12]), &(SplitJoin110_CombineDFT_Fiss_2406_2427_join[12]));
	ENDFOR
}

void CombineDFT_2338() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_2406_2427_split[13]), &(SplitJoin110_CombineDFT_Fiss_2406_2427_join[13]));
	ENDFOR
}

void CombineDFT_2339() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_2406_2427_split[14]), &(SplitJoin110_CombineDFT_Fiss_2406_2427_join[14]));
	ENDFOR
}

void CombineDFT_2340() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_2406_2427_split[15]), &(SplitJoin110_CombineDFT_Fiss_2406_2427_join[15]));
	ENDFOR
}

void CombineDFT_2341() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_2406_2427_split[16]), &(SplitJoin110_CombineDFT_Fiss_2406_2427_join[16]));
	ENDFOR
}

void CombineDFT_2342() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_2406_2427_split[17]), &(SplitJoin110_CombineDFT_Fiss_2406_2427_join[17]));
	ENDFOR
}

void CombineDFT_2343() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_2406_2427_split[18]), &(SplitJoin110_CombineDFT_Fiss_2406_2427_join[18]));
	ENDFOR
}

void CombineDFT_2344() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_2406_2427_split[19]), &(SplitJoin110_CombineDFT_Fiss_2406_2427_join[19]));
	ENDFOR
}

void CombineDFT_2345() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_2406_2427_split[20]), &(SplitJoin110_CombineDFT_Fiss_2406_2427_join[20]));
	ENDFOR
}

void CombineDFT_2346() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_2406_2427_split[21]), &(SplitJoin110_CombineDFT_Fiss_2406_2427_join[21]));
	ENDFOR
}

void CombineDFT_2347() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_2406_2427_split[22]), &(SplitJoin110_CombineDFT_Fiss_2406_2427_join[22]));
	ENDFOR
}

void CombineDFT_2348() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_2406_2427_split[23]), &(SplitJoin110_CombineDFT_Fiss_2406_2427_join[23]));
	ENDFOR
}

void CombineDFT_2349() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_2406_2427_split[24]), &(SplitJoin110_CombineDFT_Fiss_2406_2427_join[24]));
	ENDFOR
}

void CombineDFT_2350() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_2406_2427_split[25]), &(SplitJoin110_CombineDFT_Fiss_2406_2427_join[25]));
	ENDFOR
}

void CombineDFT_2351() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_2406_2427_split[26]), &(SplitJoin110_CombineDFT_Fiss_2406_2427_join[26]));
	ENDFOR
}

void CombineDFT_2352() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_2406_2427_split[27]), &(SplitJoin110_CombineDFT_Fiss_2406_2427_join[27]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2323() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 28, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin110_CombineDFT_Fiss_2406_2427_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2306WEIGHTED_ROUND_ROBIN_Splitter_2323));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2324() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 28, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2324WEIGHTED_ROUND_ROBIN_Splitter_2353, pop_float(&SplitJoin110_CombineDFT_Fiss_2406_2427_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2355() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_2407_2428_split[0]), &(SplitJoin112_CombineDFT_Fiss_2407_2428_join[0]));
	ENDFOR
}

void CombineDFT_2356() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_2407_2428_split[1]), &(SplitJoin112_CombineDFT_Fiss_2407_2428_join[1]));
	ENDFOR
}

void CombineDFT_2357() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_2407_2428_split[2]), &(SplitJoin112_CombineDFT_Fiss_2407_2428_join[2]));
	ENDFOR
}

void CombineDFT_2358() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_2407_2428_split[3]), &(SplitJoin112_CombineDFT_Fiss_2407_2428_join[3]));
	ENDFOR
}

void CombineDFT_2359() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_2407_2428_split[4]), &(SplitJoin112_CombineDFT_Fiss_2407_2428_join[4]));
	ENDFOR
}

void CombineDFT_2360() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_2407_2428_split[5]), &(SplitJoin112_CombineDFT_Fiss_2407_2428_join[5]));
	ENDFOR
}

void CombineDFT_2361() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_2407_2428_split[6]), &(SplitJoin112_CombineDFT_Fiss_2407_2428_join[6]));
	ENDFOR
}

void CombineDFT_2362() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_2407_2428_split[7]), &(SplitJoin112_CombineDFT_Fiss_2407_2428_join[7]));
	ENDFOR
}

void CombineDFT_2363() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_2407_2428_split[8]), &(SplitJoin112_CombineDFT_Fiss_2407_2428_join[8]));
	ENDFOR
}

void CombineDFT_2364() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_2407_2428_split[9]), &(SplitJoin112_CombineDFT_Fiss_2407_2428_join[9]));
	ENDFOR
}

void CombineDFT_2365() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_2407_2428_split[10]), &(SplitJoin112_CombineDFT_Fiss_2407_2428_join[10]));
	ENDFOR
}

void CombineDFT_2366() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_2407_2428_split[11]), &(SplitJoin112_CombineDFT_Fiss_2407_2428_join[11]));
	ENDFOR
}

void CombineDFT_2367() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_2407_2428_split[12]), &(SplitJoin112_CombineDFT_Fiss_2407_2428_join[12]));
	ENDFOR
}

void CombineDFT_2368() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_2407_2428_split[13]), &(SplitJoin112_CombineDFT_Fiss_2407_2428_join[13]));
	ENDFOR
}

void CombineDFT_2369() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_2407_2428_split[14]), &(SplitJoin112_CombineDFT_Fiss_2407_2428_join[14]));
	ENDFOR
}

void CombineDFT_2370() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_2407_2428_split[15]), &(SplitJoin112_CombineDFT_Fiss_2407_2428_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2353() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin112_CombineDFT_Fiss_2407_2428_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2324WEIGHTED_ROUND_ROBIN_Splitter_2353));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2354() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2354WEIGHTED_ROUND_ROBIN_Splitter_2371, pop_float(&SplitJoin112_CombineDFT_Fiss_2407_2428_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2373() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin114_CombineDFT_Fiss_2408_2429_split[0]), &(SplitJoin114_CombineDFT_Fiss_2408_2429_join[0]));
	ENDFOR
}

void CombineDFT_2374() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin114_CombineDFT_Fiss_2408_2429_split[1]), &(SplitJoin114_CombineDFT_Fiss_2408_2429_join[1]));
	ENDFOR
}

void CombineDFT_2375() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin114_CombineDFT_Fiss_2408_2429_split[2]), &(SplitJoin114_CombineDFT_Fiss_2408_2429_join[2]));
	ENDFOR
}

void CombineDFT_2376() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin114_CombineDFT_Fiss_2408_2429_split[3]), &(SplitJoin114_CombineDFT_Fiss_2408_2429_join[3]));
	ENDFOR
}

void CombineDFT_2377() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin114_CombineDFT_Fiss_2408_2429_split[4]), &(SplitJoin114_CombineDFT_Fiss_2408_2429_join[4]));
	ENDFOR
}

void CombineDFT_2378() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin114_CombineDFT_Fiss_2408_2429_split[5]), &(SplitJoin114_CombineDFT_Fiss_2408_2429_join[5]));
	ENDFOR
}

void CombineDFT_2379() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin114_CombineDFT_Fiss_2408_2429_split[6]), &(SplitJoin114_CombineDFT_Fiss_2408_2429_join[6]));
	ENDFOR
}

void CombineDFT_2380() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin114_CombineDFT_Fiss_2408_2429_split[7]), &(SplitJoin114_CombineDFT_Fiss_2408_2429_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2371() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin114_CombineDFT_Fiss_2408_2429_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2354WEIGHTED_ROUND_ROBIN_Splitter_2371));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2372() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2372WEIGHTED_ROUND_ROBIN_Splitter_2381, pop_float(&SplitJoin114_CombineDFT_Fiss_2408_2429_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2383() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin116_CombineDFT_Fiss_2409_2430_split[0]), &(SplitJoin116_CombineDFT_Fiss_2409_2430_join[0]));
	ENDFOR
}

void CombineDFT_2384() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin116_CombineDFT_Fiss_2409_2430_split[1]), &(SplitJoin116_CombineDFT_Fiss_2409_2430_join[1]));
	ENDFOR
}

void CombineDFT_2385() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin116_CombineDFT_Fiss_2409_2430_split[2]), &(SplitJoin116_CombineDFT_Fiss_2409_2430_join[2]));
	ENDFOR
}

void CombineDFT_2386() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin116_CombineDFT_Fiss_2409_2430_split[3]), &(SplitJoin116_CombineDFT_Fiss_2409_2430_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2381() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin116_CombineDFT_Fiss_2409_2430_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2372WEIGHTED_ROUND_ROBIN_Splitter_2381));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2382() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2382WEIGHTED_ROUND_ROBIN_Splitter_2387, pop_float(&SplitJoin116_CombineDFT_Fiss_2409_2430_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2389() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin118_CombineDFT_Fiss_2410_2431_split[0]), &(SplitJoin118_CombineDFT_Fiss_2410_2431_join[0]));
	ENDFOR
}

void CombineDFT_2390() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin118_CombineDFT_Fiss_2410_2431_split[1]), &(SplitJoin118_CombineDFT_Fiss_2410_2431_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2387() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin118_CombineDFT_Fiss_2410_2431_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2382WEIGHTED_ROUND_ROBIN_Splitter_2387));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin118_CombineDFT_Fiss_2410_2431_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2382WEIGHTED_ROUND_ROBIN_Splitter_2387));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2388() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2388CombineDFT_2165, pop_float(&SplitJoin118_CombineDFT_Fiss_2410_2431_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2388CombineDFT_2165, pop_float(&SplitJoin118_CombineDFT_Fiss_2410_2431_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_2165() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_2388CombineDFT_2165), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_2133_2169_2392_2413_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2167() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_2133_2169_2392_2413_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2176WEIGHTED_ROUND_ROBIN_Splitter_2167));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_2133_2169_2392_2413_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2176WEIGHTED_ROUND_ROBIN_Splitter_2167));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2168() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2168FloatPrinter_2166, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_2133_2169_2392_2413_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2168FloatPrinter_2166, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_2133_2169_2392_2413_join[1]));
		ENDFOR
	ENDFOR
}}

void FloatPrinter(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void FloatPrinter_2166() {
	FOR(uint32_t, __iter_steady_, 0, <, 1792, __iter_steady_++)
		FloatPrinter(&(WEIGHTED_ROUND_ROBIN_Joiner_2168FloatPrinter_2166));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2180WEIGHTED_ROUND_ROBIN_Splitter_2183);
	init_buffer_float(&FFTReorderSimple_2144WEIGHTED_ROUND_ROBIN_Splitter_2179);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2218WEIGHTED_ROUND_ROBIN_Splitter_2247);
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_float(&SplitJoin102_FFTReorderSimple_Fiss_2402_2423_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 2, __iter_init_1_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_2133_2169_2392_2413_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 4, __iter_init_2_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_2394_2415_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 4, __iter_init_3_++)
		init_buffer_float(&SplitJoin104_FFTReorderSimple_Fiss_2403_2424_split[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2382WEIGHTED_ROUND_ROBIN_Splitter_2387);
	FOR(int, __iter_init_4_, 0, <, 2, __iter_init_4_++)
		init_buffer_float(&SplitJoin102_FFTReorderSimple_Fiss_2402_2423_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 2, __iter_init_5_++)
		init_buffer_float(&SplitJoin118_CombineDFT_Fiss_2410_2431_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 28, __iter_init_6_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_2397_2418_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 2, __iter_init_7_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_2133_2169_2392_2413_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 28, __iter_init_8_++)
		init_buffer_float(&SplitJoin110_CombineDFT_Fiss_2406_2427_split[__iter_init_8_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2296WEIGHTED_ROUND_ROBIN_Splitter_2305);
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_float(&SplitJoin106_FFTReorderSimple_Fiss_2404_2425_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 16, __iter_init_10_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_2398_2419_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 16, __iter_init_11_++)
		init_buffer_float(&SplitJoin108_FFTReorderSimple_Fiss_2405_2426_split[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 16, __iter_init_12_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_2398_2419_split[__iter_init_12_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2372WEIGHTED_ROUND_ROBIN_Splitter_2381);
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_2393_2414_join[__iter_init_13_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2184WEIGHTED_ROUND_ROBIN_Splitter_2189);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2276WEIGHTED_ROUND_ROBIN_Splitter_2281);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2282CombineDFT_2154);
	FOR(int, __iter_init_14_, 0, <, 8, __iter_init_14_++)
		init_buffer_float(&SplitJoin114_CombineDFT_Fiss_2408_2429_join[__iter_init_14_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2306WEIGHTED_ROUND_ROBIN_Splitter_2323);
	FOR(int, __iter_init_15_, 0, <, 4, __iter_init_15_++)
		init_buffer_float(&SplitJoin116_CombineDFT_Fiss_2409_2430_split[__iter_init_15_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2176WEIGHTED_ROUND_ROBIN_Splitter_2167);
	FOR(int, __iter_init_16_, 0, <, 28, __iter_init_16_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_2397_2418_join[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 16, __iter_init_17_++)
		init_buffer_float(&SplitJoin112_CombineDFT_Fiss_2407_2428_split[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 8, __iter_init_18_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_2395_2416_split[__iter_init_18_]);
	ENDFOR
	FOR(int, __iter_init_19_, 0, <, 2, __iter_init_19_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_2401_2422_split[__iter_init_19_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2388CombineDFT_2165);
	FOR(int, __iter_init_20_, 0, <, 16, __iter_init_20_++)
		init_buffer_float(&SplitJoin108_FFTReorderSimple_Fiss_2405_2426_join[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 2, __iter_init_21_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_2401_2422_join[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 2, __iter_init_22_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_2393_2414_split[__iter_init_22_]);
	ENDFOR
	FOR(int, __iter_init_23_, 0, <, 2, __iter_init_23_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_2391_2412_split[__iter_init_23_]);
	ENDFOR
	FOR(int, __iter_init_24_, 0, <, 4, __iter_init_24_++)
		init_buffer_float(&SplitJoin116_CombineDFT_Fiss_2409_2430_join[__iter_init_24_]);
	ENDFOR
	FOR(int, __iter_init_25_, 0, <, 2, __iter_init_25_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_2391_2412_join[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 16, __iter_init_26_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_2396_2417_join[__iter_init_26_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2324WEIGHTED_ROUND_ROBIN_Splitter_2353);
	FOR(int, __iter_init_27_, 0, <, 8, __iter_init_27_++)
		init_buffer_float(&SplitJoin114_CombineDFT_Fiss_2408_2429_split[__iter_init_27_]);
	ENDFOR
	FOR(int, __iter_init_28_, 0, <, 16, __iter_init_28_++)
		init_buffer_float(&SplitJoin112_CombineDFT_Fiss_2407_2428_join[__iter_init_28_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2190WEIGHTED_ROUND_ROBIN_Splitter_2199);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2200WEIGHTED_ROUND_ROBIN_Splitter_2217);
	FOR(int, __iter_init_29_, 0, <, 8, __iter_init_29_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_2399_2420_join[__iter_init_29_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2354WEIGHTED_ROUND_ROBIN_Splitter_2371);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2266WEIGHTED_ROUND_ROBIN_Splitter_2275);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2168FloatPrinter_2166);
	FOR(int, __iter_init_30_, 0, <, 4, __iter_init_30_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_2400_2421_join[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 8, __iter_init_31_++)
		init_buffer_float(&SplitJoin106_FFTReorderSimple_Fiss_2404_2425_join[__iter_init_31_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2248WEIGHTED_ROUND_ROBIN_Splitter_2265);
	init_buffer_float(&FFTReorderSimple_2155WEIGHTED_ROUND_ROBIN_Splitter_2285);
	FOR(int, __iter_init_32_, 0, <, 8, __iter_init_32_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_2395_2416_join[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 28, __iter_init_33_++)
		init_buffer_float(&SplitJoin110_CombineDFT_Fiss_2406_2427_join[__iter_init_33_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2290WEIGHTED_ROUND_ROBIN_Splitter_2295);
	FOR(int, __iter_init_34_, 0, <, 2, __iter_init_34_++)
		init_buffer_float(&SplitJoin118_CombineDFT_Fiss_2410_2431_split[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 4, __iter_init_35_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_2400_2421_split[__iter_init_35_]);
	ENDFOR
	FOR(int, __iter_init_36_, 0, <, 16, __iter_init_36_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_2396_2417_split[__iter_init_36_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2286WEIGHTED_ROUND_ROBIN_Splitter_2289);
	FOR(int, __iter_init_37_, 0, <, 8, __iter_init_37_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_2399_2420_split[__iter_init_37_]);
	ENDFOR
	FOR(int, __iter_init_38_, 0, <, 4, __iter_init_38_++)
		init_buffer_float(&SplitJoin104_FFTReorderSimple_Fiss_2403_2424_join[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 4, __iter_init_39_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_2394_2415_split[__iter_init_39_]);
	ENDFOR
// --- init: CombineDFT_2219
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2219_s.w[i] = real ; 
		CombineDFT_2219_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2220
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2220_s.w[i] = real ; 
		CombineDFT_2220_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2221
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2221_s.w[i] = real ; 
		CombineDFT_2221_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2222
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2222_s.w[i] = real ; 
		CombineDFT_2222_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2223
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2223_s.w[i] = real ; 
		CombineDFT_2223_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2224
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2224_s.w[i] = real ; 
		CombineDFT_2224_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2225
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2225_s.w[i] = real ; 
		CombineDFT_2225_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2226
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2226_s.w[i] = real ; 
		CombineDFT_2226_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2227
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2227_s.w[i] = real ; 
		CombineDFT_2227_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2228
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2228_s.w[i] = real ; 
		CombineDFT_2228_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2229
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2229_s.w[i] = real ; 
		CombineDFT_2229_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2230
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2230_s.w[i] = real ; 
		CombineDFT_2230_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2231
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2231_s.w[i] = real ; 
		CombineDFT_2231_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2232
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2232_s.w[i] = real ; 
		CombineDFT_2232_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2233
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2233_s.w[i] = real ; 
		CombineDFT_2233_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2234
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2234_s.w[i] = real ; 
		CombineDFT_2234_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2235
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2235_s.w[i] = real ; 
		CombineDFT_2235_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2236
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2236_s.w[i] = real ; 
		CombineDFT_2236_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2237
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2237_s.w[i] = real ; 
		CombineDFT_2237_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2238
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2238_s.w[i] = real ; 
		CombineDFT_2238_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2239
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2239_s.w[i] = real ; 
		CombineDFT_2239_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2240
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2240_s.w[i] = real ; 
		CombineDFT_2240_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2241
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2241_s.w[i] = real ; 
		CombineDFT_2241_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2242
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2242_s.w[i] = real ; 
		CombineDFT_2242_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2243
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2243_s.w[i] = real ; 
		CombineDFT_2243_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2244
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2244_s.w[i] = real ; 
		CombineDFT_2244_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2245
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2245_s.w[i] = real ; 
		CombineDFT_2245_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2246
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2246_s.w[i] = real ; 
		CombineDFT_2246_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2249
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2249_s.w[i] = real ; 
		CombineDFT_2249_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2250
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2250_s.w[i] = real ; 
		CombineDFT_2250_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2251
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2251_s.w[i] = real ; 
		CombineDFT_2251_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2252
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2252_s.w[i] = real ; 
		CombineDFT_2252_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2253
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2253_s.w[i] = real ; 
		CombineDFT_2253_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2254
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2254_s.w[i] = real ; 
		CombineDFT_2254_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2255
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2255_s.w[i] = real ; 
		CombineDFT_2255_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2256
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2256_s.w[i] = real ; 
		CombineDFT_2256_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2257
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2257_s.w[i] = real ; 
		CombineDFT_2257_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2258
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2258_s.w[i] = real ; 
		CombineDFT_2258_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2259
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2259_s.w[i] = real ; 
		CombineDFT_2259_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2260
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2260_s.w[i] = real ; 
		CombineDFT_2260_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2261
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2261_s.w[i] = real ; 
		CombineDFT_2261_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2262
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2262_s.w[i] = real ; 
		CombineDFT_2262_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2263
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2263_s.w[i] = real ; 
		CombineDFT_2263_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2264
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2264_s.w[i] = real ; 
		CombineDFT_2264_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2267
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2267_s.w[i] = real ; 
		CombineDFT_2267_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2268
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2268_s.w[i] = real ; 
		CombineDFT_2268_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2269
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2269_s.w[i] = real ; 
		CombineDFT_2269_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2270
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2270_s.w[i] = real ; 
		CombineDFT_2270_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2271
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2271_s.w[i] = real ; 
		CombineDFT_2271_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2272
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2272_s.w[i] = real ; 
		CombineDFT_2272_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2273
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2273_s.w[i] = real ; 
		CombineDFT_2273_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2274
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2274_s.w[i] = real ; 
		CombineDFT_2274_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2277
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_2277_s.w[i] = real ; 
		CombineDFT_2277_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2278
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_2278_s.w[i] = real ; 
		CombineDFT_2278_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2279
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_2279_s.w[i] = real ; 
		CombineDFT_2279_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2280
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_2280_s.w[i] = real ; 
		CombineDFT_2280_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2283
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_2283_s.w[i] = real ; 
		CombineDFT_2283_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2284
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_2284_s.w[i] = real ; 
		CombineDFT_2284_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2154
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_2154_s.w[i] = real ; 
		CombineDFT_2154_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2325
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2325_s.w[i] = real ; 
		CombineDFT_2325_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2326
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2326_s.w[i] = real ; 
		CombineDFT_2326_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2327
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2327_s.w[i] = real ; 
		CombineDFT_2327_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2328
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2328_s.w[i] = real ; 
		CombineDFT_2328_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2329
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2329_s.w[i] = real ; 
		CombineDFT_2329_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2330
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2330_s.w[i] = real ; 
		CombineDFT_2330_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2331
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2331_s.w[i] = real ; 
		CombineDFT_2331_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2332
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2332_s.w[i] = real ; 
		CombineDFT_2332_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2333
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2333_s.w[i] = real ; 
		CombineDFT_2333_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2334
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2334_s.w[i] = real ; 
		CombineDFT_2334_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2335
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2335_s.w[i] = real ; 
		CombineDFT_2335_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2336
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2336_s.w[i] = real ; 
		CombineDFT_2336_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2337
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2337_s.w[i] = real ; 
		CombineDFT_2337_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2338
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2338_s.w[i] = real ; 
		CombineDFT_2338_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2339
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2339_s.w[i] = real ; 
		CombineDFT_2339_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2340
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2340_s.w[i] = real ; 
		CombineDFT_2340_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2341
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2341_s.w[i] = real ; 
		CombineDFT_2341_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2342
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2342_s.w[i] = real ; 
		CombineDFT_2342_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2343
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2343_s.w[i] = real ; 
		CombineDFT_2343_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2344
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2344_s.w[i] = real ; 
		CombineDFT_2344_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2345
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2345_s.w[i] = real ; 
		CombineDFT_2345_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2346
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2346_s.w[i] = real ; 
		CombineDFT_2346_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2347
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2347_s.w[i] = real ; 
		CombineDFT_2347_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2348
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2348_s.w[i] = real ; 
		CombineDFT_2348_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2349
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2349_s.w[i] = real ; 
		CombineDFT_2349_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2350
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2350_s.w[i] = real ; 
		CombineDFT_2350_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2351
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2351_s.w[i] = real ; 
		CombineDFT_2351_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2352
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2352_s.w[i] = real ; 
		CombineDFT_2352_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2355
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2355_s.w[i] = real ; 
		CombineDFT_2355_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2356
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2356_s.w[i] = real ; 
		CombineDFT_2356_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2357
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2357_s.w[i] = real ; 
		CombineDFT_2357_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2358
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2358_s.w[i] = real ; 
		CombineDFT_2358_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2359
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2359_s.w[i] = real ; 
		CombineDFT_2359_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2360
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2360_s.w[i] = real ; 
		CombineDFT_2360_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2361
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2361_s.w[i] = real ; 
		CombineDFT_2361_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2362
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2362_s.w[i] = real ; 
		CombineDFT_2362_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2363
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2363_s.w[i] = real ; 
		CombineDFT_2363_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2364
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2364_s.w[i] = real ; 
		CombineDFT_2364_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2365
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2365_s.w[i] = real ; 
		CombineDFT_2365_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2366
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2366_s.w[i] = real ; 
		CombineDFT_2366_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2367
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2367_s.w[i] = real ; 
		CombineDFT_2367_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2368
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2368_s.w[i] = real ; 
		CombineDFT_2368_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2369
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2369_s.w[i] = real ; 
		CombineDFT_2369_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2370
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2370_s.w[i] = real ; 
		CombineDFT_2370_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2373
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2373_s.w[i] = real ; 
		CombineDFT_2373_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2374
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2374_s.w[i] = real ; 
		CombineDFT_2374_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2375
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2375_s.w[i] = real ; 
		CombineDFT_2375_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2376
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2376_s.w[i] = real ; 
		CombineDFT_2376_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2377
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2377_s.w[i] = real ; 
		CombineDFT_2377_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2378
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2378_s.w[i] = real ; 
		CombineDFT_2378_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2379
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2379_s.w[i] = real ; 
		CombineDFT_2379_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2380
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2380_s.w[i] = real ; 
		CombineDFT_2380_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2383
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_2383_s.w[i] = real ; 
		CombineDFT_2383_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2384
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_2384_s.w[i] = real ; 
		CombineDFT_2384_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2385
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_2385_s.w[i] = real ; 
		CombineDFT_2385_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2386
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_2386_s.w[i] = real ; 
		CombineDFT_2386_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2389
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_2389_s.w[i] = real ; 
		CombineDFT_2389_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2390
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_2390_s.w[i] = real ; 
		CombineDFT_2390_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2165
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_2165_s.w[i] = real ; 
		CombineDFT_2165_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_2175();
			FFTTestSource_2177();
			FFTTestSource_2178();
		WEIGHTED_ROUND_ROBIN_Joiner_2176();
		WEIGHTED_ROUND_ROBIN_Splitter_2167();
			FFTReorderSimple_2144();
			WEIGHTED_ROUND_ROBIN_Splitter_2179();
				FFTReorderSimple_2181();
				FFTReorderSimple_2182();
			WEIGHTED_ROUND_ROBIN_Joiner_2180();
			WEIGHTED_ROUND_ROBIN_Splitter_2183();
				FFTReorderSimple_2185();
				FFTReorderSimple_2186();
				FFTReorderSimple_2187();
				FFTReorderSimple_2188();
			WEIGHTED_ROUND_ROBIN_Joiner_2184();
			WEIGHTED_ROUND_ROBIN_Splitter_2189();
				FFTReorderSimple_2191();
				FFTReorderSimple_2192();
				FFTReorderSimple_2193();
				FFTReorderSimple_2194();
				FFTReorderSimple_2195();
				FFTReorderSimple_2196();
				FFTReorderSimple_2197();
				FFTReorderSimple_2198();
			WEIGHTED_ROUND_ROBIN_Joiner_2190();
			WEIGHTED_ROUND_ROBIN_Splitter_2199();
				FFTReorderSimple_2201();
				FFTReorderSimple_2202();
				FFTReorderSimple_2203();
				FFTReorderSimple_2204();
				FFTReorderSimple_2205();
				FFTReorderSimple_2206();
				FFTReorderSimple_2207();
				FFTReorderSimple_2208();
				FFTReorderSimple_2209();
				FFTReorderSimple_2210();
				FFTReorderSimple_2211();
				FFTReorderSimple_2212();
				FFTReorderSimple_2213();
				FFTReorderSimple_2214();
				FFTReorderSimple_2215();
				FFTReorderSimple_2216();
			WEIGHTED_ROUND_ROBIN_Joiner_2200();
			WEIGHTED_ROUND_ROBIN_Splitter_2217();
				CombineDFT_2219();
				CombineDFT_2220();
				CombineDFT_2221();
				CombineDFT_2222();
				CombineDFT_2223();
				CombineDFT_2224();
				CombineDFT_2225();
				CombineDFT_2226();
				CombineDFT_2227();
				CombineDFT_2228();
				CombineDFT_2229();
				CombineDFT_2230();
				CombineDFT_2231();
				CombineDFT_2232();
				CombineDFT_2233();
				CombineDFT_2234();
				CombineDFT_2235();
				CombineDFT_2236();
				CombineDFT_2237();
				CombineDFT_2238();
				CombineDFT_2239();
				CombineDFT_2240();
				CombineDFT_2241();
				CombineDFT_2242();
				CombineDFT_2243();
				CombineDFT_2244();
				CombineDFT_2245();
				CombineDFT_2246();
			WEIGHTED_ROUND_ROBIN_Joiner_2218();
			WEIGHTED_ROUND_ROBIN_Splitter_2247();
				CombineDFT_2249();
				CombineDFT_2250();
				CombineDFT_2251();
				CombineDFT_2252();
				CombineDFT_2253();
				CombineDFT_2254();
				CombineDFT_2255();
				CombineDFT_2256();
				CombineDFT_2257();
				CombineDFT_2258();
				CombineDFT_2259();
				CombineDFT_2260();
				CombineDFT_2261();
				CombineDFT_2262();
				CombineDFT_2263();
				CombineDFT_2264();
			WEIGHTED_ROUND_ROBIN_Joiner_2248();
			WEIGHTED_ROUND_ROBIN_Splitter_2265();
				CombineDFT_2267();
				CombineDFT_2268();
				CombineDFT_2269();
				CombineDFT_2270();
				CombineDFT_2271();
				CombineDFT_2272();
				CombineDFT_2273();
				CombineDFT_2274();
			WEIGHTED_ROUND_ROBIN_Joiner_2266();
			WEIGHTED_ROUND_ROBIN_Splitter_2275();
				CombineDFT_2277();
				CombineDFT_2278();
				CombineDFT_2279();
				CombineDFT_2280();
			WEIGHTED_ROUND_ROBIN_Joiner_2276();
			WEIGHTED_ROUND_ROBIN_Splitter_2281();
				CombineDFT_2283();
				CombineDFT_2284();
			WEIGHTED_ROUND_ROBIN_Joiner_2282();
			CombineDFT_2154();
			FFTReorderSimple_2155();
			WEIGHTED_ROUND_ROBIN_Splitter_2285();
				FFTReorderSimple_2287();
				FFTReorderSimple_2288();
			WEIGHTED_ROUND_ROBIN_Joiner_2286();
			WEIGHTED_ROUND_ROBIN_Splitter_2289();
				FFTReorderSimple_2291();
				FFTReorderSimple_2292();
				FFTReorderSimple_2293();
				FFTReorderSimple_2294();
			WEIGHTED_ROUND_ROBIN_Joiner_2290();
			WEIGHTED_ROUND_ROBIN_Splitter_2295();
				FFTReorderSimple_2297();
				FFTReorderSimple_2298();
				FFTReorderSimple_2299();
				FFTReorderSimple_2300();
				FFTReorderSimple_2301();
				FFTReorderSimple_2302();
				FFTReorderSimple_2303();
				FFTReorderSimple_2304();
			WEIGHTED_ROUND_ROBIN_Joiner_2296();
			WEIGHTED_ROUND_ROBIN_Splitter_2305();
				FFTReorderSimple_2307();
				FFTReorderSimple_2308();
				FFTReorderSimple_2309();
				FFTReorderSimple_2310();
				FFTReorderSimple_2311();
				FFTReorderSimple_2312();
				FFTReorderSimple_2313();
				FFTReorderSimple_2314();
				FFTReorderSimple_2315();
				FFTReorderSimple_2316();
				FFTReorderSimple_2317();
				FFTReorderSimple_2318();
				FFTReorderSimple_2319();
				FFTReorderSimple_2320();
				FFTReorderSimple_2321();
				FFTReorderSimple_2322();
			WEIGHTED_ROUND_ROBIN_Joiner_2306();
			WEIGHTED_ROUND_ROBIN_Splitter_2323();
				CombineDFT_2325();
				CombineDFT_2326();
				CombineDFT_2327();
				CombineDFT_2328();
				CombineDFT_2329();
				CombineDFT_2330();
				CombineDFT_2331();
				CombineDFT_2332();
				CombineDFT_2333();
				CombineDFT_2334();
				CombineDFT_2335();
				CombineDFT_2336();
				CombineDFT_2337();
				CombineDFT_2338();
				CombineDFT_2339();
				CombineDFT_2340();
				CombineDFT_2341();
				CombineDFT_2342();
				CombineDFT_2343();
				CombineDFT_2344();
				CombineDFT_2345();
				CombineDFT_2346();
				CombineDFT_2347();
				CombineDFT_2348();
				CombineDFT_2349();
				CombineDFT_2350();
				CombineDFT_2351();
				CombineDFT_2352();
			WEIGHTED_ROUND_ROBIN_Joiner_2324();
			WEIGHTED_ROUND_ROBIN_Splitter_2353();
				CombineDFT_2355();
				CombineDFT_2356();
				CombineDFT_2357();
				CombineDFT_2358();
				CombineDFT_2359();
				CombineDFT_2360();
				CombineDFT_2361();
				CombineDFT_2362();
				CombineDFT_2363();
				CombineDFT_2364();
				CombineDFT_2365();
				CombineDFT_2366();
				CombineDFT_2367();
				CombineDFT_2368();
				CombineDFT_2369();
				CombineDFT_2370();
			WEIGHTED_ROUND_ROBIN_Joiner_2354();
			WEIGHTED_ROUND_ROBIN_Splitter_2371();
				CombineDFT_2373();
				CombineDFT_2374();
				CombineDFT_2375();
				CombineDFT_2376();
				CombineDFT_2377();
				CombineDFT_2378();
				CombineDFT_2379();
				CombineDFT_2380();
			WEIGHTED_ROUND_ROBIN_Joiner_2372();
			WEIGHTED_ROUND_ROBIN_Splitter_2381();
				CombineDFT_2383();
				CombineDFT_2384();
				CombineDFT_2385();
				CombineDFT_2386();
			WEIGHTED_ROUND_ROBIN_Joiner_2382();
			WEIGHTED_ROUND_ROBIN_Splitter_2387();
				CombineDFT_2389();
				CombineDFT_2390();
			WEIGHTED_ROUND_ROBIN_Joiner_2388();
			CombineDFT_2165();
		WEIGHTED_ROUND_ROBIN_Joiner_2168();
		FloatPrinter_2166();
	ENDFOR
	return EXIT_SUCCESS;
}
