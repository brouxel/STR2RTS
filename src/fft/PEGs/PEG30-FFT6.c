#include "PEG30-FFT6.h"

buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_570WEIGHTED_ROUND_ROBIN_Splitter_573;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_590WEIGHTED_ROUND_ROBIN_Splitter_607;
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_680_690_split[16];
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_678_688_join[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_574WEIGHTED_ROUND_ROBIN_Splitter_579;
buffer_complex_t SplitJoin12_CombineDFT_Fiss_683_693_split[8];
buffer_complex_t CombineDFT_566CPrinter_567;
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_677_687_join[2];
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_680_690_join[16];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_608WEIGHTED_ROUND_ROBIN_Splitter_639;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_580WEIGHTED_ROUND_ROBIN_Splitter_589;
buffer_complex_t SplitJoin12_CombineDFT_Fiss_683_693_join[8];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_658WEIGHTED_ROUND_ROBIN_Splitter_667;
buffer_complex_t SplitJoin16_CombineDFT_Fiss_685_695_split[2];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_684_694_join[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_674CombineDFT_566;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_668WEIGHTED_ROUND_ROBIN_Splitter_673;
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_678_688_split[4];
buffer_complex_t SplitJoin8_CombineDFT_Fiss_681_691_split[30];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_684_694_split[4];
buffer_complex_t FFTReorderSimple_556WEIGHTED_ROUND_ROBIN_Splitter_569;
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_679_689_split[8];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_640WEIGHTED_ROUND_ROBIN_Splitter_657;
buffer_complex_t SplitJoin10_CombineDFT_Fiss_682_692_split[16];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_685_695_join[2];
buffer_complex_t FFTTestSource_555FFTReorderSimple_556;
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_679_689_join[8];
buffer_complex_t SplitJoin8_CombineDFT_Fiss_681_691_join[30];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_682_692_join[16];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_677_687_split[2];


CombineDFT_609_t CombineDFT_609_s;
CombineDFT_609_t CombineDFT_610_s;
CombineDFT_609_t CombineDFT_611_s;
CombineDFT_609_t CombineDFT_612_s;
CombineDFT_609_t CombineDFT_613_s;
CombineDFT_609_t CombineDFT_614_s;
CombineDFT_609_t CombineDFT_615_s;
CombineDFT_609_t CombineDFT_616_s;
CombineDFT_609_t CombineDFT_617_s;
CombineDFT_609_t CombineDFT_618_s;
CombineDFT_609_t CombineDFT_619_s;
CombineDFT_609_t CombineDFT_620_s;
CombineDFT_609_t CombineDFT_621_s;
CombineDFT_609_t CombineDFT_622_s;
CombineDFT_609_t CombineDFT_623_s;
CombineDFT_609_t CombineDFT_624_s;
CombineDFT_609_t CombineDFT_625_s;
CombineDFT_609_t CombineDFT_626_s;
CombineDFT_609_t CombineDFT_627_s;
CombineDFT_609_t CombineDFT_628_s;
CombineDFT_609_t CombineDFT_629_s;
CombineDFT_609_t CombineDFT_630_s;
CombineDFT_609_t CombineDFT_631_s;
CombineDFT_609_t CombineDFT_632_s;
CombineDFT_609_t CombineDFT_633_s;
CombineDFT_609_t CombineDFT_634_s;
CombineDFT_609_t CombineDFT_635_s;
CombineDFT_609_t CombineDFT_636_s;
CombineDFT_609_t CombineDFT_637_s;
CombineDFT_609_t CombineDFT_638_s;
CombineDFT_609_t CombineDFT_641_s;
CombineDFT_609_t CombineDFT_642_s;
CombineDFT_609_t CombineDFT_643_s;
CombineDFT_609_t CombineDFT_644_s;
CombineDFT_609_t CombineDFT_645_s;
CombineDFT_609_t CombineDFT_646_s;
CombineDFT_609_t CombineDFT_647_s;
CombineDFT_609_t CombineDFT_648_s;
CombineDFT_609_t CombineDFT_649_s;
CombineDFT_609_t CombineDFT_650_s;
CombineDFT_609_t CombineDFT_651_s;
CombineDFT_609_t CombineDFT_652_s;
CombineDFT_609_t CombineDFT_653_s;
CombineDFT_609_t CombineDFT_654_s;
CombineDFT_609_t CombineDFT_655_s;
CombineDFT_609_t CombineDFT_656_s;
CombineDFT_609_t CombineDFT_659_s;
CombineDFT_609_t CombineDFT_660_s;
CombineDFT_609_t CombineDFT_661_s;
CombineDFT_609_t CombineDFT_662_s;
CombineDFT_609_t CombineDFT_663_s;
CombineDFT_609_t CombineDFT_664_s;
CombineDFT_609_t CombineDFT_665_s;
CombineDFT_609_t CombineDFT_666_s;
CombineDFT_609_t CombineDFT_669_s;
CombineDFT_609_t CombineDFT_670_s;
CombineDFT_609_t CombineDFT_671_s;
CombineDFT_609_t CombineDFT_672_s;
CombineDFT_609_t CombineDFT_675_s;
CombineDFT_609_t CombineDFT_676_s;
CombineDFT_609_t CombineDFT_566_s;

void FFTTestSource(buffer_complex_t *chanout) {
		complex_t c1;
		complex_t zero;
		c1.real = 1.0 ; 
		c1.imag = 0.0 ; 
		zero.real = 0.0 ; 
		zero.imag = 0.0 ; 
		push_complex(&(*chanout), zero) ; 
		push_complex(&(*chanout), c1) ; 
		FOR(int, i, 0,  < , 62, i++) {
			push_complex(&(*chanout), zero) ; 
		}
		ENDFOR
	}


void FFTTestSource_555() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTTestSource(&(FFTTestSource_555FFTReorderSimple_556));
	ENDFOR
}

void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 64, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&(*chanin)) ; 
		}
		ENDFOR
	}


void FFTReorderSimple_556() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(FFTTestSource_555FFTReorderSimple_556), &(FFTReorderSimple_556WEIGHTED_ROUND_ROBIN_Splitter_569));
	ENDFOR
}

void FFTReorderSimple_571() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_677_687_split[0]), &(SplitJoin0_FFTReorderSimple_Fiss_677_687_join[0]));
	ENDFOR
}

void FFTReorderSimple_572() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_677_687_split[1]), &(SplitJoin0_FFTReorderSimple_Fiss_677_687_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_569() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_677_687_split[0], pop_complex(&FFTReorderSimple_556WEIGHTED_ROUND_ROBIN_Splitter_569));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_677_687_split[1], pop_complex(&FFTReorderSimple_556WEIGHTED_ROUND_ROBIN_Splitter_569));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_570() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_570WEIGHTED_ROUND_ROBIN_Splitter_573, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_677_687_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_570WEIGHTED_ROUND_ROBIN_Splitter_573, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_677_687_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_575() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_678_688_split[0]), &(SplitJoin2_FFTReorderSimple_Fiss_678_688_join[0]));
	ENDFOR
}

void FFTReorderSimple_576() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_678_688_split[1]), &(SplitJoin2_FFTReorderSimple_Fiss_678_688_join[1]));
	ENDFOR
}

void FFTReorderSimple_577() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_678_688_split[2]), &(SplitJoin2_FFTReorderSimple_Fiss_678_688_join[2]));
	ENDFOR
}

void FFTReorderSimple_578() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_678_688_split[3]), &(SplitJoin2_FFTReorderSimple_Fiss_678_688_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_573() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin2_FFTReorderSimple_Fiss_678_688_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_570WEIGHTED_ROUND_ROBIN_Splitter_573));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_574() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_574WEIGHTED_ROUND_ROBIN_Splitter_579, pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_678_688_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_581() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_679_689_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_679_689_join[0]));
	ENDFOR
}

void FFTReorderSimple_582() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_679_689_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_679_689_join[1]));
	ENDFOR
}

void FFTReorderSimple_583() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_679_689_split[2]), &(SplitJoin4_FFTReorderSimple_Fiss_679_689_join[2]));
	ENDFOR
}

void FFTReorderSimple_584() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_679_689_split[3]), &(SplitJoin4_FFTReorderSimple_Fiss_679_689_join[3]));
	ENDFOR
}

void FFTReorderSimple_585() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_679_689_split[4]), &(SplitJoin4_FFTReorderSimple_Fiss_679_689_join[4]));
	ENDFOR
}

void FFTReorderSimple_586() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_679_689_split[5]), &(SplitJoin4_FFTReorderSimple_Fiss_679_689_join[5]));
	ENDFOR
}

void FFTReorderSimple_587() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_679_689_split[6]), &(SplitJoin4_FFTReorderSimple_Fiss_679_689_join[6]));
	ENDFOR
}

void FFTReorderSimple_588() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_679_689_split[7]), &(SplitJoin4_FFTReorderSimple_Fiss_679_689_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_579() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin4_FFTReorderSimple_Fiss_679_689_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_574WEIGHTED_ROUND_ROBIN_Splitter_579));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_580() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_580WEIGHTED_ROUND_ROBIN_Splitter_589, pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_679_689_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_591() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_680_690_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_680_690_join[0]));
	ENDFOR
}

void FFTReorderSimple_592() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_680_690_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_680_690_join[1]));
	ENDFOR
}

void FFTReorderSimple_593() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_680_690_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_680_690_join[2]));
	ENDFOR
}

void FFTReorderSimple_594() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_680_690_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_680_690_join[3]));
	ENDFOR
}

void FFTReorderSimple_595() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_680_690_split[4]), &(SplitJoin6_FFTReorderSimple_Fiss_680_690_join[4]));
	ENDFOR
}

void FFTReorderSimple_596() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_680_690_split[5]), &(SplitJoin6_FFTReorderSimple_Fiss_680_690_join[5]));
	ENDFOR
}

void FFTReorderSimple_597() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_680_690_split[6]), &(SplitJoin6_FFTReorderSimple_Fiss_680_690_join[6]));
	ENDFOR
}

void FFTReorderSimple_598() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_680_690_split[7]), &(SplitJoin6_FFTReorderSimple_Fiss_680_690_join[7]));
	ENDFOR
}

void FFTReorderSimple_599() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_680_690_split[8]), &(SplitJoin6_FFTReorderSimple_Fiss_680_690_join[8]));
	ENDFOR
}

void FFTReorderSimple_600() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_680_690_split[9]), &(SplitJoin6_FFTReorderSimple_Fiss_680_690_join[9]));
	ENDFOR
}

void FFTReorderSimple_601() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_680_690_split[10]), &(SplitJoin6_FFTReorderSimple_Fiss_680_690_join[10]));
	ENDFOR
}

void FFTReorderSimple_602() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_680_690_split[11]), &(SplitJoin6_FFTReorderSimple_Fiss_680_690_join[11]));
	ENDFOR
}

void FFTReorderSimple_603() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_680_690_split[12]), &(SplitJoin6_FFTReorderSimple_Fiss_680_690_join[12]));
	ENDFOR
}

void FFTReorderSimple_604() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_680_690_split[13]), &(SplitJoin6_FFTReorderSimple_Fiss_680_690_join[13]));
	ENDFOR
}

void FFTReorderSimple_605() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_680_690_split[14]), &(SplitJoin6_FFTReorderSimple_Fiss_680_690_join[14]));
	ENDFOR
}

void FFTReorderSimple_606() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_680_690_split[15]), &(SplitJoin6_FFTReorderSimple_Fiss_680_690_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_589() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin6_FFTReorderSimple_Fiss_680_690_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_580WEIGHTED_ROUND_ROBIN_Splitter_589));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_590() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_590WEIGHTED_ROUND_ROBIN_Splitter_607, pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_680_690_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&(*chanin), (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_609_s.wn.real) - (w.imag * CombineDFT_609_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_609_s.wn.imag) + (w.imag * CombineDFT_609_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&(*chanin)) ; 
			push_complex(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_609() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_681_691_split[0]), &(SplitJoin8_CombineDFT_Fiss_681_691_join[0]));
	ENDFOR
}

void CombineDFT_610() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_681_691_split[1]), &(SplitJoin8_CombineDFT_Fiss_681_691_join[1]));
	ENDFOR
}

void CombineDFT_611() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_681_691_split[2]), &(SplitJoin8_CombineDFT_Fiss_681_691_join[2]));
	ENDFOR
}

void CombineDFT_612() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_681_691_split[3]), &(SplitJoin8_CombineDFT_Fiss_681_691_join[3]));
	ENDFOR
}

void CombineDFT_613() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_681_691_split[4]), &(SplitJoin8_CombineDFT_Fiss_681_691_join[4]));
	ENDFOR
}

void CombineDFT_614() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_681_691_split[5]), &(SplitJoin8_CombineDFT_Fiss_681_691_join[5]));
	ENDFOR
}

void CombineDFT_615() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_681_691_split[6]), &(SplitJoin8_CombineDFT_Fiss_681_691_join[6]));
	ENDFOR
}

void CombineDFT_616() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_681_691_split[7]), &(SplitJoin8_CombineDFT_Fiss_681_691_join[7]));
	ENDFOR
}

void CombineDFT_617() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_681_691_split[8]), &(SplitJoin8_CombineDFT_Fiss_681_691_join[8]));
	ENDFOR
}

void CombineDFT_618() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_681_691_split[9]), &(SplitJoin8_CombineDFT_Fiss_681_691_join[9]));
	ENDFOR
}

void CombineDFT_619() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_681_691_split[10]), &(SplitJoin8_CombineDFT_Fiss_681_691_join[10]));
	ENDFOR
}

void CombineDFT_620() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_681_691_split[11]), &(SplitJoin8_CombineDFT_Fiss_681_691_join[11]));
	ENDFOR
}

void CombineDFT_621() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_681_691_split[12]), &(SplitJoin8_CombineDFT_Fiss_681_691_join[12]));
	ENDFOR
}

void CombineDFT_622() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_681_691_split[13]), &(SplitJoin8_CombineDFT_Fiss_681_691_join[13]));
	ENDFOR
}

void CombineDFT_623() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_681_691_split[14]), &(SplitJoin8_CombineDFT_Fiss_681_691_join[14]));
	ENDFOR
}

void CombineDFT_624() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_681_691_split[15]), &(SplitJoin8_CombineDFT_Fiss_681_691_join[15]));
	ENDFOR
}

void CombineDFT_625() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_681_691_split[16]), &(SplitJoin8_CombineDFT_Fiss_681_691_join[16]));
	ENDFOR
}

void CombineDFT_626() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_681_691_split[17]), &(SplitJoin8_CombineDFT_Fiss_681_691_join[17]));
	ENDFOR
}

void CombineDFT_627() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_681_691_split[18]), &(SplitJoin8_CombineDFT_Fiss_681_691_join[18]));
	ENDFOR
}

void CombineDFT_628() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_681_691_split[19]), &(SplitJoin8_CombineDFT_Fiss_681_691_join[19]));
	ENDFOR
}

void CombineDFT_629() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_681_691_split[20]), &(SplitJoin8_CombineDFT_Fiss_681_691_join[20]));
	ENDFOR
}

void CombineDFT_630() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_681_691_split[21]), &(SplitJoin8_CombineDFT_Fiss_681_691_join[21]));
	ENDFOR
}

void CombineDFT_631() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_681_691_split[22]), &(SplitJoin8_CombineDFT_Fiss_681_691_join[22]));
	ENDFOR
}

void CombineDFT_632() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_681_691_split[23]), &(SplitJoin8_CombineDFT_Fiss_681_691_join[23]));
	ENDFOR
}

void CombineDFT_633() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_681_691_split[24]), &(SplitJoin8_CombineDFT_Fiss_681_691_join[24]));
	ENDFOR
}

void CombineDFT_634() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_681_691_split[25]), &(SplitJoin8_CombineDFT_Fiss_681_691_join[25]));
	ENDFOR
}

void CombineDFT_635() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_681_691_split[26]), &(SplitJoin8_CombineDFT_Fiss_681_691_join[26]));
	ENDFOR
}

void CombineDFT_636() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_681_691_split[27]), &(SplitJoin8_CombineDFT_Fiss_681_691_join[27]));
	ENDFOR
}

void CombineDFT_637() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_681_691_split[28]), &(SplitJoin8_CombineDFT_Fiss_681_691_join[28]));
	ENDFOR
}

void CombineDFT_638() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_681_691_split[29]), &(SplitJoin8_CombineDFT_Fiss_681_691_join[29]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_607() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 30, __iter_++)
			push_complex(&SplitJoin8_CombineDFT_Fiss_681_691_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_590WEIGHTED_ROUND_ROBIN_Splitter_607));
			push_complex(&SplitJoin8_CombineDFT_Fiss_681_691_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_590WEIGHTED_ROUND_ROBIN_Splitter_607));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_608() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 30, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_608WEIGHTED_ROUND_ROBIN_Splitter_639, pop_complex(&SplitJoin8_CombineDFT_Fiss_681_691_join[__iter_]));
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_608WEIGHTED_ROUND_ROBIN_Splitter_639, pop_complex(&SplitJoin8_CombineDFT_Fiss_681_691_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_641() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_682_692_split[0]), &(SplitJoin10_CombineDFT_Fiss_682_692_join[0]));
	ENDFOR
}

void CombineDFT_642() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_682_692_split[1]), &(SplitJoin10_CombineDFT_Fiss_682_692_join[1]));
	ENDFOR
}

void CombineDFT_643() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_682_692_split[2]), &(SplitJoin10_CombineDFT_Fiss_682_692_join[2]));
	ENDFOR
}

void CombineDFT_644() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_682_692_split[3]), &(SplitJoin10_CombineDFT_Fiss_682_692_join[3]));
	ENDFOR
}

void CombineDFT_645() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_682_692_split[4]), &(SplitJoin10_CombineDFT_Fiss_682_692_join[4]));
	ENDFOR
}

void CombineDFT_646() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_682_692_split[5]), &(SplitJoin10_CombineDFT_Fiss_682_692_join[5]));
	ENDFOR
}

void CombineDFT_647() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_682_692_split[6]), &(SplitJoin10_CombineDFT_Fiss_682_692_join[6]));
	ENDFOR
}

void CombineDFT_648() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_682_692_split[7]), &(SplitJoin10_CombineDFT_Fiss_682_692_join[7]));
	ENDFOR
}

void CombineDFT_649() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_682_692_split[8]), &(SplitJoin10_CombineDFT_Fiss_682_692_join[8]));
	ENDFOR
}

void CombineDFT_650() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_682_692_split[9]), &(SplitJoin10_CombineDFT_Fiss_682_692_join[9]));
	ENDFOR
}

void CombineDFT_651() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_682_692_split[10]), &(SplitJoin10_CombineDFT_Fiss_682_692_join[10]));
	ENDFOR
}

void CombineDFT_652() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_682_692_split[11]), &(SplitJoin10_CombineDFT_Fiss_682_692_join[11]));
	ENDFOR
}

void CombineDFT_653() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_682_692_split[12]), &(SplitJoin10_CombineDFT_Fiss_682_692_join[12]));
	ENDFOR
}

void CombineDFT_654() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_682_692_split[13]), &(SplitJoin10_CombineDFT_Fiss_682_692_join[13]));
	ENDFOR
}

void CombineDFT_655() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_682_692_split[14]), &(SplitJoin10_CombineDFT_Fiss_682_692_join[14]));
	ENDFOR
}

void CombineDFT_656() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_682_692_split[15]), &(SplitJoin10_CombineDFT_Fiss_682_692_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_639() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin10_CombineDFT_Fiss_682_692_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_608WEIGHTED_ROUND_ROBIN_Splitter_639));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_640() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_640WEIGHTED_ROUND_ROBIN_Splitter_657, pop_complex(&SplitJoin10_CombineDFT_Fiss_682_692_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_659() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_683_693_split[0]), &(SplitJoin12_CombineDFT_Fiss_683_693_join[0]));
	ENDFOR
}

void CombineDFT_660() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_683_693_split[1]), &(SplitJoin12_CombineDFT_Fiss_683_693_join[1]));
	ENDFOR
}

void CombineDFT_661() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_683_693_split[2]), &(SplitJoin12_CombineDFT_Fiss_683_693_join[2]));
	ENDFOR
}

void CombineDFT_662() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_683_693_split[3]), &(SplitJoin12_CombineDFT_Fiss_683_693_join[3]));
	ENDFOR
}

void CombineDFT_663() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_683_693_split[4]), &(SplitJoin12_CombineDFT_Fiss_683_693_join[4]));
	ENDFOR
}

void CombineDFT_664() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_683_693_split[5]), &(SplitJoin12_CombineDFT_Fiss_683_693_join[5]));
	ENDFOR
}

void CombineDFT_665() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_683_693_split[6]), &(SplitJoin12_CombineDFT_Fiss_683_693_join[6]));
	ENDFOR
}

void CombineDFT_666() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_683_693_split[7]), &(SplitJoin12_CombineDFT_Fiss_683_693_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_657() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin12_CombineDFT_Fiss_683_693_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_640WEIGHTED_ROUND_ROBIN_Splitter_657));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_658() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_658WEIGHTED_ROUND_ROBIN_Splitter_667, pop_complex(&SplitJoin12_CombineDFT_Fiss_683_693_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_669() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_684_694_split[0]), &(SplitJoin14_CombineDFT_Fiss_684_694_join[0]));
	ENDFOR
}

void CombineDFT_670() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_684_694_split[1]), &(SplitJoin14_CombineDFT_Fiss_684_694_join[1]));
	ENDFOR
}

void CombineDFT_671() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_684_694_split[2]), &(SplitJoin14_CombineDFT_Fiss_684_694_join[2]));
	ENDFOR
}

void CombineDFT_672() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_684_694_split[3]), &(SplitJoin14_CombineDFT_Fiss_684_694_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_667() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin14_CombineDFT_Fiss_684_694_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_658WEIGHTED_ROUND_ROBIN_Splitter_667));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_668() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_668WEIGHTED_ROUND_ROBIN_Splitter_673, pop_complex(&SplitJoin14_CombineDFT_Fiss_684_694_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_675() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_685_695_split[0]), &(SplitJoin16_CombineDFT_Fiss_685_695_join[0]));
	ENDFOR
}

void CombineDFT_676() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_685_695_split[1]), &(SplitJoin16_CombineDFT_Fiss_685_695_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_673() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_685_695_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_668WEIGHTED_ROUND_ROBIN_Splitter_673));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_685_695_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_668WEIGHTED_ROUND_ROBIN_Splitter_673));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_674() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_674CombineDFT_566, pop_complex(&SplitJoin16_CombineDFT_Fiss_685_695_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_674CombineDFT_566, pop_complex(&SplitJoin16_CombineDFT_Fiss_685_695_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_566() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_674CombineDFT_566), &(CombineDFT_566CPrinter_567));
	ENDFOR
}

void CPrinter(buffer_complex_t *chanin) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		printf("%.10f", c.real);
		printf("\n");
		printf("%.10f", c.imag);
		printf("\n");
	}


void CPrinter_567() {
	FOR(uint32_t, __iter_steady_, 0, <, 960, __iter_steady_++)
		CPrinter(&(CombineDFT_566CPrinter_567));
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_570WEIGHTED_ROUND_ROBIN_Splitter_573);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_590WEIGHTED_ROUND_ROBIN_Splitter_607);
	FOR(int, __iter_init_0_, 0, <, 16, __iter_init_0_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_680_690_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 4, __iter_init_1_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_678_688_join[__iter_init_1_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_574WEIGHTED_ROUND_ROBIN_Splitter_579);
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_683_693_split[__iter_init_2_]);
	ENDFOR
	init_buffer_complex(&CombineDFT_566CPrinter_567);
	FOR(int, __iter_init_3_, 0, <, 2, __iter_init_3_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_677_687_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 16, __iter_init_4_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_680_690_join[__iter_init_4_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_608WEIGHTED_ROUND_ROBIN_Splitter_639);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_580WEIGHTED_ROUND_ROBIN_Splitter_589);
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_683_693_join[__iter_init_5_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_658WEIGHTED_ROUND_ROBIN_Splitter_667);
	FOR(int, __iter_init_6_, 0, <, 2, __iter_init_6_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_685_695_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 4, __iter_init_7_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_684_694_join[__iter_init_7_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_674CombineDFT_566);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_668WEIGHTED_ROUND_ROBIN_Splitter_673);
	FOR(int, __iter_init_8_, 0, <, 4, __iter_init_8_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_678_688_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 30, __iter_init_9_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_681_691_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 4, __iter_init_10_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_684_694_split[__iter_init_10_]);
	ENDFOR
	init_buffer_complex(&FFTReorderSimple_556WEIGHTED_ROUND_ROBIN_Splitter_569);
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_679_689_split[__iter_init_11_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_640WEIGHTED_ROUND_ROBIN_Splitter_657);
	FOR(int, __iter_init_12_, 0, <, 16, __iter_init_12_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_682_692_split[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_685_695_join[__iter_init_13_]);
	ENDFOR
	init_buffer_complex(&FFTTestSource_555FFTReorderSimple_556);
	FOR(int, __iter_init_14_, 0, <, 8, __iter_init_14_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_679_689_join[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 30, __iter_init_15_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_681_691_join[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 16, __iter_init_16_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_682_692_join[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_677_687_split[__iter_init_17_]);
	ENDFOR
// --- init: CombineDFT_609
	 {
	 ; 
	CombineDFT_609_s.wn.real = -1.0 ; 
	CombineDFT_609_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_610
	 {
	CombineDFT_610_s.wn.real = -1.0 ; 
	CombineDFT_610_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_611
	 {
	CombineDFT_611_s.wn.real = -1.0 ; 
	CombineDFT_611_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_612
	 {
	CombineDFT_612_s.wn.real = -1.0 ; 
	CombineDFT_612_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_613
	 {
	CombineDFT_613_s.wn.real = -1.0 ; 
	CombineDFT_613_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_614
	 {
	CombineDFT_614_s.wn.real = -1.0 ; 
	CombineDFT_614_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_615
	 {
	CombineDFT_615_s.wn.real = -1.0 ; 
	CombineDFT_615_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_616
	 {
	CombineDFT_616_s.wn.real = -1.0 ; 
	CombineDFT_616_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_617
	 {
	CombineDFT_617_s.wn.real = -1.0 ; 
	CombineDFT_617_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_618
	 {
	CombineDFT_618_s.wn.real = -1.0 ; 
	CombineDFT_618_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_619
	 {
	CombineDFT_619_s.wn.real = -1.0 ; 
	CombineDFT_619_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_620
	 {
	CombineDFT_620_s.wn.real = -1.0 ; 
	CombineDFT_620_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_621
	 {
	CombineDFT_621_s.wn.real = -1.0 ; 
	CombineDFT_621_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_622
	 {
	CombineDFT_622_s.wn.real = -1.0 ; 
	CombineDFT_622_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_623
	 {
	CombineDFT_623_s.wn.real = -1.0 ; 
	CombineDFT_623_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_624
	 {
	CombineDFT_624_s.wn.real = -1.0 ; 
	CombineDFT_624_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_625
	 {
	CombineDFT_625_s.wn.real = -1.0 ; 
	CombineDFT_625_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_626
	 {
	CombineDFT_626_s.wn.real = -1.0 ; 
	CombineDFT_626_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_627
	 {
	CombineDFT_627_s.wn.real = -1.0 ; 
	CombineDFT_627_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_628
	 {
	CombineDFT_628_s.wn.real = -1.0 ; 
	CombineDFT_628_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_629
	 {
	CombineDFT_629_s.wn.real = -1.0 ; 
	CombineDFT_629_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_630
	 {
	CombineDFT_630_s.wn.real = -1.0 ; 
	CombineDFT_630_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_631
	 {
	CombineDFT_631_s.wn.real = -1.0 ; 
	CombineDFT_631_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_632
	 {
	CombineDFT_632_s.wn.real = -1.0 ; 
	CombineDFT_632_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_633
	 {
	CombineDFT_633_s.wn.real = -1.0 ; 
	CombineDFT_633_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_634
	 {
	CombineDFT_634_s.wn.real = -1.0 ; 
	CombineDFT_634_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_635
	 {
	CombineDFT_635_s.wn.real = -1.0 ; 
	CombineDFT_635_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_636
	 {
	CombineDFT_636_s.wn.real = -1.0 ; 
	CombineDFT_636_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_637
	 {
	CombineDFT_637_s.wn.real = -1.0 ; 
	CombineDFT_637_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_638
	 {
	CombineDFT_638_s.wn.real = -1.0 ; 
	CombineDFT_638_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_641
	 {
	CombineDFT_641_s.wn.real = -4.371139E-8 ; 
	CombineDFT_641_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_642
	 {
	CombineDFT_642_s.wn.real = -4.371139E-8 ; 
	CombineDFT_642_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_643
	 {
	CombineDFT_643_s.wn.real = -4.371139E-8 ; 
	CombineDFT_643_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_644
	 {
	CombineDFT_644_s.wn.real = -4.371139E-8 ; 
	CombineDFT_644_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_645
	 {
	CombineDFT_645_s.wn.real = -4.371139E-8 ; 
	CombineDFT_645_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_646
	 {
	CombineDFT_646_s.wn.real = -4.371139E-8 ; 
	CombineDFT_646_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_647
	 {
	CombineDFT_647_s.wn.real = -4.371139E-8 ; 
	CombineDFT_647_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_648
	 {
	CombineDFT_648_s.wn.real = -4.371139E-8 ; 
	CombineDFT_648_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_649
	 {
	CombineDFT_649_s.wn.real = -4.371139E-8 ; 
	CombineDFT_649_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_650
	 {
	CombineDFT_650_s.wn.real = -4.371139E-8 ; 
	CombineDFT_650_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_651
	 {
	CombineDFT_651_s.wn.real = -4.371139E-8 ; 
	CombineDFT_651_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_652
	 {
	CombineDFT_652_s.wn.real = -4.371139E-8 ; 
	CombineDFT_652_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_653
	 {
	CombineDFT_653_s.wn.real = -4.371139E-8 ; 
	CombineDFT_653_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_654
	 {
	CombineDFT_654_s.wn.real = -4.371139E-8 ; 
	CombineDFT_654_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_655
	 {
	CombineDFT_655_s.wn.real = -4.371139E-8 ; 
	CombineDFT_655_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_656
	 {
	CombineDFT_656_s.wn.real = -4.371139E-8 ; 
	CombineDFT_656_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_659
	 {
	CombineDFT_659_s.wn.real = 0.70710677 ; 
	CombineDFT_659_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_660
	 {
	CombineDFT_660_s.wn.real = 0.70710677 ; 
	CombineDFT_660_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_661
	 {
	CombineDFT_661_s.wn.real = 0.70710677 ; 
	CombineDFT_661_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_662
	 {
	CombineDFT_662_s.wn.real = 0.70710677 ; 
	CombineDFT_662_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_663
	 {
	CombineDFT_663_s.wn.real = 0.70710677 ; 
	CombineDFT_663_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_664
	 {
	CombineDFT_664_s.wn.real = 0.70710677 ; 
	CombineDFT_664_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_665
	 {
	CombineDFT_665_s.wn.real = 0.70710677 ; 
	CombineDFT_665_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_666
	 {
	CombineDFT_666_s.wn.real = 0.70710677 ; 
	CombineDFT_666_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_669
	 {
	CombineDFT_669_s.wn.real = 0.9238795 ; 
	CombineDFT_669_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_670
	 {
	CombineDFT_670_s.wn.real = 0.9238795 ; 
	CombineDFT_670_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_671
	 {
	CombineDFT_671_s.wn.real = 0.9238795 ; 
	CombineDFT_671_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_672
	 {
	CombineDFT_672_s.wn.real = 0.9238795 ; 
	CombineDFT_672_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_675
	 {
	CombineDFT_675_s.wn.real = 0.98078525 ; 
	CombineDFT_675_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_676
	 {
	CombineDFT_676_s.wn.real = 0.98078525 ; 
	CombineDFT_676_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_566
	 {
	 ; 
	CombineDFT_566_s.wn.real = 0.9951847 ; 
	CombineDFT_566_s.wn.imag = -0.09801714 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FFTTestSource_555();
		FFTReorderSimple_556();
		WEIGHTED_ROUND_ROBIN_Splitter_569();
			FFTReorderSimple_571();
			FFTReorderSimple_572();
		WEIGHTED_ROUND_ROBIN_Joiner_570();
		WEIGHTED_ROUND_ROBIN_Splitter_573();
			FFTReorderSimple_575();
			FFTReorderSimple_576();
			FFTReorderSimple_577();
			FFTReorderSimple_578();
		WEIGHTED_ROUND_ROBIN_Joiner_574();
		WEIGHTED_ROUND_ROBIN_Splitter_579();
			FFTReorderSimple_581();
			FFTReorderSimple_582();
			FFTReorderSimple_583();
			FFTReorderSimple_584();
			FFTReorderSimple_585();
			FFTReorderSimple_586();
			FFTReorderSimple_587();
			FFTReorderSimple_588();
		WEIGHTED_ROUND_ROBIN_Joiner_580();
		WEIGHTED_ROUND_ROBIN_Splitter_589();
			FFTReorderSimple_591();
			FFTReorderSimple_592();
			FFTReorderSimple_593();
			FFTReorderSimple_594();
			FFTReorderSimple_595();
			FFTReorderSimple_596();
			FFTReorderSimple_597();
			FFTReorderSimple_598();
			FFTReorderSimple_599();
			FFTReorderSimple_600();
			FFTReorderSimple_601();
			FFTReorderSimple_602();
			FFTReorderSimple_603();
			FFTReorderSimple_604();
			FFTReorderSimple_605();
			FFTReorderSimple_606();
		WEIGHTED_ROUND_ROBIN_Joiner_590();
		WEIGHTED_ROUND_ROBIN_Splitter_607();
			CombineDFT_609();
			CombineDFT_610();
			CombineDFT_611();
			CombineDFT_612();
			CombineDFT_613();
			CombineDFT_614();
			CombineDFT_615();
			CombineDFT_616();
			CombineDFT_617();
			CombineDFT_618();
			CombineDFT_619();
			CombineDFT_620();
			CombineDFT_621();
			CombineDFT_622();
			CombineDFT_623();
			CombineDFT_624();
			CombineDFT_625();
			CombineDFT_626();
			CombineDFT_627();
			CombineDFT_628();
			CombineDFT_629();
			CombineDFT_630();
			CombineDFT_631();
			CombineDFT_632();
			CombineDFT_633();
			CombineDFT_634();
			CombineDFT_635();
			CombineDFT_636();
			CombineDFT_637();
			CombineDFT_638();
		WEIGHTED_ROUND_ROBIN_Joiner_608();
		WEIGHTED_ROUND_ROBIN_Splitter_639();
			CombineDFT_641();
			CombineDFT_642();
			CombineDFT_643();
			CombineDFT_644();
			CombineDFT_645();
			CombineDFT_646();
			CombineDFT_647();
			CombineDFT_648();
			CombineDFT_649();
			CombineDFT_650();
			CombineDFT_651();
			CombineDFT_652();
			CombineDFT_653();
			CombineDFT_654();
			CombineDFT_655();
			CombineDFT_656();
		WEIGHTED_ROUND_ROBIN_Joiner_640();
		WEIGHTED_ROUND_ROBIN_Splitter_657();
			CombineDFT_659();
			CombineDFT_660();
			CombineDFT_661();
			CombineDFT_662();
			CombineDFT_663();
			CombineDFT_664();
			CombineDFT_665();
			CombineDFT_666();
		WEIGHTED_ROUND_ROBIN_Joiner_658();
		WEIGHTED_ROUND_ROBIN_Splitter_667();
			CombineDFT_669();
			CombineDFT_670();
			CombineDFT_671();
			CombineDFT_672();
		WEIGHTED_ROUND_ROBIN_Joiner_668();
		WEIGHTED_ROUND_ROBIN_Splitter_673();
			CombineDFT_675();
			CombineDFT_676();
		WEIGHTED_ROUND_ROBIN_Joiner_674();
		CombineDFT_566();
		CPrinter_567();
	ENDFOR
	return EXIT_SUCCESS;
}
