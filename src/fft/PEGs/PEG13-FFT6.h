#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=1664 on the compile command line
#else
#if BUF_SIZEMAX < 1664
#error BUF_SIZEMAX too small, it must be at least 1664
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	complex_t wn;
} CombineDFT_4334_t;
void FFTTestSource(buffer_complex_t *chanout);
void FFTTestSource_4283();
void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout);
void FFTReorderSimple_4284();
void WEIGHTED_ROUND_ROBIN_Splitter_4297();
void FFTReorderSimple_4299();
void FFTReorderSimple_4300();
void WEIGHTED_ROUND_ROBIN_Joiner_4298();
void WEIGHTED_ROUND_ROBIN_Splitter_4301();
void FFTReorderSimple_4303();
void FFTReorderSimple_4304();
void FFTReorderSimple_4305();
void FFTReorderSimple_4306();
void WEIGHTED_ROUND_ROBIN_Joiner_4302();
void WEIGHTED_ROUND_ROBIN_Splitter_4307();
void FFTReorderSimple_4309();
void FFTReorderSimple_4310();
void FFTReorderSimple_4311();
void FFTReorderSimple_4312();
void FFTReorderSimple_4313();
void FFTReorderSimple_4314();
void FFTReorderSimple_4315();
void FFTReorderSimple_4316();
void WEIGHTED_ROUND_ROBIN_Joiner_4308();
void WEIGHTED_ROUND_ROBIN_Splitter_4317();
void FFTReorderSimple_4319();
void FFTReorderSimple_4320();
void FFTReorderSimple_4321();
void FFTReorderSimple_4322();
void FFTReorderSimple_4323();
void FFTReorderSimple_4324();
void FFTReorderSimple_4325();
void FFTReorderSimple_4326();
void FFTReorderSimple_4327();
void FFTReorderSimple_4328();
void FFTReorderSimple_4329();
void FFTReorderSimple_4330();
void FFTReorderSimple_4331();
void WEIGHTED_ROUND_ROBIN_Joiner_4318();
void WEIGHTED_ROUND_ROBIN_Splitter_4332();
void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout);
void CombineDFT_4334();
void CombineDFT_4335();
void CombineDFT_4336();
void CombineDFT_4337();
void CombineDFT_4338();
void CombineDFT_4339();
void CombineDFT_4340();
void CombineDFT_4341();
void CombineDFT_4342();
void CombineDFT_4343();
void CombineDFT_4344();
void CombineDFT_4345();
void CombineDFT_4346();
void WEIGHTED_ROUND_ROBIN_Joiner_4333();
void WEIGHTED_ROUND_ROBIN_Splitter_4347();
void CombineDFT_4349();
void CombineDFT_4350();
void CombineDFT_4351();
void CombineDFT_4352();
void CombineDFT_4353();
void CombineDFT_4354();
void CombineDFT_4355();
void CombineDFT_4356();
void CombineDFT_4357();
void CombineDFT_4358();
void CombineDFT_4359();
void CombineDFT_4360();
void CombineDFT_4361();
void WEIGHTED_ROUND_ROBIN_Joiner_4348();
void WEIGHTED_ROUND_ROBIN_Splitter_4362();
void CombineDFT_4364();
void CombineDFT_4365();
void CombineDFT_4366();
void CombineDFT_4367();
void CombineDFT_4368();
void CombineDFT_4369();
void CombineDFT_4370();
void CombineDFT_4371();
void WEIGHTED_ROUND_ROBIN_Joiner_4363();
void WEIGHTED_ROUND_ROBIN_Splitter_4372();
void CombineDFT_4374();
void CombineDFT_4375();
void CombineDFT_4376();
void CombineDFT_4377();
void WEIGHTED_ROUND_ROBIN_Joiner_4373();
void WEIGHTED_ROUND_ROBIN_Splitter_4378();
void CombineDFT_4380();
void CombineDFT_4381();
void WEIGHTED_ROUND_ROBIN_Joiner_4379();
void CombineDFT_4294();
void CPrinter(buffer_complex_t *chanin);
void CPrinter_4295();

#ifdef __cplusplus
}
#endif
#endif
