#include "PEG8-FFT5_nocache.h"

buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_6458WEIGHTED_ROUND_ROBIN_Splitter_6576;
buffer_complex_t SplitJoin16_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child0_6556_6625_join[2];
buffer_complex_t SplitJoin100_SplitJoin81_SplitJoin81_AnonFilter_a0_6293_6524_6603_6620_split[2];
buffer_complex_t SplitJoin96_SplitJoin77_SplitJoin77_AnonFilter_a0_6287_6521_6602_6619_join[2];
buffer_complex_t SplitJoin96_SplitJoin77_SplitJoin77_AnonFilter_a0_6287_6521_6602_6619_split[2];
buffer_complex_t SplitJoin20_SplitJoin14_SplitJoin14_split1_6229_6473_6592_6631_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_6398butterfly_6301;
buffer_complex_t SplitJoin14_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_Hier_6591_6624_split[2];
buffer_complex_t butterfly_6305Post_CollapsedDataParallel_2_6411;
buffer_complex_t SplitJoin10_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_Hier_6590_6621_split[2];
buffer_complex_t butterfly_6306Post_CollapsedDataParallel_2_6414;
buffer_complex_t SplitJoin90_SplitJoin71_SplitJoin71_AnonFilter_a0_6279_6517_6600_6617_join[2];
buffer_complex_t SplitJoin37_SplitJoin22_SplitJoin22_split2_6233_6479_6557_6633_split[4];
buffer_complex_t SplitJoin20_SplitJoin14_SplitJoin14_split1_6229_6473_6592_6631_split[2];
buffer_complex_t SplitJoin37_SplitJoin22_SplitJoin22_split2_6233_6479_6557_6633_join[4];
buffer_complex_t SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_6243_6466_6589_6609_split[2];
buffer_complex_t SplitJoin78_SplitJoin59_SplitJoin59_AnonFilter_a0_6263_6509_6597_6613_split[2];
buffer_complex_t Pre_CollapsedDataParallel_1_6404butterfly_6303;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_6416WEIGHTED_ROUND_ROBIN_Splitter_6559;
buffer_complex_t SplitJoin84_SplitJoin65_SplitJoin65_AnonFilter_a0_6271_6513_6598_6615_join[2];
buffer_complex_t SplitJoin94_SplitJoin75_SplitJoin75_AnonFilter_a0_6285_6520_6601_6618_split[2];
buffer_complex_t SplitJoin54_SplitJoin37_SplitJoin37_split2_6224_6491_6554_6630_join[2];
buffer_complex_t SplitJoin50_SplitJoin33_SplitJoin33_split2_6222_6488_6552_6629_join[2];
buffer_complex_t SplitJoin12_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child0_6550_6622_split[4];
buffer_complex_t SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6241_6465_6588_6608_join[2];
buffer_complex_t SplitJoin0_source_Fiss_6586_6605_split[2];
buffer_complex_t SplitJoin90_SplitJoin71_SplitJoin71_AnonFilter_a0_6279_6517_6600_6617_split[2];
buffer_complex_t SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6239_6464_6547_6607_split[2];
buffer_complex_t SplitJoin10_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_Hier_6590_6621_join[2];
buffer_complex_t SplitJoin78_SplitJoin59_SplitJoin59_AnonFilter_a0_6263_6509_6597_6613_join[2];
buffer_complex_t SplitJoin74_SplitJoin55_SplitJoin55_AnonFilter_a0_6257_6506_6596_6612_split[2];
buffer_complex_t SplitJoin18_SplitJoin12_SplitJoin12_split2_6218_6471_6549_6626_split[2];
buffer_complex_t SplitJoin44_SplitJoin29_SplitJoin29_split2_6220_6485_6551_6627_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_6564WEIGHTED_ROUND_ROBIN_Splitter_6565;
buffer_complex_t SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_6243_6466_6589_6609_join[2];
buffer_complex_t butterfly_6302Post_CollapsedDataParallel_2_6402;
buffer_complex_t SplitJoin94_SplitJoin75_SplitJoin75_AnonFilter_a0_6285_6520_6601_6618_join[2];
buffer_complex_t SplitJoin82_SplitJoin63_SplitJoin63_AnonFilter_a0_6269_6512_6548_6614_split[2];
buffer_complex_t butterfly_6304Post_CollapsedDataParallel_2_6408;
buffer_complex_t SplitJoin24_magnitude_Fiss_6593_6634_split[8];
buffer_complex_t SplitJoin18_SplitJoin12_SplitJoin12_split2_6218_6471_6549_6626_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_6395butterfly_6300;
buffer_complex_t SplitJoin72_SplitJoin53_SplitJoin53_AnonFilter_a0_6255_6505_6595_6611_split[2];
buffer_complex_t Pre_CollapsedDataParallel_1_6401butterfly_6302;
buffer_complex_t SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6237_6463_6587_6606_split[2];
buffer_complex_t SplitJoin86_SplitJoin67_SplitJoin67_AnonFilter_a0_6273_6514_6599_6616_split[2];
buffer_complex_t SplitJoin48_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child1_6558_6628_split[2];
buffer_complex_t SplitJoin84_SplitJoin65_SplitJoin65_AnonFilter_a0_6271_6513_6598_6615_split[2];
buffer_complex_t butterfly_6300Post_CollapsedDataParallel_2_6396;
buffer_complex_t SplitJoin14_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_Hier_6591_6624_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6577sink_6326;
buffer_complex_t butterfly_6301Post_CollapsedDataParallel_2_6399;
buffer_complex_t SplitJoin50_SplitJoin33_SplitJoin33_split2_6222_6488_6552_6629_split[2];
buffer_complex_t SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6241_6465_6588_6608_split[2];
buffer_complex_t Pre_CollapsedDataParallel_1_6410butterfly_6305;
buffer_complex_t SplitJoin44_SplitJoin29_SplitJoin29_split2_6220_6485_6551_6627_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_6570WEIGHTED_ROUND_ROBIN_Splitter_6457;
buffer_complex_t SplitJoin61_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child1_6553_6623_join[4];
buffer_complex_t SplitJoin100_SplitJoin81_SplitJoin81_AnonFilter_a0_6293_6524_6603_6620_join[2];
buffer_complex_t SplitJoin12_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child0_6550_6622_join[4];
buffer_complex_t SplitJoin86_SplitJoin67_SplitJoin67_AnonFilter_a0_6273_6514_6599_6616_join[2];
buffer_complex_t SplitJoin48_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child1_6558_6628_join[2];
buffer_complex_t SplitJoin68_SplitJoin49_SplitJoin49_AnonFilter_a0_6249_6502_6594_6610_split[2];
buffer_complex_t SplitJoin74_SplitJoin55_SplitJoin55_AnonFilter_a0_6257_6506_6596_6612_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_6573WEIGHTED_ROUND_ROBIN_Splitter_6415;
buffer_complex_t Pre_CollapsedDataParallel_1_6392butterfly_6299;
buffer_complex_t SplitJoin0_source_Fiss_6586_6605_join[2];
buffer_complex_t butterfly_6299Post_CollapsedDataParallel_2_6393;
buffer_complex_t SplitJoin72_SplitJoin53_SplitJoin53_AnonFilter_a0_6255_6505_6595_6611_join[2];
buffer_float_t SplitJoin24_magnitude_Fiss_6593_6634_join[8];
buffer_complex_t SplitJoin22_SplitJoin16_SplitJoin16_split2_6231_6474_6555_6632_join[4];
buffer_complex_t SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6239_6464_6547_6607_join[2];
buffer_complex_t SplitJoin82_SplitJoin63_SplitJoin63_AnonFilter_a0_6269_6512_6548_6614_join[2];
buffer_complex_t SplitJoin61_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child1_6553_6623_split[4];
buffer_complex_t Pre_CollapsedDataParallel_1_6407butterfly_6304;
buffer_complex_t SplitJoin54_SplitJoin37_SplitJoin37_split2_6224_6491_6554_6630_split[2];
buffer_complex_t SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6237_6463_6587_6606_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_6413butterfly_6306;
buffer_complex_t SplitJoin16_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child0_6556_6625_split[2];
buffer_complex_t SplitJoin22_SplitJoin16_SplitJoin16_split2_6231_6474_6555_6632_split[4];
buffer_complex_t SplitJoin68_SplitJoin49_SplitJoin49_AnonFilter_a0_6249_6502_6594_6610_join[2];
buffer_complex_t butterfly_6303Post_CollapsedDataParallel_2_6405;



void source_6574() {
	complex_t t;
	t.imag = 0.0 ; 
	t.real = 0.9501 ; 
	push_complex(&SplitJoin0_source_Fiss_6586_6605_join[0], t) ; 
	t.real = 0.2311 ; 
	push_complex(&SplitJoin0_source_Fiss_6586_6605_join[0], t) ; 
	t.real = 0.6068 ; 
	push_complex(&SplitJoin0_source_Fiss_6586_6605_join[0], t) ; 
	t.real = 0.486 ; 
	push_complex(&SplitJoin0_source_Fiss_6586_6605_join[0], t) ; 
	t.real = 0.8913 ; 
	push_complex(&SplitJoin0_source_Fiss_6586_6605_join[0], t) ; 
	t.real = 0.7621 ; 
	push_complex(&SplitJoin0_source_Fiss_6586_6605_join[0], t) ; 
	t.real = 0.4565 ; 
	push_complex(&SplitJoin0_source_Fiss_6586_6605_join[0], t) ; 
	t.real = 0.0185 ; 
	push_complex(&SplitJoin0_source_Fiss_6586_6605_join[0], t) ; 
}


void source_6575() {
	complex_t t;
	t.imag = 0.0 ; 
	t.real = 0.9501 ; 
	push_complex(&SplitJoin0_source_Fiss_6586_6605_join[1], t) ; 
	t.real = 0.2311 ; 
	push_complex(&SplitJoin0_source_Fiss_6586_6605_join[1], t) ; 
	t.real = 0.6068 ; 
	push_complex(&SplitJoin0_source_Fiss_6586_6605_join[1], t) ; 
	t.real = 0.486 ; 
	push_complex(&SplitJoin0_source_Fiss_6586_6605_join[1], t) ; 
	t.real = 0.8913 ; 
	push_complex(&SplitJoin0_source_Fiss_6586_6605_join[1], t) ; 
	t.real = 0.7621 ; 
	push_complex(&SplitJoin0_source_Fiss_6586_6605_join[1], t) ; 
	t.real = 0.4565 ; 
	push_complex(&SplitJoin0_source_Fiss_6586_6605_join[1], t) ; 
	t.real = 0.0185 ; 
	push_complex(&SplitJoin0_source_Fiss_6586_6605_join[1], t) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_6572() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_6573() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6573WEIGHTED_ROUND_ROBIN_Splitter_6415, pop_complex(&SplitJoin0_source_Fiss_6586_6605_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6573WEIGHTED_ROUND_ROBIN_Splitter_6415, pop_complex(&SplitJoin0_source_Fiss_6586_6605_join[1]));
	ENDFOR
}

void Identity_6245() {
	complex_t __tmp1702 = pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_6243_6466_6589_6609_split[0]);
	push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_6243_6466_6589_6609_join[0], __tmp1702) ; 
}


void Identity_6247() {
	complex_t __tmp1705 = pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_6243_6466_6589_6609_split[1]);
	push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_6243_6466_6589_6609_join[1], __tmp1705) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_6421() {
	push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_6243_6466_6589_6609_split[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6241_6465_6588_6608_split[0]));
	push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_6243_6466_6589_6609_split[1], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6241_6465_6588_6608_split[0]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_6422() {
	push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6241_6465_6588_6608_join[0], pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_6243_6466_6589_6609_join[0]));
	push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6241_6465_6588_6608_join[0], pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_6243_6466_6589_6609_join[1]));
}

void Identity_6251() {
	complex_t __tmp1713 = pop_complex(&SplitJoin68_SplitJoin49_SplitJoin49_AnonFilter_a0_6249_6502_6594_6610_split[0]);
	push_complex(&SplitJoin68_SplitJoin49_SplitJoin49_AnonFilter_a0_6249_6502_6594_6610_join[0], __tmp1713) ; 
}


void Identity_6253() {
	complex_t __tmp1716 = pop_complex(&SplitJoin68_SplitJoin49_SplitJoin49_AnonFilter_a0_6249_6502_6594_6610_split[1]);
	push_complex(&SplitJoin68_SplitJoin49_SplitJoin49_AnonFilter_a0_6249_6502_6594_6610_join[1], __tmp1716) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_6423() {
	push_complex(&SplitJoin68_SplitJoin49_SplitJoin49_AnonFilter_a0_6249_6502_6594_6610_split[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6241_6465_6588_6608_split[1]));
	push_complex(&SplitJoin68_SplitJoin49_SplitJoin49_AnonFilter_a0_6249_6502_6594_6610_split[1], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6241_6465_6588_6608_split[1]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_6424() {
	push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6241_6465_6588_6608_join[1], pop_complex(&SplitJoin68_SplitJoin49_SplitJoin49_AnonFilter_a0_6249_6502_6594_6610_join[0]));
	push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6241_6465_6588_6608_join[1], pop_complex(&SplitJoin68_SplitJoin49_SplitJoin49_AnonFilter_a0_6249_6502_6594_6610_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_6419() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6241_6465_6588_6608_split[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6239_6464_6547_6607_split[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6241_6465_6588_6608_split[1], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6239_6464_6547_6607_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6420() {
	push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6239_6464_6547_6607_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6241_6465_6588_6608_join[0]));
	push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6239_6464_6547_6607_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6241_6465_6588_6608_join[0]));
	push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6239_6464_6547_6607_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6241_6465_6588_6608_join[1]));
	push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6239_6464_6547_6607_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6241_6465_6588_6608_join[1]));
}

void Identity_6259() {
	complex_t __tmp1729 = pop_complex(&SplitJoin74_SplitJoin55_SplitJoin55_AnonFilter_a0_6257_6506_6596_6612_split[0]);
	push_complex(&SplitJoin74_SplitJoin55_SplitJoin55_AnonFilter_a0_6257_6506_6596_6612_join[0], __tmp1729) ; 
}


void Identity_6261() {
	complex_t __tmp1732 = pop_complex(&SplitJoin74_SplitJoin55_SplitJoin55_AnonFilter_a0_6257_6506_6596_6612_split[1]);
	push_complex(&SplitJoin74_SplitJoin55_SplitJoin55_AnonFilter_a0_6257_6506_6596_6612_join[1], __tmp1732) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_6427() {
	push_complex(&SplitJoin74_SplitJoin55_SplitJoin55_AnonFilter_a0_6257_6506_6596_6612_split[0], pop_complex(&SplitJoin72_SplitJoin53_SplitJoin53_AnonFilter_a0_6255_6505_6595_6611_split[0]));
	push_complex(&SplitJoin74_SplitJoin55_SplitJoin55_AnonFilter_a0_6257_6506_6596_6612_split[1], pop_complex(&SplitJoin72_SplitJoin53_SplitJoin53_AnonFilter_a0_6255_6505_6595_6611_split[0]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_6428() {
	push_complex(&SplitJoin72_SplitJoin53_SplitJoin53_AnonFilter_a0_6255_6505_6595_6611_join[0], pop_complex(&SplitJoin74_SplitJoin55_SplitJoin55_AnonFilter_a0_6257_6506_6596_6612_join[0]));
	push_complex(&SplitJoin72_SplitJoin53_SplitJoin53_AnonFilter_a0_6255_6505_6595_6611_join[0], pop_complex(&SplitJoin74_SplitJoin55_SplitJoin55_AnonFilter_a0_6257_6506_6596_6612_join[1]));
}

void Identity_6265() {
	complex_t __tmp1740 = pop_complex(&SplitJoin78_SplitJoin59_SplitJoin59_AnonFilter_a0_6263_6509_6597_6613_split[0]);
	push_complex(&SplitJoin78_SplitJoin59_SplitJoin59_AnonFilter_a0_6263_6509_6597_6613_join[0], __tmp1740) ; 
}


void Identity_6267() {
	complex_t __tmp1743 = pop_complex(&SplitJoin78_SplitJoin59_SplitJoin59_AnonFilter_a0_6263_6509_6597_6613_split[1]);
	push_complex(&SplitJoin78_SplitJoin59_SplitJoin59_AnonFilter_a0_6263_6509_6597_6613_join[1], __tmp1743) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_6429() {
	push_complex(&SplitJoin78_SplitJoin59_SplitJoin59_AnonFilter_a0_6263_6509_6597_6613_split[0], pop_complex(&SplitJoin72_SplitJoin53_SplitJoin53_AnonFilter_a0_6255_6505_6595_6611_split[1]));
	push_complex(&SplitJoin78_SplitJoin59_SplitJoin59_AnonFilter_a0_6263_6509_6597_6613_split[1], pop_complex(&SplitJoin72_SplitJoin53_SplitJoin53_AnonFilter_a0_6255_6505_6595_6611_split[1]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_6430() {
	push_complex(&SplitJoin72_SplitJoin53_SplitJoin53_AnonFilter_a0_6255_6505_6595_6611_join[1], pop_complex(&SplitJoin78_SplitJoin59_SplitJoin59_AnonFilter_a0_6263_6509_6597_6613_join[0]));
	push_complex(&SplitJoin72_SplitJoin53_SplitJoin53_AnonFilter_a0_6255_6505_6595_6611_join[1], pop_complex(&SplitJoin78_SplitJoin59_SplitJoin59_AnonFilter_a0_6263_6509_6597_6613_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_6425() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin72_SplitJoin53_SplitJoin53_AnonFilter_a0_6255_6505_6595_6611_split[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6239_6464_6547_6607_split[1]));
		push_complex(&SplitJoin72_SplitJoin53_SplitJoin53_AnonFilter_a0_6255_6505_6595_6611_split[1], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6239_6464_6547_6607_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6426() {
	push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6239_6464_6547_6607_join[1], pop_complex(&SplitJoin72_SplitJoin53_SplitJoin53_AnonFilter_a0_6255_6505_6595_6611_join[0]));
	push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6239_6464_6547_6607_join[1], pop_complex(&SplitJoin72_SplitJoin53_SplitJoin53_AnonFilter_a0_6255_6505_6595_6611_join[0]));
	push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6239_6464_6547_6607_join[1], pop_complex(&SplitJoin72_SplitJoin53_SplitJoin53_AnonFilter_a0_6255_6505_6595_6611_join[1]));
	push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6239_6464_6547_6607_join[1], pop_complex(&SplitJoin72_SplitJoin53_SplitJoin53_AnonFilter_a0_6255_6505_6595_6611_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_6417() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6239_6464_6547_6607_split[0], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6237_6463_6587_6606_split[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6239_6464_6547_6607_split[1], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6237_6463_6587_6606_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6418() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6237_6463_6587_6606_join[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6239_6464_6547_6607_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6237_6463_6587_6606_join[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6239_6464_6547_6607_join[1]));
	ENDFOR
}

void Identity_6275() {
	complex_t __tmp1761 = pop_complex(&SplitJoin86_SplitJoin67_SplitJoin67_AnonFilter_a0_6273_6514_6599_6616_split[0]);
	push_complex(&SplitJoin86_SplitJoin67_SplitJoin67_AnonFilter_a0_6273_6514_6599_6616_join[0], __tmp1761) ; 
}


void Identity_6277() {
	complex_t __tmp1764 = pop_complex(&SplitJoin86_SplitJoin67_SplitJoin67_AnonFilter_a0_6273_6514_6599_6616_split[1]);
	push_complex(&SplitJoin86_SplitJoin67_SplitJoin67_AnonFilter_a0_6273_6514_6599_6616_join[1], __tmp1764) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_6435() {
	push_complex(&SplitJoin86_SplitJoin67_SplitJoin67_AnonFilter_a0_6273_6514_6599_6616_split[0], pop_complex(&SplitJoin84_SplitJoin65_SplitJoin65_AnonFilter_a0_6271_6513_6598_6615_split[0]));
	push_complex(&SplitJoin86_SplitJoin67_SplitJoin67_AnonFilter_a0_6273_6514_6599_6616_split[1], pop_complex(&SplitJoin84_SplitJoin65_SplitJoin65_AnonFilter_a0_6271_6513_6598_6615_split[0]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_6436() {
	push_complex(&SplitJoin84_SplitJoin65_SplitJoin65_AnonFilter_a0_6271_6513_6598_6615_join[0], pop_complex(&SplitJoin86_SplitJoin67_SplitJoin67_AnonFilter_a0_6273_6514_6599_6616_join[0]));
	push_complex(&SplitJoin84_SplitJoin65_SplitJoin65_AnonFilter_a0_6271_6513_6598_6615_join[0], pop_complex(&SplitJoin86_SplitJoin67_SplitJoin67_AnonFilter_a0_6273_6514_6599_6616_join[1]));
}

void Identity_6281() {
	complex_t __tmp1772 = pop_complex(&SplitJoin90_SplitJoin71_SplitJoin71_AnonFilter_a0_6279_6517_6600_6617_split[0]);
	push_complex(&SplitJoin90_SplitJoin71_SplitJoin71_AnonFilter_a0_6279_6517_6600_6617_join[0], __tmp1772) ; 
}


void Identity_6283() {
	complex_t __tmp1775 = pop_complex(&SplitJoin90_SplitJoin71_SplitJoin71_AnonFilter_a0_6279_6517_6600_6617_split[1]);
	push_complex(&SplitJoin90_SplitJoin71_SplitJoin71_AnonFilter_a0_6279_6517_6600_6617_join[1], __tmp1775) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_6437() {
	push_complex(&SplitJoin90_SplitJoin71_SplitJoin71_AnonFilter_a0_6279_6517_6600_6617_split[0], pop_complex(&SplitJoin84_SplitJoin65_SplitJoin65_AnonFilter_a0_6271_6513_6598_6615_split[1]));
	push_complex(&SplitJoin90_SplitJoin71_SplitJoin71_AnonFilter_a0_6279_6517_6600_6617_split[1], pop_complex(&SplitJoin84_SplitJoin65_SplitJoin65_AnonFilter_a0_6271_6513_6598_6615_split[1]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_6438() {
	push_complex(&SplitJoin84_SplitJoin65_SplitJoin65_AnonFilter_a0_6271_6513_6598_6615_join[1], pop_complex(&SplitJoin90_SplitJoin71_SplitJoin71_AnonFilter_a0_6279_6517_6600_6617_join[0]));
	push_complex(&SplitJoin84_SplitJoin65_SplitJoin65_AnonFilter_a0_6271_6513_6598_6615_join[1], pop_complex(&SplitJoin90_SplitJoin71_SplitJoin71_AnonFilter_a0_6279_6517_6600_6617_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_6433() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin84_SplitJoin65_SplitJoin65_AnonFilter_a0_6271_6513_6598_6615_split[0], pop_complex(&SplitJoin82_SplitJoin63_SplitJoin63_AnonFilter_a0_6269_6512_6548_6614_split[0]));
		push_complex(&SplitJoin84_SplitJoin65_SplitJoin65_AnonFilter_a0_6271_6513_6598_6615_split[1], pop_complex(&SplitJoin82_SplitJoin63_SplitJoin63_AnonFilter_a0_6269_6512_6548_6614_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6434() {
	push_complex(&SplitJoin82_SplitJoin63_SplitJoin63_AnonFilter_a0_6269_6512_6548_6614_join[0], pop_complex(&SplitJoin84_SplitJoin65_SplitJoin65_AnonFilter_a0_6271_6513_6598_6615_join[0]));
	push_complex(&SplitJoin82_SplitJoin63_SplitJoin63_AnonFilter_a0_6269_6512_6548_6614_join[0], pop_complex(&SplitJoin84_SplitJoin65_SplitJoin65_AnonFilter_a0_6271_6513_6598_6615_join[0]));
	push_complex(&SplitJoin82_SplitJoin63_SplitJoin63_AnonFilter_a0_6269_6512_6548_6614_join[0], pop_complex(&SplitJoin84_SplitJoin65_SplitJoin65_AnonFilter_a0_6271_6513_6598_6615_join[1]));
	push_complex(&SplitJoin82_SplitJoin63_SplitJoin63_AnonFilter_a0_6269_6512_6548_6614_join[0], pop_complex(&SplitJoin84_SplitJoin65_SplitJoin65_AnonFilter_a0_6271_6513_6598_6615_join[1]));
}

void Identity_6289() {
	complex_t __tmp1788 = pop_complex(&SplitJoin96_SplitJoin77_SplitJoin77_AnonFilter_a0_6287_6521_6602_6619_split[0]);
	push_complex(&SplitJoin96_SplitJoin77_SplitJoin77_AnonFilter_a0_6287_6521_6602_6619_join[0], __tmp1788) ; 
}


void Identity_6291() {
	complex_t __tmp1791 = pop_complex(&SplitJoin96_SplitJoin77_SplitJoin77_AnonFilter_a0_6287_6521_6602_6619_split[1]);
	push_complex(&SplitJoin96_SplitJoin77_SplitJoin77_AnonFilter_a0_6287_6521_6602_6619_join[1], __tmp1791) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_6441() {
	push_complex(&SplitJoin96_SplitJoin77_SplitJoin77_AnonFilter_a0_6287_6521_6602_6619_split[0], pop_complex(&SplitJoin94_SplitJoin75_SplitJoin75_AnonFilter_a0_6285_6520_6601_6618_split[0]));
	push_complex(&SplitJoin96_SplitJoin77_SplitJoin77_AnonFilter_a0_6287_6521_6602_6619_split[1], pop_complex(&SplitJoin94_SplitJoin75_SplitJoin75_AnonFilter_a0_6285_6520_6601_6618_split[0]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_6442() {
	push_complex(&SplitJoin94_SplitJoin75_SplitJoin75_AnonFilter_a0_6285_6520_6601_6618_join[0], pop_complex(&SplitJoin96_SplitJoin77_SplitJoin77_AnonFilter_a0_6287_6521_6602_6619_join[0]));
	push_complex(&SplitJoin94_SplitJoin75_SplitJoin75_AnonFilter_a0_6285_6520_6601_6618_join[0], pop_complex(&SplitJoin96_SplitJoin77_SplitJoin77_AnonFilter_a0_6287_6521_6602_6619_join[1]));
}

void Identity_6295() {
	complex_t __tmp1799 = pop_complex(&SplitJoin100_SplitJoin81_SplitJoin81_AnonFilter_a0_6293_6524_6603_6620_split[0]);
	push_complex(&SplitJoin100_SplitJoin81_SplitJoin81_AnonFilter_a0_6293_6524_6603_6620_join[0], __tmp1799) ; 
}


void Identity_6297() {
	complex_t __tmp1802 = pop_complex(&SplitJoin100_SplitJoin81_SplitJoin81_AnonFilter_a0_6293_6524_6603_6620_split[1]);
	push_complex(&SplitJoin100_SplitJoin81_SplitJoin81_AnonFilter_a0_6293_6524_6603_6620_join[1], __tmp1802) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_6443() {
	push_complex(&SplitJoin100_SplitJoin81_SplitJoin81_AnonFilter_a0_6293_6524_6603_6620_split[0], pop_complex(&SplitJoin94_SplitJoin75_SplitJoin75_AnonFilter_a0_6285_6520_6601_6618_split[1]));
	push_complex(&SplitJoin100_SplitJoin81_SplitJoin81_AnonFilter_a0_6293_6524_6603_6620_split[1], pop_complex(&SplitJoin94_SplitJoin75_SplitJoin75_AnonFilter_a0_6285_6520_6601_6618_split[1]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_6444() {
	push_complex(&SplitJoin94_SplitJoin75_SplitJoin75_AnonFilter_a0_6285_6520_6601_6618_join[1], pop_complex(&SplitJoin100_SplitJoin81_SplitJoin81_AnonFilter_a0_6293_6524_6603_6620_join[0]));
	push_complex(&SplitJoin94_SplitJoin75_SplitJoin75_AnonFilter_a0_6285_6520_6601_6618_join[1], pop_complex(&SplitJoin100_SplitJoin81_SplitJoin81_AnonFilter_a0_6293_6524_6603_6620_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_6439() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin94_SplitJoin75_SplitJoin75_AnonFilter_a0_6285_6520_6601_6618_split[0], pop_complex(&SplitJoin82_SplitJoin63_SplitJoin63_AnonFilter_a0_6269_6512_6548_6614_split[1]));
		push_complex(&SplitJoin94_SplitJoin75_SplitJoin75_AnonFilter_a0_6285_6520_6601_6618_split[1], pop_complex(&SplitJoin82_SplitJoin63_SplitJoin63_AnonFilter_a0_6269_6512_6548_6614_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6440() {
	push_complex(&SplitJoin82_SplitJoin63_SplitJoin63_AnonFilter_a0_6269_6512_6548_6614_join[1], pop_complex(&SplitJoin94_SplitJoin75_SplitJoin75_AnonFilter_a0_6285_6520_6601_6618_join[0]));
	push_complex(&SplitJoin82_SplitJoin63_SplitJoin63_AnonFilter_a0_6269_6512_6548_6614_join[1], pop_complex(&SplitJoin94_SplitJoin75_SplitJoin75_AnonFilter_a0_6285_6520_6601_6618_join[0]));
	push_complex(&SplitJoin82_SplitJoin63_SplitJoin63_AnonFilter_a0_6269_6512_6548_6614_join[1], pop_complex(&SplitJoin94_SplitJoin75_SplitJoin75_AnonFilter_a0_6285_6520_6601_6618_join[1]));
	push_complex(&SplitJoin82_SplitJoin63_SplitJoin63_AnonFilter_a0_6269_6512_6548_6614_join[1], pop_complex(&SplitJoin94_SplitJoin75_SplitJoin75_AnonFilter_a0_6285_6520_6601_6618_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_6431() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_complex(&SplitJoin82_SplitJoin63_SplitJoin63_AnonFilter_a0_6269_6512_6548_6614_split[0], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6237_6463_6587_6606_split[1]));
		push_complex(&SplitJoin82_SplitJoin63_SplitJoin63_AnonFilter_a0_6269_6512_6548_6614_split[1], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6237_6463_6587_6606_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6432() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6237_6463_6587_6606_join[1], pop_complex(&SplitJoin82_SplitJoin63_SplitJoin63_AnonFilter_a0_6269_6512_6548_6614_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6237_6463_6587_6606_join[1], pop_complex(&SplitJoin82_SplitJoin63_SplitJoin63_AnonFilter_a0_6269_6512_6548_6614_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6415() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6237_6463_6587_6606_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6573WEIGHTED_ROUND_ROBIN_Splitter_6415));
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6237_6463_6587_6606_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6573WEIGHTED_ROUND_ROBIN_Splitter_6415));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6416() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6416WEIGHTED_ROUND_ROBIN_Splitter_6559, pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6237_6463_6587_6606_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6416WEIGHTED_ROUND_ROBIN_Splitter_6559, pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6237_6463_6587_6606_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_6392() {
 {
 {
	int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
	FOR(int, _i, 0,  < , 2, _i++) {
		push_complex(&Pre_CollapsedDataParallel_1_6392butterfly_6299, peek_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child0_6550_6622_split[0], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
		iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
	}
	ENDFOR
}
}
}
	pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child0_6550_6622_split[0]) ; 
}


void butterfly_6299() {
	complex_t WN1;
	complex_t WN2;
	complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_6392butterfly_6299));
	complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_6392butterfly_6299));
	complex_t __sa1;
	complex_t __sa2;
	WN1.real = 1.0 ; 
	WN1.imag = -0.0 ; 
	WN2.real = -1.0 ; 
	WN2.imag = 8.742278E-8 ; 
	__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
	__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
	push_complex(&butterfly_6299Post_CollapsedDataParallel_2_6393, __sa1) ; 
	__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
	__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
	push_complex(&butterfly_6299Post_CollapsedDataParallel_2_6393, __sa2) ; 
}


void Post_CollapsedDataParallel_2_6393() {
 {
 {
	FOR(int, _k, 0,  < , 2, _k++) {
 {
		push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child0_6550_6622_join[0], peek_complex(&butterfly_6299Post_CollapsedDataParallel_2_6393, (_k + 0))) ; 
	}
	}
	ENDFOR
}
}
	pop_complex(&butterfly_6299Post_CollapsedDataParallel_2_6393) ; 
}


void Pre_CollapsedDataParallel_1_6395() {
 {
 {
	int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
	FOR(int, _i, 0,  < , 2, _i++) {
		push_complex(&Pre_CollapsedDataParallel_1_6395butterfly_6300, peek_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child0_6550_6622_split[1], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
		iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
	}
	ENDFOR
}
}
}
	pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child0_6550_6622_split[1]) ; 
}


void butterfly_6300() {
	complex_t WN1;
	complex_t WN2;
	complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_6395butterfly_6300));
	complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_6395butterfly_6300));
	complex_t __sa1;
	complex_t __sa2;
	WN1.real = 1.0 ; 
	WN1.imag = -0.0 ; 
	WN2.real = -1.0 ; 
	WN2.imag = 8.742278E-8 ; 
	__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
	__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
	push_complex(&butterfly_6300Post_CollapsedDataParallel_2_6396, __sa1) ; 
	__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
	__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
	push_complex(&butterfly_6300Post_CollapsedDataParallel_2_6396, __sa2) ; 
}


void Post_CollapsedDataParallel_2_6396() {
 {
 {
	FOR(int, _k, 0,  < , 2, _k++) {
 {
		push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child0_6550_6622_join[1], peek_complex(&butterfly_6300Post_CollapsedDataParallel_2_6396, (_k + 0))) ; 
	}
	}
	ENDFOR
}
}
	pop_complex(&butterfly_6300Post_CollapsedDataParallel_2_6396) ; 
}


void Pre_CollapsedDataParallel_1_6398() {
 {
 {
	int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
	FOR(int, _i, 0,  < , 2, _i++) {
		push_complex(&Pre_CollapsedDataParallel_1_6398butterfly_6301, peek_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child0_6550_6622_split[2], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
		iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
	}
	ENDFOR
}
}
}
	pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child0_6550_6622_split[2]) ; 
}


void butterfly_6301() {
	complex_t WN1;
	complex_t WN2;
	complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_6398butterfly_6301));
	complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_6398butterfly_6301));
	complex_t __sa1;
	complex_t __sa2;
	WN1.real = 1.0 ; 
	WN1.imag = -0.0 ; 
	WN2.real = -1.0 ; 
	WN2.imag = 8.742278E-8 ; 
	__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
	__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
	push_complex(&butterfly_6301Post_CollapsedDataParallel_2_6399, __sa1) ; 
	__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
	__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
	push_complex(&butterfly_6301Post_CollapsedDataParallel_2_6399, __sa2) ; 
}


void Post_CollapsedDataParallel_2_6399() {
 {
 {
	FOR(int, _k, 0,  < , 2, _k++) {
 {
		push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child0_6550_6622_join[2], peek_complex(&butterfly_6301Post_CollapsedDataParallel_2_6399, (_k + 0))) ; 
	}
	}
	ENDFOR
}
}
	pop_complex(&butterfly_6301Post_CollapsedDataParallel_2_6399) ; 
}


void Pre_CollapsedDataParallel_1_6401() {
 {
 {
	int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
	FOR(int, _i, 0,  < , 2, _i++) {
		push_complex(&Pre_CollapsedDataParallel_1_6401butterfly_6302, peek_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child0_6550_6622_split[3], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
		iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
	}
	ENDFOR
}
}
}
	pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child0_6550_6622_split[3]) ; 
}


void butterfly_6302() {
	complex_t WN1;
	complex_t WN2;
	complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_6401butterfly_6302));
	complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_6401butterfly_6302));
	complex_t __sa1;
	complex_t __sa2;
	WN1.real = 1.0 ; 
	WN1.imag = -0.0 ; 
	WN2.real = -1.0 ; 
	WN2.imag = 8.742278E-8 ; 
	__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
	__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
	push_complex(&butterfly_6302Post_CollapsedDataParallel_2_6402, __sa1) ; 
	__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
	__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
	push_complex(&butterfly_6302Post_CollapsedDataParallel_2_6402, __sa2) ; 
}


void Post_CollapsedDataParallel_2_6402() {
 {
 {
	FOR(int, _k, 0,  < , 2, _k++) {
 {
		push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child0_6550_6622_join[3], peek_complex(&butterfly_6302Post_CollapsedDataParallel_2_6402, (_k + 0))) ; 
	}
	}
	ENDFOR
}
}
	pop_complex(&butterfly_6302Post_CollapsedDataParallel_2_6402) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_6560() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child0_6550_6622_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_Hier_6590_6621_split[0]));
		push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child0_6550_6622_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_Hier_6590_6621_split[0]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_6561() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_Hier_6590_6621_join[0], pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child0_6550_6622_join[__iter_]));
		push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_Hier_6590_6621_join[0], pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child0_6550_6622_join[__iter_]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_6404() {
 {
 {
	int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
	FOR(int, _i, 0,  < , 2, _i++) {
		push_complex(&Pre_CollapsedDataParallel_1_6404butterfly_6303, peek_complex(&SplitJoin61_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child1_6553_6623_split[0], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
		iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
	}
	ENDFOR
}
}
}
	pop_complex(&SplitJoin61_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child1_6553_6623_split[0]) ; 
}


void butterfly_6303() {
	complex_t WN1;
	complex_t WN2;
	complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_6404butterfly_6303));
	complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_6404butterfly_6303));
	complex_t __sa1;
	complex_t __sa2;
	WN1.real = 1.0 ; 
	WN1.imag = -0.0 ; 
	WN2.real = -1.0 ; 
	WN2.imag = 8.742278E-8 ; 
	__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
	__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
	push_complex(&butterfly_6303Post_CollapsedDataParallel_2_6405, __sa1) ; 
	__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
	__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
	push_complex(&butterfly_6303Post_CollapsedDataParallel_2_6405, __sa2) ; 
}


void Post_CollapsedDataParallel_2_6405() {
 {
 {
	FOR(int, _k, 0,  < , 2, _k++) {
 {
		push_complex(&SplitJoin61_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child1_6553_6623_join[0], peek_complex(&butterfly_6303Post_CollapsedDataParallel_2_6405, (_k + 0))) ; 
	}
	}
	ENDFOR
}
}
	pop_complex(&butterfly_6303Post_CollapsedDataParallel_2_6405) ; 
}


void Pre_CollapsedDataParallel_1_6407() {
 {
 {
	int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
	FOR(int, _i, 0,  < , 2, _i++) {
		push_complex(&Pre_CollapsedDataParallel_1_6407butterfly_6304, peek_complex(&SplitJoin61_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child1_6553_6623_split[1], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
		iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
	}
	ENDFOR
}
}
}
	pop_complex(&SplitJoin61_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child1_6553_6623_split[1]) ; 
}


void butterfly_6304() {
	complex_t WN1;
	complex_t WN2;
	complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_6407butterfly_6304));
	complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_6407butterfly_6304));
	complex_t __sa1;
	complex_t __sa2;
	WN1.real = 1.0 ; 
	WN1.imag = -0.0 ; 
	WN2.real = -1.0 ; 
	WN2.imag = 8.742278E-8 ; 
	__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
	__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
	push_complex(&butterfly_6304Post_CollapsedDataParallel_2_6408, __sa1) ; 
	__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
	__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
	push_complex(&butterfly_6304Post_CollapsedDataParallel_2_6408, __sa2) ; 
}


void Post_CollapsedDataParallel_2_6408() {
 {
 {
	FOR(int, _k, 0,  < , 2, _k++) {
 {
		push_complex(&SplitJoin61_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child1_6553_6623_join[1], peek_complex(&butterfly_6304Post_CollapsedDataParallel_2_6408, (_k + 0))) ; 
	}
	}
	ENDFOR
}
}
	pop_complex(&butterfly_6304Post_CollapsedDataParallel_2_6408) ; 
}


void Pre_CollapsedDataParallel_1_6410() {
 {
 {
	int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
	FOR(int, _i, 0,  < , 2, _i++) {
		push_complex(&Pre_CollapsedDataParallel_1_6410butterfly_6305, peek_complex(&SplitJoin61_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child1_6553_6623_split[2], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
		iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
	}
	ENDFOR
}
}
}
	pop_complex(&SplitJoin61_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child1_6553_6623_split[2]) ; 
}


void butterfly_6305() {
	complex_t WN1;
	complex_t WN2;
	complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_6410butterfly_6305));
	complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_6410butterfly_6305));
	complex_t __sa1;
	complex_t __sa2;
	WN1.real = 1.0 ; 
	WN1.imag = -0.0 ; 
	WN2.real = -1.0 ; 
	WN2.imag = 8.742278E-8 ; 
	__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
	__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
	push_complex(&butterfly_6305Post_CollapsedDataParallel_2_6411, __sa1) ; 
	__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
	__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
	push_complex(&butterfly_6305Post_CollapsedDataParallel_2_6411, __sa2) ; 
}


void Post_CollapsedDataParallel_2_6411() {
 {
 {
	FOR(int, _k, 0,  < , 2, _k++) {
 {
		push_complex(&SplitJoin61_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child1_6553_6623_join[2], peek_complex(&butterfly_6305Post_CollapsedDataParallel_2_6411, (_k + 0))) ; 
	}
	}
	ENDFOR
}
}
	pop_complex(&butterfly_6305Post_CollapsedDataParallel_2_6411) ; 
}


void Pre_CollapsedDataParallel_1_6413() {
 {
 {
	int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
	FOR(int, _i, 0,  < , 2, _i++) {
		push_complex(&Pre_CollapsedDataParallel_1_6413butterfly_6306, peek_complex(&SplitJoin61_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child1_6553_6623_split[3], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
		iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
	}
	ENDFOR
}
}
}
	pop_complex(&SplitJoin61_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child1_6553_6623_split[3]) ; 
}


void butterfly_6306() {
	complex_t WN1;
	complex_t WN2;
	complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_6413butterfly_6306));
	complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_6413butterfly_6306));
	complex_t __sa1;
	complex_t __sa2;
	WN1.real = 1.0 ; 
	WN1.imag = -0.0 ; 
	WN2.real = -1.0 ; 
	WN2.imag = 8.742278E-8 ; 
	__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
	__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
	push_complex(&butterfly_6306Post_CollapsedDataParallel_2_6414, __sa1) ; 
	__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
	__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
	push_complex(&butterfly_6306Post_CollapsedDataParallel_2_6414, __sa2) ; 
}


void Post_CollapsedDataParallel_2_6414() {
 {
 {
	FOR(int, _k, 0,  < , 2, _k++) {
 {
		push_complex(&SplitJoin61_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child1_6553_6623_join[3], peek_complex(&butterfly_6306Post_CollapsedDataParallel_2_6414, (_k + 0))) ; 
	}
	}
	ENDFOR
}
}
	pop_complex(&butterfly_6306Post_CollapsedDataParallel_2_6414) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_6562() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin61_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child1_6553_6623_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_Hier_6590_6621_split[1]));
		push_complex(&SplitJoin61_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child1_6553_6623_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_Hier_6590_6621_split[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_6563() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_Hier_6590_6621_join[1], pop_complex(&SplitJoin61_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child1_6553_6623_join[__iter_]));
		push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_Hier_6590_6621_join[1], pop_complex(&SplitJoin61_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child1_6553_6623_join[__iter_]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6559() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_Hier_6590_6621_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6416WEIGHTED_ROUND_ROBIN_Splitter_6559));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_Hier_6590_6621_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6416WEIGHTED_ROUND_ROBIN_Splitter_6559));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_6564() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6564WEIGHTED_ROUND_ROBIN_Splitter_6565, pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_Hier_6590_6621_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6564WEIGHTED_ROUND_ROBIN_Splitter_6565, pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_Hier_6590_6621_join[1]));
	ENDFOR
}

void butterfly_6308() {
	complex_t WN1;
	complex_t WN2;
	complex_t one = ((complex_t) pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_6218_6471_6549_6626_split[0]));
	complex_t two = ((complex_t) pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_6218_6471_6549_6626_split[0]));
	complex_t __sa1;
	complex_t __sa2;
	WN1.real = 1.0 ; 
	WN1.imag = -0.0 ; 
	WN2.real = -1.0 ; 
	WN2.imag = 8.742278E-8 ; 
	__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
	__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
	push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_6218_6471_6549_6626_join[0], __sa1) ; 
	__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
	__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
	push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_6218_6471_6549_6626_join[0], __sa2) ; 
}


void butterfly_6309() {
	complex_t WN1;
	complex_t WN2;
	complex_t one = ((complex_t) pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_6218_6471_6549_6626_split[1]));
	complex_t two = ((complex_t) pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_6218_6471_6549_6626_split[1]));
	complex_t __sa1;
	complex_t __sa2;
	WN1.real = -4.371139E-8 ; 
	WN1.imag = -1.0 ; 
	WN2.real = 1.1924881E-8 ; 
	WN2.imag = 1.0 ; 
	__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
	__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
	push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_6218_6471_6549_6626_join[1], __sa1) ; 
	__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
	__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
	push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_6218_6471_6549_6626_join[1], __sa2) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_6449() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_6218_6471_6549_6626_split[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child0_6556_6625_split[0]));
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_6218_6471_6549_6626_split[1], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child0_6556_6625_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6450() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child0_6556_6625_join[0], pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_6218_6471_6549_6626_join[0]));
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child0_6556_6625_join[0], pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_6218_6471_6549_6626_join[1]));
	ENDFOR
}}

void butterfly_6310() {
	complex_t WN1;
	complex_t WN2;
	complex_t one = ((complex_t) pop_complex(&SplitJoin44_SplitJoin29_SplitJoin29_split2_6220_6485_6551_6627_split[0]));
	complex_t two = ((complex_t) pop_complex(&SplitJoin44_SplitJoin29_SplitJoin29_split2_6220_6485_6551_6627_split[0]));
	complex_t __sa1;
	complex_t __sa2;
	WN1.real = 1.0 ; 
	WN1.imag = -0.0 ; 
	WN2.real = -1.0 ; 
	WN2.imag = 8.742278E-8 ; 
	__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
	__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
	push_complex(&SplitJoin44_SplitJoin29_SplitJoin29_split2_6220_6485_6551_6627_join[0], __sa1) ; 
	__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
	__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
	push_complex(&SplitJoin44_SplitJoin29_SplitJoin29_split2_6220_6485_6551_6627_join[0], __sa2) ; 
}


void butterfly_6311() {
	complex_t WN1;
	complex_t WN2;
	complex_t one = ((complex_t) pop_complex(&SplitJoin44_SplitJoin29_SplitJoin29_split2_6220_6485_6551_6627_split[1]));
	complex_t two = ((complex_t) pop_complex(&SplitJoin44_SplitJoin29_SplitJoin29_split2_6220_6485_6551_6627_split[1]));
	complex_t __sa1;
	complex_t __sa2;
	WN1.real = -4.371139E-8 ; 
	WN1.imag = -1.0 ; 
	WN2.real = 1.1924881E-8 ; 
	WN2.imag = 1.0 ; 
	__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
	__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
	push_complex(&SplitJoin44_SplitJoin29_SplitJoin29_split2_6220_6485_6551_6627_join[1], __sa1) ; 
	__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
	__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
	push_complex(&SplitJoin44_SplitJoin29_SplitJoin29_split2_6220_6485_6551_6627_join[1], __sa2) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_6451() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin44_SplitJoin29_SplitJoin29_split2_6220_6485_6551_6627_split[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child0_6556_6625_split[1]));
		push_complex(&SplitJoin44_SplitJoin29_SplitJoin29_split2_6220_6485_6551_6627_split[1], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child0_6556_6625_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6452() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child0_6556_6625_join[1], pop_complex(&SplitJoin44_SplitJoin29_SplitJoin29_split2_6220_6485_6551_6627_join[0]));
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child0_6556_6625_join[1], pop_complex(&SplitJoin44_SplitJoin29_SplitJoin29_split2_6220_6485_6551_6627_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_6566() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child0_6556_6625_split[0], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_Hier_6591_6624_split[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child0_6556_6625_split[1], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_Hier_6591_6624_split[0]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_6567() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_Hier_6591_6624_join[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child0_6556_6625_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_Hier_6591_6624_join[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child0_6556_6625_join[1]));
	ENDFOR
}

void butterfly_6312() {
	complex_t WN1;
	complex_t WN2;
	complex_t one = ((complex_t) pop_complex(&SplitJoin50_SplitJoin33_SplitJoin33_split2_6222_6488_6552_6629_split[0]));
	complex_t two = ((complex_t) pop_complex(&SplitJoin50_SplitJoin33_SplitJoin33_split2_6222_6488_6552_6629_split[0]));
	complex_t __sa1;
	complex_t __sa2;
	WN1.real = 1.0 ; 
	WN1.imag = -0.0 ; 
	WN2.real = -1.0 ; 
	WN2.imag = 8.742278E-8 ; 
	__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
	__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
	push_complex(&SplitJoin50_SplitJoin33_SplitJoin33_split2_6222_6488_6552_6629_join[0], __sa1) ; 
	__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
	__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
	push_complex(&SplitJoin50_SplitJoin33_SplitJoin33_split2_6222_6488_6552_6629_join[0], __sa2) ; 
}


void butterfly_6313() {
	complex_t WN1;
	complex_t WN2;
	complex_t one = ((complex_t) pop_complex(&SplitJoin50_SplitJoin33_SplitJoin33_split2_6222_6488_6552_6629_split[1]));
	complex_t two = ((complex_t) pop_complex(&SplitJoin50_SplitJoin33_SplitJoin33_split2_6222_6488_6552_6629_split[1]));
	complex_t __sa1;
	complex_t __sa2;
	WN1.real = -4.371139E-8 ; 
	WN1.imag = -1.0 ; 
	WN2.real = 1.1924881E-8 ; 
	WN2.imag = 1.0 ; 
	__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
	__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
	push_complex(&SplitJoin50_SplitJoin33_SplitJoin33_split2_6222_6488_6552_6629_join[1], __sa1) ; 
	__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
	__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
	push_complex(&SplitJoin50_SplitJoin33_SplitJoin33_split2_6222_6488_6552_6629_join[1], __sa2) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_6453() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin50_SplitJoin33_SplitJoin33_split2_6222_6488_6552_6629_split[0], pop_complex(&SplitJoin48_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child1_6558_6628_split[0]));
		push_complex(&SplitJoin50_SplitJoin33_SplitJoin33_split2_6222_6488_6552_6629_split[1], pop_complex(&SplitJoin48_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child1_6558_6628_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6454() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin48_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child1_6558_6628_join[0], pop_complex(&SplitJoin50_SplitJoin33_SplitJoin33_split2_6222_6488_6552_6629_join[0]));
		push_complex(&SplitJoin48_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child1_6558_6628_join[0], pop_complex(&SplitJoin50_SplitJoin33_SplitJoin33_split2_6222_6488_6552_6629_join[1]));
	ENDFOR
}}

void butterfly_6314() {
	complex_t WN1;
	complex_t WN2;
	complex_t one = ((complex_t) pop_complex(&SplitJoin54_SplitJoin37_SplitJoin37_split2_6224_6491_6554_6630_split[0]));
	complex_t two = ((complex_t) pop_complex(&SplitJoin54_SplitJoin37_SplitJoin37_split2_6224_6491_6554_6630_split[0]));
	complex_t __sa1;
	complex_t __sa2;
	WN1.real = 1.0 ; 
	WN1.imag = -0.0 ; 
	WN2.real = -1.0 ; 
	WN2.imag = 8.742278E-8 ; 
	__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
	__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
	push_complex(&SplitJoin54_SplitJoin37_SplitJoin37_split2_6224_6491_6554_6630_join[0], __sa1) ; 
	__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
	__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
	push_complex(&SplitJoin54_SplitJoin37_SplitJoin37_split2_6224_6491_6554_6630_join[0], __sa2) ; 
}


void butterfly_6315() {
	complex_t WN1;
	complex_t WN2;
	complex_t one = ((complex_t) pop_complex(&SplitJoin54_SplitJoin37_SplitJoin37_split2_6224_6491_6554_6630_split[1]));
	complex_t two = ((complex_t) pop_complex(&SplitJoin54_SplitJoin37_SplitJoin37_split2_6224_6491_6554_6630_split[1]));
	complex_t __sa1;
	complex_t __sa2;
	WN1.real = -4.371139E-8 ; 
	WN1.imag = -1.0 ; 
	WN2.real = 1.1924881E-8 ; 
	WN2.imag = 1.0 ; 
	__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
	__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
	push_complex(&SplitJoin54_SplitJoin37_SplitJoin37_split2_6224_6491_6554_6630_join[1], __sa1) ; 
	__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
	__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
	push_complex(&SplitJoin54_SplitJoin37_SplitJoin37_split2_6224_6491_6554_6630_join[1], __sa2) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_6455() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin54_SplitJoin37_SplitJoin37_split2_6224_6491_6554_6630_split[0], pop_complex(&SplitJoin48_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child1_6558_6628_split[1]));
		push_complex(&SplitJoin54_SplitJoin37_SplitJoin37_split2_6224_6491_6554_6630_split[1], pop_complex(&SplitJoin48_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child1_6558_6628_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6456() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin48_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child1_6558_6628_join[1], pop_complex(&SplitJoin54_SplitJoin37_SplitJoin37_split2_6224_6491_6554_6630_join[0]));
		push_complex(&SplitJoin48_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child1_6558_6628_join[1], pop_complex(&SplitJoin54_SplitJoin37_SplitJoin37_split2_6224_6491_6554_6630_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_6568() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin48_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child1_6558_6628_split[0], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_Hier_6591_6624_split[1]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin48_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child1_6558_6628_split[1], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_Hier_6591_6624_split[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_6569() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_Hier_6591_6624_join[1], pop_complex(&SplitJoin48_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child1_6558_6628_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_Hier_6591_6624_join[1], pop_complex(&SplitJoin48_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child1_6558_6628_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6565() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_Hier_6591_6624_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6564WEIGHTED_ROUND_ROBIN_Splitter_6565));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_Hier_6591_6624_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6564WEIGHTED_ROUND_ROBIN_Splitter_6565));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_6570() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6570WEIGHTED_ROUND_ROBIN_Splitter_6457, pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_Hier_6591_6624_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6570WEIGHTED_ROUND_ROBIN_Splitter_6457, pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_Hier_6591_6624_join[1]));
	ENDFOR
}

void butterfly_6317() {
	complex_t WN1;
	complex_t WN2;
	complex_t one = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_6231_6474_6555_6632_split[0]));
	complex_t two = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_6231_6474_6555_6632_split[0]));
	complex_t __sa1;
	complex_t __sa2;
	WN1.real = 1.0 ; 
	WN1.imag = -0.0 ; 
	WN2.real = -1.0 ; 
	WN2.imag = 8.742278E-8 ; 
	__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
	__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
	push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_6231_6474_6555_6632_join[0], __sa1) ; 
	__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
	__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
	push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_6231_6474_6555_6632_join[0], __sa2) ; 
}


void butterfly_6318() {
	complex_t WN1;
	complex_t WN2;
	complex_t one = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_6231_6474_6555_6632_split[1]));
	complex_t two = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_6231_6474_6555_6632_split[1]));
	complex_t __sa1;
	complex_t __sa2;
	WN1.real = 0.70710677 ; 
	WN1.imag = -0.70710677 ; 
	WN2.real = -0.70710665 ; 
	WN2.imag = 0.7071069 ; 
	__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
	__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
	push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_6231_6474_6555_6632_join[1], __sa1) ; 
	__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
	__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
	push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_6231_6474_6555_6632_join[1], __sa2) ; 
}


void butterfly_6319() {
	complex_t WN1;
	complex_t WN2;
	complex_t one = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_6231_6474_6555_6632_split[2]));
	complex_t two = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_6231_6474_6555_6632_split[2]));
	complex_t __sa1;
	complex_t __sa2;
	WN1.real = -4.371139E-8 ; 
	WN1.imag = -1.0 ; 
	WN2.real = 1.1924881E-8 ; 
	WN2.imag = 1.0 ; 
	__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
	__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
	push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_6231_6474_6555_6632_join[2], __sa1) ; 
	__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
	__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
	push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_6231_6474_6555_6632_join[2], __sa2) ; 
}


void butterfly_6320() {
	complex_t WN1;
	complex_t WN2;
	complex_t one = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_6231_6474_6555_6632_split[3]));
	complex_t two = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_6231_6474_6555_6632_split[3]));
	complex_t __sa1;
	complex_t __sa2;
	WN1.real = -0.70710677 ; 
	WN1.imag = -0.70710677 ; 
	WN2.real = 0.707107 ; 
	WN2.imag = 0.70710653 ; 
	__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
	__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
	push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_6231_6474_6555_6632_join[3], __sa1) ; 
	__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
	__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
	push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_6231_6474_6555_6632_join[3], __sa2) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_6459() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_6231_6474_6555_6632_split[__iter_], pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_6229_6473_6592_6631_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6460() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_6229_6473_6592_6631_join[0], pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_6231_6474_6555_6632_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void butterfly_6321() {
	complex_t WN1;
	complex_t WN2;
	complex_t one = ((complex_t) pop_complex(&SplitJoin37_SplitJoin22_SplitJoin22_split2_6233_6479_6557_6633_split[0]));
	complex_t two = ((complex_t) pop_complex(&SplitJoin37_SplitJoin22_SplitJoin22_split2_6233_6479_6557_6633_split[0]));
	complex_t __sa1;
	complex_t __sa2;
	WN1.real = 1.0 ; 
	WN1.imag = -0.0 ; 
	WN2.real = -1.0 ; 
	WN2.imag = 8.742278E-8 ; 
	__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
	__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
	push_complex(&SplitJoin37_SplitJoin22_SplitJoin22_split2_6233_6479_6557_6633_join[0], __sa1) ; 
	__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
	__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
	push_complex(&SplitJoin37_SplitJoin22_SplitJoin22_split2_6233_6479_6557_6633_join[0], __sa2) ; 
}


void butterfly_6322() {
	complex_t WN1;
	complex_t WN2;
	complex_t one = ((complex_t) pop_complex(&SplitJoin37_SplitJoin22_SplitJoin22_split2_6233_6479_6557_6633_split[1]));
	complex_t two = ((complex_t) pop_complex(&SplitJoin37_SplitJoin22_SplitJoin22_split2_6233_6479_6557_6633_split[1]));
	complex_t __sa1;
	complex_t __sa2;
	WN1.real = 0.70710677 ; 
	WN1.imag = -0.70710677 ; 
	WN2.real = -0.70710665 ; 
	WN2.imag = 0.7071069 ; 
	__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
	__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
	push_complex(&SplitJoin37_SplitJoin22_SplitJoin22_split2_6233_6479_6557_6633_join[1], __sa1) ; 
	__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
	__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
	push_complex(&SplitJoin37_SplitJoin22_SplitJoin22_split2_6233_6479_6557_6633_join[1], __sa2) ; 
}


void butterfly_6323() {
	complex_t WN1;
	complex_t WN2;
	complex_t one = ((complex_t) pop_complex(&SplitJoin37_SplitJoin22_SplitJoin22_split2_6233_6479_6557_6633_split[2]));
	complex_t two = ((complex_t) pop_complex(&SplitJoin37_SplitJoin22_SplitJoin22_split2_6233_6479_6557_6633_split[2]));
	complex_t __sa1;
	complex_t __sa2;
	WN1.real = -4.371139E-8 ; 
	WN1.imag = -1.0 ; 
	WN2.real = 1.1924881E-8 ; 
	WN2.imag = 1.0 ; 
	__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
	__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
	push_complex(&SplitJoin37_SplitJoin22_SplitJoin22_split2_6233_6479_6557_6633_join[2], __sa1) ; 
	__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
	__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
	push_complex(&SplitJoin37_SplitJoin22_SplitJoin22_split2_6233_6479_6557_6633_join[2], __sa2) ; 
}


void butterfly_6324() {
	complex_t WN1;
	complex_t WN2;
	complex_t one = ((complex_t) pop_complex(&SplitJoin37_SplitJoin22_SplitJoin22_split2_6233_6479_6557_6633_split[3]));
	complex_t two = ((complex_t) pop_complex(&SplitJoin37_SplitJoin22_SplitJoin22_split2_6233_6479_6557_6633_split[3]));
	complex_t __sa1;
	complex_t __sa2;
	WN1.real = -0.70710677 ; 
	WN1.imag = -0.70710677 ; 
	WN2.real = 0.707107 ; 
	WN2.imag = 0.70710653 ; 
	__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
	__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
	push_complex(&SplitJoin37_SplitJoin22_SplitJoin22_split2_6233_6479_6557_6633_join[3], __sa1) ; 
	__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
	__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
	push_complex(&SplitJoin37_SplitJoin22_SplitJoin22_split2_6233_6479_6557_6633_join[3], __sa2) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_6461() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin37_SplitJoin22_SplitJoin22_split2_6233_6479_6557_6633_split[__iter_], pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_6229_6473_6592_6631_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6462() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_6229_6473_6592_6631_join[1], pop_complex(&SplitJoin37_SplitJoin22_SplitJoin22_split2_6233_6479_6557_6633_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_6457() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_6229_6473_6592_6631_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6570WEIGHTED_ROUND_ROBIN_Splitter_6457));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_6229_6473_6592_6631_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6570WEIGHTED_ROUND_ROBIN_Splitter_6457));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_6458() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6458WEIGHTED_ROUND_ROBIN_Splitter_6576, pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_6229_6473_6592_6631_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6458WEIGHTED_ROUND_ROBIN_Splitter_6576, pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_6229_6473_6592_6631_join[1]));
	ENDFOR
}

void magnitude_6578(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_6593_6634_split[0]));
		push_float(&SplitJoin24_magnitude_Fiss_6593_6634_join[0], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_6579(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_6593_6634_split[1]));
		push_float(&SplitJoin24_magnitude_Fiss_6593_6634_join[1], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_6580(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_6593_6634_split[2]));
		push_float(&SplitJoin24_magnitude_Fiss_6593_6634_join[2], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_6581(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_6593_6634_split[3]));
		push_float(&SplitJoin24_magnitude_Fiss_6593_6634_join[3], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_6582(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_6593_6634_split[4]));
		push_float(&SplitJoin24_magnitude_Fiss_6593_6634_join[4], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_6583(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_6593_6634_split[5]));
		push_float(&SplitJoin24_magnitude_Fiss_6593_6634_join[5], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_6584(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_6593_6634_split[6]));
		push_float(&SplitJoin24_magnitude_Fiss_6593_6634_join[6], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_6585(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_6593_6634_split[7]));
		push_float(&SplitJoin24_magnitude_Fiss_6593_6634_join[7], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6576() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin24_magnitude_Fiss_6593_6634_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6458WEIGHTED_ROUND_ROBIN_Splitter_6576));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6577() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6577sink_6326, pop_float(&SplitJoin24_magnitude_Fiss_6593_6634_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void sink_6326(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		printf("%.10f", pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6577sink_6326));
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6458WEIGHTED_ROUND_ROBIN_Splitter_6576);
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child0_6556_6625_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 2, __iter_init_1_++)
		init_buffer_complex(&SplitJoin100_SplitJoin81_SplitJoin81_AnonFilter_a0_6293_6524_6603_6620_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_complex(&SplitJoin96_SplitJoin77_SplitJoin77_AnonFilter_a0_6287_6521_6602_6619_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 2, __iter_init_3_++)
		init_buffer_complex(&SplitJoin96_SplitJoin77_SplitJoin77_AnonFilter_a0_6287_6521_6602_6619_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 2, __iter_init_4_++)
		init_buffer_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_6229_6473_6592_6631_join[__iter_init_4_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_6398butterfly_6301);
	FOR(int, __iter_init_5_, 0, <, 2, __iter_init_5_++)
		init_buffer_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_Hier_6591_6624_split[__iter_init_5_]);
	ENDFOR
	init_buffer_complex(&butterfly_6305Post_CollapsedDataParallel_2_6411);
	FOR(int, __iter_init_6_, 0, <, 2, __iter_init_6_++)
		init_buffer_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_Hier_6590_6621_split[__iter_init_6_]);
	ENDFOR
	init_buffer_complex(&butterfly_6306Post_CollapsedDataParallel_2_6414);
	FOR(int, __iter_init_7_, 0, <, 2, __iter_init_7_++)
		init_buffer_complex(&SplitJoin90_SplitJoin71_SplitJoin71_AnonFilter_a0_6279_6517_6600_6617_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 4, __iter_init_8_++)
		init_buffer_complex(&SplitJoin37_SplitJoin22_SplitJoin22_split2_6233_6479_6557_6633_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 2, __iter_init_9_++)
		init_buffer_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_6229_6473_6592_6631_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 4, __iter_init_10_++)
		init_buffer_complex(&SplitJoin37_SplitJoin22_SplitJoin22_split2_6233_6479_6557_6633_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 2, __iter_init_11_++)
		init_buffer_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_6243_6466_6589_6609_split[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 2, __iter_init_12_++)
		init_buffer_complex(&SplitJoin78_SplitJoin59_SplitJoin59_AnonFilter_a0_6263_6509_6597_6613_split[__iter_init_12_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_6404butterfly_6303);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6416WEIGHTED_ROUND_ROBIN_Splitter_6559);
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_complex(&SplitJoin84_SplitJoin65_SplitJoin65_AnonFilter_a0_6271_6513_6598_6615_join[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_complex(&SplitJoin94_SplitJoin75_SplitJoin75_AnonFilter_a0_6285_6520_6601_6618_split[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 2, __iter_init_15_++)
		init_buffer_complex(&SplitJoin54_SplitJoin37_SplitJoin37_split2_6224_6491_6554_6630_join[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_complex(&SplitJoin50_SplitJoin33_SplitJoin33_split2_6222_6488_6552_6629_join[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 4, __iter_init_17_++)
		init_buffer_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child0_6550_6622_split[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 2, __iter_init_18_++)
		init_buffer_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6241_6465_6588_6608_join[__iter_init_18_]);
	ENDFOR
	FOR(int, __iter_init_19_, 0, <, 2, __iter_init_19_++)
		init_buffer_complex(&SplitJoin0_source_Fiss_6586_6605_split[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 2, __iter_init_20_++)
		init_buffer_complex(&SplitJoin90_SplitJoin71_SplitJoin71_AnonFilter_a0_6279_6517_6600_6617_split[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 2, __iter_init_21_++)
		init_buffer_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6239_6464_6547_6607_split[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 2, __iter_init_22_++)
		init_buffer_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_Hier_6590_6621_join[__iter_init_22_]);
	ENDFOR
	FOR(int, __iter_init_23_, 0, <, 2, __iter_init_23_++)
		init_buffer_complex(&SplitJoin78_SplitJoin59_SplitJoin59_AnonFilter_a0_6263_6509_6597_6613_join[__iter_init_23_]);
	ENDFOR
	FOR(int, __iter_init_24_, 0, <, 2, __iter_init_24_++)
		init_buffer_complex(&SplitJoin74_SplitJoin55_SplitJoin55_AnonFilter_a0_6257_6506_6596_6612_split[__iter_init_24_]);
	ENDFOR
	FOR(int, __iter_init_25_, 0, <, 2, __iter_init_25_++)
		init_buffer_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_6218_6471_6549_6626_split[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 2, __iter_init_26_++)
		init_buffer_complex(&SplitJoin44_SplitJoin29_SplitJoin29_split2_6220_6485_6551_6627_join[__iter_init_26_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6564WEIGHTED_ROUND_ROBIN_Splitter_6565);
	FOR(int, __iter_init_27_, 0, <, 2, __iter_init_27_++)
		init_buffer_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_6243_6466_6589_6609_join[__iter_init_27_]);
	ENDFOR
	init_buffer_complex(&butterfly_6302Post_CollapsedDataParallel_2_6402);
	FOR(int, __iter_init_28_, 0, <, 2, __iter_init_28_++)
		init_buffer_complex(&SplitJoin94_SplitJoin75_SplitJoin75_AnonFilter_a0_6285_6520_6601_6618_join[__iter_init_28_]);
	ENDFOR
	FOR(int, __iter_init_29_, 0, <, 2, __iter_init_29_++)
		init_buffer_complex(&SplitJoin82_SplitJoin63_SplitJoin63_AnonFilter_a0_6269_6512_6548_6614_split[__iter_init_29_]);
	ENDFOR
	init_buffer_complex(&butterfly_6304Post_CollapsedDataParallel_2_6408);
	FOR(int, __iter_init_30_, 0, <, 8, __iter_init_30_++)
		init_buffer_complex(&SplitJoin24_magnitude_Fiss_6593_6634_split[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 2, __iter_init_31_++)
		init_buffer_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_6218_6471_6549_6626_join[__iter_init_31_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_6395butterfly_6300);
	FOR(int, __iter_init_32_, 0, <, 2, __iter_init_32_++)
		init_buffer_complex(&SplitJoin72_SplitJoin53_SplitJoin53_AnonFilter_a0_6255_6505_6595_6611_split[__iter_init_32_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_6401butterfly_6302);
	FOR(int, __iter_init_33_, 0, <, 2, __iter_init_33_++)
		init_buffer_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6237_6463_6587_6606_split[__iter_init_33_]);
	ENDFOR
	FOR(int, __iter_init_34_, 0, <, 2, __iter_init_34_++)
		init_buffer_complex(&SplitJoin86_SplitJoin67_SplitJoin67_AnonFilter_a0_6273_6514_6599_6616_split[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 2, __iter_init_35_++)
		init_buffer_complex(&SplitJoin48_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child1_6558_6628_split[__iter_init_35_]);
	ENDFOR
	FOR(int, __iter_init_36_, 0, <, 2, __iter_init_36_++)
		init_buffer_complex(&SplitJoin84_SplitJoin65_SplitJoin65_AnonFilter_a0_6271_6513_6598_6615_split[__iter_init_36_]);
	ENDFOR
	init_buffer_complex(&butterfly_6300Post_CollapsedDataParallel_2_6396);
	FOR(int, __iter_init_37_, 0, <, 2, __iter_init_37_++)
		init_buffer_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_Hier_6591_6624_join[__iter_init_37_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6577sink_6326);
	init_buffer_complex(&butterfly_6301Post_CollapsedDataParallel_2_6399);
	FOR(int, __iter_init_38_, 0, <, 2, __iter_init_38_++)
		init_buffer_complex(&SplitJoin50_SplitJoin33_SplitJoin33_split2_6222_6488_6552_6629_split[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 2, __iter_init_39_++)
		init_buffer_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6241_6465_6588_6608_split[__iter_init_39_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_6410butterfly_6305);
	FOR(int, __iter_init_40_, 0, <, 2, __iter_init_40_++)
		init_buffer_complex(&SplitJoin44_SplitJoin29_SplitJoin29_split2_6220_6485_6551_6627_split[__iter_init_40_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6570WEIGHTED_ROUND_ROBIN_Splitter_6457);
	FOR(int, __iter_init_41_, 0, <, 4, __iter_init_41_++)
		init_buffer_complex(&SplitJoin61_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child1_6553_6623_join[__iter_init_41_]);
	ENDFOR
	FOR(int, __iter_init_42_, 0, <, 2, __iter_init_42_++)
		init_buffer_complex(&SplitJoin100_SplitJoin81_SplitJoin81_AnonFilter_a0_6293_6524_6603_6620_join[__iter_init_42_]);
	ENDFOR
	FOR(int, __iter_init_43_, 0, <, 4, __iter_init_43_++)
		init_buffer_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child0_6550_6622_join[__iter_init_43_]);
	ENDFOR
	FOR(int, __iter_init_44_, 0, <, 2, __iter_init_44_++)
		init_buffer_complex(&SplitJoin86_SplitJoin67_SplitJoin67_AnonFilter_a0_6273_6514_6599_6616_join[__iter_init_44_]);
	ENDFOR
	FOR(int, __iter_init_45_, 0, <, 2, __iter_init_45_++)
		init_buffer_complex(&SplitJoin48_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child1_6558_6628_join[__iter_init_45_]);
	ENDFOR
	FOR(int, __iter_init_46_, 0, <, 2, __iter_init_46_++)
		init_buffer_complex(&SplitJoin68_SplitJoin49_SplitJoin49_AnonFilter_a0_6249_6502_6594_6610_split[__iter_init_46_]);
	ENDFOR
	FOR(int, __iter_init_47_, 0, <, 2, __iter_init_47_++)
		init_buffer_complex(&SplitJoin74_SplitJoin55_SplitJoin55_AnonFilter_a0_6257_6506_6596_6612_join[__iter_init_47_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6573WEIGHTED_ROUND_ROBIN_Splitter_6415);
	init_buffer_complex(&Pre_CollapsedDataParallel_1_6392butterfly_6299);
	FOR(int, __iter_init_48_, 0, <, 2, __iter_init_48_++)
		init_buffer_complex(&SplitJoin0_source_Fiss_6586_6605_join[__iter_init_48_]);
	ENDFOR
	init_buffer_complex(&butterfly_6299Post_CollapsedDataParallel_2_6393);
	FOR(int, __iter_init_49_, 0, <, 2, __iter_init_49_++)
		init_buffer_complex(&SplitJoin72_SplitJoin53_SplitJoin53_AnonFilter_a0_6255_6505_6595_6611_join[__iter_init_49_]);
	ENDFOR
	FOR(int, __iter_init_50_, 0, <, 8, __iter_init_50_++)
		init_buffer_float(&SplitJoin24_magnitude_Fiss_6593_6634_join[__iter_init_50_]);
	ENDFOR
	FOR(int, __iter_init_51_, 0, <, 4, __iter_init_51_++)
		init_buffer_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_6231_6474_6555_6632_join[__iter_init_51_]);
	ENDFOR
	FOR(int, __iter_init_52_, 0, <, 2, __iter_init_52_++)
		init_buffer_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6239_6464_6547_6607_join[__iter_init_52_]);
	ENDFOR
	FOR(int, __iter_init_53_, 0, <, 2, __iter_init_53_++)
		init_buffer_complex(&SplitJoin82_SplitJoin63_SplitJoin63_AnonFilter_a0_6269_6512_6548_6614_join[__iter_init_53_]);
	ENDFOR
	FOR(int, __iter_init_54_, 0, <, 4, __iter_init_54_++)
		init_buffer_complex(&SplitJoin61_SplitJoin8_SplitJoin8_split1_6195_6468_Hier_child1_6553_6623_split[__iter_init_54_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_6407butterfly_6304);
	FOR(int, __iter_init_55_, 0, <, 2, __iter_init_55_++)
		init_buffer_complex(&SplitJoin54_SplitJoin37_SplitJoin37_split2_6224_6491_6554_6630_split[__iter_init_55_]);
	ENDFOR
	FOR(int, __iter_init_56_, 0, <, 2, __iter_init_56_++)
		init_buffer_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6237_6463_6587_6606_join[__iter_init_56_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_6413butterfly_6306);
	FOR(int, __iter_init_57_, 0, <, 2, __iter_init_57_++)
		init_buffer_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_6216_6470_Hier_child0_6556_6625_split[__iter_init_57_]);
	ENDFOR
	FOR(int, __iter_init_58_, 0, <, 4, __iter_init_58_++)
		init_buffer_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_6231_6474_6555_6632_split[__iter_init_58_]);
	ENDFOR
	FOR(int, __iter_init_59_, 0, <, 2, __iter_init_59_++)
		init_buffer_complex(&SplitJoin68_SplitJoin49_SplitJoin49_AnonFilter_a0_6249_6502_6594_6610_join[__iter_init_59_]);
	ENDFOR
	init_buffer_complex(&butterfly_6303Post_CollapsedDataParallel_2_6405);
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_6572();
			source_6574();
			source_6575();
		WEIGHTED_ROUND_ROBIN_Joiner_6573();
		WEIGHTED_ROUND_ROBIN_Splitter_6415();
			WEIGHTED_ROUND_ROBIN_Splitter_6417();
				WEIGHTED_ROUND_ROBIN_Splitter_6419();
					WEIGHTED_ROUND_ROBIN_Splitter_6421();
						Identity_6245();
						Identity_6247();
					WEIGHTED_ROUND_ROBIN_Joiner_6422();
					WEIGHTED_ROUND_ROBIN_Splitter_6423();
						Identity_6251();
						Identity_6253();
					WEIGHTED_ROUND_ROBIN_Joiner_6424();
				WEIGHTED_ROUND_ROBIN_Joiner_6420();
				WEIGHTED_ROUND_ROBIN_Splitter_6425();
					WEIGHTED_ROUND_ROBIN_Splitter_6427();
						Identity_6259();
						Identity_6261();
					WEIGHTED_ROUND_ROBIN_Joiner_6428();
					WEIGHTED_ROUND_ROBIN_Splitter_6429();
						Identity_6265();
						Identity_6267();
					WEIGHTED_ROUND_ROBIN_Joiner_6430();
				WEIGHTED_ROUND_ROBIN_Joiner_6426();
			WEIGHTED_ROUND_ROBIN_Joiner_6418();
			WEIGHTED_ROUND_ROBIN_Splitter_6431();
				WEIGHTED_ROUND_ROBIN_Splitter_6433();
					WEIGHTED_ROUND_ROBIN_Splitter_6435();
						Identity_6275();
						Identity_6277();
					WEIGHTED_ROUND_ROBIN_Joiner_6436();
					WEIGHTED_ROUND_ROBIN_Splitter_6437();
						Identity_6281();
						Identity_6283();
					WEIGHTED_ROUND_ROBIN_Joiner_6438();
				WEIGHTED_ROUND_ROBIN_Joiner_6434();
				WEIGHTED_ROUND_ROBIN_Splitter_6439();
					WEIGHTED_ROUND_ROBIN_Splitter_6441();
						Identity_6289();
						Identity_6291();
					WEIGHTED_ROUND_ROBIN_Joiner_6442();
					WEIGHTED_ROUND_ROBIN_Splitter_6443();
						Identity_6295();
						Identity_6297();
					WEIGHTED_ROUND_ROBIN_Joiner_6444();
				WEIGHTED_ROUND_ROBIN_Joiner_6440();
			WEIGHTED_ROUND_ROBIN_Joiner_6432();
		WEIGHTED_ROUND_ROBIN_Joiner_6416();
		WEIGHTED_ROUND_ROBIN_Splitter_6559();
			WEIGHTED_ROUND_ROBIN_Splitter_6560();
				Pre_CollapsedDataParallel_1_6392();
				butterfly_6299();
				Post_CollapsedDataParallel_2_6393();
				Pre_CollapsedDataParallel_1_6395();
				butterfly_6300();
				Post_CollapsedDataParallel_2_6396();
				Pre_CollapsedDataParallel_1_6398();
				butterfly_6301();
				Post_CollapsedDataParallel_2_6399();
				Pre_CollapsedDataParallel_1_6401();
				butterfly_6302();
				Post_CollapsedDataParallel_2_6402();
			WEIGHTED_ROUND_ROBIN_Joiner_6561();
			WEIGHTED_ROUND_ROBIN_Splitter_6562();
				Pre_CollapsedDataParallel_1_6404();
				butterfly_6303();
				Post_CollapsedDataParallel_2_6405();
				Pre_CollapsedDataParallel_1_6407();
				butterfly_6304();
				Post_CollapsedDataParallel_2_6408();
				Pre_CollapsedDataParallel_1_6410();
				butterfly_6305();
				Post_CollapsedDataParallel_2_6411();
				Pre_CollapsedDataParallel_1_6413();
				butterfly_6306();
				Post_CollapsedDataParallel_2_6414();
			WEIGHTED_ROUND_ROBIN_Joiner_6563();
		WEIGHTED_ROUND_ROBIN_Joiner_6564();
		WEIGHTED_ROUND_ROBIN_Splitter_6565();
			WEIGHTED_ROUND_ROBIN_Splitter_6566();
				WEIGHTED_ROUND_ROBIN_Splitter_6449();
					butterfly_6308();
					butterfly_6309();
				WEIGHTED_ROUND_ROBIN_Joiner_6450();
				WEIGHTED_ROUND_ROBIN_Splitter_6451();
					butterfly_6310();
					butterfly_6311();
				WEIGHTED_ROUND_ROBIN_Joiner_6452();
			WEIGHTED_ROUND_ROBIN_Joiner_6567();
			WEIGHTED_ROUND_ROBIN_Splitter_6568();
				WEIGHTED_ROUND_ROBIN_Splitter_6453();
					butterfly_6312();
					butterfly_6313();
				WEIGHTED_ROUND_ROBIN_Joiner_6454();
				WEIGHTED_ROUND_ROBIN_Splitter_6455();
					butterfly_6314();
					butterfly_6315();
				WEIGHTED_ROUND_ROBIN_Joiner_6456();
			WEIGHTED_ROUND_ROBIN_Joiner_6569();
		WEIGHTED_ROUND_ROBIN_Joiner_6570();
		WEIGHTED_ROUND_ROBIN_Splitter_6457();
			WEIGHTED_ROUND_ROBIN_Splitter_6459();
				butterfly_6317();
				butterfly_6318();
				butterfly_6319();
				butterfly_6320();
			WEIGHTED_ROUND_ROBIN_Joiner_6460();
			WEIGHTED_ROUND_ROBIN_Splitter_6461();
				butterfly_6321();
				butterfly_6322();
				butterfly_6323();
				butterfly_6324();
			WEIGHTED_ROUND_ROBIN_Joiner_6462();
		WEIGHTED_ROUND_ROBIN_Joiner_6458();
		WEIGHTED_ROUND_ROBIN_Splitter_6576();
			magnitude_6578();
			magnitude_6579();
			magnitude_6580();
			magnitude_6581();
			magnitude_6582();
			magnitude_6583();
			magnitude_6584();
			magnitude_6585();
		WEIGHTED_ROUND_ROBIN_Joiner_6577();
		sink_6326();
	ENDFOR
	return EXIT_SUCCESS;
}
