#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=416 on the compile command line
#else
#if BUF_SIZEMAX < 416
#error BUF_SIZEMAX too small, it must be at least 416
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif


void WEIGHTED_ROUND_ROBIN_Splitter_3597();
void source(buffer_void_t *chanin, buffer_complex_t *chanout);
void source_3599();
void source_3600();
void WEIGHTED_ROUND_ROBIN_Joiner_3598();
void WEIGHTED_ROUND_ROBIN_Splitter_3440();
void WEIGHTED_ROUND_ROBIN_Splitter_3442();
void WEIGHTED_ROUND_ROBIN_Splitter_3444();
void WEIGHTED_ROUND_ROBIN_Splitter_3446();
void Identity(buffer_complex_t *chanin, buffer_complex_t *chanout);
void Identity_3270();
void Identity_3272();
void WEIGHTED_ROUND_ROBIN_Joiner_3447();
void WEIGHTED_ROUND_ROBIN_Splitter_3448();
void Identity_3276();
void Identity_3278();
void WEIGHTED_ROUND_ROBIN_Joiner_3449();
void WEIGHTED_ROUND_ROBIN_Joiner_3445();
void WEIGHTED_ROUND_ROBIN_Splitter_3450();
void WEIGHTED_ROUND_ROBIN_Splitter_3452();
void Identity_3284();
void Identity_3286();
void WEIGHTED_ROUND_ROBIN_Joiner_3453();
void WEIGHTED_ROUND_ROBIN_Splitter_3454();
void Identity_3290();
void Identity_3292();
void WEIGHTED_ROUND_ROBIN_Joiner_3455();
void WEIGHTED_ROUND_ROBIN_Joiner_3451();
void WEIGHTED_ROUND_ROBIN_Joiner_3443();
void WEIGHTED_ROUND_ROBIN_Splitter_3456();
void WEIGHTED_ROUND_ROBIN_Splitter_3458();
void WEIGHTED_ROUND_ROBIN_Splitter_3460();
void Identity_3300();
void Identity_3302();
void WEIGHTED_ROUND_ROBIN_Joiner_3461();
void WEIGHTED_ROUND_ROBIN_Splitter_3462();
void Identity_3306();
void Identity_3308();
void WEIGHTED_ROUND_ROBIN_Joiner_3463();
void WEIGHTED_ROUND_ROBIN_Joiner_3459();
void WEIGHTED_ROUND_ROBIN_Splitter_3464();
void WEIGHTED_ROUND_ROBIN_Splitter_3466();
void Identity_3314();
void Identity_3316();
void WEIGHTED_ROUND_ROBIN_Joiner_3467();
void WEIGHTED_ROUND_ROBIN_Splitter_3468();
void Identity_3320();
void Identity_3322();
void WEIGHTED_ROUND_ROBIN_Joiner_3469();
void WEIGHTED_ROUND_ROBIN_Joiner_3465();
void WEIGHTED_ROUND_ROBIN_Joiner_3457();
void WEIGHTED_ROUND_ROBIN_Joiner_3441();
void WEIGHTED_ROUND_ROBIN_Splitter_3584();
void WEIGHTED_ROUND_ROBIN_Splitter_3585();
void Pre_CollapsedDataParallel_1(buffer_complex_t *chanin, buffer_complex_t *chanout);
void Pre_CollapsedDataParallel_1_3417();
void butterfly(buffer_complex_t *chanin, buffer_complex_t *chanout);
void butterfly_3324();
void Post_CollapsedDataParallel_2(buffer_complex_t *chanin, buffer_complex_t *chanout);
void Post_CollapsedDataParallel_2_3418();
void Pre_CollapsedDataParallel_1_3420();
void butterfly_3325();
void Post_CollapsedDataParallel_2_3421();
void Pre_CollapsedDataParallel_1_3423();
void butterfly_3326();
void Post_CollapsedDataParallel_2_3424();
void Pre_CollapsedDataParallel_1_3426();
void butterfly_3327();
void Post_CollapsedDataParallel_2_3427();
void WEIGHTED_ROUND_ROBIN_Joiner_3586();
void WEIGHTED_ROUND_ROBIN_Splitter_3587();
void Pre_CollapsedDataParallel_1_3429();
void butterfly_3328();
void Post_CollapsedDataParallel_2_3430();
void Pre_CollapsedDataParallel_1_3432();
void butterfly_3329();
void Post_CollapsedDataParallel_2_3433();
void Pre_CollapsedDataParallel_1_3435();
void butterfly_3330();
void Post_CollapsedDataParallel_2_3436();
void Pre_CollapsedDataParallel_1_3438();
void butterfly_3331();
void Post_CollapsedDataParallel_2_3439();
void WEIGHTED_ROUND_ROBIN_Joiner_3588();
void WEIGHTED_ROUND_ROBIN_Joiner_3589();
void WEIGHTED_ROUND_ROBIN_Splitter_3590();
void WEIGHTED_ROUND_ROBIN_Splitter_3591();
void WEIGHTED_ROUND_ROBIN_Splitter_3474();
void butterfly_3333();
void butterfly_3334();
void WEIGHTED_ROUND_ROBIN_Joiner_3475();
void WEIGHTED_ROUND_ROBIN_Splitter_3476();
void butterfly_3335();
void butterfly_3336();
void WEIGHTED_ROUND_ROBIN_Joiner_3477();
void WEIGHTED_ROUND_ROBIN_Joiner_3592();
void WEIGHTED_ROUND_ROBIN_Splitter_3593();
void WEIGHTED_ROUND_ROBIN_Splitter_3478();
void butterfly_3337();
void butterfly_3338();
void WEIGHTED_ROUND_ROBIN_Joiner_3479();
void WEIGHTED_ROUND_ROBIN_Splitter_3480();
void butterfly_3339();
void butterfly_3340();
void WEIGHTED_ROUND_ROBIN_Joiner_3481();
void WEIGHTED_ROUND_ROBIN_Joiner_3594();
void WEIGHTED_ROUND_ROBIN_Joiner_3595();
void WEIGHTED_ROUND_ROBIN_Splitter_3482();
void WEIGHTED_ROUND_ROBIN_Splitter_3484();
void butterfly_3342();
void butterfly_3343();
void butterfly_3344();
void butterfly_3345();
void WEIGHTED_ROUND_ROBIN_Joiner_3485();
void WEIGHTED_ROUND_ROBIN_Splitter_3486();
void butterfly_3346();
void butterfly_3347();
void butterfly_3348();
void butterfly_3349();
void WEIGHTED_ROUND_ROBIN_Joiner_3487();
void WEIGHTED_ROUND_ROBIN_Joiner_3483();
void WEIGHTED_ROUND_ROBIN_Splitter_3601();
void magnitude(buffer_complex_t *chanin, buffer_float_t *chanout);
void magnitude_3603();
void magnitude_3604();
void magnitude_3605();
void magnitude_3606();
void magnitude_3607();
void magnitude_3608();
void magnitude_3609();
void magnitude_3610();
void magnitude_3611();
void magnitude_3612();
void magnitude_3613();
void magnitude_3614();
void magnitude_3615();
void WEIGHTED_ROUND_ROBIN_Joiner_3602();
void sink(buffer_float_t *chanin);
void sink_3351();

#ifdef __cplusplus
}
#endif
#endif
