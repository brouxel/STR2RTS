#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=12800 on the compile command line
#else
#if BUF_SIZEMAX < 12800
#error BUF_SIZEMAX too small, it must be at least 12800
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float w[2];
} CombineDFT_3656_t;

typedef struct {
	float w[4];
} CombineDFT_3683_t;

typedef struct {
	float w[8];
} CombineDFT_3701_t;

typedef struct {
	float w[16];
} CombineDFT_3711_t;

typedef struct {
	float w[32];
} CombineDFT_3717_t;

typedef struct {
	float w[64];
} CombineDFT_3591_t;
void WEIGHTED_ROUND_ROBIN_Splitter_3612();
void FFTTestSource_3614();
void FFTTestSource_3615();
void WEIGHTED_ROUND_ROBIN_Joiner_3613();
void WEIGHTED_ROUND_ROBIN_Splitter_3604();
void FFTReorderSimple_3581();
void WEIGHTED_ROUND_ROBIN_Splitter_3616();
void FFTReorderSimple_3618();
void FFTReorderSimple_3619();
void WEIGHTED_ROUND_ROBIN_Joiner_3617();
void WEIGHTED_ROUND_ROBIN_Splitter_3620();
void FFTReorderSimple_3622();
void FFTReorderSimple_3623();
void FFTReorderSimple_3624();
void FFTReorderSimple_3625();
void WEIGHTED_ROUND_ROBIN_Joiner_3621();
void WEIGHTED_ROUND_ROBIN_Splitter_3626();
void FFTReorderSimple_3628();
void FFTReorderSimple_3629();
void FFTReorderSimple_3630();
void FFTReorderSimple_3631();
void FFTReorderSimple_3632();
void FFTReorderSimple_3633();
void FFTReorderSimple_3634();
void FFTReorderSimple_3635();
void WEIGHTED_ROUND_ROBIN_Joiner_3627();
void WEIGHTED_ROUND_ROBIN_Splitter_3636();
void FFTReorderSimple_3638();
void FFTReorderSimple_3639();
void FFTReorderSimple_3640();
void FFTReorderSimple_3641();
void FFTReorderSimple_3642();
void FFTReorderSimple_3643();
void FFTReorderSimple_3644();
void FFTReorderSimple_3645();
void FFTReorderSimple_3646();
void FFTReorderSimple_3647();
void FFTReorderSimple_3648();
void FFTReorderSimple_3649();
void FFTReorderSimple_3650();
void FFTReorderSimple_3651();
void FFTReorderSimple_3652();
void FFTReorderSimple_3653();
void WEIGHTED_ROUND_ROBIN_Joiner_3637();
void WEIGHTED_ROUND_ROBIN_Splitter_3654();
void CombineDFT_3656();
void CombineDFT_3657();
void CombineDFT_3658();
void CombineDFT_3659();
void CombineDFT_3660();
void CombineDFT_3661();
void CombineDFT_3662();
void CombineDFT_3663();
void CombineDFT_3664();
void CombineDFT_3665();
void CombineDFT_3666();
void CombineDFT_3667();
void CombineDFT_3668();
void CombineDFT_3669();
void CombineDFT_3670();
void CombineDFT_3671();
void CombineDFT_3672();
void CombineDFT_3673();
void CombineDFT_3674();
void CombineDFT_3675();
void CombineDFT_3676();
void CombineDFT_3677();
void CombineDFT_3678();
void CombineDFT_3679();
void CombineDFT_3680();
void WEIGHTED_ROUND_ROBIN_Joiner_3655();
void WEIGHTED_ROUND_ROBIN_Splitter_3681();
void CombineDFT_3683();
void CombineDFT_3684();
void CombineDFT_3685();
void CombineDFT_3686();
void CombineDFT_3687();
void CombineDFT_3688();
void CombineDFT_3689();
void CombineDFT_3690();
void CombineDFT_3691();
void CombineDFT_3692();
void CombineDFT_3693();
void CombineDFT_3694();
void CombineDFT_3695();
void CombineDFT_3696();
void CombineDFT_3697();
void CombineDFT_3698();
void WEIGHTED_ROUND_ROBIN_Joiner_3682();
void WEIGHTED_ROUND_ROBIN_Splitter_3699();
void CombineDFT_3701();
void CombineDFT_3702();
void CombineDFT_3703();
void CombineDFT_3704();
void CombineDFT_3705();
void CombineDFT_3706();
void CombineDFT_3707();
void CombineDFT_3708();
void WEIGHTED_ROUND_ROBIN_Joiner_3700();
void WEIGHTED_ROUND_ROBIN_Splitter_3709();
void CombineDFT_3711();
void CombineDFT_3712();
void CombineDFT_3713();
void CombineDFT_3714();
void WEIGHTED_ROUND_ROBIN_Joiner_3710();
void WEIGHTED_ROUND_ROBIN_Splitter_3715();
void CombineDFT_3717();
void CombineDFT_3718();
void WEIGHTED_ROUND_ROBIN_Joiner_3716();
void CombineDFT_3591();
void FFTReorderSimple_3592();
void WEIGHTED_ROUND_ROBIN_Splitter_3719();
void FFTReorderSimple_3721();
void FFTReorderSimple_3722();
void WEIGHTED_ROUND_ROBIN_Joiner_3720();
void WEIGHTED_ROUND_ROBIN_Splitter_3723();
void FFTReorderSimple_3725();
void FFTReorderSimple_3726();
void FFTReorderSimple_3727();
void FFTReorderSimple_3728();
void WEIGHTED_ROUND_ROBIN_Joiner_3724();
void WEIGHTED_ROUND_ROBIN_Splitter_3729();
void FFTReorderSimple_3731();
void FFTReorderSimple_3732();
void FFTReorderSimple_3733();
void FFTReorderSimple_3734();
void FFTReorderSimple_3735();
void FFTReorderSimple_3736();
void FFTReorderSimple_3737();
void FFTReorderSimple_3738();
void WEIGHTED_ROUND_ROBIN_Joiner_3730();
void WEIGHTED_ROUND_ROBIN_Splitter_3739();
void FFTReorderSimple_3741();
void FFTReorderSimple_3742();
void FFTReorderSimple_3743();
void FFTReorderSimple_3744();
void FFTReorderSimple_3745();
void FFTReorderSimple_3746();
void FFTReorderSimple_3747();
void FFTReorderSimple_3748();
void FFTReorderSimple_3749();
void FFTReorderSimple_3750();
void FFTReorderSimple_3751();
void FFTReorderSimple_3752();
void FFTReorderSimple_3753();
void FFTReorderSimple_3754();
void FFTReorderSimple_3755();
void FFTReorderSimple_3756();
void WEIGHTED_ROUND_ROBIN_Joiner_3740();
void WEIGHTED_ROUND_ROBIN_Splitter_3757();
void CombineDFT_3759();
void CombineDFT_3760();
void CombineDFT_3761();
void CombineDFT_3762();
void CombineDFT_3763();
void CombineDFT_3764();
void CombineDFT_3765();
void CombineDFT_3766();
void CombineDFT_3767();
void CombineDFT_3768();
void CombineDFT_3769();
void CombineDFT_3770();
void CombineDFT_3771();
void CombineDFT_3772();
void CombineDFT_3773();
void CombineDFT_3774();
void CombineDFT_3775();
void CombineDFT_3776();
void CombineDFT_3777();
void CombineDFT_3778();
void CombineDFT_3779();
void CombineDFT_3780();
void CombineDFT_3781();
void CombineDFT_3782();
void CombineDFT_3783();
void WEIGHTED_ROUND_ROBIN_Joiner_3758();
void WEIGHTED_ROUND_ROBIN_Splitter_3784();
void CombineDFT_3786();
void CombineDFT_3787();
void CombineDFT_3788();
void CombineDFT_3789();
void CombineDFT_3790();
void CombineDFT_3791();
void CombineDFT_3792();
void CombineDFT_3793();
void CombineDFT_3794();
void CombineDFT_3795();
void CombineDFT_3796();
void CombineDFT_3797();
void CombineDFT_3798();
void CombineDFT_3799();
void CombineDFT_3800();
void CombineDFT_3801();
void WEIGHTED_ROUND_ROBIN_Joiner_3785();
void WEIGHTED_ROUND_ROBIN_Splitter_3802();
void CombineDFT_3804();
void CombineDFT_3805();
void CombineDFT_3806();
void CombineDFT_3807();
void CombineDFT_3808();
void CombineDFT_3809();
void CombineDFT_3810();
void CombineDFT_3811();
void WEIGHTED_ROUND_ROBIN_Joiner_3803();
void WEIGHTED_ROUND_ROBIN_Splitter_3812();
void CombineDFT_3814();
void CombineDFT_3815();
void CombineDFT_3816();
void CombineDFT_3817();
void WEIGHTED_ROUND_ROBIN_Joiner_3813();
void WEIGHTED_ROUND_ROBIN_Splitter_3818();
void CombineDFT_3820();
void CombineDFT_3821();
void WEIGHTED_ROUND_ROBIN_Joiner_3819();
void CombineDFT_3602();
void WEIGHTED_ROUND_ROBIN_Joiner_3605();
void FloatPrinter_3603();

#ifdef __cplusplus
}
#endif
#endif
