#include "PEG9-FFT3_nocache.h"

buffer_float_t SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_child1_6987_7146_join[2];
buffer_float_t SplitJoin43_Butterfly_Fiss_7128_7153_join[2];
buffer_float_t SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7023WEIGHTED_ROUND_ROBIN_Splitter_7024;
buffer_float_t SplitJoin85_Butterfly_Fiss_7135_7145_split[8];
buffer_float_t SplitJoin72_Butterfly_Fiss_7134_7144_join[4];
buffer_float_t Pre_CollapsedDataParallel_1_6889WEIGHTED_ROUND_ROBIN_Splitter_7031;
buffer_float_t SplitJoin4_Butterfly_Fiss_7120_7141_split[8];
buffer_float_t SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child1_7012_7155_split[4];
buffer_float_t SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[8];
buffer_float_t SplitJoin8_Butterfly_Fiss_7121_7143_join[4];
buffer_float_t SplitJoin0_Butterfly_Fiss_7118_7139_split[9];
buffer_float_t Pre_CollapsedDataParallel_1_6898WEIGHTED_ROUND_ROBIN_Splitter_7052;
buffer_float_t Pre_CollapsedDataParallel_1_6916WEIGHTED_ROUND_ROBIN_Splitter_7094;
buffer_float_t Pre_CollapsedDataParallel_1_6910WEIGHTED_ROUND_ROBIN_Splitter_7086;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7059Post_CollapsedDataParallel_2_6902;
buffer_float_t SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child0_7011_7150_join[4];
buffer_float_t SplitJoin47_Butterfly_Fiss_7129_7154_split[2];
buffer_float_t SplitJoin4_Butterfly_Fiss_7120_7141_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7043Post_CollapsedDataParallel_2_6893;
buffer_float_t Pre_CollapsedDataParallel_1_6931WEIGHTED_ROUND_ROBIN_Splitter_7114;
buffer_float_t SplitJoin61_Butterfly_Fiss_7132_7158_split[2];
buffer_float_t SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7075Post_CollapsedDataParallel_2_6905;
buffer_float_t Post_CollapsedDataParallel_2_6896WEIGHTED_ROUND_ROBIN_Splitter_7015;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7017WEIGHTED_ROUND_ROBIN_Splitter_7018;
buffer_float_t SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_child0_6982_7142_join[2];
buffer_float_t SplitJoin89_Butterfly_Fiss_7136_7147_split[4];
buffer_float_t SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child0_7011_7150_split[4];
buffer_float_t Pre_CollapsedDataParallel_1_6925WEIGHTED_ROUND_ROBIN_Splitter_7106;
buffer_float_t SplitJoin8_Butterfly_Fiss_7121_7143_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7115Post_CollapsedDataParallel_2_6932;
buffer_float_t SplitJoin65_Butterfly_Fiss_7133_7159_join[2];
buffer_float_t SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_Hier_7122_7149_join[2];
buffer_float_t SplitJoin47_Butterfly_Fiss_7129_7154_join[2];
buffer_float_t Pre_CollapsedDataParallel_1_6928WEIGHTED_ROUND_ROBIN_Splitter_7110;
buffer_float_t SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_child1_6987_7146_split[2];
buffer_float_t SplitJoin89_Butterfly_Fiss_7136_7147_join[4];
buffer_float_t SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_Hier_7124_7160_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7029BitReverse_6669;
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_6546_6941_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_Hier_7119_7140_split[2];
buffer_float_t SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_Hier_7122_7149_split[2];
buffer_float_t Post_CollapsedDataParallel_2_6890WEIGHTED_ROUND_ROBIN_Splitter_6933;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7099Post_CollapsedDataParallel_2_6920;
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_6546_6941_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_Hier_7119_7140_join[2];
buffer_float_t SplitJoin85_Butterfly_Fiss_7135_7145_join[8];
buffer_float_t SplitJoin61_Butterfly_Fiss_7132_7158_join[2];
buffer_float_t SplitJoin65_Butterfly_Fiss_7133_7159_split[2];
buffer_float_t Pre_CollapsedDataParallel_1_6913WEIGHTED_ROUND_ROBIN_Splitter_7090;
buffer_float_t SplitJoin72_Butterfly_Fiss_7134_7144_split[4];
buffer_float_t Pre_CollapsedDataParallel_1_6904WEIGHTED_ROUND_ROBIN_Splitter_7074;
buffer_float_t SplitJoin0_Butterfly_Fiss_7118_7139_join[9];
buffer_float_t SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_Hier_7124_7160_split[2];
buffer_float_t FloatSource_6588Pre_CollapsedDataParallel_1_6889;
buffer_float_t SplitJoin39_Butterfly_Fiss_7127_7152_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7065Post_CollapsedDataParallel_2_6896;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7095Post_CollapsedDataParallel_2_6917;
buffer_float_t SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7091Post_CollapsedDataParallel_2_6914;
buffer_float_t Pre_CollapsedDataParallel_1_6919WEIGHTED_ROUND_ROBIN_Splitter_7098;
buffer_float_t SplitJoin43_Butterfly_Fiss_7128_7153_split[2];
buffer_float_t Post_CollapsedDataParallel_2_6893WEIGHTED_ROUND_ROBIN_Splitter_7013;
buffer_float_t SplitJoin14_Butterfly_Fiss_7123_7151_join[2];
buffer_float_t Pre_CollapsedDataParallel_1_6892WEIGHTED_ROUND_ROBIN_Splitter_7042;
buffer_float_t SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_child0_6982_7142_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7111Post_CollapsedDataParallel_2_6929;
buffer_float_t SplitJoin95_Butterfly_Fiss_7137_7148_join[4];
buffer_float_t SplitJoin53_Butterfly_Fiss_7130_7156_split[2];
buffer_float_t SplitJoin57_Butterfly_Fiss_7131_7157_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7107Post_CollapsedDataParallel_2_6926;
buffer_float_t SplitJoin57_Butterfly_Fiss_7131_7157_split[2];
buffer_float_t SplitJoin95_Butterfly_Fiss_7137_7148_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7087Post_CollapsedDataParallel_2_6911;
buffer_float_t Pre_CollapsedDataParallel_1_6922WEIGHTED_ROUND_ROBIN_Splitter_7102;
buffer_float_t SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child1_7012_7155_join[4];
buffer_float_t Pre_CollapsedDataParallel_1_6895WEIGHTED_ROUND_ROBIN_Splitter_7064;
buffer_float_t Pre_CollapsedDataParallel_1_6907WEIGHTED_ROUND_ROBIN_Splitter_7080;
buffer_float_t SplitJoin14_Butterfly_Fiss_7123_7151_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7032Post_CollapsedDataParallel_2_6890;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7081Post_CollapsedDataParallel_2_6908;
buffer_float_t SplitJoin53_Butterfly_Fiss_7130_7156_join[2];
buffer_float_t BitReverse_6669FloatPrinter_6670;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7053Post_CollapsedDataParallel_2_6899;
buffer_float_t Pre_CollapsedDataParallel_1_6901WEIGHTED_ROUND_ROBIN_Splitter_7058;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7103Post_CollapsedDataParallel_2_6923;
buffer_float_t SplitJoin39_Butterfly_Fiss_7127_7152_join[2];


FloatSource_6588_t FloatSource_6588_s;

void FloatSource_6588(){
	FOR(uint32_t, __iter_steady_, 0, <, 288, __iter_steady_++) {
		push_float(&FloatSource_6588Pre_CollapsedDataParallel_1_6889, FloatSource_6588_s.A_re[FloatSource_6588_s.idx]) ; 
		push_float(&FloatSource_6588Pre_CollapsedDataParallel_1_6889, FloatSource_6588_s.A_im[FloatSource_6588_s.idx]) ; 
		FloatSource_6588_s.idx++ ; 
		if((FloatSource_6588_s.idx >= 32)) {
			FloatSource_6588_s.idx = 0 ; 
		}
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_6889(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
 {
		int partialSum_k = 0;
 {
		FOR(int, _k, 0,  < , 16, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k;
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Pre_CollapsedDataParallel_1_6889WEIGHTED_ROUND_ROBIN_Splitter_7031, peek_float(&FloatSource_6588Pre_CollapsedDataParallel_1_6889, (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 32) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&FloatSource_6588Pre_CollapsedDataParallel_1_6889) ; 
	}
	ENDFOR
}

void Butterfly_7033(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[0]) ; 
		u_im = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[0]) ; 
		t_re = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[0]) ; 
		t_im = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[0]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[0], u_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[0], u_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[0], t_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7034(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[1]) ; 
		u_im = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[1]) ; 
		t_re = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[1]) ; 
		t_im = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[1]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[1], u_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[1], u_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[1], t_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[1], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7035(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[2]) ; 
		u_im = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[2]) ; 
		t_re = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[2]) ; 
		t_im = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[2]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[2], u_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[2], u_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[2], t_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[2], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7036(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[3]) ; 
		u_im = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[3]) ; 
		t_re = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[3]) ; 
		t_im = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[3]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[3], u_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[3], u_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[3], t_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[3], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7037(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[4]) ; 
		u_im = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[4]) ; 
		t_re = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[4]) ; 
		t_im = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[4]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[4], u_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[4], u_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[4], t_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[4], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7038(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[5]) ; 
		u_im = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[5]) ; 
		t_re = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[5]) ; 
		t_im = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[5]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[5], u_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[5], u_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[5], t_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[5], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7039(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[6]) ; 
		u_im = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[6]) ; 
		t_re = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[6]) ; 
		t_im = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[6]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[6], u_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[6], u_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[6], t_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[6], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7040(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[7]) ; 
		u_im = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[7]) ; 
		t_re = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[7]) ; 
		t_im = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[7]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[7], u_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[7], u_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[7], t_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[7], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7041(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[8]) ; 
		u_im = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[8]) ; 
		t_re = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[8]) ; 
		t_im = pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[8]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[8], u_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[8], u_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[8], t_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[8], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7031() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 9, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_6889WEIGHTED_ROUND_ROBIN_Splitter_7031));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7032() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 9, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7032Post_CollapsedDataParallel_2_6890, pop_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_6890(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
 {
		int kTimesWeights_i = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 16, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Post_CollapsedDataParallel_2_6890WEIGHTED_ROUND_ROBIN_Splitter_6933, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_7032Post_CollapsedDataParallel_2_6890, (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7032Post_CollapsedDataParallel_2_6890) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_6892(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
 {
		int partialSum_k = 0;
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Pre_CollapsedDataParallel_1_6892WEIGHTED_ROUND_ROBIN_Splitter_7042, peek_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_6546_6941_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_Hier_7119_7140_split[0], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 16) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_6546_6941_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_Hier_7119_7140_split[0]) ; 
	}
	ENDFOR
}

void Butterfly_7044(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[0]) ; 
		u_im = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[0]) ; 
		t_re = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[0]) ; 
		t_im = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[0]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[0], u_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[0], u_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[0], t_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7045(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[1]) ; 
		u_im = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[1]) ; 
		t_re = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[1]) ; 
		t_im = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[1]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[1], u_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[1], u_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[1], t_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[1], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7046(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[2]) ; 
		u_im = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[2]) ; 
		t_re = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[2]) ; 
		t_im = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[2]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[2], u_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[2], u_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[2], t_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[2], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7047(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[3]) ; 
		u_im = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[3]) ; 
		t_re = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[3]) ; 
		t_im = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[3]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[3], u_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[3], u_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[3], t_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[3], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7048(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[4]) ; 
		u_im = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[4]) ; 
		t_re = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[4]) ; 
		t_im = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[4]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[4], u_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[4], u_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[4], t_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[4], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7049(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[5]) ; 
		u_im = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[5]) ; 
		t_re = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[5]) ; 
		t_im = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[5]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[5], u_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[5], u_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[5], t_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[5], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7050(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[6]) ; 
		u_im = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[6]) ; 
		t_re = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[6]) ; 
		t_im = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[6]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[6], u_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[6], u_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[6], t_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[6], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7051(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[7]) ; 
		u_im = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[7]) ; 
		t_re = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[7]) ; 
		t_im = pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[7]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[7], u_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[7], u_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[7], t_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[7], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7042() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_6892WEIGHTED_ROUND_ROBIN_Splitter_7042));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7043() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7043Post_CollapsedDataParallel_2_6893, pop_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_6893(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
 {
		int kTimesWeights_i = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Post_CollapsedDataParallel_2_6893WEIGHTED_ROUND_ROBIN_Splitter_7013, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_7043Post_CollapsedDataParallel_2_6893, (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7043Post_CollapsedDataParallel_2_6893) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_6898(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
 {
		int partialSum_k = 0;
		partialSum_k = 0 ; 
 {
		FOR(int, _k, 0,  < , 4, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Pre_CollapsedDataParallel_1_6898WEIGHTED_ROUND_ROBIN_Splitter_7052, peek_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_child0_6982_7142_split[0], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_child0_6982_7142_split[0]) ; 
	}
	ENDFOR
}

void Butterfly_7054(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin8_Butterfly_Fiss_7121_7143_split[0]) ; 
		u_im = pop_float(&SplitJoin8_Butterfly_Fiss_7121_7143_split[0]) ; 
		t_re = pop_float(&SplitJoin8_Butterfly_Fiss_7121_7143_split[0]) ; 
		t_im = pop_float(&SplitJoin8_Butterfly_Fiss_7121_7143_split[0]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_7121_7143_join[0], u_re) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_7121_7143_join[0], u_im) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_7121_7143_join[0], t_re) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_7121_7143_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7055(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin8_Butterfly_Fiss_7121_7143_split[1]) ; 
		u_im = pop_float(&SplitJoin8_Butterfly_Fiss_7121_7143_split[1]) ; 
		t_re = pop_float(&SplitJoin8_Butterfly_Fiss_7121_7143_split[1]) ; 
		t_im = pop_float(&SplitJoin8_Butterfly_Fiss_7121_7143_split[1]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_7121_7143_join[1], u_re) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_7121_7143_join[1], u_im) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_7121_7143_join[1], t_re) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_7121_7143_join[1], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7056(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin8_Butterfly_Fiss_7121_7143_split[2]) ; 
		u_im = pop_float(&SplitJoin8_Butterfly_Fiss_7121_7143_split[2]) ; 
		t_re = pop_float(&SplitJoin8_Butterfly_Fiss_7121_7143_split[2]) ; 
		t_im = pop_float(&SplitJoin8_Butterfly_Fiss_7121_7143_split[2]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_7121_7143_join[2], u_re) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_7121_7143_join[2], u_im) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_7121_7143_join[2], t_re) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_7121_7143_join[2], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7057(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin8_Butterfly_Fiss_7121_7143_split[3]) ; 
		u_im = pop_float(&SplitJoin8_Butterfly_Fiss_7121_7143_split[3]) ; 
		t_re = pop_float(&SplitJoin8_Butterfly_Fiss_7121_7143_split[3]) ; 
		t_im = pop_float(&SplitJoin8_Butterfly_Fiss_7121_7143_split[3]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_7121_7143_join[3], u_re) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_7121_7143_join[3], u_im) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_7121_7143_join[3], t_re) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_7121_7143_join[3], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7052() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin8_Butterfly_Fiss_7121_7143_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_6898WEIGHTED_ROUND_ROBIN_Splitter_7052));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7053() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7053Post_CollapsedDataParallel_2_6899, pop_float(&SplitJoin8_Butterfly_Fiss_7121_7143_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_6899(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
 {
		int kTimesWeights_i = 0;
		kTimesWeights_i = 0 ; 
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 4, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_child0_6982_7142_join[0], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_7053Post_CollapsedDataParallel_2_6899, (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7053Post_CollapsedDataParallel_2_6899) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_6901(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
 {
		int partialSum_k = 0;
		partialSum_k = 0 ; 
 {
		FOR(int, _k, 0,  < , 4, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Pre_CollapsedDataParallel_1_6901WEIGHTED_ROUND_ROBIN_Splitter_7058, peek_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_child0_6982_7142_split[1], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_child0_6982_7142_split[1]) ; 
	}
	ENDFOR
}

void Butterfly_7060(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin72_Butterfly_Fiss_7134_7144_split[0]) ; 
		u_im = pop_float(&SplitJoin72_Butterfly_Fiss_7134_7144_split[0]) ; 
		t_re = pop_float(&SplitJoin72_Butterfly_Fiss_7134_7144_split[0]) ; 
		t_im = pop_float(&SplitJoin72_Butterfly_Fiss_7134_7144_split[0]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_7134_7144_join[0], u_re) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_7134_7144_join[0], u_im) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_7134_7144_join[0], t_re) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_7134_7144_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7061(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin72_Butterfly_Fiss_7134_7144_split[1]) ; 
		u_im = pop_float(&SplitJoin72_Butterfly_Fiss_7134_7144_split[1]) ; 
		t_re = pop_float(&SplitJoin72_Butterfly_Fiss_7134_7144_split[1]) ; 
		t_im = pop_float(&SplitJoin72_Butterfly_Fiss_7134_7144_split[1]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_7134_7144_join[1], u_re) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_7134_7144_join[1], u_im) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_7134_7144_join[1], t_re) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_7134_7144_join[1], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7062(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin72_Butterfly_Fiss_7134_7144_split[2]) ; 
		u_im = pop_float(&SplitJoin72_Butterfly_Fiss_7134_7144_split[2]) ; 
		t_re = pop_float(&SplitJoin72_Butterfly_Fiss_7134_7144_split[2]) ; 
		t_im = pop_float(&SplitJoin72_Butterfly_Fiss_7134_7144_split[2]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_7134_7144_join[2], u_re) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_7134_7144_join[2], u_im) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_7134_7144_join[2], t_re) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_7134_7144_join[2], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7063(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin72_Butterfly_Fiss_7134_7144_split[3]) ; 
		u_im = pop_float(&SplitJoin72_Butterfly_Fiss_7134_7144_split[3]) ; 
		t_re = pop_float(&SplitJoin72_Butterfly_Fiss_7134_7144_split[3]) ; 
		t_im = pop_float(&SplitJoin72_Butterfly_Fiss_7134_7144_split[3]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_7134_7144_join[3], u_re) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_7134_7144_join[3], u_im) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_7134_7144_join[3], t_re) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_7134_7144_join[3], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7058() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin72_Butterfly_Fiss_7134_7144_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_6901WEIGHTED_ROUND_ROBIN_Splitter_7058));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7059() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7059Post_CollapsedDataParallel_2_6902, pop_float(&SplitJoin72_Butterfly_Fiss_7134_7144_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_6902(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
 {
		int kTimesWeights_i = 0;
		kTimesWeights_i = 0 ; 
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 4, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_child0_6982_7142_join[1], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_7059Post_CollapsedDataParallel_2_6902, (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7059Post_CollapsedDataParallel_2_6902) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7013() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_child0_6982_7142_split[0], pop_float(&Post_CollapsedDataParallel_2_6893WEIGHTED_ROUND_ROBIN_Splitter_7013));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_child0_6982_7142_split[1], pop_float(&Post_CollapsedDataParallel_2_6893WEIGHTED_ROUND_ROBIN_Splitter_7013));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7014() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_6546_6941_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_Hier_7119_7140_join[0], pop_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_child0_6982_7142_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_6546_6941_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_Hier_7119_7140_join[0], pop_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_child0_6982_7142_join[1]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_6895(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
 {
		int partialSum_k = 0;
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Pre_CollapsedDataParallel_1_6895WEIGHTED_ROUND_ROBIN_Splitter_7064, peek_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_6546_6941_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_Hier_7119_7140_split[1], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 16) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_6546_6941_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_Hier_7119_7140_split[1]) ; 
	}
	ENDFOR
}

void Butterfly_7066(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[0]) ; 
		u_im = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[0]) ; 
		t_re = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[0]) ; 
		t_im = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[0]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[0], u_re) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[0], u_im) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[0], t_re) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7067(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[1]) ; 
		u_im = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[1]) ; 
		t_re = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[1]) ; 
		t_im = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[1]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[1], u_re) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[1], u_im) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[1], t_re) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[1], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7068(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[2]) ; 
		u_im = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[2]) ; 
		t_re = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[2]) ; 
		t_im = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[2]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[2], u_re) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[2], u_im) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[2], t_re) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[2], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7069(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[3]) ; 
		u_im = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[3]) ; 
		t_re = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[3]) ; 
		t_im = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[3]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[3], u_re) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[3], u_im) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[3], t_re) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[3], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7070(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[4]) ; 
		u_im = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[4]) ; 
		t_re = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[4]) ; 
		t_im = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[4]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[4], u_re) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[4], u_im) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[4], t_re) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[4], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7071(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[5]) ; 
		u_im = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[5]) ; 
		t_re = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[5]) ; 
		t_im = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[5]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[5], u_re) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[5], u_im) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[5], t_re) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[5], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7072(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[6]) ; 
		u_im = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[6]) ; 
		t_re = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[6]) ; 
		t_im = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[6]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[6], u_re) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[6], u_im) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[6], t_re) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[6], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7073(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[7]) ; 
		u_im = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[7]) ; 
		t_re = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[7]) ; 
		t_im = pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[7]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[7], u_re) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[7], u_im) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[7], t_re) ; 
		push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[7], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7064() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_6895WEIGHTED_ROUND_ROBIN_Splitter_7064));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7065() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7065Post_CollapsedDataParallel_2_6896, pop_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_6896(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
 {
		int kTimesWeights_i = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Post_CollapsedDataParallel_2_6896WEIGHTED_ROUND_ROBIN_Splitter_7015, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_7065Post_CollapsedDataParallel_2_6896, (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7065Post_CollapsedDataParallel_2_6896) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_6904(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
 {
		int partialSum_k = 0;
		partialSum_k = 0 ; 
 {
		FOR(int, _k, 0,  < , 4, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Pre_CollapsedDataParallel_1_6904WEIGHTED_ROUND_ROBIN_Splitter_7074, peek_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_child1_6987_7146_split[0], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_child1_6987_7146_split[0]) ; 
	}
	ENDFOR
}

void Butterfly_7076(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin89_Butterfly_Fiss_7136_7147_split[0]) ; 
		u_im = pop_float(&SplitJoin89_Butterfly_Fiss_7136_7147_split[0]) ; 
		t_re = pop_float(&SplitJoin89_Butterfly_Fiss_7136_7147_split[0]) ; 
		t_im = pop_float(&SplitJoin89_Butterfly_Fiss_7136_7147_split[0]) ; 
		wt_re = ((0.70710677 * t_re) - (0.70710677 * t_im)) ; 
		wt_im = ((0.70710677 * t_im) + (0.70710677 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin89_Butterfly_Fiss_7136_7147_join[0], u_re) ; 
		push_float(&SplitJoin89_Butterfly_Fiss_7136_7147_join[0], u_im) ; 
		push_float(&SplitJoin89_Butterfly_Fiss_7136_7147_join[0], t_re) ; 
		push_float(&SplitJoin89_Butterfly_Fiss_7136_7147_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7077(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin89_Butterfly_Fiss_7136_7147_split[1]) ; 
		u_im = pop_float(&SplitJoin89_Butterfly_Fiss_7136_7147_split[1]) ; 
		t_re = pop_float(&SplitJoin89_Butterfly_Fiss_7136_7147_split[1]) ; 
		t_im = pop_float(&SplitJoin89_Butterfly_Fiss_7136_7147_split[1]) ; 
		wt_re = ((0.70710677 * t_re) - (0.70710677 * t_im)) ; 
		wt_im = ((0.70710677 * t_im) + (0.70710677 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin89_Butterfly_Fiss_7136_7147_join[1], u_re) ; 
		push_float(&SplitJoin89_Butterfly_Fiss_7136_7147_join[1], u_im) ; 
		push_float(&SplitJoin89_Butterfly_Fiss_7136_7147_join[1], t_re) ; 
		push_float(&SplitJoin89_Butterfly_Fiss_7136_7147_join[1], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7078(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin89_Butterfly_Fiss_7136_7147_split[2]) ; 
		u_im = pop_float(&SplitJoin89_Butterfly_Fiss_7136_7147_split[2]) ; 
		t_re = pop_float(&SplitJoin89_Butterfly_Fiss_7136_7147_split[2]) ; 
		t_im = pop_float(&SplitJoin89_Butterfly_Fiss_7136_7147_split[2]) ; 
		wt_re = ((0.70710677 * t_re) - (0.70710677 * t_im)) ; 
		wt_im = ((0.70710677 * t_im) + (0.70710677 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin89_Butterfly_Fiss_7136_7147_join[2], u_re) ; 
		push_float(&SplitJoin89_Butterfly_Fiss_7136_7147_join[2], u_im) ; 
		push_float(&SplitJoin89_Butterfly_Fiss_7136_7147_join[2], t_re) ; 
		push_float(&SplitJoin89_Butterfly_Fiss_7136_7147_join[2], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7079(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin89_Butterfly_Fiss_7136_7147_split[3]) ; 
		u_im = pop_float(&SplitJoin89_Butterfly_Fiss_7136_7147_split[3]) ; 
		t_re = pop_float(&SplitJoin89_Butterfly_Fiss_7136_7147_split[3]) ; 
		t_im = pop_float(&SplitJoin89_Butterfly_Fiss_7136_7147_split[3]) ; 
		wt_re = ((0.70710677 * t_re) - (0.70710677 * t_im)) ; 
		wt_im = ((0.70710677 * t_im) + (0.70710677 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin89_Butterfly_Fiss_7136_7147_join[3], u_re) ; 
		push_float(&SplitJoin89_Butterfly_Fiss_7136_7147_join[3], u_im) ; 
		push_float(&SplitJoin89_Butterfly_Fiss_7136_7147_join[3], t_re) ; 
		push_float(&SplitJoin89_Butterfly_Fiss_7136_7147_join[3], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7074() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin89_Butterfly_Fiss_7136_7147_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_6904WEIGHTED_ROUND_ROBIN_Splitter_7074));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7075() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7075Post_CollapsedDataParallel_2_6905, pop_float(&SplitJoin89_Butterfly_Fiss_7136_7147_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_6905(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
 {
		int kTimesWeights_i = 0;
		kTimesWeights_i = 0 ; 
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 4, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_child1_6987_7146_join[0], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_7075Post_CollapsedDataParallel_2_6905, (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7075Post_CollapsedDataParallel_2_6905) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_6907(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
 {
		int partialSum_k = 0;
		partialSum_k = 0 ; 
 {
		FOR(int, _k, 0,  < , 4, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Pre_CollapsedDataParallel_1_6907WEIGHTED_ROUND_ROBIN_Splitter_7080, peek_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_child1_6987_7146_split[1], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_child1_6987_7146_split[1]) ; 
	}
	ENDFOR
}

void Butterfly_7082(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin95_Butterfly_Fiss_7137_7148_split[0]) ; 
		u_im = pop_float(&SplitJoin95_Butterfly_Fiss_7137_7148_split[0]) ; 
		t_re = pop_float(&SplitJoin95_Butterfly_Fiss_7137_7148_split[0]) ; 
		t_im = pop_float(&SplitJoin95_Butterfly_Fiss_7137_7148_split[0]) ; 
		wt_re = ((-0.70710677 * t_re) - (0.70710677 * t_im)) ; 
		wt_im = ((-0.70710677 * t_im) + (0.70710677 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin95_Butterfly_Fiss_7137_7148_join[0], u_re) ; 
		push_float(&SplitJoin95_Butterfly_Fiss_7137_7148_join[0], u_im) ; 
		push_float(&SplitJoin95_Butterfly_Fiss_7137_7148_join[0], t_re) ; 
		push_float(&SplitJoin95_Butterfly_Fiss_7137_7148_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7083(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin95_Butterfly_Fiss_7137_7148_split[1]) ; 
		u_im = pop_float(&SplitJoin95_Butterfly_Fiss_7137_7148_split[1]) ; 
		t_re = pop_float(&SplitJoin95_Butterfly_Fiss_7137_7148_split[1]) ; 
		t_im = pop_float(&SplitJoin95_Butterfly_Fiss_7137_7148_split[1]) ; 
		wt_re = ((-0.70710677 * t_re) - (0.70710677 * t_im)) ; 
		wt_im = ((-0.70710677 * t_im) + (0.70710677 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin95_Butterfly_Fiss_7137_7148_join[1], u_re) ; 
		push_float(&SplitJoin95_Butterfly_Fiss_7137_7148_join[1], u_im) ; 
		push_float(&SplitJoin95_Butterfly_Fiss_7137_7148_join[1], t_re) ; 
		push_float(&SplitJoin95_Butterfly_Fiss_7137_7148_join[1], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7084(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin95_Butterfly_Fiss_7137_7148_split[2]) ; 
		u_im = pop_float(&SplitJoin95_Butterfly_Fiss_7137_7148_split[2]) ; 
		t_re = pop_float(&SplitJoin95_Butterfly_Fiss_7137_7148_split[2]) ; 
		t_im = pop_float(&SplitJoin95_Butterfly_Fiss_7137_7148_split[2]) ; 
		wt_re = ((-0.70710677 * t_re) - (0.70710677 * t_im)) ; 
		wt_im = ((-0.70710677 * t_im) + (0.70710677 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin95_Butterfly_Fiss_7137_7148_join[2], u_re) ; 
		push_float(&SplitJoin95_Butterfly_Fiss_7137_7148_join[2], u_im) ; 
		push_float(&SplitJoin95_Butterfly_Fiss_7137_7148_join[2], t_re) ; 
		push_float(&SplitJoin95_Butterfly_Fiss_7137_7148_join[2], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7085(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin95_Butterfly_Fiss_7137_7148_split[3]) ; 
		u_im = pop_float(&SplitJoin95_Butterfly_Fiss_7137_7148_split[3]) ; 
		t_re = pop_float(&SplitJoin95_Butterfly_Fiss_7137_7148_split[3]) ; 
		t_im = pop_float(&SplitJoin95_Butterfly_Fiss_7137_7148_split[3]) ; 
		wt_re = ((-0.70710677 * t_re) - (0.70710677 * t_im)) ; 
		wt_im = ((-0.70710677 * t_im) + (0.70710677 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin95_Butterfly_Fiss_7137_7148_join[3], u_re) ; 
		push_float(&SplitJoin95_Butterfly_Fiss_7137_7148_join[3], u_im) ; 
		push_float(&SplitJoin95_Butterfly_Fiss_7137_7148_join[3], t_re) ; 
		push_float(&SplitJoin95_Butterfly_Fiss_7137_7148_join[3], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7080() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin95_Butterfly_Fiss_7137_7148_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_6907WEIGHTED_ROUND_ROBIN_Splitter_7080));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7081() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7081Post_CollapsedDataParallel_2_6908, pop_float(&SplitJoin95_Butterfly_Fiss_7137_7148_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_6908(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
 {
		int kTimesWeights_i = 0;
		kTimesWeights_i = 0 ; 
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 4, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_child1_6987_7146_join[1], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_7081Post_CollapsedDataParallel_2_6908, (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7081Post_CollapsedDataParallel_2_6908) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7015() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_child1_6987_7146_split[0], pop_float(&Post_CollapsedDataParallel_2_6896WEIGHTED_ROUND_ROBIN_Splitter_7015));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_child1_6987_7146_split[1], pop_float(&Post_CollapsedDataParallel_2_6896WEIGHTED_ROUND_ROBIN_Splitter_7015));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7016() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_6546_6941_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_Hier_7119_7140_join[1], pop_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_child1_6987_7146_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_6546_6941_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_Hier_7119_7140_join[1], pop_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_child1_6987_7146_join[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_6933() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_6546_6941_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_Hier_7119_7140_split[0], pop_float(&Post_CollapsedDataParallel_2_6890WEIGHTED_ROUND_ROBIN_Splitter_6933));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_6546_6941_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_Hier_7119_7140_split[1], pop_float(&Post_CollapsedDataParallel_2_6890WEIGHTED_ROUND_ROBIN_Splitter_6933));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7017() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7017WEIGHTED_ROUND_ROBIN_Splitter_7018, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_6546_6941_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_Hier_7119_7140_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7017WEIGHTED_ROUND_ROBIN_Splitter_7018, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_6546_6941_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_Hier_7119_7140_join[1]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_6910(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
 {
		int partialSum_k = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Pre_CollapsedDataParallel_1_6910WEIGHTED_ROUND_ROBIN_Splitter_7086, peek_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child0_7011_7150_split[0], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 4) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child0_7011_7150_split[0]) ; 
	}
	ENDFOR
}

void Butterfly_7088(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin14_Butterfly_Fiss_7123_7151_split[0]) ; 
		u_im = pop_float(&SplitJoin14_Butterfly_Fiss_7123_7151_split[0]) ; 
		t_re = pop_float(&SplitJoin14_Butterfly_Fiss_7123_7151_split[0]) ; 
		t_im = pop_float(&SplitJoin14_Butterfly_Fiss_7123_7151_split[0]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin14_Butterfly_Fiss_7123_7151_join[0], u_re) ; 
		push_float(&SplitJoin14_Butterfly_Fiss_7123_7151_join[0], u_im) ; 
		push_float(&SplitJoin14_Butterfly_Fiss_7123_7151_join[0], t_re) ; 
		push_float(&SplitJoin14_Butterfly_Fiss_7123_7151_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7089(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin14_Butterfly_Fiss_7123_7151_split[1]) ; 
		u_im = pop_float(&SplitJoin14_Butterfly_Fiss_7123_7151_split[1]) ; 
		t_re = pop_float(&SplitJoin14_Butterfly_Fiss_7123_7151_split[1]) ; 
		t_im = pop_float(&SplitJoin14_Butterfly_Fiss_7123_7151_split[1]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin14_Butterfly_Fiss_7123_7151_join[1], u_re) ; 
		push_float(&SplitJoin14_Butterfly_Fiss_7123_7151_join[1], u_im) ; 
		push_float(&SplitJoin14_Butterfly_Fiss_7123_7151_join[1], t_re) ; 
		push_float(&SplitJoin14_Butterfly_Fiss_7123_7151_join[1], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7086() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin14_Butterfly_Fiss_7123_7151_split[0], pop_float(&Pre_CollapsedDataParallel_1_6910WEIGHTED_ROUND_ROBIN_Splitter_7086));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin14_Butterfly_Fiss_7123_7151_split[1], pop_float(&Pre_CollapsedDataParallel_1_6910WEIGHTED_ROUND_ROBIN_Splitter_7086));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7087() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7087Post_CollapsedDataParallel_2_6911, pop_float(&SplitJoin14_Butterfly_Fiss_7123_7151_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7087Post_CollapsedDataParallel_2_6911, pop_float(&SplitJoin14_Butterfly_Fiss_7123_7151_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_6911(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
 {
		int kTimesWeights_i = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child0_7011_7150_join[0], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_7087Post_CollapsedDataParallel_2_6911, (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7087Post_CollapsedDataParallel_2_6911) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_6913(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
 {
		int partialSum_k = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Pre_CollapsedDataParallel_1_6913WEIGHTED_ROUND_ROBIN_Splitter_7090, peek_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child0_7011_7150_split[1], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 4) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child0_7011_7150_split[1]) ; 
	}
	ENDFOR
}

void Butterfly_7092(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin39_Butterfly_Fiss_7127_7152_split[0]) ; 
		u_im = pop_float(&SplitJoin39_Butterfly_Fiss_7127_7152_split[0]) ; 
		t_re = pop_float(&SplitJoin39_Butterfly_Fiss_7127_7152_split[0]) ; 
		t_im = pop_float(&SplitJoin39_Butterfly_Fiss_7127_7152_split[0]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin39_Butterfly_Fiss_7127_7152_join[0], u_re) ; 
		push_float(&SplitJoin39_Butterfly_Fiss_7127_7152_join[0], u_im) ; 
		push_float(&SplitJoin39_Butterfly_Fiss_7127_7152_join[0], t_re) ; 
		push_float(&SplitJoin39_Butterfly_Fiss_7127_7152_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7093(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin39_Butterfly_Fiss_7127_7152_split[1]) ; 
		u_im = pop_float(&SplitJoin39_Butterfly_Fiss_7127_7152_split[1]) ; 
		t_re = pop_float(&SplitJoin39_Butterfly_Fiss_7127_7152_split[1]) ; 
		t_im = pop_float(&SplitJoin39_Butterfly_Fiss_7127_7152_split[1]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin39_Butterfly_Fiss_7127_7152_join[1], u_re) ; 
		push_float(&SplitJoin39_Butterfly_Fiss_7127_7152_join[1], u_im) ; 
		push_float(&SplitJoin39_Butterfly_Fiss_7127_7152_join[1], t_re) ; 
		push_float(&SplitJoin39_Butterfly_Fiss_7127_7152_join[1], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7090() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin39_Butterfly_Fiss_7127_7152_split[0], pop_float(&Pre_CollapsedDataParallel_1_6913WEIGHTED_ROUND_ROBIN_Splitter_7090));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin39_Butterfly_Fiss_7127_7152_split[1], pop_float(&Pre_CollapsedDataParallel_1_6913WEIGHTED_ROUND_ROBIN_Splitter_7090));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7091() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7091Post_CollapsedDataParallel_2_6914, pop_float(&SplitJoin39_Butterfly_Fiss_7127_7152_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7091Post_CollapsedDataParallel_2_6914, pop_float(&SplitJoin39_Butterfly_Fiss_7127_7152_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_6914(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
 {
		int kTimesWeights_i = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child0_7011_7150_join[1], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_7091Post_CollapsedDataParallel_2_6914, (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7091Post_CollapsedDataParallel_2_6914) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_6916(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
 {
		int partialSum_k = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Pre_CollapsedDataParallel_1_6916WEIGHTED_ROUND_ROBIN_Splitter_7094, peek_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child0_7011_7150_split[2], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 4) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child0_7011_7150_split[2]) ; 
	}
	ENDFOR
}

void Butterfly_7096(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin43_Butterfly_Fiss_7128_7153_split[0]) ; 
		u_im = pop_float(&SplitJoin43_Butterfly_Fiss_7128_7153_split[0]) ; 
		t_re = pop_float(&SplitJoin43_Butterfly_Fiss_7128_7153_split[0]) ; 
		t_im = pop_float(&SplitJoin43_Butterfly_Fiss_7128_7153_split[0]) ; 
		wt_re = ((0.70710677 * t_re) - (0.70710677 * t_im)) ; 
		wt_im = ((0.70710677 * t_im) + (0.70710677 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin43_Butterfly_Fiss_7128_7153_join[0], u_re) ; 
		push_float(&SplitJoin43_Butterfly_Fiss_7128_7153_join[0], u_im) ; 
		push_float(&SplitJoin43_Butterfly_Fiss_7128_7153_join[0], t_re) ; 
		push_float(&SplitJoin43_Butterfly_Fiss_7128_7153_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7097(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin43_Butterfly_Fiss_7128_7153_split[1]) ; 
		u_im = pop_float(&SplitJoin43_Butterfly_Fiss_7128_7153_split[1]) ; 
		t_re = pop_float(&SplitJoin43_Butterfly_Fiss_7128_7153_split[1]) ; 
		t_im = pop_float(&SplitJoin43_Butterfly_Fiss_7128_7153_split[1]) ; 
		wt_re = ((0.70710677 * t_re) - (0.70710677 * t_im)) ; 
		wt_im = ((0.70710677 * t_im) + (0.70710677 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin43_Butterfly_Fiss_7128_7153_join[1], u_re) ; 
		push_float(&SplitJoin43_Butterfly_Fiss_7128_7153_join[1], u_im) ; 
		push_float(&SplitJoin43_Butterfly_Fiss_7128_7153_join[1], t_re) ; 
		push_float(&SplitJoin43_Butterfly_Fiss_7128_7153_join[1], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7094() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin43_Butterfly_Fiss_7128_7153_split[0], pop_float(&Pre_CollapsedDataParallel_1_6916WEIGHTED_ROUND_ROBIN_Splitter_7094));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin43_Butterfly_Fiss_7128_7153_split[1], pop_float(&Pre_CollapsedDataParallel_1_6916WEIGHTED_ROUND_ROBIN_Splitter_7094));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7095() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7095Post_CollapsedDataParallel_2_6917, pop_float(&SplitJoin43_Butterfly_Fiss_7128_7153_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7095Post_CollapsedDataParallel_2_6917, pop_float(&SplitJoin43_Butterfly_Fiss_7128_7153_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_6917(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
 {
		int kTimesWeights_i = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child0_7011_7150_join[2], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_7095Post_CollapsedDataParallel_2_6917, (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7095Post_CollapsedDataParallel_2_6917) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_6919(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
 {
		int partialSum_k = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Pre_CollapsedDataParallel_1_6919WEIGHTED_ROUND_ROBIN_Splitter_7098, peek_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child0_7011_7150_split[3], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 4) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child0_7011_7150_split[3]) ; 
	}
	ENDFOR
}

void Butterfly_7100(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin47_Butterfly_Fiss_7129_7154_split[0]) ; 
		u_im = pop_float(&SplitJoin47_Butterfly_Fiss_7129_7154_split[0]) ; 
		t_re = pop_float(&SplitJoin47_Butterfly_Fiss_7129_7154_split[0]) ; 
		t_im = pop_float(&SplitJoin47_Butterfly_Fiss_7129_7154_split[0]) ; 
		wt_re = ((-0.70710677 * t_re) - (0.70710677 * t_im)) ; 
		wt_im = ((-0.70710677 * t_im) + (0.70710677 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin47_Butterfly_Fiss_7129_7154_join[0], u_re) ; 
		push_float(&SplitJoin47_Butterfly_Fiss_7129_7154_join[0], u_im) ; 
		push_float(&SplitJoin47_Butterfly_Fiss_7129_7154_join[0], t_re) ; 
		push_float(&SplitJoin47_Butterfly_Fiss_7129_7154_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7101(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin47_Butterfly_Fiss_7129_7154_split[1]) ; 
		u_im = pop_float(&SplitJoin47_Butterfly_Fiss_7129_7154_split[1]) ; 
		t_re = pop_float(&SplitJoin47_Butterfly_Fiss_7129_7154_split[1]) ; 
		t_im = pop_float(&SplitJoin47_Butterfly_Fiss_7129_7154_split[1]) ; 
		wt_re = ((-0.70710677 * t_re) - (0.70710677 * t_im)) ; 
		wt_im = ((-0.70710677 * t_im) + (0.70710677 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin47_Butterfly_Fiss_7129_7154_join[1], u_re) ; 
		push_float(&SplitJoin47_Butterfly_Fiss_7129_7154_join[1], u_im) ; 
		push_float(&SplitJoin47_Butterfly_Fiss_7129_7154_join[1], t_re) ; 
		push_float(&SplitJoin47_Butterfly_Fiss_7129_7154_join[1], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7098() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin47_Butterfly_Fiss_7129_7154_split[0], pop_float(&Pre_CollapsedDataParallel_1_6919WEIGHTED_ROUND_ROBIN_Splitter_7098));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin47_Butterfly_Fiss_7129_7154_split[1], pop_float(&Pre_CollapsedDataParallel_1_6919WEIGHTED_ROUND_ROBIN_Splitter_7098));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7099() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7099Post_CollapsedDataParallel_2_6920, pop_float(&SplitJoin47_Butterfly_Fiss_7129_7154_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7099Post_CollapsedDataParallel_2_6920, pop_float(&SplitJoin47_Butterfly_Fiss_7129_7154_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_6920(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
 {
		int kTimesWeights_i = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child0_7011_7150_join[3], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_7099Post_CollapsedDataParallel_2_6920, (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7099Post_CollapsedDataParallel_2_6920) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7019() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child0_7011_7150_split[__iter_dec_], pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_Hier_7122_7149_split[0]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7020() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_Hier_7122_7149_join[0], pop_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child0_7011_7150_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_6922(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
 {
		int partialSum_k = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Pre_CollapsedDataParallel_1_6922WEIGHTED_ROUND_ROBIN_Splitter_7102, peek_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child1_7012_7155_split[0], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 4) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child1_7012_7155_split[0]) ; 
	}
	ENDFOR
}

void Butterfly_7104(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin53_Butterfly_Fiss_7130_7156_split[0]) ; 
		u_im = pop_float(&SplitJoin53_Butterfly_Fiss_7130_7156_split[0]) ; 
		t_re = pop_float(&SplitJoin53_Butterfly_Fiss_7130_7156_split[0]) ; 
		t_im = pop_float(&SplitJoin53_Butterfly_Fiss_7130_7156_split[0]) ; 
		wt_re = ((0.9238795 * t_re) - (0.38268346 * t_im)) ; 
		wt_im = ((0.9238795 * t_im) + (0.38268346 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin53_Butterfly_Fiss_7130_7156_join[0], u_re) ; 
		push_float(&SplitJoin53_Butterfly_Fiss_7130_7156_join[0], u_im) ; 
		push_float(&SplitJoin53_Butterfly_Fiss_7130_7156_join[0], t_re) ; 
		push_float(&SplitJoin53_Butterfly_Fiss_7130_7156_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7105(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin53_Butterfly_Fiss_7130_7156_split[1]) ; 
		u_im = pop_float(&SplitJoin53_Butterfly_Fiss_7130_7156_split[1]) ; 
		t_re = pop_float(&SplitJoin53_Butterfly_Fiss_7130_7156_split[1]) ; 
		t_im = pop_float(&SplitJoin53_Butterfly_Fiss_7130_7156_split[1]) ; 
		wt_re = ((0.9238795 * t_re) - (0.38268346 * t_im)) ; 
		wt_im = ((0.9238795 * t_im) + (0.38268346 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin53_Butterfly_Fiss_7130_7156_join[1], u_re) ; 
		push_float(&SplitJoin53_Butterfly_Fiss_7130_7156_join[1], u_im) ; 
		push_float(&SplitJoin53_Butterfly_Fiss_7130_7156_join[1], t_re) ; 
		push_float(&SplitJoin53_Butterfly_Fiss_7130_7156_join[1], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7102() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin53_Butterfly_Fiss_7130_7156_split[0], pop_float(&Pre_CollapsedDataParallel_1_6922WEIGHTED_ROUND_ROBIN_Splitter_7102));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin53_Butterfly_Fiss_7130_7156_split[1], pop_float(&Pre_CollapsedDataParallel_1_6922WEIGHTED_ROUND_ROBIN_Splitter_7102));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7103() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7103Post_CollapsedDataParallel_2_6923, pop_float(&SplitJoin53_Butterfly_Fiss_7130_7156_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7103Post_CollapsedDataParallel_2_6923, pop_float(&SplitJoin53_Butterfly_Fiss_7130_7156_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_6923(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
 {
		int kTimesWeights_i = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child1_7012_7155_join[0], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_7103Post_CollapsedDataParallel_2_6923, (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7103Post_CollapsedDataParallel_2_6923) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_6925(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
 {
		int partialSum_k = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Pre_CollapsedDataParallel_1_6925WEIGHTED_ROUND_ROBIN_Splitter_7106, peek_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child1_7012_7155_split[1], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 4) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child1_7012_7155_split[1]) ; 
	}
	ENDFOR
}

void Butterfly_7108(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin57_Butterfly_Fiss_7131_7157_split[0]) ; 
		u_im = pop_float(&SplitJoin57_Butterfly_Fiss_7131_7157_split[0]) ; 
		t_re = pop_float(&SplitJoin57_Butterfly_Fiss_7131_7157_split[0]) ; 
		t_im = pop_float(&SplitJoin57_Butterfly_Fiss_7131_7157_split[0]) ; 
		wt_re = ((-0.38268352 * t_re) - (0.9238795 * t_im)) ; 
		wt_im = ((-0.38268352 * t_im) + (0.9238795 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin57_Butterfly_Fiss_7131_7157_join[0], u_re) ; 
		push_float(&SplitJoin57_Butterfly_Fiss_7131_7157_join[0], u_im) ; 
		push_float(&SplitJoin57_Butterfly_Fiss_7131_7157_join[0], t_re) ; 
		push_float(&SplitJoin57_Butterfly_Fiss_7131_7157_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7109(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin57_Butterfly_Fiss_7131_7157_split[1]) ; 
		u_im = pop_float(&SplitJoin57_Butterfly_Fiss_7131_7157_split[1]) ; 
		t_re = pop_float(&SplitJoin57_Butterfly_Fiss_7131_7157_split[1]) ; 
		t_im = pop_float(&SplitJoin57_Butterfly_Fiss_7131_7157_split[1]) ; 
		wt_re = ((-0.38268352 * t_re) - (0.9238795 * t_im)) ; 
		wt_im = ((-0.38268352 * t_im) + (0.9238795 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin57_Butterfly_Fiss_7131_7157_join[1], u_re) ; 
		push_float(&SplitJoin57_Butterfly_Fiss_7131_7157_join[1], u_im) ; 
		push_float(&SplitJoin57_Butterfly_Fiss_7131_7157_join[1], t_re) ; 
		push_float(&SplitJoin57_Butterfly_Fiss_7131_7157_join[1], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7106() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin57_Butterfly_Fiss_7131_7157_split[0], pop_float(&Pre_CollapsedDataParallel_1_6925WEIGHTED_ROUND_ROBIN_Splitter_7106));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin57_Butterfly_Fiss_7131_7157_split[1], pop_float(&Pre_CollapsedDataParallel_1_6925WEIGHTED_ROUND_ROBIN_Splitter_7106));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7107() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7107Post_CollapsedDataParallel_2_6926, pop_float(&SplitJoin57_Butterfly_Fiss_7131_7157_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7107Post_CollapsedDataParallel_2_6926, pop_float(&SplitJoin57_Butterfly_Fiss_7131_7157_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_6926(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
 {
		int kTimesWeights_i = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child1_7012_7155_join[1], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_7107Post_CollapsedDataParallel_2_6926, (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7107Post_CollapsedDataParallel_2_6926) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_6928(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
 {
		int partialSum_k = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Pre_CollapsedDataParallel_1_6928WEIGHTED_ROUND_ROBIN_Splitter_7110, peek_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child1_7012_7155_split[2], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 4) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child1_7012_7155_split[2]) ; 
	}
	ENDFOR
}

void Butterfly_7112(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin61_Butterfly_Fiss_7132_7158_split[0]) ; 
		u_im = pop_float(&SplitJoin61_Butterfly_Fiss_7132_7158_split[0]) ; 
		t_re = pop_float(&SplitJoin61_Butterfly_Fiss_7132_7158_split[0]) ; 
		t_im = pop_float(&SplitJoin61_Butterfly_Fiss_7132_7158_split[0]) ; 
		wt_re = ((0.38268343 * t_re) - (0.9238795 * t_im)) ; 
		wt_im = ((0.38268343 * t_im) + (0.9238795 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin61_Butterfly_Fiss_7132_7158_join[0], u_re) ; 
		push_float(&SplitJoin61_Butterfly_Fiss_7132_7158_join[0], u_im) ; 
		push_float(&SplitJoin61_Butterfly_Fiss_7132_7158_join[0], t_re) ; 
		push_float(&SplitJoin61_Butterfly_Fiss_7132_7158_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7113(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin61_Butterfly_Fiss_7132_7158_split[1]) ; 
		u_im = pop_float(&SplitJoin61_Butterfly_Fiss_7132_7158_split[1]) ; 
		t_re = pop_float(&SplitJoin61_Butterfly_Fiss_7132_7158_split[1]) ; 
		t_im = pop_float(&SplitJoin61_Butterfly_Fiss_7132_7158_split[1]) ; 
		wt_re = ((0.38268343 * t_re) - (0.9238795 * t_im)) ; 
		wt_im = ((0.38268343 * t_im) + (0.9238795 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin61_Butterfly_Fiss_7132_7158_join[1], u_re) ; 
		push_float(&SplitJoin61_Butterfly_Fiss_7132_7158_join[1], u_im) ; 
		push_float(&SplitJoin61_Butterfly_Fiss_7132_7158_join[1], t_re) ; 
		push_float(&SplitJoin61_Butterfly_Fiss_7132_7158_join[1], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7110() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin61_Butterfly_Fiss_7132_7158_split[0], pop_float(&Pre_CollapsedDataParallel_1_6928WEIGHTED_ROUND_ROBIN_Splitter_7110));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin61_Butterfly_Fiss_7132_7158_split[1], pop_float(&Pre_CollapsedDataParallel_1_6928WEIGHTED_ROUND_ROBIN_Splitter_7110));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7111() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7111Post_CollapsedDataParallel_2_6929, pop_float(&SplitJoin61_Butterfly_Fiss_7132_7158_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7111Post_CollapsedDataParallel_2_6929, pop_float(&SplitJoin61_Butterfly_Fiss_7132_7158_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_6929(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
 {
		int kTimesWeights_i = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child1_7012_7155_join[2], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_7111Post_CollapsedDataParallel_2_6929, (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7111Post_CollapsedDataParallel_2_6929) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_6931(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
 {
		int partialSum_k = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Pre_CollapsedDataParallel_1_6931WEIGHTED_ROUND_ROBIN_Splitter_7114, peek_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child1_7012_7155_split[3], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 4) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child1_7012_7155_split[3]) ; 
	}
	ENDFOR
}

void Butterfly_7116(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin65_Butterfly_Fiss_7133_7159_split[0]) ; 
		u_im = pop_float(&SplitJoin65_Butterfly_Fiss_7133_7159_split[0]) ; 
		t_re = pop_float(&SplitJoin65_Butterfly_Fiss_7133_7159_split[0]) ; 
		t_im = pop_float(&SplitJoin65_Butterfly_Fiss_7133_7159_split[0]) ; 
		wt_re = ((-0.9238796 * t_re) - (0.38268328 * t_im)) ; 
		wt_im = ((-0.9238796 * t_im) + (0.38268328 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin65_Butterfly_Fiss_7133_7159_join[0], u_re) ; 
		push_float(&SplitJoin65_Butterfly_Fiss_7133_7159_join[0], u_im) ; 
		push_float(&SplitJoin65_Butterfly_Fiss_7133_7159_join[0], t_re) ; 
		push_float(&SplitJoin65_Butterfly_Fiss_7133_7159_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_7117(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin65_Butterfly_Fiss_7133_7159_split[1]) ; 
		u_im = pop_float(&SplitJoin65_Butterfly_Fiss_7133_7159_split[1]) ; 
		t_re = pop_float(&SplitJoin65_Butterfly_Fiss_7133_7159_split[1]) ; 
		t_im = pop_float(&SplitJoin65_Butterfly_Fiss_7133_7159_split[1]) ; 
		wt_re = ((-0.9238796 * t_re) - (0.38268328 * t_im)) ; 
		wt_im = ((-0.9238796 * t_im) + (0.38268328 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin65_Butterfly_Fiss_7133_7159_join[1], u_re) ; 
		push_float(&SplitJoin65_Butterfly_Fiss_7133_7159_join[1], u_im) ; 
		push_float(&SplitJoin65_Butterfly_Fiss_7133_7159_join[1], t_re) ; 
		push_float(&SplitJoin65_Butterfly_Fiss_7133_7159_join[1], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7114() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin65_Butterfly_Fiss_7133_7159_split[0], pop_float(&Pre_CollapsedDataParallel_1_6931WEIGHTED_ROUND_ROBIN_Splitter_7114));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin65_Butterfly_Fiss_7133_7159_split[1], pop_float(&Pre_CollapsedDataParallel_1_6931WEIGHTED_ROUND_ROBIN_Splitter_7114));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7115() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7115Post_CollapsedDataParallel_2_6932, pop_float(&SplitJoin65_Butterfly_Fiss_7133_7159_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7115Post_CollapsedDataParallel_2_6932, pop_float(&SplitJoin65_Butterfly_Fiss_7133_7159_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_6932(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
 {
		int kTimesWeights_i = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child1_7012_7155_join[3], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_7115Post_CollapsedDataParallel_2_6932, (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7115Post_CollapsedDataParallel_2_6932) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7021() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child1_7012_7155_split[__iter_dec_], pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_Hier_7122_7149_split[1]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7022() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_Hier_7122_7149_join[1], pop_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child1_7012_7155_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_7018() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_Hier_7122_7149_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7017WEIGHTED_ROUND_ROBIN_Splitter_7018));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_Hier_7122_7149_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7017WEIGHTED_ROUND_ROBIN_Splitter_7018));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7023() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7023WEIGHTED_ROUND_ROBIN_Splitter_7024, pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_Hier_7122_7149_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7023WEIGHTED_ROUND_ROBIN_Splitter_7024, pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_Hier_7122_7149_join[1]));
		ENDFOR
	ENDFOR
}}

void Butterfly_6653(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[0]) ; 
		u_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[0]) ; 
		t_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[0]) ; 
		t_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[0]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[0], u_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[0], u_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[0], t_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_6654(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[1]) ; 
		u_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[1]) ; 
		t_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[1]) ; 
		t_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[1]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[1], u_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[1], u_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[1], t_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[1], t_im) ; 
	}
	ENDFOR
}

void Butterfly_6655(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[2]) ; 
		u_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[2]) ; 
		t_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[2]) ; 
		t_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[2]) ; 
		wt_re = ((0.70710677 * t_re) - (0.70710677 * t_im)) ; 
		wt_im = ((0.70710677 * t_im) + (0.70710677 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[2], u_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[2], u_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[2], t_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[2], t_im) ; 
	}
	ENDFOR
}

void Butterfly_6656(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[3]) ; 
		u_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[3]) ; 
		t_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[3]) ; 
		t_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[3]) ; 
		wt_re = ((-0.70710677 * t_re) - (0.70710677 * t_im)) ; 
		wt_im = ((-0.70710677 * t_im) + (0.70710677 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[3], u_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[3], u_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[3], t_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[3], t_im) ; 
	}
	ENDFOR
}

void Butterfly_6657(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[4]) ; 
		u_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[4]) ; 
		t_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[4]) ; 
		t_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[4]) ; 
		wt_re = ((0.9238795 * t_re) - (0.38268346 * t_im)) ; 
		wt_im = ((0.9238795 * t_im) + (0.38268346 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[4], u_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[4], u_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[4], t_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[4], t_im) ; 
	}
	ENDFOR
}

void Butterfly_6658(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[5]) ; 
		u_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[5]) ; 
		t_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[5]) ; 
		t_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[5]) ; 
		wt_re = ((-0.38268352 * t_re) - (0.9238795 * t_im)) ; 
		wt_im = ((-0.38268352 * t_im) + (0.9238795 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[5], u_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[5], u_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[5], t_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[5], t_im) ; 
	}
	ENDFOR
}

void Butterfly_6659(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[6]) ; 
		u_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[6]) ; 
		t_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[6]) ; 
		t_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[6]) ; 
		wt_re = ((0.38268343 * t_re) - (0.9238795 * t_im)) ; 
		wt_im = ((0.38268343 * t_im) + (0.9238795 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[6], u_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[6], u_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[6], t_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[6], t_im) ; 
	}
	ENDFOR
}

void Butterfly_6660(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[7]) ; 
		u_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[7]) ; 
		t_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[7]) ; 
		t_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[7]) ; 
		wt_re = ((-0.9238796 * t_re) - (0.38268328 * t_im)) ; 
		wt_im = ((-0.9238796 * t_im) + (0.38268328 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[7], u_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[7], u_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[7], t_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[7], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7025() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[__iter_dec_], pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_Hier_7124_7160_split[0]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7026() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_Hier_7124_7160_join[0], pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Butterfly_6661(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[0]) ; 
		u_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[0]) ; 
		t_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[0]) ; 
		t_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[0]) ; 
		wt_re = ((0.98078525 * t_re) - (0.19509032 * t_im)) ; 
		wt_im = ((0.98078525 * t_im) + (0.19509032 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[0], u_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[0], u_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[0], t_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_6662(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[1]) ; 
		u_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[1]) ; 
		t_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[1]) ; 
		t_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[1]) ; 
		wt_re = ((-0.19509032 * t_re) - (0.98078525 * t_im)) ; 
		wt_im = ((-0.19509032 * t_im) + (0.98078525 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[1], u_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[1], u_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[1], t_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[1], t_im) ; 
	}
	ENDFOR
}

void Butterfly_6663(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[2]) ; 
		u_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[2]) ; 
		t_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[2]) ; 
		t_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[2]) ; 
		wt_re = ((0.5555702 * t_re) - (0.83146966 * t_im)) ; 
		wt_im = ((0.5555702 * t_im) + (0.83146966 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[2], u_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[2], u_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[2], t_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[2], t_im) ; 
	}
	ENDFOR
}

void Butterfly_6664(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[3]) ; 
		u_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[3]) ; 
		t_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[3]) ; 
		t_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[3]) ; 
		wt_re = ((-0.83146966 * t_re) - (0.5555702 * t_im)) ; 
		wt_im = ((-0.83146966 * t_im) + (0.5555702 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[3], u_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[3], u_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[3], t_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[3], t_im) ; 
	}
	ENDFOR
}

void Butterfly_6665(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[4]) ; 
		u_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[4]) ; 
		t_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[4]) ; 
		t_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[4]) ; 
		wt_re = ((0.8314696 * t_re) - (0.55557024 * t_im)) ; 
		wt_im = ((0.8314696 * t_im) + (0.55557024 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[4], u_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[4], u_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[4], t_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[4], t_im) ; 
	}
	ENDFOR
}

void Butterfly_6666(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[5]) ; 
		u_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[5]) ; 
		t_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[5]) ; 
		t_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[5]) ; 
		wt_re = ((-0.55557036 * t_re) - (0.83146954 * t_im)) ; 
		wt_im = ((-0.55557036 * t_im) + (0.83146954 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[5], u_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[5], u_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[5], t_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[5], t_im) ; 
	}
	ENDFOR
}

void Butterfly_6667(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[6]) ; 
		u_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[6]) ; 
		t_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[6]) ; 
		t_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[6]) ; 
		wt_re = ((0.19509023 * t_re) - (0.9807853 * t_im)) ; 
		wt_im = ((0.19509023 * t_im) + (0.9807853 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[6], u_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[6], u_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[6], t_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[6], t_im) ; 
	}
	ENDFOR
}

void Butterfly_6668(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[7]) ; 
		u_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[7]) ; 
		t_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[7]) ; 
		t_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[7]) ; 
		wt_re = ((-0.9807853 * t_re) - (0.19509031 * t_im)) ; 
		wt_im = ((-0.9807853 * t_im) + (0.19509031 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[7], u_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[7], u_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[7], t_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[7], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7027() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[__iter_dec_], pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_Hier_7124_7160_split[1]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7028() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_Hier_7124_7160_join[1], pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_7024() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_Hier_7124_7160_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7023WEIGHTED_ROUND_ROBIN_Splitter_7024));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_Hier_7124_7160_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7023WEIGHTED_ROUND_ROBIN_Splitter_7024));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7029() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7029BitReverse_6669, pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_Hier_7124_7160_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7029BitReverse_6669, pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_Hier_7124_7160_join[1]));
		ENDFOR
	ENDFOR
}}

int BitReverse_6669_bitrev(int inp, int numbits) {
	int rev = 0;
	FOR(int, i, 0,  < , numbits, i++) {
		rev = ((rev * 2) | (inp & 1)) ; 
		inp = (inp / 2) ; 
	}
	ENDFOR
	return rev;
}
void BitReverse_6669(){
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++) {
		FOR(int, i__conflict__0, 0,  < , 32, i__conflict__0++) {
			int br = 0;
			br = BitReverse_6669_bitrev(i__conflict__0, 5) ; 
			push_float(&BitReverse_6669FloatPrinter_6670, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_7029BitReverse_6669, (2 * br))) ; 
			push_float(&BitReverse_6669FloatPrinter_6670, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_7029BitReverse_6669, ((2 * br) + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7029BitReverse_6669) ; 
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7029BitReverse_6669) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FloatPrinter_6670(){
	FOR(uint32_t, __iter_steady_, 0, <, 288, __iter_steady_++) {
		printf("%.10f", pop_float(&BitReverse_6669FloatPrinter_6670));
		printf("\n");
		printf("%.10f", pop_float(&BitReverse_6669FloatPrinter_6670));
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_child1_6987_7146_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 2, __iter_init_1_++)
		init_buffer_float(&SplitJoin43_Butterfly_Fiss_7128_7153_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_join[__iter_init_2_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7023WEIGHTED_ROUND_ROBIN_Splitter_7024);
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_float(&SplitJoin85_Butterfly_Fiss_7135_7145_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 4, __iter_init_4_++)
		init_buffer_float(&SplitJoin72_Butterfly_Fiss_7134_7144_join[__iter_init_4_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_6889WEIGHTED_ROUND_ROBIN_Splitter_7031);
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin4_Butterfly_Fiss_7120_7141_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 4, __iter_init_6_++)
		init_buffer_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child1_7012_7155_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 4, __iter_init_8_++)
		init_buffer_float(&SplitJoin8_Butterfly_Fiss_7121_7143_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 9, __iter_init_9_++)
		init_buffer_float(&SplitJoin0_Butterfly_Fiss_7118_7139_split[__iter_init_9_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_6898WEIGHTED_ROUND_ROBIN_Splitter_7052);
	init_buffer_float(&Pre_CollapsedDataParallel_1_6916WEIGHTED_ROUND_ROBIN_Splitter_7094);
	init_buffer_float(&Pre_CollapsedDataParallel_1_6910WEIGHTED_ROUND_ROBIN_Splitter_7086);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7059Post_CollapsedDataParallel_2_6902);
	FOR(int, __iter_init_10_, 0, <, 4, __iter_init_10_++)
		init_buffer_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child0_7011_7150_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 2, __iter_init_11_++)
		init_buffer_float(&SplitJoin47_Butterfly_Fiss_7129_7154_split[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 8, __iter_init_12_++)
		init_buffer_float(&SplitJoin4_Butterfly_Fiss_7120_7141_join[__iter_init_12_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7043Post_CollapsedDataParallel_2_6893);
	init_buffer_float(&Pre_CollapsedDataParallel_1_6931WEIGHTED_ROUND_ROBIN_Splitter_7114);
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_float(&SplitJoin61_Butterfly_Fiss_7132_7158_split[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 8, __iter_init_14_++)
		init_buffer_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child0_7125_7161_split[__iter_init_14_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7075Post_CollapsedDataParallel_2_6905);
	init_buffer_float(&Post_CollapsedDataParallel_2_6896WEIGHTED_ROUND_ROBIN_Splitter_7015);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7017WEIGHTED_ROUND_ROBIN_Splitter_7018);
	FOR(int, __iter_init_15_, 0, <, 2, __iter_init_15_++)
		init_buffer_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_child0_6982_7142_join[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 4, __iter_init_16_++)
		init_buffer_float(&SplitJoin89_Butterfly_Fiss_7136_7147_split[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 4, __iter_init_17_++)
		init_buffer_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child0_7011_7150_split[__iter_init_17_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_6925WEIGHTED_ROUND_ROBIN_Splitter_7106);
	FOR(int, __iter_init_18_, 0, <, 4, __iter_init_18_++)
		init_buffer_float(&SplitJoin8_Butterfly_Fiss_7121_7143_split[__iter_init_18_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7115Post_CollapsedDataParallel_2_6932);
	FOR(int, __iter_init_19_, 0, <, 2, __iter_init_19_++)
		init_buffer_float(&SplitJoin65_Butterfly_Fiss_7133_7159_join[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 2, __iter_init_20_++)
		init_buffer_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_Hier_7122_7149_join[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 2, __iter_init_21_++)
		init_buffer_float(&SplitJoin47_Butterfly_Fiss_7129_7154_join[__iter_init_21_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_6928WEIGHTED_ROUND_ROBIN_Splitter_7110);
	FOR(int, __iter_init_22_, 0, <, 2, __iter_init_22_++)
		init_buffer_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_child1_6987_7146_split[__iter_init_22_]);
	ENDFOR
	FOR(int, __iter_init_23_, 0, <, 4, __iter_init_23_++)
		init_buffer_float(&SplitJoin89_Butterfly_Fiss_7136_7147_join[__iter_init_23_]);
	ENDFOR
	FOR(int, __iter_init_24_, 0, <, 2, __iter_init_24_++)
		init_buffer_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_Hier_7124_7160_join[__iter_init_24_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7029BitReverse_6669);
	FOR(int, __iter_init_25_, 0, <, 2, __iter_init_25_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_6546_6941_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_Hier_7119_7140_split[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 2, __iter_init_26_++)
		init_buffer_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_Hier_7122_7149_split[__iter_init_26_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_6890WEIGHTED_ROUND_ROBIN_Splitter_6933);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7099Post_CollapsedDataParallel_2_6920);
	FOR(int, __iter_init_27_, 0, <, 2, __iter_init_27_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_6546_6941_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_Hier_7119_7140_join[__iter_init_27_]);
	ENDFOR
	FOR(int, __iter_init_28_, 0, <, 8, __iter_init_28_++)
		init_buffer_float(&SplitJoin85_Butterfly_Fiss_7135_7145_join[__iter_init_28_]);
	ENDFOR
	FOR(int, __iter_init_29_, 0, <, 2, __iter_init_29_++)
		init_buffer_float(&SplitJoin61_Butterfly_Fiss_7132_7158_join[__iter_init_29_]);
	ENDFOR
	FOR(int, __iter_init_30_, 0, <, 2, __iter_init_30_++)
		init_buffer_float(&SplitJoin65_Butterfly_Fiss_7133_7159_split[__iter_init_30_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_6913WEIGHTED_ROUND_ROBIN_Splitter_7090);
	FOR(int, __iter_init_31_, 0, <, 4, __iter_init_31_++)
		init_buffer_float(&SplitJoin72_Butterfly_Fiss_7134_7144_split[__iter_init_31_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_6904WEIGHTED_ROUND_ROBIN_Splitter_7074);
	FOR(int, __iter_init_32_, 0, <, 9, __iter_init_32_++)
		init_buffer_float(&SplitJoin0_Butterfly_Fiss_7118_7139_join[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 2, __iter_init_33_++)
		init_buffer_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_Hier_7124_7160_split[__iter_init_33_]);
	ENDFOR
	init_buffer_float(&FloatSource_6588Pre_CollapsedDataParallel_1_6889);
	FOR(int, __iter_init_34_, 0, <, 2, __iter_init_34_++)
		init_buffer_float(&SplitJoin39_Butterfly_Fiss_7127_7152_split[__iter_init_34_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7065Post_CollapsedDataParallel_2_6896);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7095Post_CollapsedDataParallel_2_6917);
	FOR(int, __iter_init_35_, 0, <, 8, __iter_init_35_++)
		init_buffer_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_6586_6947_Hier_child1_7126_7162_join[__iter_init_35_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7091Post_CollapsedDataParallel_2_6914);
	init_buffer_float(&Pre_CollapsedDataParallel_1_6919WEIGHTED_ROUND_ROBIN_Splitter_7098);
	FOR(int, __iter_init_36_, 0, <, 2, __iter_init_36_++)
		init_buffer_float(&SplitJoin43_Butterfly_Fiss_7128_7153_split[__iter_init_36_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_6893WEIGHTED_ROUND_ROBIN_Splitter_7013);
	FOR(int, __iter_init_37_, 0, <, 2, __iter_init_37_++)
		init_buffer_float(&SplitJoin14_Butterfly_Fiss_7123_7151_join[__iter_init_37_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_6892WEIGHTED_ROUND_ROBIN_Splitter_7042);
	FOR(int, __iter_init_38_, 0, <, 2, __iter_init_38_++)
		init_buffer_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_6555_6943_child0_6982_7142_split[__iter_init_38_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7111Post_CollapsedDataParallel_2_6929);
	FOR(int, __iter_init_39_, 0, <, 4, __iter_init_39_++)
		init_buffer_float(&SplitJoin95_Butterfly_Fiss_7137_7148_join[__iter_init_39_]);
	ENDFOR
	FOR(int, __iter_init_40_, 0, <, 2, __iter_init_40_++)
		init_buffer_float(&SplitJoin53_Butterfly_Fiss_7130_7156_split[__iter_init_40_]);
	ENDFOR
	FOR(int, __iter_init_41_, 0, <, 2, __iter_init_41_++)
		init_buffer_float(&SplitJoin57_Butterfly_Fiss_7131_7157_join[__iter_init_41_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7107Post_CollapsedDataParallel_2_6926);
	FOR(int, __iter_init_42_, 0, <, 2, __iter_init_42_++)
		init_buffer_float(&SplitJoin57_Butterfly_Fiss_7131_7157_split[__iter_init_42_]);
	ENDFOR
	FOR(int, __iter_init_43_, 0, <, 4, __iter_init_43_++)
		init_buffer_float(&SplitJoin95_Butterfly_Fiss_7137_7148_split[__iter_init_43_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7087Post_CollapsedDataParallel_2_6911);
	init_buffer_float(&Pre_CollapsedDataParallel_1_6922WEIGHTED_ROUND_ROBIN_Splitter_7102);
	FOR(int, __iter_init_44_, 0, <, 4, __iter_init_44_++)
		init_buffer_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_6568_6945_Hier_child1_7012_7155_join[__iter_init_44_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_6895WEIGHTED_ROUND_ROBIN_Splitter_7064);
	init_buffer_float(&Pre_CollapsedDataParallel_1_6907WEIGHTED_ROUND_ROBIN_Splitter_7080);
	FOR(int, __iter_init_45_, 0, <, 2, __iter_init_45_++)
		init_buffer_float(&SplitJoin14_Butterfly_Fiss_7123_7151_split[__iter_init_45_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7032Post_CollapsedDataParallel_2_6890);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7081Post_CollapsedDataParallel_2_6908);
	FOR(int, __iter_init_46_, 0, <, 2, __iter_init_46_++)
		init_buffer_float(&SplitJoin53_Butterfly_Fiss_7130_7156_join[__iter_init_46_]);
	ENDFOR
	init_buffer_float(&BitReverse_6669FloatPrinter_6670);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7053Post_CollapsedDataParallel_2_6899);
	init_buffer_float(&Pre_CollapsedDataParallel_1_6901WEIGHTED_ROUND_ROBIN_Splitter_7058);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7103Post_CollapsedDataParallel_2_6923);
	FOR(int, __iter_init_47_, 0, <, 2, __iter_init_47_++)
		init_buffer_float(&SplitJoin39_Butterfly_Fiss_7127_7152_join[__iter_init_47_]);
	ENDFOR
// --- init: FloatSource_6588
	 {
	FOR(int, i, 0,  < , 32, i++) {
		FloatSource_6588_s.A_re[i] = 0.0 ; 
		FloatSource_6588_s.A_im[i] = 0.0 ; 
	}
	ENDFOR
	FloatSource_6588_s.A_re[1] = 1.0 ; 
	FloatSource_6588_s.idx = 0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FloatSource_6588();
		Pre_CollapsedDataParallel_1_6889();
		WEIGHTED_ROUND_ROBIN_Splitter_7031();
			Butterfly_7033();
			Butterfly_7034();
			Butterfly_7035();
			Butterfly_7036();
			Butterfly_7037();
			Butterfly_7038();
			Butterfly_7039();
			Butterfly_7040();
			Butterfly_7041();
		WEIGHTED_ROUND_ROBIN_Joiner_7032();
		Post_CollapsedDataParallel_2_6890();
		WEIGHTED_ROUND_ROBIN_Splitter_6933();
			Pre_CollapsedDataParallel_1_6892();
			WEIGHTED_ROUND_ROBIN_Splitter_7042();
				Butterfly_7044();
				Butterfly_7045();
				Butterfly_7046();
				Butterfly_7047();
				Butterfly_7048();
				Butterfly_7049();
				Butterfly_7050();
				Butterfly_7051();
			WEIGHTED_ROUND_ROBIN_Joiner_7043();
			Post_CollapsedDataParallel_2_6893();
			WEIGHTED_ROUND_ROBIN_Splitter_7013();
				Pre_CollapsedDataParallel_1_6898();
				WEIGHTED_ROUND_ROBIN_Splitter_7052();
					Butterfly_7054();
					Butterfly_7055();
					Butterfly_7056();
					Butterfly_7057();
				WEIGHTED_ROUND_ROBIN_Joiner_7053();
				Post_CollapsedDataParallel_2_6899();
				Pre_CollapsedDataParallel_1_6901();
				WEIGHTED_ROUND_ROBIN_Splitter_7058();
					Butterfly_7060();
					Butterfly_7061();
					Butterfly_7062();
					Butterfly_7063();
				WEIGHTED_ROUND_ROBIN_Joiner_7059();
				Post_CollapsedDataParallel_2_6902();
			WEIGHTED_ROUND_ROBIN_Joiner_7014();
			Pre_CollapsedDataParallel_1_6895();
			WEIGHTED_ROUND_ROBIN_Splitter_7064();
				Butterfly_7066();
				Butterfly_7067();
				Butterfly_7068();
				Butterfly_7069();
				Butterfly_7070();
				Butterfly_7071();
				Butterfly_7072();
				Butterfly_7073();
			WEIGHTED_ROUND_ROBIN_Joiner_7065();
			Post_CollapsedDataParallel_2_6896();
			WEIGHTED_ROUND_ROBIN_Splitter_7015();
				Pre_CollapsedDataParallel_1_6904();
				WEIGHTED_ROUND_ROBIN_Splitter_7074();
					Butterfly_7076();
					Butterfly_7077();
					Butterfly_7078();
					Butterfly_7079();
				WEIGHTED_ROUND_ROBIN_Joiner_7075();
				Post_CollapsedDataParallel_2_6905();
				Pre_CollapsedDataParallel_1_6907();
				WEIGHTED_ROUND_ROBIN_Splitter_7080();
					Butterfly_7082();
					Butterfly_7083();
					Butterfly_7084();
					Butterfly_7085();
				WEIGHTED_ROUND_ROBIN_Joiner_7081();
				Post_CollapsedDataParallel_2_6908();
			WEIGHTED_ROUND_ROBIN_Joiner_7016();
		WEIGHTED_ROUND_ROBIN_Joiner_7017();
		WEIGHTED_ROUND_ROBIN_Splitter_7018();
			WEIGHTED_ROUND_ROBIN_Splitter_7019();
				Pre_CollapsedDataParallel_1_6910();
				WEIGHTED_ROUND_ROBIN_Splitter_7086();
					Butterfly_7088();
					Butterfly_7089();
				WEIGHTED_ROUND_ROBIN_Joiner_7087();
				Post_CollapsedDataParallel_2_6911();
				Pre_CollapsedDataParallel_1_6913();
				WEIGHTED_ROUND_ROBIN_Splitter_7090();
					Butterfly_7092();
					Butterfly_7093();
				WEIGHTED_ROUND_ROBIN_Joiner_7091();
				Post_CollapsedDataParallel_2_6914();
				Pre_CollapsedDataParallel_1_6916();
				WEIGHTED_ROUND_ROBIN_Splitter_7094();
					Butterfly_7096();
					Butterfly_7097();
				WEIGHTED_ROUND_ROBIN_Joiner_7095();
				Post_CollapsedDataParallel_2_6917();
				Pre_CollapsedDataParallel_1_6919();
				WEIGHTED_ROUND_ROBIN_Splitter_7098();
					Butterfly_7100();
					Butterfly_7101();
				WEIGHTED_ROUND_ROBIN_Joiner_7099();
				Post_CollapsedDataParallel_2_6920();
			WEIGHTED_ROUND_ROBIN_Joiner_7020();
			WEIGHTED_ROUND_ROBIN_Splitter_7021();
				Pre_CollapsedDataParallel_1_6922();
				WEIGHTED_ROUND_ROBIN_Splitter_7102();
					Butterfly_7104();
					Butterfly_7105();
				WEIGHTED_ROUND_ROBIN_Joiner_7103();
				Post_CollapsedDataParallel_2_6923();
				Pre_CollapsedDataParallel_1_6925();
				WEIGHTED_ROUND_ROBIN_Splitter_7106();
					Butterfly_7108();
					Butterfly_7109();
				WEIGHTED_ROUND_ROBIN_Joiner_7107();
				Post_CollapsedDataParallel_2_6926();
				Pre_CollapsedDataParallel_1_6928();
				WEIGHTED_ROUND_ROBIN_Splitter_7110();
					Butterfly_7112();
					Butterfly_7113();
				WEIGHTED_ROUND_ROBIN_Joiner_7111();
				Post_CollapsedDataParallel_2_6929();
				Pre_CollapsedDataParallel_1_6931();
				WEIGHTED_ROUND_ROBIN_Splitter_7114();
					Butterfly_7116();
					Butterfly_7117();
				WEIGHTED_ROUND_ROBIN_Joiner_7115();
				Post_CollapsedDataParallel_2_6932();
			WEIGHTED_ROUND_ROBIN_Joiner_7022();
		WEIGHTED_ROUND_ROBIN_Joiner_7023();
		WEIGHTED_ROUND_ROBIN_Splitter_7024();
			WEIGHTED_ROUND_ROBIN_Splitter_7025();
				Butterfly_6653();
				Butterfly_6654();
				Butterfly_6655();
				Butterfly_6656();
				Butterfly_6657();
				Butterfly_6658();
				Butterfly_6659();
				Butterfly_6660();
			WEIGHTED_ROUND_ROBIN_Joiner_7026();
			WEIGHTED_ROUND_ROBIN_Splitter_7027();
				Butterfly_6661();
				Butterfly_6662();
				Butterfly_6663();
				Butterfly_6664();
				Butterfly_6665();
				Butterfly_6666();
				Butterfly_6667();
				Butterfly_6668();
			WEIGHTED_ROUND_ROBIN_Joiner_7028();
		WEIGHTED_ROUND_ROBIN_Joiner_7029();
		BitReverse_6669();
		FloatPrinter_6670();
	ENDFOR
	return EXIT_SUCCESS;
}
