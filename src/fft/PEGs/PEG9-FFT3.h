#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=1152 on the compile command line
#else
#if BUF_SIZEMAX < 1152
#error BUF_SIZEMAX too small, it must be at least 1152
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float A_re[32];
	float A_im[32];
	int idx;
} FloatSource_6588_t;
void FloatSource(buffer_float_t *chanout);
void FloatSource_6588();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_6889();
void WEIGHTED_ROUND_ROBIN_Splitter_7031();
void Butterfly(buffer_float_t *chanin, buffer_float_t *chanout);
void Butterfly_7033();
void Butterfly_7034();
void Butterfly_7035();
void Butterfly_7036();
void Butterfly_7037();
void Butterfly_7038();
void Butterfly_7039();
void Butterfly_7040();
void Butterfly_7041();
void WEIGHTED_ROUND_ROBIN_Joiner_7032();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_6890();
void WEIGHTED_ROUND_ROBIN_Splitter_6933();
void Pre_CollapsedDataParallel_1_6892();
void WEIGHTED_ROUND_ROBIN_Splitter_7042();
void Butterfly_7044();
void Butterfly_7045();
void Butterfly_7046();
void Butterfly_7047();
void Butterfly_7048();
void Butterfly_7049();
void Butterfly_7050();
void Butterfly_7051();
void WEIGHTED_ROUND_ROBIN_Joiner_7043();
void Post_CollapsedDataParallel_2_6893();
void WEIGHTED_ROUND_ROBIN_Splitter_7013();
void Pre_CollapsedDataParallel_1_6898();
void WEIGHTED_ROUND_ROBIN_Splitter_7052();
void Butterfly_7054();
void Butterfly_7055();
void Butterfly_7056();
void Butterfly_7057();
void WEIGHTED_ROUND_ROBIN_Joiner_7053();
void Post_CollapsedDataParallel_2_6899();
void Pre_CollapsedDataParallel_1_6901();
void WEIGHTED_ROUND_ROBIN_Splitter_7058();
void Butterfly_7060();
void Butterfly_7061();
void Butterfly_7062();
void Butterfly_7063();
void WEIGHTED_ROUND_ROBIN_Joiner_7059();
void Post_CollapsedDataParallel_2_6902();
void WEIGHTED_ROUND_ROBIN_Joiner_7014();
void Pre_CollapsedDataParallel_1_6895();
void WEIGHTED_ROUND_ROBIN_Splitter_7064();
void Butterfly_7066();
void Butterfly_7067();
void Butterfly_7068();
void Butterfly_7069();
void Butterfly_7070();
void Butterfly_7071();
void Butterfly_7072();
void Butterfly_7073();
void WEIGHTED_ROUND_ROBIN_Joiner_7065();
void Post_CollapsedDataParallel_2_6896();
void WEIGHTED_ROUND_ROBIN_Splitter_7015();
void Pre_CollapsedDataParallel_1_6904();
void WEIGHTED_ROUND_ROBIN_Splitter_7074();
void Butterfly_7076();
void Butterfly_7077();
void Butterfly_7078();
void Butterfly_7079();
void WEIGHTED_ROUND_ROBIN_Joiner_7075();
void Post_CollapsedDataParallel_2_6905();
void Pre_CollapsedDataParallel_1_6907();
void WEIGHTED_ROUND_ROBIN_Splitter_7080();
void Butterfly_7082();
void Butterfly_7083();
void Butterfly_7084();
void Butterfly_7085();
void WEIGHTED_ROUND_ROBIN_Joiner_7081();
void Post_CollapsedDataParallel_2_6908();
void WEIGHTED_ROUND_ROBIN_Joiner_7016();
void WEIGHTED_ROUND_ROBIN_Joiner_7017();
void WEIGHTED_ROUND_ROBIN_Splitter_7018();
void WEIGHTED_ROUND_ROBIN_Splitter_7019();
void Pre_CollapsedDataParallel_1_6910();
void WEIGHTED_ROUND_ROBIN_Splitter_7086();
void Butterfly_7088();
void Butterfly_7089();
void WEIGHTED_ROUND_ROBIN_Joiner_7087();
void Post_CollapsedDataParallel_2_6911();
void Pre_CollapsedDataParallel_1_6913();
void WEIGHTED_ROUND_ROBIN_Splitter_7090();
void Butterfly_7092();
void Butterfly_7093();
void WEIGHTED_ROUND_ROBIN_Joiner_7091();
void Post_CollapsedDataParallel_2_6914();
void Pre_CollapsedDataParallel_1_6916();
void WEIGHTED_ROUND_ROBIN_Splitter_7094();
void Butterfly_7096();
void Butterfly_7097();
void WEIGHTED_ROUND_ROBIN_Joiner_7095();
void Post_CollapsedDataParallel_2_6917();
void Pre_CollapsedDataParallel_1_6919();
void WEIGHTED_ROUND_ROBIN_Splitter_7098();
void Butterfly_7100();
void Butterfly_7101();
void WEIGHTED_ROUND_ROBIN_Joiner_7099();
void Post_CollapsedDataParallel_2_6920();
void WEIGHTED_ROUND_ROBIN_Joiner_7020();
void WEIGHTED_ROUND_ROBIN_Splitter_7021();
void Pre_CollapsedDataParallel_1_6922();
void WEIGHTED_ROUND_ROBIN_Splitter_7102();
void Butterfly_7104();
void Butterfly_7105();
void WEIGHTED_ROUND_ROBIN_Joiner_7103();
void Post_CollapsedDataParallel_2_6923();
void Pre_CollapsedDataParallel_1_6925();
void WEIGHTED_ROUND_ROBIN_Splitter_7106();
void Butterfly_7108();
void Butterfly_7109();
void WEIGHTED_ROUND_ROBIN_Joiner_7107();
void Post_CollapsedDataParallel_2_6926();
void Pre_CollapsedDataParallel_1_6928();
void WEIGHTED_ROUND_ROBIN_Splitter_7110();
void Butterfly_7112();
void Butterfly_7113();
void WEIGHTED_ROUND_ROBIN_Joiner_7111();
void Post_CollapsedDataParallel_2_6929();
void Pre_CollapsedDataParallel_1_6931();
void WEIGHTED_ROUND_ROBIN_Splitter_7114();
void Butterfly_7116();
void Butterfly_7117();
void WEIGHTED_ROUND_ROBIN_Joiner_7115();
void Post_CollapsedDataParallel_2_6932();
void WEIGHTED_ROUND_ROBIN_Joiner_7022();
void WEIGHTED_ROUND_ROBIN_Joiner_7023();
void WEIGHTED_ROUND_ROBIN_Splitter_7024();
void WEIGHTED_ROUND_ROBIN_Splitter_7025();
void Butterfly_6653();
void Butterfly_6654();
void Butterfly_6655();
void Butterfly_6656();
void Butterfly_6657();
void Butterfly_6658();
void Butterfly_6659();
void Butterfly_6660();
void WEIGHTED_ROUND_ROBIN_Joiner_7026();
void WEIGHTED_ROUND_ROBIN_Splitter_7027();
void Butterfly_6661();
void Butterfly_6662();
void Butterfly_6663();
void Butterfly_6664();
void Butterfly_6665();
void Butterfly_6666();
void Butterfly_6667();
void Butterfly_6668();
void WEIGHTED_ROUND_ROBIN_Joiner_7028();
void WEIGHTED_ROUND_ROBIN_Joiner_7029();
int BitReverse_6669_bitrev(int inp, int numbits);
void BitReverse(buffer_float_t *chanin, buffer_float_t *chanout);
void BitReverse_6669();
void FloatPrinter(buffer_float_t *chanin);
void FloatPrinter_6670();

#ifdef __cplusplus
}
#endif
#endif
