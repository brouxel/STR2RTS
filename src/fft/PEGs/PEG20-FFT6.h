#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=640 on the compile command line
#else
#if BUF_SIZEMAX < 640
#error BUF_SIZEMAX too small, it must be at least 640
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	complex_t wn;
} CombineDFT_2879_t;
void FFTTestSource(buffer_complex_t *chanout);
void FFTTestSource_2825();
void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout);
void FFTReorderSimple_2826();
void WEIGHTED_ROUND_ROBIN_Splitter_2839();
void FFTReorderSimple_2841();
void FFTReorderSimple_2842();
void WEIGHTED_ROUND_ROBIN_Joiner_2840();
void WEIGHTED_ROUND_ROBIN_Splitter_2843();
void FFTReorderSimple_2845();
void FFTReorderSimple_2846();
void FFTReorderSimple_2847();
void FFTReorderSimple_2848();
void WEIGHTED_ROUND_ROBIN_Joiner_2844();
void WEIGHTED_ROUND_ROBIN_Splitter_2849();
void FFTReorderSimple_2851();
void FFTReorderSimple_2852();
void FFTReorderSimple_2853();
void FFTReorderSimple_2854();
void FFTReorderSimple_2855();
void FFTReorderSimple_2856();
void FFTReorderSimple_2857();
void FFTReorderSimple_2858();
void WEIGHTED_ROUND_ROBIN_Joiner_2850();
void WEIGHTED_ROUND_ROBIN_Splitter_2859();
void FFTReorderSimple_2861();
void FFTReorderSimple_2862();
void FFTReorderSimple_2863();
void FFTReorderSimple_2864();
void FFTReorderSimple_2865();
void FFTReorderSimple_2866();
void FFTReorderSimple_2867();
void FFTReorderSimple_2868();
void FFTReorderSimple_2869();
void FFTReorderSimple_2870();
void FFTReorderSimple_2871();
void FFTReorderSimple_2872();
void FFTReorderSimple_2873();
void FFTReorderSimple_2874();
void FFTReorderSimple_2875();
void FFTReorderSimple_2876();
void WEIGHTED_ROUND_ROBIN_Joiner_2860();
void WEIGHTED_ROUND_ROBIN_Splitter_2877();
void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout);
void CombineDFT_2879();
void CombineDFT_2880();
void CombineDFT_2881();
void CombineDFT_2882();
void CombineDFT_2883();
void CombineDFT_2884();
void CombineDFT_2885();
void CombineDFT_2886();
void CombineDFT_2887();
void CombineDFT_2888();
void CombineDFT_2889();
void CombineDFT_2890();
void CombineDFT_2891();
void CombineDFT_2892();
void CombineDFT_2893();
void CombineDFT_2894();
void CombineDFT_2895();
void CombineDFT_2896();
void CombineDFT_2897();
void CombineDFT_2898();
void WEIGHTED_ROUND_ROBIN_Joiner_2878();
void WEIGHTED_ROUND_ROBIN_Splitter_2899();
void CombineDFT_2901();
void CombineDFT_2902();
void CombineDFT_2903();
void CombineDFT_2904();
void CombineDFT_2905();
void CombineDFT_2906();
void CombineDFT_2907();
void CombineDFT_2908();
void CombineDFT_2909();
void CombineDFT_2910();
void CombineDFT_2911();
void CombineDFT_2912();
void CombineDFT_2913();
void CombineDFT_2914();
void CombineDFT_2915();
void CombineDFT_2916();
void WEIGHTED_ROUND_ROBIN_Joiner_2900();
void WEIGHTED_ROUND_ROBIN_Splitter_2917();
void CombineDFT_2919();
void CombineDFT_2920();
void CombineDFT_2921();
void CombineDFT_2922();
void CombineDFT_2923();
void CombineDFT_2924();
void CombineDFT_2925();
void CombineDFT_2926();
void WEIGHTED_ROUND_ROBIN_Joiner_2918();
void WEIGHTED_ROUND_ROBIN_Splitter_2927();
void CombineDFT_2929();
void CombineDFT_2930();
void CombineDFT_2931();
void CombineDFT_2932();
void WEIGHTED_ROUND_ROBIN_Joiner_2928();
void WEIGHTED_ROUND_ROBIN_Splitter_2933();
void CombineDFT_2935();
void CombineDFT_2936();
void WEIGHTED_ROUND_ROBIN_Joiner_2934();
void CombineDFT_2836();
void CPrinter(buffer_complex_t *chanin);
void CPrinter_2837();

#ifdef __cplusplus
}
#endif
#endif
