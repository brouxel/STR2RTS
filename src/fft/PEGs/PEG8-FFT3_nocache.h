#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=128 on the compile command line
#else
#if BUF_SIZEMAX < 128
#error BUF_SIZEMAX too small, it must be at least 128
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float A_re[32];
	float A_im[32];
	int idx;
} FloatSource_7310_t;
void FloatSource_7310();
void Pre_CollapsedDataParallel_1_7611();
void WEIGHTED_ROUND_ROBIN_Splitter_7753();
void Butterfly_7755();
void Butterfly_7756();
void Butterfly_7757();
void Butterfly_7758();
void Butterfly_7759();
void Butterfly_7760();
void Butterfly_7761();
void Butterfly_7762();
void WEIGHTED_ROUND_ROBIN_Joiner_7754();
void Post_CollapsedDataParallel_2_7612();
void WEIGHTED_ROUND_ROBIN_Splitter_7655();
void Pre_CollapsedDataParallel_1_7614();
void WEIGHTED_ROUND_ROBIN_Splitter_7763();
void Butterfly_7765();
void Butterfly_7766();
void Butterfly_7767();
void Butterfly_7768();
void Butterfly_7769();
void Butterfly_7770();
void Butterfly_7771();
void Butterfly_7772();
void WEIGHTED_ROUND_ROBIN_Joiner_7764();
void Post_CollapsedDataParallel_2_7615();
void WEIGHTED_ROUND_ROBIN_Splitter_7735();
void Pre_CollapsedDataParallel_1_7620();
void WEIGHTED_ROUND_ROBIN_Splitter_7773();
void Butterfly_7775();
void Butterfly_7776();
void Butterfly_7777();
void Butterfly_7778();
void WEIGHTED_ROUND_ROBIN_Joiner_7774();
void Post_CollapsedDataParallel_2_7621();
void Pre_CollapsedDataParallel_1_7623();
void WEIGHTED_ROUND_ROBIN_Splitter_7779();
void Butterfly_7781();
void Butterfly_7782();
void Butterfly_7783();
void Butterfly_7784();
void WEIGHTED_ROUND_ROBIN_Joiner_7780();
void Post_CollapsedDataParallel_2_7624();
void WEIGHTED_ROUND_ROBIN_Joiner_7736();
void Pre_CollapsedDataParallel_1_7617();
void WEIGHTED_ROUND_ROBIN_Splitter_7785();
void Butterfly_7787();
void Butterfly_7788();
void Butterfly_7789();
void Butterfly_7790();
void Butterfly_7791();
void Butterfly_7792();
void Butterfly_7793();
void Butterfly_7794();
void WEIGHTED_ROUND_ROBIN_Joiner_7786();
void Post_CollapsedDataParallel_2_7618();
void WEIGHTED_ROUND_ROBIN_Splitter_7737();
void Pre_CollapsedDataParallel_1_7626();
void WEIGHTED_ROUND_ROBIN_Splitter_7795();
void Butterfly_7797();
void Butterfly_7798();
void Butterfly_7799();
void Butterfly_7800();
void WEIGHTED_ROUND_ROBIN_Joiner_7796();
void Post_CollapsedDataParallel_2_7627();
void Pre_CollapsedDataParallel_1_7629();
void WEIGHTED_ROUND_ROBIN_Splitter_7801();
void Butterfly_7803();
void Butterfly_7804();
void Butterfly_7805();
void Butterfly_7806();
void WEIGHTED_ROUND_ROBIN_Joiner_7802();
void Post_CollapsedDataParallel_2_7630();
void WEIGHTED_ROUND_ROBIN_Joiner_7738();
void WEIGHTED_ROUND_ROBIN_Joiner_7739();
void WEIGHTED_ROUND_ROBIN_Splitter_7740();
void WEIGHTED_ROUND_ROBIN_Splitter_7741();
void Pre_CollapsedDataParallel_1_7632();
void WEIGHTED_ROUND_ROBIN_Splitter_7807();
void Butterfly_7809();
void Butterfly_7810();
void WEIGHTED_ROUND_ROBIN_Joiner_7808();
void Post_CollapsedDataParallel_2_7633();
void Pre_CollapsedDataParallel_1_7635();
void WEIGHTED_ROUND_ROBIN_Splitter_7811();
void Butterfly_7813();
void Butterfly_7814();
void WEIGHTED_ROUND_ROBIN_Joiner_7812();
void Post_CollapsedDataParallel_2_7636();
void Pre_CollapsedDataParallel_1_7638();
void WEIGHTED_ROUND_ROBIN_Splitter_7815();
void Butterfly_7817();
void Butterfly_7818();
void WEIGHTED_ROUND_ROBIN_Joiner_7816();
void Post_CollapsedDataParallel_2_7639();
void Pre_CollapsedDataParallel_1_7641();
void WEIGHTED_ROUND_ROBIN_Splitter_7819();
void Butterfly_7821();
void Butterfly_7822();
void WEIGHTED_ROUND_ROBIN_Joiner_7820();
void Post_CollapsedDataParallel_2_7642();
void WEIGHTED_ROUND_ROBIN_Joiner_7742();
void WEIGHTED_ROUND_ROBIN_Splitter_7743();
void Pre_CollapsedDataParallel_1_7644();
void WEIGHTED_ROUND_ROBIN_Splitter_7823();
void Butterfly_7825();
void Butterfly_7826();
void WEIGHTED_ROUND_ROBIN_Joiner_7824();
void Post_CollapsedDataParallel_2_7645();
void Pre_CollapsedDataParallel_1_7647();
void WEIGHTED_ROUND_ROBIN_Splitter_7827();
void Butterfly_7829();
void Butterfly_7830();
void WEIGHTED_ROUND_ROBIN_Joiner_7828();
void Post_CollapsedDataParallel_2_7648();
void Pre_CollapsedDataParallel_1_7650();
void WEIGHTED_ROUND_ROBIN_Splitter_7831();
void Butterfly_7833();
void Butterfly_7834();
void WEIGHTED_ROUND_ROBIN_Joiner_7832();
void Post_CollapsedDataParallel_2_7651();
void Pre_CollapsedDataParallel_1_7653();
void WEIGHTED_ROUND_ROBIN_Splitter_7835();
void Butterfly_7837();
void Butterfly_7838();
void WEIGHTED_ROUND_ROBIN_Joiner_7836();
void Post_CollapsedDataParallel_2_7654();
void WEIGHTED_ROUND_ROBIN_Joiner_7744();
void WEIGHTED_ROUND_ROBIN_Joiner_7745();
void WEIGHTED_ROUND_ROBIN_Splitter_7746();
void WEIGHTED_ROUND_ROBIN_Splitter_7747();
void Butterfly_7375();
void Butterfly_7376();
void Butterfly_7377();
void Butterfly_7378();
void Butterfly_7379();
void Butterfly_7380();
void Butterfly_7381();
void Butterfly_7382();
void WEIGHTED_ROUND_ROBIN_Joiner_7748();
void WEIGHTED_ROUND_ROBIN_Splitter_7749();
void Butterfly_7383();
void Butterfly_7384();
void Butterfly_7385();
void Butterfly_7386();
void Butterfly_7387();
void Butterfly_7388();
void Butterfly_7389();
void Butterfly_7390();
void WEIGHTED_ROUND_ROBIN_Joiner_7750();
void WEIGHTED_ROUND_ROBIN_Joiner_7751();
int BitReverse_7391_bitrev(int inp, int numbits);
void BitReverse_7391();
void FloatPrinter_7392();

#ifdef __cplusplus
}
#endif
#endif
