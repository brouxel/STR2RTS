#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=1152 on the compile command line
#else
#if BUF_SIZEMAX < 1152
#error BUF_SIZEMAX too small, it must be at least 1152
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	complex_t wn;
} CombineDFT_3309_t;
void FFTTestSource_3255();
void FFTReorderSimple_3256();
void WEIGHTED_ROUND_ROBIN_Splitter_3269();
void FFTReorderSimple_3271();
void FFTReorderSimple_3272();
void WEIGHTED_ROUND_ROBIN_Joiner_3270();
void WEIGHTED_ROUND_ROBIN_Splitter_3273();
void FFTReorderSimple_3275();
void FFTReorderSimple_3276();
void FFTReorderSimple_3277();
void FFTReorderSimple_3278();
void WEIGHTED_ROUND_ROBIN_Joiner_3274();
void WEIGHTED_ROUND_ROBIN_Splitter_3279();
void FFTReorderSimple_3281();
void FFTReorderSimple_3282();
void FFTReorderSimple_3283();
void FFTReorderSimple_3284();
void FFTReorderSimple_3285();
void FFTReorderSimple_3286();
void FFTReorderSimple_3287();
void FFTReorderSimple_3288();
void WEIGHTED_ROUND_ROBIN_Joiner_3280();
void WEIGHTED_ROUND_ROBIN_Splitter_3289();
void FFTReorderSimple_3291();
void FFTReorderSimple_3292();
void FFTReorderSimple_3293();
void FFTReorderSimple_3294();
void FFTReorderSimple_3295();
void FFTReorderSimple_3296();
void FFTReorderSimple_3297();
void FFTReorderSimple_3298();
void FFTReorderSimple_3299();
void FFTReorderSimple_3300();
void FFTReorderSimple_3301();
void FFTReorderSimple_3302();
void FFTReorderSimple_3303();
void FFTReorderSimple_3304();
void FFTReorderSimple_3305();
void FFTReorderSimple_3306();
void WEIGHTED_ROUND_ROBIN_Joiner_3290();
void WEIGHTED_ROUND_ROBIN_Splitter_3307();
void CombineDFT_3309();
void CombineDFT_3310();
void CombineDFT_3311();
void CombineDFT_3312();
void CombineDFT_3313();
void CombineDFT_3314();
void CombineDFT_3315();
void CombineDFT_3316();
void CombineDFT_3317();
void CombineDFT_3318();
void CombineDFT_3319();
void CombineDFT_3320();
void CombineDFT_3321();
void CombineDFT_3322();
void CombineDFT_3323();
void CombineDFT_3324();
void CombineDFT_3325();
void CombineDFT_3326();
void WEIGHTED_ROUND_ROBIN_Joiner_3308();
void WEIGHTED_ROUND_ROBIN_Splitter_3327();
void CombineDFT_3329();
void CombineDFT_3330();
void CombineDFT_3331();
void CombineDFT_3332();
void CombineDFT_3333();
void CombineDFT_3334();
void CombineDFT_3335();
void CombineDFT_3336();
void CombineDFT_3337();
void CombineDFT_3338();
void CombineDFT_3339();
void CombineDFT_3340();
void CombineDFT_3341();
void CombineDFT_3342();
void CombineDFT_3343();
void CombineDFT_3344();
void WEIGHTED_ROUND_ROBIN_Joiner_3328();
void WEIGHTED_ROUND_ROBIN_Splitter_3345();
void CombineDFT_3347();
void CombineDFT_3348();
void CombineDFT_3349();
void CombineDFT_3350();
void CombineDFT_3351();
void CombineDFT_3352();
void CombineDFT_3353();
void CombineDFT_3354();
void WEIGHTED_ROUND_ROBIN_Joiner_3346();
void WEIGHTED_ROUND_ROBIN_Splitter_3355();
void CombineDFT_3357();
void CombineDFT_3358();
void CombineDFT_3359();
void CombineDFT_3360();
void WEIGHTED_ROUND_ROBIN_Joiner_3356();
void WEIGHTED_ROUND_ROBIN_Splitter_3361();
void CombineDFT_3363();
void CombineDFT_3364();
void WEIGHTED_ROUND_ROBIN_Joiner_3362();
void CombineDFT_3266();
void CPrinter_3267();

#ifdef __cplusplus
}
#endif
#endif
