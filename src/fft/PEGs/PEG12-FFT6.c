#include "PEG12-FFT6.h"

buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_4569_4579_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4560WEIGHTED_ROUND_ROBIN_Splitter_4565;
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_4571_4581_join[8];
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_4572_4582_split[12];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4508WEIGHTED_ROUND_ROBIN_Splitter_4521;
buffer_complex_t SplitJoin12_CombineDFT_Fiss_4575_4585_split[8];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4488WEIGHTED_ROUND_ROBIN_Splitter_4491;
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_4569_4579_join[2];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_4577_4587_split[2];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_4577_4587_join[2];
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_4570_4580_split[4];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_4571_4581_split[8];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_4576_4586_join[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4522WEIGHTED_ROUND_ROBIN_Splitter_4535;
buffer_complex_t SplitJoin10_CombineDFT_Fiss_4574_4584_join[12];
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_4570_4580_join[4];
buffer_complex_t FFTReorderSimple_4474WEIGHTED_ROUND_ROBIN_Splitter_4487;
buffer_complex_t SplitJoin14_CombineDFT_Fiss_4576_4586_split[4];
buffer_complex_t CombineDFT_4484CPrinter_4485;
buffer_complex_t FFTTestSource_4473FFTReorderSimple_4474;
buffer_complex_t SplitJoin10_CombineDFT_Fiss_4574_4584_split[12];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4498WEIGHTED_ROUND_ROBIN_Splitter_4507;
buffer_complex_t SplitJoin12_CombineDFT_Fiss_4575_4585_join[8];
buffer_complex_t SplitJoin8_CombineDFT_Fiss_4573_4583_split[12];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4566CombineDFT_4484;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_4573_4583_join[12];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4536WEIGHTED_ROUND_ROBIN_Splitter_4549;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4550WEIGHTED_ROUND_ROBIN_Splitter_4559;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4492WEIGHTED_ROUND_ROBIN_Splitter_4497;
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_4572_4582_join[12];


CombineDFT_4523_t CombineDFT_4523_s;
CombineDFT_4523_t CombineDFT_4524_s;
CombineDFT_4523_t CombineDFT_4525_s;
CombineDFT_4523_t CombineDFT_4526_s;
CombineDFT_4523_t CombineDFT_4527_s;
CombineDFT_4523_t CombineDFT_4528_s;
CombineDFT_4523_t CombineDFT_4529_s;
CombineDFT_4523_t CombineDFT_4530_s;
CombineDFT_4523_t CombineDFT_4531_s;
CombineDFT_4523_t CombineDFT_4532_s;
CombineDFT_4523_t CombineDFT_4533_s;
CombineDFT_4523_t CombineDFT_4534_s;
CombineDFT_4523_t CombineDFT_4537_s;
CombineDFT_4523_t CombineDFT_4538_s;
CombineDFT_4523_t CombineDFT_4539_s;
CombineDFT_4523_t CombineDFT_4540_s;
CombineDFT_4523_t CombineDFT_4541_s;
CombineDFT_4523_t CombineDFT_4542_s;
CombineDFT_4523_t CombineDFT_4543_s;
CombineDFT_4523_t CombineDFT_4544_s;
CombineDFT_4523_t CombineDFT_4545_s;
CombineDFT_4523_t CombineDFT_4546_s;
CombineDFT_4523_t CombineDFT_4547_s;
CombineDFT_4523_t CombineDFT_4548_s;
CombineDFT_4523_t CombineDFT_4551_s;
CombineDFT_4523_t CombineDFT_4552_s;
CombineDFT_4523_t CombineDFT_4553_s;
CombineDFT_4523_t CombineDFT_4554_s;
CombineDFT_4523_t CombineDFT_4555_s;
CombineDFT_4523_t CombineDFT_4556_s;
CombineDFT_4523_t CombineDFT_4557_s;
CombineDFT_4523_t CombineDFT_4558_s;
CombineDFT_4523_t CombineDFT_4561_s;
CombineDFT_4523_t CombineDFT_4562_s;
CombineDFT_4523_t CombineDFT_4563_s;
CombineDFT_4523_t CombineDFT_4564_s;
CombineDFT_4523_t CombineDFT_4567_s;
CombineDFT_4523_t CombineDFT_4568_s;
CombineDFT_4523_t CombineDFT_4484_s;

void FFTTestSource(buffer_complex_t *chanout) {
		complex_t c1;
		complex_t zero;
		c1.real = 1.0 ; 
		c1.imag = 0.0 ; 
		zero.real = 0.0 ; 
		zero.imag = 0.0 ; 
		push_complex(&(*chanout), zero) ; 
		push_complex(&(*chanout), c1) ; 
		FOR(int, i, 0,  < , 62, i++) {
			push_complex(&(*chanout), zero) ; 
		}
		ENDFOR
	}


void FFTTestSource_4473() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTTestSource(&(FFTTestSource_4473FFTReorderSimple_4474));
	ENDFOR
}

void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 64, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&(*chanin)) ; 
		}
		ENDFOR
	}


void FFTReorderSimple_4474() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(FFTTestSource_4473FFTReorderSimple_4474), &(FFTReorderSimple_4474WEIGHTED_ROUND_ROBIN_Splitter_4487));
	ENDFOR
}

void FFTReorderSimple_4489() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_4569_4579_split[0]), &(SplitJoin0_FFTReorderSimple_Fiss_4569_4579_join[0]));
	ENDFOR
}

void FFTReorderSimple_4490() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_4569_4579_split[1]), &(SplitJoin0_FFTReorderSimple_Fiss_4569_4579_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4487() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_4569_4579_split[0], pop_complex(&FFTReorderSimple_4474WEIGHTED_ROUND_ROBIN_Splitter_4487));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_4569_4579_split[1], pop_complex(&FFTReorderSimple_4474WEIGHTED_ROUND_ROBIN_Splitter_4487));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4488() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4488WEIGHTED_ROUND_ROBIN_Splitter_4491, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_4569_4579_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4488WEIGHTED_ROUND_ROBIN_Splitter_4491, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_4569_4579_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_4493() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_4570_4580_split[0]), &(SplitJoin2_FFTReorderSimple_Fiss_4570_4580_join[0]));
	ENDFOR
}

void FFTReorderSimple_4494() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_4570_4580_split[1]), &(SplitJoin2_FFTReorderSimple_Fiss_4570_4580_join[1]));
	ENDFOR
}

void FFTReorderSimple_4495() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_4570_4580_split[2]), &(SplitJoin2_FFTReorderSimple_Fiss_4570_4580_join[2]));
	ENDFOR
}

void FFTReorderSimple_4496() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_4570_4580_split[3]), &(SplitJoin2_FFTReorderSimple_Fiss_4570_4580_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4491() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin2_FFTReorderSimple_Fiss_4570_4580_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4488WEIGHTED_ROUND_ROBIN_Splitter_4491));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4492() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4492WEIGHTED_ROUND_ROBIN_Splitter_4497, pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_4570_4580_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_4499() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_4571_4581_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_4571_4581_join[0]));
	ENDFOR
}

void FFTReorderSimple_4500() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_4571_4581_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_4571_4581_join[1]));
	ENDFOR
}

void FFTReorderSimple_4501() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_4571_4581_split[2]), &(SplitJoin4_FFTReorderSimple_Fiss_4571_4581_join[2]));
	ENDFOR
}

void FFTReorderSimple_4502() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_4571_4581_split[3]), &(SplitJoin4_FFTReorderSimple_Fiss_4571_4581_join[3]));
	ENDFOR
}

void FFTReorderSimple_4503() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_4571_4581_split[4]), &(SplitJoin4_FFTReorderSimple_Fiss_4571_4581_join[4]));
	ENDFOR
}

void FFTReorderSimple_4504() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_4571_4581_split[5]), &(SplitJoin4_FFTReorderSimple_Fiss_4571_4581_join[5]));
	ENDFOR
}

void FFTReorderSimple_4505() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_4571_4581_split[6]), &(SplitJoin4_FFTReorderSimple_Fiss_4571_4581_join[6]));
	ENDFOR
}

void FFTReorderSimple_4506() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_4571_4581_split[7]), &(SplitJoin4_FFTReorderSimple_Fiss_4571_4581_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4497() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4571_4581_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4492WEIGHTED_ROUND_ROBIN_Splitter_4497));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4498() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4498WEIGHTED_ROUND_ROBIN_Splitter_4507, pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_4571_4581_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_4509() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4572_4582_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_4572_4582_join[0]));
	ENDFOR
}

void FFTReorderSimple_4510() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4572_4582_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_4572_4582_join[1]));
	ENDFOR
}

void FFTReorderSimple_4511() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4572_4582_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_4572_4582_join[2]));
	ENDFOR
}

void FFTReorderSimple_4512() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4572_4582_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_4572_4582_join[3]));
	ENDFOR
}

void FFTReorderSimple_4513() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4572_4582_split[4]), &(SplitJoin6_FFTReorderSimple_Fiss_4572_4582_join[4]));
	ENDFOR
}

void FFTReorderSimple_4514() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4572_4582_split[5]), &(SplitJoin6_FFTReorderSimple_Fiss_4572_4582_join[5]));
	ENDFOR
}

void FFTReorderSimple_4515() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4572_4582_split[6]), &(SplitJoin6_FFTReorderSimple_Fiss_4572_4582_join[6]));
	ENDFOR
}

void FFTReorderSimple_4516() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4572_4582_split[7]), &(SplitJoin6_FFTReorderSimple_Fiss_4572_4582_join[7]));
	ENDFOR
}

void FFTReorderSimple_4517() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4572_4582_split[8]), &(SplitJoin6_FFTReorderSimple_Fiss_4572_4582_join[8]));
	ENDFOR
}

void FFTReorderSimple_4518() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4572_4582_split[9]), &(SplitJoin6_FFTReorderSimple_Fiss_4572_4582_join[9]));
	ENDFOR
}

void FFTReorderSimple_4519() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4572_4582_split[10]), &(SplitJoin6_FFTReorderSimple_Fiss_4572_4582_join[10]));
	ENDFOR
}

void FFTReorderSimple_4520() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4572_4582_split[11]), &(SplitJoin6_FFTReorderSimple_Fiss_4572_4582_join[11]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4507() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 12, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4572_4582_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4498WEIGHTED_ROUND_ROBIN_Splitter_4507));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4508() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 12, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4508WEIGHTED_ROUND_ROBIN_Splitter_4521, pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_4572_4582_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&(*chanin), (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4523_s.wn.real) - (w.imag * CombineDFT_4523_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4523_s.wn.imag) + (w.imag * CombineDFT_4523_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&(*chanin)) ; 
			push_complex(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_4523() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4573_4583_split[0]), &(SplitJoin8_CombineDFT_Fiss_4573_4583_join[0]));
	ENDFOR
}

void CombineDFT_4524() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4573_4583_split[1]), &(SplitJoin8_CombineDFT_Fiss_4573_4583_join[1]));
	ENDFOR
}

void CombineDFT_4525() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4573_4583_split[2]), &(SplitJoin8_CombineDFT_Fiss_4573_4583_join[2]));
	ENDFOR
}

void CombineDFT_4526() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4573_4583_split[3]), &(SplitJoin8_CombineDFT_Fiss_4573_4583_join[3]));
	ENDFOR
}

void CombineDFT_4527() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4573_4583_split[4]), &(SplitJoin8_CombineDFT_Fiss_4573_4583_join[4]));
	ENDFOR
}

void CombineDFT_4528() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4573_4583_split[5]), &(SplitJoin8_CombineDFT_Fiss_4573_4583_join[5]));
	ENDFOR
}

void CombineDFT_4529() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4573_4583_split[6]), &(SplitJoin8_CombineDFT_Fiss_4573_4583_join[6]));
	ENDFOR
}

void CombineDFT_4530() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4573_4583_split[7]), &(SplitJoin8_CombineDFT_Fiss_4573_4583_join[7]));
	ENDFOR
}

void CombineDFT_4531() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4573_4583_split[8]), &(SplitJoin8_CombineDFT_Fiss_4573_4583_join[8]));
	ENDFOR
}

void CombineDFT_4532() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4573_4583_split[9]), &(SplitJoin8_CombineDFT_Fiss_4573_4583_join[9]));
	ENDFOR
}

void CombineDFT_4533() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4573_4583_split[10]), &(SplitJoin8_CombineDFT_Fiss_4573_4583_join[10]));
	ENDFOR
}

void CombineDFT_4534() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4573_4583_split[11]), &(SplitJoin8_CombineDFT_Fiss_4573_4583_join[11]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4521() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 12, __iter_++)
			push_complex(&SplitJoin8_CombineDFT_Fiss_4573_4583_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4508WEIGHTED_ROUND_ROBIN_Splitter_4521));
			push_complex(&SplitJoin8_CombineDFT_Fiss_4573_4583_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4508WEIGHTED_ROUND_ROBIN_Splitter_4521));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4522() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 12, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4522WEIGHTED_ROUND_ROBIN_Splitter_4535, pop_complex(&SplitJoin8_CombineDFT_Fiss_4573_4583_join[__iter_]));
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4522WEIGHTED_ROUND_ROBIN_Splitter_4535, pop_complex(&SplitJoin8_CombineDFT_Fiss_4573_4583_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_4537() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4574_4584_split[0]), &(SplitJoin10_CombineDFT_Fiss_4574_4584_join[0]));
	ENDFOR
}

void CombineDFT_4538() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4574_4584_split[1]), &(SplitJoin10_CombineDFT_Fiss_4574_4584_join[1]));
	ENDFOR
}

void CombineDFT_4539() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4574_4584_split[2]), &(SplitJoin10_CombineDFT_Fiss_4574_4584_join[2]));
	ENDFOR
}

void CombineDFT_4540() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4574_4584_split[3]), &(SplitJoin10_CombineDFT_Fiss_4574_4584_join[3]));
	ENDFOR
}

void CombineDFT_4541() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4574_4584_split[4]), &(SplitJoin10_CombineDFT_Fiss_4574_4584_join[4]));
	ENDFOR
}

void CombineDFT_4542() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4574_4584_split[5]), &(SplitJoin10_CombineDFT_Fiss_4574_4584_join[5]));
	ENDFOR
}

void CombineDFT_4543() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4574_4584_split[6]), &(SplitJoin10_CombineDFT_Fiss_4574_4584_join[6]));
	ENDFOR
}

void CombineDFT_4544() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4574_4584_split[7]), &(SplitJoin10_CombineDFT_Fiss_4574_4584_join[7]));
	ENDFOR
}

void CombineDFT_4545() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4574_4584_split[8]), &(SplitJoin10_CombineDFT_Fiss_4574_4584_join[8]));
	ENDFOR
}

void CombineDFT_4546() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4574_4584_split[9]), &(SplitJoin10_CombineDFT_Fiss_4574_4584_join[9]));
	ENDFOR
}

void CombineDFT_4547() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4574_4584_split[10]), &(SplitJoin10_CombineDFT_Fiss_4574_4584_join[10]));
	ENDFOR
}

void CombineDFT_4548() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4574_4584_split[11]), &(SplitJoin10_CombineDFT_Fiss_4574_4584_join[11]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4535() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 12, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin10_CombineDFT_Fiss_4574_4584_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4522WEIGHTED_ROUND_ROBIN_Splitter_4535));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4536() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 12, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4536WEIGHTED_ROUND_ROBIN_Splitter_4549, pop_complex(&SplitJoin10_CombineDFT_Fiss_4574_4584_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_4551() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_4575_4585_split[0]), &(SplitJoin12_CombineDFT_Fiss_4575_4585_join[0]));
	ENDFOR
}

void CombineDFT_4552() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_4575_4585_split[1]), &(SplitJoin12_CombineDFT_Fiss_4575_4585_join[1]));
	ENDFOR
}

void CombineDFT_4553() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_4575_4585_split[2]), &(SplitJoin12_CombineDFT_Fiss_4575_4585_join[2]));
	ENDFOR
}

void CombineDFT_4554() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_4575_4585_split[3]), &(SplitJoin12_CombineDFT_Fiss_4575_4585_join[3]));
	ENDFOR
}

void CombineDFT_4555() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_4575_4585_split[4]), &(SplitJoin12_CombineDFT_Fiss_4575_4585_join[4]));
	ENDFOR
}

void CombineDFT_4556() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_4575_4585_split[5]), &(SplitJoin12_CombineDFT_Fiss_4575_4585_join[5]));
	ENDFOR
}

void CombineDFT_4557() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_4575_4585_split[6]), &(SplitJoin12_CombineDFT_Fiss_4575_4585_join[6]));
	ENDFOR
}

void CombineDFT_4558() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_4575_4585_split[7]), &(SplitJoin12_CombineDFT_Fiss_4575_4585_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4549() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin12_CombineDFT_Fiss_4575_4585_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4536WEIGHTED_ROUND_ROBIN_Splitter_4549));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4550() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4550WEIGHTED_ROUND_ROBIN_Splitter_4559, pop_complex(&SplitJoin12_CombineDFT_Fiss_4575_4585_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_4561() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_4576_4586_split[0]), &(SplitJoin14_CombineDFT_Fiss_4576_4586_join[0]));
	ENDFOR
}

void CombineDFT_4562() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_4576_4586_split[1]), &(SplitJoin14_CombineDFT_Fiss_4576_4586_join[1]));
	ENDFOR
}

void CombineDFT_4563() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_4576_4586_split[2]), &(SplitJoin14_CombineDFT_Fiss_4576_4586_join[2]));
	ENDFOR
}

void CombineDFT_4564() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_4576_4586_split[3]), &(SplitJoin14_CombineDFT_Fiss_4576_4586_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4559() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin14_CombineDFT_Fiss_4576_4586_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4550WEIGHTED_ROUND_ROBIN_Splitter_4559));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4560() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4560WEIGHTED_ROUND_ROBIN_Splitter_4565, pop_complex(&SplitJoin14_CombineDFT_Fiss_4576_4586_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_4567() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_4577_4587_split[0]), &(SplitJoin16_CombineDFT_Fiss_4577_4587_join[0]));
	ENDFOR
}

void CombineDFT_4568() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_4577_4587_split[1]), &(SplitJoin16_CombineDFT_Fiss_4577_4587_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4565() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_4577_4587_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4560WEIGHTED_ROUND_ROBIN_Splitter_4565));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_4577_4587_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4560WEIGHTED_ROUND_ROBIN_Splitter_4565));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4566() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4566CombineDFT_4484, pop_complex(&SplitJoin16_CombineDFT_Fiss_4577_4587_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4566CombineDFT_4484, pop_complex(&SplitJoin16_CombineDFT_Fiss_4577_4587_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_4484() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_4566CombineDFT_4484), &(CombineDFT_4484CPrinter_4485));
	ENDFOR
}

void CPrinter(buffer_complex_t *chanin) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		printf("%.10f", c.real);
		printf("\n");
		printf("%.10f", c.imag);
		printf("\n");
	}


void CPrinter_4485() {
	FOR(uint32_t, __iter_steady_, 0, <, 192, __iter_steady_++)
		CPrinter(&(CombineDFT_4484CPrinter_4485));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_4569_4579_split[__iter_init_0_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4560WEIGHTED_ROUND_ROBIN_Splitter_4565);
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_4571_4581_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 12, __iter_init_2_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_4572_4582_split[__iter_init_2_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4508WEIGHTED_ROUND_ROBIN_Splitter_4521);
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_4575_4585_split[__iter_init_3_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4488WEIGHTED_ROUND_ROBIN_Splitter_4491);
	FOR(int, __iter_init_4_, 0, <, 2, __iter_init_4_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_4569_4579_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 2, __iter_init_5_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_4577_4587_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 2, __iter_init_6_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_4577_4587_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 4, __iter_init_7_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_4570_4580_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_4571_4581_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 4, __iter_init_9_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_4576_4586_join[__iter_init_9_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4522WEIGHTED_ROUND_ROBIN_Splitter_4535);
	FOR(int, __iter_init_10_, 0, <, 12, __iter_init_10_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_4574_4584_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 4, __iter_init_11_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_4570_4580_join[__iter_init_11_]);
	ENDFOR
	init_buffer_complex(&FFTReorderSimple_4474WEIGHTED_ROUND_ROBIN_Splitter_4487);
	FOR(int, __iter_init_12_, 0, <, 4, __iter_init_12_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_4576_4586_split[__iter_init_12_]);
	ENDFOR
	init_buffer_complex(&CombineDFT_4484CPrinter_4485);
	init_buffer_complex(&FFTTestSource_4473FFTReorderSimple_4474);
	FOR(int, __iter_init_13_, 0, <, 12, __iter_init_13_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_4574_4584_split[__iter_init_13_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4498WEIGHTED_ROUND_ROBIN_Splitter_4507);
	FOR(int, __iter_init_14_, 0, <, 8, __iter_init_14_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_4575_4585_join[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 12, __iter_init_15_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_4573_4583_split[__iter_init_15_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4566CombineDFT_4484);
	FOR(int, __iter_init_16_, 0, <, 12, __iter_init_16_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_4573_4583_join[__iter_init_16_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4536WEIGHTED_ROUND_ROBIN_Splitter_4549);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4550WEIGHTED_ROUND_ROBIN_Splitter_4559);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4492WEIGHTED_ROUND_ROBIN_Splitter_4497);
	FOR(int, __iter_init_17_, 0, <, 12, __iter_init_17_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_4572_4582_join[__iter_init_17_]);
	ENDFOR
// --- init: CombineDFT_4523
	 {
	 ; 
	CombineDFT_4523_s.wn.real = -1.0 ; 
	CombineDFT_4523_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4524
	 {
	CombineDFT_4524_s.wn.real = -1.0 ; 
	CombineDFT_4524_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4525
	 {
	CombineDFT_4525_s.wn.real = -1.0 ; 
	CombineDFT_4525_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4526
	 {
	CombineDFT_4526_s.wn.real = -1.0 ; 
	CombineDFT_4526_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4527
	 {
	CombineDFT_4527_s.wn.real = -1.0 ; 
	CombineDFT_4527_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4528
	 {
	CombineDFT_4528_s.wn.real = -1.0 ; 
	CombineDFT_4528_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4529
	 {
	CombineDFT_4529_s.wn.real = -1.0 ; 
	CombineDFT_4529_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4530
	 {
	CombineDFT_4530_s.wn.real = -1.0 ; 
	CombineDFT_4530_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4531
	 {
	CombineDFT_4531_s.wn.real = -1.0 ; 
	CombineDFT_4531_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4532
	 {
	CombineDFT_4532_s.wn.real = -1.0 ; 
	CombineDFT_4532_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4533
	 {
	CombineDFT_4533_s.wn.real = -1.0 ; 
	CombineDFT_4533_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4534
	 {
	CombineDFT_4534_s.wn.real = -1.0 ; 
	CombineDFT_4534_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4537
	 {
	CombineDFT_4537_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4537_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4538
	 {
	CombineDFT_4538_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4538_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4539
	 {
	CombineDFT_4539_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4539_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4540
	 {
	CombineDFT_4540_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4540_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4541
	 {
	CombineDFT_4541_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4541_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4542
	 {
	CombineDFT_4542_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4542_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4543
	 {
	CombineDFT_4543_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4543_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4544
	 {
	CombineDFT_4544_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4544_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4545
	 {
	CombineDFT_4545_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4545_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4546
	 {
	CombineDFT_4546_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4546_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4547
	 {
	CombineDFT_4547_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4547_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4548
	 {
	CombineDFT_4548_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4548_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4551
	 {
	CombineDFT_4551_s.wn.real = 0.70710677 ; 
	CombineDFT_4551_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4552
	 {
	CombineDFT_4552_s.wn.real = 0.70710677 ; 
	CombineDFT_4552_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4553
	 {
	CombineDFT_4553_s.wn.real = 0.70710677 ; 
	CombineDFT_4553_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4554
	 {
	CombineDFT_4554_s.wn.real = 0.70710677 ; 
	CombineDFT_4554_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4555
	 {
	CombineDFT_4555_s.wn.real = 0.70710677 ; 
	CombineDFT_4555_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4556
	 {
	CombineDFT_4556_s.wn.real = 0.70710677 ; 
	CombineDFT_4556_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4557
	 {
	CombineDFT_4557_s.wn.real = 0.70710677 ; 
	CombineDFT_4557_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4558
	 {
	CombineDFT_4558_s.wn.real = 0.70710677 ; 
	CombineDFT_4558_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4561
	 {
	CombineDFT_4561_s.wn.real = 0.9238795 ; 
	CombineDFT_4561_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_4562
	 {
	CombineDFT_4562_s.wn.real = 0.9238795 ; 
	CombineDFT_4562_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_4563
	 {
	CombineDFT_4563_s.wn.real = 0.9238795 ; 
	CombineDFT_4563_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_4564
	 {
	CombineDFT_4564_s.wn.real = 0.9238795 ; 
	CombineDFT_4564_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_4567
	 {
	CombineDFT_4567_s.wn.real = 0.98078525 ; 
	CombineDFT_4567_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_4568
	 {
	CombineDFT_4568_s.wn.real = 0.98078525 ; 
	CombineDFT_4568_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_4484
	 {
	 ; 
	CombineDFT_4484_s.wn.real = 0.9951847 ; 
	CombineDFT_4484_s.wn.imag = -0.09801714 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FFTTestSource_4473();
		FFTReorderSimple_4474();
		WEIGHTED_ROUND_ROBIN_Splitter_4487();
			FFTReorderSimple_4489();
			FFTReorderSimple_4490();
		WEIGHTED_ROUND_ROBIN_Joiner_4488();
		WEIGHTED_ROUND_ROBIN_Splitter_4491();
			FFTReorderSimple_4493();
			FFTReorderSimple_4494();
			FFTReorderSimple_4495();
			FFTReorderSimple_4496();
		WEIGHTED_ROUND_ROBIN_Joiner_4492();
		WEIGHTED_ROUND_ROBIN_Splitter_4497();
			FFTReorderSimple_4499();
			FFTReorderSimple_4500();
			FFTReorderSimple_4501();
			FFTReorderSimple_4502();
			FFTReorderSimple_4503();
			FFTReorderSimple_4504();
			FFTReorderSimple_4505();
			FFTReorderSimple_4506();
		WEIGHTED_ROUND_ROBIN_Joiner_4498();
		WEIGHTED_ROUND_ROBIN_Splitter_4507();
			FFTReorderSimple_4509();
			FFTReorderSimple_4510();
			FFTReorderSimple_4511();
			FFTReorderSimple_4512();
			FFTReorderSimple_4513();
			FFTReorderSimple_4514();
			FFTReorderSimple_4515();
			FFTReorderSimple_4516();
			FFTReorderSimple_4517();
			FFTReorderSimple_4518();
			FFTReorderSimple_4519();
			FFTReorderSimple_4520();
		WEIGHTED_ROUND_ROBIN_Joiner_4508();
		WEIGHTED_ROUND_ROBIN_Splitter_4521();
			CombineDFT_4523();
			CombineDFT_4524();
			CombineDFT_4525();
			CombineDFT_4526();
			CombineDFT_4527();
			CombineDFT_4528();
			CombineDFT_4529();
			CombineDFT_4530();
			CombineDFT_4531();
			CombineDFT_4532();
			CombineDFT_4533();
			CombineDFT_4534();
		WEIGHTED_ROUND_ROBIN_Joiner_4522();
		WEIGHTED_ROUND_ROBIN_Splitter_4535();
			CombineDFT_4537();
			CombineDFT_4538();
			CombineDFT_4539();
			CombineDFT_4540();
			CombineDFT_4541();
			CombineDFT_4542();
			CombineDFT_4543();
			CombineDFT_4544();
			CombineDFT_4545();
			CombineDFT_4546();
			CombineDFT_4547();
			CombineDFT_4548();
		WEIGHTED_ROUND_ROBIN_Joiner_4536();
		WEIGHTED_ROUND_ROBIN_Splitter_4549();
			CombineDFT_4551();
			CombineDFT_4552();
			CombineDFT_4553();
			CombineDFT_4554();
			CombineDFT_4555();
			CombineDFT_4556();
			CombineDFT_4557();
			CombineDFT_4558();
		WEIGHTED_ROUND_ROBIN_Joiner_4550();
		WEIGHTED_ROUND_ROBIN_Splitter_4559();
			CombineDFT_4561();
			CombineDFT_4562();
			CombineDFT_4563();
			CombineDFT_4564();
		WEIGHTED_ROUND_ROBIN_Joiner_4560();
		WEIGHTED_ROUND_ROBIN_Splitter_4565();
			CombineDFT_4567();
			CombineDFT_4568();
		WEIGHTED_ROUND_ROBIN_Joiner_4566();
		CombineDFT_4484();
		CPrinter_4485();
	ENDFOR
	return EXIT_SUCCESS;
}
