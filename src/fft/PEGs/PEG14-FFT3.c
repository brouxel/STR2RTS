#include "PEG14-FFT3.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3377WEIGHTED_ROUND_ROBIN_Splitter_3378;
buffer_float_t SplitJoin72_Butterfly_Fiss_3499_3509_join[4];
buffer_float_t SplitJoin53_Butterfly_Fiss_3495_3521_split[2];
buffer_float_t SplitJoin4_Butterfly_Fiss_3485_3506_join[8];
buffer_float_t Pre_CollapsedDataParallel_1_3270WEIGHTED_ROUND_ROBIN_Splitter_3451;
buffer_float_t SplitJoin95_Butterfly_Fiss_3502_3513_split[4];
buffer_float_t SplitJoin61_Butterfly_Fiss_3497_3523_join[2];
buffer_float_t SplitJoin4_Butterfly_Fiss_3485_3506_split[8];
buffer_float_t SplitJoin43_Butterfly_Fiss_3493_3518_split[2];
buffer_float_t Post_CollapsedDataParallel_2_3250WEIGHTED_ROUND_ROBIN_Splitter_3293;
buffer_float_t SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_Hier_3489_3525_join[2];
buffer_float_t Pre_CollapsedDataParallel_1_3249WEIGHTED_ROUND_ROBIN_Splitter_3391;
buffer_float_t SplitJoin0_Butterfly_Fiss_3483_3504_join[14];
buffer_float_t Pre_CollapsedDataParallel_1_3276WEIGHTED_ROUND_ROBIN_Splitter_3459;
buffer_float_t SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child1_3491_3527_join[8];
buffer_float_t SplitJoin85_Butterfly_Fiss_3500_3510_split[8];
buffer_float_t Pre_CollapsedDataParallel_1_3252WEIGHTED_ROUND_ROBIN_Splitter_3407;
buffer_float_t SplitJoin57_Butterfly_Fiss_3496_3522_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3383WEIGHTED_ROUND_ROBIN_Splitter_3384;
buffer_float_t Pre_CollapsedDataParallel_1_3291WEIGHTED_ROUND_ROBIN_Splitter_3479;
buffer_float_t Pre_CollapsedDataParallel_1_3258WEIGHTED_ROUND_ROBIN_Splitter_3417;
buffer_float_t BitReverse_3029FloatPrinter_3030;
buffer_float_t Pre_CollapsedDataParallel_1_3285WEIGHTED_ROUND_ROBIN_Splitter_3471;
buffer_float_t SplitJoin14_Butterfly_Fiss_3488_3516_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3452Post_CollapsedDataParallel_2_3271;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3392Post_CollapsedDataParallel_2_3250;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3456Post_CollapsedDataParallel_2_3274;
buffer_float_t SplitJoin65_Butterfly_Fiss_3498_3524_join[2];
buffer_float_t SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_Hier_3487_3514_split[2];
buffer_float_t SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_Hier_3487_3514_join[2];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_2906_3301_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_Hier_3484_3505_split[2];
buffer_float_t SplitJoin14_Butterfly_Fiss_3488_3516_join[2];
buffer_float_t SplitJoin43_Butterfly_Fiss_3493_3518_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3460Post_CollapsedDataParallel_2_3277;
buffer_float_t SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_child1_3347_3511_join[2];
buffer_float_t SplitJoin89_Butterfly_Fiss_3501_3512_join[4];
buffer_float_t SplitJoin89_Butterfly_Fiss_3501_3512_split[4];
buffer_float_t Pre_CollapsedDataParallel_1_3255WEIGHTED_ROUND_ROBIN_Splitter_3429;
buffer_float_t Pre_CollapsedDataParallel_1_3267WEIGHTED_ROUND_ROBIN_Splitter_3445;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3480Post_CollapsedDataParallel_2_3292;
buffer_float_t SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_child1_3372_3520_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3424Post_CollapsedDataParallel_2_3262;
buffer_float_t SplitJoin85_Butterfly_Fiss_3500_3510_join[8];
buffer_float_t SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_child1_3347_3511_split[2];
buffer_float_t SplitJoin39_Butterfly_Fiss_3492_3517_join[2];
buffer_float_t Pre_CollapsedDataParallel_1_3282WEIGHTED_ROUND_ROBIN_Splitter_3467;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3430Post_CollapsedDataParallel_2_3256;
buffer_float_t SplitJoin47_Butterfly_Fiss_3494_3519_split[2];
buffer_float_t SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child0_3490_3526_split[8];
buffer_float_t Pre_CollapsedDataParallel_1_3264WEIGHTED_ROUND_ROBIN_Splitter_3439;
buffer_float_t SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_child0_3371_3515_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3418Post_CollapsedDataParallel_2_3259;
buffer_float_t SplitJoin57_Butterfly_Fiss_3496_3522_split[2];
buffer_float_t SplitJoin47_Butterfly_Fiss_3494_3519_join[2];
buffer_float_t SplitJoin72_Butterfly_Fiss_3499_3509_split[4];
buffer_float_t SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child1_3491_3527_split[8];
buffer_float_t SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_child0_3371_3515_split[4];
buffer_float_t Pre_CollapsedDataParallel_1_3279WEIGHTED_ROUND_ROBIN_Splitter_3463;
buffer_float_t SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child0_3490_3526_join[8];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_2906_3301_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_Hier_3484_3505_join[2];
buffer_float_t FloatSource_2948Pre_CollapsedDataParallel_1_3249;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3408Post_CollapsedDataParallel_2_3253;
buffer_float_t Pre_CollapsedDataParallel_1_3273WEIGHTED_ROUND_ROBIN_Splitter_3455;
buffer_float_t SplitJoin0_Butterfly_Fiss_3483_3504_split[14];
buffer_float_t SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_Hier_3489_3525_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3468Post_CollapsedDataParallel_2_3283;
buffer_float_t Post_CollapsedDataParallel_2_3253WEIGHTED_ROUND_ROBIN_Splitter_3373;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3464Post_CollapsedDataParallel_2_3280;
buffer_float_t SplitJoin61_Butterfly_Fiss_3497_3523_split[2];
buffer_float_t SplitJoin95_Butterfly_Fiss_3502_3513_join[4];
buffer_float_t SplitJoin65_Butterfly_Fiss_3498_3524_split[2];
buffer_float_t SplitJoin39_Butterfly_Fiss_3492_3517_split[2];
buffer_float_t Pre_CollapsedDataParallel_1_3288WEIGHTED_ROUND_ROBIN_Splitter_3475;
buffer_float_t Pre_CollapsedDataParallel_1_3261WEIGHTED_ROUND_ROBIN_Splitter_3423;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3440Post_CollapsedDataParallel_2_3265;
buffer_float_t Post_CollapsedDataParallel_2_3256WEIGHTED_ROUND_ROBIN_Splitter_3375;
buffer_float_t SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_child0_3342_3507_join[2];
buffer_float_t SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_child0_3342_3507_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3446Post_CollapsedDataParallel_2_3268;
buffer_float_t SplitJoin8_Butterfly_Fiss_3486_3508_split[4];
buffer_float_t SplitJoin8_Butterfly_Fiss_3486_3508_join[4];
buffer_float_t SplitJoin53_Butterfly_Fiss_3495_3521_join[2];
buffer_float_t SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_child1_3372_3520_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3472Post_CollapsedDataParallel_2_3286;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3476Post_CollapsedDataParallel_2_3289;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3389BitReverse_3029;


FloatSource_2948_t FloatSource_2948_s;

void FloatSource(buffer_float_t *chanout) {
		push_float(&(*chanout), FloatSource_2948_s.A_re[FloatSource_2948_s.idx]) ; 
		push_float(&(*chanout), FloatSource_2948_s.A_im[FloatSource_2948_s.idx]) ; 
		FloatSource_2948_s.idx++ ; 
		if((FloatSource_2948_s.idx >= 32)) {
			FloatSource_2948_s.idx = 0 ; 
		}
	}


void FloatSource_2948() {
	FOR(uint32_t, __iter_steady_, 0, <, 224, __iter_steady_++)
		FloatSource(&(FloatSource_2948Pre_CollapsedDataParallel_1_3249));
	ENDFOR
}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
		int partialSum_k = 0;
 {
		FOR(int, _k, 0,  < , 16, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k;
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 32) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_3249() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(FloatSource_2948Pre_CollapsedDataParallel_1_3249), &(Pre_CollapsedDataParallel_1_3249WEIGHTED_ROUND_ROBIN_Splitter_3391));
	ENDFOR
}

void Butterfly(buffer_float_t *chanin, buffer_float_t *chanout) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&(*chanin)) ; 
		u_im = pop_float(&(*chanin)) ; 
		t_re = pop_float(&(*chanin)) ; 
		t_im = pop_float(&(*chanin)) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&(*chanout), u_re) ; 
		push_float(&(*chanout), u_im) ; 
		push_float(&(*chanout), t_re) ; 
		push_float(&(*chanout), t_im) ; 
	}


void Butterfly_3393() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_3483_3504_split[0]), &(SplitJoin0_Butterfly_Fiss_3483_3504_join[0]));
	ENDFOR
}

void Butterfly_3394() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_3483_3504_split[1]), &(SplitJoin0_Butterfly_Fiss_3483_3504_join[1]));
	ENDFOR
}

void Butterfly_3395() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_3483_3504_split[2]), &(SplitJoin0_Butterfly_Fiss_3483_3504_join[2]));
	ENDFOR
}

void Butterfly_3396() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_3483_3504_split[3]), &(SplitJoin0_Butterfly_Fiss_3483_3504_join[3]));
	ENDFOR
}

void Butterfly_3397() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_3483_3504_split[4]), &(SplitJoin0_Butterfly_Fiss_3483_3504_join[4]));
	ENDFOR
}

void Butterfly_3398() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_3483_3504_split[5]), &(SplitJoin0_Butterfly_Fiss_3483_3504_join[5]));
	ENDFOR
}

void Butterfly_3399() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_3483_3504_split[6]), &(SplitJoin0_Butterfly_Fiss_3483_3504_join[6]));
	ENDFOR
}

void Butterfly_3400() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_3483_3504_split[7]), &(SplitJoin0_Butterfly_Fiss_3483_3504_join[7]));
	ENDFOR
}

void Butterfly_3401() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_3483_3504_split[8]), &(SplitJoin0_Butterfly_Fiss_3483_3504_join[8]));
	ENDFOR
}

void Butterfly_3402() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_3483_3504_split[9]), &(SplitJoin0_Butterfly_Fiss_3483_3504_join[9]));
	ENDFOR
}

void Butterfly_3403() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_3483_3504_split[10]), &(SplitJoin0_Butterfly_Fiss_3483_3504_join[10]));
	ENDFOR
}

void Butterfly_3404() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_3483_3504_split[11]), &(SplitJoin0_Butterfly_Fiss_3483_3504_join[11]));
	ENDFOR
}

void Butterfly_3405() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_3483_3504_split[12]), &(SplitJoin0_Butterfly_Fiss_3483_3504_join[12]));
	ENDFOR
}

void Butterfly_3406() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_3483_3504_split[13]), &(SplitJoin0_Butterfly_Fiss_3483_3504_join[13]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3391() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 14, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin0_Butterfly_Fiss_3483_3504_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_3249WEIGHTED_ROUND_ROBIN_Splitter_3391));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3392() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 14, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3392Post_CollapsedDataParallel_2_3250, pop_float(&SplitJoin0_Butterfly_Fiss_3483_3504_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
		int kTimesWeights_i = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 16, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&(*chanout), peek_float(&(*chanin), (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_3250() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_3392Post_CollapsedDataParallel_2_3250), &(Post_CollapsedDataParallel_2_3250WEIGHTED_ROUND_ROBIN_Splitter_3293));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_3252() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_2906_3301_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_Hier_3484_3505_split[0]), &(Pre_CollapsedDataParallel_1_3252WEIGHTED_ROUND_ROBIN_Splitter_3407));
	ENDFOR
}

void Butterfly_3409() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_3485_3506_split[0]), &(SplitJoin4_Butterfly_Fiss_3485_3506_join[0]));
	ENDFOR
}

void Butterfly_3410() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_3485_3506_split[1]), &(SplitJoin4_Butterfly_Fiss_3485_3506_join[1]));
	ENDFOR
}

void Butterfly_3411() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_3485_3506_split[2]), &(SplitJoin4_Butterfly_Fiss_3485_3506_join[2]));
	ENDFOR
}

void Butterfly_3412() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_3485_3506_split[3]), &(SplitJoin4_Butterfly_Fiss_3485_3506_join[3]));
	ENDFOR
}

void Butterfly_3413() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_3485_3506_split[4]), &(SplitJoin4_Butterfly_Fiss_3485_3506_join[4]));
	ENDFOR
}

void Butterfly_3414() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_3485_3506_split[5]), &(SplitJoin4_Butterfly_Fiss_3485_3506_join[5]));
	ENDFOR
}

void Butterfly_3415() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_3485_3506_split[6]), &(SplitJoin4_Butterfly_Fiss_3485_3506_join[6]));
	ENDFOR
}

void Butterfly_3416() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_3485_3506_split[7]), &(SplitJoin4_Butterfly_Fiss_3485_3506_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3407() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin4_Butterfly_Fiss_3485_3506_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_3252WEIGHTED_ROUND_ROBIN_Splitter_3407));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3408() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3408Post_CollapsedDataParallel_2_3253, pop_float(&SplitJoin4_Butterfly_Fiss_3485_3506_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_3253() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_3408Post_CollapsedDataParallel_2_3253), &(Post_CollapsedDataParallel_2_3253WEIGHTED_ROUND_ROBIN_Splitter_3373));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_3258() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_child0_3342_3507_split[0]), &(Pre_CollapsedDataParallel_1_3258WEIGHTED_ROUND_ROBIN_Splitter_3417));
	ENDFOR
}

void Butterfly_3419() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin8_Butterfly_Fiss_3486_3508_split[0]), &(SplitJoin8_Butterfly_Fiss_3486_3508_join[0]));
	ENDFOR
}

void Butterfly_3420() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin8_Butterfly_Fiss_3486_3508_split[1]), &(SplitJoin8_Butterfly_Fiss_3486_3508_join[1]));
	ENDFOR
}

void Butterfly_3421() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin8_Butterfly_Fiss_3486_3508_split[2]), &(SplitJoin8_Butterfly_Fiss_3486_3508_join[2]));
	ENDFOR
}

void Butterfly_3422() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin8_Butterfly_Fiss_3486_3508_split[3]), &(SplitJoin8_Butterfly_Fiss_3486_3508_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3417() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin8_Butterfly_Fiss_3486_3508_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_3258WEIGHTED_ROUND_ROBIN_Splitter_3417));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3418() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3418Post_CollapsedDataParallel_2_3259, pop_float(&SplitJoin8_Butterfly_Fiss_3486_3508_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_3259() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_3418Post_CollapsedDataParallel_2_3259), &(SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_child0_3342_3507_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_3261() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_child0_3342_3507_split[1]), &(Pre_CollapsedDataParallel_1_3261WEIGHTED_ROUND_ROBIN_Splitter_3423));
	ENDFOR
}

void Butterfly_3425() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin72_Butterfly_Fiss_3499_3509_split[0]), &(SplitJoin72_Butterfly_Fiss_3499_3509_join[0]));
	ENDFOR
}

void Butterfly_3426() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin72_Butterfly_Fiss_3499_3509_split[1]), &(SplitJoin72_Butterfly_Fiss_3499_3509_join[1]));
	ENDFOR
}

void Butterfly_3427() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin72_Butterfly_Fiss_3499_3509_split[2]), &(SplitJoin72_Butterfly_Fiss_3499_3509_join[2]));
	ENDFOR
}

void Butterfly_3428() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin72_Butterfly_Fiss_3499_3509_split[3]), &(SplitJoin72_Butterfly_Fiss_3499_3509_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3423() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin72_Butterfly_Fiss_3499_3509_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_3261WEIGHTED_ROUND_ROBIN_Splitter_3423));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3424() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3424Post_CollapsedDataParallel_2_3262, pop_float(&SplitJoin72_Butterfly_Fiss_3499_3509_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_3262() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_3424Post_CollapsedDataParallel_2_3262), &(SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_child0_3342_3507_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3373() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_child0_3342_3507_split[0], pop_float(&Post_CollapsedDataParallel_2_3253WEIGHTED_ROUND_ROBIN_Splitter_3373));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_child0_3342_3507_split[1], pop_float(&Post_CollapsedDataParallel_2_3253WEIGHTED_ROUND_ROBIN_Splitter_3373));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3374() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_2906_3301_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_Hier_3484_3505_join[0], pop_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_child0_3342_3507_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_2906_3301_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_Hier_3484_3505_join[0], pop_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_child0_3342_3507_join[1]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_3255() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_2906_3301_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_Hier_3484_3505_split[1]), &(Pre_CollapsedDataParallel_1_3255WEIGHTED_ROUND_ROBIN_Splitter_3429));
	ENDFOR
}

void Butterfly_3431() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_3500_3510_split[0]), &(SplitJoin85_Butterfly_Fiss_3500_3510_join[0]));
	ENDFOR
}

void Butterfly_3432() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_3500_3510_split[1]), &(SplitJoin85_Butterfly_Fiss_3500_3510_join[1]));
	ENDFOR
}

void Butterfly_3433() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_3500_3510_split[2]), &(SplitJoin85_Butterfly_Fiss_3500_3510_join[2]));
	ENDFOR
}

void Butterfly_3434() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_3500_3510_split[3]), &(SplitJoin85_Butterfly_Fiss_3500_3510_join[3]));
	ENDFOR
}

void Butterfly_3435() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_3500_3510_split[4]), &(SplitJoin85_Butterfly_Fiss_3500_3510_join[4]));
	ENDFOR
}

void Butterfly_3436() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_3500_3510_split[5]), &(SplitJoin85_Butterfly_Fiss_3500_3510_join[5]));
	ENDFOR
}

void Butterfly_3437() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_3500_3510_split[6]), &(SplitJoin85_Butterfly_Fiss_3500_3510_join[6]));
	ENDFOR
}

void Butterfly_3438() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_3500_3510_split[7]), &(SplitJoin85_Butterfly_Fiss_3500_3510_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3429() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin85_Butterfly_Fiss_3500_3510_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_3255WEIGHTED_ROUND_ROBIN_Splitter_3429));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3430() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3430Post_CollapsedDataParallel_2_3256, pop_float(&SplitJoin85_Butterfly_Fiss_3500_3510_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_3256() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_3430Post_CollapsedDataParallel_2_3256), &(Post_CollapsedDataParallel_2_3256WEIGHTED_ROUND_ROBIN_Splitter_3375));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_3264() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_child1_3347_3511_split[0]), &(Pre_CollapsedDataParallel_1_3264WEIGHTED_ROUND_ROBIN_Splitter_3439));
	ENDFOR
}

void Butterfly_3441() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin89_Butterfly_Fiss_3501_3512_split[0]), &(SplitJoin89_Butterfly_Fiss_3501_3512_join[0]));
	ENDFOR
}

void Butterfly_3442() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin89_Butterfly_Fiss_3501_3512_split[1]), &(SplitJoin89_Butterfly_Fiss_3501_3512_join[1]));
	ENDFOR
}

void Butterfly_3443() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin89_Butterfly_Fiss_3501_3512_split[2]), &(SplitJoin89_Butterfly_Fiss_3501_3512_join[2]));
	ENDFOR
}

void Butterfly_3444() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin89_Butterfly_Fiss_3501_3512_split[3]), &(SplitJoin89_Butterfly_Fiss_3501_3512_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3439() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin89_Butterfly_Fiss_3501_3512_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_3264WEIGHTED_ROUND_ROBIN_Splitter_3439));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3440() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3440Post_CollapsedDataParallel_2_3265, pop_float(&SplitJoin89_Butterfly_Fiss_3501_3512_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_3265() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_3440Post_CollapsedDataParallel_2_3265), &(SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_child1_3347_3511_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_3267() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_child1_3347_3511_split[1]), &(Pre_CollapsedDataParallel_1_3267WEIGHTED_ROUND_ROBIN_Splitter_3445));
	ENDFOR
}

void Butterfly_3447() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin95_Butterfly_Fiss_3502_3513_split[0]), &(SplitJoin95_Butterfly_Fiss_3502_3513_join[0]));
	ENDFOR
}

void Butterfly_3448() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin95_Butterfly_Fiss_3502_3513_split[1]), &(SplitJoin95_Butterfly_Fiss_3502_3513_join[1]));
	ENDFOR
}

void Butterfly_3449() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin95_Butterfly_Fiss_3502_3513_split[2]), &(SplitJoin95_Butterfly_Fiss_3502_3513_join[2]));
	ENDFOR
}

void Butterfly_3450() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin95_Butterfly_Fiss_3502_3513_split[3]), &(SplitJoin95_Butterfly_Fiss_3502_3513_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3445() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin95_Butterfly_Fiss_3502_3513_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_3267WEIGHTED_ROUND_ROBIN_Splitter_3445));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3446() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3446Post_CollapsedDataParallel_2_3268, pop_float(&SplitJoin95_Butterfly_Fiss_3502_3513_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_3268() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_3446Post_CollapsedDataParallel_2_3268), &(SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_child1_3347_3511_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3375() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_child1_3347_3511_split[0], pop_float(&Post_CollapsedDataParallel_2_3256WEIGHTED_ROUND_ROBIN_Splitter_3375));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_child1_3347_3511_split[1], pop_float(&Post_CollapsedDataParallel_2_3256WEIGHTED_ROUND_ROBIN_Splitter_3375));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3376() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_2906_3301_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_Hier_3484_3505_join[1], pop_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_child1_3347_3511_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_2906_3301_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_Hier_3484_3505_join[1], pop_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_child1_3347_3511_join[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_3293() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_2906_3301_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_Hier_3484_3505_split[0], pop_float(&Post_CollapsedDataParallel_2_3250WEIGHTED_ROUND_ROBIN_Splitter_3293));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_2906_3301_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_Hier_3484_3505_split[1], pop_float(&Post_CollapsedDataParallel_2_3250WEIGHTED_ROUND_ROBIN_Splitter_3293));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3377() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3377WEIGHTED_ROUND_ROBIN_Splitter_3378, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_2906_3301_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_Hier_3484_3505_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3377WEIGHTED_ROUND_ROBIN_Splitter_3378, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_2906_3301_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_Hier_3484_3505_join[1]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_3270() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_child0_3371_3515_split[0]), &(Pre_CollapsedDataParallel_1_3270WEIGHTED_ROUND_ROBIN_Splitter_3451));
	ENDFOR
}

void Butterfly_3453() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin14_Butterfly_Fiss_3488_3516_split[0]), &(SplitJoin14_Butterfly_Fiss_3488_3516_join[0]));
	ENDFOR
}

void Butterfly_3454() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin14_Butterfly_Fiss_3488_3516_split[1]), &(SplitJoin14_Butterfly_Fiss_3488_3516_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3451() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin14_Butterfly_Fiss_3488_3516_split[0], pop_float(&Pre_CollapsedDataParallel_1_3270WEIGHTED_ROUND_ROBIN_Splitter_3451));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin14_Butterfly_Fiss_3488_3516_split[1], pop_float(&Pre_CollapsedDataParallel_1_3270WEIGHTED_ROUND_ROBIN_Splitter_3451));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3452() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3452Post_CollapsedDataParallel_2_3271, pop_float(&SplitJoin14_Butterfly_Fiss_3488_3516_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3452Post_CollapsedDataParallel_2_3271, pop_float(&SplitJoin14_Butterfly_Fiss_3488_3516_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_3271() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_3452Post_CollapsedDataParallel_2_3271), &(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_child0_3371_3515_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_3273() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_child0_3371_3515_split[1]), &(Pre_CollapsedDataParallel_1_3273WEIGHTED_ROUND_ROBIN_Splitter_3455));
	ENDFOR
}

void Butterfly_3457() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin39_Butterfly_Fiss_3492_3517_split[0]), &(SplitJoin39_Butterfly_Fiss_3492_3517_join[0]));
	ENDFOR
}

void Butterfly_3458() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin39_Butterfly_Fiss_3492_3517_split[1]), &(SplitJoin39_Butterfly_Fiss_3492_3517_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3455() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin39_Butterfly_Fiss_3492_3517_split[0], pop_float(&Pre_CollapsedDataParallel_1_3273WEIGHTED_ROUND_ROBIN_Splitter_3455));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin39_Butterfly_Fiss_3492_3517_split[1], pop_float(&Pre_CollapsedDataParallel_1_3273WEIGHTED_ROUND_ROBIN_Splitter_3455));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3456() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3456Post_CollapsedDataParallel_2_3274, pop_float(&SplitJoin39_Butterfly_Fiss_3492_3517_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3456Post_CollapsedDataParallel_2_3274, pop_float(&SplitJoin39_Butterfly_Fiss_3492_3517_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_3274() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_3456Post_CollapsedDataParallel_2_3274), &(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_child0_3371_3515_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_3276() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_child0_3371_3515_split[2]), &(Pre_CollapsedDataParallel_1_3276WEIGHTED_ROUND_ROBIN_Splitter_3459));
	ENDFOR
}

void Butterfly_3461() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin43_Butterfly_Fiss_3493_3518_split[0]), &(SplitJoin43_Butterfly_Fiss_3493_3518_join[0]));
	ENDFOR
}

void Butterfly_3462() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin43_Butterfly_Fiss_3493_3518_split[1]), &(SplitJoin43_Butterfly_Fiss_3493_3518_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3459() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin43_Butterfly_Fiss_3493_3518_split[0], pop_float(&Pre_CollapsedDataParallel_1_3276WEIGHTED_ROUND_ROBIN_Splitter_3459));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin43_Butterfly_Fiss_3493_3518_split[1], pop_float(&Pre_CollapsedDataParallel_1_3276WEIGHTED_ROUND_ROBIN_Splitter_3459));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3460() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3460Post_CollapsedDataParallel_2_3277, pop_float(&SplitJoin43_Butterfly_Fiss_3493_3518_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3460Post_CollapsedDataParallel_2_3277, pop_float(&SplitJoin43_Butterfly_Fiss_3493_3518_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_3277() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_3460Post_CollapsedDataParallel_2_3277), &(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_child0_3371_3515_join[2]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_3279() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_child0_3371_3515_split[3]), &(Pre_CollapsedDataParallel_1_3279WEIGHTED_ROUND_ROBIN_Splitter_3463));
	ENDFOR
}

void Butterfly_3465() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin47_Butterfly_Fiss_3494_3519_split[0]), &(SplitJoin47_Butterfly_Fiss_3494_3519_join[0]));
	ENDFOR
}

void Butterfly_3466() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin47_Butterfly_Fiss_3494_3519_split[1]), &(SplitJoin47_Butterfly_Fiss_3494_3519_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3463() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin47_Butterfly_Fiss_3494_3519_split[0], pop_float(&Pre_CollapsedDataParallel_1_3279WEIGHTED_ROUND_ROBIN_Splitter_3463));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin47_Butterfly_Fiss_3494_3519_split[1], pop_float(&Pre_CollapsedDataParallel_1_3279WEIGHTED_ROUND_ROBIN_Splitter_3463));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3464() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3464Post_CollapsedDataParallel_2_3280, pop_float(&SplitJoin47_Butterfly_Fiss_3494_3519_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3464Post_CollapsedDataParallel_2_3280, pop_float(&SplitJoin47_Butterfly_Fiss_3494_3519_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_3280() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_3464Post_CollapsedDataParallel_2_3280), &(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_child0_3371_3515_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3379() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_child0_3371_3515_split[__iter_dec_], pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_Hier_3487_3514_split[0]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3380() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_Hier_3487_3514_join[0], pop_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_child0_3371_3515_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_3282() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_child1_3372_3520_split[0]), &(Pre_CollapsedDataParallel_1_3282WEIGHTED_ROUND_ROBIN_Splitter_3467));
	ENDFOR
}

void Butterfly_3469() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin53_Butterfly_Fiss_3495_3521_split[0]), &(SplitJoin53_Butterfly_Fiss_3495_3521_join[0]));
	ENDFOR
}

void Butterfly_3470() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin53_Butterfly_Fiss_3495_3521_split[1]), &(SplitJoin53_Butterfly_Fiss_3495_3521_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3467() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin53_Butterfly_Fiss_3495_3521_split[0], pop_float(&Pre_CollapsedDataParallel_1_3282WEIGHTED_ROUND_ROBIN_Splitter_3467));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin53_Butterfly_Fiss_3495_3521_split[1], pop_float(&Pre_CollapsedDataParallel_1_3282WEIGHTED_ROUND_ROBIN_Splitter_3467));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3468() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3468Post_CollapsedDataParallel_2_3283, pop_float(&SplitJoin53_Butterfly_Fiss_3495_3521_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3468Post_CollapsedDataParallel_2_3283, pop_float(&SplitJoin53_Butterfly_Fiss_3495_3521_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_3283() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_3468Post_CollapsedDataParallel_2_3283), &(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_child1_3372_3520_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_3285() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_child1_3372_3520_split[1]), &(Pre_CollapsedDataParallel_1_3285WEIGHTED_ROUND_ROBIN_Splitter_3471));
	ENDFOR
}

void Butterfly_3473() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin57_Butterfly_Fiss_3496_3522_split[0]), &(SplitJoin57_Butterfly_Fiss_3496_3522_join[0]));
	ENDFOR
}

void Butterfly_3474() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin57_Butterfly_Fiss_3496_3522_split[1]), &(SplitJoin57_Butterfly_Fiss_3496_3522_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3471() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin57_Butterfly_Fiss_3496_3522_split[0], pop_float(&Pre_CollapsedDataParallel_1_3285WEIGHTED_ROUND_ROBIN_Splitter_3471));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin57_Butterfly_Fiss_3496_3522_split[1], pop_float(&Pre_CollapsedDataParallel_1_3285WEIGHTED_ROUND_ROBIN_Splitter_3471));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3472() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3472Post_CollapsedDataParallel_2_3286, pop_float(&SplitJoin57_Butterfly_Fiss_3496_3522_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3472Post_CollapsedDataParallel_2_3286, pop_float(&SplitJoin57_Butterfly_Fiss_3496_3522_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_3286() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_3472Post_CollapsedDataParallel_2_3286), &(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_child1_3372_3520_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_3288() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_child1_3372_3520_split[2]), &(Pre_CollapsedDataParallel_1_3288WEIGHTED_ROUND_ROBIN_Splitter_3475));
	ENDFOR
}

void Butterfly_3477() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin61_Butterfly_Fiss_3497_3523_split[0]), &(SplitJoin61_Butterfly_Fiss_3497_3523_join[0]));
	ENDFOR
}

void Butterfly_3478() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin61_Butterfly_Fiss_3497_3523_split[1]), &(SplitJoin61_Butterfly_Fiss_3497_3523_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3475() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin61_Butterfly_Fiss_3497_3523_split[0], pop_float(&Pre_CollapsedDataParallel_1_3288WEIGHTED_ROUND_ROBIN_Splitter_3475));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin61_Butterfly_Fiss_3497_3523_split[1], pop_float(&Pre_CollapsedDataParallel_1_3288WEIGHTED_ROUND_ROBIN_Splitter_3475));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3476() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3476Post_CollapsedDataParallel_2_3289, pop_float(&SplitJoin61_Butterfly_Fiss_3497_3523_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3476Post_CollapsedDataParallel_2_3289, pop_float(&SplitJoin61_Butterfly_Fiss_3497_3523_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_3289() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_3476Post_CollapsedDataParallel_2_3289), &(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_child1_3372_3520_join[2]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_3291() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_child1_3372_3520_split[3]), &(Pre_CollapsedDataParallel_1_3291WEIGHTED_ROUND_ROBIN_Splitter_3479));
	ENDFOR
}

void Butterfly_3481() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin65_Butterfly_Fiss_3498_3524_split[0]), &(SplitJoin65_Butterfly_Fiss_3498_3524_join[0]));
	ENDFOR
}

void Butterfly_3482() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin65_Butterfly_Fiss_3498_3524_split[1]), &(SplitJoin65_Butterfly_Fiss_3498_3524_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3479() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin65_Butterfly_Fiss_3498_3524_split[0], pop_float(&Pre_CollapsedDataParallel_1_3291WEIGHTED_ROUND_ROBIN_Splitter_3479));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin65_Butterfly_Fiss_3498_3524_split[1], pop_float(&Pre_CollapsedDataParallel_1_3291WEIGHTED_ROUND_ROBIN_Splitter_3479));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3480() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3480Post_CollapsedDataParallel_2_3292, pop_float(&SplitJoin65_Butterfly_Fiss_3498_3524_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3480Post_CollapsedDataParallel_2_3292, pop_float(&SplitJoin65_Butterfly_Fiss_3498_3524_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_3292() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_3480Post_CollapsedDataParallel_2_3292), &(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_child1_3372_3520_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3381() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_child1_3372_3520_split[__iter_dec_], pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_Hier_3487_3514_split[1]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3382() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_Hier_3487_3514_join[1], pop_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_child1_3372_3520_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_3378() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_Hier_3487_3514_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3377WEIGHTED_ROUND_ROBIN_Splitter_3378));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_Hier_3487_3514_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3377WEIGHTED_ROUND_ROBIN_Splitter_3378));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3383() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3383WEIGHTED_ROUND_ROBIN_Splitter_3384, pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_Hier_3487_3514_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3383WEIGHTED_ROUND_ROBIN_Splitter_3384, pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_Hier_3487_3514_join[1]));
		ENDFOR
	ENDFOR
}}

void Butterfly_3013() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child0_3490_3526_split[0]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child0_3490_3526_join[0]));
	ENDFOR
}

void Butterfly_3014() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child0_3490_3526_split[1]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child0_3490_3526_join[1]));
	ENDFOR
}

void Butterfly_3015() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child0_3490_3526_split[2]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child0_3490_3526_join[2]));
	ENDFOR
}

void Butterfly_3016() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child0_3490_3526_split[3]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child0_3490_3526_join[3]));
	ENDFOR
}

void Butterfly_3017() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child0_3490_3526_split[4]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child0_3490_3526_join[4]));
	ENDFOR
}

void Butterfly_3018() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child0_3490_3526_split[5]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child0_3490_3526_join[5]));
	ENDFOR
}

void Butterfly_3019() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child0_3490_3526_split[6]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child0_3490_3526_join[6]));
	ENDFOR
}

void Butterfly_3020() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child0_3490_3526_split[7]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child0_3490_3526_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3385() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child0_3490_3526_split[__iter_dec_], pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_Hier_3489_3525_split[0]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3386() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_Hier_3489_3525_join[0], pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child0_3490_3526_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Butterfly_3021() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child1_3491_3527_split[0]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child1_3491_3527_join[0]));
	ENDFOR
}

void Butterfly_3022() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child1_3491_3527_split[1]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child1_3491_3527_join[1]));
	ENDFOR
}

void Butterfly_3023() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child1_3491_3527_split[2]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child1_3491_3527_join[2]));
	ENDFOR
}

void Butterfly_3024() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child1_3491_3527_split[3]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child1_3491_3527_join[3]));
	ENDFOR
}

void Butterfly_3025() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child1_3491_3527_split[4]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child1_3491_3527_join[4]));
	ENDFOR
}

void Butterfly_3026() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child1_3491_3527_split[5]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child1_3491_3527_join[5]));
	ENDFOR
}

void Butterfly_3027() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child1_3491_3527_split[6]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child1_3491_3527_join[6]));
	ENDFOR
}

void Butterfly_3028() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child1_3491_3527_split[7]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child1_3491_3527_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3387() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child1_3491_3527_split[__iter_dec_], pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_Hier_3489_3525_split[1]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3388() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_Hier_3489_3525_join[1], pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child1_3491_3527_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_3384() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_Hier_3489_3525_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3383WEIGHTED_ROUND_ROBIN_Splitter_3384));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_Hier_3489_3525_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3383WEIGHTED_ROUND_ROBIN_Splitter_3384));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3389() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3389BitReverse_3029, pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_Hier_3489_3525_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3389BitReverse_3029, pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_Hier_3489_3525_join[1]));
		ENDFOR
	ENDFOR
}}

int BitReverse_3029_bitrev(int inp, int numbits) {
	int rev = 0;
	FOR(int, i, 0,  < , numbits, i++) {
		rev = ((rev * 2) | (inp & 1)) ; 
		inp = (inp / 2) ; 
	}
	ENDFOR
	return rev;
}
void BitReverse(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, i__conflict__0, 0,  < , 32, i__conflict__0++) {
			int br = 0;
			br = BitReverse_3029_bitrev(i__conflict__0, 5) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (2 * br))) ; 
			push_float(&(*chanout), peek_float(&(*chanin), ((2 * br) + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&(*chanin)) ; 
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void BitReverse_3029() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		BitReverse(&(WEIGHTED_ROUND_ROBIN_Joiner_3389BitReverse_3029), &(BitReverse_3029FloatPrinter_3030));
	ENDFOR
}

void FloatPrinter(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void FloatPrinter_3030() {
	FOR(uint32_t, __iter_steady_, 0, <, 224, __iter_steady_++)
		FloatPrinter(&(BitReverse_3029FloatPrinter_3030));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3377WEIGHTED_ROUND_ROBIN_Splitter_3378);
	FOR(int, __iter_init_0_, 0, <, 4, __iter_init_0_++)
		init_buffer_float(&SplitJoin72_Butterfly_Fiss_3499_3509_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 2, __iter_init_1_++)
		init_buffer_float(&SplitJoin53_Butterfly_Fiss_3495_3521_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_float(&SplitJoin4_Butterfly_Fiss_3485_3506_join[__iter_init_2_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_3270WEIGHTED_ROUND_ROBIN_Splitter_3451);
	FOR(int, __iter_init_3_, 0, <, 4, __iter_init_3_++)
		init_buffer_float(&SplitJoin95_Butterfly_Fiss_3502_3513_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 2, __iter_init_4_++)
		init_buffer_float(&SplitJoin61_Butterfly_Fiss_3497_3523_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin4_Butterfly_Fiss_3485_3506_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 2, __iter_init_6_++)
		init_buffer_float(&SplitJoin43_Butterfly_Fiss_3493_3518_split[__iter_init_6_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_3250WEIGHTED_ROUND_ROBIN_Splitter_3293);
	FOR(int, __iter_init_7_, 0, <, 2, __iter_init_7_++)
		init_buffer_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_Hier_3489_3525_join[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_3249WEIGHTED_ROUND_ROBIN_Splitter_3391);
	FOR(int, __iter_init_8_, 0, <, 14, __iter_init_8_++)
		init_buffer_float(&SplitJoin0_Butterfly_Fiss_3483_3504_join[__iter_init_8_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_3276WEIGHTED_ROUND_ROBIN_Splitter_3459);
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child1_3491_3527_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_float(&SplitJoin85_Butterfly_Fiss_3500_3510_split[__iter_init_10_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_3252WEIGHTED_ROUND_ROBIN_Splitter_3407);
	FOR(int, __iter_init_11_, 0, <, 2, __iter_init_11_++)
		init_buffer_float(&SplitJoin57_Butterfly_Fiss_3496_3522_join[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3383WEIGHTED_ROUND_ROBIN_Splitter_3384);
	init_buffer_float(&Pre_CollapsedDataParallel_1_3291WEIGHTED_ROUND_ROBIN_Splitter_3479);
	init_buffer_float(&Pre_CollapsedDataParallel_1_3258WEIGHTED_ROUND_ROBIN_Splitter_3417);
	init_buffer_float(&BitReverse_3029FloatPrinter_3030);
	init_buffer_float(&Pre_CollapsedDataParallel_1_3285WEIGHTED_ROUND_ROBIN_Splitter_3471);
	FOR(int, __iter_init_12_, 0, <, 2, __iter_init_12_++)
		init_buffer_float(&SplitJoin14_Butterfly_Fiss_3488_3516_split[__iter_init_12_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3452Post_CollapsedDataParallel_2_3271);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3392Post_CollapsedDataParallel_2_3250);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3456Post_CollapsedDataParallel_2_3274);
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_float(&SplitJoin65_Butterfly_Fiss_3498_3524_join[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_Hier_3487_3514_split[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 2, __iter_init_15_++)
		init_buffer_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_Hier_3487_3514_join[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_2906_3301_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_Hier_3484_3505_split[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_float(&SplitJoin14_Butterfly_Fiss_3488_3516_join[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 2, __iter_init_18_++)
		init_buffer_float(&SplitJoin43_Butterfly_Fiss_3493_3518_join[__iter_init_18_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3460Post_CollapsedDataParallel_2_3277);
	FOR(int, __iter_init_19_, 0, <, 2, __iter_init_19_++)
		init_buffer_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_child1_3347_3511_join[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 4, __iter_init_20_++)
		init_buffer_float(&SplitJoin89_Butterfly_Fiss_3501_3512_join[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 4, __iter_init_21_++)
		init_buffer_float(&SplitJoin89_Butterfly_Fiss_3501_3512_split[__iter_init_21_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_3255WEIGHTED_ROUND_ROBIN_Splitter_3429);
	init_buffer_float(&Pre_CollapsedDataParallel_1_3267WEIGHTED_ROUND_ROBIN_Splitter_3445);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3480Post_CollapsedDataParallel_2_3292);
	FOR(int, __iter_init_22_, 0, <, 4, __iter_init_22_++)
		init_buffer_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_child1_3372_3520_join[__iter_init_22_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3424Post_CollapsedDataParallel_2_3262);
	FOR(int, __iter_init_23_, 0, <, 8, __iter_init_23_++)
		init_buffer_float(&SplitJoin85_Butterfly_Fiss_3500_3510_join[__iter_init_23_]);
	ENDFOR
	FOR(int, __iter_init_24_, 0, <, 2, __iter_init_24_++)
		init_buffer_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_child1_3347_3511_split[__iter_init_24_]);
	ENDFOR
	FOR(int, __iter_init_25_, 0, <, 2, __iter_init_25_++)
		init_buffer_float(&SplitJoin39_Butterfly_Fiss_3492_3517_join[__iter_init_25_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_3282WEIGHTED_ROUND_ROBIN_Splitter_3467);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3430Post_CollapsedDataParallel_2_3256);
	FOR(int, __iter_init_26_, 0, <, 2, __iter_init_26_++)
		init_buffer_float(&SplitJoin47_Butterfly_Fiss_3494_3519_split[__iter_init_26_]);
	ENDFOR
	FOR(int, __iter_init_27_, 0, <, 8, __iter_init_27_++)
		init_buffer_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child0_3490_3526_split[__iter_init_27_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_3264WEIGHTED_ROUND_ROBIN_Splitter_3439);
	FOR(int, __iter_init_28_, 0, <, 4, __iter_init_28_++)
		init_buffer_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_child0_3371_3515_join[__iter_init_28_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3418Post_CollapsedDataParallel_2_3259);
	FOR(int, __iter_init_29_, 0, <, 2, __iter_init_29_++)
		init_buffer_float(&SplitJoin57_Butterfly_Fiss_3496_3522_split[__iter_init_29_]);
	ENDFOR
	FOR(int, __iter_init_30_, 0, <, 2, __iter_init_30_++)
		init_buffer_float(&SplitJoin47_Butterfly_Fiss_3494_3519_join[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 4, __iter_init_31_++)
		init_buffer_float(&SplitJoin72_Butterfly_Fiss_3499_3509_split[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 8, __iter_init_32_++)
		init_buffer_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child1_3491_3527_split[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 4, __iter_init_33_++)
		init_buffer_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_child0_3371_3515_split[__iter_init_33_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_3279WEIGHTED_ROUND_ROBIN_Splitter_3463);
	FOR(int, __iter_init_34_, 0, <, 8, __iter_init_34_++)
		init_buffer_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_child0_3490_3526_join[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 2, __iter_init_35_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_2906_3301_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_Hier_3484_3505_join[__iter_init_35_]);
	ENDFOR
	init_buffer_float(&FloatSource_2948Pre_CollapsedDataParallel_1_3249);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3408Post_CollapsedDataParallel_2_3253);
	init_buffer_float(&Pre_CollapsedDataParallel_1_3273WEIGHTED_ROUND_ROBIN_Splitter_3455);
	FOR(int, __iter_init_36_, 0, <, 14, __iter_init_36_++)
		init_buffer_float(&SplitJoin0_Butterfly_Fiss_3483_3504_split[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 2, __iter_init_37_++)
		init_buffer_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_2946_3307_Hier_Hier_3489_3525_split[__iter_init_37_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3468Post_CollapsedDataParallel_2_3283);
	init_buffer_float(&Post_CollapsedDataParallel_2_3253WEIGHTED_ROUND_ROBIN_Splitter_3373);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3464Post_CollapsedDataParallel_2_3280);
	FOR(int, __iter_init_38_, 0, <, 2, __iter_init_38_++)
		init_buffer_float(&SplitJoin61_Butterfly_Fiss_3497_3523_split[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 4, __iter_init_39_++)
		init_buffer_float(&SplitJoin95_Butterfly_Fiss_3502_3513_join[__iter_init_39_]);
	ENDFOR
	FOR(int, __iter_init_40_, 0, <, 2, __iter_init_40_++)
		init_buffer_float(&SplitJoin65_Butterfly_Fiss_3498_3524_split[__iter_init_40_]);
	ENDFOR
	FOR(int, __iter_init_41_, 0, <, 2, __iter_init_41_++)
		init_buffer_float(&SplitJoin39_Butterfly_Fiss_3492_3517_split[__iter_init_41_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_3288WEIGHTED_ROUND_ROBIN_Splitter_3475);
	init_buffer_float(&Pre_CollapsedDataParallel_1_3261WEIGHTED_ROUND_ROBIN_Splitter_3423);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3440Post_CollapsedDataParallel_2_3265);
	init_buffer_float(&Post_CollapsedDataParallel_2_3256WEIGHTED_ROUND_ROBIN_Splitter_3375);
	FOR(int, __iter_init_42_, 0, <, 2, __iter_init_42_++)
		init_buffer_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_child0_3342_3507_join[__iter_init_42_]);
	ENDFOR
	FOR(int, __iter_init_43_, 0, <, 2, __iter_init_43_++)
		init_buffer_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_2915_3303_child0_3342_3507_split[__iter_init_43_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3446Post_CollapsedDataParallel_2_3268);
	FOR(int, __iter_init_44_, 0, <, 4, __iter_init_44_++)
		init_buffer_float(&SplitJoin8_Butterfly_Fiss_3486_3508_split[__iter_init_44_]);
	ENDFOR
	FOR(int, __iter_init_45_, 0, <, 4, __iter_init_45_++)
		init_buffer_float(&SplitJoin8_Butterfly_Fiss_3486_3508_join[__iter_init_45_]);
	ENDFOR
	FOR(int, __iter_init_46_, 0, <, 2, __iter_init_46_++)
		init_buffer_float(&SplitJoin53_Butterfly_Fiss_3495_3521_join[__iter_init_46_]);
	ENDFOR
	FOR(int, __iter_init_47_, 0, <, 4, __iter_init_47_++)
		init_buffer_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_2928_3305_Hier_child1_3372_3520_split[__iter_init_47_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3472Post_CollapsedDataParallel_2_3286);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3476Post_CollapsedDataParallel_2_3289);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3389BitReverse_3029);
// --- init: FloatSource_2948
	 {
	FOR(int, i, 0,  < , 32, i++) {
		FloatSource_2948_s.A_re[i] = 0.0 ; 
		FloatSource_2948_s.A_im[i] = 0.0 ; 
	}
	ENDFOR
	FloatSource_2948_s.A_re[1] = 1.0 ; 
	FloatSource_2948_s.idx = 0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FloatSource_2948();
		Pre_CollapsedDataParallel_1_3249();
		WEIGHTED_ROUND_ROBIN_Splitter_3391();
			Butterfly_3393();
			Butterfly_3394();
			Butterfly_3395();
			Butterfly_3396();
			Butterfly_3397();
			Butterfly_3398();
			Butterfly_3399();
			Butterfly_3400();
			Butterfly_3401();
			Butterfly_3402();
			Butterfly_3403();
			Butterfly_3404();
			Butterfly_3405();
			Butterfly_3406();
		WEIGHTED_ROUND_ROBIN_Joiner_3392();
		Post_CollapsedDataParallel_2_3250();
		WEIGHTED_ROUND_ROBIN_Splitter_3293();
			Pre_CollapsedDataParallel_1_3252();
			WEIGHTED_ROUND_ROBIN_Splitter_3407();
				Butterfly_3409();
				Butterfly_3410();
				Butterfly_3411();
				Butterfly_3412();
				Butterfly_3413();
				Butterfly_3414();
				Butterfly_3415();
				Butterfly_3416();
			WEIGHTED_ROUND_ROBIN_Joiner_3408();
			Post_CollapsedDataParallel_2_3253();
			WEIGHTED_ROUND_ROBIN_Splitter_3373();
				Pre_CollapsedDataParallel_1_3258();
				WEIGHTED_ROUND_ROBIN_Splitter_3417();
					Butterfly_3419();
					Butterfly_3420();
					Butterfly_3421();
					Butterfly_3422();
				WEIGHTED_ROUND_ROBIN_Joiner_3418();
				Post_CollapsedDataParallel_2_3259();
				Pre_CollapsedDataParallel_1_3261();
				WEIGHTED_ROUND_ROBIN_Splitter_3423();
					Butterfly_3425();
					Butterfly_3426();
					Butterfly_3427();
					Butterfly_3428();
				WEIGHTED_ROUND_ROBIN_Joiner_3424();
				Post_CollapsedDataParallel_2_3262();
			WEIGHTED_ROUND_ROBIN_Joiner_3374();
			Pre_CollapsedDataParallel_1_3255();
			WEIGHTED_ROUND_ROBIN_Splitter_3429();
				Butterfly_3431();
				Butterfly_3432();
				Butterfly_3433();
				Butterfly_3434();
				Butterfly_3435();
				Butterfly_3436();
				Butterfly_3437();
				Butterfly_3438();
			WEIGHTED_ROUND_ROBIN_Joiner_3430();
			Post_CollapsedDataParallel_2_3256();
			WEIGHTED_ROUND_ROBIN_Splitter_3375();
				Pre_CollapsedDataParallel_1_3264();
				WEIGHTED_ROUND_ROBIN_Splitter_3439();
					Butterfly_3441();
					Butterfly_3442();
					Butterfly_3443();
					Butterfly_3444();
				WEIGHTED_ROUND_ROBIN_Joiner_3440();
				Post_CollapsedDataParallel_2_3265();
				Pre_CollapsedDataParallel_1_3267();
				WEIGHTED_ROUND_ROBIN_Splitter_3445();
					Butterfly_3447();
					Butterfly_3448();
					Butterfly_3449();
					Butterfly_3450();
				WEIGHTED_ROUND_ROBIN_Joiner_3446();
				Post_CollapsedDataParallel_2_3268();
			WEIGHTED_ROUND_ROBIN_Joiner_3376();
		WEIGHTED_ROUND_ROBIN_Joiner_3377();
		WEIGHTED_ROUND_ROBIN_Splitter_3378();
			WEIGHTED_ROUND_ROBIN_Splitter_3379();
				Pre_CollapsedDataParallel_1_3270();
				WEIGHTED_ROUND_ROBIN_Splitter_3451();
					Butterfly_3453();
					Butterfly_3454();
				WEIGHTED_ROUND_ROBIN_Joiner_3452();
				Post_CollapsedDataParallel_2_3271();
				Pre_CollapsedDataParallel_1_3273();
				WEIGHTED_ROUND_ROBIN_Splitter_3455();
					Butterfly_3457();
					Butterfly_3458();
				WEIGHTED_ROUND_ROBIN_Joiner_3456();
				Post_CollapsedDataParallel_2_3274();
				Pre_CollapsedDataParallel_1_3276();
				WEIGHTED_ROUND_ROBIN_Splitter_3459();
					Butterfly_3461();
					Butterfly_3462();
				WEIGHTED_ROUND_ROBIN_Joiner_3460();
				Post_CollapsedDataParallel_2_3277();
				Pre_CollapsedDataParallel_1_3279();
				WEIGHTED_ROUND_ROBIN_Splitter_3463();
					Butterfly_3465();
					Butterfly_3466();
				WEIGHTED_ROUND_ROBIN_Joiner_3464();
				Post_CollapsedDataParallel_2_3280();
			WEIGHTED_ROUND_ROBIN_Joiner_3380();
			WEIGHTED_ROUND_ROBIN_Splitter_3381();
				Pre_CollapsedDataParallel_1_3282();
				WEIGHTED_ROUND_ROBIN_Splitter_3467();
					Butterfly_3469();
					Butterfly_3470();
				WEIGHTED_ROUND_ROBIN_Joiner_3468();
				Post_CollapsedDataParallel_2_3283();
				Pre_CollapsedDataParallel_1_3285();
				WEIGHTED_ROUND_ROBIN_Splitter_3471();
					Butterfly_3473();
					Butterfly_3474();
				WEIGHTED_ROUND_ROBIN_Joiner_3472();
				Post_CollapsedDataParallel_2_3286();
				Pre_CollapsedDataParallel_1_3288();
				WEIGHTED_ROUND_ROBIN_Splitter_3475();
					Butterfly_3477();
					Butterfly_3478();
				WEIGHTED_ROUND_ROBIN_Joiner_3476();
				Post_CollapsedDataParallel_2_3289();
				Pre_CollapsedDataParallel_1_3291();
				WEIGHTED_ROUND_ROBIN_Splitter_3479();
					Butterfly_3481();
					Butterfly_3482();
				WEIGHTED_ROUND_ROBIN_Joiner_3480();
				Post_CollapsedDataParallel_2_3292();
			WEIGHTED_ROUND_ROBIN_Joiner_3382();
		WEIGHTED_ROUND_ROBIN_Joiner_3383();
		WEIGHTED_ROUND_ROBIN_Splitter_3384();
			WEIGHTED_ROUND_ROBIN_Splitter_3385();
				Butterfly_3013();
				Butterfly_3014();
				Butterfly_3015();
				Butterfly_3016();
				Butterfly_3017();
				Butterfly_3018();
				Butterfly_3019();
				Butterfly_3020();
			WEIGHTED_ROUND_ROBIN_Joiner_3386();
			WEIGHTED_ROUND_ROBIN_Splitter_3387();
				Butterfly_3021();
				Butterfly_3022();
				Butterfly_3023();
				Butterfly_3024();
				Butterfly_3025();
				Butterfly_3026();
				Butterfly_3027();
				Butterfly_3028();
			WEIGHTED_ROUND_ROBIN_Joiner_3388();
		WEIGHTED_ROUND_ROBIN_Joiner_3389();
		BitReverse_3029();
		FloatPrinter_3030();
	ENDFOR
	return EXIT_SUCCESS;
}
