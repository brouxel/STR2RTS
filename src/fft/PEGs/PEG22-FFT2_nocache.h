#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=5632 on the compile command line
#else
#if BUF_SIZEMAX < 5632
#error BUF_SIZEMAX too small, it must be at least 5632
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float w[2];
} CombineDFT_5057_t;

typedef struct {
	float w[4];
} CombineDFT_5081_t;

typedef struct {
	float w[8];
} CombineDFT_5099_t;

typedef struct {
	float w[16];
} CombineDFT_5109_t;

typedef struct {
	float w[32];
} CombineDFT_5115_t;

typedef struct {
	float w[64];
} CombineDFT_4992_t;
void WEIGHTED_ROUND_ROBIN_Splitter_5013();
void FFTTestSource_5015();
void FFTTestSource_5016();
void WEIGHTED_ROUND_ROBIN_Joiner_5014();
void WEIGHTED_ROUND_ROBIN_Splitter_5005();
void FFTReorderSimple_4982();
void WEIGHTED_ROUND_ROBIN_Splitter_5017();
void FFTReorderSimple_5019();
void FFTReorderSimple_5020();
void WEIGHTED_ROUND_ROBIN_Joiner_5018();
void WEIGHTED_ROUND_ROBIN_Splitter_5021();
void FFTReorderSimple_5023();
void FFTReorderSimple_5024();
void FFTReorderSimple_5025();
void FFTReorderSimple_5026();
void WEIGHTED_ROUND_ROBIN_Joiner_5022();
void WEIGHTED_ROUND_ROBIN_Splitter_5027();
void FFTReorderSimple_5029();
void FFTReorderSimple_5030();
void FFTReorderSimple_5031();
void FFTReorderSimple_5032();
void FFTReorderSimple_5033();
void FFTReorderSimple_5034();
void FFTReorderSimple_5035();
void FFTReorderSimple_5036();
void WEIGHTED_ROUND_ROBIN_Joiner_5028();
void WEIGHTED_ROUND_ROBIN_Splitter_5037();
void FFTReorderSimple_5039();
void FFTReorderSimple_5040();
void FFTReorderSimple_5041();
void FFTReorderSimple_5042();
void FFTReorderSimple_5043();
void FFTReorderSimple_5044();
void FFTReorderSimple_5045();
void FFTReorderSimple_5046();
void FFTReorderSimple_5047();
void FFTReorderSimple_5048();
void FFTReorderSimple_5049();
void FFTReorderSimple_5050();
void FFTReorderSimple_5051();
void FFTReorderSimple_5052();
void FFTReorderSimple_5053();
void FFTReorderSimple_5054();
void WEIGHTED_ROUND_ROBIN_Joiner_5038();
void WEIGHTED_ROUND_ROBIN_Splitter_5055();
void CombineDFT_5057();
void CombineDFT_5058();
void CombineDFT_5059();
void CombineDFT_5060();
void CombineDFT_5061();
void CombineDFT_5062();
void CombineDFT_5063();
void CombineDFT_5064();
void CombineDFT_5065();
void CombineDFT_5066();
void CombineDFT_5067();
void CombineDFT_5068();
void CombineDFT_5069();
void CombineDFT_5070();
void CombineDFT_5071();
void CombineDFT_5072();
void CombineDFT_5073();
void CombineDFT_5074();
void CombineDFT_5075();
void CombineDFT_5076();
void CombineDFT_5077();
void CombineDFT_5078();
void WEIGHTED_ROUND_ROBIN_Joiner_5056();
void WEIGHTED_ROUND_ROBIN_Splitter_5079();
void CombineDFT_5081();
void CombineDFT_5082();
void CombineDFT_5083();
void CombineDFT_5084();
void CombineDFT_5085();
void CombineDFT_5086();
void CombineDFT_5087();
void CombineDFT_5088();
void CombineDFT_5089();
void CombineDFT_5090();
void CombineDFT_5091();
void CombineDFT_5092();
void CombineDFT_5093();
void CombineDFT_5094();
void CombineDFT_5095();
void CombineDFT_5096();
void WEIGHTED_ROUND_ROBIN_Joiner_5080();
void WEIGHTED_ROUND_ROBIN_Splitter_5097();
void CombineDFT_5099();
void CombineDFT_5100();
void CombineDFT_5101();
void CombineDFT_5102();
void CombineDFT_5103();
void CombineDFT_5104();
void CombineDFT_5105();
void CombineDFT_5106();
void WEIGHTED_ROUND_ROBIN_Joiner_5098();
void WEIGHTED_ROUND_ROBIN_Splitter_5107();
void CombineDFT_5109();
void CombineDFT_5110();
void CombineDFT_5111();
void CombineDFT_5112();
void WEIGHTED_ROUND_ROBIN_Joiner_5108();
void WEIGHTED_ROUND_ROBIN_Splitter_5113();
void CombineDFT_5115();
void CombineDFT_5116();
void WEIGHTED_ROUND_ROBIN_Joiner_5114();
void CombineDFT_4992();
void FFTReorderSimple_4993();
void WEIGHTED_ROUND_ROBIN_Splitter_5117();
void FFTReorderSimple_5119();
void FFTReorderSimple_5120();
void WEIGHTED_ROUND_ROBIN_Joiner_5118();
void WEIGHTED_ROUND_ROBIN_Splitter_5121();
void FFTReorderSimple_5123();
void FFTReorderSimple_5124();
void FFTReorderSimple_5125();
void FFTReorderSimple_5126();
void WEIGHTED_ROUND_ROBIN_Joiner_5122();
void WEIGHTED_ROUND_ROBIN_Splitter_5127();
void FFTReorderSimple_5129();
void FFTReorderSimple_5130();
void FFTReorderSimple_5131();
void FFTReorderSimple_5132();
void FFTReorderSimple_5133();
void FFTReorderSimple_5134();
void FFTReorderSimple_5135();
void FFTReorderSimple_5136();
void WEIGHTED_ROUND_ROBIN_Joiner_5128();
void WEIGHTED_ROUND_ROBIN_Splitter_5137();
void FFTReorderSimple_5139();
void FFTReorderSimple_5140();
void FFTReorderSimple_5141();
void FFTReorderSimple_5142();
void FFTReorderSimple_5143();
void FFTReorderSimple_5144();
void FFTReorderSimple_5145();
void FFTReorderSimple_5146();
void FFTReorderSimple_5147();
void FFTReorderSimple_5148();
void FFTReorderSimple_5149();
void FFTReorderSimple_5150();
void FFTReorderSimple_5151();
void FFTReorderSimple_5152();
void FFTReorderSimple_5153();
void FFTReorderSimple_5154();
void WEIGHTED_ROUND_ROBIN_Joiner_5138();
void WEIGHTED_ROUND_ROBIN_Splitter_5155();
void CombineDFT_5157();
void CombineDFT_5158();
void CombineDFT_5159();
void CombineDFT_5160();
void CombineDFT_5161();
void CombineDFT_5162();
void CombineDFT_5163();
void CombineDFT_5164();
void CombineDFT_5165();
void CombineDFT_5166();
void CombineDFT_5167();
void CombineDFT_5168();
void CombineDFT_5169();
void CombineDFT_5170();
void CombineDFT_5171();
void CombineDFT_5172();
void CombineDFT_5173();
void CombineDFT_5174();
void CombineDFT_5175();
void CombineDFT_5176();
void CombineDFT_5177();
void CombineDFT_5178();
void WEIGHTED_ROUND_ROBIN_Joiner_5156();
void WEIGHTED_ROUND_ROBIN_Splitter_5179();
void CombineDFT_5181();
void CombineDFT_5182();
void CombineDFT_5183();
void CombineDFT_5184();
void CombineDFT_5185();
void CombineDFT_5186();
void CombineDFT_5187();
void CombineDFT_5188();
void CombineDFT_5189();
void CombineDFT_5190();
void CombineDFT_5191();
void CombineDFT_5192();
void CombineDFT_5193();
void CombineDFT_5194();
void CombineDFT_5195();
void CombineDFT_5196();
void WEIGHTED_ROUND_ROBIN_Joiner_5180();
void WEIGHTED_ROUND_ROBIN_Splitter_5197();
void CombineDFT_5199();
void CombineDFT_5200();
void CombineDFT_5201();
void CombineDFT_5202();
void CombineDFT_5203();
void CombineDFT_5204();
void CombineDFT_5205();
void CombineDFT_5206();
void WEIGHTED_ROUND_ROBIN_Joiner_5198();
void WEIGHTED_ROUND_ROBIN_Splitter_5207();
void CombineDFT_5209();
void CombineDFT_5210();
void CombineDFT_5211();
void CombineDFT_5212();
void WEIGHTED_ROUND_ROBIN_Joiner_5208();
void WEIGHTED_ROUND_ROBIN_Splitter_5213();
void CombineDFT_5215();
void CombineDFT_5216();
void WEIGHTED_ROUND_ROBIN_Joiner_5214();
void CombineDFT_5003();
void WEIGHTED_ROUND_ROBIN_Joiner_5006();
void FloatPrinter_5004();

#ifdef __cplusplus
}
#endif
#endif
