#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=1664 on the compile command line
#else
#if BUF_SIZEMAX < 1664
#error BUF_SIZEMAX too small, it must be at least 1664
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	complex_t wn;
} CombineDFT_1541_t;
void FFTTestSource(buffer_complex_t *chanout);
void FFTTestSource_1487();
void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout);
void FFTReorderSimple_1488();
void WEIGHTED_ROUND_ROBIN_Splitter_1501();
void FFTReorderSimple_1503();
void FFTReorderSimple_1504();
void WEIGHTED_ROUND_ROBIN_Joiner_1502();
void WEIGHTED_ROUND_ROBIN_Splitter_1505();
void FFTReorderSimple_1507();
void FFTReorderSimple_1508();
void FFTReorderSimple_1509();
void FFTReorderSimple_1510();
void WEIGHTED_ROUND_ROBIN_Joiner_1506();
void WEIGHTED_ROUND_ROBIN_Splitter_1511();
void FFTReorderSimple_1513();
void FFTReorderSimple_1514();
void FFTReorderSimple_1515();
void FFTReorderSimple_1516();
void FFTReorderSimple_1517();
void FFTReorderSimple_1518();
void FFTReorderSimple_1519();
void FFTReorderSimple_1520();
void WEIGHTED_ROUND_ROBIN_Joiner_1512();
void WEIGHTED_ROUND_ROBIN_Splitter_1521();
void FFTReorderSimple_1523();
void FFTReorderSimple_1524();
void FFTReorderSimple_1525();
void FFTReorderSimple_1526();
void FFTReorderSimple_1527();
void FFTReorderSimple_1528();
void FFTReorderSimple_1529();
void FFTReorderSimple_1530();
void FFTReorderSimple_1531();
void FFTReorderSimple_1532();
void FFTReorderSimple_1533();
void FFTReorderSimple_1534();
void FFTReorderSimple_1535();
void FFTReorderSimple_1536();
void FFTReorderSimple_1537();
void FFTReorderSimple_1538();
void WEIGHTED_ROUND_ROBIN_Joiner_1522();
void WEIGHTED_ROUND_ROBIN_Splitter_1539();
void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout);
void CombineDFT_1541();
void CombineDFT_1542();
void CombineDFT_1543();
void CombineDFT_1544();
void CombineDFT_1545();
void CombineDFT_1546();
void CombineDFT_1547();
void CombineDFT_1548();
void CombineDFT_1549();
void CombineDFT_1550();
void CombineDFT_1551();
void CombineDFT_1552();
void CombineDFT_1553();
void CombineDFT_1554();
void CombineDFT_1555();
void CombineDFT_1556();
void CombineDFT_1557();
void CombineDFT_1558();
void CombineDFT_1559();
void CombineDFT_1560();
void CombineDFT_1561();
void CombineDFT_1562();
void CombineDFT_1563();
void CombineDFT_1564();
void CombineDFT_1565();
void CombineDFT_1566();
void WEIGHTED_ROUND_ROBIN_Joiner_1540();
void WEIGHTED_ROUND_ROBIN_Splitter_1567();
void CombineDFT_1569();
void CombineDFT_1570();
void CombineDFT_1571();
void CombineDFT_1572();
void CombineDFT_1573();
void CombineDFT_1574();
void CombineDFT_1575();
void CombineDFT_1576();
void CombineDFT_1577();
void CombineDFT_1578();
void CombineDFT_1579();
void CombineDFT_1580();
void CombineDFT_1581();
void CombineDFT_1582();
void CombineDFT_1583();
void CombineDFT_1584();
void WEIGHTED_ROUND_ROBIN_Joiner_1568();
void WEIGHTED_ROUND_ROBIN_Splitter_1585();
void CombineDFT_1587();
void CombineDFT_1588();
void CombineDFT_1589();
void CombineDFT_1590();
void CombineDFT_1591();
void CombineDFT_1592();
void CombineDFT_1593();
void CombineDFT_1594();
void WEIGHTED_ROUND_ROBIN_Joiner_1586();
void WEIGHTED_ROUND_ROBIN_Splitter_1595();
void CombineDFT_1597();
void CombineDFT_1598();
void CombineDFT_1599();
void CombineDFT_1600();
void WEIGHTED_ROUND_ROBIN_Joiner_1596();
void WEIGHTED_ROUND_ROBIN_Splitter_1601();
void CombineDFT_1603();
void CombineDFT_1604();
void WEIGHTED_ROUND_ROBIN_Joiner_1602();
void CombineDFT_1498();
void CPrinter(buffer_complex_t *chanin);
void CPrinter_1499();

#ifdef __cplusplus
}
#endif
#endif
