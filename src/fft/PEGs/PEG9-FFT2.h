#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=4608 on the compile command line
#else
#if BUF_SIZEMAX < 4608
#error BUF_SIZEMAX too small, it must be at least 4608
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float w[2];
} CombineDFT_10537_t;

typedef struct {
	float w[4];
} CombineDFT_10548_t;

typedef struct {
	float w[8];
} CombineDFT_10559_t;

typedef struct {
	float w[16];
} CombineDFT_10569_t;

typedef struct {
	float w[32];
} CombineDFT_10575_t;

typedef struct {
	float w[64];
} CombineDFT_10479_t;
void WEIGHTED_ROUND_ROBIN_Splitter_10500();
void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout);
void FFTTestSource_10502();
void FFTTestSource_10503();
void WEIGHTED_ROUND_ROBIN_Joiner_10501();
void WEIGHTED_ROUND_ROBIN_Splitter_10492();
void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout);
void FFTReorderSimple_10469();
void WEIGHTED_ROUND_ROBIN_Splitter_10504();
void FFTReorderSimple_10506();
void FFTReorderSimple_10507();
void WEIGHTED_ROUND_ROBIN_Joiner_10505();
void WEIGHTED_ROUND_ROBIN_Splitter_10508();
void FFTReorderSimple_10510();
void FFTReorderSimple_10511();
void FFTReorderSimple_10512();
void FFTReorderSimple_10513();
void WEIGHTED_ROUND_ROBIN_Joiner_10509();
void WEIGHTED_ROUND_ROBIN_Splitter_10514();
void FFTReorderSimple_10516();
void FFTReorderSimple_10517();
void FFTReorderSimple_10518();
void FFTReorderSimple_10519();
void FFTReorderSimple_10520();
void FFTReorderSimple_10521();
void FFTReorderSimple_10522();
void FFTReorderSimple_10523();
void WEIGHTED_ROUND_ROBIN_Joiner_10515();
void WEIGHTED_ROUND_ROBIN_Splitter_10524();
void FFTReorderSimple_10526();
void FFTReorderSimple_10527();
void FFTReorderSimple_10528();
void FFTReorderSimple_10529();
void FFTReorderSimple_10530();
void FFTReorderSimple_10531();
void FFTReorderSimple_10532();
void FFTReorderSimple_10533();
void FFTReorderSimple_10534();
void WEIGHTED_ROUND_ROBIN_Joiner_10525();
void WEIGHTED_ROUND_ROBIN_Splitter_10535();
void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout);
void CombineDFT_10537();
void CombineDFT_10538();
void CombineDFT_10539();
void CombineDFT_10540();
void CombineDFT_10541();
void CombineDFT_10542();
void CombineDFT_10543();
void CombineDFT_10544();
void CombineDFT_10545();
void WEIGHTED_ROUND_ROBIN_Joiner_10536();
void WEIGHTED_ROUND_ROBIN_Splitter_10546();
void CombineDFT_10548();
void CombineDFT_10549();
void CombineDFT_10550();
void CombineDFT_10551();
void CombineDFT_10552();
void CombineDFT_10553();
void CombineDFT_10554();
void CombineDFT_10555();
void CombineDFT_10556();
void WEIGHTED_ROUND_ROBIN_Joiner_10547();
void WEIGHTED_ROUND_ROBIN_Splitter_10557();
void CombineDFT_10559();
void CombineDFT_10560();
void CombineDFT_10561();
void CombineDFT_10562();
void CombineDFT_10563();
void CombineDFT_10564();
void CombineDFT_10565();
void CombineDFT_10566();
void WEIGHTED_ROUND_ROBIN_Joiner_10558();
void WEIGHTED_ROUND_ROBIN_Splitter_10567();
void CombineDFT_10569();
void CombineDFT_10570();
void CombineDFT_10571();
void CombineDFT_10572();
void WEIGHTED_ROUND_ROBIN_Joiner_10568();
void WEIGHTED_ROUND_ROBIN_Splitter_10573();
void CombineDFT_10575();
void CombineDFT_10576();
void WEIGHTED_ROUND_ROBIN_Joiner_10574();
void CombineDFT_10479();
void FFTReorderSimple_10480();
void WEIGHTED_ROUND_ROBIN_Splitter_10577();
void FFTReorderSimple_10579();
void FFTReorderSimple_10580();
void WEIGHTED_ROUND_ROBIN_Joiner_10578();
void WEIGHTED_ROUND_ROBIN_Splitter_10581();
void FFTReorderSimple_10583();
void FFTReorderSimple_10584();
void FFTReorderSimple_10585();
void FFTReorderSimple_10586();
void WEIGHTED_ROUND_ROBIN_Joiner_10582();
void WEIGHTED_ROUND_ROBIN_Splitter_10587();
void FFTReorderSimple_10589();
void FFTReorderSimple_10590();
void FFTReorderSimple_10591();
void FFTReorderSimple_10592();
void FFTReorderSimple_10593();
void FFTReorderSimple_10594();
void FFTReorderSimple_10595();
void FFTReorderSimple_10596();
void WEIGHTED_ROUND_ROBIN_Joiner_10588();
void WEIGHTED_ROUND_ROBIN_Splitter_10597();
void FFTReorderSimple_10599();
void FFTReorderSimple_10600();
void FFTReorderSimple_10601();
void FFTReorderSimple_10602();
void FFTReorderSimple_10603();
void FFTReorderSimple_10604();
void FFTReorderSimple_10605();
void FFTReorderSimple_10606();
void FFTReorderSimple_10607();
void WEIGHTED_ROUND_ROBIN_Joiner_10598();
void WEIGHTED_ROUND_ROBIN_Splitter_10608();
void CombineDFT_10610();
void CombineDFT_10611();
void CombineDFT_10612();
void CombineDFT_10613();
void CombineDFT_10614();
void CombineDFT_10615();
void CombineDFT_10616();
void CombineDFT_10617();
void CombineDFT_10618();
void WEIGHTED_ROUND_ROBIN_Joiner_10609();
void WEIGHTED_ROUND_ROBIN_Splitter_10619();
void CombineDFT_10621();
void CombineDFT_10622();
void CombineDFT_10623();
void CombineDFT_10624();
void CombineDFT_10625();
void CombineDFT_10626();
void CombineDFT_10627();
void CombineDFT_10628();
void CombineDFT_10629();
void WEIGHTED_ROUND_ROBIN_Joiner_10620();
void WEIGHTED_ROUND_ROBIN_Splitter_10630();
void CombineDFT_10632();
void CombineDFT_10633();
void CombineDFT_10634();
void CombineDFT_10635();
void CombineDFT_10636();
void CombineDFT_10637();
void CombineDFT_10638();
void CombineDFT_10639();
void WEIGHTED_ROUND_ROBIN_Joiner_10631();
void WEIGHTED_ROUND_ROBIN_Splitter_10640();
void CombineDFT_10642();
void CombineDFT_10643();
void CombineDFT_10644();
void CombineDFT_10645();
void WEIGHTED_ROUND_ROBIN_Joiner_10641();
void WEIGHTED_ROUND_ROBIN_Splitter_10646();
void CombineDFT_10648();
void CombineDFT_10649();
void WEIGHTED_ROUND_ROBIN_Joiner_10647();
void CombineDFT_10490();
void WEIGHTED_ROUND_ROBIN_Joiner_10493();
void FloatPrinter(buffer_float_t *chanin);
void FloatPrinter_10491();

#ifdef __cplusplus
}
#endif
#endif
