#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=160 on the compile command line
#else
#if BUF_SIZEMAX < 160
#error BUF_SIZEMAX too small, it must be at least 160
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif


void WEIGHTED_ROUND_ROBIN_Splitter_8333();
void source_8335();
void source_8336();
void WEIGHTED_ROUND_ROBIN_Joiner_8334();
void WEIGHTED_ROUND_ROBIN_Splitter_8176();
void WEIGHTED_ROUND_ROBIN_Splitter_8178();
void WEIGHTED_ROUND_ROBIN_Splitter_8180();
void WEIGHTED_ROUND_ROBIN_Splitter_8182();
void Identity_8006();
void Identity_8008();
void WEIGHTED_ROUND_ROBIN_Joiner_8183();
void WEIGHTED_ROUND_ROBIN_Splitter_8184();
void Identity_8012();
void Identity_8014();
void WEIGHTED_ROUND_ROBIN_Joiner_8185();
void WEIGHTED_ROUND_ROBIN_Joiner_8181();
void WEIGHTED_ROUND_ROBIN_Splitter_8186();
void WEIGHTED_ROUND_ROBIN_Splitter_8188();
void Identity_8020();
void Identity_8022();
void WEIGHTED_ROUND_ROBIN_Joiner_8189();
void WEIGHTED_ROUND_ROBIN_Splitter_8190();
void Identity_8026();
void Identity_8028();
void WEIGHTED_ROUND_ROBIN_Joiner_8191();
void WEIGHTED_ROUND_ROBIN_Joiner_8187();
void WEIGHTED_ROUND_ROBIN_Joiner_8179();
void WEIGHTED_ROUND_ROBIN_Splitter_8192();
void WEIGHTED_ROUND_ROBIN_Splitter_8194();
void WEIGHTED_ROUND_ROBIN_Splitter_8196();
void Identity_8036();
void Identity_8038();
void WEIGHTED_ROUND_ROBIN_Joiner_8197();
void WEIGHTED_ROUND_ROBIN_Splitter_8198();
void Identity_8042();
void Identity_8044();
void WEIGHTED_ROUND_ROBIN_Joiner_8199();
void WEIGHTED_ROUND_ROBIN_Joiner_8195();
void WEIGHTED_ROUND_ROBIN_Splitter_8200();
void WEIGHTED_ROUND_ROBIN_Splitter_8202();
void Identity_8050();
void Identity_8052();
void WEIGHTED_ROUND_ROBIN_Joiner_8203();
void WEIGHTED_ROUND_ROBIN_Splitter_8204();
void Identity_8056();
void Identity_8058();
void WEIGHTED_ROUND_ROBIN_Joiner_8205();
void WEIGHTED_ROUND_ROBIN_Joiner_8201();
void WEIGHTED_ROUND_ROBIN_Joiner_8193();
void WEIGHTED_ROUND_ROBIN_Joiner_8177();
void WEIGHTED_ROUND_ROBIN_Splitter_8320();
void WEIGHTED_ROUND_ROBIN_Splitter_8321();
void Pre_CollapsedDataParallel_1_8153();
void butterfly_8060();
void Post_CollapsedDataParallel_2_8154();
void Pre_CollapsedDataParallel_1_8156();
void butterfly_8061();
void Post_CollapsedDataParallel_2_8157();
void Pre_CollapsedDataParallel_1_8159();
void butterfly_8062();
void Post_CollapsedDataParallel_2_8160();
void Pre_CollapsedDataParallel_1_8162();
void butterfly_8063();
void Post_CollapsedDataParallel_2_8163();
void WEIGHTED_ROUND_ROBIN_Joiner_8322();
void WEIGHTED_ROUND_ROBIN_Splitter_8323();
void Pre_CollapsedDataParallel_1_8165();
void butterfly_8064();
void Post_CollapsedDataParallel_2_8166();
void Pre_CollapsedDataParallel_1_8168();
void butterfly_8065();
void Post_CollapsedDataParallel_2_8169();
void Pre_CollapsedDataParallel_1_8171();
void butterfly_8066();
void Post_CollapsedDataParallel_2_8172();
void Pre_CollapsedDataParallel_1_8174();
void butterfly_8067();
void Post_CollapsedDataParallel_2_8175();
void WEIGHTED_ROUND_ROBIN_Joiner_8324();
void WEIGHTED_ROUND_ROBIN_Joiner_8325();
void WEIGHTED_ROUND_ROBIN_Splitter_8326();
void WEIGHTED_ROUND_ROBIN_Splitter_8327();
void WEIGHTED_ROUND_ROBIN_Splitter_8210();
void butterfly_8069();
void butterfly_8070();
void WEIGHTED_ROUND_ROBIN_Joiner_8211();
void WEIGHTED_ROUND_ROBIN_Splitter_8212();
void butterfly_8071();
void butterfly_8072();
void WEIGHTED_ROUND_ROBIN_Joiner_8213();
void WEIGHTED_ROUND_ROBIN_Joiner_8328();
void WEIGHTED_ROUND_ROBIN_Splitter_8329();
void WEIGHTED_ROUND_ROBIN_Splitter_8214();
void butterfly_8073();
void butterfly_8074();
void WEIGHTED_ROUND_ROBIN_Joiner_8215();
void WEIGHTED_ROUND_ROBIN_Splitter_8216();
void butterfly_8075();
void butterfly_8076();
void WEIGHTED_ROUND_ROBIN_Joiner_8217();
void WEIGHTED_ROUND_ROBIN_Joiner_8330();
void WEIGHTED_ROUND_ROBIN_Joiner_8331();
void WEIGHTED_ROUND_ROBIN_Splitter_8218();
void WEIGHTED_ROUND_ROBIN_Splitter_8220();
void butterfly_8078();
void butterfly_8079();
void butterfly_8080();
void butterfly_8081();
void WEIGHTED_ROUND_ROBIN_Joiner_8221();
void WEIGHTED_ROUND_ROBIN_Splitter_8222();
void butterfly_8082();
void butterfly_8083();
void butterfly_8084();
void butterfly_8085();
void WEIGHTED_ROUND_ROBIN_Joiner_8223();
void WEIGHTED_ROUND_ROBIN_Joiner_8219();
void WEIGHTED_ROUND_ROBIN_Splitter_8337();
void magnitude_8339();
void magnitude_8340();
void magnitude_8341();
void magnitude_8342();
void magnitude_8343();
void WEIGHTED_ROUND_ROBIN_Joiner_8338();
void sink_8087();

#ifdef __cplusplus
}
#endif
#endif
