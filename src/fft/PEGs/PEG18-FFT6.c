#include "PEG18-FFT6.h"

buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_3368_3378_split[16];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_3372_3382_join[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3270WEIGHTED_ROUND_ROBIN_Splitter_3273;
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_3368_3378_join[16];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3280WEIGHTED_ROUND_ROBIN_Splitter_3289;
buffer_complex_t SplitJoin10_CombineDFT_Fiss_3370_3380_join[16];
buffer_complex_t SplitJoin8_CombineDFT_Fiss_3369_3379_join[18];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_3367_3377_split[8];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3290WEIGHTED_ROUND_ROBIN_Splitter_3307;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3356WEIGHTED_ROUND_ROBIN_Splitter_3361;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3362CombineDFT_3266;
buffer_complex_t CombineDFT_3266CPrinter_3267;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3274WEIGHTED_ROUND_ROBIN_Splitter_3279;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3346WEIGHTED_ROUND_ROBIN_Splitter_3355;
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_3366_3376_join[4];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_3372_3382_split[4];
buffer_complex_t SplitJoin12_CombineDFT_Fiss_3371_3381_join[8];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_3373_3383_split[2];
buffer_complex_t FFTReorderSimple_3256WEIGHTED_ROUND_ROBIN_Splitter_3269;
buffer_complex_t SplitJoin12_CombineDFT_Fiss_3371_3381_split[8];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3328WEIGHTED_ROUND_ROBIN_Splitter_3345;
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_3366_3376_split[4];
buffer_complex_t SplitJoin8_CombineDFT_Fiss_3369_3379_split[18];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3308WEIGHTED_ROUND_ROBIN_Splitter_3327;
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_3365_3375_split[2];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_3370_3380_split[16];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_3367_3377_join[8];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_3373_3383_join[2];
buffer_complex_t FFTTestSource_3255FFTReorderSimple_3256;
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_3365_3375_join[2];


CombineDFT_3309_t CombineDFT_3309_s;
CombineDFT_3309_t CombineDFT_3310_s;
CombineDFT_3309_t CombineDFT_3311_s;
CombineDFT_3309_t CombineDFT_3312_s;
CombineDFT_3309_t CombineDFT_3313_s;
CombineDFT_3309_t CombineDFT_3314_s;
CombineDFT_3309_t CombineDFT_3315_s;
CombineDFT_3309_t CombineDFT_3316_s;
CombineDFT_3309_t CombineDFT_3317_s;
CombineDFT_3309_t CombineDFT_3318_s;
CombineDFT_3309_t CombineDFT_3319_s;
CombineDFT_3309_t CombineDFT_3320_s;
CombineDFT_3309_t CombineDFT_3321_s;
CombineDFT_3309_t CombineDFT_3322_s;
CombineDFT_3309_t CombineDFT_3323_s;
CombineDFT_3309_t CombineDFT_3324_s;
CombineDFT_3309_t CombineDFT_3325_s;
CombineDFT_3309_t CombineDFT_3326_s;
CombineDFT_3309_t CombineDFT_3329_s;
CombineDFT_3309_t CombineDFT_3330_s;
CombineDFT_3309_t CombineDFT_3331_s;
CombineDFT_3309_t CombineDFT_3332_s;
CombineDFT_3309_t CombineDFT_3333_s;
CombineDFT_3309_t CombineDFT_3334_s;
CombineDFT_3309_t CombineDFT_3335_s;
CombineDFT_3309_t CombineDFT_3336_s;
CombineDFT_3309_t CombineDFT_3337_s;
CombineDFT_3309_t CombineDFT_3338_s;
CombineDFT_3309_t CombineDFT_3339_s;
CombineDFT_3309_t CombineDFT_3340_s;
CombineDFT_3309_t CombineDFT_3341_s;
CombineDFT_3309_t CombineDFT_3342_s;
CombineDFT_3309_t CombineDFT_3343_s;
CombineDFT_3309_t CombineDFT_3344_s;
CombineDFT_3309_t CombineDFT_3347_s;
CombineDFT_3309_t CombineDFT_3348_s;
CombineDFT_3309_t CombineDFT_3349_s;
CombineDFT_3309_t CombineDFT_3350_s;
CombineDFT_3309_t CombineDFT_3351_s;
CombineDFT_3309_t CombineDFT_3352_s;
CombineDFT_3309_t CombineDFT_3353_s;
CombineDFT_3309_t CombineDFT_3354_s;
CombineDFT_3309_t CombineDFT_3357_s;
CombineDFT_3309_t CombineDFT_3358_s;
CombineDFT_3309_t CombineDFT_3359_s;
CombineDFT_3309_t CombineDFT_3360_s;
CombineDFT_3309_t CombineDFT_3363_s;
CombineDFT_3309_t CombineDFT_3364_s;
CombineDFT_3309_t CombineDFT_3266_s;

void FFTTestSource(buffer_complex_t *chanout) {
		complex_t c1;
		complex_t zero;
		c1.real = 1.0 ; 
		c1.imag = 0.0 ; 
		zero.real = 0.0 ; 
		zero.imag = 0.0 ; 
		push_complex(&(*chanout), zero) ; 
		push_complex(&(*chanout), c1) ; 
		FOR(int, i, 0,  < , 62, i++) {
			push_complex(&(*chanout), zero) ; 
		}
		ENDFOR
	}


void FFTTestSource_3255() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTTestSource(&(FFTTestSource_3255FFTReorderSimple_3256));
	ENDFOR
}

void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 64, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&(*chanin)) ; 
		}
		ENDFOR
	}


void FFTReorderSimple_3256() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(FFTTestSource_3255FFTReorderSimple_3256), &(FFTReorderSimple_3256WEIGHTED_ROUND_ROBIN_Splitter_3269));
	ENDFOR
}

void FFTReorderSimple_3271() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_3365_3375_split[0]), &(SplitJoin0_FFTReorderSimple_Fiss_3365_3375_join[0]));
	ENDFOR
}

void FFTReorderSimple_3272() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_3365_3375_split[1]), &(SplitJoin0_FFTReorderSimple_Fiss_3365_3375_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3269() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_3365_3375_split[0], pop_complex(&FFTReorderSimple_3256WEIGHTED_ROUND_ROBIN_Splitter_3269));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_3365_3375_split[1], pop_complex(&FFTReorderSimple_3256WEIGHTED_ROUND_ROBIN_Splitter_3269));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3270() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3270WEIGHTED_ROUND_ROBIN_Splitter_3273, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_3365_3375_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3270WEIGHTED_ROUND_ROBIN_Splitter_3273, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_3365_3375_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_3275() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_3366_3376_split[0]), &(SplitJoin2_FFTReorderSimple_Fiss_3366_3376_join[0]));
	ENDFOR
}

void FFTReorderSimple_3276() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_3366_3376_split[1]), &(SplitJoin2_FFTReorderSimple_Fiss_3366_3376_join[1]));
	ENDFOR
}

void FFTReorderSimple_3277() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_3366_3376_split[2]), &(SplitJoin2_FFTReorderSimple_Fiss_3366_3376_join[2]));
	ENDFOR
}

void FFTReorderSimple_3278() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_3366_3376_split[3]), &(SplitJoin2_FFTReorderSimple_Fiss_3366_3376_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3273() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin2_FFTReorderSimple_Fiss_3366_3376_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3270WEIGHTED_ROUND_ROBIN_Splitter_3273));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3274() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3274WEIGHTED_ROUND_ROBIN_Splitter_3279, pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_3366_3376_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_3281() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_3367_3377_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_3367_3377_join[0]));
	ENDFOR
}

void FFTReorderSimple_3282() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_3367_3377_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_3367_3377_join[1]));
	ENDFOR
}

void FFTReorderSimple_3283() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_3367_3377_split[2]), &(SplitJoin4_FFTReorderSimple_Fiss_3367_3377_join[2]));
	ENDFOR
}

void FFTReorderSimple_3284() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_3367_3377_split[3]), &(SplitJoin4_FFTReorderSimple_Fiss_3367_3377_join[3]));
	ENDFOR
}

void FFTReorderSimple_3285() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_3367_3377_split[4]), &(SplitJoin4_FFTReorderSimple_Fiss_3367_3377_join[4]));
	ENDFOR
}

void FFTReorderSimple_3286() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_3367_3377_split[5]), &(SplitJoin4_FFTReorderSimple_Fiss_3367_3377_join[5]));
	ENDFOR
}

void FFTReorderSimple_3287() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_3367_3377_split[6]), &(SplitJoin4_FFTReorderSimple_Fiss_3367_3377_join[6]));
	ENDFOR
}

void FFTReorderSimple_3288() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_3367_3377_split[7]), &(SplitJoin4_FFTReorderSimple_Fiss_3367_3377_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3279() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3367_3377_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3274WEIGHTED_ROUND_ROBIN_Splitter_3279));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3280() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3280WEIGHTED_ROUND_ROBIN_Splitter_3289, pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_3367_3377_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_3291() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_join[0]));
	ENDFOR
}

void FFTReorderSimple_3292() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_join[1]));
	ENDFOR
}

void FFTReorderSimple_3293() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_join[2]));
	ENDFOR
}

void FFTReorderSimple_3294() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_join[3]));
	ENDFOR
}

void FFTReorderSimple_3295() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_split[4]), &(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_join[4]));
	ENDFOR
}

void FFTReorderSimple_3296() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_split[5]), &(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_join[5]));
	ENDFOR
}

void FFTReorderSimple_3297() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_split[6]), &(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_join[6]));
	ENDFOR
}

void FFTReorderSimple_3298() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_split[7]), &(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_join[7]));
	ENDFOR
}

void FFTReorderSimple_3299() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_split[8]), &(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_join[8]));
	ENDFOR
}

void FFTReorderSimple_3300() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_split[9]), &(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_join[9]));
	ENDFOR
}

void FFTReorderSimple_3301() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_split[10]), &(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_join[10]));
	ENDFOR
}

void FFTReorderSimple_3302() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_split[11]), &(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_join[11]));
	ENDFOR
}

void FFTReorderSimple_3303() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_split[12]), &(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_join[12]));
	ENDFOR
}

void FFTReorderSimple_3304() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_split[13]), &(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_join[13]));
	ENDFOR
}

void FFTReorderSimple_3305() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_split[14]), &(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_join[14]));
	ENDFOR
}

void FFTReorderSimple_3306() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_split[15]), &(SplitJoin6_FFTReorderSimple_Fiss_3368_3378_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3289() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3368_3378_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3280WEIGHTED_ROUND_ROBIN_Splitter_3289));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3290() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3290WEIGHTED_ROUND_ROBIN_Splitter_3307, pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3368_3378_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&(*chanin), (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3309_s.wn.real) - (w.imag * CombineDFT_3309_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3309_s.wn.imag) + (w.imag * CombineDFT_3309_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&(*chanin)) ; 
			push_complex(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_3309() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3369_3379_split[0]), &(SplitJoin8_CombineDFT_Fiss_3369_3379_join[0]));
	ENDFOR
}

void CombineDFT_3310() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3369_3379_split[1]), &(SplitJoin8_CombineDFT_Fiss_3369_3379_join[1]));
	ENDFOR
}

void CombineDFT_3311() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3369_3379_split[2]), &(SplitJoin8_CombineDFT_Fiss_3369_3379_join[2]));
	ENDFOR
}

void CombineDFT_3312() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3369_3379_split[3]), &(SplitJoin8_CombineDFT_Fiss_3369_3379_join[3]));
	ENDFOR
}

void CombineDFT_3313() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3369_3379_split[4]), &(SplitJoin8_CombineDFT_Fiss_3369_3379_join[4]));
	ENDFOR
}

void CombineDFT_3314() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3369_3379_split[5]), &(SplitJoin8_CombineDFT_Fiss_3369_3379_join[5]));
	ENDFOR
}

void CombineDFT_3315() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3369_3379_split[6]), &(SplitJoin8_CombineDFT_Fiss_3369_3379_join[6]));
	ENDFOR
}

void CombineDFT_3316() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3369_3379_split[7]), &(SplitJoin8_CombineDFT_Fiss_3369_3379_join[7]));
	ENDFOR
}

void CombineDFT_3317() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3369_3379_split[8]), &(SplitJoin8_CombineDFT_Fiss_3369_3379_join[8]));
	ENDFOR
}

void CombineDFT_3318() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3369_3379_split[9]), &(SplitJoin8_CombineDFT_Fiss_3369_3379_join[9]));
	ENDFOR
}

void CombineDFT_3319() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3369_3379_split[10]), &(SplitJoin8_CombineDFT_Fiss_3369_3379_join[10]));
	ENDFOR
}

void CombineDFT_3320() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3369_3379_split[11]), &(SplitJoin8_CombineDFT_Fiss_3369_3379_join[11]));
	ENDFOR
}

void CombineDFT_3321() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3369_3379_split[12]), &(SplitJoin8_CombineDFT_Fiss_3369_3379_join[12]));
	ENDFOR
}

void CombineDFT_3322() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3369_3379_split[13]), &(SplitJoin8_CombineDFT_Fiss_3369_3379_join[13]));
	ENDFOR
}

void CombineDFT_3323() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3369_3379_split[14]), &(SplitJoin8_CombineDFT_Fiss_3369_3379_join[14]));
	ENDFOR
}

void CombineDFT_3324() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3369_3379_split[15]), &(SplitJoin8_CombineDFT_Fiss_3369_3379_join[15]));
	ENDFOR
}

void CombineDFT_3325() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3369_3379_split[16]), &(SplitJoin8_CombineDFT_Fiss_3369_3379_join[16]));
	ENDFOR
}

void CombineDFT_3326() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_3369_3379_split[17]), &(SplitJoin8_CombineDFT_Fiss_3369_3379_join[17]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3307() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 18, __iter_++)
			push_complex(&SplitJoin8_CombineDFT_Fiss_3369_3379_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3290WEIGHTED_ROUND_ROBIN_Splitter_3307));
			push_complex(&SplitJoin8_CombineDFT_Fiss_3369_3379_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3290WEIGHTED_ROUND_ROBIN_Splitter_3307));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3308() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 18, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3308WEIGHTED_ROUND_ROBIN_Splitter_3327, pop_complex(&SplitJoin8_CombineDFT_Fiss_3369_3379_join[__iter_]));
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3308WEIGHTED_ROUND_ROBIN_Splitter_3327, pop_complex(&SplitJoin8_CombineDFT_Fiss_3369_3379_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_3329() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3370_3380_split[0]), &(SplitJoin10_CombineDFT_Fiss_3370_3380_join[0]));
	ENDFOR
}

void CombineDFT_3330() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3370_3380_split[1]), &(SplitJoin10_CombineDFT_Fiss_3370_3380_join[1]));
	ENDFOR
}

void CombineDFT_3331() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3370_3380_split[2]), &(SplitJoin10_CombineDFT_Fiss_3370_3380_join[2]));
	ENDFOR
}

void CombineDFT_3332() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3370_3380_split[3]), &(SplitJoin10_CombineDFT_Fiss_3370_3380_join[3]));
	ENDFOR
}

void CombineDFT_3333() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3370_3380_split[4]), &(SplitJoin10_CombineDFT_Fiss_3370_3380_join[4]));
	ENDFOR
}

void CombineDFT_3334() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3370_3380_split[5]), &(SplitJoin10_CombineDFT_Fiss_3370_3380_join[5]));
	ENDFOR
}

void CombineDFT_3335() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3370_3380_split[6]), &(SplitJoin10_CombineDFT_Fiss_3370_3380_join[6]));
	ENDFOR
}

void CombineDFT_3336() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3370_3380_split[7]), &(SplitJoin10_CombineDFT_Fiss_3370_3380_join[7]));
	ENDFOR
}

void CombineDFT_3337() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3370_3380_split[8]), &(SplitJoin10_CombineDFT_Fiss_3370_3380_join[8]));
	ENDFOR
}

void CombineDFT_3338() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3370_3380_split[9]), &(SplitJoin10_CombineDFT_Fiss_3370_3380_join[9]));
	ENDFOR
}

void CombineDFT_3339() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3370_3380_split[10]), &(SplitJoin10_CombineDFT_Fiss_3370_3380_join[10]));
	ENDFOR
}

void CombineDFT_3340() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3370_3380_split[11]), &(SplitJoin10_CombineDFT_Fiss_3370_3380_join[11]));
	ENDFOR
}

void CombineDFT_3341() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3370_3380_split[12]), &(SplitJoin10_CombineDFT_Fiss_3370_3380_join[12]));
	ENDFOR
}

void CombineDFT_3342() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3370_3380_split[13]), &(SplitJoin10_CombineDFT_Fiss_3370_3380_join[13]));
	ENDFOR
}

void CombineDFT_3343() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3370_3380_split[14]), &(SplitJoin10_CombineDFT_Fiss_3370_3380_join[14]));
	ENDFOR
}

void CombineDFT_3344() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_3370_3380_split[15]), &(SplitJoin10_CombineDFT_Fiss_3370_3380_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3327() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin10_CombineDFT_Fiss_3370_3380_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3308WEIGHTED_ROUND_ROBIN_Splitter_3327));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3328() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3328WEIGHTED_ROUND_ROBIN_Splitter_3345, pop_complex(&SplitJoin10_CombineDFT_Fiss_3370_3380_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_3347() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3371_3381_split[0]), &(SplitJoin12_CombineDFT_Fiss_3371_3381_join[0]));
	ENDFOR
}

void CombineDFT_3348() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3371_3381_split[1]), &(SplitJoin12_CombineDFT_Fiss_3371_3381_join[1]));
	ENDFOR
}

void CombineDFT_3349() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3371_3381_split[2]), &(SplitJoin12_CombineDFT_Fiss_3371_3381_join[2]));
	ENDFOR
}

void CombineDFT_3350() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3371_3381_split[3]), &(SplitJoin12_CombineDFT_Fiss_3371_3381_join[3]));
	ENDFOR
}

void CombineDFT_3351() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3371_3381_split[4]), &(SplitJoin12_CombineDFT_Fiss_3371_3381_join[4]));
	ENDFOR
}

void CombineDFT_3352() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3371_3381_split[5]), &(SplitJoin12_CombineDFT_Fiss_3371_3381_join[5]));
	ENDFOR
}

void CombineDFT_3353() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3371_3381_split[6]), &(SplitJoin12_CombineDFT_Fiss_3371_3381_join[6]));
	ENDFOR
}

void CombineDFT_3354() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3371_3381_split[7]), &(SplitJoin12_CombineDFT_Fiss_3371_3381_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3345() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin12_CombineDFT_Fiss_3371_3381_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3328WEIGHTED_ROUND_ROBIN_Splitter_3345));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3346() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3346WEIGHTED_ROUND_ROBIN_Splitter_3355, pop_complex(&SplitJoin12_CombineDFT_Fiss_3371_3381_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_3357() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_3372_3382_split[0]), &(SplitJoin14_CombineDFT_Fiss_3372_3382_join[0]));
	ENDFOR
}

void CombineDFT_3358() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_3372_3382_split[1]), &(SplitJoin14_CombineDFT_Fiss_3372_3382_join[1]));
	ENDFOR
}

void CombineDFT_3359() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_3372_3382_split[2]), &(SplitJoin14_CombineDFT_Fiss_3372_3382_join[2]));
	ENDFOR
}

void CombineDFT_3360() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_3372_3382_split[3]), &(SplitJoin14_CombineDFT_Fiss_3372_3382_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3355() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin14_CombineDFT_Fiss_3372_3382_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3346WEIGHTED_ROUND_ROBIN_Splitter_3355));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3356() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3356WEIGHTED_ROUND_ROBIN_Splitter_3361, pop_complex(&SplitJoin14_CombineDFT_Fiss_3372_3382_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_3363() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_3373_3383_split[0]), &(SplitJoin16_CombineDFT_Fiss_3373_3383_join[0]));
	ENDFOR
}

void CombineDFT_3364() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_3373_3383_split[1]), &(SplitJoin16_CombineDFT_Fiss_3373_3383_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3361() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_3373_3383_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3356WEIGHTED_ROUND_ROBIN_Splitter_3361));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_3373_3383_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3356WEIGHTED_ROUND_ROBIN_Splitter_3361));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3362() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3362CombineDFT_3266, pop_complex(&SplitJoin16_CombineDFT_Fiss_3373_3383_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3362CombineDFT_3266, pop_complex(&SplitJoin16_CombineDFT_Fiss_3373_3383_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_3266() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_3362CombineDFT_3266), &(CombineDFT_3266CPrinter_3267));
	ENDFOR
}

void CPrinter(buffer_complex_t *chanin) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		printf("%.10f", c.real);
		printf("\n");
		printf("%.10f", c.imag);
		printf("\n");
	}


void CPrinter_3267() {
	FOR(uint32_t, __iter_steady_, 0, <, 576, __iter_steady_++)
		CPrinter(&(CombineDFT_3266CPrinter_3267));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 16, __iter_init_0_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_3368_3378_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 4, __iter_init_1_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_3372_3382_join[__iter_init_1_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3270WEIGHTED_ROUND_ROBIN_Splitter_3273);
	FOR(int, __iter_init_2_, 0, <, 16, __iter_init_2_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_3368_3378_join[__iter_init_2_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3280WEIGHTED_ROUND_ROBIN_Splitter_3289);
	FOR(int, __iter_init_3_, 0, <, 16, __iter_init_3_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_3370_3380_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 18, __iter_init_4_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_3369_3379_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_3367_3377_split[__iter_init_5_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3290WEIGHTED_ROUND_ROBIN_Splitter_3307);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3356WEIGHTED_ROUND_ROBIN_Splitter_3361);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3362CombineDFT_3266);
	init_buffer_complex(&CombineDFT_3266CPrinter_3267);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3274WEIGHTED_ROUND_ROBIN_Splitter_3279);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3346WEIGHTED_ROUND_ROBIN_Splitter_3355);
	FOR(int, __iter_init_6_, 0, <, 4, __iter_init_6_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_3366_3376_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 4, __iter_init_7_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_3372_3382_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_3371_3381_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 2, __iter_init_9_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_3373_3383_split[__iter_init_9_]);
	ENDFOR
	init_buffer_complex(&FFTReorderSimple_3256WEIGHTED_ROUND_ROBIN_Splitter_3269);
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_3371_3381_split[__iter_init_10_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3328WEIGHTED_ROUND_ROBIN_Splitter_3345);
	FOR(int, __iter_init_11_, 0, <, 4, __iter_init_11_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_3366_3376_split[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 18, __iter_init_12_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_3369_3379_split[__iter_init_12_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3308WEIGHTED_ROUND_ROBIN_Splitter_3327);
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_3365_3375_split[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 16, __iter_init_14_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_3370_3380_split[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 8, __iter_init_15_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_3367_3377_join[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_3373_3383_join[__iter_init_16_]);
	ENDFOR
	init_buffer_complex(&FFTTestSource_3255FFTReorderSimple_3256);
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_3365_3375_join[__iter_init_17_]);
	ENDFOR
// --- init: CombineDFT_3309
	 {
	 ; 
	CombineDFT_3309_s.wn.real = -1.0 ; 
	CombineDFT_3309_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3310
	 {
	CombineDFT_3310_s.wn.real = -1.0 ; 
	CombineDFT_3310_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3311
	 {
	CombineDFT_3311_s.wn.real = -1.0 ; 
	CombineDFT_3311_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3312
	 {
	CombineDFT_3312_s.wn.real = -1.0 ; 
	CombineDFT_3312_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3313
	 {
	CombineDFT_3313_s.wn.real = -1.0 ; 
	CombineDFT_3313_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3314
	 {
	CombineDFT_3314_s.wn.real = -1.0 ; 
	CombineDFT_3314_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3315
	 {
	CombineDFT_3315_s.wn.real = -1.0 ; 
	CombineDFT_3315_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3316
	 {
	CombineDFT_3316_s.wn.real = -1.0 ; 
	CombineDFT_3316_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3317
	 {
	CombineDFT_3317_s.wn.real = -1.0 ; 
	CombineDFT_3317_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3318
	 {
	CombineDFT_3318_s.wn.real = -1.0 ; 
	CombineDFT_3318_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3319
	 {
	CombineDFT_3319_s.wn.real = -1.0 ; 
	CombineDFT_3319_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3320
	 {
	CombineDFT_3320_s.wn.real = -1.0 ; 
	CombineDFT_3320_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3321
	 {
	CombineDFT_3321_s.wn.real = -1.0 ; 
	CombineDFT_3321_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3322
	 {
	CombineDFT_3322_s.wn.real = -1.0 ; 
	CombineDFT_3322_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3323
	 {
	CombineDFT_3323_s.wn.real = -1.0 ; 
	CombineDFT_3323_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3324
	 {
	CombineDFT_3324_s.wn.real = -1.0 ; 
	CombineDFT_3324_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3325
	 {
	CombineDFT_3325_s.wn.real = -1.0 ; 
	CombineDFT_3325_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3326
	 {
	CombineDFT_3326_s.wn.real = -1.0 ; 
	CombineDFT_3326_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3329
	 {
	CombineDFT_3329_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3329_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3330
	 {
	CombineDFT_3330_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3330_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3331
	 {
	CombineDFT_3331_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3331_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3332
	 {
	CombineDFT_3332_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3332_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3333
	 {
	CombineDFT_3333_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3333_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3334
	 {
	CombineDFT_3334_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3334_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3335
	 {
	CombineDFT_3335_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3335_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3336
	 {
	CombineDFT_3336_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3336_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3337
	 {
	CombineDFT_3337_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3337_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3338
	 {
	CombineDFT_3338_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3338_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3339
	 {
	CombineDFT_3339_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3339_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3340
	 {
	CombineDFT_3340_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3340_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3341
	 {
	CombineDFT_3341_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3341_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3342
	 {
	CombineDFT_3342_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3342_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3343
	 {
	CombineDFT_3343_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3343_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3344
	 {
	CombineDFT_3344_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3344_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3347
	 {
	CombineDFT_3347_s.wn.real = 0.70710677 ; 
	CombineDFT_3347_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3348
	 {
	CombineDFT_3348_s.wn.real = 0.70710677 ; 
	CombineDFT_3348_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3349
	 {
	CombineDFT_3349_s.wn.real = 0.70710677 ; 
	CombineDFT_3349_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3350
	 {
	CombineDFT_3350_s.wn.real = 0.70710677 ; 
	CombineDFT_3350_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3351
	 {
	CombineDFT_3351_s.wn.real = 0.70710677 ; 
	CombineDFT_3351_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3352
	 {
	CombineDFT_3352_s.wn.real = 0.70710677 ; 
	CombineDFT_3352_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3353
	 {
	CombineDFT_3353_s.wn.real = 0.70710677 ; 
	CombineDFT_3353_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3354
	 {
	CombineDFT_3354_s.wn.real = 0.70710677 ; 
	CombineDFT_3354_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3357
	 {
	CombineDFT_3357_s.wn.real = 0.9238795 ; 
	CombineDFT_3357_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_3358
	 {
	CombineDFT_3358_s.wn.real = 0.9238795 ; 
	CombineDFT_3358_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_3359
	 {
	CombineDFT_3359_s.wn.real = 0.9238795 ; 
	CombineDFT_3359_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_3360
	 {
	CombineDFT_3360_s.wn.real = 0.9238795 ; 
	CombineDFT_3360_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_3363
	 {
	CombineDFT_3363_s.wn.real = 0.98078525 ; 
	CombineDFT_3363_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_3364
	 {
	CombineDFT_3364_s.wn.real = 0.98078525 ; 
	CombineDFT_3364_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_3266
	 {
	 ; 
	CombineDFT_3266_s.wn.real = 0.9951847 ; 
	CombineDFT_3266_s.wn.imag = -0.09801714 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FFTTestSource_3255();
		FFTReorderSimple_3256();
		WEIGHTED_ROUND_ROBIN_Splitter_3269();
			FFTReorderSimple_3271();
			FFTReorderSimple_3272();
		WEIGHTED_ROUND_ROBIN_Joiner_3270();
		WEIGHTED_ROUND_ROBIN_Splitter_3273();
			FFTReorderSimple_3275();
			FFTReorderSimple_3276();
			FFTReorderSimple_3277();
			FFTReorderSimple_3278();
		WEIGHTED_ROUND_ROBIN_Joiner_3274();
		WEIGHTED_ROUND_ROBIN_Splitter_3279();
			FFTReorderSimple_3281();
			FFTReorderSimple_3282();
			FFTReorderSimple_3283();
			FFTReorderSimple_3284();
			FFTReorderSimple_3285();
			FFTReorderSimple_3286();
			FFTReorderSimple_3287();
			FFTReorderSimple_3288();
		WEIGHTED_ROUND_ROBIN_Joiner_3280();
		WEIGHTED_ROUND_ROBIN_Splitter_3289();
			FFTReorderSimple_3291();
			FFTReorderSimple_3292();
			FFTReorderSimple_3293();
			FFTReorderSimple_3294();
			FFTReorderSimple_3295();
			FFTReorderSimple_3296();
			FFTReorderSimple_3297();
			FFTReorderSimple_3298();
			FFTReorderSimple_3299();
			FFTReorderSimple_3300();
			FFTReorderSimple_3301();
			FFTReorderSimple_3302();
			FFTReorderSimple_3303();
			FFTReorderSimple_3304();
			FFTReorderSimple_3305();
			FFTReorderSimple_3306();
		WEIGHTED_ROUND_ROBIN_Joiner_3290();
		WEIGHTED_ROUND_ROBIN_Splitter_3307();
			CombineDFT_3309();
			CombineDFT_3310();
			CombineDFT_3311();
			CombineDFT_3312();
			CombineDFT_3313();
			CombineDFT_3314();
			CombineDFT_3315();
			CombineDFT_3316();
			CombineDFT_3317();
			CombineDFT_3318();
			CombineDFT_3319();
			CombineDFT_3320();
			CombineDFT_3321();
			CombineDFT_3322();
			CombineDFT_3323();
			CombineDFT_3324();
			CombineDFT_3325();
			CombineDFT_3326();
		WEIGHTED_ROUND_ROBIN_Joiner_3308();
		WEIGHTED_ROUND_ROBIN_Splitter_3327();
			CombineDFT_3329();
			CombineDFT_3330();
			CombineDFT_3331();
			CombineDFT_3332();
			CombineDFT_3333();
			CombineDFT_3334();
			CombineDFT_3335();
			CombineDFT_3336();
			CombineDFT_3337();
			CombineDFT_3338();
			CombineDFT_3339();
			CombineDFT_3340();
			CombineDFT_3341();
			CombineDFT_3342();
			CombineDFT_3343();
			CombineDFT_3344();
		WEIGHTED_ROUND_ROBIN_Joiner_3328();
		WEIGHTED_ROUND_ROBIN_Splitter_3345();
			CombineDFT_3347();
			CombineDFT_3348();
			CombineDFT_3349();
			CombineDFT_3350();
			CombineDFT_3351();
			CombineDFT_3352();
			CombineDFT_3353();
			CombineDFT_3354();
		WEIGHTED_ROUND_ROBIN_Joiner_3346();
		WEIGHTED_ROUND_ROBIN_Splitter_3355();
			CombineDFT_3357();
			CombineDFT_3358();
			CombineDFT_3359();
			CombineDFT_3360();
		WEIGHTED_ROUND_ROBIN_Joiner_3356();
		WEIGHTED_ROUND_ROBIN_Splitter_3361();
			CombineDFT_3363();
			CombineDFT_3364();
		WEIGHTED_ROUND_ROBIN_Joiner_3362();
		CombineDFT_3266();
		CPrinter_3267();
	ENDFOR
	return EXIT_SUCCESS;
}
