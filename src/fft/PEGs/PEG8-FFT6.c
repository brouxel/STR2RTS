#include "PEG8-FFT6.h"

buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5248WEIGHTED_ROUND_ROBIN_Splitter_5253;
buffer_complex_t FFTTestSource_5173FFTReorderSimple_5174;
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_5258_5268_split[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5238WEIGHTED_ROUND_ROBIN_Splitter_5247;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5208WEIGHTED_ROUND_ROBIN_Splitter_5217;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5218WEIGHTED_ROUND_ROBIN_Splitter_5227;
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_5257_5267_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5228WEIGHTED_ROUND_ROBIN_Splitter_5237;
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_5260_5270_split[8];
buffer_complex_t FFTReorderSimple_5174WEIGHTED_ROUND_ROBIN_Splitter_5187;
buffer_complex_t SplitJoin10_CombineDFT_Fiss_5262_5272_join[8];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5192WEIGHTED_ROUND_ROBIN_Splitter_5197;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5198WEIGHTED_ROUND_ROBIN_Splitter_5207;
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_5258_5268_join[4];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_5259_5269_split[8];
buffer_complex_t SplitJoin12_CombineDFT_Fiss_5263_5273_split[8];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_5264_5274_join[4];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_5262_5272_split[8];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5188WEIGHTED_ROUND_ROBIN_Splitter_5191;
buffer_complex_t SplitJoin12_CombineDFT_Fiss_5263_5273_join[8];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5254CombineDFT_5184;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_5261_5271_split[8];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_5264_5274_split[4];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_5265_5275_split[2];
buffer_complex_t CombineDFT_5184CPrinter_5185;
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_5257_5267_join[2];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_5259_5269_join[8];
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_5260_5270_join[8];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_5265_5275_join[2];
buffer_complex_t SplitJoin8_CombineDFT_Fiss_5261_5271_join[8];


CombineDFT_5219_t CombineDFT_5219_s;
CombineDFT_5219_t CombineDFT_5220_s;
CombineDFT_5219_t CombineDFT_5221_s;
CombineDFT_5219_t CombineDFT_5222_s;
CombineDFT_5219_t CombineDFT_5223_s;
CombineDFT_5219_t CombineDFT_5224_s;
CombineDFT_5219_t CombineDFT_5225_s;
CombineDFT_5219_t CombineDFT_5226_s;
CombineDFT_5219_t CombineDFT_5229_s;
CombineDFT_5219_t CombineDFT_5230_s;
CombineDFT_5219_t CombineDFT_5231_s;
CombineDFT_5219_t CombineDFT_5232_s;
CombineDFT_5219_t CombineDFT_5233_s;
CombineDFT_5219_t CombineDFT_5234_s;
CombineDFT_5219_t CombineDFT_5235_s;
CombineDFT_5219_t CombineDFT_5236_s;
CombineDFT_5219_t CombineDFT_5239_s;
CombineDFT_5219_t CombineDFT_5240_s;
CombineDFT_5219_t CombineDFT_5241_s;
CombineDFT_5219_t CombineDFT_5242_s;
CombineDFT_5219_t CombineDFT_5243_s;
CombineDFT_5219_t CombineDFT_5244_s;
CombineDFT_5219_t CombineDFT_5245_s;
CombineDFT_5219_t CombineDFT_5246_s;
CombineDFT_5219_t CombineDFT_5249_s;
CombineDFT_5219_t CombineDFT_5250_s;
CombineDFT_5219_t CombineDFT_5251_s;
CombineDFT_5219_t CombineDFT_5252_s;
CombineDFT_5219_t CombineDFT_5255_s;
CombineDFT_5219_t CombineDFT_5256_s;
CombineDFT_5219_t CombineDFT_5184_s;

void FFTTestSource(buffer_complex_t *chanout) {
	complex_t c1;
	complex_t zero;
	c1.real = 1.0 ; 
	c1.imag = 0.0 ; 
	zero.real = 0.0 ; 
	zero.imag = 0.0 ; 
	push_complex(&(*chanout), zero) ; 
	push_complex(&(*chanout), c1) ; 
	FOR(int, i, 0,  < , 62, i++) {
		push_complex(&(*chanout), zero) ; 
	}
	ENDFOR
}


void FFTTestSource_5173() {
	FFTTestSource(&(FFTTestSource_5173FFTReorderSimple_5174));
}

void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout) {
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		complex_t __sa7 = {
			.real = 0,
			.imag = 0
		};
		__sa7 = ((complex_t) peek_complex(&(*chanin), i)) ; 
		push_complex(&(*chanout), __sa7) ; 
	}
	ENDFOR
	FOR(int, i, 1,  < , 64, i = (i + 2)) {
		complex_t __sa8 = {
			.real = 0,
			.imag = 0
		};
		__sa8 = ((complex_t) peek_complex(&(*chanin), i)) ; 
		push_complex(&(*chanout), __sa8) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 64, i++) {
		pop_complex(&(*chanin)) ; 
	}
	ENDFOR
}


void FFTReorderSimple_5174() {
	FFTReorderSimple(&(FFTTestSource_5173FFTReorderSimple_5174), &(FFTReorderSimple_5174WEIGHTED_ROUND_ROBIN_Splitter_5187));
}

void FFTReorderSimple_5189() {
	FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_5257_5267_split[0]), &(SplitJoin0_FFTReorderSimple_Fiss_5257_5267_join[0]));
}

void FFTReorderSimple_5190() {
	FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_5257_5267_split[1]), &(SplitJoin0_FFTReorderSimple_Fiss_5257_5267_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_5187() {
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&SplitJoin0_FFTReorderSimple_Fiss_5257_5267_split[0], pop_complex(&FFTReorderSimple_5174WEIGHTED_ROUND_ROBIN_Splitter_5187));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&SplitJoin0_FFTReorderSimple_Fiss_5257_5267_split[1], pop_complex(&FFTReorderSimple_5174WEIGHTED_ROUND_ROBIN_Splitter_5187));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_5188() {
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5188WEIGHTED_ROUND_ROBIN_Splitter_5191, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_5257_5267_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5188WEIGHTED_ROUND_ROBIN_Splitter_5191, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_5257_5267_join[1]));
	ENDFOR
}

void FFTReorderSimple_5193() {
	FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_5258_5268_split[0]), &(SplitJoin2_FFTReorderSimple_Fiss_5258_5268_join[0]));
}

void FFTReorderSimple_5194() {
	FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_5258_5268_split[1]), &(SplitJoin2_FFTReorderSimple_Fiss_5258_5268_join[1]));
}

void FFTReorderSimple_5195() {
	FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_5258_5268_split[2]), &(SplitJoin2_FFTReorderSimple_Fiss_5258_5268_join[2]));
}

void FFTReorderSimple_5196() {
	FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_5258_5268_split[3]), &(SplitJoin2_FFTReorderSimple_Fiss_5258_5268_join[3]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_5191() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5258_5268_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5188WEIGHTED_ROUND_ROBIN_Splitter_5191));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_5192() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5192WEIGHTED_ROUND_ROBIN_Splitter_5197, pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_5258_5268_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void FFTReorderSimple_5199() {
	FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_5259_5269_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_5259_5269_join[0]));
}

void FFTReorderSimple_5200() {
	FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_5259_5269_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_5259_5269_join[1]));
}

void FFTReorderSimple_5201() {
	FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_5259_5269_split[2]), &(SplitJoin4_FFTReorderSimple_Fiss_5259_5269_join[2]));
}

void FFTReorderSimple_5202() {
	FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_5259_5269_split[3]), &(SplitJoin4_FFTReorderSimple_Fiss_5259_5269_join[3]));
}

void FFTReorderSimple_5203() {
	FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_5259_5269_split[4]), &(SplitJoin4_FFTReorderSimple_Fiss_5259_5269_join[4]));
}

void FFTReorderSimple_5204() {
	FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_5259_5269_split[5]), &(SplitJoin4_FFTReorderSimple_Fiss_5259_5269_join[5]));
}

void FFTReorderSimple_5205() {
	FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_5259_5269_split[6]), &(SplitJoin4_FFTReorderSimple_Fiss_5259_5269_join[6]));
}

void FFTReorderSimple_5206() {
	FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_5259_5269_split[7]), &(SplitJoin4_FFTReorderSimple_Fiss_5259_5269_join[7]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_5197() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5259_5269_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5192WEIGHTED_ROUND_ROBIN_Splitter_5197));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_5198() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5198WEIGHTED_ROUND_ROBIN_Splitter_5207, pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_5259_5269_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void FFTReorderSimple_5209() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_5260_5270_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_5260_5270_join[0]));
	ENDFOR
}

void FFTReorderSimple_5210() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_5260_5270_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_5260_5270_join[1]));
	ENDFOR
}

void FFTReorderSimple_5211() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_5260_5270_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_5260_5270_join[2]));
	ENDFOR
}

void FFTReorderSimple_5212() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_5260_5270_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_5260_5270_join[3]));
	ENDFOR
}

void FFTReorderSimple_5213() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_5260_5270_split[4]), &(SplitJoin6_FFTReorderSimple_Fiss_5260_5270_join[4]));
	ENDFOR
}

void FFTReorderSimple_5214() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_5260_5270_split[5]), &(SplitJoin6_FFTReorderSimple_Fiss_5260_5270_join[5]));
	ENDFOR
}

void FFTReorderSimple_5215() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_5260_5270_split[6]), &(SplitJoin6_FFTReorderSimple_Fiss_5260_5270_join[6]));
	ENDFOR
}

void FFTReorderSimple_5216() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_5260_5270_split[7]), &(SplitJoin6_FFTReorderSimple_Fiss_5260_5270_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5207() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5260_5270_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5198WEIGHTED_ROUND_ROBIN_Splitter_5207));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5208() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5208WEIGHTED_ROUND_ROBIN_Splitter_5217, pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_5260_5270_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&(*chanin), (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5219_s.wn.real) - (w.imag * CombineDFT_5219_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5219_s.wn.imag) + (w.imag * CombineDFT_5219_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&(*chanin)) ; 
			push_complex(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_5219() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_5261_5271_split[0]), &(SplitJoin8_CombineDFT_Fiss_5261_5271_join[0]));
	ENDFOR
}

void CombineDFT_5220() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_5261_5271_split[1]), &(SplitJoin8_CombineDFT_Fiss_5261_5271_join[1]));
	ENDFOR
}

void CombineDFT_5221() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_5261_5271_split[2]), &(SplitJoin8_CombineDFT_Fiss_5261_5271_join[2]));
	ENDFOR
}

void CombineDFT_5222() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_5261_5271_split[3]), &(SplitJoin8_CombineDFT_Fiss_5261_5271_join[3]));
	ENDFOR
}

void CombineDFT_5223() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_5261_5271_split[4]), &(SplitJoin8_CombineDFT_Fiss_5261_5271_join[4]));
	ENDFOR
}

void CombineDFT_5224() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_5261_5271_split[5]), &(SplitJoin8_CombineDFT_Fiss_5261_5271_join[5]));
	ENDFOR
}

void CombineDFT_5225() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_5261_5271_split[6]), &(SplitJoin8_CombineDFT_Fiss_5261_5271_join[6]));
	ENDFOR
}

void CombineDFT_5226() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_5261_5271_split[7]), &(SplitJoin8_CombineDFT_Fiss_5261_5271_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5217() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin8_CombineDFT_Fiss_5261_5271_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5208WEIGHTED_ROUND_ROBIN_Splitter_5217));
			push_complex(&SplitJoin8_CombineDFT_Fiss_5261_5271_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5208WEIGHTED_ROUND_ROBIN_Splitter_5217));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5218() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5218WEIGHTED_ROUND_ROBIN_Splitter_5227, pop_complex(&SplitJoin8_CombineDFT_Fiss_5261_5271_join[__iter_]));
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5218WEIGHTED_ROUND_ROBIN_Splitter_5227, pop_complex(&SplitJoin8_CombineDFT_Fiss_5261_5271_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_5229() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_5262_5272_split[0]), &(SplitJoin10_CombineDFT_Fiss_5262_5272_join[0]));
	ENDFOR
}

void CombineDFT_5230() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_5262_5272_split[1]), &(SplitJoin10_CombineDFT_Fiss_5262_5272_join[1]));
	ENDFOR
}

void CombineDFT_5231() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_5262_5272_split[2]), &(SplitJoin10_CombineDFT_Fiss_5262_5272_join[2]));
	ENDFOR
}

void CombineDFT_5232() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_5262_5272_split[3]), &(SplitJoin10_CombineDFT_Fiss_5262_5272_join[3]));
	ENDFOR
}

void CombineDFT_5233() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_5262_5272_split[4]), &(SplitJoin10_CombineDFT_Fiss_5262_5272_join[4]));
	ENDFOR
}

void CombineDFT_5234() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_5262_5272_split[5]), &(SplitJoin10_CombineDFT_Fiss_5262_5272_join[5]));
	ENDFOR
}

void CombineDFT_5235() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_5262_5272_split[6]), &(SplitJoin10_CombineDFT_Fiss_5262_5272_join[6]));
	ENDFOR
}

void CombineDFT_5236() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_5262_5272_split[7]), &(SplitJoin10_CombineDFT_Fiss_5262_5272_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5227() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin10_CombineDFT_Fiss_5262_5272_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5218WEIGHTED_ROUND_ROBIN_Splitter_5227));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5228() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5228WEIGHTED_ROUND_ROBIN_Splitter_5237, pop_complex(&SplitJoin10_CombineDFT_Fiss_5262_5272_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5239() {
	CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5263_5273_split[0]), &(SplitJoin12_CombineDFT_Fiss_5263_5273_join[0]));
}

void CombineDFT_5240() {
	CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5263_5273_split[1]), &(SplitJoin12_CombineDFT_Fiss_5263_5273_join[1]));
}

void CombineDFT_5241() {
	CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5263_5273_split[2]), &(SplitJoin12_CombineDFT_Fiss_5263_5273_join[2]));
}

void CombineDFT_5242() {
	CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5263_5273_split[3]), &(SplitJoin12_CombineDFT_Fiss_5263_5273_join[3]));
}

void CombineDFT_5243() {
	CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5263_5273_split[4]), &(SplitJoin12_CombineDFT_Fiss_5263_5273_join[4]));
}

void CombineDFT_5244() {
	CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5263_5273_split[5]), &(SplitJoin12_CombineDFT_Fiss_5263_5273_join[5]));
}

void CombineDFT_5245() {
	CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5263_5273_split[6]), &(SplitJoin12_CombineDFT_Fiss_5263_5273_join[6]));
}

void CombineDFT_5246() {
	CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5263_5273_split[7]), &(SplitJoin12_CombineDFT_Fiss_5263_5273_join[7]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_5237() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_complex(&SplitJoin12_CombineDFT_Fiss_5263_5273_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5228WEIGHTED_ROUND_ROBIN_Splitter_5237));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_5238() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5238WEIGHTED_ROUND_ROBIN_Splitter_5247, pop_complex(&SplitJoin12_CombineDFT_Fiss_5263_5273_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void CombineDFT_5249() {
	CombineDFT(&(SplitJoin14_CombineDFT_Fiss_5264_5274_split[0]), &(SplitJoin14_CombineDFT_Fiss_5264_5274_join[0]));
}

void CombineDFT_5250() {
	CombineDFT(&(SplitJoin14_CombineDFT_Fiss_5264_5274_split[1]), &(SplitJoin14_CombineDFT_Fiss_5264_5274_join[1]));
}

void CombineDFT_5251() {
	CombineDFT(&(SplitJoin14_CombineDFT_Fiss_5264_5274_split[2]), &(SplitJoin14_CombineDFT_Fiss_5264_5274_join[2]));
}

void CombineDFT_5252() {
	CombineDFT(&(SplitJoin14_CombineDFT_Fiss_5264_5274_split[3]), &(SplitJoin14_CombineDFT_Fiss_5264_5274_join[3]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_5247() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
			push_complex(&SplitJoin14_CombineDFT_Fiss_5264_5274_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5238WEIGHTED_ROUND_ROBIN_Splitter_5247));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_5248() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5248WEIGHTED_ROUND_ROBIN_Splitter_5253, pop_complex(&SplitJoin14_CombineDFT_Fiss_5264_5274_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void CombineDFT_5255() {
	CombineDFT(&(SplitJoin16_CombineDFT_Fiss_5265_5275_split[0]), &(SplitJoin16_CombineDFT_Fiss_5265_5275_join[0]));
}

void CombineDFT_5256() {
	CombineDFT(&(SplitJoin16_CombineDFT_Fiss_5265_5275_split[1]), &(SplitJoin16_CombineDFT_Fiss_5265_5275_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_5253() {
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&SplitJoin16_CombineDFT_Fiss_5265_5275_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5248WEIGHTED_ROUND_ROBIN_Splitter_5253));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&SplitJoin16_CombineDFT_Fiss_5265_5275_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5248WEIGHTED_ROUND_ROBIN_Splitter_5253));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_5254() {
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5254CombineDFT_5184, pop_complex(&SplitJoin16_CombineDFT_Fiss_5265_5275_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5254CombineDFT_5184, pop_complex(&SplitJoin16_CombineDFT_Fiss_5265_5275_join[1]));
	ENDFOR
}

void CombineDFT_5184() {
	CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_5254CombineDFT_5184), &(CombineDFT_5184CPrinter_5185));
}

void CPrinter(buffer_complex_t *chanin) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		printf("%.10f", c.real);
		printf("\n");
		printf("%.10f", c.imag);
		printf("\n");
	}


void CPrinter_5185() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		CPrinter(&(CombineDFT_5184CPrinter_5185));
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5248WEIGHTED_ROUND_ROBIN_Splitter_5253);
	init_buffer_complex(&FFTTestSource_5173FFTReorderSimple_5174);
	FOR(int, __iter_init_0_, 0, <, 4, __iter_init_0_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_5258_5268_split[__iter_init_0_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5238WEIGHTED_ROUND_ROBIN_Splitter_5247);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5208WEIGHTED_ROUND_ROBIN_Splitter_5217);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5218WEIGHTED_ROUND_ROBIN_Splitter_5227);
	FOR(int, __iter_init_1_, 0, <, 2, __iter_init_1_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_5257_5267_split[__iter_init_1_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5228WEIGHTED_ROUND_ROBIN_Splitter_5237);
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_5260_5270_split[__iter_init_2_]);
	ENDFOR
	init_buffer_complex(&FFTReorderSimple_5174WEIGHTED_ROUND_ROBIN_Splitter_5187);
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_5262_5272_join[__iter_init_3_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5192WEIGHTED_ROUND_ROBIN_Splitter_5197);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5198WEIGHTED_ROUND_ROBIN_Splitter_5207);
	FOR(int, __iter_init_4_, 0, <, 4, __iter_init_4_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_5258_5268_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_5259_5269_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_5263_5273_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 4, __iter_init_7_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_5264_5274_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_5262_5272_split[__iter_init_8_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5188WEIGHTED_ROUND_ROBIN_Splitter_5191);
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_5263_5273_join[__iter_init_9_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5254CombineDFT_5184);
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_5261_5271_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 4, __iter_init_11_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_5264_5274_split[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 2, __iter_init_12_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_5265_5275_split[__iter_init_12_]);
	ENDFOR
	init_buffer_complex(&CombineDFT_5184CPrinter_5185);
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_5257_5267_join[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 8, __iter_init_14_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_5259_5269_join[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 8, __iter_init_15_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_5260_5270_join[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_5265_5275_join[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 8, __iter_init_17_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_5261_5271_join[__iter_init_17_]);
	ENDFOR
// --- init: CombineDFT_5219
	 {
	 ; 
	CombineDFT_5219_s.wn.real = -1.0 ; 
	CombineDFT_5219_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5220
	 {
	CombineDFT_5220_s.wn.real = -1.0 ; 
	CombineDFT_5220_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5221
	 {
	CombineDFT_5221_s.wn.real = -1.0 ; 
	CombineDFT_5221_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5222
	 {
	CombineDFT_5222_s.wn.real = -1.0 ; 
	CombineDFT_5222_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5223
	 {
	CombineDFT_5223_s.wn.real = -1.0 ; 
	CombineDFT_5223_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5224
	 {
	CombineDFT_5224_s.wn.real = -1.0 ; 
	CombineDFT_5224_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5225
	 {
	CombineDFT_5225_s.wn.real = -1.0 ; 
	CombineDFT_5225_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5226
	 {
	CombineDFT_5226_s.wn.real = -1.0 ; 
	CombineDFT_5226_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5229
	 {
	CombineDFT_5229_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5229_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5230
	 {
	CombineDFT_5230_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5230_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5231
	 {
	CombineDFT_5231_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5231_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5232
	 {
	CombineDFT_5232_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5232_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5233
	 {
	CombineDFT_5233_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5233_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5234
	 {
	CombineDFT_5234_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5234_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5235
	 {
	CombineDFT_5235_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5235_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5236
	 {
	CombineDFT_5236_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5236_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5239
	 {
	CombineDFT_5239_s.wn.real = 0.70710677 ; 
	CombineDFT_5239_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5240
	 {
	CombineDFT_5240_s.wn.real = 0.70710677 ; 
	CombineDFT_5240_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5241
	 {
	CombineDFT_5241_s.wn.real = 0.70710677 ; 
	CombineDFT_5241_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5242
	 {
	CombineDFT_5242_s.wn.real = 0.70710677 ; 
	CombineDFT_5242_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5243
	 {
	CombineDFT_5243_s.wn.real = 0.70710677 ; 
	CombineDFT_5243_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5244
	 {
	CombineDFT_5244_s.wn.real = 0.70710677 ; 
	CombineDFT_5244_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5245
	 {
	CombineDFT_5245_s.wn.real = 0.70710677 ; 
	CombineDFT_5245_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5246
	 {
	CombineDFT_5246_s.wn.real = 0.70710677 ; 
	CombineDFT_5246_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5249
	 {
	CombineDFT_5249_s.wn.real = 0.9238795 ; 
	CombineDFT_5249_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_5250
	 {
	CombineDFT_5250_s.wn.real = 0.9238795 ; 
	CombineDFT_5250_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_5251
	 {
	CombineDFT_5251_s.wn.real = 0.9238795 ; 
	CombineDFT_5251_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_5252
	 {
	CombineDFT_5252_s.wn.real = 0.9238795 ; 
	CombineDFT_5252_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_5255
	 {
	CombineDFT_5255_s.wn.real = 0.98078525 ; 
	CombineDFT_5255_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_5256
	 {
	CombineDFT_5256_s.wn.real = 0.98078525 ; 
	CombineDFT_5256_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_5184
	 {
	 ; 
	CombineDFT_5184_s.wn.real = 0.9951847 ; 
	CombineDFT_5184_s.wn.imag = -0.09801714 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FFTTestSource_5173();
		FFTReorderSimple_5174();
		WEIGHTED_ROUND_ROBIN_Splitter_5187();
			FFTReorderSimple_5189();
			FFTReorderSimple_5190();
		WEIGHTED_ROUND_ROBIN_Joiner_5188();
		WEIGHTED_ROUND_ROBIN_Splitter_5191();
			FFTReorderSimple_5193();
			FFTReorderSimple_5194();
			FFTReorderSimple_5195();
			FFTReorderSimple_5196();
		WEIGHTED_ROUND_ROBIN_Joiner_5192();
		WEIGHTED_ROUND_ROBIN_Splitter_5197();
			FFTReorderSimple_5199();
			FFTReorderSimple_5200();
			FFTReorderSimple_5201();
			FFTReorderSimple_5202();
			FFTReorderSimple_5203();
			FFTReorderSimple_5204();
			FFTReorderSimple_5205();
			FFTReorderSimple_5206();
		WEIGHTED_ROUND_ROBIN_Joiner_5198();
		WEIGHTED_ROUND_ROBIN_Splitter_5207();
			FFTReorderSimple_5209();
			FFTReorderSimple_5210();
			FFTReorderSimple_5211();
			FFTReorderSimple_5212();
			FFTReorderSimple_5213();
			FFTReorderSimple_5214();
			FFTReorderSimple_5215();
			FFTReorderSimple_5216();
		WEIGHTED_ROUND_ROBIN_Joiner_5208();
		WEIGHTED_ROUND_ROBIN_Splitter_5217();
			CombineDFT_5219();
			CombineDFT_5220();
			CombineDFT_5221();
			CombineDFT_5222();
			CombineDFT_5223();
			CombineDFT_5224();
			CombineDFT_5225();
			CombineDFT_5226();
		WEIGHTED_ROUND_ROBIN_Joiner_5218();
		WEIGHTED_ROUND_ROBIN_Splitter_5227();
			CombineDFT_5229();
			CombineDFT_5230();
			CombineDFT_5231();
			CombineDFT_5232();
			CombineDFT_5233();
			CombineDFT_5234();
			CombineDFT_5235();
			CombineDFT_5236();
		WEIGHTED_ROUND_ROBIN_Joiner_5228();
		WEIGHTED_ROUND_ROBIN_Splitter_5237();
			CombineDFT_5239();
			CombineDFT_5240();
			CombineDFT_5241();
			CombineDFT_5242();
			CombineDFT_5243();
			CombineDFT_5244();
			CombineDFT_5245();
			CombineDFT_5246();
		WEIGHTED_ROUND_ROBIN_Joiner_5238();
		WEIGHTED_ROUND_ROBIN_Splitter_5247();
			CombineDFT_5249();
			CombineDFT_5250();
			CombineDFT_5251();
			CombineDFT_5252();
		WEIGHTED_ROUND_ROBIN_Joiner_5248();
		WEIGHTED_ROUND_ROBIN_Splitter_5253();
			CombineDFT_5255();
			CombineDFT_5256();
		WEIGHTED_ROUND_ROBIN_Joiner_5254();
		CombineDFT_5184();
		CPrinter_5185();
	ENDFOR
	return EXIT_SUCCESS;
}
