#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=6656 on the compile command line
#else
#if BUF_SIZEMAX < 6656
#error BUF_SIZEMAX too small, it must be at least 6656
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float w[2];
} CombineDFT_3181_t;

typedef struct {
	float w[4];
} CombineDFT_3209_t;

typedef struct {
	float w[8];
} CombineDFT_3227_t;

typedef struct {
	float w[16];
} CombineDFT_3237_t;

typedef struct {
	float w[32];
} CombineDFT_3243_t;

typedef struct {
	float w[64];
} CombineDFT_3116_t;
void WEIGHTED_ROUND_ROBIN_Splitter_3137();
void FFTTestSource_3139();
void FFTTestSource_3140();
void WEIGHTED_ROUND_ROBIN_Joiner_3138();
void WEIGHTED_ROUND_ROBIN_Splitter_3129();
void FFTReorderSimple_3106();
void WEIGHTED_ROUND_ROBIN_Splitter_3141();
void FFTReorderSimple_3143();
void FFTReorderSimple_3144();
void WEIGHTED_ROUND_ROBIN_Joiner_3142();
void WEIGHTED_ROUND_ROBIN_Splitter_3145();
void FFTReorderSimple_3147();
void FFTReorderSimple_3148();
void FFTReorderSimple_3149();
void FFTReorderSimple_3150();
void WEIGHTED_ROUND_ROBIN_Joiner_3146();
void WEIGHTED_ROUND_ROBIN_Splitter_3151();
void FFTReorderSimple_3153();
void FFTReorderSimple_3154();
void FFTReorderSimple_3155();
void FFTReorderSimple_3156();
void FFTReorderSimple_3157();
void FFTReorderSimple_3158();
void FFTReorderSimple_3159();
void FFTReorderSimple_3160();
void WEIGHTED_ROUND_ROBIN_Joiner_3152();
void WEIGHTED_ROUND_ROBIN_Splitter_3161();
void FFTReorderSimple_3163();
void FFTReorderSimple_3164();
void FFTReorderSimple_3165();
void FFTReorderSimple_3166();
void FFTReorderSimple_3167();
void FFTReorderSimple_3168();
void FFTReorderSimple_3169();
void FFTReorderSimple_3170();
void FFTReorderSimple_3171();
void FFTReorderSimple_3172();
void FFTReorderSimple_3173();
void FFTReorderSimple_3174();
void FFTReorderSimple_3175();
void FFTReorderSimple_3176();
void FFTReorderSimple_3177();
void FFTReorderSimple_3178();
void WEIGHTED_ROUND_ROBIN_Joiner_3162();
void WEIGHTED_ROUND_ROBIN_Splitter_3179();
void CombineDFT_3181();
void CombineDFT_3182();
void CombineDFT_3183();
void CombineDFT_3184();
void CombineDFT_3185();
void CombineDFT_3186();
void CombineDFT_3187();
void CombineDFT_3188();
void CombineDFT_3189();
void CombineDFT_3190();
void CombineDFT_3191();
void CombineDFT_3192();
void CombineDFT_3193();
void CombineDFT_3194();
void CombineDFT_3195();
void CombineDFT_3196();
void CombineDFT_3197();
void CombineDFT_3198();
void CombineDFT_3199();
void CombineDFT_3200();
void CombineDFT_3201();
void CombineDFT_3202();
void CombineDFT_3203();
void CombineDFT_3204();
void CombineDFT_3205();
void CombineDFT_3206();
void WEIGHTED_ROUND_ROBIN_Joiner_3180();
void WEIGHTED_ROUND_ROBIN_Splitter_3207();
void CombineDFT_3209();
void CombineDFT_3210();
void CombineDFT_3211();
void CombineDFT_3212();
void CombineDFT_3213();
void CombineDFT_3214();
void CombineDFT_3215();
void CombineDFT_3216();
void CombineDFT_3217();
void CombineDFT_3218();
void CombineDFT_3219();
void CombineDFT_3220();
void CombineDFT_3221();
void CombineDFT_3222();
void CombineDFT_3223();
void CombineDFT_3224();
void WEIGHTED_ROUND_ROBIN_Joiner_3208();
void WEIGHTED_ROUND_ROBIN_Splitter_3225();
void CombineDFT_3227();
void CombineDFT_3228();
void CombineDFT_3229();
void CombineDFT_3230();
void CombineDFT_3231();
void CombineDFT_3232();
void CombineDFT_3233();
void CombineDFT_3234();
void WEIGHTED_ROUND_ROBIN_Joiner_3226();
void WEIGHTED_ROUND_ROBIN_Splitter_3235();
void CombineDFT_3237();
void CombineDFT_3238();
void CombineDFT_3239();
void CombineDFT_3240();
void WEIGHTED_ROUND_ROBIN_Joiner_3236();
void WEIGHTED_ROUND_ROBIN_Splitter_3241();
void CombineDFT_3243();
void CombineDFT_3244();
void WEIGHTED_ROUND_ROBIN_Joiner_3242();
void CombineDFT_3116();
void FFTReorderSimple_3117();
void WEIGHTED_ROUND_ROBIN_Splitter_3245();
void FFTReorderSimple_3247();
void FFTReorderSimple_3248();
void WEIGHTED_ROUND_ROBIN_Joiner_3246();
void WEIGHTED_ROUND_ROBIN_Splitter_3249();
void FFTReorderSimple_3251();
void FFTReorderSimple_3252();
void FFTReorderSimple_3253();
void FFTReorderSimple_3254();
void WEIGHTED_ROUND_ROBIN_Joiner_3250();
void WEIGHTED_ROUND_ROBIN_Splitter_3255();
void FFTReorderSimple_3257();
void FFTReorderSimple_3258();
void FFTReorderSimple_3259();
void FFTReorderSimple_3260();
void FFTReorderSimple_3261();
void FFTReorderSimple_3262();
void FFTReorderSimple_3263();
void FFTReorderSimple_3264();
void WEIGHTED_ROUND_ROBIN_Joiner_3256();
void WEIGHTED_ROUND_ROBIN_Splitter_3265();
void FFTReorderSimple_3267();
void FFTReorderSimple_3268();
void FFTReorderSimple_3269();
void FFTReorderSimple_3270();
void FFTReorderSimple_3271();
void FFTReorderSimple_3272();
void FFTReorderSimple_3273();
void FFTReorderSimple_3274();
void FFTReorderSimple_3275();
void FFTReorderSimple_3276();
void FFTReorderSimple_3277();
void FFTReorderSimple_3278();
void FFTReorderSimple_3279();
void FFTReorderSimple_3280();
void FFTReorderSimple_3281();
void FFTReorderSimple_3282();
void WEIGHTED_ROUND_ROBIN_Joiner_3266();
void WEIGHTED_ROUND_ROBIN_Splitter_3283();
void CombineDFT_3285();
void CombineDFT_3286();
void CombineDFT_3287();
void CombineDFT_3288();
void CombineDFT_3289();
void CombineDFT_3290();
void CombineDFT_3291();
void CombineDFT_3292();
void CombineDFT_3293();
void CombineDFT_3294();
void CombineDFT_3295();
void CombineDFT_3296();
void CombineDFT_3297();
void CombineDFT_3298();
void CombineDFT_3299();
void CombineDFT_3300();
void CombineDFT_3301();
void CombineDFT_3302();
void CombineDFT_3303();
void CombineDFT_3304();
void CombineDFT_3305();
void CombineDFT_3306();
void CombineDFT_3307();
void CombineDFT_3308();
void CombineDFT_3309();
void CombineDFT_3310();
void WEIGHTED_ROUND_ROBIN_Joiner_3284();
void WEIGHTED_ROUND_ROBIN_Splitter_3311();
void CombineDFT_3313();
void CombineDFT_3314();
void CombineDFT_3315();
void CombineDFT_3316();
void CombineDFT_3317();
void CombineDFT_3318();
void CombineDFT_3319();
void CombineDFT_3320();
void CombineDFT_3321();
void CombineDFT_3322();
void CombineDFT_3323();
void CombineDFT_3324();
void CombineDFT_3325();
void CombineDFT_3326();
void CombineDFT_3327();
void CombineDFT_3328();
void WEIGHTED_ROUND_ROBIN_Joiner_3312();
void WEIGHTED_ROUND_ROBIN_Splitter_3329();
void CombineDFT_3331();
void CombineDFT_3332();
void CombineDFT_3333();
void CombineDFT_3334();
void CombineDFT_3335();
void CombineDFT_3336();
void CombineDFT_3337();
void CombineDFT_3338();
void WEIGHTED_ROUND_ROBIN_Joiner_3330();
void WEIGHTED_ROUND_ROBIN_Splitter_3339();
void CombineDFT_3341();
void CombineDFT_3342();
void CombineDFT_3343();
void CombineDFT_3344();
void WEIGHTED_ROUND_ROBIN_Joiner_3340();
void WEIGHTED_ROUND_ROBIN_Splitter_3345();
void CombineDFT_3347();
void CombineDFT_3348();
void WEIGHTED_ROUND_ROBIN_Joiner_3346();
void CombineDFT_3127();
void WEIGHTED_ROUND_ROBIN_Joiner_3130();
void FloatPrinter_3128();

#ifdef __cplusplus
}
#endif
#endif
