#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=11776 on the compile command line
#else
#if BUF_SIZEMAX < 11776
#error BUF_SIZEMAX too small, it must be at least 11776
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float w[2];
} CombineDFT_4594_t;

typedef struct {
	float w[4];
} CombineDFT_4619_t;

typedef struct {
	float w[8];
} CombineDFT_4637_t;

typedef struct {
	float w[16];
} CombineDFT_4647_t;

typedef struct {
	float w[32];
} CombineDFT_4653_t;

typedef struct {
	float w[64];
} CombineDFT_4529_t;
void WEIGHTED_ROUND_ROBIN_Splitter_4550();
void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout);
void FFTTestSource_4552();
void FFTTestSource_4553();
void WEIGHTED_ROUND_ROBIN_Joiner_4551();
void WEIGHTED_ROUND_ROBIN_Splitter_4542();
void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout);
void FFTReorderSimple_4519();
void WEIGHTED_ROUND_ROBIN_Splitter_4554();
void FFTReorderSimple_4556();
void FFTReorderSimple_4557();
void WEIGHTED_ROUND_ROBIN_Joiner_4555();
void WEIGHTED_ROUND_ROBIN_Splitter_4558();
void FFTReorderSimple_4560();
void FFTReorderSimple_4561();
void FFTReorderSimple_4562();
void FFTReorderSimple_4563();
void WEIGHTED_ROUND_ROBIN_Joiner_4559();
void WEIGHTED_ROUND_ROBIN_Splitter_4564();
void FFTReorderSimple_4566();
void FFTReorderSimple_4567();
void FFTReorderSimple_4568();
void FFTReorderSimple_4569();
void FFTReorderSimple_4570();
void FFTReorderSimple_4571();
void FFTReorderSimple_4572();
void FFTReorderSimple_4573();
void WEIGHTED_ROUND_ROBIN_Joiner_4565();
void WEIGHTED_ROUND_ROBIN_Splitter_4574();
void FFTReorderSimple_4576();
void FFTReorderSimple_4577();
void FFTReorderSimple_4578();
void FFTReorderSimple_4579();
void FFTReorderSimple_4580();
void FFTReorderSimple_4581();
void FFTReorderSimple_4582();
void FFTReorderSimple_4583();
void FFTReorderSimple_4584();
void FFTReorderSimple_4585();
void FFTReorderSimple_4586();
void FFTReorderSimple_4587();
void FFTReorderSimple_4588();
void FFTReorderSimple_4589();
void FFTReorderSimple_4590();
void FFTReorderSimple_4591();
void WEIGHTED_ROUND_ROBIN_Joiner_4575();
void WEIGHTED_ROUND_ROBIN_Splitter_4592();
void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout);
void CombineDFT_4594();
void CombineDFT_4595();
void CombineDFT_4596();
void CombineDFT_4597();
void CombineDFT_4598();
void CombineDFT_4599();
void CombineDFT_4600();
void CombineDFT_4601();
void CombineDFT_4602();
void CombineDFT_4603();
void CombineDFT_4604();
void CombineDFT_4605();
void CombineDFT_4606();
void CombineDFT_4607();
void CombineDFT_4608();
void CombineDFT_4609();
void CombineDFT_4610();
void CombineDFT_4611();
void CombineDFT_4612();
void CombineDFT_4613();
void CombineDFT_4614();
void CombineDFT_4615();
void CombineDFT_4616();
void WEIGHTED_ROUND_ROBIN_Joiner_4593();
void WEIGHTED_ROUND_ROBIN_Splitter_4617();
void CombineDFT_4619();
void CombineDFT_4620();
void CombineDFT_4621();
void CombineDFT_4622();
void CombineDFT_4623();
void CombineDFT_4624();
void CombineDFT_4625();
void CombineDFT_4626();
void CombineDFT_4627();
void CombineDFT_4628();
void CombineDFT_4629();
void CombineDFT_4630();
void CombineDFT_4631();
void CombineDFT_4632();
void CombineDFT_4633();
void CombineDFT_4634();
void WEIGHTED_ROUND_ROBIN_Joiner_4618();
void WEIGHTED_ROUND_ROBIN_Splitter_4635();
void CombineDFT_4637();
void CombineDFT_4638();
void CombineDFT_4639();
void CombineDFT_4640();
void CombineDFT_4641();
void CombineDFT_4642();
void CombineDFT_4643();
void CombineDFT_4644();
void WEIGHTED_ROUND_ROBIN_Joiner_4636();
void WEIGHTED_ROUND_ROBIN_Splitter_4645();
void CombineDFT_4647();
void CombineDFT_4648();
void CombineDFT_4649();
void CombineDFT_4650();
void WEIGHTED_ROUND_ROBIN_Joiner_4646();
void WEIGHTED_ROUND_ROBIN_Splitter_4651();
void CombineDFT_4653();
void CombineDFT_4654();
void WEIGHTED_ROUND_ROBIN_Joiner_4652();
void CombineDFT_4529();
void FFTReorderSimple_4530();
void WEIGHTED_ROUND_ROBIN_Splitter_4655();
void FFTReorderSimple_4657();
void FFTReorderSimple_4658();
void WEIGHTED_ROUND_ROBIN_Joiner_4656();
void WEIGHTED_ROUND_ROBIN_Splitter_4659();
void FFTReorderSimple_4661();
void FFTReorderSimple_4662();
void FFTReorderSimple_4663();
void FFTReorderSimple_4664();
void WEIGHTED_ROUND_ROBIN_Joiner_4660();
void WEIGHTED_ROUND_ROBIN_Splitter_4665();
void FFTReorderSimple_4667();
void FFTReorderSimple_4668();
void FFTReorderSimple_4669();
void FFTReorderSimple_4670();
void FFTReorderSimple_4671();
void FFTReorderSimple_4672();
void FFTReorderSimple_4673();
void FFTReorderSimple_4674();
void WEIGHTED_ROUND_ROBIN_Joiner_4666();
void WEIGHTED_ROUND_ROBIN_Splitter_4675();
void FFTReorderSimple_4677();
void FFTReorderSimple_4678();
void FFTReorderSimple_4679();
void FFTReorderSimple_4680();
void FFTReorderSimple_4681();
void FFTReorderSimple_4682();
void FFTReorderSimple_4683();
void FFTReorderSimple_4684();
void FFTReorderSimple_4685();
void FFTReorderSimple_4686();
void FFTReorderSimple_4687();
void FFTReorderSimple_4688();
void FFTReorderSimple_4689();
void FFTReorderSimple_4690();
void FFTReorderSimple_4691();
void FFTReorderSimple_4692();
void WEIGHTED_ROUND_ROBIN_Joiner_4676();
void WEIGHTED_ROUND_ROBIN_Splitter_4693();
void CombineDFT_4695();
void CombineDFT_4696();
void CombineDFT_4697();
void CombineDFT_4698();
void CombineDFT_4699();
void CombineDFT_4700();
void CombineDFT_4701();
void CombineDFT_4702();
void CombineDFT_4703();
void CombineDFT_4704();
void CombineDFT_4705();
void CombineDFT_4706();
void CombineDFT_4707();
void CombineDFT_4708();
void CombineDFT_4709();
void CombineDFT_4710();
void CombineDFT_4711();
void CombineDFT_4712();
void CombineDFT_4713();
void CombineDFT_4714();
void CombineDFT_4715();
void CombineDFT_4716();
void CombineDFT_4717();
void WEIGHTED_ROUND_ROBIN_Joiner_4694();
void WEIGHTED_ROUND_ROBIN_Splitter_4718();
void CombineDFT_4720();
void CombineDFT_4721();
void CombineDFT_4722();
void CombineDFT_4723();
void CombineDFT_4724();
void CombineDFT_4725();
void CombineDFT_4726();
void CombineDFT_4727();
void CombineDFT_4728();
void CombineDFT_4729();
void CombineDFT_4730();
void CombineDFT_4731();
void CombineDFT_4732();
void CombineDFT_4733();
void CombineDFT_4734();
void CombineDFT_4735();
void WEIGHTED_ROUND_ROBIN_Joiner_4719();
void WEIGHTED_ROUND_ROBIN_Splitter_4736();
void CombineDFT_4738();
void CombineDFT_4739();
void CombineDFT_4740();
void CombineDFT_4741();
void CombineDFT_4742();
void CombineDFT_4743();
void CombineDFT_4744();
void CombineDFT_4745();
void WEIGHTED_ROUND_ROBIN_Joiner_4737();
void WEIGHTED_ROUND_ROBIN_Splitter_4746();
void CombineDFT_4748();
void CombineDFT_4749();
void CombineDFT_4750();
void CombineDFT_4751();
void WEIGHTED_ROUND_ROBIN_Joiner_4747();
void WEIGHTED_ROUND_ROBIN_Splitter_4752();
void CombineDFT_4754();
void CombineDFT_4755();
void WEIGHTED_ROUND_ROBIN_Joiner_4753();
void CombineDFT_4540();
void WEIGHTED_ROUND_ROBIN_Joiner_4543();
void FloatPrinter(buffer_float_t *chanin);
void FloatPrinter_4541();

#ifdef __cplusplus
}
#endif
#endif
