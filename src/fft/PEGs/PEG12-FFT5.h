#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=96 on the compile command line
#else
#if BUF_SIZEMAX < 96
#error BUF_SIZEMAX too small, it must be at least 96
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif


void WEIGHTED_ROUND_ROBIN_Splitter_4196();
void source(buffer_void_t *chanin, buffer_complex_t *chanout);
void source_4198();
void source_4199();
void WEIGHTED_ROUND_ROBIN_Joiner_4197();
void WEIGHTED_ROUND_ROBIN_Splitter_4039();
void WEIGHTED_ROUND_ROBIN_Splitter_4041();
void WEIGHTED_ROUND_ROBIN_Splitter_4043();
void WEIGHTED_ROUND_ROBIN_Splitter_4045();
void Identity(buffer_complex_t *chanin, buffer_complex_t *chanout);
void Identity_3869();
void Identity_3871();
void WEIGHTED_ROUND_ROBIN_Joiner_4046();
void WEIGHTED_ROUND_ROBIN_Splitter_4047();
void Identity_3875();
void Identity_3877();
void WEIGHTED_ROUND_ROBIN_Joiner_4048();
void WEIGHTED_ROUND_ROBIN_Joiner_4044();
void WEIGHTED_ROUND_ROBIN_Splitter_4049();
void WEIGHTED_ROUND_ROBIN_Splitter_4051();
void Identity_3883();
void Identity_3885();
void WEIGHTED_ROUND_ROBIN_Joiner_4052();
void WEIGHTED_ROUND_ROBIN_Splitter_4053();
void Identity_3889();
void Identity_3891();
void WEIGHTED_ROUND_ROBIN_Joiner_4054();
void WEIGHTED_ROUND_ROBIN_Joiner_4050();
void WEIGHTED_ROUND_ROBIN_Joiner_4042();
void WEIGHTED_ROUND_ROBIN_Splitter_4055();
void WEIGHTED_ROUND_ROBIN_Splitter_4057();
void WEIGHTED_ROUND_ROBIN_Splitter_4059();
void Identity_3899();
void Identity_3901();
void WEIGHTED_ROUND_ROBIN_Joiner_4060();
void WEIGHTED_ROUND_ROBIN_Splitter_4061();
void Identity_3905();
void Identity_3907();
void WEIGHTED_ROUND_ROBIN_Joiner_4062();
void WEIGHTED_ROUND_ROBIN_Joiner_4058();
void WEIGHTED_ROUND_ROBIN_Splitter_4063();
void WEIGHTED_ROUND_ROBIN_Splitter_4065();
void Identity_3913();
void Identity_3915();
void WEIGHTED_ROUND_ROBIN_Joiner_4066();
void WEIGHTED_ROUND_ROBIN_Splitter_4067();
void Identity_3919();
void Identity_3921();
void WEIGHTED_ROUND_ROBIN_Joiner_4068();
void WEIGHTED_ROUND_ROBIN_Joiner_4064();
void WEIGHTED_ROUND_ROBIN_Joiner_4056();
void WEIGHTED_ROUND_ROBIN_Joiner_4040();
void WEIGHTED_ROUND_ROBIN_Splitter_4183();
void WEIGHTED_ROUND_ROBIN_Splitter_4184();
void Pre_CollapsedDataParallel_1(buffer_complex_t *chanin, buffer_complex_t *chanout);
void Pre_CollapsedDataParallel_1_4016();
void butterfly(buffer_complex_t *chanin, buffer_complex_t *chanout);
void butterfly_3923();
void Post_CollapsedDataParallel_2(buffer_complex_t *chanin, buffer_complex_t *chanout);
void Post_CollapsedDataParallel_2_4017();
void Pre_CollapsedDataParallel_1_4019();
void butterfly_3924();
void Post_CollapsedDataParallel_2_4020();
void Pre_CollapsedDataParallel_1_4022();
void butterfly_3925();
void Post_CollapsedDataParallel_2_4023();
void Pre_CollapsedDataParallel_1_4025();
void butterfly_3926();
void Post_CollapsedDataParallel_2_4026();
void WEIGHTED_ROUND_ROBIN_Joiner_4185();
void WEIGHTED_ROUND_ROBIN_Splitter_4186();
void Pre_CollapsedDataParallel_1_4028();
void butterfly_3927();
void Post_CollapsedDataParallel_2_4029();
void Pre_CollapsedDataParallel_1_4031();
void butterfly_3928();
void Post_CollapsedDataParallel_2_4032();
void Pre_CollapsedDataParallel_1_4034();
void butterfly_3929();
void Post_CollapsedDataParallel_2_4035();
void Pre_CollapsedDataParallel_1_4037();
void butterfly_3930();
void Post_CollapsedDataParallel_2_4038();
void WEIGHTED_ROUND_ROBIN_Joiner_4187();
void WEIGHTED_ROUND_ROBIN_Joiner_4188();
void WEIGHTED_ROUND_ROBIN_Splitter_4189();
void WEIGHTED_ROUND_ROBIN_Splitter_4190();
void WEIGHTED_ROUND_ROBIN_Splitter_4073();
void butterfly_3932();
void butterfly_3933();
void WEIGHTED_ROUND_ROBIN_Joiner_4074();
void WEIGHTED_ROUND_ROBIN_Splitter_4075();
void butterfly_3934();
void butterfly_3935();
void WEIGHTED_ROUND_ROBIN_Joiner_4076();
void WEIGHTED_ROUND_ROBIN_Joiner_4191();
void WEIGHTED_ROUND_ROBIN_Splitter_4192();
void WEIGHTED_ROUND_ROBIN_Splitter_4077();
void butterfly_3936();
void butterfly_3937();
void WEIGHTED_ROUND_ROBIN_Joiner_4078();
void WEIGHTED_ROUND_ROBIN_Splitter_4079();
void butterfly_3938();
void butterfly_3939();
void WEIGHTED_ROUND_ROBIN_Joiner_4080();
void WEIGHTED_ROUND_ROBIN_Joiner_4193();
void WEIGHTED_ROUND_ROBIN_Joiner_4194();
void WEIGHTED_ROUND_ROBIN_Splitter_4081();
void WEIGHTED_ROUND_ROBIN_Splitter_4083();
void butterfly_3941();
void butterfly_3942();
void butterfly_3943();
void butterfly_3944();
void WEIGHTED_ROUND_ROBIN_Joiner_4084();
void WEIGHTED_ROUND_ROBIN_Splitter_4085();
void butterfly_3945();
void butterfly_3946();
void butterfly_3947();
void butterfly_3948();
void WEIGHTED_ROUND_ROBIN_Joiner_4086();
void WEIGHTED_ROUND_ROBIN_Joiner_4082();
void WEIGHTED_ROUND_ROBIN_Splitter_4200();
void magnitude(buffer_complex_t *chanin, buffer_float_t *chanout);
void magnitude_4202();
void magnitude_4203();
void magnitude_4204();
void magnitude_4205();
void magnitude_4206();
void magnitude_4207();
void magnitude_4208();
void magnitude_4209();
void magnitude_4210();
void magnitude_4211();
void magnitude_4212();
void magnitude_4213();
void WEIGHTED_ROUND_ROBIN_Joiner_4201();
void sink(buffer_float_t *chanin);
void sink_3950();

#ifdef __cplusplus
}
#endif
#endif
