#include "PEG15-FFT6_nocache.h"

buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3971WEIGHTED_ROUND_ROBIN_Splitter_3980;
buffer_complex_t SplitJoin16_CombineDFT_Fiss_3998_4008_split[2];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_3992_4002_split[8];
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_3991_4001_split[4];
buffer_complex_t SplitJoin12_CombineDFT_Fiss_3996_4006_split[8];
buffer_complex_t SplitJoin12_CombineDFT_Fiss_3996_4006_join[8];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_3990_4000_split[2];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_3995_4005_split[15];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3987CombineDFT_3896;
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_3992_4002_join[8];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_3995_4005_join[15];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3910WEIGHTED_ROUND_ROBIN_Splitter_3919;
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[15];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_3990_4000_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3900WEIGHTED_ROUND_ROBIN_Splitter_3903;
buffer_complex_t SplitJoin16_CombineDFT_Fiss_3998_4008_join[2];
buffer_complex_t SplitJoin8_CombineDFT_Fiss_3994_4004_join[15];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3954WEIGHTED_ROUND_ROBIN_Splitter_3970;
buffer_complex_t SplitJoin14_CombineDFT_Fiss_3997_4007_join[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3981WEIGHTED_ROUND_ROBIN_Splitter_3986;
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[15];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3937WEIGHTED_ROUND_ROBIN_Splitter_3953;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3904WEIGHTED_ROUND_ROBIN_Splitter_3909;
buffer_complex_t CombineDFT_3896CPrinter_3897;
buffer_complex_t FFTReorderSimple_3886WEIGHTED_ROUND_ROBIN_Splitter_3899;
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_3991_4001_join[4];
buffer_complex_t FFTTestSource_3885FFTReorderSimple_3886;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3920WEIGHTED_ROUND_ROBIN_Splitter_3936;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_3994_4004_split[15];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_3997_4007_split[4];


CombineDFT_3938_t CombineDFT_3938_s;
CombineDFT_3938_t CombineDFT_3939_s;
CombineDFT_3938_t CombineDFT_3940_s;
CombineDFT_3938_t CombineDFT_3941_s;
CombineDFT_3938_t CombineDFT_3942_s;
CombineDFT_3938_t CombineDFT_3943_s;
CombineDFT_3938_t CombineDFT_3944_s;
CombineDFT_3938_t CombineDFT_3945_s;
CombineDFT_3938_t CombineDFT_3946_s;
CombineDFT_3938_t CombineDFT_3947_s;
CombineDFT_3938_t CombineDFT_3948_s;
CombineDFT_3938_t CombineDFT_3949_s;
CombineDFT_3938_t CombineDFT_3950_s;
CombineDFT_3938_t CombineDFT_3951_s;
CombineDFT_3938_t CombineDFT_3952_s;
CombineDFT_3938_t CombineDFT_3955_s;
CombineDFT_3938_t CombineDFT_3956_s;
CombineDFT_3938_t CombineDFT_3957_s;
CombineDFT_3938_t CombineDFT_3958_s;
CombineDFT_3938_t CombineDFT_3959_s;
CombineDFT_3938_t CombineDFT_3960_s;
CombineDFT_3938_t CombineDFT_3961_s;
CombineDFT_3938_t CombineDFT_3962_s;
CombineDFT_3938_t CombineDFT_3963_s;
CombineDFT_3938_t CombineDFT_3964_s;
CombineDFT_3938_t CombineDFT_3965_s;
CombineDFT_3938_t CombineDFT_3966_s;
CombineDFT_3938_t CombineDFT_3967_s;
CombineDFT_3938_t CombineDFT_3968_s;
CombineDFT_3938_t CombineDFT_3969_s;
CombineDFT_3938_t CombineDFT_3972_s;
CombineDFT_3938_t CombineDFT_3973_s;
CombineDFT_3938_t CombineDFT_3974_s;
CombineDFT_3938_t CombineDFT_3975_s;
CombineDFT_3938_t CombineDFT_3976_s;
CombineDFT_3938_t CombineDFT_3977_s;
CombineDFT_3938_t CombineDFT_3978_s;
CombineDFT_3938_t CombineDFT_3979_s;
CombineDFT_3938_t CombineDFT_3982_s;
CombineDFT_3938_t CombineDFT_3983_s;
CombineDFT_3938_t CombineDFT_3984_s;
CombineDFT_3938_t CombineDFT_3985_s;
CombineDFT_3938_t CombineDFT_3988_s;
CombineDFT_3938_t CombineDFT_3989_s;
CombineDFT_3938_t CombineDFT_3896_s;

void FFTTestSource_3885(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		complex_t c1;
		complex_t zero;
		c1.real = 1.0 ; 
		c1.imag = 0.0 ; 
		zero.real = 0.0 ; 
		zero.imag = 0.0 ; 
		push_complex(&FFTTestSource_3885FFTReorderSimple_3886, zero) ; 
		push_complex(&FFTTestSource_3885FFTReorderSimple_3886, c1) ; 
		FOR(int, i, 0,  < , 62, i++) {
			push_complex(&FFTTestSource_3885FFTReorderSimple_3886, zero) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3886(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&FFTTestSource_3885FFTReorderSimple_3886, i)) ; 
			push_complex(&FFTReorderSimple_3886WEIGHTED_ROUND_ROBIN_Splitter_3899, __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 64, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&FFTTestSource_3885FFTReorderSimple_3886, i)) ; 
			push_complex(&FFTReorderSimple_3886WEIGHTED_ROUND_ROBIN_Splitter_3899, __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&FFTTestSource_3885FFTReorderSimple_3886) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3901(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_3990_4000_split[0], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_3990_4000_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 32, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_3990_4000_split[0], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_3990_4000_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_3990_4000_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3902(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_3990_4000_split[1], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_3990_4000_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 32, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_3990_4000_split[1], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_3990_4000_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_3990_4000_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3899() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_3990_4000_split[0], pop_complex(&FFTReorderSimple_3886WEIGHTED_ROUND_ROBIN_Splitter_3899));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_3990_4000_split[1], pop_complex(&FFTReorderSimple_3886WEIGHTED_ROUND_ROBIN_Splitter_3899));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3900() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3900WEIGHTED_ROUND_ROBIN_Splitter_3903, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_3990_4000_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3900WEIGHTED_ROUND_ROBIN_Splitter_3903, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_3990_4000_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_3905(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_3991_4001_split[0], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_3991_4001_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_3991_4001_split[0], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_3991_4001_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_3991_4001_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3906(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_3991_4001_split[1], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_3991_4001_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_3991_4001_split[1], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_3991_4001_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_3991_4001_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3907(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_3991_4001_split[2], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_3991_4001_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_3991_4001_split[2], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_3991_4001_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_3991_4001_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3908(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_3991_4001_split[3], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_3991_4001_join[3], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_3991_4001_split[3], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_3991_4001_join[3], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_3991_4001_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3903() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin2_FFTReorderSimple_Fiss_3991_4001_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3900WEIGHTED_ROUND_ROBIN_Splitter_3903));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3904() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3904WEIGHTED_ROUND_ROBIN_Splitter_3909, pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_3991_4001_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_3911(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_split[0], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_split[0], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3912(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_split[1], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_split[1], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3913(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_split[2], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_split[2], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3914(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_split[3], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_join[3], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_split[3], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_join[3], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3915(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_split[4], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_join[4], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_split[4], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_join[4], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3916(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_split[5], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_join[5], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_split[5], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_join[5], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3917(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_split[6], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_join[6], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_split[6], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_join[6], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3918(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_split[7], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_join[7], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_split[7], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_join[7], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3909() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3904WEIGHTED_ROUND_ROBIN_Splitter_3909));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3910() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3910WEIGHTED_ROUND_ROBIN_Splitter_3919, pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_3921(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[0], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[0], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3922(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[1], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[1], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3923(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[2], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[2], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3924(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[3], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[3], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[3], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[3], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3925(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[4], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[4], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[4], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[4], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3926(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[5], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[5], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[5], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[5], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3927(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[6], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[6], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[6], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[6], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3928(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[7], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[7], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[7], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[7], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3929(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[8], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[8], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[8], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[8], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[8]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3930(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[9], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[9], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[9], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[9], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[9]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3931(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[10], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[10], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[10], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[10], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[10]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3932(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[11], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[11], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[11], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[11], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[11]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3933(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[12], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[12], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[12], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[12], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[12]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3934(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[13], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[13], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[13], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[13], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[13]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3935(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[14], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[14], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[14], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[14], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[14]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3919() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 15, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3910WEIGHTED_ROUND_ROBIN_Splitter_3919));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3920() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 15, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3920WEIGHTED_ROUND_ROBIN_Splitter_3936, pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_3938(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[0], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3938_s.wn.real) - (w.imag * CombineDFT_3938_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3938_s.wn.imag) + (w.imag * CombineDFT_3938_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[0]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3939(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[1], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3939_s.wn.real) - (w.imag * CombineDFT_3939_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3939_s.wn.imag) + (w.imag * CombineDFT_3939_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[1]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3940(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[2], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3940_s.wn.real) - (w.imag * CombineDFT_3940_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3940_s.wn.imag) + (w.imag * CombineDFT_3940_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[2]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3941(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[3], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3941_s.wn.real) - (w.imag * CombineDFT_3941_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3941_s.wn.imag) + (w.imag * CombineDFT_3941_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[3]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3942(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[4], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[4], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3942_s.wn.real) - (w.imag * CombineDFT_3942_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3942_s.wn.imag) + (w.imag * CombineDFT_3942_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[4]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3943(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[5], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[5], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3943_s.wn.real) - (w.imag * CombineDFT_3943_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3943_s.wn.imag) + (w.imag * CombineDFT_3943_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[5]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3944(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[6], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[6], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3944_s.wn.real) - (w.imag * CombineDFT_3944_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3944_s.wn.imag) + (w.imag * CombineDFT_3944_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[6]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3945(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[7], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[7], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3945_s.wn.real) - (w.imag * CombineDFT_3945_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3945_s.wn.imag) + (w.imag * CombineDFT_3945_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[7]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3946(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[8], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[8], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3946_s.wn.real) - (w.imag * CombineDFT_3946_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3946_s.wn.imag) + (w.imag * CombineDFT_3946_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[8]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_join[8], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3947(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[9], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[9], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3947_s.wn.real) - (w.imag * CombineDFT_3947_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3947_s.wn.imag) + (w.imag * CombineDFT_3947_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[9]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_join[9], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3948(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[10], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[10], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3948_s.wn.real) - (w.imag * CombineDFT_3948_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3948_s.wn.imag) + (w.imag * CombineDFT_3948_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[10]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_join[10], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3949(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[11], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[11], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3949_s.wn.real) - (w.imag * CombineDFT_3949_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3949_s.wn.imag) + (w.imag * CombineDFT_3949_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[11]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_join[11], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3950(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[12], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[12], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3950_s.wn.real) - (w.imag * CombineDFT_3950_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3950_s.wn.imag) + (w.imag * CombineDFT_3950_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[12]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_join[12], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3951(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[13], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[13], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3951_s.wn.real) - (w.imag * CombineDFT_3951_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3951_s.wn.imag) + (w.imag * CombineDFT_3951_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[13]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_join[13], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3952(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[14], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[14], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3952_s.wn.real) - (w.imag * CombineDFT_3952_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3952_s.wn.imag) + (w.imag * CombineDFT_3952_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[14]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_join[14], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3936() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 15, __iter_++)
			push_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3920WEIGHTED_ROUND_ROBIN_Splitter_3936));
			push_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3920WEIGHTED_ROUND_ROBIN_Splitter_3936));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3937() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 15, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3937WEIGHTED_ROUND_ROBIN_Splitter_3953, pop_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_join[__iter_]));
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3937WEIGHTED_ROUND_ROBIN_Splitter_3953, pop_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_3955(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[0], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3955_s.wn.real) - (w.imag * CombineDFT_3955_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3955_s.wn.imag) + (w.imag * CombineDFT_3955_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[0]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3956(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[1], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3956_s.wn.real) - (w.imag * CombineDFT_3956_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3956_s.wn.imag) + (w.imag * CombineDFT_3956_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[1]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3957(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[2], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3957_s.wn.real) - (w.imag * CombineDFT_3957_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3957_s.wn.imag) + (w.imag * CombineDFT_3957_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[2]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3958(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[3], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3958_s.wn.real) - (w.imag * CombineDFT_3958_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3958_s.wn.imag) + (w.imag * CombineDFT_3958_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[3]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3959(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[4], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[4], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3959_s.wn.real) - (w.imag * CombineDFT_3959_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3959_s.wn.imag) + (w.imag * CombineDFT_3959_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[4]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3960(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[5], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[5], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3960_s.wn.real) - (w.imag * CombineDFT_3960_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3960_s.wn.imag) + (w.imag * CombineDFT_3960_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[5]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3961(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[6], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[6], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3961_s.wn.real) - (w.imag * CombineDFT_3961_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3961_s.wn.imag) + (w.imag * CombineDFT_3961_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[6]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3962(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[7], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[7], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3962_s.wn.real) - (w.imag * CombineDFT_3962_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3962_s.wn.imag) + (w.imag * CombineDFT_3962_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[7]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3963(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[8], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[8], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3963_s.wn.real) - (w.imag * CombineDFT_3963_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3963_s.wn.imag) + (w.imag * CombineDFT_3963_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[8]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_join[8], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3964(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[9], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[9], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3964_s.wn.real) - (w.imag * CombineDFT_3964_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3964_s.wn.imag) + (w.imag * CombineDFT_3964_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[9]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_join[9], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3965(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[10], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[10], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3965_s.wn.real) - (w.imag * CombineDFT_3965_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3965_s.wn.imag) + (w.imag * CombineDFT_3965_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[10]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_join[10], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3966(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[11], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[11], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3966_s.wn.real) - (w.imag * CombineDFT_3966_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3966_s.wn.imag) + (w.imag * CombineDFT_3966_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[11]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_join[11], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3967(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[12], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[12], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3967_s.wn.real) - (w.imag * CombineDFT_3967_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3967_s.wn.imag) + (w.imag * CombineDFT_3967_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[12]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_join[12], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3968(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[13], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[13], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3968_s.wn.real) - (w.imag * CombineDFT_3968_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3968_s.wn.imag) + (w.imag * CombineDFT_3968_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[13]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_join[13], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3969(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[14], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[14], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3969_s.wn.real) - (w.imag * CombineDFT_3969_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3969_s.wn.imag) + (w.imag * CombineDFT_3969_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[14]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_join[14], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3953() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 15, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3937WEIGHTED_ROUND_ROBIN_Splitter_3953));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3954() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 15, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3954WEIGHTED_ROUND_ROBIN_Splitter_3970, pop_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_3972(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_split[0], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3972_s.wn.real) - (w.imag * CombineDFT_3972_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3972_s.wn.imag) + (w.imag * CombineDFT_3972_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_split[0]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3973(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_split[1], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3973_s.wn.real) - (w.imag * CombineDFT_3973_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3973_s.wn.imag) + (w.imag * CombineDFT_3973_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_split[1]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3974(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_split[2], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3974_s.wn.real) - (w.imag * CombineDFT_3974_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3974_s.wn.imag) + (w.imag * CombineDFT_3974_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_split[2]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3975(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_split[3], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3975_s.wn.real) - (w.imag * CombineDFT_3975_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3975_s.wn.imag) + (w.imag * CombineDFT_3975_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_split[3]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3976(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_split[4], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_split[4], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3976_s.wn.real) - (w.imag * CombineDFT_3976_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3976_s.wn.imag) + (w.imag * CombineDFT_3976_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_split[4]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3977(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_split[5], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_split[5], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3977_s.wn.real) - (w.imag * CombineDFT_3977_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3977_s.wn.imag) + (w.imag * CombineDFT_3977_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_split[5]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3978(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_split[6], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_split[6], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3978_s.wn.real) - (w.imag * CombineDFT_3978_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3978_s.wn.imag) + (w.imag * CombineDFT_3978_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_split[6]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3979(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_split[7], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_split[7], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3979_s.wn.real) - (w.imag * CombineDFT_3979_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3979_s.wn.imag) + (w.imag * CombineDFT_3979_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_split[7]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3970() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3954WEIGHTED_ROUND_ROBIN_Splitter_3970));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3971() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3971WEIGHTED_ROUND_ROBIN_Splitter_3980, pop_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_3982(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_3997_4007_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_3997_4007_split[0], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3982_s.wn.real) - (w.imag * CombineDFT_3982_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3982_s.wn.imag) + (w.imag * CombineDFT_3982_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_3997_4007_split[0]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_3997_4007_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3983(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_3997_4007_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_3997_4007_split[1], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3983_s.wn.real) - (w.imag * CombineDFT_3983_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3983_s.wn.imag) + (w.imag * CombineDFT_3983_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_3997_4007_split[1]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_3997_4007_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3984(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_3997_4007_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_3997_4007_split[2], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3984_s.wn.real) - (w.imag * CombineDFT_3984_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3984_s.wn.imag) + (w.imag * CombineDFT_3984_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_3997_4007_split[2]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_3997_4007_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3985(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_3997_4007_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_3997_4007_split[3], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3985_s.wn.real) - (w.imag * CombineDFT_3985_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3985_s.wn.imag) + (w.imag * CombineDFT_3985_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_3997_4007_split[3]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_3997_4007_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3980() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin14_CombineDFT_Fiss_3997_4007_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3971WEIGHTED_ROUND_ROBIN_Splitter_3980));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3981() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3981WEIGHTED_ROUND_ROBIN_Splitter_3986, pop_complex(&SplitJoin14_CombineDFT_Fiss_3997_4007_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_3988(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[32];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 16, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_3998_4008_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_3998_4008_split[0], (16 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(16 + i)].real = (y0.real - y1w.real) ; 
			results[(16 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3988_s.wn.real) - (w.imag * CombineDFT_3988_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3988_s.wn.imag) + (w.imag * CombineDFT_3988_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin16_CombineDFT_Fiss_3998_4008_split[0]) ; 
			push_complex(&SplitJoin16_CombineDFT_Fiss_3998_4008_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3989(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[32];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 16, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_3998_4008_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_3998_4008_split[1], (16 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(16 + i)].real = (y0.real - y1w.real) ; 
			results[(16 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3989_s.wn.real) - (w.imag * CombineDFT_3989_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3989_s.wn.imag) + (w.imag * CombineDFT_3989_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin16_CombineDFT_Fiss_3998_4008_split[1]) ; 
			push_complex(&SplitJoin16_CombineDFT_Fiss_3998_4008_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3986() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_3998_4008_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3981WEIGHTED_ROUND_ROBIN_Splitter_3986));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_3998_4008_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3981WEIGHTED_ROUND_ROBIN_Splitter_3986));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3987() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3987CombineDFT_3896, pop_complex(&SplitJoin16_CombineDFT_Fiss_3998_4008_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3987CombineDFT_3896, pop_complex(&SplitJoin16_CombineDFT_Fiss_3998_4008_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_3896(){
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[64];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3987CombineDFT_3896, i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3987CombineDFT_3896, (32 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(32 + i)].real = (y0.real - y1w.real) ; 
			results[(32 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3896_s.wn.real) - (w.imag * CombineDFT_3896_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3896_s.wn.imag) + (w.imag * CombineDFT_3896_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3987CombineDFT_3896) ; 
			push_complex(&CombineDFT_3896CPrinter_3897, results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CPrinter_3897(){
	FOR(uint32_t, __iter_steady_, 0, <, 960, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&CombineDFT_3896CPrinter_3897));
		printf("%.10f", c.real);
		printf("\n");
		printf("%.10f", c.imag);
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3971WEIGHTED_ROUND_ROBIN_Splitter_3980);
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_3998_4008_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 4, __iter_init_2_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_3991_4001_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_3996_4006_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 2, __iter_init_5_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_3990_4000_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 15, __iter_init_6_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_split[__iter_init_6_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3987CombineDFT_3896);
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_3992_4002_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 15, __iter_init_8_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_3995_4005_join[__iter_init_8_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3910WEIGHTED_ROUND_ROBIN_Splitter_3919);
	FOR(int, __iter_init_9_, 0, <, 15, __iter_init_9_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_3990_4000_join[__iter_init_10_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3900WEIGHTED_ROUND_ROBIN_Splitter_3903);
	FOR(int, __iter_init_11_, 0, <, 2, __iter_init_11_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_3998_4008_join[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 15, __iter_init_12_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_join[__iter_init_12_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3954WEIGHTED_ROUND_ROBIN_Splitter_3970);
	FOR(int, __iter_init_13_, 0, <, 4, __iter_init_13_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_3997_4007_join[__iter_init_13_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3981WEIGHTED_ROUND_ROBIN_Splitter_3986);
	FOR(int, __iter_init_14_, 0, <, 15, __iter_init_14_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_3993_4003_join[__iter_init_14_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3937WEIGHTED_ROUND_ROBIN_Splitter_3953);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3904WEIGHTED_ROUND_ROBIN_Splitter_3909);
	init_buffer_complex(&CombineDFT_3896CPrinter_3897);
	init_buffer_complex(&FFTReorderSimple_3886WEIGHTED_ROUND_ROBIN_Splitter_3899);
	FOR(int, __iter_init_15_, 0, <, 4, __iter_init_15_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_3991_4001_join[__iter_init_15_]);
	ENDFOR
	init_buffer_complex(&FFTTestSource_3885FFTReorderSimple_3886);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3920WEIGHTED_ROUND_ROBIN_Splitter_3936);
	FOR(int, __iter_init_16_, 0, <, 15, __iter_init_16_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_3994_4004_split[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 4, __iter_init_17_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_3997_4007_split[__iter_init_17_]);
	ENDFOR
// --- init: CombineDFT_3938
	 {
	 ; 
	CombineDFT_3938_s.wn.real = -1.0 ; 
	CombineDFT_3938_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3939
	 {
	 ; 
	CombineDFT_3939_s.wn.real = -1.0 ; 
	CombineDFT_3939_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3940
	 {
	 ; 
	CombineDFT_3940_s.wn.real = -1.0 ; 
	CombineDFT_3940_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3941
	 {
	 ; 
	CombineDFT_3941_s.wn.real = -1.0 ; 
	CombineDFT_3941_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3942
	 {
	 ; 
	CombineDFT_3942_s.wn.real = -1.0 ; 
	CombineDFT_3942_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3943
	 {
	 ; 
	CombineDFT_3943_s.wn.real = -1.0 ; 
	CombineDFT_3943_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3944
	 {
	 ; 
	CombineDFT_3944_s.wn.real = -1.0 ; 
	CombineDFT_3944_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3945
	 {
	 ; 
	CombineDFT_3945_s.wn.real = -1.0 ; 
	CombineDFT_3945_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3946
	 {
	 ; 
	CombineDFT_3946_s.wn.real = -1.0 ; 
	CombineDFT_3946_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3947
	 {
	 ; 
	CombineDFT_3947_s.wn.real = -1.0 ; 
	CombineDFT_3947_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3948
	 {
	 ; 
	CombineDFT_3948_s.wn.real = -1.0 ; 
	CombineDFT_3948_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3949
	 {
	 ; 
	CombineDFT_3949_s.wn.real = -1.0 ; 
	CombineDFT_3949_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3950
	 {
	 ; 
	CombineDFT_3950_s.wn.real = -1.0 ; 
	CombineDFT_3950_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3951
	 {
	 ; 
	CombineDFT_3951_s.wn.real = -1.0 ; 
	CombineDFT_3951_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3952
	 {
	 ; 
	CombineDFT_3952_s.wn.real = -1.0 ; 
	CombineDFT_3952_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3955
	 {
	 ; 
	CombineDFT_3955_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3955_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3956
	 {
	 ; 
	CombineDFT_3956_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3956_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3957
	 {
	 ; 
	CombineDFT_3957_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3957_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3958
	 {
	 ; 
	CombineDFT_3958_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3958_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3959
	 {
	 ; 
	CombineDFT_3959_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3959_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3960
	 {
	 ; 
	CombineDFT_3960_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3960_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3961
	 {
	 ; 
	CombineDFT_3961_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3961_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3962
	 {
	 ; 
	CombineDFT_3962_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3962_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3963
	 {
	 ; 
	CombineDFT_3963_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3963_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3964
	 {
	 ; 
	CombineDFT_3964_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3964_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3965
	 {
	 ; 
	CombineDFT_3965_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3965_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3966
	 {
	 ; 
	CombineDFT_3966_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3966_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3967
	 {
	 ; 
	CombineDFT_3967_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3967_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3968
	 {
	 ; 
	CombineDFT_3968_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3968_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3969
	 {
	 ; 
	CombineDFT_3969_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3969_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3972
	 {
	 ; 
	CombineDFT_3972_s.wn.real = 0.70710677 ; 
	CombineDFT_3972_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3973
	 {
	 ; 
	CombineDFT_3973_s.wn.real = 0.70710677 ; 
	CombineDFT_3973_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3974
	 {
	 ; 
	CombineDFT_3974_s.wn.real = 0.70710677 ; 
	CombineDFT_3974_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3975
	 {
	 ; 
	CombineDFT_3975_s.wn.real = 0.70710677 ; 
	CombineDFT_3975_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3976
	 {
	 ; 
	CombineDFT_3976_s.wn.real = 0.70710677 ; 
	CombineDFT_3976_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3977
	 {
	 ; 
	CombineDFT_3977_s.wn.real = 0.70710677 ; 
	CombineDFT_3977_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3978
	 {
	 ; 
	CombineDFT_3978_s.wn.real = 0.70710677 ; 
	CombineDFT_3978_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3979
	 {
	 ; 
	CombineDFT_3979_s.wn.real = 0.70710677 ; 
	CombineDFT_3979_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3982
	 {
	 ; 
	CombineDFT_3982_s.wn.real = 0.9238795 ; 
	CombineDFT_3982_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_3983
	 {
	 ; 
	CombineDFT_3983_s.wn.real = 0.9238795 ; 
	CombineDFT_3983_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_3984
	 {
	 ; 
	CombineDFT_3984_s.wn.real = 0.9238795 ; 
	CombineDFT_3984_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_3985
	 {
	 ; 
	CombineDFT_3985_s.wn.real = 0.9238795 ; 
	CombineDFT_3985_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_3988
	 {
	 ; 
	CombineDFT_3988_s.wn.real = 0.98078525 ; 
	CombineDFT_3988_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_3989
	 {
	 ; 
	CombineDFT_3989_s.wn.real = 0.98078525 ; 
	CombineDFT_3989_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_3896
	 {
	 ; 
	CombineDFT_3896_s.wn.real = 0.9951847 ; 
	CombineDFT_3896_s.wn.imag = -0.09801714 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FFTTestSource_3885();
		FFTReorderSimple_3886();
		WEIGHTED_ROUND_ROBIN_Splitter_3899();
			FFTReorderSimple_3901();
			FFTReorderSimple_3902();
		WEIGHTED_ROUND_ROBIN_Joiner_3900();
		WEIGHTED_ROUND_ROBIN_Splitter_3903();
			FFTReorderSimple_3905();
			FFTReorderSimple_3906();
			FFTReorderSimple_3907();
			FFTReorderSimple_3908();
		WEIGHTED_ROUND_ROBIN_Joiner_3904();
		WEIGHTED_ROUND_ROBIN_Splitter_3909();
			FFTReorderSimple_3911();
			FFTReorderSimple_3912();
			FFTReorderSimple_3913();
			FFTReorderSimple_3914();
			FFTReorderSimple_3915();
			FFTReorderSimple_3916();
			FFTReorderSimple_3917();
			FFTReorderSimple_3918();
		WEIGHTED_ROUND_ROBIN_Joiner_3910();
		WEIGHTED_ROUND_ROBIN_Splitter_3919();
			FFTReorderSimple_3921();
			FFTReorderSimple_3922();
			FFTReorderSimple_3923();
			FFTReorderSimple_3924();
			FFTReorderSimple_3925();
			FFTReorderSimple_3926();
			FFTReorderSimple_3927();
			FFTReorderSimple_3928();
			FFTReorderSimple_3929();
			FFTReorderSimple_3930();
			FFTReorderSimple_3931();
			FFTReorderSimple_3932();
			FFTReorderSimple_3933();
			FFTReorderSimple_3934();
			FFTReorderSimple_3935();
		WEIGHTED_ROUND_ROBIN_Joiner_3920();
		WEIGHTED_ROUND_ROBIN_Splitter_3936();
			CombineDFT_3938();
			CombineDFT_3939();
			CombineDFT_3940();
			CombineDFT_3941();
			CombineDFT_3942();
			CombineDFT_3943();
			CombineDFT_3944();
			CombineDFT_3945();
			CombineDFT_3946();
			CombineDFT_3947();
			CombineDFT_3948();
			CombineDFT_3949();
			CombineDFT_3950();
			CombineDFT_3951();
			CombineDFT_3952();
		WEIGHTED_ROUND_ROBIN_Joiner_3937();
		WEIGHTED_ROUND_ROBIN_Splitter_3953();
			CombineDFT_3955();
			CombineDFT_3956();
			CombineDFT_3957();
			CombineDFT_3958();
			CombineDFT_3959();
			CombineDFT_3960();
			CombineDFT_3961();
			CombineDFT_3962();
			CombineDFT_3963();
			CombineDFT_3964();
			CombineDFT_3965();
			CombineDFT_3966();
			CombineDFT_3967();
			CombineDFT_3968();
			CombineDFT_3969();
		WEIGHTED_ROUND_ROBIN_Joiner_3954();
		WEIGHTED_ROUND_ROBIN_Splitter_3970();
			CombineDFT_3972();
			CombineDFT_3973();
			CombineDFT_3974();
			CombineDFT_3975();
			CombineDFT_3976();
			CombineDFT_3977();
			CombineDFT_3978();
			CombineDFT_3979();
		WEIGHTED_ROUND_ROBIN_Joiner_3971();
		WEIGHTED_ROUND_ROBIN_Splitter_3980();
			CombineDFT_3982();
			CombineDFT_3983();
			CombineDFT_3984();
			CombineDFT_3985();
		WEIGHTED_ROUND_ROBIN_Joiner_3981();
		WEIGHTED_ROUND_ROBIN_Splitter_3986();
			CombineDFT_3988();
			CombineDFT_3989();
		WEIGHTED_ROUND_ROBIN_Joiner_3987();
		CombineDFT_3896();
		CPrinter_3897();
	ENDFOR
	return EXIT_SUCCESS;
}
