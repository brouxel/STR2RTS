#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=1920 on the compile command line
#else
#if BUF_SIZEMAX < 1920
#error BUF_SIZEMAX too small, it must be at least 1920
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	complex_t wn;
} CombineDFT_3938_t;
void FFTTestSource_3885();
void FFTReorderSimple_3886();
void WEIGHTED_ROUND_ROBIN_Splitter_3899();
void FFTReorderSimple_3901();
void FFTReorderSimple_3902();
void WEIGHTED_ROUND_ROBIN_Joiner_3900();
void WEIGHTED_ROUND_ROBIN_Splitter_3903();
void FFTReorderSimple_3905();
void FFTReorderSimple_3906();
void FFTReorderSimple_3907();
void FFTReorderSimple_3908();
void WEIGHTED_ROUND_ROBIN_Joiner_3904();
void WEIGHTED_ROUND_ROBIN_Splitter_3909();
void FFTReorderSimple_3911();
void FFTReorderSimple_3912();
void FFTReorderSimple_3913();
void FFTReorderSimple_3914();
void FFTReorderSimple_3915();
void FFTReorderSimple_3916();
void FFTReorderSimple_3917();
void FFTReorderSimple_3918();
void WEIGHTED_ROUND_ROBIN_Joiner_3910();
void WEIGHTED_ROUND_ROBIN_Splitter_3919();
void FFTReorderSimple_3921();
void FFTReorderSimple_3922();
void FFTReorderSimple_3923();
void FFTReorderSimple_3924();
void FFTReorderSimple_3925();
void FFTReorderSimple_3926();
void FFTReorderSimple_3927();
void FFTReorderSimple_3928();
void FFTReorderSimple_3929();
void FFTReorderSimple_3930();
void FFTReorderSimple_3931();
void FFTReorderSimple_3932();
void FFTReorderSimple_3933();
void FFTReorderSimple_3934();
void FFTReorderSimple_3935();
void WEIGHTED_ROUND_ROBIN_Joiner_3920();
void WEIGHTED_ROUND_ROBIN_Splitter_3936();
void CombineDFT_3938();
void CombineDFT_3939();
void CombineDFT_3940();
void CombineDFT_3941();
void CombineDFT_3942();
void CombineDFT_3943();
void CombineDFT_3944();
void CombineDFT_3945();
void CombineDFT_3946();
void CombineDFT_3947();
void CombineDFT_3948();
void CombineDFT_3949();
void CombineDFT_3950();
void CombineDFT_3951();
void CombineDFT_3952();
void WEIGHTED_ROUND_ROBIN_Joiner_3937();
void WEIGHTED_ROUND_ROBIN_Splitter_3953();
void CombineDFT_3955();
void CombineDFT_3956();
void CombineDFT_3957();
void CombineDFT_3958();
void CombineDFT_3959();
void CombineDFT_3960();
void CombineDFT_3961();
void CombineDFT_3962();
void CombineDFT_3963();
void CombineDFT_3964();
void CombineDFT_3965();
void CombineDFT_3966();
void CombineDFT_3967();
void CombineDFT_3968();
void CombineDFT_3969();
void WEIGHTED_ROUND_ROBIN_Joiner_3954();
void WEIGHTED_ROUND_ROBIN_Splitter_3970();
void CombineDFT_3972();
void CombineDFT_3973();
void CombineDFT_3974();
void CombineDFT_3975();
void CombineDFT_3976();
void CombineDFT_3977();
void CombineDFT_3978();
void CombineDFT_3979();
void WEIGHTED_ROUND_ROBIN_Joiner_3971();
void WEIGHTED_ROUND_ROBIN_Splitter_3980();
void CombineDFT_3982();
void CombineDFT_3983();
void CombineDFT_3984();
void CombineDFT_3985();
void WEIGHTED_ROUND_ROBIN_Joiner_3981();
void WEIGHTED_ROUND_ROBIN_Splitter_3986();
void CombineDFT_3988();
void CombineDFT_3989();
void WEIGHTED_ROUND_ROBIN_Joiner_3987();
void CombineDFT_3896();
void CPrinter_3897();

#ifdef __cplusplus
}
#endif
#endif
