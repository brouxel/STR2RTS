#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=1920 on the compile command line
#else
#if BUF_SIZEMAX < 1920
#error BUF_SIZEMAX too small, it must be at least 1920
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float A_re[32];
	float A_im[32];
	int idx;
} FloatSource_2214_t;
void FloatSource_2214();
void Pre_CollapsedDataParallel_1_2515();
void WEIGHTED_ROUND_ROBIN_Splitter_2657();
void Butterfly_2659();
void Butterfly_2660();
void Butterfly_2661();
void Butterfly_2662();
void Butterfly_2663();
void Butterfly_2664();
void Butterfly_2665();
void Butterfly_2666();
void Butterfly_2667();
void Butterfly_2668();
void Butterfly_2669();
void Butterfly_2670();
void Butterfly_2671();
void Butterfly_2672();
void Butterfly_2673();
void WEIGHTED_ROUND_ROBIN_Joiner_2658();
void Post_CollapsedDataParallel_2_2516();
void WEIGHTED_ROUND_ROBIN_Splitter_2559();
void Pre_CollapsedDataParallel_1_2518();
void WEIGHTED_ROUND_ROBIN_Splitter_2674();
void Butterfly_2676();
void Butterfly_2677();
void Butterfly_2678();
void Butterfly_2679();
void Butterfly_2680();
void Butterfly_2681();
void Butterfly_2682();
void Butterfly_2683();
void WEIGHTED_ROUND_ROBIN_Joiner_2675();
void Post_CollapsedDataParallel_2_2519();
void WEIGHTED_ROUND_ROBIN_Splitter_2639();
void Pre_CollapsedDataParallel_1_2524();
void WEIGHTED_ROUND_ROBIN_Splitter_2684();
void Butterfly_2686();
void Butterfly_2687();
void Butterfly_2688();
void Butterfly_2689();
void WEIGHTED_ROUND_ROBIN_Joiner_2685();
void Post_CollapsedDataParallel_2_2525();
void Pre_CollapsedDataParallel_1_2527();
void WEIGHTED_ROUND_ROBIN_Splitter_2690();
void Butterfly_2692();
void Butterfly_2693();
void Butterfly_2694();
void Butterfly_2695();
void WEIGHTED_ROUND_ROBIN_Joiner_2691();
void Post_CollapsedDataParallel_2_2528();
void WEIGHTED_ROUND_ROBIN_Joiner_2640();
void Pre_CollapsedDataParallel_1_2521();
void WEIGHTED_ROUND_ROBIN_Splitter_2696();
void Butterfly_2698();
void Butterfly_2699();
void Butterfly_2700();
void Butterfly_2701();
void Butterfly_2702();
void Butterfly_2703();
void Butterfly_2704();
void Butterfly_2705();
void WEIGHTED_ROUND_ROBIN_Joiner_2697();
void Post_CollapsedDataParallel_2_2522();
void WEIGHTED_ROUND_ROBIN_Splitter_2641();
void Pre_CollapsedDataParallel_1_2530();
void WEIGHTED_ROUND_ROBIN_Splitter_2706();
void Butterfly_2708();
void Butterfly_2709();
void Butterfly_2710();
void Butterfly_2711();
void WEIGHTED_ROUND_ROBIN_Joiner_2707();
void Post_CollapsedDataParallel_2_2531();
void Pre_CollapsedDataParallel_1_2533();
void WEIGHTED_ROUND_ROBIN_Splitter_2712();
void Butterfly_2714();
void Butterfly_2715();
void Butterfly_2716();
void Butterfly_2717();
void WEIGHTED_ROUND_ROBIN_Joiner_2713();
void Post_CollapsedDataParallel_2_2534();
void WEIGHTED_ROUND_ROBIN_Joiner_2642();
void WEIGHTED_ROUND_ROBIN_Joiner_2643();
void WEIGHTED_ROUND_ROBIN_Splitter_2644();
void WEIGHTED_ROUND_ROBIN_Splitter_2645();
void Pre_CollapsedDataParallel_1_2536();
void WEIGHTED_ROUND_ROBIN_Splitter_2718();
void Butterfly_2720();
void Butterfly_2721();
void WEIGHTED_ROUND_ROBIN_Joiner_2719();
void Post_CollapsedDataParallel_2_2537();
void Pre_CollapsedDataParallel_1_2539();
void WEIGHTED_ROUND_ROBIN_Splitter_2722();
void Butterfly_2724();
void Butterfly_2725();
void WEIGHTED_ROUND_ROBIN_Joiner_2723();
void Post_CollapsedDataParallel_2_2540();
void Pre_CollapsedDataParallel_1_2542();
void WEIGHTED_ROUND_ROBIN_Splitter_2726();
void Butterfly_2728();
void Butterfly_2729();
void WEIGHTED_ROUND_ROBIN_Joiner_2727();
void Post_CollapsedDataParallel_2_2543();
void Pre_CollapsedDataParallel_1_2545();
void WEIGHTED_ROUND_ROBIN_Splitter_2730();
void Butterfly_2732();
void Butterfly_2733();
void WEIGHTED_ROUND_ROBIN_Joiner_2731();
void Post_CollapsedDataParallel_2_2546();
void WEIGHTED_ROUND_ROBIN_Joiner_2646();
void WEIGHTED_ROUND_ROBIN_Splitter_2647();
void Pre_CollapsedDataParallel_1_2548();
void WEIGHTED_ROUND_ROBIN_Splitter_2734();
void Butterfly_2736();
void Butterfly_2737();
void WEIGHTED_ROUND_ROBIN_Joiner_2735();
void Post_CollapsedDataParallel_2_2549();
void Pre_CollapsedDataParallel_1_2551();
void WEIGHTED_ROUND_ROBIN_Splitter_2738();
void Butterfly_2740();
void Butterfly_2741();
void WEIGHTED_ROUND_ROBIN_Joiner_2739();
void Post_CollapsedDataParallel_2_2552();
void Pre_CollapsedDataParallel_1_2554();
void WEIGHTED_ROUND_ROBIN_Splitter_2742();
void Butterfly_2744();
void Butterfly_2745();
void WEIGHTED_ROUND_ROBIN_Joiner_2743();
void Post_CollapsedDataParallel_2_2555();
void Pre_CollapsedDataParallel_1_2557();
void WEIGHTED_ROUND_ROBIN_Splitter_2746();
void Butterfly_2748();
void Butterfly_2749();
void WEIGHTED_ROUND_ROBIN_Joiner_2747();
void Post_CollapsedDataParallel_2_2558();
void WEIGHTED_ROUND_ROBIN_Joiner_2648();
void WEIGHTED_ROUND_ROBIN_Joiner_2649();
void WEIGHTED_ROUND_ROBIN_Splitter_2650();
void WEIGHTED_ROUND_ROBIN_Splitter_2651();
void Butterfly_2279();
void Butterfly_2280();
void Butterfly_2281();
void Butterfly_2282();
void Butterfly_2283();
void Butterfly_2284();
void Butterfly_2285();
void Butterfly_2286();
void WEIGHTED_ROUND_ROBIN_Joiner_2652();
void WEIGHTED_ROUND_ROBIN_Splitter_2653();
void Butterfly_2287();
void Butterfly_2288();
void Butterfly_2289();
void Butterfly_2290();
void Butterfly_2291();
void Butterfly_2292();
void Butterfly_2293();
void Butterfly_2294();
void WEIGHTED_ROUND_ROBIN_Joiner_2654();
void WEIGHTED_ROUND_ROBIN_Joiner_2655();
int BitReverse_2295_bitrev(int inp, int numbits);
void BitReverse_2295();
void FloatPrinter_2296();

#ifdef __cplusplus
}
#endif
#endif
