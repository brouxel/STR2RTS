#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=15872 on the compile command line
#else
#if BUF_SIZEMAX < 15872
#error BUF_SIZEMAX too small, it must be at least 15872
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float w[2];
} CombineDFT_746_t;

typedef struct {
	float w[4];
} CombineDFT_779_t;

typedef struct {
	float w[8];
} CombineDFT_797_t;

typedef struct {
	float w[16];
} CombineDFT_807_t;

typedef struct {
	float w[32];
} CombineDFT_813_t;

typedef struct {
	float w[64];
} CombineDFT_681_t;
void WEIGHTED_ROUND_ROBIN_Splitter_702();
void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout);
void FFTTestSource_704();
void FFTTestSource_705();
void WEIGHTED_ROUND_ROBIN_Joiner_703();
void WEIGHTED_ROUND_ROBIN_Splitter_694();
void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout);
void FFTReorderSimple_671();
void WEIGHTED_ROUND_ROBIN_Splitter_706();
void FFTReorderSimple_708();
void FFTReorderSimple_709();
void WEIGHTED_ROUND_ROBIN_Joiner_707();
void WEIGHTED_ROUND_ROBIN_Splitter_710();
void FFTReorderSimple_712();
void FFTReorderSimple_713();
void FFTReorderSimple_714();
void FFTReorderSimple_715();
void WEIGHTED_ROUND_ROBIN_Joiner_711();
void WEIGHTED_ROUND_ROBIN_Splitter_716();
void FFTReorderSimple_718();
void FFTReorderSimple_719();
void FFTReorderSimple_720();
void FFTReorderSimple_721();
void FFTReorderSimple_722();
void FFTReorderSimple_723();
void FFTReorderSimple_724();
void FFTReorderSimple_725();
void WEIGHTED_ROUND_ROBIN_Joiner_717();
void WEIGHTED_ROUND_ROBIN_Splitter_726();
void FFTReorderSimple_728();
void FFTReorderSimple_729();
void FFTReorderSimple_730();
void FFTReorderSimple_731();
void FFTReorderSimple_732();
void FFTReorderSimple_733();
void FFTReorderSimple_734();
void FFTReorderSimple_735();
void FFTReorderSimple_736();
void FFTReorderSimple_737();
void FFTReorderSimple_738();
void FFTReorderSimple_739();
void FFTReorderSimple_740();
void FFTReorderSimple_741();
void FFTReorderSimple_742();
void FFTReorderSimple_743();
void WEIGHTED_ROUND_ROBIN_Joiner_727();
void WEIGHTED_ROUND_ROBIN_Splitter_744();
void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout);
void CombineDFT_746();
void CombineDFT_747();
void CombineDFT_748();
void CombineDFT_749();
void CombineDFT_750();
void CombineDFT_751();
void CombineDFT_752();
void CombineDFT_753();
void CombineDFT_754();
void CombineDFT_755();
void CombineDFT_756();
void CombineDFT_757();
void CombineDFT_758();
void CombineDFT_759();
void CombineDFT_760();
void CombineDFT_761();
void CombineDFT_762();
void CombineDFT_763();
void CombineDFT_764();
void CombineDFT_765();
void CombineDFT_766();
void CombineDFT_767();
void CombineDFT_768();
void CombineDFT_769();
void CombineDFT_770();
void CombineDFT_771();
void CombineDFT_772();
void CombineDFT_773();
void CombineDFT_774();
void CombineDFT_775();
void CombineDFT_776();
void WEIGHTED_ROUND_ROBIN_Joiner_745();
void WEIGHTED_ROUND_ROBIN_Splitter_777();
void CombineDFT_779();
void CombineDFT_780();
void CombineDFT_781();
void CombineDFT_782();
void CombineDFT_783();
void CombineDFT_784();
void CombineDFT_785();
void CombineDFT_786();
void CombineDFT_787();
void CombineDFT_788();
void CombineDFT_789();
void CombineDFT_790();
void CombineDFT_791();
void CombineDFT_792();
void CombineDFT_793();
void CombineDFT_794();
void WEIGHTED_ROUND_ROBIN_Joiner_778();
void WEIGHTED_ROUND_ROBIN_Splitter_795();
void CombineDFT_797();
void CombineDFT_798();
void CombineDFT_799();
void CombineDFT_800();
void CombineDFT_801();
void CombineDFT_802();
void CombineDFT_803();
void CombineDFT_804();
void WEIGHTED_ROUND_ROBIN_Joiner_796();
void WEIGHTED_ROUND_ROBIN_Splitter_805();
void CombineDFT_807();
void CombineDFT_808();
void CombineDFT_809();
void CombineDFT_810();
void WEIGHTED_ROUND_ROBIN_Joiner_806();
void WEIGHTED_ROUND_ROBIN_Splitter_811();
void CombineDFT_813();
void CombineDFT_814();
void WEIGHTED_ROUND_ROBIN_Joiner_812();
void CombineDFT_681();
void FFTReorderSimple_682();
void WEIGHTED_ROUND_ROBIN_Splitter_815();
void FFTReorderSimple_817();
void FFTReorderSimple_818();
void WEIGHTED_ROUND_ROBIN_Joiner_816();
void WEIGHTED_ROUND_ROBIN_Splitter_819();
void FFTReorderSimple_821();
void FFTReorderSimple_822();
void FFTReorderSimple_823();
void FFTReorderSimple_824();
void WEIGHTED_ROUND_ROBIN_Joiner_820();
void WEIGHTED_ROUND_ROBIN_Splitter_825();
void FFTReorderSimple_827();
void FFTReorderSimple_828();
void FFTReorderSimple_829();
void FFTReorderSimple_830();
void FFTReorderSimple_831();
void FFTReorderSimple_832();
void FFTReorderSimple_833();
void FFTReorderSimple_834();
void WEIGHTED_ROUND_ROBIN_Joiner_826();
void WEIGHTED_ROUND_ROBIN_Splitter_835();
void FFTReorderSimple_837();
void FFTReorderSimple_838();
void FFTReorderSimple_839();
void FFTReorderSimple_840();
void FFTReorderSimple_841();
void FFTReorderSimple_842();
void FFTReorderSimple_843();
void FFTReorderSimple_844();
void FFTReorderSimple_845();
void FFTReorderSimple_846();
void FFTReorderSimple_847();
void FFTReorderSimple_848();
void FFTReorderSimple_849();
void FFTReorderSimple_850();
void FFTReorderSimple_851();
void FFTReorderSimple_852();
void WEIGHTED_ROUND_ROBIN_Joiner_836();
void WEIGHTED_ROUND_ROBIN_Splitter_853();
void CombineDFT_855();
void CombineDFT_856();
void CombineDFT_857();
void CombineDFT_858();
void CombineDFT_859();
void CombineDFT_860();
void CombineDFT_861();
void CombineDFT_862();
void CombineDFT_863();
void CombineDFT_864();
void CombineDFT_865();
void CombineDFT_866();
void CombineDFT_867();
void CombineDFT_868();
void CombineDFT_869();
void CombineDFT_870();
void CombineDFT_871();
void CombineDFT_872();
void CombineDFT_873();
void CombineDFT_874();
void CombineDFT_875();
void CombineDFT_876();
void CombineDFT_877();
void CombineDFT_878();
void CombineDFT_879();
void CombineDFT_880();
void CombineDFT_881();
void CombineDFT_882();
void CombineDFT_883();
void CombineDFT_884();
void CombineDFT_885();
void WEIGHTED_ROUND_ROBIN_Joiner_854();
void WEIGHTED_ROUND_ROBIN_Splitter_886();
void CombineDFT_888();
void CombineDFT_889();
void CombineDFT_890();
void CombineDFT_891();
void CombineDFT_892();
void CombineDFT_893();
void CombineDFT_894();
void CombineDFT_895();
void CombineDFT_896();
void CombineDFT_897();
void CombineDFT_898();
void CombineDFT_899();
void CombineDFT_900();
void CombineDFT_901();
void CombineDFT_902();
void CombineDFT_903();
void WEIGHTED_ROUND_ROBIN_Joiner_887();
void WEIGHTED_ROUND_ROBIN_Splitter_904();
void CombineDFT_906();
void CombineDFT_907();
void CombineDFT_908();
void CombineDFT_909();
void CombineDFT_910();
void CombineDFT_911();
void CombineDFT_912();
void CombineDFT_913();
void WEIGHTED_ROUND_ROBIN_Joiner_905();
void WEIGHTED_ROUND_ROBIN_Splitter_914();
void CombineDFT_916();
void CombineDFT_917();
void CombineDFT_918();
void CombineDFT_919();
void WEIGHTED_ROUND_ROBIN_Joiner_915();
void WEIGHTED_ROUND_ROBIN_Splitter_920();
void CombineDFT_922();
void CombineDFT_923();
void WEIGHTED_ROUND_ROBIN_Joiner_921();
void CombineDFT_692();
void WEIGHTED_ROUND_ROBIN_Joiner_695();
void FloatPrinter(buffer_float_t *chanin);
void FloatPrinter_693();

#ifdef __cplusplus
}
#endif
#endif
