#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=1920 on the compile command line
#else
#if BUF_SIZEMAX < 1920
#error BUF_SIZEMAX too small, it must be at least 1920
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	complex_t wn;
} CombineDFT_609_t;
void FFTTestSource(buffer_complex_t *chanout);
void FFTTestSource_555();
void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout);
void FFTReorderSimple_556();
void WEIGHTED_ROUND_ROBIN_Splitter_569();
void FFTReorderSimple_571();
void FFTReorderSimple_572();
void WEIGHTED_ROUND_ROBIN_Joiner_570();
void WEIGHTED_ROUND_ROBIN_Splitter_573();
void FFTReorderSimple_575();
void FFTReorderSimple_576();
void FFTReorderSimple_577();
void FFTReorderSimple_578();
void WEIGHTED_ROUND_ROBIN_Joiner_574();
void WEIGHTED_ROUND_ROBIN_Splitter_579();
void FFTReorderSimple_581();
void FFTReorderSimple_582();
void FFTReorderSimple_583();
void FFTReorderSimple_584();
void FFTReorderSimple_585();
void FFTReorderSimple_586();
void FFTReorderSimple_587();
void FFTReorderSimple_588();
void WEIGHTED_ROUND_ROBIN_Joiner_580();
void WEIGHTED_ROUND_ROBIN_Splitter_589();
void FFTReorderSimple_591();
void FFTReorderSimple_592();
void FFTReorderSimple_593();
void FFTReorderSimple_594();
void FFTReorderSimple_595();
void FFTReorderSimple_596();
void FFTReorderSimple_597();
void FFTReorderSimple_598();
void FFTReorderSimple_599();
void FFTReorderSimple_600();
void FFTReorderSimple_601();
void FFTReorderSimple_602();
void FFTReorderSimple_603();
void FFTReorderSimple_604();
void FFTReorderSimple_605();
void FFTReorderSimple_606();
void WEIGHTED_ROUND_ROBIN_Joiner_590();
void WEIGHTED_ROUND_ROBIN_Splitter_607();
void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout);
void CombineDFT_609();
void CombineDFT_610();
void CombineDFT_611();
void CombineDFT_612();
void CombineDFT_613();
void CombineDFT_614();
void CombineDFT_615();
void CombineDFT_616();
void CombineDFT_617();
void CombineDFT_618();
void CombineDFT_619();
void CombineDFT_620();
void CombineDFT_621();
void CombineDFT_622();
void CombineDFT_623();
void CombineDFT_624();
void CombineDFT_625();
void CombineDFT_626();
void CombineDFT_627();
void CombineDFT_628();
void CombineDFT_629();
void CombineDFT_630();
void CombineDFT_631();
void CombineDFT_632();
void CombineDFT_633();
void CombineDFT_634();
void CombineDFT_635();
void CombineDFT_636();
void CombineDFT_637();
void CombineDFT_638();
void WEIGHTED_ROUND_ROBIN_Joiner_608();
void WEIGHTED_ROUND_ROBIN_Splitter_639();
void CombineDFT_641();
void CombineDFT_642();
void CombineDFT_643();
void CombineDFT_644();
void CombineDFT_645();
void CombineDFT_646();
void CombineDFT_647();
void CombineDFT_648();
void CombineDFT_649();
void CombineDFT_650();
void CombineDFT_651();
void CombineDFT_652();
void CombineDFT_653();
void CombineDFT_654();
void CombineDFT_655();
void CombineDFT_656();
void WEIGHTED_ROUND_ROBIN_Joiner_640();
void WEIGHTED_ROUND_ROBIN_Splitter_657();
void CombineDFT_659();
void CombineDFT_660();
void CombineDFT_661();
void CombineDFT_662();
void CombineDFT_663();
void CombineDFT_664();
void CombineDFT_665();
void CombineDFT_666();
void WEIGHTED_ROUND_ROBIN_Joiner_658();
void WEIGHTED_ROUND_ROBIN_Splitter_667();
void CombineDFT_669();
void CombineDFT_670();
void CombineDFT_671();
void CombineDFT_672();
void WEIGHTED_ROUND_ROBIN_Joiner_668();
void WEIGHTED_ROUND_ROBIN_Splitter_673();
void CombineDFT_675();
void CombineDFT_676();
void WEIGHTED_ROUND_ROBIN_Joiner_674();
void CombineDFT_566();
void CPrinter(buffer_complex_t *chanin);
void CPrinter_567();

#ifdef __cplusplus
}
#endif
#endif
