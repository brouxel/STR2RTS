#include "PEG4-FFT5.h"

buffer_complex_t SplitJoin70_SplitJoin55_SplitJoin55_AnonFilter_a0_8601_8850_8936_8952_split[2];
buffer_complex_t SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_8587_8810_8929_8949_split[2];
buffer_complex_t Pre_CollapsedDataParallel_1_8739butterfly_8644;
buffer_complex_t SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8583_8808_8891_8947_split[2];
buffer_complex_t SplitJoin78_SplitJoin63_SplitJoin63_AnonFilter_a0_8613_8856_8892_8954_split[2];
buffer_complex_t butterfly_8643Post_CollapsedDataParallel_2_8737;
buffer_complex_t Pre_CollapsedDataParallel_1_8742butterfly_8645;
buffer_complex_t SplitJoin82_SplitJoin67_SplitJoin67_AnonFilter_a0_8617_8858_8939_8956_join[2];
buffer_complex_t SplitJoin86_SplitJoin71_SplitJoin71_AnonFilter_a0_8623_8861_8940_8957_join[2];
buffer_complex_t SplitJoin80_SplitJoin65_SplitJoin65_AnonFilter_a0_8615_8857_8938_8955_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_8917WEIGHTED_ROUND_ROBIN_Splitter_8759;
buffer_complex_t SplitJoin70_SplitJoin55_SplitJoin55_AnonFilter_a0_8601_8850_8936_8952_join[2];
buffer_complex_t SplitJoin46_SplitJoin33_SplitJoin33_split2_8566_8832_8896_8969_join[2];
buffer_complex_t SplitJoin44_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child1_8902_8968_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_8751butterfly_8648;
buffer_complex_t SplitJoin90_SplitJoin75_SplitJoin75_AnonFilter_a0_8629_8864_8941_8958_join[2];
buffer_complex_t SplitJoin12_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child0_8894_8962_split[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_8760WEIGHTED_ROUND_ROBIN_Splitter_8903;
buffer_complex_t SplitJoin90_SplitJoin75_SplitJoin75_AnonFilter_a0_8629_8864_8941_8958_split[2];
buffer_complex_t SplitJoin10_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_Hier_8930_8961_split[2];
buffer_complex_t Pre_CollapsedDataParallel_1_8754butterfly_8649;
buffer_complex_t SplitJoin78_SplitJoin63_SplitJoin63_AnonFilter_a0_8613_8856_8892_8954_join[2];
buffer_complex_t SplitJoin80_SplitJoin65_SplitJoin65_AnonFilter_a0_8615_8857_8938_8955_join[2];
buffer_complex_t SplitJoin50_SplitJoin37_SplitJoin37_split2_8568_8835_8898_8970_join[2];
buffer_complex_t SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8585_8809_8928_8948_join[2];
buffer_complex_t butterfly_8644Post_CollapsedDataParallel_2_8740;
buffer_complex_t Pre_CollapsedDataParallel_1_8757butterfly_8650;
buffer_complex_t SplitJoin22_SplitJoin16_SplitJoin16_split2_8575_8818_8899_8972_join[4];
buffer_complex_t SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_8581_8807_8927_8946_join[2];
buffer_complex_t butterfly_8646Post_CollapsedDataParallel_2_8746;
buffer_complex_t SplitJoin14_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_Hier_8931_8964_join[2];
buffer_complex_t SplitJoin46_SplitJoin33_SplitJoin33_split2_8566_8832_8896_8969_split[2];
buffer_complex_t SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8585_8809_8928_8948_split[2];
buffer_complex_t SplitJoin82_SplitJoin67_SplitJoin67_AnonFilter_a0_8617_8858_8939_8956_split[2];
buffer_complex_t SplitJoin18_SplitJoin12_SplitJoin12_split2_8562_8815_8893_8966_split[2];
buffer_complex_t SplitJoin20_SplitJoin14_SplitJoin14_split1_8573_8817_8932_8971_split[2];
buffer_complex_t SplitJoin22_SplitJoin16_SplitJoin16_split2_8575_8818_8899_8972_split[4];
buffer_complex_t Pre_CollapsedDataParallel_1_8748butterfly_8647;
buffer_complex_t SplitJoin64_SplitJoin49_SplitJoin49_AnonFilter_a0_8593_8846_8934_8950_split[2];
buffer_complex_t butterfly_8648Post_CollapsedDataParallel_2_8752;
buffer_complex_t SplitJoin14_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_Hier_8931_8964_split[2];
buffer_complex_t butterfly_8649Post_CollapsedDataParallel_2_8755;
buffer_complex_t SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_8581_8807_8927_8946_split[2];
buffer_complex_t SplitJoin68_SplitJoin53_SplitJoin53_AnonFilter_a0_8599_8849_8935_8951_split[2];
buffer_complex_t SplitJoin68_SplitJoin53_SplitJoin53_AnonFilter_a0_8599_8849_8935_8951_join[2];
buffer_complex_t SplitJoin57_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child1_8897_8963_split[4];
buffer_complex_t butterfly_8650Post_CollapsedDataParallel_2_8758;
buffer_complex_t Pre_CollapsedDataParallel_1_8736butterfly_8643;
buffer_complex_t SplitJoin74_SplitJoin59_SplitJoin59_AnonFilter_a0_8607_8853_8937_8953_split[2];
buffer_complex_t SplitJoin86_SplitJoin71_SplitJoin71_AnonFilter_a0_8623_8861_8940_8957_split[2];
buffer_complex_t SplitJoin18_SplitJoin12_SplitJoin12_split2_8562_8815_8893_8966_join[2];
buffer_complex_t SplitJoin20_SplitJoin14_SplitJoin14_split1_8573_8817_8932_8971_join[2];
buffer_complex_t SplitJoin16_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child0_8900_8965_split[2];
buffer_complex_t SplitJoin96_SplitJoin81_SplitJoin81_AnonFilter_a0_8637_8868_8943_8960_split[2];
buffer_complex_t SplitJoin16_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child0_8900_8965_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_8745butterfly_8646;
buffer_complex_t SplitJoin33_SplitJoin22_SplitJoin22_split2_8577_8823_8901_8973_split[4];
buffer_complex_t SplitJoin40_SplitJoin29_SplitJoin29_split2_8564_8829_8895_8967_split[2];
buffer_complex_t SplitJoin64_SplitJoin49_SplitJoin49_AnonFilter_a0_8593_8846_8934_8950_join[2];
buffer_float_t SplitJoin24_magnitude_Fiss_8933_8974_join[4];
buffer_complex_t SplitJoin44_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child1_8902_8968_split[2];
buffer_complex_t SplitJoin40_SplitJoin29_SplitJoin29_split2_8564_8829_8895_8967_join[2];
buffer_complex_t SplitJoin10_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_Hier_8930_8961_join[2];
buffer_complex_t SplitJoin74_SplitJoin59_SplitJoin59_AnonFilter_a0_8607_8853_8937_8953_join[2];
buffer_complex_t SplitJoin92_SplitJoin77_SplitJoin77_AnonFilter_a0_8631_8865_8942_8959_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_8908WEIGHTED_ROUND_ROBIN_Splitter_8909;
buffer_complex_t butterfly_8645Post_CollapsedDataParallel_2_8743;
buffer_complex_t SplitJoin12_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child0_8894_8962_join[4];
buffer_complex_t butterfly_8647Post_CollapsedDataParallel_2_8749;
buffer_complex_t SplitJoin92_SplitJoin77_SplitJoin77_AnonFilter_a0_8631_8865_8942_8959_join[2];
buffer_complex_t SplitJoin33_SplitJoin22_SplitJoin22_split2_8577_8823_8901_8973_join[4];
buffer_complex_t SplitJoin24_magnitude_Fiss_8933_8974_split[4];
buffer_complex_t SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_8587_8810_8929_8949_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8921sink_8670;
buffer_complex_t SplitJoin96_SplitJoin81_SplitJoin81_AnonFilter_a0_8637_8868_8943_8960_join[2];
buffer_complex_t SplitJoin57_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child1_8897_8963_join[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_8914WEIGHTED_ROUND_ROBIN_Splitter_8801;
buffer_complex_t SplitJoin0_source_Fiss_8926_8945_join[2];
buffer_complex_t SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8583_8808_8891_8947_join[2];
buffer_complex_t SplitJoin0_source_Fiss_8926_8945_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_8802WEIGHTED_ROUND_ROBIN_Splitter_8920;
buffer_complex_t SplitJoin50_SplitJoin37_SplitJoin37_split2_8568_8835_8898_8970_split[2];



void source(buffer_void_t *chanin, buffer_complex_t *chanout) {
	complex_t t;
	t.imag = 0.0 ; 
	t.real = 0.9501 ; 
	push_complex(&(*chanout), t) ; 
	t.real = 0.2311 ; 
	push_complex(&(*chanout), t) ; 
	t.real = 0.6068 ; 
	push_complex(&(*chanout), t) ; 
	t.real = 0.486 ; 
	push_complex(&(*chanout), t) ; 
	t.real = 0.8913 ; 
	push_complex(&(*chanout), t) ; 
	t.real = 0.7621 ; 
	push_complex(&(*chanout), t) ; 
	t.real = 0.4565 ; 
	push_complex(&(*chanout), t) ; 
	t.real = 0.0185 ; 
	push_complex(&(*chanout), t) ; 
}


void source_8918() {
	source(&(SplitJoin0_source_Fiss_8926_8945_split[0]), &(SplitJoin0_source_Fiss_8926_8945_join[0]));
}

void source_8919() {
	source(&(SplitJoin0_source_Fiss_8926_8945_split[1]), &(SplitJoin0_source_Fiss_8926_8945_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_8916() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_8917() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8917WEIGHTED_ROUND_ROBIN_Splitter_8759, pop_complex(&SplitJoin0_source_Fiss_8926_8945_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8917WEIGHTED_ROUND_ROBIN_Splitter_8759, pop_complex(&SplitJoin0_source_Fiss_8926_8945_join[1]));
	ENDFOR
}

void Identity(buffer_complex_t *chanin, buffer_complex_t *chanout) {
	complex_t __tmp2310 = pop_complex(&(*chanin));
	push_complex(&(*chanout), __tmp2310) ; 
}


void Identity_8589() {
	Identity(&(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_8587_8810_8929_8949_split[0]), &(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_8587_8810_8929_8949_join[0]));
}

void Identity_8591() {
	Identity(&(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_8587_8810_8929_8949_split[1]), &(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_8587_8810_8929_8949_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_8765() {
	push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_8587_8810_8929_8949_split[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8585_8809_8928_8948_split[0]));
	push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_8587_8810_8929_8949_split[1], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8585_8809_8928_8948_split[0]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_8766() {
	push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8585_8809_8928_8948_join[0], pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_8587_8810_8929_8949_join[0]));
	push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8585_8809_8928_8948_join[0], pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_8587_8810_8929_8949_join[1]));
}

void Identity_8595() {
	Identity(&(SplitJoin64_SplitJoin49_SplitJoin49_AnonFilter_a0_8593_8846_8934_8950_split[0]), &(SplitJoin64_SplitJoin49_SplitJoin49_AnonFilter_a0_8593_8846_8934_8950_join[0]));
}

void Identity_8597() {
	Identity(&(SplitJoin64_SplitJoin49_SplitJoin49_AnonFilter_a0_8593_8846_8934_8950_split[1]), &(SplitJoin64_SplitJoin49_SplitJoin49_AnonFilter_a0_8593_8846_8934_8950_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_8767() {
	push_complex(&SplitJoin64_SplitJoin49_SplitJoin49_AnonFilter_a0_8593_8846_8934_8950_split[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8585_8809_8928_8948_split[1]));
	push_complex(&SplitJoin64_SplitJoin49_SplitJoin49_AnonFilter_a0_8593_8846_8934_8950_split[1], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8585_8809_8928_8948_split[1]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_8768() {
	push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8585_8809_8928_8948_join[1], pop_complex(&SplitJoin64_SplitJoin49_SplitJoin49_AnonFilter_a0_8593_8846_8934_8950_join[0]));
	push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8585_8809_8928_8948_join[1], pop_complex(&SplitJoin64_SplitJoin49_SplitJoin49_AnonFilter_a0_8593_8846_8934_8950_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_8763() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8585_8809_8928_8948_split[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8583_8808_8891_8947_split[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8585_8809_8928_8948_split[1], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8583_8808_8891_8947_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8764() {
	push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8583_8808_8891_8947_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8585_8809_8928_8948_join[0]));
	push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8583_8808_8891_8947_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8585_8809_8928_8948_join[0]));
	push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8583_8808_8891_8947_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8585_8809_8928_8948_join[1]));
	push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8583_8808_8891_8947_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8585_8809_8928_8948_join[1]));
}

void Identity_8603() {
	Identity(&(SplitJoin70_SplitJoin55_SplitJoin55_AnonFilter_a0_8601_8850_8936_8952_split[0]), &(SplitJoin70_SplitJoin55_SplitJoin55_AnonFilter_a0_8601_8850_8936_8952_join[0]));
}

void Identity_8605() {
	Identity(&(SplitJoin70_SplitJoin55_SplitJoin55_AnonFilter_a0_8601_8850_8936_8952_split[1]), &(SplitJoin70_SplitJoin55_SplitJoin55_AnonFilter_a0_8601_8850_8936_8952_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_8771() {
	push_complex(&SplitJoin70_SplitJoin55_SplitJoin55_AnonFilter_a0_8601_8850_8936_8952_split[0], pop_complex(&SplitJoin68_SplitJoin53_SplitJoin53_AnonFilter_a0_8599_8849_8935_8951_split[0]));
	push_complex(&SplitJoin70_SplitJoin55_SplitJoin55_AnonFilter_a0_8601_8850_8936_8952_split[1], pop_complex(&SplitJoin68_SplitJoin53_SplitJoin53_AnonFilter_a0_8599_8849_8935_8951_split[0]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_8772() {
	push_complex(&SplitJoin68_SplitJoin53_SplitJoin53_AnonFilter_a0_8599_8849_8935_8951_join[0], pop_complex(&SplitJoin70_SplitJoin55_SplitJoin55_AnonFilter_a0_8601_8850_8936_8952_join[0]));
	push_complex(&SplitJoin68_SplitJoin53_SplitJoin53_AnonFilter_a0_8599_8849_8935_8951_join[0], pop_complex(&SplitJoin70_SplitJoin55_SplitJoin55_AnonFilter_a0_8601_8850_8936_8952_join[1]));
}

void Identity_8609() {
	Identity(&(SplitJoin74_SplitJoin59_SplitJoin59_AnonFilter_a0_8607_8853_8937_8953_split[0]), &(SplitJoin74_SplitJoin59_SplitJoin59_AnonFilter_a0_8607_8853_8937_8953_join[0]));
}

void Identity_8611() {
	Identity(&(SplitJoin74_SplitJoin59_SplitJoin59_AnonFilter_a0_8607_8853_8937_8953_split[1]), &(SplitJoin74_SplitJoin59_SplitJoin59_AnonFilter_a0_8607_8853_8937_8953_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_8773() {
	push_complex(&SplitJoin74_SplitJoin59_SplitJoin59_AnonFilter_a0_8607_8853_8937_8953_split[0], pop_complex(&SplitJoin68_SplitJoin53_SplitJoin53_AnonFilter_a0_8599_8849_8935_8951_split[1]));
	push_complex(&SplitJoin74_SplitJoin59_SplitJoin59_AnonFilter_a0_8607_8853_8937_8953_split[1], pop_complex(&SplitJoin68_SplitJoin53_SplitJoin53_AnonFilter_a0_8599_8849_8935_8951_split[1]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_8774() {
	push_complex(&SplitJoin68_SplitJoin53_SplitJoin53_AnonFilter_a0_8599_8849_8935_8951_join[1], pop_complex(&SplitJoin74_SplitJoin59_SplitJoin59_AnonFilter_a0_8607_8853_8937_8953_join[0]));
	push_complex(&SplitJoin68_SplitJoin53_SplitJoin53_AnonFilter_a0_8599_8849_8935_8951_join[1], pop_complex(&SplitJoin74_SplitJoin59_SplitJoin59_AnonFilter_a0_8607_8853_8937_8953_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_8769() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin68_SplitJoin53_SplitJoin53_AnonFilter_a0_8599_8849_8935_8951_split[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8583_8808_8891_8947_split[1]));
		push_complex(&SplitJoin68_SplitJoin53_SplitJoin53_AnonFilter_a0_8599_8849_8935_8951_split[1], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8583_8808_8891_8947_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8770() {
	push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8583_8808_8891_8947_join[1], pop_complex(&SplitJoin68_SplitJoin53_SplitJoin53_AnonFilter_a0_8599_8849_8935_8951_join[0]));
	push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8583_8808_8891_8947_join[1], pop_complex(&SplitJoin68_SplitJoin53_SplitJoin53_AnonFilter_a0_8599_8849_8935_8951_join[0]));
	push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8583_8808_8891_8947_join[1], pop_complex(&SplitJoin68_SplitJoin53_SplitJoin53_AnonFilter_a0_8599_8849_8935_8951_join[1]));
	push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8583_8808_8891_8947_join[1], pop_complex(&SplitJoin68_SplitJoin53_SplitJoin53_AnonFilter_a0_8599_8849_8935_8951_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_8761() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8583_8808_8891_8947_split[0], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_8581_8807_8927_8946_split[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8583_8808_8891_8947_split[1], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_8581_8807_8927_8946_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8762() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_8581_8807_8927_8946_join[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8583_8808_8891_8947_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_8581_8807_8927_8946_join[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8583_8808_8891_8947_join[1]));
	ENDFOR
}

void Identity_8619() {
	Identity(&(SplitJoin82_SplitJoin67_SplitJoin67_AnonFilter_a0_8617_8858_8939_8956_split[0]), &(SplitJoin82_SplitJoin67_SplitJoin67_AnonFilter_a0_8617_8858_8939_8956_join[0]));
}

void Identity_8621() {
	Identity(&(SplitJoin82_SplitJoin67_SplitJoin67_AnonFilter_a0_8617_8858_8939_8956_split[1]), &(SplitJoin82_SplitJoin67_SplitJoin67_AnonFilter_a0_8617_8858_8939_8956_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_8779() {
	push_complex(&SplitJoin82_SplitJoin67_SplitJoin67_AnonFilter_a0_8617_8858_8939_8956_split[0], pop_complex(&SplitJoin80_SplitJoin65_SplitJoin65_AnonFilter_a0_8615_8857_8938_8955_split[0]));
	push_complex(&SplitJoin82_SplitJoin67_SplitJoin67_AnonFilter_a0_8617_8858_8939_8956_split[1], pop_complex(&SplitJoin80_SplitJoin65_SplitJoin65_AnonFilter_a0_8615_8857_8938_8955_split[0]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_8780() {
	push_complex(&SplitJoin80_SplitJoin65_SplitJoin65_AnonFilter_a0_8615_8857_8938_8955_join[0], pop_complex(&SplitJoin82_SplitJoin67_SplitJoin67_AnonFilter_a0_8617_8858_8939_8956_join[0]));
	push_complex(&SplitJoin80_SplitJoin65_SplitJoin65_AnonFilter_a0_8615_8857_8938_8955_join[0], pop_complex(&SplitJoin82_SplitJoin67_SplitJoin67_AnonFilter_a0_8617_8858_8939_8956_join[1]));
}

void Identity_8625() {
	Identity(&(SplitJoin86_SplitJoin71_SplitJoin71_AnonFilter_a0_8623_8861_8940_8957_split[0]), &(SplitJoin86_SplitJoin71_SplitJoin71_AnonFilter_a0_8623_8861_8940_8957_join[0]));
}

void Identity_8627() {
	Identity(&(SplitJoin86_SplitJoin71_SplitJoin71_AnonFilter_a0_8623_8861_8940_8957_split[1]), &(SplitJoin86_SplitJoin71_SplitJoin71_AnonFilter_a0_8623_8861_8940_8957_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_8781() {
	push_complex(&SplitJoin86_SplitJoin71_SplitJoin71_AnonFilter_a0_8623_8861_8940_8957_split[0], pop_complex(&SplitJoin80_SplitJoin65_SplitJoin65_AnonFilter_a0_8615_8857_8938_8955_split[1]));
	push_complex(&SplitJoin86_SplitJoin71_SplitJoin71_AnonFilter_a0_8623_8861_8940_8957_split[1], pop_complex(&SplitJoin80_SplitJoin65_SplitJoin65_AnonFilter_a0_8615_8857_8938_8955_split[1]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_8782() {
	push_complex(&SplitJoin80_SplitJoin65_SplitJoin65_AnonFilter_a0_8615_8857_8938_8955_join[1], pop_complex(&SplitJoin86_SplitJoin71_SplitJoin71_AnonFilter_a0_8623_8861_8940_8957_join[0]));
	push_complex(&SplitJoin80_SplitJoin65_SplitJoin65_AnonFilter_a0_8615_8857_8938_8955_join[1], pop_complex(&SplitJoin86_SplitJoin71_SplitJoin71_AnonFilter_a0_8623_8861_8940_8957_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_8777() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin80_SplitJoin65_SplitJoin65_AnonFilter_a0_8615_8857_8938_8955_split[0], pop_complex(&SplitJoin78_SplitJoin63_SplitJoin63_AnonFilter_a0_8613_8856_8892_8954_split[0]));
		push_complex(&SplitJoin80_SplitJoin65_SplitJoin65_AnonFilter_a0_8615_8857_8938_8955_split[1], pop_complex(&SplitJoin78_SplitJoin63_SplitJoin63_AnonFilter_a0_8613_8856_8892_8954_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8778() {
	push_complex(&SplitJoin78_SplitJoin63_SplitJoin63_AnonFilter_a0_8613_8856_8892_8954_join[0], pop_complex(&SplitJoin80_SplitJoin65_SplitJoin65_AnonFilter_a0_8615_8857_8938_8955_join[0]));
	push_complex(&SplitJoin78_SplitJoin63_SplitJoin63_AnonFilter_a0_8613_8856_8892_8954_join[0], pop_complex(&SplitJoin80_SplitJoin65_SplitJoin65_AnonFilter_a0_8615_8857_8938_8955_join[0]));
	push_complex(&SplitJoin78_SplitJoin63_SplitJoin63_AnonFilter_a0_8613_8856_8892_8954_join[0], pop_complex(&SplitJoin80_SplitJoin65_SplitJoin65_AnonFilter_a0_8615_8857_8938_8955_join[1]));
	push_complex(&SplitJoin78_SplitJoin63_SplitJoin63_AnonFilter_a0_8613_8856_8892_8954_join[0], pop_complex(&SplitJoin80_SplitJoin65_SplitJoin65_AnonFilter_a0_8615_8857_8938_8955_join[1]));
}

void Identity_8633() {
	Identity(&(SplitJoin92_SplitJoin77_SplitJoin77_AnonFilter_a0_8631_8865_8942_8959_split[0]), &(SplitJoin92_SplitJoin77_SplitJoin77_AnonFilter_a0_8631_8865_8942_8959_join[0]));
}

void Identity_8635() {
	Identity(&(SplitJoin92_SplitJoin77_SplitJoin77_AnonFilter_a0_8631_8865_8942_8959_split[1]), &(SplitJoin92_SplitJoin77_SplitJoin77_AnonFilter_a0_8631_8865_8942_8959_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_8785() {
	push_complex(&SplitJoin92_SplitJoin77_SplitJoin77_AnonFilter_a0_8631_8865_8942_8959_split[0], pop_complex(&SplitJoin90_SplitJoin75_SplitJoin75_AnonFilter_a0_8629_8864_8941_8958_split[0]));
	push_complex(&SplitJoin92_SplitJoin77_SplitJoin77_AnonFilter_a0_8631_8865_8942_8959_split[1], pop_complex(&SplitJoin90_SplitJoin75_SplitJoin75_AnonFilter_a0_8629_8864_8941_8958_split[0]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_8786() {
	push_complex(&SplitJoin90_SplitJoin75_SplitJoin75_AnonFilter_a0_8629_8864_8941_8958_join[0], pop_complex(&SplitJoin92_SplitJoin77_SplitJoin77_AnonFilter_a0_8631_8865_8942_8959_join[0]));
	push_complex(&SplitJoin90_SplitJoin75_SplitJoin75_AnonFilter_a0_8629_8864_8941_8958_join[0], pop_complex(&SplitJoin92_SplitJoin77_SplitJoin77_AnonFilter_a0_8631_8865_8942_8959_join[1]));
}

void Identity_8639() {
	Identity(&(SplitJoin96_SplitJoin81_SplitJoin81_AnonFilter_a0_8637_8868_8943_8960_split[0]), &(SplitJoin96_SplitJoin81_SplitJoin81_AnonFilter_a0_8637_8868_8943_8960_join[0]));
}

void Identity_8641() {
	Identity(&(SplitJoin96_SplitJoin81_SplitJoin81_AnonFilter_a0_8637_8868_8943_8960_split[1]), &(SplitJoin96_SplitJoin81_SplitJoin81_AnonFilter_a0_8637_8868_8943_8960_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_8787() {
	push_complex(&SplitJoin96_SplitJoin81_SplitJoin81_AnonFilter_a0_8637_8868_8943_8960_split[0], pop_complex(&SplitJoin90_SplitJoin75_SplitJoin75_AnonFilter_a0_8629_8864_8941_8958_split[1]));
	push_complex(&SplitJoin96_SplitJoin81_SplitJoin81_AnonFilter_a0_8637_8868_8943_8960_split[1], pop_complex(&SplitJoin90_SplitJoin75_SplitJoin75_AnonFilter_a0_8629_8864_8941_8958_split[1]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_8788() {
	push_complex(&SplitJoin90_SplitJoin75_SplitJoin75_AnonFilter_a0_8629_8864_8941_8958_join[1], pop_complex(&SplitJoin96_SplitJoin81_SplitJoin81_AnonFilter_a0_8637_8868_8943_8960_join[0]));
	push_complex(&SplitJoin90_SplitJoin75_SplitJoin75_AnonFilter_a0_8629_8864_8941_8958_join[1], pop_complex(&SplitJoin96_SplitJoin81_SplitJoin81_AnonFilter_a0_8637_8868_8943_8960_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_8783() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin90_SplitJoin75_SplitJoin75_AnonFilter_a0_8629_8864_8941_8958_split[0], pop_complex(&SplitJoin78_SplitJoin63_SplitJoin63_AnonFilter_a0_8613_8856_8892_8954_split[1]));
		push_complex(&SplitJoin90_SplitJoin75_SplitJoin75_AnonFilter_a0_8629_8864_8941_8958_split[1], pop_complex(&SplitJoin78_SplitJoin63_SplitJoin63_AnonFilter_a0_8613_8856_8892_8954_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8784() {
	push_complex(&SplitJoin78_SplitJoin63_SplitJoin63_AnonFilter_a0_8613_8856_8892_8954_join[1], pop_complex(&SplitJoin90_SplitJoin75_SplitJoin75_AnonFilter_a0_8629_8864_8941_8958_join[0]));
	push_complex(&SplitJoin78_SplitJoin63_SplitJoin63_AnonFilter_a0_8613_8856_8892_8954_join[1], pop_complex(&SplitJoin90_SplitJoin75_SplitJoin75_AnonFilter_a0_8629_8864_8941_8958_join[0]));
	push_complex(&SplitJoin78_SplitJoin63_SplitJoin63_AnonFilter_a0_8613_8856_8892_8954_join[1], pop_complex(&SplitJoin90_SplitJoin75_SplitJoin75_AnonFilter_a0_8629_8864_8941_8958_join[1]));
	push_complex(&SplitJoin78_SplitJoin63_SplitJoin63_AnonFilter_a0_8613_8856_8892_8954_join[1], pop_complex(&SplitJoin90_SplitJoin75_SplitJoin75_AnonFilter_a0_8629_8864_8941_8958_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_8775() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_complex(&SplitJoin78_SplitJoin63_SplitJoin63_AnonFilter_a0_8613_8856_8892_8954_split[0], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_8581_8807_8927_8946_split[1]));
		push_complex(&SplitJoin78_SplitJoin63_SplitJoin63_AnonFilter_a0_8613_8856_8892_8954_split[1], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_8581_8807_8927_8946_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8776() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_8581_8807_8927_8946_join[1], pop_complex(&SplitJoin78_SplitJoin63_SplitJoin63_AnonFilter_a0_8613_8856_8892_8954_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_8581_8807_8927_8946_join[1], pop_complex(&SplitJoin78_SplitJoin63_SplitJoin63_AnonFilter_a0_8613_8856_8892_8954_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8759() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_8581_8807_8927_8946_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8917WEIGHTED_ROUND_ROBIN_Splitter_8759));
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_8581_8807_8927_8946_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8917WEIGHTED_ROUND_ROBIN_Splitter_8759));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8760() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8760WEIGHTED_ROUND_ROBIN_Splitter_8903, pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_8581_8807_8927_8946_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8760WEIGHTED_ROUND_ROBIN_Splitter_8903, pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_8581_8807_8927_8946_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1(buffer_complex_t *chanin, buffer_complex_t *chanout) {
 {
 {
	int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
	FOR(int, _i, 0,  < , 2, _i++) {
		push_complex(&(*chanout), peek_complex(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
		iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
	}
	ENDFOR
}
}
}
	pop_complex(&(*chanin)) ; 
}


void Pre_CollapsedDataParallel_1_8736() {
	Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child0_8894_8962_split[0]), &(Pre_CollapsedDataParallel_1_8736butterfly_8643));
}

void butterfly(buffer_complex_t *chanin, buffer_complex_t *chanout) {
	complex_t WN1;
	complex_t WN2;
	complex_t one = ((complex_t) pop_complex(&(*chanin)));
	complex_t two = ((complex_t) pop_complex(&(*chanin)));
	complex_t __sa1;
	complex_t __sa2;
	WN1.real = 1.0 ; 
	WN1.imag = -0.0 ; 
	WN2.real = -1.0 ; 
	WN2.imag = 8.742278E-8 ; 
	__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
	__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
	push_complex(&(*chanout), __sa1) ; 
	__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
	__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
	push_complex(&(*chanout), __sa2) ; 
}


void butterfly_8643() {
	butterfly(&(Pre_CollapsedDataParallel_1_8736butterfly_8643), &(butterfly_8643Post_CollapsedDataParallel_2_8737));
}

void Post_CollapsedDataParallel_2(buffer_complex_t *chanin, buffer_complex_t *chanout) {
 {
 {
	FOR(int, _k, 0,  < , 2, _k++) {
 {
		push_complex(&(*chanout), peek_complex(&(*chanin), (_k + 0))) ; 
	}
	}
	ENDFOR
}
}
	pop_complex(&(*chanin)) ; 
}


void Post_CollapsedDataParallel_2_8737() {
	Post_CollapsedDataParallel_2(&(butterfly_8643Post_CollapsedDataParallel_2_8737), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child0_8894_8962_join[0]));
}

void Pre_CollapsedDataParallel_1_8739() {
	Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child0_8894_8962_split[1]), &(Pre_CollapsedDataParallel_1_8739butterfly_8644));
}

void butterfly_8644() {
	butterfly(&(Pre_CollapsedDataParallel_1_8739butterfly_8644), &(butterfly_8644Post_CollapsedDataParallel_2_8740));
}

void Post_CollapsedDataParallel_2_8740() {
	Post_CollapsedDataParallel_2(&(butterfly_8644Post_CollapsedDataParallel_2_8740), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child0_8894_8962_join[1]));
}

void Pre_CollapsedDataParallel_1_8742() {
	Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child0_8894_8962_split[2]), &(Pre_CollapsedDataParallel_1_8742butterfly_8645));
}

void butterfly_8645() {
	butterfly(&(Pre_CollapsedDataParallel_1_8742butterfly_8645), &(butterfly_8645Post_CollapsedDataParallel_2_8743));
}

void Post_CollapsedDataParallel_2_8743() {
	Post_CollapsedDataParallel_2(&(butterfly_8645Post_CollapsedDataParallel_2_8743), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child0_8894_8962_join[2]));
}

void Pre_CollapsedDataParallel_1_8745() {
	Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child0_8894_8962_split[3]), &(Pre_CollapsedDataParallel_1_8745butterfly_8646));
}

void butterfly_8646() {
	butterfly(&(Pre_CollapsedDataParallel_1_8745butterfly_8646), &(butterfly_8646Post_CollapsedDataParallel_2_8746));
}

void Post_CollapsedDataParallel_2_8746() {
	Post_CollapsedDataParallel_2(&(butterfly_8646Post_CollapsedDataParallel_2_8746), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child0_8894_8962_join[3]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_8904() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child0_8894_8962_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_Hier_8930_8961_split[0]));
		push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child0_8894_8962_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_Hier_8930_8961_split[0]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_8905() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_Hier_8930_8961_join[0], pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child0_8894_8962_join[__iter_]));
		push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_Hier_8930_8961_join[0], pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child0_8894_8962_join[__iter_]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_8748() {
	Pre_CollapsedDataParallel_1(&(SplitJoin57_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child1_8897_8963_split[0]), &(Pre_CollapsedDataParallel_1_8748butterfly_8647));
}

void butterfly_8647() {
	butterfly(&(Pre_CollapsedDataParallel_1_8748butterfly_8647), &(butterfly_8647Post_CollapsedDataParallel_2_8749));
}

void Post_CollapsedDataParallel_2_8749() {
	Post_CollapsedDataParallel_2(&(butterfly_8647Post_CollapsedDataParallel_2_8749), &(SplitJoin57_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child1_8897_8963_join[0]));
}

void Pre_CollapsedDataParallel_1_8751() {
	Pre_CollapsedDataParallel_1(&(SplitJoin57_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child1_8897_8963_split[1]), &(Pre_CollapsedDataParallel_1_8751butterfly_8648));
}

void butterfly_8648() {
	butterfly(&(Pre_CollapsedDataParallel_1_8751butterfly_8648), &(butterfly_8648Post_CollapsedDataParallel_2_8752));
}

void Post_CollapsedDataParallel_2_8752() {
	Post_CollapsedDataParallel_2(&(butterfly_8648Post_CollapsedDataParallel_2_8752), &(SplitJoin57_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child1_8897_8963_join[1]));
}

void Pre_CollapsedDataParallel_1_8754() {
	Pre_CollapsedDataParallel_1(&(SplitJoin57_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child1_8897_8963_split[2]), &(Pre_CollapsedDataParallel_1_8754butterfly_8649));
}

void butterfly_8649() {
	butterfly(&(Pre_CollapsedDataParallel_1_8754butterfly_8649), &(butterfly_8649Post_CollapsedDataParallel_2_8755));
}

void Post_CollapsedDataParallel_2_8755() {
	Post_CollapsedDataParallel_2(&(butterfly_8649Post_CollapsedDataParallel_2_8755), &(SplitJoin57_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child1_8897_8963_join[2]));
}

void Pre_CollapsedDataParallel_1_8757() {
	Pre_CollapsedDataParallel_1(&(SplitJoin57_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child1_8897_8963_split[3]), &(Pre_CollapsedDataParallel_1_8757butterfly_8650));
}

void butterfly_8650() {
	butterfly(&(Pre_CollapsedDataParallel_1_8757butterfly_8650), &(butterfly_8650Post_CollapsedDataParallel_2_8758));
}

void Post_CollapsedDataParallel_2_8758() {
	Post_CollapsedDataParallel_2(&(butterfly_8650Post_CollapsedDataParallel_2_8758), &(SplitJoin57_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child1_8897_8963_join[3]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_8906() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin57_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child1_8897_8963_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_Hier_8930_8961_split[1]));
		push_complex(&SplitJoin57_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child1_8897_8963_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_Hier_8930_8961_split[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_8907() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_Hier_8930_8961_join[1], pop_complex(&SplitJoin57_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child1_8897_8963_join[__iter_]));
		push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_Hier_8930_8961_join[1], pop_complex(&SplitJoin57_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child1_8897_8963_join[__iter_]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8903() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_Hier_8930_8961_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8760WEIGHTED_ROUND_ROBIN_Splitter_8903));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_Hier_8930_8961_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8760WEIGHTED_ROUND_ROBIN_Splitter_8903));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_8908() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8908WEIGHTED_ROUND_ROBIN_Splitter_8909, pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_Hier_8930_8961_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8908WEIGHTED_ROUND_ROBIN_Splitter_8909, pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_Hier_8930_8961_join[1]));
	ENDFOR
}

void butterfly_8652() {
	butterfly(&(SplitJoin18_SplitJoin12_SplitJoin12_split2_8562_8815_8893_8966_split[0]), &(SplitJoin18_SplitJoin12_SplitJoin12_split2_8562_8815_8893_8966_join[0]));
}

void butterfly_8653() {
	butterfly(&(SplitJoin18_SplitJoin12_SplitJoin12_split2_8562_8815_8893_8966_split[1]), &(SplitJoin18_SplitJoin12_SplitJoin12_split2_8562_8815_8893_8966_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_8793() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_8562_8815_8893_8966_split[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child0_8900_8965_split[0]));
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_8562_8815_8893_8966_split[1], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child0_8900_8965_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8794() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child0_8900_8965_join[0], pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_8562_8815_8893_8966_join[0]));
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child0_8900_8965_join[0], pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_8562_8815_8893_8966_join[1]));
	ENDFOR
}}

void butterfly_8654() {
	butterfly(&(SplitJoin40_SplitJoin29_SplitJoin29_split2_8564_8829_8895_8967_split[0]), &(SplitJoin40_SplitJoin29_SplitJoin29_split2_8564_8829_8895_8967_join[0]));
}

void butterfly_8655() {
	butterfly(&(SplitJoin40_SplitJoin29_SplitJoin29_split2_8564_8829_8895_8967_split[1]), &(SplitJoin40_SplitJoin29_SplitJoin29_split2_8564_8829_8895_8967_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_8795() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin40_SplitJoin29_SplitJoin29_split2_8564_8829_8895_8967_split[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child0_8900_8965_split[1]));
		push_complex(&SplitJoin40_SplitJoin29_SplitJoin29_split2_8564_8829_8895_8967_split[1], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child0_8900_8965_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8796() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child0_8900_8965_join[1], pop_complex(&SplitJoin40_SplitJoin29_SplitJoin29_split2_8564_8829_8895_8967_join[0]));
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child0_8900_8965_join[1], pop_complex(&SplitJoin40_SplitJoin29_SplitJoin29_split2_8564_8829_8895_8967_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_8910() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child0_8900_8965_split[0], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_Hier_8931_8964_split[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child0_8900_8965_split[1], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_Hier_8931_8964_split[0]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_8911() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_Hier_8931_8964_join[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child0_8900_8965_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_Hier_8931_8964_join[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child0_8900_8965_join[1]));
	ENDFOR
}

void butterfly_8656() {
	butterfly(&(SplitJoin46_SplitJoin33_SplitJoin33_split2_8566_8832_8896_8969_split[0]), &(SplitJoin46_SplitJoin33_SplitJoin33_split2_8566_8832_8896_8969_join[0]));
}

void butterfly_8657() {
	butterfly(&(SplitJoin46_SplitJoin33_SplitJoin33_split2_8566_8832_8896_8969_split[1]), &(SplitJoin46_SplitJoin33_SplitJoin33_split2_8566_8832_8896_8969_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_8797() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin46_SplitJoin33_SplitJoin33_split2_8566_8832_8896_8969_split[0], pop_complex(&SplitJoin44_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child1_8902_8968_split[0]));
		push_complex(&SplitJoin46_SplitJoin33_SplitJoin33_split2_8566_8832_8896_8969_split[1], pop_complex(&SplitJoin44_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child1_8902_8968_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8798() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin44_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child1_8902_8968_join[0], pop_complex(&SplitJoin46_SplitJoin33_SplitJoin33_split2_8566_8832_8896_8969_join[0]));
		push_complex(&SplitJoin44_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child1_8902_8968_join[0], pop_complex(&SplitJoin46_SplitJoin33_SplitJoin33_split2_8566_8832_8896_8969_join[1]));
	ENDFOR
}}

void butterfly_8658() {
	butterfly(&(SplitJoin50_SplitJoin37_SplitJoin37_split2_8568_8835_8898_8970_split[0]), &(SplitJoin50_SplitJoin37_SplitJoin37_split2_8568_8835_8898_8970_join[0]));
}

void butterfly_8659() {
	butterfly(&(SplitJoin50_SplitJoin37_SplitJoin37_split2_8568_8835_8898_8970_split[1]), &(SplitJoin50_SplitJoin37_SplitJoin37_split2_8568_8835_8898_8970_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_8799() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin50_SplitJoin37_SplitJoin37_split2_8568_8835_8898_8970_split[0], pop_complex(&SplitJoin44_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child1_8902_8968_split[1]));
		push_complex(&SplitJoin50_SplitJoin37_SplitJoin37_split2_8568_8835_8898_8970_split[1], pop_complex(&SplitJoin44_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child1_8902_8968_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8800() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin44_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child1_8902_8968_join[1], pop_complex(&SplitJoin50_SplitJoin37_SplitJoin37_split2_8568_8835_8898_8970_join[0]));
		push_complex(&SplitJoin44_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child1_8902_8968_join[1], pop_complex(&SplitJoin50_SplitJoin37_SplitJoin37_split2_8568_8835_8898_8970_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_8912() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin44_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child1_8902_8968_split[0], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_Hier_8931_8964_split[1]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin44_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child1_8902_8968_split[1], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_Hier_8931_8964_split[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_8913() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_Hier_8931_8964_join[1], pop_complex(&SplitJoin44_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child1_8902_8968_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_Hier_8931_8964_join[1], pop_complex(&SplitJoin44_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child1_8902_8968_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8909() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_Hier_8931_8964_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8908WEIGHTED_ROUND_ROBIN_Splitter_8909));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_Hier_8931_8964_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8908WEIGHTED_ROUND_ROBIN_Splitter_8909));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_8914() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8914WEIGHTED_ROUND_ROBIN_Splitter_8801, pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_Hier_8931_8964_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8914WEIGHTED_ROUND_ROBIN_Splitter_8801, pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_Hier_8931_8964_join[1]));
	ENDFOR
}

void butterfly_8661() {
	butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_8575_8818_8899_8972_split[0]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_8575_8818_8899_8972_join[0]));
}

void butterfly_8662() {
	butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_8575_8818_8899_8972_split[1]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_8575_8818_8899_8972_join[1]));
}

void butterfly_8663() {
	butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_8575_8818_8899_8972_split[2]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_8575_8818_8899_8972_join[2]));
}

void butterfly_8664() {
	butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_8575_8818_8899_8972_split[3]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_8575_8818_8899_8972_join[3]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_8803() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_8575_8818_8899_8972_split[__iter_], pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_8573_8817_8932_8971_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8804() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_8573_8817_8932_8971_join[0], pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_8575_8818_8899_8972_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void butterfly_8665() {
	butterfly(&(SplitJoin33_SplitJoin22_SplitJoin22_split2_8577_8823_8901_8973_split[0]), &(SplitJoin33_SplitJoin22_SplitJoin22_split2_8577_8823_8901_8973_join[0]));
}

void butterfly_8666() {
	butterfly(&(SplitJoin33_SplitJoin22_SplitJoin22_split2_8577_8823_8901_8973_split[1]), &(SplitJoin33_SplitJoin22_SplitJoin22_split2_8577_8823_8901_8973_join[1]));
}

void butterfly_8667() {
	butterfly(&(SplitJoin33_SplitJoin22_SplitJoin22_split2_8577_8823_8901_8973_split[2]), &(SplitJoin33_SplitJoin22_SplitJoin22_split2_8577_8823_8901_8973_join[2]));
}

void butterfly_8668() {
	butterfly(&(SplitJoin33_SplitJoin22_SplitJoin22_split2_8577_8823_8901_8973_split[3]), &(SplitJoin33_SplitJoin22_SplitJoin22_split2_8577_8823_8901_8973_join[3]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_8805() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin33_SplitJoin22_SplitJoin22_split2_8577_8823_8901_8973_split[__iter_], pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_8573_8817_8932_8971_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8806() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_8573_8817_8932_8971_join[1], pop_complex(&SplitJoin33_SplitJoin22_SplitJoin22_split2_8577_8823_8901_8973_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_8801() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_8573_8817_8932_8971_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8914WEIGHTED_ROUND_ROBIN_Splitter_8801));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_8573_8817_8932_8971_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8914WEIGHTED_ROUND_ROBIN_Splitter_8801));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_8802() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8802WEIGHTED_ROUND_ROBIN_Splitter_8920, pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_8573_8817_8932_8971_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8802WEIGHTED_ROUND_ROBIN_Splitter_8920, pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_8573_8817_8932_8971_join[1]));
	ENDFOR
}

void magnitude(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		push_float(&(*chanout), ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}


void magnitude_8922() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_8933_8974_split[0]), &(SplitJoin24_magnitude_Fiss_8933_8974_join[0]));
	ENDFOR
}

void magnitude_8923() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_8933_8974_split[1]), &(SplitJoin24_magnitude_Fiss_8933_8974_join[1]));
	ENDFOR
}

void magnitude_8924() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_8933_8974_split[2]), &(SplitJoin24_magnitude_Fiss_8933_8974_join[2]));
	ENDFOR
}

void magnitude_8925() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_8933_8974_split[3]), &(SplitJoin24_magnitude_Fiss_8933_8974_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8920() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin24_magnitude_Fiss_8933_8974_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8802WEIGHTED_ROUND_ROBIN_Splitter_8920));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8921() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8921sink_8670, pop_float(&SplitJoin24_magnitude_Fiss_8933_8974_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void sink(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void sink_8670() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		sink(&(WEIGHTED_ROUND_ROBIN_Joiner_8921sink_8670));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_complex(&SplitJoin70_SplitJoin55_SplitJoin55_AnonFilter_a0_8601_8850_8936_8952_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 2, __iter_init_1_++)
		init_buffer_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_8587_8810_8929_8949_split[__iter_init_1_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_8739butterfly_8644);
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8583_8808_8891_8947_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 2, __iter_init_3_++)
		init_buffer_complex(&SplitJoin78_SplitJoin63_SplitJoin63_AnonFilter_a0_8613_8856_8892_8954_split[__iter_init_3_]);
	ENDFOR
	init_buffer_complex(&butterfly_8643Post_CollapsedDataParallel_2_8737);
	init_buffer_complex(&Pre_CollapsedDataParallel_1_8742butterfly_8645);
	FOR(int, __iter_init_4_, 0, <, 2, __iter_init_4_++)
		init_buffer_complex(&SplitJoin82_SplitJoin67_SplitJoin67_AnonFilter_a0_8617_8858_8939_8956_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 2, __iter_init_5_++)
		init_buffer_complex(&SplitJoin86_SplitJoin71_SplitJoin71_AnonFilter_a0_8623_8861_8940_8957_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 2, __iter_init_6_++)
		init_buffer_complex(&SplitJoin80_SplitJoin65_SplitJoin65_AnonFilter_a0_8615_8857_8938_8955_split[__iter_init_6_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8917WEIGHTED_ROUND_ROBIN_Splitter_8759);
	FOR(int, __iter_init_7_, 0, <, 2, __iter_init_7_++)
		init_buffer_complex(&SplitJoin70_SplitJoin55_SplitJoin55_AnonFilter_a0_8601_8850_8936_8952_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_complex(&SplitJoin46_SplitJoin33_SplitJoin33_split2_8566_8832_8896_8969_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 2, __iter_init_9_++)
		init_buffer_complex(&SplitJoin44_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child1_8902_8968_join[__iter_init_9_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_8751butterfly_8648);
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_complex(&SplitJoin90_SplitJoin75_SplitJoin75_AnonFilter_a0_8629_8864_8941_8958_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 4, __iter_init_11_++)
		init_buffer_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child0_8894_8962_split[__iter_init_11_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8760WEIGHTED_ROUND_ROBIN_Splitter_8903);
	FOR(int, __iter_init_12_, 0, <, 2, __iter_init_12_++)
		init_buffer_complex(&SplitJoin90_SplitJoin75_SplitJoin75_AnonFilter_a0_8629_8864_8941_8958_split[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_Hier_8930_8961_split[__iter_init_13_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_8754butterfly_8649);
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_complex(&SplitJoin78_SplitJoin63_SplitJoin63_AnonFilter_a0_8613_8856_8892_8954_join[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 2, __iter_init_15_++)
		init_buffer_complex(&SplitJoin80_SplitJoin65_SplitJoin65_AnonFilter_a0_8615_8857_8938_8955_join[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_complex(&SplitJoin50_SplitJoin37_SplitJoin37_split2_8568_8835_8898_8970_join[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8585_8809_8928_8948_join[__iter_init_17_]);
	ENDFOR
	init_buffer_complex(&butterfly_8644Post_CollapsedDataParallel_2_8740);
	init_buffer_complex(&Pre_CollapsedDataParallel_1_8757butterfly_8650);
	FOR(int, __iter_init_18_, 0, <, 4, __iter_init_18_++)
		init_buffer_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_8575_8818_8899_8972_join[__iter_init_18_]);
	ENDFOR
	FOR(int, __iter_init_19_, 0, <, 2, __iter_init_19_++)
		init_buffer_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_8581_8807_8927_8946_join[__iter_init_19_]);
	ENDFOR
	init_buffer_complex(&butterfly_8646Post_CollapsedDataParallel_2_8746);
	FOR(int, __iter_init_20_, 0, <, 2, __iter_init_20_++)
		init_buffer_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_Hier_8931_8964_join[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 2, __iter_init_21_++)
		init_buffer_complex(&SplitJoin46_SplitJoin33_SplitJoin33_split2_8566_8832_8896_8969_split[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 2, __iter_init_22_++)
		init_buffer_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_8585_8809_8928_8948_split[__iter_init_22_]);
	ENDFOR
	FOR(int, __iter_init_23_, 0, <, 2, __iter_init_23_++)
		init_buffer_complex(&SplitJoin82_SplitJoin67_SplitJoin67_AnonFilter_a0_8617_8858_8939_8956_split[__iter_init_23_]);
	ENDFOR
	FOR(int, __iter_init_24_, 0, <, 2, __iter_init_24_++)
		init_buffer_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_8562_8815_8893_8966_split[__iter_init_24_]);
	ENDFOR
	FOR(int, __iter_init_25_, 0, <, 2, __iter_init_25_++)
		init_buffer_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_8573_8817_8932_8971_split[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 4, __iter_init_26_++)
		init_buffer_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_8575_8818_8899_8972_split[__iter_init_26_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_8748butterfly_8647);
	FOR(int, __iter_init_27_, 0, <, 2, __iter_init_27_++)
		init_buffer_complex(&SplitJoin64_SplitJoin49_SplitJoin49_AnonFilter_a0_8593_8846_8934_8950_split[__iter_init_27_]);
	ENDFOR
	init_buffer_complex(&butterfly_8648Post_CollapsedDataParallel_2_8752);
	FOR(int, __iter_init_28_, 0, <, 2, __iter_init_28_++)
		init_buffer_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_Hier_8931_8964_split[__iter_init_28_]);
	ENDFOR
	init_buffer_complex(&butterfly_8649Post_CollapsedDataParallel_2_8755);
	FOR(int, __iter_init_29_, 0, <, 2, __iter_init_29_++)
		init_buffer_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_8581_8807_8927_8946_split[__iter_init_29_]);
	ENDFOR
	FOR(int, __iter_init_30_, 0, <, 2, __iter_init_30_++)
		init_buffer_complex(&SplitJoin68_SplitJoin53_SplitJoin53_AnonFilter_a0_8599_8849_8935_8951_split[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 2, __iter_init_31_++)
		init_buffer_complex(&SplitJoin68_SplitJoin53_SplitJoin53_AnonFilter_a0_8599_8849_8935_8951_join[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 4, __iter_init_32_++)
		init_buffer_complex(&SplitJoin57_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child1_8897_8963_split[__iter_init_32_]);
	ENDFOR
	init_buffer_complex(&butterfly_8650Post_CollapsedDataParallel_2_8758);
	init_buffer_complex(&Pre_CollapsedDataParallel_1_8736butterfly_8643);
	FOR(int, __iter_init_33_, 0, <, 2, __iter_init_33_++)
		init_buffer_complex(&SplitJoin74_SplitJoin59_SplitJoin59_AnonFilter_a0_8607_8853_8937_8953_split[__iter_init_33_]);
	ENDFOR
	FOR(int, __iter_init_34_, 0, <, 2, __iter_init_34_++)
		init_buffer_complex(&SplitJoin86_SplitJoin71_SplitJoin71_AnonFilter_a0_8623_8861_8940_8957_split[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 2, __iter_init_35_++)
		init_buffer_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_8562_8815_8893_8966_join[__iter_init_35_]);
	ENDFOR
	FOR(int, __iter_init_36_, 0, <, 2, __iter_init_36_++)
		init_buffer_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_8573_8817_8932_8971_join[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 2, __iter_init_37_++)
		init_buffer_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child0_8900_8965_split[__iter_init_37_]);
	ENDFOR
	FOR(int, __iter_init_38_, 0, <, 2, __iter_init_38_++)
		init_buffer_complex(&SplitJoin96_SplitJoin81_SplitJoin81_AnonFilter_a0_8637_8868_8943_8960_split[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 2, __iter_init_39_++)
		init_buffer_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child0_8900_8965_join[__iter_init_39_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_8745butterfly_8646);
	FOR(int, __iter_init_40_, 0, <, 4, __iter_init_40_++)
		init_buffer_complex(&SplitJoin33_SplitJoin22_SplitJoin22_split2_8577_8823_8901_8973_split[__iter_init_40_]);
	ENDFOR
	FOR(int, __iter_init_41_, 0, <, 2, __iter_init_41_++)
		init_buffer_complex(&SplitJoin40_SplitJoin29_SplitJoin29_split2_8564_8829_8895_8967_split[__iter_init_41_]);
	ENDFOR
	FOR(int, __iter_init_42_, 0, <, 2, __iter_init_42_++)
		init_buffer_complex(&SplitJoin64_SplitJoin49_SplitJoin49_AnonFilter_a0_8593_8846_8934_8950_join[__iter_init_42_]);
	ENDFOR
	FOR(int, __iter_init_43_, 0, <, 4, __iter_init_43_++)
		init_buffer_float(&SplitJoin24_magnitude_Fiss_8933_8974_join[__iter_init_43_]);
	ENDFOR
	FOR(int, __iter_init_44_, 0, <, 2, __iter_init_44_++)
		init_buffer_complex(&SplitJoin44_SplitJoin10_SplitJoin10_split1_8560_8814_Hier_child1_8902_8968_split[__iter_init_44_]);
	ENDFOR
	FOR(int, __iter_init_45_, 0, <, 2, __iter_init_45_++)
		init_buffer_complex(&SplitJoin40_SplitJoin29_SplitJoin29_split2_8564_8829_8895_8967_join[__iter_init_45_]);
	ENDFOR
	FOR(int, __iter_init_46_, 0, <, 2, __iter_init_46_++)
		init_buffer_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_Hier_8930_8961_join[__iter_init_46_]);
	ENDFOR
	FOR(int, __iter_init_47_, 0, <, 2, __iter_init_47_++)
		init_buffer_complex(&SplitJoin74_SplitJoin59_SplitJoin59_AnonFilter_a0_8607_8853_8937_8953_join[__iter_init_47_]);
	ENDFOR
	FOR(int, __iter_init_48_, 0, <, 2, __iter_init_48_++)
		init_buffer_complex(&SplitJoin92_SplitJoin77_SplitJoin77_AnonFilter_a0_8631_8865_8942_8959_split[__iter_init_48_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8908WEIGHTED_ROUND_ROBIN_Splitter_8909);
	init_buffer_complex(&butterfly_8645Post_CollapsedDataParallel_2_8743);
	FOR(int, __iter_init_49_, 0, <, 4, __iter_init_49_++)
		init_buffer_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child0_8894_8962_join[__iter_init_49_]);
	ENDFOR
	init_buffer_complex(&butterfly_8647Post_CollapsedDataParallel_2_8749);
	FOR(int, __iter_init_50_, 0, <, 2, __iter_init_50_++)
		init_buffer_complex(&SplitJoin92_SplitJoin77_SplitJoin77_AnonFilter_a0_8631_8865_8942_8959_join[__iter_init_50_]);
	ENDFOR
	FOR(int, __iter_init_51_, 0, <, 4, __iter_init_51_++)
		init_buffer_complex(&SplitJoin33_SplitJoin22_SplitJoin22_split2_8577_8823_8901_8973_join[__iter_init_51_]);
	ENDFOR
	FOR(int, __iter_init_52_, 0, <, 4, __iter_init_52_++)
		init_buffer_complex(&SplitJoin24_magnitude_Fiss_8933_8974_split[__iter_init_52_]);
	ENDFOR
	FOR(int, __iter_init_53_, 0, <, 2, __iter_init_53_++)
		init_buffer_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_8587_8810_8929_8949_join[__iter_init_53_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8921sink_8670);
	FOR(int, __iter_init_54_, 0, <, 2, __iter_init_54_++)
		init_buffer_complex(&SplitJoin96_SplitJoin81_SplitJoin81_AnonFilter_a0_8637_8868_8943_8960_join[__iter_init_54_]);
	ENDFOR
	FOR(int, __iter_init_55_, 0, <, 4, __iter_init_55_++)
		init_buffer_complex(&SplitJoin57_SplitJoin8_SplitJoin8_split1_8539_8812_Hier_child1_8897_8963_join[__iter_init_55_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8914WEIGHTED_ROUND_ROBIN_Splitter_8801);
	FOR(int, __iter_init_56_, 0, <, 2, __iter_init_56_++)
		init_buffer_complex(&SplitJoin0_source_Fiss_8926_8945_join[__iter_init_56_]);
	ENDFOR
	FOR(int, __iter_init_57_, 0, <, 2, __iter_init_57_++)
		init_buffer_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_8583_8808_8891_8947_join[__iter_init_57_]);
	ENDFOR
	FOR(int, __iter_init_58_, 0, <, 2, __iter_init_58_++)
		init_buffer_complex(&SplitJoin0_source_Fiss_8926_8945_split[__iter_init_58_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_8802WEIGHTED_ROUND_ROBIN_Splitter_8920);
	FOR(int, __iter_init_59_, 0, <, 2, __iter_init_59_++)
		init_buffer_complex(&SplitJoin50_SplitJoin37_SplitJoin37_split2_8568_8835_8898_8970_split[__iter_init_59_]);
	ENDFOR
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_8916();
			source_8918();
			source_8919();
		WEIGHTED_ROUND_ROBIN_Joiner_8917();
		WEIGHTED_ROUND_ROBIN_Splitter_8759();
			WEIGHTED_ROUND_ROBIN_Splitter_8761();
				WEIGHTED_ROUND_ROBIN_Splitter_8763();
					WEIGHTED_ROUND_ROBIN_Splitter_8765();
						Identity_8589();
						Identity_8591();
					WEIGHTED_ROUND_ROBIN_Joiner_8766();
					WEIGHTED_ROUND_ROBIN_Splitter_8767();
						Identity_8595();
						Identity_8597();
					WEIGHTED_ROUND_ROBIN_Joiner_8768();
				WEIGHTED_ROUND_ROBIN_Joiner_8764();
				WEIGHTED_ROUND_ROBIN_Splitter_8769();
					WEIGHTED_ROUND_ROBIN_Splitter_8771();
						Identity_8603();
						Identity_8605();
					WEIGHTED_ROUND_ROBIN_Joiner_8772();
					WEIGHTED_ROUND_ROBIN_Splitter_8773();
						Identity_8609();
						Identity_8611();
					WEIGHTED_ROUND_ROBIN_Joiner_8774();
				WEIGHTED_ROUND_ROBIN_Joiner_8770();
			WEIGHTED_ROUND_ROBIN_Joiner_8762();
			WEIGHTED_ROUND_ROBIN_Splitter_8775();
				WEIGHTED_ROUND_ROBIN_Splitter_8777();
					WEIGHTED_ROUND_ROBIN_Splitter_8779();
						Identity_8619();
						Identity_8621();
					WEIGHTED_ROUND_ROBIN_Joiner_8780();
					WEIGHTED_ROUND_ROBIN_Splitter_8781();
						Identity_8625();
						Identity_8627();
					WEIGHTED_ROUND_ROBIN_Joiner_8782();
				WEIGHTED_ROUND_ROBIN_Joiner_8778();
				WEIGHTED_ROUND_ROBIN_Splitter_8783();
					WEIGHTED_ROUND_ROBIN_Splitter_8785();
						Identity_8633();
						Identity_8635();
					WEIGHTED_ROUND_ROBIN_Joiner_8786();
					WEIGHTED_ROUND_ROBIN_Splitter_8787();
						Identity_8639();
						Identity_8641();
					WEIGHTED_ROUND_ROBIN_Joiner_8788();
				WEIGHTED_ROUND_ROBIN_Joiner_8784();
			WEIGHTED_ROUND_ROBIN_Joiner_8776();
		WEIGHTED_ROUND_ROBIN_Joiner_8760();
		WEIGHTED_ROUND_ROBIN_Splitter_8903();
			WEIGHTED_ROUND_ROBIN_Splitter_8904();
				Pre_CollapsedDataParallel_1_8736();
				butterfly_8643();
				Post_CollapsedDataParallel_2_8737();
				Pre_CollapsedDataParallel_1_8739();
				butterfly_8644();
				Post_CollapsedDataParallel_2_8740();
				Pre_CollapsedDataParallel_1_8742();
				butterfly_8645();
				Post_CollapsedDataParallel_2_8743();
				Pre_CollapsedDataParallel_1_8745();
				butterfly_8646();
				Post_CollapsedDataParallel_2_8746();
			WEIGHTED_ROUND_ROBIN_Joiner_8905();
			WEIGHTED_ROUND_ROBIN_Splitter_8906();
				Pre_CollapsedDataParallel_1_8748();
				butterfly_8647();
				Post_CollapsedDataParallel_2_8749();
				Pre_CollapsedDataParallel_1_8751();
				butterfly_8648();
				Post_CollapsedDataParallel_2_8752();
				Pre_CollapsedDataParallel_1_8754();
				butterfly_8649();
				Post_CollapsedDataParallel_2_8755();
				Pre_CollapsedDataParallel_1_8757();
				butterfly_8650();
				Post_CollapsedDataParallel_2_8758();
			WEIGHTED_ROUND_ROBIN_Joiner_8907();
		WEIGHTED_ROUND_ROBIN_Joiner_8908();
		WEIGHTED_ROUND_ROBIN_Splitter_8909();
			WEIGHTED_ROUND_ROBIN_Splitter_8910();
				WEIGHTED_ROUND_ROBIN_Splitter_8793();
					butterfly_8652();
					butterfly_8653();
				WEIGHTED_ROUND_ROBIN_Joiner_8794();
				WEIGHTED_ROUND_ROBIN_Splitter_8795();
					butterfly_8654();
					butterfly_8655();
				WEIGHTED_ROUND_ROBIN_Joiner_8796();
			WEIGHTED_ROUND_ROBIN_Joiner_8911();
			WEIGHTED_ROUND_ROBIN_Splitter_8912();
				WEIGHTED_ROUND_ROBIN_Splitter_8797();
					butterfly_8656();
					butterfly_8657();
				WEIGHTED_ROUND_ROBIN_Joiner_8798();
				WEIGHTED_ROUND_ROBIN_Splitter_8799();
					butterfly_8658();
					butterfly_8659();
				WEIGHTED_ROUND_ROBIN_Joiner_8800();
			WEIGHTED_ROUND_ROBIN_Joiner_8913();
		WEIGHTED_ROUND_ROBIN_Joiner_8914();
		WEIGHTED_ROUND_ROBIN_Splitter_8801();
			WEIGHTED_ROUND_ROBIN_Splitter_8803();
				butterfly_8661();
				butterfly_8662();
				butterfly_8663();
				butterfly_8664();
			WEIGHTED_ROUND_ROBIN_Joiner_8804();
			WEIGHTED_ROUND_ROBIN_Splitter_8805();
				butterfly_8665();
				butterfly_8666();
				butterfly_8667();
				butterfly_8668();
			WEIGHTED_ROUND_ROBIN_Joiner_8806();
		WEIGHTED_ROUND_ROBIN_Joiner_8802();
		WEIGHTED_ROUND_ROBIN_Splitter_8920();
			magnitude_8922();
			magnitude_8923();
			magnitude_8924();
			magnitude_8925();
		WEIGHTED_ROUND_ROBIN_Joiner_8921();
		sink_8670();
	ENDFOR
	return EXIT_SUCCESS;
}
