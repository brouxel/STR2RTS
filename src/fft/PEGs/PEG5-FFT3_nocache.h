#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=640 on the compile command line
#else
#if BUF_SIZEMAX < 640
#error BUF_SIZEMAX too small, it must be at least 640
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float A_re[32];
	float A_im[32];
	int idx;
} FloatSource_9452_t;
void FloatSource_9452();
void Pre_CollapsedDataParallel_1_9753();
void WEIGHTED_ROUND_ROBIN_Splitter_9895();
void Butterfly_9897();
void Butterfly_9898();
void Butterfly_9899();
void Butterfly_9900();
void Butterfly_9901();
void WEIGHTED_ROUND_ROBIN_Joiner_9896();
void Post_CollapsedDataParallel_2_9754();
void WEIGHTED_ROUND_ROBIN_Splitter_9797();
void Pre_CollapsedDataParallel_1_9756();
void WEIGHTED_ROUND_ROBIN_Splitter_9902();
void Butterfly_9904();
void Butterfly_9905();
void Butterfly_9906();
void Butterfly_9907();
void Butterfly_9908();
void WEIGHTED_ROUND_ROBIN_Joiner_9903();
void Post_CollapsedDataParallel_2_9757();
void WEIGHTED_ROUND_ROBIN_Splitter_9877();
void Pre_CollapsedDataParallel_1_9762();
void WEIGHTED_ROUND_ROBIN_Splitter_9909();
void Butterfly_9911();
void Butterfly_9912();
void Butterfly_9913();
void Butterfly_9914();
void WEIGHTED_ROUND_ROBIN_Joiner_9910();
void Post_CollapsedDataParallel_2_9763();
void Pre_CollapsedDataParallel_1_9765();
void WEIGHTED_ROUND_ROBIN_Splitter_9915();
void Butterfly_9917();
void Butterfly_9918();
void Butterfly_9919();
void Butterfly_9920();
void WEIGHTED_ROUND_ROBIN_Joiner_9916();
void Post_CollapsedDataParallel_2_9766();
void WEIGHTED_ROUND_ROBIN_Joiner_9878();
void Pre_CollapsedDataParallel_1_9759();
void WEIGHTED_ROUND_ROBIN_Splitter_9921();
void Butterfly_9923();
void Butterfly_9924();
void Butterfly_9925();
void Butterfly_9926();
void Butterfly_9927();
void WEIGHTED_ROUND_ROBIN_Joiner_9922();
void Post_CollapsedDataParallel_2_9760();
void WEIGHTED_ROUND_ROBIN_Splitter_9879();
void Pre_CollapsedDataParallel_1_9768();
void WEIGHTED_ROUND_ROBIN_Splitter_9928();
void Butterfly_9930();
void Butterfly_9931();
void Butterfly_9932();
void Butterfly_9933();
void WEIGHTED_ROUND_ROBIN_Joiner_9929();
void Post_CollapsedDataParallel_2_9769();
void Pre_CollapsedDataParallel_1_9771();
void WEIGHTED_ROUND_ROBIN_Splitter_9934();
void Butterfly_9936();
void Butterfly_9937();
void Butterfly_9938();
void Butterfly_9939();
void WEIGHTED_ROUND_ROBIN_Joiner_9935();
void Post_CollapsedDataParallel_2_9772();
void WEIGHTED_ROUND_ROBIN_Joiner_9880();
void WEIGHTED_ROUND_ROBIN_Joiner_9881();
void WEIGHTED_ROUND_ROBIN_Splitter_9882();
void WEIGHTED_ROUND_ROBIN_Splitter_9883();
void Pre_CollapsedDataParallel_1_9774();
void WEIGHTED_ROUND_ROBIN_Splitter_9940();
void Butterfly_9942();
void Butterfly_9943();
void WEIGHTED_ROUND_ROBIN_Joiner_9941();
void Post_CollapsedDataParallel_2_9775();
void Pre_CollapsedDataParallel_1_9777();
void WEIGHTED_ROUND_ROBIN_Splitter_9944();
void Butterfly_9946();
void Butterfly_9947();
void WEIGHTED_ROUND_ROBIN_Joiner_9945();
void Post_CollapsedDataParallel_2_9778();
void Pre_CollapsedDataParallel_1_9780();
void WEIGHTED_ROUND_ROBIN_Splitter_9948();
void Butterfly_9950();
void Butterfly_9951();
void WEIGHTED_ROUND_ROBIN_Joiner_9949();
void Post_CollapsedDataParallel_2_9781();
void Pre_CollapsedDataParallel_1_9783();
void WEIGHTED_ROUND_ROBIN_Splitter_9952();
void Butterfly_9954();
void Butterfly_9955();
void WEIGHTED_ROUND_ROBIN_Joiner_9953();
void Post_CollapsedDataParallel_2_9784();
void WEIGHTED_ROUND_ROBIN_Joiner_9884();
void WEIGHTED_ROUND_ROBIN_Splitter_9885();
void Pre_CollapsedDataParallel_1_9786();
void WEIGHTED_ROUND_ROBIN_Splitter_9956();
void Butterfly_9958();
void Butterfly_9959();
void WEIGHTED_ROUND_ROBIN_Joiner_9957();
void Post_CollapsedDataParallel_2_9787();
void Pre_CollapsedDataParallel_1_9789();
void WEIGHTED_ROUND_ROBIN_Splitter_9960();
void Butterfly_9962();
void Butterfly_9963();
void WEIGHTED_ROUND_ROBIN_Joiner_9961();
void Post_CollapsedDataParallel_2_9790();
void Pre_CollapsedDataParallel_1_9792();
void WEIGHTED_ROUND_ROBIN_Splitter_9964();
void Butterfly_9966();
void Butterfly_9967();
void WEIGHTED_ROUND_ROBIN_Joiner_9965();
void Post_CollapsedDataParallel_2_9793();
void Pre_CollapsedDataParallel_1_9795();
void WEIGHTED_ROUND_ROBIN_Splitter_9968();
void Butterfly_9970();
void Butterfly_9971();
void WEIGHTED_ROUND_ROBIN_Joiner_9969();
void Post_CollapsedDataParallel_2_9796();
void WEIGHTED_ROUND_ROBIN_Joiner_9886();
void WEIGHTED_ROUND_ROBIN_Joiner_9887();
void WEIGHTED_ROUND_ROBIN_Splitter_9888();
void WEIGHTED_ROUND_ROBIN_Splitter_9889();
void Butterfly_9517();
void Butterfly_9518();
void Butterfly_9519();
void Butterfly_9520();
void Butterfly_9521();
void Butterfly_9522();
void Butterfly_9523();
void Butterfly_9524();
void WEIGHTED_ROUND_ROBIN_Joiner_9890();
void WEIGHTED_ROUND_ROBIN_Splitter_9891();
void Butterfly_9525();
void Butterfly_9526();
void Butterfly_9527();
void Butterfly_9528();
void Butterfly_9529();
void Butterfly_9530();
void Butterfly_9531();
void Butterfly_9532();
void WEIGHTED_ROUND_ROBIN_Joiner_9892();
void WEIGHTED_ROUND_ROBIN_Joiner_9893();
int BitReverse_9533_bitrev(int inp, int numbits);
void BitReverse_9533();
void FloatPrinter_9534();

#ifdef __cplusplus
}
#endif
#endif
