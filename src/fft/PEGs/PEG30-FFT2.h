#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=7680 on the compile command line
#else
#if BUF_SIZEMAX < 7680
#error BUF_SIZEMAX too small, it must be at least 7680
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float w[2];
} CombineDFT_1241_t;

typedef struct {
	float w[4];
} CombineDFT_1273_t;

typedef struct {
	float w[8];
} CombineDFT_1291_t;

typedef struct {
	float w[16];
} CombineDFT_1301_t;

typedef struct {
	float w[32];
} CombineDFT_1307_t;

typedef struct {
	float w[64];
} CombineDFT_1176_t;
void WEIGHTED_ROUND_ROBIN_Splitter_1197();
void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout);
void FFTTestSource_1199();
void FFTTestSource_1200();
void WEIGHTED_ROUND_ROBIN_Joiner_1198();
void WEIGHTED_ROUND_ROBIN_Splitter_1189();
void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout);
void FFTReorderSimple_1166();
void WEIGHTED_ROUND_ROBIN_Splitter_1201();
void FFTReorderSimple_1203();
void FFTReorderSimple_1204();
void WEIGHTED_ROUND_ROBIN_Joiner_1202();
void WEIGHTED_ROUND_ROBIN_Splitter_1205();
void FFTReorderSimple_1207();
void FFTReorderSimple_1208();
void FFTReorderSimple_1209();
void FFTReorderSimple_1210();
void WEIGHTED_ROUND_ROBIN_Joiner_1206();
void WEIGHTED_ROUND_ROBIN_Splitter_1211();
void FFTReorderSimple_1213();
void FFTReorderSimple_1214();
void FFTReorderSimple_1215();
void FFTReorderSimple_1216();
void FFTReorderSimple_1217();
void FFTReorderSimple_1218();
void FFTReorderSimple_1219();
void FFTReorderSimple_1220();
void WEIGHTED_ROUND_ROBIN_Joiner_1212();
void WEIGHTED_ROUND_ROBIN_Splitter_1221();
void FFTReorderSimple_1223();
void FFTReorderSimple_1224();
void FFTReorderSimple_1225();
void FFTReorderSimple_1226();
void FFTReorderSimple_1227();
void FFTReorderSimple_1228();
void FFTReorderSimple_1229();
void FFTReorderSimple_1230();
void FFTReorderSimple_1231();
void FFTReorderSimple_1232();
void FFTReorderSimple_1233();
void FFTReorderSimple_1234();
void FFTReorderSimple_1235();
void FFTReorderSimple_1236();
void FFTReorderSimple_1237();
void FFTReorderSimple_1238();
void WEIGHTED_ROUND_ROBIN_Joiner_1222();
void WEIGHTED_ROUND_ROBIN_Splitter_1239();
void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout);
void CombineDFT_1241();
void CombineDFT_1242();
void CombineDFT_1243();
void CombineDFT_1244();
void CombineDFT_1245();
void CombineDFT_1246();
void CombineDFT_1247();
void CombineDFT_1248();
void CombineDFT_1249();
void CombineDFT_1250();
void CombineDFT_1251();
void CombineDFT_1252();
void CombineDFT_1253();
void CombineDFT_1254();
void CombineDFT_1255();
void CombineDFT_1256();
void CombineDFT_1257();
void CombineDFT_1258();
void CombineDFT_1259();
void CombineDFT_1260();
void CombineDFT_1261();
void CombineDFT_1262();
void CombineDFT_1263();
void CombineDFT_1264();
void CombineDFT_1265();
void CombineDFT_1266();
void CombineDFT_1267();
void CombineDFT_1268();
void CombineDFT_1269();
void CombineDFT_1270();
void WEIGHTED_ROUND_ROBIN_Joiner_1240();
void WEIGHTED_ROUND_ROBIN_Splitter_1271();
void CombineDFT_1273();
void CombineDFT_1274();
void CombineDFT_1275();
void CombineDFT_1276();
void CombineDFT_1277();
void CombineDFT_1278();
void CombineDFT_1279();
void CombineDFT_1280();
void CombineDFT_1281();
void CombineDFT_1282();
void CombineDFT_1283();
void CombineDFT_1284();
void CombineDFT_1285();
void CombineDFT_1286();
void CombineDFT_1287();
void CombineDFT_1288();
void WEIGHTED_ROUND_ROBIN_Joiner_1272();
void WEIGHTED_ROUND_ROBIN_Splitter_1289();
void CombineDFT_1291();
void CombineDFT_1292();
void CombineDFT_1293();
void CombineDFT_1294();
void CombineDFT_1295();
void CombineDFT_1296();
void CombineDFT_1297();
void CombineDFT_1298();
void WEIGHTED_ROUND_ROBIN_Joiner_1290();
void WEIGHTED_ROUND_ROBIN_Splitter_1299();
void CombineDFT_1301();
void CombineDFT_1302();
void CombineDFT_1303();
void CombineDFT_1304();
void WEIGHTED_ROUND_ROBIN_Joiner_1300();
void WEIGHTED_ROUND_ROBIN_Splitter_1305();
void CombineDFT_1307();
void CombineDFT_1308();
void WEIGHTED_ROUND_ROBIN_Joiner_1306();
void CombineDFT_1176();
void FFTReorderSimple_1177();
void WEIGHTED_ROUND_ROBIN_Splitter_1309();
void FFTReorderSimple_1311();
void FFTReorderSimple_1312();
void WEIGHTED_ROUND_ROBIN_Joiner_1310();
void WEIGHTED_ROUND_ROBIN_Splitter_1313();
void FFTReorderSimple_1315();
void FFTReorderSimple_1316();
void FFTReorderSimple_1317();
void FFTReorderSimple_1318();
void WEIGHTED_ROUND_ROBIN_Joiner_1314();
void WEIGHTED_ROUND_ROBIN_Splitter_1319();
void FFTReorderSimple_1321();
void FFTReorderSimple_1322();
void FFTReorderSimple_1323();
void FFTReorderSimple_1324();
void FFTReorderSimple_1325();
void FFTReorderSimple_1326();
void FFTReorderSimple_1327();
void FFTReorderSimple_1328();
void WEIGHTED_ROUND_ROBIN_Joiner_1320();
void WEIGHTED_ROUND_ROBIN_Splitter_1329();
void FFTReorderSimple_1331();
void FFTReorderSimple_1332();
void FFTReorderSimple_1333();
void FFTReorderSimple_1334();
void FFTReorderSimple_1335();
void FFTReorderSimple_1336();
void FFTReorderSimple_1337();
void FFTReorderSimple_1338();
void FFTReorderSimple_1339();
void FFTReorderSimple_1340();
void FFTReorderSimple_1341();
void FFTReorderSimple_1342();
void FFTReorderSimple_1343();
void FFTReorderSimple_1344();
void FFTReorderSimple_1345();
void FFTReorderSimple_1346();
void WEIGHTED_ROUND_ROBIN_Joiner_1330();
void WEIGHTED_ROUND_ROBIN_Splitter_1347();
void CombineDFT_1349();
void CombineDFT_1350();
void CombineDFT_1351();
void CombineDFT_1352();
void CombineDFT_1353();
void CombineDFT_1354();
void CombineDFT_1355();
void CombineDFT_1356();
void CombineDFT_1357();
void CombineDFT_1358();
void CombineDFT_1359();
void CombineDFT_1360();
void CombineDFT_1361();
void CombineDFT_1362();
void CombineDFT_1363();
void CombineDFT_1364();
void CombineDFT_1365();
void CombineDFT_1366();
void CombineDFT_1367();
void CombineDFT_1368();
void CombineDFT_1369();
void CombineDFT_1370();
void CombineDFT_1371();
void CombineDFT_1372();
void CombineDFT_1373();
void CombineDFT_1374();
void CombineDFT_1375();
void CombineDFT_1376();
void CombineDFT_1377();
void CombineDFT_1378();
void WEIGHTED_ROUND_ROBIN_Joiner_1348();
void WEIGHTED_ROUND_ROBIN_Splitter_1379();
void CombineDFT_1381();
void CombineDFT_1382();
void CombineDFT_1383();
void CombineDFT_1384();
void CombineDFT_1385();
void CombineDFT_1386();
void CombineDFT_1387();
void CombineDFT_1388();
void CombineDFT_1389();
void CombineDFT_1390();
void CombineDFT_1391();
void CombineDFT_1392();
void CombineDFT_1393();
void CombineDFT_1394();
void CombineDFT_1395();
void CombineDFT_1396();
void WEIGHTED_ROUND_ROBIN_Joiner_1380();
void WEIGHTED_ROUND_ROBIN_Splitter_1397();
void CombineDFT_1399();
void CombineDFT_1400();
void CombineDFT_1401();
void CombineDFT_1402();
void CombineDFT_1403();
void CombineDFT_1404();
void CombineDFT_1405();
void CombineDFT_1406();
void WEIGHTED_ROUND_ROBIN_Joiner_1398();
void WEIGHTED_ROUND_ROBIN_Splitter_1407();
void CombineDFT_1409();
void CombineDFT_1410();
void CombineDFT_1411();
void CombineDFT_1412();
void WEIGHTED_ROUND_ROBIN_Joiner_1408();
void WEIGHTED_ROUND_ROBIN_Splitter_1413();
void CombineDFT_1415();
void CombineDFT_1416();
void WEIGHTED_ROUND_ROBIN_Joiner_1414();
void CombineDFT_1187();
void WEIGHTED_ROUND_ROBIN_Joiner_1190();
void FloatPrinter(buffer_float_t *chanin);
void FloatPrinter_1188();

#ifdef __cplusplus
}
#endif
#endif
