#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=384 on the compile command line
#else
#if BUF_SIZEMAX < 384
#error BUF_SIZEMAX too small, it must be at least 384
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	complex_t wn;
} CombineDFT_5525_t;
void FFTTestSource(buffer_complex_t *chanout);
void FFTTestSource_5483();
void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout);
void FFTReorderSimple_5484();
void WEIGHTED_ROUND_ROBIN_Splitter_5497();
void FFTReorderSimple_5499();
void FFTReorderSimple_5500();
void WEIGHTED_ROUND_ROBIN_Joiner_5498();
void WEIGHTED_ROUND_ROBIN_Splitter_5501();
void FFTReorderSimple_5503();
void FFTReorderSimple_5504();
void FFTReorderSimple_5505();
void FFTReorderSimple_5506();
void WEIGHTED_ROUND_ROBIN_Joiner_5502();
void WEIGHTED_ROUND_ROBIN_Splitter_5507();
void FFTReorderSimple_5509();
void FFTReorderSimple_5510();
void FFTReorderSimple_5511();
void FFTReorderSimple_5512();
void FFTReorderSimple_5513();
void FFTReorderSimple_5514();
void WEIGHTED_ROUND_ROBIN_Joiner_5508();
void WEIGHTED_ROUND_ROBIN_Splitter_5515();
void FFTReorderSimple_5517();
void FFTReorderSimple_5518();
void FFTReorderSimple_5519();
void FFTReorderSimple_5520();
void FFTReorderSimple_5521();
void FFTReorderSimple_5522();
void WEIGHTED_ROUND_ROBIN_Joiner_5516();
void WEIGHTED_ROUND_ROBIN_Splitter_5523();
void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout);
void CombineDFT_5525();
void CombineDFT_5526();
void CombineDFT_5527();
void CombineDFT_5528();
void CombineDFT_5529();
void CombineDFT_5530();
void WEIGHTED_ROUND_ROBIN_Joiner_5524();
void WEIGHTED_ROUND_ROBIN_Splitter_5531();
void CombineDFT_5533();
void CombineDFT_5534();
void CombineDFT_5535();
void CombineDFT_5536();
void CombineDFT_5537();
void CombineDFT_5538();
void WEIGHTED_ROUND_ROBIN_Joiner_5532();
void WEIGHTED_ROUND_ROBIN_Splitter_5539();
void CombineDFT_5541();
void CombineDFT_5542();
void CombineDFT_5543();
void CombineDFT_5544();
void CombineDFT_5545();
void CombineDFT_5546();
void WEIGHTED_ROUND_ROBIN_Joiner_5540();
void WEIGHTED_ROUND_ROBIN_Splitter_5547();
void CombineDFT_5549();
void CombineDFT_5550();
void CombineDFT_5551();
void CombineDFT_5552();
void WEIGHTED_ROUND_ROBIN_Joiner_5548();
void WEIGHTED_ROUND_ROBIN_Splitter_5553();
void CombineDFT_5555();
void CombineDFT_5556();
void WEIGHTED_ROUND_ROBIN_Joiner_5554();
void CombineDFT_5494();
void CPrinter(buffer_complex_t *chanin);
void CPrinter_5495();

#ifdef __cplusplus
}
#endif
#endif
