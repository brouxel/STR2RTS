#include "PEG11-FFT2.h"

buffer_float_t SplitJoin18_CombineDFT_Fiss_9933_9954_join[4];
buffer_float_t SplitJoin14_CombineDFT_Fiss_9931_9952_split[11];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9813WEIGHTED_ROUND_ROBIN_Splitter_9825;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9826WEIGHTED_ROUND_ROBIN_Splitter_9835;
buffer_float_t SplitJoin20_CombineDFT_Fiss_9934_9955_join[2];
buffer_float_t SplitJoin83_CombineDFT_Fiss_9939_9960_split[11];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9777WEIGHTED_ROUND_ROBIN_Splitter_9786;
buffer_float_t SplitJoin77_FFTReorderSimple_Fiss_9936_9957_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9866WEIGHTED_ROUND_ROBIN_Splitter_9878;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9879WEIGHTED_ROUND_ROBIN_Splitter_9891;
buffer_float_t SplitJoin91_CombineDFT_Fiss_9943_9964_join[2];
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_9929_9950_join[11];
buffer_float_t SplitJoin87_CombineDFT_Fiss_9941_9962_split[8];
buffer_float_t SplitJoin20_CombineDFT_Fiss_9934_9955_split[2];
buffer_float_t SplitJoin89_CombineDFT_Fiss_9942_9963_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9915WEIGHTED_ROUND_ROBIN_Splitter_9920;
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_9927_9948_join[4];
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_9926_9947_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9846WEIGHTED_ROUND_ROBIN_Splitter_9849;
buffer_float_t SplitJoin12_CombineDFT_Fiss_9930_9951_split[11];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9892WEIGHTED_ROUND_ROBIN_Splitter_9904;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9850WEIGHTED_ROUND_ROBIN_Splitter_9855;
buffer_float_t SplitJoin75_FFTReorderSimple_Fiss_9935_9956_join[2];
buffer_float_t SplitJoin12_CombineDFT_Fiss_9930_9951_join[11];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9763WEIGHTED_ROUND_ROBIN_Splitter_9754;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9921CombineDFT_9752;
buffer_float_t SplitJoin81_FFTReorderSimple_Fiss_9938_9959_join[11];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9800WEIGHTED_ROUND_ROBIN_Splitter_9812;
buffer_float_t FFTReorderSimple_9742WEIGHTED_ROUND_ROBIN_Splitter_9845;
buffer_float_t SplitJoin91_CombineDFT_Fiss_9943_9964_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9842CombineDFT_9741;
buffer_float_t SplitJoin85_CombineDFT_Fiss_9940_9961_split[11];
buffer_float_t SplitJoin87_CombineDFT_Fiss_9941_9962_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9767WEIGHTED_ROUND_ROBIN_Splitter_9770;
buffer_float_t SplitJoin79_FFTReorderSimple_Fiss_9937_9958_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9787WEIGHTED_ROUND_ROBIN_Splitter_9799;
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_9927_9948_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9856WEIGHTED_ROUND_ROBIN_Splitter_9865;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9836WEIGHTED_ROUND_ROBIN_Splitter_9841;
buffer_float_t SplitJoin14_CombineDFT_Fiss_9931_9952_join[11];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_9720_9756_9925_9946_join[2];
buffer_float_t SplitJoin0_FFTTestSource_Fiss_9924_9945_join[2];
buffer_float_t SplitJoin89_CombineDFT_Fiss_9942_9963_split[4];
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_9928_9949_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9905WEIGHTED_ROUND_ROBIN_Splitter_9914;
buffer_float_t SplitJoin0_FFTTestSource_Fiss_9924_9945_split[2];
buffer_float_t SplitJoin85_CombineDFT_Fiss_9940_9961_join[11];
buffer_float_t SplitJoin75_FFTReorderSimple_Fiss_9935_9956_split[2];
buffer_float_t SplitJoin77_FFTReorderSimple_Fiss_9936_9957_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9755FloatPrinter_9753;
buffer_float_t SplitJoin16_CombineDFT_Fiss_9932_9953_join[8];
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_9928_9949_join[8];
buffer_float_t SplitJoin16_CombineDFT_Fiss_9932_9953_split[8];
buffer_float_t SplitJoin79_FFTReorderSimple_Fiss_9937_9958_join[8];
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_9926_9947_split[2];
buffer_float_t SplitJoin18_CombineDFT_Fiss_9933_9954_split[4];
buffer_float_t FFTReorderSimple_9731WEIGHTED_ROUND_ROBIN_Splitter_9766;
buffer_float_t SplitJoin83_CombineDFT_Fiss_9939_9960_join[11];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_9720_9756_9925_9946_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9771WEIGHTED_ROUND_ROBIN_Splitter_9776;
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_9929_9950_split[11];
buffer_float_t SplitJoin81_FFTReorderSimple_Fiss_9938_9959_split[11];


CombineDFT_9801_t CombineDFT_9801_s;
CombineDFT_9801_t CombineDFT_9802_s;
CombineDFT_9801_t CombineDFT_9803_s;
CombineDFT_9801_t CombineDFT_9804_s;
CombineDFT_9801_t CombineDFT_9805_s;
CombineDFT_9801_t CombineDFT_9806_s;
CombineDFT_9801_t CombineDFT_9807_s;
CombineDFT_9801_t CombineDFT_9808_s;
CombineDFT_9801_t CombineDFT_9809_s;
CombineDFT_9801_t CombineDFT_9810_s;
CombineDFT_9801_t CombineDFT_9811_s;
CombineDFT_9814_t CombineDFT_9814_s;
CombineDFT_9814_t CombineDFT_9815_s;
CombineDFT_9814_t CombineDFT_9816_s;
CombineDFT_9814_t CombineDFT_9817_s;
CombineDFT_9814_t CombineDFT_9818_s;
CombineDFT_9814_t CombineDFT_9819_s;
CombineDFT_9814_t CombineDFT_9820_s;
CombineDFT_9814_t CombineDFT_9821_s;
CombineDFT_9814_t CombineDFT_9822_s;
CombineDFT_9814_t CombineDFT_9823_s;
CombineDFT_9814_t CombineDFT_9824_s;
CombineDFT_9827_t CombineDFT_9827_s;
CombineDFT_9827_t CombineDFT_9828_s;
CombineDFT_9827_t CombineDFT_9829_s;
CombineDFT_9827_t CombineDFT_9830_s;
CombineDFT_9827_t CombineDFT_9831_s;
CombineDFT_9827_t CombineDFT_9832_s;
CombineDFT_9827_t CombineDFT_9833_s;
CombineDFT_9827_t CombineDFT_9834_s;
CombineDFT_9837_t CombineDFT_9837_s;
CombineDFT_9837_t CombineDFT_9838_s;
CombineDFT_9837_t CombineDFT_9839_s;
CombineDFT_9837_t CombineDFT_9840_s;
CombineDFT_9843_t CombineDFT_9843_s;
CombineDFT_9843_t CombineDFT_9844_s;
CombineDFT_9741_t CombineDFT_9741_s;
CombineDFT_9801_t CombineDFT_9880_s;
CombineDFT_9801_t CombineDFT_9881_s;
CombineDFT_9801_t CombineDFT_9882_s;
CombineDFT_9801_t CombineDFT_9883_s;
CombineDFT_9801_t CombineDFT_9884_s;
CombineDFT_9801_t CombineDFT_9885_s;
CombineDFT_9801_t CombineDFT_9886_s;
CombineDFT_9801_t CombineDFT_9887_s;
CombineDFT_9801_t CombineDFT_9888_s;
CombineDFT_9801_t CombineDFT_9889_s;
CombineDFT_9801_t CombineDFT_9890_s;
CombineDFT_9814_t CombineDFT_9893_s;
CombineDFT_9814_t CombineDFT_9894_s;
CombineDFT_9814_t CombineDFT_9895_s;
CombineDFT_9814_t CombineDFT_9896_s;
CombineDFT_9814_t CombineDFT_9897_s;
CombineDFT_9814_t CombineDFT_9898_s;
CombineDFT_9814_t CombineDFT_9899_s;
CombineDFT_9814_t CombineDFT_9900_s;
CombineDFT_9814_t CombineDFT_9901_s;
CombineDFT_9814_t CombineDFT_9902_s;
CombineDFT_9814_t CombineDFT_9903_s;
CombineDFT_9827_t CombineDFT_9906_s;
CombineDFT_9827_t CombineDFT_9907_s;
CombineDFT_9827_t CombineDFT_9908_s;
CombineDFT_9827_t CombineDFT_9909_s;
CombineDFT_9827_t CombineDFT_9910_s;
CombineDFT_9827_t CombineDFT_9911_s;
CombineDFT_9827_t CombineDFT_9912_s;
CombineDFT_9827_t CombineDFT_9913_s;
CombineDFT_9837_t CombineDFT_9916_s;
CombineDFT_9837_t CombineDFT_9917_s;
CombineDFT_9837_t CombineDFT_9918_s;
CombineDFT_9837_t CombineDFT_9919_s;
CombineDFT_9843_t CombineDFT_9922_s;
CombineDFT_9843_t CombineDFT_9923_s;
CombineDFT_9741_t CombineDFT_9752_s;

void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), 0.0) ; 
		push_float(&(*chanout), 0.0) ; 
		push_float(&(*chanout), 1.0) ; 
		push_float(&(*chanout), 0.0) ; 
		FOR(int, i, 0,  < , 124, i++) {
			push_float(&(*chanout), 0.0) ; 
		}
		ENDFOR
	}


void FFTTestSource_9764() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_9924_9945_split[0]), &(SplitJoin0_FFTTestSource_Fiss_9924_9945_join[0]));
	ENDFOR
}

void FFTTestSource_9765() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_9924_9945_split[1]), &(SplitJoin0_FFTTestSource_Fiss_9924_9945_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9762() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_9763() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9763WEIGHTED_ROUND_ROBIN_Splitter_9754, pop_float(&SplitJoin0_FFTTestSource_Fiss_9924_9945_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9763WEIGHTED_ROUND_ROBIN_Splitter_9754, pop_float(&SplitJoin0_FFTTestSource_Fiss_9924_9945_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, i, 0,  < , 128, i = (i + 4)) {
			push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 128, i = (i + 4)) {
			push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_float(&(*chanin)) ; 
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void FFTReorderSimple_9731() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_9720_9756_9925_9946_split[0]), &(FFTReorderSimple_9731WEIGHTED_ROUND_ROBIN_Splitter_9766));
	ENDFOR
}

void FFTReorderSimple_9768() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_9926_9947_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_9926_9947_join[0]));
	ENDFOR
}

void FFTReorderSimple_9769() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_9926_9947_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_9926_9947_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9766() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_9926_9947_split[0], pop_float(&FFTReorderSimple_9731WEIGHTED_ROUND_ROBIN_Splitter_9766));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_9926_9947_split[1], pop_float(&FFTReorderSimple_9731WEIGHTED_ROUND_ROBIN_Splitter_9766));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9767() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9767WEIGHTED_ROUND_ROBIN_Splitter_9770, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_9926_9947_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9767WEIGHTED_ROUND_ROBIN_Splitter_9770, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_9926_9947_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_9772() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_9927_9948_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_9927_9948_join[0]));
	ENDFOR
}

void FFTReorderSimple_9773() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_9927_9948_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_9927_9948_join[1]));
	ENDFOR
}

void FFTReorderSimple_9774() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_9927_9948_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_9927_9948_join[2]));
	ENDFOR
}

void FFTReorderSimple_9775() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_9927_9948_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_9927_9948_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9770() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin6_FFTReorderSimple_Fiss_9927_9948_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9767WEIGHTED_ROUND_ROBIN_Splitter_9770));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9771() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9771WEIGHTED_ROUND_ROBIN_Splitter_9776, pop_float(&SplitJoin6_FFTReorderSimple_Fiss_9927_9948_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_9778() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_9928_9949_split[0]), &(SplitJoin8_FFTReorderSimple_Fiss_9928_9949_join[0]));
	ENDFOR
}

void FFTReorderSimple_9779() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_9928_9949_split[1]), &(SplitJoin8_FFTReorderSimple_Fiss_9928_9949_join[1]));
	ENDFOR
}

void FFTReorderSimple_9780() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_9928_9949_split[2]), &(SplitJoin8_FFTReorderSimple_Fiss_9928_9949_join[2]));
	ENDFOR
}

void FFTReorderSimple_9781() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_9928_9949_split[3]), &(SplitJoin8_FFTReorderSimple_Fiss_9928_9949_join[3]));
	ENDFOR
}

void FFTReorderSimple_9782() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_9928_9949_split[4]), &(SplitJoin8_FFTReorderSimple_Fiss_9928_9949_join[4]));
	ENDFOR
}

void FFTReorderSimple_9783() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_9928_9949_split[5]), &(SplitJoin8_FFTReorderSimple_Fiss_9928_9949_join[5]));
	ENDFOR
}

void FFTReorderSimple_9784() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_9928_9949_split[6]), &(SplitJoin8_FFTReorderSimple_Fiss_9928_9949_join[6]));
	ENDFOR
}

void FFTReorderSimple_9785() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_9928_9949_split[7]), &(SplitJoin8_FFTReorderSimple_Fiss_9928_9949_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9776() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin8_FFTReorderSimple_Fiss_9928_9949_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9771WEIGHTED_ROUND_ROBIN_Splitter_9776));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9777() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9777WEIGHTED_ROUND_ROBIN_Splitter_9786, pop_float(&SplitJoin8_FFTReorderSimple_Fiss_9928_9949_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_9788() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_9929_9950_split[0]), &(SplitJoin10_FFTReorderSimple_Fiss_9929_9950_join[0]));
	ENDFOR
}

void FFTReorderSimple_9789() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_9929_9950_split[1]), &(SplitJoin10_FFTReorderSimple_Fiss_9929_9950_join[1]));
	ENDFOR
}

void FFTReorderSimple_9790() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_9929_9950_split[2]), &(SplitJoin10_FFTReorderSimple_Fiss_9929_9950_join[2]));
	ENDFOR
}

void FFTReorderSimple_9791() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_9929_9950_split[3]), &(SplitJoin10_FFTReorderSimple_Fiss_9929_9950_join[3]));
	ENDFOR
}

void FFTReorderSimple_9792() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_9929_9950_split[4]), &(SplitJoin10_FFTReorderSimple_Fiss_9929_9950_join[4]));
	ENDFOR
}

void FFTReorderSimple_9793() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_9929_9950_split[5]), &(SplitJoin10_FFTReorderSimple_Fiss_9929_9950_join[5]));
	ENDFOR
}

void FFTReorderSimple_9794() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_9929_9950_split[6]), &(SplitJoin10_FFTReorderSimple_Fiss_9929_9950_join[6]));
	ENDFOR
}

void FFTReorderSimple_9795() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_9929_9950_split[7]), &(SplitJoin10_FFTReorderSimple_Fiss_9929_9950_join[7]));
	ENDFOR
}

void FFTReorderSimple_9796() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_9929_9950_split[8]), &(SplitJoin10_FFTReorderSimple_Fiss_9929_9950_join[8]));
	ENDFOR
}

void FFTReorderSimple_9797() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_9929_9950_split[9]), &(SplitJoin10_FFTReorderSimple_Fiss_9929_9950_join[9]));
	ENDFOR
}

void FFTReorderSimple_9798() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_9929_9950_split[10]), &(SplitJoin10_FFTReorderSimple_Fiss_9929_9950_join[10]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9786() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 11, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_FFTReorderSimple_Fiss_9929_9950_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9777WEIGHTED_ROUND_ROBIN_Splitter_9786));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9787() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 11, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9787WEIGHTED_ROUND_ROBIN_Splitter_9799, pop_float(&SplitJoin10_FFTReorderSimple_Fiss_9929_9950_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&(*chanin), i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&(*chanin), i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&(*chanin), (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&(*chanin), (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_9801_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_9801_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&(*chanin)) ; 
			push_float(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_9801() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_9930_9951_split[0]), &(SplitJoin12_CombineDFT_Fiss_9930_9951_join[0]));
	ENDFOR
}

void CombineDFT_9802() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_9930_9951_split[1]), &(SplitJoin12_CombineDFT_Fiss_9930_9951_join[1]));
	ENDFOR
}

void CombineDFT_9803() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_9930_9951_split[2]), &(SplitJoin12_CombineDFT_Fiss_9930_9951_join[2]));
	ENDFOR
}

void CombineDFT_9804() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_9930_9951_split[3]), &(SplitJoin12_CombineDFT_Fiss_9930_9951_join[3]));
	ENDFOR
}

void CombineDFT_9805() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_9930_9951_split[4]), &(SplitJoin12_CombineDFT_Fiss_9930_9951_join[4]));
	ENDFOR
}

void CombineDFT_9806() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_9930_9951_split[5]), &(SplitJoin12_CombineDFT_Fiss_9930_9951_join[5]));
	ENDFOR
}

void CombineDFT_9807() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_9930_9951_split[6]), &(SplitJoin12_CombineDFT_Fiss_9930_9951_join[6]));
	ENDFOR
}

void CombineDFT_9808() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_9930_9951_split[7]), &(SplitJoin12_CombineDFT_Fiss_9930_9951_join[7]));
	ENDFOR
}

void CombineDFT_9809() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_9930_9951_split[8]), &(SplitJoin12_CombineDFT_Fiss_9930_9951_join[8]));
	ENDFOR
}

void CombineDFT_9810() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_9930_9951_split[9]), &(SplitJoin12_CombineDFT_Fiss_9930_9951_join[9]));
	ENDFOR
}

void CombineDFT_9811() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_9930_9951_split[10]), &(SplitJoin12_CombineDFT_Fiss_9930_9951_join[10]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9799() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 11, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin12_CombineDFT_Fiss_9930_9951_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9787WEIGHTED_ROUND_ROBIN_Splitter_9799));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9800() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 11, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9800WEIGHTED_ROUND_ROBIN_Splitter_9812, pop_float(&SplitJoin12_CombineDFT_Fiss_9930_9951_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_9814() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_9931_9952_split[0]), &(SplitJoin14_CombineDFT_Fiss_9931_9952_join[0]));
	ENDFOR
}

void CombineDFT_9815() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_9931_9952_split[1]), &(SplitJoin14_CombineDFT_Fiss_9931_9952_join[1]));
	ENDFOR
}

void CombineDFT_9816() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_9931_9952_split[2]), &(SplitJoin14_CombineDFT_Fiss_9931_9952_join[2]));
	ENDFOR
}

void CombineDFT_9817() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_9931_9952_split[3]), &(SplitJoin14_CombineDFT_Fiss_9931_9952_join[3]));
	ENDFOR
}

void CombineDFT_9818() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_9931_9952_split[4]), &(SplitJoin14_CombineDFT_Fiss_9931_9952_join[4]));
	ENDFOR
}

void CombineDFT_9819() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_9931_9952_split[5]), &(SplitJoin14_CombineDFT_Fiss_9931_9952_join[5]));
	ENDFOR
}

void CombineDFT_9820() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_9931_9952_split[6]), &(SplitJoin14_CombineDFT_Fiss_9931_9952_join[6]));
	ENDFOR
}

void CombineDFT_9821() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_9931_9952_split[7]), &(SplitJoin14_CombineDFT_Fiss_9931_9952_join[7]));
	ENDFOR
}

void CombineDFT_9822() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_9931_9952_split[8]), &(SplitJoin14_CombineDFT_Fiss_9931_9952_join[8]));
	ENDFOR
}

void CombineDFT_9823() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_9931_9952_split[9]), &(SplitJoin14_CombineDFT_Fiss_9931_9952_join[9]));
	ENDFOR
}

void CombineDFT_9824() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_9931_9952_split[10]), &(SplitJoin14_CombineDFT_Fiss_9931_9952_join[10]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9812() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 11, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin14_CombineDFT_Fiss_9931_9952_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9800WEIGHTED_ROUND_ROBIN_Splitter_9812));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9813() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 11, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9813WEIGHTED_ROUND_ROBIN_Splitter_9825, pop_float(&SplitJoin14_CombineDFT_Fiss_9931_9952_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_9827() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_9932_9953_split[0]), &(SplitJoin16_CombineDFT_Fiss_9932_9953_join[0]));
	ENDFOR
}

void CombineDFT_9828() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_9932_9953_split[1]), &(SplitJoin16_CombineDFT_Fiss_9932_9953_join[1]));
	ENDFOR
}

void CombineDFT_9829() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_9932_9953_split[2]), &(SplitJoin16_CombineDFT_Fiss_9932_9953_join[2]));
	ENDFOR
}

void CombineDFT_9830() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_9932_9953_split[3]), &(SplitJoin16_CombineDFT_Fiss_9932_9953_join[3]));
	ENDFOR
}

void CombineDFT_9831() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_9932_9953_split[4]), &(SplitJoin16_CombineDFT_Fiss_9932_9953_join[4]));
	ENDFOR
}

void CombineDFT_9832() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_9932_9953_split[5]), &(SplitJoin16_CombineDFT_Fiss_9932_9953_join[5]));
	ENDFOR
}

void CombineDFT_9833() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_9932_9953_split[6]), &(SplitJoin16_CombineDFT_Fiss_9932_9953_join[6]));
	ENDFOR
}

void CombineDFT_9834() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_9932_9953_split[7]), &(SplitJoin16_CombineDFT_Fiss_9932_9953_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9825() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin16_CombineDFT_Fiss_9932_9953_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9813WEIGHTED_ROUND_ROBIN_Splitter_9825));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9826() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9826WEIGHTED_ROUND_ROBIN_Splitter_9835, pop_float(&SplitJoin16_CombineDFT_Fiss_9932_9953_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_9837() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_9933_9954_split[0]), &(SplitJoin18_CombineDFT_Fiss_9933_9954_join[0]));
	ENDFOR
}

void CombineDFT_9838() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_9933_9954_split[1]), &(SplitJoin18_CombineDFT_Fiss_9933_9954_join[1]));
	ENDFOR
}

void CombineDFT_9839() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_9933_9954_split[2]), &(SplitJoin18_CombineDFT_Fiss_9933_9954_join[2]));
	ENDFOR
}

void CombineDFT_9840() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_9933_9954_split[3]), &(SplitJoin18_CombineDFT_Fiss_9933_9954_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9835() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin18_CombineDFT_Fiss_9933_9954_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9826WEIGHTED_ROUND_ROBIN_Splitter_9835));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9836() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9836WEIGHTED_ROUND_ROBIN_Splitter_9841, pop_float(&SplitJoin18_CombineDFT_Fiss_9933_9954_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_9843() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin20_CombineDFT_Fiss_9934_9955_split[0]), &(SplitJoin20_CombineDFT_Fiss_9934_9955_join[0]));
	ENDFOR
}

void CombineDFT_9844() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin20_CombineDFT_Fiss_9934_9955_split[1]), &(SplitJoin20_CombineDFT_Fiss_9934_9955_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9841() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_9934_9955_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9836WEIGHTED_ROUND_ROBIN_Splitter_9841));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_9934_9955_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9836WEIGHTED_ROUND_ROBIN_Splitter_9841));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9842() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9842CombineDFT_9741, pop_float(&SplitJoin20_CombineDFT_Fiss_9934_9955_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9842CombineDFT_9741, pop_float(&SplitJoin20_CombineDFT_Fiss_9934_9955_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_9741() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_9842CombineDFT_9741), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_9720_9756_9925_9946_join[0]));
	ENDFOR
}

void FFTReorderSimple_9742() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_9720_9756_9925_9946_split[1]), &(FFTReorderSimple_9742WEIGHTED_ROUND_ROBIN_Splitter_9845));
	ENDFOR
}

void FFTReorderSimple_9847() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin75_FFTReorderSimple_Fiss_9935_9956_split[0]), &(SplitJoin75_FFTReorderSimple_Fiss_9935_9956_join[0]));
	ENDFOR
}

void FFTReorderSimple_9848() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin75_FFTReorderSimple_Fiss_9935_9956_split[1]), &(SplitJoin75_FFTReorderSimple_Fiss_9935_9956_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9845() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin75_FFTReorderSimple_Fiss_9935_9956_split[0], pop_float(&FFTReorderSimple_9742WEIGHTED_ROUND_ROBIN_Splitter_9845));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin75_FFTReorderSimple_Fiss_9935_9956_split[1], pop_float(&FFTReorderSimple_9742WEIGHTED_ROUND_ROBIN_Splitter_9845));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9846() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9846WEIGHTED_ROUND_ROBIN_Splitter_9849, pop_float(&SplitJoin75_FFTReorderSimple_Fiss_9935_9956_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9846WEIGHTED_ROUND_ROBIN_Splitter_9849, pop_float(&SplitJoin75_FFTReorderSimple_Fiss_9935_9956_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_9851() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin77_FFTReorderSimple_Fiss_9936_9957_split[0]), &(SplitJoin77_FFTReorderSimple_Fiss_9936_9957_join[0]));
	ENDFOR
}

void FFTReorderSimple_9852() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin77_FFTReorderSimple_Fiss_9936_9957_split[1]), &(SplitJoin77_FFTReorderSimple_Fiss_9936_9957_join[1]));
	ENDFOR
}

void FFTReorderSimple_9853() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin77_FFTReorderSimple_Fiss_9936_9957_split[2]), &(SplitJoin77_FFTReorderSimple_Fiss_9936_9957_join[2]));
	ENDFOR
}

void FFTReorderSimple_9854() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin77_FFTReorderSimple_Fiss_9936_9957_split[3]), &(SplitJoin77_FFTReorderSimple_Fiss_9936_9957_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9849() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin77_FFTReorderSimple_Fiss_9936_9957_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9846WEIGHTED_ROUND_ROBIN_Splitter_9849));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9850() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9850WEIGHTED_ROUND_ROBIN_Splitter_9855, pop_float(&SplitJoin77_FFTReorderSimple_Fiss_9936_9957_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_9857() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin79_FFTReorderSimple_Fiss_9937_9958_split[0]), &(SplitJoin79_FFTReorderSimple_Fiss_9937_9958_join[0]));
	ENDFOR
}

void FFTReorderSimple_9858() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin79_FFTReorderSimple_Fiss_9937_9958_split[1]), &(SplitJoin79_FFTReorderSimple_Fiss_9937_9958_join[1]));
	ENDFOR
}

void FFTReorderSimple_9859() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin79_FFTReorderSimple_Fiss_9937_9958_split[2]), &(SplitJoin79_FFTReorderSimple_Fiss_9937_9958_join[2]));
	ENDFOR
}

void FFTReorderSimple_9860() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin79_FFTReorderSimple_Fiss_9937_9958_split[3]), &(SplitJoin79_FFTReorderSimple_Fiss_9937_9958_join[3]));
	ENDFOR
}

void FFTReorderSimple_9861() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin79_FFTReorderSimple_Fiss_9937_9958_split[4]), &(SplitJoin79_FFTReorderSimple_Fiss_9937_9958_join[4]));
	ENDFOR
}

void FFTReorderSimple_9862() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin79_FFTReorderSimple_Fiss_9937_9958_split[5]), &(SplitJoin79_FFTReorderSimple_Fiss_9937_9958_join[5]));
	ENDFOR
}

void FFTReorderSimple_9863() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin79_FFTReorderSimple_Fiss_9937_9958_split[6]), &(SplitJoin79_FFTReorderSimple_Fiss_9937_9958_join[6]));
	ENDFOR
}

void FFTReorderSimple_9864() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin79_FFTReorderSimple_Fiss_9937_9958_split[7]), &(SplitJoin79_FFTReorderSimple_Fiss_9937_9958_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9855() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin79_FFTReorderSimple_Fiss_9937_9958_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9850WEIGHTED_ROUND_ROBIN_Splitter_9855));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9856() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9856WEIGHTED_ROUND_ROBIN_Splitter_9865, pop_float(&SplitJoin79_FFTReorderSimple_Fiss_9937_9958_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_9867() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin81_FFTReorderSimple_Fiss_9938_9959_split[0]), &(SplitJoin81_FFTReorderSimple_Fiss_9938_9959_join[0]));
	ENDFOR
}

void FFTReorderSimple_9868() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin81_FFTReorderSimple_Fiss_9938_9959_split[1]), &(SplitJoin81_FFTReorderSimple_Fiss_9938_9959_join[1]));
	ENDFOR
}

void FFTReorderSimple_9869() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin81_FFTReorderSimple_Fiss_9938_9959_split[2]), &(SplitJoin81_FFTReorderSimple_Fiss_9938_9959_join[2]));
	ENDFOR
}

void FFTReorderSimple_9870() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin81_FFTReorderSimple_Fiss_9938_9959_split[3]), &(SplitJoin81_FFTReorderSimple_Fiss_9938_9959_join[3]));
	ENDFOR
}

void FFTReorderSimple_9871() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin81_FFTReorderSimple_Fiss_9938_9959_split[4]), &(SplitJoin81_FFTReorderSimple_Fiss_9938_9959_join[4]));
	ENDFOR
}

void FFTReorderSimple_9872() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin81_FFTReorderSimple_Fiss_9938_9959_split[5]), &(SplitJoin81_FFTReorderSimple_Fiss_9938_9959_join[5]));
	ENDFOR
}

void FFTReorderSimple_9873() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin81_FFTReorderSimple_Fiss_9938_9959_split[6]), &(SplitJoin81_FFTReorderSimple_Fiss_9938_9959_join[6]));
	ENDFOR
}

void FFTReorderSimple_9874() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin81_FFTReorderSimple_Fiss_9938_9959_split[7]), &(SplitJoin81_FFTReorderSimple_Fiss_9938_9959_join[7]));
	ENDFOR
}

void FFTReorderSimple_9875() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin81_FFTReorderSimple_Fiss_9938_9959_split[8]), &(SplitJoin81_FFTReorderSimple_Fiss_9938_9959_join[8]));
	ENDFOR
}

void FFTReorderSimple_9876() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin81_FFTReorderSimple_Fiss_9938_9959_split[9]), &(SplitJoin81_FFTReorderSimple_Fiss_9938_9959_join[9]));
	ENDFOR
}

void FFTReorderSimple_9877() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin81_FFTReorderSimple_Fiss_9938_9959_split[10]), &(SplitJoin81_FFTReorderSimple_Fiss_9938_9959_join[10]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9865() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 11, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin81_FFTReorderSimple_Fiss_9938_9959_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9856WEIGHTED_ROUND_ROBIN_Splitter_9865));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9866() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 11, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9866WEIGHTED_ROUND_ROBIN_Splitter_9878, pop_float(&SplitJoin81_FFTReorderSimple_Fiss_9938_9959_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_9880() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin83_CombineDFT_Fiss_9939_9960_split[0]), &(SplitJoin83_CombineDFT_Fiss_9939_9960_join[0]));
	ENDFOR
}

void CombineDFT_9881() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin83_CombineDFT_Fiss_9939_9960_split[1]), &(SplitJoin83_CombineDFT_Fiss_9939_9960_join[1]));
	ENDFOR
}

void CombineDFT_9882() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin83_CombineDFT_Fiss_9939_9960_split[2]), &(SplitJoin83_CombineDFT_Fiss_9939_9960_join[2]));
	ENDFOR
}

void CombineDFT_9883() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin83_CombineDFT_Fiss_9939_9960_split[3]), &(SplitJoin83_CombineDFT_Fiss_9939_9960_join[3]));
	ENDFOR
}

void CombineDFT_9884() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin83_CombineDFT_Fiss_9939_9960_split[4]), &(SplitJoin83_CombineDFT_Fiss_9939_9960_join[4]));
	ENDFOR
}

void CombineDFT_9885() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin83_CombineDFT_Fiss_9939_9960_split[5]), &(SplitJoin83_CombineDFT_Fiss_9939_9960_join[5]));
	ENDFOR
}

void CombineDFT_9886() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin83_CombineDFT_Fiss_9939_9960_split[6]), &(SplitJoin83_CombineDFT_Fiss_9939_9960_join[6]));
	ENDFOR
}

void CombineDFT_9887() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin83_CombineDFT_Fiss_9939_9960_split[7]), &(SplitJoin83_CombineDFT_Fiss_9939_9960_join[7]));
	ENDFOR
}

void CombineDFT_9888() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin83_CombineDFT_Fiss_9939_9960_split[8]), &(SplitJoin83_CombineDFT_Fiss_9939_9960_join[8]));
	ENDFOR
}

void CombineDFT_9889() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin83_CombineDFT_Fiss_9939_9960_split[9]), &(SplitJoin83_CombineDFT_Fiss_9939_9960_join[9]));
	ENDFOR
}

void CombineDFT_9890() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin83_CombineDFT_Fiss_9939_9960_split[10]), &(SplitJoin83_CombineDFT_Fiss_9939_9960_join[10]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9878() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 11, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin83_CombineDFT_Fiss_9939_9960_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9866WEIGHTED_ROUND_ROBIN_Splitter_9878));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9879() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 11, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9879WEIGHTED_ROUND_ROBIN_Splitter_9891, pop_float(&SplitJoin83_CombineDFT_Fiss_9939_9960_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_9893() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin85_CombineDFT_Fiss_9940_9961_split[0]), &(SplitJoin85_CombineDFT_Fiss_9940_9961_join[0]));
	ENDFOR
}

void CombineDFT_9894() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin85_CombineDFT_Fiss_9940_9961_split[1]), &(SplitJoin85_CombineDFT_Fiss_9940_9961_join[1]));
	ENDFOR
}

void CombineDFT_9895() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin85_CombineDFT_Fiss_9940_9961_split[2]), &(SplitJoin85_CombineDFT_Fiss_9940_9961_join[2]));
	ENDFOR
}

void CombineDFT_9896() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin85_CombineDFT_Fiss_9940_9961_split[3]), &(SplitJoin85_CombineDFT_Fiss_9940_9961_join[3]));
	ENDFOR
}

void CombineDFT_9897() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin85_CombineDFT_Fiss_9940_9961_split[4]), &(SplitJoin85_CombineDFT_Fiss_9940_9961_join[4]));
	ENDFOR
}

void CombineDFT_9898() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin85_CombineDFT_Fiss_9940_9961_split[5]), &(SplitJoin85_CombineDFT_Fiss_9940_9961_join[5]));
	ENDFOR
}

void CombineDFT_9899() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin85_CombineDFT_Fiss_9940_9961_split[6]), &(SplitJoin85_CombineDFT_Fiss_9940_9961_join[6]));
	ENDFOR
}

void CombineDFT_9900() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin85_CombineDFT_Fiss_9940_9961_split[7]), &(SplitJoin85_CombineDFT_Fiss_9940_9961_join[7]));
	ENDFOR
}

void CombineDFT_9901() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin85_CombineDFT_Fiss_9940_9961_split[8]), &(SplitJoin85_CombineDFT_Fiss_9940_9961_join[8]));
	ENDFOR
}

void CombineDFT_9902() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin85_CombineDFT_Fiss_9940_9961_split[9]), &(SplitJoin85_CombineDFT_Fiss_9940_9961_join[9]));
	ENDFOR
}

void CombineDFT_9903() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin85_CombineDFT_Fiss_9940_9961_split[10]), &(SplitJoin85_CombineDFT_Fiss_9940_9961_join[10]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9891() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 11, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin85_CombineDFT_Fiss_9940_9961_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9879WEIGHTED_ROUND_ROBIN_Splitter_9891));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9892() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 11, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9892WEIGHTED_ROUND_ROBIN_Splitter_9904, pop_float(&SplitJoin85_CombineDFT_Fiss_9940_9961_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_9906() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin87_CombineDFT_Fiss_9941_9962_split[0]), &(SplitJoin87_CombineDFT_Fiss_9941_9962_join[0]));
	ENDFOR
}

void CombineDFT_9907() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin87_CombineDFT_Fiss_9941_9962_split[1]), &(SplitJoin87_CombineDFT_Fiss_9941_9962_join[1]));
	ENDFOR
}

void CombineDFT_9908() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin87_CombineDFT_Fiss_9941_9962_split[2]), &(SplitJoin87_CombineDFT_Fiss_9941_9962_join[2]));
	ENDFOR
}

void CombineDFT_9909() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin87_CombineDFT_Fiss_9941_9962_split[3]), &(SplitJoin87_CombineDFT_Fiss_9941_9962_join[3]));
	ENDFOR
}

void CombineDFT_9910() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin87_CombineDFT_Fiss_9941_9962_split[4]), &(SplitJoin87_CombineDFT_Fiss_9941_9962_join[4]));
	ENDFOR
}

void CombineDFT_9911() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin87_CombineDFT_Fiss_9941_9962_split[5]), &(SplitJoin87_CombineDFT_Fiss_9941_9962_join[5]));
	ENDFOR
}

void CombineDFT_9912() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin87_CombineDFT_Fiss_9941_9962_split[6]), &(SplitJoin87_CombineDFT_Fiss_9941_9962_join[6]));
	ENDFOR
}

void CombineDFT_9913() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin87_CombineDFT_Fiss_9941_9962_split[7]), &(SplitJoin87_CombineDFT_Fiss_9941_9962_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9904() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin87_CombineDFT_Fiss_9941_9962_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9892WEIGHTED_ROUND_ROBIN_Splitter_9904));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9905() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9905WEIGHTED_ROUND_ROBIN_Splitter_9914, pop_float(&SplitJoin87_CombineDFT_Fiss_9941_9962_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_9916() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin89_CombineDFT_Fiss_9942_9963_split[0]), &(SplitJoin89_CombineDFT_Fiss_9942_9963_join[0]));
	ENDFOR
}

void CombineDFT_9917() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin89_CombineDFT_Fiss_9942_9963_split[1]), &(SplitJoin89_CombineDFT_Fiss_9942_9963_join[1]));
	ENDFOR
}

void CombineDFT_9918() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin89_CombineDFT_Fiss_9942_9963_split[2]), &(SplitJoin89_CombineDFT_Fiss_9942_9963_join[2]));
	ENDFOR
}

void CombineDFT_9919() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin89_CombineDFT_Fiss_9942_9963_split[3]), &(SplitJoin89_CombineDFT_Fiss_9942_9963_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9914() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin89_CombineDFT_Fiss_9942_9963_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9905WEIGHTED_ROUND_ROBIN_Splitter_9914));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9915() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9915WEIGHTED_ROUND_ROBIN_Splitter_9920, pop_float(&SplitJoin89_CombineDFT_Fiss_9942_9963_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_9922() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin91_CombineDFT_Fiss_9943_9964_split[0]), &(SplitJoin91_CombineDFT_Fiss_9943_9964_join[0]));
	ENDFOR
}

void CombineDFT_9923() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin91_CombineDFT_Fiss_9943_9964_split[1]), &(SplitJoin91_CombineDFT_Fiss_9943_9964_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9920() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin91_CombineDFT_Fiss_9943_9964_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9915WEIGHTED_ROUND_ROBIN_Splitter_9920));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin91_CombineDFT_Fiss_9943_9964_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9915WEIGHTED_ROUND_ROBIN_Splitter_9920));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9921() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9921CombineDFT_9752, pop_float(&SplitJoin91_CombineDFT_Fiss_9943_9964_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9921CombineDFT_9752, pop_float(&SplitJoin91_CombineDFT_Fiss_9943_9964_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_9752() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_9921CombineDFT_9752), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_9720_9756_9925_9946_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9754() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_9720_9756_9925_9946_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9763WEIGHTED_ROUND_ROBIN_Splitter_9754));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_9720_9756_9925_9946_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9763WEIGHTED_ROUND_ROBIN_Splitter_9754));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9755() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9755FloatPrinter_9753, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_9720_9756_9925_9946_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9755FloatPrinter_9753, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_9720_9756_9925_9946_join[1]));
		ENDFOR
	ENDFOR
}}

void FloatPrinter(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void FloatPrinter_9753() {
	FOR(uint32_t, __iter_steady_, 0, <, 2816, __iter_steady_++)
		FloatPrinter(&(WEIGHTED_ROUND_ROBIN_Joiner_9755FloatPrinter_9753));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 4, __iter_init_0_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_9933_9954_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 11, __iter_init_1_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_9931_9952_split[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9813WEIGHTED_ROUND_ROBIN_Splitter_9825);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9826WEIGHTED_ROUND_ROBIN_Splitter_9835);
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_9934_9955_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 11, __iter_init_3_++)
		init_buffer_float(&SplitJoin83_CombineDFT_Fiss_9939_9960_split[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9777WEIGHTED_ROUND_ROBIN_Splitter_9786);
	FOR(int, __iter_init_4_, 0, <, 4, __iter_init_4_++)
		init_buffer_float(&SplitJoin77_FFTReorderSimple_Fiss_9936_9957_join[__iter_init_4_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9866WEIGHTED_ROUND_ROBIN_Splitter_9878);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9879WEIGHTED_ROUND_ROBIN_Splitter_9891);
	FOR(int, __iter_init_5_, 0, <, 2, __iter_init_5_++)
		init_buffer_float(&SplitJoin91_CombineDFT_Fiss_9943_9964_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 11, __iter_init_6_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_9929_9950_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_float(&SplitJoin87_CombineDFT_Fiss_9941_9962_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_9934_9955_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 4, __iter_init_9_++)
		init_buffer_float(&SplitJoin89_CombineDFT_Fiss_9942_9963_join[__iter_init_9_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9915WEIGHTED_ROUND_ROBIN_Splitter_9920);
	FOR(int, __iter_init_10_, 0, <, 4, __iter_init_10_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_9927_9948_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 2, __iter_init_11_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_9926_9947_join[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9846WEIGHTED_ROUND_ROBIN_Splitter_9849);
	FOR(int, __iter_init_12_, 0, <, 11, __iter_init_12_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_9930_9951_split[__iter_init_12_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9892WEIGHTED_ROUND_ROBIN_Splitter_9904);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9850WEIGHTED_ROUND_ROBIN_Splitter_9855);
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_float(&SplitJoin75_FFTReorderSimple_Fiss_9935_9956_join[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 11, __iter_init_14_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_9930_9951_join[__iter_init_14_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9763WEIGHTED_ROUND_ROBIN_Splitter_9754);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9921CombineDFT_9752);
	FOR(int, __iter_init_15_, 0, <, 11, __iter_init_15_++)
		init_buffer_float(&SplitJoin81_FFTReorderSimple_Fiss_9938_9959_join[__iter_init_15_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9800WEIGHTED_ROUND_ROBIN_Splitter_9812);
	init_buffer_float(&FFTReorderSimple_9742WEIGHTED_ROUND_ROBIN_Splitter_9845);
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_float(&SplitJoin91_CombineDFT_Fiss_9943_9964_split[__iter_init_16_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9842CombineDFT_9741);
	FOR(int, __iter_init_17_, 0, <, 11, __iter_init_17_++)
		init_buffer_float(&SplitJoin85_CombineDFT_Fiss_9940_9961_split[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 8, __iter_init_18_++)
		init_buffer_float(&SplitJoin87_CombineDFT_Fiss_9941_9962_join[__iter_init_18_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9767WEIGHTED_ROUND_ROBIN_Splitter_9770);
	FOR(int, __iter_init_19_, 0, <, 8, __iter_init_19_++)
		init_buffer_float(&SplitJoin79_FFTReorderSimple_Fiss_9937_9958_split[__iter_init_19_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9787WEIGHTED_ROUND_ROBIN_Splitter_9799);
	FOR(int, __iter_init_20_, 0, <, 4, __iter_init_20_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_9927_9948_split[__iter_init_20_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9856WEIGHTED_ROUND_ROBIN_Splitter_9865);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9836WEIGHTED_ROUND_ROBIN_Splitter_9841);
	FOR(int, __iter_init_21_, 0, <, 11, __iter_init_21_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_9931_9952_join[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 2, __iter_init_22_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_9720_9756_9925_9946_join[__iter_init_22_]);
	ENDFOR
	FOR(int, __iter_init_23_, 0, <, 2, __iter_init_23_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_9924_9945_join[__iter_init_23_]);
	ENDFOR
	FOR(int, __iter_init_24_, 0, <, 4, __iter_init_24_++)
		init_buffer_float(&SplitJoin89_CombineDFT_Fiss_9942_9963_split[__iter_init_24_]);
	ENDFOR
	FOR(int, __iter_init_25_, 0, <, 8, __iter_init_25_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_9928_9949_split[__iter_init_25_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9905WEIGHTED_ROUND_ROBIN_Splitter_9914);
	FOR(int, __iter_init_26_, 0, <, 2, __iter_init_26_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_9924_9945_split[__iter_init_26_]);
	ENDFOR
	FOR(int, __iter_init_27_, 0, <, 11, __iter_init_27_++)
		init_buffer_float(&SplitJoin85_CombineDFT_Fiss_9940_9961_join[__iter_init_27_]);
	ENDFOR
	FOR(int, __iter_init_28_, 0, <, 2, __iter_init_28_++)
		init_buffer_float(&SplitJoin75_FFTReorderSimple_Fiss_9935_9956_split[__iter_init_28_]);
	ENDFOR
	FOR(int, __iter_init_29_, 0, <, 4, __iter_init_29_++)
		init_buffer_float(&SplitJoin77_FFTReorderSimple_Fiss_9936_9957_split[__iter_init_29_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9755FloatPrinter_9753);
	FOR(int, __iter_init_30_, 0, <, 8, __iter_init_30_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_9932_9953_join[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 8, __iter_init_31_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_9928_9949_join[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 8, __iter_init_32_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_9932_9953_split[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 8, __iter_init_33_++)
		init_buffer_float(&SplitJoin79_FFTReorderSimple_Fiss_9937_9958_join[__iter_init_33_]);
	ENDFOR
	FOR(int, __iter_init_34_, 0, <, 2, __iter_init_34_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_9926_9947_split[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 4, __iter_init_35_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_9933_9954_split[__iter_init_35_]);
	ENDFOR
	init_buffer_float(&FFTReorderSimple_9731WEIGHTED_ROUND_ROBIN_Splitter_9766);
	FOR(int, __iter_init_36_, 0, <, 11, __iter_init_36_++)
		init_buffer_float(&SplitJoin83_CombineDFT_Fiss_9939_9960_join[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 2, __iter_init_37_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_9720_9756_9925_9946_split[__iter_init_37_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9771WEIGHTED_ROUND_ROBIN_Splitter_9776);
	FOR(int, __iter_init_38_, 0, <, 11, __iter_init_38_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_9929_9950_split[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 11, __iter_init_39_++)
		init_buffer_float(&SplitJoin81_FFTReorderSimple_Fiss_9938_9959_split[__iter_init_39_]);
	ENDFOR
// --- init: CombineDFT_9801
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9801_s.w[i] = real ; 
		CombineDFT_9801_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9802
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9802_s.w[i] = real ; 
		CombineDFT_9802_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9803
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9803_s.w[i] = real ; 
		CombineDFT_9803_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9804
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9804_s.w[i] = real ; 
		CombineDFT_9804_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9805
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9805_s.w[i] = real ; 
		CombineDFT_9805_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9806
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9806_s.w[i] = real ; 
		CombineDFT_9806_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9807
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9807_s.w[i] = real ; 
		CombineDFT_9807_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9808
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9808_s.w[i] = real ; 
		CombineDFT_9808_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9809
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9809_s.w[i] = real ; 
		CombineDFT_9809_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9810
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9810_s.w[i] = real ; 
		CombineDFT_9810_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9811
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9811_s.w[i] = real ; 
		CombineDFT_9811_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9814
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9814_s.w[i] = real ; 
		CombineDFT_9814_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9815
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9815_s.w[i] = real ; 
		CombineDFT_9815_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9816
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9816_s.w[i] = real ; 
		CombineDFT_9816_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9817
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9817_s.w[i] = real ; 
		CombineDFT_9817_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9818
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9818_s.w[i] = real ; 
		CombineDFT_9818_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9819
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9819_s.w[i] = real ; 
		CombineDFT_9819_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9820
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9820_s.w[i] = real ; 
		CombineDFT_9820_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9821
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9821_s.w[i] = real ; 
		CombineDFT_9821_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9822
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9822_s.w[i] = real ; 
		CombineDFT_9822_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9823
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9823_s.w[i] = real ; 
		CombineDFT_9823_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9824
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9824_s.w[i] = real ; 
		CombineDFT_9824_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9827
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9827_s.w[i] = real ; 
		CombineDFT_9827_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9828
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9828_s.w[i] = real ; 
		CombineDFT_9828_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9829
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9829_s.w[i] = real ; 
		CombineDFT_9829_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9830
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9830_s.w[i] = real ; 
		CombineDFT_9830_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9831
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9831_s.w[i] = real ; 
		CombineDFT_9831_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9832
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9832_s.w[i] = real ; 
		CombineDFT_9832_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9833
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9833_s.w[i] = real ; 
		CombineDFT_9833_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9834
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9834_s.w[i] = real ; 
		CombineDFT_9834_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9837
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_9837_s.w[i] = real ; 
		CombineDFT_9837_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9838
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_9838_s.w[i] = real ; 
		CombineDFT_9838_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9839
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_9839_s.w[i] = real ; 
		CombineDFT_9839_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9840
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_9840_s.w[i] = real ; 
		CombineDFT_9840_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9843
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_9843_s.w[i] = real ; 
		CombineDFT_9843_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9844
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_9844_s.w[i] = real ; 
		CombineDFT_9844_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9741
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_9741_s.w[i] = real ; 
		CombineDFT_9741_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9880
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9880_s.w[i] = real ; 
		CombineDFT_9880_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9881
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9881_s.w[i] = real ; 
		CombineDFT_9881_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9882
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9882_s.w[i] = real ; 
		CombineDFT_9882_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9883
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9883_s.w[i] = real ; 
		CombineDFT_9883_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9884
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9884_s.w[i] = real ; 
		CombineDFT_9884_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9885
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9885_s.w[i] = real ; 
		CombineDFT_9885_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9886
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9886_s.w[i] = real ; 
		CombineDFT_9886_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9887
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9887_s.w[i] = real ; 
		CombineDFT_9887_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9888
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9888_s.w[i] = real ; 
		CombineDFT_9888_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9889
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9889_s.w[i] = real ; 
		CombineDFT_9889_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9890
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9890_s.w[i] = real ; 
		CombineDFT_9890_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9893
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9893_s.w[i] = real ; 
		CombineDFT_9893_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9894
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9894_s.w[i] = real ; 
		CombineDFT_9894_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9895
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9895_s.w[i] = real ; 
		CombineDFT_9895_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9896
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9896_s.w[i] = real ; 
		CombineDFT_9896_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9897
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9897_s.w[i] = real ; 
		CombineDFT_9897_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9898
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9898_s.w[i] = real ; 
		CombineDFT_9898_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9899
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9899_s.w[i] = real ; 
		CombineDFT_9899_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9900
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9900_s.w[i] = real ; 
		CombineDFT_9900_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9901
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9901_s.w[i] = real ; 
		CombineDFT_9901_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9902
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9902_s.w[i] = real ; 
		CombineDFT_9902_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9903
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9903_s.w[i] = real ; 
		CombineDFT_9903_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9906
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9906_s.w[i] = real ; 
		CombineDFT_9906_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9907
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9907_s.w[i] = real ; 
		CombineDFT_9907_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9908
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9908_s.w[i] = real ; 
		CombineDFT_9908_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9909
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9909_s.w[i] = real ; 
		CombineDFT_9909_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9910
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9910_s.w[i] = real ; 
		CombineDFT_9910_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9911
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9911_s.w[i] = real ; 
		CombineDFT_9911_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9912
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9912_s.w[i] = real ; 
		CombineDFT_9912_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9913
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9913_s.w[i] = real ; 
		CombineDFT_9913_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9916
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_9916_s.w[i] = real ; 
		CombineDFT_9916_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9917
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_9917_s.w[i] = real ; 
		CombineDFT_9917_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9918
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_9918_s.w[i] = real ; 
		CombineDFT_9918_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9919
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_9919_s.w[i] = real ; 
		CombineDFT_9919_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9922
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_9922_s.w[i] = real ; 
		CombineDFT_9922_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9923
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_9923_s.w[i] = real ; 
		CombineDFT_9923_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9752
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_9752_s.w[i] = real ; 
		CombineDFT_9752_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_9762();
			FFTTestSource_9764();
			FFTTestSource_9765();
		WEIGHTED_ROUND_ROBIN_Joiner_9763();
		WEIGHTED_ROUND_ROBIN_Splitter_9754();
			FFTReorderSimple_9731();
			WEIGHTED_ROUND_ROBIN_Splitter_9766();
				FFTReorderSimple_9768();
				FFTReorderSimple_9769();
			WEIGHTED_ROUND_ROBIN_Joiner_9767();
			WEIGHTED_ROUND_ROBIN_Splitter_9770();
				FFTReorderSimple_9772();
				FFTReorderSimple_9773();
				FFTReorderSimple_9774();
				FFTReorderSimple_9775();
			WEIGHTED_ROUND_ROBIN_Joiner_9771();
			WEIGHTED_ROUND_ROBIN_Splitter_9776();
				FFTReorderSimple_9778();
				FFTReorderSimple_9779();
				FFTReorderSimple_9780();
				FFTReorderSimple_9781();
				FFTReorderSimple_9782();
				FFTReorderSimple_9783();
				FFTReorderSimple_9784();
				FFTReorderSimple_9785();
			WEIGHTED_ROUND_ROBIN_Joiner_9777();
			WEIGHTED_ROUND_ROBIN_Splitter_9786();
				FFTReorderSimple_9788();
				FFTReorderSimple_9789();
				FFTReorderSimple_9790();
				FFTReorderSimple_9791();
				FFTReorderSimple_9792();
				FFTReorderSimple_9793();
				FFTReorderSimple_9794();
				FFTReorderSimple_9795();
				FFTReorderSimple_9796();
				FFTReorderSimple_9797();
				FFTReorderSimple_9798();
			WEIGHTED_ROUND_ROBIN_Joiner_9787();
			WEIGHTED_ROUND_ROBIN_Splitter_9799();
				CombineDFT_9801();
				CombineDFT_9802();
				CombineDFT_9803();
				CombineDFT_9804();
				CombineDFT_9805();
				CombineDFT_9806();
				CombineDFT_9807();
				CombineDFT_9808();
				CombineDFT_9809();
				CombineDFT_9810();
				CombineDFT_9811();
			WEIGHTED_ROUND_ROBIN_Joiner_9800();
			WEIGHTED_ROUND_ROBIN_Splitter_9812();
				CombineDFT_9814();
				CombineDFT_9815();
				CombineDFT_9816();
				CombineDFT_9817();
				CombineDFT_9818();
				CombineDFT_9819();
				CombineDFT_9820();
				CombineDFT_9821();
				CombineDFT_9822();
				CombineDFT_9823();
				CombineDFT_9824();
			WEIGHTED_ROUND_ROBIN_Joiner_9813();
			WEIGHTED_ROUND_ROBIN_Splitter_9825();
				CombineDFT_9827();
				CombineDFT_9828();
				CombineDFT_9829();
				CombineDFT_9830();
				CombineDFT_9831();
				CombineDFT_9832();
				CombineDFT_9833();
				CombineDFT_9834();
			WEIGHTED_ROUND_ROBIN_Joiner_9826();
			WEIGHTED_ROUND_ROBIN_Splitter_9835();
				CombineDFT_9837();
				CombineDFT_9838();
				CombineDFT_9839();
				CombineDFT_9840();
			WEIGHTED_ROUND_ROBIN_Joiner_9836();
			WEIGHTED_ROUND_ROBIN_Splitter_9841();
				CombineDFT_9843();
				CombineDFT_9844();
			WEIGHTED_ROUND_ROBIN_Joiner_9842();
			CombineDFT_9741();
			FFTReorderSimple_9742();
			WEIGHTED_ROUND_ROBIN_Splitter_9845();
				FFTReorderSimple_9847();
				FFTReorderSimple_9848();
			WEIGHTED_ROUND_ROBIN_Joiner_9846();
			WEIGHTED_ROUND_ROBIN_Splitter_9849();
				FFTReorderSimple_9851();
				FFTReorderSimple_9852();
				FFTReorderSimple_9853();
				FFTReorderSimple_9854();
			WEIGHTED_ROUND_ROBIN_Joiner_9850();
			WEIGHTED_ROUND_ROBIN_Splitter_9855();
				FFTReorderSimple_9857();
				FFTReorderSimple_9858();
				FFTReorderSimple_9859();
				FFTReorderSimple_9860();
				FFTReorderSimple_9861();
				FFTReorderSimple_9862();
				FFTReorderSimple_9863();
				FFTReorderSimple_9864();
			WEIGHTED_ROUND_ROBIN_Joiner_9856();
			WEIGHTED_ROUND_ROBIN_Splitter_9865();
				FFTReorderSimple_9867();
				FFTReorderSimple_9868();
				FFTReorderSimple_9869();
				FFTReorderSimple_9870();
				FFTReorderSimple_9871();
				FFTReorderSimple_9872();
				FFTReorderSimple_9873();
				FFTReorderSimple_9874();
				FFTReorderSimple_9875();
				FFTReorderSimple_9876();
				FFTReorderSimple_9877();
			WEIGHTED_ROUND_ROBIN_Joiner_9866();
			WEIGHTED_ROUND_ROBIN_Splitter_9878();
				CombineDFT_9880();
				CombineDFT_9881();
				CombineDFT_9882();
				CombineDFT_9883();
				CombineDFT_9884();
				CombineDFT_9885();
				CombineDFT_9886();
				CombineDFT_9887();
				CombineDFT_9888();
				CombineDFT_9889();
				CombineDFT_9890();
			WEIGHTED_ROUND_ROBIN_Joiner_9879();
			WEIGHTED_ROUND_ROBIN_Splitter_9891();
				CombineDFT_9893();
				CombineDFT_9894();
				CombineDFT_9895();
				CombineDFT_9896();
				CombineDFT_9897();
				CombineDFT_9898();
				CombineDFT_9899();
				CombineDFT_9900();
				CombineDFT_9901();
				CombineDFT_9902();
				CombineDFT_9903();
			WEIGHTED_ROUND_ROBIN_Joiner_9892();
			WEIGHTED_ROUND_ROBIN_Splitter_9904();
				CombineDFT_9906();
				CombineDFT_9907();
				CombineDFT_9908();
				CombineDFT_9909();
				CombineDFT_9910();
				CombineDFT_9911();
				CombineDFT_9912();
				CombineDFT_9913();
			WEIGHTED_ROUND_ROBIN_Joiner_9905();
			WEIGHTED_ROUND_ROBIN_Splitter_9914();
				CombineDFT_9916();
				CombineDFT_9917();
				CombineDFT_9918();
				CombineDFT_9919();
			WEIGHTED_ROUND_ROBIN_Joiner_9915();
			WEIGHTED_ROUND_ROBIN_Splitter_9920();
				CombineDFT_9922();
				CombineDFT_9923();
			WEIGHTED_ROUND_ROBIN_Joiner_9921();
			CombineDFT_9752();
		WEIGHTED_ROUND_ROBIN_Joiner_9755();
		FloatPrinter_9753();
	ENDFOR
	return EXIT_SUCCESS;
}
