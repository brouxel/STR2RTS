#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=384 on the compile command line
#else
#if BUF_SIZEMAX < 384
#error BUF_SIZEMAX too small, it must be at least 384
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float A_re[32];
	float A_im[32];
	int idx;
} FloatSource_8744_t;
void FloatSource_8744();
void Pre_CollapsedDataParallel_1_9045();
void WEIGHTED_ROUND_ROBIN_Splitter_9187();
void Butterfly_9189();
void Butterfly_9190();
void Butterfly_9191();
void Butterfly_9192();
void Butterfly_9193();
void Butterfly_9194();
void WEIGHTED_ROUND_ROBIN_Joiner_9188();
void Post_CollapsedDataParallel_2_9046();
void WEIGHTED_ROUND_ROBIN_Splitter_9089();
void Pre_CollapsedDataParallel_1_9048();
void WEIGHTED_ROUND_ROBIN_Splitter_9195();
void Butterfly_9197();
void Butterfly_9198();
void Butterfly_9199();
void Butterfly_9200();
void Butterfly_9201();
void Butterfly_9202();
void WEIGHTED_ROUND_ROBIN_Joiner_9196();
void Post_CollapsedDataParallel_2_9049();
void WEIGHTED_ROUND_ROBIN_Splitter_9169();
void Pre_CollapsedDataParallel_1_9054();
void WEIGHTED_ROUND_ROBIN_Splitter_9203();
void Butterfly_9205();
void Butterfly_9206();
void Butterfly_9207();
void Butterfly_9208();
void WEIGHTED_ROUND_ROBIN_Joiner_9204();
void Post_CollapsedDataParallel_2_9055();
void Pre_CollapsedDataParallel_1_9057();
void WEIGHTED_ROUND_ROBIN_Splitter_9209();
void Butterfly_9211();
void Butterfly_9212();
void Butterfly_9213();
void Butterfly_9214();
void WEIGHTED_ROUND_ROBIN_Joiner_9210();
void Post_CollapsedDataParallel_2_9058();
void WEIGHTED_ROUND_ROBIN_Joiner_9170();
void Pre_CollapsedDataParallel_1_9051();
void WEIGHTED_ROUND_ROBIN_Splitter_9215();
void Butterfly_9217();
void Butterfly_9218();
void Butterfly_9219();
void Butterfly_9220();
void Butterfly_9221();
void Butterfly_9222();
void WEIGHTED_ROUND_ROBIN_Joiner_9216();
void Post_CollapsedDataParallel_2_9052();
void WEIGHTED_ROUND_ROBIN_Splitter_9171();
void Pre_CollapsedDataParallel_1_9060();
void WEIGHTED_ROUND_ROBIN_Splitter_9223();
void Butterfly_9225();
void Butterfly_9226();
void Butterfly_9227();
void Butterfly_9228();
void WEIGHTED_ROUND_ROBIN_Joiner_9224();
void Post_CollapsedDataParallel_2_9061();
void Pre_CollapsedDataParallel_1_9063();
void WEIGHTED_ROUND_ROBIN_Splitter_9229();
void Butterfly_9231();
void Butterfly_9232();
void Butterfly_9233();
void Butterfly_9234();
void WEIGHTED_ROUND_ROBIN_Joiner_9230();
void Post_CollapsedDataParallel_2_9064();
void WEIGHTED_ROUND_ROBIN_Joiner_9172();
void WEIGHTED_ROUND_ROBIN_Joiner_9173();
void WEIGHTED_ROUND_ROBIN_Splitter_9174();
void WEIGHTED_ROUND_ROBIN_Splitter_9175();
void Pre_CollapsedDataParallel_1_9066();
void WEIGHTED_ROUND_ROBIN_Splitter_9235();
void Butterfly_9237();
void Butterfly_9238();
void WEIGHTED_ROUND_ROBIN_Joiner_9236();
void Post_CollapsedDataParallel_2_9067();
void Pre_CollapsedDataParallel_1_9069();
void WEIGHTED_ROUND_ROBIN_Splitter_9239();
void Butterfly_9241();
void Butterfly_9242();
void WEIGHTED_ROUND_ROBIN_Joiner_9240();
void Post_CollapsedDataParallel_2_9070();
void Pre_CollapsedDataParallel_1_9072();
void WEIGHTED_ROUND_ROBIN_Splitter_9243();
void Butterfly_9245();
void Butterfly_9246();
void WEIGHTED_ROUND_ROBIN_Joiner_9244();
void Post_CollapsedDataParallel_2_9073();
void Pre_CollapsedDataParallel_1_9075();
void WEIGHTED_ROUND_ROBIN_Splitter_9247();
void Butterfly_9249();
void Butterfly_9250();
void WEIGHTED_ROUND_ROBIN_Joiner_9248();
void Post_CollapsedDataParallel_2_9076();
void WEIGHTED_ROUND_ROBIN_Joiner_9176();
void WEIGHTED_ROUND_ROBIN_Splitter_9177();
void Pre_CollapsedDataParallel_1_9078();
void WEIGHTED_ROUND_ROBIN_Splitter_9251();
void Butterfly_9253();
void Butterfly_9254();
void WEIGHTED_ROUND_ROBIN_Joiner_9252();
void Post_CollapsedDataParallel_2_9079();
void Pre_CollapsedDataParallel_1_9081();
void WEIGHTED_ROUND_ROBIN_Splitter_9255();
void Butterfly_9257();
void Butterfly_9258();
void WEIGHTED_ROUND_ROBIN_Joiner_9256();
void Post_CollapsedDataParallel_2_9082();
void Pre_CollapsedDataParallel_1_9084();
void WEIGHTED_ROUND_ROBIN_Splitter_9259();
void Butterfly_9261();
void Butterfly_9262();
void WEIGHTED_ROUND_ROBIN_Joiner_9260();
void Post_CollapsedDataParallel_2_9085();
void Pre_CollapsedDataParallel_1_9087();
void WEIGHTED_ROUND_ROBIN_Splitter_9263();
void Butterfly_9265();
void Butterfly_9266();
void WEIGHTED_ROUND_ROBIN_Joiner_9264();
void Post_CollapsedDataParallel_2_9088();
void WEIGHTED_ROUND_ROBIN_Joiner_9178();
void WEIGHTED_ROUND_ROBIN_Joiner_9179();
void WEIGHTED_ROUND_ROBIN_Splitter_9180();
void WEIGHTED_ROUND_ROBIN_Splitter_9181();
void Butterfly_8809();
void Butterfly_8810();
void Butterfly_8811();
void Butterfly_8812();
void Butterfly_8813();
void Butterfly_8814();
void Butterfly_8815();
void Butterfly_8816();
void WEIGHTED_ROUND_ROBIN_Joiner_9182();
void WEIGHTED_ROUND_ROBIN_Splitter_9183();
void Butterfly_8817();
void Butterfly_8818();
void Butterfly_8819();
void Butterfly_8820();
void Butterfly_8821();
void Butterfly_8822();
void Butterfly_8823();
void Butterfly_8824();
void WEIGHTED_ROUND_ROBIN_Joiner_9184();
void WEIGHTED_ROUND_ROBIN_Joiner_9185();
int BitReverse_8825_bitrev(int inp, int numbits);
void BitReverse_8825();
void FloatPrinter_8826();

#ifdef __cplusplus
}
#endif
#endif
