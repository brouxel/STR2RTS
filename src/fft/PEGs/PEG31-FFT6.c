#include "PEG31-FFT6.h"

buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_443_453_join[16];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_447_457_join[4];
buffer_complex_t SplitJoin12_CombineDFT_Fiss_446_456_split[8];
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_441_451_join[4];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_442_452_join[8];
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_443_453_split[16];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_336WEIGHTED_ROUND_ROBIN_Splitter_341;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_342WEIGHTED_ROUND_ROBIN_Splitter_351;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_352WEIGHTED_ROUND_ROBIN_Splitter_369;
buffer_complex_t SplitJoin14_CombineDFT_Fiss_447_457_split[4];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_442_452_split[8];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_445_455_split[16];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_431WEIGHTED_ROUND_ROBIN_Splitter_436;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_437CombineDFT_328;
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_440_450_join[2];
buffer_complex_t SplitJoin8_CombineDFT_Fiss_444_454_join[31];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_440_450_split[2];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_445_455_join[16];
buffer_complex_t FFTReorderSimple_318WEIGHTED_ROUND_ROBIN_Splitter_331;
buffer_complex_t SplitJoin16_CombineDFT_Fiss_448_458_join[2];
buffer_complex_t SplitJoin8_CombineDFT_Fiss_444_454_split[31];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_370WEIGHTED_ROUND_ROBIN_Splitter_402;
buffer_complex_t CombineDFT_328CPrinter_329;
buffer_complex_t SplitJoin12_CombineDFT_Fiss_446_456_join[8];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_403WEIGHTED_ROUND_ROBIN_Splitter_420;
buffer_complex_t SplitJoin16_CombineDFT_Fiss_448_458_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_421WEIGHTED_ROUND_ROBIN_Splitter_430;
buffer_complex_t FFTTestSource_317FFTReorderSimple_318;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_332WEIGHTED_ROUND_ROBIN_Splitter_335;
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_441_451_split[4];


CombineDFT_371_t CombineDFT_371_s;
CombineDFT_371_t CombineDFT_372_s;
CombineDFT_371_t CombineDFT_373_s;
CombineDFT_371_t CombineDFT_374_s;
CombineDFT_371_t CombineDFT_375_s;
CombineDFT_371_t CombineDFT_376_s;
CombineDFT_371_t CombineDFT_377_s;
CombineDFT_371_t CombineDFT_378_s;
CombineDFT_371_t CombineDFT_379_s;
CombineDFT_371_t CombineDFT_380_s;
CombineDFT_371_t CombineDFT_381_s;
CombineDFT_371_t CombineDFT_382_s;
CombineDFT_371_t CombineDFT_383_s;
CombineDFT_371_t CombineDFT_384_s;
CombineDFT_371_t CombineDFT_385_s;
CombineDFT_371_t CombineDFT_386_s;
CombineDFT_371_t CombineDFT_387_s;
CombineDFT_371_t CombineDFT_388_s;
CombineDFT_371_t CombineDFT_389_s;
CombineDFT_371_t CombineDFT_390_s;
CombineDFT_371_t CombineDFT_391_s;
CombineDFT_371_t CombineDFT_392_s;
CombineDFT_371_t CombineDFT_393_s;
CombineDFT_371_t CombineDFT_394_s;
CombineDFT_371_t CombineDFT_395_s;
CombineDFT_371_t CombineDFT_396_s;
CombineDFT_371_t CombineDFT_397_s;
CombineDFT_371_t CombineDFT_398_s;
CombineDFT_371_t CombineDFT_399_s;
CombineDFT_371_t CombineDFT_400_s;
CombineDFT_371_t CombineDFT_401_s;
CombineDFT_371_t CombineDFT_404_s;
CombineDFT_371_t CombineDFT_405_s;
CombineDFT_371_t CombineDFT_406_s;
CombineDFT_371_t CombineDFT_407_s;
CombineDFT_371_t CombineDFT_408_s;
CombineDFT_371_t CombineDFT_409_s;
CombineDFT_371_t CombineDFT_410_s;
CombineDFT_371_t CombineDFT_411_s;
CombineDFT_371_t CombineDFT_412_s;
CombineDFT_371_t CombineDFT_413_s;
CombineDFT_371_t CombineDFT_414_s;
CombineDFT_371_t CombineDFT_415_s;
CombineDFT_371_t CombineDFT_416_s;
CombineDFT_371_t CombineDFT_417_s;
CombineDFT_371_t CombineDFT_418_s;
CombineDFT_371_t CombineDFT_419_s;
CombineDFT_371_t CombineDFT_422_s;
CombineDFT_371_t CombineDFT_423_s;
CombineDFT_371_t CombineDFT_424_s;
CombineDFT_371_t CombineDFT_425_s;
CombineDFT_371_t CombineDFT_426_s;
CombineDFT_371_t CombineDFT_427_s;
CombineDFT_371_t CombineDFT_428_s;
CombineDFT_371_t CombineDFT_429_s;
CombineDFT_371_t CombineDFT_432_s;
CombineDFT_371_t CombineDFT_433_s;
CombineDFT_371_t CombineDFT_434_s;
CombineDFT_371_t CombineDFT_435_s;
CombineDFT_371_t CombineDFT_438_s;
CombineDFT_371_t CombineDFT_439_s;
CombineDFT_371_t CombineDFT_328_s;

void FFTTestSource(buffer_complex_t *chanout) {
		complex_t c1;
		complex_t zero;
		c1.real = 1.0 ; 
		c1.imag = 0.0 ; 
		zero.real = 0.0 ; 
		zero.imag = 0.0 ; 
		push_complex(&(*chanout), zero) ; 
		push_complex(&(*chanout), c1) ; 
		FOR(int, i, 0,  < , 62, i++) {
			push_complex(&(*chanout), zero) ; 
		}
		ENDFOR
	}


void FFTTestSource_317() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTTestSource(&(FFTTestSource_317FFTReorderSimple_318));
	ENDFOR
}

void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 64, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&(*chanin)) ; 
		}
		ENDFOR
	}


void FFTReorderSimple_318() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTReorderSimple(&(FFTTestSource_317FFTReorderSimple_318), &(FFTReorderSimple_318WEIGHTED_ROUND_ROBIN_Splitter_331));
	ENDFOR
}

void FFTReorderSimple_333() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_440_450_split[0]), &(SplitJoin0_FFTReorderSimple_Fiss_440_450_join[0]));
	ENDFOR
}

void FFTReorderSimple_334() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_440_450_split[1]), &(SplitJoin0_FFTReorderSimple_Fiss_440_450_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_331() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_440_450_split[0], pop_complex(&FFTReorderSimple_318WEIGHTED_ROUND_ROBIN_Splitter_331));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_440_450_split[1], pop_complex(&FFTReorderSimple_318WEIGHTED_ROUND_ROBIN_Splitter_331));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_332() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_332WEIGHTED_ROUND_ROBIN_Splitter_335, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_440_450_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_332WEIGHTED_ROUND_ROBIN_Splitter_335, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_440_450_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_337() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_441_451_split[0]), &(SplitJoin2_FFTReorderSimple_Fiss_441_451_join[0]));
	ENDFOR
}

void FFTReorderSimple_338() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_441_451_split[1]), &(SplitJoin2_FFTReorderSimple_Fiss_441_451_join[1]));
	ENDFOR
}

void FFTReorderSimple_339() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_441_451_split[2]), &(SplitJoin2_FFTReorderSimple_Fiss_441_451_join[2]));
	ENDFOR
}

void FFTReorderSimple_340() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_441_451_split[3]), &(SplitJoin2_FFTReorderSimple_Fiss_441_451_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_335() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin2_FFTReorderSimple_Fiss_441_451_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_332WEIGHTED_ROUND_ROBIN_Splitter_335));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_336() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_336WEIGHTED_ROUND_ROBIN_Splitter_341, pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_441_451_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_343() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_442_452_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_442_452_join[0]));
	ENDFOR
}

void FFTReorderSimple_344() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_442_452_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_442_452_join[1]));
	ENDFOR
}

void FFTReorderSimple_345() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_442_452_split[2]), &(SplitJoin4_FFTReorderSimple_Fiss_442_452_join[2]));
	ENDFOR
}

void FFTReorderSimple_346() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_442_452_split[3]), &(SplitJoin4_FFTReorderSimple_Fiss_442_452_join[3]));
	ENDFOR
}

void FFTReorderSimple_347() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_442_452_split[4]), &(SplitJoin4_FFTReorderSimple_Fiss_442_452_join[4]));
	ENDFOR
}

void FFTReorderSimple_348() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_442_452_split[5]), &(SplitJoin4_FFTReorderSimple_Fiss_442_452_join[5]));
	ENDFOR
}

void FFTReorderSimple_349() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_442_452_split[6]), &(SplitJoin4_FFTReorderSimple_Fiss_442_452_join[6]));
	ENDFOR
}

void FFTReorderSimple_350() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_442_452_split[7]), &(SplitJoin4_FFTReorderSimple_Fiss_442_452_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_341() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin4_FFTReorderSimple_Fiss_442_452_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_336WEIGHTED_ROUND_ROBIN_Splitter_341));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_342() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_342WEIGHTED_ROUND_ROBIN_Splitter_351, pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_442_452_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_353() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_443_453_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_443_453_join[0]));
	ENDFOR
}

void FFTReorderSimple_354() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_443_453_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_443_453_join[1]));
	ENDFOR
}

void FFTReorderSimple_355() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_443_453_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_443_453_join[2]));
	ENDFOR
}

void FFTReorderSimple_356() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_443_453_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_443_453_join[3]));
	ENDFOR
}

void FFTReorderSimple_357() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_443_453_split[4]), &(SplitJoin6_FFTReorderSimple_Fiss_443_453_join[4]));
	ENDFOR
}

void FFTReorderSimple_358() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_443_453_split[5]), &(SplitJoin6_FFTReorderSimple_Fiss_443_453_join[5]));
	ENDFOR
}

void FFTReorderSimple_359() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_443_453_split[6]), &(SplitJoin6_FFTReorderSimple_Fiss_443_453_join[6]));
	ENDFOR
}

void FFTReorderSimple_360() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_443_453_split[7]), &(SplitJoin6_FFTReorderSimple_Fiss_443_453_join[7]));
	ENDFOR
}

void FFTReorderSimple_361() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_443_453_split[8]), &(SplitJoin6_FFTReorderSimple_Fiss_443_453_join[8]));
	ENDFOR
}

void FFTReorderSimple_362() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_443_453_split[9]), &(SplitJoin6_FFTReorderSimple_Fiss_443_453_join[9]));
	ENDFOR
}

void FFTReorderSimple_363() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_443_453_split[10]), &(SplitJoin6_FFTReorderSimple_Fiss_443_453_join[10]));
	ENDFOR
}

void FFTReorderSimple_364() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_443_453_split[11]), &(SplitJoin6_FFTReorderSimple_Fiss_443_453_join[11]));
	ENDFOR
}

void FFTReorderSimple_365() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_443_453_split[12]), &(SplitJoin6_FFTReorderSimple_Fiss_443_453_join[12]));
	ENDFOR
}

void FFTReorderSimple_366() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_443_453_split[13]), &(SplitJoin6_FFTReorderSimple_Fiss_443_453_join[13]));
	ENDFOR
}

void FFTReorderSimple_367() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_443_453_split[14]), &(SplitJoin6_FFTReorderSimple_Fiss_443_453_join[14]));
	ENDFOR
}

void FFTReorderSimple_368() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_443_453_split[15]), &(SplitJoin6_FFTReorderSimple_Fiss_443_453_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_351() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin6_FFTReorderSimple_Fiss_443_453_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_342WEIGHTED_ROUND_ROBIN_Splitter_351));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_352() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_352WEIGHTED_ROUND_ROBIN_Splitter_369, pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_443_453_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&(*chanin), (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_371_s.wn.real) - (w.imag * CombineDFT_371_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_371_s.wn.imag) + (w.imag * CombineDFT_371_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&(*chanin)) ; 
			push_complex(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_371() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_444_454_split[0]), &(SplitJoin8_CombineDFT_Fiss_444_454_join[0]));
	ENDFOR
}

void CombineDFT_372() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_444_454_split[1]), &(SplitJoin8_CombineDFT_Fiss_444_454_join[1]));
	ENDFOR
}

void CombineDFT_373() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_444_454_split[2]), &(SplitJoin8_CombineDFT_Fiss_444_454_join[2]));
	ENDFOR
}

void CombineDFT_374() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_444_454_split[3]), &(SplitJoin8_CombineDFT_Fiss_444_454_join[3]));
	ENDFOR
}

void CombineDFT_375() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_444_454_split[4]), &(SplitJoin8_CombineDFT_Fiss_444_454_join[4]));
	ENDFOR
}

void CombineDFT_376() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_444_454_split[5]), &(SplitJoin8_CombineDFT_Fiss_444_454_join[5]));
	ENDFOR
}

void CombineDFT_377() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_444_454_split[6]), &(SplitJoin8_CombineDFT_Fiss_444_454_join[6]));
	ENDFOR
}

void CombineDFT_378() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_444_454_split[7]), &(SplitJoin8_CombineDFT_Fiss_444_454_join[7]));
	ENDFOR
}

void CombineDFT_379() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_444_454_split[8]), &(SplitJoin8_CombineDFT_Fiss_444_454_join[8]));
	ENDFOR
}

void CombineDFT_380() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_444_454_split[9]), &(SplitJoin8_CombineDFT_Fiss_444_454_join[9]));
	ENDFOR
}

void CombineDFT_381() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_444_454_split[10]), &(SplitJoin8_CombineDFT_Fiss_444_454_join[10]));
	ENDFOR
}

void CombineDFT_382() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_444_454_split[11]), &(SplitJoin8_CombineDFT_Fiss_444_454_join[11]));
	ENDFOR
}

void CombineDFT_383() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_444_454_split[12]), &(SplitJoin8_CombineDFT_Fiss_444_454_join[12]));
	ENDFOR
}

void CombineDFT_384() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_444_454_split[13]), &(SplitJoin8_CombineDFT_Fiss_444_454_join[13]));
	ENDFOR
}

void CombineDFT_385() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_444_454_split[14]), &(SplitJoin8_CombineDFT_Fiss_444_454_join[14]));
	ENDFOR
}

void CombineDFT_386() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_444_454_split[15]), &(SplitJoin8_CombineDFT_Fiss_444_454_join[15]));
	ENDFOR
}

void CombineDFT_387() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_444_454_split[16]), &(SplitJoin8_CombineDFT_Fiss_444_454_join[16]));
	ENDFOR
}

void CombineDFT_388() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_444_454_split[17]), &(SplitJoin8_CombineDFT_Fiss_444_454_join[17]));
	ENDFOR
}

void CombineDFT_389() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_444_454_split[18]), &(SplitJoin8_CombineDFT_Fiss_444_454_join[18]));
	ENDFOR
}

void CombineDFT_390() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_444_454_split[19]), &(SplitJoin8_CombineDFT_Fiss_444_454_join[19]));
	ENDFOR
}

void CombineDFT_391() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_444_454_split[20]), &(SplitJoin8_CombineDFT_Fiss_444_454_join[20]));
	ENDFOR
}

void CombineDFT_392() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_444_454_split[21]), &(SplitJoin8_CombineDFT_Fiss_444_454_join[21]));
	ENDFOR
}

void CombineDFT_393() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_444_454_split[22]), &(SplitJoin8_CombineDFT_Fiss_444_454_join[22]));
	ENDFOR
}

void CombineDFT_394() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_444_454_split[23]), &(SplitJoin8_CombineDFT_Fiss_444_454_join[23]));
	ENDFOR
}

void CombineDFT_395() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_444_454_split[24]), &(SplitJoin8_CombineDFT_Fiss_444_454_join[24]));
	ENDFOR
}

void CombineDFT_396() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_444_454_split[25]), &(SplitJoin8_CombineDFT_Fiss_444_454_join[25]));
	ENDFOR
}

void CombineDFT_397() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_444_454_split[26]), &(SplitJoin8_CombineDFT_Fiss_444_454_join[26]));
	ENDFOR
}

void CombineDFT_398() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_444_454_split[27]), &(SplitJoin8_CombineDFT_Fiss_444_454_join[27]));
	ENDFOR
}

void CombineDFT_399() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_444_454_split[28]), &(SplitJoin8_CombineDFT_Fiss_444_454_join[28]));
	ENDFOR
}

void CombineDFT_400() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_444_454_split[29]), &(SplitJoin8_CombineDFT_Fiss_444_454_join[29]));
	ENDFOR
}

void CombineDFT_401() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_444_454_split[30]), &(SplitJoin8_CombineDFT_Fiss_444_454_join[30]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_369() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 31, __iter_++)
			push_complex(&SplitJoin8_CombineDFT_Fiss_444_454_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_352WEIGHTED_ROUND_ROBIN_Splitter_369));
			push_complex(&SplitJoin8_CombineDFT_Fiss_444_454_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_352WEIGHTED_ROUND_ROBIN_Splitter_369));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_370() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 31, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_370WEIGHTED_ROUND_ROBIN_Splitter_402, pop_complex(&SplitJoin8_CombineDFT_Fiss_444_454_join[__iter_]));
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_370WEIGHTED_ROUND_ROBIN_Splitter_402, pop_complex(&SplitJoin8_CombineDFT_Fiss_444_454_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_404() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_445_455_split[0]), &(SplitJoin10_CombineDFT_Fiss_445_455_join[0]));
	ENDFOR
}

void CombineDFT_405() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_445_455_split[1]), &(SplitJoin10_CombineDFT_Fiss_445_455_join[1]));
	ENDFOR
}

void CombineDFT_406() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_445_455_split[2]), &(SplitJoin10_CombineDFT_Fiss_445_455_join[2]));
	ENDFOR
}

void CombineDFT_407() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_445_455_split[3]), &(SplitJoin10_CombineDFT_Fiss_445_455_join[3]));
	ENDFOR
}

void CombineDFT_408() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_445_455_split[4]), &(SplitJoin10_CombineDFT_Fiss_445_455_join[4]));
	ENDFOR
}

void CombineDFT_409() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_445_455_split[5]), &(SplitJoin10_CombineDFT_Fiss_445_455_join[5]));
	ENDFOR
}

void CombineDFT_410() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_445_455_split[6]), &(SplitJoin10_CombineDFT_Fiss_445_455_join[6]));
	ENDFOR
}

void CombineDFT_411() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_445_455_split[7]), &(SplitJoin10_CombineDFT_Fiss_445_455_join[7]));
	ENDFOR
}

void CombineDFT_412() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_445_455_split[8]), &(SplitJoin10_CombineDFT_Fiss_445_455_join[8]));
	ENDFOR
}

void CombineDFT_413() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_445_455_split[9]), &(SplitJoin10_CombineDFT_Fiss_445_455_join[9]));
	ENDFOR
}

void CombineDFT_414() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_445_455_split[10]), &(SplitJoin10_CombineDFT_Fiss_445_455_join[10]));
	ENDFOR
}

void CombineDFT_415() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_445_455_split[11]), &(SplitJoin10_CombineDFT_Fiss_445_455_join[11]));
	ENDFOR
}

void CombineDFT_416() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_445_455_split[12]), &(SplitJoin10_CombineDFT_Fiss_445_455_join[12]));
	ENDFOR
}

void CombineDFT_417() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_445_455_split[13]), &(SplitJoin10_CombineDFT_Fiss_445_455_join[13]));
	ENDFOR
}

void CombineDFT_418() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_445_455_split[14]), &(SplitJoin10_CombineDFT_Fiss_445_455_join[14]));
	ENDFOR
}

void CombineDFT_419() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_445_455_split[15]), &(SplitJoin10_CombineDFT_Fiss_445_455_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_402() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin10_CombineDFT_Fiss_445_455_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_370WEIGHTED_ROUND_ROBIN_Splitter_402));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_403() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_403WEIGHTED_ROUND_ROBIN_Splitter_420, pop_complex(&SplitJoin10_CombineDFT_Fiss_445_455_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_422() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_446_456_split[0]), &(SplitJoin12_CombineDFT_Fiss_446_456_join[0]));
	ENDFOR
}

void CombineDFT_423() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_446_456_split[1]), &(SplitJoin12_CombineDFT_Fiss_446_456_join[1]));
	ENDFOR
}

void CombineDFT_424() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_446_456_split[2]), &(SplitJoin12_CombineDFT_Fiss_446_456_join[2]));
	ENDFOR
}

void CombineDFT_425() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_446_456_split[3]), &(SplitJoin12_CombineDFT_Fiss_446_456_join[3]));
	ENDFOR
}

void CombineDFT_426() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_446_456_split[4]), &(SplitJoin12_CombineDFT_Fiss_446_456_join[4]));
	ENDFOR
}

void CombineDFT_427() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_446_456_split[5]), &(SplitJoin12_CombineDFT_Fiss_446_456_join[5]));
	ENDFOR
}

void CombineDFT_428() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_446_456_split[6]), &(SplitJoin12_CombineDFT_Fiss_446_456_join[6]));
	ENDFOR
}

void CombineDFT_429() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_446_456_split[7]), &(SplitJoin12_CombineDFT_Fiss_446_456_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_420() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin12_CombineDFT_Fiss_446_456_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_403WEIGHTED_ROUND_ROBIN_Splitter_420));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_421() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_421WEIGHTED_ROUND_ROBIN_Splitter_430, pop_complex(&SplitJoin12_CombineDFT_Fiss_446_456_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_432() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_447_457_split[0]), &(SplitJoin14_CombineDFT_Fiss_447_457_join[0]));
	ENDFOR
}

void CombineDFT_433() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_447_457_split[1]), &(SplitJoin14_CombineDFT_Fiss_447_457_join[1]));
	ENDFOR
}

void CombineDFT_434() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_447_457_split[2]), &(SplitJoin14_CombineDFT_Fiss_447_457_join[2]));
	ENDFOR
}

void CombineDFT_435() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_447_457_split[3]), &(SplitJoin14_CombineDFT_Fiss_447_457_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_430() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin14_CombineDFT_Fiss_447_457_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_421WEIGHTED_ROUND_ROBIN_Splitter_430));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_431() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_431WEIGHTED_ROUND_ROBIN_Splitter_436, pop_complex(&SplitJoin14_CombineDFT_Fiss_447_457_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_438() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_448_458_split[0]), &(SplitJoin16_CombineDFT_Fiss_448_458_join[0]));
	ENDFOR
}

void CombineDFT_439() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_448_458_split[1]), &(SplitJoin16_CombineDFT_Fiss_448_458_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_436() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_448_458_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_431WEIGHTED_ROUND_ROBIN_Splitter_436));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_448_458_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_431WEIGHTED_ROUND_ROBIN_Splitter_436));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_437() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_437CombineDFT_328, pop_complex(&SplitJoin16_CombineDFT_Fiss_448_458_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_437CombineDFT_328, pop_complex(&SplitJoin16_CombineDFT_Fiss_448_458_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_328() {
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_437CombineDFT_328), &(CombineDFT_328CPrinter_329));
	ENDFOR
}

void CPrinter(buffer_complex_t *chanin) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		printf("%.10f", c.real);
		printf("\n");
		printf("%.10f", c.imag);
		printf("\n");
	}


void CPrinter_329() {
	FOR(uint32_t, __iter_steady_, 0, <, 1984, __iter_steady_++)
		CPrinter(&(CombineDFT_328CPrinter_329));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 16, __iter_init_0_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_443_453_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 4, __iter_init_1_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_447_457_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_446_456_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 4, __iter_init_3_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_441_451_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_442_452_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 16, __iter_init_5_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_443_453_split[__iter_init_5_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_336WEIGHTED_ROUND_ROBIN_Splitter_341);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_342WEIGHTED_ROUND_ROBIN_Splitter_351);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_352WEIGHTED_ROUND_ROBIN_Splitter_369);
	FOR(int, __iter_init_6_, 0, <, 4, __iter_init_6_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_447_457_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_442_452_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 16, __iter_init_8_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_445_455_split[__iter_init_8_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_431WEIGHTED_ROUND_ROBIN_Splitter_436);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_437CombineDFT_328);
	FOR(int, __iter_init_9_, 0, <, 2, __iter_init_9_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_440_450_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 31, __iter_init_10_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_444_454_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 2, __iter_init_11_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_440_450_split[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 16, __iter_init_12_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_445_455_join[__iter_init_12_]);
	ENDFOR
	init_buffer_complex(&FFTReorderSimple_318WEIGHTED_ROUND_ROBIN_Splitter_331);
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_448_458_join[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 31, __iter_init_14_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_444_454_split[__iter_init_14_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_370WEIGHTED_ROUND_ROBIN_Splitter_402);
	init_buffer_complex(&CombineDFT_328CPrinter_329);
	FOR(int, __iter_init_15_, 0, <, 8, __iter_init_15_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_446_456_join[__iter_init_15_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_403WEIGHTED_ROUND_ROBIN_Splitter_420);
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_448_458_split[__iter_init_16_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_421WEIGHTED_ROUND_ROBIN_Splitter_430);
	init_buffer_complex(&FFTTestSource_317FFTReorderSimple_318);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_332WEIGHTED_ROUND_ROBIN_Splitter_335);
	FOR(int, __iter_init_17_, 0, <, 4, __iter_init_17_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_441_451_split[__iter_init_17_]);
	ENDFOR
// --- init: CombineDFT_371
	 {
	 ; 
	CombineDFT_371_s.wn.real = -1.0 ; 
	CombineDFT_371_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_372
	 {
	CombineDFT_372_s.wn.real = -1.0 ; 
	CombineDFT_372_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_373
	 {
	CombineDFT_373_s.wn.real = -1.0 ; 
	CombineDFT_373_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_374
	 {
	CombineDFT_374_s.wn.real = -1.0 ; 
	CombineDFT_374_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_375
	 {
	CombineDFT_375_s.wn.real = -1.0 ; 
	CombineDFT_375_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_376
	 {
	CombineDFT_376_s.wn.real = -1.0 ; 
	CombineDFT_376_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_377
	 {
	CombineDFT_377_s.wn.real = -1.0 ; 
	CombineDFT_377_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_378
	 {
	CombineDFT_378_s.wn.real = -1.0 ; 
	CombineDFT_378_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_379
	 {
	CombineDFT_379_s.wn.real = -1.0 ; 
	CombineDFT_379_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_380
	 {
	CombineDFT_380_s.wn.real = -1.0 ; 
	CombineDFT_380_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_381
	 {
	CombineDFT_381_s.wn.real = -1.0 ; 
	CombineDFT_381_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_382
	 {
	CombineDFT_382_s.wn.real = -1.0 ; 
	CombineDFT_382_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_383
	 {
	CombineDFT_383_s.wn.real = -1.0 ; 
	CombineDFT_383_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_384
	 {
	CombineDFT_384_s.wn.real = -1.0 ; 
	CombineDFT_384_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_385
	 {
	CombineDFT_385_s.wn.real = -1.0 ; 
	CombineDFT_385_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_386
	 {
	CombineDFT_386_s.wn.real = -1.0 ; 
	CombineDFT_386_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_387
	 {
	CombineDFT_387_s.wn.real = -1.0 ; 
	CombineDFT_387_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_388
	 {
	CombineDFT_388_s.wn.real = -1.0 ; 
	CombineDFT_388_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_389
	 {
	CombineDFT_389_s.wn.real = -1.0 ; 
	CombineDFT_389_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_390
	 {
	CombineDFT_390_s.wn.real = -1.0 ; 
	CombineDFT_390_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_391
	 {
	CombineDFT_391_s.wn.real = -1.0 ; 
	CombineDFT_391_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_392
	 {
	CombineDFT_392_s.wn.real = -1.0 ; 
	CombineDFT_392_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_393
	 {
	CombineDFT_393_s.wn.real = -1.0 ; 
	CombineDFT_393_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_394
	 {
	CombineDFT_394_s.wn.real = -1.0 ; 
	CombineDFT_394_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_395
	 {
	CombineDFT_395_s.wn.real = -1.0 ; 
	CombineDFT_395_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_396
	 {
	CombineDFT_396_s.wn.real = -1.0 ; 
	CombineDFT_396_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_397
	 {
	CombineDFT_397_s.wn.real = -1.0 ; 
	CombineDFT_397_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_398
	 {
	CombineDFT_398_s.wn.real = -1.0 ; 
	CombineDFT_398_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_399
	 {
	CombineDFT_399_s.wn.real = -1.0 ; 
	CombineDFT_399_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_400
	 {
	CombineDFT_400_s.wn.real = -1.0 ; 
	CombineDFT_400_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_401
	 {
	CombineDFT_401_s.wn.real = -1.0 ; 
	CombineDFT_401_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_404
	 {
	CombineDFT_404_s.wn.real = -4.371139E-8 ; 
	CombineDFT_404_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_405
	 {
	CombineDFT_405_s.wn.real = -4.371139E-8 ; 
	CombineDFT_405_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_406
	 {
	CombineDFT_406_s.wn.real = -4.371139E-8 ; 
	CombineDFT_406_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_407
	 {
	CombineDFT_407_s.wn.real = -4.371139E-8 ; 
	CombineDFT_407_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_408
	 {
	CombineDFT_408_s.wn.real = -4.371139E-8 ; 
	CombineDFT_408_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_409
	 {
	CombineDFT_409_s.wn.real = -4.371139E-8 ; 
	CombineDFT_409_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_410
	 {
	CombineDFT_410_s.wn.real = -4.371139E-8 ; 
	CombineDFT_410_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_411
	 {
	CombineDFT_411_s.wn.real = -4.371139E-8 ; 
	CombineDFT_411_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_412
	 {
	CombineDFT_412_s.wn.real = -4.371139E-8 ; 
	CombineDFT_412_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_413
	 {
	CombineDFT_413_s.wn.real = -4.371139E-8 ; 
	CombineDFT_413_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_414
	 {
	CombineDFT_414_s.wn.real = -4.371139E-8 ; 
	CombineDFT_414_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_415
	 {
	CombineDFT_415_s.wn.real = -4.371139E-8 ; 
	CombineDFT_415_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_416
	 {
	CombineDFT_416_s.wn.real = -4.371139E-8 ; 
	CombineDFT_416_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_417
	 {
	CombineDFT_417_s.wn.real = -4.371139E-8 ; 
	CombineDFT_417_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_418
	 {
	CombineDFT_418_s.wn.real = -4.371139E-8 ; 
	CombineDFT_418_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_419
	 {
	CombineDFT_419_s.wn.real = -4.371139E-8 ; 
	CombineDFT_419_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_422
	 {
	CombineDFT_422_s.wn.real = 0.70710677 ; 
	CombineDFT_422_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_423
	 {
	CombineDFT_423_s.wn.real = 0.70710677 ; 
	CombineDFT_423_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_424
	 {
	CombineDFT_424_s.wn.real = 0.70710677 ; 
	CombineDFT_424_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_425
	 {
	CombineDFT_425_s.wn.real = 0.70710677 ; 
	CombineDFT_425_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_426
	 {
	CombineDFT_426_s.wn.real = 0.70710677 ; 
	CombineDFT_426_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_427
	 {
	CombineDFT_427_s.wn.real = 0.70710677 ; 
	CombineDFT_427_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_428
	 {
	CombineDFT_428_s.wn.real = 0.70710677 ; 
	CombineDFT_428_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_429
	 {
	CombineDFT_429_s.wn.real = 0.70710677 ; 
	CombineDFT_429_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_432
	 {
	CombineDFT_432_s.wn.real = 0.9238795 ; 
	CombineDFT_432_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_433
	 {
	CombineDFT_433_s.wn.real = 0.9238795 ; 
	CombineDFT_433_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_434
	 {
	CombineDFT_434_s.wn.real = 0.9238795 ; 
	CombineDFT_434_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_435
	 {
	CombineDFT_435_s.wn.real = 0.9238795 ; 
	CombineDFT_435_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_438
	 {
	CombineDFT_438_s.wn.real = 0.98078525 ; 
	CombineDFT_438_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_439
	 {
	CombineDFT_439_s.wn.real = 0.98078525 ; 
	CombineDFT_439_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_328
	 {
	 ; 
	CombineDFT_328_s.wn.real = 0.9951847 ; 
	CombineDFT_328_s.wn.imag = -0.09801714 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FFTTestSource_317();
		FFTReorderSimple_318();
		WEIGHTED_ROUND_ROBIN_Splitter_331();
			FFTReorderSimple_333();
			FFTReorderSimple_334();
		WEIGHTED_ROUND_ROBIN_Joiner_332();
		WEIGHTED_ROUND_ROBIN_Splitter_335();
			FFTReorderSimple_337();
			FFTReorderSimple_338();
			FFTReorderSimple_339();
			FFTReorderSimple_340();
		WEIGHTED_ROUND_ROBIN_Joiner_336();
		WEIGHTED_ROUND_ROBIN_Splitter_341();
			FFTReorderSimple_343();
			FFTReorderSimple_344();
			FFTReorderSimple_345();
			FFTReorderSimple_346();
			FFTReorderSimple_347();
			FFTReorderSimple_348();
			FFTReorderSimple_349();
			FFTReorderSimple_350();
		WEIGHTED_ROUND_ROBIN_Joiner_342();
		WEIGHTED_ROUND_ROBIN_Splitter_351();
			FFTReorderSimple_353();
			FFTReorderSimple_354();
			FFTReorderSimple_355();
			FFTReorderSimple_356();
			FFTReorderSimple_357();
			FFTReorderSimple_358();
			FFTReorderSimple_359();
			FFTReorderSimple_360();
			FFTReorderSimple_361();
			FFTReorderSimple_362();
			FFTReorderSimple_363();
			FFTReorderSimple_364();
			FFTReorderSimple_365();
			FFTReorderSimple_366();
			FFTReorderSimple_367();
			FFTReorderSimple_368();
		WEIGHTED_ROUND_ROBIN_Joiner_352();
		WEIGHTED_ROUND_ROBIN_Splitter_369();
			CombineDFT_371();
			CombineDFT_372();
			CombineDFT_373();
			CombineDFT_374();
			CombineDFT_375();
			CombineDFT_376();
			CombineDFT_377();
			CombineDFT_378();
			CombineDFT_379();
			CombineDFT_380();
			CombineDFT_381();
			CombineDFT_382();
			CombineDFT_383();
			CombineDFT_384();
			CombineDFT_385();
			CombineDFT_386();
			CombineDFT_387();
			CombineDFT_388();
			CombineDFT_389();
			CombineDFT_390();
			CombineDFT_391();
			CombineDFT_392();
			CombineDFT_393();
			CombineDFT_394();
			CombineDFT_395();
			CombineDFT_396();
			CombineDFT_397();
			CombineDFT_398();
			CombineDFT_399();
			CombineDFT_400();
			CombineDFT_401();
		WEIGHTED_ROUND_ROBIN_Joiner_370();
		WEIGHTED_ROUND_ROBIN_Splitter_402();
			CombineDFT_404();
			CombineDFT_405();
			CombineDFT_406();
			CombineDFT_407();
			CombineDFT_408();
			CombineDFT_409();
			CombineDFT_410();
			CombineDFT_411();
			CombineDFT_412();
			CombineDFT_413();
			CombineDFT_414();
			CombineDFT_415();
			CombineDFT_416();
			CombineDFT_417();
			CombineDFT_418();
			CombineDFT_419();
		WEIGHTED_ROUND_ROBIN_Joiner_403();
		WEIGHTED_ROUND_ROBIN_Splitter_420();
			CombineDFT_422();
			CombineDFT_423();
			CombineDFT_424();
			CombineDFT_425();
			CombineDFT_426();
			CombineDFT_427();
			CombineDFT_428();
			CombineDFT_429();
		WEIGHTED_ROUND_ROBIN_Joiner_421();
		WEIGHTED_ROUND_ROBIN_Splitter_430();
			CombineDFT_432();
			CombineDFT_433();
			CombineDFT_434();
			CombineDFT_435();
		WEIGHTED_ROUND_ROBIN_Joiner_431();
		WEIGHTED_ROUND_ROBIN_Splitter_436();
			CombineDFT_438();
			CombineDFT_439();
		WEIGHTED_ROUND_ROBIN_Joiner_437();
		CombineDFT_328();
		CPrinter_329();
	ENDFOR
	return EXIT_SUCCESS;
}
