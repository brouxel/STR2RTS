#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=3584 on the compile command line
#else
#if BUF_SIZEMAX < 3584
#error BUF_SIZEMAX too small, it must be at least 3584
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float w[2];
} CombineDFT_2219_t;

typedef struct {
	float w[4];
} CombineDFT_2249_t;

typedef struct {
	float w[8];
} CombineDFT_2267_t;

typedef struct {
	float w[16];
} CombineDFT_2277_t;

typedef struct {
	float w[32];
} CombineDFT_2283_t;

typedef struct {
	float w[64];
} CombineDFT_2154_t;
void WEIGHTED_ROUND_ROBIN_Splitter_2175();
void FFTTestSource_2177();
void FFTTestSource_2178();
void WEIGHTED_ROUND_ROBIN_Joiner_2176();
void WEIGHTED_ROUND_ROBIN_Splitter_2167();
void FFTReorderSimple_2144();
void WEIGHTED_ROUND_ROBIN_Splitter_2179();
void FFTReorderSimple_2181();
void FFTReorderSimple_2182();
void WEIGHTED_ROUND_ROBIN_Joiner_2180();
void WEIGHTED_ROUND_ROBIN_Splitter_2183();
void FFTReorderSimple_2185();
void FFTReorderSimple_2186();
void FFTReorderSimple_2187();
void FFTReorderSimple_2188();
void WEIGHTED_ROUND_ROBIN_Joiner_2184();
void WEIGHTED_ROUND_ROBIN_Splitter_2189();
void FFTReorderSimple_2191();
void FFTReorderSimple_2192();
void FFTReorderSimple_2193();
void FFTReorderSimple_2194();
void FFTReorderSimple_2195();
void FFTReorderSimple_2196();
void FFTReorderSimple_2197();
void FFTReorderSimple_2198();
void WEIGHTED_ROUND_ROBIN_Joiner_2190();
void WEIGHTED_ROUND_ROBIN_Splitter_2199();
void FFTReorderSimple_2201();
void FFTReorderSimple_2202();
void FFTReorderSimple_2203();
void FFTReorderSimple_2204();
void FFTReorderSimple_2205();
void FFTReorderSimple_2206();
void FFTReorderSimple_2207();
void FFTReorderSimple_2208();
void FFTReorderSimple_2209();
void FFTReorderSimple_2210();
void FFTReorderSimple_2211();
void FFTReorderSimple_2212();
void FFTReorderSimple_2213();
void FFTReorderSimple_2214();
void FFTReorderSimple_2215();
void FFTReorderSimple_2216();
void WEIGHTED_ROUND_ROBIN_Joiner_2200();
void WEIGHTED_ROUND_ROBIN_Splitter_2217();
void CombineDFT_2219();
void CombineDFT_2220();
void CombineDFT_2221();
void CombineDFT_2222();
void CombineDFT_2223();
void CombineDFT_2224();
void CombineDFT_2225();
void CombineDFT_2226();
void CombineDFT_2227();
void CombineDFT_2228();
void CombineDFT_2229();
void CombineDFT_2230();
void CombineDFT_2231();
void CombineDFT_2232();
void CombineDFT_2233();
void CombineDFT_2234();
void CombineDFT_2235();
void CombineDFT_2236();
void CombineDFT_2237();
void CombineDFT_2238();
void CombineDFT_2239();
void CombineDFT_2240();
void CombineDFT_2241();
void CombineDFT_2242();
void CombineDFT_2243();
void CombineDFT_2244();
void CombineDFT_2245();
void CombineDFT_2246();
void WEIGHTED_ROUND_ROBIN_Joiner_2218();
void WEIGHTED_ROUND_ROBIN_Splitter_2247();
void CombineDFT_2249();
void CombineDFT_2250();
void CombineDFT_2251();
void CombineDFT_2252();
void CombineDFT_2253();
void CombineDFT_2254();
void CombineDFT_2255();
void CombineDFT_2256();
void CombineDFT_2257();
void CombineDFT_2258();
void CombineDFT_2259();
void CombineDFT_2260();
void CombineDFT_2261();
void CombineDFT_2262();
void CombineDFT_2263();
void CombineDFT_2264();
void WEIGHTED_ROUND_ROBIN_Joiner_2248();
void WEIGHTED_ROUND_ROBIN_Splitter_2265();
void CombineDFT_2267();
void CombineDFT_2268();
void CombineDFT_2269();
void CombineDFT_2270();
void CombineDFT_2271();
void CombineDFT_2272();
void CombineDFT_2273();
void CombineDFT_2274();
void WEIGHTED_ROUND_ROBIN_Joiner_2266();
void WEIGHTED_ROUND_ROBIN_Splitter_2275();
void CombineDFT_2277();
void CombineDFT_2278();
void CombineDFT_2279();
void CombineDFT_2280();
void WEIGHTED_ROUND_ROBIN_Joiner_2276();
void WEIGHTED_ROUND_ROBIN_Splitter_2281();
void CombineDFT_2283();
void CombineDFT_2284();
void WEIGHTED_ROUND_ROBIN_Joiner_2282();
void CombineDFT_2154();
void FFTReorderSimple_2155();
void WEIGHTED_ROUND_ROBIN_Splitter_2285();
void FFTReorderSimple_2287();
void FFTReorderSimple_2288();
void WEIGHTED_ROUND_ROBIN_Joiner_2286();
void WEIGHTED_ROUND_ROBIN_Splitter_2289();
void FFTReorderSimple_2291();
void FFTReorderSimple_2292();
void FFTReorderSimple_2293();
void FFTReorderSimple_2294();
void WEIGHTED_ROUND_ROBIN_Joiner_2290();
void WEIGHTED_ROUND_ROBIN_Splitter_2295();
void FFTReorderSimple_2297();
void FFTReorderSimple_2298();
void FFTReorderSimple_2299();
void FFTReorderSimple_2300();
void FFTReorderSimple_2301();
void FFTReorderSimple_2302();
void FFTReorderSimple_2303();
void FFTReorderSimple_2304();
void WEIGHTED_ROUND_ROBIN_Joiner_2296();
void WEIGHTED_ROUND_ROBIN_Splitter_2305();
void FFTReorderSimple_2307();
void FFTReorderSimple_2308();
void FFTReorderSimple_2309();
void FFTReorderSimple_2310();
void FFTReorderSimple_2311();
void FFTReorderSimple_2312();
void FFTReorderSimple_2313();
void FFTReorderSimple_2314();
void FFTReorderSimple_2315();
void FFTReorderSimple_2316();
void FFTReorderSimple_2317();
void FFTReorderSimple_2318();
void FFTReorderSimple_2319();
void FFTReorderSimple_2320();
void FFTReorderSimple_2321();
void FFTReorderSimple_2322();
void WEIGHTED_ROUND_ROBIN_Joiner_2306();
void WEIGHTED_ROUND_ROBIN_Splitter_2323();
void CombineDFT_2325();
void CombineDFT_2326();
void CombineDFT_2327();
void CombineDFT_2328();
void CombineDFT_2329();
void CombineDFT_2330();
void CombineDFT_2331();
void CombineDFT_2332();
void CombineDFT_2333();
void CombineDFT_2334();
void CombineDFT_2335();
void CombineDFT_2336();
void CombineDFT_2337();
void CombineDFT_2338();
void CombineDFT_2339();
void CombineDFT_2340();
void CombineDFT_2341();
void CombineDFT_2342();
void CombineDFT_2343();
void CombineDFT_2344();
void CombineDFT_2345();
void CombineDFT_2346();
void CombineDFT_2347();
void CombineDFT_2348();
void CombineDFT_2349();
void CombineDFT_2350();
void CombineDFT_2351();
void CombineDFT_2352();
void WEIGHTED_ROUND_ROBIN_Joiner_2324();
void WEIGHTED_ROUND_ROBIN_Splitter_2353();
void CombineDFT_2355();
void CombineDFT_2356();
void CombineDFT_2357();
void CombineDFT_2358();
void CombineDFT_2359();
void CombineDFT_2360();
void CombineDFT_2361();
void CombineDFT_2362();
void CombineDFT_2363();
void CombineDFT_2364();
void CombineDFT_2365();
void CombineDFT_2366();
void CombineDFT_2367();
void CombineDFT_2368();
void CombineDFT_2369();
void CombineDFT_2370();
void WEIGHTED_ROUND_ROBIN_Joiner_2354();
void WEIGHTED_ROUND_ROBIN_Splitter_2371();
void CombineDFT_2373();
void CombineDFT_2374();
void CombineDFT_2375();
void CombineDFT_2376();
void CombineDFT_2377();
void CombineDFT_2378();
void CombineDFT_2379();
void CombineDFT_2380();
void WEIGHTED_ROUND_ROBIN_Joiner_2372();
void WEIGHTED_ROUND_ROBIN_Splitter_2381();
void CombineDFT_2383();
void CombineDFT_2384();
void CombineDFT_2385();
void CombineDFT_2386();
void WEIGHTED_ROUND_ROBIN_Joiner_2382();
void WEIGHTED_ROUND_ROBIN_Splitter_2387();
void CombineDFT_2389();
void CombineDFT_2390();
void WEIGHTED_ROUND_ROBIN_Joiner_2388();
void CombineDFT_2165();
void WEIGHTED_ROUND_ROBIN_Joiner_2168();
void FloatPrinter_2166();

#ifdef __cplusplus
}
#endif
#endif
