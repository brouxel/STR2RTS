#include "PEG18-FFT2.h"

buffer_float_t SplitJoin96_FFTReorderSimple_Fiss_7034_7055_split[8];
buffer_float_t SplitJoin94_FFTReorderSimple_Fiss_7033_7054_join[4];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_6783_6819_7022_7043_split[2];
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_7023_7044_join[2];
buffer_float_t SplitJoin94_FFTReorderSimple_Fiss_7033_7054_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6926WEIGHTED_ROUND_ROBIN_Splitter_6929;
buffer_float_t SplitJoin92_FFTReorderSimple_Fiss_7032_7053_split[2];
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_7026_7047_join[16];
buffer_float_t SplitJoin102_CombineDFT_Fiss_7037_7058_split[16];
buffer_float_t SplitJoin108_CombineDFT_Fiss_7040_7061_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6840WEIGHTED_ROUND_ROBIN_Splitter_6849;
buffer_float_t SplitJoin0_FFTTestSource_Fiss_7021_7042_join[2];
buffer_float_t SplitJoin20_CombineDFT_Fiss_7031_7052_split[2];
buffer_float_t FFTReorderSimple_6794WEIGHTED_ROUND_ROBIN_Splitter_6829;
buffer_float_t SplitJoin12_CombineDFT_Fiss_7027_7048_split[18];
buffer_float_t SplitJoin104_CombineDFT_Fiss_7038_7059_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6964WEIGHTED_ROUND_ROBIN_Splitter_6983;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6868WEIGHTED_ROUND_ROBIN_Splitter_6887;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6906WEIGHTED_ROUND_ROBIN_Splitter_6915;
buffer_float_t SplitJoin92_FFTReorderSimple_Fiss_7032_7053_join[2];
buffer_float_t SplitJoin16_CombineDFT_Fiss_7029_7050_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7018CombineDFT_6815;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6888WEIGHTED_ROUND_ROBIN_Splitter_6905;
buffer_float_t FFTReorderSimple_6805WEIGHTED_ROUND_ROBIN_Splitter_6925;
buffer_float_t SplitJoin104_CombineDFT_Fiss_7038_7059_split[8];
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_7024_7045_join[4];
buffer_float_t SplitJoin18_CombineDFT_Fiss_7030_7051_join[4];
buffer_float_t SplitJoin98_FFTReorderSimple_Fiss_7035_7056_join[16];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_6783_6819_7022_7043_join[2];
buffer_float_t SplitJoin98_FFTReorderSimple_Fiss_7035_7056_split[16];
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_7026_7047_split[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6826WEIGHTED_ROUND_ROBIN_Splitter_6817;
buffer_float_t SplitJoin106_CombineDFT_Fiss_7039_7060_join[4];
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_7025_7046_split[8];
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_7023_7044_split[2];
buffer_float_t SplitJoin106_CombineDFT_Fiss_7039_7060_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6930WEIGHTED_ROUND_ROBIN_Splitter_6935;
buffer_float_t SplitJoin108_CombineDFT_Fiss_7040_7061_split[2];
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_7024_7045_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6834WEIGHTED_ROUND_ROBIN_Splitter_6839;
buffer_float_t SplitJoin18_CombineDFT_Fiss_7030_7051_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6946WEIGHTED_ROUND_ROBIN_Splitter_6963;
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_7025_7046_join[8];
buffer_float_t SplitJoin96_FFTReorderSimple_Fiss_7034_7055_join[8];
buffer_float_t SplitJoin0_FFTTestSource_Fiss_7021_7042_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6818FloatPrinter_6816;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7012WEIGHTED_ROUND_ROBIN_Splitter_7017;
buffer_float_t SplitJoin14_CombineDFT_Fiss_7028_7049_join[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6830WEIGHTED_ROUND_ROBIN_Splitter_6833;
buffer_float_t SplitJoin102_CombineDFT_Fiss_7037_7058_join[16];
buffer_float_t SplitJoin20_CombineDFT_Fiss_7031_7052_join[2];
buffer_float_t SplitJoin12_CombineDFT_Fiss_7027_7048_join[18];
buffer_float_t SplitJoin16_CombineDFT_Fiss_7029_7050_join[8];
buffer_float_t SplitJoin14_CombineDFT_Fiss_7028_7049_split[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6936WEIGHTED_ROUND_ROBIN_Splitter_6945;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6916WEIGHTED_ROUND_ROBIN_Splitter_6921;
buffer_float_t SplitJoin100_CombineDFT_Fiss_7036_7057_split[18];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6850WEIGHTED_ROUND_ROBIN_Splitter_6867;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6984WEIGHTED_ROUND_ROBIN_Splitter_7001;
buffer_float_t SplitJoin100_CombineDFT_Fiss_7036_7057_join[18];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6922CombineDFT_6804;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7002WEIGHTED_ROUND_ROBIN_Splitter_7011;


CombineDFT_6869_t CombineDFT_6869_s;
CombineDFT_6869_t CombineDFT_6870_s;
CombineDFT_6869_t CombineDFT_6871_s;
CombineDFT_6869_t CombineDFT_6872_s;
CombineDFT_6869_t CombineDFT_6873_s;
CombineDFT_6869_t CombineDFT_6874_s;
CombineDFT_6869_t CombineDFT_6875_s;
CombineDFT_6869_t CombineDFT_6876_s;
CombineDFT_6869_t CombineDFT_6877_s;
CombineDFT_6869_t CombineDFT_6878_s;
CombineDFT_6869_t CombineDFT_6879_s;
CombineDFT_6869_t CombineDFT_6880_s;
CombineDFT_6869_t CombineDFT_6881_s;
CombineDFT_6869_t CombineDFT_6882_s;
CombineDFT_6869_t CombineDFT_6883_s;
CombineDFT_6869_t CombineDFT_6884_s;
CombineDFT_6869_t CombineDFT_6885_s;
CombineDFT_6869_t CombineDFT_6886_s;
CombineDFT_6889_t CombineDFT_6889_s;
CombineDFT_6889_t CombineDFT_6890_s;
CombineDFT_6889_t CombineDFT_6891_s;
CombineDFT_6889_t CombineDFT_6892_s;
CombineDFT_6889_t CombineDFT_6893_s;
CombineDFT_6889_t CombineDFT_6894_s;
CombineDFT_6889_t CombineDFT_6895_s;
CombineDFT_6889_t CombineDFT_6896_s;
CombineDFT_6889_t CombineDFT_6897_s;
CombineDFT_6889_t CombineDFT_6898_s;
CombineDFT_6889_t CombineDFT_6899_s;
CombineDFT_6889_t CombineDFT_6900_s;
CombineDFT_6889_t CombineDFT_6901_s;
CombineDFT_6889_t CombineDFT_6902_s;
CombineDFT_6889_t CombineDFT_6903_s;
CombineDFT_6889_t CombineDFT_6904_s;
CombineDFT_6907_t CombineDFT_6907_s;
CombineDFT_6907_t CombineDFT_6908_s;
CombineDFT_6907_t CombineDFT_6909_s;
CombineDFT_6907_t CombineDFT_6910_s;
CombineDFT_6907_t CombineDFT_6911_s;
CombineDFT_6907_t CombineDFT_6912_s;
CombineDFT_6907_t CombineDFT_6913_s;
CombineDFT_6907_t CombineDFT_6914_s;
CombineDFT_6917_t CombineDFT_6917_s;
CombineDFT_6917_t CombineDFT_6918_s;
CombineDFT_6917_t CombineDFT_6919_s;
CombineDFT_6917_t CombineDFT_6920_s;
CombineDFT_6923_t CombineDFT_6923_s;
CombineDFT_6923_t CombineDFT_6924_s;
CombineDFT_6804_t CombineDFT_6804_s;
CombineDFT_6869_t CombineDFT_6965_s;
CombineDFT_6869_t CombineDFT_6966_s;
CombineDFT_6869_t CombineDFT_6967_s;
CombineDFT_6869_t CombineDFT_6968_s;
CombineDFT_6869_t CombineDFT_6969_s;
CombineDFT_6869_t CombineDFT_6970_s;
CombineDFT_6869_t CombineDFT_6971_s;
CombineDFT_6869_t CombineDFT_6972_s;
CombineDFT_6869_t CombineDFT_6973_s;
CombineDFT_6869_t CombineDFT_6974_s;
CombineDFT_6869_t CombineDFT_6975_s;
CombineDFT_6869_t CombineDFT_6976_s;
CombineDFT_6869_t CombineDFT_6977_s;
CombineDFT_6869_t CombineDFT_6978_s;
CombineDFT_6869_t CombineDFT_6979_s;
CombineDFT_6869_t CombineDFT_6980_s;
CombineDFT_6869_t CombineDFT_6981_s;
CombineDFT_6869_t CombineDFT_6982_s;
CombineDFT_6889_t CombineDFT_6985_s;
CombineDFT_6889_t CombineDFT_6986_s;
CombineDFT_6889_t CombineDFT_6987_s;
CombineDFT_6889_t CombineDFT_6988_s;
CombineDFT_6889_t CombineDFT_6989_s;
CombineDFT_6889_t CombineDFT_6990_s;
CombineDFT_6889_t CombineDFT_6991_s;
CombineDFT_6889_t CombineDFT_6992_s;
CombineDFT_6889_t CombineDFT_6993_s;
CombineDFT_6889_t CombineDFT_6994_s;
CombineDFT_6889_t CombineDFT_6995_s;
CombineDFT_6889_t CombineDFT_6996_s;
CombineDFT_6889_t CombineDFT_6997_s;
CombineDFT_6889_t CombineDFT_6998_s;
CombineDFT_6889_t CombineDFT_6999_s;
CombineDFT_6889_t CombineDFT_7000_s;
CombineDFT_6907_t CombineDFT_7003_s;
CombineDFT_6907_t CombineDFT_7004_s;
CombineDFT_6907_t CombineDFT_7005_s;
CombineDFT_6907_t CombineDFT_7006_s;
CombineDFT_6907_t CombineDFT_7007_s;
CombineDFT_6907_t CombineDFT_7008_s;
CombineDFT_6907_t CombineDFT_7009_s;
CombineDFT_6907_t CombineDFT_7010_s;
CombineDFT_6917_t CombineDFT_7013_s;
CombineDFT_6917_t CombineDFT_7014_s;
CombineDFT_6917_t CombineDFT_7015_s;
CombineDFT_6917_t CombineDFT_7016_s;
CombineDFT_6923_t CombineDFT_7019_s;
CombineDFT_6923_t CombineDFT_7020_s;
CombineDFT_6804_t CombineDFT_6815_s;

void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), 0.0) ; 
		push_float(&(*chanout), 0.0) ; 
		push_float(&(*chanout), 1.0) ; 
		push_float(&(*chanout), 0.0) ; 
		FOR(int, i, 0,  < , 124, i++) {
			push_float(&(*chanout), 0.0) ; 
		}
		ENDFOR
	}


void FFTTestSource_6827() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_7021_7042_split[0]), &(SplitJoin0_FFTTestSource_Fiss_7021_7042_join[0]));
	ENDFOR
}

void FFTTestSource_6828() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_7021_7042_split[1]), &(SplitJoin0_FFTTestSource_Fiss_7021_7042_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6825() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_6826() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6826WEIGHTED_ROUND_ROBIN_Splitter_6817, pop_float(&SplitJoin0_FFTTestSource_Fiss_7021_7042_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6826WEIGHTED_ROUND_ROBIN_Splitter_6817, pop_float(&SplitJoin0_FFTTestSource_Fiss_7021_7042_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, i, 0,  < , 128, i = (i + 4)) {
			push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 128, i = (i + 4)) {
			push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_float(&(*chanin)) ; 
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void FFTReorderSimple_6794() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_6783_6819_7022_7043_split[0]), &(FFTReorderSimple_6794WEIGHTED_ROUND_ROBIN_Splitter_6829));
	ENDFOR
}

void FFTReorderSimple_6831() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_7023_7044_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_7023_7044_join[0]));
	ENDFOR
}

void FFTReorderSimple_6832() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_7023_7044_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_7023_7044_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6829() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_7023_7044_split[0], pop_float(&FFTReorderSimple_6794WEIGHTED_ROUND_ROBIN_Splitter_6829));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_7023_7044_split[1], pop_float(&FFTReorderSimple_6794WEIGHTED_ROUND_ROBIN_Splitter_6829));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6830() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6830WEIGHTED_ROUND_ROBIN_Splitter_6833, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_7023_7044_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6830WEIGHTED_ROUND_ROBIN_Splitter_6833, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_7023_7044_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_6835() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_7024_7045_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_7024_7045_join[0]));
	ENDFOR
}

void FFTReorderSimple_6836() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_7024_7045_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_7024_7045_join[1]));
	ENDFOR
}

void FFTReorderSimple_6837() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_7024_7045_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_7024_7045_join[2]));
	ENDFOR
}

void FFTReorderSimple_6838() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_7024_7045_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_7024_7045_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6833() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin6_FFTReorderSimple_Fiss_7024_7045_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6830WEIGHTED_ROUND_ROBIN_Splitter_6833));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6834() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6834WEIGHTED_ROUND_ROBIN_Splitter_6839, pop_float(&SplitJoin6_FFTReorderSimple_Fiss_7024_7045_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_6841() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_7025_7046_split[0]), &(SplitJoin8_FFTReorderSimple_Fiss_7025_7046_join[0]));
	ENDFOR
}

void FFTReorderSimple_6842() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_7025_7046_split[1]), &(SplitJoin8_FFTReorderSimple_Fiss_7025_7046_join[1]));
	ENDFOR
}

void FFTReorderSimple_6843() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_7025_7046_split[2]), &(SplitJoin8_FFTReorderSimple_Fiss_7025_7046_join[2]));
	ENDFOR
}

void FFTReorderSimple_6844() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_7025_7046_split[3]), &(SplitJoin8_FFTReorderSimple_Fiss_7025_7046_join[3]));
	ENDFOR
}

void FFTReorderSimple_6845() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_7025_7046_split[4]), &(SplitJoin8_FFTReorderSimple_Fiss_7025_7046_join[4]));
	ENDFOR
}

void FFTReorderSimple_6846() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_7025_7046_split[5]), &(SplitJoin8_FFTReorderSimple_Fiss_7025_7046_join[5]));
	ENDFOR
}

void FFTReorderSimple_6847() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_7025_7046_split[6]), &(SplitJoin8_FFTReorderSimple_Fiss_7025_7046_join[6]));
	ENDFOR
}

void FFTReorderSimple_6848() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_7025_7046_split[7]), &(SplitJoin8_FFTReorderSimple_Fiss_7025_7046_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6839() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin8_FFTReorderSimple_Fiss_7025_7046_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6834WEIGHTED_ROUND_ROBIN_Splitter_6839));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6840() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6840WEIGHTED_ROUND_ROBIN_Splitter_6849, pop_float(&SplitJoin8_FFTReorderSimple_Fiss_7025_7046_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_6851() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_split[0]), &(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_join[0]));
	ENDFOR
}

void FFTReorderSimple_6852() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_split[1]), &(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_join[1]));
	ENDFOR
}

void FFTReorderSimple_6853() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_split[2]), &(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_join[2]));
	ENDFOR
}

void FFTReorderSimple_6854() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_split[3]), &(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_join[3]));
	ENDFOR
}

void FFTReorderSimple_6855() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_split[4]), &(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_join[4]));
	ENDFOR
}

void FFTReorderSimple_6856() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_split[5]), &(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_join[5]));
	ENDFOR
}

void FFTReorderSimple_6857() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_split[6]), &(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_join[6]));
	ENDFOR
}

void FFTReorderSimple_6858() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_split[7]), &(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_join[7]));
	ENDFOR
}

void FFTReorderSimple_6859() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_split[8]), &(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_join[8]));
	ENDFOR
}

void FFTReorderSimple_6860() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_split[9]), &(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_join[9]));
	ENDFOR
}

void FFTReorderSimple_6861() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_split[10]), &(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_join[10]));
	ENDFOR
}

void FFTReorderSimple_6862() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_split[11]), &(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_join[11]));
	ENDFOR
}

void FFTReorderSimple_6863() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_split[12]), &(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_join[12]));
	ENDFOR
}

void FFTReorderSimple_6864() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_split[13]), &(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_join[13]));
	ENDFOR
}

void FFTReorderSimple_6865() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_split[14]), &(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_join[14]));
	ENDFOR
}

void FFTReorderSimple_6866() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_split[15]), &(SplitJoin10_FFTReorderSimple_Fiss_7026_7047_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6849() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_FFTReorderSimple_Fiss_7026_7047_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6840WEIGHTED_ROUND_ROBIN_Splitter_6849));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6850() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6850WEIGHTED_ROUND_ROBIN_Splitter_6867, pop_float(&SplitJoin10_FFTReorderSimple_Fiss_7026_7047_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&(*chanin), i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&(*chanin), i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&(*chanin), (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&(*chanin), (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_6869_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_6869_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&(*chanin)) ; 
			push_float(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_6869() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7027_7048_split[0]), &(SplitJoin12_CombineDFT_Fiss_7027_7048_join[0]));
	ENDFOR
}

void CombineDFT_6870() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7027_7048_split[1]), &(SplitJoin12_CombineDFT_Fiss_7027_7048_join[1]));
	ENDFOR
}

void CombineDFT_6871() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7027_7048_split[2]), &(SplitJoin12_CombineDFT_Fiss_7027_7048_join[2]));
	ENDFOR
}

void CombineDFT_6872() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7027_7048_split[3]), &(SplitJoin12_CombineDFT_Fiss_7027_7048_join[3]));
	ENDFOR
}

void CombineDFT_6873() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7027_7048_split[4]), &(SplitJoin12_CombineDFT_Fiss_7027_7048_join[4]));
	ENDFOR
}

void CombineDFT_6874() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7027_7048_split[5]), &(SplitJoin12_CombineDFT_Fiss_7027_7048_join[5]));
	ENDFOR
}

void CombineDFT_6875() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7027_7048_split[6]), &(SplitJoin12_CombineDFT_Fiss_7027_7048_join[6]));
	ENDFOR
}

void CombineDFT_6876() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7027_7048_split[7]), &(SplitJoin12_CombineDFT_Fiss_7027_7048_join[7]));
	ENDFOR
}

void CombineDFT_6877() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7027_7048_split[8]), &(SplitJoin12_CombineDFT_Fiss_7027_7048_join[8]));
	ENDFOR
}

void CombineDFT_6878() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7027_7048_split[9]), &(SplitJoin12_CombineDFT_Fiss_7027_7048_join[9]));
	ENDFOR
}

void CombineDFT_6879() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7027_7048_split[10]), &(SplitJoin12_CombineDFT_Fiss_7027_7048_join[10]));
	ENDFOR
}

void CombineDFT_6880() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7027_7048_split[11]), &(SplitJoin12_CombineDFT_Fiss_7027_7048_join[11]));
	ENDFOR
}

void CombineDFT_6881() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7027_7048_split[12]), &(SplitJoin12_CombineDFT_Fiss_7027_7048_join[12]));
	ENDFOR
}

void CombineDFT_6882() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7027_7048_split[13]), &(SplitJoin12_CombineDFT_Fiss_7027_7048_join[13]));
	ENDFOR
}

void CombineDFT_6883() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7027_7048_split[14]), &(SplitJoin12_CombineDFT_Fiss_7027_7048_join[14]));
	ENDFOR
}

void CombineDFT_6884() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7027_7048_split[15]), &(SplitJoin12_CombineDFT_Fiss_7027_7048_join[15]));
	ENDFOR
}

void CombineDFT_6885() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7027_7048_split[16]), &(SplitJoin12_CombineDFT_Fiss_7027_7048_join[16]));
	ENDFOR
}

void CombineDFT_6886() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7027_7048_split[17]), &(SplitJoin12_CombineDFT_Fiss_7027_7048_join[17]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6867() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 18, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin12_CombineDFT_Fiss_7027_7048_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6850WEIGHTED_ROUND_ROBIN_Splitter_6867));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6868() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 18, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6868WEIGHTED_ROUND_ROBIN_Splitter_6887, pop_float(&SplitJoin12_CombineDFT_Fiss_7027_7048_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_6889() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7028_7049_split[0]), &(SplitJoin14_CombineDFT_Fiss_7028_7049_join[0]));
	ENDFOR
}

void CombineDFT_6890() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7028_7049_split[1]), &(SplitJoin14_CombineDFT_Fiss_7028_7049_join[1]));
	ENDFOR
}

void CombineDFT_6891() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7028_7049_split[2]), &(SplitJoin14_CombineDFT_Fiss_7028_7049_join[2]));
	ENDFOR
}

void CombineDFT_6892() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7028_7049_split[3]), &(SplitJoin14_CombineDFT_Fiss_7028_7049_join[3]));
	ENDFOR
}

void CombineDFT_6893() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7028_7049_split[4]), &(SplitJoin14_CombineDFT_Fiss_7028_7049_join[4]));
	ENDFOR
}

void CombineDFT_6894() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7028_7049_split[5]), &(SplitJoin14_CombineDFT_Fiss_7028_7049_join[5]));
	ENDFOR
}

void CombineDFT_6895() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7028_7049_split[6]), &(SplitJoin14_CombineDFT_Fiss_7028_7049_join[6]));
	ENDFOR
}

void CombineDFT_6896() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7028_7049_split[7]), &(SplitJoin14_CombineDFT_Fiss_7028_7049_join[7]));
	ENDFOR
}

void CombineDFT_6897() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7028_7049_split[8]), &(SplitJoin14_CombineDFT_Fiss_7028_7049_join[8]));
	ENDFOR
}

void CombineDFT_6898() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7028_7049_split[9]), &(SplitJoin14_CombineDFT_Fiss_7028_7049_join[9]));
	ENDFOR
}

void CombineDFT_6899() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7028_7049_split[10]), &(SplitJoin14_CombineDFT_Fiss_7028_7049_join[10]));
	ENDFOR
}

void CombineDFT_6900() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7028_7049_split[11]), &(SplitJoin14_CombineDFT_Fiss_7028_7049_join[11]));
	ENDFOR
}

void CombineDFT_6901() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7028_7049_split[12]), &(SplitJoin14_CombineDFT_Fiss_7028_7049_join[12]));
	ENDFOR
}

void CombineDFT_6902() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7028_7049_split[13]), &(SplitJoin14_CombineDFT_Fiss_7028_7049_join[13]));
	ENDFOR
}

void CombineDFT_6903() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7028_7049_split[14]), &(SplitJoin14_CombineDFT_Fiss_7028_7049_join[14]));
	ENDFOR
}

void CombineDFT_6904() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7028_7049_split[15]), &(SplitJoin14_CombineDFT_Fiss_7028_7049_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6887() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin14_CombineDFT_Fiss_7028_7049_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6868WEIGHTED_ROUND_ROBIN_Splitter_6887));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6888() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6888WEIGHTED_ROUND_ROBIN_Splitter_6905, pop_float(&SplitJoin14_CombineDFT_Fiss_7028_7049_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_6907() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_7029_7050_split[0]), &(SplitJoin16_CombineDFT_Fiss_7029_7050_join[0]));
	ENDFOR
}

void CombineDFT_6908() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_7029_7050_split[1]), &(SplitJoin16_CombineDFT_Fiss_7029_7050_join[1]));
	ENDFOR
}

void CombineDFT_6909() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_7029_7050_split[2]), &(SplitJoin16_CombineDFT_Fiss_7029_7050_join[2]));
	ENDFOR
}

void CombineDFT_6910() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_7029_7050_split[3]), &(SplitJoin16_CombineDFT_Fiss_7029_7050_join[3]));
	ENDFOR
}

void CombineDFT_6911() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_7029_7050_split[4]), &(SplitJoin16_CombineDFT_Fiss_7029_7050_join[4]));
	ENDFOR
}

void CombineDFT_6912() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_7029_7050_split[5]), &(SplitJoin16_CombineDFT_Fiss_7029_7050_join[5]));
	ENDFOR
}

void CombineDFT_6913() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_7029_7050_split[6]), &(SplitJoin16_CombineDFT_Fiss_7029_7050_join[6]));
	ENDFOR
}

void CombineDFT_6914() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_7029_7050_split[7]), &(SplitJoin16_CombineDFT_Fiss_7029_7050_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6905() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin16_CombineDFT_Fiss_7029_7050_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6888WEIGHTED_ROUND_ROBIN_Splitter_6905));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6906() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6906WEIGHTED_ROUND_ROBIN_Splitter_6915, pop_float(&SplitJoin16_CombineDFT_Fiss_7029_7050_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_6917() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_7030_7051_split[0]), &(SplitJoin18_CombineDFT_Fiss_7030_7051_join[0]));
	ENDFOR
}

void CombineDFT_6918() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_7030_7051_split[1]), &(SplitJoin18_CombineDFT_Fiss_7030_7051_join[1]));
	ENDFOR
}

void CombineDFT_6919() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_7030_7051_split[2]), &(SplitJoin18_CombineDFT_Fiss_7030_7051_join[2]));
	ENDFOR
}

void CombineDFT_6920() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_7030_7051_split[3]), &(SplitJoin18_CombineDFT_Fiss_7030_7051_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6915() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin18_CombineDFT_Fiss_7030_7051_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6906WEIGHTED_ROUND_ROBIN_Splitter_6915));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6916() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6916WEIGHTED_ROUND_ROBIN_Splitter_6921, pop_float(&SplitJoin18_CombineDFT_Fiss_7030_7051_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_6923() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin20_CombineDFT_Fiss_7031_7052_split[0]), &(SplitJoin20_CombineDFT_Fiss_7031_7052_join[0]));
	ENDFOR
}

void CombineDFT_6924() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin20_CombineDFT_Fiss_7031_7052_split[1]), &(SplitJoin20_CombineDFT_Fiss_7031_7052_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6921() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_7031_7052_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6916WEIGHTED_ROUND_ROBIN_Splitter_6921));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_7031_7052_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6916WEIGHTED_ROUND_ROBIN_Splitter_6921));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6922() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6922CombineDFT_6804, pop_float(&SplitJoin20_CombineDFT_Fiss_7031_7052_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6922CombineDFT_6804, pop_float(&SplitJoin20_CombineDFT_Fiss_7031_7052_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_6804() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_6922CombineDFT_6804), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_6783_6819_7022_7043_join[0]));
	ENDFOR
}

void FFTReorderSimple_6805() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_6783_6819_7022_7043_split[1]), &(FFTReorderSimple_6805WEIGHTED_ROUND_ROBIN_Splitter_6925));
	ENDFOR
}

void FFTReorderSimple_6927() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin92_FFTReorderSimple_Fiss_7032_7053_split[0]), &(SplitJoin92_FFTReorderSimple_Fiss_7032_7053_join[0]));
	ENDFOR
}

void FFTReorderSimple_6928() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin92_FFTReorderSimple_Fiss_7032_7053_split[1]), &(SplitJoin92_FFTReorderSimple_Fiss_7032_7053_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6925() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin92_FFTReorderSimple_Fiss_7032_7053_split[0], pop_float(&FFTReorderSimple_6805WEIGHTED_ROUND_ROBIN_Splitter_6925));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin92_FFTReorderSimple_Fiss_7032_7053_split[1], pop_float(&FFTReorderSimple_6805WEIGHTED_ROUND_ROBIN_Splitter_6925));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6926() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6926WEIGHTED_ROUND_ROBIN_Splitter_6929, pop_float(&SplitJoin92_FFTReorderSimple_Fiss_7032_7053_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6926WEIGHTED_ROUND_ROBIN_Splitter_6929, pop_float(&SplitJoin92_FFTReorderSimple_Fiss_7032_7053_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_6931() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin94_FFTReorderSimple_Fiss_7033_7054_split[0]), &(SplitJoin94_FFTReorderSimple_Fiss_7033_7054_join[0]));
	ENDFOR
}

void FFTReorderSimple_6932() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin94_FFTReorderSimple_Fiss_7033_7054_split[1]), &(SplitJoin94_FFTReorderSimple_Fiss_7033_7054_join[1]));
	ENDFOR
}

void FFTReorderSimple_6933() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin94_FFTReorderSimple_Fiss_7033_7054_split[2]), &(SplitJoin94_FFTReorderSimple_Fiss_7033_7054_join[2]));
	ENDFOR
}

void FFTReorderSimple_6934() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin94_FFTReorderSimple_Fiss_7033_7054_split[3]), &(SplitJoin94_FFTReorderSimple_Fiss_7033_7054_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6929() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin94_FFTReorderSimple_Fiss_7033_7054_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6926WEIGHTED_ROUND_ROBIN_Splitter_6929));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6930() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6930WEIGHTED_ROUND_ROBIN_Splitter_6935, pop_float(&SplitJoin94_FFTReorderSimple_Fiss_7033_7054_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_6937() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin96_FFTReorderSimple_Fiss_7034_7055_split[0]), &(SplitJoin96_FFTReorderSimple_Fiss_7034_7055_join[0]));
	ENDFOR
}

void FFTReorderSimple_6938() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin96_FFTReorderSimple_Fiss_7034_7055_split[1]), &(SplitJoin96_FFTReorderSimple_Fiss_7034_7055_join[1]));
	ENDFOR
}

void FFTReorderSimple_6939() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin96_FFTReorderSimple_Fiss_7034_7055_split[2]), &(SplitJoin96_FFTReorderSimple_Fiss_7034_7055_join[2]));
	ENDFOR
}

void FFTReorderSimple_6940() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin96_FFTReorderSimple_Fiss_7034_7055_split[3]), &(SplitJoin96_FFTReorderSimple_Fiss_7034_7055_join[3]));
	ENDFOR
}

void FFTReorderSimple_6941() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin96_FFTReorderSimple_Fiss_7034_7055_split[4]), &(SplitJoin96_FFTReorderSimple_Fiss_7034_7055_join[4]));
	ENDFOR
}

void FFTReorderSimple_6942() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin96_FFTReorderSimple_Fiss_7034_7055_split[5]), &(SplitJoin96_FFTReorderSimple_Fiss_7034_7055_join[5]));
	ENDFOR
}

void FFTReorderSimple_6943() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin96_FFTReorderSimple_Fiss_7034_7055_split[6]), &(SplitJoin96_FFTReorderSimple_Fiss_7034_7055_join[6]));
	ENDFOR
}

void FFTReorderSimple_6944() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin96_FFTReorderSimple_Fiss_7034_7055_split[7]), &(SplitJoin96_FFTReorderSimple_Fiss_7034_7055_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6935() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin96_FFTReorderSimple_Fiss_7034_7055_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6930WEIGHTED_ROUND_ROBIN_Splitter_6935));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6936() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6936WEIGHTED_ROUND_ROBIN_Splitter_6945, pop_float(&SplitJoin96_FFTReorderSimple_Fiss_7034_7055_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_6947() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_split[0]), &(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_join[0]));
	ENDFOR
}

void FFTReorderSimple_6948() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_split[1]), &(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_join[1]));
	ENDFOR
}

void FFTReorderSimple_6949() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_split[2]), &(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_join[2]));
	ENDFOR
}

void FFTReorderSimple_6950() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_split[3]), &(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_join[3]));
	ENDFOR
}

void FFTReorderSimple_6951() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_split[4]), &(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_join[4]));
	ENDFOR
}

void FFTReorderSimple_6952() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_split[5]), &(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_join[5]));
	ENDFOR
}

void FFTReorderSimple_6953() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_split[6]), &(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_join[6]));
	ENDFOR
}

void FFTReorderSimple_6954() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_split[7]), &(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_join[7]));
	ENDFOR
}

void FFTReorderSimple_6955() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_split[8]), &(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_join[8]));
	ENDFOR
}

void FFTReorderSimple_6956() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_split[9]), &(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_join[9]));
	ENDFOR
}

void FFTReorderSimple_6957() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_split[10]), &(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_join[10]));
	ENDFOR
}

void FFTReorderSimple_6958() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_split[11]), &(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_join[11]));
	ENDFOR
}

void FFTReorderSimple_6959() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_split[12]), &(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_join[12]));
	ENDFOR
}

void FFTReorderSimple_6960() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_split[13]), &(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_join[13]));
	ENDFOR
}

void FFTReorderSimple_6961() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_split[14]), &(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_join[14]));
	ENDFOR
}

void FFTReorderSimple_6962() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_split[15]), &(SplitJoin98_FFTReorderSimple_Fiss_7035_7056_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6945() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin98_FFTReorderSimple_Fiss_7035_7056_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6936WEIGHTED_ROUND_ROBIN_Splitter_6945));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6946() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6946WEIGHTED_ROUND_ROBIN_Splitter_6963, pop_float(&SplitJoin98_FFTReorderSimple_Fiss_7035_7056_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_6965() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7036_7057_split[0]), &(SplitJoin100_CombineDFT_Fiss_7036_7057_join[0]));
	ENDFOR
}

void CombineDFT_6966() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7036_7057_split[1]), &(SplitJoin100_CombineDFT_Fiss_7036_7057_join[1]));
	ENDFOR
}

void CombineDFT_6967() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7036_7057_split[2]), &(SplitJoin100_CombineDFT_Fiss_7036_7057_join[2]));
	ENDFOR
}

void CombineDFT_6968() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7036_7057_split[3]), &(SplitJoin100_CombineDFT_Fiss_7036_7057_join[3]));
	ENDFOR
}

void CombineDFT_6969() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7036_7057_split[4]), &(SplitJoin100_CombineDFT_Fiss_7036_7057_join[4]));
	ENDFOR
}

void CombineDFT_6970() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7036_7057_split[5]), &(SplitJoin100_CombineDFT_Fiss_7036_7057_join[5]));
	ENDFOR
}

void CombineDFT_6971() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7036_7057_split[6]), &(SplitJoin100_CombineDFT_Fiss_7036_7057_join[6]));
	ENDFOR
}

void CombineDFT_6972() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7036_7057_split[7]), &(SplitJoin100_CombineDFT_Fiss_7036_7057_join[7]));
	ENDFOR
}

void CombineDFT_6973() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7036_7057_split[8]), &(SplitJoin100_CombineDFT_Fiss_7036_7057_join[8]));
	ENDFOR
}

void CombineDFT_6974() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7036_7057_split[9]), &(SplitJoin100_CombineDFT_Fiss_7036_7057_join[9]));
	ENDFOR
}

void CombineDFT_6975() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7036_7057_split[10]), &(SplitJoin100_CombineDFT_Fiss_7036_7057_join[10]));
	ENDFOR
}

void CombineDFT_6976() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7036_7057_split[11]), &(SplitJoin100_CombineDFT_Fiss_7036_7057_join[11]));
	ENDFOR
}

void CombineDFT_6977() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7036_7057_split[12]), &(SplitJoin100_CombineDFT_Fiss_7036_7057_join[12]));
	ENDFOR
}

void CombineDFT_6978() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7036_7057_split[13]), &(SplitJoin100_CombineDFT_Fiss_7036_7057_join[13]));
	ENDFOR
}

void CombineDFT_6979() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7036_7057_split[14]), &(SplitJoin100_CombineDFT_Fiss_7036_7057_join[14]));
	ENDFOR
}

void CombineDFT_6980() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7036_7057_split[15]), &(SplitJoin100_CombineDFT_Fiss_7036_7057_join[15]));
	ENDFOR
}

void CombineDFT_6981() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7036_7057_split[16]), &(SplitJoin100_CombineDFT_Fiss_7036_7057_join[16]));
	ENDFOR
}

void CombineDFT_6982() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7036_7057_split[17]), &(SplitJoin100_CombineDFT_Fiss_7036_7057_join[17]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6963() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 18, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin100_CombineDFT_Fiss_7036_7057_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6946WEIGHTED_ROUND_ROBIN_Splitter_6963));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6964() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 18, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6964WEIGHTED_ROUND_ROBIN_Splitter_6983, pop_float(&SplitJoin100_CombineDFT_Fiss_7036_7057_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_6985() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_7037_7058_split[0]), &(SplitJoin102_CombineDFT_Fiss_7037_7058_join[0]));
	ENDFOR
}

void CombineDFT_6986() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_7037_7058_split[1]), &(SplitJoin102_CombineDFT_Fiss_7037_7058_join[1]));
	ENDFOR
}

void CombineDFT_6987() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_7037_7058_split[2]), &(SplitJoin102_CombineDFT_Fiss_7037_7058_join[2]));
	ENDFOR
}

void CombineDFT_6988() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_7037_7058_split[3]), &(SplitJoin102_CombineDFT_Fiss_7037_7058_join[3]));
	ENDFOR
}

void CombineDFT_6989() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_7037_7058_split[4]), &(SplitJoin102_CombineDFT_Fiss_7037_7058_join[4]));
	ENDFOR
}

void CombineDFT_6990() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_7037_7058_split[5]), &(SplitJoin102_CombineDFT_Fiss_7037_7058_join[5]));
	ENDFOR
}

void CombineDFT_6991() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_7037_7058_split[6]), &(SplitJoin102_CombineDFT_Fiss_7037_7058_join[6]));
	ENDFOR
}

void CombineDFT_6992() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_7037_7058_split[7]), &(SplitJoin102_CombineDFT_Fiss_7037_7058_join[7]));
	ENDFOR
}

void CombineDFT_6993() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_7037_7058_split[8]), &(SplitJoin102_CombineDFT_Fiss_7037_7058_join[8]));
	ENDFOR
}

void CombineDFT_6994() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_7037_7058_split[9]), &(SplitJoin102_CombineDFT_Fiss_7037_7058_join[9]));
	ENDFOR
}

void CombineDFT_6995() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_7037_7058_split[10]), &(SplitJoin102_CombineDFT_Fiss_7037_7058_join[10]));
	ENDFOR
}

void CombineDFT_6996() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_7037_7058_split[11]), &(SplitJoin102_CombineDFT_Fiss_7037_7058_join[11]));
	ENDFOR
}

void CombineDFT_6997() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_7037_7058_split[12]), &(SplitJoin102_CombineDFT_Fiss_7037_7058_join[12]));
	ENDFOR
}

void CombineDFT_6998() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_7037_7058_split[13]), &(SplitJoin102_CombineDFT_Fiss_7037_7058_join[13]));
	ENDFOR
}

void CombineDFT_6999() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_7037_7058_split[14]), &(SplitJoin102_CombineDFT_Fiss_7037_7058_join[14]));
	ENDFOR
}

void CombineDFT_7000() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_7037_7058_split[15]), &(SplitJoin102_CombineDFT_Fiss_7037_7058_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6983() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin102_CombineDFT_Fiss_7037_7058_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6964WEIGHTED_ROUND_ROBIN_Splitter_6983));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6984() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6984WEIGHTED_ROUND_ROBIN_Splitter_7001, pop_float(&SplitJoin102_CombineDFT_Fiss_7037_7058_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_7003() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin104_CombineDFT_Fiss_7038_7059_split[0]), &(SplitJoin104_CombineDFT_Fiss_7038_7059_join[0]));
	ENDFOR
}

void CombineDFT_7004() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin104_CombineDFT_Fiss_7038_7059_split[1]), &(SplitJoin104_CombineDFT_Fiss_7038_7059_join[1]));
	ENDFOR
}

void CombineDFT_7005() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin104_CombineDFT_Fiss_7038_7059_split[2]), &(SplitJoin104_CombineDFT_Fiss_7038_7059_join[2]));
	ENDFOR
}

void CombineDFT_7006() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin104_CombineDFT_Fiss_7038_7059_split[3]), &(SplitJoin104_CombineDFT_Fiss_7038_7059_join[3]));
	ENDFOR
}

void CombineDFT_7007() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin104_CombineDFT_Fiss_7038_7059_split[4]), &(SplitJoin104_CombineDFT_Fiss_7038_7059_join[4]));
	ENDFOR
}

void CombineDFT_7008() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin104_CombineDFT_Fiss_7038_7059_split[5]), &(SplitJoin104_CombineDFT_Fiss_7038_7059_join[5]));
	ENDFOR
}

void CombineDFT_7009() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin104_CombineDFT_Fiss_7038_7059_split[6]), &(SplitJoin104_CombineDFT_Fiss_7038_7059_join[6]));
	ENDFOR
}

void CombineDFT_7010() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin104_CombineDFT_Fiss_7038_7059_split[7]), &(SplitJoin104_CombineDFT_Fiss_7038_7059_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7001() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin104_CombineDFT_Fiss_7038_7059_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6984WEIGHTED_ROUND_ROBIN_Splitter_7001));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7002() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7002WEIGHTED_ROUND_ROBIN_Splitter_7011, pop_float(&SplitJoin104_CombineDFT_Fiss_7038_7059_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_7013() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin106_CombineDFT_Fiss_7039_7060_split[0]), &(SplitJoin106_CombineDFT_Fiss_7039_7060_join[0]));
	ENDFOR
}

void CombineDFT_7014() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin106_CombineDFT_Fiss_7039_7060_split[1]), &(SplitJoin106_CombineDFT_Fiss_7039_7060_join[1]));
	ENDFOR
}

void CombineDFT_7015() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin106_CombineDFT_Fiss_7039_7060_split[2]), &(SplitJoin106_CombineDFT_Fiss_7039_7060_join[2]));
	ENDFOR
}

void CombineDFT_7016() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin106_CombineDFT_Fiss_7039_7060_split[3]), &(SplitJoin106_CombineDFT_Fiss_7039_7060_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7011() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin106_CombineDFT_Fiss_7039_7060_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7002WEIGHTED_ROUND_ROBIN_Splitter_7011));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7012() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7012WEIGHTED_ROUND_ROBIN_Splitter_7017, pop_float(&SplitJoin106_CombineDFT_Fiss_7039_7060_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_7019() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_7040_7061_split[0]), &(SplitJoin108_CombineDFT_Fiss_7040_7061_join[0]));
	ENDFOR
}

void CombineDFT_7020() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_7040_7061_split[1]), &(SplitJoin108_CombineDFT_Fiss_7040_7061_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7017() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin108_CombineDFT_Fiss_7040_7061_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7012WEIGHTED_ROUND_ROBIN_Splitter_7017));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin108_CombineDFT_Fiss_7040_7061_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7012WEIGHTED_ROUND_ROBIN_Splitter_7017));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7018() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7018CombineDFT_6815, pop_float(&SplitJoin108_CombineDFT_Fiss_7040_7061_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7018CombineDFT_6815, pop_float(&SplitJoin108_CombineDFT_Fiss_7040_7061_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_6815() {
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_7018CombineDFT_6815), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_6783_6819_7022_7043_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6817() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_6783_6819_7022_7043_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6826WEIGHTED_ROUND_ROBIN_Splitter_6817));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_6783_6819_7022_7043_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6826WEIGHTED_ROUND_ROBIN_Splitter_6817));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6818() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 9, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6818FloatPrinter_6816, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_6783_6819_7022_7043_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6818FloatPrinter_6816, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_6783_6819_7022_7043_join[1]));
		ENDFOR
	ENDFOR
}}

void FloatPrinter(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void FloatPrinter_6816() {
	FOR(uint32_t, __iter_steady_, 0, <, 2304, __iter_steady_++)
		FloatPrinter(&(WEIGHTED_ROUND_ROBIN_Joiner_6818FloatPrinter_6816));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_float(&SplitJoin96_FFTReorderSimple_Fiss_7034_7055_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 4, __iter_init_1_++)
		init_buffer_float(&SplitJoin94_FFTReorderSimple_Fiss_7033_7054_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_6783_6819_7022_7043_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 2, __iter_init_3_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_7023_7044_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 4, __iter_init_4_++)
		init_buffer_float(&SplitJoin94_FFTReorderSimple_Fiss_7033_7054_split[__iter_init_4_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6926WEIGHTED_ROUND_ROBIN_Splitter_6929);
	FOR(int, __iter_init_5_, 0, <, 2, __iter_init_5_++)
		init_buffer_float(&SplitJoin92_FFTReorderSimple_Fiss_7032_7053_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 16, __iter_init_6_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_7026_7047_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 16, __iter_init_7_++)
		init_buffer_float(&SplitJoin102_CombineDFT_Fiss_7037_7058_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_float(&SplitJoin108_CombineDFT_Fiss_7040_7061_join[__iter_init_8_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6840WEIGHTED_ROUND_ROBIN_Splitter_6849);
	FOR(int, __iter_init_9_, 0, <, 2, __iter_init_9_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_7021_7042_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_7031_7052_split[__iter_init_10_]);
	ENDFOR
	init_buffer_float(&FFTReorderSimple_6794WEIGHTED_ROUND_ROBIN_Splitter_6829);
	FOR(int, __iter_init_11_, 0, <, 18, __iter_init_11_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_7027_7048_split[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 8, __iter_init_12_++)
		init_buffer_float(&SplitJoin104_CombineDFT_Fiss_7038_7059_join[__iter_init_12_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6964WEIGHTED_ROUND_ROBIN_Splitter_6983);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6868WEIGHTED_ROUND_ROBIN_Splitter_6887);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6906WEIGHTED_ROUND_ROBIN_Splitter_6915);
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_float(&SplitJoin92_FFTReorderSimple_Fiss_7032_7053_join[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 8, __iter_init_14_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_7029_7050_split[__iter_init_14_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7018CombineDFT_6815);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6888WEIGHTED_ROUND_ROBIN_Splitter_6905);
	init_buffer_float(&FFTReorderSimple_6805WEIGHTED_ROUND_ROBIN_Splitter_6925);
	FOR(int, __iter_init_15_, 0, <, 8, __iter_init_15_++)
		init_buffer_float(&SplitJoin104_CombineDFT_Fiss_7038_7059_split[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 4, __iter_init_16_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_7024_7045_join[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 4, __iter_init_17_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_7030_7051_join[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 16, __iter_init_18_++)
		init_buffer_float(&SplitJoin98_FFTReorderSimple_Fiss_7035_7056_join[__iter_init_18_]);
	ENDFOR
	FOR(int, __iter_init_19_, 0, <, 2, __iter_init_19_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_6783_6819_7022_7043_join[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 16, __iter_init_20_++)
		init_buffer_float(&SplitJoin98_FFTReorderSimple_Fiss_7035_7056_split[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 16, __iter_init_21_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_7026_7047_split[__iter_init_21_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6826WEIGHTED_ROUND_ROBIN_Splitter_6817);
	FOR(int, __iter_init_22_, 0, <, 4, __iter_init_22_++)
		init_buffer_float(&SplitJoin106_CombineDFT_Fiss_7039_7060_join[__iter_init_22_]);
	ENDFOR
	FOR(int, __iter_init_23_, 0, <, 8, __iter_init_23_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_7025_7046_split[__iter_init_23_]);
	ENDFOR
	FOR(int, __iter_init_24_, 0, <, 2, __iter_init_24_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_7023_7044_split[__iter_init_24_]);
	ENDFOR
	FOR(int, __iter_init_25_, 0, <, 4, __iter_init_25_++)
		init_buffer_float(&SplitJoin106_CombineDFT_Fiss_7039_7060_split[__iter_init_25_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6930WEIGHTED_ROUND_ROBIN_Splitter_6935);
	FOR(int, __iter_init_26_, 0, <, 2, __iter_init_26_++)
		init_buffer_float(&SplitJoin108_CombineDFT_Fiss_7040_7061_split[__iter_init_26_]);
	ENDFOR
	FOR(int, __iter_init_27_, 0, <, 4, __iter_init_27_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_7024_7045_split[__iter_init_27_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6834WEIGHTED_ROUND_ROBIN_Splitter_6839);
	FOR(int, __iter_init_28_, 0, <, 4, __iter_init_28_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_7030_7051_split[__iter_init_28_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6946WEIGHTED_ROUND_ROBIN_Splitter_6963);
	FOR(int, __iter_init_29_, 0, <, 8, __iter_init_29_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_7025_7046_join[__iter_init_29_]);
	ENDFOR
	FOR(int, __iter_init_30_, 0, <, 8, __iter_init_30_++)
		init_buffer_float(&SplitJoin96_FFTReorderSimple_Fiss_7034_7055_join[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 2, __iter_init_31_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_7021_7042_split[__iter_init_31_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6818FloatPrinter_6816);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7012WEIGHTED_ROUND_ROBIN_Splitter_7017);
	FOR(int, __iter_init_32_, 0, <, 16, __iter_init_32_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_7028_7049_join[__iter_init_32_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6830WEIGHTED_ROUND_ROBIN_Splitter_6833);
	FOR(int, __iter_init_33_, 0, <, 16, __iter_init_33_++)
		init_buffer_float(&SplitJoin102_CombineDFT_Fiss_7037_7058_join[__iter_init_33_]);
	ENDFOR
	FOR(int, __iter_init_34_, 0, <, 2, __iter_init_34_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_7031_7052_join[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 18, __iter_init_35_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_7027_7048_join[__iter_init_35_]);
	ENDFOR
	FOR(int, __iter_init_36_, 0, <, 8, __iter_init_36_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_7029_7050_join[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 16, __iter_init_37_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_7028_7049_split[__iter_init_37_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6936WEIGHTED_ROUND_ROBIN_Splitter_6945);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6916WEIGHTED_ROUND_ROBIN_Splitter_6921);
	FOR(int, __iter_init_38_, 0, <, 18, __iter_init_38_++)
		init_buffer_float(&SplitJoin100_CombineDFT_Fiss_7036_7057_split[__iter_init_38_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6850WEIGHTED_ROUND_ROBIN_Splitter_6867);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6984WEIGHTED_ROUND_ROBIN_Splitter_7001);
	FOR(int, __iter_init_39_, 0, <, 18, __iter_init_39_++)
		init_buffer_float(&SplitJoin100_CombineDFT_Fiss_7036_7057_join[__iter_init_39_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6922CombineDFT_6804);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7002WEIGHTED_ROUND_ROBIN_Splitter_7011);
// --- init: CombineDFT_6869
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6869_s.w[i] = real ; 
		CombineDFT_6869_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6870
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6870_s.w[i] = real ; 
		CombineDFT_6870_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6871
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6871_s.w[i] = real ; 
		CombineDFT_6871_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6872
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6872_s.w[i] = real ; 
		CombineDFT_6872_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6873
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6873_s.w[i] = real ; 
		CombineDFT_6873_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6874
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6874_s.w[i] = real ; 
		CombineDFT_6874_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6875
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6875_s.w[i] = real ; 
		CombineDFT_6875_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6876
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6876_s.w[i] = real ; 
		CombineDFT_6876_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6877
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6877_s.w[i] = real ; 
		CombineDFT_6877_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6878
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6878_s.w[i] = real ; 
		CombineDFT_6878_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6879
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6879_s.w[i] = real ; 
		CombineDFT_6879_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6880
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6880_s.w[i] = real ; 
		CombineDFT_6880_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6881
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6881_s.w[i] = real ; 
		CombineDFT_6881_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6882
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6882_s.w[i] = real ; 
		CombineDFT_6882_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6883
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6883_s.w[i] = real ; 
		CombineDFT_6883_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6884
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6884_s.w[i] = real ; 
		CombineDFT_6884_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6885
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6885_s.w[i] = real ; 
		CombineDFT_6885_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6886
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6886_s.w[i] = real ; 
		CombineDFT_6886_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6889
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6889_s.w[i] = real ; 
		CombineDFT_6889_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6890
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6890_s.w[i] = real ; 
		CombineDFT_6890_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6891
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6891_s.w[i] = real ; 
		CombineDFT_6891_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6892
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6892_s.w[i] = real ; 
		CombineDFT_6892_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6893
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6893_s.w[i] = real ; 
		CombineDFT_6893_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6894
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6894_s.w[i] = real ; 
		CombineDFT_6894_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6895
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6895_s.w[i] = real ; 
		CombineDFT_6895_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6896
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6896_s.w[i] = real ; 
		CombineDFT_6896_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6897
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6897_s.w[i] = real ; 
		CombineDFT_6897_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6898
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6898_s.w[i] = real ; 
		CombineDFT_6898_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6899
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6899_s.w[i] = real ; 
		CombineDFT_6899_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6900
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6900_s.w[i] = real ; 
		CombineDFT_6900_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6901
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6901_s.w[i] = real ; 
		CombineDFT_6901_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6902
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6902_s.w[i] = real ; 
		CombineDFT_6902_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6903
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6903_s.w[i] = real ; 
		CombineDFT_6903_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6904
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6904_s.w[i] = real ; 
		CombineDFT_6904_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6907
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_6907_s.w[i] = real ; 
		CombineDFT_6907_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6908
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_6908_s.w[i] = real ; 
		CombineDFT_6908_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6909
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_6909_s.w[i] = real ; 
		CombineDFT_6909_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6910
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_6910_s.w[i] = real ; 
		CombineDFT_6910_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6911
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_6911_s.w[i] = real ; 
		CombineDFT_6911_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6912
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_6912_s.w[i] = real ; 
		CombineDFT_6912_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6913
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_6913_s.w[i] = real ; 
		CombineDFT_6913_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6914
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_6914_s.w[i] = real ; 
		CombineDFT_6914_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6917
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_6917_s.w[i] = real ; 
		CombineDFT_6917_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6918
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_6918_s.w[i] = real ; 
		CombineDFT_6918_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6919
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_6919_s.w[i] = real ; 
		CombineDFT_6919_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6920
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_6920_s.w[i] = real ; 
		CombineDFT_6920_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6923
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_6923_s.w[i] = real ; 
		CombineDFT_6923_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6924
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_6924_s.w[i] = real ; 
		CombineDFT_6924_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6804
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_6804_s.w[i] = real ; 
		CombineDFT_6804_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6965
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6965_s.w[i] = real ; 
		CombineDFT_6965_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6966
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6966_s.w[i] = real ; 
		CombineDFT_6966_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6967
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6967_s.w[i] = real ; 
		CombineDFT_6967_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6968
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6968_s.w[i] = real ; 
		CombineDFT_6968_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6969
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6969_s.w[i] = real ; 
		CombineDFT_6969_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6970
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6970_s.w[i] = real ; 
		CombineDFT_6970_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6971
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6971_s.w[i] = real ; 
		CombineDFT_6971_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6972
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6972_s.w[i] = real ; 
		CombineDFT_6972_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6973
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6973_s.w[i] = real ; 
		CombineDFT_6973_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6974
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6974_s.w[i] = real ; 
		CombineDFT_6974_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6975
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6975_s.w[i] = real ; 
		CombineDFT_6975_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6976
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6976_s.w[i] = real ; 
		CombineDFT_6976_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6977
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6977_s.w[i] = real ; 
		CombineDFT_6977_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6978
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6978_s.w[i] = real ; 
		CombineDFT_6978_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6979
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6979_s.w[i] = real ; 
		CombineDFT_6979_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6980
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6980_s.w[i] = real ; 
		CombineDFT_6980_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6981
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6981_s.w[i] = real ; 
		CombineDFT_6981_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6982
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6982_s.w[i] = real ; 
		CombineDFT_6982_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6985
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6985_s.w[i] = real ; 
		CombineDFT_6985_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6986
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6986_s.w[i] = real ; 
		CombineDFT_6986_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6987
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6987_s.w[i] = real ; 
		CombineDFT_6987_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6988
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6988_s.w[i] = real ; 
		CombineDFT_6988_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6989
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6989_s.w[i] = real ; 
		CombineDFT_6989_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6990
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6990_s.w[i] = real ; 
		CombineDFT_6990_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6991
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6991_s.w[i] = real ; 
		CombineDFT_6991_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6992
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6992_s.w[i] = real ; 
		CombineDFT_6992_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6993
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6993_s.w[i] = real ; 
		CombineDFT_6993_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6994
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6994_s.w[i] = real ; 
		CombineDFT_6994_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6995
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6995_s.w[i] = real ; 
		CombineDFT_6995_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6996
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6996_s.w[i] = real ; 
		CombineDFT_6996_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6997
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6997_s.w[i] = real ; 
		CombineDFT_6997_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6998
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6998_s.w[i] = real ; 
		CombineDFT_6998_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6999
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6999_s.w[i] = real ; 
		CombineDFT_6999_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7000
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7000_s.w[i] = real ; 
		CombineDFT_7000_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7003
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7003_s.w[i] = real ; 
		CombineDFT_7003_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7004
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7004_s.w[i] = real ; 
		CombineDFT_7004_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7005
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7005_s.w[i] = real ; 
		CombineDFT_7005_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7006
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7006_s.w[i] = real ; 
		CombineDFT_7006_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7007
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7007_s.w[i] = real ; 
		CombineDFT_7007_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7008
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7008_s.w[i] = real ; 
		CombineDFT_7008_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7009
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7009_s.w[i] = real ; 
		CombineDFT_7009_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7010
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7010_s.w[i] = real ; 
		CombineDFT_7010_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7013
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_7013_s.w[i] = real ; 
		CombineDFT_7013_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7014
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_7014_s.w[i] = real ; 
		CombineDFT_7014_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7015
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_7015_s.w[i] = real ; 
		CombineDFT_7015_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7016
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_7016_s.w[i] = real ; 
		CombineDFT_7016_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7019
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_7019_s.w[i] = real ; 
		CombineDFT_7019_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7020
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_7020_s.w[i] = real ; 
		CombineDFT_7020_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6815
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_6815_s.w[i] = real ; 
		CombineDFT_6815_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_6825();
			FFTTestSource_6827();
			FFTTestSource_6828();
		WEIGHTED_ROUND_ROBIN_Joiner_6826();
		WEIGHTED_ROUND_ROBIN_Splitter_6817();
			FFTReorderSimple_6794();
			WEIGHTED_ROUND_ROBIN_Splitter_6829();
				FFTReorderSimple_6831();
				FFTReorderSimple_6832();
			WEIGHTED_ROUND_ROBIN_Joiner_6830();
			WEIGHTED_ROUND_ROBIN_Splitter_6833();
				FFTReorderSimple_6835();
				FFTReorderSimple_6836();
				FFTReorderSimple_6837();
				FFTReorderSimple_6838();
			WEIGHTED_ROUND_ROBIN_Joiner_6834();
			WEIGHTED_ROUND_ROBIN_Splitter_6839();
				FFTReorderSimple_6841();
				FFTReorderSimple_6842();
				FFTReorderSimple_6843();
				FFTReorderSimple_6844();
				FFTReorderSimple_6845();
				FFTReorderSimple_6846();
				FFTReorderSimple_6847();
				FFTReorderSimple_6848();
			WEIGHTED_ROUND_ROBIN_Joiner_6840();
			WEIGHTED_ROUND_ROBIN_Splitter_6849();
				FFTReorderSimple_6851();
				FFTReorderSimple_6852();
				FFTReorderSimple_6853();
				FFTReorderSimple_6854();
				FFTReorderSimple_6855();
				FFTReorderSimple_6856();
				FFTReorderSimple_6857();
				FFTReorderSimple_6858();
				FFTReorderSimple_6859();
				FFTReorderSimple_6860();
				FFTReorderSimple_6861();
				FFTReorderSimple_6862();
				FFTReorderSimple_6863();
				FFTReorderSimple_6864();
				FFTReorderSimple_6865();
				FFTReorderSimple_6866();
			WEIGHTED_ROUND_ROBIN_Joiner_6850();
			WEIGHTED_ROUND_ROBIN_Splitter_6867();
				CombineDFT_6869();
				CombineDFT_6870();
				CombineDFT_6871();
				CombineDFT_6872();
				CombineDFT_6873();
				CombineDFT_6874();
				CombineDFT_6875();
				CombineDFT_6876();
				CombineDFT_6877();
				CombineDFT_6878();
				CombineDFT_6879();
				CombineDFT_6880();
				CombineDFT_6881();
				CombineDFT_6882();
				CombineDFT_6883();
				CombineDFT_6884();
				CombineDFT_6885();
				CombineDFT_6886();
			WEIGHTED_ROUND_ROBIN_Joiner_6868();
			WEIGHTED_ROUND_ROBIN_Splitter_6887();
				CombineDFT_6889();
				CombineDFT_6890();
				CombineDFT_6891();
				CombineDFT_6892();
				CombineDFT_6893();
				CombineDFT_6894();
				CombineDFT_6895();
				CombineDFT_6896();
				CombineDFT_6897();
				CombineDFT_6898();
				CombineDFT_6899();
				CombineDFT_6900();
				CombineDFT_6901();
				CombineDFT_6902();
				CombineDFT_6903();
				CombineDFT_6904();
			WEIGHTED_ROUND_ROBIN_Joiner_6888();
			WEIGHTED_ROUND_ROBIN_Splitter_6905();
				CombineDFT_6907();
				CombineDFT_6908();
				CombineDFT_6909();
				CombineDFT_6910();
				CombineDFT_6911();
				CombineDFT_6912();
				CombineDFT_6913();
				CombineDFT_6914();
			WEIGHTED_ROUND_ROBIN_Joiner_6906();
			WEIGHTED_ROUND_ROBIN_Splitter_6915();
				CombineDFT_6917();
				CombineDFT_6918();
				CombineDFT_6919();
				CombineDFT_6920();
			WEIGHTED_ROUND_ROBIN_Joiner_6916();
			WEIGHTED_ROUND_ROBIN_Splitter_6921();
				CombineDFT_6923();
				CombineDFT_6924();
			WEIGHTED_ROUND_ROBIN_Joiner_6922();
			CombineDFT_6804();
			FFTReorderSimple_6805();
			WEIGHTED_ROUND_ROBIN_Splitter_6925();
				FFTReorderSimple_6927();
				FFTReorderSimple_6928();
			WEIGHTED_ROUND_ROBIN_Joiner_6926();
			WEIGHTED_ROUND_ROBIN_Splitter_6929();
				FFTReorderSimple_6931();
				FFTReorderSimple_6932();
				FFTReorderSimple_6933();
				FFTReorderSimple_6934();
			WEIGHTED_ROUND_ROBIN_Joiner_6930();
			WEIGHTED_ROUND_ROBIN_Splitter_6935();
				FFTReorderSimple_6937();
				FFTReorderSimple_6938();
				FFTReorderSimple_6939();
				FFTReorderSimple_6940();
				FFTReorderSimple_6941();
				FFTReorderSimple_6942();
				FFTReorderSimple_6943();
				FFTReorderSimple_6944();
			WEIGHTED_ROUND_ROBIN_Joiner_6936();
			WEIGHTED_ROUND_ROBIN_Splitter_6945();
				FFTReorderSimple_6947();
				FFTReorderSimple_6948();
				FFTReorderSimple_6949();
				FFTReorderSimple_6950();
				FFTReorderSimple_6951();
				FFTReorderSimple_6952();
				FFTReorderSimple_6953();
				FFTReorderSimple_6954();
				FFTReorderSimple_6955();
				FFTReorderSimple_6956();
				FFTReorderSimple_6957();
				FFTReorderSimple_6958();
				FFTReorderSimple_6959();
				FFTReorderSimple_6960();
				FFTReorderSimple_6961();
				FFTReorderSimple_6962();
			WEIGHTED_ROUND_ROBIN_Joiner_6946();
			WEIGHTED_ROUND_ROBIN_Splitter_6963();
				CombineDFT_6965();
				CombineDFT_6966();
				CombineDFT_6967();
				CombineDFT_6968();
				CombineDFT_6969();
				CombineDFT_6970();
				CombineDFT_6971();
				CombineDFT_6972();
				CombineDFT_6973();
				CombineDFT_6974();
				CombineDFT_6975();
				CombineDFT_6976();
				CombineDFT_6977();
				CombineDFT_6978();
				CombineDFT_6979();
				CombineDFT_6980();
				CombineDFT_6981();
				CombineDFT_6982();
			WEIGHTED_ROUND_ROBIN_Joiner_6964();
			WEIGHTED_ROUND_ROBIN_Splitter_6983();
				CombineDFT_6985();
				CombineDFT_6986();
				CombineDFT_6987();
				CombineDFT_6988();
				CombineDFT_6989();
				CombineDFT_6990();
				CombineDFT_6991();
				CombineDFT_6992();
				CombineDFT_6993();
				CombineDFT_6994();
				CombineDFT_6995();
				CombineDFT_6996();
				CombineDFT_6997();
				CombineDFT_6998();
				CombineDFT_6999();
				CombineDFT_7000();
			WEIGHTED_ROUND_ROBIN_Joiner_6984();
			WEIGHTED_ROUND_ROBIN_Splitter_7001();
				CombineDFT_7003();
				CombineDFT_7004();
				CombineDFT_7005();
				CombineDFT_7006();
				CombineDFT_7007();
				CombineDFT_7008();
				CombineDFT_7009();
				CombineDFT_7010();
			WEIGHTED_ROUND_ROBIN_Joiner_7002();
			WEIGHTED_ROUND_ROBIN_Splitter_7011();
				CombineDFT_7013();
				CombineDFT_7014();
				CombineDFT_7015();
				CombineDFT_7016();
			WEIGHTED_ROUND_ROBIN_Joiner_7012();
			WEIGHTED_ROUND_ROBIN_Splitter_7017();
				CombineDFT_7019();
				CombineDFT_7020();
			WEIGHTED_ROUND_ROBIN_Joiner_7018();
			CombineDFT_6815();
		WEIGHTED_ROUND_ROBIN_Joiner_6818();
		FloatPrinter_6816();
	ENDFOR
	return EXIT_SUCCESS;
}
