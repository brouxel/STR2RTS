#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=896 on the compile command line
#else
#if BUF_SIZEMAX < 896
#error BUF_SIZEMAX too small, it must be at least 896
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	complex_t wn;
} CombineDFT_1079_t;
void FFTTestSource_1025();
void FFTReorderSimple_1026();
void WEIGHTED_ROUND_ROBIN_Splitter_1039();
void FFTReorderSimple_1041();
void FFTReorderSimple_1042();
void WEIGHTED_ROUND_ROBIN_Joiner_1040();
void WEIGHTED_ROUND_ROBIN_Splitter_1043();
void FFTReorderSimple_1045();
void FFTReorderSimple_1046();
void FFTReorderSimple_1047();
void FFTReorderSimple_1048();
void WEIGHTED_ROUND_ROBIN_Joiner_1044();
void WEIGHTED_ROUND_ROBIN_Splitter_1049();
void FFTReorderSimple_1051();
void FFTReorderSimple_1052();
void FFTReorderSimple_1053();
void FFTReorderSimple_1054();
void FFTReorderSimple_1055();
void FFTReorderSimple_1056();
void FFTReorderSimple_1057();
void FFTReorderSimple_1058();
void WEIGHTED_ROUND_ROBIN_Joiner_1050();
void WEIGHTED_ROUND_ROBIN_Splitter_1059();
void FFTReorderSimple_1061();
void FFTReorderSimple_1062();
void FFTReorderSimple_1063();
void FFTReorderSimple_1064();
void FFTReorderSimple_1065();
void FFTReorderSimple_1066();
void FFTReorderSimple_1067();
void FFTReorderSimple_1068();
void FFTReorderSimple_1069();
void FFTReorderSimple_1070();
void FFTReorderSimple_1071();
void FFTReorderSimple_1072();
void FFTReorderSimple_1073();
void FFTReorderSimple_1074();
void FFTReorderSimple_1075();
void FFTReorderSimple_1076();
void WEIGHTED_ROUND_ROBIN_Joiner_1060();
void WEIGHTED_ROUND_ROBIN_Splitter_1077();
void CombineDFT_1079();
void CombineDFT_1080();
void CombineDFT_1081();
void CombineDFT_1082();
void CombineDFT_1083();
void CombineDFT_1084();
void CombineDFT_1085();
void CombineDFT_1086();
void CombineDFT_1087();
void CombineDFT_1088();
void CombineDFT_1089();
void CombineDFT_1090();
void CombineDFT_1091();
void CombineDFT_1092();
void CombineDFT_1093();
void CombineDFT_1094();
void CombineDFT_1095();
void CombineDFT_1096();
void CombineDFT_1097();
void CombineDFT_1098();
void CombineDFT_1099();
void CombineDFT_1100();
void CombineDFT_1101();
void CombineDFT_1102();
void CombineDFT_1103();
void CombineDFT_1104();
void CombineDFT_1105();
void CombineDFT_1106();
void WEIGHTED_ROUND_ROBIN_Joiner_1078();
void WEIGHTED_ROUND_ROBIN_Splitter_1107();
void CombineDFT_1109();
void CombineDFT_1110();
void CombineDFT_1111();
void CombineDFT_1112();
void CombineDFT_1113();
void CombineDFT_1114();
void CombineDFT_1115();
void CombineDFT_1116();
void CombineDFT_1117();
void CombineDFT_1118();
void CombineDFT_1119();
void CombineDFT_1120();
void CombineDFT_1121();
void CombineDFT_1122();
void CombineDFT_1123();
void CombineDFT_1124();
void WEIGHTED_ROUND_ROBIN_Joiner_1108();
void WEIGHTED_ROUND_ROBIN_Splitter_1125();
void CombineDFT_1127();
void CombineDFT_1128();
void CombineDFT_1129();
void CombineDFT_1130();
void CombineDFT_1131();
void CombineDFT_1132();
void CombineDFT_1133();
void CombineDFT_1134();
void WEIGHTED_ROUND_ROBIN_Joiner_1126();
void WEIGHTED_ROUND_ROBIN_Splitter_1135();
void CombineDFT_1137();
void CombineDFT_1138();
void CombineDFT_1139();
void CombineDFT_1140();
void WEIGHTED_ROUND_ROBIN_Joiner_1136();
void WEIGHTED_ROUND_ROBIN_Splitter_1141();
void CombineDFT_1143();
void CombineDFT_1144();
void WEIGHTED_ROUND_ROBIN_Joiner_1142();
void CombineDFT_1036();
void CPrinter_1037();

#ifdef __cplusplus
}
#endif
#endif
