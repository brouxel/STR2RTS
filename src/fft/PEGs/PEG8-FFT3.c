#include "PEG8-FFT3.h"

buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_7268_7663_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_Hier_7840_7861_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7739WEIGHTED_ROUND_ROBIN_Splitter_7740;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7745WEIGHTED_ROUND_ROBIN_Splitter_7746;
buffer_float_t Pre_CollapsedDataParallel_1_7629WEIGHTED_ROUND_ROBIN_Splitter_7801;
buffer_float_t SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_child0_7704_7863_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7802Post_CollapsedDataParallel_2_7630;
buffer_float_t SplitJoin61_Butterfly_Fiss_7853_7879_join[2];
buffer_float_t BitReverse_7391FloatPrinter_7392;
buffer_float_t SplitJoin0_Butterfly_Fiss_7839_7860_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7836Post_CollapsedDataParallel_2_7654;
buffer_float_t SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child1_7847_7883_join[8];
buffer_float_t Pre_CollapsedDataParallel_1_7620WEIGHTED_ROUND_ROBIN_Splitter_7773;
buffer_float_t Pre_CollapsedDataParallel_1_7644WEIGHTED_ROUND_ROBIN_Splitter_7823;
buffer_float_t SplitJoin85_Butterfly_Fiss_7856_7866_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7832Post_CollapsedDataParallel_2_7651;
buffer_float_t SplitJoin8_Butterfly_Fiss_7842_7864_split[4];
buffer_float_t SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_Hier_7843_7870_split[2];
buffer_float_t Pre_CollapsedDataParallel_1_7611WEIGHTED_ROUND_ROBIN_Splitter_7753;
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_7268_7663_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_Hier_7840_7861_split[2];
buffer_float_t SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_child0_7733_7871_join[4];
buffer_float_t SplitJoin57_Butterfly_Fiss_7852_7878_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7780Post_CollapsedDataParallel_2_7624;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7820Post_CollapsedDataParallel_2_7642;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7828Post_CollapsedDataParallel_2_7648;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7824Post_CollapsedDataParallel_2_7645;
buffer_float_t SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_child0_7733_7871_split[4];
buffer_float_t SplitJoin65_Butterfly_Fiss_7854_7880_split[2];
buffer_float_t SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child0_7846_7882_join[8];
buffer_float_t Post_CollapsedDataParallel_2_7615WEIGHTED_ROUND_ROBIN_Splitter_7735;
buffer_float_t SplitJoin61_Butterfly_Fiss_7853_7879_split[2];
buffer_float_t SplitJoin53_Butterfly_Fiss_7851_7877_join[2];
buffer_float_t Pre_CollapsedDataParallel_1_7647WEIGHTED_ROUND_ROBIN_Splitter_7827;
buffer_float_t Pre_CollapsedDataParallel_1_7614WEIGHTED_ROUND_ROBIN_Splitter_7763;
buffer_float_t Pre_CollapsedDataParallel_1_7650WEIGHTED_ROUND_ROBIN_Splitter_7831;
buffer_float_t SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_child0_7704_7863_join[2];
buffer_float_t SplitJoin89_Butterfly_Fiss_7857_7868_split[4];
buffer_float_t SplitJoin39_Butterfly_Fiss_7848_7873_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7808Post_CollapsedDataParallel_2_7633;
buffer_float_t SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_Hier_7845_7881_join[2];
buffer_float_t SplitJoin4_Butterfly_Fiss_7841_7862_split[8];
buffer_float_t FloatSource_7310Pre_CollapsedDataParallel_1_7611;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7774Post_CollapsedDataParallel_2_7621;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7786Post_CollapsedDataParallel_2_7618;
buffer_float_t Pre_CollapsedDataParallel_1_7635WEIGHTED_ROUND_ROBIN_Splitter_7811;
buffer_float_t SplitJoin47_Butterfly_Fiss_7850_7875_split[2];
buffer_float_t SplitJoin43_Butterfly_Fiss_7849_7874_split[2];
buffer_float_t SplitJoin8_Butterfly_Fiss_7842_7864_join[4];
buffer_float_t SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child0_7846_7882_split[8];
buffer_float_t Pre_CollapsedDataParallel_1_7617WEIGHTED_ROUND_ROBIN_Splitter_7785;
buffer_float_t SplitJoin43_Butterfly_Fiss_7849_7874_join[2];
buffer_float_t SplitJoin0_Butterfly_Fiss_7839_7860_join[8];
buffer_float_t SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child1_7847_7883_split[8];
buffer_float_t Post_CollapsedDataParallel_2_7618WEIGHTED_ROUND_ROBIN_Splitter_7737;
buffer_float_t SplitJoin72_Butterfly_Fiss_7855_7865_split[4];
buffer_float_t SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_child1_7734_7876_split[4];
buffer_float_t Pre_CollapsedDataParallel_1_7632WEIGHTED_ROUND_ROBIN_Splitter_7807;
buffer_float_t SplitJoin4_Butterfly_Fiss_7841_7862_join[8];
buffer_float_t SplitJoin95_Butterfly_Fiss_7858_7869_join[4];
buffer_float_t SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_Hier_7843_7870_join[2];
buffer_float_t SplitJoin14_Butterfly_Fiss_7844_7872_split[2];
buffer_float_t Pre_CollapsedDataParallel_1_7626WEIGHTED_ROUND_ROBIN_Splitter_7795;
buffer_float_t Post_CollapsedDataParallel_2_7612WEIGHTED_ROUND_ROBIN_Splitter_7655;
buffer_float_t SplitJoin14_Butterfly_Fiss_7844_7872_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7812Post_CollapsedDataParallel_2_7636;
buffer_float_t Pre_CollapsedDataParallel_1_7638WEIGHTED_ROUND_ROBIN_Splitter_7815;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7816Post_CollapsedDataParallel_2_7639;
buffer_float_t SplitJoin65_Butterfly_Fiss_7854_7880_join[2];
buffer_float_t Pre_CollapsedDataParallel_1_7623WEIGHTED_ROUND_ROBIN_Splitter_7779;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7754Post_CollapsedDataParallel_2_7612;
buffer_float_t SplitJoin47_Butterfly_Fiss_7850_7875_join[2];
buffer_float_t SplitJoin53_Butterfly_Fiss_7851_7877_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7764Post_CollapsedDataParallel_2_7615;
buffer_float_t SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_child1_7709_7867_join[2];
buffer_float_t Pre_CollapsedDataParallel_1_7653WEIGHTED_ROUND_ROBIN_Splitter_7835;
buffer_float_t SplitJoin57_Butterfly_Fiss_7852_7878_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7796Post_CollapsedDataParallel_2_7627;
buffer_float_t SplitJoin95_Butterfly_Fiss_7858_7869_split[4];
buffer_float_t SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_Hier_7845_7881_split[2];
buffer_float_t SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_child1_7734_7876_join[4];
buffer_float_t SplitJoin72_Butterfly_Fiss_7855_7865_join[4];
buffer_float_t SplitJoin89_Butterfly_Fiss_7857_7868_join[4];
buffer_float_t SplitJoin85_Butterfly_Fiss_7856_7866_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7751BitReverse_7391;
buffer_float_t SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_child1_7709_7867_split[2];
buffer_float_t SplitJoin39_Butterfly_Fiss_7848_7873_split[2];
buffer_float_t Pre_CollapsedDataParallel_1_7641WEIGHTED_ROUND_ROBIN_Splitter_7819;


FloatSource_7310_t FloatSource_7310_s;

void FloatSource(buffer_float_t *chanout) {
		push_float(&(*chanout), FloatSource_7310_s.A_re[FloatSource_7310_s.idx]) ; 
		push_float(&(*chanout), FloatSource_7310_s.A_im[FloatSource_7310_s.idx]) ; 
		FloatSource_7310_s.idx++ ; 
		if((FloatSource_7310_s.idx >= 32)) {
			FloatSource_7310_s.idx = 0 ; 
		}
	}


void FloatSource_7310() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FloatSource(&(FloatSource_7310Pre_CollapsedDataParallel_1_7611));
	ENDFOR
}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
	int partialSum_k = 0;
 {
	FOR(int, _k, 0,  < , 16, _k++) {
		int iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
			}
			ENDFOR
		}
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 32) ; 
		}
		ENDFOR
	}
		partialSum_k = (partialSum_k + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&(*chanin)) ; 
}


void Pre_CollapsedDataParallel_1_7611() {
	Pre_CollapsedDataParallel_1(&(FloatSource_7310Pre_CollapsedDataParallel_1_7611), &(Pre_CollapsedDataParallel_1_7611WEIGHTED_ROUND_ROBIN_Splitter_7753));
}

void Butterfly(buffer_float_t *chanin, buffer_float_t *chanout) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&(*chanin)) ; 
		u_im = pop_float(&(*chanin)) ; 
		t_re = pop_float(&(*chanin)) ; 
		t_im = pop_float(&(*chanin)) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&(*chanout), u_re) ; 
		push_float(&(*chanout), u_im) ; 
		push_float(&(*chanout), t_re) ; 
		push_float(&(*chanout), t_im) ; 
	}


void Butterfly_7755() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_7839_7860_split[0]), &(SplitJoin0_Butterfly_Fiss_7839_7860_join[0]));
	ENDFOR
}

void Butterfly_7756() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_7839_7860_split[1]), &(SplitJoin0_Butterfly_Fiss_7839_7860_join[1]));
	ENDFOR
}

void Butterfly_7757() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_7839_7860_split[2]), &(SplitJoin0_Butterfly_Fiss_7839_7860_join[2]));
	ENDFOR
}

void Butterfly_7758() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_7839_7860_split[3]), &(SplitJoin0_Butterfly_Fiss_7839_7860_join[3]));
	ENDFOR
}

void Butterfly_7759() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_7839_7860_split[4]), &(SplitJoin0_Butterfly_Fiss_7839_7860_join[4]));
	ENDFOR
}

void Butterfly_7760() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_7839_7860_split[5]), &(SplitJoin0_Butterfly_Fiss_7839_7860_join[5]));
	ENDFOR
}

void Butterfly_7761() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_7839_7860_split[6]), &(SplitJoin0_Butterfly_Fiss_7839_7860_join[6]));
	ENDFOR
}

void Butterfly_7762() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_7839_7860_split[7]), &(SplitJoin0_Butterfly_Fiss_7839_7860_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7753() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin0_Butterfly_Fiss_7839_7860_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_7611WEIGHTED_ROUND_ROBIN_Splitter_7753));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7754() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7754Post_CollapsedDataParallel_2_7612, pop_float(&SplitJoin0_Butterfly_Fiss_7839_7860_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
	int kTimesWeights_i = 0;
 {
	FOR(int, _k, 0,  < , 2, _k++) {
		int partialSum_i = 0;
 {
		FOR(int, _i, 0,  < , 16, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&(*chanout), peek_float(&(*chanin), (kTimesWeights_i + (partialSum_i + _j)))) ; 
			}
			ENDFOR
		}
			partialSum_i = (partialSum_i + 4) ; 
		}
		ENDFOR
	}
		kTimesWeights_i = (kTimesWeights_i + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&(*chanin)) ; 
}


void Post_CollapsedDataParallel_2_7612() {
	Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_7754Post_CollapsedDataParallel_2_7612), &(Post_CollapsedDataParallel_2_7612WEIGHTED_ROUND_ROBIN_Splitter_7655));
}

void Pre_CollapsedDataParallel_1_7614() {
	Pre_CollapsedDataParallel_1(&(SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_7268_7663_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_Hier_7840_7861_split[0]), &(Pre_CollapsedDataParallel_1_7614WEIGHTED_ROUND_ROBIN_Splitter_7763));
}

void Butterfly_7765() {
	Butterfly(&(SplitJoin4_Butterfly_Fiss_7841_7862_split[0]), &(SplitJoin4_Butterfly_Fiss_7841_7862_join[0]));
}

void Butterfly_7766() {
	Butterfly(&(SplitJoin4_Butterfly_Fiss_7841_7862_split[1]), &(SplitJoin4_Butterfly_Fiss_7841_7862_join[1]));
}

void Butterfly_7767() {
	Butterfly(&(SplitJoin4_Butterfly_Fiss_7841_7862_split[2]), &(SplitJoin4_Butterfly_Fiss_7841_7862_join[2]));
}

void Butterfly_7768() {
	Butterfly(&(SplitJoin4_Butterfly_Fiss_7841_7862_split[3]), &(SplitJoin4_Butterfly_Fiss_7841_7862_join[3]));
}

void Butterfly_7769() {
	Butterfly(&(SplitJoin4_Butterfly_Fiss_7841_7862_split[4]), &(SplitJoin4_Butterfly_Fiss_7841_7862_join[4]));
}

void Butterfly_7770() {
	Butterfly(&(SplitJoin4_Butterfly_Fiss_7841_7862_split[5]), &(SplitJoin4_Butterfly_Fiss_7841_7862_join[5]));
}

void Butterfly_7771() {
	Butterfly(&(SplitJoin4_Butterfly_Fiss_7841_7862_split[6]), &(SplitJoin4_Butterfly_Fiss_7841_7862_join[6]));
}

void Butterfly_7772() {
	Butterfly(&(SplitJoin4_Butterfly_Fiss_7841_7862_split[7]), &(SplitJoin4_Butterfly_Fiss_7841_7862_join[7]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7763() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_float(&SplitJoin4_Butterfly_Fiss_7841_7862_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_7614WEIGHTED_ROUND_ROBIN_Splitter_7763));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7764() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7764Post_CollapsedDataParallel_2_7615, pop_float(&SplitJoin4_Butterfly_Fiss_7841_7862_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void Post_CollapsedDataParallel_2_7615() {
	Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_7764Post_CollapsedDataParallel_2_7615), &(Post_CollapsedDataParallel_2_7615WEIGHTED_ROUND_ROBIN_Splitter_7735));
}

void Pre_CollapsedDataParallel_1_7620() {
	Pre_CollapsedDataParallel_1(&(SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_child0_7704_7863_split[0]), &(Pre_CollapsedDataParallel_1_7620WEIGHTED_ROUND_ROBIN_Splitter_7773));
}

void Butterfly_7775() {
	Butterfly(&(SplitJoin8_Butterfly_Fiss_7842_7864_split[0]), &(SplitJoin8_Butterfly_Fiss_7842_7864_join[0]));
}

void Butterfly_7776() {
	Butterfly(&(SplitJoin8_Butterfly_Fiss_7842_7864_split[1]), &(SplitJoin8_Butterfly_Fiss_7842_7864_join[1]));
}

void Butterfly_7777() {
	Butterfly(&(SplitJoin8_Butterfly_Fiss_7842_7864_split[2]), &(SplitJoin8_Butterfly_Fiss_7842_7864_join[2]));
}

void Butterfly_7778() {
	Butterfly(&(SplitJoin8_Butterfly_Fiss_7842_7864_split[3]), &(SplitJoin8_Butterfly_Fiss_7842_7864_join[3]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7773() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_float(&SplitJoin8_Butterfly_Fiss_7842_7864_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_7620WEIGHTED_ROUND_ROBIN_Splitter_7773));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7774() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7774Post_CollapsedDataParallel_2_7621, pop_float(&SplitJoin8_Butterfly_Fiss_7842_7864_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void Post_CollapsedDataParallel_2_7621() {
	Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_7774Post_CollapsedDataParallel_2_7621), &(SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_child0_7704_7863_join[0]));
}

void Pre_CollapsedDataParallel_1_7623() {
	Pre_CollapsedDataParallel_1(&(SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_child0_7704_7863_split[1]), &(Pre_CollapsedDataParallel_1_7623WEIGHTED_ROUND_ROBIN_Splitter_7779));
}

void Butterfly_7781() {
	Butterfly(&(SplitJoin72_Butterfly_Fiss_7855_7865_split[0]), &(SplitJoin72_Butterfly_Fiss_7855_7865_join[0]));
}

void Butterfly_7782() {
	Butterfly(&(SplitJoin72_Butterfly_Fiss_7855_7865_split[1]), &(SplitJoin72_Butterfly_Fiss_7855_7865_join[1]));
}

void Butterfly_7783() {
	Butterfly(&(SplitJoin72_Butterfly_Fiss_7855_7865_split[2]), &(SplitJoin72_Butterfly_Fiss_7855_7865_join[2]));
}

void Butterfly_7784() {
	Butterfly(&(SplitJoin72_Butterfly_Fiss_7855_7865_split[3]), &(SplitJoin72_Butterfly_Fiss_7855_7865_join[3]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7779() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_float(&SplitJoin72_Butterfly_Fiss_7855_7865_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_7623WEIGHTED_ROUND_ROBIN_Splitter_7779));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7780() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7780Post_CollapsedDataParallel_2_7624, pop_float(&SplitJoin72_Butterfly_Fiss_7855_7865_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void Post_CollapsedDataParallel_2_7624() {
	Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_7780Post_CollapsedDataParallel_2_7624), &(SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_child0_7704_7863_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7735() {
	FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
		push_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_child0_7704_7863_split[0], pop_float(&Post_CollapsedDataParallel_2_7615WEIGHTED_ROUND_ROBIN_Splitter_7735));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
		push_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_child0_7704_7863_split[1], pop_float(&Post_CollapsedDataParallel_2_7615WEIGHTED_ROUND_ROBIN_Splitter_7735));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7736() {
	FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
		push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_7268_7663_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_Hier_7840_7861_join[0], pop_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_child0_7704_7863_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
		push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_7268_7663_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_Hier_7840_7861_join[0], pop_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_child0_7704_7863_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_7617() {
	Pre_CollapsedDataParallel_1(&(SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_7268_7663_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_Hier_7840_7861_split[1]), &(Pre_CollapsedDataParallel_1_7617WEIGHTED_ROUND_ROBIN_Splitter_7785));
}

void Butterfly_7787() {
	Butterfly(&(SplitJoin85_Butterfly_Fiss_7856_7866_split[0]), &(SplitJoin85_Butterfly_Fiss_7856_7866_join[0]));
}

void Butterfly_7788() {
	Butterfly(&(SplitJoin85_Butterfly_Fiss_7856_7866_split[1]), &(SplitJoin85_Butterfly_Fiss_7856_7866_join[1]));
}

void Butterfly_7789() {
	Butterfly(&(SplitJoin85_Butterfly_Fiss_7856_7866_split[2]), &(SplitJoin85_Butterfly_Fiss_7856_7866_join[2]));
}

void Butterfly_7790() {
	Butterfly(&(SplitJoin85_Butterfly_Fiss_7856_7866_split[3]), &(SplitJoin85_Butterfly_Fiss_7856_7866_join[3]));
}

void Butterfly_7791() {
	Butterfly(&(SplitJoin85_Butterfly_Fiss_7856_7866_split[4]), &(SplitJoin85_Butterfly_Fiss_7856_7866_join[4]));
}

void Butterfly_7792() {
	Butterfly(&(SplitJoin85_Butterfly_Fiss_7856_7866_split[5]), &(SplitJoin85_Butterfly_Fiss_7856_7866_join[5]));
}

void Butterfly_7793() {
	Butterfly(&(SplitJoin85_Butterfly_Fiss_7856_7866_split[6]), &(SplitJoin85_Butterfly_Fiss_7856_7866_join[6]));
}

void Butterfly_7794() {
	Butterfly(&(SplitJoin85_Butterfly_Fiss_7856_7866_split[7]), &(SplitJoin85_Butterfly_Fiss_7856_7866_join[7]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7785() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_float(&SplitJoin85_Butterfly_Fiss_7856_7866_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_7617WEIGHTED_ROUND_ROBIN_Splitter_7785));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7786() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7786Post_CollapsedDataParallel_2_7618, pop_float(&SplitJoin85_Butterfly_Fiss_7856_7866_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void Post_CollapsedDataParallel_2_7618() {
	Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_7786Post_CollapsedDataParallel_2_7618), &(Post_CollapsedDataParallel_2_7618WEIGHTED_ROUND_ROBIN_Splitter_7737));
}

void Pre_CollapsedDataParallel_1_7626() {
	Pre_CollapsedDataParallel_1(&(SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_child1_7709_7867_split[0]), &(Pre_CollapsedDataParallel_1_7626WEIGHTED_ROUND_ROBIN_Splitter_7795));
}

void Butterfly_7797() {
	Butterfly(&(SplitJoin89_Butterfly_Fiss_7857_7868_split[0]), &(SplitJoin89_Butterfly_Fiss_7857_7868_join[0]));
}

void Butterfly_7798() {
	Butterfly(&(SplitJoin89_Butterfly_Fiss_7857_7868_split[1]), &(SplitJoin89_Butterfly_Fiss_7857_7868_join[1]));
}

void Butterfly_7799() {
	Butterfly(&(SplitJoin89_Butterfly_Fiss_7857_7868_split[2]), &(SplitJoin89_Butterfly_Fiss_7857_7868_join[2]));
}

void Butterfly_7800() {
	Butterfly(&(SplitJoin89_Butterfly_Fiss_7857_7868_split[3]), &(SplitJoin89_Butterfly_Fiss_7857_7868_join[3]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7795() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_float(&SplitJoin89_Butterfly_Fiss_7857_7868_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_7626WEIGHTED_ROUND_ROBIN_Splitter_7795));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7796() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7796Post_CollapsedDataParallel_2_7627, pop_float(&SplitJoin89_Butterfly_Fiss_7857_7868_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void Post_CollapsedDataParallel_2_7627() {
	Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_7796Post_CollapsedDataParallel_2_7627), &(SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_child1_7709_7867_join[0]));
}

void Pre_CollapsedDataParallel_1_7629() {
	Pre_CollapsedDataParallel_1(&(SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_child1_7709_7867_split[1]), &(Pre_CollapsedDataParallel_1_7629WEIGHTED_ROUND_ROBIN_Splitter_7801));
}

void Butterfly_7803() {
	Butterfly(&(SplitJoin95_Butterfly_Fiss_7858_7869_split[0]), &(SplitJoin95_Butterfly_Fiss_7858_7869_join[0]));
}

void Butterfly_7804() {
	Butterfly(&(SplitJoin95_Butterfly_Fiss_7858_7869_split[1]), &(SplitJoin95_Butterfly_Fiss_7858_7869_join[1]));
}

void Butterfly_7805() {
	Butterfly(&(SplitJoin95_Butterfly_Fiss_7858_7869_split[2]), &(SplitJoin95_Butterfly_Fiss_7858_7869_join[2]));
}

void Butterfly_7806() {
	Butterfly(&(SplitJoin95_Butterfly_Fiss_7858_7869_split[3]), &(SplitJoin95_Butterfly_Fiss_7858_7869_join[3]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7801() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_float(&SplitJoin95_Butterfly_Fiss_7858_7869_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_7629WEIGHTED_ROUND_ROBIN_Splitter_7801));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7802() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7802Post_CollapsedDataParallel_2_7630, pop_float(&SplitJoin95_Butterfly_Fiss_7858_7869_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void Post_CollapsedDataParallel_2_7630() {
	Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_7802Post_CollapsedDataParallel_2_7630), &(SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_child1_7709_7867_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7737() {
	FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
		push_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_child1_7709_7867_split[0], pop_float(&Post_CollapsedDataParallel_2_7618WEIGHTED_ROUND_ROBIN_Splitter_7737));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
		push_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_child1_7709_7867_split[1], pop_float(&Post_CollapsedDataParallel_2_7618WEIGHTED_ROUND_ROBIN_Splitter_7737));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7738() {
	FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
		push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_7268_7663_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_Hier_7840_7861_join[1], pop_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_child1_7709_7867_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
		push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_7268_7663_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_Hier_7840_7861_join[1], pop_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_child1_7709_7867_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7655() {
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_7268_7663_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_Hier_7840_7861_split[0], pop_float(&Post_CollapsedDataParallel_2_7612WEIGHTED_ROUND_ROBIN_Splitter_7655));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_7268_7663_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_Hier_7840_7861_split[1], pop_float(&Post_CollapsedDataParallel_2_7612WEIGHTED_ROUND_ROBIN_Splitter_7655));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7739() {
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7739WEIGHTED_ROUND_ROBIN_Splitter_7740, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_7268_7663_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_Hier_7840_7861_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7739WEIGHTED_ROUND_ROBIN_Splitter_7740, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_7268_7663_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_Hier_7840_7861_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_7632() {
	Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_child0_7733_7871_split[0]), &(Pre_CollapsedDataParallel_1_7632WEIGHTED_ROUND_ROBIN_Splitter_7807));
}

void Butterfly_7809() {
	Butterfly(&(SplitJoin14_Butterfly_Fiss_7844_7872_split[0]), &(SplitJoin14_Butterfly_Fiss_7844_7872_join[0]));
}

void Butterfly_7810() {
	Butterfly(&(SplitJoin14_Butterfly_Fiss_7844_7872_split[1]), &(SplitJoin14_Butterfly_Fiss_7844_7872_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7807() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin14_Butterfly_Fiss_7844_7872_split[0], pop_float(&Pre_CollapsedDataParallel_1_7632WEIGHTED_ROUND_ROBIN_Splitter_7807));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin14_Butterfly_Fiss_7844_7872_split[1], pop_float(&Pre_CollapsedDataParallel_1_7632WEIGHTED_ROUND_ROBIN_Splitter_7807));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7808() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7808Post_CollapsedDataParallel_2_7633, pop_float(&SplitJoin14_Butterfly_Fiss_7844_7872_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7808Post_CollapsedDataParallel_2_7633, pop_float(&SplitJoin14_Butterfly_Fiss_7844_7872_join[1]));
	ENDFOR
}

void Post_CollapsedDataParallel_2_7633() {
	Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_7808Post_CollapsedDataParallel_2_7633), &(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_child0_7733_7871_join[0]));
}

void Pre_CollapsedDataParallel_1_7635() {
	Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_child0_7733_7871_split[1]), &(Pre_CollapsedDataParallel_1_7635WEIGHTED_ROUND_ROBIN_Splitter_7811));
}

void Butterfly_7813() {
	Butterfly(&(SplitJoin39_Butterfly_Fiss_7848_7873_split[0]), &(SplitJoin39_Butterfly_Fiss_7848_7873_join[0]));
}

void Butterfly_7814() {
	Butterfly(&(SplitJoin39_Butterfly_Fiss_7848_7873_split[1]), &(SplitJoin39_Butterfly_Fiss_7848_7873_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7811() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin39_Butterfly_Fiss_7848_7873_split[0], pop_float(&Pre_CollapsedDataParallel_1_7635WEIGHTED_ROUND_ROBIN_Splitter_7811));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin39_Butterfly_Fiss_7848_7873_split[1], pop_float(&Pre_CollapsedDataParallel_1_7635WEIGHTED_ROUND_ROBIN_Splitter_7811));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7812() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7812Post_CollapsedDataParallel_2_7636, pop_float(&SplitJoin39_Butterfly_Fiss_7848_7873_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7812Post_CollapsedDataParallel_2_7636, pop_float(&SplitJoin39_Butterfly_Fiss_7848_7873_join[1]));
	ENDFOR
}

void Post_CollapsedDataParallel_2_7636() {
	Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_7812Post_CollapsedDataParallel_2_7636), &(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_child0_7733_7871_join[1]));
}

void Pre_CollapsedDataParallel_1_7638() {
	Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_child0_7733_7871_split[2]), &(Pre_CollapsedDataParallel_1_7638WEIGHTED_ROUND_ROBIN_Splitter_7815));
}

void Butterfly_7817() {
	Butterfly(&(SplitJoin43_Butterfly_Fiss_7849_7874_split[0]), &(SplitJoin43_Butterfly_Fiss_7849_7874_join[0]));
}

void Butterfly_7818() {
	Butterfly(&(SplitJoin43_Butterfly_Fiss_7849_7874_split[1]), &(SplitJoin43_Butterfly_Fiss_7849_7874_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7815() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin43_Butterfly_Fiss_7849_7874_split[0], pop_float(&Pre_CollapsedDataParallel_1_7638WEIGHTED_ROUND_ROBIN_Splitter_7815));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin43_Butterfly_Fiss_7849_7874_split[1], pop_float(&Pre_CollapsedDataParallel_1_7638WEIGHTED_ROUND_ROBIN_Splitter_7815));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7816() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7816Post_CollapsedDataParallel_2_7639, pop_float(&SplitJoin43_Butterfly_Fiss_7849_7874_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7816Post_CollapsedDataParallel_2_7639, pop_float(&SplitJoin43_Butterfly_Fiss_7849_7874_join[1]));
	ENDFOR
}

void Post_CollapsedDataParallel_2_7639() {
	Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_7816Post_CollapsedDataParallel_2_7639), &(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_child0_7733_7871_join[2]));
}

void Pre_CollapsedDataParallel_1_7641() {
	Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_child0_7733_7871_split[3]), &(Pre_CollapsedDataParallel_1_7641WEIGHTED_ROUND_ROBIN_Splitter_7819));
}

void Butterfly_7821() {
	Butterfly(&(SplitJoin47_Butterfly_Fiss_7850_7875_split[0]), &(SplitJoin47_Butterfly_Fiss_7850_7875_join[0]));
}

void Butterfly_7822() {
	Butterfly(&(SplitJoin47_Butterfly_Fiss_7850_7875_split[1]), &(SplitJoin47_Butterfly_Fiss_7850_7875_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7819() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin47_Butterfly_Fiss_7850_7875_split[0], pop_float(&Pre_CollapsedDataParallel_1_7641WEIGHTED_ROUND_ROBIN_Splitter_7819));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin47_Butterfly_Fiss_7850_7875_split[1], pop_float(&Pre_CollapsedDataParallel_1_7641WEIGHTED_ROUND_ROBIN_Splitter_7819));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7820() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7820Post_CollapsedDataParallel_2_7642, pop_float(&SplitJoin47_Butterfly_Fiss_7850_7875_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7820Post_CollapsedDataParallel_2_7642, pop_float(&SplitJoin47_Butterfly_Fiss_7850_7875_join[1]));
	ENDFOR
}

void Post_CollapsedDataParallel_2_7642() {
	Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_7820Post_CollapsedDataParallel_2_7642), &(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_child0_7733_7871_join[3]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7741() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_child0_7733_7871_split[__iter_dec_], pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_Hier_7843_7870_split[0]));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7742() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_Hier_7843_7870_join[0], pop_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_child0_7733_7871_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void Pre_CollapsedDataParallel_1_7644() {
	Pre_CollapsedDataParallel_1(&(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_child1_7734_7876_split[0]), &(Pre_CollapsedDataParallel_1_7644WEIGHTED_ROUND_ROBIN_Splitter_7823));
}

void Butterfly_7825() {
	Butterfly(&(SplitJoin53_Butterfly_Fiss_7851_7877_split[0]), &(SplitJoin53_Butterfly_Fiss_7851_7877_join[0]));
}

void Butterfly_7826() {
	Butterfly(&(SplitJoin53_Butterfly_Fiss_7851_7877_split[1]), &(SplitJoin53_Butterfly_Fiss_7851_7877_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7823() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin53_Butterfly_Fiss_7851_7877_split[0], pop_float(&Pre_CollapsedDataParallel_1_7644WEIGHTED_ROUND_ROBIN_Splitter_7823));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin53_Butterfly_Fiss_7851_7877_split[1], pop_float(&Pre_CollapsedDataParallel_1_7644WEIGHTED_ROUND_ROBIN_Splitter_7823));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7824() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7824Post_CollapsedDataParallel_2_7645, pop_float(&SplitJoin53_Butterfly_Fiss_7851_7877_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7824Post_CollapsedDataParallel_2_7645, pop_float(&SplitJoin53_Butterfly_Fiss_7851_7877_join[1]));
	ENDFOR
}

void Post_CollapsedDataParallel_2_7645() {
	Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_7824Post_CollapsedDataParallel_2_7645), &(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_child1_7734_7876_join[0]));
}

void Pre_CollapsedDataParallel_1_7647() {
	Pre_CollapsedDataParallel_1(&(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_child1_7734_7876_split[1]), &(Pre_CollapsedDataParallel_1_7647WEIGHTED_ROUND_ROBIN_Splitter_7827));
}

void Butterfly_7829() {
	Butterfly(&(SplitJoin57_Butterfly_Fiss_7852_7878_split[0]), &(SplitJoin57_Butterfly_Fiss_7852_7878_join[0]));
}

void Butterfly_7830() {
	Butterfly(&(SplitJoin57_Butterfly_Fiss_7852_7878_split[1]), &(SplitJoin57_Butterfly_Fiss_7852_7878_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7827() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin57_Butterfly_Fiss_7852_7878_split[0], pop_float(&Pre_CollapsedDataParallel_1_7647WEIGHTED_ROUND_ROBIN_Splitter_7827));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin57_Butterfly_Fiss_7852_7878_split[1], pop_float(&Pre_CollapsedDataParallel_1_7647WEIGHTED_ROUND_ROBIN_Splitter_7827));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7828() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7828Post_CollapsedDataParallel_2_7648, pop_float(&SplitJoin57_Butterfly_Fiss_7852_7878_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7828Post_CollapsedDataParallel_2_7648, pop_float(&SplitJoin57_Butterfly_Fiss_7852_7878_join[1]));
	ENDFOR
}

void Post_CollapsedDataParallel_2_7648() {
	Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_7828Post_CollapsedDataParallel_2_7648), &(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_child1_7734_7876_join[1]));
}

void Pre_CollapsedDataParallel_1_7650() {
	Pre_CollapsedDataParallel_1(&(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_child1_7734_7876_split[2]), &(Pre_CollapsedDataParallel_1_7650WEIGHTED_ROUND_ROBIN_Splitter_7831));
}

void Butterfly_7833() {
	Butterfly(&(SplitJoin61_Butterfly_Fiss_7853_7879_split[0]), &(SplitJoin61_Butterfly_Fiss_7853_7879_join[0]));
}

void Butterfly_7834() {
	Butterfly(&(SplitJoin61_Butterfly_Fiss_7853_7879_split[1]), &(SplitJoin61_Butterfly_Fiss_7853_7879_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7831() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin61_Butterfly_Fiss_7853_7879_split[0], pop_float(&Pre_CollapsedDataParallel_1_7650WEIGHTED_ROUND_ROBIN_Splitter_7831));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin61_Butterfly_Fiss_7853_7879_split[1], pop_float(&Pre_CollapsedDataParallel_1_7650WEIGHTED_ROUND_ROBIN_Splitter_7831));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7832() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7832Post_CollapsedDataParallel_2_7651, pop_float(&SplitJoin61_Butterfly_Fiss_7853_7879_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7832Post_CollapsedDataParallel_2_7651, pop_float(&SplitJoin61_Butterfly_Fiss_7853_7879_join[1]));
	ENDFOR
}

void Post_CollapsedDataParallel_2_7651() {
	Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_7832Post_CollapsedDataParallel_2_7651), &(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_child1_7734_7876_join[2]));
}

void Pre_CollapsedDataParallel_1_7653() {
	Pre_CollapsedDataParallel_1(&(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_child1_7734_7876_split[3]), &(Pre_CollapsedDataParallel_1_7653WEIGHTED_ROUND_ROBIN_Splitter_7835));
}

void Butterfly_7837() {
	Butterfly(&(SplitJoin65_Butterfly_Fiss_7854_7880_split[0]), &(SplitJoin65_Butterfly_Fiss_7854_7880_join[0]));
}

void Butterfly_7838() {
	Butterfly(&(SplitJoin65_Butterfly_Fiss_7854_7880_split[1]), &(SplitJoin65_Butterfly_Fiss_7854_7880_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7835() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin65_Butterfly_Fiss_7854_7880_split[0], pop_float(&Pre_CollapsedDataParallel_1_7653WEIGHTED_ROUND_ROBIN_Splitter_7835));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin65_Butterfly_Fiss_7854_7880_split[1], pop_float(&Pre_CollapsedDataParallel_1_7653WEIGHTED_ROUND_ROBIN_Splitter_7835));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7836() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7836Post_CollapsedDataParallel_2_7654, pop_float(&SplitJoin65_Butterfly_Fiss_7854_7880_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7836Post_CollapsedDataParallel_2_7654, pop_float(&SplitJoin65_Butterfly_Fiss_7854_7880_join[1]));
	ENDFOR
}

void Post_CollapsedDataParallel_2_7654() {
	Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_7836Post_CollapsedDataParallel_2_7654), &(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_child1_7734_7876_join[3]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7743() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_child1_7734_7876_split[__iter_dec_], pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_Hier_7843_7870_split[1]));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7744() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_Hier_7843_7870_join[1], pop_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_child1_7734_7876_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7740() {
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_Hier_7843_7870_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7739WEIGHTED_ROUND_ROBIN_Splitter_7740));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_Hier_7843_7870_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7739WEIGHTED_ROUND_ROBIN_Splitter_7740));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7745() {
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7745WEIGHTED_ROUND_ROBIN_Splitter_7746, pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_Hier_7843_7870_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7745WEIGHTED_ROUND_ROBIN_Splitter_7746, pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_Hier_7843_7870_join[1]));
	ENDFOR
}

void Butterfly_7375() {
	Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child0_7846_7882_split[0]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child0_7846_7882_join[0]));
}

void Butterfly_7376() {
	Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child0_7846_7882_split[1]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child0_7846_7882_join[1]));
}

void Butterfly_7377() {
	Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child0_7846_7882_split[2]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child0_7846_7882_join[2]));
}

void Butterfly_7378() {
	Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child0_7846_7882_split[3]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child0_7846_7882_join[3]));
}

void Butterfly_7379() {
	Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child0_7846_7882_split[4]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child0_7846_7882_join[4]));
}

void Butterfly_7380() {
	Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child0_7846_7882_split[5]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child0_7846_7882_join[5]));
}

void Butterfly_7381() {
	Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child0_7846_7882_split[6]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child0_7846_7882_join[6]));
}

void Butterfly_7382() {
	Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child0_7846_7882_split[7]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child0_7846_7882_join[7]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7747() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child0_7846_7882_split[__iter_dec_], pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_Hier_7845_7881_split[0]));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7748() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_Hier_7845_7881_join[0], pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child0_7846_7882_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void Butterfly_7383() {
	Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child1_7847_7883_split[0]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child1_7847_7883_join[0]));
}

void Butterfly_7384() {
	Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child1_7847_7883_split[1]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child1_7847_7883_join[1]));
}

void Butterfly_7385() {
	Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child1_7847_7883_split[2]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child1_7847_7883_join[2]));
}

void Butterfly_7386() {
	Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child1_7847_7883_split[3]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child1_7847_7883_join[3]));
}

void Butterfly_7387() {
	Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child1_7847_7883_split[4]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child1_7847_7883_join[4]));
}

void Butterfly_7388() {
	Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child1_7847_7883_split[5]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child1_7847_7883_join[5]));
}

void Butterfly_7389() {
	Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child1_7847_7883_split[6]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child1_7847_7883_join[6]));
}

void Butterfly_7390() {
	Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child1_7847_7883_split[7]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child1_7847_7883_join[7]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7749() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child1_7847_7883_split[__iter_dec_], pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_Hier_7845_7881_split[1]));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7750() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_Hier_7845_7881_join[1], pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child1_7847_7883_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7746() {
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_Hier_7845_7881_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7745WEIGHTED_ROUND_ROBIN_Splitter_7746));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_Hier_7845_7881_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7745WEIGHTED_ROUND_ROBIN_Splitter_7746));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7751() {
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7751BitReverse_7391, pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_Hier_7845_7881_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7751BitReverse_7391, pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_Hier_7845_7881_join[1]));
	ENDFOR
}

int BitReverse_7391_bitrev(int inp, int numbits) {
	int rev = 0;
	FOR(int, i, 0,  < , numbits, i++) {
		rev = ((rev * 2) | (inp & 1)) ; 
		inp = (inp / 2) ; 
	}
	ENDFOR
	return rev;
}
void BitReverse(buffer_float_t *chanin, buffer_float_t *chanout) {
	FOR(int, i__conflict__0, 0,  < , 32, i__conflict__0++) {
		int br = 0;
		br = BitReverse_7391_bitrev(i__conflict__0, 5) ; 
		push_float(&(*chanout), peek_float(&(*chanin), (2 * br))) ; 
		push_float(&(*chanout), peek_float(&(*chanin), ((2 * br) + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 32, i++) {
		pop_float(&(*chanin)) ; 
		pop_float(&(*chanin)) ; 
	}
	ENDFOR
}


void BitReverse_7391() {
	BitReverse(&(WEIGHTED_ROUND_ROBIN_Joiner_7751BitReverse_7391), &(BitReverse_7391FloatPrinter_7392));
}

void FloatPrinter(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void FloatPrinter_7392() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FloatPrinter(&(BitReverse_7391FloatPrinter_7392));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_7268_7663_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_Hier_7840_7861_join[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7739WEIGHTED_ROUND_ROBIN_Splitter_7740);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7745WEIGHTED_ROUND_ROBIN_Splitter_7746);
	init_buffer_float(&Pre_CollapsedDataParallel_1_7629WEIGHTED_ROUND_ROBIN_Splitter_7801);
	FOR(int, __iter_init_1_, 0, <, 2, __iter_init_1_++)
		init_buffer_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_child0_7704_7863_split[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7802Post_CollapsedDataParallel_2_7630);
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_float(&SplitJoin61_Butterfly_Fiss_7853_7879_join[__iter_init_2_]);
	ENDFOR
	init_buffer_float(&BitReverse_7391FloatPrinter_7392);
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_float(&SplitJoin0_Butterfly_Fiss_7839_7860_split[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7836Post_CollapsedDataParallel_2_7654);
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child1_7847_7883_join[__iter_init_4_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_7620WEIGHTED_ROUND_ROBIN_Splitter_7773);
	init_buffer_float(&Pre_CollapsedDataParallel_1_7644WEIGHTED_ROUND_ROBIN_Splitter_7823);
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin85_Butterfly_Fiss_7856_7866_split[__iter_init_5_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7832Post_CollapsedDataParallel_2_7651);
	FOR(int, __iter_init_6_, 0, <, 4, __iter_init_6_++)
		init_buffer_float(&SplitJoin8_Butterfly_Fiss_7842_7864_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 2, __iter_init_7_++)
		init_buffer_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_Hier_7843_7870_split[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_7611WEIGHTED_ROUND_ROBIN_Splitter_7753);
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_7268_7663_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_Hier_7840_7861_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 4, __iter_init_9_++)
		init_buffer_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_child0_7733_7871_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_float(&SplitJoin57_Butterfly_Fiss_7852_7878_join[__iter_init_10_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7780Post_CollapsedDataParallel_2_7624);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7820Post_CollapsedDataParallel_2_7642);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7828Post_CollapsedDataParallel_2_7648);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7824Post_CollapsedDataParallel_2_7645);
	FOR(int, __iter_init_11_, 0, <, 4, __iter_init_11_++)
		init_buffer_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_child0_7733_7871_split[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 2, __iter_init_12_++)
		init_buffer_float(&SplitJoin65_Butterfly_Fiss_7854_7880_split[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 8, __iter_init_13_++)
		init_buffer_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child0_7846_7882_join[__iter_init_13_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_7615WEIGHTED_ROUND_ROBIN_Splitter_7735);
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_float(&SplitJoin61_Butterfly_Fiss_7853_7879_split[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 2, __iter_init_15_++)
		init_buffer_float(&SplitJoin53_Butterfly_Fiss_7851_7877_join[__iter_init_15_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_7647WEIGHTED_ROUND_ROBIN_Splitter_7827);
	init_buffer_float(&Pre_CollapsedDataParallel_1_7614WEIGHTED_ROUND_ROBIN_Splitter_7763);
	init_buffer_float(&Pre_CollapsedDataParallel_1_7650WEIGHTED_ROUND_ROBIN_Splitter_7831);
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_child0_7704_7863_join[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 4, __iter_init_17_++)
		init_buffer_float(&SplitJoin89_Butterfly_Fiss_7857_7868_split[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 2, __iter_init_18_++)
		init_buffer_float(&SplitJoin39_Butterfly_Fiss_7848_7873_join[__iter_init_18_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7808Post_CollapsedDataParallel_2_7633);
	FOR(int, __iter_init_19_, 0, <, 2, __iter_init_19_++)
		init_buffer_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_Hier_7845_7881_join[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 8, __iter_init_20_++)
		init_buffer_float(&SplitJoin4_Butterfly_Fiss_7841_7862_split[__iter_init_20_]);
	ENDFOR
	init_buffer_float(&FloatSource_7310Pre_CollapsedDataParallel_1_7611);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7774Post_CollapsedDataParallel_2_7621);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7786Post_CollapsedDataParallel_2_7618);
	init_buffer_float(&Pre_CollapsedDataParallel_1_7635WEIGHTED_ROUND_ROBIN_Splitter_7811);
	FOR(int, __iter_init_21_, 0, <, 2, __iter_init_21_++)
		init_buffer_float(&SplitJoin47_Butterfly_Fiss_7850_7875_split[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 2, __iter_init_22_++)
		init_buffer_float(&SplitJoin43_Butterfly_Fiss_7849_7874_split[__iter_init_22_]);
	ENDFOR
	FOR(int, __iter_init_23_, 0, <, 4, __iter_init_23_++)
		init_buffer_float(&SplitJoin8_Butterfly_Fiss_7842_7864_join[__iter_init_23_]);
	ENDFOR
	FOR(int, __iter_init_24_, 0, <, 8, __iter_init_24_++)
		init_buffer_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child0_7846_7882_split[__iter_init_24_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_7617WEIGHTED_ROUND_ROBIN_Splitter_7785);
	FOR(int, __iter_init_25_, 0, <, 2, __iter_init_25_++)
		init_buffer_float(&SplitJoin43_Butterfly_Fiss_7849_7874_join[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 8, __iter_init_26_++)
		init_buffer_float(&SplitJoin0_Butterfly_Fiss_7839_7860_join[__iter_init_26_]);
	ENDFOR
	FOR(int, __iter_init_27_, 0, <, 8, __iter_init_27_++)
		init_buffer_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_child1_7847_7883_split[__iter_init_27_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_7618WEIGHTED_ROUND_ROBIN_Splitter_7737);
	FOR(int, __iter_init_28_, 0, <, 4, __iter_init_28_++)
		init_buffer_float(&SplitJoin72_Butterfly_Fiss_7855_7865_split[__iter_init_28_]);
	ENDFOR
	FOR(int, __iter_init_29_, 0, <, 4, __iter_init_29_++)
		init_buffer_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_child1_7734_7876_split[__iter_init_29_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_7632WEIGHTED_ROUND_ROBIN_Splitter_7807);
	FOR(int, __iter_init_30_, 0, <, 8, __iter_init_30_++)
		init_buffer_float(&SplitJoin4_Butterfly_Fiss_7841_7862_join[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 4, __iter_init_31_++)
		init_buffer_float(&SplitJoin95_Butterfly_Fiss_7858_7869_join[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 2, __iter_init_32_++)
		init_buffer_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_Hier_7843_7870_join[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 2, __iter_init_33_++)
		init_buffer_float(&SplitJoin14_Butterfly_Fiss_7844_7872_split[__iter_init_33_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_7626WEIGHTED_ROUND_ROBIN_Splitter_7795);
	init_buffer_float(&Post_CollapsedDataParallel_2_7612WEIGHTED_ROUND_ROBIN_Splitter_7655);
	FOR(int, __iter_init_34_, 0, <, 2, __iter_init_34_++)
		init_buffer_float(&SplitJoin14_Butterfly_Fiss_7844_7872_join[__iter_init_34_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7812Post_CollapsedDataParallel_2_7636);
	init_buffer_float(&Pre_CollapsedDataParallel_1_7638WEIGHTED_ROUND_ROBIN_Splitter_7815);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7816Post_CollapsedDataParallel_2_7639);
	FOR(int, __iter_init_35_, 0, <, 2, __iter_init_35_++)
		init_buffer_float(&SplitJoin65_Butterfly_Fiss_7854_7880_join[__iter_init_35_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_7623WEIGHTED_ROUND_ROBIN_Splitter_7779);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7754Post_CollapsedDataParallel_2_7612);
	FOR(int, __iter_init_36_, 0, <, 2, __iter_init_36_++)
		init_buffer_float(&SplitJoin47_Butterfly_Fiss_7850_7875_join[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 2, __iter_init_37_++)
		init_buffer_float(&SplitJoin53_Butterfly_Fiss_7851_7877_split[__iter_init_37_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7764Post_CollapsedDataParallel_2_7615);
	FOR(int, __iter_init_38_, 0, <, 2, __iter_init_38_++)
		init_buffer_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_child1_7709_7867_join[__iter_init_38_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_7653WEIGHTED_ROUND_ROBIN_Splitter_7835);
	FOR(int, __iter_init_39_, 0, <, 2, __iter_init_39_++)
		init_buffer_float(&SplitJoin57_Butterfly_Fiss_7852_7878_split[__iter_init_39_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7796Post_CollapsedDataParallel_2_7627);
	FOR(int, __iter_init_40_, 0, <, 4, __iter_init_40_++)
		init_buffer_float(&SplitJoin95_Butterfly_Fiss_7858_7869_split[__iter_init_40_]);
	ENDFOR
	FOR(int, __iter_init_41_, 0, <, 2, __iter_init_41_++)
		init_buffer_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_7308_7669_Hier_Hier_7845_7881_split[__iter_init_41_]);
	ENDFOR
	FOR(int, __iter_init_42_, 0, <, 4, __iter_init_42_++)
		init_buffer_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_7290_7667_Hier_child1_7734_7876_join[__iter_init_42_]);
	ENDFOR
	FOR(int, __iter_init_43_, 0, <, 4, __iter_init_43_++)
		init_buffer_float(&SplitJoin72_Butterfly_Fiss_7855_7865_join[__iter_init_43_]);
	ENDFOR
	FOR(int, __iter_init_44_, 0, <, 4, __iter_init_44_++)
		init_buffer_float(&SplitJoin89_Butterfly_Fiss_7857_7868_join[__iter_init_44_]);
	ENDFOR
	FOR(int, __iter_init_45_, 0, <, 8, __iter_init_45_++)
		init_buffer_float(&SplitJoin85_Butterfly_Fiss_7856_7866_join[__iter_init_45_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7751BitReverse_7391);
	FOR(int, __iter_init_46_, 0, <, 2, __iter_init_46_++)
		init_buffer_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_7277_7665_child1_7709_7867_split[__iter_init_46_]);
	ENDFOR
	FOR(int, __iter_init_47_, 0, <, 2, __iter_init_47_++)
		init_buffer_float(&SplitJoin39_Butterfly_Fiss_7848_7873_split[__iter_init_47_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_7641WEIGHTED_ROUND_ROBIN_Splitter_7819);
// --- init: FloatSource_7310
	 {
	FOR(int, i, 0,  < , 32, i++) {
		FloatSource_7310_s.A_re[i] = 0.0 ; 
		FloatSource_7310_s.A_im[i] = 0.0 ; 
	}
	ENDFOR
	FloatSource_7310_s.A_re[1] = 1.0 ; 
	FloatSource_7310_s.idx = 0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FloatSource_7310();
		Pre_CollapsedDataParallel_1_7611();
		WEIGHTED_ROUND_ROBIN_Splitter_7753();
			Butterfly_7755();
			Butterfly_7756();
			Butterfly_7757();
			Butterfly_7758();
			Butterfly_7759();
			Butterfly_7760();
			Butterfly_7761();
			Butterfly_7762();
		WEIGHTED_ROUND_ROBIN_Joiner_7754();
		Post_CollapsedDataParallel_2_7612();
		WEIGHTED_ROUND_ROBIN_Splitter_7655();
			Pre_CollapsedDataParallel_1_7614();
			WEIGHTED_ROUND_ROBIN_Splitter_7763();
				Butterfly_7765();
				Butterfly_7766();
				Butterfly_7767();
				Butterfly_7768();
				Butterfly_7769();
				Butterfly_7770();
				Butterfly_7771();
				Butterfly_7772();
			WEIGHTED_ROUND_ROBIN_Joiner_7764();
			Post_CollapsedDataParallel_2_7615();
			WEIGHTED_ROUND_ROBIN_Splitter_7735();
				Pre_CollapsedDataParallel_1_7620();
				WEIGHTED_ROUND_ROBIN_Splitter_7773();
					Butterfly_7775();
					Butterfly_7776();
					Butterfly_7777();
					Butterfly_7778();
				WEIGHTED_ROUND_ROBIN_Joiner_7774();
				Post_CollapsedDataParallel_2_7621();
				Pre_CollapsedDataParallel_1_7623();
				WEIGHTED_ROUND_ROBIN_Splitter_7779();
					Butterfly_7781();
					Butterfly_7782();
					Butterfly_7783();
					Butterfly_7784();
				WEIGHTED_ROUND_ROBIN_Joiner_7780();
				Post_CollapsedDataParallel_2_7624();
			WEIGHTED_ROUND_ROBIN_Joiner_7736();
			Pre_CollapsedDataParallel_1_7617();
			WEIGHTED_ROUND_ROBIN_Splitter_7785();
				Butterfly_7787();
				Butterfly_7788();
				Butterfly_7789();
				Butterfly_7790();
				Butterfly_7791();
				Butterfly_7792();
				Butterfly_7793();
				Butterfly_7794();
			WEIGHTED_ROUND_ROBIN_Joiner_7786();
			Post_CollapsedDataParallel_2_7618();
			WEIGHTED_ROUND_ROBIN_Splitter_7737();
				Pre_CollapsedDataParallel_1_7626();
				WEIGHTED_ROUND_ROBIN_Splitter_7795();
					Butterfly_7797();
					Butterfly_7798();
					Butterfly_7799();
					Butterfly_7800();
				WEIGHTED_ROUND_ROBIN_Joiner_7796();
				Post_CollapsedDataParallel_2_7627();
				Pre_CollapsedDataParallel_1_7629();
				WEIGHTED_ROUND_ROBIN_Splitter_7801();
					Butterfly_7803();
					Butterfly_7804();
					Butterfly_7805();
					Butterfly_7806();
				WEIGHTED_ROUND_ROBIN_Joiner_7802();
				Post_CollapsedDataParallel_2_7630();
			WEIGHTED_ROUND_ROBIN_Joiner_7738();
		WEIGHTED_ROUND_ROBIN_Joiner_7739();
		WEIGHTED_ROUND_ROBIN_Splitter_7740();
			WEIGHTED_ROUND_ROBIN_Splitter_7741();
				Pre_CollapsedDataParallel_1_7632();
				WEIGHTED_ROUND_ROBIN_Splitter_7807();
					Butterfly_7809();
					Butterfly_7810();
				WEIGHTED_ROUND_ROBIN_Joiner_7808();
				Post_CollapsedDataParallel_2_7633();
				Pre_CollapsedDataParallel_1_7635();
				WEIGHTED_ROUND_ROBIN_Splitter_7811();
					Butterfly_7813();
					Butterfly_7814();
				WEIGHTED_ROUND_ROBIN_Joiner_7812();
				Post_CollapsedDataParallel_2_7636();
				Pre_CollapsedDataParallel_1_7638();
				WEIGHTED_ROUND_ROBIN_Splitter_7815();
					Butterfly_7817();
					Butterfly_7818();
				WEIGHTED_ROUND_ROBIN_Joiner_7816();
				Post_CollapsedDataParallel_2_7639();
				Pre_CollapsedDataParallel_1_7641();
				WEIGHTED_ROUND_ROBIN_Splitter_7819();
					Butterfly_7821();
					Butterfly_7822();
				WEIGHTED_ROUND_ROBIN_Joiner_7820();
				Post_CollapsedDataParallel_2_7642();
			WEIGHTED_ROUND_ROBIN_Joiner_7742();
			WEIGHTED_ROUND_ROBIN_Splitter_7743();
				Pre_CollapsedDataParallel_1_7644();
				WEIGHTED_ROUND_ROBIN_Splitter_7823();
					Butterfly_7825();
					Butterfly_7826();
				WEIGHTED_ROUND_ROBIN_Joiner_7824();
				Post_CollapsedDataParallel_2_7645();
				Pre_CollapsedDataParallel_1_7647();
				WEIGHTED_ROUND_ROBIN_Splitter_7827();
					Butterfly_7829();
					Butterfly_7830();
				WEIGHTED_ROUND_ROBIN_Joiner_7828();
				Post_CollapsedDataParallel_2_7648();
				Pre_CollapsedDataParallel_1_7650();
				WEIGHTED_ROUND_ROBIN_Splitter_7831();
					Butterfly_7833();
					Butterfly_7834();
				WEIGHTED_ROUND_ROBIN_Joiner_7832();
				Post_CollapsedDataParallel_2_7651();
				Pre_CollapsedDataParallel_1_7653();
				WEIGHTED_ROUND_ROBIN_Splitter_7835();
					Butterfly_7837();
					Butterfly_7838();
				WEIGHTED_ROUND_ROBIN_Joiner_7836();
				Post_CollapsedDataParallel_2_7654();
			WEIGHTED_ROUND_ROBIN_Joiner_7744();
		WEIGHTED_ROUND_ROBIN_Joiner_7745();
		WEIGHTED_ROUND_ROBIN_Splitter_7746();
			WEIGHTED_ROUND_ROBIN_Splitter_7747();
				Butterfly_7375();
				Butterfly_7376();
				Butterfly_7377();
				Butterfly_7378();
				Butterfly_7379();
				Butterfly_7380();
				Butterfly_7381();
				Butterfly_7382();
			WEIGHTED_ROUND_ROBIN_Joiner_7748();
			WEIGHTED_ROUND_ROBIN_Splitter_7749();
				Butterfly_7383();
				Butterfly_7384();
				Butterfly_7385();
				Butterfly_7386();
				Butterfly_7387();
				Butterfly_7388();
				Butterfly_7389();
				Butterfly_7390();
			WEIGHTED_ROUND_ROBIN_Joiner_7750();
		WEIGHTED_ROUND_ROBIN_Joiner_7751();
		BitReverse_7391();
		FloatPrinter_7392();
	ENDFOR
	return EXIT_SUCCESS;
}
