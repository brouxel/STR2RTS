#include "PEG11-FFT5_nocache.h"

buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4785WEIGHTED_ROUND_ROBIN_Splitter_4786;
buffer_complex_t SplitJoin57_SplitJoin37_SplitJoin37_split2_4445_4712_4775_4854_join[2];
buffer_complex_t SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_4464_4687_4813_4833_split[2];
buffer_complex_t Pre_CollapsedDataParallel_1_4613butterfly_4520;
buffer_complex_t SplitJoin75_SplitJoin53_SplitJoin53_AnonFilter_a0_4476_4726_4819_4835_split[2];
buffer_complex_t butterfly_4527Post_CollapsedDataParallel_2_4635;
buffer_complex_t SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_4464_4687_4813_4833_join[2];
buffer_complex_t butterfly_4526Post_CollapsedDataParallel_2_4632;
buffer_complex_t SplitJoin20_SplitJoin14_SplitJoin14_split1_4450_4694_4816_4855_split[2];
buffer_complex_t SplitJoin51_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child1_4779_4852_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_4634butterfly_4527;
buffer_complex_t SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_4462_4686_4812_4832_join[2];
buffer_complex_t SplitJoin89_SplitJoin67_SplitJoin67_AnonFilter_a0_4494_4735_4823_4840_split[2];
buffer_complex_t Pre_CollapsedDataParallel_1_4625butterfly_4524;
buffer_complex_t SplitJoin16_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child0_4777_4849_join[2];
buffer_complex_t SplitJoin40_SplitJoin22_SplitJoin22_split2_4454_4700_4778_4857_join[4];
buffer_complex_t SplitJoin10_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_Hier_4814_4845_split[2];
buffer_complex_t SplitJoin97_SplitJoin75_SplitJoin75_AnonFilter_a0_4506_4741_4825_4842_join[2];
buffer_complex_t SplitJoin14_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_Hier_4815_4848_join[2];
buffer_complex_t SplitJoin53_SplitJoin33_SplitJoin33_split2_4443_4709_4773_4853_join[2];
buffer_complex_t SplitJoin93_SplitJoin71_SplitJoin71_AnonFilter_a0_4500_4738_4824_4841_join[2];
buffer_complex_t SplitJoin51_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child1_4779_4852_split[2];
buffer_complex_t SplitJoin89_SplitJoin67_SplitJoin67_AnonFilter_a0_4494_4735_4823_4840_join[2];
buffer_complex_t SplitJoin0_source_Fiss_4810_4829_join[2];
buffer_complex_t SplitJoin14_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_Hier_4815_4848_split[2];
buffer_complex_t SplitJoin40_SplitJoin22_SplitJoin22_split2_4454_4700_4778_4857_split[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4794WEIGHTED_ROUND_ROBIN_Splitter_4636;
buffer_complex_t SplitJoin71_SplitJoin49_SplitJoin49_AnonFilter_a0_4470_4723_4818_4834_join[2];
buffer_complex_t SplitJoin85_SplitJoin63_SplitJoin63_AnonFilter_a0_4490_4733_4769_4838_join[2];
buffer_complex_t SplitJoin22_SplitJoin16_SplitJoin16_split2_4452_4695_4776_4856_join[4];
buffer_complex_t SplitJoin16_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child0_4777_4849_split[2];
buffer_complex_t SplitJoin81_SplitJoin59_SplitJoin59_AnonFilter_a0_4484_4730_4821_4837_join[2];
buffer_complex_t SplitJoin20_SplitJoin14_SplitJoin14_split1_4450_4694_4816_4855_join[2];
buffer_complex_t SplitJoin81_SplitJoin59_SplitJoin59_AnonFilter_a0_4484_4730_4821_4837_split[2];
buffer_complex_t SplitJoin99_SplitJoin77_SplitJoin77_AnonFilter_a0_4508_4742_4826_4843_split[2];
buffer_complex_t SplitJoin47_SplitJoin29_SplitJoin29_split2_4441_4706_4772_4851_split[2];
buffer_complex_t butterfly_4520Post_CollapsedDataParallel_2_4614;
buffer_complex_t Pre_CollapsedDataParallel_1_4622butterfly_4523;
buffer_complex_t SplitJoin18_SplitJoin12_SplitJoin12_split2_4439_4692_4770_4850_split[2];
buffer_complex_t SplitJoin75_SplitJoin53_SplitJoin53_AnonFilter_a0_4476_4726_4819_4835_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_4616butterfly_4521;
buffer_complex_t SplitJoin87_SplitJoin65_SplitJoin65_AnonFilter_a0_4492_4734_4822_4839_join[2];
buffer_complex_t SplitJoin12_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child0_4771_4846_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4798sink_4547;
buffer_complex_t SplitJoin64_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child1_4774_4847_join[4];
buffer_float_t SplitJoin24_magnitude_Fiss_4817_4858_join[11];
buffer_complex_t SplitJoin93_SplitJoin71_SplitJoin71_AnonFilter_a0_4500_4738_4824_4841_split[2];
buffer_complex_t SplitJoin22_SplitJoin16_SplitJoin16_split2_4452_4695_4776_4856_split[4];
buffer_complex_t SplitJoin87_SplitJoin65_SplitJoin65_AnonFilter_a0_4492_4734_4822_4839_split[2];
buffer_complex_t Pre_CollapsedDataParallel_1_4628butterfly_4525;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4679WEIGHTED_ROUND_ROBIN_Splitter_4797;
buffer_complex_t SplitJoin12_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child0_4771_4846_split[4];
buffer_complex_t SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_4460_4685_4768_4831_join[2];
buffer_complex_t SplitJoin103_SplitJoin81_SplitJoin81_AnonFilter_a0_4514_4745_4827_4844_join[2];
buffer_complex_t SplitJoin97_SplitJoin75_SplitJoin75_AnonFilter_a0_4506_4741_4825_4842_split[2];
buffer_complex_t SplitJoin0_source_Fiss_4810_4829_split[2];
buffer_complex_t butterfly_4522Post_CollapsedDataParallel_2_4620;
buffer_complex_t SplitJoin64_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child1_4774_4847_split[4];
buffer_complex_t Pre_CollapsedDataParallel_1_4631butterfly_4526;
buffer_complex_t SplitJoin99_SplitJoin77_SplitJoin77_AnonFilter_a0_4508_4742_4826_4843_join[2];
buffer_complex_t butterfly_4523Post_CollapsedDataParallel_2_4623;
buffer_complex_t SplitJoin47_SplitJoin29_SplitJoin29_split2_4441_4706_4772_4851_join[2];
buffer_complex_t butterfly_4524Post_CollapsedDataParallel_2_4626;
buffer_complex_t butterfly_4525Post_CollapsedDataParallel_2_4629;
buffer_complex_t SplitJoin77_SplitJoin55_SplitJoin55_AnonFilter_a0_4478_4727_4820_4836_join[2];
buffer_complex_t SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_4458_4684_4811_4830_join[2];
buffer_complex_t SplitJoin103_SplitJoin81_SplitJoin81_AnonFilter_a0_4514_4745_4827_4844_split[2];
buffer_complex_t Pre_CollapsedDataParallel_1_4619butterfly_4522;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4791WEIGHTED_ROUND_ROBIN_Splitter_4678;
buffer_complex_t SplitJoin57_SplitJoin37_SplitJoin37_split2_4445_4712_4775_4854_split[2];
buffer_complex_t SplitJoin77_SplitJoin55_SplitJoin55_AnonFilter_a0_4478_4727_4820_4836_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4637WEIGHTED_ROUND_ROBIN_Splitter_4780;
buffer_complex_t SplitJoin24_magnitude_Fiss_4817_4858_split[11];
buffer_complex_t SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_4458_4684_4811_4830_split[2];
buffer_complex_t SplitJoin53_SplitJoin33_SplitJoin33_split2_4443_4709_4773_4853_split[2];
buffer_complex_t SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_4460_4685_4768_4831_split[2];
buffer_complex_t SplitJoin71_SplitJoin49_SplitJoin49_AnonFilter_a0_4470_4723_4818_4834_split[2];
buffer_complex_t SplitJoin18_SplitJoin12_SplitJoin12_split2_4439_4692_4770_4850_join[2];
buffer_complex_t SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_4462_4686_4812_4832_split[2];
buffer_complex_t SplitJoin10_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_Hier_4814_4845_join[2];
buffer_complex_t SplitJoin85_SplitJoin63_SplitJoin63_AnonFilter_a0_4490_4733_4769_4838_split[2];
buffer_complex_t butterfly_4521Post_CollapsedDataParallel_2_4617;



void source_4795(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t t;
		t.imag = 0.0 ; 
		t.real = 0.9501 ; 
		push_complex(&SplitJoin0_source_Fiss_4810_4829_join[0], t) ; 
		t.real = 0.2311 ; 
		push_complex(&SplitJoin0_source_Fiss_4810_4829_join[0], t) ; 
		t.real = 0.6068 ; 
		push_complex(&SplitJoin0_source_Fiss_4810_4829_join[0], t) ; 
		t.real = 0.486 ; 
		push_complex(&SplitJoin0_source_Fiss_4810_4829_join[0], t) ; 
		t.real = 0.8913 ; 
		push_complex(&SplitJoin0_source_Fiss_4810_4829_join[0], t) ; 
		t.real = 0.7621 ; 
		push_complex(&SplitJoin0_source_Fiss_4810_4829_join[0], t) ; 
		t.real = 0.4565 ; 
		push_complex(&SplitJoin0_source_Fiss_4810_4829_join[0], t) ; 
		t.real = 0.0185 ; 
		push_complex(&SplitJoin0_source_Fiss_4810_4829_join[0], t) ; 
	}
	ENDFOR
}

void source_4796(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t t;
		t.imag = 0.0 ; 
		t.real = 0.9501 ; 
		push_complex(&SplitJoin0_source_Fiss_4810_4829_join[1], t) ; 
		t.real = 0.2311 ; 
		push_complex(&SplitJoin0_source_Fiss_4810_4829_join[1], t) ; 
		t.real = 0.6068 ; 
		push_complex(&SplitJoin0_source_Fiss_4810_4829_join[1], t) ; 
		t.real = 0.486 ; 
		push_complex(&SplitJoin0_source_Fiss_4810_4829_join[1], t) ; 
		t.real = 0.8913 ; 
		push_complex(&SplitJoin0_source_Fiss_4810_4829_join[1], t) ; 
		t.real = 0.7621 ; 
		push_complex(&SplitJoin0_source_Fiss_4810_4829_join[1], t) ; 
		t.real = 0.4565 ; 
		push_complex(&SplitJoin0_source_Fiss_4810_4829_join[1], t) ; 
		t.real = 0.0185 ; 
		push_complex(&SplitJoin0_source_Fiss_4810_4829_join[1], t) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4793() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_4794() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4794WEIGHTED_ROUND_ROBIN_Splitter_4636, pop_complex(&SplitJoin0_source_Fiss_4810_4829_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4794WEIGHTED_ROUND_ROBIN_Splitter_4636, pop_complex(&SplitJoin0_source_Fiss_4810_4829_join[1]));
		ENDFOR
	ENDFOR
}}

void Identity_4466(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t __tmp1246 = pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_4464_4687_4813_4833_split[0]);
		push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_4464_4687_4813_4833_join[0], __tmp1246) ; 
	}
	ENDFOR
}

void Identity_4468(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t __tmp1249 = pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_4464_4687_4813_4833_split[1]);
		push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_4464_4687_4813_4833_join[1], __tmp1249) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4642() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_4464_4687_4813_4833_split[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_4462_4686_4812_4832_split[0]));
		push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_4464_4687_4813_4833_split[1], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_4462_4686_4812_4832_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4643() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_4462_4686_4812_4832_join[0], pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_4464_4687_4813_4833_join[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_4462_4686_4812_4832_join[0], pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_4464_4687_4813_4833_join[1]));
	ENDFOR
}}

void Identity_4472(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t __tmp1257 = pop_complex(&SplitJoin71_SplitJoin49_SplitJoin49_AnonFilter_a0_4470_4723_4818_4834_split[0]);
		push_complex(&SplitJoin71_SplitJoin49_SplitJoin49_AnonFilter_a0_4470_4723_4818_4834_join[0], __tmp1257) ; 
	}
	ENDFOR
}

void Identity_4474(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t __tmp1260 = pop_complex(&SplitJoin71_SplitJoin49_SplitJoin49_AnonFilter_a0_4470_4723_4818_4834_split[1]);
		push_complex(&SplitJoin71_SplitJoin49_SplitJoin49_AnonFilter_a0_4470_4723_4818_4834_join[1], __tmp1260) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4644() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		push_complex(&SplitJoin71_SplitJoin49_SplitJoin49_AnonFilter_a0_4470_4723_4818_4834_split[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_4462_4686_4812_4832_split[1]));
		push_complex(&SplitJoin71_SplitJoin49_SplitJoin49_AnonFilter_a0_4470_4723_4818_4834_split[1], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_4462_4686_4812_4832_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4645() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_4462_4686_4812_4832_join[1], pop_complex(&SplitJoin71_SplitJoin49_SplitJoin49_AnonFilter_a0_4470_4723_4818_4834_join[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_4462_4686_4812_4832_join[1], pop_complex(&SplitJoin71_SplitJoin49_SplitJoin49_AnonFilter_a0_4470_4723_4818_4834_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_4640() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 22, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_4462_4686_4812_4832_split[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_4460_4685_4768_4831_split[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_4462_4686_4812_4832_split[1], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_4460_4685_4768_4831_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4641() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_4460_4685_4768_4831_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_4462_4686_4812_4832_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_4460_4685_4768_4831_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_4462_4686_4812_4832_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_4460_4685_4768_4831_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_4462_4686_4812_4832_join[1]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_4460_4685_4768_4831_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_4462_4686_4812_4832_join[1]));
	ENDFOR
}}

void Identity_4480(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t __tmp1273 = pop_complex(&SplitJoin77_SplitJoin55_SplitJoin55_AnonFilter_a0_4478_4727_4820_4836_split[0]);
		push_complex(&SplitJoin77_SplitJoin55_SplitJoin55_AnonFilter_a0_4478_4727_4820_4836_join[0], __tmp1273) ; 
	}
	ENDFOR
}

void Identity_4482(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t __tmp1276 = pop_complex(&SplitJoin77_SplitJoin55_SplitJoin55_AnonFilter_a0_4478_4727_4820_4836_split[1]);
		push_complex(&SplitJoin77_SplitJoin55_SplitJoin55_AnonFilter_a0_4478_4727_4820_4836_join[1], __tmp1276) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4648() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		push_complex(&SplitJoin77_SplitJoin55_SplitJoin55_AnonFilter_a0_4478_4727_4820_4836_split[0], pop_complex(&SplitJoin75_SplitJoin53_SplitJoin53_AnonFilter_a0_4476_4726_4819_4835_split[0]));
		push_complex(&SplitJoin77_SplitJoin55_SplitJoin55_AnonFilter_a0_4478_4727_4820_4836_split[1], pop_complex(&SplitJoin75_SplitJoin53_SplitJoin53_AnonFilter_a0_4476_4726_4819_4835_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4649() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		push_complex(&SplitJoin75_SplitJoin53_SplitJoin53_AnonFilter_a0_4476_4726_4819_4835_join[0], pop_complex(&SplitJoin77_SplitJoin55_SplitJoin55_AnonFilter_a0_4478_4727_4820_4836_join[0]));
		push_complex(&SplitJoin75_SplitJoin53_SplitJoin53_AnonFilter_a0_4476_4726_4819_4835_join[0], pop_complex(&SplitJoin77_SplitJoin55_SplitJoin55_AnonFilter_a0_4478_4727_4820_4836_join[1]));
	ENDFOR
}}

void Identity_4486(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t __tmp1284 = pop_complex(&SplitJoin81_SplitJoin59_SplitJoin59_AnonFilter_a0_4484_4730_4821_4837_split[0]);
		push_complex(&SplitJoin81_SplitJoin59_SplitJoin59_AnonFilter_a0_4484_4730_4821_4837_join[0], __tmp1284) ; 
	}
	ENDFOR
}

void Identity_4488(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t __tmp1287 = pop_complex(&SplitJoin81_SplitJoin59_SplitJoin59_AnonFilter_a0_4484_4730_4821_4837_split[1]);
		push_complex(&SplitJoin81_SplitJoin59_SplitJoin59_AnonFilter_a0_4484_4730_4821_4837_join[1], __tmp1287) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4650() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		push_complex(&SplitJoin81_SplitJoin59_SplitJoin59_AnonFilter_a0_4484_4730_4821_4837_split[0], pop_complex(&SplitJoin75_SplitJoin53_SplitJoin53_AnonFilter_a0_4476_4726_4819_4835_split[1]));
		push_complex(&SplitJoin81_SplitJoin59_SplitJoin59_AnonFilter_a0_4484_4730_4821_4837_split[1], pop_complex(&SplitJoin75_SplitJoin53_SplitJoin53_AnonFilter_a0_4476_4726_4819_4835_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4651() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		push_complex(&SplitJoin75_SplitJoin53_SplitJoin53_AnonFilter_a0_4476_4726_4819_4835_join[1], pop_complex(&SplitJoin81_SplitJoin59_SplitJoin59_AnonFilter_a0_4484_4730_4821_4837_join[0]));
		push_complex(&SplitJoin75_SplitJoin53_SplitJoin53_AnonFilter_a0_4476_4726_4819_4835_join[1], pop_complex(&SplitJoin81_SplitJoin59_SplitJoin59_AnonFilter_a0_4484_4730_4821_4837_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_4646() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 22, __iter_steady_++)
		push_complex(&SplitJoin75_SplitJoin53_SplitJoin53_AnonFilter_a0_4476_4726_4819_4835_split[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_4460_4685_4768_4831_split[1]));
		push_complex(&SplitJoin75_SplitJoin53_SplitJoin53_AnonFilter_a0_4476_4726_4819_4835_split[1], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_4460_4685_4768_4831_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4647() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_4460_4685_4768_4831_join[1], pop_complex(&SplitJoin75_SplitJoin53_SplitJoin53_AnonFilter_a0_4476_4726_4819_4835_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_4460_4685_4768_4831_join[1], pop_complex(&SplitJoin75_SplitJoin53_SplitJoin53_AnonFilter_a0_4476_4726_4819_4835_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_4460_4685_4768_4831_join[1], pop_complex(&SplitJoin75_SplitJoin53_SplitJoin53_AnonFilter_a0_4476_4726_4819_4835_join[1]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_4460_4685_4768_4831_join[1], pop_complex(&SplitJoin75_SplitJoin53_SplitJoin53_AnonFilter_a0_4476_4726_4819_4835_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_4638() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 44, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_4460_4685_4768_4831_split[0], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_4458_4684_4811_4830_split[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_4460_4685_4768_4831_split[1], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_4458_4684_4811_4830_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4639() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_4458_4684_4811_4830_join[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_4460_4685_4768_4831_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_4458_4684_4811_4830_join[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_4460_4685_4768_4831_join[1]));
		ENDFOR
	ENDFOR
}}

void Identity_4496(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t __tmp1305 = pop_complex(&SplitJoin89_SplitJoin67_SplitJoin67_AnonFilter_a0_4494_4735_4823_4840_split[0]);
		push_complex(&SplitJoin89_SplitJoin67_SplitJoin67_AnonFilter_a0_4494_4735_4823_4840_join[0], __tmp1305) ; 
	}
	ENDFOR
}

void Identity_4498(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t __tmp1308 = pop_complex(&SplitJoin89_SplitJoin67_SplitJoin67_AnonFilter_a0_4494_4735_4823_4840_split[1]);
		push_complex(&SplitJoin89_SplitJoin67_SplitJoin67_AnonFilter_a0_4494_4735_4823_4840_join[1], __tmp1308) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4656() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		push_complex(&SplitJoin89_SplitJoin67_SplitJoin67_AnonFilter_a0_4494_4735_4823_4840_split[0], pop_complex(&SplitJoin87_SplitJoin65_SplitJoin65_AnonFilter_a0_4492_4734_4822_4839_split[0]));
		push_complex(&SplitJoin89_SplitJoin67_SplitJoin67_AnonFilter_a0_4494_4735_4823_4840_split[1], pop_complex(&SplitJoin87_SplitJoin65_SplitJoin65_AnonFilter_a0_4492_4734_4822_4839_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4657() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		push_complex(&SplitJoin87_SplitJoin65_SplitJoin65_AnonFilter_a0_4492_4734_4822_4839_join[0], pop_complex(&SplitJoin89_SplitJoin67_SplitJoin67_AnonFilter_a0_4494_4735_4823_4840_join[0]));
		push_complex(&SplitJoin87_SplitJoin65_SplitJoin65_AnonFilter_a0_4492_4734_4822_4839_join[0], pop_complex(&SplitJoin89_SplitJoin67_SplitJoin67_AnonFilter_a0_4494_4735_4823_4840_join[1]));
	ENDFOR
}}

void Identity_4502(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t __tmp1316 = pop_complex(&SplitJoin93_SplitJoin71_SplitJoin71_AnonFilter_a0_4500_4738_4824_4841_split[0]);
		push_complex(&SplitJoin93_SplitJoin71_SplitJoin71_AnonFilter_a0_4500_4738_4824_4841_join[0], __tmp1316) ; 
	}
	ENDFOR
}

void Identity_4504(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t __tmp1319 = pop_complex(&SplitJoin93_SplitJoin71_SplitJoin71_AnonFilter_a0_4500_4738_4824_4841_split[1]);
		push_complex(&SplitJoin93_SplitJoin71_SplitJoin71_AnonFilter_a0_4500_4738_4824_4841_join[1], __tmp1319) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4658() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		push_complex(&SplitJoin93_SplitJoin71_SplitJoin71_AnonFilter_a0_4500_4738_4824_4841_split[0], pop_complex(&SplitJoin87_SplitJoin65_SplitJoin65_AnonFilter_a0_4492_4734_4822_4839_split[1]));
		push_complex(&SplitJoin93_SplitJoin71_SplitJoin71_AnonFilter_a0_4500_4738_4824_4841_split[1], pop_complex(&SplitJoin87_SplitJoin65_SplitJoin65_AnonFilter_a0_4492_4734_4822_4839_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4659() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		push_complex(&SplitJoin87_SplitJoin65_SplitJoin65_AnonFilter_a0_4492_4734_4822_4839_join[1], pop_complex(&SplitJoin93_SplitJoin71_SplitJoin71_AnonFilter_a0_4500_4738_4824_4841_join[0]));
		push_complex(&SplitJoin87_SplitJoin65_SplitJoin65_AnonFilter_a0_4492_4734_4822_4839_join[1], pop_complex(&SplitJoin93_SplitJoin71_SplitJoin71_AnonFilter_a0_4500_4738_4824_4841_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_4654() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 22, __iter_steady_++)
		push_complex(&SplitJoin87_SplitJoin65_SplitJoin65_AnonFilter_a0_4492_4734_4822_4839_split[0], pop_complex(&SplitJoin85_SplitJoin63_SplitJoin63_AnonFilter_a0_4490_4733_4769_4838_split[0]));
		push_complex(&SplitJoin87_SplitJoin65_SplitJoin65_AnonFilter_a0_4492_4734_4822_4839_split[1], pop_complex(&SplitJoin85_SplitJoin63_SplitJoin63_AnonFilter_a0_4490_4733_4769_4838_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4655() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		push_complex(&SplitJoin85_SplitJoin63_SplitJoin63_AnonFilter_a0_4490_4733_4769_4838_join[0], pop_complex(&SplitJoin87_SplitJoin65_SplitJoin65_AnonFilter_a0_4492_4734_4822_4839_join[0]));
		push_complex(&SplitJoin85_SplitJoin63_SplitJoin63_AnonFilter_a0_4490_4733_4769_4838_join[0], pop_complex(&SplitJoin87_SplitJoin65_SplitJoin65_AnonFilter_a0_4492_4734_4822_4839_join[0]));
		push_complex(&SplitJoin85_SplitJoin63_SplitJoin63_AnonFilter_a0_4490_4733_4769_4838_join[0], pop_complex(&SplitJoin87_SplitJoin65_SplitJoin65_AnonFilter_a0_4492_4734_4822_4839_join[1]));
		push_complex(&SplitJoin85_SplitJoin63_SplitJoin63_AnonFilter_a0_4490_4733_4769_4838_join[0], pop_complex(&SplitJoin87_SplitJoin65_SplitJoin65_AnonFilter_a0_4492_4734_4822_4839_join[1]));
	ENDFOR
}}

void Identity_4510(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t __tmp1332 = pop_complex(&SplitJoin99_SplitJoin77_SplitJoin77_AnonFilter_a0_4508_4742_4826_4843_split[0]);
		push_complex(&SplitJoin99_SplitJoin77_SplitJoin77_AnonFilter_a0_4508_4742_4826_4843_join[0], __tmp1332) ; 
	}
	ENDFOR
}

void Identity_4512(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t __tmp1335 = pop_complex(&SplitJoin99_SplitJoin77_SplitJoin77_AnonFilter_a0_4508_4742_4826_4843_split[1]);
		push_complex(&SplitJoin99_SplitJoin77_SplitJoin77_AnonFilter_a0_4508_4742_4826_4843_join[1], __tmp1335) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4662() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		push_complex(&SplitJoin99_SplitJoin77_SplitJoin77_AnonFilter_a0_4508_4742_4826_4843_split[0], pop_complex(&SplitJoin97_SplitJoin75_SplitJoin75_AnonFilter_a0_4506_4741_4825_4842_split[0]));
		push_complex(&SplitJoin99_SplitJoin77_SplitJoin77_AnonFilter_a0_4508_4742_4826_4843_split[1], pop_complex(&SplitJoin97_SplitJoin75_SplitJoin75_AnonFilter_a0_4506_4741_4825_4842_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4663() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		push_complex(&SplitJoin97_SplitJoin75_SplitJoin75_AnonFilter_a0_4506_4741_4825_4842_join[0], pop_complex(&SplitJoin99_SplitJoin77_SplitJoin77_AnonFilter_a0_4508_4742_4826_4843_join[0]));
		push_complex(&SplitJoin97_SplitJoin75_SplitJoin75_AnonFilter_a0_4506_4741_4825_4842_join[0], pop_complex(&SplitJoin99_SplitJoin77_SplitJoin77_AnonFilter_a0_4508_4742_4826_4843_join[1]));
	ENDFOR
}}

void Identity_4516(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t __tmp1343 = pop_complex(&SplitJoin103_SplitJoin81_SplitJoin81_AnonFilter_a0_4514_4745_4827_4844_split[0]);
		push_complex(&SplitJoin103_SplitJoin81_SplitJoin81_AnonFilter_a0_4514_4745_4827_4844_join[0], __tmp1343) ; 
	}
	ENDFOR
}

void Identity_4518(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t __tmp1346 = pop_complex(&SplitJoin103_SplitJoin81_SplitJoin81_AnonFilter_a0_4514_4745_4827_4844_split[1]);
		push_complex(&SplitJoin103_SplitJoin81_SplitJoin81_AnonFilter_a0_4514_4745_4827_4844_join[1], __tmp1346) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4664() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		push_complex(&SplitJoin103_SplitJoin81_SplitJoin81_AnonFilter_a0_4514_4745_4827_4844_split[0], pop_complex(&SplitJoin97_SplitJoin75_SplitJoin75_AnonFilter_a0_4506_4741_4825_4842_split[1]));
		push_complex(&SplitJoin103_SplitJoin81_SplitJoin81_AnonFilter_a0_4514_4745_4827_4844_split[1], pop_complex(&SplitJoin97_SplitJoin75_SplitJoin75_AnonFilter_a0_4506_4741_4825_4842_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4665() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		push_complex(&SplitJoin97_SplitJoin75_SplitJoin75_AnonFilter_a0_4506_4741_4825_4842_join[1], pop_complex(&SplitJoin103_SplitJoin81_SplitJoin81_AnonFilter_a0_4514_4745_4827_4844_join[0]));
		push_complex(&SplitJoin97_SplitJoin75_SplitJoin75_AnonFilter_a0_4506_4741_4825_4842_join[1], pop_complex(&SplitJoin103_SplitJoin81_SplitJoin81_AnonFilter_a0_4514_4745_4827_4844_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_4660() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 22, __iter_steady_++)
		push_complex(&SplitJoin97_SplitJoin75_SplitJoin75_AnonFilter_a0_4506_4741_4825_4842_split[0], pop_complex(&SplitJoin85_SplitJoin63_SplitJoin63_AnonFilter_a0_4490_4733_4769_4838_split[1]));
		push_complex(&SplitJoin97_SplitJoin75_SplitJoin75_AnonFilter_a0_4506_4741_4825_4842_split[1], pop_complex(&SplitJoin85_SplitJoin63_SplitJoin63_AnonFilter_a0_4490_4733_4769_4838_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4661() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		push_complex(&SplitJoin85_SplitJoin63_SplitJoin63_AnonFilter_a0_4490_4733_4769_4838_join[1], pop_complex(&SplitJoin97_SplitJoin75_SplitJoin75_AnonFilter_a0_4506_4741_4825_4842_join[0]));
		push_complex(&SplitJoin85_SplitJoin63_SplitJoin63_AnonFilter_a0_4490_4733_4769_4838_join[1], pop_complex(&SplitJoin97_SplitJoin75_SplitJoin75_AnonFilter_a0_4506_4741_4825_4842_join[0]));
		push_complex(&SplitJoin85_SplitJoin63_SplitJoin63_AnonFilter_a0_4490_4733_4769_4838_join[1], pop_complex(&SplitJoin97_SplitJoin75_SplitJoin75_AnonFilter_a0_4506_4741_4825_4842_join[1]));
		push_complex(&SplitJoin85_SplitJoin63_SplitJoin63_AnonFilter_a0_4490_4733_4769_4838_join[1], pop_complex(&SplitJoin97_SplitJoin75_SplitJoin75_AnonFilter_a0_4506_4741_4825_4842_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_4652() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 44, __iter_steady_++)
		push_complex(&SplitJoin85_SplitJoin63_SplitJoin63_AnonFilter_a0_4490_4733_4769_4838_split[0], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_4458_4684_4811_4830_split[1]));
		push_complex(&SplitJoin85_SplitJoin63_SplitJoin63_AnonFilter_a0_4490_4733_4769_4838_split[1], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_4458_4684_4811_4830_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4653() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_4458_4684_4811_4830_join[1], pop_complex(&SplitJoin85_SplitJoin63_SplitJoin63_AnonFilter_a0_4490_4733_4769_4838_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_4458_4684_4811_4830_join[1], pop_complex(&SplitJoin85_SplitJoin63_SplitJoin63_AnonFilter_a0_4490_4733_4769_4838_join[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_4636() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 88, __iter_steady_++)
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_4458_4684_4811_4830_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4794WEIGHTED_ROUND_ROBIN_Splitter_4636));
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_4458_4684_4811_4830_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4794WEIGHTED_ROUND_ROBIN_Splitter_4636));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4637() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4637WEIGHTED_ROUND_ROBIN_Splitter_4780, pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_4458_4684_4811_4830_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4637WEIGHTED_ROUND_ROBIN_Splitter_4780, pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_4458_4684_4811_4830_join[1]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_4613(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
 {
 {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
			push_complex(&Pre_CollapsedDataParallel_1_4613butterfly_4520, peek_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child0_4771_4846_split[0], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
		}
		ENDFOR
	}
	}
	}
		pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child0_4771_4846_split[0]) ; 
	}
	ENDFOR
}

void butterfly_4520(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_4613butterfly_4520));
		complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_4613butterfly_4520));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&butterfly_4520Post_CollapsedDataParallel_2_4614, __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&butterfly_4520Post_CollapsedDataParallel_2_4614, __sa2) ; 
	}
	ENDFOR
}

void Post_CollapsedDataParallel_2_4614(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 2, _k++) {
 {
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child0_4771_4846_join[0], peek_complex(&butterfly_4520Post_CollapsedDataParallel_2_4614, (_k + 0))) ; 
		}
		}
		ENDFOR
	}
	}
		pop_complex(&butterfly_4520Post_CollapsedDataParallel_2_4614) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_4616(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
 {
 {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
			push_complex(&Pre_CollapsedDataParallel_1_4616butterfly_4521, peek_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child0_4771_4846_split[1], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
		}
		ENDFOR
	}
	}
	}
		pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child0_4771_4846_split[1]) ; 
	}
	ENDFOR
}

void butterfly_4521(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_4616butterfly_4521));
		complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_4616butterfly_4521));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&butterfly_4521Post_CollapsedDataParallel_2_4617, __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&butterfly_4521Post_CollapsedDataParallel_2_4617, __sa2) ; 
	}
	ENDFOR
}

void Post_CollapsedDataParallel_2_4617(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 2, _k++) {
 {
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child0_4771_4846_join[1], peek_complex(&butterfly_4521Post_CollapsedDataParallel_2_4617, (_k + 0))) ; 
		}
		}
		ENDFOR
	}
	}
		pop_complex(&butterfly_4521Post_CollapsedDataParallel_2_4617) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_4619(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
 {
 {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
			push_complex(&Pre_CollapsedDataParallel_1_4619butterfly_4522, peek_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child0_4771_4846_split[2], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
		}
		ENDFOR
	}
	}
	}
		pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child0_4771_4846_split[2]) ; 
	}
	ENDFOR
}

void butterfly_4522(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_4619butterfly_4522));
		complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_4619butterfly_4522));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&butterfly_4522Post_CollapsedDataParallel_2_4620, __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&butterfly_4522Post_CollapsedDataParallel_2_4620, __sa2) ; 
	}
	ENDFOR
}

void Post_CollapsedDataParallel_2_4620(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 2, _k++) {
 {
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child0_4771_4846_join[2], peek_complex(&butterfly_4522Post_CollapsedDataParallel_2_4620, (_k + 0))) ; 
		}
		}
		ENDFOR
	}
	}
		pop_complex(&butterfly_4522Post_CollapsedDataParallel_2_4620) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_4622(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
 {
 {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
			push_complex(&Pre_CollapsedDataParallel_1_4622butterfly_4523, peek_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child0_4771_4846_split[3], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
		}
		ENDFOR
	}
	}
	}
		pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child0_4771_4846_split[3]) ; 
	}
	ENDFOR
}

void butterfly_4523(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_4622butterfly_4523));
		complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_4622butterfly_4523));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&butterfly_4523Post_CollapsedDataParallel_2_4623, __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&butterfly_4523Post_CollapsedDataParallel_2_4623, __sa2) ; 
	}
	ENDFOR
}

void Post_CollapsedDataParallel_2_4623(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 2, _k++) {
 {
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child0_4771_4846_join[3], peek_complex(&butterfly_4523Post_CollapsedDataParallel_2_4623, (_k + 0))) ; 
		}
		}
		ENDFOR
	}
	}
		pop_complex(&butterfly_4523Post_CollapsedDataParallel_2_4623) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4781() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child0_4771_4846_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_Hier_4814_4845_split[0]));
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child0_4771_4846_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_Hier_4814_4845_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4782() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_Hier_4814_4845_join[0], pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child0_4771_4846_join[__iter_]));
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_Hier_4814_4845_join[0], pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child0_4771_4846_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_4625(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
 {
 {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
			push_complex(&Pre_CollapsedDataParallel_1_4625butterfly_4524, peek_complex(&SplitJoin64_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child1_4774_4847_split[0], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
		}
		ENDFOR
	}
	}
	}
		pop_complex(&SplitJoin64_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child1_4774_4847_split[0]) ; 
	}
	ENDFOR
}

void butterfly_4524(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_4625butterfly_4524));
		complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_4625butterfly_4524));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&butterfly_4524Post_CollapsedDataParallel_2_4626, __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&butterfly_4524Post_CollapsedDataParallel_2_4626, __sa2) ; 
	}
	ENDFOR
}

void Post_CollapsedDataParallel_2_4626(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 2, _k++) {
 {
			push_complex(&SplitJoin64_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child1_4774_4847_join[0], peek_complex(&butterfly_4524Post_CollapsedDataParallel_2_4626, (_k + 0))) ; 
		}
		}
		ENDFOR
	}
	}
		pop_complex(&butterfly_4524Post_CollapsedDataParallel_2_4626) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_4628(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
 {
 {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
			push_complex(&Pre_CollapsedDataParallel_1_4628butterfly_4525, peek_complex(&SplitJoin64_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child1_4774_4847_split[1], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
		}
		ENDFOR
	}
	}
	}
		pop_complex(&SplitJoin64_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child1_4774_4847_split[1]) ; 
	}
	ENDFOR
}

void butterfly_4525(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_4628butterfly_4525));
		complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_4628butterfly_4525));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&butterfly_4525Post_CollapsedDataParallel_2_4629, __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&butterfly_4525Post_CollapsedDataParallel_2_4629, __sa2) ; 
	}
	ENDFOR
}

void Post_CollapsedDataParallel_2_4629(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 2, _k++) {
 {
			push_complex(&SplitJoin64_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child1_4774_4847_join[1], peek_complex(&butterfly_4525Post_CollapsedDataParallel_2_4629, (_k + 0))) ; 
		}
		}
		ENDFOR
	}
	}
		pop_complex(&butterfly_4525Post_CollapsedDataParallel_2_4629) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_4631(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
 {
 {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
			push_complex(&Pre_CollapsedDataParallel_1_4631butterfly_4526, peek_complex(&SplitJoin64_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child1_4774_4847_split[2], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
		}
		ENDFOR
	}
	}
	}
		pop_complex(&SplitJoin64_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child1_4774_4847_split[2]) ; 
	}
	ENDFOR
}

void butterfly_4526(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_4631butterfly_4526));
		complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_4631butterfly_4526));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&butterfly_4526Post_CollapsedDataParallel_2_4632, __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&butterfly_4526Post_CollapsedDataParallel_2_4632, __sa2) ; 
	}
	ENDFOR
}

void Post_CollapsedDataParallel_2_4632(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 2, _k++) {
 {
			push_complex(&SplitJoin64_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child1_4774_4847_join[2], peek_complex(&butterfly_4526Post_CollapsedDataParallel_2_4632, (_k + 0))) ; 
		}
		}
		ENDFOR
	}
	}
		pop_complex(&butterfly_4526Post_CollapsedDataParallel_2_4632) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_4634(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
 {
 {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
			push_complex(&Pre_CollapsedDataParallel_1_4634butterfly_4527, peek_complex(&SplitJoin64_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child1_4774_4847_split[3], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
		}
		ENDFOR
	}
	}
	}
		pop_complex(&SplitJoin64_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child1_4774_4847_split[3]) ; 
	}
	ENDFOR
}

void butterfly_4527(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_4634butterfly_4527));
		complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_4634butterfly_4527));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&butterfly_4527Post_CollapsedDataParallel_2_4635, __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&butterfly_4527Post_CollapsedDataParallel_2_4635, __sa2) ; 
	}
	ENDFOR
}

void Post_CollapsedDataParallel_2_4635(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 2, _k++) {
 {
			push_complex(&SplitJoin64_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child1_4774_4847_join[3], peek_complex(&butterfly_4527Post_CollapsedDataParallel_2_4635, (_k + 0))) ; 
		}
		}
		ENDFOR
	}
	}
		pop_complex(&butterfly_4527Post_CollapsedDataParallel_2_4635) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4783() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin64_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child1_4774_4847_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_Hier_4814_4845_split[1]));
			push_complex(&SplitJoin64_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child1_4774_4847_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_Hier_4814_4845_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4784() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_Hier_4814_4845_join[1], pop_complex(&SplitJoin64_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child1_4774_4847_join[__iter_]));
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_Hier_4814_4845_join[1], pop_complex(&SplitJoin64_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child1_4774_4847_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_4780() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_Hier_4814_4845_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4637WEIGHTED_ROUND_ROBIN_Splitter_4780));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_Hier_4814_4845_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4637WEIGHTED_ROUND_ROBIN_Splitter_4780));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4785() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4785WEIGHTED_ROUND_ROBIN_Splitter_4786, pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_Hier_4814_4845_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4785WEIGHTED_ROUND_ROBIN_Splitter_4786, pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_Hier_4814_4845_join[1]));
		ENDFOR
	ENDFOR
}}

void butterfly_4529(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_4439_4692_4770_4850_split[0]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_4439_4692_4770_4850_split[0]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_4439_4692_4770_4850_join[0], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_4439_4692_4770_4850_join[0], __sa2) ; 
	}
	ENDFOR
}

void butterfly_4530(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_4439_4692_4770_4850_split[1]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_4439_4692_4770_4850_split[1]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = -4.371139E-8 ; 
		WN1.imag = -1.0 ; 
		WN2.real = 1.1924881E-8 ; 
		WN2.imag = 1.0 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_4439_4692_4770_4850_join[1], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_4439_4692_4770_4850_join[1], __sa2) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4670() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 22, __iter_steady_++)
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_4439_4692_4770_4850_split[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child0_4777_4849_split[0]));
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_4439_4692_4770_4850_split[1], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child0_4777_4849_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4671() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 22, __iter_steady_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child0_4777_4849_join[0], pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_4439_4692_4770_4850_join[0]));
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child0_4777_4849_join[0], pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_4439_4692_4770_4850_join[1]));
	ENDFOR
}}

void butterfly_4531(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin47_SplitJoin29_SplitJoin29_split2_4441_4706_4772_4851_split[0]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin47_SplitJoin29_SplitJoin29_split2_4441_4706_4772_4851_split[0]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin47_SplitJoin29_SplitJoin29_split2_4441_4706_4772_4851_join[0], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin47_SplitJoin29_SplitJoin29_split2_4441_4706_4772_4851_join[0], __sa2) ; 
	}
	ENDFOR
}

void butterfly_4532(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin47_SplitJoin29_SplitJoin29_split2_4441_4706_4772_4851_split[1]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin47_SplitJoin29_SplitJoin29_split2_4441_4706_4772_4851_split[1]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = -4.371139E-8 ; 
		WN1.imag = -1.0 ; 
		WN2.real = 1.1924881E-8 ; 
		WN2.imag = 1.0 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin47_SplitJoin29_SplitJoin29_split2_4441_4706_4772_4851_join[1], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin47_SplitJoin29_SplitJoin29_split2_4441_4706_4772_4851_join[1], __sa2) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4672() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 22, __iter_steady_++)
		push_complex(&SplitJoin47_SplitJoin29_SplitJoin29_split2_4441_4706_4772_4851_split[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child0_4777_4849_split[1]));
		push_complex(&SplitJoin47_SplitJoin29_SplitJoin29_split2_4441_4706_4772_4851_split[1], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child0_4777_4849_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4673() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 22, __iter_steady_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child0_4777_4849_join[1], pop_complex(&SplitJoin47_SplitJoin29_SplitJoin29_split2_4441_4706_4772_4851_join[0]));
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child0_4777_4849_join[1], pop_complex(&SplitJoin47_SplitJoin29_SplitJoin29_split2_4441_4706_4772_4851_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_4787() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child0_4777_4849_split[0], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_Hier_4815_4848_split[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child0_4777_4849_split[1], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_Hier_4815_4848_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4788() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_Hier_4815_4848_join[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child0_4777_4849_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_Hier_4815_4848_join[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child0_4777_4849_join[1]));
		ENDFOR
	ENDFOR
}}

void butterfly_4533(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin53_SplitJoin33_SplitJoin33_split2_4443_4709_4773_4853_split[0]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin53_SplitJoin33_SplitJoin33_split2_4443_4709_4773_4853_split[0]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin53_SplitJoin33_SplitJoin33_split2_4443_4709_4773_4853_join[0], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin53_SplitJoin33_SplitJoin33_split2_4443_4709_4773_4853_join[0], __sa2) ; 
	}
	ENDFOR
}

void butterfly_4534(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin53_SplitJoin33_SplitJoin33_split2_4443_4709_4773_4853_split[1]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin53_SplitJoin33_SplitJoin33_split2_4443_4709_4773_4853_split[1]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = -4.371139E-8 ; 
		WN1.imag = -1.0 ; 
		WN2.real = 1.1924881E-8 ; 
		WN2.imag = 1.0 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin53_SplitJoin33_SplitJoin33_split2_4443_4709_4773_4853_join[1], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin53_SplitJoin33_SplitJoin33_split2_4443_4709_4773_4853_join[1], __sa2) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4674() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 22, __iter_steady_++)
		push_complex(&SplitJoin53_SplitJoin33_SplitJoin33_split2_4443_4709_4773_4853_split[0], pop_complex(&SplitJoin51_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child1_4779_4852_split[0]));
		push_complex(&SplitJoin53_SplitJoin33_SplitJoin33_split2_4443_4709_4773_4853_split[1], pop_complex(&SplitJoin51_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child1_4779_4852_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4675() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 22, __iter_steady_++)
		push_complex(&SplitJoin51_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child1_4779_4852_join[0], pop_complex(&SplitJoin53_SplitJoin33_SplitJoin33_split2_4443_4709_4773_4853_join[0]));
		push_complex(&SplitJoin51_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child1_4779_4852_join[0], pop_complex(&SplitJoin53_SplitJoin33_SplitJoin33_split2_4443_4709_4773_4853_join[1]));
	ENDFOR
}}

void butterfly_4535(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin57_SplitJoin37_SplitJoin37_split2_4445_4712_4775_4854_split[0]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin57_SplitJoin37_SplitJoin37_split2_4445_4712_4775_4854_split[0]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin57_SplitJoin37_SplitJoin37_split2_4445_4712_4775_4854_join[0], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin57_SplitJoin37_SplitJoin37_split2_4445_4712_4775_4854_join[0], __sa2) ; 
	}
	ENDFOR
}

void butterfly_4536(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin57_SplitJoin37_SplitJoin37_split2_4445_4712_4775_4854_split[1]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin57_SplitJoin37_SplitJoin37_split2_4445_4712_4775_4854_split[1]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = -4.371139E-8 ; 
		WN1.imag = -1.0 ; 
		WN2.real = 1.1924881E-8 ; 
		WN2.imag = 1.0 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin57_SplitJoin37_SplitJoin37_split2_4445_4712_4775_4854_join[1], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin57_SplitJoin37_SplitJoin37_split2_4445_4712_4775_4854_join[1], __sa2) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4676() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 22, __iter_steady_++)
		push_complex(&SplitJoin57_SplitJoin37_SplitJoin37_split2_4445_4712_4775_4854_split[0], pop_complex(&SplitJoin51_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child1_4779_4852_split[1]));
		push_complex(&SplitJoin57_SplitJoin37_SplitJoin37_split2_4445_4712_4775_4854_split[1], pop_complex(&SplitJoin51_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child1_4779_4852_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4677() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 22, __iter_steady_++)
		push_complex(&SplitJoin51_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child1_4779_4852_join[1], pop_complex(&SplitJoin57_SplitJoin37_SplitJoin37_split2_4445_4712_4775_4854_join[0]));
		push_complex(&SplitJoin51_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child1_4779_4852_join[1], pop_complex(&SplitJoin57_SplitJoin37_SplitJoin37_split2_4445_4712_4775_4854_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_4789() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin51_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child1_4779_4852_split[0], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_Hier_4815_4848_split[1]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin51_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child1_4779_4852_split[1], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_Hier_4815_4848_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4790() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_Hier_4815_4848_join[1], pop_complex(&SplitJoin51_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child1_4779_4852_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_Hier_4815_4848_join[1], pop_complex(&SplitJoin51_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child1_4779_4852_join[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_4786() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_Hier_4815_4848_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4785WEIGHTED_ROUND_ROBIN_Splitter_4786));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_Hier_4815_4848_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4785WEIGHTED_ROUND_ROBIN_Splitter_4786));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4791() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4791WEIGHTED_ROUND_ROBIN_Splitter_4678, pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_Hier_4815_4848_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4791WEIGHTED_ROUND_ROBIN_Splitter_4678, pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_Hier_4815_4848_join[1]));
		ENDFOR
	ENDFOR
}}

void butterfly_4538(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_4452_4695_4776_4856_split[0]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_4452_4695_4776_4856_split[0]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_4452_4695_4776_4856_join[0], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_4452_4695_4776_4856_join[0], __sa2) ; 
	}
	ENDFOR
}

void butterfly_4539(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_4452_4695_4776_4856_split[1]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_4452_4695_4776_4856_split[1]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 0.70710677 ; 
		WN1.imag = -0.70710677 ; 
		WN2.real = -0.70710665 ; 
		WN2.imag = 0.7071069 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_4452_4695_4776_4856_join[1], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_4452_4695_4776_4856_join[1], __sa2) ; 
	}
	ENDFOR
}

void butterfly_4540(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_4452_4695_4776_4856_split[2]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_4452_4695_4776_4856_split[2]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = -4.371139E-8 ; 
		WN1.imag = -1.0 ; 
		WN2.real = 1.1924881E-8 ; 
		WN2.imag = 1.0 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_4452_4695_4776_4856_join[2], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_4452_4695_4776_4856_join[2], __sa2) ; 
	}
	ENDFOR
}

void butterfly_4541(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_4452_4695_4776_4856_split[3]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_4452_4695_4776_4856_split[3]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = -0.70710677 ; 
		WN1.imag = -0.70710677 ; 
		WN2.real = 0.707107 ; 
		WN2.imag = 0.70710653 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_4452_4695_4776_4856_join[3], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_4452_4695_4776_4856_join[3], __sa2) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4680() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 22, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_4452_4695_4776_4856_split[__iter_], pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_4450_4694_4816_4855_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4681() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 22, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_4450_4694_4816_4855_join[0], pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_4452_4695_4776_4856_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void butterfly_4542(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin40_SplitJoin22_SplitJoin22_split2_4454_4700_4778_4857_split[0]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin40_SplitJoin22_SplitJoin22_split2_4454_4700_4778_4857_split[0]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin40_SplitJoin22_SplitJoin22_split2_4454_4700_4778_4857_join[0], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin40_SplitJoin22_SplitJoin22_split2_4454_4700_4778_4857_join[0], __sa2) ; 
	}
	ENDFOR
}

void butterfly_4543(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin40_SplitJoin22_SplitJoin22_split2_4454_4700_4778_4857_split[1]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin40_SplitJoin22_SplitJoin22_split2_4454_4700_4778_4857_split[1]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 0.70710677 ; 
		WN1.imag = -0.70710677 ; 
		WN2.real = -0.70710665 ; 
		WN2.imag = 0.7071069 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin40_SplitJoin22_SplitJoin22_split2_4454_4700_4778_4857_join[1], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin40_SplitJoin22_SplitJoin22_split2_4454_4700_4778_4857_join[1], __sa2) ; 
	}
	ENDFOR
}

void butterfly_4544(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin40_SplitJoin22_SplitJoin22_split2_4454_4700_4778_4857_split[2]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin40_SplitJoin22_SplitJoin22_split2_4454_4700_4778_4857_split[2]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = -4.371139E-8 ; 
		WN1.imag = -1.0 ; 
		WN2.real = 1.1924881E-8 ; 
		WN2.imag = 1.0 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin40_SplitJoin22_SplitJoin22_split2_4454_4700_4778_4857_join[2], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin40_SplitJoin22_SplitJoin22_split2_4454_4700_4778_4857_join[2], __sa2) ; 
	}
	ENDFOR
}

void butterfly_4545(){
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin40_SplitJoin22_SplitJoin22_split2_4454_4700_4778_4857_split[3]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin40_SplitJoin22_SplitJoin22_split2_4454_4700_4778_4857_split[3]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = -0.70710677 ; 
		WN1.imag = -0.70710677 ; 
		WN2.real = 0.707107 ; 
		WN2.imag = 0.70710653 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin40_SplitJoin22_SplitJoin22_split2_4454_4700_4778_4857_join[3], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin40_SplitJoin22_SplitJoin22_split2_4454_4700_4778_4857_join[3], __sa2) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4682() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 22, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin40_SplitJoin22_SplitJoin22_split2_4454_4700_4778_4857_split[__iter_], pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_4450_4694_4816_4855_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4683() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 22, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_4450_4694_4816_4855_join[1], pop_complex(&SplitJoin40_SplitJoin22_SplitJoin22_split2_4454_4700_4778_4857_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_4678() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_4450_4694_4816_4855_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4791WEIGHTED_ROUND_ROBIN_Splitter_4678));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_4450_4694_4816_4855_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4791WEIGHTED_ROUND_ROBIN_Splitter_4678));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4679() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4679WEIGHTED_ROUND_ROBIN_Splitter_4797, pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_4450_4694_4816_4855_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4679WEIGHTED_ROUND_ROBIN_Splitter_4797, pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_4450_4694_4816_4855_join[1]));
		ENDFOR
	ENDFOR
}}

void magnitude_4799(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_4817_4858_split[0]));
		push_float(&SplitJoin24_magnitude_Fiss_4817_4858_join[0], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_4800(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_4817_4858_split[1]));
		push_float(&SplitJoin24_magnitude_Fiss_4817_4858_join[1], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_4801(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_4817_4858_split[2]));
		push_float(&SplitJoin24_magnitude_Fiss_4817_4858_join[2], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_4802(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_4817_4858_split[3]));
		push_float(&SplitJoin24_magnitude_Fiss_4817_4858_join[3], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_4803(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_4817_4858_split[4]));
		push_float(&SplitJoin24_magnitude_Fiss_4817_4858_join[4], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_4804(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_4817_4858_split[5]));
		push_float(&SplitJoin24_magnitude_Fiss_4817_4858_join[5], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_4805(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_4817_4858_split[6]));
		push_float(&SplitJoin24_magnitude_Fiss_4817_4858_join[6], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_4806(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_4817_4858_split[7]));
		push_float(&SplitJoin24_magnitude_Fiss_4817_4858_join[7], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_4807(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_4817_4858_split[8]));
		push_float(&SplitJoin24_magnitude_Fiss_4817_4858_join[8], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_4808(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_4817_4858_split[9]));
		push_float(&SplitJoin24_magnitude_Fiss_4817_4858_join[9], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_4809(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_4817_4858_split[10]));
		push_float(&SplitJoin24_magnitude_Fiss_4817_4858_join[10], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4797() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 11, __iter_++)
			push_complex(&SplitJoin24_magnitude_Fiss_4817_4858_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4679WEIGHTED_ROUND_ROBIN_Splitter_4797));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4798() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 11, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4798sink_4547, pop_float(&SplitJoin24_magnitude_Fiss_4817_4858_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void sink_4547(){
	FOR(uint32_t, __iter_steady_, 0, <, 176, __iter_steady_++) {
		printf("%.10f", pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4798sink_4547));
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4785WEIGHTED_ROUND_ROBIN_Splitter_4786);
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_complex(&SplitJoin57_SplitJoin37_SplitJoin37_split2_4445_4712_4775_4854_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 2, __iter_init_1_++)
		init_buffer_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_4464_4687_4813_4833_split[__iter_init_1_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_4613butterfly_4520);
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_complex(&SplitJoin75_SplitJoin53_SplitJoin53_AnonFilter_a0_4476_4726_4819_4835_split[__iter_init_2_]);
	ENDFOR
	init_buffer_complex(&butterfly_4527Post_CollapsedDataParallel_2_4635);
	FOR(int, __iter_init_3_, 0, <, 2, __iter_init_3_++)
		init_buffer_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_4464_4687_4813_4833_join[__iter_init_3_]);
	ENDFOR
	init_buffer_complex(&butterfly_4526Post_CollapsedDataParallel_2_4632);
	FOR(int, __iter_init_4_, 0, <, 2, __iter_init_4_++)
		init_buffer_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_4450_4694_4816_4855_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 2, __iter_init_5_++)
		init_buffer_complex(&SplitJoin51_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child1_4779_4852_join[__iter_init_5_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_4634butterfly_4527);
	FOR(int, __iter_init_6_, 0, <, 2, __iter_init_6_++)
		init_buffer_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_4462_4686_4812_4832_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 2, __iter_init_7_++)
		init_buffer_complex(&SplitJoin89_SplitJoin67_SplitJoin67_AnonFilter_a0_4494_4735_4823_4840_split[__iter_init_7_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_4625butterfly_4524);
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child0_4777_4849_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 4, __iter_init_9_++)
		init_buffer_complex(&SplitJoin40_SplitJoin22_SplitJoin22_split2_4454_4700_4778_4857_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_Hier_4814_4845_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 2, __iter_init_11_++)
		init_buffer_complex(&SplitJoin97_SplitJoin75_SplitJoin75_AnonFilter_a0_4506_4741_4825_4842_join[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 2, __iter_init_12_++)
		init_buffer_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_Hier_4815_4848_join[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_complex(&SplitJoin53_SplitJoin33_SplitJoin33_split2_4443_4709_4773_4853_join[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_complex(&SplitJoin93_SplitJoin71_SplitJoin71_AnonFilter_a0_4500_4738_4824_4841_join[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 2, __iter_init_15_++)
		init_buffer_complex(&SplitJoin51_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child1_4779_4852_split[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_complex(&SplitJoin89_SplitJoin67_SplitJoin67_AnonFilter_a0_4494_4735_4823_4840_join[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_complex(&SplitJoin0_source_Fiss_4810_4829_join[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 2, __iter_init_18_++)
		init_buffer_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_Hier_4815_4848_split[__iter_init_18_]);
	ENDFOR
	FOR(int, __iter_init_19_, 0, <, 4, __iter_init_19_++)
		init_buffer_complex(&SplitJoin40_SplitJoin22_SplitJoin22_split2_4454_4700_4778_4857_split[__iter_init_19_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4794WEIGHTED_ROUND_ROBIN_Splitter_4636);
	FOR(int, __iter_init_20_, 0, <, 2, __iter_init_20_++)
		init_buffer_complex(&SplitJoin71_SplitJoin49_SplitJoin49_AnonFilter_a0_4470_4723_4818_4834_join[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 2, __iter_init_21_++)
		init_buffer_complex(&SplitJoin85_SplitJoin63_SplitJoin63_AnonFilter_a0_4490_4733_4769_4838_join[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 4, __iter_init_22_++)
		init_buffer_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_4452_4695_4776_4856_join[__iter_init_22_]);
	ENDFOR
	FOR(int, __iter_init_23_, 0, <, 2, __iter_init_23_++)
		init_buffer_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_4437_4691_Hier_child0_4777_4849_split[__iter_init_23_]);
	ENDFOR
	FOR(int, __iter_init_24_, 0, <, 2, __iter_init_24_++)
		init_buffer_complex(&SplitJoin81_SplitJoin59_SplitJoin59_AnonFilter_a0_4484_4730_4821_4837_join[__iter_init_24_]);
	ENDFOR
	FOR(int, __iter_init_25_, 0, <, 2, __iter_init_25_++)
		init_buffer_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_4450_4694_4816_4855_join[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 2, __iter_init_26_++)
		init_buffer_complex(&SplitJoin81_SplitJoin59_SplitJoin59_AnonFilter_a0_4484_4730_4821_4837_split[__iter_init_26_]);
	ENDFOR
	FOR(int, __iter_init_27_, 0, <, 2, __iter_init_27_++)
		init_buffer_complex(&SplitJoin99_SplitJoin77_SplitJoin77_AnonFilter_a0_4508_4742_4826_4843_split[__iter_init_27_]);
	ENDFOR
	FOR(int, __iter_init_28_, 0, <, 2, __iter_init_28_++)
		init_buffer_complex(&SplitJoin47_SplitJoin29_SplitJoin29_split2_4441_4706_4772_4851_split[__iter_init_28_]);
	ENDFOR
	init_buffer_complex(&butterfly_4520Post_CollapsedDataParallel_2_4614);
	init_buffer_complex(&Pre_CollapsedDataParallel_1_4622butterfly_4523);
	FOR(int, __iter_init_29_, 0, <, 2, __iter_init_29_++)
		init_buffer_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_4439_4692_4770_4850_split[__iter_init_29_]);
	ENDFOR
	FOR(int, __iter_init_30_, 0, <, 2, __iter_init_30_++)
		init_buffer_complex(&SplitJoin75_SplitJoin53_SplitJoin53_AnonFilter_a0_4476_4726_4819_4835_join[__iter_init_30_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_4616butterfly_4521);
	FOR(int, __iter_init_31_, 0, <, 2, __iter_init_31_++)
		init_buffer_complex(&SplitJoin87_SplitJoin65_SplitJoin65_AnonFilter_a0_4492_4734_4822_4839_join[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 4, __iter_init_32_++)
		init_buffer_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child0_4771_4846_join[__iter_init_32_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4798sink_4547);
	FOR(int, __iter_init_33_, 0, <, 4, __iter_init_33_++)
		init_buffer_complex(&SplitJoin64_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child1_4774_4847_join[__iter_init_33_]);
	ENDFOR
	FOR(int, __iter_init_34_, 0, <, 11, __iter_init_34_++)
		init_buffer_float(&SplitJoin24_magnitude_Fiss_4817_4858_join[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 2, __iter_init_35_++)
		init_buffer_complex(&SplitJoin93_SplitJoin71_SplitJoin71_AnonFilter_a0_4500_4738_4824_4841_split[__iter_init_35_]);
	ENDFOR
	FOR(int, __iter_init_36_, 0, <, 4, __iter_init_36_++)
		init_buffer_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_4452_4695_4776_4856_split[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 2, __iter_init_37_++)
		init_buffer_complex(&SplitJoin87_SplitJoin65_SplitJoin65_AnonFilter_a0_4492_4734_4822_4839_split[__iter_init_37_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_4628butterfly_4525);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4679WEIGHTED_ROUND_ROBIN_Splitter_4797);
	FOR(int, __iter_init_38_, 0, <, 4, __iter_init_38_++)
		init_buffer_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child0_4771_4846_split[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 2, __iter_init_39_++)
		init_buffer_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_4460_4685_4768_4831_join[__iter_init_39_]);
	ENDFOR
	FOR(int, __iter_init_40_, 0, <, 2, __iter_init_40_++)
		init_buffer_complex(&SplitJoin103_SplitJoin81_SplitJoin81_AnonFilter_a0_4514_4745_4827_4844_join[__iter_init_40_]);
	ENDFOR
	FOR(int, __iter_init_41_, 0, <, 2, __iter_init_41_++)
		init_buffer_complex(&SplitJoin97_SplitJoin75_SplitJoin75_AnonFilter_a0_4506_4741_4825_4842_split[__iter_init_41_]);
	ENDFOR
	FOR(int, __iter_init_42_, 0, <, 2, __iter_init_42_++)
		init_buffer_complex(&SplitJoin0_source_Fiss_4810_4829_split[__iter_init_42_]);
	ENDFOR
	init_buffer_complex(&butterfly_4522Post_CollapsedDataParallel_2_4620);
	FOR(int, __iter_init_43_, 0, <, 4, __iter_init_43_++)
		init_buffer_complex(&SplitJoin64_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_child1_4774_4847_split[__iter_init_43_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_4631butterfly_4526);
	FOR(int, __iter_init_44_, 0, <, 2, __iter_init_44_++)
		init_buffer_complex(&SplitJoin99_SplitJoin77_SplitJoin77_AnonFilter_a0_4508_4742_4826_4843_join[__iter_init_44_]);
	ENDFOR
	init_buffer_complex(&butterfly_4523Post_CollapsedDataParallel_2_4623);
	FOR(int, __iter_init_45_, 0, <, 2, __iter_init_45_++)
		init_buffer_complex(&SplitJoin47_SplitJoin29_SplitJoin29_split2_4441_4706_4772_4851_join[__iter_init_45_]);
	ENDFOR
	init_buffer_complex(&butterfly_4524Post_CollapsedDataParallel_2_4626);
	init_buffer_complex(&butterfly_4525Post_CollapsedDataParallel_2_4629);
	FOR(int, __iter_init_46_, 0, <, 2, __iter_init_46_++)
		init_buffer_complex(&SplitJoin77_SplitJoin55_SplitJoin55_AnonFilter_a0_4478_4727_4820_4836_join[__iter_init_46_]);
	ENDFOR
	FOR(int, __iter_init_47_, 0, <, 2, __iter_init_47_++)
		init_buffer_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_4458_4684_4811_4830_join[__iter_init_47_]);
	ENDFOR
	FOR(int, __iter_init_48_, 0, <, 2, __iter_init_48_++)
		init_buffer_complex(&SplitJoin103_SplitJoin81_SplitJoin81_AnonFilter_a0_4514_4745_4827_4844_split[__iter_init_48_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_4619butterfly_4522);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4791WEIGHTED_ROUND_ROBIN_Splitter_4678);
	FOR(int, __iter_init_49_, 0, <, 2, __iter_init_49_++)
		init_buffer_complex(&SplitJoin57_SplitJoin37_SplitJoin37_split2_4445_4712_4775_4854_split[__iter_init_49_]);
	ENDFOR
	FOR(int, __iter_init_50_, 0, <, 2, __iter_init_50_++)
		init_buffer_complex(&SplitJoin77_SplitJoin55_SplitJoin55_AnonFilter_a0_4478_4727_4820_4836_split[__iter_init_50_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4637WEIGHTED_ROUND_ROBIN_Splitter_4780);
	FOR(int, __iter_init_51_, 0, <, 11, __iter_init_51_++)
		init_buffer_complex(&SplitJoin24_magnitude_Fiss_4817_4858_split[__iter_init_51_]);
	ENDFOR
	FOR(int, __iter_init_52_, 0, <, 2, __iter_init_52_++)
		init_buffer_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_4458_4684_4811_4830_split[__iter_init_52_]);
	ENDFOR
	FOR(int, __iter_init_53_, 0, <, 2, __iter_init_53_++)
		init_buffer_complex(&SplitJoin53_SplitJoin33_SplitJoin33_split2_4443_4709_4773_4853_split[__iter_init_53_]);
	ENDFOR
	FOR(int, __iter_init_54_, 0, <, 2, __iter_init_54_++)
		init_buffer_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_4460_4685_4768_4831_split[__iter_init_54_]);
	ENDFOR
	FOR(int, __iter_init_55_, 0, <, 2, __iter_init_55_++)
		init_buffer_complex(&SplitJoin71_SplitJoin49_SplitJoin49_AnonFilter_a0_4470_4723_4818_4834_split[__iter_init_55_]);
	ENDFOR
	FOR(int, __iter_init_56_, 0, <, 2, __iter_init_56_++)
		init_buffer_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_4439_4692_4770_4850_join[__iter_init_56_]);
	ENDFOR
	FOR(int, __iter_init_57_, 0, <, 2, __iter_init_57_++)
		init_buffer_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_4462_4686_4812_4832_split[__iter_init_57_]);
	ENDFOR
	FOR(int, __iter_init_58_, 0, <, 2, __iter_init_58_++)
		init_buffer_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_4416_4689_Hier_Hier_4814_4845_join[__iter_init_58_]);
	ENDFOR
	FOR(int, __iter_init_59_, 0, <, 2, __iter_init_59_++)
		init_buffer_complex(&SplitJoin85_SplitJoin63_SplitJoin63_AnonFilter_a0_4490_4733_4769_4838_split[__iter_init_59_]);
	ENDFOR
	init_buffer_complex(&butterfly_4521Post_CollapsedDataParallel_2_4617);
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_4793();
			source_4795();
			source_4796();
		WEIGHTED_ROUND_ROBIN_Joiner_4794();
		WEIGHTED_ROUND_ROBIN_Splitter_4636();
			WEIGHTED_ROUND_ROBIN_Splitter_4638();
				WEIGHTED_ROUND_ROBIN_Splitter_4640();
					WEIGHTED_ROUND_ROBIN_Splitter_4642();
						Identity_4466();
						Identity_4468();
					WEIGHTED_ROUND_ROBIN_Joiner_4643();
					WEIGHTED_ROUND_ROBIN_Splitter_4644();
						Identity_4472();
						Identity_4474();
					WEIGHTED_ROUND_ROBIN_Joiner_4645();
				WEIGHTED_ROUND_ROBIN_Joiner_4641();
				WEIGHTED_ROUND_ROBIN_Splitter_4646();
					WEIGHTED_ROUND_ROBIN_Splitter_4648();
						Identity_4480();
						Identity_4482();
					WEIGHTED_ROUND_ROBIN_Joiner_4649();
					WEIGHTED_ROUND_ROBIN_Splitter_4650();
						Identity_4486();
						Identity_4488();
					WEIGHTED_ROUND_ROBIN_Joiner_4651();
				WEIGHTED_ROUND_ROBIN_Joiner_4647();
			WEIGHTED_ROUND_ROBIN_Joiner_4639();
			WEIGHTED_ROUND_ROBIN_Splitter_4652();
				WEIGHTED_ROUND_ROBIN_Splitter_4654();
					WEIGHTED_ROUND_ROBIN_Splitter_4656();
						Identity_4496();
						Identity_4498();
					WEIGHTED_ROUND_ROBIN_Joiner_4657();
					WEIGHTED_ROUND_ROBIN_Splitter_4658();
						Identity_4502();
						Identity_4504();
					WEIGHTED_ROUND_ROBIN_Joiner_4659();
				WEIGHTED_ROUND_ROBIN_Joiner_4655();
				WEIGHTED_ROUND_ROBIN_Splitter_4660();
					WEIGHTED_ROUND_ROBIN_Splitter_4662();
						Identity_4510();
						Identity_4512();
					WEIGHTED_ROUND_ROBIN_Joiner_4663();
					WEIGHTED_ROUND_ROBIN_Splitter_4664();
						Identity_4516();
						Identity_4518();
					WEIGHTED_ROUND_ROBIN_Joiner_4665();
				WEIGHTED_ROUND_ROBIN_Joiner_4661();
			WEIGHTED_ROUND_ROBIN_Joiner_4653();
		WEIGHTED_ROUND_ROBIN_Joiner_4637();
		WEIGHTED_ROUND_ROBIN_Splitter_4780();
			WEIGHTED_ROUND_ROBIN_Splitter_4781();
				Pre_CollapsedDataParallel_1_4613();
				butterfly_4520();
				Post_CollapsedDataParallel_2_4614();
				Pre_CollapsedDataParallel_1_4616();
				butterfly_4521();
				Post_CollapsedDataParallel_2_4617();
				Pre_CollapsedDataParallel_1_4619();
				butterfly_4522();
				Post_CollapsedDataParallel_2_4620();
				Pre_CollapsedDataParallel_1_4622();
				butterfly_4523();
				Post_CollapsedDataParallel_2_4623();
			WEIGHTED_ROUND_ROBIN_Joiner_4782();
			WEIGHTED_ROUND_ROBIN_Splitter_4783();
				Pre_CollapsedDataParallel_1_4625();
				butterfly_4524();
				Post_CollapsedDataParallel_2_4626();
				Pre_CollapsedDataParallel_1_4628();
				butterfly_4525();
				Post_CollapsedDataParallel_2_4629();
				Pre_CollapsedDataParallel_1_4631();
				butterfly_4526();
				Post_CollapsedDataParallel_2_4632();
				Pre_CollapsedDataParallel_1_4634();
				butterfly_4527();
				Post_CollapsedDataParallel_2_4635();
			WEIGHTED_ROUND_ROBIN_Joiner_4784();
		WEIGHTED_ROUND_ROBIN_Joiner_4785();
		WEIGHTED_ROUND_ROBIN_Splitter_4786();
			WEIGHTED_ROUND_ROBIN_Splitter_4787();
				WEIGHTED_ROUND_ROBIN_Splitter_4670();
					butterfly_4529();
					butterfly_4530();
				WEIGHTED_ROUND_ROBIN_Joiner_4671();
				WEIGHTED_ROUND_ROBIN_Splitter_4672();
					butterfly_4531();
					butterfly_4532();
				WEIGHTED_ROUND_ROBIN_Joiner_4673();
			WEIGHTED_ROUND_ROBIN_Joiner_4788();
			WEIGHTED_ROUND_ROBIN_Splitter_4789();
				WEIGHTED_ROUND_ROBIN_Splitter_4674();
					butterfly_4533();
					butterfly_4534();
				WEIGHTED_ROUND_ROBIN_Joiner_4675();
				WEIGHTED_ROUND_ROBIN_Splitter_4676();
					butterfly_4535();
					butterfly_4536();
				WEIGHTED_ROUND_ROBIN_Joiner_4677();
			WEIGHTED_ROUND_ROBIN_Joiner_4790();
		WEIGHTED_ROUND_ROBIN_Joiner_4791();
		WEIGHTED_ROUND_ROBIN_Splitter_4678();
			WEIGHTED_ROUND_ROBIN_Splitter_4680();
				butterfly_4538();
				butterfly_4539();
				butterfly_4540();
				butterfly_4541();
			WEIGHTED_ROUND_ROBIN_Joiner_4681();
			WEIGHTED_ROUND_ROBIN_Splitter_4682();
				butterfly_4542();
				butterfly_4543();
				butterfly_4544();
				butterfly_4545();
			WEIGHTED_ROUND_ROBIN_Joiner_4683();
		WEIGHTED_ROUND_ROBIN_Joiner_4679();
		WEIGHTED_ROUND_ROBIN_Splitter_4797();
			magnitude_4799();
			magnitude_4800();
			magnitude_4801();
			magnitude_4802();
			magnitude_4803();
			magnitude_4804();
			magnitude_4805();
			magnitude_4806();
			magnitude_4807();
			magnitude_4808();
			magnitude_4809();
		WEIGHTED_ROUND_ROBIN_Joiner_4798();
		sink_4547();
	ENDFOR
	return EXIT_SUCCESS;
}
