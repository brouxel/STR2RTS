#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=1408 on the compile command line
#else
#if BUF_SIZEMAX < 1408
#error BUF_SIZEMAX too small, it must be at least 1408
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float A_re[32];
	float A_im[32];
	int idx;
} FloatSource_5138_t;
void FloatSource(buffer_float_t *chanout);
void FloatSource_5138();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_5439();
void WEIGHTED_ROUND_ROBIN_Splitter_5581();
void Butterfly(buffer_float_t *chanin, buffer_float_t *chanout);
void Butterfly_5583();
void Butterfly_5584();
void Butterfly_5585();
void Butterfly_5586();
void Butterfly_5587();
void Butterfly_5588();
void Butterfly_5589();
void Butterfly_5590();
void Butterfly_5591();
void Butterfly_5592();
void Butterfly_5593();
void WEIGHTED_ROUND_ROBIN_Joiner_5582();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_5440();
void WEIGHTED_ROUND_ROBIN_Splitter_5483();
void Pre_CollapsedDataParallel_1_5442();
void WEIGHTED_ROUND_ROBIN_Splitter_5594();
void Butterfly_5596();
void Butterfly_5597();
void Butterfly_5598();
void Butterfly_5599();
void Butterfly_5600();
void Butterfly_5601();
void Butterfly_5602();
void Butterfly_5603();
void WEIGHTED_ROUND_ROBIN_Joiner_5595();
void Post_CollapsedDataParallel_2_5443();
void WEIGHTED_ROUND_ROBIN_Splitter_5563();
void Pre_CollapsedDataParallel_1_5448();
void WEIGHTED_ROUND_ROBIN_Splitter_5604();
void Butterfly_5606();
void Butterfly_5607();
void Butterfly_5608();
void Butterfly_5609();
void WEIGHTED_ROUND_ROBIN_Joiner_5605();
void Post_CollapsedDataParallel_2_5449();
void Pre_CollapsedDataParallel_1_5451();
void WEIGHTED_ROUND_ROBIN_Splitter_5610();
void Butterfly_5612();
void Butterfly_5613();
void Butterfly_5614();
void Butterfly_5615();
void WEIGHTED_ROUND_ROBIN_Joiner_5611();
void Post_CollapsedDataParallel_2_5452();
void WEIGHTED_ROUND_ROBIN_Joiner_5564();
void Pre_CollapsedDataParallel_1_5445();
void WEIGHTED_ROUND_ROBIN_Splitter_5616();
void Butterfly_5618();
void Butterfly_5619();
void Butterfly_5620();
void Butterfly_5621();
void Butterfly_5622();
void Butterfly_5623();
void Butterfly_5624();
void Butterfly_5625();
void WEIGHTED_ROUND_ROBIN_Joiner_5617();
void Post_CollapsedDataParallel_2_5446();
void WEIGHTED_ROUND_ROBIN_Splitter_5565();
void Pre_CollapsedDataParallel_1_5454();
void WEIGHTED_ROUND_ROBIN_Splitter_5626();
void Butterfly_5628();
void Butterfly_5629();
void Butterfly_5630();
void Butterfly_5631();
void WEIGHTED_ROUND_ROBIN_Joiner_5627();
void Post_CollapsedDataParallel_2_5455();
void Pre_CollapsedDataParallel_1_5457();
void WEIGHTED_ROUND_ROBIN_Splitter_5632();
void Butterfly_5634();
void Butterfly_5635();
void Butterfly_5636();
void Butterfly_5637();
void WEIGHTED_ROUND_ROBIN_Joiner_5633();
void Post_CollapsedDataParallel_2_5458();
void WEIGHTED_ROUND_ROBIN_Joiner_5566();
void WEIGHTED_ROUND_ROBIN_Joiner_5567();
void WEIGHTED_ROUND_ROBIN_Splitter_5568();
void WEIGHTED_ROUND_ROBIN_Splitter_5569();
void Pre_CollapsedDataParallel_1_5460();
void WEIGHTED_ROUND_ROBIN_Splitter_5638();
void Butterfly_5640();
void Butterfly_5641();
void WEIGHTED_ROUND_ROBIN_Joiner_5639();
void Post_CollapsedDataParallel_2_5461();
void Pre_CollapsedDataParallel_1_5463();
void WEIGHTED_ROUND_ROBIN_Splitter_5642();
void Butterfly_5644();
void Butterfly_5645();
void WEIGHTED_ROUND_ROBIN_Joiner_5643();
void Post_CollapsedDataParallel_2_5464();
void Pre_CollapsedDataParallel_1_5466();
void WEIGHTED_ROUND_ROBIN_Splitter_5646();
void Butterfly_5648();
void Butterfly_5649();
void WEIGHTED_ROUND_ROBIN_Joiner_5647();
void Post_CollapsedDataParallel_2_5467();
void Pre_CollapsedDataParallel_1_5469();
void WEIGHTED_ROUND_ROBIN_Splitter_5650();
void Butterfly_5652();
void Butterfly_5653();
void WEIGHTED_ROUND_ROBIN_Joiner_5651();
void Post_CollapsedDataParallel_2_5470();
void WEIGHTED_ROUND_ROBIN_Joiner_5570();
void WEIGHTED_ROUND_ROBIN_Splitter_5571();
void Pre_CollapsedDataParallel_1_5472();
void WEIGHTED_ROUND_ROBIN_Splitter_5654();
void Butterfly_5656();
void Butterfly_5657();
void WEIGHTED_ROUND_ROBIN_Joiner_5655();
void Post_CollapsedDataParallel_2_5473();
void Pre_CollapsedDataParallel_1_5475();
void WEIGHTED_ROUND_ROBIN_Splitter_5658();
void Butterfly_5660();
void Butterfly_5661();
void WEIGHTED_ROUND_ROBIN_Joiner_5659();
void Post_CollapsedDataParallel_2_5476();
void Pre_CollapsedDataParallel_1_5478();
void WEIGHTED_ROUND_ROBIN_Splitter_5662();
void Butterfly_5664();
void Butterfly_5665();
void WEIGHTED_ROUND_ROBIN_Joiner_5663();
void Post_CollapsedDataParallel_2_5479();
void Pre_CollapsedDataParallel_1_5481();
void WEIGHTED_ROUND_ROBIN_Splitter_5666();
void Butterfly_5668();
void Butterfly_5669();
void WEIGHTED_ROUND_ROBIN_Joiner_5667();
void Post_CollapsedDataParallel_2_5482();
void WEIGHTED_ROUND_ROBIN_Joiner_5572();
void WEIGHTED_ROUND_ROBIN_Joiner_5573();
void WEIGHTED_ROUND_ROBIN_Splitter_5574();
void WEIGHTED_ROUND_ROBIN_Splitter_5575();
void Butterfly_5203();
void Butterfly_5204();
void Butterfly_5205();
void Butterfly_5206();
void Butterfly_5207();
void Butterfly_5208();
void Butterfly_5209();
void Butterfly_5210();
void WEIGHTED_ROUND_ROBIN_Joiner_5576();
void WEIGHTED_ROUND_ROBIN_Splitter_5577();
void Butterfly_5211();
void Butterfly_5212();
void Butterfly_5213();
void Butterfly_5214();
void Butterfly_5215();
void Butterfly_5216();
void Butterfly_5217();
void Butterfly_5218();
void WEIGHTED_ROUND_ROBIN_Joiner_5578();
void WEIGHTED_ROUND_ROBIN_Joiner_5579();
int BitReverse_5219_bitrev(int inp, int numbits);
void BitReverse(buffer_float_t *chanin, buffer_float_t *chanout);
void BitReverse_5219();
void FloatPrinter(buffer_float_t *chanin);
void FloatPrinter_5220();

#ifdef __cplusplus
}
#endif
#endif
