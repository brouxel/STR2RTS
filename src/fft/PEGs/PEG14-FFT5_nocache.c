#include "PEG14-FFT5_nocache.h"

buffer_complex_t SplitJoin100_SplitJoin75_SplitJoin75_AnonFilter_a0_2709_2944_3031_3048_join[2];
buffer_complex_t SplitJoin24_magnitude_Fiss_3023_3064_split[14];
buffer_complex_t Pre_CollapsedDataParallel_1_2816butterfly_2723;
buffer_complex_t SplitJoin80_SplitJoin55_SplitJoin55_AnonFilter_a0_2681_2930_3026_3042_split[2];
buffer_complex_t SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2661_2887_3017_3036_split[2];
buffer_complex_t SplitJoin90_SplitJoin65_SplitJoin65_AnonFilter_a0_2695_2937_3028_3045_join[2];
buffer_complex_t SplitJoin18_SplitJoin12_SplitJoin12_split2_2642_2895_2973_3056_split[2];
buffer_complex_t SplitJoin43_SplitJoin22_SplitJoin22_split2_2657_2903_2981_3063_join[4];
buffer_complex_t SplitJoin56_SplitJoin33_SplitJoin33_split2_2646_2912_2976_3059_split[2];
buffer_complex_t butterfly_2723Post_CollapsedDataParallel_2_2817;
buffer_complex_t SplitJoin90_SplitJoin65_SplitJoin65_AnonFilter_a0_2695_2937_3028_3045_split[2];
buffer_complex_t SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_2667_2890_3019_3039_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2840WEIGHTED_ROUND_ROBIN_Splitter_2983;
buffer_complex_t SplitJoin88_SplitJoin63_SplitJoin63_AnonFilter_a0_2693_2936_2972_3044_join[2];
buffer_complex_t SplitJoin56_SplitJoin33_SplitJoin33_split2_2646_2912_2976_3059_join[2];
buffer_complex_t SplitJoin78_SplitJoin53_SplitJoin53_AnonFilter_a0_2679_2929_3025_3041_split[2];
buffer_complex_t SplitJoin22_SplitJoin16_SplitJoin16_split2_2655_2898_2979_3062_split[4];
buffer_complex_t SplitJoin43_SplitJoin22_SplitJoin22_split2_2657_2903_2981_3063_split[4];
buffer_complex_t SplitJoin84_SplitJoin59_SplitJoin59_AnonFilter_a0_2687_2933_3027_3043_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2882WEIGHTED_ROUND_ROBIN_Splitter_3000;
buffer_complex_t SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2665_2889_3018_3038_split[2];
buffer_complex_t Pre_CollapsedDataParallel_1_2828butterfly_2727;
buffer_complex_t SplitJoin10_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_Hier_3020_3051_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_2834butterfly_2729;
buffer_complex_t Pre_CollapsedDataParallel_1_2831butterfly_2728;
buffer_complex_t SplitJoin92_SplitJoin67_SplitJoin67_AnonFilter_a0_2697_2938_3029_3046_split[2];
buffer_complex_t SplitJoin74_SplitJoin49_SplitJoin49_AnonFilter_a0_2673_2926_3024_3040_join[2];
buffer_complex_t SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2663_2888_2971_3037_split[2];
buffer_complex_t SplitJoin12_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child0_2974_3052_join[4];
buffer_complex_t Pre_CollapsedDataParallel_1_2825butterfly_2726;
buffer_float_t SplitJoin24_magnitude_Fiss_3023_3064_join[14];
buffer_complex_t SplitJoin74_SplitJoin49_SplitJoin49_AnonFilter_a0_2673_2926_3024_3040_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2997WEIGHTED_ROUND_ROBIN_Splitter_2839;
buffer_complex_t butterfly_2727Post_CollapsedDataParallel_2_2829;
buffer_complex_t butterfly_2725Post_CollapsedDataParallel_2_2823;
buffer_complex_t SplitJoin88_SplitJoin63_SplitJoin63_AnonFilter_a0_2693_2936_2972_3044_split[2];
buffer_complex_t SplitJoin96_SplitJoin71_SplitJoin71_AnonFilter_a0_2703_2941_3030_3047_split[2];
buffer_complex_t SplitJoin78_SplitJoin53_SplitJoin53_AnonFilter_a0_2679_2929_3025_3041_join[2];
buffer_complex_t SplitJoin54_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child1_2982_3058_join[2];
buffer_complex_t SplitJoin18_SplitJoin12_SplitJoin12_split2_2642_2895_2973_3056_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_2822butterfly_2725;
buffer_complex_t SplitJoin50_SplitJoin29_SplitJoin29_split2_2644_2909_2975_3057_split[2];
buffer_complex_t SplitJoin80_SplitJoin55_SplitJoin55_AnonFilter_a0_2681_2930_3026_3042_join[2];
buffer_complex_t butterfly_2730Post_CollapsedDataParallel_2_2838;
buffer_complex_t SplitJoin0_source_Fiss_3016_3035_split[2];
buffer_complex_t SplitJoin102_SplitJoin77_SplitJoin77_AnonFilter_a0_2711_2945_3032_3049_split[2];
buffer_complex_t SplitJoin106_SplitJoin81_SplitJoin81_AnonFilter_a0_2717_2948_3033_3050_split[2];
buffer_complex_t SplitJoin106_SplitJoin81_SplitJoin81_AnonFilter_a0_2717_2948_3033_3050_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_2819butterfly_2724;
buffer_complex_t SplitJoin50_SplitJoin29_SplitJoin29_split2_2644_2909_2975_3057_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3001sink_2750;
buffer_complex_t SplitJoin100_SplitJoin75_SplitJoin75_AnonFilter_a0_2709_2944_3031_3048_split[2];
buffer_complex_t SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2663_2888_2971_3037_join[2];
buffer_complex_t SplitJoin14_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_Hier_3021_3054_split[2];
buffer_complex_t SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2665_2889_3018_3038_join[2];
buffer_complex_t SplitJoin14_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_Hier_3021_3054_join[2];
buffer_complex_t SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_2667_2890_3019_3039_split[2];
buffer_complex_t butterfly_2728Post_CollapsedDataParallel_2_2832;
buffer_complex_t SplitJoin20_SplitJoin14_SplitJoin14_split1_2653_2897_3022_3061_split[2];
buffer_complex_t SplitJoin12_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child0_2974_3052_split[4];
buffer_complex_t butterfly_2729Post_CollapsedDataParallel_2_2835;
buffer_complex_t SplitJoin60_SplitJoin37_SplitJoin37_split2_2648_2915_2978_3060_split[2];
buffer_complex_t SplitJoin84_SplitJoin59_SplitJoin59_AnonFilter_a0_2687_2933_3027_3043_split[2];
buffer_complex_t SplitJoin60_SplitJoin37_SplitJoin37_split2_2648_2915_2978_3060_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_2837butterfly_2730;
buffer_complex_t SplitJoin54_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child1_2982_3058_split[2];
buffer_complex_t SplitJoin22_SplitJoin16_SplitJoin16_split2_2655_2898_2979_3062_join[4];
buffer_complex_t SplitJoin0_source_Fiss_3016_3035_join[2];
buffer_complex_t SplitJoin10_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_Hier_3020_3051_split[2];
buffer_complex_t SplitJoin16_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child0_2980_3055_join[2];
buffer_complex_t SplitJoin67_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child1_2977_3053_join[4];
buffer_complex_t SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2661_2887_3017_3036_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2988WEIGHTED_ROUND_ROBIN_Splitter_2989;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2994WEIGHTED_ROUND_ROBIN_Splitter_2881;
buffer_complex_t SplitJoin67_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child1_2977_3053_split[4];
buffer_complex_t butterfly_2726Post_CollapsedDataParallel_2_2826;
buffer_complex_t SplitJoin92_SplitJoin67_SplitJoin67_AnonFilter_a0_2697_2938_3029_3046_join[2];
buffer_complex_t SplitJoin102_SplitJoin77_SplitJoin77_AnonFilter_a0_2711_2945_3032_3049_join[2];
buffer_complex_t butterfly_2724Post_CollapsedDataParallel_2_2820;
buffer_complex_t SplitJoin16_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child0_2980_3055_split[2];
buffer_complex_t SplitJoin96_SplitJoin71_SplitJoin71_AnonFilter_a0_2703_2941_3030_3047_join[2];
buffer_complex_t SplitJoin20_SplitJoin14_SplitJoin14_split1_2653_2897_3022_3061_join[2];



void source_2998(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t t;
		t.imag = 0.0 ; 
		t.real = 0.9501 ; 
		push_complex(&SplitJoin0_source_Fiss_3016_3035_join[0], t) ; 
		t.real = 0.2311 ; 
		push_complex(&SplitJoin0_source_Fiss_3016_3035_join[0], t) ; 
		t.real = 0.6068 ; 
		push_complex(&SplitJoin0_source_Fiss_3016_3035_join[0], t) ; 
		t.real = 0.486 ; 
		push_complex(&SplitJoin0_source_Fiss_3016_3035_join[0], t) ; 
		t.real = 0.8913 ; 
		push_complex(&SplitJoin0_source_Fiss_3016_3035_join[0], t) ; 
		t.real = 0.7621 ; 
		push_complex(&SplitJoin0_source_Fiss_3016_3035_join[0], t) ; 
		t.real = 0.4565 ; 
		push_complex(&SplitJoin0_source_Fiss_3016_3035_join[0], t) ; 
		t.real = 0.0185 ; 
		push_complex(&SplitJoin0_source_Fiss_3016_3035_join[0], t) ; 
	}
	ENDFOR
}

void source_2999(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t t;
		t.imag = 0.0 ; 
		t.real = 0.9501 ; 
		push_complex(&SplitJoin0_source_Fiss_3016_3035_join[1], t) ; 
		t.real = 0.2311 ; 
		push_complex(&SplitJoin0_source_Fiss_3016_3035_join[1], t) ; 
		t.real = 0.6068 ; 
		push_complex(&SplitJoin0_source_Fiss_3016_3035_join[1], t) ; 
		t.real = 0.486 ; 
		push_complex(&SplitJoin0_source_Fiss_3016_3035_join[1], t) ; 
		t.real = 0.8913 ; 
		push_complex(&SplitJoin0_source_Fiss_3016_3035_join[1], t) ; 
		t.real = 0.7621 ; 
		push_complex(&SplitJoin0_source_Fiss_3016_3035_join[1], t) ; 
		t.real = 0.4565 ; 
		push_complex(&SplitJoin0_source_Fiss_3016_3035_join[1], t) ; 
		t.real = 0.0185 ; 
		push_complex(&SplitJoin0_source_Fiss_3016_3035_join[1], t) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2996() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_2997() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2997WEIGHTED_ROUND_ROBIN_Splitter_2839, pop_complex(&SplitJoin0_source_Fiss_3016_3035_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2997WEIGHTED_ROUND_ROBIN_Splitter_2839, pop_complex(&SplitJoin0_source_Fiss_3016_3035_join[1]));
		ENDFOR
	ENDFOR
}}

void Identity_2669(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t __tmp790 = pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_2667_2890_3019_3039_split[0]);
		push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_2667_2890_3019_3039_join[0], __tmp790) ; 
	}
	ENDFOR
}

void Identity_2671(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t __tmp793 = pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_2667_2890_3019_3039_split[1]);
		push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_2667_2890_3019_3039_join[1], __tmp793) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2845() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_2667_2890_3019_3039_split[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2665_2889_3018_3038_split[0]));
		push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_2667_2890_3019_3039_split[1], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2665_2889_3018_3038_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2846() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2665_2889_3018_3038_join[0], pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_2667_2890_3019_3039_join[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2665_2889_3018_3038_join[0], pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_2667_2890_3019_3039_join[1]));
	ENDFOR
}}

void Identity_2675(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t __tmp801 = pop_complex(&SplitJoin74_SplitJoin49_SplitJoin49_AnonFilter_a0_2673_2926_3024_3040_split[0]);
		push_complex(&SplitJoin74_SplitJoin49_SplitJoin49_AnonFilter_a0_2673_2926_3024_3040_join[0], __tmp801) ; 
	}
	ENDFOR
}

void Identity_2677(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t __tmp804 = pop_complex(&SplitJoin74_SplitJoin49_SplitJoin49_AnonFilter_a0_2673_2926_3024_3040_split[1]);
		push_complex(&SplitJoin74_SplitJoin49_SplitJoin49_AnonFilter_a0_2673_2926_3024_3040_join[1], __tmp804) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2847() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin74_SplitJoin49_SplitJoin49_AnonFilter_a0_2673_2926_3024_3040_split[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2665_2889_3018_3038_split[1]));
		push_complex(&SplitJoin74_SplitJoin49_SplitJoin49_AnonFilter_a0_2673_2926_3024_3040_split[1], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2665_2889_3018_3038_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2848() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2665_2889_3018_3038_join[1], pop_complex(&SplitJoin74_SplitJoin49_SplitJoin49_AnonFilter_a0_2673_2926_3024_3040_join[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2665_2889_3018_3038_join[1], pop_complex(&SplitJoin74_SplitJoin49_SplitJoin49_AnonFilter_a0_2673_2926_3024_3040_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_2843() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2665_2889_3018_3038_split[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2663_2888_2971_3037_split[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2665_2889_3018_3038_split[1], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2663_2888_2971_3037_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2844() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2663_2888_2971_3037_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2665_2889_3018_3038_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2663_2888_2971_3037_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2665_2889_3018_3038_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2663_2888_2971_3037_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2665_2889_3018_3038_join[1]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2663_2888_2971_3037_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2665_2889_3018_3038_join[1]));
	ENDFOR
}}

void Identity_2683(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t __tmp817 = pop_complex(&SplitJoin80_SplitJoin55_SplitJoin55_AnonFilter_a0_2681_2930_3026_3042_split[0]);
		push_complex(&SplitJoin80_SplitJoin55_SplitJoin55_AnonFilter_a0_2681_2930_3026_3042_join[0], __tmp817) ; 
	}
	ENDFOR
}

void Identity_2685(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t __tmp820 = pop_complex(&SplitJoin80_SplitJoin55_SplitJoin55_AnonFilter_a0_2681_2930_3026_3042_split[1]);
		push_complex(&SplitJoin80_SplitJoin55_SplitJoin55_AnonFilter_a0_2681_2930_3026_3042_join[1], __tmp820) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2851() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin80_SplitJoin55_SplitJoin55_AnonFilter_a0_2681_2930_3026_3042_split[0], pop_complex(&SplitJoin78_SplitJoin53_SplitJoin53_AnonFilter_a0_2679_2929_3025_3041_split[0]));
		push_complex(&SplitJoin80_SplitJoin55_SplitJoin55_AnonFilter_a0_2681_2930_3026_3042_split[1], pop_complex(&SplitJoin78_SplitJoin53_SplitJoin53_AnonFilter_a0_2679_2929_3025_3041_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2852() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin78_SplitJoin53_SplitJoin53_AnonFilter_a0_2679_2929_3025_3041_join[0], pop_complex(&SplitJoin80_SplitJoin55_SplitJoin55_AnonFilter_a0_2681_2930_3026_3042_join[0]));
		push_complex(&SplitJoin78_SplitJoin53_SplitJoin53_AnonFilter_a0_2679_2929_3025_3041_join[0], pop_complex(&SplitJoin80_SplitJoin55_SplitJoin55_AnonFilter_a0_2681_2930_3026_3042_join[1]));
	ENDFOR
}}

void Identity_2689(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t __tmp828 = pop_complex(&SplitJoin84_SplitJoin59_SplitJoin59_AnonFilter_a0_2687_2933_3027_3043_split[0]);
		push_complex(&SplitJoin84_SplitJoin59_SplitJoin59_AnonFilter_a0_2687_2933_3027_3043_join[0], __tmp828) ; 
	}
	ENDFOR
}

void Identity_2691(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t __tmp831 = pop_complex(&SplitJoin84_SplitJoin59_SplitJoin59_AnonFilter_a0_2687_2933_3027_3043_split[1]);
		push_complex(&SplitJoin84_SplitJoin59_SplitJoin59_AnonFilter_a0_2687_2933_3027_3043_join[1], __tmp831) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2853() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin84_SplitJoin59_SplitJoin59_AnonFilter_a0_2687_2933_3027_3043_split[0], pop_complex(&SplitJoin78_SplitJoin53_SplitJoin53_AnonFilter_a0_2679_2929_3025_3041_split[1]));
		push_complex(&SplitJoin84_SplitJoin59_SplitJoin59_AnonFilter_a0_2687_2933_3027_3043_split[1], pop_complex(&SplitJoin78_SplitJoin53_SplitJoin53_AnonFilter_a0_2679_2929_3025_3041_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2854() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin78_SplitJoin53_SplitJoin53_AnonFilter_a0_2679_2929_3025_3041_join[1], pop_complex(&SplitJoin84_SplitJoin59_SplitJoin59_AnonFilter_a0_2687_2933_3027_3043_join[0]));
		push_complex(&SplitJoin78_SplitJoin53_SplitJoin53_AnonFilter_a0_2679_2929_3025_3041_join[1], pop_complex(&SplitJoin84_SplitJoin59_SplitJoin59_AnonFilter_a0_2687_2933_3027_3043_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_2849() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		push_complex(&SplitJoin78_SplitJoin53_SplitJoin53_AnonFilter_a0_2679_2929_3025_3041_split[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2663_2888_2971_3037_split[1]));
		push_complex(&SplitJoin78_SplitJoin53_SplitJoin53_AnonFilter_a0_2679_2929_3025_3041_split[1], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2663_2888_2971_3037_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2850() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2663_2888_2971_3037_join[1], pop_complex(&SplitJoin78_SplitJoin53_SplitJoin53_AnonFilter_a0_2679_2929_3025_3041_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2663_2888_2971_3037_join[1], pop_complex(&SplitJoin78_SplitJoin53_SplitJoin53_AnonFilter_a0_2679_2929_3025_3041_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2663_2888_2971_3037_join[1], pop_complex(&SplitJoin78_SplitJoin53_SplitJoin53_AnonFilter_a0_2679_2929_3025_3041_join[1]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2663_2888_2971_3037_join[1], pop_complex(&SplitJoin78_SplitJoin53_SplitJoin53_AnonFilter_a0_2679_2929_3025_3041_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_2841() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 28, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2663_2888_2971_3037_split[0], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2661_2887_3017_3036_split[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2663_2888_2971_3037_split[1], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2661_2887_3017_3036_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2842() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2661_2887_3017_3036_join[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2663_2888_2971_3037_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2661_2887_3017_3036_join[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2663_2888_2971_3037_join[1]));
		ENDFOR
	ENDFOR
}}

void Identity_2699(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t __tmp849 = pop_complex(&SplitJoin92_SplitJoin67_SplitJoin67_AnonFilter_a0_2697_2938_3029_3046_split[0]);
		push_complex(&SplitJoin92_SplitJoin67_SplitJoin67_AnonFilter_a0_2697_2938_3029_3046_join[0], __tmp849) ; 
	}
	ENDFOR
}

void Identity_2701(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t __tmp852 = pop_complex(&SplitJoin92_SplitJoin67_SplitJoin67_AnonFilter_a0_2697_2938_3029_3046_split[1]);
		push_complex(&SplitJoin92_SplitJoin67_SplitJoin67_AnonFilter_a0_2697_2938_3029_3046_join[1], __tmp852) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2859() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin92_SplitJoin67_SplitJoin67_AnonFilter_a0_2697_2938_3029_3046_split[0], pop_complex(&SplitJoin90_SplitJoin65_SplitJoin65_AnonFilter_a0_2695_2937_3028_3045_split[0]));
		push_complex(&SplitJoin92_SplitJoin67_SplitJoin67_AnonFilter_a0_2697_2938_3029_3046_split[1], pop_complex(&SplitJoin90_SplitJoin65_SplitJoin65_AnonFilter_a0_2695_2937_3028_3045_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2860() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin90_SplitJoin65_SplitJoin65_AnonFilter_a0_2695_2937_3028_3045_join[0], pop_complex(&SplitJoin92_SplitJoin67_SplitJoin67_AnonFilter_a0_2697_2938_3029_3046_join[0]));
		push_complex(&SplitJoin90_SplitJoin65_SplitJoin65_AnonFilter_a0_2695_2937_3028_3045_join[0], pop_complex(&SplitJoin92_SplitJoin67_SplitJoin67_AnonFilter_a0_2697_2938_3029_3046_join[1]));
	ENDFOR
}}

void Identity_2705(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t __tmp860 = pop_complex(&SplitJoin96_SplitJoin71_SplitJoin71_AnonFilter_a0_2703_2941_3030_3047_split[0]);
		push_complex(&SplitJoin96_SplitJoin71_SplitJoin71_AnonFilter_a0_2703_2941_3030_3047_join[0], __tmp860) ; 
	}
	ENDFOR
}

void Identity_2707(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t __tmp863 = pop_complex(&SplitJoin96_SplitJoin71_SplitJoin71_AnonFilter_a0_2703_2941_3030_3047_split[1]);
		push_complex(&SplitJoin96_SplitJoin71_SplitJoin71_AnonFilter_a0_2703_2941_3030_3047_join[1], __tmp863) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2861() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin96_SplitJoin71_SplitJoin71_AnonFilter_a0_2703_2941_3030_3047_split[0], pop_complex(&SplitJoin90_SplitJoin65_SplitJoin65_AnonFilter_a0_2695_2937_3028_3045_split[1]));
		push_complex(&SplitJoin96_SplitJoin71_SplitJoin71_AnonFilter_a0_2703_2941_3030_3047_split[1], pop_complex(&SplitJoin90_SplitJoin65_SplitJoin65_AnonFilter_a0_2695_2937_3028_3045_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2862() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin90_SplitJoin65_SplitJoin65_AnonFilter_a0_2695_2937_3028_3045_join[1], pop_complex(&SplitJoin96_SplitJoin71_SplitJoin71_AnonFilter_a0_2703_2941_3030_3047_join[0]));
		push_complex(&SplitJoin90_SplitJoin65_SplitJoin65_AnonFilter_a0_2695_2937_3028_3045_join[1], pop_complex(&SplitJoin96_SplitJoin71_SplitJoin71_AnonFilter_a0_2703_2941_3030_3047_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_2857() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		push_complex(&SplitJoin90_SplitJoin65_SplitJoin65_AnonFilter_a0_2695_2937_3028_3045_split[0], pop_complex(&SplitJoin88_SplitJoin63_SplitJoin63_AnonFilter_a0_2693_2936_2972_3044_split[0]));
		push_complex(&SplitJoin90_SplitJoin65_SplitJoin65_AnonFilter_a0_2695_2937_3028_3045_split[1], pop_complex(&SplitJoin88_SplitJoin63_SplitJoin63_AnonFilter_a0_2693_2936_2972_3044_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2858() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin88_SplitJoin63_SplitJoin63_AnonFilter_a0_2693_2936_2972_3044_join[0], pop_complex(&SplitJoin90_SplitJoin65_SplitJoin65_AnonFilter_a0_2695_2937_3028_3045_join[0]));
		push_complex(&SplitJoin88_SplitJoin63_SplitJoin63_AnonFilter_a0_2693_2936_2972_3044_join[0], pop_complex(&SplitJoin90_SplitJoin65_SplitJoin65_AnonFilter_a0_2695_2937_3028_3045_join[0]));
		push_complex(&SplitJoin88_SplitJoin63_SplitJoin63_AnonFilter_a0_2693_2936_2972_3044_join[0], pop_complex(&SplitJoin90_SplitJoin65_SplitJoin65_AnonFilter_a0_2695_2937_3028_3045_join[1]));
		push_complex(&SplitJoin88_SplitJoin63_SplitJoin63_AnonFilter_a0_2693_2936_2972_3044_join[0], pop_complex(&SplitJoin90_SplitJoin65_SplitJoin65_AnonFilter_a0_2695_2937_3028_3045_join[1]));
	ENDFOR
}}

void Identity_2713(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t __tmp876 = pop_complex(&SplitJoin102_SplitJoin77_SplitJoin77_AnonFilter_a0_2711_2945_3032_3049_split[0]);
		push_complex(&SplitJoin102_SplitJoin77_SplitJoin77_AnonFilter_a0_2711_2945_3032_3049_join[0], __tmp876) ; 
	}
	ENDFOR
}

void Identity_2715(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t __tmp879 = pop_complex(&SplitJoin102_SplitJoin77_SplitJoin77_AnonFilter_a0_2711_2945_3032_3049_split[1]);
		push_complex(&SplitJoin102_SplitJoin77_SplitJoin77_AnonFilter_a0_2711_2945_3032_3049_join[1], __tmp879) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2865() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin102_SplitJoin77_SplitJoin77_AnonFilter_a0_2711_2945_3032_3049_split[0], pop_complex(&SplitJoin100_SplitJoin75_SplitJoin75_AnonFilter_a0_2709_2944_3031_3048_split[0]));
		push_complex(&SplitJoin102_SplitJoin77_SplitJoin77_AnonFilter_a0_2711_2945_3032_3049_split[1], pop_complex(&SplitJoin100_SplitJoin75_SplitJoin75_AnonFilter_a0_2709_2944_3031_3048_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2866() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin100_SplitJoin75_SplitJoin75_AnonFilter_a0_2709_2944_3031_3048_join[0], pop_complex(&SplitJoin102_SplitJoin77_SplitJoin77_AnonFilter_a0_2711_2945_3032_3049_join[0]));
		push_complex(&SplitJoin100_SplitJoin75_SplitJoin75_AnonFilter_a0_2709_2944_3031_3048_join[0], pop_complex(&SplitJoin102_SplitJoin77_SplitJoin77_AnonFilter_a0_2711_2945_3032_3049_join[1]));
	ENDFOR
}}

void Identity_2719(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t __tmp887 = pop_complex(&SplitJoin106_SplitJoin81_SplitJoin81_AnonFilter_a0_2717_2948_3033_3050_split[0]);
		push_complex(&SplitJoin106_SplitJoin81_SplitJoin81_AnonFilter_a0_2717_2948_3033_3050_join[0], __tmp887) ; 
	}
	ENDFOR
}

void Identity_2721(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t __tmp890 = pop_complex(&SplitJoin106_SplitJoin81_SplitJoin81_AnonFilter_a0_2717_2948_3033_3050_split[1]);
		push_complex(&SplitJoin106_SplitJoin81_SplitJoin81_AnonFilter_a0_2717_2948_3033_3050_join[1], __tmp890) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2867() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin106_SplitJoin81_SplitJoin81_AnonFilter_a0_2717_2948_3033_3050_split[0], pop_complex(&SplitJoin100_SplitJoin75_SplitJoin75_AnonFilter_a0_2709_2944_3031_3048_split[1]));
		push_complex(&SplitJoin106_SplitJoin81_SplitJoin81_AnonFilter_a0_2717_2948_3033_3050_split[1], pop_complex(&SplitJoin100_SplitJoin75_SplitJoin75_AnonFilter_a0_2709_2944_3031_3048_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2868() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin100_SplitJoin75_SplitJoin75_AnonFilter_a0_2709_2944_3031_3048_join[1], pop_complex(&SplitJoin106_SplitJoin81_SplitJoin81_AnonFilter_a0_2717_2948_3033_3050_join[0]));
		push_complex(&SplitJoin100_SplitJoin75_SplitJoin75_AnonFilter_a0_2709_2944_3031_3048_join[1], pop_complex(&SplitJoin106_SplitJoin81_SplitJoin81_AnonFilter_a0_2717_2948_3033_3050_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_2863() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		push_complex(&SplitJoin100_SplitJoin75_SplitJoin75_AnonFilter_a0_2709_2944_3031_3048_split[0], pop_complex(&SplitJoin88_SplitJoin63_SplitJoin63_AnonFilter_a0_2693_2936_2972_3044_split[1]));
		push_complex(&SplitJoin100_SplitJoin75_SplitJoin75_AnonFilter_a0_2709_2944_3031_3048_split[1], pop_complex(&SplitJoin88_SplitJoin63_SplitJoin63_AnonFilter_a0_2693_2936_2972_3044_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2864() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin88_SplitJoin63_SplitJoin63_AnonFilter_a0_2693_2936_2972_3044_join[1], pop_complex(&SplitJoin100_SplitJoin75_SplitJoin75_AnonFilter_a0_2709_2944_3031_3048_join[0]));
		push_complex(&SplitJoin88_SplitJoin63_SplitJoin63_AnonFilter_a0_2693_2936_2972_3044_join[1], pop_complex(&SplitJoin100_SplitJoin75_SplitJoin75_AnonFilter_a0_2709_2944_3031_3048_join[0]));
		push_complex(&SplitJoin88_SplitJoin63_SplitJoin63_AnonFilter_a0_2693_2936_2972_3044_join[1], pop_complex(&SplitJoin100_SplitJoin75_SplitJoin75_AnonFilter_a0_2709_2944_3031_3048_join[1]));
		push_complex(&SplitJoin88_SplitJoin63_SplitJoin63_AnonFilter_a0_2693_2936_2972_3044_join[1], pop_complex(&SplitJoin100_SplitJoin75_SplitJoin75_AnonFilter_a0_2709_2944_3031_3048_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_2855() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 28, __iter_steady_++)
		push_complex(&SplitJoin88_SplitJoin63_SplitJoin63_AnonFilter_a0_2693_2936_2972_3044_split[0], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2661_2887_3017_3036_split[1]));
		push_complex(&SplitJoin88_SplitJoin63_SplitJoin63_AnonFilter_a0_2693_2936_2972_3044_split[1], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2661_2887_3017_3036_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2856() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2661_2887_3017_3036_join[1], pop_complex(&SplitJoin88_SplitJoin63_SplitJoin63_AnonFilter_a0_2693_2936_2972_3044_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2661_2887_3017_3036_join[1], pop_complex(&SplitJoin88_SplitJoin63_SplitJoin63_AnonFilter_a0_2693_2936_2972_3044_join[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_2839() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 56, __iter_steady_++)
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2661_2887_3017_3036_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2997WEIGHTED_ROUND_ROBIN_Splitter_2839));
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2661_2887_3017_3036_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2997WEIGHTED_ROUND_ROBIN_Splitter_2839));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2840() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2840WEIGHTED_ROUND_ROBIN_Splitter_2983, pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2661_2887_3017_3036_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2840WEIGHTED_ROUND_ROBIN_Splitter_2983, pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2661_2887_3017_3036_join[1]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_2816(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
 {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
			push_complex(&Pre_CollapsedDataParallel_1_2816butterfly_2723, peek_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child0_2974_3052_split[0], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
		}
		ENDFOR
	}
	}
	}
		pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child0_2974_3052_split[0]) ; 
	}
	ENDFOR
}

void butterfly_2723(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_2816butterfly_2723));
		complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_2816butterfly_2723));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&butterfly_2723Post_CollapsedDataParallel_2_2817, __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&butterfly_2723Post_CollapsedDataParallel_2_2817, __sa2) ; 
	}
	ENDFOR
}

void Post_CollapsedDataParallel_2_2817(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 2, _k++) {
 {
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child0_2974_3052_join[0], peek_complex(&butterfly_2723Post_CollapsedDataParallel_2_2817, (_k + 0))) ; 
		}
		}
		ENDFOR
	}
	}
		pop_complex(&butterfly_2723Post_CollapsedDataParallel_2_2817) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_2819(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
 {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
			push_complex(&Pre_CollapsedDataParallel_1_2819butterfly_2724, peek_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child0_2974_3052_split[1], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
		}
		ENDFOR
	}
	}
	}
		pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child0_2974_3052_split[1]) ; 
	}
	ENDFOR
}

void butterfly_2724(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_2819butterfly_2724));
		complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_2819butterfly_2724));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&butterfly_2724Post_CollapsedDataParallel_2_2820, __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&butterfly_2724Post_CollapsedDataParallel_2_2820, __sa2) ; 
	}
	ENDFOR
}

void Post_CollapsedDataParallel_2_2820(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 2, _k++) {
 {
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child0_2974_3052_join[1], peek_complex(&butterfly_2724Post_CollapsedDataParallel_2_2820, (_k + 0))) ; 
		}
		}
		ENDFOR
	}
	}
		pop_complex(&butterfly_2724Post_CollapsedDataParallel_2_2820) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_2822(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
 {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
			push_complex(&Pre_CollapsedDataParallel_1_2822butterfly_2725, peek_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child0_2974_3052_split[2], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
		}
		ENDFOR
	}
	}
	}
		pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child0_2974_3052_split[2]) ; 
	}
	ENDFOR
}

void butterfly_2725(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_2822butterfly_2725));
		complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_2822butterfly_2725));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&butterfly_2725Post_CollapsedDataParallel_2_2823, __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&butterfly_2725Post_CollapsedDataParallel_2_2823, __sa2) ; 
	}
	ENDFOR
}

void Post_CollapsedDataParallel_2_2823(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 2, _k++) {
 {
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child0_2974_3052_join[2], peek_complex(&butterfly_2725Post_CollapsedDataParallel_2_2823, (_k + 0))) ; 
		}
		}
		ENDFOR
	}
	}
		pop_complex(&butterfly_2725Post_CollapsedDataParallel_2_2823) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_2825(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
 {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
			push_complex(&Pre_CollapsedDataParallel_1_2825butterfly_2726, peek_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child0_2974_3052_split[3], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
		}
		ENDFOR
	}
	}
	}
		pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child0_2974_3052_split[3]) ; 
	}
	ENDFOR
}

void butterfly_2726(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_2825butterfly_2726));
		complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_2825butterfly_2726));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&butterfly_2726Post_CollapsedDataParallel_2_2826, __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&butterfly_2726Post_CollapsedDataParallel_2_2826, __sa2) ; 
	}
	ENDFOR
}

void Post_CollapsedDataParallel_2_2826(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 2, _k++) {
 {
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child0_2974_3052_join[3], peek_complex(&butterfly_2726Post_CollapsedDataParallel_2_2826, (_k + 0))) ; 
		}
		}
		ENDFOR
	}
	}
		pop_complex(&butterfly_2726Post_CollapsedDataParallel_2_2826) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2984() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child0_2974_3052_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_Hier_3020_3051_split[0]));
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child0_2974_3052_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_Hier_3020_3051_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2985() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_Hier_3020_3051_join[0], pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child0_2974_3052_join[__iter_]));
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_Hier_3020_3051_join[0], pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child0_2974_3052_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_2828(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
 {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
			push_complex(&Pre_CollapsedDataParallel_1_2828butterfly_2727, peek_complex(&SplitJoin67_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child1_2977_3053_split[0], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
		}
		ENDFOR
	}
	}
	}
		pop_complex(&SplitJoin67_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child1_2977_3053_split[0]) ; 
	}
	ENDFOR
}

void butterfly_2727(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_2828butterfly_2727));
		complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_2828butterfly_2727));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&butterfly_2727Post_CollapsedDataParallel_2_2829, __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&butterfly_2727Post_CollapsedDataParallel_2_2829, __sa2) ; 
	}
	ENDFOR
}

void Post_CollapsedDataParallel_2_2829(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 2, _k++) {
 {
			push_complex(&SplitJoin67_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child1_2977_3053_join[0], peek_complex(&butterfly_2727Post_CollapsedDataParallel_2_2829, (_k + 0))) ; 
		}
		}
		ENDFOR
	}
	}
		pop_complex(&butterfly_2727Post_CollapsedDataParallel_2_2829) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_2831(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
 {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
			push_complex(&Pre_CollapsedDataParallel_1_2831butterfly_2728, peek_complex(&SplitJoin67_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child1_2977_3053_split[1], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
		}
		ENDFOR
	}
	}
	}
		pop_complex(&SplitJoin67_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child1_2977_3053_split[1]) ; 
	}
	ENDFOR
}

void butterfly_2728(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_2831butterfly_2728));
		complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_2831butterfly_2728));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&butterfly_2728Post_CollapsedDataParallel_2_2832, __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&butterfly_2728Post_CollapsedDataParallel_2_2832, __sa2) ; 
	}
	ENDFOR
}

void Post_CollapsedDataParallel_2_2832(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 2, _k++) {
 {
			push_complex(&SplitJoin67_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child1_2977_3053_join[1], peek_complex(&butterfly_2728Post_CollapsedDataParallel_2_2832, (_k + 0))) ; 
		}
		}
		ENDFOR
	}
	}
		pop_complex(&butterfly_2728Post_CollapsedDataParallel_2_2832) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_2834(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
 {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
			push_complex(&Pre_CollapsedDataParallel_1_2834butterfly_2729, peek_complex(&SplitJoin67_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child1_2977_3053_split[2], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
		}
		ENDFOR
	}
	}
	}
		pop_complex(&SplitJoin67_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child1_2977_3053_split[2]) ; 
	}
	ENDFOR
}

void butterfly_2729(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_2834butterfly_2729));
		complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_2834butterfly_2729));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&butterfly_2729Post_CollapsedDataParallel_2_2835, __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&butterfly_2729Post_CollapsedDataParallel_2_2835, __sa2) ; 
	}
	ENDFOR
}

void Post_CollapsedDataParallel_2_2835(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 2, _k++) {
 {
			push_complex(&SplitJoin67_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child1_2977_3053_join[2], peek_complex(&butterfly_2729Post_CollapsedDataParallel_2_2835, (_k + 0))) ; 
		}
		}
		ENDFOR
	}
	}
		pop_complex(&butterfly_2729Post_CollapsedDataParallel_2_2835) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_2837(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
 {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
			push_complex(&Pre_CollapsedDataParallel_1_2837butterfly_2730, peek_complex(&SplitJoin67_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child1_2977_3053_split[3], (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
		}
		ENDFOR
	}
	}
	}
		pop_complex(&SplitJoin67_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child1_2977_3053_split[3]) ; 
	}
	ENDFOR
}

void butterfly_2730(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_2837butterfly_2730));
		complex_t two = ((complex_t) pop_complex(&Pre_CollapsedDataParallel_1_2837butterfly_2730));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&butterfly_2730Post_CollapsedDataParallel_2_2838, __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&butterfly_2730Post_CollapsedDataParallel_2_2838, __sa2) ; 
	}
	ENDFOR
}

void Post_CollapsedDataParallel_2_2838(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 2, _k++) {
 {
			push_complex(&SplitJoin67_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child1_2977_3053_join[3], peek_complex(&butterfly_2730Post_CollapsedDataParallel_2_2838, (_k + 0))) ; 
		}
		}
		ENDFOR
	}
	}
		pop_complex(&butterfly_2730Post_CollapsedDataParallel_2_2838) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2986() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin67_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child1_2977_3053_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_Hier_3020_3051_split[1]));
			push_complex(&SplitJoin67_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child1_2977_3053_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_Hier_3020_3051_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2987() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_Hier_3020_3051_join[1], pop_complex(&SplitJoin67_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child1_2977_3053_join[__iter_]));
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_Hier_3020_3051_join[1], pop_complex(&SplitJoin67_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child1_2977_3053_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_2983() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_Hier_3020_3051_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2840WEIGHTED_ROUND_ROBIN_Splitter_2983));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_Hier_3020_3051_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2840WEIGHTED_ROUND_ROBIN_Splitter_2983));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2988() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2988WEIGHTED_ROUND_ROBIN_Splitter_2989, pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_Hier_3020_3051_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2988WEIGHTED_ROUND_ROBIN_Splitter_2989, pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_Hier_3020_3051_join[1]));
		ENDFOR
	ENDFOR
}}

void butterfly_2732(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_2642_2895_2973_3056_split[0]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_2642_2895_2973_3056_split[0]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_2642_2895_2973_3056_join[0], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_2642_2895_2973_3056_join[0], __sa2) ; 
	}
	ENDFOR
}

void butterfly_2733(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_2642_2895_2973_3056_split[1]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_2642_2895_2973_3056_split[1]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = -4.371139E-8 ; 
		WN1.imag = -1.0 ; 
		WN2.real = 1.1924881E-8 ; 
		WN2.imag = 1.0 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_2642_2895_2973_3056_join[1], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_2642_2895_2973_3056_join[1], __sa2) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2873() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_2642_2895_2973_3056_split[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child0_2980_3055_split[0]));
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_2642_2895_2973_3056_split[1], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child0_2980_3055_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2874() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child0_2980_3055_join[0], pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_2642_2895_2973_3056_join[0]));
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child0_2980_3055_join[0], pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_2642_2895_2973_3056_join[1]));
	ENDFOR
}}

void butterfly_2734(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin50_SplitJoin29_SplitJoin29_split2_2644_2909_2975_3057_split[0]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin50_SplitJoin29_SplitJoin29_split2_2644_2909_2975_3057_split[0]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin50_SplitJoin29_SplitJoin29_split2_2644_2909_2975_3057_join[0], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin50_SplitJoin29_SplitJoin29_split2_2644_2909_2975_3057_join[0], __sa2) ; 
	}
	ENDFOR
}

void butterfly_2735(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin50_SplitJoin29_SplitJoin29_split2_2644_2909_2975_3057_split[1]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin50_SplitJoin29_SplitJoin29_split2_2644_2909_2975_3057_split[1]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = -4.371139E-8 ; 
		WN1.imag = -1.0 ; 
		WN2.real = 1.1924881E-8 ; 
		WN2.imag = 1.0 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin50_SplitJoin29_SplitJoin29_split2_2644_2909_2975_3057_join[1], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin50_SplitJoin29_SplitJoin29_split2_2644_2909_2975_3057_join[1], __sa2) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2875() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		push_complex(&SplitJoin50_SplitJoin29_SplitJoin29_split2_2644_2909_2975_3057_split[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child0_2980_3055_split[1]));
		push_complex(&SplitJoin50_SplitJoin29_SplitJoin29_split2_2644_2909_2975_3057_split[1], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child0_2980_3055_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2876() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child0_2980_3055_join[1], pop_complex(&SplitJoin50_SplitJoin29_SplitJoin29_split2_2644_2909_2975_3057_join[0]));
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child0_2980_3055_join[1], pop_complex(&SplitJoin50_SplitJoin29_SplitJoin29_split2_2644_2909_2975_3057_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_2990() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child0_2980_3055_split[0], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_Hier_3021_3054_split[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child0_2980_3055_split[1], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_Hier_3021_3054_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2991() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_Hier_3021_3054_join[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child0_2980_3055_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_Hier_3021_3054_join[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child0_2980_3055_join[1]));
		ENDFOR
	ENDFOR
}}

void butterfly_2736(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin56_SplitJoin33_SplitJoin33_split2_2646_2912_2976_3059_split[0]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin56_SplitJoin33_SplitJoin33_split2_2646_2912_2976_3059_split[0]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin56_SplitJoin33_SplitJoin33_split2_2646_2912_2976_3059_join[0], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin56_SplitJoin33_SplitJoin33_split2_2646_2912_2976_3059_join[0], __sa2) ; 
	}
	ENDFOR
}

void butterfly_2737(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin56_SplitJoin33_SplitJoin33_split2_2646_2912_2976_3059_split[1]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin56_SplitJoin33_SplitJoin33_split2_2646_2912_2976_3059_split[1]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = -4.371139E-8 ; 
		WN1.imag = -1.0 ; 
		WN2.real = 1.1924881E-8 ; 
		WN2.imag = 1.0 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin56_SplitJoin33_SplitJoin33_split2_2646_2912_2976_3059_join[1], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin56_SplitJoin33_SplitJoin33_split2_2646_2912_2976_3059_join[1], __sa2) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2877() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		push_complex(&SplitJoin56_SplitJoin33_SplitJoin33_split2_2646_2912_2976_3059_split[0], pop_complex(&SplitJoin54_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child1_2982_3058_split[0]));
		push_complex(&SplitJoin56_SplitJoin33_SplitJoin33_split2_2646_2912_2976_3059_split[1], pop_complex(&SplitJoin54_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child1_2982_3058_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2878() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		push_complex(&SplitJoin54_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child1_2982_3058_join[0], pop_complex(&SplitJoin56_SplitJoin33_SplitJoin33_split2_2646_2912_2976_3059_join[0]));
		push_complex(&SplitJoin54_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child1_2982_3058_join[0], pop_complex(&SplitJoin56_SplitJoin33_SplitJoin33_split2_2646_2912_2976_3059_join[1]));
	ENDFOR
}}

void butterfly_2738(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin60_SplitJoin37_SplitJoin37_split2_2648_2915_2978_3060_split[0]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin60_SplitJoin37_SplitJoin37_split2_2648_2915_2978_3060_split[0]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin60_SplitJoin37_SplitJoin37_split2_2648_2915_2978_3060_join[0], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin60_SplitJoin37_SplitJoin37_split2_2648_2915_2978_3060_join[0], __sa2) ; 
	}
	ENDFOR
}

void butterfly_2739(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin60_SplitJoin37_SplitJoin37_split2_2648_2915_2978_3060_split[1]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin60_SplitJoin37_SplitJoin37_split2_2648_2915_2978_3060_split[1]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = -4.371139E-8 ; 
		WN1.imag = -1.0 ; 
		WN2.real = 1.1924881E-8 ; 
		WN2.imag = 1.0 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin60_SplitJoin37_SplitJoin37_split2_2648_2915_2978_3060_join[1], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin60_SplitJoin37_SplitJoin37_split2_2648_2915_2978_3060_join[1], __sa2) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2879() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		push_complex(&SplitJoin60_SplitJoin37_SplitJoin37_split2_2648_2915_2978_3060_split[0], pop_complex(&SplitJoin54_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child1_2982_3058_split[1]));
		push_complex(&SplitJoin60_SplitJoin37_SplitJoin37_split2_2648_2915_2978_3060_split[1], pop_complex(&SplitJoin54_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child1_2982_3058_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2880() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		push_complex(&SplitJoin54_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child1_2982_3058_join[1], pop_complex(&SplitJoin60_SplitJoin37_SplitJoin37_split2_2648_2915_2978_3060_join[0]));
		push_complex(&SplitJoin54_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child1_2982_3058_join[1], pop_complex(&SplitJoin60_SplitJoin37_SplitJoin37_split2_2648_2915_2978_3060_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_2992() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin54_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child1_2982_3058_split[0], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_Hier_3021_3054_split[1]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin54_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child1_2982_3058_split[1], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_Hier_3021_3054_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2993() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_Hier_3021_3054_join[1], pop_complex(&SplitJoin54_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child1_2982_3058_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_Hier_3021_3054_join[1], pop_complex(&SplitJoin54_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child1_2982_3058_join[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_2989() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_Hier_3021_3054_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2988WEIGHTED_ROUND_ROBIN_Splitter_2989));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_Hier_3021_3054_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2988WEIGHTED_ROUND_ROBIN_Splitter_2989));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2994() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2994WEIGHTED_ROUND_ROBIN_Splitter_2881, pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_Hier_3021_3054_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2994WEIGHTED_ROUND_ROBIN_Splitter_2881, pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_Hier_3021_3054_join[1]));
		ENDFOR
	ENDFOR
}}

void butterfly_2741(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_2655_2898_2979_3062_split[0]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_2655_2898_2979_3062_split[0]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_2655_2898_2979_3062_join[0], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_2655_2898_2979_3062_join[0], __sa2) ; 
	}
	ENDFOR
}

void butterfly_2742(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_2655_2898_2979_3062_split[1]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_2655_2898_2979_3062_split[1]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 0.70710677 ; 
		WN1.imag = -0.70710677 ; 
		WN2.real = -0.70710665 ; 
		WN2.imag = 0.7071069 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_2655_2898_2979_3062_join[1], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_2655_2898_2979_3062_join[1], __sa2) ; 
	}
	ENDFOR
}

void butterfly_2743(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_2655_2898_2979_3062_split[2]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_2655_2898_2979_3062_split[2]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = -4.371139E-8 ; 
		WN1.imag = -1.0 ; 
		WN2.real = 1.1924881E-8 ; 
		WN2.imag = 1.0 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_2655_2898_2979_3062_join[2], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_2655_2898_2979_3062_join[2], __sa2) ; 
	}
	ENDFOR
}

void butterfly_2744(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_2655_2898_2979_3062_split[3]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_2655_2898_2979_3062_split[3]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = -0.70710677 ; 
		WN1.imag = -0.70710677 ; 
		WN2.real = 0.707107 ; 
		WN2.imag = 0.70710653 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_2655_2898_2979_3062_join[3], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_2655_2898_2979_3062_join[3], __sa2) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2883() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_2655_2898_2979_3062_split[__iter_], pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_2653_2897_3022_3061_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2884() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_2653_2897_3022_3061_join[0], pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_2655_2898_2979_3062_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void butterfly_2745(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin43_SplitJoin22_SplitJoin22_split2_2657_2903_2981_3063_split[0]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin43_SplitJoin22_SplitJoin22_split2_2657_2903_2981_3063_split[0]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin43_SplitJoin22_SplitJoin22_split2_2657_2903_2981_3063_join[0], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin43_SplitJoin22_SplitJoin22_split2_2657_2903_2981_3063_join[0], __sa2) ; 
	}
	ENDFOR
}

void butterfly_2746(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin43_SplitJoin22_SplitJoin22_split2_2657_2903_2981_3063_split[1]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin43_SplitJoin22_SplitJoin22_split2_2657_2903_2981_3063_split[1]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 0.70710677 ; 
		WN1.imag = -0.70710677 ; 
		WN2.real = -0.70710665 ; 
		WN2.imag = 0.7071069 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin43_SplitJoin22_SplitJoin22_split2_2657_2903_2981_3063_join[1], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin43_SplitJoin22_SplitJoin22_split2_2657_2903_2981_3063_join[1], __sa2) ; 
	}
	ENDFOR
}

void butterfly_2747(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin43_SplitJoin22_SplitJoin22_split2_2657_2903_2981_3063_split[2]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin43_SplitJoin22_SplitJoin22_split2_2657_2903_2981_3063_split[2]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = -4.371139E-8 ; 
		WN1.imag = -1.0 ; 
		WN2.real = 1.1924881E-8 ; 
		WN2.imag = 1.0 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin43_SplitJoin22_SplitJoin22_split2_2657_2903_2981_3063_join[2], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin43_SplitJoin22_SplitJoin22_split2_2657_2903_2981_3063_join[2], __sa2) ; 
	}
	ENDFOR
}

void butterfly_2748(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&SplitJoin43_SplitJoin22_SplitJoin22_split2_2657_2903_2981_3063_split[3]));
		complex_t two = ((complex_t) pop_complex(&SplitJoin43_SplitJoin22_SplitJoin22_split2_2657_2903_2981_3063_split[3]));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = -0.70710677 ; 
		WN1.imag = -0.70710677 ; 
		WN2.real = 0.707107 ; 
		WN2.imag = 0.70710653 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&SplitJoin43_SplitJoin22_SplitJoin22_split2_2657_2903_2981_3063_join[3], __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&SplitJoin43_SplitJoin22_SplitJoin22_split2_2657_2903_2981_3063_join[3], __sa2) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2885() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin43_SplitJoin22_SplitJoin22_split2_2657_2903_2981_3063_split[__iter_], pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_2653_2897_3022_3061_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2886() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_2653_2897_3022_3061_join[1], pop_complex(&SplitJoin43_SplitJoin22_SplitJoin22_split2_2657_2903_2981_3063_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_2881() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_2653_2897_3022_3061_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2994WEIGHTED_ROUND_ROBIN_Splitter_2881));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_2653_2897_3022_3061_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2994WEIGHTED_ROUND_ROBIN_Splitter_2881));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2882() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2882WEIGHTED_ROUND_ROBIN_Splitter_3000, pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_2653_2897_3022_3061_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2882WEIGHTED_ROUND_ROBIN_Splitter_3000, pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_2653_2897_3022_3061_join[1]));
		ENDFOR
	ENDFOR
}}

void magnitude_3002(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_3023_3064_split[0]));
		push_float(&SplitJoin24_magnitude_Fiss_3023_3064_join[0], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_3003(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_3023_3064_split[1]));
		push_float(&SplitJoin24_magnitude_Fiss_3023_3064_join[1], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_3004(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_3023_3064_split[2]));
		push_float(&SplitJoin24_magnitude_Fiss_3023_3064_join[2], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_3005(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_3023_3064_split[3]));
		push_float(&SplitJoin24_magnitude_Fiss_3023_3064_join[3], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_3006(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_3023_3064_split[4]));
		push_float(&SplitJoin24_magnitude_Fiss_3023_3064_join[4], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_3007(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_3023_3064_split[5]));
		push_float(&SplitJoin24_magnitude_Fiss_3023_3064_join[5], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_3008(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_3023_3064_split[6]));
		push_float(&SplitJoin24_magnitude_Fiss_3023_3064_join[6], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_3009(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_3023_3064_split[7]));
		push_float(&SplitJoin24_magnitude_Fiss_3023_3064_join[7], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_3010(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_3023_3064_split[8]));
		push_float(&SplitJoin24_magnitude_Fiss_3023_3064_join[8], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_3011(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_3023_3064_split[9]));
		push_float(&SplitJoin24_magnitude_Fiss_3023_3064_join[9], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_3012(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_3023_3064_split[10]));
		push_float(&SplitJoin24_magnitude_Fiss_3023_3064_join[10], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_3013(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_3023_3064_split[11]));
		push_float(&SplitJoin24_magnitude_Fiss_3023_3064_join[11], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_3014(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_3023_3064_split[12]));
		push_float(&SplitJoin24_magnitude_Fiss_3023_3064_join[12], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void magnitude_3015(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&SplitJoin24_magnitude_Fiss_3023_3064_split[13]));
		push_float(&SplitJoin24_magnitude_Fiss_3023_3064_join[13], ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3000() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 14, __iter_++)
			push_complex(&SplitJoin24_magnitude_Fiss_3023_3064_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2882WEIGHTED_ROUND_ROBIN_Splitter_3000));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3001() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 14, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3001sink_2750, pop_float(&SplitJoin24_magnitude_Fiss_3023_3064_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void sink_2750(){
	FOR(uint32_t, __iter_steady_, 0, <, 112, __iter_steady_++) {
		printf("%.10f", pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3001sink_2750));
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_complex(&SplitJoin100_SplitJoin75_SplitJoin75_AnonFilter_a0_2709_2944_3031_3048_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 14, __iter_init_1_++)
		init_buffer_complex(&SplitJoin24_magnitude_Fiss_3023_3064_split[__iter_init_1_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_2816butterfly_2723);
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_complex(&SplitJoin80_SplitJoin55_SplitJoin55_AnonFilter_a0_2681_2930_3026_3042_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 2, __iter_init_3_++)
		init_buffer_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2661_2887_3017_3036_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 2, __iter_init_4_++)
		init_buffer_complex(&SplitJoin90_SplitJoin65_SplitJoin65_AnonFilter_a0_2695_2937_3028_3045_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 2, __iter_init_5_++)
		init_buffer_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_2642_2895_2973_3056_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 4, __iter_init_6_++)
		init_buffer_complex(&SplitJoin43_SplitJoin22_SplitJoin22_split2_2657_2903_2981_3063_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 2, __iter_init_7_++)
		init_buffer_complex(&SplitJoin56_SplitJoin33_SplitJoin33_split2_2646_2912_2976_3059_split[__iter_init_7_]);
	ENDFOR
	init_buffer_complex(&butterfly_2723Post_CollapsedDataParallel_2_2817);
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_complex(&SplitJoin90_SplitJoin65_SplitJoin65_AnonFilter_a0_2695_2937_3028_3045_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 2, __iter_init_9_++)
		init_buffer_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_2667_2890_3019_3039_join[__iter_init_9_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2840WEIGHTED_ROUND_ROBIN_Splitter_2983);
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_complex(&SplitJoin88_SplitJoin63_SplitJoin63_AnonFilter_a0_2693_2936_2972_3044_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 2, __iter_init_11_++)
		init_buffer_complex(&SplitJoin56_SplitJoin33_SplitJoin33_split2_2646_2912_2976_3059_join[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 2, __iter_init_12_++)
		init_buffer_complex(&SplitJoin78_SplitJoin53_SplitJoin53_AnonFilter_a0_2679_2929_3025_3041_split[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 4, __iter_init_13_++)
		init_buffer_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_2655_2898_2979_3062_split[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 4, __iter_init_14_++)
		init_buffer_complex(&SplitJoin43_SplitJoin22_SplitJoin22_split2_2657_2903_2981_3063_split[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 2, __iter_init_15_++)
		init_buffer_complex(&SplitJoin84_SplitJoin59_SplitJoin59_AnonFilter_a0_2687_2933_3027_3043_join[__iter_init_15_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2882WEIGHTED_ROUND_ROBIN_Splitter_3000);
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2665_2889_3018_3038_split[__iter_init_16_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_2828butterfly_2727);
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_Hier_3020_3051_join[__iter_init_17_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_2834butterfly_2729);
	init_buffer_complex(&Pre_CollapsedDataParallel_1_2831butterfly_2728);
	FOR(int, __iter_init_18_, 0, <, 2, __iter_init_18_++)
		init_buffer_complex(&SplitJoin92_SplitJoin67_SplitJoin67_AnonFilter_a0_2697_2938_3029_3046_split[__iter_init_18_]);
	ENDFOR
	FOR(int, __iter_init_19_, 0, <, 2, __iter_init_19_++)
		init_buffer_complex(&SplitJoin74_SplitJoin49_SplitJoin49_AnonFilter_a0_2673_2926_3024_3040_join[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 2, __iter_init_20_++)
		init_buffer_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2663_2888_2971_3037_split[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 4, __iter_init_21_++)
		init_buffer_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child0_2974_3052_join[__iter_init_21_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_2825butterfly_2726);
	FOR(int, __iter_init_22_, 0, <, 14, __iter_init_22_++)
		init_buffer_float(&SplitJoin24_magnitude_Fiss_3023_3064_join[__iter_init_22_]);
	ENDFOR
	FOR(int, __iter_init_23_, 0, <, 2, __iter_init_23_++)
		init_buffer_complex(&SplitJoin74_SplitJoin49_SplitJoin49_AnonFilter_a0_2673_2926_3024_3040_split[__iter_init_23_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2997WEIGHTED_ROUND_ROBIN_Splitter_2839);
	init_buffer_complex(&butterfly_2727Post_CollapsedDataParallel_2_2829);
	init_buffer_complex(&butterfly_2725Post_CollapsedDataParallel_2_2823);
	FOR(int, __iter_init_24_, 0, <, 2, __iter_init_24_++)
		init_buffer_complex(&SplitJoin88_SplitJoin63_SplitJoin63_AnonFilter_a0_2693_2936_2972_3044_split[__iter_init_24_]);
	ENDFOR
	FOR(int, __iter_init_25_, 0, <, 2, __iter_init_25_++)
		init_buffer_complex(&SplitJoin96_SplitJoin71_SplitJoin71_AnonFilter_a0_2703_2941_3030_3047_split[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 2, __iter_init_26_++)
		init_buffer_complex(&SplitJoin78_SplitJoin53_SplitJoin53_AnonFilter_a0_2679_2929_3025_3041_join[__iter_init_26_]);
	ENDFOR
	FOR(int, __iter_init_27_, 0, <, 2, __iter_init_27_++)
		init_buffer_complex(&SplitJoin54_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child1_2982_3058_join[__iter_init_27_]);
	ENDFOR
	FOR(int, __iter_init_28_, 0, <, 2, __iter_init_28_++)
		init_buffer_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_2642_2895_2973_3056_join[__iter_init_28_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_2822butterfly_2725);
	FOR(int, __iter_init_29_, 0, <, 2, __iter_init_29_++)
		init_buffer_complex(&SplitJoin50_SplitJoin29_SplitJoin29_split2_2644_2909_2975_3057_split[__iter_init_29_]);
	ENDFOR
	FOR(int, __iter_init_30_, 0, <, 2, __iter_init_30_++)
		init_buffer_complex(&SplitJoin80_SplitJoin55_SplitJoin55_AnonFilter_a0_2681_2930_3026_3042_join[__iter_init_30_]);
	ENDFOR
	init_buffer_complex(&butterfly_2730Post_CollapsedDataParallel_2_2838);
	FOR(int, __iter_init_31_, 0, <, 2, __iter_init_31_++)
		init_buffer_complex(&SplitJoin0_source_Fiss_3016_3035_split[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 2, __iter_init_32_++)
		init_buffer_complex(&SplitJoin102_SplitJoin77_SplitJoin77_AnonFilter_a0_2711_2945_3032_3049_split[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 2, __iter_init_33_++)
		init_buffer_complex(&SplitJoin106_SplitJoin81_SplitJoin81_AnonFilter_a0_2717_2948_3033_3050_split[__iter_init_33_]);
	ENDFOR
	FOR(int, __iter_init_34_, 0, <, 2, __iter_init_34_++)
		init_buffer_complex(&SplitJoin106_SplitJoin81_SplitJoin81_AnonFilter_a0_2717_2948_3033_3050_join[__iter_init_34_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_2819butterfly_2724);
	FOR(int, __iter_init_35_, 0, <, 2, __iter_init_35_++)
		init_buffer_complex(&SplitJoin50_SplitJoin29_SplitJoin29_split2_2644_2909_2975_3057_join[__iter_init_35_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3001sink_2750);
	FOR(int, __iter_init_36_, 0, <, 2, __iter_init_36_++)
		init_buffer_complex(&SplitJoin100_SplitJoin75_SplitJoin75_AnonFilter_a0_2709_2944_3031_3048_split[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 2, __iter_init_37_++)
		init_buffer_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_2663_2888_2971_3037_join[__iter_init_37_]);
	ENDFOR
	FOR(int, __iter_init_38_, 0, <, 2, __iter_init_38_++)
		init_buffer_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_Hier_3021_3054_split[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 2, __iter_init_39_++)
		init_buffer_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_2665_2889_3018_3038_join[__iter_init_39_]);
	ENDFOR
	FOR(int, __iter_init_40_, 0, <, 2, __iter_init_40_++)
		init_buffer_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_Hier_3021_3054_join[__iter_init_40_]);
	ENDFOR
	FOR(int, __iter_init_41_, 0, <, 2, __iter_init_41_++)
		init_buffer_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_2667_2890_3019_3039_split[__iter_init_41_]);
	ENDFOR
	init_buffer_complex(&butterfly_2728Post_CollapsedDataParallel_2_2832);
	FOR(int, __iter_init_42_, 0, <, 2, __iter_init_42_++)
		init_buffer_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_2653_2897_3022_3061_split[__iter_init_42_]);
	ENDFOR
	FOR(int, __iter_init_43_, 0, <, 4, __iter_init_43_++)
		init_buffer_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child0_2974_3052_split[__iter_init_43_]);
	ENDFOR
	init_buffer_complex(&butterfly_2729Post_CollapsedDataParallel_2_2835);
	FOR(int, __iter_init_44_, 0, <, 2, __iter_init_44_++)
		init_buffer_complex(&SplitJoin60_SplitJoin37_SplitJoin37_split2_2648_2915_2978_3060_split[__iter_init_44_]);
	ENDFOR
	FOR(int, __iter_init_45_, 0, <, 2, __iter_init_45_++)
		init_buffer_complex(&SplitJoin84_SplitJoin59_SplitJoin59_AnonFilter_a0_2687_2933_3027_3043_split[__iter_init_45_]);
	ENDFOR
	FOR(int, __iter_init_46_, 0, <, 2, __iter_init_46_++)
		init_buffer_complex(&SplitJoin60_SplitJoin37_SplitJoin37_split2_2648_2915_2978_3060_join[__iter_init_46_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_2837butterfly_2730);
	FOR(int, __iter_init_47_, 0, <, 2, __iter_init_47_++)
		init_buffer_complex(&SplitJoin54_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child1_2982_3058_split[__iter_init_47_]);
	ENDFOR
	FOR(int, __iter_init_48_, 0, <, 4, __iter_init_48_++)
		init_buffer_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_2655_2898_2979_3062_join[__iter_init_48_]);
	ENDFOR
	FOR(int, __iter_init_49_, 0, <, 2, __iter_init_49_++)
		init_buffer_complex(&SplitJoin0_source_Fiss_3016_3035_join[__iter_init_49_]);
	ENDFOR
	FOR(int, __iter_init_50_, 0, <, 2, __iter_init_50_++)
		init_buffer_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_Hier_3020_3051_split[__iter_init_50_]);
	ENDFOR
	FOR(int, __iter_init_51_, 0, <, 2, __iter_init_51_++)
		init_buffer_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child0_2980_3055_join[__iter_init_51_]);
	ENDFOR
	FOR(int, __iter_init_52_, 0, <, 4, __iter_init_52_++)
		init_buffer_complex(&SplitJoin67_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child1_2977_3053_join[__iter_init_52_]);
	ENDFOR
	FOR(int, __iter_init_53_, 0, <, 2, __iter_init_53_++)
		init_buffer_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_2661_2887_3017_3036_join[__iter_init_53_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2988WEIGHTED_ROUND_ROBIN_Splitter_2989);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2994WEIGHTED_ROUND_ROBIN_Splitter_2881);
	FOR(int, __iter_init_54_, 0, <, 4, __iter_init_54_++)
		init_buffer_complex(&SplitJoin67_SplitJoin8_SplitJoin8_split1_2619_2892_Hier_child1_2977_3053_split[__iter_init_54_]);
	ENDFOR
	init_buffer_complex(&butterfly_2726Post_CollapsedDataParallel_2_2826);
	FOR(int, __iter_init_55_, 0, <, 2, __iter_init_55_++)
		init_buffer_complex(&SplitJoin92_SplitJoin67_SplitJoin67_AnonFilter_a0_2697_2938_3029_3046_join[__iter_init_55_]);
	ENDFOR
	FOR(int, __iter_init_56_, 0, <, 2, __iter_init_56_++)
		init_buffer_complex(&SplitJoin102_SplitJoin77_SplitJoin77_AnonFilter_a0_2711_2945_3032_3049_join[__iter_init_56_]);
	ENDFOR
	init_buffer_complex(&butterfly_2724Post_CollapsedDataParallel_2_2820);
	FOR(int, __iter_init_57_, 0, <, 2, __iter_init_57_++)
		init_buffer_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_2640_2894_Hier_child0_2980_3055_split[__iter_init_57_]);
	ENDFOR
	FOR(int, __iter_init_58_, 0, <, 2, __iter_init_58_++)
		init_buffer_complex(&SplitJoin96_SplitJoin71_SplitJoin71_AnonFilter_a0_2703_2941_3030_3047_join[__iter_init_58_]);
	ENDFOR
	FOR(int, __iter_init_59_, 0, <, 2, __iter_init_59_++)
		init_buffer_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_2653_2897_3022_3061_join[__iter_init_59_]);
	ENDFOR
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_2996();
			source_2998();
			source_2999();
		WEIGHTED_ROUND_ROBIN_Joiner_2997();
		WEIGHTED_ROUND_ROBIN_Splitter_2839();
			WEIGHTED_ROUND_ROBIN_Splitter_2841();
				WEIGHTED_ROUND_ROBIN_Splitter_2843();
					WEIGHTED_ROUND_ROBIN_Splitter_2845();
						Identity_2669();
						Identity_2671();
					WEIGHTED_ROUND_ROBIN_Joiner_2846();
					WEIGHTED_ROUND_ROBIN_Splitter_2847();
						Identity_2675();
						Identity_2677();
					WEIGHTED_ROUND_ROBIN_Joiner_2848();
				WEIGHTED_ROUND_ROBIN_Joiner_2844();
				WEIGHTED_ROUND_ROBIN_Splitter_2849();
					WEIGHTED_ROUND_ROBIN_Splitter_2851();
						Identity_2683();
						Identity_2685();
					WEIGHTED_ROUND_ROBIN_Joiner_2852();
					WEIGHTED_ROUND_ROBIN_Splitter_2853();
						Identity_2689();
						Identity_2691();
					WEIGHTED_ROUND_ROBIN_Joiner_2854();
				WEIGHTED_ROUND_ROBIN_Joiner_2850();
			WEIGHTED_ROUND_ROBIN_Joiner_2842();
			WEIGHTED_ROUND_ROBIN_Splitter_2855();
				WEIGHTED_ROUND_ROBIN_Splitter_2857();
					WEIGHTED_ROUND_ROBIN_Splitter_2859();
						Identity_2699();
						Identity_2701();
					WEIGHTED_ROUND_ROBIN_Joiner_2860();
					WEIGHTED_ROUND_ROBIN_Splitter_2861();
						Identity_2705();
						Identity_2707();
					WEIGHTED_ROUND_ROBIN_Joiner_2862();
				WEIGHTED_ROUND_ROBIN_Joiner_2858();
				WEIGHTED_ROUND_ROBIN_Splitter_2863();
					WEIGHTED_ROUND_ROBIN_Splitter_2865();
						Identity_2713();
						Identity_2715();
					WEIGHTED_ROUND_ROBIN_Joiner_2866();
					WEIGHTED_ROUND_ROBIN_Splitter_2867();
						Identity_2719();
						Identity_2721();
					WEIGHTED_ROUND_ROBIN_Joiner_2868();
				WEIGHTED_ROUND_ROBIN_Joiner_2864();
			WEIGHTED_ROUND_ROBIN_Joiner_2856();
		WEIGHTED_ROUND_ROBIN_Joiner_2840();
		WEIGHTED_ROUND_ROBIN_Splitter_2983();
			WEIGHTED_ROUND_ROBIN_Splitter_2984();
				Pre_CollapsedDataParallel_1_2816();
				butterfly_2723();
				Post_CollapsedDataParallel_2_2817();
				Pre_CollapsedDataParallel_1_2819();
				butterfly_2724();
				Post_CollapsedDataParallel_2_2820();
				Pre_CollapsedDataParallel_1_2822();
				butterfly_2725();
				Post_CollapsedDataParallel_2_2823();
				Pre_CollapsedDataParallel_1_2825();
				butterfly_2726();
				Post_CollapsedDataParallel_2_2826();
			WEIGHTED_ROUND_ROBIN_Joiner_2985();
			WEIGHTED_ROUND_ROBIN_Splitter_2986();
				Pre_CollapsedDataParallel_1_2828();
				butterfly_2727();
				Post_CollapsedDataParallel_2_2829();
				Pre_CollapsedDataParallel_1_2831();
				butterfly_2728();
				Post_CollapsedDataParallel_2_2832();
				Pre_CollapsedDataParallel_1_2834();
				butterfly_2729();
				Post_CollapsedDataParallel_2_2835();
				Pre_CollapsedDataParallel_1_2837();
				butterfly_2730();
				Post_CollapsedDataParallel_2_2838();
			WEIGHTED_ROUND_ROBIN_Joiner_2987();
		WEIGHTED_ROUND_ROBIN_Joiner_2988();
		WEIGHTED_ROUND_ROBIN_Splitter_2989();
			WEIGHTED_ROUND_ROBIN_Splitter_2990();
				WEIGHTED_ROUND_ROBIN_Splitter_2873();
					butterfly_2732();
					butterfly_2733();
				WEIGHTED_ROUND_ROBIN_Joiner_2874();
				WEIGHTED_ROUND_ROBIN_Splitter_2875();
					butterfly_2734();
					butterfly_2735();
				WEIGHTED_ROUND_ROBIN_Joiner_2876();
			WEIGHTED_ROUND_ROBIN_Joiner_2991();
			WEIGHTED_ROUND_ROBIN_Splitter_2992();
				WEIGHTED_ROUND_ROBIN_Splitter_2877();
					butterfly_2736();
					butterfly_2737();
				WEIGHTED_ROUND_ROBIN_Joiner_2878();
				WEIGHTED_ROUND_ROBIN_Splitter_2879();
					butterfly_2738();
					butterfly_2739();
				WEIGHTED_ROUND_ROBIN_Joiner_2880();
			WEIGHTED_ROUND_ROBIN_Joiner_2993();
		WEIGHTED_ROUND_ROBIN_Joiner_2994();
		WEIGHTED_ROUND_ROBIN_Splitter_2881();
			WEIGHTED_ROUND_ROBIN_Splitter_2883();
				butterfly_2741();
				butterfly_2742();
				butterfly_2743();
				butterfly_2744();
			WEIGHTED_ROUND_ROBIN_Joiner_2884();
			WEIGHTED_ROUND_ROBIN_Splitter_2885();
				butterfly_2745();
				butterfly_2746();
				butterfly_2747();
				butterfly_2748();
			WEIGHTED_ROUND_ROBIN_Joiner_2886();
		WEIGHTED_ROUND_ROBIN_Joiner_2882();
		WEIGHTED_ROUND_ROBIN_Splitter_3000();
			magnitude_3002();
			magnitude_3003();
			magnitude_3004();
			magnitude_3005();
			magnitude_3006();
			magnitude_3007();
			magnitude_3008();
			magnitude_3009();
			magnitude_3010();
			magnitude_3011();
			magnitude_3012();
			magnitude_3013();
			magnitude_3014();
			magnitude_3015();
		WEIGHTED_ROUND_ROBIN_Joiner_3001();
		sink_2750();
	ENDFOR
	return EXIT_SUCCESS;
}
