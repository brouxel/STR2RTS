#include "PEG6-FFT3.h"

buffer_float_t SplitJoin83_Butterfly_Fiss_9284_9294_split[6];
buffer_float_t SplitJoin43_Butterfly_Fiss_9277_9302_split[2];
buffer_float_t SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_child0_9167_9299_join[4];
buffer_float_t SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_Hier_9271_9298_join[2];
buffer_float_t Pre_CollapsedDataParallel_1_9078WEIGHTED_ROUND_ROBIN_Splitter_9251;
buffer_float_t Pre_CollapsedDataParallel_1_9072WEIGHTED_ROUND_ROBIN_Splitter_9243;
buffer_float_t Pre_CollapsedDataParallel_1_9063WEIGHTED_ROUND_ROBIN_Splitter_9229;
buffer_float_t SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_child1_9168_9304_split[4];
buffer_float_t SplitJoin8_Butterfly_Fiss_9270_9292_split[4];
buffer_float_t BitReverse_8825FloatPrinter_8826;
buffer_float_t Post_CollapsedDataParallel_2_9052WEIGHTED_ROUND_ROBIN_Splitter_9171;
buffer_float_t SplitJoin57_Butterfly_Fiss_9280_9306_split[2];
buffer_float_t Pre_CollapsedDataParallel_1_9045WEIGHTED_ROUND_ROBIN_Splitter_9187;
buffer_float_t Pre_CollapsedDataParallel_1_9054WEIGHTED_ROUND_ROBIN_Splitter_9203;
buffer_float_t Pre_CollapsedDataParallel_1_9084WEIGHTED_ROUND_ROBIN_Splitter_9259;
buffer_float_t SplitJoin39_Butterfly_Fiss_9276_9301_join[2];
buffer_float_t SplitJoin39_Butterfly_Fiss_9276_9301_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9204Post_CollapsedDataParallel_2_9055;
buffer_float_t Pre_CollapsedDataParallel_1_9087WEIGHTED_ROUND_ROBIN_Splitter_9263;
buffer_float_t SplitJoin72_Butterfly_Fiss_9283_9293_split[4];
buffer_float_t SplitJoin4_Butterfly_Fiss_9269_9290_split[6];
buffer_float_t SplitJoin85_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_child1_9143_9295_join[2];
buffer_float_t SplitJoin8_Butterfly_Fiss_9270_9292_join[4];
buffer_float_t SplitJoin14_Butterfly_Fiss_9272_9300_split[2];
buffer_float_t SplitJoin0_Butterfly_Fiss_9267_9288_split[6];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9173WEIGHTED_ROUND_ROBIN_Splitter_9174;
buffer_float_t SplitJoin61_Butterfly_Fiss_9281_9307_split[2];
buffer_float_t SplitJoin83_Butterfly_Fiss_9284_9294_join[6];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9224Post_CollapsedDataParallel_2_9061;
buffer_float_t SplitJoin0_Butterfly_Fiss_9267_9288_join[6];
buffer_float_t SplitJoin47_Butterfly_Fiss_9278_9303_join[2];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_8702_9097_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_Hier_9268_9289_split[2];
buffer_float_t SplitJoin93_Butterfly_Fiss_9286_9297_split[4];
buffer_float_t SplitJoin53_Butterfly_Fiss_9279_9305_split[2];
buffer_float_t SplitJoin61_Butterfly_Fiss_9281_9307_join[2];
buffer_float_t Post_CollapsedDataParallel_2_9046WEIGHTED_ROUND_ROBIN_Splitter_9089;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9256Post_CollapsedDataParallel_2_9082;
buffer_float_t SplitJoin87_Butterfly_Fiss_9285_9296_join[4];
buffer_float_t Pre_CollapsedDataParallel_1_9075WEIGHTED_ROUND_ROBIN_Splitter_9247;
buffer_float_t SplitJoin57_Butterfly_Fiss_9280_9306_join[2];
buffer_float_t SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_child0_9138_9291_split[2];
buffer_float_t SplitJoin14_Butterfly_Fiss_9272_9300_join[2];
buffer_float_t Pre_CollapsedDataParallel_1_9057WEIGHTED_ROUND_ROBIN_Splitter_9209;
buffer_float_t Pre_CollapsedDataParallel_1_9051WEIGHTED_ROUND_ROBIN_Splitter_9215;
buffer_float_t SplitJoin65_Butterfly_Fiss_9282_9308_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9185BitReverse_8825;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9230Post_CollapsedDataParallel_2_9064;
buffer_float_t SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child0_9274_9310_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9236Post_CollapsedDataParallel_2_9067;
buffer_float_t SplitJoin4_Butterfly_Fiss_9269_9290_join[6];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9240Post_CollapsedDataParallel_2_9070;
buffer_float_t SplitJoin47_Butterfly_Fiss_9278_9303_split[2];
buffer_float_t SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_Hier_9273_9309_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9216Post_CollapsedDataParallel_2_9052;
buffer_float_t SplitJoin43_Butterfly_Fiss_9277_9302_join[2];
buffer_float_t SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child1_9275_9311_split[8];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_8702_9097_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_Hier_9268_9289_join[2];
buffer_float_t SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child0_9274_9310_join[8];
buffer_float_t SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_child0_9167_9299_split[4];
buffer_float_t SplitJoin85_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_child1_9143_9295_split[2];
buffer_float_t Post_CollapsedDataParallel_2_9049WEIGHTED_ROUND_ROBIN_Splitter_9169;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9179WEIGHTED_ROUND_ROBIN_Splitter_9180;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9252Post_CollapsedDataParallel_2_9079;
buffer_float_t SplitJoin65_Butterfly_Fiss_9282_9308_join[2];
buffer_float_t Pre_CollapsedDataParallel_1_9081WEIGHTED_ROUND_ROBIN_Splitter_9255;
buffer_float_t SplitJoin93_Butterfly_Fiss_9286_9297_join[4];
buffer_float_t SplitJoin72_Butterfly_Fiss_9283_9293_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9260Post_CollapsedDataParallel_2_9085;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9264Post_CollapsedDataParallel_2_9088;
buffer_float_t Pre_CollapsedDataParallel_1_9060WEIGHTED_ROUND_ROBIN_Splitter_9223;
buffer_float_t SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_child1_9168_9304_join[4];
buffer_float_t Pre_CollapsedDataParallel_1_9066WEIGHTED_ROUND_ROBIN_Splitter_9235;
buffer_float_t SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child1_9275_9311_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9188Post_CollapsedDataParallel_2_9046;
buffer_float_t Pre_CollapsedDataParallel_1_9048WEIGHTED_ROUND_ROBIN_Splitter_9195;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9196Post_CollapsedDataParallel_2_9049;
buffer_float_t SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_Hier_9271_9298_split[2];
buffer_float_t Pre_CollapsedDataParallel_1_9069WEIGHTED_ROUND_ROBIN_Splitter_9239;
buffer_float_t SplitJoin87_Butterfly_Fiss_9285_9296_split[4];
buffer_float_t SplitJoin53_Butterfly_Fiss_9279_9305_join[2];
buffer_float_t SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_child0_9138_9291_join[2];
buffer_float_t SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_Hier_9273_9309_join[2];
buffer_float_t FloatSource_8744Pre_CollapsedDataParallel_1_9045;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9244Post_CollapsedDataParallel_2_9073;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9210Post_CollapsedDataParallel_2_9058;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9248Post_CollapsedDataParallel_2_9076;


FloatSource_8744_t FloatSource_8744_s;

void FloatSource(buffer_float_t *chanout) {
		push_float(&(*chanout), FloatSource_8744_s.A_re[FloatSource_8744_s.idx]) ; 
		push_float(&(*chanout), FloatSource_8744_s.A_im[FloatSource_8744_s.idx]) ; 
		FloatSource_8744_s.idx++ ; 
		if((FloatSource_8744_s.idx >= 32)) {
			FloatSource_8744_s.idx = 0 ; 
		}
	}


void FloatSource_8744() {
	FOR(uint32_t, __iter_steady_, 0, <, 96, __iter_steady_++)
		FloatSource(&(FloatSource_8744Pre_CollapsedDataParallel_1_9045));
	ENDFOR
}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
		int partialSum_k = 0;
 {
		FOR(int, _k, 0,  < , 16, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k;
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 32) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_9045() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(FloatSource_8744Pre_CollapsedDataParallel_1_9045), &(Pre_CollapsedDataParallel_1_9045WEIGHTED_ROUND_ROBIN_Splitter_9187));
	ENDFOR
}

void Butterfly(buffer_float_t *chanin, buffer_float_t *chanout) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&(*chanin)) ; 
		u_im = pop_float(&(*chanin)) ; 
		t_re = pop_float(&(*chanin)) ; 
		t_im = pop_float(&(*chanin)) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&(*chanout), u_re) ; 
		push_float(&(*chanout), u_im) ; 
		push_float(&(*chanout), t_re) ; 
		push_float(&(*chanout), t_im) ; 
	}


void Butterfly_9189() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_9267_9288_split[0]), &(SplitJoin0_Butterfly_Fiss_9267_9288_join[0]));
	ENDFOR
}

void Butterfly_9190() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_9267_9288_split[1]), &(SplitJoin0_Butterfly_Fiss_9267_9288_join[1]));
	ENDFOR
}

void Butterfly_9191() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_9267_9288_split[2]), &(SplitJoin0_Butterfly_Fiss_9267_9288_join[2]));
	ENDFOR
}

void Butterfly_9192() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_9267_9288_split[3]), &(SplitJoin0_Butterfly_Fiss_9267_9288_join[3]));
	ENDFOR
}

void Butterfly_9193() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_9267_9288_split[4]), &(SplitJoin0_Butterfly_Fiss_9267_9288_join[4]));
	ENDFOR
}

void Butterfly_9194() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_9267_9288_split[5]), &(SplitJoin0_Butterfly_Fiss_9267_9288_join[5]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9187() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin0_Butterfly_Fiss_9267_9288_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_9045WEIGHTED_ROUND_ROBIN_Splitter_9187));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9188() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9188Post_CollapsedDataParallel_2_9046, pop_float(&SplitJoin0_Butterfly_Fiss_9267_9288_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
		int kTimesWeights_i = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 16, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&(*chanout), peek_float(&(*chanin), (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_9046() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_9188Post_CollapsedDataParallel_2_9046), &(Post_CollapsedDataParallel_2_9046WEIGHTED_ROUND_ROBIN_Splitter_9089));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_9048() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_8702_9097_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_Hier_9268_9289_split[0]), &(Pre_CollapsedDataParallel_1_9048WEIGHTED_ROUND_ROBIN_Splitter_9195));
	ENDFOR
}

void Butterfly_9197() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_9269_9290_split[0]), &(SplitJoin4_Butterfly_Fiss_9269_9290_join[0]));
	ENDFOR
}

void Butterfly_9198() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_9269_9290_split[1]), &(SplitJoin4_Butterfly_Fiss_9269_9290_join[1]));
	ENDFOR
}

void Butterfly_9199() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_9269_9290_split[2]), &(SplitJoin4_Butterfly_Fiss_9269_9290_join[2]));
	ENDFOR
}

void Butterfly_9200() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_9269_9290_split[3]), &(SplitJoin4_Butterfly_Fiss_9269_9290_join[3]));
	ENDFOR
}

void Butterfly_9201() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_9269_9290_split[4]), &(SplitJoin4_Butterfly_Fiss_9269_9290_join[4]));
	ENDFOR
}

void Butterfly_9202() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_9269_9290_split[5]), &(SplitJoin4_Butterfly_Fiss_9269_9290_join[5]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9195() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin4_Butterfly_Fiss_9269_9290_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_9048WEIGHTED_ROUND_ROBIN_Splitter_9195));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9196() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9196Post_CollapsedDataParallel_2_9049, pop_float(&SplitJoin4_Butterfly_Fiss_9269_9290_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_9049() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_9196Post_CollapsedDataParallel_2_9049), &(Post_CollapsedDataParallel_2_9049WEIGHTED_ROUND_ROBIN_Splitter_9169));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_9054() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_child0_9138_9291_split[0]), &(Pre_CollapsedDataParallel_1_9054WEIGHTED_ROUND_ROBIN_Splitter_9203));
	ENDFOR
}

void Butterfly_9205() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin8_Butterfly_Fiss_9270_9292_split[0]), &(SplitJoin8_Butterfly_Fiss_9270_9292_join[0]));
	ENDFOR
}

void Butterfly_9206() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin8_Butterfly_Fiss_9270_9292_split[1]), &(SplitJoin8_Butterfly_Fiss_9270_9292_join[1]));
	ENDFOR
}

void Butterfly_9207() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin8_Butterfly_Fiss_9270_9292_split[2]), &(SplitJoin8_Butterfly_Fiss_9270_9292_join[2]));
	ENDFOR
}

void Butterfly_9208() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin8_Butterfly_Fiss_9270_9292_split[3]), &(SplitJoin8_Butterfly_Fiss_9270_9292_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9203() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin8_Butterfly_Fiss_9270_9292_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_9054WEIGHTED_ROUND_ROBIN_Splitter_9203));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9204() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9204Post_CollapsedDataParallel_2_9055, pop_float(&SplitJoin8_Butterfly_Fiss_9270_9292_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_9055() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_9204Post_CollapsedDataParallel_2_9055), &(SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_child0_9138_9291_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_9057() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_child0_9138_9291_split[1]), &(Pre_CollapsedDataParallel_1_9057WEIGHTED_ROUND_ROBIN_Splitter_9209));
	ENDFOR
}

void Butterfly_9211() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin72_Butterfly_Fiss_9283_9293_split[0]), &(SplitJoin72_Butterfly_Fiss_9283_9293_join[0]));
	ENDFOR
}

void Butterfly_9212() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin72_Butterfly_Fiss_9283_9293_split[1]), &(SplitJoin72_Butterfly_Fiss_9283_9293_join[1]));
	ENDFOR
}

void Butterfly_9213() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin72_Butterfly_Fiss_9283_9293_split[2]), &(SplitJoin72_Butterfly_Fiss_9283_9293_join[2]));
	ENDFOR
}

void Butterfly_9214() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin72_Butterfly_Fiss_9283_9293_split[3]), &(SplitJoin72_Butterfly_Fiss_9283_9293_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9209() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin72_Butterfly_Fiss_9283_9293_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_9057WEIGHTED_ROUND_ROBIN_Splitter_9209));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9210() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9210Post_CollapsedDataParallel_2_9058, pop_float(&SplitJoin72_Butterfly_Fiss_9283_9293_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_9058() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_9210Post_CollapsedDataParallel_2_9058), &(SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_child0_9138_9291_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9169() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_child0_9138_9291_split[0], pop_float(&Post_CollapsedDataParallel_2_9049WEIGHTED_ROUND_ROBIN_Splitter_9169));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_child0_9138_9291_split[1], pop_float(&Post_CollapsedDataParallel_2_9049WEIGHTED_ROUND_ROBIN_Splitter_9169));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9170() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_8702_9097_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_Hier_9268_9289_join[0], pop_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_child0_9138_9291_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_8702_9097_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_Hier_9268_9289_join[0], pop_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_child0_9138_9291_join[1]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_9051() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_8702_9097_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_Hier_9268_9289_split[1]), &(Pre_CollapsedDataParallel_1_9051WEIGHTED_ROUND_ROBIN_Splitter_9215));
	ENDFOR
}

void Butterfly_9217() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin83_Butterfly_Fiss_9284_9294_split[0]), &(SplitJoin83_Butterfly_Fiss_9284_9294_join[0]));
	ENDFOR
}

void Butterfly_9218() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin83_Butterfly_Fiss_9284_9294_split[1]), &(SplitJoin83_Butterfly_Fiss_9284_9294_join[1]));
	ENDFOR
}

void Butterfly_9219() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin83_Butterfly_Fiss_9284_9294_split[2]), &(SplitJoin83_Butterfly_Fiss_9284_9294_join[2]));
	ENDFOR
}

void Butterfly_9220() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin83_Butterfly_Fiss_9284_9294_split[3]), &(SplitJoin83_Butterfly_Fiss_9284_9294_join[3]));
	ENDFOR
}

void Butterfly_9221() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin83_Butterfly_Fiss_9284_9294_split[4]), &(SplitJoin83_Butterfly_Fiss_9284_9294_join[4]));
	ENDFOR
}

void Butterfly_9222() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin83_Butterfly_Fiss_9284_9294_split[5]), &(SplitJoin83_Butterfly_Fiss_9284_9294_join[5]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9215() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin83_Butterfly_Fiss_9284_9294_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_9051WEIGHTED_ROUND_ROBIN_Splitter_9215));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9216() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9216Post_CollapsedDataParallel_2_9052, pop_float(&SplitJoin83_Butterfly_Fiss_9284_9294_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_9052() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_9216Post_CollapsedDataParallel_2_9052), &(Post_CollapsedDataParallel_2_9052WEIGHTED_ROUND_ROBIN_Splitter_9171));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_9060() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin85_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_child1_9143_9295_split[0]), &(Pre_CollapsedDataParallel_1_9060WEIGHTED_ROUND_ROBIN_Splitter_9223));
	ENDFOR
}

void Butterfly_9225() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin87_Butterfly_Fiss_9285_9296_split[0]), &(SplitJoin87_Butterfly_Fiss_9285_9296_join[0]));
	ENDFOR
}

void Butterfly_9226() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin87_Butterfly_Fiss_9285_9296_split[1]), &(SplitJoin87_Butterfly_Fiss_9285_9296_join[1]));
	ENDFOR
}

void Butterfly_9227() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin87_Butterfly_Fiss_9285_9296_split[2]), &(SplitJoin87_Butterfly_Fiss_9285_9296_join[2]));
	ENDFOR
}

void Butterfly_9228() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin87_Butterfly_Fiss_9285_9296_split[3]), &(SplitJoin87_Butterfly_Fiss_9285_9296_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9223() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin87_Butterfly_Fiss_9285_9296_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_9060WEIGHTED_ROUND_ROBIN_Splitter_9223));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9224() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9224Post_CollapsedDataParallel_2_9061, pop_float(&SplitJoin87_Butterfly_Fiss_9285_9296_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_9061() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_9224Post_CollapsedDataParallel_2_9061), &(SplitJoin85_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_child1_9143_9295_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_9063() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin85_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_child1_9143_9295_split[1]), &(Pre_CollapsedDataParallel_1_9063WEIGHTED_ROUND_ROBIN_Splitter_9229));
	ENDFOR
}

void Butterfly_9231() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin93_Butterfly_Fiss_9286_9297_split[0]), &(SplitJoin93_Butterfly_Fiss_9286_9297_join[0]));
	ENDFOR
}

void Butterfly_9232() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin93_Butterfly_Fiss_9286_9297_split[1]), &(SplitJoin93_Butterfly_Fiss_9286_9297_join[1]));
	ENDFOR
}

void Butterfly_9233() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin93_Butterfly_Fiss_9286_9297_split[2]), &(SplitJoin93_Butterfly_Fiss_9286_9297_join[2]));
	ENDFOR
}

void Butterfly_9234() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin93_Butterfly_Fiss_9286_9297_split[3]), &(SplitJoin93_Butterfly_Fiss_9286_9297_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9229() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin93_Butterfly_Fiss_9286_9297_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_9063WEIGHTED_ROUND_ROBIN_Splitter_9229));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9230() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9230Post_CollapsedDataParallel_2_9064, pop_float(&SplitJoin93_Butterfly_Fiss_9286_9297_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_9064() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_9230Post_CollapsedDataParallel_2_9064), &(SplitJoin85_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_child1_9143_9295_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9171() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin85_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_child1_9143_9295_split[0], pop_float(&Post_CollapsedDataParallel_2_9052WEIGHTED_ROUND_ROBIN_Splitter_9171));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin85_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_child1_9143_9295_split[1], pop_float(&Post_CollapsedDataParallel_2_9052WEIGHTED_ROUND_ROBIN_Splitter_9171));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9172() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_8702_9097_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_Hier_9268_9289_join[1], pop_float(&SplitJoin85_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_child1_9143_9295_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_8702_9097_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_Hier_9268_9289_join[1], pop_float(&SplitJoin85_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_child1_9143_9295_join[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_9089() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_8702_9097_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_Hier_9268_9289_split[0], pop_float(&Post_CollapsedDataParallel_2_9046WEIGHTED_ROUND_ROBIN_Splitter_9089));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_8702_9097_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_Hier_9268_9289_split[1], pop_float(&Post_CollapsedDataParallel_2_9046WEIGHTED_ROUND_ROBIN_Splitter_9089));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9173() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9173WEIGHTED_ROUND_ROBIN_Splitter_9174, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_8702_9097_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_Hier_9268_9289_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9173WEIGHTED_ROUND_ROBIN_Splitter_9174, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_8702_9097_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_Hier_9268_9289_join[1]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_9066() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_child0_9167_9299_split[0]), &(Pre_CollapsedDataParallel_1_9066WEIGHTED_ROUND_ROBIN_Splitter_9235));
	ENDFOR
}

void Butterfly_9237() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin14_Butterfly_Fiss_9272_9300_split[0]), &(SplitJoin14_Butterfly_Fiss_9272_9300_join[0]));
	ENDFOR
}

void Butterfly_9238() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin14_Butterfly_Fiss_9272_9300_split[1]), &(SplitJoin14_Butterfly_Fiss_9272_9300_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9235() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin14_Butterfly_Fiss_9272_9300_split[0], pop_float(&Pre_CollapsedDataParallel_1_9066WEIGHTED_ROUND_ROBIN_Splitter_9235));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin14_Butterfly_Fiss_9272_9300_split[1], pop_float(&Pre_CollapsedDataParallel_1_9066WEIGHTED_ROUND_ROBIN_Splitter_9235));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9236() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9236Post_CollapsedDataParallel_2_9067, pop_float(&SplitJoin14_Butterfly_Fiss_9272_9300_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9236Post_CollapsedDataParallel_2_9067, pop_float(&SplitJoin14_Butterfly_Fiss_9272_9300_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_9067() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_9236Post_CollapsedDataParallel_2_9067), &(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_child0_9167_9299_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_9069() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_child0_9167_9299_split[1]), &(Pre_CollapsedDataParallel_1_9069WEIGHTED_ROUND_ROBIN_Splitter_9239));
	ENDFOR
}

void Butterfly_9241() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin39_Butterfly_Fiss_9276_9301_split[0]), &(SplitJoin39_Butterfly_Fiss_9276_9301_join[0]));
	ENDFOR
}

void Butterfly_9242() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin39_Butterfly_Fiss_9276_9301_split[1]), &(SplitJoin39_Butterfly_Fiss_9276_9301_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9239() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin39_Butterfly_Fiss_9276_9301_split[0], pop_float(&Pre_CollapsedDataParallel_1_9069WEIGHTED_ROUND_ROBIN_Splitter_9239));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin39_Butterfly_Fiss_9276_9301_split[1], pop_float(&Pre_CollapsedDataParallel_1_9069WEIGHTED_ROUND_ROBIN_Splitter_9239));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9240() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9240Post_CollapsedDataParallel_2_9070, pop_float(&SplitJoin39_Butterfly_Fiss_9276_9301_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9240Post_CollapsedDataParallel_2_9070, pop_float(&SplitJoin39_Butterfly_Fiss_9276_9301_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_9070() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_9240Post_CollapsedDataParallel_2_9070), &(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_child0_9167_9299_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_9072() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_child0_9167_9299_split[2]), &(Pre_CollapsedDataParallel_1_9072WEIGHTED_ROUND_ROBIN_Splitter_9243));
	ENDFOR
}

void Butterfly_9245() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin43_Butterfly_Fiss_9277_9302_split[0]), &(SplitJoin43_Butterfly_Fiss_9277_9302_join[0]));
	ENDFOR
}

void Butterfly_9246() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin43_Butterfly_Fiss_9277_9302_split[1]), &(SplitJoin43_Butterfly_Fiss_9277_9302_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9243() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin43_Butterfly_Fiss_9277_9302_split[0], pop_float(&Pre_CollapsedDataParallel_1_9072WEIGHTED_ROUND_ROBIN_Splitter_9243));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin43_Butterfly_Fiss_9277_9302_split[1], pop_float(&Pre_CollapsedDataParallel_1_9072WEIGHTED_ROUND_ROBIN_Splitter_9243));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9244() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9244Post_CollapsedDataParallel_2_9073, pop_float(&SplitJoin43_Butterfly_Fiss_9277_9302_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9244Post_CollapsedDataParallel_2_9073, pop_float(&SplitJoin43_Butterfly_Fiss_9277_9302_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_9073() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_9244Post_CollapsedDataParallel_2_9073), &(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_child0_9167_9299_join[2]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_9075() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_child0_9167_9299_split[3]), &(Pre_CollapsedDataParallel_1_9075WEIGHTED_ROUND_ROBIN_Splitter_9247));
	ENDFOR
}

void Butterfly_9249() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin47_Butterfly_Fiss_9278_9303_split[0]), &(SplitJoin47_Butterfly_Fiss_9278_9303_join[0]));
	ENDFOR
}

void Butterfly_9250() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin47_Butterfly_Fiss_9278_9303_split[1]), &(SplitJoin47_Butterfly_Fiss_9278_9303_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9247() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin47_Butterfly_Fiss_9278_9303_split[0], pop_float(&Pre_CollapsedDataParallel_1_9075WEIGHTED_ROUND_ROBIN_Splitter_9247));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin47_Butterfly_Fiss_9278_9303_split[1], pop_float(&Pre_CollapsedDataParallel_1_9075WEIGHTED_ROUND_ROBIN_Splitter_9247));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9248() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9248Post_CollapsedDataParallel_2_9076, pop_float(&SplitJoin47_Butterfly_Fiss_9278_9303_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9248Post_CollapsedDataParallel_2_9076, pop_float(&SplitJoin47_Butterfly_Fiss_9278_9303_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_9076() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_9248Post_CollapsedDataParallel_2_9076), &(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_child0_9167_9299_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9175() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_child0_9167_9299_split[__iter_dec_], pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_Hier_9271_9298_split[0]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9176() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_Hier_9271_9298_join[0], pop_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_child0_9167_9299_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_9078() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_child1_9168_9304_split[0]), &(Pre_CollapsedDataParallel_1_9078WEIGHTED_ROUND_ROBIN_Splitter_9251));
	ENDFOR
}

void Butterfly_9253() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin53_Butterfly_Fiss_9279_9305_split[0]), &(SplitJoin53_Butterfly_Fiss_9279_9305_join[0]));
	ENDFOR
}

void Butterfly_9254() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin53_Butterfly_Fiss_9279_9305_split[1]), &(SplitJoin53_Butterfly_Fiss_9279_9305_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9251() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin53_Butterfly_Fiss_9279_9305_split[0], pop_float(&Pre_CollapsedDataParallel_1_9078WEIGHTED_ROUND_ROBIN_Splitter_9251));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin53_Butterfly_Fiss_9279_9305_split[1], pop_float(&Pre_CollapsedDataParallel_1_9078WEIGHTED_ROUND_ROBIN_Splitter_9251));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9252() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9252Post_CollapsedDataParallel_2_9079, pop_float(&SplitJoin53_Butterfly_Fiss_9279_9305_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9252Post_CollapsedDataParallel_2_9079, pop_float(&SplitJoin53_Butterfly_Fiss_9279_9305_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_9079() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_9252Post_CollapsedDataParallel_2_9079), &(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_child1_9168_9304_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_9081() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_child1_9168_9304_split[1]), &(Pre_CollapsedDataParallel_1_9081WEIGHTED_ROUND_ROBIN_Splitter_9255));
	ENDFOR
}

void Butterfly_9257() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin57_Butterfly_Fiss_9280_9306_split[0]), &(SplitJoin57_Butterfly_Fiss_9280_9306_join[0]));
	ENDFOR
}

void Butterfly_9258() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin57_Butterfly_Fiss_9280_9306_split[1]), &(SplitJoin57_Butterfly_Fiss_9280_9306_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9255() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin57_Butterfly_Fiss_9280_9306_split[0], pop_float(&Pre_CollapsedDataParallel_1_9081WEIGHTED_ROUND_ROBIN_Splitter_9255));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin57_Butterfly_Fiss_9280_9306_split[1], pop_float(&Pre_CollapsedDataParallel_1_9081WEIGHTED_ROUND_ROBIN_Splitter_9255));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9256() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9256Post_CollapsedDataParallel_2_9082, pop_float(&SplitJoin57_Butterfly_Fiss_9280_9306_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9256Post_CollapsedDataParallel_2_9082, pop_float(&SplitJoin57_Butterfly_Fiss_9280_9306_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_9082() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_9256Post_CollapsedDataParallel_2_9082), &(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_child1_9168_9304_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_9084() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_child1_9168_9304_split[2]), &(Pre_CollapsedDataParallel_1_9084WEIGHTED_ROUND_ROBIN_Splitter_9259));
	ENDFOR
}

void Butterfly_9261() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin61_Butterfly_Fiss_9281_9307_split[0]), &(SplitJoin61_Butterfly_Fiss_9281_9307_join[0]));
	ENDFOR
}

void Butterfly_9262() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin61_Butterfly_Fiss_9281_9307_split[1]), &(SplitJoin61_Butterfly_Fiss_9281_9307_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9259() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin61_Butterfly_Fiss_9281_9307_split[0], pop_float(&Pre_CollapsedDataParallel_1_9084WEIGHTED_ROUND_ROBIN_Splitter_9259));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin61_Butterfly_Fiss_9281_9307_split[1], pop_float(&Pre_CollapsedDataParallel_1_9084WEIGHTED_ROUND_ROBIN_Splitter_9259));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9260() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9260Post_CollapsedDataParallel_2_9085, pop_float(&SplitJoin61_Butterfly_Fiss_9281_9307_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9260Post_CollapsedDataParallel_2_9085, pop_float(&SplitJoin61_Butterfly_Fiss_9281_9307_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_9085() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_9260Post_CollapsedDataParallel_2_9085), &(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_child1_9168_9304_join[2]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_9087() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_child1_9168_9304_split[3]), &(Pre_CollapsedDataParallel_1_9087WEIGHTED_ROUND_ROBIN_Splitter_9263));
	ENDFOR
}

void Butterfly_9265() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin65_Butterfly_Fiss_9282_9308_split[0]), &(SplitJoin65_Butterfly_Fiss_9282_9308_join[0]));
	ENDFOR
}

void Butterfly_9266() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin65_Butterfly_Fiss_9282_9308_split[1]), &(SplitJoin65_Butterfly_Fiss_9282_9308_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9263() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin65_Butterfly_Fiss_9282_9308_split[0], pop_float(&Pre_CollapsedDataParallel_1_9087WEIGHTED_ROUND_ROBIN_Splitter_9263));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin65_Butterfly_Fiss_9282_9308_split[1], pop_float(&Pre_CollapsedDataParallel_1_9087WEIGHTED_ROUND_ROBIN_Splitter_9263));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9264() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9264Post_CollapsedDataParallel_2_9088, pop_float(&SplitJoin65_Butterfly_Fiss_9282_9308_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9264Post_CollapsedDataParallel_2_9088, pop_float(&SplitJoin65_Butterfly_Fiss_9282_9308_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_9088() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_9264Post_CollapsedDataParallel_2_9088), &(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_child1_9168_9304_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9177() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_child1_9168_9304_split[__iter_dec_], pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_Hier_9271_9298_split[1]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9178() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_Hier_9271_9298_join[1], pop_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_child1_9168_9304_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_9174() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_Hier_9271_9298_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9173WEIGHTED_ROUND_ROBIN_Splitter_9174));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_Hier_9271_9298_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9173WEIGHTED_ROUND_ROBIN_Splitter_9174));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9179() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9179WEIGHTED_ROUND_ROBIN_Splitter_9180, pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_Hier_9271_9298_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9179WEIGHTED_ROUND_ROBIN_Splitter_9180, pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_Hier_9271_9298_join[1]));
		ENDFOR
	ENDFOR
}}

void Butterfly_8809() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child0_9274_9310_split[0]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child0_9274_9310_join[0]));
	ENDFOR
}

void Butterfly_8810() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child0_9274_9310_split[1]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child0_9274_9310_join[1]));
	ENDFOR
}

void Butterfly_8811() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child0_9274_9310_split[2]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child0_9274_9310_join[2]));
	ENDFOR
}

void Butterfly_8812() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child0_9274_9310_split[3]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child0_9274_9310_join[3]));
	ENDFOR
}

void Butterfly_8813() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child0_9274_9310_split[4]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child0_9274_9310_join[4]));
	ENDFOR
}

void Butterfly_8814() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child0_9274_9310_split[5]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child0_9274_9310_join[5]));
	ENDFOR
}

void Butterfly_8815() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child0_9274_9310_split[6]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child0_9274_9310_join[6]));
	ENDFOR
}

void Butterfly_8816() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child0_9274_9310_split[7]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child0_9274_9310_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9181() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child0_9274_9310_split[__iter_dec_], pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_Hier_9273_9309_split[0]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9182() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_Hier_9273_9309_join[0], pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child0_9274_9310_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Butterfly_8817() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child1_9275_9311_split[0]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child1_9275_9311_join[0]));
	ENDFOR
}

void Butterfly_8818() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child1_9275_9311_split[1]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child1_9275_9311_join[1]));
	ENDFOR
}

void Butterfly_8819() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child1_9275_9311_split[2]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child1_9275_9311_join[2]));
	ENDFOR
}

void Butterfly_8820() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child1_9275_9311_split[3]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child1_9275_9311_join[3]));
	ENDFOR
}

void Butterfly_8821() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child1_9275_9311_split[4]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child1_9275_9311_join[4]));
	ENDFOR
}

void Butterfly_8822() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child1_9275_9311_split[5]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child1_9275_9311_join[5]));
	ENDFOR
}

void Butterfly_8823() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child1_9275_9311_split[6]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child1_9275_9311_join[6]));
	ENDFOR
}

void Butterfly_8824() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child1_9275_9311_split[7]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child1_9275_9311_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9183() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child1_9275_9311_split[__iter_dec_], pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_Hier_9273_9309_split[1]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9184() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_Hier_9273_9309_join[1], pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child1_9275_9311_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_9180() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_Hier_9273_9309_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9179WEIGHTED_ROUND_ROBIN_Splitter_9180));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_Hier_9273_9309_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9179WEIGHTED_ROUND_ROBIN_Splitter_9180));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9185() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9185BitReverse_8825, pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_Hier_9273_9309_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9185BitReverse_8825, pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_Hier_9273_9309_join[1]));
		ENDFOR
	ENDFOR
}}

int BitReverse_8825_bitrev(int inp, int numbits) {
	int rev = 0;
	FOR(int, i, 0,  < , numbits, i++) {
		rev = ((rev * 2) | (inp & 1)) ; 
		inp = (inp / 2) ; 
	}
	ENDFOR
	return rev;
}
void BitReverse(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, i__conflict__0, 0,  < , 32, i__conflict__0++) {
			int br = 0;
			br = BitReverse_8825_bitrev(i__conflict__0, 5) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (2 * br))) ; 
			push_float(&(*chanout), peek_float(&(*chanin), ((2 * br) + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&(*chanin)) ; 
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void BitReverse_8825() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		BitReverse(&(WEIGHTED_ROUND_ROBIN_Joiner_9185BitReverse_8825), &(BitReverse_8825FloatPrinter_8826));
	ENDFOR
}

void FloatPrinter(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void FloatPrinter_8826() {
	FOR(uint32_t, __iter_steady_, 0, <, 96, __iter_steady_++)
		FloatPrinter(&(BitReverse_8825FloatPrinter_8826));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 6, __iter_init_0_++)
		init_buffer_float(&SplitJoin83_Butterfly_Fiss_9284_9294_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 2, __iter_init_1_++)
		init_buffer_float(&SplitJoin43_Butterfly_Fiss_9277_9302_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 4, __iter_init_2_++)
		init_buffer_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_child0_9167_9299_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 2, __iter_init_3_++)
		init_buffer_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_Hier_9271_9298_join[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_9078WEIGHTED_ROUND_ROBIN_Splitter_9251);
	init_buffer_float(&Pre_CollapsedDataParallel_1_9072WEIGHTED_ROUND_ROBIN_Splitter_9243);
	init_buffer_float(&Pre_CollapsedDataParallel_1_9063WEIGHTED_ROUND_ROBIN_Splitter_9229);
	FOR(int, __iter_init_4_, 0, <, 4, __iter_init_4_++)
		init_buffer_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_child1_9168_9304_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 4, __iter_init_5_++)
		init_buffer_float(&SplitJoin8_Butterfly_Fiss_9270_9292_split[__iter_init_5_]);
	ENDFOR
	init_buffer_float(&BitReverse_8825FloatPrinter_8826);
	init_buffer_float(&Post_CollapsedDataParallel_2_9052WEIGHTED_ROUND_ROBIN_Splitter_9171);
	FOR(int, __iter_init_6_, 0, <, 2, __iter_init_6_++)
		init_buffer_float(&SplitJoin57_Butterfly_Fiss_9280_9306_split[__iter_init_6_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_9045WEIGHTED_ROUND_ROBIN_Splitter_9187);
	init_buffer_float(&Pre_CollapsedDataParallel_1_9054WEIGHTED_ROUND_ROBIN_Splitter_9203);
	init_buffer_float(&Pre_CollapsedDataParallel_1_9084WEIGHTED_ROUND_ROBIN_Splitter_9259);
	FOR(int, __iter_init_7_, 0, <, 2, __iter_init_7_++)
		init_buffer_float(&SplitJoin39_Butterfly_Fiss_9276_9301_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_float(&SplitJoin39_Butterfly_Fiss_9276_9301_split[__iter_init_8_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9204Post_CollapsedDataParallel_2_9055);
	init_buffer_float(&Pre_CollapsedDataParallel_1_9087WEIGHTED_ROUND_ROBIN_Splitter_9263);
	FOR(int, __iter_init_9_, 0, <, 4, __iter_init_9_++)
		init_buffer_float(&SplitJoin72_Butterfly_Fiss_9283_9293_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 6, __iter_init_10_++)
		init_buffer_float(&SplitJoin4_Butterfly_Fiss_9269_9290_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 2, __iter_init_11_++)
		init_buffer_float(&SplitJoin85_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_child1_9143_9295_join[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 4, __iter_init_12_++)
		init_buffer_float(&SplitJoin8_Butterfly_Fiss_9270_9292_join[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_float(&SplitJoin14_Butterfly_Fiss_9272_9300_split[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 6, __iter_init_14_++)
		init_buffer_float(&SplitJoin0_Butterfly_Fiss_9267_9288_split[__iter_init_14_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9173WEIGHTED_ROUND_ROBIN_Splitter_9174);
	FOR(int, __iter_init_15_, 0, <, 2, __iter_init_15_++)
		init_buffer_float(&SplitJoin61_Butterfly_Fiss_9281_9307_split[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 6, __iter_init_16_++)
		init_buffer_float(&SplitJoin83_Butterfly_Fiss_9284_9294_join[__iter_init_16_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9224Post_CollapsedDataParallel_2_9061);
	FOR(int, __iter_init_17_, 0, <, 6, __iter_init_17_++)
		init_buffer_float(&SplitJoin0_Butterfly_Fiss_9267_9288_join[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 2, __iter_init_18_++)
		init_buffer_float(&SplitJoin47_Butterfly_Fiss_9278_9303_join[__iter_init_18_]);
	ENDFOR
	FOR(int, __iter_init_19_, 0, <, 2, __iter_init_19_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_8702_9097_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_Hier_9268_9289_split[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 4, __iter_init_20_++)
		init_buffer_float(&SplitJoin93_Butterfly_Fiss_9286_9297_split[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 2, __iter_init_21_++)
		init_buffer_float(&SplitJoin53_Butterfly_Fiss_9279_9305_split[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 2, __iter_init_22_++)
		init_buffer_float(&SplitJoin61_Butterfly_Fiss_9281_9307_join[__iter_init_22_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_9046WEIGHTED_ROUND_ROBIN_Splitter_9089);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9256Post_CollapsedDataParallel_2_9082);
	FOR(int, __iter_init_23_, 0, <, 4, __iter_init_23_++)
		init_buffer_float(&SplitJoin87_Butterfly_Fiss_9285_9296_join[__iter_init_23_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_9075WEIGHTED_ROUND_ROBIN_Splitter_9247);
	FOR(int, __iter_init_24_, 0, <, 2, __iter_init_24_++)
		init_buffer_float(&SplitJoin57_Butterfly_Fiss_9280_9306_join[__iter_init_24_]);
	ENDFOR
	FOR(int, __iter_init_25_, 0, <, 2, __iter_init_25_++)
		init_buffer_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_child0_9138_9291_split[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 2, __iter_init_26_++)
		init_buffer_float(&SplitJoin14_Butterfly_Fiss_9272_9300_join[__iter_init_26_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_9057WEIGHTED_ROUND_ROBIN_Splitter_9209);
	init_buffer_float(&Pre_CollapsedDataParallel_1_9051WEIGHTED_ROUND_ROBIN_Splitter_9215);
	FOR(int, __iter_init_27_, 0, <, 2, __iter_init_27_++)
		init_buffer_float(&SplitJoin65_Butterfly_Fiss_9282_9308_split[__iter_init_27_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9185BitReverse_8825);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9230Post_CollapsedDataParallel_2_9064);
	FOR(int, __iter_init_28_, 0, <, 8, __iter_init_28_++)
		init_buffer_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child0_9274_9310_split[__iter_init_28_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9236Post_CollapsedDataParallel_2_9067);
	FOR(int, __iter_init_29_, 0, <, 6, __iter_init_29_++)
		init_buffer_float(&SplitJoin4_Butterfly_Fiss_9269_9290_join[__iter_init_29_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9240Post_CollapsedDataParallel_2_9070);
	FOR(int, __iter_init_30_, 0, <, 2, __iter_init_30_++)
		init_buffer_float(&SplitJoin47_Butterfly_Fiss_9278_9303_split[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 2, __iter_init_31_++)
		init_buffer_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_Hier_9273_9309_split[__iter_init_31_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9216Post_CollapsedDataParallel_2_9052);
	FOR(int, __iter_init_32_, 0, <, 2, __iter_init_32_++)
		init_buffer_float(&SplitJoin43_Butterfly_Fiss_9277_9302_join[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 8, __iter_init_33_++)
		init_buffer_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child1_9275_9311_split[__iter_init_33_]);
	ENDFOR
	FOR(int, __iter_init_34_, 0, <, 2, __iter_init_34_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_8702_9097_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_Hier_9268_9289_join[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 8, __iter_init_35_++)
		init_buffer_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child0_9274_9310_join[__iter_init_35_]);
	ENDFOR
	FOR(int, __iter_init_36_, 0, <, 4, __iter_init_36_++)
		init_buffer_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_child0_9167_9299_split[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 2, __iter_init_37_++)
		init_buffer_float(&SplitJoin85_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_child1_9143_9295_split[__iter_init_37_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_9049WEIGHTED_ROUND_ROBIN_Splitter_9169);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9179WEIGHTED_ROUND_ROBIN_Splitter_9180);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9252Post_CollapsedDataParallel_2_9079);
	FOR(int, __iter_init_38_, 0, <, 2, __iter_init_38_++)
		init_buffer_float(&SplitJoin65_Butterfly_Fiss_9282_9308_join[__iter_init_38_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_9081WEIGHTED_ROUND_ROBIN_Splitter_9255);
	FOR(int, __iter_init_39_, 0, <, 4, __iter_init_39_++)
		init_buffer_float(&SplitJoin93_Butterfly_Fiss_9286_9297_join[__iter_init_39_]);
	ENDFOR
	FOR(int, __iter_init_40_, 0, <, 4, __iter_init_40_++)
		init_buffer_float(&SplitJoin72_Butterfly_Fiss_9283_9293_join[__iter_init_40_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9260Post_CollapsedDataParallel_2_9085);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9264Post_CollapsedDataParallel_2_9088);
	init_buffer_float(&Pre_CollapsedDataParallel_1_9060WEIGHTED_ROUND_ROBIN_Splitter_9223);
	FOR(int, __iter_init_41_, 0, <, 4, __iter_init_41_++)
		init_buffer_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_child1_9168_9304_join[__iter_init_41_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_9066WEIGHTED_ROUND_ROBIN_Splitter_9235);
	FOR(int, __iter_init_42_, 0, <, 8, __iter_init_42_++)
		init_buffer_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_child1_9275_9311_join[__iter_init_42_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9188Post_CollapsedDataParallel_2_9046);
	init_buffer_float(&Pre_CollapsedDataParallel_1_9048WEIGHTED_ROUND_ROBIN_Splitter_9195);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9196Post_CollapsedDataParallel_2_9049);
	FOR(int, __iter_init_43_, 0, <, 2, __iter_init_43_++)
		init_buffer_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_8724_9101_Hier_Hier_9271_9298_split[__iter_init_43_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_9069WEIGHTED_ROUND_ROBIN_Splitter_9239);
	FOR(int, __iter_init_44_, 0, <, 4, __iter_init_44_++)
		init_buffer_float(&SplitJoin87_Butterfly_Fiss_9285_9296_split[__iter_init_44_]);
	ENDFOR
	FOR(int, __iter_init_45_, 0, <, 2, __iter_init_45_++)
		init_buffer_float(&SplitJoin53_Butterfly_Fiss_9279_9305_join[__iter_init_45_]);
	ENDFOR
	FOR(int, __iter_init_46_, 0, <, 2, __iter_init_46_++)
		init_buffer_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_8711_9099_child0_9138_9291_join[__iter_init_46_]);
	ENDFOR
	FOR(int, __iter_init_47_, 0, <, 2, __iter_init_47_++)
		init_buffer_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_8742_9103_Hier_Hier_9273_9309_join[__iter_init_47_]);
	ENDFOR
	init_buffer_float(&FloatSource_8744Pre_CollapsedDataParallel_1_9045);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9244Post_CollapsedDataParallel_2_9073);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9210Post_CollapsedDataParallel_2_9058);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9248Post_CollapsedDataParallel_2_9076);
// --- init: FloatSource_8744
	 {
	FOR(int, i, 0,  < , 32, i++) {
		FloatSource_8744_s.A_re[i] = 0.0 ; 
		FloatSource_8744_s.A_im[i] = 0.0 ; 
	}
	ENDFOR
	FloatSource_8744_s.A_re[1] = 1.0 ; 
	FloatSource_8744_s.idx = 0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FloatSource_8744();
		Pre_CollapsedDataParallel_1_9045();
		WEIGHTED_ROUND_ROBIN_Splitter_9187();
			Butterfly_9189();
			Butterfly_9190();
			Butterfly_9191();
			Butterfly_9192();
			Butterfly_9193();
			Butterfly_9194();
		WEIGHTED_ROUND_ROBIN_Joiner_9188();
		Post_CollapsedDataParallel_2_9046();
		WEIGHTED_ROUND_ROBIN_Splitter_9089();
			Pre_CollapsedDataParallel_1_9048();
			WEIGHTED_ROUND_ROBIN_Splitter_9195();
				Butterfly_9197();
				Butterfly_9198();
				Butterfly_9199();
				Butterfly_9200();
				Butterfly_9201();
				Butterfly_9202();
			WEIGHTED_ROUND_ROBIN_Joiner_9196();
			Post_CollapsedDataParallel_2_9049();
			WEIGHTED_ROUND_ROBIN_Splitter_9169();
				Pre_CollapsedDataParallel_1_9054();
				WEIGHTED_ROUND_ROBIN_Splitter_9203();
					Butterfly_9205();
					Butterfly_9206();
					Butterfly_9207();
					Butterfly_9208();
				WEIGHTED_ROUND_ROBIN_Joiner_9204();
				Post_CollapsedDataParallel_2_9055();
				Pre_CollapsedDataParallel_1_9057();
				WEIGHTED_ROUND_ROBIN_Splitter_9209();
					Butterfly_9211();
					Butterfly_9212();
					Butterfly_9213();
					Butterfly_9214();
				WEIGHTED_ROUND_ROBIN_Joiner_9210();
				Post_CollapsedDataParallel_2_9058();
			WEIGHTED_ROUND_ROBIN_Joiner_9170();
			Pre_CollapsedDataParallel_1_9051();
			WEIGHTED_ROUND_ROBIN_Splitter_9215();
				Butterfly_9217();
				Butterfly_9218();
				Butterfly_9219();
				Butterfly_9220();
				Butterfly_9221();
				Butterfly_9222();
			WEIGHTED_ROUND_ROBIN_Joiner_9216();
			Post_CollapsedDataParallel_2_9052();
			WEIGHTED_ROUND_ROBIN_Splitter_9171();
				Pre_CollapsedDataParallel_1_9060();
				WEIGHTED_ROUND_ROBIN_Splitter_9223();
					Butterfly_9225();
					Butterfly_9226();
					Butterfly_9227();
					Butterfly_9228();
				WEIGHTED_ROUND_ROBIN_Joiner_9224();
				Post_CollapsedDataParallel_2_9061();
				Pre_CollapsedDataParallel_1_9063();
				WEIGHTED_ROUND_ROBIN_Splitter_9229();
					Butterfly_9231();
					Butterfly_9232();
					Butterfly_9233();
					Butterfly_9234();
				WEIGHTED_ROUND_ROBIN_Joiner_9230();
				Post_CollapsedDataParallel_2_9064();
			WEIGHTED_ROUND_ROBIN_Joiner_9172();
		WEIGHTED_ROUND_ROBIN_Joiner_9173();
		WEIGHTED_ROUND_ROBIN_Splitter_9174();
			WEIGHTED_ROUND_ROBIN_Splitter_9175();
				Pre_CollapsedDataParallel_1_9066();
				WEIGHTED_ROUND_ROBIN_Splitter_9235();
					Butterfly_9237();
					Butterfly_9238();
				WEIGHTED_ROUND_ROBIN_Joiner_9236();
				Post_CollapsedDataParallel_2_9067();
				Pre_CollapsedDataParallel_1_9069();
				WEIGHTED_ROUND_ROBIN_Splitter_9239();
					Butterfly_9241();
					Butterfly_9242();
				WEIGHTED_ROUND_ROBIN_Joiner_9240();
				Post_CollapsedDataParallel_2_9070();
				Pre_CollapsedDataParallel_1_9072();
				WEIGHTED_ROUND_ROBIN_Splitter_9243();
					Butterfly_9245();
					Butterfly_9246();
				WEIGHTED_ROUND_ROBIN_Joiner_9244();
				Post_CollapsedDataParallel_2_9073();
				Pre_CollapsedDataParallel_1_9075();
				WEIGHTED_ROUND_ROBIN_Splitter_9247();
					Butterfly_9249();
					Butterfly_9250();
				WEIGHTED_ROUND_ROBIN_Joiner_9248();
				Post_CollapsedDataParallel_2_9076();
			WEIGHTED_ROUND_ROBIN_Joiner_9176();
			WEIGHTED_ROUND_ROBIN_Splitter_9177();
				Pre_CollapsedDataParallel_1_9078();
				WEIGHTED_ROUND_ROBIN_Splitter_9251();
					Butterfly_9253();
					Butterfly_9254();
				WEIGHTED_ROUND_ROBIN_Joiner_9252();
				Post_CollapsedDataParallel_2_9079();
				Pre_CollapsedDataParallel_1_9081();
				WEIGHTED_ROUND_ROBIN_Splitter_9255();
					Butterfly_9257();
					Butterfly_9258();
				WEIGHTED_ROUND_ROBIN_Joiner_9256();
				Post_CollapsedDataParallel_2_9082();
				Pre_CollapsedDataParallel_1_9084();
				WEIGHTED_ROUND_ROBIN_Splitter_9259();
					Butterfly_9261();
					Butterfly_9262();
				WEIGHTED_ROUND_ROBIN_Joiner_9260();
				Post_CollapsedDataParallel_2_9085();
				Pre_CollapsedDataParallel_1_9087();
				WEIGHTED_ROUND_ROBIN_Splitter_9263();
					Butterfly_9265();
					Butterfly_9266();
				WEIGHTED_ROUND_ROBIN_Joiner_9264();
				Post_CollapsedDataParallel_2_9088();
			WEIGHTED_ROUND_ROBIN_Joiner_9178();
		WEIGHTED_ROUND_ROBIN_Joiner_9179();
		WEIGHTED_ROUND_ROBIN_Splitter_9180();
			WEIGHTED_ROUND_ROBIN_Splitter_9181();
				Butterfly_8809();
				Butterfly_8810();
				Butterfly_8811();
				Butterfly_8812();
				Butterfly_8813();
				Butterfly_8814();
				Butterfly_8815();
				Butterfly_8816();
			WEIGHTED_ROUND_ROBIN_Joiner_9182();
			WEIGHTED_ROUND_ROBIN_Splitter_9183();
				Butterfly_8817();
				Butterfly_8818();
				Butterfly_8819();
				Butterfly_8820();
				Butterfly_8821();
				Butterfly_8822();
				Butterfly_8823();
				Butterfly_8824();
			WEIGHTED_ROUND_ROBIN_Joiner_9184();
		WEIGHTED_ROUND_ROBIN_Joiner_9185();
		BitReverse_8825();
		FloatPrinter_8826();
	ENDFOR
	return EXIT_SUCCESS;
}
