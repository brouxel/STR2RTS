#include "PEG29-FFT6.h"

buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_913_923_split[4];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_919_929_split[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_810WEIGHTED_ROUND_ROBIN_Splitter_815;
buffer_complex_t CombineDFT_802CPrinter_803;
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_915_925_split[16];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_919_929_join[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_909CombineDFT_802;
buffer_complex_t SplitJoin12_CombineDFT_Fiss_918_928_join[8];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_903WEIGHTED_ROUND_ROBIN_Splitter_908;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_826WEIGHTED_ROUND_ROBIN_Splitter_843;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_893WEIGHTED_ROUND_ROBIN_Splitter_902;
buffer_complex_t SplitJoin16_CombineDFT_Fiss_920_930_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_875WEIGHTED_ROUND_ROBIN_Splitter_892;
buffer_complex_t FFTTestSource_791FFTReorderSimple_792;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_844WEIGHTED_ROUND_ROBIN_Splitter_874;
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_914_924_split[8];
buffer_complex_t SplitJoin12_CombineDFT_Fiss_918_928_split[8];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_917_927_split[16];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_920_930_join[2];
buffer_complex_t FFTReorderSimple_792WEIGHTED_ROUND_ROBIN_Splitter_805;
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_915_925_join[16];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_806WEIGHTED_ROUND_ROBIN_Splitter_809;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_916_926_join[29];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_914_924_join[8];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_912_922_split[2];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_912_922_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_816WEIGHTED_ROUND_ROBIN_Splitter_825;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_916_926_split[29];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_917_927_join[16];
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_913_923_join[4];


CombineDFT_845_t CombineDFT_845_s;
CombineDFT_845_t CombineDFT_846_s;
CombineDFT_845_t CombineDFT_847_s;
CombineDFT_845_t CombineDFT_848_s;
CombineDFT_845_t CombineDFT_849_s;
CombineDFT_845_t CombineDFT_850_s;
CombineDFT_845_t CombineDFT_851_s;
CombineDFT_845_t CombineDFT_852_s;
CombineDFT_845_t CombineDFT_853_s;
CombineDFT_845_t CombineDFT_854_s;
CombineDFT_845_t CombineDFT_855_s;
CombineDFT_845_t CombineDFT_856_s;
CombineDFT_845_t CombineDFT_857_s;
CombineDFT_845_t CombineDFT_858_s;
CombineDFT_845_t CombineDFT_859_s;
CombineDFT_845_t CombineDFT_860_s;
CombineDFT_845_t CombineDFT_861_s;
CombineDFT_845_t CombineDFT_862_s;
CombineDFT_845_t CombineDFT_863_s;
CombineDFT_845_t CombineDFT_864_s;
CombineDFT_845_t CombineDFT_865_s;
CombineDFT_845_t CombineDFT_866_s;
CombineDFT_845_t CombineDFT_867_s;
CombineDFT_845_t CombineDFT_868_s;
CombineDFT_845_t CombineDFT_869_s;
CombineDFT_845_t CombineDFT_870_s;
CombineDFT_845_t CombineDFT_871_s;
CombineDFT_845_t CombineDFT_872_s;
CombineDFT_845_t CombineDFT_873_s;
CombineDFT_845_t CombineDFT_876_s;
CombineDFT_845_t CombineDFT_877_s;
CombineDFT_845_t CombineDFT_878_s;
CombineDFT_845_t CombineDFT_879_s;
CombineDFT_845_t CombineDFT_880_s;
CombineDFT_845_t CombineDFT_881_s;
CombineDFT_845_t CombineDFT_882_s;
CombineDFT_845_t CombineDFT_883_s;
CombineDFT_845_t CombineDFT_884_s;
CombineDFT_845_t CombineDFT_885_s;
CombineDFT_845_t CombineDFT_886_s;
CombineDFT_845_t CombineDFT_887_s;
CombineDFT_845_t CombineDFT_888_s;
CombineDFT_845_t CombineDFT_889_s;
CombineDFT_845_t CombineDFT_890_s;
CombineDFT_845_t CombineDFT_891_s;
CombineDFT_845_t CombineDFT_894_s;
CombineDFT_845_t CombineDFT_895_s;
CombineDFT_845_t CombineDFT_896_s;
CombineDFT_845_t CombineDFT_897_s;
CombineDFT_845_t CombineDFT_898_s;
CombineDFT_845_t CombineDFT_899_s;
CombineDFT_845_t CombineDFT_900_s;
CombineDFT_845_t CombineDFT_901_s;
CombineDFT_845_t CombineDFT_904_s;
CombineDFT_845_t CombineDFT_905_s;
CombineDFT_845_t CombineDFT_906_s;
CombineDFT_845_t CombineDFT_907_s;
CombineDFT_845_t CombineDFT_910_s;
CombineDFT_845_t CombineDFT_911_s;
CombineDFT_845_t CombineDFT_802_s;

void FFTTestSource(buffer_complex_t *chanout) {
		complex_t c1;
		complex_t zero;
		c1.real = 1.0 ; 
		c1.imag = 0.0 ; 
		zero.real = 0.0 ; 
		zero.imag = 0.0 ; 
		push_complex(&(*chanout), zero) ; 
		push_complex(&(*chanout), c1) ; 
		FOR(int, i, 0,  < , 62, i++) {
			push_complex(&(*chanout), zero) ; 
		}
		ENDFOR
	}


void FFTTestSource_791() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTTestSource(&(FFTTestSource_791FFTReorderSimple_792));
	ENDFOR
}

void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 64, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&(*chanin)) ; 
		}
		ENDFOR
	}


void FFTReorderSimple_792() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTReorderSimple(&(FFTTestSource_791FFTReorderSimple_792), &(FFTReorderSimple_792WEIGHTED_ROUND_ROBIN_Splitter_805));
	ENDFOR
}

void FFTReorderSimple_807() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_912_922_split[0]), &(SplitJoin0_FFTReorderSimple_Fiss_912_922_join[0]));
	ENDFOR
}

void FFTReorderSimple_808() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_912_922_split[1]), &(SplitJoin0_FFTReorderSimple_Fiss_912_922_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_805() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_912_922_split[0], pop_complex(&FFTReorderSimple_792WEIGHTED_ROUND_ROBIN_Splitter_805));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_912_922_split[1], pop_complex(&FFTReorderSimple_792WEIGHTED_ROUND_ROBIN_Splitter_805));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_806() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_806WEIGHTED_ROUND_ROBIN_Splitter_809, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_912_922_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_806WEIGHTED_ROUND_ROBIN_Splitter_809, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_912_922_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_811() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_913_923_split[0]), &(SplitJoin2_FFTReorderSimple_Fiss_913_923_join[0]));
	ENDFOR
}

void FFTReorderSimple_812() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_913_923_split[1]), &(SplitJoin2_FFTReorderSimple_Fiss_913_923_join[1]));
	ENDFOR
}

void FFTReorderSimple_813() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_913_923_split[2]), &(SplitJoin2_FFTReorderSimple_Fiss_913_923_join[2]));
	ENDFOR
}

void FFTReorderSimple_814() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_913_923_split[3]), &(SplitJoin2_FFTReorderSimple_Fiss_913_923_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_809() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin2_FFTReorderSimple_Fiss_913_923_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_806WEIGHTED_ROUND_ROBIN_Splitter_809));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_810() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_810WEIGHTED_ROUND_ROBIN_Splitter_815, pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_913_923_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_817() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_914_924_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_914_924_join[0]));
	ENDFOR
}

void FFTReorderSimple_818() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_914_924_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_914_924_join[1]));
	ENDFOR
}

void FFTReorderSimple_819() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_914_924_split[2]), &(SplitJoin4_FFTReorderSimple_Fiss_914_924_join[2]));
	ENDFOR
}

void FFTReorderSimple_820() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_914_924_split[3]), &(SplitJoin4_FFTReorderSimple_Fiss_914_924_join[3]));
	ENDFOR
}

void FFTReorderSimple_821() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_914_924_split[4]), &(SplitJoin4_FFTReorderSimple_Fiss_914_924_join[4]));
	ENDFOR
}

void FFTReorderSimple_822() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_914_924_split[5]), &(SplitJoin4_FFTReorderSimple_Fiss_914_924_join[5]));
	ENDFOR
}

void FFTReorderSimple_823() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_914_924_split[6]), &(SplitJoin4_FFTReorderSimple_Fiss_914_924_join[6]));
	ENDFOR
}

void FFTReorderSimple_824() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_914_924_split[7]), &(SplitJoin4_FFTReorderSimple_Fiss_914_924_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_815() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin4_FFTReorderSimple_Fiss_914_924_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_810WEIGHTED_ROUND_ROBIN_Splitter_815));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_816() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_816WEIGHTED_ROUND_ROBIN_Splitter_825, pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_914_924_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_827() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_915_925_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_915_925_join[0]));
	ENDFOR
}

void FFTReorderSimple_828() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_915_925_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_915_925_join[1]));
	ENDFOR
}

void FFTReorderSimple_829() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_915_925_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_915_925_join[2]));
	ENDFOR
}

void FFTReorderSimple_830() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_915_925_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_915_925_join[3]));
	ENDFOR
}

void FFTReorderSimple_831() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_915_925_split[4]), &(SplitJoin6_FFTReorderSimple_Fiss_915_925_join[4]));
	ENDFOR
}

void FFTReorderSimple_832() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_915_925_split[5]), &(SplitJoin6_FFTReorderSimple_Fiss_915_925_join[5]));
	ENDFOR
}

void FFTReorderSimple_833() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_915_925_split[6]), &(SplitJoin6_FFTReorderSimple_Fiss_915_925_join[6]));
	ENDFOR
}

void FFTReorderSimple_834() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_915_925_split[7]), &(SplitJoin6_FFTReorderSimple_Fiss_915_925_join[7]));
	ENDFOR
}

void FFTReorderSimple_835() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_915_925_split[8]), &(SplitJoin6_FFTReorderSimple_Fiss_915_925_join[8]));
	ENDFOR
}

void FFTReorderSimple_836() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_915_925_split[9]), &(SplitJoin6_FFTReorderSimple_Fiss_915_925_join[9]));
	ENDFOR
}

void FFTReorderSimple_837() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_915_925_split[10]), &(SplitJoin6_FFTReorderSimple_Fiss_915_925_join[10]));
	ENDFOR
}

void FFTReorderSimple_838() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_915_925_split[11]), &(SplitJoin6_FFTReorderSimple_Fiss_915_925_join[11]));
	ENDFOR
}

void FFTReorderSimple_839() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_915_925_split[12]), &(SplitJoin6_FFTReorderSimple_Fiss_915_925_join[12]));
	ENDFOR
}

void FFTReorderSimple_840() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_915_925_split[13]), &(SplitJoin6_FFTReorderSimple_Fiss_915_925_join[13]));
	ENDFOR
}

void FFTReorderSimple_841() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_915_925_split[14]), &(SplitJoin6_FFTReorderSimple_Fiss_915_925_join[14]));
	ENDFOR
}

void FFTReorderSimple_842() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_915_925_split[15]), &(SplitJoin6_FFTReorderSimple_Fiss_915_925_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_825() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin6_FFTReorderSimple_Fiss_915_925_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_816WEIGHTED_ROUND_ROBIN_Splitter_825));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_826() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_826WEIGHTED_ROUND_ROBIN_Splitter_843, pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_915_925_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&(*chanin), (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_845_s.wn.real) - (w.imag * CombineDFT_845_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_845_s.wn.imag) + (w.imag * CombineDFT_845_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&(*chanin)) ; 
			push_complex(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_845() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_916_926_split[0]), &(SplitJoin8_CombineDFT_Fiss_916_926_join[0]));
	ENDFOR
}

void CombineDFT_846() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_916_926_split[1]), &(SplitJoin8_CombineDFT_Fiss_916_926_join[1]));
	ENDFOR
}

void CombineDFT_847() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_916_926_split[2]), &(SplitJoin8_CombineDFT_Fiss_916_926_join[2]));
	ENDFOR
}

void CombineDFT_848() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_916_926_split[3]), &(SplitJoin8_CombineDFT_Fiss_916_926_join[3]));
	ENDFOR
}

void CombineDFT_849() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_916_926_split[4]), &(SplitJoin8_CombineDFT_Fiss_916_926_join[4]));
	ENDFOR
}

void CombineDFT_850() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_916_926_split[5]), &(SplitJoin8_CombineDFT_Fiss_916_926_join[5]));
	ENDFOR
}

void CombineDFT_851() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_916_926_split[6]), &(SplitJoin8_CombineDFT_Fiss_916_926_join[6]));
	ENDFOR
}

void CombineDFT_852() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_916_926_split[7]), &(SplitJoin8_CombineDFT_Fiss_916_926_join[7]));
	ENDFOR
}

void CombineDFT_853() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_916_926_split[8]), &(SplitJoin8_CombineDFT_Fiss_916_926_join[8]));
	ENDFOR
}

void CombineDFT_854() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_916_926_split[9]), &(SplitJoin8_CombineDFT_Fiss_916_926_join[9]));
	ENDFOR
}

void CombineDFT_855() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_916_926_split[10]), &(SplitJoin8_CombineDFT_Fiss_916_926_join[10]));
	ENDFOR
}

void CombineDFT_856() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_916_926_split[11]), &(SplitJoin8_CombineDFT_Fiss_916_926_join[11]));
	ENDFOR
}

void CombineDFT_857() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_916_926_split[12]), &(SplitJoin8_CombineDFT_Fiss_916_926_join[12]));
	ENDFOR
}

void CombineDFT_858() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_916_926_split[13]), &(SplitJoin8_CombineDFT_Fiss_916_926_join[13]));
	ENDFOR
}

void CombineDFT_859() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_916_926_split[14]), &(SplitJoin8_CombineDFT_Fiss_916_926_join[14]));
	ENDFOR
}

void CombineDFT_860() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_916_926_split[15]), &(SplitJoin8_CombineDFT_Fiss_916_926_join[15]));
	ENDFOR
}

void CombineDFT_861() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_916_926_split[16]), &(SplitJoin8_CombineDFT_Fiss_916_926_join[16]));
	ENDFOR
}

void CombineDFT_862() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_916_926_split[17]), &(SplitJoin8_CombineDFT_Fiss_916_926_join[17]));
	ENDFOR
}

void CombineDFT_863() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_916_926_split[18]), &(SplitJoin8_CombineDFT_Fiss_916_926_join[18]));
	ENDFOR
}

void CombineDFT_864() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_916_926_split[19]), &(SplitJoin8_CombineDFT_Fiss_916_926_join[19]));
	ENDFOR
}

void CombineDFT_865() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_916_926_split[20]), &(SplitJoin8_CombineDFT_Fiss_916_926_join[20]));
	ENDFOR
}

void CombineDFT_866() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_916_926_split[21]), &(SplitJoin8_CombineDFT_Fiss_916_926_join[21]));
	ENDFOR
}

void CombineDFT_867() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_916_926_split[22]), &(SplitJoin8_CombineDFT_Fiss_916_926_join[22]));
	ENDFOR
}

void CombineDFT_868() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_916_926_split[23]), &(SplitJoin8_CombineDFT_Fiss_916_926_join[23]));
	ENDFOR
}

void CombineDFT_869() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_916_926_split[24]), &(SplitJoin8_CombineDFT_Fiss_916_926_join[24]));
	ENDFOR
}

void CombineDFT_870() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_916_926_split[25]), &(SplitJoin8_CombineDFT_Fiss_916_926_join[25]));
	ENDFOR
}

void CombineDFT_871() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_916_926_split[26]), &(SplitJoin8_CombineDFT_Fiss_916_926_join[26]));
	ENDFOR
}

void CombineDFT_872() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_916_926_split[27]), &(SplitJoin8_CombineDFT_Fiss_916_926_join[27]));
	ENDFOR
}

void CombineDFT_873() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_916_926_split[28]), &(SplitJoin8_CombineDFT_Fiss_916_926_join[28]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_843() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 29, __iter_++)
			push_complex(&SplitJoin8_CombineDFT_Fiss_916_926_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_826WEIGHTED_ROUND_ROBIN_Splitter_843));
			push_complex(&SplitJoin8_CombineDFT_Fiss_916_926_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_826WEIGHTED_ROUND_ROBIN_Splitter_843));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_844() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 29, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_844WEIGHTED_ROUND_ROBIN_Splitter_874, pop_complex(&SplitJoin8_CombineDFT_Fiss_916_926_join[__iter_]));
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_844WEIGHTED_ROUND_ROBIN_Splitter_874, pop_complex(&SplitJoin8_CombineDFT_Fiss_916_926_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_876() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_917_927_split[0]), &(SplitJoin10_CombineDFT_Fiss_917_927_join[0]));
	ENDFOR
}

void CombineDFT_877() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_917_927_split[1]), &(SplitJoin10_CombineDFT_Fiss_917_927_join[1]));
	ENDFOR
}

void CombineDFT_878() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_917_927_split[2]), &(SplitJoin10_CombineDFT_Fiss_917_927_join[2]));
	ENDFOR
}

void CombineDFT_879() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_917_927_split[3]), &(SplitJoin10_CombineDFT_Fiss_917_927_join[3]));
	ENDFOR
}

void CombineDFT_880() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_917_927_split[4]), &(SplitJoin10_CombineDFT_Fiss_917_927_join[4]));
	ENDFOR
}

void CombineDFT_881() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_917_927_split[5]), &(SplitJoin10_CombineDFT_Fiss_917_927_join[5]));
	ENDFOR
}

void CombineDFT_882() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_917_927_split[6]), &(SplitJoin10_CombineDFT_Fiss_917_927_join[6]));
	ENDFOR
}

void CombineDFT_883() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_917_927_split[7]), &(SplitJoin10_CombineDFT_Fiss_917_927_join[7]));
	ENDFOR
}

void CombineDFT_884() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_917_927_split[8]), &(SplitJoin10_CombineDFT_Fiss_917_927_join[8]));
	ENDFOR
}

void CombineDFT_885() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_917_927_split[9]), &(SplitJoin10_CombineDFT_Fiss_917_927_join[9]));
	ENDFOR
}

void CombineDFT_886() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_917_927_split[10]), &(SplitJoin10_CombineDFT_Fiss_917_927_join[10]));
	ENDFOR
}

void CombineDFT_887() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_917_927_split[11]), &(SplitJoin10_CombineDFT_Fiss_917_927_join[11]));
	ENDFOR
}

void CombineDFT_888() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_917_927_split[12]), &(SplitJoin10_CombineDFT_Fiss_917_927_join[12]));
	ENDFOR
}

void CombineDFT_889() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_917_927_split[13]), &(SplitJoin10_CombineDFT_Fiss_917_927_join[13]));
	ENDFOR
}

void CombineDFT_890() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_917_927_split[14]), &(SplitJoin10_CombineDFT_Fiss_917_927_join[14]));
	ENDFOR
}

void CombineDFT_891() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_917_927_split[15]), &(SplitJoin10_CombineDFT_Fiss_917_927_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_874() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin10_CombineDFT_Fiss_917_927_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_844WEIGHTED_ROUND_ROBIN_Splitter_874));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_875() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_875WEIGHTED_ROUND_ROBIN_Splitter_892, pop_complex(&SplitJoin10_CombineDFT_Fiss_917_927_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_894() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_918_928_split[0]), &(SplitJoin12_CombineDFT_Fiss_918_928_join[0]));
	ENDFOR
}

void CombineDFT_895() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_918_928_split[1]), &(SplitJoin12_CombineDFT_Fiss_918_928_join[1]));
	ENDFOR
}

void CombineDFT_896() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_918_928_split[2]), &(SplitJoin12_CombineDFT_Fiss_918_928_join[2]));
	ENDFOR
}

void CombineDFT_897() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_918_928_split[3]), &(SplitJoin12_CombineDFT_Fiss_918_928_join[3]));
	ENDFOR
}

void CombineDFT_898() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_918_928_split[4]), &(SplitJoin12_CombineDFT_Fiss_918_928_join[4]));
	ENDFOR
}

void CombineDFT_899() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_918_928_split[5]), &(SplitJoin12_CombineDFT_Fiss_918_928_join[5]));
	ENDFOR
}

void CombineDFT_900() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_918_928_split[6]), &(SplitJoin12_CombineDFT_Fiss_918_928_join[6]));
	ENDFOR
}

void CombineDFT_901() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_918_928_split[7]), &(SplitJoin12_CombineDFT_Fiss_918_928_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_892() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin12_CombineDFT_Fiss_918_928_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_875WEIGHTED_ROUND_ROBIN_Splitter_892));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_893() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_893WEIGHTED_ROUND_ROBIN_Splitter_902, pop_complex(&SplitJoin12_CombineDFT_Fiss_918_928_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_904() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_919_929_split[0]), &(SplitJoin14_CombineDFT_Fiss_919_929_join[0]));
	ENDFOR
}

void CombineDFT_905() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_919_929_split[1]), &(SplitJoin14_CombineDFT_Fiss_919_929_join[1]));
	ENDFOR
}

void CombineDFT_906() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_919_929_split[2]), &(SplitJoin14_CombineDFT_Fiss_919_929_join[2]));
	ENDFOR
}

void CombineDFT_907() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_919_929_split[3]), &(SplitJoin14_CombineDFT_Fiss_919_929_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_902() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin14_CombineDFT_Fiss_919_929_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_893WEIGHTED_ROUND_ROBIN_Splitter_902));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_903() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_903WEIGHTED_ROUND_ROBIN_Splitter_908, pop_complex(&SplitJoin14_CombineDFT_Fiss_919_929_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_910() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_920_930_split[0]), &(SplitJoin16_CombineDFT_Fiss_920_930_join[0]));
	ENDFOR
}

void CombineDFT_911() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_920_930_split[1]), &(SplitJoin16_CombineDFT_Fiss_920_930_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_908() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_920_930_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_903WEIGHTED_ROUND_ROBIN_Splitter_908));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_920_930_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_903WEIGHTED_ROUND_ROBIN_Splitter_908));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_909() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_909CombineDFT_802, pop_complex(&SplitJoin16_CombineDFT_Fiss_920_930_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_909CombineDFT_802, pop_complex(&SplitJoin16_CombineDFT_Fiss_920_930_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_802() {
	FOR(uint32_t, __iter_steady_, 0, <, 29, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_909CombineDFT_802), &(CombineDFT_802CPrinter_803));
	ENDFOR
}

void CPrinter(buffer_complex_t *chanin) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		printf("%.10f", c.real);
		printf("\n");
		printf("%.10f", c.imag);
		printf("\n");
	}


void CPrinter_803() {
	FOR(uint32_t, __iter_steady_, 0, <, 1856, __iter_steady_++)
		CPrinter(&(CombineDFT_802CPrinter_803));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 4, __iter_init_0_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_913_923_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 4, __iter_init_1_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_919_929_split[__iter_init_1_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_810WEIGHTED_ROUND_ROBIN_Splitter_815);
	init_buffer_complex(&CombineDFT_802CPrinter_803);
	FOR(int, __iter_init_2_, 0, <, 16, __iter_init_2_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_915_925_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 4, __iter_init_3_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_919_929_join[__iter_init_3_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_909CombineDFT_802);
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_918_928_join[__iter_init_4_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_903WEIGHTED_ROUND_ROBIN_Splitter_908);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_826WEIGHTED_ROUND_ROBIN_Splitter_843);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_893WEIGHTED_ROUND_ROBIN_Splitter_902);
	FOR(int, __iter_init_5_, 0, <, 2, __iter_init_5_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_920_930_split[__iter_init_5_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_875WEIGHTED_ROUND_ROBIN_Splitter_892);
	init_buffer_complex(&FFTTestSource_791FFTReorderSimple_792);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_844WEIGHTED_ROUND_ROBIN_Splitter_874);
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_914_924_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_918_928_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 16, __iter_init_8_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_917_927_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 2, __iter_init_9_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_920_930_join[__iter_init_9_]);
	ENDFOR
	init_buffer_complex(&FFTReorderSimple_792WEIGHTED_ROUND_ROBIN_Splitter_805);
	FOR(int, __iter_init_10_, 0, <, 16, __iter_init_10_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_915_925_join[__iter_init_10_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_806WEIGHTED_ROUND_ROBIN_Splitter_809);
	FOR(int, __iter_init_11_, 0, <, 29, __iter_init_11_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_916_926_join[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 8, __iter_init_12_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_914_924_join[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_912_922_split[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_912_922_join[__iter_init_14_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_816WEIGHTED_ROUND_ROBIN_Splitter_825);
	FOR(int, __iter_init_15_, 0, <, 29, __iter_init_15_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_916_926_split[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 16, __iter_init_16_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_917_927_join[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 4, __iter_init_17_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_913_923_join[__iter_init_17_]);
	ENDFOR
// --- init: CombineDFT_845
	 {
	 ; 
	CombineDFT_845_s.wn.real = -1.0 ; 
	CombineDFT_845_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_846
	 {
	CombineDFT_846_s.wn.real = -1.0 ; 
	CombineDFT_846_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_847
	 {
	CombineDFT_847_s.wn.real = -1.0 ; 
	CombineDFT_847_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_848
	 {
	CombineDFT_848_s.wn.real = -1.0 ; 
	CombineDFT_848_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_849
	 {
	CombineDFT_849_s.wn.real = -1.0 ; 
	CombineDFT_849_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_850
	 {
	CombineDFT_850_s.wn.real = -1.0 ; 
	CombineDFT_850_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_851
	 {
	CombineDFT_851_s.wn.real = -1.0 ; 
	CombineDFT_851_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_852
	 {
	CombineDFT_852_s.wn.real = -1.0 ; 
	CombineDFT_852_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_853
	 {
	CombineDFT_853_s.wn.real = -1.0 ; 
	CombineDFT_853_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_854
	 {
	CombineDFT_854_s.wn.real = -1.0 ; 
	CombineDFT_854_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_855
	 {
	CombineDFT_855_s.wn.real = -1.0 ; 
	CombineDFT_855_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_856
	 {
	CombineDFT_856_s.wn.real = -1.0 ; 
	CombineDFT_856_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_857
	 {
	CombineDFT_857_s.wn.real = -1.0 ; 
	CombineDFT_857_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_858
	 {
	CombineDFT_858_s.wn.real = -1.0 ; 
	CombineDFT_858_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_859
	 {
	CombineDFT_859_s.wn.real = -1.0 ; 
	CombineDFT_859_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_860
	 {
	CombineDFT_860_s.wn.real = -1.0 ; 
	CombineDFT_860_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_861
	 {
	CombineDFT_861_s.wn.real = -1.0 ; 
	CombineDFT_861_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_862
	 {
	CombineDFT_862_s.wn.real = -1.0 ; 
	CombineDFT_862_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_863
	 {
	CombineDFT_863_s.wn.real = -1.0 ; 
	CombineDFT_863_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_864
	 {
	CombineDFT_864_s.wn.real = -1.0 ; 
	CombineDFT_864_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_865
	 {
	CombineDFT_865_s.wn.real = -1.0 ; 
	CombineDFT_865_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_866
	 {
	CombineDFT_866_s.wn.real = -1.0 ; 
	CombineDFT_866_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_867
	 {
	CombineDFT_867_s.wn.real = -1.0 ; 
	CombineDFT_867_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_868
	 {
	CombineDFT_868_s.wn.real = -1.0 ; 
	CombineDFT_868_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_869
	 {
	CombineDFT_869_s.wn.real = -1.0 ; 
	CombineDFT_869_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_870
	 {
	CombineDFT_870_s.wn.real = -1.0 ; 
	CombineDFT_870_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_871
	 {
	CombineDFT_871_s.wn.real = -1.0 ; 
	CombineDFT_871_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_872
	 {
	CombineDFT_872_s.wn.real = -1.0 ; 
	CombineDFT_872_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_873
	 {
	CombineDFT_873_s.wn.real = -1.0 ; 
	CombineDFT_873_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_876
	 {
	CombineDFT_876_s.wn.real = -4.371139E-8 ; 
	CombineDFT_876_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_877
	 {
	CombineDFT_877_s.wn.real = -4.371139E-8 ; 
	CombineDFT_877_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_878
	 {
	CombineDFT_878_s.wn.real = -4.371139E-8 ; 
	CombineDFT_878_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_879
	 {
	CombineDFT_879_s.wn.real = -4.371139E-8 ; 
	CombineDFT_879_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_880
	 {
	CombineDFT_880_s.wn.real = -4.371139E-8 ; 
	CombineDFT_880_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_881
	 {
	CombineDFT_881_s.wn.real = -4.371139E-8 ; 
	CombineDFT_881_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_882
	 {
	CombineDFT_882_s.wn.real = -4.371139E-8 ; 
	CombineDFT_882_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_883
	 {
	CombineDFT_883_s.wn.real = -4.371139E-8 ; 
	CombineDFT_883_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_884
	 {
	CombineDFT_884_s.wn.real = -4.371139E-8 ; 
	CombineDFT_884_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_885
	 {
	CombineDFT_885_s.wn.real = -4.371139E-8 ; 
	CombineDFT_885_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_886
	 {
	CombineDFT_886_s.wn.real = -4.371139E-8 ; 
	CombineDFT_886_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_887
	 {
	CombineDFT_887_s.wn.real = -4.371139E-8 ; 
	CombineDFT_887_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_888
	 {
	CombineDFT_888_s.wn.real = -4.371139E-8 ; 
	CombineDFT_888_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_889
	 {
	CombineDFT_889_s.wn.real = -4.371139E-8 ; 
	CombineDFT_889_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_890
	 {
	CombineDFT_890_s.wn.real = -4.371139E-8 ; 
	CombineDFT_890_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_891
	 {
	CombineDFT_891_s.wn.real = -4.371139E-8 ; 
	CombineDFT_891_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_894
	 {
	CombineDFT_894_s.wn.real = 0.70710677 ; 
	CombineDFT_894_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_895
	 {
	CombineDFT_895_s.wn.real = 0.70710677 ; 
	CombineDFT_895_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_896
	 {
	CombineDFT_896_s.wn.real = 0.70710677 ; 
	CombineDFT_896_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_897
	 {
	CombineDFT_897_s.wn.real = 0.70710677 ; 
	CombineDFT_897_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_898
	 {
	CombineDFT_898_s.wn.real = 0.70710677 ; 
	CombineDFT_898_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_899
	 {
	CombineDFT_899_s.wn.real = 0.70710677 ; 
	CombineDFT_899_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_900
	 {
	CombineDFT_900_s.wn.real = 0.70710677 ; 
	CombineDFT_900_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_901
	 {
	CombineDFT_901_s.wn.real = 0.70710677 ; 
	CombineDFT_901_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_904
	 {
	CombineDFT_904_s.wn.real = 0.9238795 ; 
	CombineDFT_904_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_905
	 {
	CombineDFT_905_s.wn.real = 0.9238795 ; 
	CombineDFT_905_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_906
	 {
	CombineDFT_906_s.wn.real = 0.9238795 ; 
	CombineDFT_906_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_907
	 {
	CombineDFT_907_s.wn.real = 0.9238795 ; 
	CombineDFT_907_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_910
	 {
	CombineDFT_910_s.wn.real = 0.98078525 ; 
	CombineDFT_910_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_911
	 {
	CombineDFT_911_s.wn.real = 0.98078525 ; 
	CombineDFT_911_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_802
	 {
	 ; 
	CombineDFT_802_s.wn.real = 0.9951847 ; 
	CombineDFT_802_s.wn.imag = -0.09801714 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FFTTestSource_791();
		FFTReorderSimple_792();
		WEIGHTED_ROUND_ROBIN_Splitter_805();
			FFTReorderSimple_807();
			FFTReorderSimple_808();
		WEIGHTED_ROUND_ROBIN_Joiner_806();
		WEIGHTED_ROUND_ROBIN_Splitter_809();
			FFTReorderSimple_811();
			FFTReorderSimple_812();
			FFTReorderSimple_813();
			FFTReorderSimple_814();
		WEIGHTED_ROUND_ROBIN_Joiner_810();
		WEIGHTED_ROUND_ROBIN_Splitter_815();
			FFTReorderSimple_817();
			FFTReorderSimple_818();
			FFTReorderSimple_819();
			FFTReorderSimple_820();
			FFTReorderSimple_821();
			FFTReorderSimple_822();
			FFTReorderSimple_823();
			FFTReorderSimple_824();
		WEIGHTED_ROUND_ROBIN_Joiner_816();
		WEIGHTED_ROUND_ROBIN_Splitter_825();
			FFTReorderSimple_827();
			FFTReorderSimple_828();
			FFTReorderSimple_829();
			FFTReorderSimple_830();
			FFTReorderSimple_831();
			FFTReorderSimple_832();
			FFTReorderSimple_833();
			FFTReorderSimple_834();
			FFTReorderSimple_835();
			FFTReorderSimple_836();
			FFTReorderSimple_837();
			FFTReorderSimple_838();
			FFTReorderSimple_839();
			FFTReorderSimple_840();
			FFTReorderSimple_841();
			FFTReorderSimple_842();
		WEIGHTED_ROUND_ROBIN_Joiner_826();
		WEIGHTED_ROUND_ROBIN_Splitter_843();
			CombineDFT_845();
			CombineDFT_846();
			CombineDFT_847();
			CombineDFT_848();
			CombineDFT_849();
			CombineDFT_850();
			CombineDFT_851();
			CombineDFT_852();
			CombineDFT_853();
			CombineDFT_854();
			CombineDFT_855();
			CombineDFT_856();
			CombineDFT_857();
			CombineDFT_858();
			CombineDFT_859();
			CombineDFT_860();
			CombineDFT_861();
			CombineDFT_862();
			CombineDFT_863();
			CombineDFT_864();
			CombineDFT_865();
			CombineDFT_866();
			CombineDFT_867();
			CombineDFT_868();
			CombineDFT_869();
			CombineDFT_870();
			CombineDFT_871();
			CombineDFT_872();
			CombineDFT_873();
		WEIGHTED_ROUND_ROBIN_Joiner_844();
		WEIGHTED_ROUND_ROBIN_Splitter_874();
			CombineDFT_876();
			CombineDFT_877();
			CombineDFT_878();
			CombineDFT_879();
			CombineDFT_880();
			CombineDFT_881();
			CombineDFT_882();
			CombineDFT_883();
			CombineDFT_884();
			CombineDFT_885();
			CombineDFT_886();
			CombineDFT_887();
			CombineDFT_888();
			CombineDFT_889();
			CombineDFT_890();
			CombineDFT_891();
		WEIGHTED_ROUND_ROBIN_Joiner_875();
		WEIGHTED_ROUND_ROBIN_Splitter_892();
			CombineDFT_894();
			CombineDFT_895();
			CombineDFT_896();
			CombineDFT_897();
			CombineDFT_898();
			CombineDFT_899();
			CombineDFT_900();
			CombineDFT_901();
		WEIGHTED_ROUND_ROBIN_Joiner_893();
		WEIGHTED_ROUND_ROBIN_Splitter_902();
			CombineDFT_904();
			CombineDFT_905();
			CombineDFT_906();
			CombineDFT_907();
		WEIGHTED_ROUND_ROBIN_Joiner_903();
		WEIGHTED_ROUND_ROBIN_Splitter_908();
			CombineDFT_910();
			CombineDFT_911();
		WEIGHTED_ROUND_ROBIN_Joiner_909();
		CombineDFT_802();
		CPrinter_803();
	ENDFOR
	return EXIT_SUCCESS;
}
