#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=896 on the compile command line
#else
#if BUF_SIZEMAX < 896
#error BUF_SIZEMAX too small, it must be at least 896
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float A_re[32];
	float A_im[32];
	int idx;
} FloatSource_2948_t;
void FloatSource_2948();
void Pre_CollapsedDataParallel_1_3249();
void WEIGHTED_ROUND_ROBIN_Splitter_3391();
void Butterfly_3393();
void Butterfly_3394();
void Butterfly_3395();
void Butterfly_3396();
void Butterfly_3397();
void Butterfly_3398();
void Butterfly_3399();
void Butterfly_3400();
void Butterfly_3401();
void Butterfly_3402();
void Butterfly_3403();
void Butterfly_3404();
void Butterfly_3405();
void Butterfly_3406();
void WEIGHTED_ROUND_ROBIN_Joiner_3392();
void Post_CollapsedDataParallel_2_3250();
void WEIGHTED_ROUND_ROBIN_Splitter_3293();
void Pre_CollapsedDataParallel_1_3252();
void WEIGHTED_ROUND_ROBIN_Splitter_3407();
void Butterfly_3409();
void Butterfly_3410();
void Butterfly_3411();
void Butterfly_3412();
void Butterfly_3413();
void Butterfly_3414();
void Butterfly_3415();
void Butterfly_3416();
void WEIGHTED_ROUND_ROBIN_Joiner_3408();
void Post_CollapsedDataParallel_2_3253();
void WEIGHTED_ROUND_ROBIN_Splitter_3373();
void Pre_CollapsedDataParallel_1_3258();
void WEIGHTED_ROUND_ROBIN_Splitter_3417();
void Butterfly_3419();
void Butterfly_3420();
void Butterfly_3421();
void Butterfly_3422();
void WEIGHTED_ROUND_ROBIN_Joiner_3418();
void Post_CollapsedDataParallel_2_3259();
void Pre_CollapsedDataParallel_1_3261();
void WEIGHTED_ROUND_ROBIN_Splitter_3423();
void Butterfly_3425();
void Butterfly_3426();
void Butterfly_3427();
void Butterfly_3428();
void WEIGHTED_ROUND_ROBIN_Joiner_3424();
void Post_CollapsedDataParallel_2_3262();
void WEIGHTED_ROUND_ROBIN_Joiner_3374();
void Pre_CollapsedDataParallel_1_3255();
void WEIGHTED_ROUND_ROBIN_Splitter_3429();
void Butterfly_3431();
void Butterfly_3432();
void Butterfly_3433();
void Butterfly_3434();
void Butterfly_3435();
void Butterfly_3436();
void Butterfly_3437();
void Butterfly_3438();
void WEIGHTED_ROUND_ROBIN_Joiner_3430();
void Post_CollapsedDataParallel_2_3256();
void WEIGHTED_ROUND_ROBIN_Splitter_3375();
void Pre_CollapsedDataParallel_1_3264();
void WEIGHTED_ROUND_ROBIN_Splitter_3439();
void Butterfly_3441();
void Butterfly_3442();
void Butterfly_3443();
void Butterfly_3444();
void WEIGHTED_ROUND_ROBIN_Joiner_3440();
void Post_CollapsedDataParallel_2_3265();
void Pre_CollapsedDataParallel_1_3267();
void WEIGHTED_ROUND_ROBIN_Splitter_3445();
void Butterfly_3447();
void Butterfly_3448();
void Butterfly_3449();
void Butterfly_3450();
void WEIGHTED_ROUND_ROBIN_Joiner_3446();
void Post_CollapsedDataParallel_2_3268();
void WEIGHTED_ROUND_ROBIN_Joiner_3376();
void WEIGHTED_ROUND_ROBIN_Joiner_3377();
void WEIGHTED_ROUND_ROBIN_Splitter_3378();
void WEIGHTED_ROUND_ROBIN_Splitter_3379();
void Pre_CollapsedDataParallel_1_3270();
void WEIGHTED_ROUND_ROBIN_Splitter_3451();
void Butterfly_3453();
void Butterfly_3454();
void WEIGHTED_ROUND_ROBIN_Joiner_3452();
void Post_CollapsedDataParallel_2_3271();
void Pre_CollapsedDataParallel_1_3273();
void WEIGHTED_ROUND_ROBIN_Splitter_3455();
void Butterfly_3457();
void Butterfly_3458();
void WEIGHTED_ROUND_ROBIN_Joiner_3456();
void Post_CollapsedDataParallel_2_3274();
void Pre_CollapsedDataParallel_1_3276();
void WEIGHTED_ROUND_ROBIN_Splitter_3459();
void Butterfly_3461();
void Butterfly_3462();
void WEIGHTED_ROUND_ROBIN_Joiner_3460();
void Post_CollapsedDataParallel_2_3277();
void Pre_CollapsedDataParallel_1_3279();
void WEIGHTED_ROUND_ROBIN_Splitter_3463();
void Butterfly_3465();
void Butterfly_3466();
void WEIGHTED_ROUND_ROBIN_Joiner_3464();
void Post_CollapsedDataParallel_2_3280();
void WEIGHTED_ROUND_ROBIN_Joiner_3380();
void WEIGHTED_ROUND_ROBIN_Splitter_3381();
void Pre_CollapsedDataParallel_1_3282();
void WEIGHTED_ROUND_ROBIN_Splitter_3467();
void Butterfly_3469();
void Butterfly_3470();
void WEIGHTED_ROUND_ROBIN_Joiner_3468();
void Post_CollapsedDataParallel_2_3283();
void Pre_CollapsedDataParallel_1_3285();
void WEIGHTED_ROUND_ROBIN_Splitter_3471();
void Butterfly_3473();
void Butterfly_3474();
void WEIGHTED_ROUND_ROBIN_Joiner_3472();
void Post_CollapsedDataParallel_2_3286();
void Pre_CollapsedDataParallel_1_3288();
void WEIGHTED_ROUND_ROBIN_Splitter_3475();
void Butterfly_3477();
void Butterfly_3478();
void WEIGHTED_ROUND_ROBIN_Joiner_3476();
void Post_CollapsedDataParallel_2_3289();
void Pre_CollapsedDataParallel_1_3291();
void WEIGHTED_ROUND_ROBIN_Splitter_3479();
void Butterfly_3481();
void Butterfly_3482();
void WEIGHTED_ROUND_ROBIN_Joiner_3480();
void Post_CollapsedDataParallel_2_3292();
void WEIGHTED_ROUND_ROBIN_Joiner_3382();
void WEIGHTED_ROUND_ROBIN_Joiner_3383();
void WEIGHTED_ROUND_ROBIN_Splitter_3384();
void WEIGHTED_ROUND_ROBIN_Splitter_3385();
void Butterfly_3013();
void Butterfly_3014();
void Butterfly_3015();
void Butterfly_3016();
void Butterfly_3017();
void Butterfly_3018();
void Butterfly_3019();
void Butterfly_3020();
void WEIGHTED_ROUND_ROBIN_Joiner_3386();
void WEIGHTED_ROUND_ROBIN_Splitter_3387();
void Butterfly_3021();
void Butterfly_3022();
void Butterfly_3023();
void Butterfly_3024();
void Butterfly_3025();
void Butterfly_3026();
void Butterfly_3027();
void Butterfly_3028();
void WEIGHTED_ROUND_ROBIN_Joiner_3388();
void WEIGHTED_ROUND_ROBIN_Joiner_3389();
int BitReverse_3029_bitrev(int inp, int numbits);
void BitReverse_3029();
void FloatPrinter_3030();

#ifdef __cplusplus
}
#endif
#endif
