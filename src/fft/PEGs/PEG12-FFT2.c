#include "PEG12-FFT2.h"

buffer_float_t FFTReorderSimple_9355WEIGHTED_ROUND_ROBIN_Splitter_9461;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9534WEIGHTED_ROUND_ROBIN_Splitter_9539;
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_9548_9569_split[12];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9428WEIGHTED_ROUND_ROBIN_Splitter_9441;
buffer_float_t SplitJoin94_CombineDFT_Fiss_9562_9583_split[2];
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_9545_9566_join[2];
buffer_float_t SplitJoin14_CombineDFT_Fiss_9550_9571_split[12];
buffer_float_t SplitJoin86_CombineDFT_Fiss_9558_9579_split[12];
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_9548_9569_join[12];
buffer_float_t SplitJoin0_FFTTestSource_Fiss_9543_9564_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9384WEIGHTED_ROUND_ROBIN_Splitter_9389;
buffer_float_t SplitJoin82_FFTReorderSimple_Fiss_9556_9577_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9452WEIGHTED_ROUND_ROBIN_Splitter_9457;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9466WEIGHTED_ROUND_ROBIN_Splitter_9471;
buffer_float_t SplitJoin88_CombineDFT_Fiss_9559_9580_join[12];
buffer_float_t SplitJoin12_CombineDFT_Fiss_9549_9570_join[12];
buffer_float_t SplitJoin92_CombineDFT_Fiss_9561_9582_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9390WEIGHTED_ROUND_ROBIN_Splitter_9399;
buffer_float_t SplitJoin12_CombineDFT_Fiss_9549_9570_split[12];
buffer_float_t SplitJoin14_CombineDFT_Fiss_9550_9571_join[12];
buffer_float_t SplitJoin88_CombineDFT_Fiss_9559_9580_split[12];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_9333_9369_9544_9565_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9482WEIGHTED_ROUND_ROBIN_Splitter_9495;
buffer_float_t SplitJoin90_CombineDFT_Fiss_9560_9581_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9376WEIGHTED_ROUND_ROBIN_Splitter_9367;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9442WEIGHTED_ROUND_ROBIN_Splitter_9451;
buffer_float_t SplitJoin94_CombineDFT_Fiss_9562_9583_join[2];
buffer_float_t SplitJoin18_CombineDFT_Fiss_9552_9573_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9510WEIGHTED_ROUND_ROBIN_Splitter_9523;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9414WEIGHTED_ROUND_ROBIN_Splitter_9427;
buffer_float_t SplitJoin80_FFTReorderSimple_Fiss_9555_9576_join[4];
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_9545_9566_split[2];
buffer_float_t SplitJoin84_FFTReorderSimple_Fiss_9557_9578_split[12];
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_9547_9568_split[8];
buffer_float_t SplitJoin16_CombineDFT_Fiss_9551_9572_join[8];
buffer_float_t SplitJoin86_CombineDFT_Fiss_9558_9579_join[12];
buffer_float_t SplitJoin82_FFTReorderSimple_Fiss_9556_9577_split[8];
buffer_float_t SplitJoin92_CombineDFT_Fiss_9561_9582_join[4];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_9333_9369_9544_9565_split[2];
buffer_float_t SplitJoin16_CombineDFT_Fiss_9551_9572_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9462WEIGHTED_ROUND_ROBIN_Splitter_9465;
buffer_float_t SplitJoin80_FFTReorderSimple_Fiss_9555_9576_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9458CombineDFT_9354;
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_9547_9568_join[8];
buffer_float_t SplitJoin20_CombineDFT_Fiss_9553_9574_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9524WEIGHTED_ROUND_ROBIN_Splitter_9533;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9540CombineDFT_9365;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9400WEIGHTED_ROUND_ROBIN_Splitter_9413;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9496WEIGHTED_ROUND_ROBIN_Splitter_9509;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9380WEIGHTED_ROUND_ROBIN_Splitter_9383;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9472WEIGHTED_ROUND_ROBIN_Splitter_9481;
buffer_float_t SplitJoin78_FFTReorderSimple_Fiss_9554_9575_split[2];
buffer_float_t SplitJoin90_CombineDFT_Fiss_9560_9581_join[8];
buffer_float_t SplitJoin0_FFTTestSource_Fiss_9543_9564_split[2];
buffer_float_t SplitJoin20_CombineDFT_Fiss_9553_9574_split[2];
buffer_float_t FFTReorderSimple_9344WEIGHTED_ROUND_ROBIN_Splitter_9379;
buffer_float_t SplitJoin78_FFTReorderSimple_Fiss_9554_9575_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9368FloatPrinter_9366;
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_9546_9567_split[4];
buffer_float_t SplitJoin84_FFTReorderSimple_Fiss_9557_9578_join[12];
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_9546_9567_join[4];
buffer_float_t SplitJoin18_CombineDFT_Fiss_9552_9573_join[4];


CombineDFT_9415_t CombineDFT_9415_s;
CombineDFT_9415_t CombineDFT_9416_s;
CombineDFT_9415_t CombineDFT_9417_s;
CombineDFT_9415_t CombineDFT_9418_s;
CombineDFT_9415_t CombineDFT_9419_s;
CombineDFT_9415_t CombineDFT_9420_s;
CombineDFT_9415_t CombineDFT_9421_s;
CombineDFT_9415_t CombineDFT_9422_s;
CombineDFT_9415_t CombineDFT_9423_s;
CombineDFT_9415_t CombineDFT_9424_s;
CombineDFT_9415_t CombineDFT_9425_s;
CombineDFT_9415_t CombineDFT_9426_s;
CombineDFT_9429_t CombineDFT_9429_s;
CombineDFT_9429_t CombineDFT_9430_s;
CombineDFT_9429_t CombineDFT_9431_s;
CombineDFT_9429_t CombineDFT_9432_s;
CombineDFT_9429_t CombineDFT_9433_s;
CombineDFT_9429_t CombineDFT_9434_s;
CombineDFT_9429_t CombineDFT_9435_s;
CombineDFT_9429_t CombineDFT_9436_s;
CombineDFT_9429_t CombineDFT_9437_s;
CombineDFT_9429_t CombineDFT_9438_s;
CombineDFT_9429_t CombineDFT_9439_s;
CombineDFT_9429_t CombineDFT_9440_s;
CombineDFT_9443_t CombineDFT_9443_s;
CombineDFT_9443_t CombineDFT_9444_s;
CombineDFT_9443_t CombineDFT_9445_s;
CombineDFT_9443_t CombineDFT_9446_s;
CombineDFT_9443_t CombineDFT_9447_s;
CombineDFT_9443_t CombineDFT_9448_s;
CombineDFT_9443_t CombineDFT_9449_s;
CombineDFT_9443_t CombineDFT_9450_s;
CombineDFT_9453_t CombineDFT_9453_s;
CombineDFT_9453_t CombineDFT_9454_s;
CombineDFT_9453_t CombineDFT_9455_s;
CombineDFT_9453_t CombineDFT_9456_s;
CombineDFT_9459_t CombineDFT_9459_s;
CombineDFT_9459_t CombineDFT_9460_s;
CombineDFT_9354_t CombineDFT_9354_s;
CombineDFT_9415_t CombineDFT_9497_s;
CombineDFT_9415_t CombineDFT_9498_s;
CombineDFT_9415_t CombineDFT_9499_s;
CombineDFT_9415_t CombineDFT_9500_s;
CombineDFT_9415_t CombineDFT_9501_s;
CombineDFT_9415_t CombineDFT_9502_s;
CombineDFT_9415_t CombineDFT_9503_s;
CombineDFT_9415_t CombineDFT_9504_s;
CombineDFT_9415_t CombineDFT_9505_s;
CombineDFT_9415_t CombineDFT_9506_s;
CombineDFT_9415_t CombineDFT_9507_s;
CombineDFT_9415_t CombineDFT_9508_s;
CombineDFT_9429_t CombineDFT_9511_s;
CombineDFT_9429_t CombineDFT_9512_s;
CombineDFT_9429_t CombineDFT_9513_s;
CombineDFT_9429_t CombineDFT_9514_s;
CombineDFT_9429_t CombineDFT_9515_s;
CombineDFT_9429_t CombineDFT_9516_s;
CombineDFT_9429_t CombineDFT_9517_s;
CombineDFT_9429_t CombineDFT_9518_s;
CombineDFT_9429_t CombineDFT_9519_s;
CombineDFT_9429_t CombineDFT_9520_s;
CombineDFT_9429_t CombineDFT_9521_s;
CombineDFT_9429_t CombineDFT_9522_s;
CombineDFT_9443_t CombineDFT_9525_s;
CombineDFT_9443_t CombineDFT_9526_s;
CombineDFT_9443_t CombineDFT_9527_s;
CombineDFT_9443_t CombineDFT_9528_s;
CombineDFT_9443_t CombineDFT_9529_s;
CombineDFT_9443_t CombineDFT_9530_s;
CombineDFT_9443_t CombineDFT_9531_s;
CombineDFT_9443_t CombineDFT_9532_s;
CombineDFT_9453_t CombineDFT_9535_s;
CombineDFT_9453_t CombineDFT_9536_s;
CombineDFT_9453_t CombineDFT_9537_s;
CombineDFT_9453_t CombineDFT_9538_s;
CombineDFT_9459_t CombineDFT_9541_s;
CombineDFT_9459_t CombineDFT_9542_s;
CombineDFT_9354_t CombineDFT_9365_s;

void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), 0.0) ; 
		push_float(&(*chanout), 0.0) ; 
		push_float(&(*chanout), 1.0) ; 
		push_float(&(*chanout), 0.0) ; 
		FOR(int, i, 0,  < , 124, i++) {
			push_float(&(*chanout), 0.0) ; 
		}
		ENDFOR
	}


void FFTTestSource_9377() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_9543_9564_split[0]), &(SplitJoin0_FFTTestSource_Fiss_9543_9564_join[0]));
	ENDFOR
}

void FFTTestSource_9378() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_9543_9564_split[1]), &(SplitJoin0_FFTTestSource_Fiss_9543_9564_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9375() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_9376() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9376WEIGHTED_ROUND_ROBIN_Splitter_9367, pop_float(&SplitJoin0_FFTTestSource_Fiss_9543_9564_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9376WEIGHTED_ROUND_ROBIN_Splitter_9367, pop_float(&SplitJoin0_FFTTestSource_Fiss_9543_9564_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, i, 0,  < , 128, i = (i + 4)) {
			push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 128, i = (i + 4)) {
			push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_float(&(*chanin)) ; 
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void FFTReorderSimple_9344() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_9333_9369_9544_9565_split[0]), &(FFTReorderSimple_9344WEIGHTED_ROUND_ROBIN_Splitter_9379));
	ENDFOR
}

void FFTReorderSimple_9381() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_9545_9566_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_9545_9566_join[0]));
	ENDFOR
}

void FFTReorderSimple_9382() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_9545_9566_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_9545_9566_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9379() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_9545_9566_split[0], pop_float(&FFTReorderSimple_9344WEIGHTED_ROUND_ROBIN_Splitter_9379));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_9545_9566_split[1], pop_float(&FFTReorderSimple_9344WEIGHTED_ROUND_ROBIN_Splitter_9379));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9380() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9380WEIGHTED_ROUND_ROBIN_Splitter_9383, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_9545_9566_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9380WEIGHTED_ROUND_ROBIN_Splitter_9383, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_9545_9566_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_9385() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_9546_9567_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_9546_9567_join[0]));
	ENDFOR
}

void FFTReorderSimple_9386() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_9546_9567_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_9546_9567_join[1]));
	ENDFOR
}

void FFTReorderSimple_9387() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_9546_9567_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_9546_9567_join[2]));
	ENDFOR
}

void FFTReorderSimple_9388() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_9546_9567_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_9546_9567_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9383() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin6_FFTReorderSimple_Fiss_9546_9567_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9380WEIGHTED_ROUND_ROBIN_Splitter_9383));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9384() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9384WEIGHTED_ROUND_ROBIN_Splitter_9389, pop_float(&SplitJoin6_FFTReorderSimple_Fiss_9546_9567_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_9391() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_9547_9568_split[0]), &(SplitJoin8_FFTReorderSimple_Fiss_9547_9568_join[0]));
	ENDFOR
}

void FFTReorderSimple_9392() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_9547_9568_split[1]), &(SplitJoin8_FFTReorderSimple_Fiss_9547_9568_join[1]));
	ENDFOR
}

void FFTReorderSimple_9393() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_9547_9568_split[2]), &(SplitJoin8_FFTReorderSimple_Fiss_9547_9568_join[2]));
	ENDFOR
}

void FFTReorderSimple_9394() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_9547_9568_split[3]), &(SplitJoin8_FFTReorderSimple_Fiss_9547_9568_join[3]));
	ENDFOR
}

void FFTReorderSimple_9395() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_9547_9568_split[4]), &(SplitJoin8_FFTReorderSimple_Fiss_9547_9568_join[4]));
	ENDFOR
}

void FFTReorderSimple_9396() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_9547_9568_split[5]), &(SplitJoin8_FFTReorderSimple_Fiss_9547_9568_join[5]));
	ENDFOR
}

void FFTReorderSimple_9397() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_9547_9568_split[6]), &(SplitJoin8_FFTReorderSimple_Fiss_9547_9568_join[6]));
	ENDFOR
}

void FFTReorderSimple_9398() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_9547_9568_split[7]), &(SplitJoin8_FFTReorderSimple_Fiss_9547_9568_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9389() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin8_FFTReorderSimple_Fiss_9547_9568_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9384WEIGHTED_ROUND_ROBIN_Splitter_9389));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9390() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9390WEIGHTED_ROUND_ROBIN_Splitter_9399, pop_float(&SplitJoin8_FFTReorderSimple_Fiss_9547_9568_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_9401() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_9548_9569_split[0]), &(SplitJoin10_FFTReorderSimple_Fiss_9548_9569_join[0]));
	ENDFOR
}

void FFTReorderSimple_9402() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_9548_9569_split[1]), &(SplitJoin10_FFTReorderSimple_Fiss_9548_9569_join[1]));
	ENDFOR
}

void FFTReorderSimple_9403() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_9548_9569_split[2]), &(SplitJoin10_FFTReorderSimple_Fiss_9548_9569_join[2]));
	ENDFOR
}

void FFTReorderSimple_9404() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_9548_9569_split[3]), &(SplitJoin10_FFTReorderSimple_Fiss_9548_9569_join[3]));
	ENDFOR
}

void FFTReorderSimple_9405() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_9548_9569_split[4]), &(SplitJoin10_FFTReorderSimple_Fiss_9548_9569_join[4]));
	ENDFOR
}

void FFTReorderSimple_9406() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_9548_9569_split[5]), &(SplitJoin10_FFTReorderSimple_Fiss_9548_9569_join[5]));
	ENDFOR
}

void FFTReorderSimple_9407() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_9548_9569_split[6]), &(SplitJoin10_FFTReorderSimple_Fiss_9548_9569_join[6]));
	ENDFOR
}

void FFTReorderSimple_9408() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_9548_9569_split[7]), &(SplitJoin10_FFTReorderSimple_Fiss_9548_9569_join[7]));
	ENDFOR
}

void FFTReorderSimple_9409() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_9548_9569_split[8]), &(SplitJoin10_FFTReorderSimple_Fiss_9548_9569_join[8]));
	ENDFOR
}

void FFTReorderSimple_9410() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_9548_9569_split[9]), &(SplitJoin10_FFTReorderSimple_Fiss_9548_9569_join[9]));
	ENDFOR
}

void FFTReorderSimple_9411() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_9548_9569_split[10]), &(SplitJoin10_FFTReorderSimple_Fiss_9548_9569_join[10]));
	ENDFOR
}

void FFTReorderSimple_9412() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_9548_9569_split[11]), &(SplitJoin10_FFTReorderSimple_Fiss_9548_9569_join[11]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9399() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 12, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_FFTReorderSimple_Fiss_9548_9569_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9390WEIGHTED_ROUND_ROBIN_Splitter_9399));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9400() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 12, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9400WEIGHTED_ROUND_ROBIN_Splitter_9413, pop_float(&SplitJoin10_FFTReorderSimple_Fiss_9548_9569_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&(*chanin), i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&(*chanin), i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&(*chanin), (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&(*chanin), (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_9415_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_9415_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&(*chanin)) ; 
			push_float(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_9415() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_9549_9570_split[0]), &(SplitJoin12_CombineDFT_Fiss_9549_9570_join[0]));
	ENDFOR
}

void CombineDFT_9416() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_9549_9570_split[1]), &(SplitJoin12_CombineDFT_Fiss_9549_9570_join[1]));
	ENDFOR
}

void CombineDFT_9417() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_9549_9570_split[2]), &(SplitJoin12_CombineDFT_Fiss_9549_9570_join[2]));
	ENDFOR
}

void CombineDFT_9418() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_9549_9570_split[3]), &(SplitJoin12_CombineDFT_Fiss_9549_9570_join[3]));
	ENDFOR
}

void CombineDFT_9419() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_9549_9570_split[4]), &(SplitJoin12_CombineDFT_Fiss_9549_9570_join[4]));
	ENDFOR
}

void CombineDFT_9420() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_9549_9570_split[5]), &(SplitJoin12_CombineDFT_Fiss_9549_9570_join[5]));
	ENDFOR
}

void CombineDFT_9421() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_9549_9570_split[6]), &(SplitJoin12_CombineDFT_Fiss_9549_9570_join[6]));
	ENDFOR
}

void CombineDFT_9422() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_9549_9570_split[7]), &(SplitJoin12_CombineDFT_Fiss_9549_9570_join[7]));
	ENDFOR
}

void CombineDFT_9423() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_9549_9570_split[8]), &(SplitJoin12_CombineDFT_Fiss_9549_9570_join[8]));
	ENDFOR
}

void CombineDFT_9424() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_9549_9570_split[9]), &(SplitJoin12_CombineDFT_Fiss_9549_9570_join[9]));
	ENDFOR
}

void CombineDFT_9425() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_9549_9570_split[10]), &(SplitJoin12_CombineDFT_Fiss_9549_9570_join[10]));
	ENDFOR
}

void CombineDFT_9426() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_9549_9570_split[11]), &(SplitJoin12_CombineDFT_Fiss_9549_9570_join[11]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9413() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 12, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin12_CombineDFT_Fiss_9549_9570_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9400WEIGHTED_ROUND_ROBIN_Splitter_9413));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9414() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 12, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9414WEIGHTED_ROUND_ROBIN_Splitter_9427, pop_float(&SplitJoin12_CombineDFT_Fiss_9549_9570_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_9429() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_9550_9571_split[0]), &(SplitJoin14_CombineDFT_Fiss_9550_9571_join[0]));
	ENDFOR
}

void CombineDFT_9430() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_9550_9571_split[1]), &(SplitJoin14_CombineDFT_Fiss_9550_9571_join[1]));
	ENDFOR
}

void CombineDFT_9431() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_9550_9571_split[2]), &(SplitJoin14_CombineDFT_Fiss_9550_9571_join[2]));
	ENDFOR
}

void CombineDFT_9432() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_9550_9571_split[3]), &(SplitJoin14_CombineDFT_Fiss_9550_9571_join[3]));
	ENDFOR
}

void CombineDFT_9433() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_9550_9571_split[4]), &(SplitJoin14_CombineDFT_Fiss_9550_9571_join[4]));
	ENDFOR
}

void CombineDFT_9434() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_9550_9571_split[5]), &(SplitJoin14_CombineDFT_Fiss_9550_9571_join[5]));
	ENDFOR
}

void CombineDFT_9435() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_9550_9571_split[6]), &(SplitJoin14_CombineDFT_Fiss_9550_9571_join[6]));
	ENDFOR
}

void CombineDFT_9436() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_9550_9571_split[7]), &(SplitJoin14_CombineDFT_Fiss_9550_9571_join[7]));
	ENDFOR
}

void CombineDFT_9437() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_9550_9571_split[8]), &(SplitJoin14_CombineDFT_Fiss_9550_9571_join[8]));
	ENDFOR
}

void CombineDFT_9438() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_9550_9571_split[9]), &(SplitJoin14_CombineDFT_Fiss_9550_9571_join[9]));
	ENDFOR
}

void CombineDFT_9439() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_9550_9571_split[10]), &(SplitJoin14_CombineDFT_Fiss_9550_9571_join[10]));
	ENDFOR
}

void CombineDFT_9440() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_9550_9571_split[11]), &(SplitJoin14_CombineDFT_Fiss_9550_9571_join[11]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9427() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 12, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin14_CombineDFT_Fiss_9550_9571_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9414WEIGHTED_ROUND_ROBIN_Splitter_9427));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9428() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 12, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9428WEIGHTED_ROUND_ROBIN_Splitter_9441, pop_float(&SplitJoin14_CombineDFT_Fiss_9550_9571_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_9443() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_9551_9572_split[0]), &(SplitJoin16_CombineDFT_Fiss_9551_9572_join[0]));
	ENDFOR
}

void CombineDFT_9444() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_9551_9572_split[1]), &(SplitJoin16_CombineDFT_Fiss_9551_9572_join[1]));
	ENDFOR
}

void CombineDFT_9445() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_9551_9572_split[2]), &(SplitJoin16_CombineDFT_Fiss_9551_9572_join[2]));
	ENDFOR
}

void CombineDFT_9446() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_9551_9572_split[3]), &(SplitJoin16_CombineDFT_Fiss_9551_9572_join[3]));
	ENDFOR
}

void CombineDFT_9447() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_9551_9572_split[4]), &(SplitJoin16_CombineDFT_Fiss_9551_9572_join[4]));
	ENDFOR
}

void CombineDFT_9448() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_9551_9572_split[5]), &(SplitJoin16_CombineDFT_Fiss_9551_9572_join[5]));
	ENDFOR
}

void CombineDFT_9449() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_9551_9572_split[6]), &(SplitJoin16_CombineDFT_Fiss_9551_9572_join[6]));
	ENDFOR
}

void CombineDFT_9450() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_9551_9572_split[7]), &(SplitJoin16_CombineDFT_Fiss_9551_9572_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9441() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin16_CombineDFT_Fiss_9551_9572_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9428WEIGHTED_ROUND_ROBIN_Splitter_9441));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9442() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9442WEIGHTED_ROUND_ROBIN_Splitter_9451, pop_float(&SplitJoin16_CombineDFT_Fiss_9551_9572_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_9453() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_9552_9573_split[0]), &(SplitJoin18_CombineDFT_Fiss_9552_9573_join[0]));
	ENDFOR
}

void CombineDFT_9454() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_9552_9573_split[1]), &(SplitJoin18_CombineDFT_Fiss_9552_9573_join[1]));
	ENDFOR
}

void CombineDFT_9455() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_9552_9573_split[2]), &(SplitJoin18_CombineDFT_Fiss_9552_9573_join[2]));
	ENDFOR
}

void CombineDFT_9456() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_9552_9573_split[3]), &(SplitJoin18_CombineDFT_Fiss_9552_9573_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9451() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin18_CombineDFT_Fiss_9552_9573_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9442WEIGHTED_ROUND_ROBIN_Splitter_9451));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9452() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9452WEIGHTED_ROUND_ROBIN_Splitter_9457, pop_float(&SplitJoin18_CombineDFT_Fiss_9552_9573_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_9459() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin20_CombineDFT_Fiss_9553_9574_split[0]), &(SplitJoin20_CombineDFT_Fiss_9553_9574_join[0]));
	ENDFOR
}

void CombineDFT_9460() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin20_CombineDFT_Fiss_9553_9574_split[1]), &(SplitJoin20_CombineDFT_Fiss_9553_9574_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9457() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_9553_9574_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9452WEIGHTED_ROUND_ROBIN_Splitter_9457));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_9553_9574_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9452WEIGHTED_ROUND_ROBIN_Splitter_9457));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9458() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9458CombineDFT_9354, pop_float(&SplitJoin20_CombineDFT_Fiss_9553_9574_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9458CombineDFT_9354, pop_float(&SplitJoin20_CombineDFT_Fiss_9553_9574_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_9354() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_9458CombineDFT_9354), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_9333_9369_9544_9565_join[0]));
	ENDFOR
}

void FFTReorderSimple_9355() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_9333_9369_9544_9565_split[1]), &(FFTReorderSimple_9355WEIGHTED_ROUND_ROBIN_Splitter_9461));
	ENDFOR
}

void FFTReorderSimple_9463() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin78_FFTReorderSimple_Fiss_9554_9575_split[0]), &(SplitJoin78_FFTReorderSimple_Fiss_9554_9575_join[0]));
	ENDFOR
}

void FFTReorderSimple_9464() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin78_FFTReorderSimple_Fiss_9554_9575_split[1]), &(SplitJoin78_FFTReorderSimple_Fiss_9554_9575_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9461() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin78_FFTReorderSimple_Fiss_9554_9575_split[0], pop_float(&FFTReorderSimple_9355WEIGHTED_ROUND_ROBIN_Splitter_9461));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin78_FFTReorderSimple_Fiss_9554_9575_split[1], pop_float(&FFTReorderSimple_9355WEIGHTED_ROUND_ROBIN_Splitter_9461));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9462() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9462WEIGHTED_ROUND_ROBIN_Splitter_9465, pop_float(&SplitJoin78_FFTReorderSimple_Fiss_9554_9575_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9462WEIGHTED_ROUND_ROBIN_Splitter_9465, pop_float(&SplitJoin78_FFTReorderSimple_Fiss_9554_9575_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_9467() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin80_FFTReorderSimple_Fiss_9555_9576_split[0]), &(SplitJoin80_FFTReorderSimple_Fiss_9555_9576_join[0]));
	ENDFOR
}

void FFTReorderSimple_9468() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin80_FFTReorderSimple_Fiss_9555_9576_split[1]), &(SplitJoin80_FFTReorderSimple_Fiss_9555_9576_join[1]));
	ENDFOR
}

void FFTReorderSimple_9469() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin80_FFTReorderSimple_Fiss_9555_9576_split[2]), &(SplitJoin80_FFTReorderSimple_Fiss_9555_9576_join[2]));
	ENDFOR
}

void FFTReorderSimple_9470() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin80_FFTReorderSimple_Fiss_9555_9576_split[3]), &(SplitJoin80_FFTReorderSimple_Fiss_9555_9576_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9465() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin80_FFTReorderSimple_Fiss_9555_9576_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9462WEIGHTED_ROUND_ROBIN_Splitter_9465));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9466() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9466WEIGHTED_ROUND_ROBIN_Splitter_9471, pop_float(&SplitJoin80_FFTReorderSimple_Fiss_9555_9576_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_9473() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin82_FFTReorderSimple_Fiss_9556_9577_split[0]), &(SplitJoin82_FFTReorderSimple_Fiss_9556_9577_join[0]));
	ENDFOR
}

void FFTReorderSimple_9474() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin82_FFTReorderSimple_Fiss_9556_9577_split[1]), &(SplitJoin82_FFTReorderSimple_Fiss_9556_9577_join[1]));
	ENDFOR
}

void FFTReorderSimple_9475() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin82_FFTReorderSimple_Fiss_9556_9577_split[2]), &(SplitJoin82_FFTReorderSimple_Fiss_9556_9577_join[2]));
	ENDFOR
}

void FFTReorderSimple_9476() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin82_FFTReorderSimple_Fiss_9556_9577_split[3]), &(SplitJoin82_FFTReorderSimple_Fiss_9556_9577_join[3]));
	ENDFOR
}

void FFTReorderSimple_9477() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin82_FFTReorderSimple_Fiss_9556_9577_split[4]), &(SplitJoin82_FFTReorderSimple_Fiss_9556_9577_join[4]));
	ENDFOR
}

void FFTReorderSimple_9478() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin82_FFTReorderSimple_Fiss_9556_9577_split[5]), &(SplitJoin82_FFTReorderSimple_Fiss_9556_9577_join[5]));
	ENDFOR
}

void FFTReorderSimple_9479() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin82_FFTReorderSimple_Fiss_9556_9577_split[6]), &(SplitJoin82_FFTReorderSimple_Fiss_9556_9577_join[6]));
	ENDFOR
}

void FFTReorderSimple_9480() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin82_FFTReorderSimple_Fiss_9556_9577_split[7]), &(SplitJoin82_FFTReorderSimple_Fiss_9556_9577_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9471() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin82_FFTReorderSimple_Fiss_9556_9577_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9466WEIGHTED_ROUND_ROBIN_Splitter_9471));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9472() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9472WEIGHTED_ROUND_ROBIN_Splitter_9481, pop_float(&SplitJoin82_FFTReorderSimple_Fiss_9556_9577_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_9483() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin84_FFTReorderSimple_Fiss_9557_9578_split[0]), &(SplitJoin84_FFTReorderSimple_Fiss_9557_9578_join[0]));
	ENDFOR
}

void FFTReorderSimple_9484() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin84_FFTReorderSimple_Fiss_9557_9578_split[1]), &(SplitJoin84_FFTReorderSimple_Fiss_9557_9578_join[1]));
	ENDFOR
}

void FFTReorderSimple_9485() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin84_FFTReorderSimple_Fiss_9557_9578_split[2]), &(SplitJoin84_FFTReorderSimple_Fiss_9557_9578_join[2]));
	ENDFOR
}

void FFTReorderSimple_9486() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin84_FFTReorderSimple_Fiss_9557_9578_split[3]), &(SplitJoin84_FFTReorderSimple_Fiss_9557_9578_join[3]));
	ENDFOR
}

void FFTReorderSimple_9487() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin84_FFTReorderSimple_Fiss_9557_9578_split[4]), &(SplitJoin84_FFTReorderSimple_Fiss_9557_9578_join[4]));
	ENDFOR
}

void FFTReorderSimple_9488() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin84_FFTReorderSimple_Fiss_9557_9578_split[5]), &(SplitJoin84_FFTReorderSimple_Fiss_9557_9578_join[5]));
	ENDFOR
}

void FFTReorderSimple_9489() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin84_FFTReorderSimple_Fiss_9557_9578_split[6]), &(SplitJoin84_FFTReorderSimple_Fiss_9557_9578_join[6]));
	ENDFOR
}

void FFTReorderSimple_9490() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin84_FFTReorderSimple_Fiss_9557_9578_split[7]), &(SplitJoin84_FFTReorderSimple_Fiss_9557_9578_join[7]));
	ENDFOR
}

void FFTReorderSimple_9491() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin84_FFTReorderSimple_Fiss_9557_9578_split[8]), &(SplitJoin84_FFTReorderSimple_Fiss_9557_9578_join[8]));
	ENDFOR
}

void FFTReorderSimple_9492() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin84_FFTReorderSimple_Fiss_9557_9578_split[9]), &(SplitJoin84_FFTReorderSimple_Fiss_9557_9578_join[9]));
	ENDFOR
}

void FFTReorderSimple_9493() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin84_FFTReorderSimple_Fiss_9557_9578_split[10]), &(SplitJoin84_FFTReorderSimple_Fiss_9557_9578_join[10]));
	ENDFOR
}

void FFTReorderSimple_9494() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin84_FFTReorderSimple_Fiss_9557_9578_split[11]), &(SplitJoin84_FFTReorderSimple_Fiss_9557_9578_join[11]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9481() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 12, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin84_FFTReorderSimple_Fiss_9557_9578_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9472WEIGHTED_ROUND_ROBIN_Splitter_9481));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9482() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 12, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9482WEIGHTED_ROUND_ROBIN_Splitter_9495, pop_float(&SplitJoin84_FFTReorderSimple_Fiss_9557_9578_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_9497() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin86_CombineDFT_Fiss_9558_9579_split[0]), &(SplitJoin86_CombineDFT_Fiss_9558_9579_join[0]));
	ENDFOR
}

void CombineDFT_9498() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin86_CombineDFT_Fiss_9558_9579_split[1]), &(SplitJoin86_CombineDFT_Fiss_9558_9579_join[1]));
	ENDFOR
}

void CombineDFT_9499() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin86_CombineDFT_Fiss_9558_9579_split[2]), &(SplitJoin86_CombineDFT_Fiss_9558_9579_join[2]));
	ENDFOR
}

void CombineDFT_9500() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin86_CombineDFT_Fiss_9558_9579_split[3]), &(SplitJoin86_CombineDFT_Fiss_9558_9579_join[3]));
	ENDFOR
}

void CombineDFT_9501() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin86_CombineDFT_Fiss_9558_9579_split[4]), &(SplitJoin86_CombineDFT_Fiss_9558_9579_join[4]));
	ENDFOR
}

void CombineDFT_9502() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin86_CombineDFT_Fiss_9558_9579_split[5]), &(SplitJoin86_CombineDFT_Fiss_9558_9579_join[5]));
	ENDFOR
}

void CombineDFT_9503() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin86_CombineDFT_Fiss_9558_9579_split[6]), &(SplitJoin86_CombineDFT_Fiss_9558_9579_join[6]));
	ENDFOR
}

void CombineDFT_9504() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin86_CombineDFT_Fiss_9558_9579_split[7]), &(SplitJoin86_CombineDFT_Fiss_9558_9579_join[7]));
	ENDFOR
}

void CombineDFT_9505() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin86_CombineDFT_Fiss_9558_9579_split[8]), &(SplitJoin86_CombineDFT_Fiss_9558_9579_join[8]));
	ENDFOR
}

void CombineDFT_9506() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin86_CombineDFT_Fiss_9558_9579_split[9]), &(SplitJoin86_CombineDFT_Fiss_9558_9579_join[9]));
	ENDFOR
}

void CombineDFT_9507() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin86_CombineDFT_Fiss_9558_9579_split[10]), &(SplitJoin86_CombineDFT_Fiss_9558_9579_join[10]));
	ENDFOR
}

void CombineDFT_9508() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin86_CombineDFT_Fiss_9558_9579_split[11]), &(SplitJoin86_CombineDFT_Fiss_9558_9579_join[11]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9495() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 12, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin86_CombineDFT_Fiss_9558_9579_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9482WEIGHTED_ROUND_ROBIN_Splitter_9495));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9496() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 12, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9496WEIGHTED_ROUND_ROBIN_Splitter_9509, pop_float(&SplitJoin86_CombineDFT_Fiss_9558_9579_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_9511() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin88_CombineDFT_Fiss_9559_9580_split[0]), &(SplitJoin88_CombineDFT_Fiss_9559_9580_join[0]));
	ENDFOR
}

void CombineDFT_9512() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin88_CombineDFT_Fiss_9559_9580_split[1]), &(SplitJoin88_CombineDFT_Fiss_9559_9580_join[1]));
	ENDFOR
}

void CombineDFT_9513() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin88_CombineDFT_Fiss_9559_9580_split[2]), &(SplitJoin88_CombineDFT_Fiss_9559_9580_join[2]));
	ENDFOR
}

void CombineDFT_9514() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin88_CombineDFT_Fiss_9559_9580_split[3]), &(SplitJoin88_CombineDFT_Fiss_9559_9580_join[3]));
	ENDFOR
}

void CombineDFT_9515() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin88_CombineDFT_Fiss_9559_9580_split[4]), &(SplitJoin88_CombineDFT_Fiss_9559_9580_join[4]));
	ENDFOR
}

void CombineDFT_9516() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin88_CombineDFT_Fiss_9559_9580_split[5]), &(SplitJoin88_CombineDFT_Fiss_9559_9580_join[5]));
	ENDFOR
}

void CombineDFT_9517() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin88_CombineDFT_Fiss_9559_9580_split[6]), &(SplitJoin88_CombineDFT_Fiss_9559_9580_join[6]));
	ENDFOR
}

void CombineDFT_9518() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin88_CombineDFT_Fiss_9559_9580_split[7]), &(SplitJoin88_CombineDFT_Fiss_9559_9580_join[7]));
	ENDFOR
}

void CombineDFT_9519() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin88_CombineDFT_Fiss_9559_9580_split[8]), &(SplitJoin88_CombineDFT_Fiss_9559_9580_join[8]));
	ENDFOR
}

void CombineDFT_9520() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin88_CombineDFT_Fiss_9559_9580_split[9]), &(SplitJoin88_CombineDFT_Fiss_9559_9580_join[9]));
	ENDFOR
}

void CombineDFT_9521() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin88_CombineDFT_Fiss_9559_9580_split[10]), &(SplitJoin88_CombineDFT_Fiss_9559_9580_join[10]));
	ENDFOR
}

void CombineDFT_9522() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin88_CombineDFT_Fiss_9559_9580_split[11]), &(SplitJoin88_CombineDFT_Fiss_9559_9580_join[11]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9509() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 12, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin88_CombineDFT_Fiss_9559_9580_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9496WEIGHTED_ROUND_ROBIN_Splitter_9509));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9510() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 12, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9510WEIGHTED_ROUND_ROBIN_Splitter_9523, pop_float(&SplitJoin88_CombineDFT_Fiss_9559_9580_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_9525() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin90_CombineDFT_Fiss_9560_9581_split[0]), &(SplitJoin90_CombineDFT_Fiss_9560_9581_join[0]));
	ENDFOR
}

void CombineDFT_9526() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin90_CombineDFT_Fiss_9560_9581_split[1]), &(SplitJoin90_CombineDFT_Fiss_9560_9581_join[1]));
	ENDFOR
}

void CombineDFT_9527() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin90_CombineDFT_Fiss_9560_9581_split[2]), &(SplitJoin90_CombineDFT_Fiss_9560_9581_join[2]));
	ENDFOR
}

void CombineDFT_9528() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin90_CombineDFT_Fiss_9560_9581_split[3]), &(SplitJoin90_CombineDFT_Fiss_9560_9581_join[3]));
	ENDFOR
}

void CombineDFT_9529() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin90_CombineDFT_Fiss_9560_9581_split[4]), &(SplitJoin90_CombineDFT_Fiss_9560_9581_join[4]));
	ENDFOR
}

void CombineDFT_9530() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin90_CombineDFT_Fiss_9560_9581_split[5]), &(SplitJoin90_CombineDFT_Fiss_9560_9581_join[5]));
	ENDFOR
}

void CombineDFT_9531() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin90_CombineDFT_Fiss_9560_9581_split[6]), &(SplitJoin90_CombineDFT_Fiss_9560_9581_join[6]));
	ENDFOR
}

void CombineDFT_9532() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin90_CombineDFT_Fiss_9560_9581_split[7]), &(SplitJoin90_CombineDFT_Fiss_9560_9581_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9523() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin90_CombineDFT_Fiss_9560_9581_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9510WEIGHTED_ROUND_ROBIN_Splitter_9523));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9524() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9524WEIGHTED_ROUND_ROBIN_Splitter_9533, pop_float(&SplitJoin90_CombineDFT_Fiss_9560_9581_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_9535() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin92_CombineDFT_Fiss_9561_9582_split[0]), &(SplitJoin92_CombineDFT_Fiss_9561_9582_join[0]));
	ENDFOR
}

void CombineDFT_9536() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin92_CombineDFT_Fiss_9561_9582_split[1]), &(SplitJoin92_CombineDFT_Fiss_9561_9582_join[1]));
	ENDFOR
}

void CombineDFT_9537() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin92_CombineDFT_Fiss_9561_9582_split[2]), &(SplitJoin92_CombineDFT_Fiss_9561_9582_join[2]));
	ENDFOR
}

void CombineDFT_9538() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin92_CombineDFT_Fiss_9561_9582_split[3]), &(SplitJoin92_CombineDFT_Fiss_9561_9582_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9533() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin92_CombineDFT_Fiss_9561_9582_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9524WEIGHTED_ROUND_ROBIN_Splitter_9533));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9534() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9534WEIGHTED_ROUND_ROBIN_Splitter_9539, pop_float(&SplitJoin92_CombineDFT_Fiss_9561_9582_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_9541() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin94_CombineDFT_Fiss_9562_9583_split[0]), &(SplitJoin94_CombineDFT_Fiss_9562_9583_join[0]));
	ENDFOR
}

void CombineDFT_9542() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin94_CombineDFT_Fiss_9562_9583_split[1]), &(SplitJoin94_CombineDFT_Fiss_9562_9583_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9539() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin94_CombineDFT_Fiss_9562_9583_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9534WEIGHTED_ROUND_ROBIN_Splitter_9539));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin94_CombineDFT_Fiss_9562_9583_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9534WEIGHTED_ROUND_ROBIN_Splitter_9539));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9540() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9540CombineDFT_9365, pop_float(&SplitJoin94_CombineDFT_Fiss_9562_9583_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9540CombineDFT_9365, pop_float(&SplitJoin94_CombineDFT_Fiss_9562_9583_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_9365() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_9540CombineDFT_9365), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_9333_9369_9544_9565_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9367() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_9333_9369_9544_9565_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9376WEIGHTED_ROUND_ROBIN_Splitter_9367));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_9333_9369_9544_9565_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_9376WEIGHTED_ROUND_ROBIN_Splitter_9367));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9368() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9368FloatPrinter_9366, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_9333_9369_9544_9565_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9368FloatPrinter_9366, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_9333_9369_9544_9565_join[1]));
		ENDFOR
	ENDFOR
}}

void FloatPrinter(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void FloatPrinter_9366() {
	FOR(uint32_t, __iter_steady_, 0, <, 768, __iter_steady_++)
		FloatPrinter(&(WEIGHTED_ROUND_ROBIN_Joiner_9368FloatPrinter_9366));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&FFTReorderSimple_9355WEIGHTED_ROUND_ROBIN_Splitter_9461);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9534WEIGHTED_ROUND_ROBIN_Splitter_9539);
	FOR(int, __iter_init_0_, 0, <, 12, __iter_init_0_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_9548_9569_split[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9428WEIGHTED_ROUND_ROBIN_Splitter_9441);
	FOR(int, __iter_init_1_, 0, <, 2, __iter_init_1_++)
		init_buffer_float(&SplitJoin94_CombineDFT_Fiss_9562_9583_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_9545_9566_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 12, __iter_init_3_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_9550_9571_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 12, __iter_init_4_++)
		init_buffer_float(&SplitJoin86_CombineDFT_Fiss_9558_9579_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 12, __iter_init_5_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_9548_9569_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 2, __iter_init_6_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_9543_9564_join[__iter_init_6_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9384WEIGHTED_ROUND_ROBIN_Splitter_9389);
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_float(&SplitJoin82_FFTReorderSimple_Fiss_9556_9577_join[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9452WEIGHTED_ROUND_ROBIN_Splitter_9457);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9466WEIGHTED_ROUND_ROBIN_Splitter_9471);
	FOR(int, __iter_init_8_, 0, <, 12, __iter_init_8_++)
		init_buffer_float(&SplitJoin88_CombineDFT_Fiss_9559_9580_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 12, __iter_init_9_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_9549_9570_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 4, __iter_init_10_++)
		init_buffer_float(&SplitJoin92_CombineDFT_Fiss_9561_9582_split[__iter_init_10_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9390WEIGHTED_ROUND_ROBIN_Splitter_9399);
	FOR(int, __iter_init_11_, 0, <, 12, __iter_init_11_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_9549_9570_split[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 12, __iter_init_12_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_9550_9571_join[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 12, __iter_init_13_++)
		init_buffer_float(&SplitJoin88_CombineDFT_Fiss_9559_9580_split[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_9333_9369_9544_9565_join[__iter_init_14_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9482WEIGHTED_ROUND_ROBIN_Splitter_9495);
	FOR(int, __iter_init_15_, 0, <, 8, __iter_init_15_++)
		init_buffer_float(&SplitJoin90_CombineDFT_Fiss_9560_9581_split[__iter_init_15_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9376WEIGHTED_ROUND_ROBIN_Splitter_9367);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9442WEIGHTED_ROUND_ROBIN_Splitter_9451);
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_float(&SplitJoin94_CombineDFT_Fiss_9562_9583_join[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 4, __iter_init_17_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_9552_9573_split[__iter_init_17_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9510WEIGHTED_ROUND_ROBIN_Splitter_9523);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9414WEIGHTED_ROUND_ROBIN_Splitter_9427);
	FOR(int, __iter_init_18_, 0, <, 4, __iter_init_18_++)
		init_buffer_float(&SplitJoin80_FFTReorderSimple_Fiss_9555_9576_join[__iter_init_18_]);
	ENDFOR
	FOR(int, __iter_init_19_, 0, <, 2, __iter_init_19_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_9545_9566_split[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 12, __iter_init_20_++)
		init_buffer_float(&SplitJoin84_FFTReorderSimple_Fiss_9557_9578_split[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 8, __iter_init_21_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_9547_9568_split[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 8, __iter_init_22_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_9551_9572_join[__iter_init_22_]);
	ENDFOR
	FOR(int, __iter_init_23_, 0, <, 12, __iter_init_23_++)
		init_buffer_float(&SplitJoin86_CombineDFT_Fiss_9558_9579_join[__iter_init_23_]);
	ENDFOR
	FOR(int, __iter_init_24_, 0, <, 8, __iter_init_24_++)
		init_buffer_float(&SplitJoin82_FFTReorderSimple_Fiss_9556_9577_split[__iter_init_24_]);
	ENDFOR
	FOR(int, __iter_init_25_, 0, <, 4, __iter_init_25_++)
		init_buffer_float(&SplitJoin92_CombineDFT_Fiss_9561_9582_join[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 2, __iter_init_26_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_9333_9369_9544_9565_split[__iter_init_26_]);
	ENDFOR
	FOR(int, __iter_init_27_, 0, <, 8, __iter_init_27_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_9551_9572_split[__iter_init_27_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9462WEIGHTED_ROUND_ROBIN_Splitter_9465);
	FOR(int, __iter_init_28_, 0, <, 4, __iter_init_28_++)
		init_buffer_float(&SplitJoin80_FFTReorderSimple_Fiss_9555_9576_split[__iter_init_28_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9458CombineDFT_9354);
	FOR(int, __iter_init_29_, 0, <, 8, __iter_init_29_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_9547_9568_join[__iter_init_29_]);
	ENDFOR
	FOR(int, __iter_init_30_, 0, <, 2, __iter_init_30_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_9553_9574_join[__iter_init_30_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9524WEIGHTED_ROUND_ROBIN_Splitter_9533);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9540CombineDFT_9365);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9400WEIGHTED_ROUND_ROBIN_Splitter_9413);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9496WEIGHTED_ROUND_ROBIN_Splitter_9509);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9380WEIGHTED_ROUND_ROBIN_Splitter_9383);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9472WEIGHTED_ROUND_ROBIN_Splitter_9481);
	FOR(int, __iter_init_31_, 0, <, 2, __iter_init_31_++)
		init_buffer_float(&SplitJoin78_FFTReorderSimple_Fiss_9554_9575_split[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 8, __iter_init_32_++)
		init_buffer_float(&SplitJoin90_CombineDFT_Fiss_9560_9581_join[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 2, __iter_init_33_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_9543_9564_split[__iter_init_33_]);
	ENDFOR
	FOR(int, __iter_init_34_, 0, <, 2, __iter_init_34_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_9553_9574_split[__iter_init_34_]);
	ENDFOR
	init_buffer_float(&FFTReorderSimple_9344WEIGHTED_ROUND_ROBIN_Splitter_9379);
	FOR(int, __iter_init_35_, 0, <, 2, __iter_init_35_++)
		init_buffer_float(&SplitJoin78_FFTReorderSimple_Fiss_9554_9575_join[__iter_init_35_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9368FloatPrinter_9366);
	FOR(int, __iter_init_36_, 0, <, 4, __iter_init_36_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_9546_9567_split[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 12, __iter_init_37_++)
		init_buffer_float(&SplitJoin84_FFTReorderSimple_Fiss_9557_9578_join[__iter_init_37_]);
	ENDFOR
	FOR(int, __iter_init_38_, 0, <, 4, __iter_init_38_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_9546_9567_join[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 4, __iter_init_39_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_9552_9573_join[__iter_init_39_]);
	ENDFOR
// --- init: CombineDFT_9415
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9415_s.w[i] = real ; 
		CombineDFT_9415_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9416
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9416_s.w[i] = real ; 
		CombineDFT_9416_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9417
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9417_s.w[i] = real ; 
		CombineDFT_9417_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9418
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9418_s.w[i] = real ; 
		CombineDFT_9418_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9419
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9419_s.w[i] = real ; 
		CombineDFT_9419_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9420
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9420_s.w[i] = real ; 
		CombineDFT_9420_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9421
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9421_s.w[i] = real ; 
		CombineDFT_9421_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9422
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9422_s.w[i] = real ; 
		CombineDFT_9422_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9423
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9423_s.w[i] = real ; 
		CombineDFT_9423_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9424
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9424_s.w[i] = real ; 
		CombineDFT_9424_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9425
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9425_s.w[i] = real ; 
		CombineDFT_9425_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9426
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9426_s.w[i] = real ; 
		CombineDFT_9426_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9429
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9429_s.w[i] = real ; 
		CombineDFT_9429_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9430
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9430_s.w[i] = real ; 
		CombineDFT_9430_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9431
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9431_s.w[i] = real ; 
		CombineDFT_9431_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9432
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9432_s.w[i] = real ; 
		CombineDFT_9432_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9433
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9433_s.w[i] = real ; 
		CombineDFT_9433_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9434
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9434_s.w[i] = real ; 
		CombineDFT_9434_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9435
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9435_s.w[i] = real ; 
		CombineDFT_9435_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9436
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9436_s.w[i] = real ; 
		CombineDFT_9436_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9437
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9437_s.w[i] = real ; 
		CombineDFT_9437_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9438
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9438_s.w[i] = real ; 
		CombineDFT_9438_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9439
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9439_s.w[i] = real ; 
		CombineDFT_9439_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9440
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9440_s.w[i] = real ; 
		CombineDFT_9440_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9443
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9443_s.w[i] = real ; 
		CombineDFT_9443_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9444
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9444_s.w[i] = real ; 
		CombineDFT_9444_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9445
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9445_s.w[i] = real ; 
		CombineDFT_9445_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9446
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9446_s.w[i] = real ; 
		CombineDFT_9446_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9447
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9447_s.w[i] = real ; 
		CombineDFT_9447_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9448
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9448_s.w[i] = real ; 
		CombineDFT_9448_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9449
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9449_s.w[i] = real ; 
		CombineDFT_9449_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9450
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9450_s.w[i] = real ; 
		CombineDFT_9450_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9453
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_9453_s.w[i] = real ; 
		CombineDFT_9453_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9454
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_9454_s.w[i] = real ; 
		CombineDFT_9454_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9455
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_9455_s.w[i] = real ; 
		CombineDFT_9455_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9456
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_9456_s.w[i] = real ; 
		CombineDFT_9456_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9459
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_9459_s.w[i] = real ; 
		CombineDFT_9459_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9460
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_9460_s.w[i] = real ; 
		CombineDFT_9460_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9354
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_9354_s.w[i] = real ; 
		CombineDFT_9354_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9497
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9497_s.w[i] = real ; 
		CombineDFT_9497_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9498
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9498_s.w[i] = real ; 
		CombineDFT_9498_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9499
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9499_s.w[i] = real ; 
		CombineDFT_9499_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9500
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9500_s.w[i] = real ; 
		CombineDFT_9500_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9501
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9501_s.w[i] = real ; 
		CombineDFT_9501_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9502
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9502_s.w[i] = real ; 
		CombineDFT_9502_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9503
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9503_s.w[i] = real ; 
		CombineDFT_9503_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9504
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9504_s.w[i] = real ; 
		CombineDFT_9504_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9505
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9505_s.w[i] = real ; 
		CombineDFT_9505_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9506
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9506_s.w[i] = real ; 
		CombineDFT_9506_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9507
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9507_s.w[i] = real ; 
		CombineDFT_9507_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9508
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_9508_s.w[i] = real ; 
		CombineDFT_9508_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9511
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9511_s.w[i] = real ; 
		CombineDFT_9511_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9512
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9512_s.w[i] = real ; 
		CombineDFT_9512_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9513
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9513_s.w[i] = real ; 
		CombineDFT_9513_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9514
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9514_s.w[i] = real ; 
		CombineDFT_9514_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9515
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9515_s.w[i] = real ; 
		CombineDFT_9515_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9516
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9516_s.w[i] = real ; 
		CombineDFT_9516_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9517
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9517_s.w[i] = real ; 
		CombineDFT_9517_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9518
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9518_s.w[i] = real ; 
		CombineDFT_9518_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9519
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9519_s.w[i] = real ; 
		CombineDFT_9519_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9520
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9520_s.w[i] = real ; 
		CombineDFT_9520_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9521
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9521_s.w[i] = real ; 
		CombineDFT_9521_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9522
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_9522_s.w[i] = real ; 
		CombineDFT_9522_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9525
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9525_s.w[i] = real ; 
		CombineDFT_9525_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9526
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9526_s.w[i] = real ; 
		CombineDFT_9526_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9527
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9527_s.w[i] = real ; 
		CombineDFT_9527_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9528
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9528_s.w[i] = real ; 
		CombineDFT_9528_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9529
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9529_s.w[i] = real ; 
		CombineDFT_9529_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9530
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9530_s.w[i] = real ; 
		CombineDFT_9530_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9531
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9531_s.w[i] = real ; 
		CombineDFT_9531_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9532
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_9532_s.w[i] = real ; 
		CombineDFT_9532_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9535
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_9535_s.w[i] = real ; 
		CombineDFT_9535_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9536
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_9536_s.w[i] = real ; 
		CombineDFT_9536_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9537
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_9537_s.w[i] = real ; 
		CombineDFT_9537_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9538
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_9538_s.w[i] = real ; 
		CombineDFT_9538_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9541
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_9541_s.w[i] = real ; 
		CombineDFT_9541_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9542
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_9542_s.w[i] = real ; 
		CombineDFT_9542_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_9365
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_9365_s.w[i] = real ; 
		CombineDFT_9365_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_9375();
			FFTTestSource_9377();
			FFTTestSource_9378();
		WEIGHTED_ROUND_ROBIN_Joiner_9376();
		WEIGHTED_ROUND_ROBIN_Splitter_9367();
			FFTReorderSimple_9344();
			WEIGHTED_ROUND_ROBIN_Splitter_9379();
				FFTReorderSimple_9381();
				FFTReorderSimple_9382();
			WEIGHTED_ROUND_ROBIN_Joiner_9380();
			WEIGHTED_ROUND_ROBIN_Splitter_9383();
				FFTReorderSimple_9385();
				FFTReorderSimple_9386();
				FFTReorderSimple_9387();
				FFTReorderSimple_9388();
			WEIGHTED_ROUND_ROBIN_Joiner_9384();
			WEIGHTED_ROUND_ROBIN_Splitter_9389();
				FFTReorderSimple_9391();
				FFTReorderSimple_9392();
				FFTReorderSimple_9393();
				FFTReorderSimple_9394();
				FFTReorderSimple_9395();
				FFTReorderSimple_9396();
				FFTReorderSimple_9397();
				FFTReorderSimple_9398();
			WEIGHTED_ROUND_ROBIN_Joiner_9390();
			WEIGHTED_ROUND_ROBIN_Splitter_9399();
				FFTReorderSimple_9401();
				FFTReorderSimple_9402();
				FFTReorderSimple_9403();
				FFTReorderSimple_9404();
				FFTReorderSimple_9405();
				FFTReorderSimple_9406();
				FFTReorderSimple_9407();
				FFTReorderSimple_9408();
				FFTReorderSimple_9409();
				FFTReorderSimple_9410();
				FFTReorderSimple_9411();
				FFTReorderSimple_9412();
			WEIGHTED_ROUND_ROBIN_Joiner_9400();
			WEIGHTED_ROUND_ROBIN_Splitter_9413();
				CombineDFT_9415();
				CombineDFT_9416();
				CombineDFT_9417();
				CombineDFT_9418();
				CombineDFT_9419();
				CombineDFT_9420();
				CombineDFT_9421();
				CombineDFT_9422();
				CombineDFT_9423();
				CombineDFT_9424();
				CombineDFT_9425();
				CombineDFT_9426();
			WEIGHTED_ROUND_ROBIN_Joiner_9414();
			WEIGHTED_ROUND_ROBIN_Splitter_9427();
				CombineDFT_9429();
				CombineDFT_9430();
				CombineDFT_9431();
				CombineDFT_9432();
				CombineDFT_9433();
				CombineDFT_9434();
				CombineDFT_9435();
				CombineDFT_9436();
				CombineDFT_9437();
				CombineDFT_9438();
				CombineDFT_9439();
				CombineDFT_9440();
			WEIGHTED_ROUND_ROBIN_Joiner_9428();
			WEIGHTED_ROUND_ROBIN_Splitter_9441();
				CombineDFT_9443();
				CombineDFT_9444();
				CombineDFT_9445();
				CombineDFT_9446();
				CombineDFT_9447();
				CombineDFT_9448();
				CombineDFT_9449();
				CombineDFT_9450();
			WEIGHTED_ROUND_ROBIN_Joiner_9442();
			WEIGHTED_ROUND_ROBIN_Splitter_9451();
				CombineDFT_9453();
				CombineDFT_9454();
				CombineDFT_9455();
				CombineDFT_9456();
			WEIGHTED_ROUND_ROBIN_Joiner_9452();
			WEIGHTED_ROUND_ROBIN_Splitter_9457();
				CombineDFT_9459();
				CombineDFT_9460();
			WEIGHTED_ROUND_ROBIN_Joiner_9458();
			CombineDFT_9354();
			FFTReorderSimple_9355();
			WEIGHTED_ROUND_ROBIN_Splitter_9461();
				FFTReorderSimple_9463();
				FFTReorderSimple_9464();
			WEIGHTED_ROUND_ROBIN_Joiner_9462();
			WEIGHTED_ROUND_ROBIN_Splitter_9465();
				FFTReorderSimple_9467();
				FFTReorderSimple_9468();
				FFTReorderSimple_9469();
				FFTReorderSimple_9470();
			WEIGHTED_ROUND_ROBIN_Joiner_9466();
			WEIGHTED_ROUND_ROBIN_Splitter_9471();
				FFTReorderSimple_9473();
				FFTReorderSimple_9474();
				FFTReorderSimple_9475();
				FFTReorderSimple_9476();
				FFTReorderSimple_9477();
				FFTReorderSimple_9478();
				FFTReorderSimple_9479();
				FFTReorderSimple_9480();
			WEIGHTED_ROUND_ROBIN_Joiner_9472();
			WEIGHTED_ROUND_ROBIN_Splitter_9481();
				FFTReorderSimple_9483();
				FFTReorderSimple_9484();
				FFTReorderSimple_9485();
				FFTReorderSimple_9486();
				FFTReorderSimple_9487();
				FFTReorderSimple_9488();
				FFTReorderSimple_9489();
				FFTReorderSimple_9490();
				FFTReorderSimple_9491();
				FFTReorderSimple_9492();
				FFTReorderSimple_9493();
				FFTReorderSimple_9494();
			WEIGHTED_ROUND_ROBIN_Joiner_9482();
			WEIGHTED_ROUND_ROBIN_Splitter_9495();
				CombineDFT_9497();
				CombineDFT_9498();
				CombineDFT_9499();
				CombineDFT_9500();
				CombineDFT_9501();
				CombineDFT_9502();
				CombineDFT_9503();
				CombineDFT_9504();
				CombineDFT_9505();
				CombineDFT_9506();
				CombineDFT_9507();
				CombineDFT_9508();
			WEIGHTED_ROUND_ROBIN_Joiner_9496();
			WEIGHTED_ROUND_ROBIN_Splitter_9509();
				CombineDFT_9511();
				CombineDFT_9512();
				CombineDFT_9513();
				CombineDFT_9514();
				CombineDFT_9515();
				CombineDFT_9516();
				CombineDFT_9517();
				CombineDFT_9518();
				CombineDFT_9519();
				CombineDFT_9520();
				CombineDFT_9521();
				CombineDFT_9522();
			WEIGHTED_ROUND_ROBIN_Joiner_9510();
			WEIGHTED_ROUND_ROBIN_Splitter_9523();
				CombineDFT_9525();
				CombineDFT_9526();
				CombineDFT_9527();
				CombineDFT_9528();
				CombineDFT_9529();
				CombineDFT_9530();
				CombineDFT_9531();
				CombineDFT_9532();
			WEIGHTED_ROUND_ROBIN_Joiner_9524();
			WEIGHTED_ROUND_ROBIN_Splitter_9533();
				CombineDFT_9535();
				CombineDFT_9536();
				CombineDFT_9537();
				CombineDFT_9538();
			WEIGHTED_ROUND_ROBIN_Joiner_9534();
			WEIGHTED_ROUND_ROBIN_Splitter_9539();
				CombineDFT_9541();
				CombineDFT_9542();
			WEIGHTED_ROUND_ROBIN_Joiner_9540();
			CombineDFT_9365();
		WEIGHTED_ROUND_ROBIN_Joiner_9368();
		FloatPrinter_9366();
	ENDFOR
	return EXIT_SUCCESS;
}
