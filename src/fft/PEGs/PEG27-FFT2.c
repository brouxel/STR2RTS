#include "PEG27-FFT2.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2683WEIGHTED_ROUND_ROBIN_Splitter_2700;
buffer_float_t SplitJoin16_CombineDFT_Fiss_2880_2901_join[8];
buffer_float_t SplitJoin111_CombineDFT_Fiss_2888_2909_join[16];
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_2876_2897_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2869CombineDFT_2648;
buffer_float_t SplitJoin12_CombineDFT_Fiss_2878_2899_join[27];
buffer_float_t SplitJoin20_CombineDFT_Fiss_2882_2903_split[2];
buffer_float_t SplitJoin107_FFTReorderSimple_Fiss_2886_2907_split[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2667WEIGHTED_ROUND_ROBIN_Splitter_2672;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2730WEIGHTED_ROUND_ROBIN_Splitter_2747;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2768WEIGHTED_ROUND_ROBIN_Splitter_2771;
buffer_float_t SplitJoin105_FFTReorderSimple_Fiss_2885_2906_split[8];
buffer_float_t SplitJoin103_FFTReorderSimple_Fiss_2884_2905_join[4];
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_2875_2896_split[4];
buffer_float_t SplitJoin109_CombineDFT_Fiss_2887_2908_join[27];
buffer_float_t SplitJoin111_CombineDFT_Fiss_2888_2909_split[16];
buffer_float_t SplitJoin115_CombineDFT_Fiss_2890_2911_join[4];
buffer_float_t SplitJoin0_FFTTestSource_Fiss_2872_2893_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2659WEIGHTED_ROUND_ROBIN_Splitter_2650;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2863WEIGHTED_ROUND_ROBIN_Splitter_2868;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2748WEIGHTED_ROUND_ROBIN_Splitter_2757;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2806WEIGHTED_ROUND_ROBIN_Splitter_2834;
buffer_float_t FFTReorderSimple_2638WEIGHTED_ROUND_ROBIN_Splitter_2767;
buffer_float_t SplitJoin101_FFTReorderSimple_Fiss_2883_2904_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2673WEIGHTED_ROUND_ROBIN_Splitter_2682;
buffer_float_t SplitJoin16_CombineDFT_Fiss_2880_2901_split[8];
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_2877_2898_join[16];
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_2875_2896_join[4];
buffer_float_t SplitJoin103_FFTReorderSimple_Fiss_2884_2905_split[4];
buffer_float_t SplitJoin117_CombineDFT_Fiss_2891_2912_join[2];
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_2877_2898_split[16];
buffer_float_t SplitJoin18_CombineDFT_Fiss_2881_2902_split[4];
buffer_float_t SplitJoin20_CombineDFT_Fiss_2882_2903_join[2];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_2616_2652_2873_2894_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2663WEIGHTED_ROUND_ROBIN_Splitter_2666;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2758WEIGHTED_ROUND_ROBIN_Splitter_2763;
buffer_float_t SplitJoin113_CombineDFT_Fiss_2889_2910_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2778WEIGHTED_ROUND_ROBIN_Splitter_2787;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2835WEIGHTED_ROUND_ROBIN_Splitter_2852;
buffer_float_t SplitJoin107_FFTReorderSimple_Fiss_2886_2907_join[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2772WEIGHTED_ROUND_ROBIN_Splitter_2777;
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_2874_2895_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2764CombineDFT_2637;
buffer_float_t SplitJoin117_CombineDFT_Fiss_2891_2912_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2701WEIGHTED_ROUND_ROBIN_Splitter_2729;
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_2616_2652_2873_2894_split[2];
buffer_float_t SplitJoin12_CombineDFT_Fiss_2878_2899_split[27];
buffer_float_t SplitJoin14_CombineDFT_Fiss_2879_2900_join[16];
buffer_float_t FFTReorderSimple_2627WEIGHTED_ROUND_ROBIN_Splitter_2662;
buffer_float_t SplitJoin109_CombineDFT_Fiss_2887_2908_split[27];
buffer_float_t SplitJoin18_CombineDFT_Fiss_2881_2902_join[4];
buffer_float_t SplitJoin14_CombineDFT_Fiss_2879_2900_split[16];
buffer_float_t SplitJoin101_FFTReorderSimple_Fiss_2883_2904_split[2];
buffer_float_t SplitJoin105_FFTReorderSimple_Fiss_2885_2906_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2651FloatPrinter_2649;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2853WEIGHTED_ROUND_ROBIN_Splitter_2862;
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_2874_2895_split[2];
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_2876_2897_join[8];
buffer_float_t SplitJoin115_CombineDFT_Fiss_2890_2911_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_2788WEIGHTED_ROUND_ROBIN_Splitter_2805;
buffer_float_t SplitJoin113_CombineDFT_Fiss_2889_2910_join[8];
buffer_float_t SplitJoin0_FFTTestSource_Fiss_2872_2893_join[2];


CombineDFT_2702_t CombineDFT_2702_s;
CombineDFT_2702_t CombineDFT_2703_s;
CombineDFT_2702_t CombineDFT_2704_s;
CombineDFT_2702_t CombineDFT_2705_s;
CombineDFT_2702_t CombineDFT_2706_s;
CombineDFT_2702_t CombineDFT_2707_s;
CombineDFT_2702_t CombineDFT_2708_s;
CombineDFT_2702_t CombineDFT_2709_s;
CombineDFT_2702_t CombineDFT_2710_s;
CombineDFT_2702_t CombineDFT_2711_s;
CombineDFT_2702_t CombineDFT_2712_s;
CombineDFT_2702_t CombineDFT_2713_s;
CombineDFT_2702_t CombineDFT_2714_s;
CombineDFT_2702_t CombineDFT_2715_s;
CombineDFT_2702_t CombineDFT_2716_s;
CombineDFT_2702_t CombineDFT_2717_s;
CombineDFT_2702_t CombineDFT_2718_s;
CombineDFT_2702_t CombineDFT_2719_s;
CombineDFT_2702_t CombineDFT_2720_s;
CombineDFT_2702_t CombineDFT_2721_s;
CombineDFT_2702_t CombineDFT_2722_s;
CombineDFT_2702_t CombineDFT_2723_s;
CombineDFT_2702_t CombineDFT_2724_s;
CombineDFT_2702_t CombineDFT_2725_s;
CombineDFT_2702_t CombineDFT_2726_s;
CombineDFT_2702_t CombineDFT_2727_s;
CombineDFT_2702_t CombineDFT_2728_s;
CombineDFT_2731_t CombineDFT_2731_s;
CombineDFT_2731_t CombineDFT_2732_s;
CombineDFT_2731_t CombineDFT_2733_s;
CombineDFT_2731_t CombineDFT_2734_s;
CombineDFT_2731_t CombineDFT_2735_s;
CombineDFT_2731_t CombineDFT_2736_s;
CombineDFT_2731_t CombineDFT_2737_s;
CombineDFT_2731_t CombineDFT_2738_s;
CombineDFT_2731_t CombineDFT_2739_s;
CombineDFT_2731_t CombineDFT_2740_s;
CombineDFT_2731_t CombineDFT_2741_s;
CombineDFT_2731_t CombineDFT_2742_s;
CombineDFT_2731_t CombineDFT_2743_s;
CombineDFT_2731_t CombineDFT_2744_s;
CombineDFT_2731_t CombineDFT_2745_s;
CombineDFT_2731_t CombineDFT_2746_s;
CombineDFT_2749_t CombineDFT_2749_s;
CombineDFT_2749_t CombineDFT_2750_s;
CombineDFT_2749_t CombineDFT_2751_s;
CombineDFT_2749_t CombineDFT_2752_s;
CombineDFT_2749_t CombineDFT_2753_s;
CombineDFT_2749_t CombineDFT_2754_s;
CombineDFT_2749_t CombineDFT_2755_s;
CombineDFT_2749_t CombineDFT_2756_s;
CombineDFT_2759_t CombineDFT_2759_s;
CombineDFT_2759_t CombineDFT_2760_s;
CombineDFT_2759_t CombineDFT_2761_s;
CombineDFT_2759_t CombineDFT_2762_s;
CombineDFT_2765_t CombineDFT_2765_s;
CombineDFT_2765_t CombineDFT_2766_s;
CombineDFT_2637_t CombineDFT_2637_s;
CombineDFT_2702_t CombineDFT_2807_s;
CombineDFT_2702_t CombineDFT_2808_s;
CombineDFT_2702_t CombineDFT_2809_s;
CombineDFT_2702_t CombineDFT_2810_s;
CombineDFT_2702_t CombineDFT_2811_s;
CombineDFT_2702_t CombineDFT_2812_s;
CombineDFT_2702_t CombineDFT_2813_s;
CombineDFT_2702_t CombineDFT_2814_s;
CombineDFT_2702_t CombineDFT_2815_s;
CombineDFT_2702_t CombineDFT_2816_s;
CombineDFT_2702_t CombineDFT_2817_s;
CombineDFT_2702_t CombineDFT_2818_s;
CombineDFT_2702_t CombineDFT_2819_s;
CombineDFT_2702_t CombineDFT_2820_s;
CombineDFT_2702_t CombineDFT_2821_s;
CombineDFT_2702_t CombineDFT_2822_s;
CombineDFT_2702_t CombineDFT_2823_s;
CombineDFT_2702_t CombineDFT_2824_s;
CombineDFT_2702_t CombineDFT_2825_s;
CombineDFT_2702_t CombineDFT_2826_s;
CombineDFT_2702_t CombineDFT_2827_s;
CombineDFT_2702_t CombineDFT_2828_s;
CombineDFT_2702_t CombineDFT_2829_s;
CombineDFT_2702_t CombineDFT_2830_s;
CombineDFT_2702_t CombineDFT_2831_s;
CombineDFT_2702_t CombineDFT_2832_s;
CombineDFT_2702_t CombineDFT_2833_s;
CombineDFT_2731_t CombineDFT_2836_s;
CombineDFT_2731_t CombineDFT_2837_s;
CombineDFT_2731_t CombineDFT_2838_s;
CombineDFT_2731_t CombineDFT_2839_s;
CombineDFT_2731_t CombineDFT_2840_s;
CombineDFT_2731_t CombineDFT_2841_s;
CombineDFT_2731_t CombineDFT_2842_s;
CombineDFT_2731_t CombineDFT_2843_s;
CombineDFT_2731_t CombineDFT_2844_s;
CombineDFT_2731_t CombineDFT_2845_s;
CombineDFT_2731_t CombineDFT_2846_s;
CombineDFT_2731_t CombineDFT_2847_s;
CombineDFT_2731_t CombineDFT_2848_s;
CombineDFT_2731_t CombineDFT_2849_s;
CombineDFT_2731_t CombineDFT_2850_s;
CombineDFT_2731_t CombineDFT_2851_s;
CombineDFT_2749_t CombineDFT_2854_s;
CombineDFT_2749_t CombineDFT_2855_s;
CombineDFT_2749_t CombineDFT_2856_s;
CombineDFT_2749_t CombineDFT_2857_s;
CombineDFT_2749_t CombineDFT_2858_s;
CombineDFT_2749_t CombineDFT_2859_s;
CombineDFT_2749_t CombineDFT_2860_s;
CombineDFT_2749_t CombineDFT_2861_s;
CombineDFT_2759_t CombineDFT_2864_s;
CombineDFT_2759_t CombineDFT_2865_s;
CombineDFT_2759_t CombineDFT_2866_s;
CombineDFT_2759_t CombineDFT_2867_s;
CombineDFT_2765_t CombineDFT_2870_s;
CombineDFT_2765_t CombineDFT_2871_s;
CombineDFT_2637_t CombineDFT_2648_s;

void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), 0.0) ; 
		push_float(&(*chanout), 0.0) ; 
		push_float(&(*chanout), 1.0) ; 
		push_float(&(*chanout), 0.0) ; 
		FOR(int, i, 0,  < , 124, i++) {
			push_float(&(*chanout), 0.0) ; 
		}
		ENDFOR
	}


void FFTTestSource_2660() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_2872_2893_split[0]), &(SplitJoin0_FFTTestSource_Fiss_2872_2893_join[0]));
	ENDFOR
}

void FFTTestSource_2661() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_2872_2893_split[1]), &(SplitJoin0_FFTTestSource_Fiss_2872_2893_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2658() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_2659() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2659WEIGHTED_ROUND_ROBIN_Splitter_2650, pop_float(&SplitJoin0_FFTTestSource_Fiss_2872_2893_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2659WEIGHTED_ROUND_ROBIN_Splitter_2650, pop_float(&SplitJoin0_FFTTestSource_Fiss_2872_2893_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, i, 0,  < , 128, i = (i + 4)) {
			push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 128, i = (i + 4)) {
			push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_float(&(*chanin)) ; 
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void FFTReorderSimple_2627() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_2616_2652_2873_2894_split[0]), &(FFTReorderSimple_2627WEIGHTED_ROUND_ROBIN_Splitter_2662));
	ENDFOR
}

void FFTReorderSimple_2664() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_2874_2895_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_2874_2895_join[0]));
	ENDFOR
}

void FFTReorderSimple_2665() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_2874_2895_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_2874_2895_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2662() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_2874_2895_split[0], pop_float(&FFTReorderSimple_2627WEIGHTED_ROUND_ROBIN_Splitter_2662));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_2874_2895_split[1], pop_float(&FFTReorderSimple_2627WEIGHTED_ROUND_ROBIN_Splitter_2662));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2663() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2663WEIGHTED_ROUND_ROBIN_Splitter_2666, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_2874_2895_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2663WEIGHTED_ROUND_ROBIN_Splitter_2666, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_2874_2895_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_2668() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2875_2896_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_2875_2896_join[0]));
	ENDFOR
}

void FFTReorderSimple_2669() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2875_2896_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_2875_2896_join[1]));
	ENDFOR
}

void FFTReorderSimple_2670() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2875_2896_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_2875_2896_join[2]));
	ENDFOR
}

void FFTReorderSimple_2671() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2875_2896_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_2875_2896_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2666() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin6_FFTReorderSimple_Fiss_2875_2896_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2663WEIGHTED_ROUND_ROBIN_Splitter_2666));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2667() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2667WEIGHTED_ROUND_ROBIN_Splitter_2672, pop_float(&SplitJoin6_FFTReorderSimple_Fiss_2875_2896_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_2674() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_2876_2897_split[0]), &(SplitJoin8_FFTReorderSimple_Fiss_2876_2897_join[0]));
	ENDFOR
}

void FFTReorderSimple_2675() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_2876_2897_split[1]), &(SplitJoin8_FFTReorderSimple_Fiss_2876_2897_join[1]));
	ENDFOR
}

void FFTReorderSimple_2676() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_2876_2897_split[2]), &(SplitJoin8_FFTReorderSimple_Fiss_2876_2897_join[2]));
	ENDFOR
}

void FFTReorderSimple_2677() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_2876_2897_split[3]), &(SplitJoin8_FFTReorderSimple_Fiss_2876_2897_join[3]));
	ENDFOR
}

void FFTReorderSimple_2678() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_2876_2897_split[4]), &(SplitJoin8_FFTReorderSimple_Fiss_2876_2897_join[4]));
	ENDFOR
}

void FFTReorderSimple_2679() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_2876_2897_split[5]), &(SplitJoin8_FFTReorderSimple_Fiss_2876_2897_join[5]));
	ENDFOR
}

void FFTReorderSimple_2680() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_2876_2897_split[6]), &(SplitJoin8_FFTReorderSimple_Fiss_2876_2897_join[6]));
	ENDFOR
}

void FFTReorderSimple_2681() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_2876_2897_split[7]), &(SplitJoin8_FFTReorderSimple_Fiss_2876_2897_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2672() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin8_FFTReorderSimple_Fiss_2876_2897_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2667WEIGHTED_ROUND_ROBIN_Splitter_2672));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2673() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2673WEIGHTED_ROUND_ROBIN_Splitter_2682, pop_float(&SplitJoin8_FFTReorderSimple_Fiss_2876_2897_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_2684() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_split[0]), &(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_join[0]));
	ENDFOR
}

void FFTReorderSimple_2685() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_split[1]), &(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_join[1]));
	ENDFOR
}

void FFTReorderSimple_2686() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_split[2]), &(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_join[2]));
	ENDFOR
}

void FFTReorderSimple_2687() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_split[3]), &(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_join[3]));
	ENDFOR
}

void FFTReorderSimple_2688() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_split[4]), &(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_join[4]));
	ENDFOR
}

void FFTReorderSimple_2689() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_split[5]), &(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_join[5]));
	ENDFOR
}

void FFTReorderSimple_2690() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_split[6]), &(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_join[6]));
	ENDFOR
}

void FFTReorderSimple_2691() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_split[7]), &(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_join[7]));
	ENDFOR
}

void FFTReorderSimple_2692() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_split[8]), &(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_join[8]));
	ENDFOR
}

void FFTReorderSimple_2693() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_split[9]), &(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_join[9]));
	ENDFOR
}

void FFTReorderSimple_2694() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_split[10]), &(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_join[10]));
	ENDFOR
}

void FFTReorderSimple_2695() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_split[11]), &(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_join[11]));
	ENDFOR
}

void FFTReorderSimple_2696() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_split[12]), &(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_join[12]));
	ENDFOR
}

void FFTReorderSimple_2697() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_split[13]), &(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_join[13]));
	ENDFOR
}

void FFTReorderSimple_2698() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_split[14]), &(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_join[14]));
	ENDFOR
}

void FFTReorderSimple_2699() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_split[15]), &(SplitJoin10_FFTReorderSimple_Fiss_2877_2898_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2682() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_FFTReorderSimple_Fiss_2877_2898_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2673WEIGHTED_ROUND_ROBIN_Splitter_2682));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2683() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2683WEIGHTED_ROUND_ROBIN_Splitter_2700, pop_float(&SplitJoin10_FFTReorderSimple_Fiss_2877_2898_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&(*chanin), i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&(*chanin), i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&(*chanin), (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&(*chanin), (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_2702_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_2702_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&(*chanin)) ; 
			push_float(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_2702() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2878_2899_split[0]), &(SplitJoin12_CombineDFT_Fiss_2878_2899_join[0]));
	ENDFOR
}

void CombineDFT_2703() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2878_2899_split[1]), &(SplitJoin12_CombineDFT_Fiss_2878_2899_join[1]));
	ENDFOR
}

void CombineDFT_2704() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2878_2899_split[2]), &(SplitJoin12_CombineDFT_Fiss_2878_2899_join[2]));
	ENDFOR
}

void CombineDFT_2705() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2878_2899_split[3]), &(SplitJoin12_CombineDFT_Fiss_2878_2899_join[3]));
	ENDFOR
}

void CombineDFT_2706() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2878_2899_split[4]), &(SplitJoin12_CombineDFT_Fiss_2878_2899_join[4]));
	ENDFOR
}

void CombineDFT_2707() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2878_2899_split[5]), &(SplitJoin12_CombineDFT_Fiss_2878_2899_join[5]));
	ENDFOR
}

void CombineDFT_2708() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2878_2899_split[6]), &(SplitJoin12_CombineDFT_Fiss_2878_2899_join[6]));
	ENDFOR
}

void CombineDFT_2709() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2878_2899_split[7]), &(SplitJoin12_CombineDFT_Fiss_2878_2899_join[7]));
	ENDFOR
}

void CombineDFT_2710() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2878_2899_split[8]), &(SplitJoin12_CombineDFT_Fiss_2878_2899_join[8]));
	ENDFOR
}

void CombineDFT_2711() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2878_2899_split[9]), &(SplitJoin12_CombineDFT_Fiss_2878_2899_join[9]));
	ENDFOR
}

void CombineDFT_2712() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2878_2899_split[10]), &(SplitJoin12_CombineDFT_Fiss_2878_2899_join[10]));
	ENDFOR
}

void CombineDFT_2713() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2878_2899_split[11]), &(SplitJoin12_CombineDFT_Fiss_2878_2899_join[11]));
	ENDFOR
}

void CombineDFT_2714() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2878_2899_split[12]), &(SplitJoin12_CombineDFT_Fiss_2878_2899_join[12]));
	ENDFOR
}

void CombineDFT_2715() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2878_2899_split[13]), &(SplitJoin12_CombineDFT_Fiss_2878_2899_join[13]));
	ENDFOR
}

void CombineDFT_2716() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2878_2899_split[14]), &(SplitJoin12_CombineDFT_Fiss_2878_2899_join[14]));
	ENDFOR
}

void CombineDFT_2717() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2878_2899_split[15]), &(SplitJoin12_CombineDFT_Fiss_2878_2899_join[15]));
	ENDFOR
}

void CombineDFT_2718() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2878_2899_split[16]), &(SplitJoin12_CombineDFT_Fiss_2878_2899_join[16]));
	ENDFOR
}

void CombineDFT_2719() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2878_2899_split[17]), &(SplitJoin12_CombineDFT_Fiss_2878_2899_join[17]));
	ENDFOR
}

void CombineDFT_2720() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2878_2899_split[18]), &(SplitJoin12_CombineDFT_Fiss_2878_2899_join[18]));
	ENDFOR
}

void CombineDFT_2721() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2878_2899_split[19]), &(SplitJoin12_CombineDFT_Fiss_2878_2899_join[19]));
	ENDFOR
}

void CombineDFT_2722() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2878_2899_split[20]), &(SplitJoin12_CombineDFT_Fiss_2878_2899_join[20]));
	ENDFOR
}

void CombineDFT_2723() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2878_2899_split[21]), &(SplitJoin12_CombineDFT_Fiss_2878_2899_join[21]));
	ENDFOR
}

void CombineDFT_2724() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2878_2899_split[22]), &(SplitJoin12_CombineDFT_Fiss_2878_2899_join[22]));
	ENDFOR
}

void CombineDFT_2725() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2878_2899_split[23]), &(SplitJoin12_CombineDFT_Fiss_2878_2899_join[23]));
	ENDFOR
}

void CombineDFT_2726() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2878_2899_split[24]), &(SplitJoin12_CombineDFT_Fiss_2878_2899_join[24]));
	ENDFOR
}

void CombineDFT_2727() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2878_2899_split[25]), &(SplitJoin12_CombineDFT_Fiss_2878_2899_join[25]));
	ENDFOR
}

void CombineDFT_2728() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2878_2899_split[26]), &(SplitJoin12_CombineDFT_Fiss_2878_2899_join[26]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2700() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 27, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin12_CombineDFT_Fiss_2878_2899_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2683WEIGHTED_ROUND_ROBIN_Splitter_2700));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2701() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 27, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2701WEIGHTED_ROUND_ROBIN_Splitter_2729, pop_float(&SplitJoin12_CombineDFT_Fiss_2878_2899_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2731() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2879_2900_split[0]), &(SplitJoin14_CombineDFT_Fiss_2879_2900_join[0]));
	ENDFOR
}

void CombineDFT_2732() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2879_2900_split[1]), &(SplitJoin14_CombineDFT_Fiss_2879_2900_join[1]));
	ENDFOR
}

void CombineDFT_2733() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2879_2900_split[2]), &(SplitJoin14_CombineDFT_Fiss_2879_2900_join[2]));
	ENDFOR
}

void CombineDFT_2734() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2879_2900_split[3]), &(SplitJoin14_CombineDFT_Fiss_2879_2900_join[3]));
	ENDFOR
}

void CombineDFT_2735() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2879_2900_split[4]), &(SplitJoin14_CombineDFT_Fiss_2879_2900_join[4]));
	ENDFOR
}

void CombineDFT_2736() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2879_2900_split[5]), &(SplitJoin14_CombineDFT_Fiss_2879_2900_join[5]));
	ENDFOR
}

void CombineDFT_2737() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2879_2900_split[6]), &(SplitJoin14_CombineDFT_Fiss_2879_2900_join[6]));
	ENDFOR
}

void CombineDFT_2738() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2879_2900_split[7]), &(SplitJoin14_CombineDFT_Fiss_2879_2900_join[7]));
	ENDFOR
}

void CombineDFT_2739() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2879_2900_split[8]), &(SplitJoin14_CombineDFT_Fiss_2879_2900_join[8]));
	ENDFOR
}

void CombineDFT_2740() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2879_2900_split[9]), &(SplitJoin14_CombineDFT_Fiss_2879_2900_join[9]));
	ENDFOR
}

void CombineDFT_2741() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2879_2900_split[10]), &(SplitJoin14_CombineDFT_Fiss_2879_2900_join[10]));
	ENDFOR
}

void CombineDFT_2742() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2879_2900_split[11]), &(SplitJoin14_CombineDFT_Fiss_2879_2900_join[11]));
	ENDFOR
}

void CombineDFT_2743() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2879_2900_split[12]), &(SplitJoin14_CombineDFT_Fiss_2879_2900_join[12]));
	ENDFOR
}

void CombineDFT_2744() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2879_2900_split[13]), &(SplitJoin14_CombineDFT_Fiss_2879_2900_join[13]));
	ENDFOR
}

void CombineDFT_2745() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2879_2900_split[14]), &(SplitJoin14_CombineDFT_Fiss_2879_2900_join[14]));
	ENDFOR
}

void CombineDFT_2746() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2879_2900_split[15]), &(SplitJoin14_CombineDFT_Fiss_2879_2900_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2729() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin14_CombineDFT_Fiss_2879_2900_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2701WEIGHTED_ROUND_ROBIN_Splitter_2729));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2730() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2730WEIGHTED_ROUND_ROBIN_Splitter_2747, pop_float(&SplitJoin14_CombineDFT_Fiss_2879_2900_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2749() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_2880_2901_split[0]), &(SplitJoin16_CombineDFT_Fiss_2880_2901_join[0]));
	ENDFOR
}

void CombineDFT_2750() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_2880_2901_split[1]), &(SplitJoin16_CombineDFT_Fiss_2880_2901_join[1]));
	ENDFOR
}

void CombineDFT_2751() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_2880_2901_split[2]), &(SplitJoin16_CombineDFT_Fiss_2880_2901_join[2]));
	ENDFOR
}

void CombineDFT_2752() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_2880_2901_split[3]), &(SplitJoin16_CombineDFT_Fiss_2880_2901_join[3]));
	ENDFOR
}

void CombineDFT_2753() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_2880_2901_split[4]), &(SplitJoin16_CombineDFT_Fiss_2880_2901_join[4]));
	ENDFOR
}

void CombineDFT_2754() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_2880_2901_split[5]), &(SplitJoin16_CombineDFT_Fiss_2880_2901_join[5]));
	ENDFOR
}

void CombineDFT_2755() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_2880_2901_split[6]), &(SplitJoin16_CombineDFT_Fiss_2880_2901_join[6]));
	ENDFOR
}

void CombineDFT_2756() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_2880_2901_split[7]), &(SplitJoin16_CombineDFT_Fiss_2880_2901_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2747() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin16_CombineDFT_Fiss_2880_2901_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2730WEIGHTED_ROUND_ROBIN_Splitter_2747));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2748() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2748WEIGHTED_ROUND_ROBIN_Splitter_2757, pop_float(&SplitJoin16_CombineDFT_Fiss_2880_2901_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2759() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_2881_2902_split[0]), &(SplitJoin18_CombineDFT_Fiss_2881_2902_join[0]));
	ENDFOR
}

void CombineDFT_2760() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_2881_2902_split[1]), &(SplitJoin18_CombineDFT_Fiss_2881_2902_join[1]));
	ENDFOR
}

void CombineDFT_2761() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_2881_2902_split[2]), &(SplitJoin18_CombineDFT_Fiss_2881_2902_join[2]));
	ENDFOR
}

void CombineDFT_2762() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_2881_2902_split[3]), &(SplitJoin18_CombineDFT_Fiss_2881_2902_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2757() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin18_CombineDFT_Fiss_2881_2902_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2748WEIGHTED_ROUND_ROBIN_Splitter_2757));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2758() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2758WEIGHTED_ROUND_ROBIN_Splitter_2763, pop_float(&SplitJoin18_CombineDFT_Fiss_2881_2902_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2765() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin20_CombineDFT_Fiss_2882_2903_split[0]), &(SplitJoin20_CombineDFT_Fiss_2882_2903_join[0]));
	ENDFOR
}

void CombineDFT_2766() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin20_CombineDFT_Fiss_2882_2903_split[1]), &(SplitJoin20_CombineDFT_Fiss_2882_2903_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2763() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_2882_2903_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2758WEIGHTED_ROUND_ROBIN_Splitter_2763));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_2882_2903_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2758WEIGHTED_ROUND_ROBIN_Splitter_2763));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2764() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2764CombineDFT_2637, pop_float(&SplitJoin20_CombineDFT_Fiss_2882_2903_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2764CombineDFT_2637, pop_float(&SplitJoin20_CombineDFT_Fiss_2882_2903_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_2637() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_2764CombineDFT_2637), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_2616_2652_2873_2894_join[0]));
	ENDFOR
}

void FFTReorderSimple_2638() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_2616_2652_2873_2894_split[1]), &(FFTReorderSimple_2638WEIGHTED_ROUND_ROBIN_Splitter_2767));
	ENDFOR
}

void FFTReorderSimple_2769() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin101_FFTReorderSimple_Fiss_2883_2904_split[0]), &(SplitJoin101_FFTReorderSimple_Fiss_2883_2904_join[0]));
	ENDFOR
}

void FFTReorderSimple_2770() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin101_FFTReorderSimple_Fiss_2883_2904_split[1]), &(SplitJoin101_FFTReorderSimple_Fiss_2883_2904_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2767() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin101_FFTReorderSimple_Fiss_2883_2904_split[0], pop_float(&FFTReorderSimple_2638WEIGHTED_ROUND_ROBIN_Splitter_2767));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin101_FFTReorderSimple_Fiss_2883_2904_split[1], pop_float(&FFTReorderSimple_2638WEIGHTED_ROUND_ROBIN_Splitter_2767));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2768() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2768WEIGHTED_ROUND_ROBIN_Splitter_2771, pop_float(&SplitJoin101_FFTReorderSimple_Fiss_2883_2904_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2768WEIGHTED_ROUND_ROBIN_Splitter_2771, pop_float(&SplitJoin101_FFTReorderSimple_Fiss_2883_2904_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_2773() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin103_FFTReorderSimple_Fiss_2884_2905_split[0]), &(SplitJoin103_FFTReorderSimple_Fiss_2884_2905_join[0]));
	ENDFOR
}

void FFTReorderSimple_2774() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin103_FFTReorderSimple_Fiss_2884_2905_split[1]), &(SplitJoin103_FFTReorderSimple_Fiss_2884_2905_join[1]));
	ENDFOR
}

void FFTReorderSimple_2775() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin103_FFTReorderSimple_Fiss_2884_2905_split[2]), &(SplitJoin103_FFTReorderSimple_Fiss_2884_2905_join[2]));
	ENDFOR
}

void FFTReorderSimple_2776() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin103_FFTReorderSimple_Fiss_2884_2905_split[3]), &(SplitJoin103_FFTReorderSimple_Fiss_2884_2905_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2771() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin103_FFTReorderSimple_Fiss_2884_2905_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2768WEIGHTED_ROUND_ROBIN_Splitter_2771));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2772() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2772WEIGHTED_ROUND_ROBIN_Splitter_2777, pop_float(&SplitJoin103_FFTReorderSimple_Fiss_2884_2905_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_2779() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin105_FFTReorderSimple_Fiss_2885_2906_split[0]), &(SplitJoin105_FFTReorderSimple_Fiss_2885_2906_join[0]));
	ENDFOR
}

void FFTReorderSimple_2780() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin105_FFTReorderSimple_Fiss_2885_2906_split[1]), &(SplitJoin105_FFTReorderSimple_Fiss_2885_2906_join[1]));
	ENDFOR
}

void FFTReorderSimple_2781() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin105_FFTReorderSimple_Fiss_2885_2906_split[2]), &(SplitJoin105_FFTReorderSimple_Fiss_2885_2906_join[2]));
	ENDFOR
}

void FFTReorderSimple_2782() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin105_FFTReorderSimple_Fiss_2885_2906_split[3]), &(SplitJoin105_FFTReorderSimple_Fiss_2885_2906_join[3]));
	ENDFOR
}

void FFTReorderSimple_2783() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin105_FFTReorderSimple_Fiss_2885_2906_split[4]), &(SplitJoin105_FFTReorderSimple_Fiss_2885_2906_join[4]));
	ENDFOR
}

void FFTReorderSimple_2784() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin105_FFTReorderSimple_Fiss_2885_2906_split[5]), &(SplitJoin105_FFTReorderSimple_Fiss_2885_2906_join[5]));
	ENDFOR
}

void FFTReorderSimple_2785() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin105_FFTReorderSimple_Fiss_2885_2906_split[6]), &(SplitJoin105_FFTReorderSimple_Fiss_2885_2906_join[6]));
	ENDFOR
}

void FFTReorderSimple_2786() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin105_FFTReorderSimple_Fiss_2885_2906_split[7]), &(SplitJoin105_FFTReorderSimple_Fiss_2885_2906_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2777() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin105_FFTReorderSimple_Fiss_2885_2906_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2772WEIGHTED_ROUND_ROBIN_Splitter_2777));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2778() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2778WEIGHTED_ROUND_ROBIN_Splitter_2787, pop_float(&SplitJoin105_FFTReorderSimple_Fiss_2885_2906_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_2789() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_split[0]), &(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_join[0]));
	ENDFOR
}

void FFTReorderSimple_2790() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_split[1]), &(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_join[1]));
	ENDFOR
}

void FFTReorderSimple_2791() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_split[2]), &(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_join[2]));
	ENDFOR
}

void FFTReorderSimple_2792() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_split[3]), &(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_join[3]));
	ENDFOR
}

void FFTReorderSimple_2793() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_split[4]), &(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_join[4]));
	ENDFOR
}

void FFTReorderSimple_2794() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_split[5]), &(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_join[5]));
	ENDFOR
}

void FFTReorderSimple_2795() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_split[6]), &(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_join[6]));
	ENDFOR
}

void FFTReorderSimple_2796() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_split[7]), &(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_join[7]));
	ENDFOR
}

void FFTReorderSimple_2797() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_split[8]), &(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_join[8]));
	ENDFOR
}

void FFTReorderSimple_2798() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_split[9]), &(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_join[9]));
	ENDFOR
}

void FFTReorderSimple_2799() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_split[10]), &(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_join[10]));
	ENDFOR
}

void FFTReorderSimple_2800() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_split[11]), &(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_join[11]));
	ENDFOR
}

void FFTReorderSimple_2801() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_split[12]), &(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_join[12]));
	ENDFOR
}

void FFTReorderSimple_2802() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_split[13]), &(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_join[13]));
	ENDFOR
}

void FFTReorderSimple_2803() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_split[14]), &(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_join[14]));
	ENDFOR
}

void FFTReorderSimple_2804() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_split[15]), &(SplitJoin107_FFTReorderSimple_Fiss_2886_2907_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2787() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin107_FFTReorderSimple_Fiss_2886_2907_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2778WEIGHTED_ROUND_ROBIN_Splitter_2787));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2788() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2788WEIGHTED_ROUND_ROBIN_Splitter_2805, pop_float(&SplitJoin107_FFTReorderSimple_Fiss_2886_2907_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2807() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin109_CombineDFT_Fiss_2887_2908_split[0]), &(SplitJoin109_CombineDFT_Fiss_2887_2908_join[0]));
	ENDFOR
}

void CombineDFT_2808() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin109_CombineDFT_Fiss_2887_2908_split[1]), &(SplitJoin109_CombineDFT_Fiss_2887_2908_join[1]));
	ENDFOR
}

void CombineDFT_2809() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin109_CombineDFT_Fiss_2887_2908_split[2]), &(SplitJoin109_CombineDFT_Fiss_2887_2908_join[2]));
	ENDFOR
}

void CombineDFT_2810() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin109_CombineDFT_Fiss_2887_2908_split[3]), &(SplitJoin109_CombineDFT_Fiss_2887_2908_join[3]));
	ENDFOR
}

void CombineDFT_2811() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin109_CombineDFT_Fiss_2887_2908_split[4]), &(SplitJoin109_CombineDFT_Fiss_2887_2908_join[4]));
	ENDFOR
}

void CombineDFT_2812() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin109_CombineDFT_Fiss_2887_2908_split[5]), &(SplitJoin109_CombineDFT_Fiss_2887_2908_join[5]));
	ENDFOR
}

void CombineDFT_2813() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin109_CombineDFT_Fiss_2887_2908_split[6]), &(SplitJoin109_CombineDFT_Fiss_2887_2908_join[6]));
	ENDFOR
}

void CombineDFT_2814() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin109_CombineDFT_Fiss_2887_2908_split[7]), &(SplitJoin109_CombineDFT_Fiss_2887_2908_join[7]));
	ENDFOR
}

void CombineDFT_2815() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin109_CombineDFT_Fiss_2887_2908_split[8]), &(SplitJoin109_CombineDFT_Fiss_2887_2908_join[8]));
	ENDFOR
}

void CombineDFT_2816() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin109_CombineDFT_Fiss_2887_2908_split[9]), &(SplitJoin109_CombineDFT_Fiss_2887_2908_join[9]));
	ENDFOR
}

void CombineDFT_2817() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin109_CombineDFT_Fiss_2887_2908_split[10]), &(SplitJoin109_CombineDFT_Fiss_2887_2908_join[10]));
	ENDFOR
}

void CombineDFT_2818() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin109_CombineDFT_Fiss_2887_2908_split[11]), &(SplitJoin109_CombineDFT_Fiss_2887_2908_join[11]));
	ENDFOR
}

void CombineDFT_2819() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin109_CombineDFT_Fiss_2887_2908_split[12]), &(SplitJoin109_CombineDFT_Fiss_2887_2908_join[12]));
	ENDFOR
}

void CombineDFT_2820() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin109_CombineDFT_Fiss_2887_2908_split[13]), &(SplitJoin109_CombineDFT_Fiss_2887_2908_join[13]));
	ENDFOR
}

void CombineDFT_2821() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin109_CombineDFT_Fiss_2887_2908_split[14]), &(SplitJoin109_CombineDFT_Fiss_2887_2908_join[14]));
	ENDFOR
}

void CombineDFT_2822() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin109_CombineDFT_Fiss_2887_2908_split[15]), &(SplitJoin109_CombineDFT_Fiss_2887_2908_join[15]));
	ENDFOR
}

void CombineDFT_2823() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin109_CombineDFT_Fiss_2887_2908_split[16]), &(SplitJoin109_CombineDFT_Fiss_2887_2908_join[16]));
	ENDFOR
}

void CombineDFT_2824() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin109_CombineDFT_Fiss_2887_2908_split[17]), &(SplitJoin109_CombineDFT_Fiss_2887_2908_join[17]));
	ENDFOR
}

void CombineDFT_2825() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin109_CombineDFT_Fiss_2887_2908_split[18]), &(SplitJoin109_CombineDFT_Fiss_2887_2908_join[18]));
	ENDFOR
}

void CombineDFT_2826() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin109_CombineDFT_Fiss_2887_2908_split[19]), &(SplitJoin109_CombineDFT_Fiss_2887_2908_join[19]));
	ENDFOR
}

void CombineDFT_2827() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin109_CombineDFT_Fiss_2887_2908_split[20]), &(SplitJoin109_CombineDFT_Fiss_2887_2908_join[20]));
	ENDFOR
}

void CombineDFT_2828() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin109_CombineDFT_Fiss_2887_2908_split[21]), &(SplitJoin109_CombineDFT_Fiss_2887_2908_join[21]));
	ENDFOR
}

void CombineDFT_2829() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin109_CombineDFT_Fiss_2887_2908_split[22]), &(SplitJoin109_CombineDFT_Fiss_2887_2908_join[22]));
	ENDFOR
}

void CombineDFT_2830() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin109_CombineDFT_Fiss_2887_2908_split[23]), &(SplitJoin109_CombineDFT_Fiss_2887_2908_join[23]));
	ENDFOR
}

void CombineDFT_2831() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin109_CombineDFT_Fiss_2887_2908_split[24]), &(SplitJoin109_CombineDFT_Fiss_2887_2908_join[24]));
	ENDFOR
}

void CombineDFT_2832() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin109_CombineDFT_Fiss_2887_2908_split[25]), &(SplitJoin109_CombineDFT_Fiss_2887_2908_join[25]));
	ENDFOR
}

void CombineDFT_2833() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin109_CombineDFT_Fiss_2887_2908_split[26]), &(SplitJoin109_CombineDFT_Fiss_2887_2908_join[26]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2805() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 27, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin109_CombineDFT_Fiss_2887_2908_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2788WEIGHTED_ROUND_ROBIN_Splitter_2805));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2806() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 27, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2806WEIGHTED_ROUND_ROBIN_Splitter_2834, pop_float(&SplitJoin109_CombineDFT_Fiss_2887_2908_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2836() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin111_CombineDFT_Fiss_2888_2909_split[0]), &(SplitJoin111_CombineDFT_Fiss_2888_2909_join[0]));
	ENDFOR
}

void CombineDFT_2837() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin111_CombineDFT_Fiss_2888_2909_split[1]), &(SplitJoin111_CombineDFT_Fiss_2888_2909_join[1]));
	ENDFOR
}

void CombineDFT_2838() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin111_CombineDFT_Fiss_2888_2909_split[2]), &(SplitJoin111_CombineDFT_Fiss_2888_2909_join[2]));
	ENDFOR
}

void CombineDFT_2839() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin111_CombineDFT_Fiss_2888_2909_split[3]), &(SplitJoin111_CombineDFT_Fiss_2888_2909_join[3]));
	ENDFOR
}

void CombineDFT_2840() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin111_CombineDFT_Fiss_2888_2909_split[4]), &(SplitJoin111_CombineDFT_Fiss_2888_2909_join[4]));
	ENDFOR
}

void CombineDFT_2841() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin111_CombineDFT_Fiss_2888_2909_split[5]), &(SplitJoin111_CombineDFT_Fiss_2888_2909_join[5]));
	ENDFOR
}

void CombineDFT_2842() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin111_CombineDFT_Fiss_2888_2909_split[6]), &(SplitJoin111_CombineDFT_Fiss_2888_2909_join[6]));
	ENDFOR
}

void CombineDFT_2843() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin111_CombineDFT_Fiss_2888_2909_split[7]), &(SplitJoin111_CombineDFT_Fiss_2888_2909_join[7]));
	ENDFOR
}

void CombineDFT_2844() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin111_CombineDFT_Fiss_2888_2909_split[8]), &(SplitJoin111_CombineDFT_Fiss_2888_2909_join[8]));
	ENDFOR
}

void CombineDFT_2845() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin111_CombineDFT_Fiss_2888_2909_split[9]), &(SplitJoin111_CombineDFT_Fiss_2888_2909_join[9]));
	ENDFOR
}

void CombineDFT_2846() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin111_CombineDFT_Fiss_2888_2909_split[10]), &(SplitJoin111_CombineDFT_Fiss_2888_2909_join[10]));
	ENDFOR
}

void CombineDFT_2847() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin111_CombineDFT_Fiss_2888_2909_split[11]), &(SplitJoin111_CombineDFT_Fiss_2888_2909_join[11]));
	ENDFOR
}

void CombineDFT_2848() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin111_CombineDFT_Fiss_2888_2909_split[12]), &(SplitJoin111_CombineDFT_Fiss_2888_2909_join[12]));
	ENDFOR
}

void CombineDFT_2849() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin111_CombineDFT_Fiss_2888_2909_split[13]), &(SplitJoin111_CombineDFT_Fiss_2888_2909_join[13]));
	ENDFOR
}

void CombineDFT_2850() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin111_CombineDFT_Fiss_2888_2909_split[14]), &(SplitJoin111_CombineDFT_Fiss_2888_2909_join[14]));
	ENDFOR
}

void CombineDFT_2851() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin111_CombineDFT_Fiss_2888_2909_split[15]), &(SplitJoin111_CombineDFT_Fiss_2888_2909_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2834() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin111_CombineDFT_Fiss_2888_2909_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2806WEIGHTED_ROUND_ROBIN_Splitter_2834));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2835() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2835WEIGHTED_ROUND_ROBIN_Splitter_2852, pop_float(&SplitJoin111_CombineDFT_Fiss_2888_2909_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2854() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin113_CombineDFT_Fiss_2889_2910_split[0]), &(SplitJoin113_CombineDFT_Fiss_2889_2910_join[0]));
	ENDFOR
}

void CombineDFT_2855() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin113_CombineDFT_Fiss_2889_2910_split[1]), &(SplitJoin113_CombineDFT_Fiss_2889_2910_join[1]));
	ENDFOR
}

void CombineDFT_2856() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin113_CombineDFT_Fiss_2889_2910_split[2]), &(SplitJoin113_CombineDFT_Fiss_2889_2910_join[2]));
	ENDFOR
}

void CombineDFT_2857() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin113_CombineDFT_Fiss_2889_2910_split[3]), &(SplitJoin113_CombineDFT_Fiss_2889_2910_join[3]));
	ENDFOR
}

void CombineDFT_2858() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin113_CombineDFT_Fiss_2889_2910_split[4]), &(SplitJoin113_CombineDFT_Fiss_2889_2910_join[4]));
	ENDFOR
}

void CombineDFT_2859() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin113_CombineDFT_Fiss_2889_2910_split[5]), &(SplitJoin113_CombineDFT_Fiss_2889_2910_join[5]));
	ENDFOR
}

void CombineDFT_2860() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin113_CombineDFT_Fiss_2889_2910_split[6]), &(SplitJoin113_CombineDFT_Fiss_2889_2910_join[6]));
	ENDFOR
}

void CombineDFT_2861() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin113_CombineDFT_Fiss_2889_2910_split[7]), &(SplitJoin113_CombineDFT_Fiss_2889_2910_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2852() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin113_CombineDFT_Fiss_2889_2910_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2835WEIGHTED_ROUND_ROBIN_Splitter_2852));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2853() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2853WEIGHTED_ROUND_ROBIN_Splitter_2862, pop_float(&SplitJoin113_CombineDFT_Fiss_2889_2910_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2864() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin115_CombineDFT_Fiss_2890_2911_split[0]), &(SplitJoin115_CombineDFT_Fiss_2890_2911_join[0]));
	ENDFOR
}

void CombineDFT_2865() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin115_CombineDFT_Fiss_2890_2911_split[1]), &(SplitJoin115_CombineDFT_Fiss_2890_2911_join[1]));
	ENDFOR
}

void CombineDFT_2866() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin115_CombineDFT_Fiss_2890_2911_split[2]), &(SplitJoin115_CombineDFT_Fiss_2890_2911_join[2]));
	ENDFOR
}

void CombineDFT_2867() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin115_CombineDFT_Fiss_2890_2911_split[3]), &(SplitJoin115_CombineDFT_Fiss_2890_2911_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2862() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin115_CombineDFT_Fiss_2890_2911_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2853WEIGHTED_ROUND_ROBIN_Splitter_2862));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2863() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2863WEIGHTED_ROUND_ROBIN_Splitter_2868, pop_float(&SplitJoin115_CombineDFT_Fiss_2890_2911_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2870() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin117_CombineDFT_Fiss_2891_2912_split[0]), &(SplitJoin117_CombineDFT_Fiss_2891_2912_join[0]));
	ENDFOR
}

void CombineDFT_2871() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(SplitJoin117_CombineDFT_Fiss_2891_2912_split[1]), &(SplitJoin117_CombineDFT_Fiss_2891_2912_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2868() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin117_CombineDFT_Fiss_2891_2912_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2863WEIGHTED_ROUND_ROBIN_Splitter_2868));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin117_CombineDFT_Fiss_2891_2912_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2863WEIGHTED_ROUND_ROBIN_Splitter_2868));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2869() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2869CombineDFT_2648, pop_float(&SplitJoin117_CombineDFT_Fiss_2891_2912_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2869CombineDFT_2648, pop_float(&SplitJoin117_CombineDFT_Fiss_2891_2912_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_2648() {
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_2869CombineDFT_2648), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_2616_2652_2873_2894_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2650() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_2616_2652_2873_2894_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2659WEIGHTED_ROUND_ROBIN_Splitter_2650));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_2616_2652_2873_2894_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_2659WEIGHTED_ROUND_ROBIN_Splitter_2650));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2651() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2651FloatPrinter_2649, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_2616_2652_2873_2894_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_2651FloatPrinter_2649, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_2616_2652_2873_2894_join[1]));
		ENDFOR
	ENDFOR
}}

void FloatPrinter(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void FloatPrinter_2649() {
	FOR(uint32_t, __iter_steady_, 0, <, 6912, __iter_steady_++)
		FloatPrinter(&(WEIGHTED_ROUND_ROBIN_Joiner_2651FloatPrinter_2649));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2683WEIGHTED_ROUND_ROBIN_Splitter_2700);
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_2880_2901_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 16, __iter_init_1_++)
		init_buffer_float(&SplitJoin111_CombineDFT_Fiss_2888_2909_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_2876_2897_split[__iter_init_2_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2869CombineDFT_2648);
	FOR(int, __iter_init_3_, 0, <, 27, __iter_init_3_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_2878_2899_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 2, __iter_init_4_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_2882_2903_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 16, __iter_init_5_++)
		init_buffer_float(&SplitJoin107_FFTReorderSimple_Fiss_2886_2907_split[__iter_init_5_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2667WEIGHTED_ROUND_ROBIN_Splitter_2672);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2730WEIGHTED_ROUND_ROBIN_Splitter_2747);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2768WEIGHTED_ROUND_ROBIN_Splitter_2771);
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_float(&SplitJoin105_FFTReorderSimple_Fiss_2885_2906_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 4, __iter_init_7_++)
		init_buffer_float(&SplitJoin103_FFTReorderSimple_Fiss_2884_2905_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 4, __iter_init_8_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_2875_2896_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 27, __iter_init_9_++)
		init_buffer_float(&SplitJoin109_CombineDFT_Fiss_2887_2908_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 16, __iter_init_10_++)
		init_buffer_float(&SplitJoin111_CombineDFT_Fiss_2888_2909_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 4, __iter_init_11_++)
		init_buffer_float(&SplitJoin115_CombineDFT_Fiss_2890_2911_join[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 2, __iter_init_12_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_2872_2893_split[__iter_init_12_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2659WEIGHTED_ROUND_ROBIN_Splitter_2650);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2863WEIGHTED_ROUND_ROBIN_Splitter_2868);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2748WEIGHTED_ROUND_ROBIN_Splitter_2757);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2806WEIGHTED_ROUND_ROBIN_Splitter_2834);
	init_buffer_float(&FFTReorderSimple_2638WEIGHTED_ROUND_ROBIN_Splitter_2767);
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_float(&SplitJoin101_FFTReorderSimple_Fiss_2883_2904_join[__iter_init_13_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2673WEIGHTED_ROUND_ROBIN_Splitter_2682);
	FOR(int, __iter_init_14_, 0, <, 8, __iter_init_14_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_2880_2901_split[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 16, __iter_init_15_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_2877_2898_join[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 4, __iter_init_16_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_2875_2896_join[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 4, __iter_init_17_++)
		init_buffer_float(&SplitJoin103_FFTReorderSimple_Fiss_2884_2905_split[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 2, __iter_init_18_++)
		init_buffer_float(&SplitJoin117_CombineDFT_Fiss_2891_2912_join[__iter_init_18_]);
	ENDFOR
	FOR(int, __iter_init_19_, 0, <, 16, __iter_init_19_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_2877_2898_split[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 4, __iter_init_20_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_2881_2902_split[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 2, __iter_init_21_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_2882_2903_join[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 2, __iter_init_22_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_2616_2652_2873_2894_join[__iter_init_22_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2663WEIGHTED_ROUND_ROBIN_Splitter_2666);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2758WEIGHTED_ROUND_ROBIN_Splitter_2763);
	FOR(int, __iter_init_23_, 0, <, 8, __iter_init_23_++)
		init_buffer_float(&SplitJoin113_CombineDFT_Fiss_2889_2910_split[__iter_init_23_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2778WEIGHTED_ROUND_ROBIN_Splitter_2787);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2835WEIGHTED_ROUND_ROBIN_Splitter_2852);
	FOR(int, __iter_init_24_, 0, <, 16, __iter_init_24_++)
		init_buffer_float(&SplitJoin107_FFTReorderSimple_Fiss_2886_2907_join[__iter_init_24_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2772WEIGHTED_ROUND_ROBIN_Splitter_2777);
	FOR(int, __iter_init_25_, 0, <, 2, __iter_init_25_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_2874_2895_join[__iter_init_25_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2764CombineDFT_2637);
	FOR(int, __iter_init_26_, 0, <, 2, __iter_init_26_++)
		init_buffer_float(&SplitJoin117_CombineDFT_Fiss_2891_2912_split[__iter_init_26_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2701WEIGHTED_ROUND_ROBIN_Splitter_2729);
	FOR(int, __iter_init_27_, 0, <, 2, __iter_init_27_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_2616_2652_2873_2894_split[__iter_init_27_]);
	ENDFOR
	FOR(int, __iter_init_28_, 0, <, 27, __iter_init_28_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_2878_2899_split[__iter_init_28_]);
	ENDFOR
	FOR(int, __iter_init_29_, 0, <, 16, __iter_init_29_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_2879_2900_join[__iter_init_29_]);
	ENDFOR
	init_buffer_float(&FFTReorderSimple_2627WEIGHTED_ROUND_ROBIN_Splitter_2662);
	FOR(int, __iter_init_30_, 0, <, 27, __iter_init_30_++)
		init_buffer_float(&SplitJoin109_CombineDFT_Fiss_2887_2908_split[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 4, __iter_init_31_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_2881_2902_join[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 16, __iter_init_32_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_2879_2900_split[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 2, __iter_init_33_++)
		init_buffer_float(&SplitJoin101_FFTReorderSimple_Fiss_2883_2904_split[__iter_init_33_]);
	ENDFOR
	FOR(int, __iter_init_34_, 0, <, 8, __iter_init_34_++)
		init_buffer_float(&SplitJoin105_FFTReorderSimple_Fiss_2885_2906_join[__iter_init_34_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2651FloatPrinter_2649);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2853WEIGHTED_ROUND_ROBIN_Splitter_2862);
	FOR(int, __iter_init_35_, 0, <, 2, __iter_init_35_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_2874_2895_split[__iter_init_35_]);
	ENDFOR
	FOR(int, __iter_init_36_, 0, <, 8, __iter_init_36_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_2876_2897_join[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 4, __iter_init_37_++)
		init_buffer_float(&SplitJoin115_CombineDFT_Fiss_2890_2911_split[__iter_init_37_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_2788WEIGHTED_ROUND_ROBIN_Splitter_2805);
	FOR(int, __iter_init_38_, 0, <, 8, __iter_init_38_++)
		init_buffer_float(&SplitJoin113_CombineDFT_Fiss_2889_2910_join[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 2, __iter_init_39_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_2872_2893_join[__iter_init_39_]);
	ENDFOR
// --- init: CombineDFT_2702
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2702_s.w[i] = real ; 
		CombineDFT_2702_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2703
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2703_s.w[i] = real ; 
		CombineDFT_2703_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2704
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2704_s.w[i] = real ; 
		CombineDFT_2704_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2705
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2705_s.w[i] = real ; 
		CombineDFT_2705_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2706
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2706_s.w[i] = real ; 
		CombineDFT_2706_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2707
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2707_s.w[i] = real ; 
		CombineDFT_2707_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2708
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2708_s.w[i] = real ; 
		CombineDFT_2708_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2709
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2709_s.w[i] = real ; 
		CombineDFT_2709_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2710
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2710_s.w[i] = real ; 
		CombineDFT_2710_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2711
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2711_s.w[i] = real ; 
		CombineDFT_2711_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2712
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2712_s.w[i] = real ; 
		CombineDFT_2712_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2713
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2713_s.w[i] = real ; 
		CombineDFT_2713_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2714
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2714_s.w[i] = real ; 
		CombineDFT_2714_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2715
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2715_s.w[i] = real ; 
		CombineDFT_2715_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2716
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2716_s.w[i] = real ; 
		CombineDFT_2716_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2717
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2717_s.w[i] = real ; 
		CombineDFT_2717_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2718
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2718_s.w[i] = real ; 
		CombineDFT_2718_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2719
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2719_s.w[i] = real ; 
		CombineDFT_2719_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2720
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2720_s.w[i] = real ; 
		CombineDFT_2720_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2721
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2721_s.w[i] = real ; 
		CombineDFT_2721_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2722
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2722_s.w[i] = real ; 
		CombineDFT_2722_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2723
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2723_s.w[i] = real ; 
		CombineDFT_2723_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2724
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2724_s.w[i] = real ; 
		CombineDFT_2724_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2725
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2725_s.w[i] = real ; 
		CombineDFT_2725_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2726
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2726_s.w[i] = real ; 
		CombineDFT_2726_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2727
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2727_s.w[i] = real ; 
		CombineDFT_2727_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2728
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2728_s.w[i] = real ; 
		CombineDFT_2728_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2731
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2731_s.w[i] = real ; 
		CombineDFT_2731_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2732
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2732_s.w[i] = real ; 
		CombineDFT_2732_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2733
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2733_s.w[i] = real ; 
		CombineDFT_2733_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2734
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2734_s.w[i] = real ; 
		CombineDFT_2734_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2735
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2735_s.w[i] = real ; 
		CombineDFT_2735_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2736
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2736_s.w[i] = real ; 
		CombineDFT_2736_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2737
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2737_s.w[i] = real ; 
		CombineDFT_2737_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2738
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2738_s.w[i] = real ; 
		CombineDFT_2738_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2739
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2739_s.w[i] = real ; 
		CombineDFT_2739_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2740
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2740_s.w[i] = real ; 
		CombineDFT_2740_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2741
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2741_s.w[i] = real ; 
		CombineDFT_2741_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2742
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2742_s.w[i] = real ; 
		CombineDFT_2742_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2743
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2743_s.w[i] = real ; 
		CombineDFT_2743_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2744
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2744_s.w[i] = real ; 
		CombineDFT_2744_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2745
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2745_s.w[i] = real ; 
		CombineDFT_2745_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2746
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2746_s.w[i] = real ; 
		CombineDFT_2746_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2749
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2749_s.w[i] = real ; 
		CombineDFT_2749_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2750
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2750_s.w[i] = real ; 
		CombineDFT_2750_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2751
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2751_s.w[i] = real ; 
		CombineDFT_2751_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2752
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2752_s.w[i] = real ; 
		CombineDFT_2752_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2753
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2753_s.w[i] = real ; 
		CombineDFT_2753_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2754
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2754_s.w[i] = real ; 
		CombineDFT_2754_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2755
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2755_s.w[i] = real ; 
		CombineDFT_2755_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2756
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2756_s.w[i] = real ; 
		CombineDFT_2756_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2759
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_2759_s.w[i] = real ; 
		CombineDFT_2759_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2760
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_2760_s.w[i] = real ; 
		CombineDFT_2760_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2761
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_2761_s.w[i] = real ; 
		CombineDFT_2761_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2762
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_2762_s.w[i] = real ; 
		CombineDFT_2762_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2765
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_2765_s.w[i] = real ; 
		CombineDFT_2765_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2766
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_2766_s.w[i] = real ; 
		CombineDFT_2766_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2637
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_2637_s.w[i] = real ; 
		CombineDFT_2637_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2807
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2807_s.w[i] = real ; 
		CombineDFT_2807_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2808
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2808_s.w[i] = real ; 
		CombineDFT_2808_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2809
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2809_s.w[i] = real ; 
		CombineDFT_2809_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2810
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2810_s.w[i] = real ; 
		CombineDFT_2810_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2811
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2811_s.w[i] = real ; 
		CombineDFT_2811_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2812
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2812_s.w[i] = real ; 
		CombineDFT_2812_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2813
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2813_s.w[i] = real ; 
		CombineDFT_2813_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2814
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2814_s.w[i] = real ; 
		CombineDFT_2814_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2815
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2815_s.w[i] = real ; 
		CombineDFT_2815_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2816
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2816_s.w[i] = real ; 
		CombineDFT_2816_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2817
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2817_s.w[i] = real ; 
		CombineDFT_2817_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2818
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2818_s.w[i] = real ; 
		CombineDFT_2818_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2819
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2819_s.w[i] = real ; 
		CombineDFT_2819_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2820
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2820_s.w[i] = real ; 
		CombineDFT_2820_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2821
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2821_s.w[i] = real ; 
		CombineDFT_2821_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2822
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2822_s.w[i] = real ; 
		CombineDFT_2822_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2823
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2823_s.w[i] = real ; 
		CombineDFT_2823_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2824
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2824_s.w[i] = real ; 
		CombineDFT_2824_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2825
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2825_s.w[i] = real ; 
		CombineDFT_2825_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2826
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2826_s.w[i] = real ; 
		CombineDFT_2826_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2827
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2827_s.w[i] = real ; 
		CombineDFT_2827_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2828
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2828_s.w[i] = real ; 
		CombineDFT_2828_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2829
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2829_s.w[i] = real ; 
		CombineDFT_2829_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2830
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2830_s.w[i] = real ; 
		CombineDFT_2830_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2831
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2831_s.w[i] = real ; 
		CombineDFT_2831_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2832
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2832_s.w[i] = real ; 
		CombineDFT_2832_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2833
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_2833_s.w[i] = real ; 
		CombineDFT_2833_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2836
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2836_s.w[i] = real ; 
		CombineDFT_2836_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2837
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2837_s.w[i] = real ; 
		CombineDFT_2837_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2838
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2838_s.w[i] = real ; 
		CombineDFT_2838_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2839
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2839_s.w[i] = real ; 
		CombineDFT_2839_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2840
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2840_s.w[i] = real ; 
		CombineDFT_2840_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2841
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2841_s.w[i] = real ; 
		CombineDFT_2841_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2842
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2842_s.w[i] = real ; 
		CombineDFT_2842_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2843
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2843_s.w[i] = real ; 
		CombineDFT_2843_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2844
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2844_s.w[i] = real ; 
		CombineDFT_2844_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2845
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2845_s.w[i] = real ; 
		CombineDFT_2845_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2846
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2846_s.w[i] = real ; 
		CombineDFT_2846_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2847
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2847_s.w[i] = real ; 
		CombineDFT_2847_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2848
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2848_s.w[i] = real ; 
		CombineDFT_2848_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2849
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2849_s.w[i] = real ; 
		CombineDFT_2849_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2850
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2850_s.w[i] = real ; 
		CombineDFT_2850_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2851
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_2851_s.w[i] = real ; 
		CombineDFT_2851_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2854
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2854_s.w[i] = real ; 
		CombineDFT_2854_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2855
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2855_s.w[i] = real ; 
		CombineDFT_2855_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2856
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2856_s.w[i] = real ; 
		CombineDFT_2856_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2857
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2857_s.w[i] = real ; 
		CombineDFT_2857_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2858
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2858_s.w[i] = real ; 
		CombineDFT_2858_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2859
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2859_s.w[i] = real ; 
		CombineDFT_2859_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2860
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2860_s.w[i] = real ; 
		CombineDFT_2860_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2861
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_2861_s.w[i] = real ; 
		CombineDFT_2861_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2864
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_2864_s.w[i] = real ; 
		CombineDFT_2864_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2865
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_2865_s.w[i] = real ; 
		CombineDFT_2865_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2866
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_2866_s.w[i] = real ; 
		CombineDFT_2866_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2867
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_2867_s.w[i] = real ; 
		CombineDFT_2867_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2870
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_2870_s.w[i] = real ; 
		CombineDFT_2870_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2871
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_2871_s.w[i] = real ; 
		CombineDFT_2871_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_2648
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_2648_s.w[i] = real ; 
		CombineDFT_2648_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_2658();
			FFTTestSource_2660();
			FFTTestSource_2661();
		WEIGHTED_ROUND_ROBIN_Joiner_2659();
		WEIGHTED_ROUND_ROBIN_Splitter_2650();
			FFTReorderSimple_2627();
			WEIGHTED_ROUND_ROBIN_Splitter_2662();
				FFTReorderSimple_2664();
				FFTReorderSimple_2665();
			WEIGHTED_ROUND_ROBIN_Joiner_2663();
			WEIGHTED_ROUND_ROBIN_Splitter_2666();
				FFTReorderSimple_2668();
				FFTReorderSimple_2669();
				FFTReorderSimple_2670();
				FFTReorderSimple_2671();
			WEIGHTED_ROUND_ROBIN_Joiner_2667();
			WEIGHTED_ROUND_ROBIN_Splitter_2672();
				FFTReorderSimple_2674();
				FFTReorderSimple_2675();
				FFTReorderSimple_2676();
				FFTReorderSimple_2677();
				FFTReorderSimple_2678();
				FFTReorderSimple_2679();
				FFTReorderSimple_2680();
				FFTReorderSimple_2681();
			WEIGHTED_ROUND_ROBIN_Joiner_2673();
			WEIGHTED_ROUND_ROBIN_Splitter_2682();
				FFTReorderSimple_2684();
				FFTReorderSimple_2685();
				FFTReorderSimple_2686();
				FFTReorderSimple_2687();
				FFTReorderSimple_2688();
				FFTReorderSimple_2689();
				FFTReorderSimple_2690();
				FFTReorderSimple_2691();
				FFTReorderSimple_2692();
				FFTReorderSimple_2693();
				FFTReorderSimple_2694();
				FFTReorderSimple_2695();
				FFTReorderSimple_2696();
				FFTReorderSimple_2697();
				FFTReorderSimple_2698();
				FFTReorderSimple_2699();
			WEIGHTED_ROUND_ROBIN_Joiner_2683();
			WEIGHTED_ROUND_ROBIN_Splitter_2700();
				CombineDFT_2702();
				CombineDFT_2703();
				CombineDFT_2704();
				CombineDFT_2705();
				CombineDFT_2706();
				CombineDFT_2707();
				CombineDFT_2708();
				CombineDFT_2709();
				CombineDFT_2710();
				CombineDFT_2711();
				CombineDFT_2712();
				CombineDFT_2713();
				CombineDFT_2714();
				CombineDFT_2715();
				CombineDFT_2716();
				CombineDFT_2717();
				CombineDFT_2718();
				CombineDFT_2719();
				CombineDFT_2720();
				CombineDFT_2721();
				CombineDFT_2722();
				CombineDFT_2723();
				CombineDFT_2724();
				CombineDFT_2725();
				CombineDFT_2726();
				CombineDFT_2727();
				CombineDFT_2728();
			WEIGHTED_ROUND_ROBIN_Joiner_2701();
			WEIGHTED_ROUND_ROBIN_Splitter_2729();
				CombineDFT_2731();
				CombineDFT_2732();
				CombineDFT_2733();
				CombineDFT_2734();
				CombineDFT_2735();
				CombineDFT_2736();
				CombineDFT_2737();
				CombineDFT_2738();
				CombineDFT_2739();
				CombineDFT_2740();
				CombineDFT_2741();
				CombineDFT_2742();
				CombineDFT_2743();
				CombineDFT_2744();
				CombineDFT_2745();
				CombineDFT_2746();
			WEIGHTED_ROUND_ROBIN_Joiner_2730();
			WEIGHTED_ROUND_ROBIN_Splitter_2747();
				CombineDFT_2749();
				CombineDFT_2750();
				CombineDFT_2751();
				CombineDFT_2752();
				CombineDFT_2753();
				CombineDFT_2754();
				CombineDFT_2755();
				CombineDFT_2756();
			WEIGHTED_ROUND_ROBIN_Joiner_2748();
			WEIGHTED_ROUND_ROBIN_Splitter_2757();
				CombineDFT_2759();
				CombineDFT_2760();
				CombineDFT_2761();
				CombineDFT_2762();
			WEIGHTED_ROUND_ROBIN_Joiner_2758();
			WEIGHTED_ROUND_ROBIN_Splitter_2763();
				CombineDFT_2765();
				CombineDFT_2766();
			WEIGHTED_ROUND_ROBIN_Joiner_2764();
			CombineDFT_2637();
			FFTReorderSimple_2638();
			WEIGHTED_ROUND_ROBIN_Splitter_2767();
				FFTReorderSimple_2769();
				FFTReorderSimple_2770();
			WEIGHTED_ROUND_ROBIN_Joiner_2768();
			WEIGHTED_ROUND_ROBIN_Splitter_2771();
				FFTReorderSimple_2773();
				FFTReorderSimple_2774();
				FFTReorderSimple_2775();
				FFTReorderSimple_2776();
			WEIGHTED_ROUND_ROBIN_Joiner_2772();
			WEIGHTED_ROUND_ROBIN_Splitter_2777();
				FFTReorderSimple_2779();
				FFTReorderSimple_2780();
				FFTReorderSimple_2781();
				FFTReorderSimple_2782();
				FFTReorderSimple_2783();
				FFTReorderSimple_2784();
				FFTReorderSimple_2785();
				FFTReorderSimple_2786();
			WEIGHTED_ROUND_ROBIN_Joiner_2778();
			WEIGHTED_ROUND_ROBIN_Splitter_2787();
				FFTReorderSimple_2789();
				FFTReorderSimple_2790();
				FFTReorderSimple_2791();
				FFTReorderSimple_2792();
				FFTReorderSimple_2793();
				FFTReorderSimple_2794();
				FFTReorderSimple_2795();
				FFTReorderSimple_2796();
				FFTReorderSimple_2797();
				FFTReorderSimple_2798();
				FFTReorderSimple_2799();
				FFTReorderSimple_2800();
				FFTReorderSimple_2801();
				FFTReorderSimple_2802();
				FFTReorderSimple_2803();
				FFTReorderSimple_2804();
			WEIGHTED_ROUND_ROBIN_Joiner_2788();
			WEIGHTED_ROUND_ROBIN_Splitter_2805();
				CombineDFT_2807();
				CombineDFT_2808();
				CombineDFT_2809();
				CombineDFT_2810();
				CombineDFT_2811();
				CombineDFT_2812();
				CombineDFT_2813();
				CombineDFT_2814();
				CombineDFT_2815();
				CombineDFT_2816();
				CombineDFT_2817();
				CombineDFT_2818();
				CombineDFT_2819();
				CombineDFT_2820();
				CombineDFT_2821();
				CombineDFT_2822();
				CombineDFT_2823();
				CombineDFT_2824();
				CombineDFT_2825();
				CombineDFT_2826();
				CombineDFT_2827();
				CombineDFT_2828();
				CombineDFT_2829();
				CombineDFT_2830();
				CombineDFT_2831();
				CombineDFT_2832();
				CombineDFT_2833();
			WEIGHTED_ROUND_ROBIN_Joiner_2806();
			WEIGHTED_ROUND_ROBIN_Splitter_2834();
				CombineDFT_2836();
				CombineDFT_2837();
				CombineDFT_2838();
				CombineDFT_2839();
				CombineDFT_2840();
				CombineDFT_2841();
				CombineDFT_2842();
				CombineDFT_2843();
				CombineDFT_2844();
				CombineDFT_2845();
				CombineDFT_2846();
				CombineDFT_2847();
				CombineDFT_2848();
				CombineDFT_2849();
				CombineDFT_2850();
				CombineDFT_2851();
			WEIGHTED_ROUND_ROBIN_Joiner_2835();
			WEIGHTED_ROUND_ROBIN_Splitter_2852();
				CombineDFT_2854();
				CombineDFT_2855();
				CombineDFT_2856();
				CombineDFT_2857();
				CombineDFT_2858();
				CombineDFT_2859();
				CombineDFT_2860();
				CombineDFT_2861();
			WEIGHTED_ROUND_ROBIN_Joiner_2853();
			WEIGHTED_ROUND_ROBIN_Splitter_2862();
				CombineDFT_2864();
				CombineDFT_2865();
				CombineDFT_2866();
				CombineDFT_2867();
			WEIGHTED_ROUND_ROBIN_Joiner_2863();
			WEIGHTED_ROUND_ROBIN_Splitter_2868();
				CombineDFT_2870();
				CombineDFT_2871();
			WEIGHTED_ROUND_ROBIN_Joiner_2869();
			CombineDFT_2648();
		WEIGHTED_ROUND_ROBIN_Joiner_2651();
		FloatPrinter_2649();
	ENDFOR
	return EXIT_SUCCESS;
}
