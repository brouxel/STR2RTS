#include "PEG4-FFT2.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12170WEIGHTED_ROUND_ROBIN_Splitter_12175;
buffer_float_t SplitJoin54_CombineDFT_Fiss_12206_12227_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12102WEIGHTED_ROUND_ROBIN_Splitter_12107;
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_12194_12215_join[4];
buffer_float_t SplitJoin46_FFTReorderSimple_Fiss_12202_12223_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12126WEIGHTED_ROUND_ROBIN_Splitter_12131;
buffer_float_t SplitJoin12_CombineDFT_Fiss_12197_12218_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12164WEIGHTED_ROUND_ROBIN_Splitter_12169;
buffer_float_t SplitJoin0_FFTTestSource_Fiss_12191_12212_join[2];
buffer_float_t SplitJoin48_FFTReorderSimple_Fiss_12203_12224_split[4];
buffer_float_t SplitJoin58_CombineDFT_Fiss_12208_12229_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12188CombineDFT_12077;
buffer_float_t SplitJoin0_FFTTestSource_Fiss_12191_12212_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12146WEIGHTED_ROUND_ROBIN_Splitter_12151;
buffer_float_t SplitJoin60_CombineDFT_Fiss_12209_12230_join[4];
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_12196_12217_join[4];
buffer_float_t SplitJoin16_CombineDFT_Fiss_12199_12220_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12132WEIGHTED_ROUND_ROBIN_Splitter_12137;
buffer_float_t SplitJoin58_CombineDFT_Fiss_12208_12229_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12138CombineDFT_12066;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12152WEIGHTED_ROUND_ROBIN_Splitter_12157;
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_12193_12214_join[2];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_12045_12081_12192_12213_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12108WEIGHTED_ROUND_ROBIN_Splitter_12113;
buffer_float_t FFTReorderSimple_12056WEIGHTED_ROUND_ROBIN_Splitter_12091;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12120WEIGHTED_ROUND_ROBIN_Splitter_12125;
buffer_float_t SplitJoin62_CombineDFT_Fiss_12210_12231_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12092WEIGHTED_ROUND_ROBIN_Splitter_12095;
buffer_float_t SplitJoin52_FFTReorderSimple_Fiss_12205_12226_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12182WEIGHTED_ROUND_ROBIN_Splitter_12187;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12080FloatPrinter_12078;
buffer_float_t SplitJoin12_CombineDFT_Fiss_12197_12218_join[4];
buffer_float_t SplitJoin18_CombineDFT_Fiss_12200_12221_join[4];
buffer_float_t FFTReorderSimple_12067WEIGHTED_ROUND_ROBIN_Splitter_12141;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12114WEIGHTED_ROUND_ROBIN_Splitter_12119;
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_12196_12217_split[4];
buffer_float_t SplitJoin52_FFTReorderSimple_Fiss_12205_12226_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12158WEIGHTED_ROUND_ROBIN_Splitter_12163;
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_12195_12216_split[4];
buffer_float_t SplitJoin16_CombineDFT_Fiss_12199_12220_split[4];
buffer_float_t SplitJoin20_CombineDFT_Fiss_12201_12222_join[2];
buffer_float_t SplitJoin48_FFTReorderSimple_Fiss_12203_12224_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12096WEIGHTED_ROUND_ROBIN_Splitter_12101;
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_12195_12216_join[4];
buffer_float_t SplitJoin54_CombineDFT_Fiss_12206_12227_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12142WEIGHTED_ROUND_ROBIN_Splitter_12145;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12176WEIGHTED_ROUND_ROBIN_Splitter_12181;
buffer_float_t SplitJoin14_CombineDFT_Fiss_12198_12219_join[4];
buffer_float_t SplitJoin46_FFTReorderSimple_Fiss_12202_12223_join[2];
buffer_float_t SplitJoin60_CombineDFT_Fiss_12209_12230_split[4];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_12045_12081_12192_12213_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12088WEIGHTED_ROUND_ROBIN_Splitter_12079;
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_12193_12214_split[2];
buffer_float_t SplitJoin62_CombineDFT_Fiss_12210_12231_join[2];
buffer_float_t SplitJoin18_CombineDFT_Fiss_12200_12221_split[4];
buffer_float_t SplitJoin56_CombineDFT_Fiss_12207_12228_join[4];
buffer_float_t SplitJoin56_CombineDFT_Fiss_12207_12228_split[4];
buffer_float_t SplitJoin50_FFTReorderSimple_Fiss_12204_12225_join[4];
buffer_float_t SplitJoin20_CombineDFT_Fiss_12201_12222_split[2];
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_12194_12215_split[4];
buffer_float_t SplitJoin50_FFTReorderSimple_Fiss_12204_12225_split[4];
buffer_float_t SplitJoin14_CombineDFT_Fiss_12198_12219_split[4];


CombineDFT_12115_t CombineDFT_12115_s;
CombineDFT_12115_t CombineDFT_12116_s;
CombineDFT_12115_t CombineDFT_12117_s;
CombineDFT_12115_t CombineDFT_12118_s;
CombineDFT_12121_t CombineDFT_12121_s;
CombineDFT_12121_t CombineDFT_12122_s;
CombineDFT_12121_t CombineDFT_12123_s;
CombineDFT_12121_t CombineDFT_12124_s;
CombineDFT_12127_t CombineDFT_12127_s;
CombineDFT_12127_t CombineDFT_12128_s;
CombineDFT_12127_t CombineDFT_12129_s;
CombineDFT_12127_t CombineDFT_12130_s;
CombineDFT_12133_t CombineDFT_12133_s;
CombineDFT_12133_t CombineDFT_12134_s;
CombineDFT_12133_t CombineDFT_12135_s;
CombineDFT_12133_t CombineDFT_12136_s;
CombineDFT_12139_t CombineDFT_12139_s;
CombineDFT_12139_t CombineDFT_12140_s;
CombineDFT_12066_t CombineDFT_12066_s;
CombineDFT_12115_t CombineDFT_12165_s;
CombineDFT_12115_t CombineDFT_12166_s;
CombineDFT_12115_t CombineDFT_12167_s;
CombineDFT_12115_t CombineDFT_12168_s;
CombineDFT_12121_t CombineDFT_12171_s;
CombineDFT_12121_t CombineDFT_12172_s;
CombineDFT_12121_t CombineDFT_12173_s;
CombineDFT_12121_t CombineDFT_12174_s;
CombineDFT_12127_t CombineDFT_12177_s;
CombineDFT_12127_t CombineDFT_12178_s;
CombineDFT_12127_t CombineDFT_12179_s;
CombineDFT_12127_t CombineDFT_12180_s;
CombineDFT_12133_t CombineDFT_12183_s;
CombineDFT_12133_t CombineDFT_12184_s;
CombineDFT_12133_t CombineDFT_12185_s;
CombineDFT_12133_t CombineDFT_12186_s;
CombineDFT_12139_t CombineDFT_12189_s;
CombineDFT_12139_t CombineDFT_12190_s;
CombineDFT_12066_t CombineDFT_12077_s;

void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout) {
	push_float(&(*chanout), 0.0) ; 
	push_float(&(*chanout), 0.0) ; 
	push_float(&(*chanout), 1.0) ; 
	push_float(&(*chanout), 0.0) ; 
	FOR(int, i, 0,  < , 124, i++) {
		push_float(&(*chanout), 0.0) ; 
	}
	ENDFOR
}


void FFTTestSource_12089() {
	FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_12191_12212_split[0]), &(SplitJoin0_FFTTestSource_Fiss_12191_12212_join[0]));
}

void FFTTestSource_12090() {
	FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_12191_12212_split[1]), &(SplitJoin0_FFTTestSource_Fiss_12191_12212_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_12087() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_12088() {
	FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12088WEIGHTED_ROUND_ROBIN_Splitter_12079, pop_float(&SplitJoin0_FFTTestSource_Fiss_12191_12212_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12088WEIGHTED_ROUND_ROBIN_Splitter_12079, pop_float(&SplitJoin0_FFTTestSource_Fiss_12191_12212_join[1]));
	ENDFOR
}

void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout) {
	FOR(int, i, 0,  < , 128, i = (i + 4)) {
		push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
		push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 128, i = (i + 4)) {
		push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
		push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 64, i++) {
		pop_float(&(*chanin)) ; 
		pop_float(&(*chanin)) ; 
	}
	ENDFOR
}


void FFTReorderSimple_12056() {
	FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_12045_12081_12192_12213_split[0]), &(FFTReorderSimple_12056WEIGHTED_ROUND_ROBIN_Splitter_12091));
}

void FFTReorderSimple_12093() {
	FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_12193_12214_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_12193_12214_join[0]));
}

void FFTReorderSimple_12094() {
	FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_12193_12214_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_12193_12214_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_12091() {
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&SplitJoin4_FFTReorderSimple_Fiss_12193_12214_split[0], pop_float(&FFTReorderSimple_12056WEIGHTED_ROUND_ROBIN_Splitter_12091));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&SplitJoin4_FFTReorderSimple_Fiss_12193_12214_split[1], pop_float(&FFTReorderSimple_12056WEIGHTED_ROUND_ROBIN_Splitter_12091));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_12092() {
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12092WEIGHTED_ROUND_ROBIN_Splitter_12095, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_12193_12214_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12092WEIGHTED_ROUND_ROBIN_Splitter_12095, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_12193_12214_join[1]));
	ENDFOR
}

void FFTReorderSimple_12097() {
	FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_12194_12215_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_12194_12215_join[0]));
}

void FFTReorderSimple_12098() {
	FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_12194_12215_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_12194_12215_join[1]));
}

void FFTReorderSimple_12099() {
	FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_12194_12215_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_12194_12215_join[2]));
}

void FFTReorderSimple_12100() {
	FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_12194_12215_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_12194_12215_join[3]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_12095() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_12194_12215_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12092WEIGHTED_ROUND_ROBIN_Splitter_12095));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_12096() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12096WEIGHTED_ROUND_ROBIN_Splitter_12101, pop_float(&SplitJoin6_FFTReorderSimple_Fiss_12194_12215_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void FFTReorderSimple_12103() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_12195_12216_split[0]), &(SplitJoin8_FFTReorderSimple_Fiss_12195_12216_join[0]));
	ENDFOR
}

void FFTReorderSimple_12104() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_12195_12216_split[1]), &(SplitJoin8_FFTReorderSimple_Fiss_12195_12216_join[1]));
	ENDFOR
}

void FFTReorderSimple_12105() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_12195_12216_split[2]), &(SplitJoin8_FFTReorderSimple_Fiss_12195_12216_join[2]));
	ENDFOR
}

void FFTReorderSimple_12106() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_12195_12216_split[3]), &(SplitJoin8_FFTReorderSimple_Fiss_12195_12216_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12101() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin8_FFTReorderSimple_Fiss_12195_12216_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12096WEIGHTED_ROUND_ROBIN_Splitter_12101));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12102() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12102WEIGHTED_ROUND_ROBIN_Splitter_12107, pop_float(&SplitJoin8_FFTReorderSimple_Fiss_12195_12216_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_12109() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_12196_12217_split[0]), &(SplitJoin10_FFTReorderSimple_Fiss_12196_12217_join[0]));
	ENDFOR
}

void FFTReorderSimple_12110() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_12196_12217_split[1]), &(SplitJoin10_FFTReorderSimple_Fiss_12196_12217_join[1]));
	ENDFOR
}

void FFTReorderSimple_12111() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_12196_12217_split[2]), &(SplitJoin10_FFTReorderSimple_Fiss_12196_12217_join[2]));
	ENDFOR
}

void FFTReorderSimple_12112() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_12196_12217_split[3]), &(SplitJoin10_FFTReorderSimple_Fiss_12196_12217_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12107() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_FFTReorderSimple_Fiss_12196_12217_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12102WEIGHTED_ROUND_ROBIN_Splitter_12107));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12108() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12108WEIGHTED_ROUND_ROBIN_Splitter_12113, pop_float(&SplitJoin10_FFTReorderSimple_Fiss_12196_12217_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&(*chanin), i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&(*chanin), i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&(*chanin), (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&(*chanin), (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_12115_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_12115_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&(*chanin)) ; 
			push_float(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_12115() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_12197_12218_split[0]), &(SplitJoin12_CombineDFT_Fiss_12197_12218_join[0]));
	ENDFOR
}

void CombineDFT_12116() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_12197_12218_split[1]), &(SplitJoin12_CombineDFT_Fiss_12197_12218_join[1]));
	ENDFOR
}

void CombineDFT_12117() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_12197_12218_split[2]), &(SplitJoin12_CombineDFT_Fiss_12197_12218_join[2]));
	ENDFOR
}

void CombineDFT_12118() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_12197_12218_split[3]), &(SplitJoin12_CombineDFT_Fiss_12197_12218_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12113() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin12_CombineDFT_Fiss_12197_12218_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12108WEIGHTED_ROUND_ROBIN_Splitter_12113));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12114() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12114WEIGHTED_ROUND_ROBIN_Splitter_12119, pop_float(&SplitJoin12_CombineDFT_Fiss_12197_12218_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_12121() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_12198_12219_split[0]), &(SplitJoin14_CombineDFT_Fiss_12198_12219_join[0]));
	ENDFOR
}

void CombineDFT_12122() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_12198_12219_split[1]), &(SplitJoin14_CombineDFT_Fiss_12198_12219_join[1]));
	ENDFOR
}

void CombineDFT_12123() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_12198_12219_split[2]), &(SplitJoin14_CombineDFT_Fiss_12198_12219_join[2]));
	ENDFOR
}

void CombineDFT_12124() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_12198_12219_split[3]), &(SplitJoin14_CombineDFT_Fiss_12198_12219_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12119() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin14_CombineDFT_Fiss_12198_12219_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12114WEIGHTED_ROUND_ROBIN_Splitter_12119));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12120() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12120WEIGHTED_ROUND_ROBIN_Splitter_12125, pop_float(&SplitJoin14_CombineDFT_Fiss_12198_12219_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_12127() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_12199_12220_split[0]), &(SplitJoin16_CombineDFT_Fiss_12199_12220_join[0]));
	ENDFOR
}

void CombineDFT_12128() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_12199_12220_split[1]), &(SplitJoin16_CombineDFT_Fiss_12199_12220_join[1]));
	ENDFOR
}

void CombineDFT_12129() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_12199_12220_split[2]), &(SplitJoin16_CombineDFT_Fiss_12199_12220_join[2]));
	ENDFOR
}

void CombineDFT_12130() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_12199_12220_split[3]), &(SplitJoin16_CombineDFT_Fiss_12199_12220_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12125() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin16_CombineDFT_Fiss_12199_12220_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12120WEIGHTED_ROUND_ROBIN_Splitter_12125));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12126() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12126WEIGHTED_ROUND_ROBIN_Splitter_12131, pop_float(&SplitJoin16_CombineDFT_Fiss_12199_12220_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_12133() {
	CombineDFT(&(SplitJoin18_CombineDFT_Fiss_12200_12221_split[0]), &(SplitJoin18_CombineDFT_Fiss_12200_12221_join[0]));
}

void CombineDFT_12134() {
	CombineDFT(&(SplitJoin18_CombineDFT_Fiss_12200_12221_split[1]), &(SplitJoin18_CombineDFT_Fiss_12200_12221_join[1]));
}

void CombineDFT_12135() {
	CombineDFT(&(SplitJoin18_CombineDFT_Fiss_12200_12221_split[2]), &(SplitJoin18_CombineDFT_Fiss_12200_12221_join[2]));
}

void CombineDFT_12136() {
	CombineDFT(&(SplitJoin18_CombineDFT_Fiss_12200_12221_split[3]), &(SplitJoin18_CombineDFT_Fiss_12200_12221_join[3]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_12131() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
			push_float(&SplitJoin18_CombineDFT_Fiss_12200_12221_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12126WEIGHTED_ROUND_ROBIN_Splitter_12131));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_12132() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12132WEIGHTED_ROUND_ROBIN_Splitter_12137, pop_float(&SplitJoin18_CombineDFT_Fiss_12200_12221_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void CombineDFT_12139() {
	CombineDFT(&(SplitJoin20_CombineDFT_Fiss_12201_12222_split[0]), &(SplitJoin20_CombineDFT_Fiss_12201_12222_join[0]));
}

void CombineDFT_12140() {
	CombineDFT(&(SplitJoin20_CombineDFT_Fiss_12201_12222_split[1]), &(SplitJoin20_CombineDFT_Fiss_12201_12222_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_12137() {
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&SplitJoin20_CombineDFT_Fiss_12201_12222_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12132WEIGHTED_ROUND_ROBIN_Splitter_12137));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&SplitJoin20_CombineDFT_Fiss_12201_12222_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12132WEIGHTED_ROUND_ROBIN_Splitter_12137));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_12138() {
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12138CombineDFT_12066, pop_float(&SplitJoin20_CombineDFT_Fiss_12201_12222_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12138CombineDFT_12066, pop_float(&SplitJoin20_CombineDFT_Fiss_12201_12222_join[1]));
	ENDFOR
}

void CombineDFT_12066() {
	CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_12138CombineDFT_12066), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_12045_12081_12192_12213_join[0]));
}

void FFTReorderSimple_12067() {
	FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_12045_12081_12192_12213_split[1]), &(FFTReorderSimple_12067WEIGHTED_ROUND_ROBIN_Splitter_12141));
}

void FFTReorderSimple_12143() {
	FFTReorderSimple(&(SplitJoin46_FFTReorderSimple_Fiss_12202_12223_split[0]), &(SplitJoin46_FFTReorderSimple_Fiss_12202_12223_join[0]));
}

void FFTReorderSimple_12144() {
	FFTReorderSimple(&(SplitJoin46_FFTReorderSimple_Fiss_12202_12223_split[1]), &(SplitJoin46_FFTReorderSimple_Fiss_12202_12223_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_12141() {
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&SplitJoin46_FFTReorderSimple_Fiss_12202_12223_split[0], pop_float(&FFTReorderSimple_12067WEIGHTED_ROUND_ROBIN_Splitter_12141));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&SplitJoin46_FFTReorderSimple_Fiss_12202_12223_split[1], pop_float(&FFTReorderSimple_12067WEIGHTED_ROUND_ROBIN_Splitter_12141));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_12142() {
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12142WEIGHTED_ROUND_ROBIN_Splitter_12145, pop_float(&SplitJoin46_FFTReorderSimple_Fiss_12202_12223_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12142WEIGHTED_ROUND_ROBIN_Splitter_12145, pop_float(&SplitJoin46_FFTReorderSimple_Fiss_12202_12223_join[1]));
	ENDFOR
}

void FFTReorderSimple_12147() {
	FFTReorderSimple(&(SplitJoin48_FFTReorderSimple_Fiss_12203_12224_split[0]), &(SplitJoin48_FFTReorderSimple_Fiss_12203_12224_join[0]));
}

void FFTReorderSimple_12148() {
	FFTReorderSimple(&(SplitJoin48_FFTReorderSimple_Fiss_12203_12224_split[1]), &(SplitJoin48_FFTReorderSimple_Fiss_12203_12224_join[1]));
}

void FFTReorderSimple_12149() {
	FFTReorderSimple(&(SplitJoin48_FFTReorderSimple_Fiss_12203_12224_split[2]), &(SplitJoin48_FFTReorderSimple_Fiss_12203_12224_join[2]));
}

void FFTReorderSimple_12150() {
	FFTReorderSimple(&(SplitJoin48_FFTReorderSimple_Fiss_12203_12224_split[3]), &(SplitJoin48_FFTReorderSimple_Fiss_12203_12224_join[3]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_12145() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
			push_float(&SplitJoin48_FFTReorderSimple_Fiss_12203_12224_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12142WEIGHTED_ROUND_ROBIN_Splitter_12145));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_12146() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12146WEIGHTED_ROUND_ROBIN_Splitter_12151, pop_float(&SplitJoin48_FFTReorderSimple_Fiss_12203_12224_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void FFTReorderSimple_12153() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin50_FFTReorderSimple_Fiss_12204_12225_split[0]), &(SplitJoin50_FFTReorderSimple_Fiss_12204_12225_join[0]));
	ENDFOR
}

void FFTReorderSimple_12154() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin50_FFTReorderSimple_Fiss_12204_12225_split[1]), &(SplitJoin50_FFTReorderSimple_Fiss_12204_12225_join[1]));
	ENDFOR
}

void FFTReorderSimple_12155() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin50_FFTReorderSimple_Fiss_12204_12225_split[2]), &(SplitJoin50_FFTReorderSimple_Fiss_12204_12225_join[2]));
	ENDFOR
}

void FFTReorderSimple_12156() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin50_FFTReorderSimple_Fiss_12204_12225_split[3]), &(SplitJoin50_FFTReorderSimple_Fiss_12204_12225_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12151() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin50_FFTReorderSimple_Fiss_12204_12225_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12146WEIGHTED_ROUND_ROBIN_Splitter_12151));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12152() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12152WEIGHTED_ROUND_ROBIN_Splitter_12157, pop_float(&SplitJoin50_FFTReorderSimple_Fiss_12204_12225_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_12159() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin52_FFTReorderSimple_Fiss_12205_12226_split[0]), &(SplitJoin52_FFTReorderSimple_Fiss_12205_12226_join[0]));
	ENDFOR
}

void FFTReorderSimple_12160() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin52_FFTReorderSimple_Fiss_12205_12226_split[1]), &(SplitJoin52_FFTReorderSimple_Fiss_12205_12226_join[1]));
	ENDFOR
}

void FFTReorderSimple_12161() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin52_FFTReorderSimple_Fiss_12205_12226_split[2]), &(SplitJoin52_FFTReorderSimple_Fiss_12205_12226_join[2]));
	ENDFOR
}

void FFTReorderSimple_12162() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin52_FFTReorderSimple_Fiss_12205_12226_split[3]), &(SplitJoin52_FFTReorderSimple_Fiss_12205_12226_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12157() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin52_FFTReorderSimple_Fiss_12205_12226_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12152WEIGHTED_ROUND_ROBIN_Splitter_12157));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12158() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12158WEIGHTED_ROUND_ROBIN_Splitter_12163, pop_float(&SplitJoin52_FFTReorderSimple_Fiss_12205_12226_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_12165() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin54_CombineDFT_Fiss_12206_12227_split[0]), &(SplitJoin54_CombineDFT_Fiss_12206_12227_join[0]));
	ENDFOR
}

void CombineDFT_12166() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin54_CombineDFT_Fiss_12206_12227_split[1]), &(SplitJoin54_CombineDFT_Fiss_12206_12227_join[1]));
	ENDFOR
}

void CombineDFT_12167() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin54_CombineDFT_Fiss_12206_12227_split[2]), &(SplitJoin54_CombineDFT_Fiss_12206_12227_join[2]));
	ENDFOR
}

void CombineDFT_12168() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin54_CombineDFT_Fiss_12206_12227_split[3]), &(SplitJoin54_CombineDFT_Fiss_12206_12227_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12163() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin54_CombineDFT_Fiss_12206_12227_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12158WEIGHTED_ROUND_ROBIN_Splitter_12163));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12164() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12164WEIGHTED_ROUND_ROBIN_Splitter_12169, pop_float(&SplitJoin54_CombineDFT_Fiss_12206_12227_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_12171() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin56_CombineDFT_Fiss_12207_12228_split[0]), &(SplitJoin56_CombineDFT_Fiss_12207_12228_join[0]));
	ENDFOR
}

void CombineDFT_12172() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin56_CombineDFT_Fiss_12207_12228_split[1]), &(SplitJoin56_CombineDFT_Fiss_12207_12228_join[1]));
	ENDFOR
}

void CombineDFT_12173() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin56_CombineDFT_Fiss_12207_12228_split[2]), &(SplitJoin56_CombineDFT_Fiss_12207_12228_join[2]));
	ENDFOR
}

void CombineDFT_12174() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin56_CombineDFT_Fiss_12207_12228_split[3]), &(SplitJoin56_CombineDFT_Fiss_12207_12228_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12169() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin56_CombineDFT_Fiss_12207_12228_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12164WEIGHTED_ROUND_ROBIN_Splitter_12169));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12170() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12170WEIGHTED_ROUND_ROBIN_Splitter_12175, pop_float(&SplitJoin56_CombineDFT_Fiss_12207_12228_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_12177() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin58_CombineDFT_Fiss_12208_12229_split[0]), &(SplitJoin58_CombineDFT_Fiss_12208_12229_join[0]));
	ENDFOR
}

void CombineDFT_12178() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin58_CombineDFT_Fiss_12208_12229_split[1]), &(SplitJoin58_CombineDFT_Fiss_12208_12229_join[1]));
	ENDFOR
}

void CombineDFT_12179() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin58_CombineDFT_Fiss_12208_12229_split[2]), &(SplitJoin58_CombineDFT_Fiss_12208_12229_join[2]));
	ENDFOR
}

void CombineDFT_12180() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin58_CombineDFT_Fiss_12208_12229_split[3]), &(SplitJoin58_CombineDFT_Fiss_12208_12229_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12175() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin58_CombineDFT_Fiss_12208_12229_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12170WEIGHTED_ROUND_ROBIN_Splitter_12175));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12176() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12176WEIGHTED_ROUND_ROBIN_Splitter_12181, pop_float(&SplitJoin58_CombineDFT_Fiss_12208_12229_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_12183() {
	CombineDFT(&(SplitJoin60_CombineDFT_Fiss_12209_12230_split[0]), &(SplitJoin60_CombineDFT_Fiss_12209_12230_join[0]));
}

void CombineDFT_12184() {
	CombineDFT(&(SplitJoin60_CombineDFT_Fiss_12209_12230_split[1]), &(SplitJoin60_CombineDFT_Fiss_12209_12230_join[1]));
}

void CombineDFT_12185() {
	CombineDFT(&(SplitJoin60_CombineDFT_Fiss_12209_12230_split[2]), &(SplitJoin60_CombineDFT_Fiss_12209_12230_join[2]));
}

void CombineDFT_12186() {
	CombineDFT(&(SplitJoin60_CombineDFT_Fiss_12209_12230_split[3]), &(SplitJoin60_CombineDFT_Fiss_12209_12230_join[3]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_12181() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
			push_float(&SplitJoin60_CombineDFT_Fiss_12209_12230_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12176WEIGHTED_ROUND_ROBIN_Splitter_12181));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_12182() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12182WEIGHTED_ROUND_ROBIN_Splitter_12187, pop_float(&SplitJoin60_CombineDFT_Fiss_12209_12230_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void CombineDFT_12189() {
	CombineDFT(&(SplitJoin62_CombineDFT_Fiss_12210_12231_split[0]), &(SplitJoin62_CombineDFT_Fiss_12210_12231_join[0]));
}

void CombineDFT_12190() {
	CombineDFT(&(SplitJoin62_CombineDFT_Fiss_12210_12231_split[1]), &(SplitJoin62_CombineDFT_Fiss_12210_12231_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_12187() {
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&SplitJoin62_CombineDFT_Fiss_12210_12231_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12182WEIGHTED_ROUND_ROBIN_Splitter_12187));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&SplitJoin62_CombineDFT_Fiss_12210_12231_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12182WEIGHTED_ROUND_ROBIN_Splitter_12187));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_12188() {
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12188CombineDFT_12077, pop_float(&SplitJoin62_CombineDFT_Fiss_12210_12231_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12188CombineDFT_12077, pop_float(&SplitJoin62_CombineDFT_Fiss_12210_12231_join[1]));
	ENDFOR
}

void CombineDFT_12077() {
	CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_12188CombineDFT_12077), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_12045_12081_12192_12213_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_12079() {
	FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
		push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_12045_12081_12192_12213_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12088WEIGHTED_ROUND_ROBIN_Splitter_12079));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
		push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_12045_12081_12192_12213_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12088WEIGHTED_ROUND_ROBIN_Splitter_12079));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_12080() {
	FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12080FloatPrinter_12078, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_12045_12081_12192_12213_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12080FloatPrinter_12078, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_12045_12081_12192_12213_join[1]));
	ENDFOR
}

void FloatPrinter(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void FloatPrinter_12078() {
	FOR(uint32_t, __iter_steady_, 0, <, 256, __iter_steady_++)
		FloatPrinter(&(WEIGHTED_ROUND_ROBIN_Joiner_12080FloatPrinter_12078));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12170WEIGHTED_ROUND_ROBIN_Splitter_12175);
	FOR(int, __iter_init_0_, 0, <, 4, __iter_init_0_++)
		init_buffer_float(&SplitJoin54_CombineDFT_Fiss_12206_12227_join[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12102WEIGHTED_ROUND_ROBIN_Splitter_12107);
	FOR(int, __iter_init_1_, 0, <, 4, __iter_init_1_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_12194_12215_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_float(&SplitJoin46_FFTReorderSimple_Fiss_12202_12223_split[__iter_init_2_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12126WEIGHTED_ROUND_ROBIN_Splitter_12131);
	FOR(int, __iter_init_3_, 0, <, 4, __iter_init_3_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_12197_12218_split[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12164WEIGHTED_ROUND_ROBIN_Splitter_12169);
	FOR(int, __iter_init_4_, 0, <, 2, __iter_init_4_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_12191_12212_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 4, __iter_init_5_++)
		init_buffer_float(&SplitJoin48_FFTReorderSimple_Fiss_12203_12224_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 4, __iter_init_6_++)
		init_buffer_float(&SplitJoin58_CombineDFT_Fiss_12208_12229_split[__iter_init_6_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12188CombineDFT_12077);
	FOR(int, __iter_init_7_, 0, <, 2, __iter_init_7_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_12191_12212_split[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12146WEIGHTED_ROUND_ROBIN_Splitter_12151);
	FOR(int, __iter_init_8_, 0, <, 4, __iter_init_8_++)
		init_buffer_float(&SplitJoin60_CombineDFT_Fiss_12209_12230_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 4, __iter_init_9_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_12196_12217_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 4, __iter_init_10_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_12199_12220_join[__iter_init_10_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12132WEIGHTED_ROUND_ROBIN_Splitter_12137);
	FOR(int, __iter_init_11_, 0, <, 4, __iter_init_11_++)
		init_buffer_float(&SplitJoin58_CombineDFT_Fiss_12208_12229_join[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12138CombineDFT_12066);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12152WEIGHTED_ROUND_ROBIN_Splitter_12157);
	FOR(int, __iter_init_12_, 0, <, 2, __iter_init_12_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_12193_12214_join[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_12045_12081_12192_12213_split[__iter_init_13_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12108WEIGHTED_ROUND_ROBIN_Splitter_12113);
	init_buffer_float(&FFTReorderSimple_12056WEIGHTED_ROUND_ROBIN_Splitter_12091);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12120WEIGHTED_ROUND_ROBIN_Splitter_12125);
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_float(&SplitJoin62_CombineDFT_Fiss_12210_12231_split[__iter_init_14_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12092WEIGHTED_ROUND_ROBIN_Splitter_12095);
	FOR(int, __iter_init_15_, 0, <, 4, __iter_init_15_++)
		init_buffer_float(&SplitJoin52_FFTReorderSimple_Fiss_12205_12226_join[__iter_init_15_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12182WEIGHTED_ROUND_ROBIN_Splitter_12187);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12080FloatPrinter_12078);
	FOR(int, __iter_init_16_, 0, <, 4, __iter_init_16_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_12197_12218_join[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 4, __iter_init_17_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_12200_12221_join[__iter_init_17_]);
	ENDFOR
	init_buffer_float(&FFTReorderSimple_12067WEIGHTED_ROUND_ROBIN_Splitter_12141);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12114WEIGHTED_ROUND_ROBIN_Splitter_12119);
	FOR(int, __iter_init_18_, 0, <, 4, __iter_init_18_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_12196_12217_split[__iter_init_18_]);
	ENDFOR
	FOR(int, __iter_init_19_, 0, <, 4, __iter_init_19_++)
		init_buffer_float(&SplitJoin52_FFTReorderSimple_Fiss_12205_12226_split[__iter_init_19_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12158WEIGHTED_ROUND_ROBIN_Splitter_12163);
	FOR(int, __iter_init_20_, 0, <, 4, __iter_init_20_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_12195_12216_split[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 4, __iter_init_21_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_12199_12220_split[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 2, __iter_init_22_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_12201_12222_join[__iter_init_22_]);
	ENDFOR
	FOR(int, __iter_init_23_, 0, <, 4, __iter_init_23_++)
		init_buffer_float(&SplitJoin48_FFTReorderSimple_Fiss_12203_12224_join[__iter_init_23_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12096WEIGHTED_ROUND_ROBIN_Splitter_12101);
	FOR(int, __iter_init_24_, 0, <, 4, __iter_init_24_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_12195_12216_join[__iter_init_24_]);
	ENDFOR
	FOR(int, __iter_init_25_, 0, <, 4, __iter_init_25_++)
		init_buffer_float(&SplitJoin54_CombineDFT_Fiss_12206_12227_split[__iter_init_25_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12142WEIGHTED_ROUND_ROBIN_Splitter_12145);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12176WEIGHTED_ROUND_ROBIN_Splitter_12181);
	FOR(int, __iter_init_26_, 0, <, 4, __iter_init_26_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_12198_12219_join[__iter_init_26_]);
	ENDFOR
	FOR(int, __iter_init_27_, 0, <, 2, __iter_init_27_++)
		init_buffer_float(&SplitJoin46_FFTReorderSimple_Fiss_12202_12223_join[__iter_init_27_]);
	ENDFOR
	FOR(int, __iter_init_28_, 0, <, 4, __iter_init_28_++)
		init_buffer_float(&SplitJoin60_CombineDFT_Fiss_12209_12230_split[__iter_init_28_]);
	ENDFOR
	FOR(int, __iter_init_29_, 0, <, 2, __iter_init_29_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_12045_12081_12192_12213_join[__iter_init_29_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12088WEIGHTED_ROUND_ROBIN_Splitter_12079);
	FOR(int, __iter_init_30_, 0, <, 2, __iter_init_30_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_12193_12214_split[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 2, __iter_init_31_++)
		init_buffer_float(&SplitJoin62_CombineDFT_Fiss_12210_12231_join[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 4, __iter_init_32_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_12200_12221_split[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 4, __iter_init_33_++)
		init_buffer_float(&SplitJoin56_CombineDFT_Fiss_12207_12228_join[__iter_init_33_]);
	ENDFOR
	FOR(int, __iter_init_34_, 0, <, 4, __iter_init_34_++)
		init_buffer_float(&SplitJoin56_CombineDFT_Fiss_12207_12228_split[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 4, __iter_init_35_++)
		init_buffer_float(&SplitJoin50_FFTReorderSimple_Fiss_12204_12225_join[__iter_init_35_]);
	ENDFOR
	FOR(int, __iter_init_36_, 0, <, 2, __iter_init_36_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_12201_12222_split[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 4, __iter_init_37_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_12194_12215_split[__iter_init_37_]);
	ENDFOR
	FOR(int, __iter_init_38_, 0, <, 4, __iter_init_38_++)
		init_buffer_float(&SplitJoin50_FFTReorderSimple_Fiss_12204_12225_split[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 4, __iter_init_39_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_12198_12219_split[__iter_init_39_]);
	ENDFOR
// --- init: CombineDFT_12115
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_12115_s.w[i] = real ; 
		CombineDFT_12115_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12116
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_12116_s.w[i] = real ; 
		CombineDFT_12116_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12117
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_12117_s.w[i] = real ; 
		CombineDFT_12117_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12118
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_12118_s.w[i] = real ; 
		CombineDFT_12118_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12121
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_12121_s.w[i] = real ; 
		CombineDFT_12121_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12122
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_12122_s.w[i] = real ; 
		CombineDFT_12122_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12123
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_12123_s.w[i] = real ; 
		CombineDFT_12123_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12124
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_12124_s.w[i] = real ; 
		CombineDFT_12124_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12127
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_12127_s.w[i] = real ; 
		CombineDFT_12127_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12128
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_12128_s.w[i] = real ; 
		CombineDFT_12128_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12129
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_12129_s.w[i] = real ; 
		CombineDFT_12129_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12130
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_12130_s.w[i] = real ; 
		CombineDFT_12130_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12133
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_12133_s.w[i] = real ; 
		CombineDFT_12133_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12134
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_12134_s.w[i] = real ; 
		CombineDFT_12134_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12135
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_12135_s.w[i] = real ; 
		CombineDFT_12135_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12136
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_12136_s.w[i] = real ; 
		CombineDFT_12136_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12139
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_12139_s.w[i] = real ; 
		CombineDFT_12139_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12140
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_12140_s.w[i] = real ; 
		CombineDFT_12140_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12066
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_12066_s.w[i] = real ; 
		CombineDFT_12066_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12165
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_12165_s.w[i] = real ; 
		CombineDFT_12165_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12166
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_12166_s.w[i] = real ; 
		CombineDFT_12166_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12167
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_12167_s.w[i] = real ; 
		CombineDFT_12167_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12168
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_12168_s.w[i] = real ; 
		CombineDFT_12168_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12171
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_12171_s.w[i] = real ; 
		CombineDFT_12171_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12172
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_12172_s.w[i] = real ; 
		CombineDFT_12172_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12173
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_12173_s.w[i] = real ; 
		CombineDFT_12173_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12174
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_12174_s.w[i] = real ; 
		CombineDFT_12174_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12177
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_12177_s.w[i] = real ; 
		CombineDFT_12177_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12178
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_12178_s.w[i] = real ; 
		CombineDFT_12178_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12179
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_12179_s.w[i] = real ; 
		CombineDFT_12179_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12180
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_12180_s.w[i] = real ; 
		CombineDFT_12180_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12183
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_12183_s.w[i] = real ; 
		CombineDFT_12183_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12184
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_12184_s.w[i] = real ; 
		CombineDFT_12184_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12185
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_12185_s.w[i] = real ; 
		CombineDFT_12185_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12186
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_12186_s.w[i] = real ; 
		CombineDFT_12186_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12189
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_12189_s.w[i] = real ; 
		CombineDFT_12189_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12190
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_12190_s.w[i] = real ; 
		CombineDFT_12190_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12077
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_12077_s.w[i] = real ; 
		CombineDFT_12077_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_12087();
			FFTTestSource_12089();
			FFTTestSource_12090();
		WEIGHTED_ROUND_ROBIN_Joiner_12088();
		WEIGHTED_ROUND_ROBIN_Splitter_12079();
			FFTReorderSimple_12056();
			WEIGHTED_ROUND_ROBIN_Splitter_12091();
				FFTReorderSimple_12093();
				FFTReorderSimple_12094();
			WEIGHTED_ROUND_ROBIN_Joiner_12092();
			WEIGHTED_ROUND_ROBIN_Splitter_12095();
				FFTReorderSimple_12097();
				FFTReorderSimple_12098();
				FFTReorderSimple_12099();
				FFTReorderSimple_12100();
			WEIGHTED_ROUND_ROBIN_Joiner_12096();
			WEIGHTED_ROUND_ROBIN_Splitter_12101();
				FFTReorderSimple_12103();
				FFTReorderSimple_12104();
				FFTReorderSimple_12105();
				FFTReorderSimple_12106();
			WEIGHTED_ROUND_ROBIN_Joiner_12102();
			WEIGHTED_ROUND_ROBIN_Splitter_12107();
				FFTReorderSimple_12109();
				FFTReorderSimple_12110();
				FFTReorderSimple_12111();
				FFTReorderSimple_12112();
			WEIGHTED_ROUND_ROBIN_Joiner_12108();
			WEIGHTED_ROUND_ROBIN_Splitter_12113();
				CombineDFT_12115();
				CombineDFT_12116();
				CombineDFT_12117();
				CombineDFT_12118();
			WEIGHTED_ROUND_ROBIN_Joiner_12114();
			WEIGHTED_ROUND_ROBIN_Splitter_12119();
				CombineDFT_12121();
				CombineDFT_12122();
				CombineDFT_12123();
				CombineDFT_12124();
			WEIGHTED_ROUND_ROBIN_Joiner_12120();
			WEIGHTED_ROUND_ROBIN_Splitter_12125();
				CombineDFT_12127();
				CombineDFT_12128();
				CombineDFT_12129();
				CombineDFT_12130();
			WEIGHTED_ROUND_ROBIN_Joiner_12126();
			WEIGHTED_ROUND_ROBIN_Splitter_12131();
				CombineDFT_12133();
				CombineDFT_12134();
				CombineDFT_12135();
				CombineDFT_12136();
			WEIGHTED_ROUND_ROBIN_Joiner_12132();
			WEIGHTED_ROUND_ROBIN_Splitter_12137();
				CombineDFT_12139();
				CombineDFT_12140();
			WEIGHTED_ROUND_ROBIN_Joiner_12138();
			CombineDFT_12066();
			FFTReorderSimple_12067();
			WEIGHTED_ROUND_ROBIN_Splitter_12141();
				FFTReorderSimple_12143();
				FFTReorderSimple_12144();
			WEIGHTED_ROUND_ROBIN_Joiner_12142();
			WEIGHTED_ROUND_ROBIN_Splitter_12145();
				FFTReorderSimple_12147();
				FFTReorderSimple_12148();
				FFTReorderSimple_12149();
				FFTReorderSimple_12150();
			WEIGHTED_ROUND_ROBIN_Joiner_12146();
			WEIGHTED_ROUND_ROBIN_Splitter_12151();
				FFTReorderSimple_12153();
				FFTReorderSimple_12154();
				FFTReorderSimple_12155();
				FFTReorderSimple_12156();
			WEIGHTED_ROUND_ROBIN_Joiner_12152();
			WEIGHTED_ROUND_ROBIN_Splitter_12157();
				FFTReorderSimple_12159();
				FFTReorderSimple_12160();
				FFTReorderSimple_12161();
				FFTReorderSimple_12162();
			WEIGHTED_ROUND_ROBIN_Joiner_12158();
			WEIGHTED_ROUND_ROBIN_Splitter_12163();
				CombineDFT_12165();
				CombineDFT_12166();
				CombineDFT_12167();
				CombineDFT_12168();
			WEIGHTED_ROUND_ROBIN_Joiner_12164();
			WEIGHTED_ROUND_ROBIN_Splitter_12169();
				CombineDFT_12171();
				CombineDFT_12172();
				CombineDFT_12173();
				CombineDFT_12174();
			WEIGHTED_ROUND_ROBIN_Joiner_12170();
			WEIGHTED_ROUND_ROBIN_Splitter_12175();
				CombineDFT_12177();
				CombineDFT_12178();
				CombineDFT_12179();
				CombineDFT_12180();
			WEIGHTED_ROUND_ROBIN_Joiner_12176();
			WEIGHTED_ROUND_ROBIN_Splitter_12181();
				CombineDFT_12183();
				CombineDFT_12184();
				CombineDFT_12185();
				CombineDFT_12186();
			WEIGHTED_ROUND_ROBIN_Joiner_12182();
			WEIGHTED_ROUND_ROBIN_Splitter_12187();
				CombineDFT_12189();
				CombineDFT_12190();
			WEIGHTED_ROUND_ROBIN_Joiner_12188();
			CombineDFT_12077();
		WEIGHTED_ROUND_ROBIN_Joiner_12080();
		FloatPrinter_12078();
	ENDFOR
	return EXIT_SUCCESS;
}
