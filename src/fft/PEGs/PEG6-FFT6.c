#include "PEG6-FFT6.h"

buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_5558_5568_join[4];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_5559_5569_split[6];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5516WEIGHTED_ROUND_ROBIN_Splitter_5523;
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_5560_5570_join[6];
buffer_complex_t FFTReorderSimple_5484WEIGHTED_ROUND_ROBIN_Splitter_5497;
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_5558_5568_split[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5502WEIGHTED_ROUND_ROBIN_Splitter_5507;
buffer_complex_t SplitJoin14_CombineDFT_Fiss_5564_5574_join[4];
buffer_complex_t FFTTestSource_5483FFTReorderSimple_5484;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5532WEIGHTED_ROUND_ROBIN_Splitter_5539;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5498WEIGHTED_ROUND_ROBIN_Splitter_5501;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_5561_5571_join[6];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5540WEIGHTED_ROUND_ROBIN_Splitter_5547;
buffer_complex_t SplitJoin14_CombineDFT_Fiss_5564_5574_split[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5524WEIGHTED_ROUND_ROBIN_Splitter_5531;
buffer_complex_t SplitJoin12_CombineDFT_Fiss_5563_5573_split[6];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5554CombineDFT_5494;
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_5559_5569_join[6];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_5565_5575_join[2];
buffer_complex_t CombineDFT_5494CPrinter_5495;
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_5557_5567_join[2];
buffer_complex_t SplitJoin8_CombineDFT_Fiss_5561_5571_split[6];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5508WEIGHTED_ROUND_ROBIN_Splitter_5515;
buffer_complex_t SplitJoin12_CombineDFT_Fiss_5563_5573_join[6];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_5557_5567_split[2];
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_5560_5570_split[6];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_5562_5572_join[6];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_5562_5572_split[6];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_5565_5575_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5548WEIGHTED_ROUND_ROBIN_Splitter_5553;


CombineDFT_5525_t CombineDFT_5525_s;
CombineDFT_5525_t CombineDFT_5526_s;
CombineDFT_5525_t CombineDFT_5527_s;
CombineDFT_5525_t CombineDFT_5528_s;
CombineDFT_5525_t CombineDFT_5529_s;
CombineDFT_5525_t CombineDFT_5530_s;
CombineDFT_5525_t CombineDFT_5533_s;
CombineDFT_5525_t CombineDFT_5534_s;
CombineDFT_5525_t CombineDFT_5535_s;
CombineDFT_5525_t CombineDFT_5536_s;
CombineDFT_5525_t CombineDFT_5537_s;
CombineDFT_5525_t CombineDFT_5538_s;
CombineDFT_5525_t CombineDFT_5541_s;
CombineDFT_5525_t CombineDFT_5542_s;
CombineDFT_5525_t CombineDFT_5543_s;
CombineDFT_5525_t CombineDFT_5544_s;
CombineDFT_5525_t CombineDFT_5545_s;
CombineDFT_5525_t CombineDFT_5546_s;
CombineDFT_5525_t CombineDFT_5549_s;
CombineDFT_5525_t CombineDFT_5550_s;
CombineDFT_5525_t CombineDFT_5551_s;
CombineDFT_5525_t CombineDFT_5552_s;
CombineDFT_5525_t CombineDFT_5555_s;
CombineDFT_5525_t CombineDFT_5556_s;
CombineDFT_5525_t CombineDFT_5494_s;

void FFTTestSource(buffer_complex_t *chanout) {
		complex_t c1;
		complex_t zero;
		c1.real = 1.0 ; 
		c1.imag = 0.0 ; 
		zero.real = 0.0 ; 
		zero.imag = 0.0 ; 
		push_complex(&(*chanout), zero) ; 
		push_complex(&(*chanout), c1) ; 
		FOR(int, i, 0,  < , 62, i++) {
			push_complex(&(*chanout), zero) ; 
		}
		ENDFOR
	}


void FFTTestSource_5483() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTTestSource(&(FFTTestSource_5483FFTReorderSimple_5484));
	ENDFOR
}

void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 64, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&(*chanin)) ; 
		}
		ENDFOR
	}


void FFTReorderSimple_5484() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(FFTTestSource_5483FFTReorderSimple_5484), &(FFTReorderSimple_5484WEIGHTED_ROUND_ROBIN_Splitter_5497));
	ENDFOR
}

void FFTReorderSimple_5499() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_5557_5567_split[0]), &(SplitJoin0_FFTReorderSimple_Fiss_5557_5567_join[0]));
	ENDFOR
}

void FFTReorderSimple_5500() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_5557_5567_split[1]), &(SplitJoin0_FFTReorderSimple_Fiss_5557_5567_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5497() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_5557_5567_split[0], pop_complex(&FFTReorderSimple_5484WEIGHTED_ROUND_ROBIN_Splitter_5497));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_5557_5567_split[1], pop_complex(&FFTReorderSimple_5484WEIGHTED_ROUND_ROBIN_Splitter_5497));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5498() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5498WEIGHTED_ROUND_ROBIN_Splitter_5501, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_5557_5567_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5498WEIGHTED_ROUND_ROBIN_Splitter_5501, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_5557_5567_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_5503() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_5558_5568_split[0]), &(SplitJoin2_FFTReorderSimple_Fiss_5558_5568_join[0]));
	ENDFOR
}

void FFTReorderSimple_5504() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_5558_5568_split[1]), &(SplitJoin2_FFTReorderSimple_Fiss_5558_5568_join[1]));
	ENDFOR
}

void FFTReorderSimple_5505() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_5558_5568_split[2]), &(SplitJoin2_FFTReorderSimple_Fiss_5558_5568_join[2]));
	ENDFOR
}

void FFTReorderSimple_5506() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_5558_5568_split[3]), &(SplitJoin2_FFTReorderSimple_Fiss_5558_5568_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5501() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5558_5568_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5498WEIGHTED_ROUND_ROBIN_Splitter_5501));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5502() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5502WEIGHTED_ROUND_ROBIN_Splitter_5507, pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_5558_5568_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_5509() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_5559_5569_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_5559_5569_join[0]));
	ENDFOR
}

void FFTReorderSimple_5510() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_5559_5569_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_5559_5569_join[1]));
	ENDFOR
}

void FFTReorderSimple_5511() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_5559_5569_split[2]), &(SplitJoin4_FFTReorderSimple_Fiss_5559_5569_join[2]));
	ENDFOR
}

void FFTReorderSimple_5512() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_5559_5569_split[3]), &(SplitJoin4_FFTReorderSimple_Fiss_5559_5569_join[3]));
	ENDFOR
}

void FFTReorderSimple_5513() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_5559_5569_split[4]), &(SplitJoin4_FFTReorderSimple_Fiss_5559_5569_join[4]));
	ENDFOR
}

void FFTReorderSimple_5514() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_5559_5569_split[5]), &(SplitJoin4_FFTReorderSimple_Fiss_5559_5569_join[5]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5507() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5559_5569_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5502WEIGHTED_ROUND_ROBIN_Splitter_5507));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5508() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5508WEIGHTED_ROUND_ROBIN_Splitter_5515, pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_5559_5569_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_5517() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_5560_5570_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_5560_5570_join[0]));
	ENDFOR
}

void FFTReorderSimple_5518() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_5560_5570_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_5560_5570_join[1]));
	ENDFOR
}

void FFTReorderSimple_5519() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_5560_5570_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_5560_5570_join[2]));
	ENDFOR
}

void FFTReorderSimple_5520() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_5560_5570_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_5560_5570_join[3]));
	ENDFOR
}

void FFTReorderSimple_5521() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_5560_5570_split[4]), &(SplitJoin6_FFTReorderSimple_Fiss_5560_5570_join[4]));
	ENDFOR
}

void FFTReorderSimple_5522() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_5560_5570_split[5]), &(SplitJoin6_FFTReorderSimple_Fiss_5560_5570_join[5]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5515() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5560_5570_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5508WEIGHTED_ROUND_ROBIN_Splitter_5515));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5516() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5516WEIGHTED_ROUND_ROBIN_Splitter_5523, pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_5560_5570_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&(*chanin), (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5525_s.wn.real) - (w.imag * CombineDFT_5525_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5525_s.wn.imag) + (w.imag * CombineDFT_5525_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&(*chanin)) ; 
			push_complex(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_5525() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_5561_5571_split[0]), &(SplitJoin8_CombineDFT_Fiss_5561_5571_join[0]));
	ENDFOR
}

void CombineDFT_5526() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_5561_5571_split[1]), &(SplitJoin8_CombineDFT_Fiss_5561_5571_join[1]));
	ENDFOR
}

void CombineDFT_5527() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_5561_5571_split[2]), &(SplitJoin8_CombineDFT_Fiss_5561_5571_join[2]));
	ENDFOR
}

void CombineDFT_5528() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_5561_5571_split[3]), &(SplitJoin8_CombineDFT_Fiss_5561_5571_join[3]));
	ENDFOR
}

void CombineDFT_5529() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_5561_5571_split[4]), &(SplitJoin8_CombineDFT_Fiss_5561_5571_join[4]));
	ENDFOR
}

void CombineDFT_5530() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_5561_5571_split[5]), &(SplitJoin8_CombineDFT_Fiss_5561_5571_join[5]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5523() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 6, __iter_++)
			push_complex(&SplitJoin8_CombineDFT_Fiss_5561_5571_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5516WEIGHTED_ROUND_ROBIN_Splitter_5523));
			push_complex(&SplitJoin8_CombineDFT_Fiss_5561_5571_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5516WEIGHTED_ROUND_ROBIN_Splitter_5523));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5524() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 6, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5524WEIGHTED_ROUND_ROBIN_Splitter_5531, pop_complex(&SplitJoin8_CombineDFT_Fiss_5561_5571_join[__iter_]));
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5524WEIGHTED_ROUND_ROBIN_Splitter_5531, pop_complex(&SplitJoin8_CombineDFT_Fiss_5561_5571_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_5533() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_5562_5572_split[0]), &(SplitJoin10_CombineDFT_Fiss_5562_5572_join[0]));
	ENDFOR
}

void CombineDFT_5534() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_5562_5572_split[1]), &(SplitJoin10_CombineDFT_Fiss_5562_5572_join[1]));
	ENDFOR
}

void CombineDFT_5535() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_5562_5572_split[2]), &(SplitJoin10_CombineDFT_Fiss_5562_5572_join[2]));
	ENDFOR
}

void CombineDFT_5536() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_5562_5572_split[3]), &(SplitJoin10_CombineDFT_Fiss_5562_5572_join[3]));
	ENDFOR
}

void CombineDFT_5537() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_5562_5572_split[4]), &(SplitJoin10_CombineDFT_Fiss_5562_5572_join[4]));
	ENDFOR
}

void CombineDFT_5538() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_5562_5572_split[5]), &(SplitJoin10_CombineDFT_Fiss_5562_5572_join[5]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5531() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin10_CombineDFT_Fiss_5562_5572_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5524WEIGHTED_ROUND_ROBIN_Splitter_5531));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5532() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5532WEIGHTED_ROUND_ROBIN_Splitter_5539, pop_complex(&SplitJoin10_CombineDFT_Fiss_5562_5572_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5541() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5563_5573_split[0]), &(SplitJoin12_CombineDFT_Fiss_5563_5573_join[0]));
	ENDFOR
}

void CombineDFT_5542() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5563_5573_split[1]), &(SplitJoin12_CombineDFT_Fiss_5563_5573_join[1]));
	ENDFOR
}

void CombineDFT_5543() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5563_5573_split[2]), &(SplitJoin12_CombineDFT_Fiss_5563_5573_join[2]));
	ENDFOR
}

void CombineDFT_5544() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5563_5573_split[3]), &(SplitJoin12_CombineDFT_Fiss_5563_5573_join[3]));
	ENDFOR
}

void CombineDFT_5545() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5563_5573_split[4]), &(SplitJoin12_CombineDFT_Fiss_5563_5573_join[4]));
	ENDFOR
}

void CombineDFT_5546() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5563_5573_split[5]), &(SplitJoin12_CombineDFT_Fiss_5563_5573_join[5]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5539() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin12_CombineDFT_Fiss_5563_5573_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5532WEIGHTED_ROUND_ROBIN_Splitter_5539));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5540() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5540WEIGHTED_ROUND_ROBIN_Splitter_5547, pop_complex(&SplitJoin12_CombineDFT_Fiss_5563_5573_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5549() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_5564_5574_split[0]), &(SplitJoin14_CombineDFT_Fiss_5564_5574_join[0]));
	ENDFOR
}

void CombineDFT_5550() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_5564_5574_split[1]), &(SplitJoin14_CombineDFT_Fiss_5564_5574_join[1]));
	ENDFOR
}

void CombineDFT_5551() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_5564_5574_split[2]), &(SplitJoin14_CombineDFT_Fiss_5564_5574_join[2]));
	ENDFOR
}

void CombineDFT_5552() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_5564_5574_split[3]), &(SplitJoin14_CombineDFT_Fiss_5564_5574_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5547() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin14_CombineDFT_Fiss_5564_5574_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5540WEIGHTED_ROUND_ROBIN_Splitter_5547));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5548() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5548WEIGHTED_ROUND_ROBIN_Splitter_5553, pop_complex(&SplitJoin14_CombineDFT_Fiss_5564_5574_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5555() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_5565_5575_split[0]), &(SplitJoin16_CombineDFT_Fiss_5565_5575_join[0]));
	ENDFOR
}

void CombineDFT_5556() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_5565_5575_split[1]), &(SplitJoin16_CombineDFT_Fiss_5565_5575_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5553() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_5565_5575_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5548WEIGHTED_ROUND_ROBIN_Splitter_5553));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_5565_5575_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5548WEIGHTED_ROUND_ROBIN_Splitter_5553));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5554() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5554CombineDFT_5494, pop_complex(&SplitJoin16_CombineDFT_Fiss_5565_5575_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5554CombineDFT_5494, pop_complex(&SplitJoin16_CombineDFT_Fiss_5565_5575_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_5494() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_5554CombineDFT_5494), &(CombineDFT_5494CPrinter_5495));
	ENDFOR
}

void CPrinter(buffer_complex_t *chanin) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		printf("%.10f", c.real);
		printf("\n");
		printf("%.10f", c.imag);
		printf("\n");
	}


void CPrinter_5495() {
	FOR(uint32_t, __iter_steady_, 0, <, 192, __iter_steady_++)
		CPrinter(&(CombineDFT_5494CPrinter_5495));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 4, __iter_init_0_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_5558_5568_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 6, __iter_init_1_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_5559_5569_split[__iter_init_1_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5516WEIGHTED_ROUND_ROBIN_Splitter_5523);
	FOR(int, __iter_init_2_, 0, <, 6, __iter_init_2_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_5560_5570_join[__iter_init_2_]);
	ENDFOR
	init_buffer_complex(&FFTReorderSimple_5484WEIGHTED_ROUND_ROBIN_Splitter_5497);
	FOR(int, __iter_init_3_, 0, <, 4, __iter_init_3_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_5558_5568_split[__iter_init_3_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5502WEIGHTED_ROUND_ROBIN_Splitter_5507);
	FOR(int, __iter_init_4_, 0, <, 4, __iter_init_4_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_5564_5574_join[__iter_init_4_]);
	ENDFOR
	init_buffer_complex(&FFTTestSource_5483FFTReorderSimple_5484);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5532WEIGHTED_ROUND_ROBIN_Splitter_5539);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5498WEIGHTED_ROUND_ROBIN_Splitter_5501);
	FOR(int, __iter_init_5_, 0, <, 6, __iter_init_5_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_5561_5571_join[__iter_init_5_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5540WEIGHTED_ROUND_ROBIN_Splitter_5547);
	FOR(int, __iter_init_6_, 0, <, 4, __iter_init_6_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_5564_5574_split[__iter_init_6_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5524WEIGHTED_ROUND_ROBIN_Splitter_5531);
	FOR(int, __iter_init_7_, 0, <, 6, __iter_init_7_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_5563_5573_split[__iter_init_7_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5554CombineDFT_5494);
	FOR(int, __iter_init_8_, 0, <, 6, __iter_init_8_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_5559_5569_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 2, __iter_init_9_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_5565_5575_join[__iter_init_9_]);
	ENDFOR
	init_buffer_complex(&CombineDFT_5494CPrinter_5495);
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_5557_5567_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 6, __iter_init_11_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_5561_5571_split[__iter_init_11_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5508WEIGHTED_ROUND_ROBIN_Splitter_5515);
	FOR(int, __iter_init_12_, 0, <, 6, __iter_init_12_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_5563_5573_join[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_5557_5567_split[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 6, __iter_init_14_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_5560_5570_split[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 6, __iter_init_15_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_5562_5572_join[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 6, __iter_init_16_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_5562_5572_split[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_5565_5575_split[__iter_init_17_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5548WEIGHTED_ROUND_ROBIN_Splitter_5553);
// --- init: CombineDFT_5525
	 {
	 ; 
	CombineDFT_5525_s.wn.real = -1.0 ; 
	CombineDFT_5525_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5526
	 {
	CombineDFT_5526_s.wn.real = -1.0 ; 
	CombineDFT_5526_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5527
	 {
	CombineDFT_5527_s.wn.real = -1.0 ; 
	CombineDFT_5527_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5528
	 {
	CombineDFT_5528_s.wn.real = -1.0 ; 
	CombineDFT_5528_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5529
	 {
	CombineDFT_5529_s.wn.real = -1.0 ; 
	CombineDFT_5529_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5530
	 {
	CombineDFT_5530_s.wn.real = -1.0 ; 
	CombineDFT_5530_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5533
	 {
	CombineDFT_5533_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5533_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5534
	 {
	CombineDFT_5534_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5534_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5535
	 {
	CombineDFT_5535_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5535_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5536
	 {
	CombineDFT_5536_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5536_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5537
	 {
	CombineDFT_5537_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5537_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5538
	 {
	CombineDFT_5538_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5538_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5541
	 {
	CombineDFT_5541_s.wn.real = 0.70710677 ; 
	CombineDFT_5541_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5542
	 {
	CombineDFT_5542_s.wn.real = 0.70710677 ; 
	CombineDFT_5542_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5543
	 {
	CombineDFT_5543_s.wn.real = 0.70710677 ; 
	CombineDFT_5543_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5544
	 {
	CombineDFT_5544_s.wn.real = 0.70710677 ; 
	CombineDFT_5544_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5545
	 {
	CombineDFT_5545_s.wn.real = 0.70710677 ; 
	CombineDFT_5545_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5546
	 {
	CombineDFT_5546_s.wn.real = 0.70710677 ; 
	CombineDFT_5546_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5549
	 {
	CombineDFT_5549_s.wn.real = 0.9238795 ; 
	CombineDFT_5549_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_5550
	 {
	CombineDFT_5550_s.wn.real = 0.9238795 ; 
	CombineDFT_5550_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_5551
	 {
	CombineDFT_5551_s.wn.real = 0.9238795 ; 
	CombineDFT_5551_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_5552
	 {
	CombineDFT_5552_s.wn.real = 0.9238795 ; 
	CombineDFT_5552_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_5555
	 {
	CombineDFT_5555_s.wn.real = 0.98078525 ; 
	CombineDFT_5555_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_5556
	 {
	CombineDFT_5556_s.wn.real = 0.98078525 ; 
	CombineDFT_5556_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_5494
	 {
	 ; 
	CombineDFT_5494_s.wn.real = 0.9951847 ; 
	CombineDFT_5494_s.wn.imag = -0.09801714 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FFTTestSource_5483();
		FFTReorderSimple_5484();
		WEIGHTED_ROUND_ROBIN_Splitter_5497();
			FFTReorderSimple_5499();
			FFTReorderSimple_5500();
		WEIGHTED_ROUND_ROBIN_Joiner_5498();
		WEIGHTED_ROUND_ROBIN_Splitter_5501();
			FFTReorderSimple_5503();
			FFTReorderSimple_5504();
			FFTReorderSimple_5505();
			FFTReorderSimple_5506();
		WEIGHTED_ROUND_ROBIN_Joiner_5502();
		WEIGHTED_ROUND_ROBIN_Splitter_5507();
			FFTReorderSimple_5509();
			FFTReorderSimple_5510();
			FFTReorderSimple_5511();
			FFTReorderSimple_5512();
			FFTReorderSimple_5513();
			FFTReorderSimple_5514();
		WEIGHTED_ROUND_ROBIN_Joiner_5508();
		WEIGHTED_ROUND_ROBIN_Splitter_5515();
			FFTReorderSimple_5517();
			FFTReorderSimple_5518();
			FFTReorderSimple_5519();
			FFTReorderSimple_5520();
			FFTReorderSimple_5521();
			FFTReorderSimple_5522();
		WEIGHTED_ROUND_ROBIN_Joiner_5516();
		WEIGHTED_ROUND_ROBIN_Splitter_5523();
			CombineDFT_5525();
			CombineDFT_5526();
			CombineDFT_5527();
			CombineDFT_5528();
			CombineDFT_5529();
			CombineDFT_5530();
		WEIGHTED_ROUND_ROBIN_Joiner_5524();
		WEIGHTED_ROUND_ROBIN_Splitter_5531();
			CombineDFT_5533();
			CombineDFT_5534();
			CombineDFT_5535();
			CombineDFT_5536();
			CombineDFT_5537();
			CombineDFT_5538();
		WEIGHTED_ROUND_ROBIN_Joiner_5532();
		WEIGHTED_ROUND_ROBIN_Splitter_5539();
			CombineDFT_5541();
			CombineDFT_5542();
			CombineDFT_5543();
			CombineDFT_5544();
			CombineDFT_5545();
			CombineDFT_5546();
		WEIGHTED_ROUND_ROBIN_Joiner_5540();
		WEIGHTED_ROUND_ROBIN_Splitter_5547();
			CombineDFT_5549();
			CombineDFT_5550();
			CombineDFT_5551();
			CombineDFT_5552();
		WEIGHTED_ROUND_ROBIN_Joiner_5548();
		WEIGHTED_ROUND_ROBIN_Splitter_5553();
			CombineDFT_5555();
			CombineDFT_5556();
		WEIGHTED_ROUND_ROBIN_Joiner_5554();
		CombineDFT_5494();
		CPrinter_5495();
	ENDFOR
	return EXIT_SUCCESS;
}
