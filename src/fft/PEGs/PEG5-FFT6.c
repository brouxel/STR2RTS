#include "PEG5-FFT6.h"

buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5683WEIGHTED_ROUND_ROBIN_Splitter_5688;
buffer_complex_t SplitJoin12_CombineDFT_Fiss_5698_5708_join[5];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_5697_5707_split[5];
buffer_complex_t FFTReorderSimple_5624WEIGHTED_ROUND_ROBIN_Splitter_5637;
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_5692_5702_split[2];
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_5693_5703_split[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5676WEIGHTED_ROUND_ROBIN_Splitter_5682;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5689CombineDFT_5634;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5655WEIGHTED_ROUND_ROBIN_Splitter_5661;
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_5695_5705_join[5];
buffer_complex_t FFTTestSource_5623FFTReorderSimple_5624;
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_5694_5704_split[5];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_5700_5710_join[2];
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_5693_5703_join[4];
buffer_complex_t SplitJoin12_CombineDFT_Fiss_5698_5708_split[5];
buffer_complex_t CombineDFT_5634CPrinter_5635;
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_5695_5705_split[5];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5638WEIGHTED_ROUND_ROBIN_Splitter_5641;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5648WEIGHTED_ROUND_ROBIN_Splitter_5654;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5642WEIGHTED_ROUND_ROBIN_Splitter_5647;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_5696_5706_join[5];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_5700_5710_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5662WEIGHTED_ROUND_ROBIN_Splitter_5668;
buffer_complex_t SplitJoin14_CombineDFT_Fiss_5699_5709_join[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5669WEIGHTED_ROUND_ROBIN_Splitter_5675;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_5696_5706_split[5];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_5692_5702_join[2];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_5694_5704_join[5];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_5699_5709_split[4];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_5697_5707_join[5];


CombineDFT_5663_t CombineDFT_5663_s;
CombineDFT_5663_t CombineDFT_5664_s;
CombineDFT_5663_t CombineDFT_5665_s;
CombineDFT_5663_t CombineDFT_5666_s;
CombineDFT_5663_t CombineDFT_5667_s;
CombineDFT_5663_t CombineDFT_5670_s;
CombineDFT_5663_t CombineDFT_5671_s;
CombineDFT_5663_t CombineDFT_5672_s;
CombineDFT_5663_t CombineDFT_5673_s;
CombineDFT_5663_t CombineDFT_5674_s;
CombineDFT_5663_t CombineDFT_5677_s;
CombineDFT_5663_t CombineDFT_5678_s;
CombineDFT_5663_t CombineDFT_5679_s;
CombineDFT_5663_t CombineDFT_5680_s;
CombineDFT_5663_t CombineDFT_5681_s;
CombineDFT_5663_t CombineDFT_5684_s;
CombineDFT_5663_t CombineDFT_5685_s;
CombineDFT_5663_t CombineDFT_5686_s;
CombineDFT_5663_t CombineDFT_5687_s;
CombineDFT_5663_t CombineDFT_5690_s;
CombineDFT_5663_t CombineDFT_5691_s;
CombineDFT_5663_t CombineDFT_5634_s;

void FFTTestSource(buffer_complex_t *chanout) {
		complex_t c1;
		complex_t zero;
		c1.real = 1.0 ; 
		c1.imag = 0.0 ; 
		zero.real = 0.0 ; 
		zero.imag = 0.0 ; 
		push_complex(&(*chanout), zero) ; 
		push_complex(&(*chanout), c1) ; 
		FOR(int, i, 0,  < , 62, i++) {
			push_complex(&(*chanout), zero) ; 
		}
		ENDFOR
	}


void FFTTestSource_5623() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTTestSource(&(FFTTestSource_5623FFTReorderSimple_5624));
	ENDFOR
}

void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 64, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&(*chanin)) ; 
		}
		ENDFOR
	}


void FFTReorderSimple_5624() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(FFTTestSource_5623FFTReorderSimple_5624), &(FFTReorderSimple_5624WEIGHTED_ROUND_ROBIN_Splitter_5637));
	ENDFOR
}

void FFTReorderSimple_5639() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_5692_5702_split[0]), &(SplitJoin0_FFTReorderSimple_Fiss_5692_5702_join[0]));
	ENDFOR
}

void FFTReorderSimple_5640() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_5692_5702_split[1]), &(SplitJoin0_FFTReorderSimple_Fiss_5692_5702_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5637() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_5692_5702_split[0], pop_complex(&FFTReorderSimple_5624WEIGHTED_ROUND_ROBIN_Splitter_5637));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_5692_5702_split[1], pop_complex(&FFTReorderSimple_5624WEIGHTED_ROUND_ROBIN_Splitter_5637));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5638() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5638WEIGHTED_ROUND_ROBIN_Splitter_5641, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_5692_5702_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5638WEIGHTED_ROUND_ROBIN_Splitter_5641, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_5692_5702_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_5643() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_5693_5703_split[0]), &(SplitJoin2_FFTReorderSimple_Fiss_5693_5703_join[0]));
	ENDFOR
}

void FFTReorderSimple_5644() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_5693_5703_split[1]), &(SplitJoin2_FFTReorderSimple_Fiss_5693_5703_join[1]));
	ENDFOR
}

void FFTReorderSimple_5645() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_5693_5703_split[2]), &(SplitJoin2_FFTReorderSimple_Fiss_5693_5703_join[2]));
	ENDFOR
}

void FFTReorderSimple_5646() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_5693_5703_split[3]), &(SplitJoin2_FFTReorderSimple_Fiss_5693_5703_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5641() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5693_5703_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5638WEIGHTED_ROUND_ROBIN_Splitter_5641));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5642() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5642WEIGHTED_ROUND_ROBIN_Splitter_5647, pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_5693_5703_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_5649() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_5694_5704_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_5694_5704_join[0]));
	ENDFOR
}

void FFTReorderSimple_5650() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_5694_5704_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_5694_5704_join[1]));
	ENDFOR
}

void FFTReorderSimple_5651() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_5694_5704_split[2]), &(SplitJoin4_FFTReorderSimple_Fiss_5694_5704_join[2]));
	ENDFOR
}

void FFTReorderSimple_5652() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_5694_5704_split[3]), &(SplitJoin4_FFTReorderSimple_Fiss_5694_5704_join[3]));
	ENDFOR
}

void FFTReorderSimple_5653() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_5694_5704_split[4]), &(SplitJoin4_FFTReorderSimple_Fiss_5694_5704_join[4]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5647() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5694_5704_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5642WEIGHTED_ROUND_ROBIN_Splitter_5647));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5648() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5648WEIGHTED_ROUND_ROBIN_Splitter_5654, pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_5694_5704_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_5656() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_5695_5705_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_5695_5705_join[0]));
	ENDFOR
}

void FFTReorderSimple_5657() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_5695_5705_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_5695_5705_join[1]));
	ENDFOR
}

void FFTReorderSimple_5658() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_5695_5705_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_5695_5705_join[2]));
	ENDFOR
}

void FFTReorderSimple_5659() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_5695_5705_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_5695_5705_join[3]));
	ENDFOR
}

void FFTReorderSimple_5660() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_5695_5705_split[4]), &(SplitJoin6_FFTReorderSimple_Fiss_5695_5705_join[4]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5654() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5695_5705_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5648WEIGHTED_ROUND_ROBIN_Splitter_5654));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5655() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5655WEIGHTED_ROUND_ROBIN_Splitter_5661, pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_5695_5705_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&(*chanin), (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5663_s.wn.real) - (w.imag * CombineDFT_5663_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5663_s.wn.imag) + (w.imag * CombineDFT_5663_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&(*chanin)) ; 
			push_complex(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_5663() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_5696_5706_split[0]), &(SplitJoin8_CombineDFT_Fiss_5696_5706_join[0]));
	ENDFOR
}

void CombineDFT_5664() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_5696_5706_split[1]), &(SplitJoin8_CombineDFT_Fiss_5696_5706_join[1]));
	ENDFOR
}

void CombineDFT_5665() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_5696_5706_split[2]), &(SplitJoin8_CombineDFT_Fiss_5696_5706_join[2]));
	ENDFOR
}

void CombineDFT_5666() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_5696_5706_split[3]), &(SplitJoin8_CombineDFT_Fiss_5696_5706_join[3]));
	ENDFOR
}

void CombineDFT_5667() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_5696_5706_split[4]), &(SplitJoin8_CombineDFT_Fiss_5696_5706_join[4]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5661() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 5, __iter_++)
			push_complex(&SplitJoin8_CombineDFT_Fiss_5696_5706_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5655WEIGHTED_ROUND_ROBIN_Splitter_5661));
			push_complex(&SplitJoin8_CombineDFT_Fiss_5696_5706_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5655WEIGHTED_ROUND_ROBIN_Splitter_5661));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5662() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 5, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5662WEIGHTED_ROUND_ROBIN_Splitter_5668, pop_complex(&SplitJoin8_CombineDFT_Fiss_5696_5706_join[__iter_]));
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5662WEIGHTED_ROUND_ROBIN_Splitter_5668, pop_complex(&SplitJoin8_CombineDFT_Fiss_5696_5706_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_5670() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_5697_5707_split[0]), &(SplitJoin10_CombineDFT_Fiss_5697_5707_join[0]));
	ENDFOR
}

void CombineDFT_5671() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_5697_5707_split[1]), &(SplitJoin10_CombineDFT_Fiss_5697_5707_join[1]));
	ENDFOR
}

void CombineDFT_5672() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_5697_5707_split[2]), &(SplitJoin10_CombineDFT_Fiss_5697_5707_join[2]));
	ENDFOR
}

void CombineDFT_5673() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_5697_5707_split[3]), &(SplitJoin10_CombineDFT_Fiss_5697_5707_join[3]));
	ENDFOR
}

void CombineDFT_5674() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_5697_5707_split[4]), &(SplitJoin10_CombineDFT_Fiss_5697_5707_join[4]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5668() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin10_CombineDFT_Fiss_5697_5707_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5662WEIGHTED_ROUND_ROBIN_Splitter_5668));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5669() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5669WEIGHTED_ROUND_ROBIN_Splitter_5675, pop_complex(&SplitJoin10_CombineDFT_Fiss_5697_5707_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5677() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5698_5708_split[0]), &(SplitJoin12_CombineDFT_Fiss_5698_5708_join[0]));
	ENDFOR
}

void CombineDFT_5678() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5698_5708_split[1]), &(SplitJoin12_CombineDFT_Fiss_5698_5708_join[1]));
	ENDFOR
}

void CombineDFT_5679() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5698_5708_split[2]), &(SplitJoin12_CombineDFT_Fiss_5698_5708_join[2]));
	ENDFOR
}

void CombineDFT_5680() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5698_5708_split[3]), &(SplitJoin12_CombineDFT_Fiss_5698_5708_join[3]));
	ENDFOR
}

void CombineDFT_5681() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_5698_5708_split[4]), &(SplitJoin12_CombineDFT_Fiss_5698_5708_join[4]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5675() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin12_CombineDFT_Fiss_5698_5708_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5669WEIGHTED_ROUND_ROBIN_Splitter_5675));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5676() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5676WEIGHTED_ROUND_ROBIN_Splitter_5682, pop_complex(&SplitJoin12_CombineDFT_Fiss_5698_5708_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5684() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_5699_5709_split[0]), &(SplitJoin14_CombineDFT_Fiss_5699_5709_join[0]));
	ENDFOR
}

void CombineDFT_5685() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_5699_5709_split[1]), &(SplitJoin14_CombineDFT_Fiss_5699_5709_join[1]));
	ENDFOR
}

void CombineDFT_5686() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_5699_5709_split[2]), &(SplitJoin14_CombineDFT_Fiss_5699_5709_join[2]));
	ENDFOR
}

void CombineDFT_5687() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_5699_5709_split[3]), &(SplitJoin14_CombineDFT_Fiss_5699_5709_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5682() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin14_CombineDFT_Fiss_5699_5709_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5676WEIGHTED_ROUND_ROBIN_Splitter_5682));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5683() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5683WEIGHTED_ROUND_ROBIN_Splitter_5688, pop_complex(&SplitJoin14_CombineDFT_Fiss_5699_5709_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5690() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_5700_5710_split[0]), &(SplitJoin16_CombineDFT_Fiss_5700_5710_join[0]));
	ENDFOR
}

void CombineDFT_5691() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_5700_5710_split[1]), &(SplitJoin16_CombineDFT_Fiss_5700_5710_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5688() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_5700_5710_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5683WEIGHTED_ROUND_ROBIN_Splitter_5688));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_5700_5710_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5683WEIGHTED_ROUND_ROBIN_Splitter_5688));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5689() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5689CombineDFT_5634, pop_complex(&SplitJoin16_CombineDFT_Fiss_5700_5710_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5689CombineDFT_5634, pop_complex(&SplitJoin16_CombineDFT_Fiss_5700_5710_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_5634() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_5689CombineDFT_5634), &(CombineDFT_5634CPrinter_5635));
	ENDFOR
}

void CPrinter(buffer_complex_t *chanin) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		printf("%.10f", c.real);
		printf("\n");
		printf("%.10f", c.imag);
		printf("\n");
	}


void CPrinter_5635() {
	FOR(uint32_t, __iter_steady_, 0, <, 320, __iter_steady_++)
		CPrinter(&(CombineDFT_5634CPrinter_5635));
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5683WEIGHTED_ROUND_ROBIN_Splitter_5688);
	FOR(int, __iter_init_0_, 0, <, 5, __iter_init_0_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_5698_5708_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 5, __iter_init_1_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_5697_5707_split[__iter_init_1_]);
	ENDFOR
	init_buffer_complex(&FFTReorderSimple_5624WEIGHTED_ROUND_ROBIN_Splitter_5637);
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_5692_5702_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 4, __iter_init_3_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_5693_5703_split[__iter_init_3_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5676WEIGHTED_ROUND_ROBIN_Splitter_5682);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5689CombineDFT_5634);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5655WEIGHTED_ROUND_ROBIN_Splitter_5661);
	FOR(int, __iter_init_4_, 0, <, 5, __iter_init_4_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_5695_5705_join[__iter_init_4_]);
	ENDFOR
	init_buffer_complex(&FFTTestSource_5623FFTReorderSimple_5624);
	FOR(int, __iter_init_5_, 0, <, 5, __iter_init_5_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_5694_5704_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 2, __iter_init_6_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_5700_5710_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 4, __iter_init_7_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_5693_5703_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 5, __iter_init_8_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_5698_5708_split[__iter_init_8_]);
	ENDFOR
	init_buffer_complex(&CombineDFT_5634CPrinter_5635);
	FOR(int, __iter_init_9_, 0, <, 5, __iter_init_9_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_5695_5705_split[__iter_init_9_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5638WEIGHTED_ROUND_ROBIN_Splitter_5641);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5648WEIGHTED_ROUND_ROBIN_Splitter_5654);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5642WEIGHTED_ROUND_ROBIN_Splitter_5647);
	FOR(int, __iter_init_10_, 0, <, 5, __iter_init_10_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_5696_5706_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 2, __iter_init_11_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_5700_5710_split[__iter_init_11_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5662WEIGHTED_ROUND_ROBIN_Splitter_5668);
	FOR(int, __iter_init_12_, 0, <, 4, __iter_init_12_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_5699_5709_join[__iter_init_12_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5669WEIGHTED_ROUND_ROBIN_Splitter_5675);
	FOR(int, __iter_init_13_, 0, <, 5, __iter_init_13_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_5696_5706_split[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_5692_5702_join[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 5, __iter_init_15_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_5694_5704_join[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 4, __iter_init_16_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_5699_5709_split[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 5, __iter_init_17_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_5697_5707_join[__iter_init_17_]);
	ENDFOR
// --- init: CombineDFT_5663
	 {
	 ; 
	CombineDFT_5663_s.wn.real = -1.0 ; 
	CombineDFT_5663_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5664
	 {
	CombineDFT_5664_s.wn.real = -1.0 ; 
	CombineDFT_5664_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5665
	 {
	CombineDFT_5665_s.wn.real = -1.0 ; 
	CombineDFT_5665_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5666
	 {
	CombineDFT_5666_s.wn.real = -1.0 ; 
	CombineDFT_5666_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5667
	 {
	CombineDFT_5667_s.wn.real = -1.0 ; 
	CombineDFT_5667_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5670
	 {
	CombineDFT_5670_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5670_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5671
	 {
	CombineDFT_5671_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5671_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5672
	 {
	CombineDFT_5672_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5672_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5673
	 {
	CombineDFT_5673_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5673_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5674
	 {
	CombineDFT_5674_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5674_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5677
	 {
	CombineDFT_5677_s.wn.real = 0.70710677 ; 
	CombineDFT_5677_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5678
	 {
	CombineDFT_5678_s.wn.real = 0.70710677 ; 
	CombineDFT_5678_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5679
	 {
	CombineDFT_5679_s.wn.real = 0.70710677 ; 
	CombineDFT_5679_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5680
	 {
	CombineDFT_5680_s.wn.real = 0.70710677 ; 
	CombineDFT_5680_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5681
	 {
	CombineDFT_5681_s.wn.real = 0.70710677 ; 
	CombineDFT_5681_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5684
	 {
	CombineDFT_5684_s.wn.real = 0.9238795 ; 
	CombineDFT_5684_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_5685
	 {
	CombineDFT_5685_s.wn.real = 0.9238795 ; 
	CombineDFT_5685_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_5686
	 {
	CombineDFT_5686_s.wn.real = 0.9238795 ; 
	CombineDFT_5686_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_5687
	 {
	CombineDFT_5687_s.wn.real = 0.9238795 ; 
	CombineDFT_5687_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_5690
	 {
	CombineDFT_5690_s.wn.real = 0.98078525 ; 
	CombineDFT_5690_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_5691
	 {
	CombineDFT_5691_s.wn.real = 0.98078525 ; 
	CombineDFT_5691_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_5634
	 {
	 ; 
	CombineDFT_5634_s.wn.real = 0.9951847 ; 
	CombineDFT_5634_s.wn.imag = -0.09801714 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FFTTestSource_5623();
		FFTReorderSimple_5624();
		WEIGHTED_ROUND_ROBIN_Splitter_5637();
			FFTReorderSimple_5639();
			FFTReorderSimple_5640();
		WEIGHTED_ROUND_ROBIN_Joiner_5638();
		WEIGHTED_ROUND_ROBIN_Splitter_5641();
			FFTReorderSimple_5643();
			FFTReorderSimple_5644();
			FFTReorderSimple_5645();
			FFTReorderSimple_5646();
		WEIGHTED_ROUND_ROBIN_Joiner_5642();
		WEIGHTED_ROUND_ROBIN_Splitter_5647();
			FFTReorderSimple_5649();
			FFTReorderSimple_5650();
			FFTReorderSimple_5651();
			FFTReorderSimple_5652();
			FFTReorderSimple_5653();
		WEIGHTED_ROUND_ROBIN_Joiner_5648();
		WEIGHTED_ROUND_ROBIN_Splitter_5654();
			FFTReorderSimple_5656();
			FFTReorderSimple_5657();
			FFTReorderSimple_5658();
			FFTReorderSimple_5659();
			FFTReorderSimple_5660();
		WEIGHTED_ROUND_ROBIN_Joiner_5655();
		WEIGHTED_ROUND_ROBIN_Splitter_5661();
			CombineDFT_5663();
			CombineDFT_5664();
			CombineDFT_5665();
			CombineDFT_5666();
			CombineDFT_5667();
		WEIGHTED_ROUND_ROBIN_Joiner_5662();
		WEIGHTED_ROUND_ROBIN_Splitter_5668();
			CombineDFT_5670();
			CombineDFT_5671();
			CombineDFT_5672();
			CombineDFT_5673();
			CombineDFT_5674();
		WEIGHTED_ROUND_ROBIN_Joiner_5669();
		WEIGHTED_ROUND_ROBIN_Splitter_5675();
			CombineDFT_5677();
			CombineDFT_5678();
			CombineDFT_5679();
			CombineDFT_5680();
			CombineDFT_5681();
		WEIGHTED_ROUND_ROBIN_Joiner_5676();
		WEIGHTED_ROUND_ROBIN_Splitter_5682();
			CombineDFT_5684();
			CombineDFT_5685();
			CombineDFT_5686();
			CombineDFT_5687();
		WEIGHTED_ROUND_ROBIN_Joiner_5683();
		WEIGHTED_ROUND_ROBIN_Splitter_5688();
			CombineDFT_5690();
			CombineDFT_5691();
		WEIGHTED_ROUND_ROBIN_Joiner_5689();
		CombineDFT_5634();
		CPrinter_5635();
	ENDFOR
	return EXIT_SUCCESS;
}
