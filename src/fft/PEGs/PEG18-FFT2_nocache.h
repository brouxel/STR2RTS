#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=4608 on the compile command line
#else
#if BUF_SIZEMAX < 4608
#error BUF_SIZEMAX too small, it must be at least 4608
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float w[2];
} CombineDFT_6869_t;

typedef struct {
	float w[4];
} CombineDFT_6889_t;

typedef struct {
	float w[8];
} CombineDFT_6907_t;

typedef struct {
	float w[16];
} CombineDFT_6917_t;

typedef struct {
	float w[32];
} CombineDFT_6923_t;

typedef struct {
	float w[64];
} CombineDFT_6804_t;
void WEIGHTED_ROUND_ROBIN_Splitter_6825();
void FFTTestSource_6827();
void FFTTestSource_6828();
void WEIGHTED_ROUND_ROBIN_Joiner_6826();
void WEIGHTED_ROUND_ROBIN_Splitter_6817();
void FFTReorderSimple_6794();
void WEIGHTED_ROUND_ROBIN_Splitter_6829();
void FFTReorderSimple_6831();
void FFTReorderSimple_6832();
void WEIGHTED_ROUND_ROBIN_Joiner_6830();
void WEIGHTED_ROUND_ROBIN_Splitter_6833();
void FFTReorderSimple_6835();
void FFTReorderSimple_6836();
void FFTReorderSimple_6837();
void FFTReorderSimple_6838();
void WEIGHTED_ROUND_ROBIN_Joiner_6834();
void WEIGHTED_ROUND_ROBIN_Splitter_6839();
void FFTReorderSimple_6841();
void FFTReorderSimple_6842();
void FFTReorderSimple_6843();
void FFTReorderSimple_6844();
void FFTReorderSimple_6845();
void FFTReorderSimple_6846();
void FFTReorderSimple_6847();
void FFTReorderSimple_6848();
void WEIGHTED_ROUND_ROBIN_Joiner_6840();
void WEIGHTED_ROUND_ROBIN_Splitter_6849();
void FFTReorderSimple_6851();
void FFTReorderSimple_6852();
void FFTReorderSimple_6853();
void FFTReorderSimple_6854();
void FFTReorderSimple_6855();
void FFTReorderSimple_6856();
void FFTReorderSimple_6857();
void FFTReorderSimple_6858();
void FFTReorderSimple_6859();
void FFTReorderSimple_6860();
void FFTReorderSimple_6861();
void FFTReorderSimple_6862();
void FFTReorderSimple_6863();
void FFTReorderSimple_6864();
void FFTReorderSimple_6865();
void FFTReorderSimple_6866();
void WEIGHTED_ROUND_ROBIN_Joiner_6850();
void WEIGHTED_ROUND_ROBIN_Splitter_6867();
void CombineDFT_6869();
void CombineDFT_6870();
void CombineDFT_6871();
void CombineDFT_6872();
void CombineDFT_6873();
void CombineDFT_6874();
void CombineDFT_6875();
void CombineDFT_6876();
void CombineDFT_6877();
void CombineDFT_6878();
void CombineDFT_6879();
void CombineDFT_6880();
void CombineDFT_6881();
void CombineDFT_6882();
void CombineDFT_6883();
void CombineDFT_6884();
void CombineDFT_6885();
void CombineDFT_6886();
void WEIGHTED_ROUND_ROBIN_Joiner_6868();
void WEIGHTED_ROUND_ROBIN_Splitter_6887();
void CombineDFT_6889();
void CombineDFT_6890();
void CombineDFT_6891();
void CombineDFT_6892();
void CombineDFT_6893();
void CombineDFT_6894();
void CombineDFT_6895();
void CombineDFT_6896();
void CombineDFT_6897();
void CombineDFT_6898();
void CombineDFT_6899();
void CombineDFT_6900();
void CombineDFT_6901();
void CombineDFT_6902();
void CombineDFT_6903();
void CombineDFT_6904();
void WEIGHTED_ROUND_ROBIN_Joiner_6888();
void WEIGHTED_ROUND_ROBIN_Splitter_6905();
void CombineDFT_6907();
void CombineDFT_6908();
void CombineDFT_6909();
void CombineDFT_6910();
void CombineDFT_6911();
void CombineDFT_6912();
void CombineDFT_6913();
void CombineDFT_6914();
void WEIGHTED_ROUND_ROBIN_Joiner_6906();
void WEIGHTED_ROUND_ROBIN_Splitter_6915();
void CombineDFT_6917();
void CombineDFT_6918();
void CombineDFT_6919();
void CombineDFT_6920();
void WEIGHTED_ROUND_ROBIN_Joiner_6916();
void WEIGHTED_ROUND_ROBIN_Splitter_6921();
void CombineDFT_6923();
void CombineDFT_6924();
void WEIGHTED_ROUND_ROBIN_Joiner_6922();
void CombineDFT_6804();
void FFTReorderSimple_6805();
void WEIGHTED_ROUND_ROBIN_Splitter_6925();
void FFTReorderSimple_6927();
void FFTReorderSimple_6928();
void WEIGHTED_ROUND_ROBIN_Joiner_6926();
void WEIGHTED_ROUND_ROBIN_Splitter_6929();
void FFTReorderSimple_6931();
void FFTReorderSimple_6932();
void FFTReorderSimple_6933();
void FFTReorderSimple_6934();
void WEIGHTED_ROUND_ROBIN_Joiner_6930();
void WEIGHTED_ROUND_ROBIN_Splitter_6935();
void FFTReorderSimple_6937();
void FFTReorderSimple_6938();
void FFTReorderSimple_6939();
void FFTReorderSimple_6940();
void FFTReorderSimple_6941();
void FFTReorderSimple_6942();
void FFTReorderSimple_6943();
void FFTReorderSimple_6944();
void WEIGHTED_ROUND_ROBIN_Joiner_6936();
void WEIGHTED_ROUND_ROBIN_Splitter_6945();
void FFTReorderSimple_6947();
void FFTReorderSimple_6948();
void FFTReorderSimple_6949();
void FFTReorderSimple_6950();
void FFTReorderSimple_6951();
void FFTReorderSimple_6952();
void FFTReorderSimple_6953();
void FFTReorderSimple_6954();
void FFTReorderSimple_6955();
void FFTReorderSimple_6956();
void FFTReorderSimple_6957();
void FFTReorderSimple_6958();
void FFTReorderSimple_6959();
void FFTReorderSimple_6960();
void FFTReorderSimple_6961();
void FFTReorderSimple_6962();
void WEIGHTED_ROUND_ROBIN_Joiner_6946();
void WEIGHTED_ROUND_ROBIN_Splitter_6963();
void CombineDFT_6965();
void CombineDFT_6966();
void CombineDFT_6967();
void CombineDFT_6968();
void CombineDFT_6969();
void CombineDFT_6970();
void CombineDFT_6971();
void CombineDFT_6972();
void CombineDFT_6973();
void CombineDFT_6974();
void CombineDFT_6975();
void CombineDFT_6976();
void CombineDFT_6977();
void CombineDFT_6978();
void CombineDFT_6979();
void CombineDFT_6980();
void CombineDFT_6981();
void CombineDFT_6982();
void WEIGHTED_ROUND_ROBIN_Joiner_6964();
void WEIGHTED_ROUND_ROBIN_Splitter_6983();
void CombineDFT_6985();
void CombineDFT_6986();
void CombineDFT_6987();
void CombineDFT_6988();
void CombineDFT_6989();
void CombineDFT_6990();
void CombineDFT_6991();
void CombineDFT_6992();
void CombineDFT_6993();
void CombineDFT_6994();
void CombineDFT_6995();
void CombineDFT_6996();
void CombineDFT_6997();
void CombineDFT_6998();
void CombineDFT_6999();
void CombineDFT_7000();
void WEIGHTED_ROUND_ROBIN_Joiner_6984();
void WEIGHTED_ROUND_ROBIN_Splitter_7001();
void CombineDFT_7003();
void CombineDFT_7004();
void CombineDFT_7005();
void CombineDFT_7006();
void CombineDFT_7007();
void CombineDFT_7008();
void CombineDFT_7009();
void CombineDFT_7010();
void WEIGHTED_ROUND_ROBIN_Joiner_7002();
void WEIGHTED_ROUND_ROBIN_Splitter_7011();
void CombineDFT_7013();
void CombineDFT_7014();
void CombineDFT_7015();
void CombineDFT_7016();
void WEIGHTED_ROUND_ROBIN_Joiner_7012();
void WEIGHTED_ROUND_ROBIN_Splitter_7017();
void CombineDFT_7019();
void CombineDFT_7020();
void WEIGHTED_ROUND_ROBIN_Joiner_7018();
void CombineDFT_6815();
void WEIGHTED_ROUND_ROBIN_Joiner_6818();
void FloatPrinter_6816();

#ifdef __cplusplus
}
#endif
#endif
