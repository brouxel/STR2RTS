#include "PEG30-FFT2.h"

buffer_float_t SplitJoin112_CombineDFT_Fiss_1432_1453_join[30];
buffer_float_t SplitJoin0_FFTTestSource_Fiss_1417_1438_split[2];
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_1422_1443_split[16];
buffer_float_t SplitJoin112_CombineDFT_Fiss_1432_1453_split[30];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1190FloatPrinter_1188;
buffer_float_t SplitJoin18_CombineDFT_Fiss_1426_1447_split[4];
buffer_float_t SplitJoin110_FFTReorderSimple_Fiss_1431_1452_join[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1206WEIGHTED_ROUND_ROBIN_Splitter_1211;
buffer_float_t SplitJoin118_CombineDFT_Fiss_1435_1456_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1314WEIGHTED_ROUND_ROBIN_Splitter_1319;
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_1155_1191_1418_1439_split[2];
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_1419_1440_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1306CombineDFT_1176;
buffer_float_t SplitJoin12_CombineDFT_Fiss_1423_1444_split[30];
buffer_float_t FFTReorderSimple_1177WEIGHTED_ROUND_ROBIN_Splitter_1309;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1408WEIGHTED_ROUND_ROBIN_Splitter_1413;
buffer_float_t SplitJoin18_CombineDFT_Fiss_1426_1447_join[4];
buffer_float_t SplitJoin12_CombineDFT_Fiss_1423_1444_join[30];
buffer_float_t SplitJoin118_CombineDFT_Fiss_1435_1456_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1330WEIGHTED_ROUND_ROBIN_Splitter_1347;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1212WEIGHTED_ROUND_ROBIN_Splitter_1221;
buffer_float_t SplitJoin120_CombineDFT_Fiss_1436_1457_join[2];
buffer_float_t SplitJoin114_CombineDFT_Fiss_1433_1454_join[16];
buffer_float_t SplitJoin106_FFTReorderSimple_Fiss_1429_1450_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1310WEIGHTED_ROUND_ROBIN_Splitter_1313;
buffer_float_t SplitJoin20_CombineDFT_Fiss_1427_1448_join[2];
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_1420_1441_split[4];
buffer_float_t SplitJoin14_CombineDFT_Fiss_1424_1445_join[16];
buffer_float_t SplitJoin110_FFTReorderSimple_Fiss_1431_1452_split[16];
buffer_float_t SplitJoin116_CombineDFT_Fiss_1434_1455_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1290WEIGHTED_ROUND_ROBIN_Splitter_1299;
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_1421_1442_join[8];
buffer_float_t SplitJoin108_FFTReorderSimple_Fiss_1430_1451_join[8];
buffer_float_t SplitJoin0_FFTTestSource_Fiss_1417_1438_join[2];
buffer_float_t SplitJoin20_CombineDFT_Fiss_1427_1448_split[2];
buffer_float_t FFTReorderSimple_1166WEIGHTED_ROUND_ROBIN_Splitter_1201;
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_1421_1442_split[8];
buffer_float_t SplitJoin116_CombineDFT_Fiss_1434_1455_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1198WEIGHTED_ROUND_ROBIN_Splitter_1189;
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_1419_1440_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1202WEIGHTED_ROUND_ROBIN_Splitter_1205;
buffer_float_t SplitJoin104_FFTReorderSimple_Fiss_1428_1449_join[2];
buffer_float_t SplitJoin16_CombineDFT_Fiss_1425_1446_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1240WEIGHTED_ROUND_ROBIN_Splitter_1271;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1348WEIGHTED_ROUND_ROBIN_Splitter_1379;
buffer_float_t SplitJoin14_CombineDFT_Fiss_1424_1445_split[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1398WEIGHTED_ROUND_ROBIN_Splitter_1407;
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_1420_1441_join[4];
buffer_float_t SplitJoin114_CombineDFT_Fiss_1433_1454_split[16];
buffer_float_t SplitJoin120_CombineDFT_Fiss_1436_1457_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1272WEIGHTED_ROUND_ROBIN_Splitter_1289;
buffer_float_t SplitJoin106_FFTReorderSimple_Fiss_1429_1450_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1222WEIGHTED_ROUND_ROBIN_Splitter_1239;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1380WEIGHTED_ROUND_ROBIN_Splitter_1397;
buffer_float_t SplitJoin108_FFTReorderSimple_Fiss_1430_1451_split[8];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_1155_1191_1418_1439_join[2];
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_1422_1443_join[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1300WEIGHTED_ROUND_ROBIN_Splitter_1305;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1320WEIGHTED_ROUND_ROBIN_Splitter_1329;
buffer_float_t SplitJoin16_CombineDFT_Fiss_1425_1446_split[8];
buffer_float_t SplitJoin104_FFTReorderSimple_Fiss_1428_1449_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1414CombineDFT_1187;


CombineDFT_1241_t CombineDFT_1241_s;
CombineDFT_1241_t CombineDFT_1242_s;
CombineDFT_1241_t CombineDFT_1243_s;
CombineDFT_1241_t CombineDFT_1244_s;
CombineDFT_1241_t CombineDFT_1245_s;
CombineDFT_1241_t CombineDFT_1246_s;
CombineDFT_1241_t CombineDFT_1247_s;
CombineDFT_1241_t CombineDFT_1248_s;
CombineDFT_1241_t CombineDFT_1249_s;
CombineDFT_1241_t CombineDFT_1250_s;
CombineDFT_1241_t CombineDFT_1251_s;
CombineDFT_1241_t CombineDFT_1252_s;
CombineDFT_1241_t CombineDFT_1253_s;
CombineDFT_1241_t CombineDFT_1254_s;
CombineDFT_1241_t CombineDFT_1255_s;
CombineDFT_1241_t CombineDFT_1256_s;
CombineDFT_1241_t CombineDFT_1257_s;
CombineDFT_1241_t CombineDFT_1258_s;
CombineDFT_1241_t CombineDFT_1259_s;
CombineDFT_1241_t CombineDFT_1260_s;
CombineDFT_1241_t CombineDFT_1261_s;
CombineDFT_1241_t CombineDFT_1262_s;
CombineDFT_1241_t CombineDFT_1263_s;
CombineDFT_1241_t CombineDFT_1264_s;
CombineDFT_1241_t CombineDFT_1265_s;
CombineDFT_1241_t CombineDFT_1266_s;
CombineDFT_1241_t CombineDFT_1267_s;
CombineDFT_1241_t CombineDFT_1268_s;
CombineDFT_1241_t CombineDFT_1269_s;
CombineDFT_1241_t CombineDFT_1270_s;
CombineDFT_1273_t CombineDFT_1273_s;
CombineDFT_1273_t CombineDFT_1274_s;
CombineDFT_1273_t CombineDFT_1275_s;
CombineDFT_1273_t CombineDFT_1276_s;
CombineDFT_1273_t CombineDFT_1277_s;
CombineDFT_1273_t CombineDFT_1278_s;
CombineDFT_1273_t CombineDFT_1279_s;
CombineDFT_1273_t CombineDFT_1280_s;
CombineDFT_1273_t CombineDFT_1281_s;
CombineDFT_1273_t CombineDFT_1282_s;
CombineDFT_1273_t CombineDFT_1283_s;
CombineDFT_1273_t CombineDFT_1284_s;
CombineDFT_1273_t CombineDFT_1285_s;
CombineDFT_1273_t CombineDFT_1286_s;
CombineDFT_1273_t CombineDFT_1287_s;
CombineDFT_1273_t CombineDFT_1288_s;
CombineDFT_1291_t CombineDFT_1291_s;
CombineDFT_1291_t CombineDFT_1292_s;
CombineDFT_1291_t CombineDFT_1293_s;
CombineDFT_1291_t CombineDFT_1294_s;
CombineDFT_1291_t CombineDFT_1295_s;
CombineDFT_1291_t CombineDFT_1296_s;
CombineDFT_1291_t CombineDFT_1297_s;
CombineDFT_1291_t CombineDFT_1298_s;
CombineDFT_1301_t CombineDFT_1301_s;
CombineDFT_1301_t CombineDFT_1302_s;
CombineDFT_1301_t CombineDFT_1303_s;
CombineDFT_1301_t CombineDFT_1304_s;
CombineDFT_1307_t CombineDFT_1307_s;
CombineDFT_1307_t CombineDFT_1308_s;
CombineDFT_1176_t CombineDFT_1176_s;
CombineDFT_1241_t CombineDFT_1349_s;
CombineDFT_1241_t CombineDFT_1350_s;
CombineDFT_1241_t CombineDFT_1351_s;
CombineDFT_1241_t CombineDFT_1352_s;
CombineDFT_1241_t CombineDFT_1353_s;
CombineDFT_1241_t CombineDFT_1354_s;
CombineDFT_1241_t CombineDFT_1355_s;
CombineDFT_1241_t CombineDFT_1356_s;
CombineDFT_1241_t CombineDFT_1357_s;
CombineDFT_1241_t CombineDFT_1358_s;
CombineDFT_1241_t CombineDFT_1359_s;
CombineDFT_1241_t CombineDFT_1360_s;
CombineDFT_1241_t CombineDFT_1361_s;
CombineDFT_1241_t CombineDFT_1362_s;
CombineDFT_1241_t CombineDFT_1363_s;
CombineDFT_1241_t CombineDFT_1364_s;
CombineDFT_1241_t CombineDFT_1365_s;
CombineDFT_1241_t CombineDFT_1366_s;
CombineDFT_1241_t CombineDFT_1367_s;
CombineDFT_1241_t CombineDFT_1368_s;
CombineDFT_1241_t CombineDFT_1369_s;
CombineDFT_1241_t CombineDFT_1370_s;
CombineDFT_1241_t CombineDFT_1371_s;
CombineDFT_1241_t CombineDFT_1372_s;
CombineDFT_1241_t CombineDFT_1373_s;
CombineDFT_1241_t CombineDFT_1374_s;
CombineDFT_1241_t CombineDFT_1375_s;
CombineDFT_1241_t CombineDFT_1376_s;
CombineDFT_1241_t CombineDFT_1377_s;
CombineDFT_1241_t CombineDFT_1378_s;
CombineDFT_1273_t CombineDFT_1381_s;
CombineDFT_1273_t CombineDFT_1382_s;
CombineDFT_1273_t CombineDFT_1383_s;
CombineDFT_1273_t CombineDFT_1384_s;
CombineDFT_1273_t CombineDFT_1385_s;
CombineDFT_1273_t CombineDFT_1386_s;
CombineDFT_1273_t CombineDFT_1387_s;
CombineDFT_1273_t CombineDFT_1388_s;
CombineDFT_1273_t CombineDFT_1389_s;
CombineDFT_1273_t CombineDFT_1390_s;
CombineDFT_1273_t CombineDFT_1391_s;
CombineDFT_1273_t CombineDFT_1392_s;
CombineDFT_1273_t CombineDFT_1393_s;
CombineDFT_1273_t CombineDFT_1394_s;
CombineDFT_1273_t CombineDFT_1395_s;
CombineDFT_1273_t CombineDFT_1396_s;
CombineDFT_1291_t CombineDFT_1399_s;
CombineDFT_1291_t CombineDFT_1400_s;
CombineDFT_1291_t CombineDFT_1401_s;
CombineDFT_1291_t CombineDFT_1402_s;
CombineDFT_1291_t CombineDFT_1403_s;
CombineDFT_1291_t CombineDFT_1404_s;
CombineDFT_1291_t CombineDFT_1405_s;
CombineDFT_1291_t CombineDFT_1406_s;
CombineDFT_1301_t CombineDFT_1409_s;
CombineDFT_1301_t CombineDFT_1410_s;
CombineDFT_1301_t CombineDFT_1411_s;
CombineDFT_1301_t CombineDFT_1412_s;
CombineDFT_1307_t CombineDFT_1415_s;
CombineDFT_1307_t CombineDFT_1416_s;
CombineDFT_1176_t CombineDFT_1187_s;

void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), 0.0) ; 
		push_float(&(*chanout), 0.0) ; 
		push_float(&(*chanout), 1.0) ; 
		push_float(&(*chanout), 0.0) ; 
		FOR(int, i, 0,  < , 124, i++) {
			push_float(&(*chanout), 0.0) ; 
		}
		ENDFOR
	}


void FFTTestSource_1199() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_1417_1438_split[0]), &(SplitJoin0_FFTTestSource_Fiss_1417_1438_join[0]));
	ENDFOR
}

void FFTTestSource_1200() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_1417_1438_split[1]), &(SplitJoin0_FFTTestSource_Fiss_1417_1438_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1197() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_1198() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1198WEIGHTED_ROUND_ROBIN_Splitter_1189, pop_float(&SplitJoin0_FFTTestSource_Fiss_1417_1438_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1198WEIGHTED_ROUND_ROBIN_Splitter_1189, pop_float(&SplitJoin0_FFTTestSource_Fiss_1417_1438_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, i, 0,  < , 128, i = (i + 4)) {
			push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 128, i = (i + 4)) {
			push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_float(&(*chanin)) ; 
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void FFTReorderSimple_1166() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_1155_1191_1418_1439_split[0]), &(FFTReorderSimple_1166WEIGHTED_ROUND_ROBIN_Splitter_1201));
	ENDFOR
}

void FFTReorderSimple_1203() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_1419_1440_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_1419_1440_join[0]));
	ENDFOR
}

void FFTReorderSimple_1204() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_1419_1440_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_1419_1440_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1201() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_1419_1440_split[0], pop_float(&FFTReorderSimple_1166WEIGHTED_ROUND_ROBIN_Splitter_1201));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_1419_1440_split[1], pop_float(&FFTReorderSimple_1166WEIGHTED_ROUND_ROBIN_Splitter_1201));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1202() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1202WEIGHTED_ROUND_ROBIN_Splitter_1205, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_1419_1440_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1202WEIGHTED_ROUND_ROBIN_Splitter_1205, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_1419_1440_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_1207() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1420_1441_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_1420_1441_join[0]));
	ENDFOR
}

void FFTReorderSimple_1208() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1420_1441_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_1420_1441_join[1]));
	ENDFOR
}

void FFTReorderSimple_1209() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1420_1441_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_1420_1441_join[2]));
	ENDFOR
}

void FFTReorderSimple_1210() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1420_1441_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_1420_1441_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1205() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin6_FFTReorderSimple_Fiss_1420_1441_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1202WEIGHTED_ROUND_ROBIN_Splitter_1205));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1206() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1206WEIGHTED_ROUND_ROBIN_Splitter_1211, pop_float(&SplitJoin6_FFTReorderSimple_Fiss_1420_1441_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_1213() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_1421_1442_split[0]), &(SplitJoin8_FFTReorderSimple_Fiss_1421_1442_join[0]));
	ENDFOR
}

void FFTReorderSimple_1214() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_1421_1442_split[1]), &(SplitJoin8_FFTReorderSimple_Fiss_1421_1442_join[1]));
	ENDFOR
}

void FFTReorderSimple_1215() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_1421_1442_split[2]), &(SplitJoin8_FFTReorderSimple_Fiss_1421_1442_join[2]));
	ENDFOR
}

void FFTReorderSimple_1216() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_1421_1442_split[3]), &(SplitJoin8_FFTReorderSimple_Fiss_1421_1442_join[3]));
	ENDFOR
}

void FFTReorderSimple_1217() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_1421_1442_split[4]), &(SplitJoin8_FFTReorderSimple_Fiss_1421_1442_join[4]));
	ENDFOR
}

void FFTReorderSimple_1218() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_1421_1442_split[5]), &(SplitJoin8_FFTReorderSimple_Fiss_1421_1442_join[5]));
	ENDFOR
}

void FFTReorderSimple_1219() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_1421_1442_split[6]), &(SplitJoin8_FFTReorderSimple_Fiss_1421_1442_join[6]));
	ENDFOR
}

void FFTReorderSimple_1220() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_1421_1442_split[7]), &(SplitJoin8_FFTReorderSimple_Fiss_1421_1442_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1211() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin8_FFTReorderSimple_Fiss_1421_1442_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1206WEIGHTED_ROUND_ROBIN_Splitter_1211));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1212() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1212WEIGHTED_ROUND_ROBIN_Splitter_1221, pop_float(&SplitJoin8_FFTReorderSimple_Fiss_1421_1442_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_1223() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_split[0]), &(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_join[0]));
	ENDFOR
}

void FFTReorderSimple_1224() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_split[1]), &(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_join[1]));
	ENDFOR
}

void FFTReorderSimple_1225() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_split[2]), &(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_join[2]));
	ENDFOR
}

void FFTReorderSimple_1226() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_split[3]), &(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_join[3]));
	ENDFOR
}

void FFTReorderSimple_1227() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_split[4]), &(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_join[4]));
	ENDFOR
}

void FFTReorderSimple_1228() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_split[5]), &(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_join[5]));
	ENDFOR
}

void FFTReorderSimple_1229() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_split[6]), &(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_join[6]));
	ENDFOR
}

void FFTReorderSimple_1230() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_split[7]), &(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_join[7]));
	ENDFOR
}

void FFTReorderSimple_1231() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_split[8]), &(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_join[8]));
	ENDFOR
}

void FFTReorderSimple_1232() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_split[9]), &(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_join[9]));
	ENDFOR
}

void FFTReorderSimple_1233() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_split[10]), &(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_join[10]));
	ENDFOR
}

void FFTReorderSimple_1234() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_split[11]), &(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_join[11]));
	ENDFOR
}

void FFTReorderSimple_1235() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_split[12]), &(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_join[12]));
	ENDFOR
}

void FFTReorderSimple_1236() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_split[13]), &(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_join[13]));
	ENDFOR
}

void FFTReorderSimple_1237() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_split[14]), &(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_join[14]));
	ENDFOR
}

void FFTReorderSimple_1238() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_split[15]), &(SplitJoin10_FFTReorderSimple_Fiss_1422_1443_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1221() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_FFTReorderSimple_Fiss_1422_1443_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1212WEIGHTED_ROUND_ROBIN_Splitter_1221));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1222() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1222WEIGHTED_ROUND_ROBIN_Splitter_1239, pop_float(&SplitJoin10_FFTReorderSimple_Fiss_1422_1443_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&(*chanin), i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&(*chanin), i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&(*chanin), (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&(*chanin), (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_1241_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_1241_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&(*chanin)) ; 
			push_float(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_1241() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1423_1444_split[0]), &(SplitJoin12_CombineDFT_Fiss_1423_1444_join[0]));
	ENDFOR
}

void CombineDFT_1242() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1423_1444_split[1]), &(SplitJoin12_CombineDFT_Fiss_1423_1444_join[1]));
	ENDFOR
}

void CombineDFT_1243() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1423_1444_split[2]), &(SplitJoin12_CombineDFT_Fiss_1423_1444_join[2]));
	ENDFOR
}

void CombineDFT_1244() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1423_1444_split[3]), &(SplitJoin12_CombineDFT_Fiss_1423_1444_join[3]));
	ENDFOR
}

void CombineDFT_1245() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1423_1444_split[4]), &(SplitJoin12_CombineDFT_Fiss_1423_1444_join[4]));
	ENDFOR
}

void CombineDFT_1246() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1423_1444_split[5]), &(SplitJoin12_CombineDFT_Fiss_1423_1444_join[5]));
	ENDFOR
}

void CombineDFT_1247() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1423_1444_split[6]), &(SplitJoin12_CombineDFT_Fiss_1423_1444_join[6]));
	ENDFOR
}

void CombineDFT_1248() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1423_1444_split[7]), &(SplitJoin12_CombineDFT_Fiss_1423_1444_join[7]));
	ENDFOR
}

void CombineDFT_1249() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1423_1444_split[8]), &(SplitJoin12_CombineDFT_Fiss_1423_1444_join[8]));
	ENDFOR
}

void CombineDFT_1250() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1423_1444_split[9]), &(SplitJoin12_CombineDFT_Fiss_1423_1444_join[9]));
	ENDFOR
}

void CombineDFT_1251() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1423_1444_split[10]), &(SplitJoin12_CombineDFT_Fiss_1423_1444_join[10]));
	ENDFOR
}

void CombineDFT_1252() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1423_1444_split[11]), &(SplitJoin12_CombineDFT_Fiss_1423_1444_join[11]));
	ENDFOR
}

void CombineDFT_1253() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1423_1444_split[12]), &(SplitJoin12_CombineDFT_Fiss_1423_1444_join[12]));
	ENDFOR
}

void CombineDFT_1254() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1423_1444_split[13]), &(SplitJoin12_CombineDFT_Fiss_1423_1444_join[13]));
	ENDFOR
}

void CombineDFT_1255() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1423_1444_split[14]), &(SplitJoin12_CombineDFT_Fiss_1423_1444_join[14]));
	ENDFOR
}

void CombineDFT_1256() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1423_1444_split[15]), &(SplitJoin12_CombineDFT_Fiss_1423_1444_join[15]));
	ENDFOR
}

void CombineDFT_1257() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1423_1444_split[16]), &(SplitJoin12_CombineDFT_Fiss_1423_1444_join[16]));
	ENDFOR
}

void CombineDFT_1258() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1423_1444_split[17]), &(SplitJoin12_CombineDFT_Fiss_1423_1444_join[17]));
	ENDFOR
}

void CombineDFT_1259() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1423_1444_split[18]), &(SplitJoin12_CombineDFT_Fiss_1423_1444_join[18]));
	ENDFOR
}

void CombineDFT_1260() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1423_1444_split[19]), &(SplitJoin12_CombineDFT_Fiss_1423_1444_join[19]));
	ENDFOR
}

void CombineDFT_1261() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1423_1444_split[20]), &(SplitJoin12_CombineDFT_Fiss_1423_1444_join[20]));
	ENDFOR
}

void CombineDFT_1262() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1423_1444_split[21]), &(SplitJoin12_CombineDFT_Fiss_1423_1444_join[21]));
	ENDFOR
}

void CombineDFT_1263() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1423_1444_split[22]), &(SplitJoin12_CombineDFT_Fiss_1423_1444_join[22]));
	ENDFOR
}

void CombineDFT_1264() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1423_1444_split[23]), &(SplitJoin12_CombineDFT_Fiss_1423_1444_join[23]));
	ENDFOR
}

void CombineDFT_1265() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1423_1444_split[24]), &(SplitJoin12_CombineDFT_Fiss_1423_1444_join[24]));
	ENDFOR
}

void CombineDFT_1266() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1423_1444_split[25]), &(SplitJoin12_CombineDFT_Fiss_1423_1444_join[25]));
	ENDFOR
}

void CombineDFT_1267() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1423_1444_split[26]), &(SplitJoin12_CombineDFT_Fiss_1423_1444_join[26]));
	ENDFOR
}

void CombineDFT_1268() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1423_1444_split[27]), &(SplitJoin12_CombineDFT_Fiss_1423_1444_join[27]));
	ENDFOR
}

void CombineDFT_1269() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1423_1444_split[28]), &(SplitJoin12_CombineDFT_Fiss_1423_1444_join[28]));
	ENDFOR
}

void CombineDFT_1270() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1423_1444_split[29]), &(SplitJoin12_CombineDFT_Fiss_1423_1444_join[29]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1239() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 30, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin12_CombineDFT_Fiss_1423_1444_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1222WEIGHTED_ROUND_ROBIN_Splitter_1239));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1240() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 30, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1240WEIGHTED_ROUND_ROBIN_Splitter_1271, pop_float(&SplitJoin12_CombineDFT_Fiss_1423_1444_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_1273() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_1424_1445_split[0]), &(SplitJoin14_CombineDFT_Fiss_1424_1445_join[0]));
	ENDFOR
}

void CombineDFT_1274() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_1424_1445_split[1]), &(SplitJoin14_CombineDFT_Fiss_1424_1445_join[1]));
	ENDFOR
}

void CombineDFT_1275() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_1424_1445_split[2]), &(SplitJoin14_CombineDFT_Fiss_1424_1445_join[2]));
	ENDFOR
}

void CombineDFT_1276() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_1424_1445_split[3]), &(SplitJoin14_CombineDFT_Fiss_1424_1445_join[3]));
	ENDFOR
}

void CombineDFT_1277() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_1424_1445_split[4]), &(SplitJoin14_CombineDFT_Fiss_1424_1445_join[4]));
	ENDFOR
}

void CombineDFT_1278() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_1424_1445_split[5]), &(SplitJoin14_CombineDFT_Fiss_1424_1445_join[5]));
	ENDFOR
}

void CombineDFT_1279() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_1424_1445_split[6]), &(SplitJoin14_CombineDFT_Fiss_1424_1445_join[6]));
	ENDFOR
}

void CombineDFT_1280() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_1424_1445_split[7]), &(SplitJoin14_CombineDFT_Fiss_1424_1445_join[7]));
	ENDFOR
}

void CombineDFT_1281() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_1424_1445_split[8]), &(SplitJoin14_CombineDFT_Fiss_1424_1445_join[8]));
	ENDFOR
}

void CombineDFT_1282() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_1424_1445_split[9]), &(SplitJoin14_CombineDFT_Fiss_1424_1445_join[9]));
	ENDFOR
}

void CombineDFT_1283() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_1424_1445_split[10]), &(SplitJoin14_CombineDFT_Fiss_1424_1445_join[10]));
	ENDFOR
}

void CombineDFT_1284() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_1424_1445_split[11]), &(SplitJoin14_CombineDFT_Fiss_1424_1445_join[11]));
	ENDFOR
}

void CombineDFT_1285() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_1424_1445_split[12]), &(SplitJoin14_CombineDFT_Fiss_1424_1445_join[12]));
	ENDFOR
}

void CombineDFT_1286() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_1424_1445_split[13]), &(SplitJoin14_CombineDFT_Fiss_1424_1445_join[13]));
	ENDFOR
}

void CombineDFT_1287() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_1424_1445_split[14]), &(SplitJoin14_CombineDFT_Fiss_1424_1445_join[14]));
	ENDFOR
}

void CombineDFT_1288() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_1424_1445_split[15]), &(SplitJoin14_CombineDFT_Fiss_1424_1445_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1271() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin14_CombineDFT_Fiss_1424_1445_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1240WEIGHTED_ROUND_ROBIN_Splitter_1271));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1272() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1272WEIGHTED_ROUND_ROBIN_Splitter_1289, pop_float(&SplitJoin14_CombineDFT_Fiss_1424_1445_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_1291() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_1425_1446_split[0]), &(SplitJoin16_CombineDFT_Fiss_1425_1446_join[0]));
	ENDFOR
}

void CombineDFT_1292() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_1425_1446_split[1]), &(SplitJoin16_CombineDFT_Fiss_1425_1446_join[1]));
	ENDFOR
}

void CombineDFT_1293() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_1425_1446_split[2]), &(SplitJoin16_CombineDFT_Fiss_1425_1446_join[2]));
	ENDFOR
}

void CombineDFT_1294() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_1425_1446_split[3]), &(SplitJoin16_CombineDFT_Fiss_1425_1446_join[3]));
	ENDFOR
}

void CombineDFT_1295() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_1425_1446_split[4]), &(SplitJoin16_CombineDFT_Fiss_1425_1446_join[4]));
	ENDFOR
}

void CombineDFT_1296() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_1425_1446_split[5]), &(SplitJoin16_CombineDFT_Fiss_1425_1446_join[5]));
	ENDFOR
}

void CombineDFT_1297() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_1425_1446_split[6]), &(SplitJoin16_CombineDFT_Fiss_1425_1446_join[6]));
	ENDFOR
}

void CombineDFT_1298() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_1425_1446_split[7]), &(SplitJoin16_CombineDFT_Fiss_1425_1446_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1289() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin16_CombineDFT_Fiss_1425_1446_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1272WEIGHTED_ROUND_ROBIN_Splitter_1289));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1290() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1290WEIGHTED_ROUND_ROBIN_Splitter_1299, pop_float(&SplitJoin16_CombineDFT_Fiss_1425_1446_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_1301() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_1426_1447_split[0]), &(SplitJoin18_CombineDFT_Fiss_1426_1447_join[0]));
	ENDFOR
}

void CombineDFT_1302() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_1426_1447_split[1]), &(SplitJoin18_CombineDFT_Fiss_1426_1447_join[1]));
	ENDFOR
}

void CombineDFT_1303() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_1426_1447_split[2]), &(SplitJoin18_CombineDFT_Fiss_1426_1447_join[2]));
	ENDFOR
}

void CombineDFT_1304() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_1426_1447_split[3]), &(SplitJoin18_CombineDFT_Fiss_1426_1447_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1299() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin18_CombineDFT_Fiss_1426_1447_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1290WEIGHTED_ROUND_ROBIN_Splitter_1299));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1300() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1300WEIGHTED_ROUND_ROBIN_Splitter_1305, pop_float(&SplitJoin18_CombineDFT_Fiss_1426_1447_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_1307() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin20_CombineDFT_Fiss_1427_1448_split[0]), &(SplitJoin20_CombineDFT_Fiss_1427_1448_join[0]));
	ENDFOR
}

void CombineDFT_1308() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin20_CombineDFT_Fiss_1427_1448_split[1]), &(SplitJoin20_CombineDFT_Fiss_1427_1448_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1305() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_1427_1448_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1300WEIGHTED_ROUND_ROBIN_Splitter_1305));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_1427_1448_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1300WEIGHTED_ROUND_ROBIN_Splitter_1305));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1306() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1306CombineDFT_1176, pop_float(&SplitJoin20_CombineDFT_Fiss_1427_1448_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1306CombineDFT_1176, pop_float(&SplitJoin20_CombineDFT_Fiss_1427_1448_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_1176() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_1306CombineDFT_1176), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_1155_1191_1418_1439_join[0]));
	ENDFOR
}

void FFTReorderSimple_1177() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_1155_1191_1418_1439_split[1]), &(FFTReorderSimple_1177WEIGHTED_ROUND_ROBIN_Splitter_1309));
	ENDFOR
}

void FFTReorderSimple_1311() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin104_FFTReorderSimple_Fiss_1428_1449_split[0]), &(SplitJoin104_FFTReorderSimple_Fiss_1428_1449_join[0]));
	ENDFOR
}

void FFTReorderSimple_1312() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin104_FFTReorderSimple_Fiss_1428_1449_split[1]), &(SplitJoin104_FFTReorderSimple_Fiss_1428_1449_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1309() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin104_FFTReorderSimple_Fiss_1428_1449_split[0], pop_float(&FFTReorderSimple_1177WEIGHTED_ROUND_ROBIN_Splitter_1309));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin104_FFTReorderSimple_Fiss_1428_1449_split[1], pop_float(&FFTReorderSimple_1177WEIGHTED_ROUND_ROBIN_Splitter_1309));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1310() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1310WEIGHTED_ROUND_ROBIN_Splitter_1313, pop_float(&SplitJoin104_FFTReorderSimple_Fiss_1428_1449_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1310WEIGHTED_ROUND_ROBIN_Splitter_1313, pop_float(&SplitJoin104_FFTReorderSimple_Fiss_1428_1449_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_1315() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin106_FFTReorderSimple_Fiss_1429_1450_split[0]), &(SplitJoin106_FFTReorderSimple_Fiss_1429_1450_join[0]));
	ENDFOR
}

void FFTReorderSimple_1316() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin106_FFTReorderSimple_Fiss_1429_1450_split[1]), &(SplitJoin106_FFTReorderSimple_Fiss_1429_1450_join[1]));
	ENDFOR
}

void FFTReorderSimple_1317() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin106_FFTReorderSimple_Fiss_1429_1450_split[2]), &(SplitJoin106_FFTReorderSimple_Fiss_1429_1450_join[2]));
	ENDFOR
}

void FFTReorderSimple_1318() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin106_FFTReorderSimple_Fiss_1429_1450_split[3]), &(SplitJoin106_FFTReorderSimple_Fiss_1429_1450_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1313() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin106_FFTReorderSimple_Fiss_1429_1450_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1310WEIGHTED_ROUND_ROBIN_Splitter_1313));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1314() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1314WEIGHTED_ROUND_ROBIN_Splitter_1319, pop_float(&SplitJoin106_FFTReorderSimple_Fiss_1429_1450_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_1321() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin108_FFTReorderSimple_Fiss_1430_1451_split[0]), &(SplitJoin108_FFTReorderSimple_Fiss_1430_1451_join[0]));
	ENDFOR
}

void FFTReorderSimple_1322() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin108_FFTReorderSimple_Fiss_1430_1451_split[1]), &(SplitJoin108_FFTReorderSimple_Fiss_1430_1451_join[1]));
	ENDFOR
}

void FFTReorderSimple_1323() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin108_FFTReorderSimple_Fiss_1430_1451_split[2]), &(SplitJoin108_FFTReorderSimple_Fiss_1430_1451_join[2]));
	ENDFOR
}

void FFTReorderSimple_1324() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin108_FFTReorderSimple_Fiss_1430_1451_split[3]), &(SplitJoin108_FFTReorderSimple_Fiss_1430_1451_join[3]));
	ENDFOR
}

void FFTReorderSimple_1325() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin108_FFTReorderSimple_Fiss_1430_1451_split[4]), &(SplitJoin108_FFTReorderSimple_Fiss_1430_1451_join[4]));
	ENDFOR
}

void FFTReorderSimple_1326() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin108_FFTReorderSimple_Fiss_1430_1451_split[5]), &(SplitJoin108_FFTReorderSimple_Fiss_1430_1451_join[5]));
	ENDFOR
}

void FFTReorderSimple_1327() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin108_FFTReorderSimple_Fiss_1430_1451_split[6]), &(SplitJoin108_FFTReorderSimple_Fiss_1430_1451_join[6]));
	ENDFOR
}

void FFTReorderSimple_1328() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin108_FFTReorderSimple_Fiss_1430_1451_split[7]), &(SplitJoin108_FFTReorderSimple_Fiss_1430_1451_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1319() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin108_FFTReorderSimple_Fiss_1430_1451_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1314WEIGHTED_ROUND_ROBIN_Splitter_1319));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1320() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1320WEIGHTED_ROUND_ROBIN_Splitter_1329, pop_float(&SplitJoin108_FFTReorderSimple_Fiss_1430_1451_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_1331() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_split[0]), &(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_join[0]));
	ENDFOR
}

void FFTReorderSimple_1332() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_split[1]), &(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_join[1]));
	ENDFOR
}

void FFTReorderSimple_1333() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_split[2]), &(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_join[2]));
	ENDFOR
}

void FFTReorderSimple_1334() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_split[3]), &(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_join[3]));
	ENDFOR
}

void FFTReorderSimple_1335() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_split[4]), &(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_join[4]));
	ENDFOR
}

void FFTReorderSimple_1336() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_split[5]), &(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_join[5]));
	ENDFOR
}

void FFTReorderSimple_1337() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_split[6]), &(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_join[6]));
	ENDFOR
}

void FFTReorderSimple_1338() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_split[7]), &(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_join[7]));
	ENDFOR
}

void FFTReorderSimple_1339() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_split[8]), &(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_join[8]));
	ENDFOR
}

void FFTReorderSimple_1340() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_split[9]), &(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_join[9]));
	ENDFOR
}

void FFTReorderSimple_1341() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_split[10]), &(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_join[10]));
	ENDFOR
}

void FFTReorderSimple_1342() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_split[11]), &(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_join[11]));
	ENDFOR
}

void FFTReorderSimple_1343() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_split[12]), &(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_join[12]));
	ENDFOR
}

void FFTReorderSimple_1344() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_split[13]), &(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_join[13]));
	ENDFOR
}

void FFTReorderSimple_1345() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_split[14]), &(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_join[14]));
	ENDFOR
}

void FFTReorderSimple_1346() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_split[15]), &(SplitJoin110_FFTReorderSimple_Fiss_1431_1452_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1329() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin110_FFTReorderSimple_Fiss_1431_1452_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1320WEIGHTED_ROUND_ROBIN_Splitter_1329));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1330() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1330WEIGHTED_ROUND_ROBIN_Splitter_1347, pop_float(&SplitJoin110_FFTReorderSimple_Fiss_1431_1452_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_1349() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_1432_1453_split[0]), &(SplitJoin112_CombineDFT_Fiss_1432_1453_join[0]));
	ENDFOR
}

void CombineDFT_1350() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_1432_1453_split[1]), &(SplitJoin112_CombineDFT_Fiss_1432_1453_join[1]));
	ENDFOR
}

void CombineDFT_1351() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_1432_1453_split[2]), &(SplitJoin112_CombineDFT_Fiss_1432_1453_join[2]));
	ENDFOR
}

void CombineDFT_1352() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_1432_1453_split[3]), &(SplitJoin112_CombineDFT_Fiss_1432_1453_join[3]));
	ENDFOR
}

void CombineDFT_1353() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_1432_1453_split[4]), &(SplitJoin112_CombineDFT_Fiss_1432_1453_join[4]));
	ENDFOR
}

void CombineDFT_1354() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_1432_1453_split[5]), &(SplitJoin112_CombineDFT_Fiss_1432_1453_join[5]));
	ENDFOR
}

void CombineDFT_1355() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_1432_1453_split[6]), &(SplitJoin112_CombineDFT_Fiss_1432_1453_join[6]));
	ENDFOR
}

void CombineDFT_1356() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_1432_1453_split[7]), &(SplitJoin112_CombineDFT_Fiss_1432_1453_join[7]));
	ENDFOR
}

void CombineDFT_1357() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_1432_1453_split[8]), &(SplitJoin112_CombineDFT_Fiss_1432_1453_join[8]));
	ENDFOR
}

void CombineDFT_1358() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_1432_1453_split[9]), &(SplitJoin112_CombineDFT_Fiss_1432_1453_join[9]));
	ENDFOR
}

void CombineDFT_1359() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_1432_1453_split[10]), &(SplitJoin112_CombineDFT_Fiss_1432_1453_join[10]));
	ENDFOR
}

void CombineDFT_1360() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_1432_1453_split[11]), &(SplitJoin112_CombineDFT_Fiss_1432_1453_join[11]));
	ENDFOR
}

void CombineDFT_1361() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_1432_1453_split[12]), &(SplitJoin112_CombineDFT_Fiss_1432_1453_join[12]));
	ENDFOR
}

void CombineDFT_1362() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_1432_1453_split[13]), &(SplitJoin112_CombineDFT_Fiss_1432_1453_join[13]));
	ENDFOR
}

void CombineDFT_1363() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_1432_1453_split[14]), &(SplitJoin112_CombineDFT_Fiss_1432_1453_join[14]));
	ENDFOR
}

void CombineDFT_1364() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_1432_1453_split[15]), &(SplitJoin112_CombineDFT_Fiss_1432_1453_join[15]));
	ENDFOR
}

void CombineDFT_1365() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_1432_1453_split[16]), &(SplitJoin112_CombineDFT_Fiss_1432_1453_join[16]));
	ENDFOR
}

void CombineDFT_1366() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_1432_1453_split[17]), &(SplitJoin112_CombineDFT_Fiss_1432_1453_join[17]));
	ENDFOR
}

void CombineDFT_1367() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_1432_1453_split[18]), &(SplitJoin112_CombineDFT_Fiss_1432_1453_join[18]));
	ENDFOR
}

void CombineDFT_1368() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_1432_1453_split[19]), &(SplitJoin112_CombineDFT_Fiss_1432_1453_join[19]));
	ENDFOR
}

void CombineDFT_1369() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_1432_1453_split[20]), &(SplitJoin112_CombineDFT_Fiss_1432_1453_join[20]));
	ENDFOR
}

void CombineDFT_1370() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_1432_1453_split[21]), &(SplitJoin112_CombineDFT_Fiss_1432_1453_join[21]));
	ENDFOR
}

void CombineDFT_1371() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_1432_1453_split[22]), &(SplitJoin112_CombineDFT_Fiss_1432_1453_join[22]));
	ENDFOR
}

void CombineDFT_1372() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_1432_1453_split[23]), &(SplitJoin112_CombineDFT_Fiss_1432_1453_join[23]));
	ENDFOR
}

void CombineDFT_1373() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_1432_1453_split[24]), &(SplitJoin112_CombineDFT_Fiss_1432_1453_join[24]));
	ENDFOR
}

void CombineDFT_1374() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_1432_1453_split[25]), &(SplitJoin112_CombineDFT_Fiss_1432_1453_join[25]));
	ENDFOR
}

void CombineDFT_1375() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_1432_1453_split[26]), &(SplitJoin112_CombineDFT_Fiss_1432_1453_join[26]));
	ENDFOR
}

void CombineDFT_1376() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_1432_1453_split[27]), &(SplitJoin112_CombineDFT_Fiss_1432_1453_join[27]));
	ENDFOR
}

void CombineDFT_1377() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_1432_1453_split[28]), &(SplitJoin112_CombineDFT_Fiss_1432_1453_join[28]));
	ENDFOR
}

void CombineDFT_1378() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_1432_1453_split[29]), &(SplitJoin112_CombineDFT_Fiss_1432_1453_join[29]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1347() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 30, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin112_CombineDFT_Fiss_1432_1453_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1330WEIGHTED_ROUND_ROBIN_Splitter_1347));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1348() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 30, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1348WEIGHTED_ROUND_ROBIN_Splitter_1379, pop_float(&SplitJoin112_CombineDFT_Fiss_1432_1453_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_1381() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin114_CombineDFT_Fiss_1433_1454_split[0]), &(SplitJoin114_CombineDFT_Fiss_1433_1454_join[0]));
	ENDFOR
}

void CombineDFT_1382() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin114_CombineDFT_Fiss_1433_1454_split[1]), &(SplitJoin114_CombineDFT_Fiss_1433_1454_join[1]));
	ENDFOR
}

void CombineDFT_1383() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin114_CombineDFT_Fiss_1433_1454_split[2]), &(SplitJoin114_CombineDFT_Fiss_1433_1454_join[2]));
	ENDFOR
}

void CombineDFT_1384() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin114_CombineDFT_Fiss_1433_1454_split[3]), &(SplitJoin114_CombineDFT_Fiss_1433_1454_join[3]));
	ENDFOR
}

void CombineDFT_1385() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin114_CombineDFT_Fiss_1433_1454_split[4]), &(SplitJoin114_CombineDFT_Fiss_1433_1454_join[4]));
	ENDFOR
}

void CombineDFT_1386() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin114_CombineDFT_Fiss_1433_1454_split[5]), &(SplitJoin114_CombineDFT_Fiss_1433_1454_join[5]));
	ENDFOR
}

void CombineDFT_1387() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin114_CombineDFT_Fiss_1433_1454_split[6]), &(SplitJoin114_CombineDFT_Fiss_1433_1454_join[6]));
	ENDFOR
}

void CombineDFT_1388() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin114_CombineDFT_Fiss_1433_1454_split[7]), &(SplitJoin114_CombineDFT_Fiss_1433_1454_join[7]));
	ENDFOR
}

void CombineDFT_1389() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin114_CombineDFT_Fiss_1433_1454_split[8]), &(SplitJoin114_CombineDFT_Fiss_1433_1454_join[8]));
	ENDFOR
}

void CombineDFT_1390() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin114_CombineDFT_Fiss_1433_1454_split[9]), &(SplitJoin114_CombineDFT_Fiss_1433_1454_join[9]));
	ENDFOR
}

void CombineDFT_1391() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin114_CombineDFT_Fiss_1433_1454_split[10]), &(SplitJoin114_CombineDFT_Fiss_1433_1454_join[10]));
	ENDFOR
}

void CombineDFT_1392() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin114_CombineDFT_Fiss_1433_1454_split[11]), &(SplitJoin114_CombineDFT_Fiss_1433_1454_join[11]));
	ENDFOR
}

void CombineDFT_1393() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin114_CombineDFT_Fiss_1433_1454_split[12]), &(SplitJoin114_CombineDFT_Fiss_1433_1454_join[12]));
	ENDFOR
}

void CombineDFT_1394() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin114_CombineDFT_Fiss_1433_1454_split[13]), &(SplitJoin114_CombineDFT_Fiss_1433_1454_join[13]));
	ENDFOR
}

void CombineDFT_1395() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin114_CombineDFT_Fiss_1433_1454_split[14]), &(SplitJoin114_CombineDFT_Fiss_1433_1454_join[14]));
	ENDFOR
}

void CombineDFT_1396() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin114_CombineDFT_Fiss_1433_1454_split[15]), &(SplitJoin114_CombineDFT_Fiss_1433_1454_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1379() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin114_CombineDFT_Fiss_1433_1454_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1348WEIGHTED_ROUND_ROBIN_Splitter_1379));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1380() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1380WEIGHTED_ROUND_ROBIN_Splitter_1397, pop_float(&SplitJoin114_CombineDFT_Fiss_1433_1454_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_1399() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin116_CombineDFT_Fiss_1434_1455_split[0]), &(SplitJoin116_CombineDFT_Fiss_1434_1455_join[0]));
	ENDFOR
}

void CombineDFT_1400() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin116_CombineDFT_Fiss_1434_1455_split[1]), &(SplitJoin116_CombineDFT_Fiss_1434_1455_join[1]));
	ENDFOR
}

void CombineDFT_1401() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin116_CombineDFT_Fiss_1434_1455_split[2]), &(SplitJoin116_CombineDFT_Fiss_1434_1455_join[2]));
	ENDFOR
}

void CombineDFT_1402() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin116_CombineDFT_Fiss_1434_1455_split[3]), &(SplitJoin116_CombineDFT_Fiss_1434_1455_join[3]));
	ENDFOR
}

void CombineDFT_1403() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin116_CombineDFT_Fiss_1434_1455_split[4]), &(SplitJoin116_CombineDFT_Fiss_1434_1455_join[4]));
	ENDFOR
}

void CombineDFT_1404() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin116_CombineDFT_Fiss_1434_1455_split[5]), &(SplitJoin116_CombineDFT_Fiss_1434_1455_join[5]));
	ENDFOR
}

void CombineDFT_1405() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin116_CombineDFT_Fiss_1434_1455_split[6]), &(SplitJoin116_CombineDFT_Fiss_1434_1455_join[6]));
	ENDFOR
}

void CombineDFT_1406() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin116_CombineDFT_Fiss_1434_1455_split[7]), &(SplitJoin116_CombineDFT_Fiss_1434_1455_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1397() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin116_CombineDFT_Fiss_1434_1455_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1380WEIGHTED_ROUND_ROBIN_Splitter_1397));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1398() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1398WEIGHTED_ROUND_ROBIN_Splitter_1407, pop_float(&SplitJoin116_CombineDFT_Fiss_1434_1455_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_1409() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin118_CombineDFT_Fiss_1435_1456_split[0]), &(SplitJoin118_CombineDFT_Fiss_1435_1456_join[0]));
	ENDFOR
}

void CombineDFT_1410() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin118_CombineDFT_Fiss_1435_1456_split[1]), &(SplitJoin118_CombineDFT_Fiss_1435_1456_join[1]));
	ENDFOR
}

void CombineDFT_1411() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin118_CombineDFT_Fiss_1435_1456_split[2]), &(SplitJoin118_CombineDFT_Fiss_1435_1456_join[2]));
	ENDFOR
}

void CombineDFT_1412() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin118_CombineDFT_Fiss_1435_1456_split[3]), &(SplitJoin118_CombineDFT_Fiss_1435_1456_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1407() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin118_CombineDFT_Fiss_1435_1456_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1398WEIGHTED_ROUND_ROBIN_Splitter_1407));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1408() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1408WEIGHTED_ROUND_ROBIN_Splitter_1413, pop_float(&SplitJoin118_CombineDFT_Fiss_1435_1456_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_1415() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin120_CombineDFT_Fiss_1436_1457_split[0]), &(SplitJoin120_CombineDFT_Fiss_1436_1457_join[0]));
	ENDFOR
}

void CombineDFT_1416() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(SplitJoin120_CombineDFT_Fiss_1436_1457_split[1]), &(SplitJoin120_CombineDFT_Fiss_1436_1457_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1413() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin120_CombineDFT_Fiss_1436_1457_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1408WEIGHTED_ROUND_ROBIN_Splitter_1413));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin120_CombineDFT_Fiss_1436_1457_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1408WEIGHTED_ROUND_ROBIN_Splitter_1413));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1414() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1414CombineDFT_1187, pop_float(&SplitJoin120_CombineDFT_Fiss_1436_1457_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1414CombineDFT_1187, pop_float(&SplitJoin120_CombineDFT_Fiss_1436_1457_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_1187() {
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_1414CombineDFT_1187), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_1155_1191_1418_1439_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1189() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_1155_1191_1418_1439_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1198WEIGHTED_ROUND_ROBIN_Splitter_1189));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_1155_1191_1418_1439_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1198WEIGHTED_ROUND_ROBIN_Splitter_1189));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1190() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 15, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1190FloatPrinter_1188, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_1155_1191_1418_1439_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1190FloatPrinter_1188, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_1155_1191_1418_1439_join[1]));
		ENDFOR
	ENDFOR
}}

void FloatPrinter(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void FloatPrinter_1188() {
	FOR(uint32_t, __iter_steady_, 0, <, 3840, __iter_steady_++)
		FloatPrinter(&(WEIGHTED_ROUND_ROBIN_Joiner_1190FloatPrinter_1188));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 30, __iter_init_0_++)
		init_buffer_float(&SplitJoin112_CombineDFT_Fiss_1432_1453_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 2, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_1417_1438_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 16, __iter_init_2_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_1422_1443_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 30, __iter_init_3_++)
		init_buffer_float(&SplitJoin112_CombineDFT_Fiss_1432_1453_split[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1190FloatPrinter_1188);
	FOR(int, __iter_init_4_, 0, <, 4, __iter_init_4_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_1426_1447_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 16, __iter_init_5_++)
		init_buffer_float(&SplitJoin110_FFTReorderSimple_Fiss_1431_1452_join[__iter_init_5_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1206WEIGHTED_ROUND_ROBIN_Splitter_1211);
	FOR(int, __iter_init_6_, 0, <, 4, __iter_init_6_++)
		init_buffer_float(&SplitJoin118_CombineDFT_Fiss_1435_1456_split[__iter_init_6_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1314WEIGHTED_ROUND_ROBIN_Splitter_1319);
	FOR(int, __iter_init_7_, 0, <, 2, __iter_init_7_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_1155_1191_1418_1439_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_1419_1440_split[__iter_init_8_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1306CombineDFT_1176);
	FOR(int, __iter_init_9_, 0, <, 30, __iter_init_9_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_1423_1444_split[__iter_init_9_]);
	ENDFOR
	init_buffer_float(&FFTReorderSimple_1177WEIGHTED_ROUND_ROBIN_Splitter_1309);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1408WEIGHTED_ROUND_ROBIN_Splitter_1413);
	FOR(int, __iter_init_10_, 0, <, 4, __iter_init_10_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_1426_1447_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 30, __iter_init_11_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_1423_1444_join[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 4, __iter_init_12_++)
		init_buffer_float(&SplitJoin118_CombineDFT_Fiss_1435_1456_join[__iter_init_12_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1330WEIGHTED_ROUND_ROBIN_Splitter_1347);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1212WEIGHTED_ROUND_ROBIN_Splitter_1221);
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_float(&SplitJoin120_CombineDFT_Fiss_1436_1457_join[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 16, __iter_init_14_++)
		init_buffer_float(&SplitJoin114_CombineDFT_Fiss_1433_1454_join[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 4, __iter_init_15_++)
		init_buffer_float(&SplitJoin106_FFTReorderSimple_Fiss_1429_1450_join[__iter_init_15_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1310WEIGHTED_ROUND_ROBIN_Splitter_1313);
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_1427_1448_join[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 4, __iter_init_17_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_1420_1441_split[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 16, __iter_init_18_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_1424_1445_join[__iter_init_18_]);
	ENDFOR
	FOR(int, __iter_init_19_, 0, <, 16, __iter_init_19_++)
		init_buffer_float(&SplitJoin110_FFTReorderSimple_Fiss_1431_1452_split[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 8, __iter_init_20_++)
		init_buffer_float(&SplitJoin116_CombineDFT_Fiss_1434_1455_split[__iter_init_20_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1290WEIGHTED_ROUND_ROBIN_Splitter_1299);
	FOR(int, __iter_init_21_, 0, <, 8, __iter_init_21_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_1421_1442_join[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 8, __iter_init_22_++)
		init_buffer_float(&SplitJoin108_FFTReorderSimple_Fiss_1430_1451_join[__iter_init_22_]);
	ENDFOR
	FOR(int, __iter_init_23_, 0, <, 2, __iter_init_23_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_1417_1438_join[__iter_init_23_]);
	ENDFOR
	FOR(int, __iter_init_24_, 0, <, 2, __iter_init_24_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_1427_1448_split[__iter_init_24_]);
	ENDFOR
	init_buffer_float(&FFTReorderSimple_1166WEIGHTED_ROUND_ROBIN_Splitter_1201);
	FOR(int, __iter_init_25_, 0, <, 8, __iter_init_25_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_1421_1442_split[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 8, __iter_init_26_++)
		init_buffer_float(&SplitJoin116_CombineDFT_Fiss_1434_1455_join[__iter_init_26_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1198WEIGHTED_ROUND_ROBIN_Splitter_1189);
	FOR(int, __iter_init_27_, 0, <, 2, __iter_init_27_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_1419_1440_join[__iter_init_27_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1202WEIGHTED_ROUND_ROBIN_Splitter_1205);
	FOR(int, __iter_init_28_, 0, <, 2, __iter_init_28_++)
		init_buffer_float(&SplitJoin104_FFTReorderSimple_Fiss_1428_1449_join[__iter_init_28_]);
	ENDFOR
	FOR(int, __iter_init_29_, 0, <, 8, __iter_init_29_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_1425_1446_join[__iter_init_29_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1240WEIGHTED_ROUND_ROBIN_Splitter_1271);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1348WEIGHTED_ROUND_ROBIN_Splitter_1379);
	FOR(int, __iter_init_30_, 0, <, 16, __iter_init_30_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_1424_1445_split[__iter_init_30_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1398WEIGHTED_ROUND_ROBIN_Splitter_1407);
	FOR(int, __iter_init_31_, 0, <, 4, __iter_init_31_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_1420_1441_join[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 16, __iter_init_32_++)
		init_buffer_float(&SplitJoin114_CombineDFT_Fiss_1433_1454_split[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 2, __iter_init_33_++)
		init_buffer_float(&SplitJoin120_CombineDFT_Fiss_1436_1457_split[__iter_init_33_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1272WEIGHTED_ROUND_ROBIN_Splitter_1289);
	FOR(int, __iter_init_34_, 0, <, 4, __iter_init_34_++)
		init_buffer_float(&SplitJoin106_FFTReorderSimple_Fiss_1429_1450_split[__iter_init_34_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1222WEIGHTED_ROUND_ROBIN_Splitter_1239);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1380WEIGHTED_ROUND_ROBIN_Splitter_1397);
	FOR(int, __iter_init_35_, 0, <, 8, __iter_init_35_++)
		init_buffer_float(&SplitJoin108_FFTReorderSimple_Fiss_1430_1451_split[__iter_init_35_]);
	ENDFOR
	FOR(int, __iter_init_36_, 0, <, 2, __iter_init_36_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_1155_1191_1418_1439_join[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 16, __iter_init_37_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_1422_1443_join[__iter_init_37_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1300WEIGHTED_ROUND_ROBIN_Splitter_1305);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1320WEIGHTED_ROUND_ROBIN_Splitter_1329);
	FOR(int, __iter_init_38_, 0, <, 8, __iter_init_38_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_1425_1446_split[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 2, __iter_init_39_++)
		init_buffer_float(&SplitJoin104_FFTReorderSimple_Fiss_1428_1449_split[__iter_init_39_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1414CombineDFT_1187);
// --- init: CombineDFT_1241
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1241_s.w[i] = real ; 
		CombineDFT_1241_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1242
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1242_s.w[i] = real ; 
		CombineDFT_1242_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1243
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1243_s.w[i] = real ; 
		CombineDFT_1243_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1244
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1244_s.w[i] = real ; 
		CombineDFT_1244_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1245
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1245_s.w[i] = real ; 
		CombineDFT_1245_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1246
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1246_s.w[i] = real ; 
		CombineDFT_1246_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1247
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1247_s.w[i] = real ; 
		CombineDFT_1247_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1248
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1248_s.w[i] = real ; 
		CombineDFT_1248_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1249
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1249_s.w[i] = real ; 
		CombineDFT_1249_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1250
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1250_s.w[i] = real ; 
		CombineDFT_1250_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1251
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1251_s.w[i] = real ; 
		CombineDFT_1251_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1252
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1252_s.w[i] = real ; 
		CombineDFT_1252_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1253
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1253_s.w[i] = real ; 
		CombineDFT_1253_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1254
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1254_s.w[i] = real ; 
		CombineDFT_1254_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1255
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1255_s.w[i] = real ; 
		CombineDFT_1255_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1256
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1256_s.w[i] = real ; 
		CombineDFT_1256_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1257
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1257_s.w[i] = real ; 
		CombineDFT_1257_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1258
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1258_s.w[i] = real ; 
		CombineDFT_1258_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1259
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1259_s.w[i] = real ; 
		CombineDFT_1259_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1260
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1260_s.w[i] = real ; 
		CombineDFT_1260_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1261
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1261_s.w[i] = real ; 
		CombineDFT_1261_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1262
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1262_s.w[i] = real ; 
		CombineDFT_1262_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1263
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1263_s.w[i] = real ; 
		CombineDFT_1263_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1264
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1264_s.w[i] = real ; 
		CombineDFT_1264_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1265
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1265_s.w[i] = real ; 
		CombineDFT_1265_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1266
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1266_s.w[i] = real ; 
		CombineDFT_1266_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1267
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1267_s.w[i] = real ; 
		CombineDFT_1267_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1268
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1268_s.w[i] = real ; 
		CombineDFT_1268_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1269
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1269_s.w[i] = real ; 
		CombineDFT_1269_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1270
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1270_s.w[i] = real ; 
		CombineDFT_1270_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1273
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1273_s.w[i] = real ; 
		CombineDFT_1273_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1274
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1274_s.w[i] = real ; 
		CombineDFT_1274_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1275
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1275_s.w[i] = real ; 
		CombineDFT_1275_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1276
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1276_s.w[i] = real ; 
		CombineDFT_1276_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1277
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1277_s.w[i] = real ; 
		CombineDFT_1277_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1278
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1278_s.w[i] = real ; 
		CombineDFT_1278_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1279
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1279_s.w[i] = real ; 
		CombineDFT_1279_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1280
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1280_s.w[i] = real ; 
		CombineDFT_1280_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1281
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1281_s.w[i] = real ; 
		CombineDFT_1281_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1282
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1282_s.w[i] = real ; 
		CombineDFT_1282_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1283
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1283_s.w[i] = real ; 
		CombineDFT_1283_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1284
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1284_s.w[i] = real ; 
		CombineDFT_1284_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1285
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1285_s.w[i] = real ; 
		CombineDFT_1285_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1286
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1286_s.w[i] = real ; 
		CombineDFT_1286_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1287
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1287_s.w[i] = real ; 
		CombineDFT_1287_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1288
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1288_s.w[i] = real ; 
		CombineDFT_1288_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1291
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_1291_s.w[i] = real ; 
		CombineDFT_1291_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1292
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_1292_s.w[i] = real ; 
		CombineDFT_1292_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1293
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_1293_s.w[i] = real ; 
		CombineDFT_1293_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1294
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_1294_s.w[i] = real ; 
		CombineDFT_1294_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1295
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_1295_s.w[i] = real ; 
		CombineDFT_1295_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1296
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_1296_s.w[i] = real ; 
		CombineDFT_1296_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1297
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_1297_s.w[i] = real ; 
		CombineDFT_1297_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1298
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_1298_s.w[i] = real ; 
		CombineDFT_1298_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1301
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_1301_s.w[i] = real ; 
		CombineDFT_1301_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1302
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_1302_s.w[i] = real ; 
		CombineDFT_1302_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1303
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_1303_s.w[i] = real ; 
		CombineDFT_1303_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1304
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_1304_s.w[i] = real ; 
		CombineDFT_1304_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1307
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_1307_s.w[i] = real ; 
		CombineDFT_1307_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1308
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_1308_s.w[i] = real ; 
		CombineDFT_1308_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1176
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_1176_s.w[i] = real ; 
		CombineDFT_1176_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1349
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1349_s.w[i] = real ; 
		CombineDFT_1349_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1350
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1350_s.w[i] = real ; 
		CombineDFT_1350_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1351
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1351_s.w[i] = real ; 
		CombineDFT_1351_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1352
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1352_s.w[i] = real ; 
		CombineDFT_1352_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1353
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1353_s.w[i] = real ; 
		CombineDFT_1353_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1354
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1354_s.w[i] = real ; 
		CombineDFT_1354_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1355
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1355_s.w[i] = real ; 
		CombineDFT_1355_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1356
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1356_s.w[i] = real ; 
		CombineDFT_1356_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1357
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1357_s.w[i] = real ; 
		CombineDFT_1357_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1358
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1358_s.w[i] = real ; 
		CombineDFT_1358_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1359
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1359_s.w[i] = real ; 
		CombineDFT_1359_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1360
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1360_s.w[i] = real ; 
		CombineDFT_1360_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1361
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1361_s.w[i] = real ; 
		CombineDFT_1361_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1362
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1362_s.w[i] = real ; 
		CombineDFT_1362_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1363
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1363_s.w[i] = real ; 
		CombineDFT_1363_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1364
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1364_s.w[i] = real ; 
		CombineDFT_1364_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1365
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1365_s.w[i] = real ; 
		CombineDFT_1365_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1366
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1366_s.w[i] = real ; 
		CombineDFT_1366_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1367
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1367_s.w[i] = real ; 
		CombineDFT_1367_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1368
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1368_s.w[i] = real ; 
		CombineDFT_1368_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1369
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1369_s.w[i] = real ; 
		CombineDFT_1369_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1370
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1370_s.w[i] = real ; 
		CombineDFT_1370_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1371
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1371_s.w[i] = real ; 
		CombineDFT_1371_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1372
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1372_s.w[i] = real ; 
		CombineDFT_1372_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1373
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1373_s.w[i] = real ; 
		CombineDFT_1373_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1374
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1374_s.w[i] = real ; 
		CombineDFT_1374_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1375
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1375_s.w[i] = real ; 
		CombineDFT_1375_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1376
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1376_s.w[i] = real ; 
		CombineDFT_1376_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1377
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1377_s.w[i] = real ; 
		CombineDFT_1377_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1378
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_1378_s.w[i] = real ; 
		CombineDFT_1378_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1381
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1381_s.w[i] = real ; 
		CombineDFT_1381_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1382
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1382_s.w[i] = real ; 
		CombineDFT_1382_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1383
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1383_s.w[i] = real ; 
		CombineDFT_1383_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1384
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1384_s.w[i] = real ; 
		CombineDFT_1384_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1385
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1385_s.w[i] = real ; 
		CombineDFT_1385_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1386
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1386_s.w[i] = real ; 
		CombineDFT_1386_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1387
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1387_s.w[i] = real ; 
		CombineDFT_1387_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1388
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1388_s.w[i] = real ; 
		CombineDFT_1388_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1389
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1389_s.w[i] = real ; 
		CombineDFT_1389_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1390
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1390_s.w[i] = real ; 
		CombineDFT_1390_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1391
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1391_s.w[i] = real ; 
		CombineDFT_1391_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1392
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1392_s.w[i] = real ; 
		CombineDFT_1392_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1393
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1393_s.w[i] = real ; 
		CombineDFT_1393_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1394
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1394_s.w[i] = real ; 
		CombineDFT_1394_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1395
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1395_s.w[i] = real ; 
		CombineDFT_1395_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1396
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_1396_s.w[i] = real ; 
		CombineDFT_1396_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1399
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_1399_s.w[i] = real ; 
		CombineDFT_1399_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1400
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_1400_s.w[i] = real ; 
		CombineDFT_1400_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1401
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_1401_s.w[i] = real ; 
		CombineDFT_1401_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1402
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_1402_s.w[i] = real ; 
		CombineDFT_1402_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1403
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_1403_s.w[i] = real ; 
		CombineDFT_1403_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1404
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_1404_s.w[i] = real ; 
		CombineDFT_1404_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1405
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_1405_s.w[i] = real ; 
		CombineDFT_1405_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1406
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_1406_s.w[i] = real ; 
		CombineDFT_1406_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1409
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_1409_s.w[i] = real ; 
		CombineDFT_1409_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1410
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_1410_s.w[i] = real ; 
		CombineDFT_1410_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1411
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_1411_s.w[i] = real ; 
		CombineDFT_1411_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1412
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_1412_s.w[i] = real ; 
		CombineDFT_1412_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1415
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_1415_s.w[i] = real ; 
		CombineDFT_1415_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1416
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_1416_s.w[i] = real ; 
		CombineDFT_1416_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_1187
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_1187_s.w[i] = real ; 
		CombineDFT_1187_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_1197();
			FFTTestSource_1199();
			FFTTestSource_1200();
		WEIGHTED_ROUND_ROBIN_Joiner_1198();
		WEIGHTED_ROUND_ROBIN_Splitter_1189();
			FFTReorderSimple_1166();
			WEIGHTED_ROUND_ROBIN_Splitter_1201();
				FFTReorderSimple_1203();
				FFTReorderSimple_1204();
			WEIGHTED_ROUND_ROBIN_Joiner_1202();
			WEIGHTED_ROUND_ROBIN_Splitter_1205();
				FFTReorderSimple_1207();
				FFTReorderSimple_1208();
				FFTReorderSimple_1209();
				FFTReorderSimple_1210();
			WEIGHTED_ROUND_ROBIN_Joiner_1206();
			WEIGHTED_ROUND_ROBIN_Splitter_1211();
				FFTReorderSimple_1213();
				FFTReorderSimple_1214();
				FFTReorderSimple_1215();
				FFTReorderSimple_1216();
				FFTReorderSimple_1217();
				FFTReorderSimple_1218();
				FFTReorderSimple_1219();
				FFTReorderSimple_1220();
			WEIGHTED_ROUND_ROBIN_Joiner_1212();
			WEIGHTED_ROUND_ROBIN_Splitter_1221();
				FFTReorderSimple_1223();
				FFTReorderSimple_1224();
				FFTReorderSimple_1225();
				FFTReorderSimple_1226();
				FFTReorderSimple_1227();
				FFTReorderSimple_1228();
				FFTReorderSimple_1229();
				FFTReorderSimple_1230();
				FFTReorderSimple_1231();
				FFTReorderSimple_1232();
				FFTReorderSimple_1233();
				FFTReorderSimple_1234();
				FFTReorderSimple_1235();
				FFTReorderSimple_1236();
				FFTReorderSimple_1237();
				FFTReorderSimple_1238();
			WEIGHTED_ROUND_ROBIN_Joiner_1222();
			WEIGHTED_ROUND_ROBIN_Splitter_1239();
				CombineDFT_1241();
				CombineDFT_1242();
				CombineDFT_1243();
				CombineDFT_1244();
				CombineDFT_1245();
				CombineDFT_1246();
				CombineDFT_1247();
				CombineDFT_1248();
				CombineDFT_1249();
				CombineDFT_1250();
				CombineDFT_1251();
				CombineDFT_1252();
				CombineDFT_1253();
				CombineDFT_1254();
				CombineDFT_1255();
				CombineDFT_1256();
				CombineDFT_1257();
				CombineDFT_1258();
				CombineDFT_1259();
				CombineDFT_1260();
				CombineDFT_1261();
				CombineDFT_1262();
				CombineDFT_1263();
				CombineDFT_1264();
				CombineDFT_1265();
				CombineDFT_1266();
				CombineDFT_1267();
				CombineDFT_1268();
				CombineDFT_1269();
				CombineDFT_1270();
			WEIGHTED_ROUND_ROBIN_Joiner_1240();
			WEIGHTED_ROUND_ROBIN_Splitter_1271();
				CombineDFT_1273();
				CombineDFT_1274();
				CombineDFT_1275();
				CombineDFT_1276();
				CombineDFT_1277();
				CombineDFT_1278();
				CombineDFT_1279();
				CombineDFT_1280();
				CombineDFT_1281();
				CombineDFT_1282();
				CombineDFT_1283();
				CombineDFT_1284();
				CombineDFT_1285();
				CombineDFT_1286();
				CombineDFT_1287();
				CombineDFT_1288();
			WEIGHTED_ROUND_ROBIN_Joiner_1272();
			WEIGHTED_ROUND_ROBIN_Splitter_1289();
				CombineDFT_1291();
				CombineDFT_1292();
				CombineDFT_1293();
				CombineDFT_1294();
				CombineDFT_1295();
				CombineDFT_1296();
				CombineDFT_1297();
				CombineDFT_1298();
			WEIGHTED_ROUND_ROBIN_Joiner_1290();
			WEIGHTED_ROUND_ROBIN_Splitter_1299();
				CombineDFT_1301();
				CombineDFT_1302();
				CombineDFT_1303();
				CombineDFT_1304();
			WEIGHTED_ROUND_ROBIN_Joiner_1300();
			WEIGHTED_ROUND_ROBIN_Splitter_1305();
				CombineDFT_1307();
				CombineDFT_1308();
			WEIGHTED_ROUND_ROBIN_Joiner_1306();
			CombineDFT_1176();
			FFTReorderSimple_1177();
			WEIGHTED_ROUND_ROBIN_Splitter_1309();
				FFTReorderSimple_1311();
				FFTReorderSimple_1312();
			WEIGHTED_ROUND_ROBIN_Joiner_1310();
			WEIGHTED_ROUND_ROBIN_Splitter_1313();
				FFTReorderSimple_1315();
				FFTReorderSimple_1316();
				FFTReorderSimple_1317();
				FFTReorderSimple_1318();
			WEIGHTED_ROUND_ROBIN_Joiner_1314();
			WEIGHTED_ROUND_ROBIN_Splitter_1319();
				FFTReorderSimple_1321();
				FFTReorderSimple_1322();
				FFTReorderSimple_1323();
				FFTReorderSimple_1324();
				FFTReorderSimple_1325();
				FFTReorderSimple_1326();
				FFTReorderSimple_1327();
				FFTReorderSimple_1328();
			WEIGHTED_ROUND_ROBIN_Joiner_1320();
			WEIGHTED_ROUND_ROBIN_Splitter_1329();
				FFTReorderSimple_1331();
				FFTReorderSimple_1332();
				FFTReorderSimple_1333();
				FFTReorderSimple_1334();
				FFTReorderSimple_1335();
				FFTReorderSimple_1336();
				FFTReorderSimple_1337();
				FFTReorderSimple_1338();
				FFTReorderSimple_1339();
				FFTReorderSimple_1340();
				FFTReorderSimple_1341();
				FFTReorderSimple_1342();
				FFTReorderSimple_1343();
				FFTReorderSimple_1344();
				FFTReorderSimple_1345();
				FFTReorderSimple_1346();
			WEIGHTED_ROUND_ROBIN_Joiner_1330();
			WEIGHTED_ROUND_ROBIN_Splitter_1347();
				CombineDFT_1349();
				CombineDFT_1350();
				CombineDFT_1351();
				CombineDFT_1352();
				CombineDFT_1353();
				CombineDFT_1354();
				CombineDFT_1355();
				CombineDFT_1356();
				CombineDFT_1357();
				CombineDFT_1358();
				CombineDFT_1359();
				CombineDFT_1360();
				CombineDFT_1361();
				CombineDFT_1362();
				CombineDFT_1363();
				CombineDFT_1364();
				CombineDFT_1365();
				CombineDFT_1366();
				CombineDFT_1367();
				CombineDFT_1368();
				CombineDFT_1369();
				CombineDFT_1370();
				CombineDFT_1371();
				CombineDFT_1372();
				CombineDFT_1373();
				CombineDFT_1374();
				CombineDFT_1375();
				CombineDFT_1376();
				CombineDFT_1377();
				CombineDFT_1378();
			WEIGHTED_ROUND_ROBIN_Joiner_1348();
			WEIGHTED_ROUND_ROBIN_Splitter_1379();
				CombineDFT_1381();
				CombineDFT_1382();
				CombineDFT_1383();
				CombineDFT_1384();
				CombineDFT_1385();
				CombineDFT_1386();
				CombineDFT_1387();
				CombineDFT_1388();
				CombineDFT_1389();
				CombineDFT_1390();
				CombineDFT_1391();
				CombineDFT_1392();
				CombineDFT_1393();
				CombineDFT_1394();
				CombineDFT_1395();
				CombineDFT_1396();
			WEIGHTED_ROUND_ROBIN_Joiner_1380();
			WEIGHTED_ROUND_ROBIN_Splitter_1397();
				CombineDFT_1399();
				CombineDFT_1400();
				CombineDFT_1401();
				CombineDFT_1402();
				CombineDFT_1403();
				CombineDFT_1404();
				CombineDFT_1405();
				CombineDFT_1406();
			WEIGHTED_ROUND_ROBIN_Joiner_1398();
			WEIGHTED_ROUND_ROBIN_Splitter_1407();
				CombineDFT_1409();
				CombineDFT_1410();
				CombineDFT_1411();
				CombineDFT_1412();
			WEIGHTED_ROUND_ROBIN_Joiner_1408();
			WEIGHTED_ROUND_ROBIN_Splitter_1413();
				CombineDFT_1415();
				CombineDFT_1416();
			WEIGHTED_ROUND_ROBIN_Joiner_1414();
			CombineDFT_1187();
		WEIGHTED_ROUND_ROBIN_Joiner_1190();
		FloatPrinter_1188();
	ENDFOR
	return EXIT_SUCCESS;
}
