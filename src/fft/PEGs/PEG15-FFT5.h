#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=480 on the compile command line
#else
#if BUF_SIZEMAX < 480
#error BUF_SIZEMAX too small, it must be at least 480
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif


void WEIGHTED_ROUND_ROBIN_Splitter_2393();
void source(buffer_void_t *chanin, buffer_complex_t *chanout);
void source_2395();
void source_2396();
void WEIGHTED_ROUND_ROBIN_Joiner_2394();
void WEIGHTED_ROUND_ROBIN_Splitter_2236();
void WEIGHTED_ROUND_ROBIN_Splitter_2238();
void WEIGHTED_ROUND_ROBIN_Splitter_2240();
void WEIGHTED_ROUND_ROBIN_Splitter_2242();
void Identity(buffer_complex_t *chanin, buffer_complex_t *chanout);
void Identity_2066();
void Identity_2068();
void WEIGHTED_ROUND_ROBIN_Joiner_2243();
void WEIGHTED_ROUND_ROBIN_Splitter_2244();
void Identity_2072();
void Identity_2074();
void WEIGHTED_ROUND_ROBIN_Joiner_2245();
void WEIGHTED_ROUND_ROBIN_Joiner_2241();
void WEIGHTED_ROUND_ROBIN_Splitter_2246();
void WEIGHTED_ROUND_ROBIN_Splitter_2248();
void Identity_2080();
void Identity_2082();
void WEIGHTED_ROUND_ROBIN_Joiner_2249();
void WEIGHTED_ROUND_ROBIN_Splitter_2250();
void Identity_2086();
void Identity_2088();
void WEIGHTED_ROUND_ROBIN_Joiner_2251();
void WEIGHTED_ROUND_ROBIN_Joiner_2247();
void WEIGHTED_ROUND_ROBIN_Joiner_2239();
void WEIGHTED_ROUND_ROBIN_Splitter_2252();
void WEIGHTED_ROUND_ROBIN_Splitter_2254();
void WEIGHTED_ROUND_ROBIN_Splitter_2256();
void Identity_2096();
void Identity_2098();
void WEIGHTED_ROUND_ROBIN_Joiner_2257();
void WEIGHTED_ROUND_ROBIN_Splitter_2258();
void Identity_2102();
void Identity_2104();
void WEIGHTED_ROUND_ROBIN_Joiner_2259();
void WEIGHTED_ROUND_ROBIN_Joiner_2255();
void WEIGHTED_ROUND_ROBIN_Splitter_2260();
void WEIGHTED_ROUND_ROBIN_Splitter_2262();
void Identity_2110();
void Identity_2112();
void WEIGHTED_ROUND_ROBIN_Joiner_2263();
void WEIGHTED_ROUND_ROBIN_Splitter_2264();
void Identity_2116();
void Identity_2118();
void WEIGHTED_ROUND_ROBIN_Joiner_2265();
void WEIGHTED_ROUND_ROBIN_Joiner_2261();
void WEIGHTED_ROUND_ROBIN_Joiner_2253();
void WEIGHTED_ROUND_ROBIN_Joiner_2237();
void WEIGHTED_ROUND_ROBIN_Splitter_2380();
void WEIGHTED_ROUND_ROBIN_Splitter_2381();
void Pre_CollapsedDataParallel_1(buffer_complex_t *chanin, buffer_complex_t *chanout);
void Pre_CollapsedDataParallel_1_2213();
void butterfly(buffer_complex_t *chanin, buffer_complex_t *chanout);
void butterfly_2120();
void Post_CollapsedDataParallel_2(buffer_complex_t *chanin, buffer_complex_t *chanout);
void Post_CollapsedDataParallel_2_2214();
void Pre_CollapsedDataParallel_1_2216();
void butterfly_2121();
void Post_CollapsedDataParallel_2_2217();
void Pre_CollapsedDataParallel_1_2219();
void butterfly_2122();
void Post_CollapsedDataParallel_2_2220();
void Pre_CollapsedDataParallel_1_2222();
void butterfly_2123();
void Post_CollapsedDataParallel_2_2223();
void WEIGHTED_ROUND_ROBIN_Joiner_2382();
void WEIGHTED_ROUND_ROBIN_Splitter_2383();
void Pre_CollapsedDataParallel_1_2225();
void butterfly_2124();
void Post_CollapsedDataParallel_2_2226();
void Pre_CollapsedDataParallel_1_2228();
void butterfly_2125();
void Post_CollapsedDataParallel_2_2229();
void Pre_CollapsedDataParallel_1_2231();
void butterfly_2126();
void Post_CollapsedDataParallel_2_2232();
void Pre_CollapsedDataParallel_1_2234();
void butterfly_2127();
void Post_CollapsedDataParallel_2_2235();
void WEIGHTED_ROUND_ROBIN_Joiner_2384();
void WEIGHTED_ROUND_ROBIN_Joiner_2385();
void WEIGHTED_ROUND_ROBIN_Splitter_2386();
void WEIGHTED_ROUND_ROBIN_Splitter_2387();
void WEIGHTED_ROUND_ROBIN_Splitter_2270();
void butterfly_2129();
void butterfly_2130();
void WEIGHTED_ROUND_ROBIN_Joiner_2271();
void WEIGHTED_ROUND_ROBIN_Splitter_2272();
void butterfly_2131();
void butterfly_2132();
void WEIGHTED_ROUND_ROBIN_Joiner_2273();
void WEIGHTED_ROUND_ROBIN_Joiner_2388();
void WEIGHTED_ROUND_ROBIN_Splitter_2389();
void WEIGHTED_ROUND_ROBIN_Splitter_2274();
void butterfly_2133();
void butterfly_2134();
void WEIGHTED_ROUND_ROBIN_Joiner_2275();
void WEIGHTED_ROUND_ROBIN_Splitter_2276();
void butterfly_2135();
void butterfly_2136();
void WEIGHTED_ROUND_ROBIN_Joiner_2277();
void WEIGHTED_ROUND_ROBIN_Joiner_2390();
void WEIGHTED_ROUND_ROBIN_Joiner_2391();
void WEIGHTED_ROUND_ROBIN_Splitter_2278();
void WEIGHTED_ROUND_ROBIN_Splitter_2280();
void butterfly_2138();
void butterfly_2139();
void butterfly_2140();
void butterfly_2141();
void WEIGHTED_ROUND_ROBIN_Joiner_2281();
void WEIGHTED_ROUND_ROBIN_Splitter_2282();
void butterfly_2142();
void butterfly_2143();
void butterfly_2144();
void butterfly_2145();
void WEIGHTED_ROUND_ROBIN_Joiner_2283();
void WEIGHTED_ROUND_ROBIN_Joiner_2279();
void WEIGHTED_ROUND_ROBIN_Splitter_2397();
void magnitude(buffer_complex_t *chanin, buffer_float_t *chanout);
void magnitude_2399();
void magnitude_2400();
void magnitude_2401();
void magnitude_2402();
void magnitude_2403();
void magnitude_2404();
void magnitude_2405();
void magnitude_2406();
void magnitude_2407();
void magnitude_2408();
void magnitude_2409();
void magnitude_2410();
void magnitude_2411();
void magnitude_2412();
void magnitude_2413();
void WEIGHTED_ROUND_ROBIN_Joiner_2398();
void sink(buffer_float_t *chanin);
void sink_2147();

#ifdef __cplusplus
}
#endif
#endif
