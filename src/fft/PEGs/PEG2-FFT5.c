#include "PEG2-FFT5.h"

buffer_complex_t butterfly_9804Post_CollapsedDataParallel_2_9900;
buffer_complex_t butterfly_9805Post_CollapsedDataParallel_2_9903;
buffer_float_t SplitJoin24_magnitude_Fiss_10091_10132_join[2];
buffer_complex_t SplitJoin72_SplitJoin59_SplitJoin59_AnonFilter_a0_9767_10013_10095_10111_join[2];
buffer_complex_t SplitJoin84_SplitJoin71_SplitJoin71_AnonFilter_a0_9783_10021_10098_10115_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_9920WEIGHTED_ROUND_ROBIN_Splitter_10063;
buffer_complex_t butterfly_9806Post_CollapsedDataParallel_2_9906;
buffer_complex_t SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9745_9969_10086_10106_join[2];
buffer_complex_t butterfly_9807Post_CollapsedDataParallel_2_9909;
buffer_complex_t SplitJoin22_SplitJoin16_SplitJoin16_split2_9735_9978_10059_10130_join[4];
buffer_complex_t SplitJoin10_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_Hier_10088_10119_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_9914butterfly_9809;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_10077WEIGHTED_ROUND_ROBIN_Splitter_9919;
buffer_complex_t SplitJoin76_SplitJoin63_SplitJoin63_AnonFilter_a0_9773_10016_10052_10112_join[2];
buffer_complex_t SplitJoin90_SplitJoin77_SplitJoin77_AnonFilter_a0_9791_10025_10100_10117_split[2];
buffer_complex_t SplitJoin68_SplitJoin55_SplitJoin55_AnonFilter_a0_9761_10010_10094_10110_split[2];
buffer_complex_t SplitJoin31_SplitJoin22_SplitJoin22_split2_9737_9983_10061_10131_split[4];
buffer_complex_t SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9743_9968_10051_10105_join[2];
buffer_complex_t SplitJoin48_SplitJoin37_SplitJoin37_split2_9728_9995_10058_10128_split[2];
buffer_complex_t SplitJoin78_SplitJoin65_SplitJoin65_AnonFilter_a0_9775_10017_10096_10113_split[2];
buffer_complex_t SplitJoin80_SplitJoin67_SplitJoin67_AnonFilter_a0_9777_10018_10097_10114_join[2];
buffer_complex_t SplitJoin90_SplitJoin77_SplitJoin77_AnonFilter_a0_9791_10025_10100_10117_join[2];
buffer_complex_t butterfly_9803Post_CollapsedDataParallel_2_9897;
buffer_complex_t butterfly_9808Post_CollapsedDataParallel_2_9912;
buffer_complex_t SplitJoin42_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child1_10062_10126_split[2];
buffer_complex_t SplitJoin66_SplitJoin53_SplitJoin53_AnonFilter_a0_9759_10009_10093_10109_split[2];
buffer_complex_t SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9743_9968_10051_10105_split[2];
buffer_complex_t SplitJoin84_SplitJoin71_SplitJoin71_AnonFilter_a0_9783_10021_10098_10115_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_9962WEIGHTED_ROUND_ROBIN_Splitter_10080;
buffer_complex_t Pre_CollapsedDataParallel_1_9917butterfly_9810;
buffer_complex_t SplitJoin14_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_Hier_10089_10122_split[2];
buffer_complex_t SplitJoin44_SplitJoin33_SplitJoin33_split2_9726_9992_10056_10127_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_9899butterfly_9804;
buffer_complex_t SplitJoin24_magnitude_Fiss_10091_10132_split[2];
buffer_complex_t SplitJoin18_SplitJoin12_SplitJoin12_split2_9722_9975_10053_10124_split[2];
buffer_complex_t SplitJoin38_SplitJoin29_SplitJoin29_split2_9724_9989_10055_10125_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_9902butterfly_9805;
buffer_complex_t SplitJoin72_SplitJoin59_SplitJoin59_AnonFilter_a0_9767_10013_10095_10111_split[2];
buffer_complex_t SplitJoin94_SplitJoin81_SplitJoin81_AnonFilter_a0_9797_10028_10101_10118_join[2];
buffer_complex_t SplitJoin55_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child1_10057_10121_split[4];
buffer_complex_t SplitJoin20_SplitJoin14_SplitJoin14_split1_9733_9977_10090_10129_join[2];
buffer_complex_t SplitJoin0_source_Fiss_10084_10103_join[2];
buffer_complex_t SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9741_9967_10085_10104_join[2];
buffer_complex_t SplitJoin66_SplitJoin53_SplitJoin53_AnonFilter_a0_9759_10009_10093_10109_join[2];
buffer_complex_t SplitJoin38_SplitJoin29_SplitJoin29_split2_9724_9989_10055_10125_split[2];
buffer_complex_t SplitJoin88_SplitJoin75_SplitJoin75_AnonFilter_a0_9789_10024_10099_10116_join[2];
buffer_complex_t SplitJoin12_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child0_10054_10120_split[4];
buffer_complex_t SplitJoin22_SplitJoin16_SplitJoin16_split2_9735_9978_10059_10130_split[4];
buffer_complex_t SplitJoin12_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child0_10054_10120_join[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_10074WEIGHTED_ROUND_ROBIN_Splitter_9961;
buffer_complex_t SplitJoin16_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child0_10060_10123_join[2];
buffer_complex_t SplitJoin78_SplitJoin65_SplitJoin65_AnonFilter_a0_9775_10017_10096_10113_join[2];
buffer_complex_t SplitJoin16_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child0_10060_10123_split[2];
buffer_complex_t Pre_CollapsedDataParallel_1_9908butterfly_9807;
buffer_complex_t SplitJoin88_SplitJoin75_SplitJoin75_AnonFilter_a0_9789_10024_10099_10116_split[2];
buffer_complex_t SplitJoin94_SplitJoin81_SplitJoin81_AnonFilter_a0_9797_10028_10101_10118_split[2];
buffer_complex_t SplitJoin18_SplitJoin12_SplitJoin12_split2_9722_9975_10053_10124_join[2];
buffer_complex_t SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_9747_9970_10087_10107_split[2];
buffer_complex_t SplitJoin31_SplitJoin22_SplitJoin22_split2_9737_9983_10061_10131_join[4];
buffer_complex_t SplitJoin10_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_Hier_10088_10119_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_10068WEIGHTED_ROUND_ROBIN_Splitter_10069;
buffer_complex_t SplitJoin55_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child1_10057_10121_join[4];
buffer_complex_t SplitJoin62_SplitJoin49_SplitJoin49_AnonFilter_a0_9753_10006_10092_10108_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_9905butterfly_9806;
buffer_complex_t SplitJoin20_SplitJoin14_SplitJoin14_split1_9733_9977_10090_10129_split[2];
buffer_complex_t SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9741_9967_10085_10104_split[2];
buffer_complex_t SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_9747_9970_10087_10107_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_9896butterfly_9803;
buffer_complex_t SplitJoin48_SplitJoin37_SplitJoin37_split2_9728_9995_10058_10128_join[2];
buffer_complex_t SplitJoin42_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child1_10062_10126_join[2];
buffer_complex_t butterfly_9809Post_CollapsedDataParallel_2_9915;
buffer_complex_t SplitJoin68_SplitJoin55_SplitJoin55_AnonFilter_a0_9761_10010_10094_10110_join[2];
buffer_complex_t SplitJoin14_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_Hier_10089_10122_join[2];
buffer_complex_t SplitJoin0_source_Fiss_10084_10103_split[2];
buffer_complex_t Pre_CollapsedDataParallel_1_9911butterfly_9808;
buffer_complex_t butterfly_9810Post_CollapsedDataParallel_2_9918;
buffer_complex_t SplitJoin62_SplitJoin49_SplitJoin49_AnonFilter_a0_9753_10006_10092_10108_split[2];
buffer_complex_t SplitJoin80_SplitJoin67_SplitJoin67_AnonFilter_a0_9777_10018_10097_10114_split[2];
buffer_complex_t SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9745_9969_10086_10106_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10081sink_9830;
buffer_complex_t SplitJoin44_SplitJoin33_SplitJoin33_split2_9726_9992_10056_10127_split[2];
buffer_complex_t SplitJoin76_SplitJoin63_SplitJoin63_AnonFilter_a0_9773_10016_10052_10112_split[2];



void source(buffer_void_t *chanin, buffer_complex_t *chanout) {
	complex_t t;
	t.imag = 0.0 ; 
	t.real = 0.9501 ; 
	push_complex(&(*chanout), t) ; 
	t.real = 0.2311 ; 
	push_complex(&(*chanout), t) ; 
	t.real = 0.6068 ; 
	push_complex(&(*chanout), t) ; 
	t.real = 0.486 ; 
	push_complex(&(*chanout), t) ; 
	t.real = 0.8913 ; 
	push_complex(&(*chanout), t) ; 
	t.real = 0.7621 ; 
	push_complex(&(*chanout), t) ; 
	t.real = 0.4565 ; 
	push_complex(&(*chanout), t) ; 
	t.real = 0.0185 ; 
	push_complex(&(*chanout), t) ; 
}


void source_10078() {
	source(&(SplitJoin0_source_Fiss_10084_10103_split[0]), &(SplitJoin0_source_Fiss_10084_10103_join[0]));
}

void source_10079() {
	source(&(SplitJoin0_source_Fiss_10084_10103_split[1]), &(SplitJoin0_source_Fiss_10084_10103_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_10076() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_10077() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_10077WEIGHTED_ROUND_ROBIN_Splitter_9919, pop_complex(&SplitJoin0_source_Fiss_10084_10103_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_10077WEIGHTED_ROUND_ROBIN_Splitter_9919, pop_complex(&SplitJoin0_source_Fiss_10084_10103_join[1]));
	ENDFOR
}

void Identity(buffer_complex_t *chanin, buffer_complex_t *chanout) {
	complex_t __tmp2614 = pop_complex(&(*chanin));
	push_complex(&(*chanout), __tmp2614) ; 
}


void Identity_9749() {
	Identity(&(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_9747_9970_10087_10107_split[0]), &(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_9747_9970_10087_10107_join[0]));
}

void Identity_9751() {
	Identity(&(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_9747_9970_10087_10107_split[1]), &(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_9747_9970_10087_10107_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_9925() {
	push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_9747_9970_10087_10107_split[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9745_9969_10086_10106_split[0]));
	push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_9747_9970_10087_10107_split[1], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9745_9969_10086_10106_split[0]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_9926() {
	push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9745_9969_10086_10106_join[0], pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_9747_9970_10087_10107_join[0]));
	push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9745_9969_10086_10106_join[0], pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_9747_9970_10087_10107_join[1]));
}

void Identity_9755() {
	Identity(&(SplitJoin62_SplitJoin49_SplitJoin49_AnonFilter_a0_9753_10006_10092_10108_split[0]), &(SplitJoin62_SplitJoin49_SplitJoin49_AnonFilter_a0_9753_10006_10092_10108_join[0]));
}

void Identity_9757() {
	Identity(&(SplitJoin62_SplitJoin49_SplitJoin49_AnonFilter_a0_9753_10006_10092_10108_split[1]), &(SplitJoin62_SplitJoin49_SplitJoin49_AnonFilter_a0_9753_10006_10092_10108_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_9927() {
	push_complex(&SplitJoin62_SplitJoin49_SplitJoin49_AnonFilter_a0_9753_10006_10092_10108_split[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9745_9969_10086_10106_split[1]));
	push_complex(&SplitJoin62_SplitJoin49_SplitJoin49_AnonFilter_a0_9753_10006_10092_10108_split[1], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9745_9969_10086_10106_split[1]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_9928() {
	push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9745_9969_10086_10106_join[1], pop_complex(&SplitJoin62_SplitJoin49_SplitJoin49_AnonFilter_a0_9753_10006_10092_10108_join[0]));
	push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9745_9969_10086_10106_join[1], pop_complex(&SplitJoin62_SplitJoin49_SplitJoin49_AnonFilter_a0_9753_10006_10092_10108_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_9923() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9745_9969_10086_10106_split[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9743_9968_10051_10105_split[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9745_9969_10086_10106_split[1], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9743_9968_10051_10105_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9924() {
	push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9743_9968_10051_10105_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9745_9969_10086_10106_join[0]));
	push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9743_9968_10051_10105_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9745_9969_10086_10106_join[0]));
	push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9743_9968_10051_10105_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9745_9969_10086_10106_join[1]));
	push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9743_9968_10051_10105_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9745_9969_10086_10106_join[1]));
}

void Identity_9763() {
	Identity(&(SplitJoin68_SplitJoin55_SplitJoin55_AnonFilter_a0_9761_10010_10094_10110_split[0]), &(SplitJoin68_SplitJoin55_SplitJoin55_AnonFilter_a0_9761_10010_10094_10110_join[0]));
}

void Identity_9765() {
	Identity(&(SplitJoin68_SplitJoin55_SplitJoin55_AnonFilter_a0_9761_10010_10094_10110_split[1]), &(SplitJoin68_SplitJoin55_SplitJoin55_AnonFilter_a0_9761_10010_10094_10110_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_9931() {
	push_complex(&SplitJoin68_SplitJoin55_SplitJoin55_AnonFilter_a0_9761_10010_10094_10110_split[0], pop_complex(&SplitJoin66_SplitJoin53_SplitJoin53_AnonFilter_a0_9759_10009_10093_10109_split[0]));
	push_complex(&SplitJoin68_SplitJoin55_SplitJoin55_AnonFilter_a0_9761_10010_10094_10110_split[1], pop_complex(&SplitJoin66_SplitJoin53_SplitJoin53_AnonFilter_a0_9759_10009_10093_10109_split[0]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_9932() {
	push_complex(&SplitJoin66_SplitJoin53_SplitJoin53_AnonFilter_a0_9759_10009_10093_10109_join[0], pop_complex(&SplitJoin68_SplitJoin55_SplitJoin55_AnonFilter_a0_9761_10010_10094_10110_join[0]));
	push_complex(&SplitJoin66_SplitJoin53_SplitJoin53_AnonFilter_a0_9759_10009_10093_10109_join[0], pop_complex(&SplitJoin68_SplitJoin55_SplitJoin55_AnonFilter_a0_9761_10010_10094_10110_join[1]));
}

void Identity_9769() {
	Identity(&(SplitJoin72_SplitJoin59_SplitJoin59_AnonFilter_a0_9767_10013_10095_10111_split[0]), &(SplitJoin72_SplitJoin59_SplitJoin59_AnonFilter_a0_9767_10013_10095_10111_join[0]));
}

void Identity_9771() {
	Identity(&(SplitJoin72_SplitJoin59_SplitJoin59_AnonFilter_a0_9767_10013_10095_10111_split[1]), &(SplitJoin72_SplitJoin59_SplitJoin59_AnonFilter_a0_9767_10013_10095_10111_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_9933() {
	push_complex(&SplitJoin72_SplitJoin59_SplitJoin59_AnonFilter_a0_9767_10013_10095_10111_split[0], pop_complex(&SplitJoin66_SplitJoin53_SplitJoin53_AnonFilter_a0_9759_10009_10093_10109_split[1]));
	push_complex(&SplitJoin72_SplitJoin59_SplitJoin59_AnonFilter_a0_9767_10013_10095_10111_split[1], pop_complex(&SplitJoin66_SplitJoin53_SplitJoin53_AnonFilter_a0_9759_10009_10093_10109_split[1]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_9934() {
	push_complex(&SplitJoin66_SplitJoin53_SplitJoin53_AnonFilter_a0_9759_10009_10093_10109_join[1], pop_complex(&SplitJoin72_SplitJoin59_SplitJoin59_AnonFilter_a0_9767_10013_10095_10111_join[0]));
	push_complex(&SplitJoin66_SplitJoin53_SplitJoin53_AnonFilter_a0_9759_10009_10093_10109_join[1], pop_complex(&SplitJoin72_SplitJoin59_SplitJoin59_AnonFilter_a0_9767_10013_10095_10111_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_9929() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin66_SplitJoin53_SplitJoin53_AnonFilter_a0_9759_10009_10093_10109_split[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9743_9968_10051_10105_split[1]));
		push_complex(&SplitJoin66_SplitJoin53_SplitJoin53_AnonFilter_a0_9759_10009_10093_10109_split[1], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9743_9968_10051_10105_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9930() {
	push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9743_9968_10051_10105_join[1], pop_complex(&SplitJoin66_SplitJoin53_SplitJoin53_AnonFilter_a0_9759_10009_10093_10109_join[0]));
	push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9743_9968_10051_10105_join[1], pop_complex(&SplitJoin66_SplitJoin53_SplitJoin53_AnonFilter_a0_9759_10009_10093_10109_join[0]));
	push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9743_9968_10051_10105_join[1], pop_complex(&SplitJoin66_SplitJoin53_SplitJoin53_AnonFilter_a0_9759_10009_10093_10109_join[1]));
	push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9743_9968_10051_10105_join[1], pop_complex(&SplitJoin66_SplitJoin53_SplitJoin53_AnonFilter_a0_9759_10009_10093_10109_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_9921() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9743_9968_10051_10105_split[0], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9741_9967_10085_10104_split[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9743_9968_10051_10105_split[1], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9741_9967_10085_10104_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9922() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9741_9967_10085_10104_join[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9743_9968_10051_10105_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9741_9967_10085_10104_join[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9743_9968_10051_10105_join[1]));
	ENDFOR
}

void Identity_9779() {
	Identity(&(SplitJoin80_SplitJoin67_SplitJoin67_AnonFilter_a0_9777_10018_10097_10114_split[0]), &(SplitJoin80_SplitJoin67_SplitJoin67_AnonFilter_a0_9777_10018_10097_10114_join[0]));
}

void Identity_9781() {
	Identity(&(SplitJoin80_SplitJoin67_SplitJoin67_AnonFilter_a0_9777_10018_10097_10114_split[1]), &(SplitJoin80_SplitJoin67_SplitJoin67_AnonFilter_a0_9777_10018_10097_10114_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_9939() {
	push_complex(&SplitJoin80_SplitJoin67_SplitJoin67_AnonFilter_a0_9777_10018_10097_10114_split[0], pop_complex(&SplitJoin78_SplitJoin65_SplitJoin65_AnonFilter_a0_9775_10017_10096_10113_split[0]));
	push_complex(&SplitJoin80_SplitJoin67_SplitJoin67_AnonFilter_a0_9777_10018_10097_10114_split[1], pop_complex(&SplitJoin78_SplitJoin65_SplitJoin65_AnonFilter_a0_9775_10017_10096_10113_split[0]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_9940() {
	push_complex(&SplitJoin78_SplitJoin65_SplitJoin65_AnonFilter_a0_9775_10017_10096_10113_join[0], pop_complex(&SplitJoin80_SplitJoin67_SplitJoin67_AnonFilter_a0_9777_10018_10097_10114_join[0]));
	push_complex(&SplitJoin78_SplitJoin65_SplitJoin65_AnonFilter_a0_9775_10017_10096_10113_join[0], pop_complex(&SplitJoin80_SplitJoin67_SplitJoin67_AnonFilter_a0_9777_10018_10097_10114_join[1]));
}

void Identity_9785() {
	Identity(&(SplitJoin84_SplitJoin71_SplitJoin71_AnonFilter_a0_9783_10021_10098_10115_split[0]), &(SplitJoin84_SplitJoin71_SplitJoin71_AnonFilter_a0_9783_10021_10098_10115_join[0]));
}

void Identity_9787() {
	Identity(&(SplitJoin84_SplitJoin71_SplitJoin71_AnonFilter_a0_9783_10021_10098_10115_split[1]), &(SplitJoin84_SplitJoin71_SplitJoin71_AnonFilter_a0_9783_10021_10098_10115_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_9941() {
	push_complex(&SplitJoin84_SplitJoin71_SplitJoin71_AnonFilter_a0_9783_10021_10098_10115_split[0], pop_complex(&SplitJoin78_SplitJoin65_SplitJoin65_AnonFilter_a0_9775_10017_10096_10113_split[1]));
	push_complex(&SplitJoin84_SplitJoin71_SplitJoin71_AnonFilter_a0_9783_10021_10098_10115_split[1], pop_complex(&SplitJoin78_SplitJoin65_SplitJoin65_AnonFilter_a0_9775_10017_10096_10113_split[1]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_9942() {
	push_complex(&SplitJoin78_SplitJoin65_SplitJoin65_AnonFilter_a0_9775_10017_10096_10113_join[1], pop_complex(&SplitJoin84_SplitJoin71_SplitJoin71_AnonFilter_a0_9783_10021_10098_10115_join[0]));
	push_complex(&SplitJoin78_SplitJoin65_SplitJoin65_AnonFilter_a0_9775_10017_10096_10113_join[1], pop_complex(&SplitJoin84_SplitJoin71_SplitJoin71_AnonFilter_a0_9783_10021_10098_10115_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_9937() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin78_SplitJoin65_SplitJoin65_AnonFilter_a0_9775_10017_10096_10113_split[0], pop_complex(&SplitJoin76_SplitJoin63_SplitJoin63_AnonFilter_a0_9773_10016_10052_10112_split[0]));
		push_complex(&SplitJoin78_SplitJoin65_SplitJoin65_AnonFilter_a0_9775_10017_10096_10113_split[1], pop_complex(&SplitJoin76_SplitJoin63_SplitJoin63_AnonFilter_a0_9773_10016_10052_10112_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9938() {
	push_complex(&SplitJoin76_SplitJoin63_SplitJoin63_AnonFilter_a0_9773_10016_10052_10112_join[0], pop_complex(&SplitJoin78_SplitJoin65_SplitJoin65_AnonFilter_a0_9775_10017_10096_10113_join[0]));
	push_complex(&SplitJoin76_SplitJoin63_SplitJoin63_AnonFilter_a0_9773_10016_10052_10112_join[0], pop_complex(&SplitJoin78_SplitJoin65_SplitJoin65_AnonFilter_a0_9775_10017_10096_10113_join[0]));
	push_complex(&SplitJoin76_SplitJoin63_SplitJoin63_AnonFilter_a0_9773_10016_10052_10112_join[0], pop_complex(&SplitJoin78_SplitJoin65_SplitJoin65_AnonFilter_a0_9775_10017_10096_10113_join[1]));
	push_complex(&SplitJoin76_SplitJoin63_SplitJoin63_AnonFilter_a0_9773_10016_10052_10112_join[0], pop_complex(&SplitJoin78_SplitJoin65_SplitJoin65_AnonFilter_a0_9775_10017_10096_10113_join[1]));
}

void Identity_9793() {
	Identity(&(SplitJoin90_SplitJoin77_SplitJoin77_AnonFilter_a0_9791_10025_10100_10117_split[0]), &(SplitJoin90_SplitJoin77_SplitJoin77_AnonFilter_a0_9791_10025_10100_10117_join[0]));
}

void Identity_9795() {
	Identity(&(SplitJoin90_SplitJoin77_SplitJoin77_AnonFilter_a0_9791_10025_10100_10117_split[1]), &(SplitJoin90_SplitJoin77_SplitJoin77_AnonFilter_a0_9791_10025_10100_10117_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_9945() {
	push_complex(&SplitJoin90_SplitJoin77_SplitJoin77_AnonFilter_a0_9791_10025_10100_10117_split[0], pop_complex(&SplitJoin88_SplitJoin75_SplitJoin75_AnonFilter_a0_9789_10024_10099_10116_split[0]));
	push_complex(&SplitJoin90_SplitJoin77_SplitJoin77_AnonFilter_a0_9791_10025_10100_10117_split[1], pop_complex(&SplitJoin88_SplitJoin75_SplitJoin75_AnonFilter_a0_9789_10024_10099_10116_split[0]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_9946() {
	push_complex(&SplitJoin88_SplitJoin75_SplitJoin75_AnonFilter_a0_9789_10024_10099_10116_join[0], pop_complex(&SplitJoin90_SplitJoin77_SplitJoin77_AnonFilter_a0_9791_10025_10100_10117_join[0]));
	push_complex(&SplitJoin88_SplitJoin75_SplitJoin75_AnonFilter_a0_9789_10024_10099_10116_join[0], pop_complex(&SplitJoin90_SplitJoin77_SplitJoin77_AnonFilter_a0_9791_10025_10100_10117_join[1]));
}

void Identity_9799() {
	Identity(&(SplitJoin94_SplitJoin81_SplitJoin81_AnonFilter_a0_9797_10028_10101_10118_split[0]), &(SplitJoin94_SplitJoin81_SplitJoin81_AnonFilter_a0_9797_10028_10101_10118_join[0]));
}

void Identity_9801() {
	Identity(&(SplitJoin94_SplitJoin81_SplitJoin81_AnonFilter_a0_9797_10028_10101_10118_split[1]), &(SplitJoin94_SplitJoin81_SplitJoin81_AnonFilter_a0_9797_10028_10101_10118_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_9947() {
	push_complex(&SplitJoin94_SplitJoin81_SplitJoin81_AnonFilter_a0_9797_10028_10101_10118_split[0], pop_complex(&SplitJoin88_SplitJoin75_SplitJoin75_AnonFilter_a0_9789_10024_10099_10116_split[1]));
	push_complex(&SplitJoin94_SplitJoin81_SplitJoin81_AnonFilter_a0_9797_10028_10101_10118_split[1], pop_complex(&SplitJoin88_SplitJoin75_SplitJoin75_AnonFilter_a0_9789_10024_10099_10116_split[1]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_9948() {
	push_complex(&SplitJoin88_SplitJoin75_SplitJoin75_AnonFilter_a0_9789_10024_10099_10116_join[1], pop_complex(&SplitJoin94_SplitJoin81_SplitJoin81_AnonFilter_a0_9797_10028_10101_10118_join[0]));
	push_complex(&SplitJoin88_SplitJoin75_SplitJoin75_AnonFilter_a0_9789_10024_10099_10116_join[1], pop_complex(&SplitJoin94_SplitJoin81_SplitJoin81_AnonFilter_a0_9797_10028_10101_10118_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_9943() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin88_SplitJoin75_SplitJoin75_AnonFilter_a0_9789_10024_10099_10116_split[0], pop_complex(&SplitJoin76_SplitJoin63_SplitJoin63_AnonFilter_a0_9773_10016_10052_10112_split[1]));
		push_complex(&SplitJoin88_SplitJoin75_SplitJoin75_AnonFilter_a0_9789_10024_10099_10116_split[1], pop_complex(&SplitJoin76_SplitJoin63_SplitJoin63_AnonFilter_a0_9773_10016_10052_10112_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9944() {
	push_complex(&SplitJoin76_SplitJoin63_SplitJoin63_AnonFilter_a0_9773_10016_10052_10112_join[1], pop_complex(&SplitJoin88_SplitJoin75_SplitJoin75_AnonFilter_a0_9789_10024_10099_10116_join[0]));
	push_complex(&SplitJoin76_SplitJoin63_SplitJoin63_AnonFilter_a0_9773_10016_10052_10112_join[1], pop_complex(&SplitJoin88_SplitJoin75_SplitJoin75_AnonFilter_a0_9789_10024_10099_10116_join[0]));
	push_complex(&SplitJoin76_SplitJoin63_SplitJoin63_AnonFilter_a0_9773_10016_10052_10112_join[1], pop_complex(&SplitJoin88_SplitJoin75_SplitJoin75_AnonFilter_a0_9789_10024_10099_10116_join[1]));
	push_complex(&SplitJoin76_SplitJoin63_SplitJoin63_AnonFilter_a0_9773_10016_10052_10112_join[1], pop_complex(&SplitJoin88_SplitJoin75_SplitJoin75_AnonFilter_a0_9789_10024_10099_10116_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_9935() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_complex(&SplitJoin76_SplitJoin63_SplitJoin63_AnonFilter_a0_9773_10016_10052_10112_split[0], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9741_9967_10085_10104_split[1]));
		push_complex(&SplitJoin76_SplitJoin63_SplitJoin63_AnonFilter_a0_9773_10016_10052_10112_split[1], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9741_9967_10085_10104_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9936() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9741_9967_10085_10104_join[1], pop_complex(&SplitJoin76_SplitJoin63_SplitJoin63_AnonFilter_a0_9773_10016_10052_10112_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9741_9967_10085_10104_join[1], pop_complex(&SplitJoin76_SplitJoin63_SplitJoin63_AnonFilter_a0_9773_10016_10052_10112_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9919() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9741_9967_10085_10104_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_10077WEIGHTED_ROUND_ROBIN_Splitter_9919));
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9741_9967_10085_10104_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_10077WEIGHTED_ROUND_ROBIN_Splitter_9919));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9920() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9920WEIGHTED_ROUND_ROBIN_Splitter_10063, pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9741_9967_10085_10104_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9920WEIGHTED_ROUND_ROBIN_Splitter_10063, pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9741_9967_10085_10104_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1(buffer_complex_t *chanin, buffer_complex_t *chanout) {
 {
 {
	int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
	FOR(int, _i, 0,  < , 2, _i++) {
		push_complex(&(*chanout), peek_complex(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
		iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
	}
	ENDFOR
}
}
}
	pop_complex(&(*chanin)) ; 
}


void Pre_CollapsedDataParallel_1_9896() {
	Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child0_10054_10120_split[0]), &(Pre_CollapsedDataParallel_1_9896butterfly_9803));
}

void butterfly(buffer_complex_t *chanin, buffer_complex_t *chanout) {
	complex_t WN1;
	complex_t WN2;
	complex_t one = ((complex_t) pop_complex(&(*chanin)));
	complex_t two = ((complex_t) pop_complex(&(*chanin)));
	complex_t __sa1;
	complex_t __sa2;
	WN1.real = 1.0 ; 
	WN1.imag = -0.0 ; 
	WN2.real = -1.0 ; 
	WN2.imag = 8.742278E-8 ; 
	__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
	__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
	push_complex(&(*chanout), __sa1) ; 
	__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
	__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
	push_complex(&(*chanout), __sa2) ; 
}


void butterfly_9803() {
	butterfly(&(Pre_CollapsedDataParallel_1_9896butterfly_9803), &(butterfly_9803Post_CollapsedDataParallel_2_9897));
}

void Post_CollapsedDataParallel_2(buffer_complex_t *chanin, buffer_complex_t *chanout) {
 {
 {
	FOR(int, _k, 0,  < , 2, _k++) {
 {
		push_complex(&(*chanout), peek_complex(&(*chanin), (_k + 0))) ; 
	}
	}
	ENDFOR
}
}
	pop_complex(&(*chanin)) ; 
}


void Post_CollapsedDataParallel_2_9897() {
	Post_CollapsedDataParallel_2(&(butterfly_9803Post_CollapsedDataParallel_2_9897), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child0_10054_10120_join[0]));
}

void Pre_CollapsedDataParallel_1_9899() {
	Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child0_10054_10120_split[1]), &(Pre_CollapsedDataParallel_1_9899butterfly_9804));
}

void butterfly_9804() {
	butterfly(&(Pre_CollapsedDataParallel_1_9899butterfly_9804), &(butterfly_9804Post_CollapsedDataParallel_2_9900));
}

void Post_CollapsedDataParallel_2_9900() {
	Post_CollapsedDataParallel_2(&(butterfly_9804Post_CollapsedDataParallel_2_9900), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child0_10054_10120_join[1]));
}

void Pre_CollapsedDataParallel_1_9902() {
	Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child0_10054_10120_split[2]), &(Pre_CollapsedDataParallel_1_9902butterfly_9805));
}

void butterfly_9805() {
	butterfly(&(Pre_CollapsedDataParallel_1_9902butterfly_9805), &(butterfly_9805Post_CollapsedDataParallel_2_9903));
}

void Post_CollapsedDataParallel_2_9903() {
	Post_CollapsedDataParallel_2(&(butterfly_9805Post_CollapsedDataParallel_2_9903), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child0_10054_10120_join[2]));
}

void Pre_CollapsedDataParallel_1_9905() {
	Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child0_10054_10120_split[3]), &(Pre_CollapsedDataParallel_1_9905butterfly_9806));
}

void butterfly_9806() {
	butterfly(&(Pre_CollapsedDataParallel_1_9905butterfly_9806), &(butterfly_9806Post_CollapsedDataParallel_2_9906));
}

void Post_CollapsedDataParallel_2_9906() {
	Post_CollapsedDataParallel_2(&(butterfly_9806Post_CollapsedDataParallel_2_9906), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child0_10054_10120_join[3]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_10064() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child0_10054_10120_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_Hier_10088_10119_split[0]));
		push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child0_10054_10120_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_Hier_10088_10119_split[0]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10065() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_Hier_10088_10119_join[0], pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child0_10054_10120_join[__iter_]));
		push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_Hier_10088_10119_join[0], pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child0_10054_10120_join[__iter_]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_9908() {
	Pre_CollapsedDataParallel_1(&(SplitJoin55_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child1_10057_10121_split[0]), &(Pre_CollapsedDataParallel_1_9908butterfly_9807));
}

void butterfly_9807() {
	butterfly(&(Pre_CollapsedDataParallel_1_9908butterfly_9807), &(butterfly_9807Post_CollapsedDataParallel_2_9909));
}

void Post_CollapsedDataParallel_2_9909() {
	Post_CollapsedDataParallel_2(&(butterfly_9807Post_CollapsedDataParallel_2_9909), &(SplitJoin55_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child1_10057_10121_join[0]));
}

void Pre_CollapsedDataParallel_1_9911() {
	Pre_CollapsedDataParallel_1(&(SplitJoin55_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child1_10057_10121_split[1]), &(Pre_CollapsedDataParallel_1_9911butterfly_9808));
}

void butterfly_9808() {
	butterfly(&(Pre_CollapsedDataParallel_1_9911butterfly_9808), &(butterfly_9808Post_CollapsedDataParallel_2_9912));
}

void Post_CollapsedDataParallel_2_9912() {
	Post_CollapsedDataParallel_2(&(butterfly_9808Post_CollapsedDataParallel_2_9912), &(SplitJoin55_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child1_10057_10121_join[1]));
}

void Pre_CollapsedDataParallel_1_9914() {
	Pre_CollapsedDataParallel_1(&(SplitJoin55_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child1_10057_10121_split[2]), &(Pre_CollapsedDataParallel_1_9914butterfly_9809));
}

void butterfly_9809() {
	butterfly(&(Pre_CollapsedDataParallel_1_9914butterfly_9809), &(butterfly_9809Post_CollapsedDataParallel_2_9915));
}

void Post_CollapsedDataParallel_2_9915() {
	Post_CollapsedDataParallel_2(&(butterfly_9809Post_CollapsedDataParallel_2_9915), &(SplitJoin55_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child1_10057_10121_join[2]));
}

void Pre_CollapsedDataParallel_1_9917() {
	Pre_CollapsedDataParallel_1(&(SplitJoin55_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child1_10057_10121_split[3]), &(Pre_CollapsedDataParallel_1_9917butterfly_9810));
}

void butterfly_9810() {
	butterfly(&(Pre_CollapsedDataParallel_1_9917butterfly_9810), &(butterfly_9810Post_CollapsedDataParallel_2_9918));
}

void Post_CollapsedDataParallel_2_9918() {
	Post_CollapsedDataParallel_2(&(butterfly_9810Post_CollapsedDataParallel_2_9918), &(SplitJoin55_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child1_10057_10121_join[3]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_10066() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin55_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child1_10057_10121_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_Hier_10088_10119_split[1]));
		push_complex(&SplitJoin55_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child1_10057_10121_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_Hier_10088_10119_split[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10067() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_Hier_10088_10119_join[1], pop_complex(&SplitJoin55_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child1_10057_10121_join[__iter_]));
		push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_Hier_10088_10119_join[1], pop_complex(&SplitJoin55_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child1_10057_10121_join[__iter_]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10063() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_Hier_10088_10119_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9920WEIGHTED_ROUND_ROBIN_Splitter_10063));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_Hier_10088_10119_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9920WEIGHTED_ROUND_ROBIN_Splitter_10063));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10068() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_10068WEIGHTED_ROUND_ROBIN_Splitter_10069, pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_Hier_10088_10119_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_10068WEIGHTED_ROUND_ROBIN_Splitter_10069, pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_Hier_10088_10119_join[1]));
	ENDFOR
}

void butterfly_9812() {
	butterfly(&(SplitJoin18_SplitJoin12_SplitJoin12_split2_9722_9975_10053_10124_split[0]), &(SplitJoin18_SplitJoin12_SplitJoin12_split2_9722_9975_10053_10124_join[0]));
}

void butterfly_9813() {
	butterfly(&(SplitJoin18_SplitJoin12_SplitJoin12_split2_9722_9975_10053_10124_split[1]), &(SplitJoin18_SplitJoin12_SplitJoin12_split2_9722_9975_10053_10124_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_9953() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_9722_9975_10053_10124_split[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child0_10060_10123_split[0]));
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_9722_9975_10053_10124_split[1], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child0_10060_10123_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9954() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child0_10060_10123_join[0], pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_9722_9975_10053_10124_join[0]));
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child0_10060_10123_join[0], pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_9722_9975_10053_10124_join[1]));
	ENDFOR
}}

void butterfly_9814() {
	butterfly(&(SplitJoin38_SplitJoin29_SplitJoin29_split2_9724_9989_10055_10125_split[0]), &(SplitJoin38_SplitJoin29_SplitJoin29_split2_9724_9989_10055_10125_join[0]));
}

void butterfly_9815() {
	butterfly(&(SplitJoin38_SplitJoin29_SplitJoin29_split2_9724_9989_10055_10125_split[1]), &(SplitJoin38_SplitJoin29_SplitJoin29_split2_9724_9989_10055_10125_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_9955() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin38_SplitJoin29_SplitJoin29_split2_9724_9989_10055_10125_split[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child0_10060_10123_split[1]));
		push_complex(&SplitJoin38_SplitJoin29_SplitJoin29_split2_9724_9989_10055_10125_split[1], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child0_10060_10123_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9956() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child0_10060_10123_join[1], pop_complex(&SplitJoin38_SplitJoin29_SplitJoin29_split2_9724_9989_10055_10125_join[0]));
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child0_10060_10123_join[1], pop_complex(&SplitJoin38_SplitJoin29_SplitJoin29_split2_9724_9989_10055_10125_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_10070() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child0_10060_10123_split[0], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_Hier_10089_10122_split[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child0_10060_10123_split[1], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_Hier_10089_10122_split[0]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10071() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_Hier_10089_10122_join[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child0_10060_10123_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_Hier_10089_10122_join[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child0_10060_10123_join[1]));
	ENDFOR
}

void butterfly_9816() {
	butterfly(&(SplitJoin44_SplitJoin33_SplitJoin33_split2_9726_9992_10056_10127_split[0]), &(SplitJoin44_SplitJoin33_SplitJoin33_split2_9726_9992_10056_10127_join[0]));
}

void butterfly_9817() {
	butterfly(&(SplitJoin44_SplitJoin33_SplitJoin33_split2_9726_9992_10056_10127_split[1]), &(SplitJoin44_SplitJoin33_SplitJoin33_split2_9726_9992_10056_10127_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_9957() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin44_SplitJoin33_SplitJoin33_split2_9726_9992_10056_10127_split[0], pop_complex(&SplitJoin42_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child1_10062_10126_split[0]));
		push_complex(&SplitJoin44_SplitJoin33_SplitJoin33_split2_9726_9992_10056_10127_split[1], pop_complex(&SplitJoin42_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child1_10062_10126_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9958() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin42_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child1_10062_10126_join[0], pop_complex(&SplitJoin44_SplitJoin33_SplitJoin33_split2_9726_9992_10056_10127_join[0]));
		push_complex(&SplitJoin42_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child1_10062_10126_join[0], pop_complex(&SplitJoin44_SplitJoin33_SplitJoin33_split2_9726_9992_10056_10127_join[1]));
	ENDFOR
}}

void butterfly_9818() {
	butterfly(&(SplitJoin48_SplitJoin37_SplitJoin37_split2_9728_9995_10058_10128_split[0]), &(SplitJoin48_SplitJoin37_SplitJoin37_split2_9728_9995_10058_10128_join[0]));
}

void butterfly_9819() {
	butterfly(&(SplitJoin48_SplitJoin37_SplitJoin37_split2_9728_9995_10058_10128_split[1]), &(SplitJoin48_SplitJoin37_SplitJoin37_split2_9728_9995_10058_10128_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_9959() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin48_SplitJoin37_SplitJoin37_split2_9728_9995_10058_10128_split[0], pop_complex(&SplitJoin42_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child1_10062_10126_split[1]));
		push_complex(&SplitJoin48_SplitJoin37_SplitJoin37_split2_9728_9995_10058_10128_split[1], pop_complex(&SplitJoin42_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child1_10062_10126_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9960() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin42_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child1_10062_10126_join[1], pop_complex(&SplitJoin48_SplitJoin37_SplitJoin37_split2_9728_9995_10058_10128_join[0]));
		push_complex(&SplitJoin42_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child1_10062_10126_join[1], pop_complex(&SplitJoin48_SplitJoin37_SplitJoin37_split2_9728_9995_10058_10128_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_10072() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin42_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child1_10062_10126_split[0], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_Hier_10089_10122_split[1]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin42_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child1_10062_10126_split[1], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_Hier_10089_10122_split[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10073() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_Hier_10089_10122_join[1], pop_complex(&SplitJoin42_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child1_10062_10126_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_Hier_10089_10122_join[1], pop_complex(&SplitJoin42_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child1_10062_10126_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10069() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_Hier_10089_10122_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_10068WEIGHTED_ROUND_ROBIN_Splitter_10069));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_Hier_10089_10122_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_10068WEIGHTED_ROUND_ROBIN_Splitter_10069));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10074() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_10074WEIGHTED_ROUND_ROBIN_Splitter_9961, pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_Hier_10089_10122_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_10074WEIGHTED_ROUND_ROBIN_Splitter_9961, pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_Hier_10089_10122_join[1]));
	ENDFOR
}

void butterfly_9821() {
	butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_9735_9978_10059_10130_split[0]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_9735_9978_10059_10130_join[0]));
}

void butterfly_9822() {
	butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_9735_9978_10059_10130_split[1]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_9735_9978_10059_10130_join[1]));
}

void butterfly_9823() {
	butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_9735_9978_10059_10130_split[2]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_9735_9978_10059_10130_join[2]));
}

void butterfly_9824() {
	butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_9735_9978_10059_10130_split[3]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_9735_9978_10059_10130_join[3]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_9963() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_9735_9978_10059_10130_split[__iter_], pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_9733_9977_10090_10129_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9964() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_9733_9977_10090_10129_join[0], pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_9735_9978_10059_10130_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void butterfly_9825() {
	butterfly(&(SplitJoin31_SplitJoin22_SplitJoin22_split2_9737_9983_10061_10131_split[0]), &(SplitJoin31_SplitJoin22_SplitJoin22_split2_9737_9983_10061_10131_join[0]));
}

void butterfly_9826() {
	butterfly(&(SplitJoin31_SplitJoin22_SplitJoin22_split2_9737_9983_10061_10131_split[1]), &(SplitJoin31_SplitJoin22_SplitJoin22_split2_9737_9983_10061_10131_join[1]));
}

void butterfly_9827() {
	butterfly(&(SplitJoin31_SplitJoin22_SplitJoin22_split2_9737_9983_10061_10131_split[2]), &(SplitJoin31_SplitJoin22_SplitJoin22_split2_9737_9983_10061_10131_join[2]));
}

void butterfly_9828() {
	butterfly(&(SplitJoin31_SplitJoin22_SplitJoin22_split2_9737_9983_10061_10131_split[3]), &(SplitJoin31_SplitJoin22_SplitJoin22_split2_9737_9983_10061_10131_join[3]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_9965() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin31_SplitJoin22_SplitJoin22_split2_9737_9983_10061_10131_split[__iter_], pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_9733_9977_10090_10129_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9966() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_9733_9977_10090_10129_join[1], pop_complex(&SplitJoin31_SplitJoin22_SplitJoin22_split2_9737_9983_10061_10131_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_9961() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_9733_9977_10090_10129_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_10074WEIGHTED_ROUND_ROBIN_Splitter_9961));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_9733_9977_10090_10129_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_10074WEIGHTED_ROUND_ROBIN_Splitter_9961));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_9962() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9962WEIGHTED_ROUND_ROBIN_Splitter_10080, pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_9733_9977_10090_10129_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9962WEIGHTED_ROUND_ROBIN_Splitter_10080, pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_9733_9977_10090_10129_join[1]));
	ENDFOR
}

void magnitude(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		push_float(&(*chanout), ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}


void magnitude_10082() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_10091_10132_split[0]), &(SplitJoin24_magnitude_Fiss_10091_10132_join[0]));
	ENDFOR
}

void magnitude_10083() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_10091_10132_split[1]), &(SplitJoin24_magnitude_Fiss_10091_10132_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10080() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		push_complex(&SplitJoin24_magnitude_Fiss_10091_10132_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9962WEIGHTED_ROUND_ROBIN_Splitter_10080));
		push_complex(&SplitJoin24_magnitude_Fiss_10091_10132_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9962WEIGHTED_ROUND_ROBIN_Splitter_10080));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10081() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10081sink_9830, pop_float(&SplitJoin24_magnitude_Fiss_10091_10132_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10081sink_9830, pop_float(&SplitJoin24_magnitude_Fiss_10091_10132_join[1]));
	ENDFOR
}}

void sink(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void sink_9830() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		sink(&(WEIGHTED_ROUND_ROBIN_Joiner_10081sink_9830));
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&butterfly_9804Post_CollapsedDataParallel_2_9900);
	init_buffer_complex(&butterfly_9805Post_CollapsedDataParallel_2_9903);
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_float(&SplitJoin24_magnitude_Fiss_10091_10132_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 2, __iter_init_1_++)
		init_buffer_complex(&SplitJoin72_SplitJoin59_SplitJoin59_AnonFilter_a0_9767_10013_10095_10111_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_complex(&SplitJoin84_SplitJoin71_SplitJoin71_AnonFilter_a0_9783_10021_10098_10115_split[__iter_init_2_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9920WEIGHTED_ROUND_ROBIN_Splitter_10063);
	init_buffer_complex(&butterfly_9806Post_CollapsedDataParallel_2_9906);
	FOR(int, __iter_init_3_, 0, <, 2, __iter_init_3_++)
		init_buffer_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9745_9969_10086_10106_join[__iter_init_3_]);
	ENDFOR
	init_buffer_complex(&butterfly_9807Post_CollapsedDataParallel_2_9909);
	FOR(int, __iter_init_4_, 0, <, 4, __iter_init_4_++)
		init_buffer_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_9735_9978_10059_10130_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 2, __iter_init_5_++)
		init_buffer_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_Hier_10088_10119_join[__iter_init_5_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_9914butterfly_9809);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_10077WEIGHTED_ROUND_ROBIN_Splitter_9919);
	FOR(int, __iter_init_6_, 0, <, 2, __iter_init_6_++)
		init_buffer_complex(&SplitJoin76_SplitJoin63_SplitJoin63_AnonFilter_a0_9773_10016_10052_10112_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 2, __iter_init_7_++)
		init_buffer_complex(&SplitJoin90_SplitJoin77_SplitJoin77_AnonFilter_a0_9791_10025_10100_10117_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_complex(&SplitJoin68_SplitJoin55_SplitJoin55_AnonFilter_a0_9761_10010_10094_10110_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 4, __iter_init_9_++)
		init_buffer_complex(&SplitJoin31_SplitJoin22_SplitJoin22_split2_9737_9983_10061_10131_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9743_9968_10051_10105_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 2, __iter_init_11_++)
		init_buffer_complex(&SplitJoin48_SplitJoin37_SplitJoin37_split2_9728_9995_10058_10128_split[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 2, __iter_init_12_++)
		init_buffer_complex(&SplitJoin78_SplitJoin65_SplitJoin65_AnonFilter_a0_9775_10017_10096_10113_split[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_complex(&SplitJoin80_SplitJoin67_SplitJoin67_AnonFilter_a0_9777_10018_10097_10114_join[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_complex(&SplitJoin90_SplitJoin77_SplitJoin77_AnonFilter_a0_9791_10025_10100_10117_join[__iter_init_14_]);
	ENDFOR
	init_buffer_complex(&butterfly_9803Post_CollapsedDataParallel_2_9897);
	init_buffer_complex(&butterfly_9808Post_CollapsedDataParallel_2_9912);
	FOR(int, __iter_init_15_, 0, <, 2, __iter_init_15_++)
		init_buffer_complex(&SplitJoin42_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child1_10062_10126_split[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_complex(&SplitJoin66_SplitJoin53_SplitJoin53_AnonFilter_a0_9759_10009_10093_10109_split[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9743_9968_10051_10105_split[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 2, __iter_init_18_++)
		init_buffer_complex(&SplitJoin84_SplitJoin71_SplitJoin71_AnonFilter_a0_9783_10021_10098_10115_join[__iter_init_18_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9962WEIGHTED_ROUND_ROBIN_Splitter_10080);
	init_buffer_complex(&Pre_CollapsedDataParallel_1_9917butterfly_9810);
	FOR(int, __iter_init_19_, 0, <, 2, __iter_init_19_++)
		init_buffer_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_Hier_10089_10122_split[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 2, __iter_init_20_++)
		init_buffer_complex(&SplitJoin44_SplitJoin33_SplitJoin33_split2_9726_9992_10056_10127_join[__iter_init_20_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_9899butterfly_9804);
	FOR(int, __iter_init_21_, 0, <, 2, __iter_init_21_++)
		init_buffer_complex(&SplitJoin24_magnitude_Fiss_10091_10132_split[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 2, __iter_init_22_++)
		init_buffer_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_9722_9975_10053_10124_split[__iter_init_22_]);
	ENDFOR
	FOR(int, __iter_init_23_, 0, <, 2, __iter_init_23_++)
		init_buffer_complex(&SplitJoin38_SplitJoin29_SplitJoin29_split2_9724_9989_10055_10125_join[__iter_init_23_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_9902butterfly_9805);
	FOR(int, __iter_init_24_, 0, <, 2, __iter_init_24_++)
		init_buffer_complex(&SplitJoin72_SplitJoin59_SplitJoin59_AnonFilter_a0_9767_10013_10095_10111_split[__iter_init_24_]);
	ENDFOR
	FOR(int, __iter_init_25_, 0, <, 2, __iter_init_25_++)
		init_buffer_complex(&SplitJoin94_SplitJoin81_SplitJoin81_AnonFilter_a0_9797_10028_10101_10118_join[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 4, __iter_init_26_++)
		init_buffer_complex(&SplitJoin55_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child1_10057_10121_split[__iter_init_26_]);
	ENDFOR
	FOR(int, __iter_init_27_, 0, <, 2, __iter_init_27_++)
		init_buffer_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_9733_9977_10090_10129_join[__iter_init_27_]);
	ENDFOR
	FOR(int, __iter_init_28_, 0, <, 2, __iter_init_28_++)
		init_buffer_complex(&SplitJoin0_source_Fiss_10084_10103_join[__iter_init_28_]);
	ENDFOR
	FOR(int, __iter_init_29_, 0, <, 2, __iter_init_29_++)
		init_buffer_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9741_9967_10085_10104_join[__iter_init_29_]);
	ENDFOR
	FOR(int, __iter_init_30_, 0, <, 2, __iter_init_30_++)
		init_buffer_complex(&SplitJoin66_SplitJoin53_SplitJoin53_AnonFilter_a0_9759_10009_10093_10109_join[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 2, __iter_init_31_++)
		init_buffer_complex(&SplitJoin38_SplitJoin29_SplitJoin29_split2_9724_9989_10055_10125_split[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 2, __iter_init_32_++)
		init_buffer_complex(&SplitJoin88_SplitJoin75_SplitJoin75_AnonFilter_a0_9789_10024_10099_10116_join[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 4, __iter_init_33_++)
		init_buffer_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child0_10054_10120_split[__iter_init_33_]);
	ENDFOR
	FOR(int, __iter_init_34_, 0, <, 4, __iter_init_34_++)
		init_buffer_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_9735_9978_10059_10130_split[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 4, __iter_init_35_++)
		init_buffer_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child0_10054_10120_join[__iter_init_35_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_10074WEIGHTED_ROUND_ROBIN_Splitter_9961);
	FOR(int, __iter_init_36_, 0, <, 2, __iter_init_36_++)
		init_buffer_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child0_10060_10123_join[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 2, __iter_init_37_++)
		init_buffer_complex(&SplitJoin78_SplitJoin65_SplitJoin65_AnonFilter_a0_9775_10017_10096_10113_join[__iter_init_37_]);
	ENDFOR
	FOR(int, __iter_init_38_, 0, <, 2, __iter_init_38_++)
		init_buffer_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child0_10060_10123_split[__iter_init_38_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_9908butterfly_9807);
	FOR(int, __iter_init_39_, 0, <, 2, __iter_init_39_++)
		init_buffer_complex(&SplitJoin88_SplitJoin75_SplitJoin75_AnonFilter_a0_9789_10024_10099_10116_split[__iter_init_39_]);
	ENDFOR
	FOR(int, __iter_init_40_, 0, <, 2, __iter_init_40_++)
		init_buffer_complex(&SplitJoin94_SplitJoin81_SplitJoin81_AnonFilter_a0_9797_10028_10101_10118_split[__iter_init_40_]);
	ENDFOR
	FOR(int, __iter_init_41_, 0, <, 2, __iter_init_41_++)
		init_buffer_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_9722_9975_10053_10124_join[__iter_init_41_]);
	ENDFOR
	FOR(int, __iter_init_42_, 0, <, 2, __iter_init_42_++)
		init_buffer_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_9747_9970_10087_10107_split[__iter_init_42_]);
	ENDFOR
	FOR(int, __iter_init_43_, 0, <, 4, __iter_init_43_++)
		init_buffer_complex(&SplitJoin31_SplitJoin22_SplitJoin22_split2_9737_9983_10061_10131_join[__iter_init_43_]);
	ENDFOR
	FOR(int, __iter_init_44_, 0, <, 2, __iter_init_44_++)
		init_buffer_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_Hier_10088_10119_split[__iter_init_44_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_10068WEIGHTED_ROUND_ROBIN_Splitter_10069);
	FOR(int, __iter_init_45_, 0, <, 4, __iter_init_45_++)
		init_buffer_complex(&SplitJoin55_SplitJoin8_SplitJoin8_split1_9699_9972_Hier_child1_10057_10121_join[__iter_init_45_]);
	ENDFOR
	FOR(int, __iter_init_46_, 0, <, 2, __iter_init_46_++)
		init_buffer_complex(&SplitJoin62_SplitJoin49_SplitJoin49_AnonFilter_a0_9753_10006_10092_10108_join[__iter_init_46_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_9905butterfly_9806);
	FOR(int, __iter_init_47_, 0, <, 2, __iter_init_47_++)
		init_buffer_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_9733_9977_10090_10129_split[__iter_init_47_]);
	ENDFOR
	FOR(int, __iter_init_48_, 0, <, 2, __iter_init_48_++)
		init_buffer_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9741_9967_10085_10104_split[__iter_init_48_]);
	ENDFOR
	FOR(int, __iter_init_49_, 0, <, 2, __iter_init_49_++)
		init_buffer_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_9747_9970_10087_10107_join[__iter_init_49_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_9896butterfly_9803);
	FOR(int, __iter_init_50_, 0, <, 2, __iter_init_50_++)
		init_buffer_complex(&SplitJoin48_SplitJoin37_SplitJoin37_split2_9728_9995_10058_10128_join[__iter_init_50_]);
	ENDFOR
	FOR(int, __iter_init_51_, 0, <, 2, __iter_init_51_++)
		init_buffer_complex(&SplitJoin42_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_child1_10062_10126_join[__iter_init_51_]);
	ENDFOR
	init_buffer_complex(&butterfly_9809Post_CollapsedDataParallel_2_9915);
	FOR(int, __iter_init_52_, 0, <, 2, __iter_init_52_++)
		init_buffer_complex(&SplitJoin68_SplitJoin55_SplitJoin55_AnonFilter_a0_9761_10010_10094_10110_join[__iter_init_52_]);
	ENDFOR
	FOR(int, __iter_init_53_, 0, <, 2, __iter_init_53_++)
		init_buffer_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_9720_9974_Hier_Hier_10089_10122_join[__iter_init_53_]);
	ENDFOR
	FOR(int, __iter_init_54_, 0, <, 2, __iter_init_54_++)
		init_buffer_complex(&SplitJoin0_source_Fiss_10084_10103_split[__iter_init_54_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_9911butterfly_9808);
	init_buffer_complex(&butterfly_9810Post_CollapsedDataParallel_2_9918);
	FOR(int, __iter_init_55_, 0, <, 2, __iter_init_55_++)
		init_buffer_complex(&SplitJoin62_SplitJoin49_SplitJoin49_AnonFilter_a0_9753_10006_10092_10108_split[__iter_init_55_]);
	ENDFOR
	FOR(int, __iter_init_56_, 0, <, 2, __iter_init_56_++)
		init_buffer_complex(&SplitJoin80_SplitJoin67_SplitJoin67_AnonFilter_a0_9777_10018_10097_10114_split[__iter_init_56_]);
	ENDFOR
	FOR(int, __iter_init_57_, 0, <, 2, __iter_init_57_++)
		init_buffer_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9745_9969_10086_10106_split[__iter_init_57_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10081sink_9830);
	FOR(int, __iter_init_58_, 0, <, 2, __iter_init_58_++)
		init_buffer_complex(&SplitJoin44_SplitJoin33_SplitJoin33_split2_9726_9992_10056_10127_split[__iter_init_58_]);
	ENDFOR
	FOR(int, __iter_init_59_, 0, <, 2, __iter_init_59_++)
		init_buffer_complex(&SplitJoin76_SplitJoin63_SplitJoin63_AnonFilter_a0_9773_10016_10052_10112_split[__iter_init_59_]);
	ENDFOR
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_10076();
			source_10078();
			source_10079();
		WEIGHTED_ROUND_ROBIN_Joiner_10077();
		WEIGHTED_ROUND_ROBIN_Splitter_9919();
			WEIGHTED_ROUND_ROBIN_Splitter_9921();
				WEIGHTED_ROUND_ROBIN_Splitter_9923();
					WEIGHTED_ROUND_ROBIN_Splitter_9925();
						Identity_9749();
						Identity_9751();
					WEIGHTED_ROUND_ROBIN_Joiner_9926();
					WEIGHTED_ROUND_ROBIN_Splitter_9927();
						Identity_9755();
						Identity_9757();
					WEIGHTED_ROUND_ROBIN_Joiner_9928();
				WEIGHTED_ROUND_ROBIN_Joiner_9924();
				WEIGHTED_ROUND_ROBIN_Splitter_9929();
					WEIGHTED_ROUND_ROBIN_Splitter_9931();
						Identity_9763();
						Identity_9765();
					WEIGHTED_ROUND_ROBIN_Joiner_9932();
					WEIGHTED_ROUND_ROBIN_Splitter_9933();
						Identity_9769();
						Identity_9771();
					WEIGHTED_ROUND_ROBIN_Joiner_9934();
				WEIGHTED_ROUND_ROBIN_Joiner_9930();
			WEIGHTED_ROUND_ROBIN_Joiner_9922();
			WEIGHTED_ROUND_ROBIN_Splitter_9935();
				WEIGHTED_ROUND_ROBIN_Splitter_9937();
					WEIGHTED_ROUND_ROBIN_Splitter_9939();
						Identity_9779();
						Identity_9781();
					WEIGHTED_ROUND_ROBIN_Joiner_9940();
					WEIGHTED_ROUND_ROBIN_Splitter_9941();
						Identity_9785();
						Identity_9787();
					WEIGHTED_ROUND_ROBIN_Joiner_9942();
				WEIGHTED_ROUND_ROBIN_Joiner_9938();
				WEIGHTED_ROUND_ROBIN_Splitter_9943();
					WEIGHTED_ROUND_ROBIN_Splitter_9945();
						Identity_9793();
						Identity_9795();
					WEIGHTED_ROUND_ROBIN_Joiner_9946();
					WEIGHTED_ROUND_ROBIN_Splitter_9947();
						Identity_9799();
						Identity_9801();
					WEIGHTED_ROUND_ROBIN_Joiner_9948();
				WEIGHTED_ROUND_ROBIN_Joiner_9944();
			WEIGHTED_ROUND_ROBIN_Joiner_9936();
		WEIGHTED_ROUND_ROBIN_Joiner_9920();
		WEIGHTED_ROUND_ROBIN_Splitter_10063();
			WEIGHTED_ROUND_ROBIN_Splitter_10064();
				Pre_CollapsedDataParallel_1_9896();
				butterfly_9803();
				Post_CollapsedDataParallel_2_9897();
				Pre_CollapsedDataParallel_1_9899();
				butterfly_9804();
				Post_CollapsedDataParallel_2_9900();
				Pre_CollapsedDataParallel_1_9902();
				butterfly_9805();
				Post_CollapsedDataParallel_2_9903();
				Pre_CollapsedDataParallel_1_9905();
				butterfly_9806();
				Post_CollapsedDataParallel_2_9906();
			WEIGHTED_ROUND_ROBIN_Joiner_10065();
			WEIGHTED_ROUND_ROBIN_Splitter_10066();
				Pre_CollapsedDataParallel_1_9908();
				butterfly_9807();
				Post_CollapsedDataParallel_2_9909();
				Pre_CollapsedDataParallel_1_9911();
				butterfly_9808();
				Post_CollapsedDataParallel_2_9912();
				Pre_CollapsedDataParallel_1_9914();
				butterfly_9809();
				Post_CollapsedDataParallel_2_9915();
				Pre_CollapsedDataParallel_1_9917();
				butterfly_9810();
				Post_CollapsedDataParallel_2_9918();
			WEIGHTED_ROUND_ROBIN_Joiner_10067();
		WEIGHTED_ROUND_ROBIN_Joiner_10068();
		WEIGHTED_ROUND_ROBIN_Splitter_10069();
			WEIGHTED_ROUND_ROBIN_Splitter_10070();
				WEIGHTED_ROUND_ROBIN_Splitter_9953();
					butterfly_9812();
					butterfly_9813();
				WEIGHTED_ROUND_ROBIN_Joiner_9954();
				WEIGHTED_ROUND_ROBIN_Splitter_9955();
					butterfly_9814();
					butterfly_9815();
				WEIGHTED_ROUND_ROBIN_Joiner_9956();
			WEIGHTED_ROUND_ROBIN_Joiner_10071();
			WEIGHTED_ROUND_ROBIN_Splitter_10072();
				WEIGHTED_ROUND_ROBIN_Splitter_9957();
					butterfly_9816();
					butterfly_9817();
				WEIGHTED_ROUND_ROBIN_Joiner_9958();
				WEIGHTED_ROUND_ROBIN_Splitter_9959();
					butterfly_9818();
					butterfly_9819();
				WEIGHTED_ROUND_ROBIN_Joiner_9960();
			WEIGHTED_ROUND_ROBIN_Joiner_10073();
		WEIGHTED_ROUND_ROBIN_Joiner_10074();
		WEIGHTED_ROUND_ROBIN_Splitter_9961();
			WEIGHTED_ROUND_ROBIN_Splitter_9963();
				butterfly_9821();
				butterfly_9822();
				butterfly_9823();
				butterfly_9824();
			WEIGHTED_ROUND_ROBIN_Joiner_9964();
			WEIGHTED_ROUND_ROBIN_Splitter_9965();
				butterfly_9825();
				butterfly_9826();
				butterfly_9827();
				butterfly_9828();
			WEIGHTED_ROUND_ROBIN_Joiner_9966();
		WEIGHTED_ROUND_ROBIN_Joiner_9962();
		WEIGHTED_ROUND_ROBIN_Splitter_10080();
			magnitude_10082();
			magnitude_10083();
		WEIGHTED_ROUND_ROBIN_Joiner_10081();
		sink_9830();
	ENDFOR
	return EXIT_SUCCESS;
}
