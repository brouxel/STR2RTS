#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=128 on the compile command line
#else
#if BUF_SIZEMAX < 128
#error BUF_SIZEMAX too small, it must be at least 128
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	complex_t wn;
} CombineDFT_5791_t;
void FFTTestSource_5753();
void FFTReorderSimple_5754();
void WEIGHTED_ROUND_ROBIN_Splitter_5767();
void FFTReorderSimple_5769();
void FFTReorderSimple_5770();
void WEIGHTED_ROUND_ROBIN_Joiner_5768();
void WEIGHTED_ROUND_ROBIN_Splitter_5771();
void FFTReorderSimple_5773();
void FFTReorderSimple_5774();
void FFTReorderSimple_5775();
void FFTReorderSimple_5776();
void WEIGHTED_ROUND_ROBIN_Joiner_5772();
void WEIGHTED_ROUND_ROBIN_Splitter_5777();
void FFTReorderSimple_5779();
void FFTReorderSimple_5780();
void FFTReorderSimple_5781();
void FFTReorderSimple_5782();
void WEIGHTED_ROUND_ROBIN_Joiner_5778();
void WEIGHTED_ROUND_ROBIN_Splitter_5783();
void FFTReorderSimple_5785();
void FFTReorderSimple_5786();
void FFTReorderSimple_5787();
void FFTReorderSimple_5788();
void WEIGHTED_ROUND_ROBIN_Joiner_5784();
void WEIGHTED_ROUND_ROBIN_Splitter_5789();
void CombineDFT_5791();
void CombineDFT_5792();
void CombineDFT_5793();
void CombineDFT_5794();
void WEIGHTED_ROUND_ROBIN_Joiner_5790();
void WEIGHTED_ROUND_ROBIN_Splitter_5795();
void CombineDFT_5797();
void CombineDFT_5798();
void CombineDFT_5799();
void CombineDFT_5800();
void WEIGHTED_ROUND_ROBIN_Joiner_5796();
void WEIGHTED_ROUND_ROBIN_Splitter_5801();
void CombineDFT_5803();
void CombineDFT_5804();
void CombineDFT_5805();
void CombineDFT_5806();
void WEIGHTED_ROUND_ROBIN_Joiner_5802();
void WEIGHTED_ROUND_ROBIN_Splitter_5807();
void CombineDFT_5809();
void CombineDFT_5810();
void CombineDFT_5811();
void CombineDFT_5812();
void WEIGHTED_ROUND_ROBIN_Joiner_5808();
void WEIGHTED_ROUND_ROBIN_Splitter_5813();
void CombineDFT_5815();
void CombineDFT_5816();
void WEIGHTED_ROUND_ROBIN_Joiner_5814();
void CombineDFT_5764();
void CPrinter_5765();

#ifdef __cplusplus
}
#endif
#endif
