#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=128 on the compile command line
#else
#if BUF_SIZEMAX < 128
#error BUF_SIZEMAX too small, it must be at least 128
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float A_re[32];
	float A_im[32];
	int idx;
} FloatSource_10154_t;
void FloatSource_10154();
void Pre_CollapsedDataParallel_1_10455();
void WEIGHTED_ROUND_ROBIN_Splitter_10597();
void Butterfly_10599();
void Butterfly_10600();
void Butterfly_10601();
void Butterfly_10602();
void WEIGHTED_ROUND_ROBIN_Joiner_10598();
void Post_CollapsedDataParallel_2_10456();
void WEIGHTED_ROUND_ROBIN_Splitter_10499();
void Pre_CollapsedDataParallel_1_10458();
void WEIGHTED_ROUND_ROBIN_Splitter_10603();
void Butterfly_10605();
void Butterfly_10606();
void Butterfly_10607();
void Butterfly_10608();
void WEIGHTED_ROUND_ROBIN_Joiner_10604();
void Post_CollapsedDataParallel_2_10459();
void WEIGHTED_ROUND_ROBIN_Splitter_10579();
void Pre_CollapsedDataParallel_1_10464();
void WEIGHTED_ROUND_ROBIN_Splitter_10609();
void Butterfly_10611();
void Butterfly_10612();
void Butterfly_10613();
void Butterfly_10614();
void WEIGHTED_ROUND_ROBIN_Joiner_10610();
void Post_CollapsedDataParallel_2_10465();
void Pre_CollapsedDataParallel_1_10467();
void WEIGHTED_ROUND_ROBIN_Splitter_10615();
void Butterfly_10617();
void Butterfly_10618();
void Butterfly_10619();
void Butterfly_10620();
void WEIGHTED_ROUND_ROBIN_Joiner_10616();
void Post_CollapsedDataParallel_2_10468();
void WEIGHTED_ROUND_ROBIN_Joiner_10580();
void Pre_CollapsedDataParallel_1_10461();
void WEIGHTED_ROUND_ROBIN_Splitter_10621();
void Butterfly_10623();
void Butterfly_10624();
void Butterfly_10625();
void Butterfly_10626();
void WEIGHTED_ROUND_ROBIN_Joiner_10622();
void Post_CollapsedDataParallel_2_10462();
void WEIGHTED_ROUND_ROBIN_Splitter_10581();
void Pre_CollapsedDataParallel_1_10470();
void WEIGHTED_ROUND_ROBIN_Splitter_10627();
void Butterfly_10629();
void Butterfly_10630();
void Butterfly_10631();
void Butterfly_10632();
void WEIGHTED_ROUND_ROBIN_Joiner_10628();
void Post_CollapsedDataParallel_2_10471();
void Pre_CollapsedDataParallel_1_10473();
void WEIGHTED_ROUND_ROBIN_Splitter_10633();
void Butterfly_10635();
void Butterfly_10636();
void Butterfly_10637();
void Butterfly_10638();
void WEIGHTED_ROUND_ROBIN_Joiner_10634();
void Post_CollapsedDataParallel_2_10474();
void WEIGHTED_ROUND_ROBIN_Joiner_10582();
void WEIGHTED_ROUND_ROBIN_Joiner_10583();
void WEIGHTED_ROUND_ROBIN_Splitter_10584();
void WEIGHTED_ROUND_ROBIN_Splitter_10585();
void Pre_CollapsedDataParallel_1_10476();
void WEIGHTED_ROUND_ROBIN_Splitter_10639();
void Butterfly_10641();
void Butterfly_10642();
void WEIGHTED_ROUND_ROBIN_Joiner_10640();
void Post_CollapsedDataParallel_2_10477();
void Pre_CollapsedDataParallel_1_10479();
void WEIGHTED_ROUND_ROBIN_Splitter_10643();
void Butterfly_10645();
void Butterfly_10646();
void WEIGHTED_ROUND_ROBIN_Joiner_10644();
void Post_CollapsedDataParallel_2_10480();
void Pre_CollapsedDataParallel_1_10482();
void WEIGHTED_ROUND_ROBIN_Splitter_10647();
void Butterfly_10649();
void Butterfly_10650();
void WEIGHTED_ROUND_ROBIN_Joiner_10648();
void Post_CollapsedDataParallel_2_10483();
void Pre_CollapsedDataParallel_1_10485();
void WEIGHTED_ROUND_ROBIN_Splitter_10651();
void Butterfly_10653();
void Butterfly_10654();
void WEIGHTED_ROUND_ROBIN_Joiner_10652();
void Post_CollapsedDataParallel_2_10486();
void WEIGHTED_ROUND_ROBIN_Joiner_10586();
void WEIGHTED_ROUND_ROBIN_Splitter_10587();
void Pre_CollapsedDataParallel_1_10488();
void WEIGHTED_ROUND_ROBIN_Splitter_10655();
void Butterfly_10657();
void Butterfly_10658();
void WEIGHTED_ROUND_ROBIN_Joiner_10656();
void Post_CollapsedDataParallel_2_10489();
void Pre_CollapsedDataParallel_1_10491();
void WEIGHTED_ROUND_ROBIN_Splitter_10659();
void Butterfly_10661();
void Butterfly_10662();
void WEIGHTED_ROUND_ROBIN_Joiner_10660();
void Post_CollapsedDataParallel_2_10492();
void Pre_CollapsedDataParallel_1_10494();
void WEIGHTED_ROUND_ROBIN_Splitter_10663();
void Butterfly_10665();
void Butterfly_10666();
void WEIGHTED_ROUND_ROBIN_Joiner_10664();
void Post_CollapsedDataParallel_2_10495();
void Pre_CollapsedDataParallel_1_10497();
void WEIGHTED_ROUND_ROBIN_Splitter_10667();
void Butterfly_10669();
void Butterfly_10670();
void WEIGHTED_ROUND_ROBIN_Joiner_10668();
void Post_CollapsedDataParallel_2_10498();
void WEIGHTED_ROUND_ROBIN_Joiner_10588();
void WEIGHTED_ROUND_ROBIN_Joiner_10589();
void WEIGHTED_ROUND_ROBIN_Splitter_10590();
void WEIGHTED_ROUND_ROBIN_Splitter_10591();
void Butterfly_10219();
void Butterfly_10220();
void Butterfly_10221();
void Butterfly_10222();
void Butterfly_10223();
void Butterfly_10224();
void Butterfly_10225();
void Butterfly_10226();
void WEIGHTED_ROUND_ROBIN_Joiner_10592();
void WEIGHTED_ROUND_ROBIN_Splitter_10593();
void Butterfly_10227();
void Butterfly_10228();
void Butterfly_10229();
void Butterfly_10230();
void Butterfly_10231();
void Butterfly_10232();
void Butterfly_10233();
void Butterfly_10234();
void WEIGHTED_ROUND_ROBIN_Joiner_10594();
void WEIGHTED_ROUND_ROBIN_Joiner_10595();
int BitReverse_10235_bitrev(int inp, int numbits);
void BitReverse_10235();
void FloatPrinter_10236();

#ifdef __cplusplus
}
#endif
#endif
