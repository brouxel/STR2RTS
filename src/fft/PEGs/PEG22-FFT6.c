#include "PEG22-FFT6.h"

buffer_complex_t FFTTestSource_2387FFTReorderSimple_2388;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2482WEIGHTED_ROUND_ROBIN_Splitter_2491;
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_2502_2512_join[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2422WEIGHTED_ROUND_ROBIN_Splitter_2439;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2412WEIGHTED_ROUND_ROBIN_Splitter_2421;
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_2504_2514_split[16];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_2509_2519_join[2];
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_2504_2514_join[16];
buffer_complex_t SplitJoin12_CombineDFT_Fiss_2507_2517_join[8];
buffer_complex_t SplitJoin12_CombineDFT_Fiss_2507_2517_split[8];
buffer_complex_t SplitJoin8_CombineDFT_Fiss_2505_2515_join[22];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2492WEIGHTED_ROUND_ROBIN_Splitter_2497;
buffer_complex_t SplitJoin10_CombineDFT_Fiss_2506_2516_split[16];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_2506_2516_join[16];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2498CombineDFT_2398;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2406WEIGHTED_ROUND_ROBIN_Splitter_2411;
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_2501_2511_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2402WEIGHTED_ROUND_ROBIN_Splitter_2405;
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_2503_2513_join[8];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_2508_2518_split[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2464WEIGHTED_ROUND_ROBIN_Splitter_2481;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_2505_2515_split[22];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2440WEIGHTED_ROUND_ROBIN_Splitter_2463;
buffer_complex_t SplitJoin16_CombineDFT_Fiss_2509_2519_split[2];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_2503_2513_split[8];
buffer_complex_t FFTReorderSimple_2388WEIGHTED_ROUND_ROBIN_Splitter_2401;
buffer_complex_t CombineDFT_2398CPrinter_2399;
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_2502_2512_split[4];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_2508_2518_join[4];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_2501_2511_split[2];


CombineDFT_2441_t CombineDFT_2441_s;
CombineDFT_2441_t CombineDFT_2442_s;
CombineDFT_2441_t CombineDFT_2443_s;
CombineDFT_2441_t CombineDFT_2444_s;
CombineDFT_2441_t CombineDFT_2445_s;
CombineDFT_2441_t CombineDFT_2446_s;
CombineDFT_2441_t CombineDFT_2447_s;
CombineDFT_2441_t CombineDFT_2448_s;
CombineDFT_2441_t CombineDFT_2449_s;
CombineDFT_2441_t CombineDFT_2450_s;
CombineDFT_2441_t CombineDFT_2451_s;
CombineDFT_2441_t CombineDFT_2452_s;
CombineDFT_2441_t CombineDFT_2453_s;
CombineDFT_2441_t CombineDFT_2454_s;
CombineDFT_2441_t CombineDFT_2455_s;
CombineDFT_2441_t CombineDFT_2456_s;
CombineDFT_2441_t CombineDFT_2457_s;
CombineDFT_2441_t CombineDFT_2458_s;
CombineDFT_2441_t CombineDFT_2459_s;
CombineDFT_2441_t CombineDFT_2460_s;
CombineDFT_2441_t CombineDFT_2461_s;
CombineDFT_2441_t CombineDFT_2462_s;
CombineDFT_2441_t CombineDFT_2465_s;
CombineDFT_2441_t CombineDFT_2466_s;
CombineDFT_2441_t CombineDFT_2467_s;
CombineDFT_2441_t CombineDFT_2468_s;
CombineDFT_2441_t CombineDFT_2469_s;
CombineDFT_2441_t CombineDFT_2470_s;
CombineDFT_2441_t CombineDFT_2471_s;
CombineDFT_2441_t CombineDFT_2472_s;
CombineDFT_2441_t CombineDFT_2473_s;
CombineDFT_2441_t CombineDFT_2474_s;
CombineDFT_2441_t CombineDFT_2475_s;
CombineDFT_2441_t CombineDFT_2476_s;
CombineDFT_2441_t CombineDFT_2477_s;
CombineDFT_2441_t CombineDFT_2478_s;
CombineDFT_2441_t CombineDFT_2479_s;
CombineDFT_2441_t CombineDFT_2480_s;
CombineDFT_2441_t CombineDFT_2483_s;
CombineDFT_2441_t CombineDFT_2484_s;
CombineDFT_2441_t CombineDFT_2485_s;
CombineDFT_2441_t CombineDFT_2486_s;
CombineDFT_2441_t CombineDFT_2487_s;
CombineDFT_2441_t CombineDFT_2488_s;
CombineDFT_2441_t CombineDFT_2489_s;
CombineDFT_2441_t CombineDFT_2490_s;
CombineDFT_2441_t CombineDFT_2493_s;
CombineDFT_2441_t CombineDFT_2494_s;
CombineDFT_2441_t CombineDFT_2495_s;
CombineDFT_2441_t CombineDFT_2496_s;
CombineDFT_2441_t CombineDFT_2499_s;
CombineDFT_2441_t CombineDFT_2500_s;
CombineDFT_2441_t CombineDFT_2398_s;

void FFTTestSource(buffer_complex_t *chanout) {
		complex_t c1;
		complex_t zero;
		c1.real = 1.0 ; 
		c1.imag = 0.0 ; 
		zero.real = 0.0 ; 
		zero.imag = 0.0 ; 
		push_complex(&(*chanout), zero) ; 
		push_complex(&(*chanout), c1) ; 
		FOR(int, i, 0,  < , 62, i++) {
			push_complex(&(*chanout), zero) ; 
		}
		ENDFOR
	}


void FFTTestSource_2387() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTTestSource(&(FFTTestSource_2387FFTReorderSimple_2388));
	ENDFOR
}

void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 64, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&(*chanin)) ; 
		}
		ENDFOR
	}


void FFTReorderSimple_2388() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(FFTTestSource_2387FFTReorderSimple_2388), &(FFTReorderSimple_2388WEIGHTED_ROUND_ROBIN_Splitter_2401));
	ENDFOR
}

void FFTReorderSimple_2403() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_2501_2511_split[0]), &(SplitJoin0_FFTReorderSimple_Fiss_2501_2511_join[0]));
	ENDFOR
}

void FFTReorderSimple_2404() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_2501_2511_split[1]), &(SplitJoin0_FFTReorderSimple_Fiss_2501_2511_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2401() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_2501_2511_split[0], pop_complex(&FFTReorderSimple_2388WEIGHTED_ROUND_ROBIN_Splitter_2401));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_2501_2511_split[1], pop_complex(&FFTReorderSimple_2388WEIGHTED_ROUND_ROBIN_Splitter_2401));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2402() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2402WEIGHTED_ROUND_ROBIN_Splitter_2405, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_2501_2511_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2402WEIGHTED_ROUND_ROBIN_Splitter_2405, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_2501_2511_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_2407() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_2502_2512_split[0]), &(SplitJoin2_FFTReorderSimple_Fiss_2502_2512_join[0]));
	ENDFOR
}

void FFTReorderSimple_2408() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_2502_2512_split[1]), &(SplitJoin2_FFTReorderSimple_Fiss_2502_2512_join[1]));
	ENDFOR
}

void FFTReorderSimple_2409() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_2502_2512_split[2]), &(SplitJoin2_FFTReorderSimple_Fiss_2502_2512_join[2]));
	ENDFOR
}

void FFTReorderSimple_2410() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_2502_2512_split[3]), &(SplitJoin2_FFTReorderSimple_Fiss_2502_2512_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2405() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin2_FFTReorderSimple_Fiss_2502_2512_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2402WEIGHTED_ROUND_ROBIN_Splitter_2405));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2406() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2406WEIGHTED_ROUND_ROBIN_Splitter_2411, pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_2502_2512_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_2413() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_2503_2513_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_2503_2513_join[0]));
	ENDFOR
}

void FFTReorderSimple_2414() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_2503_2513_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_2503_2513_join[1]));
	ENDFOR
}

void FFTReorderSimple_2415() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_2503_2513_split[2]), &(SplitJoin4_FFTReorderSimple_Fiss_2503_2513_join[2]));
	ENDFOR
}

void FFTReorderSimple_2416() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_2503_2513_split[3]), &(SplitJoin4_FFTReorderSimple_Fiss_2503_2513_join[3]));
	ENDFOR
}

void FFTReorderSimple_2417() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_2503_2513_split[4]), &(SplitJoin4_FFTReorderSimple_Fiss_2503_2513_join[4]));
	ENDFOR
}

void FFTReorderSimple_2418() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_2503_2513_split[5]), &(SplitJoin4_FFTReorderSimple_Fiss_2503_2513_join[5]));
	ENDFOR
}

void FFTReorderSimple_2419() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_2503_2513_split[6]), &(SplitJoin4_FFTReorderSimple_Fiss_2503_2513_join[6]));
	ENDFOR
}

void FFTReorderSimple_2420() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_2503_2513_split[7]), &(SplitJoin4_FFTReorderSimple_Fiss_2503_2513_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2411() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2503_2513_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2406WEIGHTED_ROUND_ROBIN_Splitter_2411));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2412() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2412WEIGHTED_ROUND_ROBIN_Splitter_2421, pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_2503_2513_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_2423() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_join[0]));
	ENDFOR
}

void FFTReorderSimple_2424() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_join[1]));
	ENDFOR
}

void FFTReorderSimple_2425() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_join[2]));
	ENDFOR
}

void FFTReorderSimple_2426() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_join[3]));
	ENDFOR
}

void FFTReorderSimple_2427() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_split[4]), &(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_join[4]));
	ENDFOR
}

void FFTReorderSimple_2428() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_split[5]), &(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_join[5]));
	ENDFOR
}

void FFTReorderSimple_2429() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_split[6]), &(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_join[6]));
	ENDFOR
}

void FFTReorderSimple_2430() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_split[7]), &(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_join[7]));
	ENDFOR
}

void FFTReorderSimple_2431() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_split[8]), &(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_join[8]));
	ENDFOR
}

void FFTReorderSimple_2432() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_split[9]), &(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_join[9]));
	ENDFOR
}

void FFTReorderSimple_2433() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_split[10]), &(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_join[10]));
	ENDFOR
}

void FFTReorderSimple_2434() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_split[11]), &(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_join[11]));
	ENDFOR
}

void FFTReorderSimple_2435() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_split[12]), &(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_join[12]));
	ENDFOR
}

void FFTReorderSimple_2436() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_split[13]), &(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_join[13]));
	ENDFOR
}

void FFTReorderSimple_2437() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_split[14]), &(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_join[14]));
	ENDFOR
}

void FFTReorderSimple_2438() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_split[15]), &(SplitJoin6_FFTReorderSimple_Fiss_2504_2514_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2421() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2504_2514_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2412WEIGHTED_ROUND_ROBIN_Splitter_2421));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2422() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2422WEIGHTED_ROUND_ROBIN_Splitter_2439, pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2504_2514_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&(*chanin), (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2441_s.wn.real) - (w.imag * CombineDFT_2441_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2441_s.wn.imag) + (w.imag * CombineDFT_2441_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&(*chanin)) ; 
			push_complex(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_2441() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2505_2515_split[0]), &(SplitJoin8_CombineDFT_Fiss_2505_2515_join[0]));
	ENDFOR
}

void CombineDFT_2442() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2505_2515_split[1]), &(SplitJoin8_CombineDFT_Fiss_2505_2515_join[1]));
	ENDFOR
}

void CombineDFT_2443() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2505_2515_split[2]), &(SplitJoin8_CombineDFT_Fiss_2505_2515_join[2]));
	ENDFOR
}

void CombineDFT_2444() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2505_2515_split[3]), &(SplitJoin8_CombineDFT_Fiss_2505_2515_join[3]));
	ENDFOR
}

void CombineDFT_2445() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2505_2515_split[4]), &(SplitJoin8_CombineDFT_Fiss_2505_2515_join[4]));
	ENDFOR
}

void CombineDFT_2446() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2505_2515_split[5]), &(SplitJoin8_CombineDFT_Fiss_2505_2515_join[5]));
	ENDFOR
}

void CombineDFT_2447() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2505_2515_split[6]), &(SplitJoin8_CombineDFT_Fiss_2505_2515_join[6]));
	ENDFOR
}

void CombineDFT_2448() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2505_2515_split[7]), &(SplitJoin8_CombineDFT_Fiss_2505_2515_join[7]));
	ENDFOR
}

void CombineDFT_2449() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2505_2515_split[8]), &(SplitJoin8_CombineDFT_Fiss_2505_2515_join[8]));
	ENDFOR
}

void CombineDFT_2450() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2505_2515_split[9]), &(SplitJoin8_CombineDFT_Fiss_2505_2515_join[9]));
	ENDFOR
}

void CombineDFT_2451() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2505_2515_split[10]), &(SplitJoin8_CombineDFT_Fiss_2505_2515_join[10]));
	ENDFOR
}

void CombineDFT_2452() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2505_2515_split[11]), &(SplitJoin8_CombineDFT_Fiss_2505_2515_join[11]));
	ENDFOR
}

void CombineDFT_2453() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2505_2515_split[12]), &(SplitJoin8_CombineDFT_Fiss_2505_2515_join[12]));
	ENDFOR
}

void CombineDFT_2454() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2505_2515_split[13]), &(SplitJoin8_CombineDFT_Fiss_2505_2515_join[13]));
	ENDFOR
}

void CombineDFT_2455() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2505_2515_split[14]), &(SplitJoin8_CombineDFT_Fiss_2505_2515_join[14]));
	ENDFOR
}

void CombineDFT_2456() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2505_2515_split[15]), &(SplitJoin8_CombineDFT_Fiss_2505_2515_join[15]));
	ENDFOR
}

void CombineDFT_2457() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2505_2515_split[16]), &(SplitJoin8_CombineDFT_Fiss_2505_2515_join[16]));
	ENDFOR
}

void CombineDFT_2458() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2505_2515_split[17]), &(SplitJoin8_CombineDFT_Fiss_2505_2515_join[17]));
	ENDFOR
}

void CombineDFT_2459() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2505_2515_split[18]), &(SplitJoin8_CombineDFT_Fiss_2505_2515_join[18]));
	ENDFOR
}

void CombineDFT_2460() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2505_2515_split[19]), &(SplitJoin8_CombineDFT_Fiss_2505_2515_join[19]));
	ENDFOR
}

void CombineDFT_2461() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2505_2515_split[20]), &(SplitJoin8_CombineDFT_Fiss_2505_2515_join[20]));
	ENDFOR
}

void CombineDFT_2462() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2505_2515_split[21]), &(SplitJoin8_CombineDFT_Fiss_2505_2515_join[21]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2439() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 22, __iter_++)
			push_complex(&SplitJoin8_CombineDFT_Fiss_2505_2515_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2422WEIGHTED_ROUND_ROBIN_Splitter_2439));
			push_complex(&SplitJoin8_CombineDFT_Fiss_2505_2515_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2422WEIGHTED_ROUND_ROBIN_Splitter_2439));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2440() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 22, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2440WEIGHTED_ROUND_ROBIN_Splitter_2463, pop_complex(&SplitJoin8_CombineDFT_Fiss_2505_2515_join[__iter_]));
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2440WEIGHTED_ROUND_ROBIN_Splitter_2463, pop_complex(&SplitJoin8_CombineDFT_Fiss_2505_2515_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_2465() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2506_2516_split[0]), &(SplitJoin10_CombineDFT_Fiss_2506_2516_join[0]));
	ENDFOR
}

void CombineDFT_2466() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2506_2516_split[1]), &(SplitJoin10_CombineDFT_Fiss_2506_2516_join[1]));
	ENDFOR
}

void CombineDFT_2467() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2506_2516_split[2]), &(SplitJoin10_CombineDFT_Fiss_2506_2516_join[2]));
	ENDFOR
}

void CombineDFT_2468() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2506_2516_split[3]), &(SplitJoin10_CombineDFT_Fiss_2506_2516_join[3]));
	ENDFOR
}

void CombineDFT_2469() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2506_2516_split[4]), &(SplitJoin10_CombineDFT_Fiss_2506_2516_join[4]));
	ENDFOR
}

void CombineDFT_2470() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2506_2516_split[5]), &(SplitJoin10_CombineDFT_Fiss_2506_2516_join[5]));
	ENDFOR
}

void CombineDFT_2471() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2506_2516_split[6]), &(SplitJoin10_CombineDFT_Fiss_2506_2516_join[6]));
	ENDFOR
}

void CombineDFT_2472() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2506_2516_split[7]), &(SplitJoin10_CombineDFT_Fiss_2506_2516_join[7]));
	ENDFOR
}

void CombineDFT_2473() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2506_2516_split[8]), &(SplitJoin10_CombineDFT_Fiss_2506_2516_join[8]));
	ENDFOR
}

void CombineDFT_2474() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2506_2516_split[9]), &(SplitJoin10_CombineDFT_Fiss_2506_2516_join[9]));
	ENDFOR
}

void CombineDFT_2475() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2506_2516_split[10]), &(SplitJoin10_CombineDFT_Fiss_2506_2516_join[10]));
	ENDFOR
}

void CombineDFT_2476() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2506_2516_split[11]), &(SplitJoin10_CombineDFT_Fiss_2506_2516_join[11]));
	ENDFOR
}

void CombineDFT_2477() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2506_2516_split[12]), &(SplitJoin10_CombineDFT_Fiss_2506_2516_join[12]));
	ENDFOR
}

void CombineDFT_2478() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2506_2516_split[13]), &(SplitJoin10_CombineDFT_Fiss_2506_2516_join[13]));
	ENDFOR
}

void CombineDFT_2479() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2506_2516_split[14]), &(SplitJoin10_CombineDFT_Fiss_2506_2516_join[14]));
	ENDFOR
}

void CombineDFT_2480() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2506_2516_split[15]), &(SplitJoin10_CombineDFT_Fiss_2506_2516_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2463() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin10_CombineDFT_Fiss_2506_2516_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2440WEIGHTED_ROUND_ROBIN_Splitter_2463));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2464() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2464WEIGHTED_ROUND_ROBIN_Splitter_2481, pop_complex(&SplitJoin10_CombineDFT_Fiss_2506_2516_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2483() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2507_2517_split[0]), &(SplitJoin12_CombineDFT_Fiss_2507_2517_join[0]));
	ENDFOR
}

void CombineDFT_2484() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2507_2517_split[1]), &(SplitJoin12_CombineDFT_Fiss_2507_2517_join[1]));
	ENDFOR
}

void CombineDFT_2485() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2507_2517_split[2]), &(SplitJoin12_CombineDFT_Fiss_2507_2517_join[2]));
	ENDFOR
}

void CombineDFT_2486() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2507_2517_split[3]), &(SplitJoin12_CombineDFT_Fiss_2507_2517_join[3]));
	ENDFOR
}

void CombineDFT_2487() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2507_2517_split[4]), &(SplitJoin12_CombineDFT_Fiss_2507_2517_join[4]));
	ENDFOR
}

void CombineDFT_2488() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2507_2517_split[5]), &(SplitJoin12_CombineDFT_Fiss_2507_2517_join[5]));
	ENDFOR
}

void CombineDFT_2489() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2507_2517_split[6]), &(SplitJoin12_CombineDFT_Fiss_2507_2517_join[6]));
	ENDFOR
}

void CombineDFT_2490() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2507_2517_split[7]), &(SplitJoin12_CombineDFT_Fiss_2507_2517_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2481() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin12_CombineDFT_Fiss_2507_2517_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2464WEIGHTED_ROUND_ROBIN_Splitter_2481));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2482() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2482WEIGHTED_ROUND_ROBIN_Splitter_2491, pop_complex(&SplitJoin12_CombineDFT_Fiss_2507_2517_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2493() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2508_2518_split[0]), &(SplitJoin14_CombineDFT_Fiss_2508_2518_join[0]));
	ENDFOR
}

void CombineDFT_2494() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2508_2518_split[1]), &(SplitJoin14_CombineDFT_Fiss_2508_2518_join[1]));
	ENDFOR
}

void CombineDFT_2495() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2508_2518_split[2]), &(SplitJoin14_CombineDFT_Fiss_2508_2518_join[2]));
	ENDFOR
}

void CombineDFT_2496() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2508_2518_split[3]), &(SplitJoin14_CombineDFT_Fiss_2508_2518_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2491() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin14_CombineDFT_Fiss_2508_2518_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2482WEIGHTED_ROUND_ROBIN_Splitter_2491));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2492() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2492WEIGHTED_ROUND_ROBIN_Splitter_2497, pop_complex(&SplitJoin14_CombineDFT_Fiss_2508_2518_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2499() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_2509_2519_split[0]), &(SplitJoin16_CombineDFT_Fiss_2509_2519_join[0]));
	ENDFOR
}

void CombineDFT_2500() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_2509_2519_split[1]), &(SplitJoin16_CombineDFT_Fiss_2509_2519_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2497() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_2509_2519_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2492WEIGHTED_ROUND_ROBIN_Splitter_2497));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_2509_2519_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2492WEIGHTED_ROUND_ROBIN_Splitter_2497));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2498() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2498CombineDFT_2398, pop_complex(&SplitJoin16_CombineDFT_Fiss_2509_2519_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2498CombineDFT_2398, pop_complex(&SplitJoin16_CombineDFT_Fiss_2509_2519_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_2398() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_2498CombineDFT_2398), &(CombineDFT_2398CPrinter_2399));
	ENDFOR
}

void CPrinter(buffer_complex_t *chanin) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		printf("%.10f", c.real);
		printf("\n");
		printf("%.10f", c.imag);
		printf("\n");
	}


void CPrinter_2399() {
	FOR(uint32_t, __iter_steady_, 0, <, 704, __iter_steady_++)
		CPrinter(&(CombineDFT_2398CPrinter_2399));
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&FFTTestSource_2387FFTReorderSimple_2388);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2482WEIGHTED_ROUND_ROBIN_Splitter_2491);
	FOR(int, __iter_init_0_, 0, <, 4, __iter_init_0_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_2502_2512_join[__iter_init_0_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2422WEIGHTED_ROUND_ROBIN_Splitter_2439);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2412WEIGHTED_ROUND_ROBIN_Splitter_2421);
	FOR(int, __iter_init_1_, 0, <, 16, __iter_init_1_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_2504_2514_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_2509_2519_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 16, __iter_init_3_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_2504_2514_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_2507_2517_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_2507_2517_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 22, __iter_init_6_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_2505_2515_join[__iter_init_6_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2492WEIGHTED_ROUND_ROBIN_Splitter_2497);
	FOR(int, __iter_init_7_, 0, <, 16, __iter_init_7_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_2506_2516_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 16, __iter_init_8_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_2506_2516_join[__iter_init_8_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2498CombineDFT_2398);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2406WEIGHTED_ROUND_ROBIN_Splitter_2411);
	FOR(int, __iter_init_9_, 0, <, 2, __iter_init_9_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_2501_2511_join[__iter_init_9_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2402WEIGHTED_ROUND_ROBIN_Splitter_2405);
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_2503_2513_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 4, __iter_init_11_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_2508_2518_split[__iter_init_11_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2464WEIGHTED_ROUND_ROBIN_Splitter_2481);
	FOR(int, __iter_init_12_, 0, <, 22, __iter_init_12_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_2505_2515_split[__iter_init_12_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2440WEIGHTED_ROUND_ROBIN_Splitter_2463);
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_2509_2519_split[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 8, __iter_init_14_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_2503_2513_split[__iter_init_14_]);
	ENDFOR
	init_buffer_complex(&FFTReorderSimple_2388WEIGHTED_ROUND_ROBIN_Splitter_2401);
	init_buffer_complex(&CombineDFT_2398CPrinter_2399);
	FOR(int, __iter_init_15_, 0, <, 4, __iter_init_15_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_2502_2512_split[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 4, __iter_init_16_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_2508_2518_join[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_2501_2511_split[__iter_init_17_]);
	ENDFOR
// --- init: CombineDFT_2441
	 {
	 ; 
	CombineDFT_2441_s.wn.real = -1.0 ; 
	CombineDFT_2441_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2442
	 {
	CombineDFT_2442_s.wn.real = -1.0 ; 
	CombineDFT_2442_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2443
	 {
	CombineDFT_2443_s.wn.real = -1.0 ; 
	CombineDFT_2443_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2444
	 {
	CombineDFT_2444_s.wn.real = -1.0 ; 
	CombineDFT_2444_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2445
	 {
	CombineDFT_2445_s.wn.real = -1.0 ; 
	CombineDFT_2445_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2446
	 {
	CombineDFT_2446_s.wn.real = -1.0 ; 
	CombineDFT_2446_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2447
	 {
	CombineDFT_2447_s.wn.real = -1.0 ; 
	CombineDFT_2447_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2448
	 {
	CombineDFT_2448_s.wn.real = -1.0 ; 
	CombineDFT_2448_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2449
	 {
	CombineDFT_2449_s.wn.real = -1.0 ; 
	CombineDFT_2449_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2450
	 {
	CombineDFT_2450_s.wn.real = -1.0 ; 
	CombineDFT_2450_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2451
	 {
	CombineDFT_2451_s.wn.real = -1.0 ; 
	CombineDFT_2451_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2452
	 {
	CombineDFT_2452_s.wn.real = -1.0 ; 
	CombineDFT_2452_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2453
	 {
	CombineDFT_2453_s.wn.real = -1.0 ; 
	CombineDFT_2453_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2454
	 {
	CombineDFT_2454_s.wn.real = -1.0 ; 
	CombineDFT_2454_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2455
	 {
	CombineDFT_2455_s.wn.real = -1.0 ; 
	CombineDFT_2455_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2456
	 {
	CombineDFT_2456_s.wn.real = -1.0 ; 
	CombineDFT_2456_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2457
	 {
	CombineDFT_2457_s.wn.real = -1.0 ; 
	CombineDFT_2457_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2458
	 {
	CombineDFT_2458_s.wn.real = -1.0 ; 
	CombineDFT_2458_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2459
	 {
	CombineDFT_2459_s.wn.real = -1.0 ; 
	CombineDFT_2459_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2460
	 {
	CombineDFT_2460_s.wn.real = -1.0 ; 
	CombineDFT_2460_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2461
	 {
	CombineDFT_2461_s.wn.real = -1.0 ; 
	CombineDFT_2461_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2462
	 {
	CombineDFT_2462_s.wn.real = -1.0 ; 
	CombineDFT_2462_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2465
	 {
	CombineDFT_2465_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2465_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2466
	 {
	CombineDFT_2466_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2466_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2467
	 {
	CombineDFT_2467_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2467_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2468
	 {
	CombineDFT_2468_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2468_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2469
	 {
	CombineDFT_2469_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2469_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2470
	 {
	CombineDFT_2470_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2470_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2471
	 {
	CombineDFT_2471_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2471_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2472
	 {
	CombineDFT_2472_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2472_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2473
	 {
	CombineDFT_2473_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2473_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2474
	 {
	CombineDFT_2474_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2474_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2475
	 {
	CombineDFT_2475_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2475_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2476
	 {
	CombineDFT_2476_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2476_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2477
	 {
	CombineDFT_2477_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2477_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2478
	 {
	CombineDFT_2478_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2478_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2479
	 {
	CombineDFT_2479_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2479_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2480
	 {
	CombineDFT_2480_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2480_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2483
	 {
	CombineDFT_2483_s.wn.real = 0.70710677 ; 
	CombineDFT_2483_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2484
	 {
	CombineDFT_2484_s.wn.real = 0.70710677 ; 
	CombineDFT_2484_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2485
	 {
	CombineDFT_2485_s.wn.real = 0.70710677 ; 
	CombineDFT_2485_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2486
	 {
	CombineDFT_2486_s.wn.real = 0.70710677 ; 
	CombineDFT_2486_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2487
	 {
	CombineDFT_2487_s.wn.real = 0.70710677 ; 
	CombineDFT_2487_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2488
	 {
	CombineDFT_2488_s.wn.real = 0.70710677 ; 
	CombineDFT_2488_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2489
	 {
	CombineDFT_2489_s.wn.real = 0.70710677 ; 
	CombineDFT_2489_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2490
	 {
	CombineDFT_2490_s.wn.real = 0.70710677 ; 
	CombineDFT_2490_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2493
	 {
	CombineDFT_2493_s.wn.real = 0.9238795 ; 
	CombineDFT_2493_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_2494
	 {
	CombineDFT_2494_s.wn.real = 0.9238795 ; 
	CombineDFT_2494_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_2495
	 {
	CombineDFT_2495_s.wn.real = 0.9238795 ; 
	CombineDFT_2495_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_2496
	 {
	CombineDFT_2496_s.wn.real = 0.9238795 ; 
	CombineDFT_2496_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_2499
	 {
	CombineDFT_2499_s.wn.real = 0.98078525 ; 
	CombineDFT_2499_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_2500
	 {
	CombineDFT_2500_s.wn.real = 0.98078525 ; 
	CombineDFT_2500_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_2398
	 {
	 ; 
	CombineDFT_2398_s.wn.real = 0.9951847 ; 
	CombineDFT_2398_s.wn.imag = -0.09801714 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FFTTestSource_2387();
		FFTReorderSimple_2388();
		WEIGHTED_ROUND_ROBIN_Splitter_2401();
			FFTReorderSimple_2403();
			FFTReorderSimple_2404();
		WEIGHTED_ROUND_ROBIN_Joiner_2402();
		WEIGHTED_ROUND_ROBIN_Splitter_2405();
			FFTReorderSimple_2407();
			FFTReorderSimple_2408();
			FFTReorderSimple_2409();
			FFTReorderSimple_2410();
		WEIGHTED_ROUND_ROBIN_Joiner_2406();
		WEIGHTED_ROUND_ROBIN_Splitter_2411();
			FFTReorderSimple_2413();
			FFTReorderSimple_2414();
			FFTReorderSimple_2415();
			FFTReorderSimple_2416();
			FFTReorderSimple_2417();
			FFTReorderSimple_2418();
			FFTReorderSimple_2419();
			FFTReorderSimple_2420();
		WEIGHTED_ROUND_ROBIN_Joiner_2412();
		WEIGHTED_ROUND_ROBIN_Splitter_2421();
			FFTReorderSimple_2423();
			FFTReorderSimple_2424();
			FFTReorderSimple_2425();
			FFTReorderSimple_2426();
			FFTReorderSimple_2427();
			FFTReorderSimple_2428();
			FFTReorderSimple_2429();
			FFTReorderSimple_2430();
			FFTReorderSimple_2431();
			FFTReorderSimple_2432();
			FFTReorderSimple_2433();
			FFTReorderSimple_2434();
			FFTReorderSimple_2435();
			FFTReorderSimple_2436();
			FFTReorderSimple_2437();
			FFTReorderSimple_2438();
		WEIGHTED_ROUND_ROBIN_Joiner_2422();
		WEIGHTED_ROUND_ROBIN_Splitter_2439();
			CombineDFT_2441();
			CombineDFT_2442();
			CombineDFT_2443();
			CombineDFT_2444();
			CombineDFT_2445();
			CombineDFT_2446();
			CombineDFT_2447();
			CombineDFT_2448();
			CombineDFT_2449();
			CombineDFT_2450();
			CombineDFT_2451();
			CombineDFT_2452();
			CombineDFT_2453();
			CombineDFT_2454();
			CombineDFT_2455();
			CombineDFT_2456();
			CombineDFT_2457();
			CombineDFT_2458();
			CombineDFT_2459();
			CombineDFT_2460();
			CombineDFT_2461();
			CombineDFT_2462();
		WEIGHTED_ROUND_ROBIN_Joiner_2440();
		WEIGHTED_ROUND_ROBIN_Splitter_2463();
			CombineDFT_2465();
			CombineDFT_2466();
			CombineDFT_2467();
			CombineDFT_2468();
			CombineDFT_2469();
			CombineDFT_2470();
			CombineDFT_2471();
			CombineDFT_2472();
			CombineDFT_2473();
			CombineDFT_2474();
			CombineDFT_2475();
			CombineDFT_2476();
			CombineDFT_2477();
			CombineDFT_2478();
			CombineDFT_2479();
			CombineDFT_2480();
		WEIGHTED_ROUND_ROBIN_Joiner_2464();
		WEIGHTED_ROUND_ROBIN_Splitter_2481();
			CombineDFT_2483();
			CombineDFT_2484();
			CombineDFT_2485();
			CombineDFT_2486();
			CombineDFT_2487();
			CombineDFT_2488();
			CombineDFT_2489();
			CombineDFT_2490();
		WEIGHTED_ROUND_ROBIN_Joiner_2482();
		WEIGHTED_ROUND_ROBIN_Splitter_2491();
			CombineDFT_2493();
			CombineDFT_2494();
			CombineDFT_2495();
			CombineDFT_2496();
		WEIGHTED_ROUND_ROBIN_Joiner_2492();
		WEIGHTED_ROUND_ROBIN_Splitter_2497();
			CombineDFT_2499();
			CombineDFT_2500();
		WEIGHTED_ROUND_ROBIN_Joiner_2498();
		CombineDFT_2398();
		CPrinter_2399();
	ENDFOR
	return EXIT_SUCCESS;
}
