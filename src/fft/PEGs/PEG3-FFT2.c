#include "PEG3-FFT2.h"

buffer_float_t SplitJoin16_CombineDFT_Fiss_12444_12465_split[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12408WEIGHTED_ROUND_ROBIN_Splitter_12412;
buffer_float_t SplitJoin47_CombineDFT_Fiss_12451_12472_split[3];
buffer_float_t SplitJoin16_CombineDFT_Fiss_12444_12465_join[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12347WEIGHTED_ROUND_ROBIN_Splitter_12338;
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_12438_12459_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12394WEIGHTED_ROUND_ROBIN_Splitter_12397;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12428WEIGHTED_ROUND_ROBIN_Splitter_12432;
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_12441_12462_split[3];
buffer_float_t SplitJoin43_FFTReorderSimple_Fiss_12449_12470_split[3];
buffer_float_t SplitJoin14_CombineDFT_Fiss_12443_12464_join[3];
buffer_float_t SplitJoin43_FFTReorderSimple_Fiss_12449_12470_join[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12418WEIGHTED_ROUND_ROBIN_Splitter_12422;
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_12438_12459_join[2];
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_12439_12460_join[3];
buffer_float_t SplitJoin53_CombineDFT_Fiss_12454_12475_split[3];
buffer_float_t SplitJoin53_CombineDFT_Fiss_12454_12475_join[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12433CombineDFT_12336;
buffer_float_t SplitJoin12_CombineDFT_Fiss_12442_12463_join[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12380WEIGHTED_ROUND_ROBIN_Splitter_12384;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12339FloatPrinter_12337;
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_12440_12461_split[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12360WEIGHTED_ROUND_ROBIN_Splitter_12364;
buffer_float_t SplitJoin49_CombineDFT_Fiss_12452_12473_split[3];
buffer_float_t SplitJoin51_CombineDFT_Fiss_12453_12474_split[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12390CombineDFT_12325;
buffer_float_t SplitJoin55_CombineDFT_Fiss_12455_12476_join[2];
buffer_float_t SplitJoin0_FFTTestSource_Fiss_12436_12457_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12413WEIGHTED_ROUND_ROBIN_Splitter_12417;
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_12441_12462_join[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12375WEIGHTED_ROUND_ROBIN_Splitter_12379;
buffer_float_t SplitJoin41_FFTReorderSimple_Fiss_12448_12469_join[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12398WEIGHTED_ROUND_ROBIN_Splitter_12402;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12423WEIGHTED_ROUND_ROBIN_Splitter_12427;
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_12304_12340_12437_12458_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12385WEIGHTED_ROUND_ROBIN_Splitter_12389;
buffer_float_t FFTReorderSimple_12315WEIGHTED_ROUND_ROBIN_Splitter_12350;
buffer_float_t SplitJoin0_FFTTestSource_Fiss_12436_12457_split[2];
buffer_float_t SplitJoin47_CombineDFT_Fiss_12451_12472_join[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12351WEIGHTED_ROUND_ROBIN_Splitter_12354;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12355WEIGHTED_ROUND_ROBIN_Splitter_12359;
buffer_float_t SplitJoin14_CombineDFT_Fiss_12443_12464_split[3];
buffer_float_t FFTReorderSimple_12326WEIGHTED_ROUND_ROBIN_Splitter_12393;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12403WEIGHTED_ROUND_ROBIN_Splitter_12407;
buffer_float_t SplitJoin39_FFTReorderSimple_Fiss_12447_12468_split[2];
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_12440_12461_join[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12365WEIGHTED_ROUND_ROBIN_Splitter_12369;
buffer_float_t SplitJoin20_CombineDFT_Fiss_12446_12467_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_12370WEIGHTED_ROUND_ROBIN_Splitter_12374;
buffer_float_t SplitJoin18_CombineDFT_Fiss_12445_12466_join[3];
buffer_float_t SplitJoin45_FFTReorderSimple_Fiss_12450_12471_split[3];
buffer_float_t SplitJoin20_CombineDFT_Fiss_12446_12467_join[2];
buffer_float_t SplitJoin18_CombineDFT_Fiss_12445_12466_split[3];
buffer_float_t SplitJoin49_CombineDFT_Fiss_12452_12473_join[3];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_12304_12340_12437_12458_join[2];
buffer_float_t SplitJoin45_FFTReorderSimple_Fiss_12450_12471_join[3];
buffer_float_t SplitJoin51_CombineDFT_Fiss_12453_12474_join[3];
buffer_float_t SplitJoin55_CombineDFT_Fiss_12455_12476_split[2];
buffer_float_t SplitJoin12_CombineDFT_Fiss_12442_12463_split[3];
buffer_float_t SplitJoin39_FFTReorderSimple_Fiss_12447_12468_join[2];
buffer_float_t SplitJoin41_FFTReorderSimple_Fiss_12448_12469_split[3];
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_12439_12460_split[3];


CombineDFT_12371_t CombineDFT_12371_s;
CombineDFT_12371_t CombineDFT_12372_s;
CombineDFT_12371_t CombineDFT_12373_s;
CombineDFT_12376_t CombineDFT_12376_s;
CombineDFT_12376_t CombineDFT_12377_s;
CombineDFT_12376_t CombineDFT_12378_s;
CombineDFT_12381_t CombineDFT_12381_s;
CombineDFT_12381_t CombineDFT_12382_s;
CombineDFT_12381_t CombineDFT_12383_s;
CombineDFT_12386_t CombineDFT_12386_s;
CombineDFT_12386_t CombineDFT_12387_s;
CombineDFT_12386_t CombineDFT_12388_s;
CombineDFT_12391_t CombineDFT_12391_s;
CombineDFT_12391_t CombineDFT_12392_s;
CombineDFT_12325_t CombineDFT_12325_s;
CombineDFT_12371_t CombineDFT_12414_s;
CombineDFT_12371_t CombineDFT_12415_s;
CombineDFT_12371_t CombineDFT_12416_s;
CombineDFT_12376_t CombineDFT_12419_s;
CombineDFT_12376_t CombineDFT_12420_s;
CombineDFT_12376_t CombineDFT_12421_s;
CombineDFT_12381_t CombineDFT_12424_s;
CombineDFT_12381_t CombineDFT_12425_s;
CombineDFT_12381_t CombineDFT_12426_s;
CombineDFT_12386_t CombineDFT_12429_s;
CombineDFT_12386_t CombineDFT_12430_s;
CombineDFT_12386_t CombineDFT_12431_s;
CombineDFT_12391_t CombineDFT_12434_s;
CombineDFT_12391_t CombineDFT_12435_s;
CombineDFT_12325_t CombineDFT_12336_s;

void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), 0.0) ; 
		push_float(&(*chanout), 0.0) ; 
		push_float(&(*chanout), 1.0) ; 
		push_float(&(*chanout), 0.0) ; 
		FOR(int, i, 0,  < , 124, i++) {
			push_float(&(*chanout), 0.0) ; 
		}
		ENDFOR
	}


void FFTTestSource_12348() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_12436_12457_split[0]), &(SplitJoin0_FFTTestSource_Fiss_12436_12457_join[0]));
	ENDFOR
}

void FFTTestSource_12349() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_12436_12457_split[1]), &(SplitJoin0_FFTTestSource_Fiss_12436_12457_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12346() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_12347() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12347WEIGHTED_ROUND_ROBIN_Splitter_12338, pop_float(&SplitJoin0_FFTTestSource_Fiss_12436_12457_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12347WEIGHTED_ROUND_ROBIN_Splitter_12338, pop_float(&SplitJoin0_FFTTestSource_Fiss_12436_12457_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, i, 0,  < , 128, i = (i + 4)) {
			push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 128, i = (i + 4)) {
			push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_float(&(*chanin)) ; 
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void FFTReorderSimple_12315() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_12304_12340_12437_12458_split[0]), &(FFTReorderSimple_12315WEIGHTED_ROUND_ROBIN_Splitter_12350));
	ENDFOR
}

void FFTReorderSimple_12352() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_12438_12459_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_12438_12459_join[0]));
	ENDFOR
}

void FFTReorderSimple_12353() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_12438_12459_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_12438_12459_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12350() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_12438_12459_split[0], pop_float(&FFTReorderSimple_12315WEIGHTED_ROUND_ROBIN_Splitter_12350));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_12438_12459_split[1], pop_float(&FFTReorderSimple_12315WEIGHTED_ROUND_ROBIN_Splitter_12350));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12351() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12351WEIGHTED_ROUND_ROBIN_Splitter_12354, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_12438_12459_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12351WEIGHTED_ROUND_ROBIN_Splitter_12354, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_12438_12459_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_12356() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_12439_12460_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_12439_12460_join[0]));
	ENDFOR
}

void FFTReorderSimple_12357() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_12439_12460_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_12439_12460_join[1]));
	ENDFOR
}

void FFTReorderSimple_12358() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_12439_12460_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_12439_12460_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12354() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin6_FFTReorderSimple_Fiss_12439_12460_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12351WEIGHTED_ROUND_ROBIN_Splitter_12354));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12355() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12355WEIGHTED_ROUND_ROBIN_Splitter_12359, pop_float(&SplitJoin6_FFTReorderSimple_Fiss_12439_12460_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_12361() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_12440_12461_split[0]), &(SplitJoin8_FFTReorderSimple_Fiss_12440_12461_join[0]));
	ENDFOR
}

void FFTReorderSimple_12362() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_12440_12461_split[1]), &(SplitJoin8_FFTReorderSimple_Fiss_12440_12461_join[1]));
	ENDFOR
}

void FFTReorderSimple_12363() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_12440_12461_split[2]), &(SplitJoin8_FFTReorderSimple_Fiss_12440_12461_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12359() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin8_FFTReorderSimple_Fiss_12440_12461_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12355WEIGHTED_ROUND_ROBIN_Splitter_12359));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12360() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12360WEIGHTED_ROUND_ROBIN_Splitter_12364, pop_float(&SplitJoin8_FFTReorderSimple_Fiss_12440_12461_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_12366() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_12441_12462_split[0]), &(SplitJoin10_FFTReorderSimple_Fiss_12441_12462_join[0]));
	ENDFOR
}

void FFTReorderSimple_12367() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_12441_12462_split[1]), &(SplitJoin10_FFTReorderSimple_Fiss_12441_12462_join[1]));
	ENDFOR
}

void FFTReorderSimple_12368() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_12441_12462_split[2]), &(SplitJoin10_FFTReorderSimple_Fiss_12441_12462_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12364() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_FFTReorderSimple_Fiss_12441_12462_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12360WEIGHTED_ROUND_ROBIN_Splitter_12364));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12365() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12365WEIGHTED_ROUND_ROBIN_Splitter_12369, pop_float(&SplitJoin10_FFTReorderSimple_Fiss_12441_12462_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&(*chanin), i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&(*chanin), i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&(*chanin), (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&(*chanin), (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_12371_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_12371_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&(*chanin)) ; 
			push_float(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_12371() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_12442_12463_split[0]), &(SplitJoin12_CombineDFT_Fiss_12442_12463_join[0]));
	ENDFOR
}

void CombineDFT_12372() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_12442_12463_split[1]), &(SplitJoin12_CombineDFT_Fiss_12442_12463_join[1]));
	ENDFOR
}

void CombineDFT_12373() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_12442_12463_split[2]), &(SplitJoin12_CombineDFT_Fiss_12442_12463_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12369() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin12_CombineDFT_Fiss_12442_12463_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12365WEIGHTED_ROUND_ROBIN_Splitter_12369));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12370() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12370WEIGHTED_ROUND_ROBIN_Splitter_12374, pop_float(&SplitJoin12_CombineDFT_Fiss_12442_12463_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_12376() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_12443_12464_split[0]), &(SplitJoin14_CombineDFT_Fiss_12443_12464_join[0]));
	ENDFOR
}

void CombineDFT_12377() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_12443_12464_split[1]), &(SplitJoin14_CombineDFT_Fiss_12443_12464_join[1]));
	ENDFOR
}

void CombineDFT_12378() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_12443_12464_split[2]), &(SplitJoin14_CombineDFT_Fiss_12443_12464_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12374() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin14_CombineDFT_Fiss_12443_12464_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12370WEIGHTED_ROUND_ROBIN_Splitter_12374));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12375() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12375WEIGHTED_ROUND_ROBIN_Splitter_12379, pop_float(&SplitJoin14_CombineDFT_Fiss_12443_12464_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_12381() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_12444_12465_split[0]), &(SplitJoin16_CombineDFT_Fiss_12444_12465_join[0]));
	ENDFOR
}

void CombineDFT_12382() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_12444_12465_split[1]), &(SplitJoin16_CombineDFT_Fiss_12444_12465_join[1]));
	ENDFOR
}

void CombineDFT_12383() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_12444_12465_split[2]), &(SplitJoin16_CombineDFT_Fiss_12444_12465_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12379() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin16_CombineDFT_Fiss_12444_12465_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12375WEIGHTED_ROUND_ROBIN_Splitter_12379));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12380() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12380WEIGHTED_ROUND_ROBIN_Splitter_12384, pop_float(&SplitJoin16_CombineDFT_Fiss_12444_12465_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_12386() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_12445_12466_split[0]), &(SplitJoin18_CombineDFT_Fiss_12445_12466_join[0]));
	ENDFOR
}

void CombineDFT_12387() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_12445_12466_split[1]), &(SplitJoin18_CombineDFT_Fiss_12445_12466_join[1]));
	ENDFOR
}

void CombineDFT_12388() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_12445_12466_split[2]), &(SplitJoin18_CombineDFT_Fiss_12445_12466_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12384() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin18_CombineDFT_Fiss_12445_12466_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12380WEIGHTED_ROUND_ROBIN_Splitter_12384));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12385() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12385WEIGHTED_ROUND_ROBIN_Splitter_12389, pop_float(&SplitJoin18_CombineDFT_Fiss_12445_12466_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_12391() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin20_CombineDFT_Fiss_12446_12467_split[0]), &(SplitJoin20_CombineDFT_Fiss_12446_12467_join[0]));
	ENDFOR
}

void CombineDFT_12392() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin20_CombineDFT_Fiss_12446_12467_split[1]), &(SplitJoin20_CombineDFT_Fiss_12446_12467_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12389() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_12446_12467_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12385WEIGHTED_ROUND_ROBIN_Splitter_12389));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_12446_12467_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12385WEIGHTED_ROUND_ROBIN_Splitter_12389));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12390() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12390CombineDFT_12325, pop_float(&SplitJoin20_CombineDFT_Fiss_12446_12467_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12390CombineDFT_12325, pop_float(&SplitJoin20_CombineDFT_Fiss_12446_12467_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_12325() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_12390CombineDFT_12325), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_12304_12340_12437_12458_join[0]));
	ENDFOR
}

void FFTReorderSimple_12326() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_12304_12340_12437_12458_split[1]), &(FFTReorderSimple_12326WEIGHTED_ROUND_ROBIN_Splitter_12393));
	ENDFOR
}

void FFTReorderSimple_12395() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin39_FFTReorderSimple_Fiss_12447_12468_split[0]), &(SplitJoin39_FFTReorderSimple_Fiss_12447_12468_join[0]));
	ENDFOR
}

void FFTReorderSimple_12396() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin39_FFTReorderSimple_Fiss_12447_12468_split[1]), &(SplitJoin39_FFTReorderSimple_Fiss_12447_12468_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12393() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin39_FFTReorderSimple_Fiss_12447_12468_split[0], pop_float(&FFTReorderSimple_12326WEIGHTED_ROUND_ROBIN_Splitter_12393));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin39_FFTReorderSimple_Fiss_12447_12468_split[1], pop_float(&FFTReorderSimple_12326WEIGHTED_ROUND_ROBIN_Splitter_12393));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12394() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12394WEIGHTED_ROUND_ROBIN_Splitter_12397, pop_float(&SplitJoin39_FFTReorderSimple_Fiss_12447_12468_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12394WEIGHTED_ROUND_ROBIN_Splitter_12397, pop_float(&SplitJoin39_FFTReorderSimple_Fiss_12447_12468_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_12399() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin41_FFTReorderSimple_Fiss_12448_12469_split[0]), &(SplitJoin41_FFTReorderSimple_Fiss_12448_12469_join[0]));
	ENDFOR
}

void FFTReorderSimple_12400() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin41_FFTReorderSimple_Fiss_12448_12469_split[1]), &(SplitJoin41_FFTReorderSimple_Fiss_12448_12469_join[1]));
	ENDFOR
}

void FFTReorderSimple_12401() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin41_FFTReorderSimple_Fiss_12448_12469_split[2]), &(SplitJoin41_FFTReorderSimple_Fiss_12448_12469_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12397() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin41_FFTReorderSimple_Fiss_12448_12469_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12394WEIGHTED_ROUND_ROBIN_Splitter_12397));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12398() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12398WEIGHTED_ROUND_ROBIN_Splitter_12402, pop_float(&SplitJoin41_FFTReorderSimple_Fiss_12448_12469_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_12404() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin43_FFTReorderSimple_Fiss_12449_12470_split[0]), &(SplitJoin43_FFTReorderSimple_Fiss_12449_12470_join[0]));
	ENDFOR
}

void FFTReorderSimple_12405() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin43_FFTReorderSimple_Fiss_12449_12470_split[1]), &(SplitJoin43_FFTReorderSimple_Fiss_12449_12470_join[1]));
	ENDFOR
}

void FFTReorderSimple_12406() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin43_FFTReorderSimple_Fiss_12449_12470_split[2]), &(SplitJoin43_FFTReorderSimple_Fiss_12449_12470_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12402() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin43_FFTReorderSimple_Fiss_12449_12470_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12398WEIGHTED_ROUND_ROBIN_Splitter_12402));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12403() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12403WEIGHTED_ROUND_ROBIN_Splitter_12407, pop_float(&SplitJoin43_FFTReorderSimple_Fiss_12449_12470_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_12409() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin45_FFTReorderSimple_Fiss_12450_12471_split[0]), &(SplitJoin45_FFTReorderSimple_Fiss_12450_12471_join[0]));
	ENDFOR
}

void FFTReorderSimple_12410() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin45_FFTReorderSimple_Fiss_12450_12471_split[1]), &(SplitJoin45_FFTReorderSimple_Fiss_12450_12471_join[1]));
	ENDFOR
}

void FFTReorderSimple_12411() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin45_FFTReorderSimple_Fiss_12450_12471_split[2]), &(SplitJoin45_FFTReorderSimple_Fiss_12450_12471_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12407() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin45_FFTReorderSimple_Fiss_12450_12471_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12403WEIGHTED_ROUND_ROBIN_Splitter_12407));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12408() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12408WEIGHTED_ROUND_ROBIN_Splitter_12412, pop_float(&SplitJoin45_FFTReorderSimple_Fiss_12450_12471_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_12414() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin47_CombineDFT_Fiss_12451_12472_split[0]), &(SplitJoin47_CombineDFT_Fiss_12451_12472_join[0]));
	ENDFOR
}

void CombineDFT_12415() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin47_CombineDFT_Fiss_12451_12472_split[1]), &(SplitJoin47_CombineDFT_Fiss_12451_12472_join[1]));
	ENDFOR
}

void CombineDFT_12416() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin47_CombineDFT_Fiss_12451_12472_split[2]), &(SplitJoin47_CombineDFT_Fiss_12451_12472_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12412() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin47_CombineDFT_Fiss_12451_12472_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12408WEIGHTED_ROUND_ROBIN_Splitter_12412));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12413() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12413WEIGHTED_ROUND_ROBIN_Splitter_12417, pop_float(&SplitJoin47_CombineDFT_Fiss_12451_12472_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_12419() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin49_CombineDFT_Fiss_12452_12473_split[0]), &(SplitJoin49_CombineDFT_Fiss_12452_12473_join[0]));
	ENDFOR
}

void CombineDFT_12420() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin49_CombineDFT_Fiss_12452_12473_split[1]), &(SplitJoin49_CombineDFT_Fiss_12452_12473_join[1]));
	ENDFOR
}

void CombineDFT_12421() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin49_CombineDFT_Fiss_12452_12473_split[2]), &(SplitJoin49_CombineDFT_Fiss_12452_12473_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12417() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin49_CombineDFT_Fiss_12452_12473_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12413WEIGHTED_ROUND_ROBIN_Splitter_12417));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12418() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12418WEIGHTED_ROUND_ROBIN_Splitter_12422, pop_float(&SplitJoin49_CombineDFT_Fiss_12452_12473_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_12424() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin51_CombineDFT_Fiss_12453_12474_split[0]), &(SplitJoin51_CombineDFT_Fiss_12453_12474_join[0]));
	ENDFOR
}

void CombineDFT_12425() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin51_CombineDFT_Fiss_12453_12474_split[1]), &(SplitJoin51_CombineDFT_Fiss_12453_12474_join[1]));
	ENDFOR
}

void CombineDFT_12426() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin51_CombineDFT_Fiss_12453_12474_split[2]), &(SplitJoin51_CombineDFT_Fiss_12453_12474_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12422() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin51_CombineDFT_Fiss_12453_12474_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12418WEIGHTED_ROUND_ROBIN_Splitter_12422));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12423() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12423WEIGHTED_ROUND_ROBIN_Splitter_12427, pop_float(&SplitJoin51_CombineDFT_Fiss_12453_12474_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_12429() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin53_CombineDFT_Fiss_12454_12475_split[0]), &(SplitJoin53_CombineDFT_Fiss_12454_12475_join[0]));
	ENDFOR
}

void CombineDFT_12430() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin53_CombineDFT_Fiss_12454_12475_split[1]), &(SplitJoin53_CombineDFT_Fiss_12454_12475_join[1]));
	ENDFOR
}

void CombineDFT_12431() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin53_CombineDFT_Fiss_12454_12475_split[2]), &(SplitJoin53_CombineDFT_Fiss_12454_12475_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12427() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin53_CombineDFT_Fiss_12454_12475_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12423WEIGHTED_ROUND_ROBIN_Splitter_12427));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12428() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12428WEIGHTED_ROUND_ROBIN_Splitter_12432, pop_float(&SplitJoin53_CombineDFT_Fiss_12454_12475_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_12434() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin55_CombineDFT_Fiss_12455_12476_split[0]), &(SplitJoin55_CombineDFT_Fiss_12455_12476_join[0]));
	ENDFOR
}

void CombineDFT_12435() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin55_CombineDFT_Fiss_12455_12476_split[1]), &(SplitJoin55_CombineDFT_Fiss_12455_12476_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12432() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin55_CombineDFT_Fiss_12455_12476_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12428WEIGHTED_ROUND_ROBIN_Splitter_12432));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin55_CombineDFT_Fiss_12455_12476_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12428WEIGHTED_ROUND_ROBIN_Splitter_12432));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12433() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12433CombineDFT_12336, pop_float(&SplitJoin55_CombineDFT_Fiss_12455_12476_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12433CombineDFT_12336, pop_float(&SplitJoin55_CombineDFT_Fiss_12455_12476_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_12336() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_12433CombineDFT_12336), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_12304_12340_12437_12458_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_12338() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_12304_12340_12437_12458_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12347WEIGHTED_ROUND_ROBIN_Splitter_12338));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_12304_12340_12437_12458_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_12347WEIGHTED_ROUND_ROBIN_Splitter_12338));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_12339() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12339FloatPrinter_12337, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_12304_12340_12437_12458_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_12339FloatPrinter_12337, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_12304_12340_12437_12458_join[1]));
		ENDFOR
	ENDFOR
}}

void FloatPrinter(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void FloatPrinter_12337() {
	FOR(uint32_t, __iter_steady_, 0, <, 768, __iter_steady_++)
		FloatPrinter(&(WEIGHTED_ROUND_ROBIN_Joiner_12339FloatPrinter_12337));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 3, __iter_init_0_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_12444_12465_split[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12408WEIGHTED_ROUND_ROBIN_Splitter_12412);
	FOR(int, __iter_init_1_, 0, <, 3, __iter_init_1_++)
		init_buffer_float(&SplitJoin47_CombineDFT_Fiss_12451_12472_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 3, __iter_init_2_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_12444_12465_join[__iter_init_2_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12347WEIGHTED_ROUND_ROBIN_Splitter_12338);
	FOR(int, __iter_init_3_, 0, <, 2, __iter_init_3_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_12438_12459_split[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12394WEIGHTED_ROUND_ROBIN_Splitter_12397);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12428WEIGHTED_ROUND_ROBIN_Splitter_12432);
	FOR(int, __iter_init_4_, 0, <, 3, __iter_init_4_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_12441_12462_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 3, __iter_init_5_++)
		init_buffer_float(&SplitJoin43_FFTReorderSimple_Fiss_12449_12470_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 3, __iter_init_6_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_12443_12464_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 3, __iter_init_7_++)
		init_buffer_float(&SplitJoin43_FFTReorderSimple_Fiss_12449_12470_join[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12418WEIGHTED_ROUND_ROBIN_Splitter_12422);
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_12438_12459_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 3, __iter_init_9_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_12439_12460_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 3, __iter_init_10_++)
		init_buffer_float(&SplitJoin53_CombineDFT_Fiss_12454_12475_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 3, __iter_init_11_++)
		init_buffer_float(&SplitJoin53_CombineDFT_Fiss_12454_12475_join[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12433CombineDFT_12336);
	FOR(int, __iter_init_12_, 0, <, 3, __iter_init_12_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_12442_12463_join[__iter_init_12_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12380WEIGHTED_ROUND_ROBIN_Splitter_12384);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12339FloatPrinter_12337);
	FOR(int, __iter_init_13_, 0, <, 3, __iter_init_13_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_12440_12461_split[__iter_init_13_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12360WEIGHTED_ROUND_ROBIN_Splitter_12364);
	FOR(int, __iter_init_14_, 0, <, 3, __iter_init_14_++)
		init_buffer_float(&SplitJoin49_CombineDFT_Fiss_12452_12473_split[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 3, __iter_init_15_++)
		init_buffer_float(&SplitJoin51_CombineDFT_Fiss_12453_12474_split[__iter_init_15_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12390CombineDFT_12325);
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_float(&SplitJoin55_CombineDFT_Fiss_12455_12476_join[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_12436_12457_join[__iter_init_17_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12413WEIGHTED_ROUND_ROBIN_Splitter_12417);
	FOR(int, __iter_init_18_, 0, <, 3, __iter_init_18_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_12441_12462_join[__iter_init_18_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12375WEIGHTED_ROUND_ROBIN_Splitter_12379);
	FOR(int, __iter_init_19_, 0, <, 3, __iter_init_19_++)
		init_buffer_float(&SplitJoin41_FFTReorderSimple_Fiss_12448_12469_join[__iter_init_19_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12398WEIGHTED_ROUND_ROBIN_Splitter_12402);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12423WEIGHTED_ROUND_ROBIN_Splitter_12427);
	FOR(int, __iter_init_20_, 0, <, 2, __iter_init_20_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_12304_12340_12437_12458_split[__iter_init_20_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12385WEIGHTED_ROUND_ROBIN_Splitter_12389);
	init_buffer_float(&FFTReorderSimple_12315WEIGHTED_ROUND_ROBIN_Splitter_12350);
	FOR(int, __iter_init_21_, 0, <, 2, __iter_init_21_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_12436_12457_split[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 3, __iter_init_22_++)
		init_buffer_float(&SplitJoin47_CombineDFT_Fiss_12451_12472_join[__iter_init_22_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12351WEIGHTED_ROUND_ROBIN_Splitter_12354);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12355WEIGHTED_ROUND_ROBIN_Splitter_12359);
	FOR(int, __iter_init_23_, 0, <, 3, __iter_init_23_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_12443_12464_split[__iter_init_23_]);
	ENDFOR
	init_buffer_float(&FFTReorderSimple_12326WEIGHTED_ROUND_ROBIN_Splitter_12393);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12403WEIGHTED_ROUND_ROBIN_Splitter_12407);
	FOR(int, __iter_init_24_, 0, <, 2, __iter_init_24_++)
		init_buffer_float(&SplitJoin39_FFTReorderSimple_Fiss_12447_12468_split[__iter_init_24_]);
	ENDFOR
	FOR(int, __iter_init_25_, 0, <, 3, __iter_init_25_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_12440_12461_join[__iter_init_25_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12365WEIGHTED_ROUND_ROBIN_Splitter_12369);
	FOR(int, __iter_init_26_, 0, <, 2, __iter_init_26_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_12446_12467_split[__iter_init_26_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_12370WEIGHTED_ROUND_ROBIN_Splitter_12374);
	FOR(int, __iter_init_27_, 0, <, 3, __iter_init_27_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_12445_12466_join[__iter_init_27_]);
	ENDFOR
	FOR(int, __iter_init_28_, 0, <, 3, __iter_init_28_++)
		init_buffer_float(&SplitJoin45_FFTReorderSimple_Fiss_12450_12471_split[__iter_init_28_]);
	ENDFOR
	FOR(int, __iter_init_29_, 0, <, 2, __iter_init_29_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_12446_12467_join[__iter_init_29_]);
	ENDFOR
	FOR(int, __iter_init_30_, 0, <, 3, __iter_init_30_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_12445_12466_split[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 3, __iter_init_31_++)
		init_buffer_float(&SplitJoin49_CombineDFT_Fiss_12452_12473_join[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 2, __iter_init_32_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_12304_12340_12437_12458_join[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 3, __iter_init_33_++)
		init_buffer_float(&SplitJoin45_FFTReorderSimple_Fiss_12450_12471_join[__iter_init_33_]);
	ENDFOR
	FOR(int, __iter_init_34_, 0, <, 3, __iter_init_34_++)
		init_buffer_float(&SplitJoin51_CombineDFT_Fiss_12453_12474_join[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 2, __iter_init_35_++)
		init_buffer_float(&SplitJoin55_CombineDFT_Fiss_12455_12476_split[__iter_init_35_]);
	ENDFOR
	FOR(int, __iter_init_36_, 0, <, 3, __iter_init_36_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_12442_12463_split[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 2, __iter_init_37_++)
		init_buffer_float(&SplitJoin39_FFTReorderSimple_Fiss_12447_12468_join[__iter_init_37_]);
	ENDFOR
	FOR(int, __iter_init_38_, 0, <, 3, __iter_init_38_++)
		init_buffer_float(&SplitJoin41_FFTReorderSimple_Fiss_12448_12469_split[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 3, __iter_init_39_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_12439_12460_split[__iter_init_39_]);
	ENDFOR
// --- init: CombineDFT_12371
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_12371_s.w[i] = real ; 
		CombineDFT_12371_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12372
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_12372_s.w[i] = real ; 
		CombineDFT_12372_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12373
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_12373_s.w[i] = real ; 
		CombineDFT_12373_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12376
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_12376_s.w[i] = real ; 
		CombineDFT_12376_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12377
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_12377_s.w[i] = real ; 
		CombineDFT_12377_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12378
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_12378_s.w[i] = real ; 
		CombineDFT_12378_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12381
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_12381_s.w[i] = real ; 
		CombineDFT_12381_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12382
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_12382_s.w[i] = real ; 
		CombineDFT_12382_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12383
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_12383_s.w[i] = real ; 
		CombineDFT_12383_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12386
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_12386_s.w[i] = real ; 
		CombineDFT_12386_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12387
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_12387_s.w[i] = real ; 
		CombineDFT_12387_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12388
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_12388_s.w[i] = real ; 
		CombineDFT_12388_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12391
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_12391_s.w[i] = real ; 
		CombineDFT_12391_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12392
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_12392_s.w[i] = real ; 
		CombineDFT_12392_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12325
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_12325_s.w[i] = real ; 
		CombineDFT_12325_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12414
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_12414_s.w[i] = real ; 
		CombineDFT_12414_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12415
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_12415_s.w[i] = real ; 
		CombineDFT_12415_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12416
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_12416_s.w[i] = real ; 
		CombineDFT_12416_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12419
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_12419_s.w[i] = real ; 
		CombineDFT_12419_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12420
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_12420_s.w[i] = real ; 
		CombineDFT_12420_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12421
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_12421_s.w[i] = real ; 
		CombineDFT_12421_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12424
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_12424_s.w[i] = real ; 
		CombineDFT_12424_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12425
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_12425_s.w[i] = real ; 
		CombineDFT_12425_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12426
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_12426_s.w[i] = real ; 
		CombineDFT_12426_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12429
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_12429_s.w[i] = real ; 
		CombineDFT_12429_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12430
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_12430_s.w[i] = real ; 
		CombineDFT_12430_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12431
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_12431_s.w[i] = real ; 
		CombineDFT_12431_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12434
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_12434_s.w[i] = real ; 
		CombineDFT_12434_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12435
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_12435_s.w[i] = real ; 
		CombineDFT_12435_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_12336
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_12336_s.w[i] = real ; 
		CombineDFT_12336_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_12346();
			FFTTestSource_12348();
			FFTTestSource_12349();
		WEIGHTED_ROUND_ROBIN_Joiner_12347();
		WEIGHTED_ROUND_ROBIN_Splitter_12338();
			FFTReorderSimple_12315();
			WEIGHTED_ROUND_ROBIN_Splitter_12350();
				FFTReorderSimple_12352();
				FFTReorderSimple_12353();
			WEIGHTED_ROUND_ROBIN_Joiner_12351();
			WEIGHTED_ROUND_ROBIN_Splitter_12354();
				FFTReorderSimple_12356();
				FFTReorderSimple_12357();
				FFTReorderSimple_12358();
			WEIGHTED_ROUND_ROBIN_Joiner_12355();
			WEIGHTED_ROUND_ROBIN_Splitter_12359();
				FFTReorderSimple_12361();
				FFTReorderSimple_12362();
				FFTReorderSimple_12363();
			WEIGHTED_ROUND_ROBIN_Joiner_12360();
			WEIGHTED_ROUND_ROBIN_Splitter_12364();
				FFTReorderSimple_12366();
				FFTReorderSimple_12367();
				FFTReorderSimple_12368();
			WEIGHTED_ROUND_ROBIN_Joiner_12365();
			WEIGHTED_ROUND_ROBIN_Splitter_12369();
				CombineDFT_12371();
				CombineDFT_12372();
				CombineDFT_12373();
			WEIGHTED_ROUND_ROBIN_Joiner_12370();
			WEIGHTED_ROUND_ROBIN_Splitter_12374();
				CombineDFT_12376();
				CombineDFT_12377();
				CombineDFT_12378();
			WEIGHTED_ROUND_ROBIN_Joiner_12375();
			WEIGHTED_ROUND_ROBIN_Splitter_12379();
				CombineDFT_12381();
				CombineDFT_12382();
				CombineDFT_12383();
			WEIGHTED_ROUND_ROBIN_Joiner_12380();
			WEIGHTED_ROUND_ROBIN_Splitter_12384();
				CombineDFT_12386();
				CombineDFT_12387();
				CombineDFT_12388();
			WEIGHTED_ROUND_ROBIN_Joiner_12385();
			WEIGHTED_ROUND_ROBIN_Splitter_12389();
				CombineDFT_12391();
				CombineDFT_12392();
			WEIGHTED_ROUND_ROBIN_Joiner_12390();
			CombineDFT_12325();
			FFTReorderSimple_12326();
			WEIGHTED_ROUND_ROBIN_Splitter_12393();
				FFTReorderSimple_12395();
				FFTReorderSimple_12396();
			WEIGHTED_ROUND_ROBIN_Joiner_12394();
			WEIGHTED_ROUND_ROBIN_Splitter_12397();
				FFTReorderSimple_12399();
				FFTReorderSimple_12400();
				FFTReorderSimple_12401();
			WEIGHTED_ROUND_ROBIN_Joiner_12398();
			WEIGHTED_ROUND_ROBIN_Splitter_12402();
				FFTReorderSimple_12404();
				FFTReorderSimple_12405();
				FFTReorderSimple_12406();
			WEIGHTED_ROUND_ROBIN_Joiner_12403();
			WEIGHTED_ROUND_ROBIN_Splitter_12407();
				FFTReorderSimple_12409();
				FFTReorderSimple_12410();
				FFTReorderSimple_12411();
			WEIGHTED_ROUND_ROBIN_Joiner_12408();
			WEIGHTED_ROUND_ROBIN_Splitter_12412();
				CombineDFT_12414();
				CombineDFT_12415();
				CombineDFT_12416();
			WEIGHTED_ROUND_ROBIN_Joiner_12413();
			WEIGHTED_ROUND_ROBIN_Splitter_12417();
				CombineDFT_12419();
				CombineDFT_12420();
				CombineDFT_12421();
			WEIGHTED_ROUND_ROBIN_Joiner_12418();
			WEIGHTED_ROUND_ROBIN_Splitter_12422();
				CombineDFT_12424();
				CombineDFT_12425();
				CombineDFT_12426();
			WEIGHTED_ROUND_ROBIN_Joiner_12423();
			WEIGHTED_ROUND_ROBIN_Splitter_12427();
				CombineDFT_12429();
				CombineDFT_12430();
				CombineDFT_12431();
			WEIGHTED_ROUND_ROBIN_Joiner_12428();
			WEIGHTED_ROUND_ROBIN_Splitter_12432();
				CombineDFT_12434();
				CombineDFT_12435();
			WEIGHTED_ROUND_ROBIN_Joiner_12433();
			CombineDFT_12336();
		WEIGHTED_ROUND_ROBIN_Joiner_12339();
		FloatPrinter_12337();
	ENDFOR
	return EXIT_SUCCESS;
}
