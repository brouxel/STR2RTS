#include "PEG3-FFT5.h"

buffer_complex_t Pre_CollapsedDataParallel_1_9329butterfly_9228;
buffer_complex_t Pre_CollapsedDataParallel_1_9338butterfly_9231;
buffer_complex_t SplitJoin63_SplitJoin49_SplitJoin49_AnonFilter_a0_9174_9427_9514_9530_join[2];
buffer_complex_t SplitJoin43_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child1_9483_9548_split[2];
buffer_complex_t SplitJoin20_SplitJoin14_SplitJoin14_split1_9154_9398_9512_9551_join[2];
buffer_complex_t SplitJoin73_SplitJoin59_SplitJoin59_AnonFilter_a0_9188_9434_9517_9533_split[2];
buffer_complex_t SplitJoin67_SplitJoin53_SplitJoin53_AnonFilter_a0_9180_9430_9515_9531_split[2];
buffer_complex_t SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9162_9388_9507_9526_join[2];
buffer_complex_t SplitJoin39_SplitJoin29_SplitJoin29_split2_9145_9410_9476_9547_split[2];
buffer_complex_t SplitJoin56_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child1_9478_9543_split[4];
buffer_complex_t SplitJoin81_SplitJoin67_SplitJoin67_AnonFilter_a0_9198_9439_9519_9536_split[2];
buffer_complex_t SplitJoin18_SplitJoin12_SplitJoin12_split2_9143_9396_9474_9546_split[2];
buffer_complex_t Pre_CollapsedDataParallel_1_9320butterfly_9225;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_9341WEIGHTED_ROUND_ROBIN_Splitter_9484;
buffer_complex_t SplitJoin77_SplitJoin63_SplitJoin63_AnonFilter_a0_9194_9437_9473_9534_split[2];
buffer_complex_t SplitJoin89_SplitJoin75_SplitJoin75_AnonFilter_a0_9210_9445_9521_9538_split[2];
buffer_complex_t SplitJoin39_SplitJoin29_SplitJoin29_split2_9145_9410_9476_9547_join[2];
buffer_complex_t SplitJoin10_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_Hier_9510_9541_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_9383WEIGHTED_ROUND_ROBIN_Splitter_9501;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_9495WEIGHTED_ROUND_ROBIN_Splitter_9382;
buffer_complex_t SplitJoin45_SplitJoin33_SplitJoin33_split2_9147_9413_9477_9549_split[2];
buffer_complex_t SplitJoin22_SplitJoin16_SplitJoin16_split2_9156_9399_9480_9552_split[4];
buffer_complex_t SplitJoin91_SplitJoin77_SplitJoin77_AnonFilter_a0_9212_9446_9522_9539_split[2];
buffer_complex_t SplitJoin73_SplitJoin59_SplitJoin59_AnonFilter_a0_9188_9434_9517_9533_join[2];
buffer_complex_t SplitJoin91_SplitJoin77_SplitJoin77_AnonFilter_a0_9212_9446_9522_9539_join[2];
buffer_complex_t SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9166_9390_9508_9528_split[2];
buffer_complex_t SplitJoin79_SplitJoin65_SplitJoin65_AnonFilter_a0_9196_9438_9518_9535_join[2];
buffer_complex_t butterfly_9224Post_CollapsedDataParallel_2_9318;
buffer_complex_t Pre_CollapsedDataParallel_1_9326butterfly_9227;
buffer_complex_t SplitJoin67_SplitJoin53_SplitJoin53_AnonFilter_a0_9180_9430_9515_9531_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_9332butterfly_9229;
buffer_complex_t SplitJoin18_SplitJoin12_SplitJoin12_split2_9143_9396_9474_9546_join[2];
buffer_complex_t butterfly_9229Post_CollapsedDataParallel_2_9333;
buffer_complex_t SplitJoin0_source_Fiss_9506_9525_join[2];
buffer_complex_t SplitJoin69_SplitJoin55_SplitJoin55_AnonFilter_a0_9182_9431_9516_9532_join[2];
buffer_complex_t SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9162_9388_9507_9526_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_9489WEIGHTED_ROUND_ROBIN_Splitter_9490;
buffer_complex_t SplitJoin85_SplitJoin71_SplitJoin71_AnonFilter_a0_9204_9442_9520_9537_split[2];
buffer_complex_t SplitJoin10_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_Hier_9510_9541_split[2];
buffer_complex_t SplitJoin77_SplitJoin63_SplitJoin63_AnonFilter_a0_9194_9437_9473_9534_join[2];
buffer_complex_t SplitJoin56_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child1_9478_9543_join[4];
buffer_complex_t SplitJoin43_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child1_9483_9548_join[2];
buffer_complex_t SplitJoin49_SplitJoin37_SplitJoin37_split2_9149_9416_9479_9550_join[2];
buffer_complex_t SplitJoin16_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child0_9481_9545_split[2];
buffer_complex_t SplitJoin12_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child0_9475_9542_split[4];
buffer_complex_t SplitJoin20_SplitJoin14_SplitJoin14_split1_9154_9398_9512_9551_split[2];
buffer_complex_t SplitJoin22_SplitJoin16_SplitJoin16_split2_9156_9399_9480_9552_join[4];
buffer_complex_t SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_9168_9391_9509_9529_join[2];
buffer_complex_t SplitJoin12_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child0_9475_9542_join[4];
buffer_complex_t Pre_CollapsedDataParallel_1_9335butterfly_9230;
buffer_complex_t SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_9168_9391_9509_9529_split[2];
buffer_complex_t SplitJoin14_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_Hier_9511_9544_split[2];
buffer_complex_t SplitJoin45_SplitJoin33_SplitJoin33_split2_9147_9413_9477_9549_join[2];
buffer_complex_t SplitJoin95_SplitJoin81_SplitJoin81_AnonFilter_a0_9218_9449_9523_9540_split[2];
buffer_complex_t butterfly_9227Post_CollapsedDataParallel_2_9327;
buffer_complex_t SplitJoin79_SplitJoin65_SplitJoin65_AnonFilter_a0_9196_9438_9518_9535_split[2];
buffer_complex_t SplitJoin16_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child0_9481_9545_join[2];
buffer_complex_t butterfly_9230Post_CollapsedDataParallel_2_9336;
buffer_complex_t SplitJoin95_SplitJoin81_SplitJoin81_AnonFilter_a0_9218_9449_9523_9540_join[2];
buffer_float_t SplitJoin24_magnitude_Fiss_9513_9554_join[3];
buffer_complex_t butterfly_9226Post_CollapsedDataParallel_2_9324;
buffer_complex_t butterfly_9225Post_CollapsedDataParallel_2_9321;
buffer_complex_t SplitJoin69_SplitJoin55_SplitJoin55_AnonFilter_a0_9182_9431_9516_9532_split[2];
buffer_complex_t butterfly_9228Post_CollapsedDataParallel_2_9330;
buffer_complex_t butterfly_9231Post_CollapsedDataParallel_2_9339;
buffer_complex_t SplitJoin14_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_Hier_9511_9544_join[2];
buffer_complex_t SplitJoin49_SplitJoin37_SplitJoin37_split2_9149_9416_9479_9550_split[2];
buffer_complex_t SplitJoin24_magnitude_Fiss_9513_9554_split[3];
buffer_complex_t SplitJoin0_source_Fiss_9506_9525_split[2];
buffer_complex_t SplitJoin85_SplitJoin71_SplitJoin71_AnonFilter_a0_9204_9442_9520_9537_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_9317butterfly_9224;
buffer_complex_t SplitJoin32_SplitJoin22_SplitJoin22_split2_9158_9404_9482_9553_join[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_9498WEIGHTED_ROUND_ROBIN_Splitter_9340;
buffer_complex_t SplitJoin81_SplitJoin67_SplitJoin67_AnonFilter_a0_9198_9439_9519_9536_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_9323butterfly_9226;
buffer_complex_t SplitJoin89_SplitJoin75_SplitJoin75_AnonFilter_a0_9210_9445_9521_9538_join[2];
buffer_complex_t SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9164_9389_9472_9527_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_9502sink_9251;
buffer_complex_t SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9164_9389_9472_9527_join[2];
buffer_complex_t SplitJoin32_SplitJoin22_SplitJoin22_split2_9158_9404_9482_9553_split[4];
buffer_complex_t SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9166_9390_9508_9528_join[2];
buffer_complex_t SplitJoin63_SplitJoin49_SplitJoin49_AnonFilter_a0_9174_9427_9514_9530_split[2];



void source(buffer_void_t *chanin, buffer_complex_t *chanout) {
		complex_t t;
		t.imag = 0.0 ; 
		t.real = 0.9501 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.2311 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.6068 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.486 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.8913 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.7621 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.4565 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.0185 ; 
		push_complex(&(*chanout), t) ; 
	}


void source_9499() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		source(&(SplitJoin0_source_Fiss_9506_9525_split[0]), &(SplitJoin0_source_Fiss_9506_9525_join[0]));
	ENDFOR
}

void source_9500() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		source(&(SplitJoin0_source_Fiss_9506_9525_split[1]), &(SplitJoin0_source_Fiss_9506_9525_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9497() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_9498() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9498WEIGHTED_ROUND_ROBIN_Splitter_9340, pop_complex(&SplitJoin0_source_Fiss_9506_9525_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9498WEIGHTED_ROUND_ROBIN_Splitter_9340, pop_complex(&SplitJoin0_source_Fiss_9506_9525_join[1]));
		ENDFOR
	ENDFOR
}}

void Identity(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t __tmp2462 = pop_complex(&(*chanin));
		push_complex(&(*chanout), __tmp2462) ; 
	}


void Identity_9170() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_9168_9391_9509_9529_split[0]), &(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_9168_9391_9509_9529_join[0]));
	ENDFOR
}

void Identity_9172() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_9168_9391_9509_9529_split[1]), &(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_9168_9391_9509_9529_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9346() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_9168_9391_9509_9529_split[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9166_9390_9508_9528_split[0]));
		push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_9168_9391_9509_9529_split[1], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9166_9390_9508_9528_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9347() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9166_9390_9508_9528_join[0], pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_9168_9391_9509_9529_join[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9166_9390_9508_9528_join[0], pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_9168_9391_9509_9529_join[1]));
	ENDFOR
}}

void Identity_9176() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin63_SplitJoin49_SplitJoin49_AnonFilter_a0_9174_9427_9514_9530_split[0]), &(SplitJoin63_SplitJoin49_SplitJoin49_AnonFilter_a0_9174_9427_9514_9530_join[0]));
	ENDFOR
}

void Identity_9178() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin63_SplitJoin49_SplitJoin49_AnonFilter_a0_9174_9427_9514_9530_split[1]), &(SplitJoin63_SplitJoin49_SplitJoin49_AnonFilter_a0_9174_9427_9514_9530_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9348() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin63_SplitJoin49_SplitJoin49_AnonFilter_a0_9174_9427_9514_9530_split[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9166_9390_9508_9528_split[1]));
		push_complex(&SplitJoin63_SplitJoin49_SplitJoin49_AnonFilter_a0_9174_9427_9514_9530_split[1], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9166_9390_9508_9528_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9349() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9166_9390_9508_9528_join[1], pop_complex(&SplitJoin63_SplitJoin49_SplitJoin49_AnonFilter_a0_9174_9427_9514_9530_join[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9166_9390_9508_9528_join[1], pop_complex(&SplitJoin63_SplitJoin49_SplitJoin49_AnonFilter_a0_9174_9427_9514_9530_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_9344() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9166_9390_9508_9528_split[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9164_9389_9472_9527_split[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9166_9390_9508_9528_split[1], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9164_9389_9472_9527_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9345() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9164_9389_9472_9527_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9166_9390_9508_9528_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9164_9389_9472_9527_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9166_9390_9508_9528_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9164_9389_9472_9527_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9166_9390_9508_9528_join[1]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9164_9389_9472_9527_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9166_9390_9508_9528_join[1]));
	ENDFOR
}}

void Identity_9184() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin69_SplitJoin55_SplitJoin55_AnonFilter_a0_9182_9431_9516_9532_split[0]), &(SplitJoin69_SplitJoin55_SplitJoin55_AnonFilter_a0_9182_9431_9516_9532_join[0]));
	ENDFOR
}

void Identity_9186() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin69_SplitJoin55_SplitJoin55_AnonFilter_a0_9182_9431_9516_9532_split[1]), &(SplitJoin69_SplitJoin55_SplitJoin55_AnonFilter_a0_9182_9431_9516_9532_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9352() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin69_SplitJoin55_SplitJoin55_AnonFilter_a0_9182_9431_9516_9532_split[0], pop_complex(&SplitJoin67_SplitJoin53_SplitJoin53_AnonFilter_a0_9180_9430_9515_9531_split[0]));
		push_complex(&SplitJoin69_SplitJoin55_SplitJoin55_AnonFilter_a0_9182_9431_9516_9532_split[1], pop_complex(&SplitJoin67_SplitJoin53_SplitJoin53_AnonFilter_a0_9180_9430_9515_9531_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9353() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin67_SplitJoin53_SplitJoin53_AnonFilter_a0_9180_9430_9515_9531_join[0], pop_complex(&SplitJoin69_SplitJoin55_SplitJoin55_AnonFilter_a0_9182_9431_9516_9532_join[0]));
		push_complex(&SplitJoin67_SplitJoin53_SplitJoin53_AnonFilter_a0_9180_9430_9515_9531_join[0], pop_complex(&SplitJoin69_SplitJoin55_SplitJoin55_AnonFilter_a0_9182_9431_9516_9532_join[1]));
	ENDFOR
}}

void Identity_9190() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin73_SplitJoin59_SplitJoin59_AnonFilter_a0_9188_9434_9517_9533_split[0]), &(SplitJoin73_SplitJoin59_SplitJoin59_AnonFilter_a0_9188_9434_9517_9533_join[0]));
	ENDFOR
}

void Identity_9192() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin73_SplitJoin59_SplitJoin59_AnonFilter_a0_9188_9434_9517_9533_split[1]), &(SplitJoin73_SplitJoin59_SplitJoin59_AnonFilter_a0_9188_9434_9517_9533_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9354() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin73_SplitJoin59_SplitJoin59_AnonFilter_a0_9188_9434_9517_9533_split[0], pop_complex(&SplitJoin67_SplitJoin53_SplitJoin53_AnonFilter_a0_9180_9430_9515_9531_split[1]));
		push_complex(&SplitJoin73_SplitJoin59_SplitJoin59_AnonFilter_a0_9188_9434_9517_9533_split[1], pop_complex(&SplitJoin67_SplitJoin53_SplitJoin53_AnonFilter_a0_9180_9430_9515_9531_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9355() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin67_SplitJoin53_SplitJoin53_AnonFilter_a0_9180_9430_9515_9531_join[1], pop_complex(&SplitJoin73_SplitJoin59_SplitJoin59_AnonFilter_a0_9188_9434_9517_9533_join[0]));
		push_complex(&SplitJoin67_SplitJoin53_SplitJoin53_AnonFilter_a0_9180_9430_9515_9531_join[1], pop_complex(&SplitJoin73_SplitJoin59_SplitJoin59_AnonFilter_a0_9188_9434_9517_9533_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_9350() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin67_SplitJoin53_SplitJoin53_AnonFilter_a0_9180_9430_9515_9531_split[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9164_9389_9472_9527_split[1]));
		push_complex(&SplitJoin67_SplitJoin53_SplitJoin53_AnonFilter_a0_9180_9430_9515_9531_split[1], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9164_9389_9472_9527_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9351() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9164_9389_9472_9527_join[1], pop_complex(&SplitJoin67_SplitJoin53_SplitJoin53_AnonFilter_a0_9180_9430_9515_9531_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9164_9389_9472_9527_join[1], pop_complex(&SplitJoin67_SplitJoin53_SplitJoin53_AnonFilter_a0_9180_9430_9515_9531_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9164_9389_9472_9527_join[1], pop_complex(&SplitJoin67_SplitJoin53_SplitJoin53_AnonFilter_a0_9180_9430_9515_9531_join[1]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9164_9389_9472_9527_join[1], pop_complex(&SplitJoin67_SplitJoin53_SplitJoin53_AnonFilter_a0_9180_9430_9515_9531_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_9342() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 12, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9164_9389_9472_9527_split[0], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9162_9388_9507_9526_split[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9164_9389_9472_9527_split[1], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9162_9388_9507_9526_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9343() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9162_9388_9507_9526_join[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9164_9389_9472_9527_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9162_9388_9507_9526_join[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9164_9389_9472_9527_join[1]));
		ENDFOR
	ENDFOR
}}

void Identity_9200() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin81_SplitJoin67_SplitJoin67_AnonFilter_a0_9198_9439_9519_9536_split[0]), &(SplitJoin81_SplitJoin67_SplitJoin67_AnonFilter_a0_9198_9439_9519_9536_join[0]));
	ENDFOR
}

void Identity_9202() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin81_SplitJoin67_SplitJoin67_AnonFilter_a0_9198_9439_9519_9536_split[1]), &(SplitJoin81_SplitJoin67_SplitJoin67_AnonFilter_a0_9198_9439_9519_9536_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9360() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin81_SplitJoin67_SplitJoin67_AnonFilter_a0_9198_9439_9519_9536_split[0], pop_complex(&SplitJoin79_SplitJoin65_SplitJoin65_AnonFilter_a0_9196_9438_9518_9535_split[0]));
		push_complex(&SplitJoin81_SplitJoin67_SplitJoin67_AnonFilter_a0_9198_9439_9519_9536_split[1], pop_complex(&SplitJoin79_SplitJoin65_SplitJoin65_AnonFilter_a0_9196_9438_9518_9535_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9361() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin79_SplitJoin65_SplitJoin65_AnonFilter_a0_9196_9438_9518_9535_join[0], pop_complex(&SplitJoin81_SplitJoin67_SplitJoin67_AnonFilter_a0_9198_9439_9519_9536_join[0]));
		push_complex(&SplitJoin79_SplitJoin65_SplitJoin65_AnonFilter_a0_9196_9438_9518_9535_join[0], pop_complex(&SplitJoin81_SplitJoin67_SplitJoin67_AnonFilter_a0_9198_9439_9519_9536_join[1]));
	ENDFOR
}}

void Identity_9206() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin85_SplitJoin71_SplitJoin71_AnonFilter_a0_9204_9442_9520_9537_split[0]), &(SplitJoin85_SplitJoin71_SplitJoin71_AnonFilter_a0_9204_9442_9520_9537_join[0]));
	ENDFOR
}

void Identity_9208() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin85_SplitJoin71_SplitJoin71_AnonFilter_a0_9204_9442_9520_9537_split[1]), &(SplitJoin85_SplitJoin71_SplitJoin71_AnonFilter_a0_9204_9442_9520_9537_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9362() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin85_SplitJoin71_SplitJoin71_AnonFilter_a0_9204_9442_9520_9537_split[0], pop_complex(&SplitJoin79_SplitJoin65_SplitJoin65_AnonFilter_a0_9196_9438_9518_9535_split[1]));
		push_complex(&SplitJoin85_SplitJoin71_SplitJoin71_AnonFilter_a0_9204_9442_9520_9537_split[1], pop_complex(&SplitJoin79_SplitJoin65_SplitJoin65_AnonFilter_a0_9196_9438_9518_9535_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9363() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin79_SplitJoin65_SplitJoin65_AnonFilter_a0_9196_9438_9518_9535_join[1], pop_complex(&SplitJoin85_SplitJoin71_SplitJoin71_AnonFilter_a0_9204_9442_9520_9537_join[0]));
		push_complex(&SplitJoin79_SplitJoin65_SplitJoin65_AnonFilter_a0_9196_9438_9518_9535_join[1], pop_complex(&SplitJoin85_SplitJoin71_SplitJoin71_AnonFilter_a0_9204_9442_9520_9537_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_9358() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin79_SplitJoin65_SplitJoin65_AnonFilter_a0_9196_9438_9518_9535_split[0], pop_complex(&SplitJoin77_SplitJoin63_SplitJoin63_AnonFilter_a0_9194_9437_9473_9534_split[0]));
		push_complex(&SplitJoin79_SplitJoin65_SplitJoin65_AnonFilter_a0_9196_9438_9518_9535_split[1], pop_complex(&SplitJoin77_SplitJoin63_SplitJoin63_AnonFilter_a0_9194_9437_9473_9534_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9359() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin77_SplitJoin63_SplitJoin63_AnonFilter_a0_9194_9437_9473_9534_join[0], pop_complex(&SplitJoin79_SplitJoin65_SplitJoin65_AnonFilter_a0_9196_9438_9518_9535_join[0]));
		push_complex(&SplitJoin77_SplitJoin63_SplitJoin63_AnonFilter_a0_9194_9437_9473_9534_join[0], pop_complex(&SplitJoin79_SplitJoin65_SplitJoin65_AnonFilter_a0_9196_9438_9518_9535_join[0]));
		push_complex(&SplitJoin77_SplitJoin63_SplitJoin63_AnonFilter_a0_9194_9437_9473_9534_join[0], pop_complex(&SplitJoin79_SplitJoin65_SplitJoin65_AnonFilter_a0_9196_9438_9518_9535_join[1]));
		push_complex(&SplitJoin77_SplitJoin63_SplitJoin63_AnonFilter_a0_9194_9437_9473_9534_join[0], pop_complex(&SplitJoin79_SplitJoin65_SplitJoin65_AnonFilter_a0_9196_9438_9518_9535_join[1]));
	ENDFOR
}}

void Identity_9214() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin91_SplitJoin77_SplitJoin77_AnonFilter_a0_9212_9446_9522_9539_split[0]), &(SplitJoin91_SplitJoin77_SplitJoin77_AnonFilter_a0_9212_9446_9522_9539_join[0]));
	ENDFOR
}

void Identity_9216() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin91_SplitJoin77_SplitJoin77_AnonFilter_a0_9212_9446_9522_9539_split[1]), &(SplitJoin91_SplitJoin77_SplitJoin77_AnonFilter_a0_9212_9446_9522_9539_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9366() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin91_SplitJoin77_SplitJoin77_AnonFilter_a0_9212_9446_9522_9539_split[0], pop_complex(&SplitJoin89_SplitJoin75_SplitJoin75_AnonFilter_a0_9210_9445_9521_9538_split[0]));
		push_complex(&SplitJoin91_SplitJoin77_SplitJoin77_AnonFilter_a0_9212_9446_9522_9539_split[1], pop_complex(&SplitJoin89_SplitJoin75_SplitJoin75_AnonFilter_a0_9210_9445_9521_9538_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9367() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin89_SplitJoin75_SplitJoin75_AnonFilter_a0_9210_9445_9521_9538_join[0], pop_complex(&SplitJoin91_SplitJoin77_SplitJoin77_AnonFilter_a0_9212_9446_9522_9539_join[0]));
		push_complex(&SplitJoin89_SplitJoin75_SplitJoin75_AnonFilter_a0_9210_9445_9521_9538_join[0], pop_complex(&SplitJoin91_SplitJoin77_SplitJoin77_AnonFilter_a0_9212_9446_9522_9539_join[1]));
	ENDFOR
}}

void Identity_9220() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin95_SplitJoin81_SplitJoin81_AnonFilter_a0_9218_9449_9523_9540_split[0]), &(SplitJoin95_SplitJoin81_SplitJoin81_AnonFilter_a0_9218_9449_9523_9540_join[0]));
	ENDFOR
}

void Identity_9222() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin95_SplitJoin81_SplitJoin81_AnonFilter_a0_9218_9449_9523_9540_split[1]), &(SplitJoin95_SplitJoin81_SplitJoin81_AnonFilter_a0_9218_9449_9523_9540_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9368() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin95_SplitJoin81_SplitJoin81_AnonFilter_a0_9218_9449_9523_9540_split[0], pop_complex(&SplitJoin89_SplitJoin75_SplitJoin75_AnonFilter_a0_9210_9445_9521_9538_split[1]));
		push_complex(&SplitJoin95_SplitJoin81_SplitJoin81_AnonFilter_a0_9218_9449_9523_9540_split[1], pop_complex(&SplitJoin89_SplitJoin75_SplitJoin75_AnonFilter_a0_9210_9445_9521_9538_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9369() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin89_SplitJoin75_SplitJoin75_AnonFilter_a0_9210_9445_9521_9538_join[1], pop_complex(&SplitJoin95_SplitJoin81_SplitJoin81_AnonFilter_a0_9218_9449_9523_9540_join[0]));
		push_complex(&SplitJoin89_SplitJoin75_SplitJoin75_AnonFilter_a0_9210_9445_9521_9538_join[1], pop_complex(&SplitJoin95_SplitJoin81_SplitJoin81_AnonFilter_a0_9218_9449_9523_9540_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_9364() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin89_SplitJoin75_SplitJoin75_AnonFilter_a0_9210_9445_9521_9538_split[0], pop_complex(&SplitJoin77_SplitJoin63_SplitJoin63_AnonFilter_a0_9194_9437_9473_9534_split[1]));
		push_complex(&SplitJoin89_SplitJoin75_SplitJoin75_AnonFilter_a0_9210_9445_9521_9538_split[1], pop_complex(&SplitJoin77_SplitJoin63_SplitJoin63_AnonFilter_a0_9194_9437_9473_9534_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9365() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin77_SplitJoin63_SplitJoin63_AnonFilter_a0_9194_9437_9473_9534_join[1], pop_complex(&SplitJoin89_SplitJoin75_SplitJoin75_AnonFilter_a0_9210_9445_9521_9538_join[0]));
		push_complex(&SplitJoin77_SplitJoin63_SplitJoin63_AnonFilter_a0_9194_9437_9473_9534_join[1], pop_complex(&SplitJoin89_SplitJoin75_SplitJoin75_AnonFilter_a0_9210_9445_9521_9538_join[0]));
		push_complex(&SplitJoin77_SplitJoin63_SplitJoin63_AnonFilter_a0_9194_9437_9473_9534_join[1], pop_complex(&SplitJoin89_SplitJoin75_SplitJoin75_AnonFilter_a0_9210_9445_9521_9538_join[1]));
		push_complex(&SplitJoin77_SplitJoin63_SplitJoin63_AnonFilter_a0_9194_9437_9473_9534_join[1], pop_complex(&SplitJoin89_SplitJoin75_SplitJoin75_AnonFilter_a0_9210_9445_9521_9538_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_9356() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 12, __iter_steady_++)
		push_complex(&SplitJoin77_SplitJoin63_SplitJoin63_AnonFilter_a0_9194_9437_9473_9534_split[0], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9162_9388_9507_9526_split[1]));
		push_complex(&SplitJoin77_SplitJoin63_SplitJoin63_AnonFilter_a0_9194_9437_9473_9534_split[1], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9162_9388_9507_9526_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9357() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9162_9388_9507_9526_join[1], pop_complex(&SplitJoin77_SplitJoin63_SplitJoin63_AnonFilter_a0_9194_9437_9473_9534_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9162_9388_9507_9526_join[1], pop_complex(&SplitJoin77_SplitJoin63_SplitJoin63_AnonFilter_a0_9194_9437_9473_9534_join[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_9340() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 24, __iter_steady_++)
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9162_9388_9507_9526_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9498WEIGHTED_ROUND_ROBIN_Splitter_9340));
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9162_9388_9507_9526_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9498WEIGHTED_ROUND_ROBIN_Splitter_9340));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9341() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9341WEIGHTED_ROUND_ROBIN_Splitter_9484, pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9162_9388_9507_9526_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9341WEIGHTED_ROUND_ROBIN_Splitter_9484, pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9162_9388_9507_9526_join[1]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_complex_t *chanin, buffer_complex_t *chanout) {
 {
 {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
			push_complex(&(*chanout), peek_complex(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
		}
		ENDFOR
	}
	}
	}
		pop_complex(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_9317() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child0_9475_9542_split[0]), &(Pre_CollapsedDataParallel_1_9317butterfly_9224));
	ENDFOR
}

void butterfly(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&(*chanin)));
		complex_t two = ((complex_t) pop_complex(&(*chanin)));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&(*chanout), __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&(*chanout), __sa2) ; 
	}


void butterfly_9224() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_9317butterfly_9224), &(butterfly_9224Post_CollapsedDataParallel_2_9318));
	ENDFOR
}

void Post_CollapsedDataParallel_2(buffer_complex_t *chanin, buffer_complex_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 2, _k++) {
 {
			push_complex(&(*chanout), peek_complex(&(*chanin), (_k + 0))) ; 
		}
		}
		ENDFOR
	}
	}
		pop_complex(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_9318() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_9224Post_CollapsedDataParallel_2_9318), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child0_9475_9542_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_9320() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child0_9475_9542_split[1]), &(Pre_CollapsedDataParallel_1_9320butterfly_9225));
	ENDFOR
}

void butterfly_9225() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_9320butterfly_9225), &(butterfly_9225Post_CollapsedDataParallel_2_9321));
	ENDFOR
}

void Post_CollapsedDataParallel_2_9321() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_9225Post_CollapsedDataParallel_2_9321), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child0_9475_9542_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_9323() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child0_9475_9542_split[2]), &(Pre_CollapsedDataParallel_1_9323butterfly_9226));
	ENDFOR
}

void butterfly_9226() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_9323butterfly_9226), &(butterfly_9226Post_CollapsedDataParallel_2_9324));
	ENDFOR
}

void Post_CollapsedDataParallel_2_9324() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_9226Post_CollapsedDataParallel_2_9324), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child0_9475_9542_join[2]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_9326() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child0_9475_9542_split[3]), &(Pre_CollapsedDataParallel_1_9326butterfly_9227));
	ENDFOR
}

void butterfly_9227() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_9326butterfly_9227), &(butterfly_9227Post_CollapsedDataParallel_2_9327));
	ENDFOR
}

void Post_CollapsedDataParallel_2_9327() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_9227Post_CollapsedDataParallel_2_9327), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child0_9475_9542_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9485() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child0_9475_9542_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_Hier_9510_9541_split[0]));
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child0_9475_9542_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_Hier_9510_9541_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9486() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_Hier_9510_9541_join[0], pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child0_9475_9542_join[__iter_]));
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_Hier_9510_9541_join[0], pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child0_9475_9542_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_9329() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin56_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child1_9478_9543_split[0]), &(Pre_CollapsedDataParallel_1_9329butterfly_9228));
	ENDFOR
}

void butterfly_9228() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_9329butterfly_9228), &(butterfly_9228Post_CollapsedDataParallel_2_9330));
	ENDFOR
}

void Post_CollapsedDataParallel_2_9330() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_9228Post_CollapsedDataParallel_2_9330), &(SplitJoin56_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child1_9478_9543_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_9332() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin56_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child1_9478_9543_split[1]), &(Pre_CollapsedDataParallel_1_9332butterfly_9229));
	ENDFOR
}

void butterfly_9229() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_9332butterfly_9229), &(butterfly_9229Post_CollapsedDataParallel_2_9333));
	ENDFOR
}

void Post_CollapsedDataParallel_2_9333() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_9229Post_CollapsedDataParallel_2_9333), &(SplitJoin56_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child1_9478_9543_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_9335() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin56_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child1_9478_9543_split[2]), &(Pre_CollapsedDataParallel_1_9335butterfly_9230));
	ENDFOR
}

void butterfly_9230() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_9335butterfly_9230), &(butterfly_9230Post_CollapsedDataParallel_2_9336));
	ENDFOR
}

void Post_CollapsedDataParallel_2_9336() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_9230Post_CollapsedDataParallel_2_9336), &(SplitJoin56_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child1_9478_9543_join[2]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_9338() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin56_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child1_9478_9543_split[3]), &(Pre_CollapsedDataParallel_1_9338butterfly_9231));
	ENDFOR
}

void butterfly_9231() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_9338butterfly_9231), &(butterfly_9231Post_CollapsedDataParallel_2_9339));
	ENDFOR
}

void Post_CollapsedDataParallel_2_9339() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_9231Post_CollapsedDataParallel_2_9339), &(SplitJoin56_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child1_9478_9543_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9487() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin56_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child1_9478_9543_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_Hier_9510_9541_split[1]));
			push_complex(&SplitJoin56_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child1_9478_9543_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_Hier_9510_9541_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9488() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_Hier_9510_9541_join[1], pop_complex(&SplitJoin56_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child1_9478_9543_join[__iter_]));
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_Hier_9510_9541_join[1], pop_complex(&SplitJoin56_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child1_9478_9543_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_9484() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_Hier_9510_9541_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9341WEIGHTED_ROUND_ROBIN_Splitter_9484));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_Hier_9510_9541_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9341WEIGHTED_ROUND_ROBIN_Splitter_9484));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9489() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9489WEIGHTED_ROUND_ROBIN_Splitter_9490, pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_Hier_9510_9541_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9489WEIGHTED_ROUND_ROBIN_Splitter_9490, pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_Hier_9510_9541_join[1]));
		ENDFOR
	ENDFOR
}}

void butterfly_9233() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin18_SplitJoin12_SplitJoin12_split2_9143_9396_9474_9546_split[0]), &(SplitJoin18_SplitJoin12_SplitJoin12_split2_9143_9396_9474_9546_join[0]));
	ENDFOR
}

void butterfly_9234() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin18_SplitJoin12_SplitJoin12_split2_9143_9396_9474_9546_split[1]), &(SplitJoin18_SplitJoin12_SplitJoin12_split2_9143_9396_9474_9546_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9374() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_9143_9396_9474_9546_split[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child0_9481_9545_split[0]));
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_9143_9396_9474_9546_split[1], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child0_9481_9545_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9375() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child0_9481_9545_join[0], pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_9143_9396_9474_9546_join[0]));
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child0_9481_9545_join[0], pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_9143_9396_9474_9546_join[1]));
	ENDFOR
}}

void butterfly_9235() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin39_SplitJoin29_SplitJoin29_split2_9145_9410_9476_9547_split[0]), &(SplitJoin39_SplitJoin29_SplitJoin29_split2_9145_9410_9476_9547_join[0]));
	ENDFOR
}

void butterfly_9236() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin39_SplitJoin29_SplitJoin29_split2_9145_9410_9476_9547_split[1]), &(SplitJoin39_SplitJoin29_SplitJoin29_split2_9145_9410_9476_9547_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9376() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin39_SplitJoin29_SplitJoin29_split2_9145_9410_9476_9547_split[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child0_9481_9545_split[1]));
		push_complex(&SplitJoin39_SplitJoin29_SplitJoin29_split2_9145_9410_9476_9547_split[1], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child0_9481_9545_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9377() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child0_9481_9545_join[1], pop_complex(&SplitJoin39_SplitJoin29_SplitJoin29_split2_9145_9410_9476_9547_join[0]));
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child0_9481_9545_join[1], pop_complex(&SplitJoin39_SplitJoin29_SplitJoin29_split2_9145_9410_9476_9547_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_9491() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child0_9481_9545_split[0], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_Hier_9511_9544_split[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child0_9481_9545_split[1], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_Hier_9511_9544_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9492() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_Hier_9511_9544_join[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child0_9481_9545_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_Hier_9511_9544_join[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child0_9481_9545_join[1]));
		ENDFOR
	ENDFOR
}}

void butterfly_9237() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin45_SplitJoin33_SplitJoin33_split2_9147_9413_9477_9549_split[0]), &(SplitJoin45_SplitJoin33_SplitJoin33_split2_9147_9413_9477_9549_join[0]));
	ENDFOR
}

void butterfly_9238() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin45_SplitJoin33_SplitJoin33_split2_9147_9413_9477_9549_split[1]), &(SplitJoin45_SplitJoin33_SplitJoin33_split2_9147_9413_9477_9549_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9378() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin45_SplitJoin33_SplitJoin33_split2_9147_9413_9477_9549_split[0], pop_complex(&SplitJoin43_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child1_9483_9548_split[0]));
		push_complex(&SplitJoin45_SplitJoin33_SplitJoin33_split2_9147_9413_9477_9549_split[1], pop_complex(&SplitJoin43_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child1_9483_9548_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9379() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin43_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child1_9483_9548_join[0], pop_complex(&SplitJoin45_SplitJoin33_SplitJoin33_split2_9147_9413_9477_9549_join[0]));
		push_complex(&SplitJoin43_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child1_9483_9548_join[0], pop_complex(&SplitJoin45_SplitJoin33_SplitJoin33_split2_9147_9413_9477_9549_join[1]));
	ENDFOR
}}

void butterfly_9239() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin49_SplitJoin37_SplitJoin37_split2_9149_9416_9479_9550_split[0]), &(SplitJoin49_SplitJoin37_SplitJoin37_split2_9149_9416_9479_9550_join[0]));
	ENDFOR
}

void butterfly_9240() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin49_SplitJoin37_SplitJoin37_split2_9149_9416_9479_9550_split[1]), &(SplitJoin49_SplitJoin37_SplitJoin37_split2_9149_9416_9479_9550_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9380() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin49_SplitJoin37_SplitJoin37_split2_9149_9416_9479_9550_split[0], pop_complex(&SplitJoin43_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child1_9483_9548_split[1]));
		push_complex(&SplitJoin49_SplitJoin37_SplitJoin37_split2_9149_9416_9479_9550_split[1], pop_complex(&SplitJoin43_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child1_9483_9548_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9381() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin43_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child1_9483_9548_join[1], pop_complex(&SplitJoin49_SplitJoin37_SplitJoin37_split2_9149_9416_9479_9550_join[0]));
		push_complex(&SplitJoin43_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child1_9483_9548_join[1], pop_complex(&SplitJoin49_SplitJoin37_SplitJoin37_split2_9149_9416_9479_9550_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_9493() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin43_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child1_9483_9548_split[0], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_Hier_9511_9544_split[1]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin43_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child1_9483_9548_split[1], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_Hier_9511_9544_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9494() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_Hier_9511_9544_join[1], pop_complex(&SplitJoin43_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child1_9483_9548_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_Hier_9511_9544_join[1], pop_complex(&SplitJoin43_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child1_9483_9548_join[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_9490() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_Hier_9511_9544_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9489WEIGHTED_ROUND_ROBIN_Splitter_9490));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_Hier_9511_9544_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9489WEIGHTED_ROUND_ROBIN_Splitter_9490));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9495() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9495WEIGHTED_ROUND_ROBIN_Splitter_9382, pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_Hier_9511_9544_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9495WEIGHTED_ROUND_ROBIN_Splitter_9382, pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_Hier_9511_9544_join[1]));
		ENDFOR
	ENDFOR
}}

void butterfly_9242() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_9156_9399_9480_9552_split[0]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_9156_9399_9480_9552_join[0]));
	ENDFOR
}

void butterfly_9243() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_9156_9399_9480_9552_split[1]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_9156_9399_9480_9552_join[1]));
	ENDFOR
}

void butterfly_9244() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_9156_9399_9480_9552_split[2]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_9156_9399_9480_9552_join[2]));
	ENDFOR
}

void butterfly_9245() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_9156_9399_9480_9552_split[3]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_9156_9399_9480_9552_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9384() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_9156_9399_9480_9552_split[__iter_], pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_9154_9398_9512_9551_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9385() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_9154_9398_9512_9551_join[0], pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_9156_9399_9480_9552_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void butterfly_9246() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin32_SplitJoin22_SplitJoin22_split2_9158_9404_9482_9553_split[0]), &(SplitJoin32_SplitJoin22_SplitJoin22_split2_9158_9404_9482_9553_join[0]));
	ENDFOR
}

void butterfly_9247() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin32_SplitJoin22_SplitJoin22_split2_9158_9404_9482_9553_split[1]), &(SplitJoin32_SplitJoin22_SplitJoin22_split2_9158_9404_9482_9553_join[1]));
	ENDFOR
}

void butterfly_9248() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin32_SplitJoin22_SplitJoin22_split2_9158_9404_9482_9553_split[2]), &(SplitJoin32_SplitJoin22_SplitJoin22_split2_9158_9404_9482_9553_join[2]));
	ENDFOR
}

void butterfly_9249() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin32_SplitJoin22_SplitJoin22_split2_9158_9404_9482_9553_split[3]), &(SplitJoin32_SplitJoin22_SplitJoin22_split2_9158_9404_9482_9553_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9386() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin32_SplitJoin22_SplitJoin22_split2_9158_9404_9482_9553_split[__iter_], pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_9154_9398_9512_9551_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9387() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_9154_9398_9512_9551_join[1], pop_complex(&SplitJoin32_SplitJoin22_SplitJoin22_split2_9158_9404_9482_9553_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_9382() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_9154_9398_9512_9551_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9495WEIGHTED_ROUND_ROBIN_Splitter_9382));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_9154_9398_9512_9551_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9495WEIGHTED_ROUND_ROBIN_Splitter_9382));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9383() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9383WEIGHTED_ROUND_ROBIN_Splitter_9501, pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_9154_9398_9512_9551_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9383WEIGHTED_ROUND_ROBIN_Splitter_9501, pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_9154_9398_9512_9551_join[1]));
		ENDFOR
	ENDFOR
}}

void magnitude(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		push_float(&(*chanout), ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}


void magnitude_9503() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_9513_9554_split[0]), &(SplitJoin24_magnitude_Fiss_9513_9554_join[0]));
	ENDFOR
}

void magnitude_9504() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_9513_9554_split[1]), &(SplitJoin24_magnitude_Fiss_9513_9554_join[1]));
	ENDFOR
}

void magnitude_9505() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_9513_9554_split[2]), &(SplitJoin24_magnitude_Fiss_9513_9554_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_9501() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_complex(&SplitJoin24_magnitude_Fiss_9513_9554_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9383WEIGHTED_ROUND_ROBIN_Splitter_9501));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_9502() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_9502sink_9251, pop_float(&SplitJoin24_magnitude_Fiss_9513_9554_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void sink(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void sink_9251() {
	FOR(uint32_t, __iter_steady_, 0, <, 48, __iter_steady_++)
		sink(&(WEIGHTED_ROUND_ROBIN_Joiner_9502sink_9251));
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&Pre_CollapsedDataParallel_1_9329butterfly_9228);
	init_buffer_complex(&Pre_CollapsedDataParallel_1_9338butterfly_9231);
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_complex(&SplitJoin63_SplitJoin49_SplitJoin49_AnonFilter_a0_9174_9427_9514_9530_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 2, __iter_init_1_++)
		init_buffer_complex(&SplitJoin43_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child1_9483_9548_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_9154_9398_9512_9551_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 2, __iter_init_3_++)
		init_buffer_complex(&SplitJoin73_SplitJoin59_SplitJoin59_AnonFilter_a0_9188_9434_9517_9533_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 2, __iter_init_4_++)
		init_buffer_complex(&SplitJoin67_SplitJoin53_SplitJoin53_AnonFilter_a0_9180_9430_9515_9531_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 2, __iter_init_5_++)
		init_buffer_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9162_9388_9507_9526_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 2, __iter_init_6_++)
		init_buffer_complex(&SplitJoin39_SplitJoin29_SplitJoin29_split2_9145_9410_9476_9547_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 4, __iter_init_7_++)
		init_buffer_complex(&SplitJoin56_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child1_9478_9543_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_complex(&SplitJoin81_SplitJoin67_SplitJoin67_AnonFilter_a0_9198_9439_9519_9536_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 2, __iter_init_9_++)
		init_buffer_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_9143_9396_9474_9546_split[__iter_init_9_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_9320butterfly_9225);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9341WEIGHTED_ROUND_ROBIN_Splitter_9484);
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_complex(&SplitJoin77_SplitJoin63_SplitJoin63_AnonFilter_a0_9194_9437_9473_9534_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 2, __iter_init_11_++)
		init_buffer_complex(&SplitJoin89_SplitJoin75_SplitJoin75_AnonFilter_a0_9210_9445_9521_9538_split[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 2, __iter_init_12_++)
		init_buffer_complex(&SplitJoin39_SplitJoin29_SplitJoin29_split2_9145_9410_9476_9547_join[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_Hier_9510_9541_join[__iter_init_13_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9383WEIGHTED_ROUND_ROBIN_Splitter_9501);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9495WEIGHTED_ROUND_ROBIN_Splitter_9382);
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_complex(&SplitJoin45_SplitJoin33_SplitJoin33_split2_9147_9413_9477_9549_split[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 4, __iter_init_15_++)
		init_buffer_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_9156_9399_9480_9552_split[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_complex(&SplitJoin91_SplitJoin77_SplitJoin77_AnonFilter_a0_9212_9446_9522_9539_split[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_complex(&SplitJoin73_SplitJoin59_SplitJoin59_AnonFilter_a0_9188_9434_9517_9533_join[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 2, __iter_init_18_++)
		init_buffer_complex(&SplitJoin91_SplitJoin77_SplitJoin77_AnonFilter_a0_9212_9446_9522_9539_join[__iter_init_18_]);
	ENDFOR
	FOR(int, __iter_init_19_, 0, <, 2, __iter_init_19_++)
		init_buffer_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9166_9390_9508_9528_split[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 2, __iter_init_20_++)
		init_buffer_complex(&SplitJoin79_SplitJoin65_SplitJoin65_AnonFilter_a0_9196_9438_9518_9535_join[__iter_init_20_]);
	ENDFOR
	init_buffer_complex(&butterfly_9224Post_CollapsedDataParallel_2_9318);
	init_buffer_complex(&Pre_CollapsedDataParallel_1_9326butterfly_9227);
	FOR(int, __iter_init_21_, 0, <, 2, __iter_init_21_++)
		init_buffer_complex(&SplitJoin67_SplitJoin53_SplitJoin53_AnonFilter_a0_9180_9430_9515_9531_join[__iter_init_21_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_9332butterfly_9229);
	FOR(int, __iter_init_22_, 0, <, 2, __iter_init_22_++)
		init_buffer_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_9143_9396_9474_9546_join[__iter_init_22_]);
	ENDFOR
	init_buffer_complex(&butterfly_9229Post_CollapsedDataParallel_2_9333);
	FOR(int, __iter_init_23_, 0, <, 2, __iter_init_23_++)
		init_buffer_complex(&SplitJoin0_source_Fiss_9506_9525_join[__iter_init_23_]);
	ENDFOR
	FOR(int, __iter_init_24_, 0, <, 2, __iter_init_24_++)
		init_buffer_complex(&SplitJoin69_SplitJoin55_SplitJoin55_AnonFilter_a0_9182_9431_9516_9532_join[__iter_init_24_]);
	ENDFOR
	FOR(int, __iter_init_25_, 0, <, 2, __iter_init_25_++)
		init_buffer_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_9162_9388_9507_9526_split[__iter_init_25_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9489WEIGHTED_ROUND_ROBIN_Splitter_9490);
	FOR(int, __iter_init_26_, 0, <, 2, __iter_init_26_++)
		init_buffer_complex(&SplitJoin85_SplitJoin71_SplitJoin71_AnonFilter_a0_9204_9442_9520_9537_split[__iter_init_26_]);
	ENDFOR
	FOR(int, __iter_init_27_, 0, <, 2, __iter_init_27_++)
		init_buffer_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_Hier_9510_9541_split[__iter_init_27_]);
	ENDFOR
	FOR(int, __iter_init_28_, 0, <, 2, __iter_init_28_++)
		init_buffer_complex(&SplitJoin77_SplitJoin63_SplitJoin63_AnonFilter_a0_9194_9437_9473_9534_join[__iter_init_28_]);
	ENDFOR
	FOR(int, __iter_init_29_, 0, <, 4, __iter_init_29_++)
		init_buffer_complex(&SplitJoin56_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child1_9478_9543_join[__iter_init_29_]);
	ENDFOR
	FOR(int, __iter_init_30_, 0, <, 2, __iter_init_30_++)
		init_buffer_complex(&SplitJoin43_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child1_9483_9548_join[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 2, __iter_init_31_++)
		init_buffer_complex(&SplitJoin49_SplitJoin37_SplitJoin37_split2_9149_9416_9479_9550_join[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 2, __iter_init_32_++)
		init_buffer_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child0_9481_9545_split[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 4, __iter_init_33_++)
		init_buffer_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child0_9475_9542_split[__iter_init_33_]);
	ENDFOR
	FOR(int, __iter_init_34_, 0, <, 2, __iter_init_34_++)
		init_buffer_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_9154_9398_9512_9551_split[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 4, __iter_init_35_++)
		init_buffer_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_9156_9399_9480_9552_join[__iter_init_35_]);
	ENDFOR
	FOR(int, __iter_init_36_, 0, <, 2, __iter_init_36_++)
		init_buffer_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_9168_9391_9509_9529_join[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 4, __iter_init_37_++)
		init_buffer_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_9120_9393_Hier_child0_9475_9542_join[__iter_init_37_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_9335butterfly_9230);
	FOR(int, __iter_init_38_, 0, <, 2, __iter_init_38_++)
		init_buffer_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_9168_9391_9509_9529_split[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 2, __iter_init_39_++)
		init_buffer_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_Hier_9511_9544_split[__iter_init_39_]);
	ENDFOR
	FOR(int, __iter_init_40_, 0, <, 2, __iter_init_40_++)
		init_buffer_complex(&SplitJoin45_SplitJoin33_SplitJoin33_split2_9147_9413_9477_9549_join[__iter_init_40_]);
	ENDFOR
	FOR(int, __iter_init_41_, 0, <, 2, __iter_init_41_++)
		init_buffer_complex(&SplitJoin95_SplitJoin81_SplitJoin81_AnonFilter_a0_9218_9449_9523_9540_split[__iter_init_41_]);
	ENDFOR
	init_buffer_complex(&butterfly_9227Post_CollapsedDataParallel_2_9327);
	FOR(int, __iter_init_42_, 0, <, 2, __iter_init_42_++)
		init_buffer_complex(&SplitJoin79_SplitJoin65_SplitJoin65_AnonFilter_a0_9196_9438_9518_9535_split[__iter_init_42_]);
	ENDFOR
	FOR(int, __iter_init_43_, 0, <, 2, __iter_init_43_++)
		init_buffer_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_child0_9481_9545_join[__iter_init_43_]);
	ENDFOR
	init_buffer_complex(&butterfly_9230Post_CollapsedDataParallel_2_9336);
	FOR(int, __iter_init_44_, 0, <, 2, __iter_init_44_++)
		init_buffer_complex(&SplitJoin95_SplitJoin81_SplitJoin81_AnonFilter_a0_9218_9449_9523_9540_join[__iter_init_44_]);
	ENDFOR
	FOR(int, __iter_init_45_, 0, <, 3, __iter_init_45_++)
		init_buffer_float(&SplitJoin24_magnitude_Fiss_9513_9554_join[__iter_init_45_]);
	ENDFOR
	init_buffer_complex(&butterfly_9226Post_CollapsedDataParallel_2_9324);
	init_buffer_complex(&butterfly_9225Post_CollapsedDataParallel_2_9321);
	FOR(int, __iter_init_46_, 0, <, 2, __iter_init_46_++)
		init_buffer_complex(&SplitJoin69_SplitJoin55_SplitJoin55_AnonFilter_a0_9182_9431_9516_9532_split[__iter_init_46_]);
	ENDFOR
	init_buffer_complex(&butterfly_9228Post_CollapsedDataParallel_2_9330);
	init_buffer_complex(&butterfly_9231Post_CollapsedDataParallel_2_9339);
	FOR(int, __iter_init_47_, 0, <, 2, __iter_init_47_++)
		init_buffer_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_9141_9395_Hier_Hier_9511_9544_join[__iter_init_47_]);
	ENDFOR
	FOR(int, __iter_init_48_, 0, <, 2, __iter_init_48_++)
		init_buffer_complex(&SplitJoin49_SplitJoin37_SplitJoin37_split2_9149_9416_9479_9550_split[__iter_init_48_]);
	ENDFOR
	FOR(int, __iter_init_49_, 0, <, 3, __iter_init_49_++)
		init_buffer_complex(&SplitJoin24_magnitude_Fiss_9513_9554_split[__iter_init_49_]);
	ENDFOR
	FOR(int, __iter_init_50_, 0, <, 2, __iter_init_50_++)
		init_buffer_complex(&SplitJoin0_source_Fiss_9506_9525_split[__iter_init_50_]);
	ENDFOR
	FOR(int, __iter_init_51_, 0, <, 2, __iter_init_51_++)
		init_buffer_complex(&SplitJoin85_SplitJoin71_SplitJoin71_AnonFilter_a0_9204_9442_9520_9537_join[__iter_init_51_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_9317butterfly_9224);
	FOR(int, __iter_init_52_, 0, <, 4, __iter_init_52_++)
		init_buffer_complex(&SplitJoin32_SplitJoin22_SplitJoin22_split2_9158_9404_9482_9553_join[__iter_init_52_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_9498WEIGHTED_ROUND_ROBIN_Splitter_9340);
	FOR(int, __iter_init_53_, 0, <, 2, __iter_init_53_++)
		init_buffer_complex(&SplitJoin81_SplitJoin67_SplitJoin67_AnonFilter_a0_9198_9439_9519_9536_join[__iter_init_53_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_9323butterfly_9226);
	FOR(int, __iter_init_54_, 0, <, 2, __iter_init_54_++)
		init_buffer_complex(&SplitJoin89_SplitJoin75_SplitJoin75_AnonFilter_a0_9210_9445_9521_9538_join[__iter_init_54_]);
	ENDFOR
	FOR(int, __iter_init_55_, 0, <, 2, __iter_init_55_++)
		init_buffer_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9164_9389_9472_9527_split[__iter_init_55_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_9502sink_9251);
	FOR(int, __iter_init_56_, 0, <, 2, __iter_init_56_++)
		init_buffer_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_9164_9389_9472_9527_join[__iter_init_56_]);
	ENDFOR
	FOR(int, __iter_init_57_, 0, <, 4, __iter_init_57_++)
		init_buffer_complex(&SplitJoin32_SplitJoin22_SplitJoin22_split2_9158_9404_9482_9553_split[__iter_init_57_]);
	ENDFOR
	FOR(int, __iter_init_58_, 0, <, 2, __iter_init_58_++)
		init_buffer_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_9166_9390_9508_9528_join[__iter_init_58_]);
	ENDFOR
	FOR(int, __iter_init_59_, 0, <, 2, __iter_init_59_++)
		init_buffer_complex(&SplitJoin63_SplitJoin49_SplitJoin49_AnonFilter_a0_9174_9427_9514_9530_split[__iter_init_59_]);
	ENDFOR
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_9497();
			source_9499();
			source_9500();
		WEIGHTED_ROUND_ROBIN_Joiner_9498();
		WEIGHTED_ROUND_ROBIN_Splitter_9340();
			WEIGHTED_ROUND_ROBIN_Splitter_9342();
				WEIGHTED_ROUND_ROBIN_Splitter_9344();
					WEIGHTED_ROUND_ROBIN_Splitter_9346();
						Identity_9170();
						Identity_9172();
					WEIGHTED_ROUND_ROBIN_Joiner_9347();
					WEIGHTED_ROUND_ROBIN_Splitter_9348();
						Identity_9176();
						Identity_9178();
					WEIGHTED_ROUND_ROBIN_Joiner_9349();
				WEIGHTED_ROUND_ROBIN_Joiner_9345();
				WEIGHTED_ROUND_ROBIN_Splitter_9350();
					WEIGHTED_ROUND_ROBIN_Splitter_9352();
						Identity_9184();
						Identity_9186();
					WEIGHTED_ROUND_ROBIN_Joiner_9353();
					WEIGHTED_ROUND_ROBIN_Splitter_9354();
						Identity_9190();
						Identity_9192();
					WEIGHTED_ROUND_ROBIN_Joiner_9355();
				WEIGHTED_ROUND_ROBIN_Joiner_9351();
			WEIGHTED_ROUND_ROBIN_Joiner_9343();
			WEIGHTED_ROUND_ROBIN_Splitter_9356();
				WEIGHTED_ROUND_ROBIN_Splitter_9358();
					WEIGHTED_ROUND_ROBIN_Splitter_9360();
						Identity_9200();
						Identity_9202();
					WEIGHTED_ROUND_ROBIN_Joiner_9361();
					WEIGHTED_ROUND_ROBIN_Splitter_9362();
						Identity_9206();
						Identity_9208();
					WEIGHTED_ROUND_ROBIN_Joiner_9363();
				WEIGHTED_ROUND_ROBIN_Joiner_9359();
				WEIGHTED_ROUND_ROBIN_Splitter_9364();
					WEIGHTED_ROUND_ROBIN_Splitter_9366();
						Identity_9214();
						Identity_9216();
					WEIGHTED_ROUND_ROBIN_Joiner_9367();
					WEIGHTED_ROUND_ROBIN_Splitter_9368();
						Identity_9220();
						Identity_9222();
					WEIGHTED_ROUND_ROBIN_Joiner_9369();
				WEIGHTED_ROUND_ROBIN_Joiner_9365();
			WEIGHTED_ROUND_ROBIN_Joiner_9357();
		WEIGHTED_ROUND_ROBIN_Joiner_9341();
		WEIGHTED_ROUND_ROBIN_Splitter_9484();
			WEIGHTED_ROUND_ROBIN_Splitter_9485();
				Pre_CollapsedDataParallel_1_9317();
				butterfly_9224();
				Post_CollapsedDataParallel_2_9318();
				Pre_CollapsedDataParallel_1_9320();
				butterfly_9225();
				Post_CollapsedDataParallel_2_9321();
				Pre_CollapsedDataParallel_1_9323();
				butterfly_9226();
				Post_CollapsedDataParallel_2_9324();
				Pre_CollapsedDataParallel_1_9326();
				butterfly_9227();
				Post_CollapsedDataParallel_2_9327();
			WEIGHTED_ROUND_ROBIN_Joiner_9486();
			WEIGHTED_ROUND_ROBIN_Splitter_9487();
				Pre_CollapsedDataParallel_1_9329();
				butterfly_9228();
				Post_CollapsedDataParallel_2_9330();
				Pre_CollapsedDataParallel_1_9332();
				butterfly_9229();
				Post_CollapsedDataParallel_2_9333();
				Pre_CollapsedDataParallel_1_9335();
				butterfly_9230();
				Post_CollapsedDataParallel_2_9336();
				Pre_CollapsedDataParallel_1_9338();
				butterfly_9231();
				Post_CollapsedDataParallel_2_9339();
			WEIGHTED_ROUND_ROBIN_Joiner_9488();
		WEIGHTED_ROUND_ROBIN_Joiner_9489();
		WEIGHTED_ROUND_ROBIN_Splitter_9490();
			WEIGHTED_ROUND_ROBIN_Splitter_9491();
				WEIGHTED_ROUND_ROBIN_Splitter_9374();
					butterfly_9233();
					butterfly_9234();
				WEIGHTED_ROUND_ROBIN_Joiner_9375();
				WEIGHTED_ROUND_ROBIN_Splitter_9376();
					butterfly_9235();
					butterfly_9236();
				WEIGHTED_ROUND_ROBIN_Joiner_9377();
			WEIGHTED_ROUND_ROBIN_Joiner_9492();
			WEIGHTED_ROUND_ROBIN_Splitter_9493();
				WEIGHTED_ROUND_ROBIN_Splitter_9378();
					butterfly_9237();
					butterfly_9238();
				WEIGHTED_ROUND_ROBIN_Joiner_9379();
				WEIGHTED_ROUND_ROBIN_Splitter_9380();
					butterfly_9239();
					butterfly_9240();
				WEIGHTED_ROUND_ROBIN_Joiner_9381();
			WEIGHTED_ROUND_ROBIN_Joiner_9494();
		WEIGHTED_ROUND_ROBIN_Joiner_9495();
		WEIGHTED_ROUND_ROBIN_Splitter_9382();
			WEIGHTED_ROUND_ROBIN_Splitter_9384();
				butterfly_9242();
				butterfly_9243();
				butterfly_9244();
				butterfly_9245();
			WEIGHTED_ROUND_ROBIN_Joiner_9385();
			WEIGHTED_ROUND_ROBIN_Splitter_9386();
				butterfly_9246();
				butterfly_9247();
				butterfly_9248();
				butterfly_9249();
			WEIGHTED_ROUND_ROBIN_Joiner_9387();
		WEIGHTED_ROUND_ROBIN_Joiner_9383();
		WEIGHTED_ROUND_ROBIN_Splitter_9501();
			magnitude_9503();
			magnitude_9504();
			magnitude_9505();
		WEIGHTED_ROUND_ROBIN_Joiner_9502();
		sink_9251();
	ENDFOR
	return EXIT_SUCCESS;
}
