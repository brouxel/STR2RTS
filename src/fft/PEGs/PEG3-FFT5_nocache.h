#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=96 on the compile command line
#else
#if BUF_SIZEMAX < 96
#error BUF_SIZEMAX too small, it must be at least 96
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif


void WEIGHTED_ROUND_ROBIN_Splitter_9497();
void source_9499();
void source_9500();
void WEIGHTED_ROUND_ROBIN_Joiner_9498();
void WEIGHTED_ROUND_ROBIN_Splitter_9340();
void WEIGHTED_ROUND_ROBIN_Splitter_9342();
void WEIGHTED_ROUND_ROBIN_Splitter_9344();
void WEIGHTED_ROUND_ROBIN_Splitter_9346();
void Identity_9170();
void Identity_9172();
void WEIGHTED_ROUND_ROBIN_Joiner_9347();
void WEIGHTED_ROUND_ROBIN_Splitter_9348();
void Identity_9176();
void Identity_9178();
void WEIGHTED_ROUND_ROBIN_Joiner_9349();
void WEIGHTED_ROUND_ROBIN_Joiner_9345();
void WEIGHTED_ROUND_ROBIN_Splitter_9350();
void WEIGHTED_ROUND_ROBIN_Splitter_9352();
void Identity_9184();
void Identity_9186();
void WEIGHTED_ROUND_ROBIN_Joiner_9353();
void WEIGHTED_ROUND_ROBIN_Splitter_9354();
void Identity_9190();
void Identity_9192();
void WEIGHTED_ROUND_ROBIN_Joiner_9355();
void WEIGHTED_ROUND_ROBIN_Joiner_9351();
void WEIGHTED_ROUND_ROBIN_Joiner_9343();
void WEIGHTED_ROUND_ROBIN_Splitter_9356();
void WEIGHTED_ROUND_ROBIN_Splitter_9358();
void WEIGHTED_ROUND_ROBIN_Splitter_9360();
void Identity_9200();
void Identity_9202();
void WEIGHTED_ROUND_ROBIN_Joiner_9361();
void WEIGHTED_ROUND_ROBIN_Splitter_9362();
void Identity_9206();
void Identity_9208();
void WEIGHTED_ROUND_ROBIN_Joiner_9363();
void WEIGHTED_ROUND_ROBIN_Joiner_9359();
void WEIGHTED_ROUND_ROBIN_Splitter_9364();
void WEIGHTED_ROUND_ROBIN_Splitter_9366();
void Identity_9214();
void Identity_9216();
void WEIGHTED_ROUND_ROBIN_Joiner_9367();
void WEIGHTED_ROUND_ROBIN_Splitter_9368();
void Identity_9220();
void Identity_9222();
void WEIGHTED_ROUND_ROBIN_Joiner_9369();
void WEIGHTED_ROUND_ROBIN_Joiner_9365();
void WEIGHTED_ROUND_ROBIN_Joiner_9357();
void WEIGHTED_ROUND_ROBIN_Joiner_9341();
void WEIGHTED_ROUND_ROBIN_Splitter_9484();
void WEIGHTED_ROUND_ROBIN_Splitter_9485();
void Pre_CollapsedDataParallel_1_9317();
void butterfly_9224();
void Post_CollapsedDataParallel_2_9318();
void Pre_CollapsedDataParallel_1_9320();
void butterfly_9225();
void Post_CollapsedDataParallel_2_9321();
void Pre_CollapsedDataParallel_1_9323();
void butterfly_9226();
void Post_CollapsedDataParallel_2_9324();
void Pre_CollapsedDataParallel_1_9326();
void butterfly_9227();
void Post_CollapsedDataParallel_2_9327();
void WEIGHTED_ROUND_ROBIN_Joiner_9486();
void WEIGHTED_ROUND_ROBIN_Splitter_9487();
void Pre_CollapsedDataParallel_1_9329();
void butterfly_9228();
void Post_CollapsedDataParallel_2_9330();
void Pre_CollapsedDataParallel_1_9332();
void butterfly_9229();
void Post_CollapsedDataParallel_2_9333();
void Pre_CollapsedDataParallel_1_9335();
void butterfly_9230();
void Post_CollapsedDataParallel_2_9336();
void Pre_CollapsedDataParallel_1_9338();
void butterfly_9231();
void Post_CollapsedDataParallel_2_9339();
void WEIGHTED_ROUND_ROBIN_Joiner_9488();
void WEIGHTED_ROUND_ROBIN_Joiner_9489();
void WEIGHTED_ROUND_ROBIN_Splitter_9490();
void WEIGHTED_ROUND_ROBIN_Splitter_9491();
void WEIGHTED_ROUND_ROBIN_Splitter_9374();
void butterfly_9233();
void butterfly_9234();
void WEIGHTED_ROUND_ROBIN_Joiner_9375();
void WEIGHTED_ROUND_ROBIN_Splitter_9376();
void butterfly_9235();
void butterfly_9236();
void WEIGHTED_ROUND_ROBIN_Joiner_9377();
void WEIGHTED_ROUND_ROBIN_Joiner_9492();
void WEIGHTED_ROUND_ROBIN_Splitter_9493();
void WEIGHTED_ROUND_ROBIN_Splitter_9378();
void butterfly_9237();
void butterfly_9238();
void WEIGHTED_ROUND_ROBIN_Joiner_9379();
void WEIGHTED_ROUND_ROBIN_Splitter_9380();
void butterfly_9239();
void butterfly_9240();
void WEIGHTED_ROUND_ROBIN_Joiner_9381();
void WEIGHTED_ROUND_ROBIN_Joiner_9494();
void WEIGHTED_ROUND_ROBIN_Joiner_9495();
void WEIGHTED_ROUND_ROBIN_Splitter_9382();
void WEIGHTED_ROUND_ROBIN_Splitter_9384();
void butterfly_9242();
void butterfly_9243();
void butterfly_9244();
void butterfly_9245();
void WEIGHTED_ROUND_ROBIN_Joiner_9385();
void WEIGHTED_ROUND_ROBIN_Splitter_9386();
void butterfly_9246();
void butterfly_9247();
void butterfly_9248();
void butterfly_9249();
void WEIGHTED_ROUND_ROBIN_Joiner_9387();
void WEIGHTED_ROUND_ROBIN_Joiner_9383();
void WEIGHTED_ROUND_ROBIN_Splitter_9501();
void magnitude_9503();
void magnitude_9504();
void magnitude_9505();
void WEIGHTED_ROUND_ROBIN_Joiner_9502();
void sink_9251();

#ifdef __cplusplus
}
#endif
#endif
