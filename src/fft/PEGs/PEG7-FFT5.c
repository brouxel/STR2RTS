#include "PEG7-FFT5.h"

buffer_complex_t SplitJoin89_SplitJoin71_SplitJoin71_AnonFilter_a0_6868_7106_7188_7205_join[2];
buffer_complex_t SplitJoin99_SplitJoin81_SplitJoin81_AnonFilter_a0_6882_7113_7191_7208_join[2];
buffer_complex_t SplitJoin83_SplitJoin65_SplitJoin65_AnonFilter_a0_6860_7102_7186_7203_join[2];
buffer_complex_t SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6830_7054_7176_7196_join[2];
buffer_complex_t SplitJoin60_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child1_7142_7211_split[4];
buffer_complex_t SplitJoin47_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child1_7147_7216_join[2];
buffer_complex_t SplitJoin43_SplitJoin29_SplitJoin29_split2_6809_7074_7140_7215_join[2];
buffer_complex_t SplitJoin77_SplitJoin59_SplitJoin59_AnonFilter_a0_6852_7098_7185_7201_join[2];
buffer_complex_t SplitJoin53_SplitJoin37_SplitJoin37_split2_6813_7080_7143_7218_split[2];
buffer_complex_t SplitJoin93_SplitJoin75_SplitJoin75_AnonFilter_a0_6874_7109_7189_7206_join[2];
buffer_complex_t butterfly_6888Post_CollapsedDataParallel_2_6982;
buffer_complex_t SplitJoin85_SplitJoin67_SplitJoin67_AnonFilter_a0_6862_7103_7187_7204_join[2];
buffer_complex_t SplitJoin36_SplitJoin22_SplitJoin22_split2_6822_7068_7146_7221_join[4];
buffer_complex_t SplitJoin49_SplitJoin33_SplitJoin33_split2_6811_7077_7141_7217_split[2];
buffer_complex_t SplitJoin71_SplitJoin53_SplitJoin53_AnonFilter_a0_6844_7094_7183_7199_split[2];
buffer_complex_t butterfly_6889Post_CollapsedDataParallel_2_6985;
buffer_complex_t SplitJoin49_SplitJoin33_SplitJoin33_split2_6811_7077_7141_7217_join[2];
buffer_complex_t SplitJoin22_SplitJoin16_SplitJoin16_split2_6820_7063_7144_7220_join[4];
buffer_complex_t SplitJoin14_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_Hier_7179_7212_split[2];
buffer_complex_t SplitJoin16_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child0_7145_7213_split[2];
buffer_complex_t SplitJoin67_SplitJoin49_SplitJoin49_AnonFilter_a0_6838_7091_7182_7198_split[2];
buffer_complex_t butterfly_6893Post_CollapsedDataParallel_2_6997;
buffer_complex_t SplitJoin20_SplitJoin14_SplitJoin14_split1_6818_7062_7180_7219_join[2];
buffer_complex_t butterfly_6891Post_CollapsedDataParallel_2_6991;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_7047WEIGHTED_ROUND_ROBIN_Splitter_7165;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_7159WEIGHTED_ROUND_ROBIN_Splitter_7046;
buffer_complex_t SplitJoin18_SplitJoin12_SplitJoin12_split2_6807_7060_7138_7214_split[2];
buffer_complex_t SplitJoin95_SplitJoin77_SplitJoin77_AnonFilter_a0_6876_7110_7190_7207_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_6987butterfly_6890;
buffer_complex_t butterfly_6895Post_CollapsedDataParallel_2_7003;
buffer_complex_t Pre_CollapsedDataParallel_1_6993butterfly_6892;
buffer_complex_t SplitJoin0_source_Fiss_7174_7193_split[2];
buffer_complex_t SplitJoin60_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child1_7142_7211_join[4];
buffer_complex_t SplitJoin43_SplitJoin29_SplitJoin29_split2_6809_7074_7140_7215_split[2];
buffer_complex_t SplitJoin12_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child0_7139_7210_split[4];
buffer_complex_t SplitJoin16_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child0_7145_7213_join[2];
buffer_complex_t SplitJoin0_source_Fiss_7174_7193_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_6984butterfly_6889;
buffer_complex_t SplitJoin93_SplitJoin75_SplitJoin75_AnonFilter_a0_6874_7109_7189_7206_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_7162WEIGHTED_ROUND_ROBIN_Splitter_7004;
buffer_complex_t SplitJoin85_SplitJoin67_SplitJoin67_AnonFilter_a0_6862_7103_7187_7204_split[2];
buffer_complex_t Pre_CollapsedDataParallel_1_7002butterfly_6895;
buffer_complex_t SplitJoin67_SplitJoin49_SplitJoin49_AnonFilter_a0_6838_7091_7182_7198_join[2];
buffer_complex_t SplitJoin81_SplitJoin63_SplitJoin63_AnonFilter_a0_6858_7101_7137_7202_split[2];
buffer_complex_t Pre_CollapsedDataParallel_1_6999butterfly_6894;
buffer_complex_t SplitJoin73_SplitJoin55_SplitJoin55_AnonFilter_a0_6846_7095_7184_7200_join[2];
buffer_complex_t SplitJoin99_SplitJoin81_SplitJoin81_AnonFilter_a0_6882_7113_7191_7208_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7166sink_6915;
buffer_complex_t SplitJoin89_SplitJoin71_SplitJoin71_AnonFilter_a0_6868_7106_7188_7205_split[2];
buffer_complex_t SplitJoin71_SplitJoin53_SplitJoin53_AnonFilter_a0_6844_7094_7183_7199_join[2];
buffer_complex_t SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6826_7052_7175_7194_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_6990butterfly_6891;
buffer_complex_t SplitJoin83_SplitJoin65_SplitJoin65_AnonFilter_a0_6860_7102_7186_7203_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_7153WEIGHTED_ROUND_ROBIN_Splitter_7154;
buffer_complex_t SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6826_7052_7175_7194_split[2];
buffer_complex_t SplitJoin10_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_Hier_7178_7209_split[2];
buffer_complex_t SplitJoin10_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_Hier_7178_7209_join[2];
buffer_complex_t SplitJoin81_SplitJoin63_SplitJoin63_AnonFilter_a0_6858_7101_7137_7202_join[2];
buffer_complex_t SplitJoin20_SplitJoin14_SplitJoin14_split1_6818_7062_7180_7219_split[2];
buffer_complex_t SplitJoin22_SplitJoin16_SplitJoin16_split2_6820_7063_7144_7220_split[4];
buffer_complex_t butterfly_6892Post_CollapsedDataParallel_2_6994;
buffer_complex_t SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_6832_7055_7177_7197_join[2];
buffer_complex_t SplitJoin18_SplitJoin12_SplitJoin12_split2_6807_7060_7138_7214_join[2];
buffer_complex_t SplitJoin36_SplitJoin22_SplitJoin22_split2_6822_7068_7146_7221_split[4];
buffer_float_t SplitJoin24_magnitude_Fiss_7181_7222_join[7];
buffer_complex_t SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_6832_7055_7177_7197_split[2];
buffer_complex_t Pre_CollapsedDataParallel_1_6981butterfly_6888;
buffer_complex_t SplitJoin47_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child1_7147_7216_split[2];
buffer_complex_t butterfly_6894Post_CollapsedDataParallel_2_7000;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_7005WEIGHTED_ROUND_ROBIN_Splitter_7148;
buffer_complex_t SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6828_7053_7136_7195_join[2];
buffer_complex_t SplitJoin77_SplitJoin59_SplitJoin59_AnonFilter_a0_6852_7098_7185_7201_split[2];
buffer_complex_t SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6828_7053_7136_7195_split[2];
buffer_complex_t SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6830_7054_7176_7196_split[2];
buffer_complex_t SplitJoin95_SplitJoin77_SplitJoin77_AnonFilter_a0_6876_7110_7190_7207_split[2];
buffer_complex_t Pre_CollapsedDataParallel_1_6996butterfly_6893;
buffer_complex_t SplitJoin53_SplitJoin37_SplitJoin37_split2_6813_7080_7143_7218_join[2];
buffer_complex_t SplitJoin14_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_Hier_7179_7212_join[2];
buffer_complex_t SplitJoin24_magnitude_Fiss_7181_7222_split[7];
buffer_complex_t SplitJoin73_SplitJoin55_SplitJoin55_AnonFilter_a0_6846_7095_7184_7200_split[2];
buffer_complex_t SplitJoin12_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child0_7139_7210_join[4];
buffer_complex_t butterfly_6890Post_CollapsedDataParallel_2_6988;



void source(buffer_void_t *chanin, buffer_complex_t *chanout) {
		complex_t t;
		t.imag = 0.0 ; 
		t.real = 0.9501 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.2311 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.6068 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.486 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.8913 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.7621 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.4565 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.0185 ; 
		push_complex(&(*chanout), t) ; 
	}


void source_7163() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		source(&(SplitJoin0_source_Fiss_7174_7193_split[0]), &(SplitJoin0_source_Fiss_7174_7193_join[0]));
	ENDFOR
}

void source_7164() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		source(&(SplitJoin0_source_Fiss_7174_7193_split[1]), &(SplitJoin0_source_Fiss_7174_7193_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7161() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_7162() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7162WEIGHTED_ROUND_ROBIN_Splitter_7004, pop_complex(&SplitJoin0_source_Fiss_7174_7193_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7162WEIGHTED_ROUND_ROBIN_Splitter_7004, pop_complex(&SplitJoin0_source_Fiss_7174_7193_join[1]));
		ENDFOR
	ENDFOR
}}

void Identity(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t __tmp1854 = pop_complex(&(*chanin));
		push_complex(&(*chanout), __tmp1854) ; 
	}


void Identity_6834() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Identity(&(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_6832_7055_7177_7197_split[0]), &(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_6832_7055_7177_7197_join[0]));
	ENDFOR
}

void Identity_6836() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Identity(&(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_6832_7055_7177_7197_split[1]), &(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_6832_7055_7177_7197_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7010() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_6832_7055_7177_7197_split[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6830_7054_7176_7196_split[0]));
		push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_6832_7055_7177_7197_split[1], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6830_7054_7176_7196_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7011() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6830_7054_7176_7196_join[0], pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_6832_7055_7177_7197_join[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6830_7054_7176_7196_join[0], pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_6832_7055_7177_7197_join[1]));
	ENDFOR
}}

void Identity_6840() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Identity(&(SplitJoin67_SplitJoin49_SplitJoin49_AnonFilter_a0_6838_7091_7182_7198_split[0]), &(SplitJoin67_SplitJoin49_SplitJoin49_AnonFilter_a0_6838_7091_7182_7198_join[0]));
	ENDFOR
}

void Identity_6842() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Identity(&(SplitJoin67_SplitJoin49_SplitJoin49_AnonFilter_a0_6838_7091_7182_7198_split[1]), &(SplitJoin67_SplitJoin49_SplitJoin49_AnonFilter_a0_6838_7091_7182_7198_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7012() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin67_SplitJoin49_SplitJoin49_AnonFilter_a0_6838_7091_7182_7198_split[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6830_7054_7176_7196_split[1]));
		push_complex(&SplitJoin67_SplitJoin49_SplitJoin49_AnonFilter_a0_6838_7091_7182_7198_split[1], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6830_7054_7176_7196_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7013() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6830_7054_7176_7196_join[1], pop_complex(&SplitJoin67_SplitJoin49_SplitJoin49_AnonFilter_a0_6838_7091_7182_7198_join[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6830_7054_7176_7196_join[1], pop_complex(&SplitJoin67_SplitJoin49_SplitJoin49_AnonFilter_a0_6838_7091_7182_7198_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_7008() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6830_7054_7176_7196_split[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6828_7053_7136_7195_split[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6830_7054_7176_7196_split[1], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6828_7053_7136_7195_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7009() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6828_7053_7136_7195_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6830_7054_7176_7196_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6828_7053_7136_7195_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6830_7054_7176_7196_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6828_7053_7136_7195_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6830_7054_7176_7196_join[1]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6828_7053_7136_7195_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6830_7054_7176_7196_join[1]));
	ENDFOR
}}

void Identity_6848() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Identity(&(SplitJoin73_SplitJoin55_SplitJoin55_AnonFilter_a0_6846_7095_7184_7200_split[0]), &(SplitJoin73_SplitJoin55_SplitJoin55_AnonFilter_a0_6846_7095_7184_7200_join[0]));
	ENDFOR
}

void Identity_6850() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Identity(&(SplitJoin73_SplitJoin55_SplitJoin55_AnonFilter_a0_6846_7095_7184_7200_split[1]), &(SplitJoin73_SplitJoin55_SplitJoin55_AnonFilter_a0_6846_7095_7184_7200_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7016() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin73_SplitJoin55_SplitJoin55_AnonFilter_a0_6846_7095_7184_7200_split[0], pop_complex(&SplitJoin71_SplitJoin53_SplitJoin53_AnonFilter_a0_6844_7094_7183_7199_split[0]));
		push_complex(&SplitJoin73_SplitJoin55_SplitJoin55_AnonFilter_a0_6846_7095_7184_7200_split[1], pop_complex(&SplitJoin71_SplitJoin53_SplitJoin53_AnonFilter_a0_6844_7094_7183_7199_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7017() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin71_SplitJoin53_SplitJoin53_AnonFilter_a0_6844_7094_7183_7199_join[0], pop_complex(&SplitJoin73_SplitJoin55_SplitJoin55_AnonFilter_a0_6846_7095_7184_7200_join[0]));
		push_complex(&SplitJoin71_SplitJoin53_SplitJoin53_AnonFilter_a0_6844_7094_7183_7199_join[0], pop_complex(&SplitJoin73_SplitJoin55_SplitJoin55_AnonFilter_a0_6846_7095_7184_7200_join[1]));
	ENDFOR
}}

void Identity_6854() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Identity(&(SplitJoin77_SplitJoin59_SplitJoin59_AnonFilter_a0_6852_7098_7185_7201_split[0]), &(SplitJoin77_SplitJoin59_SplitJoin59_AnonFilter_a0_6852_7098_7185_7201_join[0]));
	ENDFOR
}

void Identity_6856() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Identity(&(SplitJoin77_SplitJoin59_SplitJoin59_AnonFilter_a0_6852_7098_7185_7201_split[1]), &(SplitJoin77_SplitJoin59_SplitJoin59_AnonFilter_a0_6852_7098_7185_7201_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7018() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin77_SplitJoin59_SplitJoin59_AnonFilter_a0_6852_7098_7185_7201_split[0], pop_complex(&SplitJoin71_SplitJoin53_SplitJoin53_AnonFilter_a0_6844_7094_7183_7199_split[1]));
		push_complex(&SplitJoin77_SplitJoin59_SplitJoin59_AnonFilter_a0_6852_7098_7185_7201_split[1], pop_complex(&SplitJoin71_SplitJoin53_SplitJoin53_AnonFilter_a0_6844_7094_7183_7199_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7019() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin71_SplitJoin53_SplitJoin53_AnonFilter_a0_6844_7094_7183_7199_join[1], pop_complex(&SplitJoin77_SplitJoin59_SplitJoin59_AnonFilter_a0_6852_7098_7185_7201_join[0]));
		push_complex(&SplitJoin71_SplitJoin53_SplitJoin53_AnonFilter_a0_6844_7094_7183_7199_join[1], pop_complex(&SplitJoin77_SplitJoin59_SplitJoin59_AnonFilter_a0_6852_7098_7185_7201_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_7014() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		push_complex(&SplitJoin71_SplitJoin53_SplitJoin53_AnonFilter_a0_6844_7094_7183_7199_split[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6828_7053_7136_7195_split[1]));
		push_complex(&SplitJoin71_SplitJoin53_SplitJoin53_AnonFilter_a0_6844_7094_7183_7199_split[1], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6828_7053_7136_7195_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7015() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6828_7053_7136_7195_join[1], pop_complex(&SplitJoin71_SplitJoin53_SplitJoin53_AnonFilter_a0_6844_7094_7183_7199_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6828_7053_7136_7195_join[1], pop_complex(&SplitJoin71_SplitJoin53_SplitJoin53_AnonFilter_a0_6844_7094_7183_7199_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6828_7053_7136_7195_join[1], pop_complex(&SplitJoin71_SplitJoin53_SplitJoin53_AnonFilter_a0_6844_7094_7183_7199_join[1]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6828_7053_7136_7195_join[1], pop_complex(&SplitJoin71_SplitJoin53_SplitJoin53_AnonFilter_a0_6844_7094_7183_7199_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_7006() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 28, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6828_7053_7136_7195_split[0], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6826_7052_7175_7194_split[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6828_7053_7136_7195_split[1], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6826_7052_7175_7194_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7007() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6826_7052_7175_7194_join[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6828_7053_7136_7195_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6826_7052_7175_7194_join[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6828_7053_7136_7195_join[1]));
		ENDFOR
	ENDFOR
}}

void Identity_6864() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Identity(&(SplitJoin85_SplitJoin67_SplitJoin67_AnonFilter_a0_6862_7103_7187_7204_split[0]), &(SplitJoin85_SplitJoin67_SplitJoin67_AnonFilter_a0_6862_7103_7187_7204_join[0]));
	ENDFOR
}

void Identity_6866() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Identity(&(SplitJoin85_SplitJoin67_SplitJoin67_AnonFilter_a0_6862_7103_7187_7204_split[1]), &(SplitJoin85_SplitJoin67_SplitJoin67_AnonFilter_a0_6862_7103_7187_7204_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7024() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin85_SplitJoin67_SplitJoin67_AnonFilter_a0_6862_7103_7187_7204_split[0], pop_complex(&SplitJoin83_SplitJoin65_SplitJoin65_AnonFilter_a0_6860_7102_7186_7203_split[0]));
		push_complex(&SplitJoin85_SplitJoin67_SplitJoin67_AnonFilter_a0_6862_7103_7187_7204_split[1], pop_complex(&SplitJoin83_SplitJoin65_SplitJoin65_AnonFilter_a0_6860_7102_7186_7203_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7025() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin83_SplitJoin65_SplitJoin65_AnonFilter_a0_6860_7102_7186_7203_join[0], pop_complex(&SplitJoin85_SplitJoin67_SplitJoin67_AnonFilter_a0_6862_7103_7187_7204_join[0]));
		push_complex(&SplitJoin83_SplitJoin65_SplitJoin65_AnonFilter_a0_6860_7102_7186_7203_join[0], pop_complex(&SplitJoin85_SplitJoin67_SplitJoin67_AnonFilter_a0_6862_7103_7187_7204_join[1]));
	ENDFOR
}}

void Identity_6870() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Identity(&(SplitJoin89_SplitJoin71_SplitJoin71_AnonFilter_a0_6868_7106_7188_7205_split[0]), &(SplitJoin89_SplitJoin71_SplitJoin71_AnonFilter_a0_6868_7106_7188_7205_join[0]));
	ENDFOR
}

void Identity_6872() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Identity(&(SplitJoin89_SplitJoin71_SplitJoin71_AnonFilter_a0_6868_7106_7188_7205_split[1]), &(SplitJoin89_SplitJoin71_SplitJoin71_AnonFilter_a0_6868_7106_7188_7205_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7026() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin89_SplitJoin71_SplitJoin71_AnonFilter_a0_6868_7106_7188_7205_split[0], pop_complex(&SplitJoin83_SplitJoin65_SplitJoin65_AnonFilter_a0_6860_7102_7186_7203_split[1]));
		push_complex(&SplitJoin89_SplitJoin71_SplitJoin71_AnonFilter_a0_6868_7106_7188_7205_split[1], pop_complex(&SplitJoin83_SplitJoin65_SplitJoin65_AnonFilter_a0_6860_7102_7186_7203_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7027() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin83_SplitJoin65_SplitJoin65_AnonFilter_a0_6860_7102_7186_7203_join[1], pop_complex(&SplitJoin89_SplitJoin71_SplitJoin71_AnonFilter_a0_6868_7106_7188_7205_join[0]));
		push_complex(&SplitJoin83_SplitJoin65_SplitJoin65_AnonFilter_a0_6860_7102_7186_7203_join[1], pop_complex(&SplitJoin89_SplitJoin71_SplitJoin71_AnonFilter_a0_6868_7106_7188_7205_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_7022() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		push_complex(&SplitJoin83_SplitJoin65_SplitJoin65_AnonFilter_a0_6860_7102_7186_7203_split[0], pop_complex(&SplitJoin81_SplitJoin63_SplitJoin63_AnonFilter_a0_6858_7101_7137_7202_split[0]));
		push_complex(&SplitJoin83_SplitJoin65_SplitJoin65_AnonFilter_a0_6860_7102_7186_7203_split[1], pop_complex(&SplitJoin81_SplitJoin63_SplitJoin63_AnonFilter_a0_6858_7101_7137_7202_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7023() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin81_SplitJoin63_SplitJoin63_AnonFilter_a0_6858_7101_7137_7202_join[0], pop_complex(&SplitJoin83_SplitJoin65_SplitJoin65_AnonFilter_a0_6860_7102_7186_7203_join[0]));
		push_complex(&SplitJoin81_SplitJoin63_SplitJoin63_AnonFilter_a0_6858_7101_7137_7202_join[0], pop_complex(&SplitJoin83_SplitJoin65_SplitJoin65_AnonFilter_a0_6860_7102_7186_7203_join[0]));
		push_complex(&SplitJoin81_SplitJoin63_SplitJoin63_AnonFilter_a0_6858_7101_7137_7202_join[0], pop_complex(&SplitJoin83_SplitJoin65_SplitJoin65_AnonFilter_a0_6860_7102_7186_7203_join[1]));
		push_complex(&SplitJoin81_SplitJoin63_SplitJoin63_AnonFilter_a0_6858_7101_7137_7202_join[0], pop_complex(&SplitJoin83_SplitJoin65_SplitJoin65_AnonFilter_a0_6860_7102_7186_7203_join[1]));
	ENDFOR
}}

void Identity_6878() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Identity(&(SplitJoin95_SplitJoin77_SplitJoin77_AnonFilter_a0_6876_7110_7190_7207_split[0]), &(SplitJoin95_SplitJoin77_SplitJoin77_AnonFilter_a0_6876_7110_7190_7207_join[0]));
	ENDFOR
}

void Identity_6880() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Identity(&(SplitJoin95_SplitJoin77_SplitJoin77_AnonFilter_a0_6876_7110_7190_7207_split[1]), &(SplitJoin95_SplitJoin77_SplitJoin77_AnonFilter_a0_6876_7110_7190_7207_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7030() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin95_SplitJoin77_SplitJoin77_AnonFilter_a0_6876_7110_7190_7207_split[0], pop_complex(&SplitJoin93_SplitJoin75_SplitJoin75_AnonFilter_a0_6874_7109_7189_7206_split[0]));
		push_complex(&SplitJoin95_SplitJoin77_SplitJoin77_AnonFilter_a0_6876_7110_7190_7207_split[1], pop_complex(&SplitJoin93_SplitJoin75_SplitJoin75_AnonFilter_a0_6874_7109_7189_7206_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7031() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin93_SplitJoin75_SplitJoin75_AnonFilter_a0_6874_7109_7189_7206_join[0], pop_complex(&SplitJoin95_SplitJoin77_SplitJoin77_AnonFilter_a0_6876_7110_7190_7207_join[0]));
		push_complex(&SplitJoin93_SplitJoin75_SplitJoin75_AnonFilter_a0_6874_7109_7189_7206_join[0], pop_complex(&SplitJoin95_SplitJoin77_SplitJoin77_AnonFilter_a0_6876_7110_7190_7207_join[1]));
	ENDFOR
}}

void Identity_6884() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Identity(&(SplitJoin99_SplitJoin81_SplitJoin81_AnonFilter_a0_6882_7113_7191_7208_split[0]), &(SplitJoin99_SplitJoin81_SplitJoin81_AnonFilter_a0_6882_7113_7191_7208_join[0]));
	ENDFOR
}

void Identity_6886() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Identity(&(SplitJoin99_SplitJoin81_SplitJoin81_AnonFilter_a0_6882_7113_7191_7208_split[1]), &(SplitJoin99_SplitJoin81_SplitJoin81_AnonFilter_a0_6882_7113_7191_7208_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7032() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin99_SplitJoin81_SplitJoin81_AnonFilter_a0_6882_7113_7191_7208_split[0], pop_complex(&SplitJoin93_SplitJoin75_SplitJoin75_AnonFilter_a0_6874_7109_7189_7206_split[1]));
		push_complex(&SplitJoin99_SplitJoin81_SplitJoin81_AnonFilter_a0_6882_7113_7191_7208_split[1], pop_complex(&SplitJoin93_SplitJoin75_SplitJoin75_AnonFilter_a0_6874_7109_7189_7206_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7033() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin93_SplitJoin75_SplitJoin75_AnonFilter_a0_6874_7109_7189_7206_join[1], pop_complex(&SplitJoin99_SplitJoin81_SplitJoin81_AnonFilter_a0_6882_7113_7191_7208_join[0]));
		push_complex(&SplitJoin93_SplitJoin75_SplitJoin75_AnonFilter_a0_6874_7109_7189_7206_join[1], pop_complex(&SplitJoin99_SplitJoin81_SplitJoin81_AnonFilter_a0_6882_7113_7191_7208_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_7028() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		push_complex(&SplitJoin93_SplitJoin75_SplitJoin75_AnonFilter_a0_6874_7109_7189_7206_split[0], pop_complex(&SplitJoin81_SplitJoin63_SplitJoin63_AnonFilter_a0_6858_7101_7137_7202_split[1]));
		push_complex(&SplitJoin93_SplitJoin75_SplitJoin75_AnonFilter_a0_6874_7109_7189_7206_split[1], pop_complex(&SplitJoin81_SplitJoin63_SplitJoin63_AnonFilter_a0_6858_7101_7137_7202_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7029() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		push_complex(&SplitJoin81_SplitJoin63_SplitJoin63_AnonFilter_a0_6858_7101_7137_7202_join[1], pop_complex(&SplitJoin93_SplitJoin75_SplitJoin75_AnonFilter_a0_6874_7109_7189_7206_join[0]));
		push_complex(&SplitJoin81_SplitJoin63_SplitJoin63_AnonFilter_a0_6858_7101_7137_7202_join[1], pop_complex(&SplitJoin93_SplitJoin75_SplitJoin75_AnonFilter_a0_6874_7109_7189_7206_join[0]));
		push_complex(&SplitJoin81_SplitJoin63_SplitJoin63_AnonFilter_a0_6858_7101_7137_7202_join[1], pop_complex(&SplitJoin93_SplitJoin75_SplitJoin75_AnonFilter_a0_6874_7109_7189_7206_join[1]));
		push_complex(&SplitJoin81_SplitJoin63_SplitJoin63_AnonFilter_a0_6858_7101_7137_7202_join[1], pop_complex(&SplitJoin93_SplitJoin75_SplitJoin75_AnonFilter_a0_6874_7109_7189_7206_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_7020() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 28, __iter_steady_++)
		push_complex(&SplitJoin81_SplitJoin63_SplitJoin63_AnonFilter_a0_6858_7101_7137_7202_split[0], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6826_7052_7175_7194_split[1]));
		push_complex(&SplitJoin81_SplitJoin63_SplitJoin63_AnonFilter_a0_6858_7101_7137_7202_split[1], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6826_7052_7175_7194_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7021() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6826_7052_7175_7194_join[1], pop_complex(&SplitJoin81_SplitJoin63_SplitJoin63_AnonFilter_a0_6858_7101_7137_7202_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6826_7052_7175_7194_join[1], pop_complex(&SplitJoin81_SplitJoin63_SplitJoin63_AnonFilter_a0_6858_7101_7137_7202_join[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_7004() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 56, __iter_steady_++)
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6826_7052_7175_7194_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7162WEIGHTED_ROUND_ROBIN_Splitter_7004));
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6826_7052_7175_7194_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7162WEIGHTED_ROUND_ROBIN_Splitter_7004));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7005() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7005WEIGHTED_ROUND_ROBIN_Splitter_7148, pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6826_7052_7175_7194_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7005WEIGHTED_ROUND_ROBIN_Splitter_7148, pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6826_7052_7175_7194_join[1]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_complex_t *chanin, buffer_complex_t *chanout) {
 {
 {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
			push_complex(&(*chanout), peek_complex(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
		}
		ENDFOR
	}
	}
	}
		pop_complex(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_6981() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child0_7139_7210_split[0]), &(Pre_CollapsedDataParallel_1_6981butterfly_6888));
	ENDFOR
}

void butterfly(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&(*chanin)));
		complex_t two = ((complex_t) pop_complex(&(*chanin)));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&(*chanout), __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&(*chanout), __sa2) ; 
	}


void butterfly_6888() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_6981butterfly_6888), &(butterfly_6888Post_CollapsedDataParallel_2_6982));
	ENDFOR
}

void Post_CollapsedDataParallel_2(buffer_complex_t *chanin, buffer_complex_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 2, _k++) {
 {
			push_complex(&(*chanout), peek_complex(&(*chanin), (_k + 0))) ; 
		}
		}
		ENDFOR
	}
	}
		pop_complex(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_6982() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_6888Post_CollapsedDataParallel_2_6982), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child0_7139_7210_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_6984() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child0_7139_7210_split[1]), &(Pre_CollapsedDataParallel_1_6984butterfly_6889));
	ENDFOR
}

void butterfly_6889() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_6984butterfly_6889), &(butterfly_6889Post_CollapsedDataParallel_2_6985));
	ENDFOR
}

void Post_CollapsedDataParallel_2_6985() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_6889Post_CollapsedDataParallel_2_6985), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child0_7139_7210_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_6987() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child0_7139_7210_split[2]), &(Pre_CollapsedDataParallel_1_6987butterfly_6890));
	ENDFOR
}

void butterfly_6890() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_6987butterfly_6890), &(butterfly_6890Post_CollapsedDataParallel_2_6988));
	ENDFOR
}

void Post_CollapsedDataParallel_2_6988() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_6890Post_CollapsedDataParallel_2_6988), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child0_7139_7210_join[2]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_6990() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child0_7139_7210_split[3]), &(Pre_CollapsedDataParallel_1_6990butterfly_6891));
	ENDFOR
}

void butterfly_6891() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_6990butterfly_6891), &(butterfly_6891Post_CollapsedDataParallel_2_6991));
	ENDFOR
}

void Post_CollapsedDataParallel_2_6991() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_6891Post_CollapsedDataParallel_2_6991), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child0_7139_7210_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7149() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child0_7139_7210_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_Hier_7178_7209_split[0]));
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child0_7139_7210_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_Hier_7178_7209_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7150() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_Hier_7178_7209_join[0], pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child0_7139_7210_join[__iter_]));
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_Hier_7178_7209_join[0], pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child0_7139_7210_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_6993() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin60_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child1_7142_7211_split[0]), &(Pre_CollapsedDataParallel_1_6993butterfly_6892));
	ENDFOR
}

void butterfly_6892() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_6993butterfly_6892), &(butterfly_6892Post_CollapsedDataParallel_2_6994));
	ENDFOR
}

void Post_CollapsedDataParallel_2_6994() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_6892Post_CollapsedDataParallel_2_6994), &(SplitJoin60_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child1_7142_7211_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_6996() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin60_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child1_7142_7211_split[1]), &(Pre_CollapsedDataParallel_1_6996butterfly_6893));
	ENDFOR
}

void butterfly_6893() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_6996butterfly_6893), &(butterfly_6893Post_CollapsedDataParallel_2_6997));
	ENDFOR
}

void Post_CollapsedDataParallel_2_6997() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_6893Post_CollapsedDataParallel_2_6997), &(SplitJoin60_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child1_7142_7211_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_6999() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin60_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child1_7142_7211_split[2]), &(Pre_CollapsedDataParallel_1_6999butterfly_6894));
	ENDFOR
}

void butterfly_6894() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_6999butterfly_6894), &(butterfly_6894Post_CollapsedDataParallel_2_7000));
	ENDFOR
}

void Post_CollapsedDataParallel_2_7000() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_6894Post_CollapsedDataParallel_2_7000), &(SplitJoin60_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child1_7142_7211_join[2]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_7002() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin60_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child1_7142_7211_split[3]), &(Pre_CollapsedDataParallel_1_7002butterfly_6895));
	ENDFOR
}

void butterfly_6895() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_7002butterfly_6895), &(butterfly_6895Post_CollapsedDataParallel_2_7003));
	ENDFOR
}

void Post_CollapsedDataParallel_2_7003() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_6895Post_CollapsedDataParallel_2_7003), &(SplitJoin60_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child1_7142_7211_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7151() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin60_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child1_7142_7211_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_Hier_7178_7209_split[1]));
			push_complex(&SplitJoin60_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child1_7142_7211_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_Hier_7178_7209_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7152() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_Hier_7178_7209_join[1], pop_complex(&SplitJoin60_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child1_7142_7211_join[__iter_]));
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_Hier_7178_7209_join[1], pop_complex(&SplitJoin60_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child1_7142_7211_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_7148() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_Hier_7178_7209_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7005WEIGHTED_ROUND_ROBIN_Splitter_7148));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_Hier_7178_7209_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7005WEIGHTED_ROUND_ROBIN_Splitter_7148));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7153() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7153WEIGHTED_ROUND_ROBIN_Splitter_7154, pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_Hier_7178_7209_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7153WEIGHTED_ROUND_ROBIN_Splitter_7154, pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_Hier_7178_7209_join[1]));
		ENDFOR
	ENDFOR
}}

void butterfly_6897() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		butterfly(&(SplitJoin18_SplitJoin12_SplitJoin12_split2_6807_7060_7138_7214_split[0]), &(SplitJoin18_SplitJoin12_SplitJoin12_split2_6807_7060_7138_7214_join[0]));
	ENDFOR
}

void butterfly_6898() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		butterfly(&(SplitJoin18_SplitJoin12_SplitJoin12_split2_6807_7060_7138_7214_split[1]), &(SplitJoin18_SplitJoin12_SplitJoin12_split2_6807_7060_7138_7214_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7038() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_6807_7060_7138_7214_split[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child0_7145_7213_split[0]));
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_6807_7060_7138_7214_split[1], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child0_7145_7213_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7039() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child0_7145_7213_join[0], pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_6807_7060_7138_7214_join[0]));
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child0_7145_7213_join[0], pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_6807_7060_7138_7214_join[1]));
	ENDFOR
}}

void butterfly_6899() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		butterfly(&(SplitJoin43_SplitJoin29_SplitJoin29_split2_6809_7074_7140_7215_split[0]), &(SplitJoin43_SplitJoin29_SplitJoin29_split2_6809_7074_7140_7215_join[0]));
	ENDFOR
}

void butterfly_6900() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		butterfly(&(SplitJoin43_SplitJoin29_SplitJoin29_split2_6809_7074_7140_7215_split[1]), &(SplitJoin43_SplitJoin29_SplitJoin29_split2_6809_7074_7140_7215_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7040() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		push_complex(&SplitJoin43_SplitJoin29_SplitJoin29_split2_6809_7074_7140_7215_split[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child0_7145_7213_split[1]));
		push_complex(&SplitJoin43_SplitJoin29_SplitJoin29_split2_6809_7074_7140_7215_split[1], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child0_7145_7213_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7041() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child0_7145_7213_join[1], pop_complex(&SplitJoin43_SplitJoin29_SplitJoin29_split2_6809_7074_7140_7215_join[0]));
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child0_7145_7213_join[1], pop_complex(&SplitJoin43_SplitJoin29_SplitJoin29_split2_6809_7074_7140_7215_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_7155() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child0_7145_7213_split[0], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_Hier_7179_7212_split[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child0_7145_7213_split[1], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_Hier_7179_7212_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7156() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_Hier_7179_7212_join[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child0_7145_7213_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_Hier_7179_7212_join[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child0_7145_7213_join[1]));
		ENDFOR
	ENDFOR
}}

void butterfly_6901() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		butterfly(&(SplitJoin49_SplitJoin33_SplitJoin33_split2_6811_7077_7141_7217_split[0]), &(SplitJoin49_SplitJoin33_SplitJoin33_split2_6811_7077_7141_7217_join[0]));
	ENDFOR
}

void butterfly_6902() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		butterfly(&(SplitJoin49_SplitJoin33_SplitJoin33_split2_6811_7077_7141_7217_split[1]), &(SplitJoin49_SplitJoin33_SplitJoin33_split2_6811_7077_7141_7217_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7042() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		push_complex(&SplitJoin49_SplitJoin33_SplitJoin33_split2_6811_7077_7141_7217_split[0], pop_complex(&SplitJoin47_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child1_7147_7216_split[0]));
		push_complex(&SplitJoin49_SplitJoin33_SplitJoin33_split2_6811_7077_7141_7217_split[1], pop_complex(&SplitJoin47_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child1_7147_7216_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7043() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		push_complex(&SplitJoin47_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child1_7147_7216_join[0], pop_complex(&SplitJoin49_SplitJoin33_SplitJoin33_split2_6811_7077_7141_7217_join[0]));
		push_complex(&SplitJoin47_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child1_7147_7216_join[0], pop_complex(&SplitJoin49_SplitJoin33_SplitJoin33_split2_6811_7077_7141_7217_join[1]));
	ENDFOR
}}

void butterfly_6903() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		butterfly(&(SplitJoin53_SplitJoin37_SplitJoin37_split2_6813_7080_7143_7218_split[0]), &(SplitJoin53_SplitJoin37_SplitJoin37_split2_6813_7080_7143_7218_join[0]));
	ENDFOR
}

void butterfly_6904() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		butterfly(&(SplitJoin53_SplitJoin37_SplitJoin37_split2_6813_7080_7143_7218_split[1]), &(SplitJoin53_SplitJoin37_SplitJoin37_split2_6813_7080_7143_7218_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7044() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		push_complex(&SplitJoin53_SplitJoin37_SplitJoin37_split2_6813_7080_7143_7218_split[0], pop_complex(&SplitJoin47_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child1_7147_7216_split[1]));
		push_complex(&SplitJoin53_SplitJoin37_SplitJoin37_split2_6813_7080_7143_7218_split[1], pop_complex(&SplitJoin47_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child1_7147_7216_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7045() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		push_complex(&SplitJoin47_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child1_7147_7216_join[1], pop_complex(&SplitJoin53_SplitJoin37_SplitJoin37_split2_6813_7080_7143_7218_join[0]));
		push_complex(&SplitJoin47_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child1_7147_7216_join[1], pop_complex(&SplitJoin53_SplitJoin37_SplitJoin37_split2_6813_7080_7143_7218_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_7157() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin47_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child1_7147_7216_split[0], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_Hier_7179_7212_split[1]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin47_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child1_7147_7216_split[1], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_Hier_7179_7212_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7158() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_Hier_7179_7212_join[1], pop_complex(&SplitJoin47_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child1_7147_7216_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_Hier_7179_7212_join[1], pop_complex(&SplitJoin47_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child1_7147_7216_join[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_7154() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_Hier_7179_7212_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7153WEIGHTED_ROUND_ROBIN_Splitter_7154));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_Hier_7179_7212_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7153WEIGHTED_ROUND_ROBIN_Splitter_7154));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7159() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7159WEIGHTED_ROUND_ROBIN_Splitter_7046, pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_Hier_7179_7212_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7159WEIGHTED_ROUND_ROBIN_Splitter_7046, pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_Hier_7179_7212_join[1]));
		ENDFOR
	ENDFOR
}}

void butterfly_6906() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_6820_7063_7144_7220_split[0]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_6820_7063_7144_7220_join[0]));
	ENDFOR
}

void butterfly_6907() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_6820_7063_7144_7220_split[1]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_6820_7063_7144_7220_join[1]));
	ENDFOR
}

void butterfly_6908() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_6820_7063_7144_7220_split[2]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_6820_7063_7144_7220_join[2]));
	ENDFOR
}

void butterfly_6909() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_6820_7063_7144_7220_split[3]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_6820_7063_7144_7220_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7048() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_6820_7063_7144_7220_split[__iter_], pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_6818_7062_7180_7219_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7049() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_6818_7062_7180_7219_join[0], pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_6820_7063_7144_7220_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void butterfly_6910() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		butterfly(&(SplitJoin36_SplitJoin22_SplitJoin22_split2_6822_7068_7146_7221_split[0]), &(SplitJoin36_SplitJoin22_SplitJoin22_split2_6822_7068_7146_7221_join[0]));
	ENDFOR
}

void butterfly_6911() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		butterfly(&(SplitJoin36_SplitJoin22_SplitJoin22_split2_6822_7068_7146_7221_split[1]), &(SplitJoin36_SplitJoin22_SplitJoin22_split2_6822_7068_7146_7221_join[1]));
	ENDFOR
}

void butterfly_6912() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		butterfly(&(SplitJoin36_SplitJoin22_SplitJoin22_split2_6822_7068_7146_7221_split[2]), &(SplitJoin36_SplitJoin22_SplitJoin22_split2_6822_7068_7146_7221_join[2]));
	ENDFOR
}

void butterfly_6913() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		butterfly(&(SplitJoin36_SplitJoin22_SplitJoin22_split2_6822_7068_7146_7221_split[3]), &(SplitJoin36_SplitJoin22_SplitJoin22_split2_6822_7068_7146_7221_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7050() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin36_SplitJoin22_SplitJoin22_split2_6822_7068_7146_7221_split[__iter_], pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_6818_7062_7180_7219_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7051() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 14, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_6818_7062_7180_7219_join[1], pop_complex(&SplitJoin36_SplitJoin22_SplitJoin22_split2_6822_7068_7146_7221_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_7046() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_6818_7062_7180_7219_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7159WEIGHTED_ROUND_ROBIN_Splitter_7046));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_6818_7062_7180_7219_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7159WEIGHTED_ROUND_ROBIN_Splitter_7046));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7047() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7047WEIGHTED_ROUND_ROBIN_Splitter_7165, pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_6818_7062_7180_7219_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7047WEIGHTED_ROUND_ROBIN_Splitter_7165, pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_6818_7062_7180_7219_join[1]));
		ENDFOR
	ENDFOR
}}

void magnitude(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		push_float(&(*chanout), ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}


void magnitude_7167() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_7181_7222_split[0]), &(SplitJoin24_magnitude_Fiss_7181_7222_join[0]));
	ENDFOR
}

void magnitude_7168() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_7181_7222_split[1]), &(SplitJoin24_magnitude_Fiss_7181_7222_join[1]));
	ENDFOR
}

void magnitude_7169() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_7181_7222_split[2]), &(SplitJoin24_magnitude_Fiss_7181_7222_join[2]));
	ENDFOR
}

void magnitude_7170() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_7181_7222_split[3]), &(SplitJoin24_magnitude_Fiss_7181_7222_join[3]));
	ENDFOR
}

void magnitude_7171() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_7181_7222_split[4]), &(SplitJoin24_magnitude_Fiss_7181_7222_join[4]));
	ENDFOR
}

void magnitude_7172() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_7181_7222_split[5]), &(SplitJoin24_magnitude_Fiss_7181_7222_join[5]));
	ENDFOR
}

void magnitude_7173() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_7181_7222_split[6]), &(SplitJoin24_magnitude_Fiss_7181_7222_join[6]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7165() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_complex(&SplitJoin24_magnitude_Fiss_7181_7222_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7047WEIGHTED_ROUND_ROBIN_Splitter_7165));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7166() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7166sink_6915, pop_float(&SplitJoin24_magnitude_Fiss_7181_7222_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void sink(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void sink_6915() {
	FOR(uint32_t, __iter_steady_, 0, <, 112, __iter_steady_++)
		sink(&(WEIGHTED_ROUND_ROBIN_Joiner_7166sink_6915));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_complex(&SplitJoin89_SplitJoin71_SplitJoin71_AnonFilter_a0_6868_7106_7188_7205_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 2, __iter_init_1_++)
		init_buffer_complex(&SplitJoin99_SplitJoin81_SplitJoin81_AnonFilter_a0_6882_7113_7191_7208_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_complex(&SplitJoin83_SplitJoin65_SplitJoin65_AnonFilter_a0_6860_7102_7186_7203_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 2, __iter_init_3_++)
		init_buffer_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6830_7054_7176_7196_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 4, __iter_init_4_++)
		init_buffer_complex(&SplitJoin60_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child1_7142_7211_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 2, __iter_init_5_++)
		init_buffer_complex(&SplitJoin47_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child1_7147_7216_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 2, __iter_init_6_++)
		init_buffer_complex(&SplitJoin43_SplitJoin29_SplitJoin29_split2_6809_7074_7140_7215_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 2, __iter_init_7_++)
		init_buffer_complex(&SplitJoin77_SplitJoin59_SplitJoin59_AnonFilter_a0_6852_7098_7185_7201_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_complex(&SplitJoin53_SplitJoin37_SplitJoin37_split2_6813_7080_7143_7218_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 2, __iter_init_9_++)
		init_buffer_complex(&SplitJoin93_SplitJoin75_SplitJoin75_AnonFilter_a0_6874_7109_7189_7206_join[__iter_init_9_]);
	ENDFOR
	init_buffer_complex(&butterfly_6888Post_CollapsedDataParallel_2_6982);
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_complex(&SplitJoin85_SplitJoin67_SplitJoin67_AnonFilter_a0_6862_7103_7187_7204_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 4, __iter_init_11_++)
		init_buffer_complex(&SplitJoin36_SplitJoin22_SplitJoin22_split2_6822_7068_7146_7221_join[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 2, __iter_init_12_++)
		init_buffer_complex(&SplitJoin49_SplitJoin33_SplitJoin33_split2_6811_7077_7141_7217_split[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_complex(&SplitJoin71_SplitJoin53_SplitJoin53_AnonFilter_a0_6844_7094_7183_7199_split[__iter_init_13_]);
	ENDFOR
	init_buffer_complex(&butterfly_6889Post_CollapsedDataParallel_2_6985);
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_complex(&SplitJoin49_SplitJoin33_SplitJoin33_split2_6811_7077_7141_7217_join[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 4, __iter_init_15_++)
		init_buffer_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_6820_7063_7144_7220_join[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_Hier_7179_7212_split[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child0_7145_7213_split[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 2, __iter_init_18_++)
		init_buffer_complex(&SplitJoin67_SplitJoin49_SplitJoin49_AnonFilter_a0_6838_7091_7182_7198_split[__iter_init_18_]);
	ENDFOR
	init_buffer_complex(&butterfly_6893Post_CollapsedDataParallel_2_6997);
	FOR(int, __iter_init_19_, 0, <, 2, __iter_init_19_++)
		init_buffer_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_6818_7062_7180_7219_join[__iter_init_19_]);
	ENDFOR
	init_buffer_complex(&butterfly_6891Post_CollapsedDataParallel_2_6991);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7047WEIGHTED_ROUND_ROBIN_Splitter_7165);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7159WEIGHTED_ROUND_ROBIN_Splitter_7046);
	FOR(int, __iter_init_20_, 0, <, 2, __iter_init_20_++)
		init_buffer_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_6807_7060_7138_7214_split[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 2, __iter_init_21_++)
		init_buffer_complex(&SplitJoin95_SplitJoin77_SplitJoin77_AnonFilter_a0_6876_7110_7190_7207_join[__iter_init_21_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_6987butterfly_6890);
	init_buffer_complex(&butterfly_6895Post_CollapsedDataParallel_2_7003);
	init_buffer_complex(&Pre_CollapsedDataParallel_1_6993butterfly_6892);
	FOR(int, __iter_init_22_, 0, <, 2, __iter_init_22_++)
		init_buffer_complex(&SplitJoin0_source_Fiss_7174_7193_split[__iter_init_22_]);
	ENDFOR
	FOR(int, __iter_init_23_, 0, <, 4, __iter_init_23_++)
		init_buffer_complex(&SplitJoin60_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child1_7142_7211_join[__iter_init_23_]);
	ENDFOR
	FOR(int, __iter_init_24_, 0, <, 2, __iter_init_24_++)
		init_buffer_complex(&SplitJoin43_SplitJoin29_SplitJoin29_split2_6809_7074_7140_7215_split[__iter_init_24_]);
	ENDFOR
	FOR(int, __iter_init_25_, 0, <, 4, __iter_init_25_++)
		init_buffer_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child0_7139_7210_split[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 2, __iter_init_26_++)
		init_buffer_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child0_7145_7213_join[__iter_init_26_]);
	ENDFOR
	FOR(int, __iter_init_27_, 0, <, 2, __iter_init_27_++)
		init_buffer_complex(&SplitJoin0_source_Fiss_7174_7193_join[__iter_init_27_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_6984butterfly_6889);
	FOR(int, __iter_init_28_, 0, <, 2, __iter_init_28_++)
		init_buffer_complex(&SplitJoin93_SplitJoin75_SplitJoin75_AnonFilter_a0_6874_7109_7189_7206_split[__iter_init_28_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7162WEIGHTED_ROUND_ROBIN_Splitter_7004);
	FOR(int, __iter_init_29_, 0, <, 2, __iter_init_29_++)
		init_buffer_complex(&SplitJoin85_SplitJoin67_SplitJoin67_AnonFilter_a0_6862_7103_7187_7204_split[__iter_init_29_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_7002butterfly_6895);
	FOR(int, __iter_init_30_, 0, <, 2, __iter_init_30_++)
		init_buffer_complex(&SplitJoin67_SplitJoin49_SplitJoin49_AnonFilter_a0_6838_7091_7182_7198_join[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 2, __iter_init_31_++)
		init_buffer_complex(&SplitJoin81_SplitJoin63_SplitJoin63_AnonFilter_a0_6858_7101_7137_7202_split[__iter_init_31_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_6999butterfly_6894);
	FOR(int, __iter_init_32_, 0, <, 2, __iter_init_32_++)
		init_buffer_complex(&SplitJoin73_SplitJoin55_SplitJoin55_AnonFilter_a0_6846_7095_7184_7200_join[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 2, __iter_init_33_++)
		init_buffer_complex(&SplitJoin99_SplitJoin81_SplitJoin81_AnonFilter_a0_6882_7113_7191_7208_split[__iter_init_33_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7166sink_6915);
	FOR(int, __iter_init_34_, 0, <, 2, __iter_init_34_++)
		init_buffer_complex(&SplitJoin89_SplitJoin71_SplitJoin71_AnonFilter_a0_6868_7106_7188_7205_split[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 2, __iter_init_35_++)
		init_buffer_complex(&SplitJoin71_SplitJoin53_SplitJoin53_AnonFilter_a0_6844_7094_7183_7199_join[__iter_init_35_]);
	ENDFOR
	FOR(int, __iter_init_36_, 0, <, 2, __iter_init_36_++)
		init_buffer_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6826_7052_7175_7194_join[__iter_init_36_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_6990butterfly_6891);
	FOR(int, __iter_init_37_, 0, <, 2, __iter_init_37_++)
		init_buffer_complex(&SplitJoin83_SplitJoin65_SplitJoin65_AnonFilter_a0_6860_7102_7186_7203_split[__iter_init_37_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7153WEIGHTED_ROUND_ROBIN_Splitter_7154);
	FOR(int, __iter_init_38_, 0, <, 2, __iter_init_38_++)
		init_buffer_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_6826_7052_7175_7194_split[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 2, __iter_init_39_++)
		init_buffer_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_Hier_7178_7209_split[__iter_init_39_]);
	ENDFOR
	FOR(int, __iter_init_40_, 0, <, 2, __iter_init_40_++)
		init_buffer_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_Hier_7178_7209_join[__iter_init_40_]);
	ENDFOR
	FOR(int, __iter_init_41_, 0, <, 2, __iter_init_41_++)
		init_buffer_complex(&SplitJoin81_SplitJoin63_SplitJoin63_AnonFilter_a0_6858_7101_7137_7202_join[__iter_init_41_]);
	ENDFOR
	FOR(int, __iter_init_42_, 0, <, 2, __iter_init_42_++)
		init_buffer_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_6818_7062_7180_7219_split[__iter_init_42_]);
	ENDFOR
	FOR(int, __iter_init_43_, 0, <, 4, __iter_init_43_++)
		init_buffer_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_6820_7063_7144_7220_split[__iter_init_43_]);
	ENDFOR
	init_buffer_complex(&butterfly_6892Post_CollapsedDataParallel_2_6994);
	FOR(int, __iter_init_44_, 0, <, 2, __iter_init_44_++)
		init_buffer_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_6832_7055_7177_7197_join[__iter_init_44_]);
	ENDFOR
	FOR(int, __iter_init_45_, 0, <, 2, __iter_init_45_++)
		init_buffer_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_6807_7060_7138_7214_join[__iter_init_45_]);
	ENDFOR
	FOR(int, __iter_init_46_, 0, <, 4, __iter_init_46_++)
		init_buffer_complex(&SplitJoin36_SplitJoin22_SplitJoin22_split2_6822_7068_7146_7221_split[__iter_init_46_]);
	ENDFOR
	FOR(int, __iter_init_47_, 0, <, 7, __iter_init_47_++)
		init_buffer_float(&SplitJoin24_magnitude_Fiss_7181_7222_join[__iter_init_47_]);
	ENDFOR
	FOR(int, __iter_init_48_, 0, <, 2, __iter_init_48_++)
		init_buffer_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_6832_7055_7177_7197_split[__iter_init_48_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_6981butterfly_6888);
	FOR(int, __iter_init_49_, 0, <, 2, __iter_init_49_++)
		init_buffer_complex(&SplitJoin47_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_child1_7147_7216_split[__iter_init_49_]);
	ENDFOR
	init_buffer_complex(&butterfly_6894Post_CollapsedDataParallel_2_7000);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7005WEIGHTED_ROUND_ROBIN_Splitter_7148);
	FOR(int, __iter_init_50_, 0, <, 2, __iter_init_50_++)
		init_buffer_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6828_7053_7136_7195_join[__iter_init_50_]);
	ENDFOR
	FOR(int, __iter_init_51_, 0, <, 2, __iter_init_51_++)
		init_buffer_complex(&SplitJoin77_SplitJoin59_SplitJoin59_AnonFilter_a0_6852_7098_7185_7201_split[__iter_init_51_]);
	ENDFOR
	FOR(int, __iter_init_52_, 0, <, 2, __iter_init_52_++)
		init_buffer_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_6828_7053_7136_7195_split[__iter_init_52_]);
	ENDFOR
	FOR(int, __iter_init_53_, 0, <, 2, __iter_init_53_++)
		init_buffer_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_6830_7054_7176_7196_split[__iter_init_53_]);
	ENDFOR
	FOR(int, __iter_init_54_, 0, <, 2, __iter_init_54_++)
		init_buffer_complex(&SplitJoin95_SplitJoin77_SplitJoin77_AnonFilter_a0_6876_7110_7190_7207_split[__iter_init_54_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_6996butterfly_6893);
	FOR(int, __iter_init_55_, 0, <, 2, __iter_init_55_++)
		init_buffer_complex(&SplitJoin53_SplitJoin37_SplitJoin37_split2_6813_7080_7143_7218_join[__iter_init_55_]);
	ENDFOR
	FOR(int, __iter_init_56_, 0, <, 2, __iter_init_56_++)
		init_buffer_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_6805_7059_Hier_Hier_7179_7212_join[__iter_init_56_]);
	ENDFOR
	FOR(int, __iter_init_57_, 0, <, 7, __iter_init_57_++)
		init_buffer_complex(&SplitJoin24_magnitude_Fiss_7181_7222_split[__iter_init_57_]);
	ENDFOR
	FOR(int, __iter_init_58_, 0, <, 2, __iter_init_58_++)
		init_buffer_complex(&SplitJoin73_SplitJoin55_SplitJoin55_AnonFilter_a0_6846_7095_7184_7200_split[__iter_init_58_]);
	ENDFOR
	FOR(int, __iter_init_59_, 0, <, 4, __iter_init_59_++)
		init_buffer_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_6784_7057_Hier_child0_7139_7210_join[__iter_init_59_]);
	ENDFOR
	init_buffer_complex(&butterfly_6890Post_CollapsedDataParallel_2_6988);
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_7161();
			source_7163();
			source_7164();
		WEIGHTED_ROUND_ROBIN_Joiner_7162();
		WEIGHTED_ROUND_ROBIN_Splitter_7004();
			WEIGHTED_ROUND_ROBIN_Splitter_7006();
				WEIGHTED_ROUND_ROBIN_Splitter_7008();
					WEIGHTED_ROUND_ROBIN_Splitter_7010();
						Identity_6834();
						Identity_6836();
					WEIGHTED_ROUND_ROBIN_Joiner_7011();
					WEIGHTED_ROUND_ROBIN_Splitter_7012();
						Identity_6840();
						Identity_6842();
					WEIGHTED_ROUND_ROBIN_Joiner_7013();
				WEIGHTED_ROUND_ROBIN_Joiner_7009();
				WEIGHTED_ROUND_ROBIN_Splitter_7014();
					WEIGHTED_ROUND_ROBIN_Splitter_7016();
						Identity_6848();
						Identity_6850();
					WEIGHTED_ROUND_ROBIN_Joiner_7017();
					WEIGHTED_ROUND_ROBIN_Splitter_7018();
						Identity_6854();
						Identity_6856();
					WEIGHTED_ROUND_ROBIN_Joiner_7019();
				WEIGHTED_ROUND_ROBIN_Joiner_7015();
			WEIGHTED_ROUND_ROBIN_Joiner_7007();
			WEIGHTED_ROUND_ROBIN_Splitter_7020();
				WEIGHTED_ROUND_ROBIN_Splitter_7022();
					WEIGHTED_ROUND_ROBIN_Splitter_7024();
						Identity_6864();
						Identity_6866();
					WEIGHTED_ROUND_ROBIN_Joiner_7025();
					WEIGHTED_ROUND_ROBIN_Splitter_7026();
						Identity_6870();
						Identity_6872();
					WEIGHTED_ROUND_ROBIN_Joiner_7027();
				WEIGHTED_ROUND_ROBIN_Joiner_7023();
				WEIGHTED_ROUND_ROBIN_Splitter_7028();
					WEIGHTED_ROUND_ROBIN_Splitter_7030();
						Identity_6878();
						Identity_6880();
					WEIGHTED_ROUND_ROBIN_Joiner_7031();
					WEIGHTED_ROUND_ROBIN_Splitter_7032();
						Identity_6884();
						Identity_6886();
					WEIGHTED_ROUND_ROBIN_Joiner_7033();
				WEIGHTED_ROUND_ROBIN_Joiner_7029();
			WEIGHTED_ROUND_ROBIN_Joiner_7021();
		WEIGHTED_ROUND_ROBIN_Joiner_7005();
		WEIGHTED_ROUND_ROBIN_Splitter_7148();
			WEIGHTED_ROUND_ROBIN_Splitter_7149();
				Pre_CollapsedDataParallel_1_6981();
				butterfly_6888();
				Post_CollapsedDataParallel_2_6982();
				Pre_CollapsedDataParallel_1_6984();
				butterfly_6889();
				Post_CollapsedDataParallel_2_6985();
				Pre_CollapsedDataParallel_1_6987();
				butterfly_6890();
				Post_CollapsedDataParallel_2_6988();
				Pre_CollapsedDataParallel_1_6990();
				butterfly_6891();
				Post_CollapsedDataParallel_2_6991();
			WEIGHTED_ROUND_ROBIN_Joiner_7150();
			WEIGHTED_ROUND_ROBIN_Splitter_7151();
				Pre_CollapsedDataParallel_1_6993();
				butterfly_6892();
				Post_CollapsedDataParallel_2_6994();
				Pre_CollapsedDataParallel_1_6996();
				butterfly_6893();
				Post_CollapsedDataParallel_2_6997();
				Pre_CollapsedDataParallel_1_6999();
				butterfly_6894();
				Post_CollapsedDataParallel_2_7000();
				Pre_CollapsedDataParallel_1_7002();
				butterfly_6895();
				Post_CollapsedDataParallel_2_7003();
			WEIGHTED_ROUND_ROBIN_Joiner_7152();
		WEIGHTED_ROUND_ROBIN_Joiner_7153();
		WEIGHTED_ROUND_ROBIN_Splitter_7154();
			WEIGHTED_ROUND_ROBIN_Splitter_7155();
				WEIGHTED_ROUND_ROBIN_Splitter_7038();
					butterfly_6897();
					butterfly_6898();
				WEIGHTED_ROUND_ROBIN_Joiner_7039();
				WEIGHTED_ROUND_ROBIN_Splitter_7040();
					butterfly_6899();
					butterfly_6900();
				WEIGHTED_ROUND_ROBIN_Joiner_7041();
			WEIGHTED_ROUND_ROBIN_Joiner_7156();
			WEIGHTED_ROUND_ROBIN_Splitter_7157();
				WEIGHTED_ROUND_ROBIN_Splitter_7042();
					butterfly_6901();
					butterfly_6902();
				WEIGHTED_ROUND_ROBIN_Joiner_7043();
				WEIGHTED_ROUND_ROBIN_Splitter_7044();
					butterfly_6903();
					butterfly_6904();
				WEIGHTED_ROUND_ROBIN_Joiner_7045();
			WEIGHTED_ROUND_ROBIN_Joiner_7158();
		WEIGHTED_ROUND_ROBIN_Joiner_7159();
		WEIGHTED_ROUND_ROBIN_Splitter_7046();
			WEIGHTED_ROUND_ROBIN_Splitter_7048();
				butterfly_6906();
				butterfly_6907();
				butterfly_6908();
				butterfly_6909();
			WEIGHTED_ROUND_ROBIN_Joiner_7049();
			WEIGHTED_ROUND_ROBIN_Splitter_7050();
				butterfly_6910();
				butterfly_6911();
				butterfly_6912();
				butterfly_6913();
			WEIGHTED_ROUND_ROBIN_Joiner_7051();
		WEIGHTED_ROUND_ROBIN_Joiner_7047();
		WEIGHTED_ROUND_ROBIN_Splitter_7165();
			magnitude_7167();
			magnitude_7168();
			magnitude_7169();
			magnitude_7170();
			magnitude_7171();
			magnitude_7172();
			magnitude_7173();
		WEIGHTED_ROUND_ROBIN_Joiner_7166();
		sink_6915();
	ENDFOR
	return EXIT_SUCCESS;
}
