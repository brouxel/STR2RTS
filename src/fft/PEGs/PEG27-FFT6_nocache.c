#include "PEG27-FFT6_nocache.h"

buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1272WEIGHTED_ROUND_ROBIN_Splitter_1275;
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_1377_1387_split[4];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_1378_1388_join[8];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1367WEIGHTED_ROUND_ROBIN_Splitter_1372;
buffer_complex_t SplitJoin16_CombineDFT_Fiss_1384_1394_join[2];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_1376_1386_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1357WEIGHTED_ROUND_ROBIN_Splitter_1366;
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_1376_1386_join[2];
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[16];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1292WEIGHTED_ROUND_ROBIN_Splitter_1309;
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_1378_1388_split[8];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1339WEIGHTED_ROUND_ROBIN_Splitter_1356;
buffer_complex_t SplitJoin12_CombineDFT_Fiss_1382_1392_split[8];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1373CombineDFT_1268;
buffer_complex_t SplitJoin10_CombineDFT_Fiss_1381_1391_join[16];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_1381_1391_split[16];
buffer_complex_t FFTReorderSimple_1258WEIGHTED_ROUND_ROBIN_Splitter_1271;
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_1377_1387_join[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1282WEIGHTED_ROUND_ROBIN_Splitter_1291;
buffer_complex_t FFTTestSource_1257FFTReorderSimple_1258;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_1380_1390_join[27];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_1383_1393_split[4];
buffer_complex_t CombineDFT_1268CPrinter_1269;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_1380_1390_split[27];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_1383_1393_join[4];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_1384_1394_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1310WEIGHTED_ROUND_ROBIN_Splitter_1338;
buffer_complex_t SplitJoin12_CombineDFT_Fiss_1382_1392_join[8];
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[16];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1276WEIGHTED_ROUND_ROBIN_Splitter_1281;


CombineDFT_1311_t CombineDFT_1311_s;
CombineDFT_1311_t CombineDFT_1312_s;
CombineDFT_1311_t CombineDFT_1313_s;
CombineDFT_1311_t CombineDFT_1314_s;
CombineDFT_1311_t CombineDFT_1315_s;
CombineDFT_1311_t CombineDFT_1316_s;
CombineDFT_1311_t CombineDFT_1317_s;
CombineDFT_1311_t CombineDFT_1318_s;
CombineDFT_1311_t CombineDFT_1319_s;
CombineDFT_1311_t CombineDFT_1320_s;
CombineDFT_1311_t CombineDFT_1321_s;
CombineDFT_1311_t CombineDFT_1322_s;
CombineDFT_1311_t CombineDFT_1323_s;
CombineDFT_1311_t CombineDFT_1324_s;
CombineDFT_1311_t CombineDFT_1325_s;
CombineDFT_1311_t CombineDFT_1326_s;
CombineDFT_1311_t CombineDFT_1327_s;
CombineDFT_1311_t CombineDFT_1328_s;
CombineDFT_1311_t CombineDFT_1329_s;
CombineDFT_1311_t CombineDFT_1330_s;
CombineDFT_1311_t CombineDFT_1331_s;
CombineDFT_1311_t CombineDFT_1332_s;
CombineDFT_1311_t CombineDFT_1333_s;
CombineDFT_1311_t CombineDFT_1334_s;
CombineDFT_1311_t CombineDFT_1335_s;
CombineDFT_1311_t CombineDFT_1336_s;
CombineDFT_1311_t CombineDFT_1337_s;
CombineDFT_1311_t CombineDFT_1340_s;
CombineDFT_1311_t CombineDFT_1341_s;
CombineDFT_1311_t CombineDFT_1342_s;
CombineDFT_1311_t CombineDFT_1343_s;
CombineDFT_1311_t CombineDFT_1344_s;
CombineDFT_1311_t CombineDFT_1345_s;
CombineDFT_1311_t CombineDFT_1346_s;
CombineDFT_1311_t CombineDFT_1347_s;
CombineDFT_1311_t CombineDFT_1348_s;
CombineDFT_1311_t CombineDFT_1349_s;
CombineDFT_1311_t CombineDFT_1350_s;
CombineDFT_1311_t CombineDFT_1351_s;
CombineDFT_1311_t CombineDFT_1352_s;
CombineDFT_1311_t CombineDFT_1353_s;
CombineDFT_1311_t CombineDFT_1354_s;
CombineDFT_1311_t CombineDFT_1355_s;
CombineDFT_1311_t CombineDFT_1358_s;
CombineDFT_1311_t CombineDFT_1359_s;
CombineDFT_1311_t CombineDFT_1360_s;
CombineDFT_1311_t CombineDFT_1361_s;
CombineDFT_1311_t CombineDFT_1362_s;
CombineDFT_1311_t CombineDFT_1363_s;
CombineDFT_1311_t CombineDFT_1364_s;
CombineDFT_1311_t CombineDFT_1365_s;
CombineDFT_1311_t CombineDFT_1368_s;
CombineDFT_1311_t CombineDFT_1369_s;
CombineDFT_1311_t CombineDFT_1370_s;
CombineDFT_1311_t CombineDFT_1371_s;
CombineDFT_1311_t CombineDFT_1374_s;
CombineDFT_1311_t CombineDFT_1375_s;
CombineDFT_1311_t CombineDFT_1268_s;

void FFTTestSource_1257(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t c1;
		complex_t zero;
		c1.real = 1.0 ; 
		c1.imag = 0.0 ; 
		zero.real = 0.0 ; 
		zero.imag = 0.0 ; 
		push_complex(&FFTTestSource_1257FFTReorderSimple_1258, zero) ; 
		push_complex(&FFTTestSource_1257FFTReorderSimple_1258, c1) ; 
		FOR(int, i, 0,  < , 62, i++) {
			push_complex(&FFTTestSource_1257FFTReorderSimple_1258, zero) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_1258(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&FFTTestSource_1257FFTReorderSimple_1258, i)) ; 
			push_complex(&FFTReorderSimple_1258WEIGHTED_ROUND_ROBIN_Splitter_1271, __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 64, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&FFTTestSource_1257FFTReorderSimple_1258, i)) ; 
			push_complex(&FFTReorderSimple_1258WEIGHTED_ROUND_ROBIN_Splitter_1271, __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&FFTTestSource_1257FFTReorderSimple_1258) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_1273(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_1376_1386_split[0], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_1376_1386_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 32, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_1376_1386_split[0], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_1376_1386_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_1376_1386_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_1274(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_1376_1386_split[1], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_1376_1386_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 32, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_1376_1386_split[1], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_1376_1386_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_1376_1386_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1271() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_1376_1386_split[0], pop_complex(&FFTReorderSimple_1258WEIGHTED_ROUND_ROBIN_Splitter_1271));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_1376_1386_split[1], pop_complex(&FFTReorderSimple_1258WEIGHTED_ROUND_ROBIN_Splitter_1271));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1272() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1272WEIGHTED_ROUND_ROBIN_Splitter_1275, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_1376_1386_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1272WEIGHTED_ROUND_ROBIN_Splitter_1275, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_1376_1386_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_1277(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_1377_1387_split[0], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_1377_1387_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_1377_1387_split[0], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_1377_1387_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_1377_1387_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_1278(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_1377_1387_split[1], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_1377_1387_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_1377_1387_split[1], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_1377_1387_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_1377_1387_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_1279(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_1377_1387_split[2], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_1377_1387_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_1377_1387_split[2], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_1377_1387_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_1377_1387_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_1280(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_1377_1387_split[3], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_1377_1387_join[3], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_1377_1387_split[3], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_1377_1387_join[3], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_1377_1387_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1275() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin2_FFTReorderSimple_Fiss_1377_1387_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1272WEIGHTED_ROUND_ROBIN_Splitter_1275));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1276() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1276WEIGHTED_ROUND_ROBIN_Splitter_1281, pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_1377_1387_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_1283(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_split[0], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_split[0], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_1284(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_split[1], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_split[1], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_1285(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_split[2], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_split[2], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_1286(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_split[3], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_join[3], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_split[3], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_join[3], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_1287(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_split[4], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_join[4], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_split[4], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_join[4], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_1288(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_split[5], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_join[5], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_split[5], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_join[5], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_1289(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_split[6], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_join[6], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_split[6], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_join[6], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_1290(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_split[7], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_join[7], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_split[7], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_join[7], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1281() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1276WEIGHTED_ROUND_ROBIN_Splitter_1281));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1282() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1282WEIGHTED_ROUND_ROBIN_Splitter_1291, pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_1293(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[0], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[0], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_1294(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[1], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[1], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_1295(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[2], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[2], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_1296(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[3], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[3], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[3], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[3], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_1297(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[4], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[4], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[4], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[4], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_1298(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[5], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[5], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[5], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[5], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_1299(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[6], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[6], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[6], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[6], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_1300(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[7], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[7], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[7], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[7], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_1301(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[8], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[8], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[8], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[8], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[8]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_1302(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[9], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[9], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[9], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[9], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[9]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_1303(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[10], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[10], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[10], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[10], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[10]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_1304(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[11], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[11], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[11], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[11], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[11]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_1305(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[12], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[12], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[12], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[12], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[12]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_1306(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[13], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[13], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[13], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[13], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[13]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_1307(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[14], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[14], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[14], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[14], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[14]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_1308(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[15], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[15], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[15], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[15], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[15]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1291() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1282WEIGHTED_ROUND_ROBIN_Splitter_1291));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1292() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1292WEIGHTED_ROUND_ROBIN_Splitter_1309, pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_1311(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[0], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1311_s.wn.real) - (w.imag * CombineDFT_1311_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1311_s.wn.imag) + (w.imag * CombineDFT_1311_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[0]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1312(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[1], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1312_s.wn.real) - (w.imag * CombineDFT_1312_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1312_s.wn.imag) + (w.imag * CombineDFT_1312_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[1]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1313(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[2], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1313_s.wn.real) - (w.imag * CombineDFT_1313_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1313_s.wn.imag) + (w.imag * CombineDFT_1313_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[2]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1314(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[3], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1314_s.wn.real) - (w.imag * CombineDFT_1314_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1314_s.wn.imag) + (w.imag * CombineDFT_1314_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[3]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1315(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[4], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[4], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1315_s.wn.real) - (w.imag * CombineDFT_1315_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1315_s.wn.imag) + (w.imag * CombineDFT_1315_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[4]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1316(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[5], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[5], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1316_s.wn.real) - (w.imag * CombineDFT_1316_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1316_s.wn.imag) + (w.imag * CombineDFT_1316_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[5]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1317(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[6], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[6], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1317_s.wn.real) - (w.imag * CombineDFT_1317_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1317_s.wn.imag) + (w.imag * CombineDFT_1317_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[6]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1318(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[7], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[7], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1318_s.wn.real) - (w.imag * CombineDFT_1318_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1318_s.wn.imag) + (w.imag * CombineDFT_1318_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[7]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1319(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[8], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[8], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1319_s.wn.real) - (w.imag * CombineDFT_1319_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1319_s.wn.imag) + (w.imag * CombineDFT_1319_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[8]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_join[8], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1320(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[9], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[9], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1320_s.wn.real) - (w.imag * CombineDFT_1320_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1320_s.wn.imag) + (w.imag * CombineDFT_1320_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[9]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_join[9], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1321(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[10], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[10], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1321_s.wn.real) - (w.imag * CombineDFT_1321_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1321_s.wn.imag) + (w.imag * CombineDFT_1321_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[10]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_join[10], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1322(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[11], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[11], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1322_s.wn.real) - (w.imag * CombineDFT_1322_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1322_s.wn.imag) + (w.imag * CombineDFT_1322_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[11]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_join[11], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1323(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[12], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[12], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1323_s.wn.real) - (w.imag * CombineDFT_1323_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1323_s.wn.imag) + (w.imag * CombineDFT_1323_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[12]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_join[12], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1324(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[13], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[13], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1324_s.wn.real) - (w.imag * CombineDFT_1324_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1324_s.wn.imag) + (w.imag * CombineDFT_1324_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[13]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_join[13], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1325(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[14], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[14], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1325_s.wn.real) - (w.imag * CombineDFT_1325_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1325_s.wn.imag) + (w.imag * CombineDFT_1325_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[14]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_join[14], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1326(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[15], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[15], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1326_s.wn.real) - (w.imag * CombineDFT_1326_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1326_s.wn.imag) + (w.imag * CombineDFT_1326_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[15]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_join[15], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1327(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[16], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[16], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1327_s.wn.real) - (w.imag * CombineDFT_1327_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1327_s.wn.imag) + (w.imag * CombineDFT_1327_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[16]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_join[16], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1328(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[17], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[17], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1328_s.wn.real) - (w.imag * CombineDFT_1328_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1328_s.wn.imag) + (w.imag * CombineDFT_1328_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[17]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_join[17], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1329(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[18], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[18], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1329_s.wn.real) - (w.imag * CombineDFT_1329_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1329_s.wn.imag) + (w.imag * CombineDFT_1329_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[18]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_join[18], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1330(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[19], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[19], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1330_s.wn.real) - (w.imag * CombineDFT_1330_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1330_s.wn.imag) + (w.imag * CombineDFT_1330_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[19]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_join[19], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1331(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[20], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[20], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1331_s.wn.real) - (w.imag * CombineDFT_1331_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1331_s.wn.imag) + (w.imag * CombineDFT_1331_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[20]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_join[20], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1332(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[21], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[21], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1332_s.wn.real) - (w.imag * CombineDFT_1332_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1332_s.wn.imag) + (w.imag * CombineDFT_1332_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[21]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_join[21], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1333(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[22], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[22], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1333_s.wn.real) - (w.imag * CombineDFT_1333_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1333_s.wn.imag) + (w.imag * CombineDFT_1333_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[22]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_join[22], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1334(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[23], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[23], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1334_s.wn.real) - (w.imag * CombineDFT_1334_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1334_s.wn.imag) + (w.imag * CombineDFT_1334_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[23]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_join[23], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1335(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[24], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[24], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1335_s.wn.real) - (w.imag * CombineDFT_1335_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1335_s.wn.imag) + (w.imag * CombineDFT_1335_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[24]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_join[24], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1336(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[25], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[25], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1336_s.wn.real) - (w.imag * CombineDFT_1336_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1336_s.wn.imag) + (w.imag * CombineDFT_1336_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[25]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_join[25], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1337(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[26], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[26], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1337_s.wn.real) - (w.imag * CombineDFT_1337_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1337_s.wn.imag) + (w.imag * CombineDFT_1337_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[26]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_join[26], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1309() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 27, __iter_++)
			push_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1292WEIGHTED_ROUND_ROBIN_Splitter_1309));
			push_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1292WEIGHTED_ROUND_ROBIN_Splitter_1309));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1310() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 27, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1310WEIGHTED_ROUND_ROBIN_Splitter_1338, pop_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_join[__iter_]));
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1310WEIGHTED_ROUND_ROBIN_Splitter_1338, pop_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_1340(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[0], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1340_s.wn.real) - (w.imag * CombineDFT_1340_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1340_s.wn.imag) + (w.imag * CombineDFT_1340_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[0]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1341(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[1], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1341_s.wn.real) - (w.imag * CombineDFT_1341_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1341_s.wn.imag) + (w.imag * CombineDFT_1341_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[1]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1342(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[2], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1342_s.wn.real) - (w.imag * CombineDFT_1342_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1342_s.wn.imag) + (w.imag * CombineDFT_1342_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[2]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1343(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[3], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1343_s.wn.real) - (w.imag * CombineDFT_1343_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1343_s.wn.imag) + (w.imag * CombineDFT_1343_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[3]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1344(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[4], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[4], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1344_s.wn.real) - (w.imag * CombineDFT_1344_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1344_s.wn.imag) + (w.imag * CombineDFT_1344_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[4]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1345(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[5], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[5], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1345_s.wn.real) - (w.imag * CombineDFT_1345_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1345_s.wn.imag) + (w.imag * CombineDFT_1345_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[5]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1346(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[6], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[6], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1346_s.wn.real) - (w.imag * CombineDFT_1346_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1346_s.wn.imag) + (w.imag * CombineDFT_1346_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[6]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1347(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[7], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[7], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1347_s.wn.real) - (w.imag * CombineDFT_1347_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1347_s.wn.imag) + (w.imag * CombineDFT_1347_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[7]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1348(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[8], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[8], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1348_s.wn.real) - (w.imag * CombineDFT_1348_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1348_s.wn.imag) + (w.imag * CombineDFT_1348_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[8]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_join[8], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1349(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[9], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[9], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1349_s.wn.real) - (w.imag * CombineDFT_1349_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1349_s.wn.imag) + (w.imag * CombineDFT_1349_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[9]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_join[9], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1350(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[10], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[10], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1350_s.wn.real) - (w.imag * CombineDFT_1350_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1350_s.wn.imag) + (w.imag * CombineDFT_1350_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[10]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_join[10], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1351(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[11], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[11], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1351_s.wn.real) - (w.imag * CombineDFT_1351_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1351_s.wn.imag) + (w.imag * CombineDFT_1351_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[11]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_join[11], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1352(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[12], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[12], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1352_s.wn.real) - (w.imag * CombineDFT_1352_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1352_s.wn.imag) + (w.imag * CombineDFT_1352_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[12]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_join[12], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1353(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[13], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[13], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1353_s.wn.real) - (w.imag * CombineDFT_1353_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1353_s.wn.imag) + (w.imag * CombineDFT_1353_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[13]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_join[13], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1354(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[14], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[14], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1354_s.wn.real) - (w.imag * CombineDFT_1354_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1354_s.wn.imag) + (w.imag * CombineDFT_1354_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[14]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_join[14], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1355(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[15], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[15], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1355_s.wn.real) - (w.imag * CombineDFT_1355_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1355_s.wn.imag) + (w.imag * CombineDFT_1355_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[15]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_join[15], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1338() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1310WEIGHTED_ROUND_ROBIN_Splitter_1338));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1339() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1339WEIGHTED_ROUND_ROBIN_Splitter_1356, pop_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_1358(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_split[0], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1358_s.wn.real) - (w.imag * CombineDFT_1358_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1358_s.wn.imag) + (w.imag * CombineDFT_1358_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_split[0]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1359(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_split[1], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1359_s.wn.real) - (w.imag * CombineDFT_1359_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1359_s.wn.imag) + (w.imag * CombineDFT_1359_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_split[1]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1360(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_split[2], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1360_s.wn.real) - (w.imag * CombineDFT_1360_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1360_s.wn.imag) + (w.imag * CombineDFT_1360_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_split[2]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1361(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_split[3], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1361_s.wn.real) - (w.imag * CombineDFT_1361_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1361_s.wn.imag) + (w.imag * CombineDFT_1361_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_split[3]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1362(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_split[4], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_split[4], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1362_s.wn.real) - (w.imag * CombineDFT_1362_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1362_s.wn.imag) + (w.imag * CombineDFT_1362_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_split[4]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1363(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_split[5], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_split[5], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1363_s.wn.real) - (w.imag * CombineDFT_1363_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1363_s.wn.imag) + (w.imag * CombineDFT_1363_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_split[5]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1364(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_split[6], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_split[6], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1364_s.wn.real) - (w.imag * CombineDFT_1364_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1364_s.wn.imag) + (w.imag * CombineDFT_1364_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_split[6]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1365(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_split[7], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_split[7], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1365_s.wn.real) - (w.imag * CombineDFT_1365_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1365_s.wn.imag) + (w.imag * CombineDFT_1365_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_split[7]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1356() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1339WEIGHTED_ROUND_ROBIN_Splitter_1356));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1357() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1357WEIGHTED_ROUND_ROBIN_Splitter_1366, pop_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_1368(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_1383_1393_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_1383_1393_split[0], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1368_s.wn.real) - (w.imag * CombineDFT_1368_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1368_s.wn.imag) + (w.imag * CombineDFT_1368_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_1383_1393_split[0]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_1383_1393_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1369(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_1383_1393_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_1383_1393_split[1], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1369_s.wn.real) - (w.imag * CombineDFT_1369_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1369_s.wn.imag) + (w.imag * CombineDFT_1369_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_1383_1393_split[1]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_1383_1393_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1370(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_1383_1393_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_1383_1393_split[2], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1370_s.wn.real) - (w.imag * CombineDFT_1370_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1370_s.wn.imag) + (w.imag * CombineDFT_1370_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_1383_1393_split[2]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_1383_1393_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1371(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_1383_1393_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_1383_1393_split[3], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1371_s.wn.real) - (w.imag * CombineDFT_1371_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1371_s.wn.imag) + (w.imag * CombineDFT_1371_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_1383_1393_split[3]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_1383_1393_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1366() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin14_CombineDFT_Fiss_1383_1393_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1357WEIGHTED_ROUND_ROBIN_Splitter_1366));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1367() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1367WEIGHTED_ROUND_ROBIN_Splitter_1372, pop_complex(&SplitJoin14_CombineDFT_Fiss_1383_1393_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_1374(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[32];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 16, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_1384_1394_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_1384_1394_split[0], (16 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(16 + i)].real = (y0.real - y1w.real) ; 
			results[(16 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1374_s.wn.real) - (w.imag * CombineDFT_1374_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1374_s.wn.imag) + (w.imag * CombineDFT_1374_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin16_CombineDFT_Fiss_1384_1394_split[0]) ; 
			push_complex(&SplitJoin16_CombineDFT_Fiss_1384_1394_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_1375(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[32];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 16, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_1384_1394_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_1384_1394_split[1], (16 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(16 + i)].real = (y0.real - y1w.real) ; 
			results[(16 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1375_s.wn.real) - (w.imag * CombineDFT_1375_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1375_s.wn.imag) + (w.imag * CombineDFT_1375_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin16_CombineDFT_Fiss_1384_1394_split[1]) ; 
			push_complex(&SplitJoin16_CombineDFT_Fiss_1384_1394_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1372() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_1384_1394_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1367WEIGHTED_ROUND_ROBIN_Splitter_1372));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_1384_1394_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1367WEIGHTED_ROUND_ROBIN_Splitter_1372));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1373() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1373CombineDFT_1268, pop_complex(&SplitJoin16_CombineDFT_Fiss_1384_1394_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1373CombineDFT_1268, pop_complex(&SplitJoin16_CombineDFT_Fiss_1384_1394_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_1268(){
	FOR(uint32_t, __iter_steady_, 0, <, 27, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[64];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1373CombineDFT_1268, i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1373CombineDFT_1268, (32 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(32 + i)].real = (y0.real - y1w.real) ; 
			results[(32 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1268_s.wn.real) - (w.imag * CombineDFT_1268_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1268_s.wn.imag) + (w.imag * CombineDFT_1268_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1373CombineDFT_1268) ; 
			push_complex(&CombineDFT_1268CPrinter_1269, results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CPrinter_1269(){
	FOR(uint32_t, __iter_steady_, 0, <, 1728, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&CombineDFT_1268CPrinter_1269));
		printf("%.10f", c.real);
		printf("\n");
		printf("%.10f", c.imag);
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1272WEIGHTED_ROUND_ROBIN_Splitter_1275);
	FOR(int, __iter_init_0_, 0, <, 4, __iter_init_0_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_1377_1387_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_join[__iter_init_1_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1367WEIGHTED_ROUND_ROBIN_Splitter_1372);
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_1384_1394_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 2, __iter_init_3_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_1376_1386_split[__iter_init_3_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1357WEIGHTED_ROUND_ROBIN_Splitter_1366);
	FOR(int, __iter_init_4_, 0, <, 2, __iter_init_4_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_1376_1386_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 16, __iter_init_5_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_split[__iter_init_5_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1292WEIGHTED_ROUND_ROBIN_Splitter_1309);
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_1378_1388_split[__iter_init_6_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1339WEIGHTED_ROUND_ROBIN_Splitter_1356);
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_split[__iter_init_7_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1373CombineDFT_1268);
	FOR(int, __iter_init_8_, 0, <, 16, __iter_init_8_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 16, __iter_init_9_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_1381_1391_split[__iter_init_9_]);
	ENDFOR
	init_buffer_complex(&FFTReorderSimple_1258WEIGHTED_ROUND_ROBIN_Splitter_1271);
	FOR(int, __iter_init_10_, 0, <, 4, __iter_init_10_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_1377_1387_join[__iter_init_10_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1282WEIGHTED_ROUND_ROBIN_Splitter_1291);
	init_buffer_complex(&FFTTestSource_1257FFTReorderSimple_1258);
	FOR(int, __iter_init_11_, 0, <, 27, __iter_init_11_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_join[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 4, __iter_init_12_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_1383_1393_split[__iter_init_12_]);
	ENDFOR
	init_buffer_complex(&CombineDFT_1268CPrinter_1269);
	FOR(int, __iter_init_13_, 0, <, 27, __iter_init_13_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_1380_1390_split[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 4, __iter_init_14_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_1383_1393_join[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 2, __iter_init_15_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_1384_1394_split[__iter_init_15_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1310WEIGHTED_ROUND_ROBIN_Splitter_1338);
	FOR(int, __iter_init_16_, 0, <, 8, __iter_init_16_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_1382_1392_join[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 16, __iter_init_17_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_1379_1389_join[__iter_init_17_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1276WEIGHTED_ROUND_ROBIN_Splitter_1281);
// --- init: CombineDFT_1311
	 {
	 ; 
	CombineDFT_1311_s.wn.real = -1.0 ; 
	CombineDFT_1311_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1312
	 {
	 ; 
	CombineDFT_1312_s.wn.real = -1.0 ; 
	CombineDFT_1312_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1313
	 {
	 ; 
	CombineDFT_1313_s.wn.real = -1.0 ; 
	CombineDFT_1313_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1314
	 {
	 ; 
	CombineDFT_1314_s.wn.real = -1.0 ; 
	CombineDFT_1314_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1315
	 {
	 ; 
	CombineDFT_1315_s.wn.real = -1.0 ; 
	CombineDFT_1315_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1316
	 {
	 ; 
	CombineDFT_1316_s.wn.real = -1.0 ; 
	CombineDFT_1316_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1317
	 {
	 ; 
	CombineDFT_1317_s.wn.real = -1.0 ; 
	CombineDFT_1317_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1318
	 {
	 ; 
	CombineDFT_1318_s.wn.real = -1.0 ; 
	CombineDFT_1318_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1319
	 {
	 ; 
	CombineDFT_1319_s.wn.real = -1.0 ; 
	CombineDFT_1319_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1320
	 {
	 ; 
	CombineDFT_1320_s.wn.real = -1.0 ; 
	CombineDFT_1320_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1321
	 {
	 ; 
	CombineDFT_1321_s.wn.real = -1.0 ; 
	CombineDFT_1321_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1322
	 {
	 ; 
	CombineDFT_1322_s.wn.real = -1.0 ; 
	CombineDFT_1322_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1323
	 {
	 ; 
	CombineDFT_1323_s.wn.real = -1.0 ; 
	CombineDFT_1323_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1324
	 {
	 ; 
	CombineDFT_1324_s.wn.real = -1.0 ; 
	CombineDFT_1324_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1325
	 {
	 ; 
	CombineDFT_1325_s.wn.real = -1.0 ; 
	CombineDFT_1325_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1326
	 {
	 ; 
	CombineDFT_1326_s.wn.real = -1.0 ; 
	CombineDFT_1326_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1327
	 {
	 ; 
	CombineDFT_1327_s.wn.real = -1.0 ; 
	CombineDFT_1327_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1328
	 {
	 ; 
	CombineDFT_1328_s.wn.real = -1.0 ; 
	CombineDFT_1328_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1329
	 {
	 ; 
	CombineDFT_1329_s.wn.real = -1.0 ; 
	CombineDFT_1329_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1330
	 {
	 ; 
	CombineDFT_1330_s.wn.real = -1.0 ; 
	CombineDFT_1330_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1331
	 {
	 ; 
	CombineDFT_1331_s.wn.real = -1.0 ; 
	CombineDFT_1331_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1332
	 {
	 ; 
	CombineDFT_1332_s.wn.real = -1.0 ; 
	CombineDFT_1332_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1333
	 {
	 ; 
	CombineDFT_1333_s.wn.real = -1.0 ; 
	CombineDFT_1333_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1334
	 {
	 ; 
	CombineDFT_1334_s.wn.real = -1.0 ; 
	CombineDFT_1334_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1335
	 {
	 ; 
	CombineDFT_1335_s.wn.real = -1.0 ; 
	CombineDFT_1335_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1336
	 {
	 ; 
	CombineDFT_1336_s.wn.real = -1.0 ; 
	CombineDFT_1336_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1337
	 {
	 ; 
	CombineDFT_1337_s.wn.real = -1.0 ; 
	CombineDFT_1337_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1340
	 {
	 ; 
	CombineDFT_1340_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1340_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1341
	 {
	 ; 
	CombineDFT_1341_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1341_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1342
	 {
	 ; 
	CombineDFT_1342_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1342_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1343
	 {
	 ; 
	CombineDFT_1343_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1343_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1344
	 {
	 ; 
	CombineDFT_1344_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1344_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1345
	 {
	 ; 
	CombineDFT_1345_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1345_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1346
	 {
	 ; 
	CombineDFT_1346_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1346_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1347
	 {
	 ; 
	CombineDFT_1347_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1347_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1348
	 {
	 ; 
	CombineDFT_1348_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1348_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1349
	 {
	 ; 
	CombineDFT_1349_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1349_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1350
	 {
	 ; 
	CombineDFT_1350_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1350_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1351
	 {
	 ; 
	CombineDFT_1351_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1351_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1352
	 {
	 ; 
	CombineDFT_1352_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1352_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1353
	 {
	 ; 
	CombineDFT_1353_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1353_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1354
	 {
	 ; 
	CombineDFT_1354_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1354_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1355
	 {
	 ; 
	CombineDFT_1355_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1355_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1358
	 {
	 ; 
	CombineDFT_1358_s.wn.real = 0.70710677 ; 
	CombineDFT_1358_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1359
	 {
	 ; 
	CombineDFT_1359_s.wn.real = 0.70710677 ; 
	CombineDFT_1359_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1360
	 {
	 ; 
	CombineDFT_1360_s.wn.real = 0.70710677 ; 
	CombineDFT_1360_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1361
	 {
	 ; 
	CombineDFT_1361_s.wn.real = 0.70710677 ; 
	CombineDFT_1361_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1362
	 {
	 ; 
	CombineDFT_1362_s.wn.real = 0.70710677 ; 
	CombineDFT_1362_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1363
	 {
	 ; 
	CombineDFT_1363_s.wn.real = 0.70710677 ; 
	CombineDFT_1363_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1364
	 {
	 ; 
	CombineDFT_1364_s.wn.real = 0.70710677 ; 
	CombineDFT_1364_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1365
	 {
	 ; 
	CombineDFT_1365_s.wn.real = 0.70710677 ; 
	CombineDFT_1365_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1368
	 {
	 ; 
	CombineDFT_1368_s.wn.real = 0.9238795 ; 
	CombineDFT_1368_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_1369
	 {
	 ; 
	CombineDFT_1369_s.wn.real = 0.9238795 ; 
	CombineDFT_1369_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_1370
	 {
	 ; 
	CombineDFT_1370_s.wn.real = 0.9238795 ; 
	CombineDFT_1370_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_1371
	 {
	 ; 
	CombineDFT_1371_s.wn.real = 0.9238795 ; 
	CombineDFT_1371_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_1374
	 {
	 ; 
	CombineDFT_1374_s.wn.real = 0.98078525 ; 
	CombineDFT_1374_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_1375
	 {
	 ; 
	CombineDFT_1375_s.wn.real = 0.98078525 ; 
	CombineDFT_1375_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_1268
	 {
	 ; 
	CombineDFT_1268_s.wn.real = 0.9951847 ; 
	CombineDFT_1268_s.wn.imag = -0.09801714 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FFTTestSource_1257();
		FFTReorderSimple_1258();
		WEIGHTED_ROUND_ROBIN_Splitter_1271();
			FFTReorderSimple_1273();
			FFTReorderSimple_1274();
		WEIGHTED_ROUND_ROBIN_Joiner_1272();
		WEIGHTED_ROUND_ROBIN_Splitter_1275();
			FFTReorderSimple_1277();
			FFTReorderSimple_1278();
			FFTReorderSimple_1279();
			FFTReorderSimple_1280();
		WEIGHTED_ROUND_ROBIN_Joiner_1276();
		WEIGHTED_ROUND_ROBIN_Splitter_1281();
			FFTReorderSimple_1283();
			FFTReorderSimple_1284();
			FFTReorderSimple_1285();
			FFTReorderSimple_1286();
			FFTReorderSimple_1287();
			FFTReorderSimple_1288();
			FFTReorderSimple_1289();
			FFTReorderSimple_1290();
		WEIGHTED_ROUND_ROBIN_Joiner_1282();
		WEIGHTED_ROUND_ROBIN_Splitter_1291();
			FFTReorderSimple_1293();
			FFTReorderSimple_1294();
			FFTReorderSimple_1295();
			FFTReorderSimple_1296();
			FFTReorderSimple_1297();
			FFTReorderSimple_1298();
			FFTReorderSimple_1299();
			FFTReorderSimple_1300();
			FFTReorderSimple_1301();
			FFTReorderSimple_1302();
			FFTReorderSimple_1303();
			FFTReorderSimple_1304();
			FFTReorderSimple_1305();
			FFTReorderSimple_1306();
			FFTReorderSimple_1307();
			FFTReorderSimple_1308();
		WEIGHTED_ROUND_ROBIN_Joiner_1292();
		WEIGHTED_ROUND_ROBIN_Splitter_1309();
			CombineDFT_1311();
			CombineDFT_1312();
			CombineDFT_1313();
			CombineDFT_1314();
			CombineDFT_1315();
			CombineDFT_1316();
			CombineDFT_1317();
			CombineDFT_1318();
			CombineDFT_1319();
			CombineDFT_1320();
			CombineDFT_1321();
			CombineDFT_1322();
			CombineDFT_1323();
			CombineDFT_1324();
			CombineDFT_1325();
			CombineDFT_1326();
			CombineDFT_1327();
			CombineDFT_1328();
			CombineDFT_1329();
			CombineDFT_1330();
			CombineDFT_1331();
			CombineDFT_1332();
			CombineDFT_1333();
			CombineDFT_1334();
			CombineDFT_1335();
			CombineDFT_1336();
			CombineDFT_1337();
		WEIGHTED_ROUND_ROBIN_Joiner_1310();
		WEIGHTED_ROUND_ROBIN_Splitter_1338();
			CombineDFT_1340();
			CombineDFT_1341();
			CombineDFT_1342();
			CombineDFT_1343();
			CombineDFT_1344();
			CombineDFT_1345();
			CombineDFT_1346();
			CombineDFT_1347();
			CombineDFT_1348();
			CombineDFT_1349();
			CombineDFT_1350();
			CombineDFT_1351();
			CombineDFT_1352();
			CombineDFT_1353();
			CombineDFT_1354();
			CombineDFT_1355();
		WEIGHTED_ROUND_ROBIN_Joiner_1339();
		WEIGHTED_ROUND_ROBIN_Splitter_1356();
			CombineDFT_1358();
			CombineDFT_1359();
			CombineDFT_1360();
			CombineDFT_1361();
			CombineDFT_1362();
			CombineDFT_1363();
			CombineDFT_1364();
			CombineDFT_1365();
		WEIGHTED_ROUND_ROBIN_Joiner_1357();
		WEIGHTED_ROUND_ROBIN_Splitter_1366();
			CombineDFT_1368();
			CombineDFT_1369();
			CombineDFT_1370();
			CombineDFT_1371();
		WEIGHTED_ROUND_ROBIN_Joiner_1367();
		WEIGHTED_ROUND_ROBIN_Splitter_1372();
			CombineDFT_1374();
			CombineDFT_1375();
		WEIGHTED_ROUND_ROBIN_Joiner_1373();
		CombineDFT_1268();
		CPrinter_1269();
	ENDFOR
	return EXIT_SUCCESS;
}
