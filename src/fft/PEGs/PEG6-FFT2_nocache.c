#include "PEG6-FFT2_nocache.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11608WEIGHTED_ROUND_ROBIN_Splitter_11615;
buffer_float_t SplitJoin72_CombineDFT_Fiss_11652_11673_join[2];
buffer_float_t SplitJoin14_CombineDFT_Fiss_11640_11661_join[6];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11556WEIGHTED_ROUND_ROBIN_Splitter_11563;
buffer_float_t SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[6];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11502FloatPrinter_11500;
buffer_float_t SplitJoin60_FFTReorderSimple_Fiss_11646_11667_join[6];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11532WEIGHTED_ROUND_ROBIN_Splitter_11539;
buffer_float_t SplitJoin58_FFTReorderSimple_Fiss_11645_11666_join[4];
buffer_float_t SplitJoin16_CombineDFT_Fiss_11641_11662_split[6];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11467_11503_11634_11655_join[2];
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_11636_11657_split[4];
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_11637_11658_join[6];
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_11638_11659_join[6];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11574WEIGHTED_ROUND_ROBIN_Splitter_11577;
buffer_float_t SplitJoin58_FFTReorderSimple_Fiss_11645_11666_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11570CombineDFT_11488;
buffer_float_t SplitJoin64_CombineDFT_Fiss_11648_11669_split[6];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11518WEIGHTED_ROUND_ROBIN_Splitter_11523;
buffer_float_t SplitJoin56_FFTReorderSimple_Fiss_11644_11665_join[2];
buffer_float_t FFTReorderSimple_11489WEIGHTED_ROUND_ROBIN_Splitter_11573;
buffer_float_t SplitJoin64_CombineDFT_Fiss_11648_11669_join[6];
buffer_float_t SplitJoin66_CombineDFT_Fiss_11649_11670_split[6];
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_11635_11656_split[2];
buffer_float_t SplitJoin68_CombineDFT_Fiss_11650_11671_join[6];
buffer_float_t SplitJoin70_CombineDFT_Fiss_11651_11672_join[4];
buffer_float_t SplitJoin0_FFTTestSource_Fiss_11633_11654_join[2];
buffer_float_t FFTReorderSimple_11478WEIGHTED_ROUND_ROBIN_Splitter_11513;
buffer_float_t SplitJoin14_CombineDFT_Fiss_11640_11661_split[6];
buffer_float_t SplitJoin18_CombineDFT_Fiss_11642_11663_split[4];
buffer_float_t SplitJoin56_FFTReorderSimple_Fiss_11644_11665_split[2];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11467_11503_11634_11655_split[2];
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_11636_11657_join[4];
buffer_float_t SplitJoin12_CombineDFT_Fiss_11639_11660_join[6];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11584WEIGHTED_ROUND_ROBIN_Splitter_11591;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11514WEIGHTED_ROUND_ROBIN_Splitter_11517;
buffer_float_t SplitJoin18_CombineDFT_Fiss_11642_11663_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11616WEIGHTED_ROUND_ROBIN_Splitter_11623;
buffer_float_t SplitJoin72_CombineDFT_Fiss_11652_11673_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11630CombineDFT_11499;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11624WEIGHTED_ROUND_ROBIN_Splitter_11629;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11578WEIGHTED_ROUND_ROBIN_Splitter_11583;
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[6];
buffer_float_t SplitJoin20_CombineDFT_Fiss_11643_11664_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11592WEIGHTED_ROUND_ROBIN_Splitter_11599;
buffer_float_t SplitJoin16_CombineDFT_Fiss_11641_11662_join[6];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11600WEIGHTED_ROUND_ROBIN_Splitter_11607;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11524WEIGHTED_ROUND_ROBIN_Splitter_11531;
buffer_float_t SplitJoin70_CombineDFT_Fiss_11651_11672_split[4];
buffer_float_t SplitJoin0_FFTTestSource_Fiss_11633_11654_split[2];
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_11635_11656_join[2];
buffer_float_t SplitJoin12_CombineDFT_Fiss_11639_11660_split[6];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11548WEIGHTED_ROUND_ROBIN_Splitter_11555;
buffer_float_t SplitJoin62_FFTReorderSimple_Fiss_11647_11668_join[6];
buffer_float_t SplitJoin68_CombineDFT_Fiss_11650_11671_split[6];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11540WEIGHTED_ROUND_ROBIN_Splitter_11547;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11510WEIGHTED_ROUND_ROBIN_Splitter_11501;
buffer_float_t SplitJoin20_CombineDFT_Fiss_11643_11664_split[2];
buffer_float_t SplitJoin66_CombineDFT_Fiss_11649_11670_join[6];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11564WEIGHTED_ROUND_ROBIN_Splitter_11569;
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[6];
buffer_float_t SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[6];


CombineDFT_11541_t CombineDFT_11541_s;
CombineDFT_11541_t CombineDFT_11542_s;
CombineDFT_11541_t CombineDFT_11543_s;
CombineDFT_11541_t CombineDFT_11544_s;
CombineDFT_11541_t CombineDFT_11545_s;
CombineDFT_11541_t CombineDFT_11546_s;
CombineDFT_11549_t CombineDFT_11549_s;
CombineDFT_11549_t CombineDFT_11550_s;
CombineDFT_11549_t CombineDFT_11551_s;
CombineDFT_11549_t CombineDFT_11552_s;
CombineDFT_11549_t CombineDFT_11553_s;
CombineDFT_11549_t CombineDFT_11554_s;
CombineDFT_11557_t CombineDFT_11557_s;
CombineDFT_11557_t CombineDFT_11558_s;
CombineDFT_11557_t CombineDFT_11559_s;
CombineDFT_11557_t CombineDFT_11560_s;
CombineDFT_11557_t CombineDFT_11561_s;
CombineDFT_11557_t CombineDFT_11562_s;
CombineDFT_11565_t CombineDFT_11565_s;
CombineDFT_11565_t CombineDFT_11566_s;
CombineDFT_11565_t CombineDFT_11567_s;
CombineDFT_11565_t CombineDFT_11568_s;
CombineDFT_11571_t CombineDFT_11571_s;
CombineDFT_11571_t CombineDFT_11572_s;
CombineDFT_11488_t CombineDFT_11488_s;
CombineDFT_11541_t CombineDFT_11601_s;
CombineDFT_11541_t CombineDFT_11602_s;
CombineDFT_11541_t CombineDFT_11603_s;
CombineDFT_11541_t CombineDFT_11604_s;
CombineDFT_11541_t CombineDFT_11605_s;
CombineDFT_11541_t CombineDFT_11606_s;
CombineDFT_11549_t CombineDFT_11609_s;
CombineDFT_11549_t CombineDFT_11610_s;
CombineDFT_11549_t CombineDFT_11611_s;
CombineDFT_11549_t CombineDFT_11612_s;
CombineDFT_11549_t CombineDFT_11613_s;
CombineDFT_11549_t CombineDFT_11614_s;
CombineDFT_11557_t CombineDFT_11617_s;
CombineDFT_11557_t CombineDFT_11618_s;
CombineDFT_11557_t CombineDFT_11619_s;
CombineDFT_11557_t CombineDFT_11620_s;
CombineDFT_11557_t CombineDFT_11621_s;
CombineDFT_11557_t CombineDFT_11622_s;
CombineDFT_11565_t CombineDFT_11625_s;
CombineDFT_11565_t CombineDFT_11626_s;
CombineDFT_11565_t CombineDFT_11627_s;
CombineDFT_11565_t CombineDFT_11628_s;
CombineDFT_11571_t CombineDFT_11631_s;
CombineDFT_11571_t CombineDFT_11632_s;
CombineDFT_11488_t CombineDFT_11499_s;

void FFTTestSource_11511(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		push_float(&SplitJoin0_FFTTestSource_Fiss_11633_11654_join[0], 0.0) ; 
		push_float(&SplitJoin0_FFTTestSource_Fiss_11633_11654_join[0], 0.0) ; 
		push_float(&SplitJoin0_FFTTestSource_Fiss_11633_11654_join[0], 1.0) ; 
		push_float(&SplitJoin0_FFTTestSource_Fiss_11633_11654_join[0], 0.0) ; 
		FOR(int, i, 0,  < , 124, i++) {
			push_float(&SplitJoin0_FFTTestSource_Fiss_11633_11654_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTTestSource_11512(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		push_float(&SplitJoin0_FFTTestSource_Fiss_11633_11654_join[1], 0.0) ; 
		push_float(&SplitJoin0_FFTTestSource_Fiss_11633_11654_join[1], 0.0) ; 
		push_float(&SplitJoin0_FFTTestSource_Fiss_11633_11654_join[1], 1.0) ; 
		push_float(&SplitJoin0_FFTTestSource_Fiss_11633_11654_join[1], 0.0) ; 
		FOR(int, i, 0,  < , 124, i++) {
			push_float(&SplitJoin0_FFTTestSource_Fiss_11633_11654_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11509() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_11510() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11510WEIGHTED_ROUND_ROBIN_Splitter_11501, pop_float(&SplitJoin0_FFTTestSource_Fiss_11633_11654_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11510WEIGHTED_ROUND_ROBIN_Splitter_11501, pop_float(&SplitJoin0_FFTTestSource_Fiss_11633_11654_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_11478(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, i, 0,  < , 128, i = (i + 4)) {
			push_float(&FFTReorderSimple_11478WEIGHTED_ROUND_ROBIN_Splitter_11513, peek_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11467_11503_11634_11655_split[0], i)) ; 
			push_float(&FFTReorderSimple_11478WEIGHTED_ROUND_ROBIN_Splitter_11513, peek_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11467_11503_11634_11655_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 128, i = (i + 4)) {
			push_float(&FFTReorderSimple_11478WEIGHTED_ROUND_ROBIN_Splitter_11513, peek_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11467_11503_11634_11655_split[0], i)) ; 
			push_float(&FFTReorderSimple_11478WEIGHTED_ROUND_ROBIN_Splitter_11513, peek_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11467_11503_11634_11655_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11467_11503_11634_11655_split[0]) ; 
			pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11467_11503_11634_11655_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_11515(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i = (i + 4)) {
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_11635_11656_join[0], peek_float(&SplitJoin4_FFTReorderSimple_Fiss_11635_11656_split[0], i)) ; 
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_11635_11656_join[0], peek_float(&SplitJoin4_FFTReorderSimple_Fiss_11635_11656_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 64, i = (i + 4)) {
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_11635_11656_join[0], peek_float(&SplitJoin4_FFTReorderSimple_Fiss_11635_11656_split[0], i)) ; 
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_11635_11656_join[0], peek_float(&SplitJoin4_FFTReorderSimple_Fiss_11635_11656_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&SplitJoin4_FFTReorderSimple_Fiss_11635_11656_split[0]) ; 
			pop_float(&SplitJoin4_FFTReorderSimple_Fiss_11635_11656_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_11516(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i = (i + 4)) {
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_11635_11656_join[1], peek_float(&SplitJoin4_FFTReorderSimple_Fiss_11635_11656_split[1], i)) ; 
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_11635_11656_join[1], peek_float(&SplitJoin4_FFTReorderSimple_Fiss_11635_11656_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 64, i = (i + 4)) {
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_11635_11656_join[1], peek_float(&SplitJoin4_FFTReorderSimple_Fiss_11635_11656_split[1], i)) ; 
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_11635_11656_join[1], peek_float(&SplitJoin4_FFTReorderSimple_Fiss_11635_11656_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&SplitJoin4_FFTReorderSimple_Fiss_11635_11656_split[1]) ; 
			pop_float(&SplitJoin4_FFTReorderSimple_Fiss_11635_11656_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11513() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_11635_11656_split[0], pop_float(&FFTReorderSimple_11478WEIGHTED_ROUND_ROBIN_Splitter_11513));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_11635_11656_split[1], pop_float(&FFTReorderSimple_11478WEIGHTED_ROUND_ROBIN_Splitter_11513));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11514() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11514WEIGHTED_ROUND_ROBIN_Splitter_11517, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_11635_11656_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11514WEIGHTED_ROUND_ROBIN_Splitter_11517, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_11635_11656_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_11519(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_join[0], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_split[0], i)) ; 
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_join[0], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_join[0], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_split[0], i)) ; 
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_join[0], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_split[0]) ; 
			pop_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_11520(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_join[1], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_split[1], i)) ; 
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_join[1], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_join[1], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_split[1], i)) ; 
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_join[1], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_split[1]) ; 
			pop_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_11521(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_join[2], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_split[2], i)) ; 
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_join[2], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_split[2], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_join[2], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_split[2], i)) ; 
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_join[2], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_split[2], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_split[2]) ; 
			pop_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_11522(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_join[3], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_split[3], i)) ; 
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_join[3], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_split[3], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_join[3], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_split[3], i)) ; 
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_join[3], peek_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_split[3], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_split[3]) ; 
			pop_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11517() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11514WEIGHTED_ROUND_ROBIN_Splitter_11517));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11518() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11518WEIGHTED_ROUND_ROBIN_Splitter_11523, pop_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_11525(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_join[0], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[0], i)) ; 
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_join[0], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_join[0], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[0], i)) ; 
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_join[0], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[0]) ; 
			pop_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_11526(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_join[1], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[1], i)) ; 
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_join[1], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_join[1], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[1], i)) ; 
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_join[1], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[1]) ; 
			pop_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_11527(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_join[2], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[2], i)) ; 
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_join[2], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[2], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_join[2], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[2], i)) ; 
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_join[2], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[2], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[2]) ; 
			pop_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_11528(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_join[3], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[3], i)) ; 
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_join[3], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[3], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_join[3], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[3], i)) ; 
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_join[3], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[3], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[3]) ; 
			pop_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_11529(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_join[4], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[4], i)) ; 
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_join[4], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[4], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_join[4], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[4], i)) ; 
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_join[4], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[4], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[4]) ; 
			pop_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_11530(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_join[5], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[5], i)) ; 
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_join[5], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[5], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_join[5], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[5], i)) ; 
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_join[5], peek_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[5], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[5]) ; 
			pop_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11523() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11518WEIGHTED_ROUND_ROBIN_Splitter_11523));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11524() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11524WEIGHTED_ROUND_ROBIN_Splitter_11531, pop_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_11533(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_join[0], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[0], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_join[0], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_join[0], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[0], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_join[0], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[0]) ; 
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_11534(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_join[1], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[1], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_join[1], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_join[1], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[1], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_join[1], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[1]) ; 
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_11535(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_join[2], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[2], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_join[2], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[2], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_join[2], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[2], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_join[2], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[2], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[2]) ; 
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_11536(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_join[3], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[3], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_join[3], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[3], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_join[3], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[3], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_join[3], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[3], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[3]) ; 
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_11537(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_join[4], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[4], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_join[4], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[4], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_join[4], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[4], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_join[4], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[4], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[4]) ; 
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_11538(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_join[5], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[5], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_join[5], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[5], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_join[5], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[5], i)) ; 
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_join[5], peek_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[5], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[5]) ; 
			pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11531() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11524WEIGHTED_ROUND_ROBIN_Splitter_11531));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11532() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11532WEIGHTED_ROUND_ROBIN_Splitter_11539, pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_11541(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[0], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[0], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[0], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[0], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11541_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11541_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[0]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11542(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[1], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[1], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[1], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[1], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11542_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11542_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[1]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11543(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[2], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[2], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[2], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[2], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11543_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11543_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[2]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11544(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[3], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[3], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[3], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[3], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11544_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11544_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[3]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11545(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[4], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[4], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[4], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[4], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11545_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11545_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[4]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11546(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[5], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[5], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[5], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[5], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11546_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11546_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[5]) ; 
			push_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11539() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11532WEIGHTED_ROUND_ROBIN_Splitter_11539));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11540() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11540WEIGHTED_ROUND_ROBIN_Splitter_11547, pop_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_11549(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[0], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[0], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[0], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[0], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11549_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11549_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[0]) ; 
			push_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11550(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[1], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[1], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[1], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[1], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11550_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11550_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[1]) ; 
			push_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11551(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[2], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[2], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[2], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[2], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11551_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11551_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[2]) ; 
			push_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11552(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[3], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[3], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[3], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[3], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11552_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11552_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[3]) ; 
			push_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11553(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[4], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[4], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[4], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[4], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11553_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11553_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[4]) ; 
			push_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11554(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[5], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[5], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[5], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[5], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11554_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11554_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[5]) ; 
			push_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11547() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11540WEIGHTED_ROUND_ROBIN_Splitter_11547));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11548() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11548WEIGHTED_ROUND_ROBIN_Splitter_11555, pop_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_11557(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float results[16];
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[0], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[0], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[0], (8 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[0], (8 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11557_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11557_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(8 + i)] = (y0_r - y1w_r) ; 
			results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[0]) ; 
			push_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11558(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float results[16];
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[1], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[1], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[1], (8 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[1], (8 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11558_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11558_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(8 + i)] = (y0_r - y1w_r) ; 
			results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[1]) ; 
			push_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11559(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float results[16];
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[2], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[2], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[2], (8 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[2], (8 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11559_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11559_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(8 + i)] = (y0_r - y1w_r) ; 
			results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[2]) ; 
			push_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11560(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float results[16];
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[3], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[3], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[3], (8 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[3], (8 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11560_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11560_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(8 + i)] = (y0_r - y1w_r) ; 
			results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[3]) ; 
			push_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11561(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float results[16];
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[4], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[4], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[4], (8 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[4], (8 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11561_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11561_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(8 + i)] = (y0_r - y1w_r) ; 
			results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[4]) ; 
			push_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11562(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float results[16];
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[5], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[5], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[5], (8 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[5], (8 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11562_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11562_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(8 + i)] = (y0_r - y1w_r) ; 
			results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[5]) ; 
			push_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11555() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11548WEIGHTED_ROUND_ROBIN_Splitter_11555));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11556() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11556WEIGHTED_ROUND_ROBIN_Splitter_11563, pop_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_11565(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		float results[32];
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin18_CombineDFT_Fiss_11642_11663_split[0], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin18_CombineDFT_Fiss_11642_11663_split[0], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin18_CombineDFT_Fiss_11642_11663_split[0], (16 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin18_CombineDFT_Fiss_11642_11663_split[0], (16 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11565_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11565_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(16 + i)] = (y0_r - y1w_r) ; 
			results[((16 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&SplitJoin18_CombineDFT_Fiss_11642_11663_split[0]) ; 
			push_float(&SplitJoin18_CombineDFT_Fiss_11642_11663_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11566(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		float results[32];
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin18_CombineDFT_Fiss_11642_11663_split[1], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin18_CombineDFT_Fiss_11642_11663_split[1], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin18_CombineDFT_Fiss_11642_11663_split[1], (16 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin18_CombineDFT_Fiss_11642_11663_split[1], (16 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11566_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11566_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(16 + i)] = (y0_r - y1w_r) ; 
			results[((16 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&SplitJoin18_CombineDFT_Fiss_11642_11663_split[1]) ; 
			push_float(&SplitJoin18_CombineDFT_Fiss_11642_11663_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11567(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		float results[32];
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin18_CombineDFT_Fiss_11642_11663_split[2], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin18_CombineDFT_Fiss_11642_11663_split[2], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin18_CombineDFT_Fiss_11642_11663_split[2], (16 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin18_CombineDFT_Fiss_11642_11663_split[2], (16 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11567_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11567_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(16 + i)] = (y0_r - y1w_r) ; 
			results[((16 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&SplitJoin18_CombineDFT_Fiss_11642_11663_split[2]) ; 
			push_float(&SplitJoin18_CombineDFT_Fiss_11642_11663_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11568(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		float results[32];
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin18_CombineDFT_Fiss_11642_11663_split[3], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin18_CombineDFT_Fiss_11642_11663_split[3], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin18_CombineDFT_Fiss_11642_11663_split[3], (16 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin18_CombineDFT_Fiss_11642_11663_split[3], (16 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11568_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11568_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(16 + i)] = (y0_r - y1w_r) ; 
			results[((16 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&SplitJoin18_CombineDFT_Fiss_11642_11663_split[3]) ; 
			push_float(&SplitJoin18_CombineDFT_Fiss_11642_11663_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11563() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin18_CombineDFT_Fiss_11642_11663_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11556WEIGHTED_ROUND_ROBIN_Splitter_11563));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11564() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11564WEIGHTED_ROUND_ROBIN_Splitter_11569, pop_float(&SplitJoin18_CombineDFT_Fiss_11642_11663_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_11571(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		float results[64];
		FOR(int, i, 0,  < , 32, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin20_CombineDFT_Fiss_11643_11664_split[0], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin20_CombineDFT_Fiss_11643_11664_split[0], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin20_CombineDFT_Fiss_11643_11664_split[0], (32 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin20_CombineDFT_Fiss_11643_11664_split[0], (32 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11571_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11571_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(32 + i)] = (y0_r - y1w_r) ; 
			results[((32 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_float(&SplitJoin20_CombineDFT_Fiss_11643_11664_split[0]) ; 
			push_float(&SplitJoin20_CombineDFT_Fiss_11643_11664_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11572(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		float results[64];
		FOR(int, i, 0,  < , 32, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin20_CombineDFT_Fiss_11643_11664_split[1], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin20_CombineDFT_Fiss_11643_11664_split[1], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin20_CombineDFT_Fiss_11643_11664_split[1], (32 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin20_CombineDFT_Fiss_11643_11664_split[1], (32 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11572_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11572_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(32 + i)] = (y0_r - y1w_r) ; 
			results[((32 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_float(&SplitJoin20_CombineDFT_Fiss_11643_11664_split[1]) ; 
			push_float(&SplitJoin20_CombineDFT_Fiss_11643_11664_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11569() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_11643_11664_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11564WEIGHTED_ROUND_ROBIN_Splitter_11569));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_11643_11664_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11564WEIGHTED_ROUND_ROBIN_Splitter_11569));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11570() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11570CombineDFT_11488, pop_float(&SplitJoin20_CombineDFT_Fiss_11643_11664_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11570CombineDFT_11488, pop_float(&SplitJoin20_CombineDFT_Fiss_11643_11664_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_11488(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		float results[128];
		FOR(int, i, 0,  < , 64, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_11570CombineDFT_11488, i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_11570CombineDFT_11488, i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_11570CombineDFT_11488, (64 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_11570CombineDFT_11488, (64 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11488_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11488_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(64 + i)] = (y0_r - y1w_r) ; 
			results[((64 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 128, i++) {
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11570CombineDFT_11488) ; 
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11467_11503_11634_11655_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_11489(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, i, 0,  < , 128, i = (i + 4)) {
			push_float(&FFTReorderSimple_11489WEIGHTED_ROUND_ROBIN_Splitter_11573, peek_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11467_11503_11634_11655_split[1], i)) ; 
			push_float(&FFTReorderSimple_11489WEIGHTED_ROUND_ROBIN_Splitter_11573, peek_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11467_11503_11634_11655_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 128, i = (i + 4)) {
			push_float(&FFTReorderSimple_11489WEIGHTED_ROUND_ROBIN_Splitter_11573, peek_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11467_11503_11634_11655_split[1], i)) ; 
			push_float(&FFTReorderSimple_11489WEIGHTED_ROUND_ROBIN_Splitter_11573, peek_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11467_11503_11634_11655_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11467_11503_11634_11655_split[1]) ; 
			pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11467_11503_11634_11655_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_11575(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i = (i + 4)) {
			push_float(&SplitJoin56_FFTReorderSimple_Fiss_11644_11665_join[0], peek_float(&SplitJoin56_FFTReorderSimple_Fiss_11644_11665_split[0], i)) ; 
			push_float(&SplitJoin56_FFTReorderSimple_Fiss_11644_11665_join[0], peek_float(&SplitJoin56_FFTReorderSimple_Fiss_11644_11665_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 64, i = (i + 4)) {
			push_float(&SplitJoin56_FFTReorderSimple_Fiss_11644_11665_join[0], peek_float(&SplitJoin56_FFTReorderSimple_Fiss_11644_11665_split[0], i)) ; 
			push_float(&SplitJoin56_FFTReorderSimple_Fiss_11644_11665_join[0], peek_float(&SplitJoin56_FFTReorderSimple_Fiss_11644_11665_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&SplitJoin56_FFTReorderSimple_Fiss_11644_11665_split[0]) ; 
			pop_float(&SplitJoin56_FFTReorderSimple_Fiss_11644_11665_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_11576(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i = (i + 4)) {
			push_float(&SplitJoin56_FFTReorderSimple_Fiss_11644_11665_join[1], peek_float(&SplitJoin56_FFTReorderSimple_Fiss_11644_11665_split[1], i)) ; 
			push_float(&SplitJoin56_FFTReorderSimple_Fiss_11644_11665_join[1], peek_float(&SplitJoin56_FFTReorderSimple_Fiss_11644_11665_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 64, i = (i + 4)) {
			push_float(&SplitJoin56_FFTReorderSimple_Fiss_11644_11665_join[1], peek_float(&SplitJoin56_FFTReorderSimple_Fiss_11644_11665_split[1], i)) ; 
			push_float(&SplitJoin56_FFTReorderSimple_Fiss_11644_11665_join[1], peek_float(&SplitJoin56_FFTReorderSimple_Fiss_11644_11665_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&SplitJoin56_FFTReorderSimple_Fiss_11644_11665_split[1]) ; 
			pop_float(&SplitJoin56_FFTReorderSimple_Fiss_11644_11665_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11573() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin56_FFTReorderSimple_Fiss_11644_11665_split[0], pop_float(&FFTReorderSimple_11489WEIGHTED_ROUND_ROBIN_Splitter_11573));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin56_FFTReorderSimple_Fiss_11644_11665_split[1], pop_float(&FFTReorderSimple_11489WEIGHTED_ROUND_ROBIN_Splitter_11573));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11574() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11574WEIGHTED_ROUND_ROBIN_Splitter_11577, pop_float(&SplitJoin56_FFTReorderSimple_Fiss_11644_11665_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11574WEIGHTED_ROUND_ROBIN_Splitter_11577, pop_float(&SplitJoin56_FFTReorderSimple_Fiss_11644_11665_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_11579(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_join[0], peek_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_split[0], i)) ; 
			push_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_join[0], peek_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_join[0], peek_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_split[0], i)) ; 
			push_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_join[0], peek_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_split[0]) ; 
			pop_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_11580(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_join[1], peek_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_split[1], i)) ; 
			push_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_join[1], peek_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_join[1], peek_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_split[1], i)) ; 
			push_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_join[1], peek_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_split[1]) ; 
			pop_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_11581(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_join[2], peek_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_split[2], i)) ; 
			push_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_join[2], peek_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_split[2], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_join[2], peek_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_split[2], i)) ; 
			push_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_join[2], peek_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_split[2], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_split[2]) ; 
			pop_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_11582(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_join[3], peek_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_split[3], i)) ; 
			push_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_join[3], peek_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_split[3], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 32, i = (i + 4)) {
			push_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_join[3], peek_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_split[3], i)) ; 
			push_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_join[3], peek_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_split[3], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_split[3]) ; 
			pop_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11577() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11574WEIGHTED_ROUND_ROBIN_Splitter_11577));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11578() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11578WEIGHTED_ROUND_ROBIN_Splitter_11583, pop_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_11585(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_join[0], peek_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[0], i)) ; 
			push_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_join[0], peek_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_join[0], peek_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[0], i)) ; 
			push_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_join[0], peek_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[0]) ; 
			pop_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_11586(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_join[1], peek_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[1], i)) ; 
			push_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_join[1], peek_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_join[1], peek_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[1], i)) ; 
			push_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_join[1], peek_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[1]) ; 
			pop_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_11587(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_join[2], peek_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[2], i)) ; 
			push_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_join[2], peek_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[2], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_join[2], peek_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[2], i)) ; 
			push_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_join[2], peek_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[2], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[2]) ; 
			pop_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_11588(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_join[3], peek_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[3], i)) ; 
			push_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_join[3], peek_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[3], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_join[3], peek_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[3], i)) ; 
			push_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_join[3], peek_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[3], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[3]) ; 
			pop_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_11589(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_join[4], peek_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[4], i)) ; 
			push_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_join[4], peek_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[4], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_join[4], peek_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[4], i)) ; 
			push_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_join[4], peek_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[4], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[4]) ; 
			pop_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_11590(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_join[5], peek_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[5], i)) ; 
			push_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_join[5], peek_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[5], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 16, i = (i + 4)) {
			push_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_join[5], peek_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[5], i)) ; 
			push_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_join[5], peek_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[5], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[5]) ; 
			pop_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11583() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11578WEIGHTED_ROUND_ROBIN_Splitter_11583));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11584() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11584WEIGHTED_ROUND_ROBIN_Splitter_11591, pop_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_11593(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_join[0], peek_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[0], i)) ; 
			push_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_join[0], peek_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_join[0], peek_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[0], i)) ; 
			push_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_join[0], peek_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[0], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[0]) ; 
			pop_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_11594(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_join[1], peek_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[1], i)) ; 
			push_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_join[1], peek_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_join[1], peek_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[1], i)) ; 
			push_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_join[1], peek_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[1], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[1]) ; 
			pop_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_11595(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_join[2], peek_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[2], i)) ; 
			push_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_join[2], peek_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[2], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_join[2], peek_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[2], i)) ; 
			push_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_join[2], peek_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[2], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[2]) ; 
			pop_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_11596(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_join[3], peek_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[3], i)) ; 
			push_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_join[3], peek_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[3], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_join[3], peek_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[3], i)) ; 
			push_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_join[3], peek_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[3], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[3]) ; 
			pop_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_11597(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_join[4], peek_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[4], i)) ; 
			push_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_join[4], peek_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[4], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_join[4], peek_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[4], i)) ; 
			push_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_join[4], peek_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[4], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[4]) ; 
			pop_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_11598(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_join[5], peek_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[5], i)) ; 
			push_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_join[5], peek_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[5], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 8, i = (i + 4)) {
			push_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_join[5], peek_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[5], i)) ; 
			push_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_join[5], peek_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[5], (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[5]) ; 
			pop_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11591() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11584WEIGHTED_ROUND_ROBIN_Splitter_11591));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11592() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11592WEIGHTED_ROUND_ROBIN_Splitter_11599, pop_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_11601(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[0], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[0], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[0], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[0], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11601_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11601_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[0]) ; 
			push_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11602(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[1], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[1], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[1], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[1], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11602_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11602_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[1]) ; 
			push_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11603(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[2], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[2], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[2], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[2], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11603_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11603_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[2]) ; 
			push_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11604(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[3], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[3], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[3], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[3], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11604_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11604_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[3]) ; 
			push_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11605(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[4], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[4], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[4], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[4], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11605_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11605_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[4]) ; 
			push_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11606(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[5], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[5], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[5], (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[5], (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11606_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11606_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[5]) ; 
			push_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11599() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11592WEIGHTED_ROUND_ROBIN_Splitter_11599));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11600() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11600WEIGHTED_ROUND_ROBIN_Splitter_11607, pop_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_11609(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[0], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[0], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[0], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[0], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11609_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11609_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[0]) ; 
			push_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11610(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[1], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[1], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[1], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[1], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11610_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11610_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[1]) ; 
			push_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11611(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[2], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[2], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[2], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[2], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11611_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11611_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[2]) ; 
			push_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11612(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[3], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[3], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[3], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[3], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11612_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11612_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[3]) ; 
			push_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11613(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[4], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[4], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[4], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[4], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11613_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11613_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[4]) ; 
			push_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11614(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		float results[8];
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[5], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[5], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[5], (4 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[5], (4 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11614_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11614_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(4 + i)] = (y0_r - y1w_r) ; 
			results[((4 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[5]) ; 
			push_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11607() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11600WEIGHTED_ROUND_ROBIN_Splitter_11607));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11608() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11608WEIGHTED_ROUND_ROBIN_Splitter_11615, pop_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_11617(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float results[16];
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[0], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[0], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[0], (8 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[0], (8 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11617_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11617_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(8 + i)] = (y0_r - y1w_r) ; 
			results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[0]) ; 
			push_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11618(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float results[16];
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[1], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[1], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[1], (8 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[1], (8 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11618_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11618_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(8 + i)] = (y0_r - y1w_r) ; 
			results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[1]) ; 
			push_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11619(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float results[16];
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[2], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[2], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[2], (8 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[2], (8 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11619_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11619_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(8 + i)] = (y0_r - y1w_r) ; 
			results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[2]) ; 
			push_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11620(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float results[16];
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[3], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[3], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[3], (8 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[3], (8 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11620_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11620_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(8 + i)] = (y0_r - y1w_r) ; 
			results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[3]) ; 
			push_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11621(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float results[16];
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[4], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[4], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[4], (8 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[4], (8 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11621_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11621_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(8 + i)] = (y0_r - y1w_r) ; 
			results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[4]) ; 
			push_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11622(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float results[16];
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[5], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[5], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[5], (8 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[5], (8 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11622_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11622_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(8 + i)] = (y0_r - y1w_r) ; 
			results[((8 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[5]) ; 
			push_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11615() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11608WEIGHTED_ROUND_ROBIN_Splitter_11615));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11616() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 6, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11616WEIGHTED_ROUND_ROBIN_Splitter_11623, pop_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_11625(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		float results[32];
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin70_CombineDFT_Fiss_11651_11672_split[0], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin70_CombineDFT_Fiss_11651_11672_split[0], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin70_CombineDFT_Fiss_11651_11672_split[0], (16 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin70_CombineDFT_Fiss_11651_11672_split[0], (16 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11625_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11625_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(16 + i)] = (y0_r - y1w_r) ; 
			results[((16 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&SplitJoin70_CombineDFT_Fiss_11651_11672_split[0]) ; 
			push_float(&SplitJoin70_CombineDFT_Fiss_11651_11672_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11626(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		float results[32];
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin70_CombineDFT_Fiss_11651_11672_split[1], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin70_CombineDFT_Fiss_11651_11672_split[1], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin70_CombineDFT_Fiss_11651_11672_split[1], (16 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin70_CombineDFT_Fiss_11651_11672_split[1], (16 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11626_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11626_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(16 + i)] = (y0_r - y1w_r) ; 
			results[((16 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&SplitJoin70_CombineDFT_Fiss_11651_11672_split[1]) ; 
			push_float(&SplitJoin70_CombineDFT_Fiss_11651_11672_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11627(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		float results[32];
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin70_CombineDFT_Fiss_11651_11672_split[2], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin70_CombineDFT_Fiss_11651_11672_split[2], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin70_CombineDFT_Fiss_11651_11672_split[2], (16 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin70_CombineDFT_Fiss_11651_11672_split[2], (16 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11627_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11627_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(16 + i)] = (y0_r - y1w_r) ; 
			results[((16 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&SplitJoin70_CombineDFT_Fiss_11651_11672_split[2]) ; 
			push_float(&SplitJoin70_CombineDFT_Fiss_11651_11672_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11628(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		float results[32];
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin70_CombineDFT_Fiss_11651_11672_split[3], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin70_CombineDFT_Fiss_11651_11672_split[3], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin70_CombineDFT_Fiss_11651_11672_split[3], (16 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin70_CombineDFT_Fiss_11651_11672_split[3], (16 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11628_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11628_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(16 + i)] = (y0_r - y1w_r) ; 
			results[((16 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&SplitJoin70_CombineDFT_Fiss_11651_11672_split[3]) ; 
			push_float(&SplitJoin70_CombineDFT_Fiss_11651_11672_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11623() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin70_CombineDFT_Fiss_11651_11672_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11616WEIGHTED_ROUND_ROBIN_Splitter_11623));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11624() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11624WEIGHTED_ROUND_ROBIN_Splitter_11629, pop_float(&SplitJoin70_CombineDFT_Fiss_11651_11672_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_11631(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		float results[64];
		FOR(int, i, 0,  < , 32, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin72_CombineDFT_Fiss_11652_11673_split[0], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin72_CombineDFT_Fiss_11652_11673_split[0], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin72_CombineDFT_Fiss_11652_11673_split[0], (32 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin72_CombineDFT_Fiss_11652_11673_split[0], (32 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11631_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11631_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(32 + i)] = (y0_r - y1w_r) ; 
			results[((32 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_float(&SplitJoin72_CombineDFT_Fiss_11652_11673_split[0]) ; 
			push_float(&SplitJoin72_CombineDFT_Fiss_11652_11673_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_11632(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		float results[64];
		FOR(int, i, 0,  < , 32, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&SplitJoin72_CombineDFT_Fiss_11652_11673_split[1], i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&SplitJoin72_CombineDFT_Fiss_11652_11673_split[1], i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&SplitJoin72_CombineDFT_Fiss_11652_11673_split[1], (32 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&SplitJoin72_CombineDFT_Fiss_11652_11673_split[1], (32 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11632_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11632_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(32 + i)] = (y0_r - y1w_r) ; 
			results[((32 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_float(&SplitJoin72_CombineDFT_Fiss_11652_11673_split[1]) ; 
			push_float(&SplitJoin72_CombineDFT_Fiss_11652_11673_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11629() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin72_CombineDFT_Fiss_11652_11673_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11624WEIGHTED_ROUND_ROBIN_Splitter_11629));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin72_CombineDFT_Fiss_11652_11673_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11624WEIGHTED_ROUND_ROBIN_Splitter_11629));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11630() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11630CombineDFT_11499, pop_float(&SplitJoin72_CombineDFT_Fiss_11652_11673_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11630CombineDFT_11499, pop_float(&SplitJoin72_CombineDFT_Fiss_11652_11673_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_11499(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		float results[128];
		FOR(int, i, 0,  < , 64, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_11630CombineDFT_11499, i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_11630CombineDFT_11499, i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_11630CombineDFT_11499, (64 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_11630CombineDFT_11499, (64 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11499_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11499_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(64 + i)] = (y0_r - y1w_r) ; 
			results[((64 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 128, i++) {
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11630CombineDFT_11499) ; 
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11467_11503_11634_11655_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11501() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11467_11503_11634_11655_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11510WEIGHTED_ROUND_ROBIN_Splitter_11501));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11467_11503_11634_11655_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11510WEIGHTED_ROUND_ROBIN_Splitter_11501));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11502() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11502FloatPrinter_11500, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11467_11503_11634_11655_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11502FloatPrinter_11500, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11467_11503_11634_11655_join[1]));
		ENDFOR
	ENDFOR
}}

void FloatPrinter_11500(){
	FOR(uint32_t, __iter_steady_, 0, <, 768, __iter_steady_++) {
		printf("%.10f", pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11502FloatPrinter_11500));
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11608WEIGHTED_ROUND_ROBIN_Splitter_11615);
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_float(&SplitJoin72_CombineDFT_Fiss_11652_11673_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 6, __iter_init_1_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_join[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11556WEIGHTED_ROUND_ROBIN_Splitter_11563);
	FOR(int, __iter_init_2_, 0, <, 6, __iter_init_2_++)
		init_buffer_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_split[__iter_init_2_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11502FloatPrinter_11500);
	FOR(int, __iter_init_3_, 0, <, 6, __iter_init_3_++)
		init_buffer_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_join[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11532WEIGHTED_ROUND_ROBIN_Splitter_11539);
	FOR(int, __iter_init_4_, 0, <, 4, __iter_init_4_++)
		init_buffer_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 6, __iter_init_5_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 2, __iter_init_6_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11467_11503_11634_11655_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 4, __iter_init_7_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 6, __iter_init_8_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 6, __iter_init_9_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_join[__iter_init_9_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11574WEIGHTED_ROUND_ROBIN_Splitter_11577);
	FOR(int, __iter_init_10_, 0, <, 4, __iter_init_10_++)
		init_buffer_float(&SplitJoin58_FFTReorderSimple_Fiss_11645_11666_split[__iter_init_10_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11570CombineDFT_11488);
	FOR(int, __iter_init_11_, 0, <, 6, __iter_init_11_++)
		init_buffer_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_split[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11518WEIGHTED_ROUND_ROBIN_Splitter_11523);
	FOR(int, __iter_init_12_, 0, <, 2, __iter_init_12_++)
		init_buffer_float(&SplitJoin56_FFTReorderSimple_Fiss_11644_11665_join[__iter_init_12_]);
	ENDFOR
	init_buffer_float(&FFTReorderSimple_11489WEIGHTED_ROUND_ROBIN_Splitter_11573);
	FOR(int, __iter_init_13_, 0, <, 6, __iter_init_13_++)
		init_buffer_float(&SplitJoin64_CombineDFT_Fiss_11648_11669_join[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 6, __iter_init_14_++)
		init_buffer_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_split[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 2, __iter_init_15_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_11635_11656_split[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 6, __iter_init_16_++)
		init_buffer_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_join[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 4, __iter_init_17_++)
		init_buffer_float(&SplitJoin70_CombineDFT_Fiss_11651_11672_join[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 2, __iter_init_18_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_11633_11654_join[__iter_init_18_]);
	ENDFOR
	init_buffer_float(&FFTReorderSimple_11478WEIGHTED_ROUND_ROBIN_Splitter_11513);
	FOR(int, __iter_init_19_, 0, <, 6, __iter_init_19_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_11640_11661_split[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 4, __iter_init_20_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_11642_11663_split[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 2, __iter_init_21_++)
		init_buffer_float(&SplitJoin56_FFTReorderSimple_Fiss_11644_11665_split[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 2, __iter_init_22_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11467_11503_11634_11655_split[__iter_init_22_]);
	ENDFOR
	FOR(int, __iter_init_23_, 0, <, 4, __iter_init_23_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_11636_11657_join[__iter_init_23_]);
	ENDFOR
	FOR(int, __iter_init_24_, 0, <, 6, __iter_init_24_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_join[__iter_init_24_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11584WEIGHTED_ROUND_ROBIN_Splitter_11591);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11514WEIGHTED_ROUND_ROBIN_Splitter_11517);
	FOR(int, __iter_init_25_, 0, <, 4, __iter_init_25_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_11642_11663_join[__iter_init_25_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11616WEIGHTED_ROUND_ROBIN_Splitter_11623);
	FOR(int, __iter_init_26_, 0, <, 2, __iter_init_26_++)
		init_buffer_float(&SplitJoin72_CombineDFT_Fiss_11652_11673_split[__iter_init_26_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11630CombineDFT_11499);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11624WEIGHTED_ROUND_ROBIN_Splitter_11629);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11578WEIGHTED_ROUND_ROBIN_Splitter_11583);
	FOR(int, __iter_init_27_, 0, <, 6, __iter_init_27_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_11637_11658_split[__iter_init_27_]);
	ENDFOR
	FOR(int, __iter_init_28_, 0, <, 2, __iter_init_28_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_11643_11664_join[__iter_init_28_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11592WEIGHTED_ROUND_ROBIN_Splitter_11599);
	FOR(int, __iter_init_29_, 0, <, 6, __iter_init_29_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_11641_11662_join[__iter_init_29_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11600WEIGHTED_ROUND_ROBIN_Splitter_11607);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11524WEIGHTED_ROUND_ROBIN_Splitter_11531);
	FOR(int, __iter_init_30_, 0, <, 4, __iter_init_30_++)
		init_buffer_float(&SplitJoin70_CombineDFT_Fiss_11651_11672_split[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 2, __iter_init_31_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_11633_11654_split[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 2, __iter_init_32_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_11635_11656_join[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 6, __iter_init_33_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_11639_11660_split[__iter_init_33_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11548WEIGHTED_ROUND_ROBIN_Splitter_11555);
	FOR(int, __iter_init_34_, 0, <, 6, __iter_init_34_++)
		init_buffer_float(&SplitJoin62_FFTReorderSimple_Fiss_11647_11668_join[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 6, __iter_init_35_++)
		init_buffer_float(&SplitJoin68_CombineDFT_Fiss_11650_11671_split[__iter_init_35_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11540WEIGHTED_ROUND_ROBIN_Splitter_11547);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11510WEIGHTED_ROUND_ROBIN_Splitter_11501);
	FOR(int, __iter_init_36_, 0, <, 2, __iter_init_36_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_11643_11664_split[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 6, __iter_init_37_++)
		init_buffer_float(&SplitJoin66_CombineDFT_Fiss_11649_11670_join[__iter_init_37_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11564WEIGHTED_ROUND_ROBIN_Splitter_11569);
	FOR(int, __iter_init_38_, 0, <, 6, __iter_init_38_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_11638_11659_split[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 6, __iter_init_39_++)
		init_buffer_float(&SplitJoin60_FFTReorderSimple_Fiss_11646_11667_split[__iter_init_39_]);
	ENDFOR
// --- init: CombineDFT_11541
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11541_s.w[i] = real ; 
		CombineDFT_11541_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11542
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11542_s.w[i] = real ; 
		CombineDFT_11542_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11543
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11543_s.w[i] = real ; 
		CombineDFT_11543_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11544
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11544_s.w[i] = real ; 
		CombineDFT_11544_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11545
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11545_s.w[i] = real ; 
		CombineDFT_11545_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11546
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11546_s.w[i] = real ; 
		CombineDFT_11546_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11549
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11549_s.w[i] = real ; 
		CombineDFT_11549_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11550
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11550_s.w[i] = real ; 
		CombineDFT_11550_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11551
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11551_s.w[i] = real ; 
		CombineDFT_11551_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11552
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11552_s.w[i] = real ; 
		CombineDFT_11552_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11553
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11553_s.w[i] = real ; 
		CombineDFT_11553_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11554
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11554_s.w[i] = real ; 
		CombineDFT_11554_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11557
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11557_s.w[i] = real ; 
		CombineDFT_11557_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11558
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11558_s.w[i] = real ; 
		CombineDFT_11558_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11559
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11559_s.w[i] = real ; 
		CombineDFT_11559_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11560
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11560_s.w[i] = real ; 
		CombineDFT_11560_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11561
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11561_s.w[i] = real ; 
		CombineDFT_11561_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11562
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11562_s.w[i] = real ; 
		CombineDFT_11562_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11565
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_11565_s.w[i] = real ; 
		CombineDFT_11565_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11566
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_11566_s.w[i] = real ; 
		CombineDFT_11566_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11567
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_11567_s.w[i] = real ; 
		CombineDFT_11567_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11568
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_11568_s.w[i] = real ; 
		CombineDFT_11568_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11571
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_11571_s.w[i] = real ; 
		CombineDFT_11571_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11572
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_11572_s.w[i] = real ; 
		CombineDFT_11572_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11488
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_11488_s.w[i] = real ; 
		CombineDFT_11488_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11601
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11601_s.w[i] = real ; 
		CombineDFT_11601_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11602
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11602_s.w[i] = real ; 
		CombineDFT_11602_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11603
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11603_s.w[i] = real ; 
		CombineDFT_11603_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11604
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11604_s.w[i] = real ; 
		CombineDFT_11604_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11605
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11605_s.w[i] = real ; 
		CombineDFT_11605_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11606
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11606_s.w[i] = real ; 
		CombineDFT_11606_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11609
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11609_s.w[i] = real ; 
		CombineDFT_11609_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11610
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11610_s.w[i] = real ; 
		CombineDFT_11610_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11611
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11611_s.w[i] = real ; 
		CombineDFT_11611_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11612
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11612_s.w[i] = real ; 
		CombineDFT_11612_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11613
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11613_s.w[i] = real ; 
		CombineDFT_11613_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11614
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11614_s.w[i] = real ; 
		CombineDFT_11614_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11617
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11617_s.w[i] = real ; 
		CombineDFT_11617_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11618
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11618_s.w[i] = real ; 
		CombineDFT_11618_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11619
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11619_s.w[i] = real ; 
		CombineDFT_11619_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11620
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11620_s.w[i] = real ; 
		CombineDFT_11620_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11621
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11621_s.w[i] = real ; 
		CombineDFT_11621_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11622
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11622_s.w[i] = real ; 
		CombineDFT_11622_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11625
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_11625_s.w[i] = real ; 
		CombineDFT_11625_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11626
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_11626_s.w[i] = real ; 
		CombineDFT_11626_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11627
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_11627_s.w[i] = real ; 
		CombineDFT_11627_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11628
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_11628_s.w[i] = real ; 
		CombineDFT_11628_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11631
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_11631_s.w[i] = real ; 
		CombineDFT_11631_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11632
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_11632_s.w[i] = real ; 
		CombineDFT_11632_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11499
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_11499_s.w[i] = real ; 
		CombineDFT_11499_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_11509();
			FFTTestSource_11511();
			FFTTestSource_11512();
		WEIGHTED_ROUND_ROBIN_Joiner_11510();
		WEIGHTED_ROUND_ROBIN_Splitter_11501();
			FFTReorderSimple_11478();
			WEIGHTED_ROUND_ROBIN_Splitter_11513();
				FFTReorderSimple_11515();
				FFTReorderSimple_11516();
			WEIGHTED_ROUND_ROBIN_Joiner_11514();
			WEIGHTED_ROUND_ROBIN_Splitter_11517();
				FFTReorderSimple_11519();
				FFTReorderSimple_11520();
				FFTReorderSimple_11521();
				FFTReorderSimple_11522();
			WEIGHTED_ROUND_ROBIN_Joiner_11518();
			WEIGHTED_ROUND_ROBIN_Splitter_11523();
				FFTReorderSimple_11525();
				FFTReorderSimple_11526();
				FFTReorderSimple_11527();
				FFTReorderSimple_11528();
				FFTReorderSimple_11529();
				FFTReorderSimple_11530();
			WEIGHTED_ROUND_ROBIN_Joiner_11524();
			WEIGHTED_ROUND_ROBIN_Splitter_11531();
				FFTReorderSimple_11533();
				FFTReorderSimple_11534();
				FFTReorderSimple_11535();
				FFTReorderSimple_11536();
				FFTReorderSimple_11537();
				FFTReorderSimple_11538();
			WEIGHTED_ROUND_ROBIN_Joiner_11532();
			WEIGHTED_ROUND_ROBIN_Splitter_11539();
				CombineDFT_11541();
				CombineDFT_11542();
				CombineDFT_11543();
				CombineDFT_11544();
				CombineDFT_11545();
				CombineDFT_11546();
			WEIGHTED_ROUND_ROBIN_Joiner_11540();
			WEIGHTED_ROUND_ROBIN_Splitter_11547();
				CombineDFT_11549();
				CombineDFT_11550();
				CombineDFT_11551();
				CombineDFT_11552();
				CombineDFT_11553();
				CombineDFT_11554();
			WEIGHTED_ROUND_ROBIN_Joiner_11548();
			WEIGHTED_ROUND_ROBIN_Splitter_11555();
				CombineDFT_11557();
				CombineDFT_11558();
				CombineDFT_11559();
				CombineDFT_11560();
				CombineDFT_11561();
				CombineDFT_11562();
			WEIGHTED_ROUND_ROBIN_Joiner_11556();
			WEIGHTED_ROUND_ROBIN_Splitter_11563();
				CombineDFT_11565();
				CombineDFT_11566();
				CombineDFT_11567();
				CombineDFT_11568();
			WEIGHTED_ROUND_ROBIN_Joiner_11564();
			WEIGHTED_ROUND_ROBIN_Splitter_11569();
				CombineDFT_11571();
				CombineDFT_11572();
			WEIGHTED_ROUND_ROBIN_Joiner_11570();
			CombineDFT_11488();
			FFTReorderSimple_11489();
			WEIGHTED_ROUND_ROBIN_Splitter_11573();
				FFTReorderSimple_11575();
				FFTReorderSimple_11576();
			WEIGHTED_ROUND_ROBIN_Joiner_11574();
			WEIGHTED_ROUND_ROBIN_Splitter_11577();
				FFTReorderSimple_11579();
				FFTReorderSimple_11580();
				FFTReorderSimple_11581();
				FFTReorderSimple_11582();
			WEIGHTED_ROUND_ROBIN_Joiner_11578();
			WEIGHTED_ROUND_ROBIN_Splitter_11583();
				FFTReorderSimple_11585();
				FFTReorderSimple_11586();
				FFTReorderSimple_11587();
				FFTReorderSimple_11588();
				FFTReorderSimple_11589();
				FFTReorderSimple_11590();
			WEIGHTED_ROUND_ROBIN_Joiner_11584();
			WEIGHTED_ROUND_ROBIN_Splitter_11591();
				FFTReorderSimple_11593();
				FFTReorderSimple_11594();
				FFTReorderSimple_11595();
				FFTReorderSimple_11596();
				FFTReorderSimple_11597();
				FFTReorderSimple_11598();
			WEIGHTED_ROUND_ROBIN_Joiner_11592();
			WEIGHTED_ROUND_ROBIN_Splitter_11599();
				CombineDFT_11601();
				CombineDFT_11602();
				CombineDFT_11603();
				CombineDFT_11604();
				CombineDFT_11605();
				CombineDFT_11606();
			WEIGHTED_ROUND_ROBIN_Joiner_11600();
			WEIGHTED_ROUND_ROBIN_Splitter_11607();
				CombineDFT_11609();
				CombineDFT_11610();
				CombineDFT_11611();
				CombineDFT_11612();
				CombineDFT_11613();
				CombineDFT_11614();
			WEIGHTED_ROUND_ROBIN_Joiner_11608();
			WEIGHTED_ROUND_ROBIN_Splitter_11615();
				CombineDFT_11617();
				CombineDFT_11618();
				CombineDFT_11619();
				CombineDFT_11620();
				CombineDFT_11621();
				CombineDFT_11622();
			WEIGHTED_ROUND_ROBIN_Joiner_11616();
			WEIGHTED_ROUND_ROBIN_Splitter_11623();
				CombineDFT_11625();
				CombineDFT_11626();
				CombineDFT_11627();
				CombineDFT_11628();
			WEIGHTED_ROUND_ROBIN_Joiner_11624();
			WEIGHTED_ROUND_ROBIN_Splitter_11629();
				CombineDFT_11631();
				CombineDFT_11632();
			WEIGHTED_ROUND_ROBIN_Joiner_11630();
			CombineDFT_11499();
		WEIGHTED_ROUND_ROBIN_Joiner_11502();
		FloatPrinter_11500();
	ENDFOR
	return EXIT_SUCCESS;
}
