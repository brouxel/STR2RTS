#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=128 on the compile command line
#else
#if BUF_SIZEMAX < 128
#error BUF_SIZEMAX too small, it must be at least 128
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	complex_t wn;
} CombineDFT_5219_t;
void FFTTestSource(buffer_complex_t *chanout);
void FFTTestSource_5173();
void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout);
void FFTReorderSimple_5174();
void WEIGHTED_ROUND_ROBIN_Splitter_5187();
void FFTReorderSimple_5189();
void FFTReorderSimple_5190();
void WEIGHTED_ROUND_ROBIN_Joiner_5188();
void WEIGHTED_ROUND_ROBIN_Splitter_5191();
void FFTReorderSimple_5193();
void FFTReorderSimple_5194();
void FFTReorderSimple_5195();
void FFTReorderSimple_5196();
void WEIGHTED_ROUND_ROBIN_Joiner_5192();
void WEIGHTED_ROUND_ROBIN_Splitter_5197();
void FFTReorderSimple_5199();
void FFTReorderSimple_5200();
void FFTReorderSimple_5201();
void FFTReorderSimple_5202();
void FFTReorderSimple_5203();
void FFTReorderSimple_5204();
void FFTReorderSimple_5205();
void FFTReorderSimple_5206();
void WEIGHTED_ROUND_ROBIN_Joiner_5198();
void WEIGHTED_ROUND_ROBIN_Splitter_5207();
void FFTReorderSimple_5209();
void FFTReorderSimple_5210();
void FFTReorderSimple_5211();
void FFTReorderSimple_5212();
void FFTReorderSimple_5213();
void FFTReorderSimple_5214();
void FFTReorderSimple_5215();
void FFTReorderSimple_5216();
void WEIGHTED_ROUND_ROBIN_Joiner_5208();
void WEIGHTED_ROUND_ROBIN_Splitter_5217();
void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout);
void CombineDFT_5219();
void CombineDFT_5220();
void CombineDFT_5221();
void CombineDFT_5222();
void CombineDFT_5223();
void CombineDFT_5224();
void CombineDFT_5225();
void CombineDFT_5226();
void WEIGHTED_ROUND_ROBIN_Joiner_5218();
void WEIGHTED_ROUND_ROBIN_Splitter_5227();
void CombineDFT_5229();
void CombineDFT_5230();
void CombineDFT_5231();
void CombineDFT_5232();
void CombineDFT_5233();
void CombineDFT_5234();
void CombineDFT_5235();
void CombineDFT_5236();
void WEIGHTED_ROUND_ROBIN_Joiner_5228();
void WEIGHTED_ROUND_ROBIN_Splitter_5237();
void CombineDFT_5239();
void CombineDFT_5240();
void CombineDFT_5241();
void CombineDFT_5242();
void CombineDFT_5243();
void CombineDFT_5244();
void CombineDFT_5245();
void CombineDFT_5246();
void WEIGHTED_ROUND_ROBIN_Joiner_5238();
void WEIGHTED_ROUND_ROBIN_Splitter_5247();
void CombineDFT_5249();
void CombineDFT_5250();
void CombineDFT_5251();
void CombineDFT_5252();
void WEIGHTED_ROUND_ROBIN_Joiner_5248();
void WEIGHTED_ROUND_ROBIN_Splitter_5253();
void CombineDFT_5255();
void CombineDFT_5256();
void WEIGHTED_ROUND_ROBIN_Joiner_5254();
void CombineDFT_5184();
void CPrinter(buffer_complex_t *chanin);
void CPrinter_5185();

#ifdef __cplusplus
}
#endif
#endif
