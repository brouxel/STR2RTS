#include "PEG17-FFT2.h"

buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_7226_7262_7463_7484_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7443WEIGHTED_ROUND_ROBIN_Splitter_7452;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7364CombineDFT_7247;
buffer_float_t SplitJoin93_FFTReorderSimple_Fiss_7474_7495_join[4];
buffer_float_t FFTReorderSimple_7237WEIGHTED_ROUND_ROBIN_Splitter_7272;
buffer_float_t SplitJoin107_CombineDFT_Fiss_7481_7502_join[2];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_7226_7262_7463_7484_split[2];
buffer_float_t SplitJoin12_CombineDFT_Fiss_7468_7489_join[17];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7348WEIGHTED_ROUND_ROBIN_Splitter_7357;
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_7466_7487_join[8];
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_7467_7488_split[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7261FloatPrinter_7259;
buffer_float_t SplitJoin16_CombineDFT_Fiss_7470_7491_join[8];
buffer_float_t SplitJoin99_CombineDFT_Fiss_7477_7498_split[17];
buffer_float_t SplitJoin95_FFTReorderSimple_Fiss_7475_7496_join[8];
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_7464_7485_split[2];
buffer_float_t SplitJoin105_CombineDFT_Fiss_7480_7501_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7453WEIGHTED_ROUND_ROBIN_Splitter_7458;
buffer_float_t SplitJoin91_FFTReorderSimple_Fiss_7473_7494_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7269WEIGHTED_ROUND_ROBIN_Splitter_7260;
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_7467_7488_join[16];
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_7465_7486_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7406WEIGHTED_ROUND_ROBIN_Splitter_7424;
buffer_float_t SplitJoin18_CombineDFT_Fiss_7471_7492_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7372WEIGHTED_ROUND_ROBIN_Splitter_7377;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7293WEIGHTED_ROUND_ROBIN_Splitter_7310;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7330WEIGHTED_ROUND_ROBIN_Splitter_7347;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7368WEIGHTED_ROUND_ROBIN_Splitter_7371;
buffer_float_t SplitJoin18_CombineDFT_Fiss_7471_7492_split[4];
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_7466_7487_split[8];
buffer_float_t SplitJoin103_CombineDFT_Fiss_7479_7500_split[8];
buffer_float_t SplitJoin97_FFTReorderSimple_Fiss_7476_7497_join[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7283WEIGHTED_ROUND_ROBIN_Splitter_7292;
buffer_float_t SplitJoin14_CombineDFT_Fiss_7469_7490_split[16];
buffer_float_t SplitJoin97_FFTReorderSimple_Fiss_7476_7497_split[16];
buffer_float_t SplitJoin99_CombineDFT_Fiss_7477_7498_join[17];
buffer_float_t FFTReorderSimple_7248WEIGHTED_ROUND_ROBIN_Splitter_7367;
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_7464_7485_join[2];
buffer_float_t SplitJoin16_CombineDFT_Fiss_7470_7491_split[8];
buffer_float_t SplitJoin95_FFTReorderSimple_Fiss_7475_7496_split[8];
buffer_float_t SplitJoin14_CombineDFT_Fiss_7469_7490_join[16];
buffer_float_t SplitJoin103_CombineDFT_Fiss_7479_7500_join[8];
buffer_float_t SplitJoin101_CombineDFT_Fiss_7478_7499_split[16];
buffer_float_t SplitJoin93_FFTReorderSimple_Fiss_7474_7495_split[4];
buffer_float_t SplitJoin91_FFTReorderSimple_Fiss_7473_7494_split[2];
buffer_float_t SplitJoin101_CombineDFT_Fiss_7478_7499_join[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7311WEIGHTED_ROUND_ROBIN_Splitter_7329;
buffer_float_t SplitJoin0_FFTTestSource_Fiss_7462_7483_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7388WEIGHTED_ROUND_ROBIN_Splitter_7405;
buffer_float_t SplitJoin20_CombineDFT_Fiss_7472_7493_split[2];
buffer_float_t SplitJoin12_CombineDFT_Fiss_7468_7489_split[17];
buffer_float_t SplitJoin107_CombineDFT_Fiss_7481_7502_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7378WEIGHTED_ROUND_ROBIN_Splitter_7387;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7425WEIGHTED_ROUND_ROBIN_Splitter_7442;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7358WEIGHTED_ROUND_ROBIN_Splitter_7363;
buffer_float_t SplitJoin20_CombineDFT_Fiss_7472_7493_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7277WEIGHTED_ROUND_ROBIN_Splitter_7282;
buffer_float_t SplitJoin105_CombineDFT_Fiss_7480_7501_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7459CombineDFT_7258;
buffer_float_t SplitJoin0_FFTTestSource_Fiss_7462_7483_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7273WEIGHTED_ROUND_ROBIN_Splitter_7276;
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_7465_7486_split[4];


CombineDFT_7312_t CombineDFT_7312_s;
CombineDFT_7312_t CombineDFT_7313_s;
CombineDFT_7312_t CombineDFT_7314_s;
CombineDFT_7312_t CombineDFT_7315_s;
CombineDFT_7312_t CombineDFT_7316_s;
CombineDFT_7312_t CombineDFT_7317_s;
CombineDFT_7312_t CombineDFT_7318_s;
CombineDFT_7312_t CombineDFT_7319_s;
CombineDFT_7312_t CombineDFT_7320_s;
CombineDFT_7312_t CombineDFT_7321_s;
CombineDFT_7312_t CombineDFT_7322_s;
CombineDFT_7312_t CombineDFT_7323_s;
CombineDFT_7312_t CombineDFT_7324_s;
CombineDFT_7312_t CombineDFT_7325_s;
CombineDFT_7312_t CombineDFT_7326_s;
CombineDFT_7312_t CombineDFT_7327_s;
CombineDFT_7312_t CombineDFT_7328_s;
CombineDFT_7331_t CombineDFT_7331_s;
CombineDFT_7331_t CombineDFT_7332_s;
CombineDFT_7331_t CombineDFT_7333_s;
CombineDFT_7331_t CombineDFT_7334_s;
CombineDFT_7331_t CombineDFT_7335_s;
CombineDFT_7331_t CombineDFT_7336_s;
CombineDFT_7331_t CombineDFT_7337_s;
CombineDFT_7331_t CombineDFT_7338_s;
CombineDFT_7331_t CombineDFT_7339_s;
CombineDFT_7331_t CombineDFT_7340_s;
CombineDFT_7331_t CombineDFT_7341_s;
CombineDFT_7331_t CombineDFT_7342_s;
CombineDFT_7331_t CombineDFT_7343_s;
CombineDFT_7331_t CombineDFT_7344_s;
CombineDFT_7331_t CombineDFT_7345_s;
CombineDFT_7331_t CombineDFT_7346_s;
CombineDFT_7349_t CombineDFT_7349_s;
CombineDFT_7349_t CombineDFT_7350_s;
CombineDFT_7349_t CombineDFT_7351_s;
CombineDFT_7349_t CombineDFT_7352_s;
CombineDFT_7349_t CombineDFT_7353_s;
CombineDFT_7349_t CombineDFT_7354_s;
CombineDFT_7349_t CombineDFT_7355_s;
CombineDFT_7349_t CombineDFT_7356_s;
CombineDFT_7359_t CombineDFT_7359_s;
CombineDFT_7359_t CombineDFT_7360_s;
CombineDFT_7359_t CombineDFT_7361_s;
CombineDFT_7359_t CombineDFT_7362_s;
CombineDFT_7365_t CombineDFT_7365_s;
CombineDFT_7365_t CombineDFT_7366_s;
CombineDFT_7247_t CombineDFT_7247_s;
CombineDFT_7312_t CombineDFT_7407_s;
CombineDFT_7312_t CombineDFT_7408_s;
CombineDFT_7312_t CombineDFT_7409_s;
CombineDFT_7312_t CombineDFT_7410_s;
CombineDFT_7312_t CombineDFT_7411_s;
CombineDFT_7312_t CombineDFT_7412_s;
CombineDFT_7312_t CombineDFT_7413_s;
CombineDFT_7312_t CombineDFT_7414_s;
CombineDFT_7312_t CombineDFT_7415_s;
CombineDFT_7312_t CombineDFT_7416_s;
CombineDFT_7312_t CombineDFT_7417_s;
CombineDFT_7312_t CombineDFT_7418_s;
CombineDFT_7312_t CombineDFT_7419_s;
CombineDFT_7312_t CombineDFT_7420_s;
CombineDFT_7312_t CombineDFT_7421_s;
CombineDFT_7312_t CombineDFT_7422_s;
CombineDFT_7312_t CombineDFT_7423_s;
CombineDFT_7331_t CombineDFT_7426_s;
CombineDFT_7331_t CombineDFT_7427_s;
CombineDFT_7331_t CombineDFT_7428_s;
CombineDFT_7331_t CombineDFT_7429_s;
CombineDFT_7331_t CombineDFT_7430_s;
CombineDFT_7331_t CombineDFT_7431_s;
CombineDFT_7331_t CombineDFT_7432_s;
CombineDFT_7331_t CombineDFT_7433_s;
CombineDFT_7331_t CombineDFT_7434_s;
CombineDFT_7331_t CombineDFT_7435_s;
CombineDFT_7331_t CombineDFT_7436_s;
CombineDFT_7331_t CombineDFT_7437_s;
CombineDFT_7331_t CombineDFT_7438_s;
CombineDFT_7331_t CombineDFT_7439_s;
CombineDFT_7331_t CombineDFT_7440_s;
CombineDFT_7331_t CombineDFT_7441_s;
CombineDFT_7349_t CombineDFT_7444_s;
CombineDFT_7349_t CombineDFT_7445_s;
CombineDFT_7349_t CombineDFT_7446_s;
CombineDFT_7349_t CombineDFT_7447_s;
CombineDFT_7349_t CombineDFT_7448_s;
CombineDFT_7349_t CombineDFT_7449_s;
CombineDFT_7349_t CombineDFT_7450_s;
CombineDFT_7349_t CombineDFT_7451_s;
CombineDFT_7359_t CombineDFT_7454_s;
CombineDFT_7359_t CombineDFT_7455_s;
CombineDFT_7359_t CombineDFT_7456_s;
CombineDFT_7359_t CombineDFT_7457_s;
CombineDFT_7365_t CombineDFT_7460_s;
CombineDFT_7365_t CombineDFT_7461_s;
CombineDFT_7247_t CombineDFT_7258_s;

void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), 0.0) ; 
		push_float(&(*chanout), 0.0) ; 
		push_float(&(*chanout), 1.0) ; 
		push_float(&(*chanout), 0.0) ; 
		FOR(int, i, 0,  < , 124, i++) {
			push_float(&(*chanout), 0.0) ; 
		}
		ENDFOR
	}


void FFTTestSource_7270() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_7462_7483_split[0]), &(SplitJoin0_FFTTestSource_Fiss_7462_7483_join[0]));
	ENDFOR
}

void FFTTestSource_7271() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_7462_7483_split[1]), &(SplitJoin0_FFTTestSource_Fiss_7462_7483_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7268() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_7269() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7269WEIGHTED_ROUND_ROBIN_Splitter_7260, pop_float(&SplitJoin0_FFTTestSource_Fiss_7462_7483_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7269WEIGHTED_ROUND_ROBIN_Splitter_7260, pop_float(&SplitJoin0_FFTTestSource_Fiss_7462_7483_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, i, 0,  < , 128, i = (i + 4)) {
			push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 128, i = (i + 4)) {
			push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_float(&(*chanin)) ; 
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void FFTReorderSimple_7237() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_7226_7262_7463_7484_split[0]), &(FFTReorderSimple_7237WEIGHTED_ROUND_ROBIN_Splitter_7272));
	ENDFOR
}

void FFTReorderSimple_7274() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_7464_7485_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_7464_7485_join[0]));
	ENDFOR
}

void FFTReorderSimple_7275() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_7464_7485_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_7464_7485_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7272() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_7464_7485_split[0], pop_float(&FFTReorderSimple_7237WEIGHTED_ROUND_ROBIN_Splitter_7272));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_7464_7485_split[1], pop_float(&FFTReorderSimple_7237WEIGHTED_ROUND_ROBIN_Splitter_7272));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7273() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7273WEIGHTED_ROUND_ROBIN_Splitter_7276, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_7464_7485_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7273WEIGHTED_ROUND_ROBIN_Splitter_7276, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_7464_7485_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_7278() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_7465_7486_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_7465_7486_join[0]));
	ENDFOR
}

void FFTReorderSimple_7279() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_7465_7486_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_7465_7486_join[1]));
	ENDFOR
}

void FFTReorderSimple_7280() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_7465_7486_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_7465_7486_join[2]));
	ENDFOR
}

void FFTReorderSimple_7281() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_7465_7486_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_7465_7486_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7276() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin6_FFTReorderSimple_Fiss_7465_7486_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7273WEIGHTED_ROUND_ROBIN_Splitter_7276));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7277() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7277WEIGHTED_ROUND_ROBIN_Splitter_7282, pop_float(&SplitJoin6_FFTReorderSimple_Fiss_7465_7486_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_7284() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_7466_7487_split[0]), &(SplitJoin8_FFTReorderSimple_Fiss_7466_7487_join[0]));
	ENDFOR
}

void FFTReorderSimple_7285() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_7466_7487_split[1]), &(SplitJoin8_FFTReorderSimple_Fiss_7466_7487_join[1]));
	ENDFOR
}

void FFTReorderSimple_7286() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_7466_7487_split[2]), &(SplitJoin8_FFTReorderSimple_Fiss_7466_7487_join[2]));
	ENDFOR
}

void FFTReorderSimple_7287() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_7466_7487_split[3]), &(SplitJoin8_FFTReorderSimple_Fiss_7466_7487_join[3]));
	ENDFOR
}

void FFTReorderSimple_7288() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_7466_7487_split[4]), &(SplitJoin8_FFTReorderSimple_Fiss_7466_7487_join[4]));
	ENDFOR
}

void FFTReorderSimple_7289() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_7466_7487_split[5]), &(SplitJoin8_FFTReorderSimple_Fiss_7466_7487_join[5]));
	ENDFOR
}

void FFTReorderSimple_7290() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_7466_7487_split[6]), &(SplitJoin8_FFTReorderSimple_Fiss_7466_7487_join[6]));
	ENDFOR
}

void FFTReorderSimple_7291() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_7466_7487_split[7]), &(SplitJoin8_FFTReorderSimple_Fiss_7466_7487_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7282() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin8_FFTReorderSimple_Fiss_7466_7487_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7277WEIGHTED_ROUND_ROBIN_Splitter_7282));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7283() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7283WEIGHTED_ROUND_ROBIN_Splitter_7292, pop_float(&SplitJoin8_FFTReorderSimple_Fiss_7466_7487_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_7294() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_split[0]), &(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_join[0]));
	ENDFOR
}

void FFTReorderSimple_7295() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_split[1]), &(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_join[1]));
	ENDFOR
}

void FFTReorderSimple_7296() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_split[2]), &(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_join[2]));
	ENDFOR
}

void FFTReorderSimple_7297() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_split[3]), &(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_join[3]));
	ENDFOR
}

void FFTReorderSimple_7298() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_split[4]), &(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_join[4]));
	ENDFOR
}

void FFTReorderSimple_7299() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_split[5]), &(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_join[5]));
	ENDFOR
}

void FFTReorderSimple_7300() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_split[6]), &(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_join[6]));
	ENDFOR
}

void FFTReorderSimple_7301() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_split[7]), &(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_join[7]));
	ENDFOR
}

void FFTReorderSimple_7302() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_split[8]), &(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_join[8]));
	ENDFOR
}

void FFTReorderSimple_7303() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_split[9]), &(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_join[9]));
	ENDFOR
}

void FFTReorderSimple_7304() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_split[10]), &(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_join[10]));
	ENDFOR
}

void FFTReorderSimple_7305() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_split[11]), &(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_join[11]));
	ENDFOR
}

void FFTReorderSimple_7306() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_split[12]), &(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_join[12]));
	ENDFOR
}

void FFTReorderSimple_7307() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_split[13]), &(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_join[13]));
	ENDFOR
}

void FFTReorderSimple_7308() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_split[14]), &(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_join[14]));
	ENDFOR
}

void FFTReorderSimple_7309() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_split[15]), &(SplitJoin10_FFTReorderSimple_Fiss_7467_7488_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7292() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_FFTReorderSimple_Fiss_7467_7488_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7283WEIGHTED_ROUND_ROBIN_Splitter_7292));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7293() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7293WEIGHTED_ROUND_ROBIN_Splitter_7310, pop_float(&SplitJoin10_FFTReorderSimple_Fiss_7467_7488_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&(*chanin), i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&(*chanin), i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&(*chanin), (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&(*chanin), (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_7312_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_7312_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&(*chanin)) ; 
			push_float(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_7312() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7468_7489_split[0]), &(SplitJoin12_CombineDFT_Fiss_7468_7489_join[0]));
	ENDFOR
}

void CombineDFT_7313() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7468_7489_split[1]), &(SplitJoin12_CombineDFT_Fiss_7468_7489_join[1]));
	ENDFOR
}

void CombineDFT_7314() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7468_7489_split[2]), &(SplitJoin12_CombineDFT_Fiss_7468_7489_join[2]));
	ENDFOR
}

void CombineDFT_7315() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7468_7489_split[3]), &(SplitJoin12_CombineDFT_Fiss_7468_7489_join[3]));
	ENDFOR
}

void CombineDFT_7316() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7468_7489_split[4]), &(SplitJoin12_CombineDFT_Fiss_7468_7489_join[4]));
	ENDFOR
}

void CombineDFT_7317() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7468_7489_split[5]), &(SplitJoin12_CombineDFT_Fiss_7468_7489_join[5]));
	ENDFOR
}

void CombineDFT_7318() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7468_7489_split[6]), &(SplitJoin12_CombineDFT_Fiss_7468_7489_join[6]));
	ENDFOR
}

void CombineDFT_7319() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7468_7489_split[7]), &(SplitJoin12_CombineDFT_Fiss_7468_7489_join[7]));
	ENDFOR
}

void CombineDFT_7320() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7468_7489_split[8]), &(SplitJoin12_CombineDFT_Fiss_7468_7489_join[8]));
	ENDFOR
}

void CombineDFT_7321() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7468_7489_split[9]), &(SplitJoin12_CombineDFT_Fiss_7468_7489_join[9]));
	ENDFOR
}

void CombineDFT_7322() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7468_7489_split[10]), &(SplitJoin12_CombineDFT_Fiss_7468_7489_join[10]));
	ENDFOR
}

void CombineDFT_7323() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7468_7489_split[11]), &(SplitJoin12_CombineDFT_Fiss_7468_7489_join[11]));
	ENDFOR
}

void CombineDFT_7324() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7468_7489_split[12]), &(SplitJoin12_CombineDFT_Fiss_7468_7489_join[12]));
	ENDFOR
}

void CombineDFT_7325() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7468_7489_split[13]), &(SplitJoin12_CombineDFT_Fiss_7468_7489_join[13]));
	ENDFOR
}

void CombineDFT_7326() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7468_7489_split[14]), &(SplitJoin12_CombineDFT_Fiss_7468_7489_join[14]));
	ENDFOR
}

void CombineDFT_7327() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7468_7489_split[15]), &(SplitJoin12_CombineDFT_Fiss_7468_7489_join[15]));
	ENDFOR
}

void CombineDFT_7328() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7468_7489_split[16]), &(SplitJoin12_CombineDFT_Fiss_7468_7489_join[16]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7310() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 17, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin12_CombineDFT_Fiss_7468_7489_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7293WEIGHTED_ROUND_ROBIN_Splitter_7310));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7311() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 17, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7311WEIGHTED_ROUND_ROBIN_Splitter_7329, pop_float(&SplitJoin12_CombineDFT_Fiss_7468_7489_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_7331() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7469_7490_split[0]), &(SplitJoin14_CombineDFT_Fiss_7469_7490_join[0]));
	ENDFOR
}

void CombineDFT_7332() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7469_7490_split[1]), &(SplitJoin14_CombineDFT_Fiss_7469_7490_join[1]));
	ENDFOR
}

void CombineDFT_7333() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7469_7490_split[2]), &(SplitJoin14_CombineDFT_Fiss_7469_7490_join[2]));
	ENDFOR
}

void CombineDFT_7334() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7469_7490_split[3]), &(SplitJoin14_CombineDFT_Fiss_7469_7490_join[3]));
	ENDFOR
}

void CombineDFT_7335() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7469_7490_split[4]), &(SplitJoin14_CombineDFT_Fiss_7469_7490_join[4]));
	ENDFOR
}

void CombineDFT_7336() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7469_7490_split[5]), &(SplitJoin14_CombineDFT_Fiss_7469_7490_join[5]));
	ENDFOR
}

void CombineDFT_7337() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7469_7490_split[6]), &(SplitJoin14_CombineDFT_Fiss_7469_7490_join[6]));
	ENDFOR
}

void CombineDFT_7338() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7469_7490_split[7]), &(SplitJoin14_CombineDFT_Fiss_7469_7490_join[7]));
	ENDFOR
}

void CombineDFT_7339() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7469_7490_split[8]), &(SplitJoin14_CombineDFT_Fiss_7469_7490_join[8]));
	ENDFOR
}

void CombineDFT_7340() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7469_7490_split[9]), &(SplitJoin14_CombineDFT_Fiss_7469_7490_join[9]));
	ENDFOR
}

void CombineDFT_7341() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7469_7490_split[10]), &(SplitJoin14_CombineDFT_Fiss_7469_7490_join[10]));
	ENDFOR
}

void CombineDFT_7342() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7469_7490_split[11]), &(SplitJoin14_CombineDFT_Fiss_7469_7490_join[11]));
	ENDFOR
}

void CombineDFT_7343() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7469_7490_split[12]), &(SplitJoin14_CombineDFT_Fiss_7469_7490_join[12]));
	ENDFOR
}

void CombineDFT_7344() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7469_7490_split[13]), &(SplitJoin14_CombineDFT_Fiss_7469_7490_join[13]));
	ENDFOR
}

void CombineDFT_7345() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7469_7490_split[14]), &(SplitJoin14_CombineDFT_Fiss_7469_7490_join[14]));
	ENDFOR
}

void CombineDFT_7346() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7469_7490_split[15]), &(SplitJoin14_CombineDFT_Fiss_7469_7490_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7329() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin14_CombineDFT_Fiss_7469_7490_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7311WEIGHTED_ROUND_ROBIN_Splitter_7329));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7330() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7330WEIGHTED_ROUND_ROBIN_Splitter_7347, pop_float(&SplitJoin14_CombineDFT_Fiss_7469_7490_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_7349() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_7470_7491_split[0]), &(SplitJoin16_CombineDFT_Fiss_7470_7491_join[0]));
	ENDFOR
}

void CombineDFT_7350() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_7470_7491_split[1]), &(SplitJoin16_CombineDFT_Fiss_7470_7491_join[1]));
	ENDFOR
}

void CombineDFT_7351() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_7470_7491_split[2]), &(SplitJoin16_CombineDFT_Fiss_7470_7491_join[2]));
	ENDFOR
}

void CombineDFT_7352() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_7470_7491_split[3]), &(SplitJoin16_CombineDFT_Fiss_7470_7491_join[3]));
	ENDFOR
}

void CombineDFT_7353() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_7470_7491_split[4]), &(SplitJoin16_CombineDFT_Fiss_7470_7491_join[4]));
	ENDFOR
}

void CombineDFT_7354() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_7470_7491_split[5]), &(SplitJoin16_CombineDFT_Fiss_7470_7491_join[5]));
	ENDFOR
}

void CombineDFT_7355() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_7470_7491_split[6]), &(SplitJoin16_CombineDFT_Fiss_7470_7491_join[6]));
	ENDFOR
}

void CombineDFT_7356() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_7470_7491_split[7]), &(SplitJoin16_CombineDFT_Fiss_7470_7491_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7347() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin16_CombineDFT_Fiss_7470_7491_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7330WEIGHTED_ROUND_ROBIN_Splitter_7347));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7348() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7348WEIGHTED_ROUND_ROBIN_Splitter_7357, pop_float(&SplitJoin16_CombineDFT_Fiss_7470_7491_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_7359() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_7471_7492_split[0]), &(SplitJoin18_CombineDFT_Fiss_7471_7492_join[0]));
	ENDFOR
}

void CombineDFT_7360() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_7471_7492_split[1]), &(SplitJoin18_CombineDFT_Fiss_7471_7492_join[1]));
	ENDFOR
}

void CombineDFT_7361() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_7471_7492_split[2]), &(SplitJoin18_CombineDFT_Fiss_7471_7492_join[2]));
	ENDFOR
}

void CombineDFT_7362() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_7471_7492_split[3]), &(SplitJoin18_CombineDFT_Fiss_7471_7492_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7357() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin18_CombineDFT_Fiss_7471_7492_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7348WEIGHTED_ROUND_ROBIN_Splitter_7357));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7358() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7358WEIGHTED_ROUND_ROBIN_Splitter_7363, pop_float(&SplitJoin18_CombineDFT_Fiss_7471_7492_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_7365() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin20_CombineDFT_Fiss_7472_7493_split[0]), &(SplitJoin20_CombineDFT_Fiss_7472_7493_join[0]));
	ENDFOR
}

void CombineDFT_7366() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin20_CombineDFT_Fiss_7472_7493_split[1]), &(SplitJoin20_CombineDFT_Fiss_7472_7493_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7363() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_7472_7493_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7358WEIGHTED_ROUND_ROBIN_Splitter_7363));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_7472_7493_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7358WEIGHTED_ROUND_ROBIN_Splitter_7363));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7364() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7364CombineDFT_7247, pop_float(&SplitJoin20_CombineDFT_Fiss_7472_7493_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7364CombineDFT_7247, pop_float(&SplitJoin20_CombineDFT_Fiss_7472_7493_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_7247() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_7364CombineDFT_7247), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_7226_7262_7463_7484_join[0]));
	ENDFOR
}

void FFTReorderSimple_7248() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_7226_7262_7463_7484_split[1]), &(FFTReorderSimple_7248WEIGHTED_ROUND_ROBIN_Splitter_7367));
	ENDFOR
}

void FFTReorderSimple_7369() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin91_FFTReorderSimple_Fiss_7473_7494_split[0]), &(SplitJoin91_FFTReorderSimple_Fiss_7473_7494_join[0]));
	ENDFOR
}

void FFTReorderSimple_7370() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin91_FFTReorderSimple_Fiss_7473_7494_split[1]), &(SplitJoin91_FFTReorderSimple_Fiss_7473_7494_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7367() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin91_FFTReorderSimple_Fiss_7473_7494_split[0], pop_float(&FFTReorderSimple_7248WEIGHTED_ROUND_ROBIN_Splitter_7367));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin91_FFTReorderSimple_Fiss_7473_7494_split[1], pop_float(&FFTReorderSimple_7248WEIGHTED_ROUND_ROBIN_Splitter_7367));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7368() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7368WEIGHTED_ROUND_ROBIN_Splitter_7371, pop_float(&SplitJoin91_FFTReorderSimple_Fiss_7473_7494_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7368WEIGHTED_ROUND_ROBIN_Splitter_7371, pop_float(&SplitJoin91_FFTReorderSimple_Fiss_7473_7494_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_7373() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin93_FFTReorderSimple_Fiss_7474_7495_split[0]), &(SplitJoin93_FFTReorderSimple_Fiss_7474_7495_join[0]));
	ENDFOR
}

void FFTReorderSimple_7374() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin93_FFTReorderSimple_Fiss_7474_7495_split[1]), &(SplitJoin93_FFTReorderSimple_Fiss_7474_7495_join[1]));
	ENDFOR
}

void FFTReorderSimple_7375() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin93_FFTReorderSimple_Fiss_7474_7495_split[2]), &(SplitJoin93_FFTReorderSimple_Fiss_7474_7495_join[2]));
	ENDFOR
}

void FFTReorderSimple_7376() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin93_FFTReorderSimple_Fiss_7474_7495_split[3]), &(SplitJoin93_FFTReorderSimple_Fiss_7474_7495_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7371() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin93_FFTReorderSimple_Fiss_7474_7495_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7368WEIGHTED_ROUND_ROBIN_Splitter_7371));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7372() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7372WEIGHTED_ROUND_ROBIN_Splitter_7377, pop_float(&SplitJoin93_FFTReorderSimple_Fiss_7474_7495_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_7379() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin95_FFTReorderSimple_Fiss_7475_7496_split[0]), &(SplitJoin95_FFTReorderSimple_Fiss_7475_7496_join[0]));
	ENDFOR
}

void FFTReorderSimple_7380() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin95_FFTReorderSimple_Fiss_7475_7496_split[1]), &(SplitJoin95_FFTReorderSimple_Fiss_7475_7496_join[1]));
	ENDFOR
}

void FFTReorderSimple_7381() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin95_FFTReorderSimple_Fiss_7475_7496_split[2]), &(SplitJoin95_FFTReorderSimple_Fiss_7475_7496_join[2]));
	ENDFOR
}

void FFTReorderSimple_7382() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin95_FFTReorderSimple_Fiss_7475_7496_split[3]), &(SplitJoin95_FFTReorderSimple_Fiss_7475_7496_join[3]));
	ENDFOR
}

void FFTReorderSimple_7383() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin95_FFTReorderSimple_Fiss_7475_7496_split[4]), &(SplitJoin95_FFTReorderSimple_Fiss_7475_7496_join[4]));
	ENDFOR
}

void FFTReorderSimple_7384() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin95_FFTReorderSimple_Fiss_7475_7496_split[5]), &(SplitJoin95_FFTReorderSimple_Fiss_7475_7496_join[5]));
	ENDFOR
}

void FFTReorderSimple_7385() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin95_FFTReorderSimple_Fiss_7475_7496_split[6]), &(SplitJoin95_FFTReorderSimple_Fiss_7475_7496_join[6]));
	ENDFOR
}

void FFTReorderSimple_7386() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin95_FFTReorderSimple_Fiss_7475_7496_split[7]), &(SplitJoin95_FFTReorderSimple_Fiss_7475_7496_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7377() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin95_FFTReorderSimple_Fiss_7475_7496_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7372WEIGHTED_ROUND_ROBIN_Splitter_7377));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7378() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7378WEIGHTED_ROUND_ROBIN_Splitter_7387, pop_float(&SplitJoin95_FFTReorderSimple_Fiss_7475_7496_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_7389() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_split[0]), &(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_join[0]));
	ENDFOR
}

void FFTReorderSimple_7390() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_split[1]), &(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_join[1]));
	ENDFOR
}

void FFTReorderSimple_7391() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_split[2]), &(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_join[2]));
	ENDFOR
}

void FFTReorderSimple_7392() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_split[3]), &(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_join[3]));
	ENDFOR
}

void FFTReorderSimple_7393() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_split[4]), &(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_join[4]));
	ENDFOR
}

void FFTReorderSimple_7394() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_split[5]), &(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_join[5]));
	ENDFOR
}

void FFTReorderSimple_7395() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_split[6]), &(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_join[6]));
	ENDFOR
}

void FFTReorderSimple_7396() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_split[7]), &(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_join[7]));
	ENDFOR
}

void FFTReorderSimple_7397() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_split[8]), &(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_join[8]));
	ENDFOR
}

void FFTReorderSimple_7398() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_split[9]), &(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_join[9]));
	ENDFOR
}

void FFTReorderSimple_7399() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_split[10]), &(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_join[10]));
	ENDFOR
}

void FFTReorderSimple_7400() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_split[11]), &(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_join[11]));
	ENDFOR
}

void FFTReorderSimple_7401() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_split[12]), &(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_join[12]));
	ENDFOR
}

void FFTReorderSimple_7402() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_split[13]), &(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_join[13]));
	ENDFOR
}

void FFTReorderSimple_7403() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_split[14]), &(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_join[14]));
	ENDFOR
}

void FFTReorderSimple_7404() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_split[15]), &(SplitJoin97_FFTReorderSimple_Fiss_7476_7497_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7387() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin97_FFTReorderSimple_Fiss_7476_7497_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7378WEIGHTED_ROUND_ROBIN_Splitter_7387));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7388() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7388WEIGHTED_ROUND_ROBIN_Splitter_7405, pop_float(&SplitJoin97_FFTReorderSimple_Fiss_7476_7497_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_7407() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin99_CombineDFT_Fiss_7477_7498_split[0]), &(SplitJoin99_CombineDFT_Fiss_7477_7498_join[0]));
	ENDFOR
}

void CombineDFT_7408() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin99_CombineDFT_Fiss_7477_7498_split[1]), &(SplitJoin99_CombineDFT_Fiss_7477_7498_join[1]));
	ENDFOR
}

void CombineDFT_7409() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin99_CombineDFT_Fiss_7477_7498_split[2]), &(SplitJoin99_CombineDFT_Fiss_7477_7498_join[2]));
	ENDFOR
}

void CombineDFT_7410() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin99_CombineDFT_Fiss_7477_7498_split[3]), &(SplitJoin99_CombineDFT_Fiss_7477_7498_join[3]));
	ENDFOR
}

void CombineDFT_7411() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin99_CombineDFT_Fiss_7477_7498_split[4]), &(SplitJoin99_CombineDFT_Fiss_7477_7498_join[4]));
	ENDFOR
}

void CombineDFT_7412() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin99_CombineDFT_Fiss_7477_7498_split[5]), &(SplitJoin99_CombineDFT_Fiss_7477_7498_join[5]));
	ENDFOR
}

void CombineDFT_7413() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin99_CombineDFT_Fiss_7477_7498_split[6]), &(SplitJoin99_CombineDFT_Fiss_7477_7498_join[6]));
	ENDFOR
}

void CombineDFT_7414() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin99_CombineDFT_Fiss_7477_7498_split[7]), &(SplitJoin99_CombineDFT_Fiss_7477_7498_join[7]));
	ENDFOR
}

void CombineDFT_7415() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin99_CombineDFT_Fiss_7477_7498_split[8]), &(SplitJoin99_CombineDFT_Fiss_7477_7498_join[8]));
	ENDFOR
}

void CombineDFT_7416() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin99_CombineDFT_Fiss_7477_7498_split[9]), &(SplitJoin99_CombineDFT_Fiss_7477_7498_join[9]));
	ENDFOR
}

void CombineDFT_7417() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin99_CombineDFT_Fiss_7477_7498_split[10]), &(SplitJoin99_CombineDFT_Fiss_7477_7498_join[10]));
	ENDFOR
}

void CombineDFT_7418() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin99_CombineDFT_Fiss_7477_7498_split[11]), &(SplitJoin99_CombineDFT_Fiss_7477_7498_join[11]));
	ENDFOR
}

void CombineDFT_7419() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin99_CombineDFT_Fiss_7477_7498_split[12]), &(SplitJoin99_CombineDFT_Fiss_7477_7498_join[12]));
	ENDFOR
}

void CombineDFT_7420() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin99_CombineDFT_Fiss_7477_7498_split[13]), &(SplitJoin99_CombineDFT_Fiss_7477_7498_join[13]));
	ENDFOR
}

void CombineDFT_7421() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin99_CombineDFT_Fiss_7477_7498_split[14]), &(SplitJoin99_CombineDFT_Fiss_7477_7498_join[14]));
	ENDFOR
}

void CombineDFT_7422() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin99_CombineDFT_Fiss_7477_7498_split[15]), &(SplitJoin99_CombineDFT_Fiss_7477_7498_join[15]));
	ENDFOR
}

void CombineDFT_7423() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin99_CombineDFT_Fiss_7477_7498_split[16]), &(SplitJoin99_CombineDFT_Fiss_7477_7498_join[16]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7405() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 17, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin99_CombineDFT_Fiss_7477_7498_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7388WEIGHTED_ROUND_ROBIN_Splitter_7405));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7406() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 17, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7406WEIGHTED_ROUND_ROBIN_Splitter_7424, pop_float(&SplitJoin99_CombineDFT_Fiss_7477_7498_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_7426() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin101_CombineDFT_Fiss_7478_7499_split[0]), &(SplitJoin101_CombineDFT_Fiss_7478_7499_join[0]));
	ENDFOR
}

void CombineDFT_7427() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin101_CombineDFT_Fiss_7478_7499_split[1]), &(SplitJoin101_CombineDFT_Fiss_7478_7499_join[1]));
	ENDFOR
}

void CombineDFT_7428() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin101_CombineDFT_Fiss_7478_7499_split[2]), &(SplitJoin101_CombineDFT_Fiss_7478_7499_join[2]));
	ENDFOR
}

void CombineDFT_7429() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin101_CombineDFT_Fiss_7478_7499_split[3]), &(SplitJoin101_CombineDFT_Fiss_7478_7499_join[3]));
	ENDFOR
}

void CombineDFT_7430() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin101_CombineDFT_Fiss_7478_7499_split[4]), &(SplitJoin101_CombineDFT_Fiss_7478_7499_join[4]));
	ENDFOR
}

void CombineDFT_7431() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin101_CombineDFT_Fiss_7478_7499_split[5]), &(SplitJoin101_CombineDFT_Fiss_7478_7499_join[5]));
	ENDFOR
}

void CombineDFT_7432() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin101_CombineDFT_Fiss_7478_7499_split[6]), &(SplitJoin101_CombineDFT_Fiss_7478_7499_join[6]));
	ENDFOR
}

void CombineDFT_7433() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin101_CombineDFT_Fiss_7478_7499_split[7]), &(SplitJoin101_CombineDFT_Fiss_7478_7499_join[7]));
	ENDFOR
}

void CombineDFT_7434() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin101_CombineDFT_Fiss_7478_7499_split[8]), &(SplitJoin101_CombineDFT_Fiss_7478_7499_join[8]));
	ENDFOR
}

void CombineDFT_7435() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin101_CombineDFT_Fiss_7478_7499_split[9]), &(SplitJoin101_CombineDFT_Fiss_7478_7499_join[9]));
	ENDFOR
}

void CombineDFT_7436() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin101_CombineDFT_Fiss_7478_7499_split[10]), &(SplitJoin101_CombineDFT_Fiss_7478_7499_join[10]));
	ENDFOR
}

void CombineDFT_7437() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin101_CombineDFT_Fiss_7478_7499_split[11]), &(SplitJoin101_CombineDFT_Fiss_7478_7499_join[11]));
	ENDFOR
}

void CombineDFT_7438() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin101_CombineDFT_Fiss_7478_7499_split[12]), &(SplitJoin101_CombineDFT_Fiss_7478_7499_join[12]));
	ENDFOR
}

void CombineDFT_7439() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin101_CombineDFT_Fiss_7478_7499_split[13]), &(SplitJoin101_CombineDFT_Fiss_7478_7499_join[13]));
	ENDFOR
}

void CombineDFT_7440() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin101_CombineDFT_Fiss_7478_7499_split[14]), &(SplitJoin101_CombineDFT_Fiss_7478_7499_join[14]));
	ENDFOR
}

void CombineDFT_7441() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin101_CombineDFT_Fiss_7478_7499_split[15]), &(SplitJoin101_CombineDFT_Fiss_7478_7499_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7424() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin101_CombineDFT_Fiss_7478_7499_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7406WEIGHTED_ROUND_ROBIN_Splitter_7424));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7425() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7425WEIGHTED_ROUND_ROBIN_Splitter_7442, pop_float(&SplitJoin101_CombineDFT_Fiss_7478_7499_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_7444() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin103_CombineDFT_Fiss_7479_7500_split[0]), &(SplitJoin103_CombineDFT_Fiss_7479_7500_join[0]));
	ENDFOR
}

void CombineDFT_7445() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin103_CombineDFT_Fiss_7479_7500_split[1]), &(SplitJoin103_CombineDFT_Fiss_7479_7500_join[1]));
	ENDFOR
}

void CombineDFT_7446() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin103_CombineDFT_Fiss_7479_7500_split[2]), &(SplitJoin103_CombineDFT_Fiss_7479_7500_join[2]));
	ENDFOR
}

void CombineDFT_7447() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin103_CombineDFT_Fiss_7479_7500_split[3]), &(SplitJoin103_CombineDFT_Fiss_7479_7500_join[3]));
	ENDFOR
}

void CombineDFT_7448() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin103_CombineDFT_Fiss_7479_7500_split[4]), &(SplitJoin103_CombineDFT_Fiss_7479_7500_join[4]));
	ENDFOR
}

void CombineDFT_7449() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin103_CombineDFT_Fiss_7479_7500_split[5]), &(SplitJoin103_CombineDFT_Fiss_7479_7500_join[5]));
	ENDFOR
}

void CombineDFT_7450() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin103_CombineDFT_Fiss_7479_7500_split[6]), &(SplitJoin103_CombineDFT_Fiss_7479_7500_join[6]));
	ENDFOR
}

void CombineDFT_7451() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin103_CombineDFT_Fiss_7479_7500_split[7]), &(SplitJoin103_CombineDFT_Fiss_7479_7500_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7442() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin103_CombineDFT_Fiss_7479_7500_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7425WEIGHTED_ROUND_ROBIN_Splitter_7442));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7443() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7443WEIGHTED_ROUND_ROBIN_Splitter_7452, pop_float(&SplitJoin103_CombineDFT_Fiss_7479_7500_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_7454() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin105_CombineDFT_Fiss_7480_7501_split[0]), &(SplitJoin105_CombineDFT_Fiss_7480_7501_join[0]));
	ENDFOR
}

void CombineDFT_7455() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin105_CombineDFT_Fiss_7480_7501_split[1]), &(SplitJoin105_CombineDFT_Fiss_7480_7501_join[1]));
	ENDFOR
}

void CombineDFT_7456() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin105_CombineDFT_Fiss_7480_7501_split[2]), &(SplitJoin105_CombineDFT_Fiss_7480_7501_join[2]));
	ENDFOR
}

void CombineDFT_7457() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin105_CombineDFT_Fiss_7480_7501_split[3]), &(SplitJoin105_CombineDFT_Fiss_7480_7501_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7452() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin105_CombineDFT_Fiss_7480_7501_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7443WEIGHTED_ROUND_ROBIN_Splitter_7452));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7453() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7453WEIGHTED_ROUND_ROBIN_Splitter_7458, pop_float(&SplitJoin105_CombineDFT_Fiss_7480_7501_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_7460() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin107_CombineDFT_Fiss_7481_7502_split[0]), &(SplitJoin107_CombineDFT_Fiss_7481_7502_join[0]));
	ENDFOR
}

void CombineDFT_7461() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(SplitJoin107_CombineDFT_Fiss_7481_7502_split[1]), &(SplitJoin107_CombineDFT_Fiss_7481_7502_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7458() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin107_CombineDFT_Fiss_7481_7502_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7453WEIGHTED_ROUND_ROBIN_Splitter_7458));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin107_CombineDFT_Fiss_7481_7502_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7453WEIGHTED_ROUND_ROBIN_Splitter_7458));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7459() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7459CombineDFT_7258, pop_float(&SplitJoin107_CombineDFT_Fiss_7481_7502_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7459CombineDFT_7258, pop_float(&SplitJoin107_CombineDFT_Fiss_7481_7502_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_7258() {
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_7459CombineDFT_7258), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_7226_7262_7463_7484_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7260() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_7226_7262_7463_7484_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7269WEIGHTED_ROUND_ROBIN_Splitter_7260));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_7226_7262_7463_7484_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7269WEIGHTED_ROUND_ROBIN_Splitter_7260));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7261() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 17, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7261FloatPrinter_7259, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_7226_7262_7463_7484_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7261FloatPrinter_7259, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_7226_7262_7463_7484_join[1]));
		ENDFOR
	ENDFOR
}}

void FloatPrinter(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void FloatPrinter_7259() {
	FOR(uint32_t, __iter_steady_, 0, <, 4352, __iter_steady_++)
		FloatPrinter(&(WEIGHTED_ROUND_ROBIN_Joiner_7261FloatPrinter_7259));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_7226_7262_7463_7484_join[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7443WEIGHTED_ROUND_ROBIN_Splitter_7452);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7364CombineDFT_7247);
	FOR(int, __iter_init_1_, 0, <, 4, __iter_init_1_++)
		init_buffer_float(&SplitJoin93_FFTReorderSimple_Fiss_7474_7495_join[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&FFTReorderSimple_7237WEIGHTED_ROUND_ROBIN_Splitter_7272);
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_float(&SplitJoin107_CombineDFT_Fiss_7481_7502_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 2, __iter_init_3_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_7226_7262_7463_7484_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 17, __iter_init_4_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_7468_7489_join[__iter_init_4_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7348WEIGHTED_ROUND_ROBIN_Splitter_7357);
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_7466_7487_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 16, __iter_init_6_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_7467_7488_split[__iter_init_6_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7261FloatPrinter_7259);
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_7470_7491_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 17, __iter_init_8_++)
		init_buffer_float(&SplitJoin99_CombineDFT_Fiss_7477_7498_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_float(&SplitJoin95_FFTReorderSimple_Fiss_7475_7496_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_7464_7485_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 4, __iter_init_11_++)
		init_buffer_float(&SplitJoin105_CombineDFT_Fiss_7480_7501_join[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7453WEIGHTED_ROUND_ROBIN_Splitter_7458);
	FOR(int, __iter_init_12_, 0, <, 2, __iter_init_12_++)
		init_buffer_float(&SplitJoin91_FFTReorderSimple_Fiss_7473_7494_join[__iter_init_12_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7269WEIGHTED_ROUND_ROBIN_Splitter_7260);
	FOR(int, __iter_init_13_, 0, <, 16, __iter_init_13_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_7467_7488_join[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 4, __iter_init_14_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_7465_7486_join[__iter_init_14_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7406WEIGHTED_ROUND_ROBIN_Splitter_7424);
	FOR(int, __iter_init_15_, 0, <, 4, __iter_init_15_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_7471_7492_join[__iter_init_15_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7372WEIGHTED_ROUND_ROBIN_Splitter_7377);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7293WEIGHTED_ROUND_ROBIN_Splitter_7310);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7330WEIGHTED_ROUND_ROBIN_Splitter_7347);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7368WEIGHTED_ROUND_ROBIN_Splitter_7371);
	FOR(int, __iter_init_16_, 0, <, 4, __iter_init_16_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_7471_7492_split[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 8, __iter_init_17_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_7466_7487_split[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 8, __iter_init_18_++)
		init_buffer_float(&SplitJoin103_CombineDFT_Fiss_7479_7500_split[__iter_init_18_]);
	ENDFOR
	FOR(int, __iter_init_19_, 0, <, 16, __iter_init_19_++)
		init_buffer_float(&SplitJoin97_FFTReorderSimple_Fiss_7476_7497_join[__iter_init_19_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7283WEIGHTED_ROUND_ROBIN_Splitter_7292);
	FOR(int, __iter_init_20_, 0, <, 16, __iter_init_20_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_7469_7490_split[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 16, __iter_init_21_++)
		init_buffer_float(&SplitJoin97_FFTReorderSimple_Fiss_7476_7497_split[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 17, __iter_init_22_++)
		init_buffer_float(&SplitJoin99_CombineDFT_Fiss_7477_7498_join[__iter_init_22_]);
	ENDFOR
	init_buffer_float(&FFTReorderSimple_7248WEIGHTED_ROUND_ROBIN_Splitter_7367);
	FOR(int, __iter_init_23_, 0, <, 2, __iter_init_23_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_7464_7485_join[__iter_init_23_]);
	ENDFOR
	FOR(int, __iter_init_24_, 0, <, 8, __iter_init_24_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_7470_7491_split[__iter_init_24_]);
	ENDFOR
	FOR(int, __iter_init_25_, 0, <, 8, __iter_init_25_++)
		init_buffer_float(&SplitJoin95_FFTReorderSimple_Fiss_7475_7496_split[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 16, __iter_init_26_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_7469_7490_join[__iter_init_26_]);
	ENDFOR
	FOR(int, __iter_init_27_, 0, <, 8, __iter_init_27_++)
		init_buffer_float(&SplitJoin103_CombineDFT_Fiss_7479_7500_join[__iter_init_27_]);
	ENDFOR
	FOR(int, __iter_init_28_, 0, <, 16, __iter_init_28_++)
		init_buffer_float(&SplitJoin101_CombineDFT_Fiss_7478_7499_split[__iter_init_28_]);
	ENDFOR
	FOR(int, __iter_init_29_, 0, <, 4, __iter_init_29_++)
		init_buffer_float(&SplitJoin93_FFTReorderSimple_Fiss_7474_7495_split[__iter_init_29_]);
	ENDFOR
	FOR(int, __iter_init_30_, 0, <, 2, __iter_init_30_++)
		init_buffer_float(&SplitJoin91_FFTReorderSimple_Fiss_7473_7494_split[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 16, __iter_init_31_++)
		init_buffer_float(&SplitJoin101_CombineDFT_Fiss_7478_7499_join[__iter_init_31_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7311WEIGHTED_ROUND_ROBIN_Splitter_7329);
	FOR(int, __iter_init_32_, 0, <, 2, __iter_init_32_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_7462_7483_split[__iter_init_32_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7388WEIGHTED_ROUND_ROBIN_Splitter_7405);
	FOR(int, __iter_init_33_, 0, <, 2, __iter_init_33_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_7472_7493_split[__iter_init_33_]);
	ENDFOR
	FOR(int, __iter_init_34_, 0, <, 17, __iter_init_34_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_7468_7489_split[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 2, __iter_init_35_++)
		init_buffer_float(&SplitJoin107_CombineDFT_Fiss_7481_7502_split[__iter_init_35_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7378WEIGHTED_ROUND_ROBIN_Splitter_7387);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7425WEIGHTED_ROUND_ROBIN_Splitter_7442);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7358WEIGHTED_ROUND_ROBIN_Splitter_7363);
	FOR(int, __iter_init_36_, 0, <, 2, __iter_init_36_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_7472_7493_join[__iter_init_36_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7277WEIGHTED_ROUND_ROBIN_Splitter_7282);
	FOR(int, __iter_init_37_, 0, <, 4, __iter_init_37_++)
		init_buffer_float(&SplitJoin105_CombineDFT_Fiss_7480_7501_split[__iter_init_37_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7459CombineDFT_7258);
	FOR(int, __iter_init_38_, 0, <, 2, __iter_init_38_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_7462_7483_join[__iter_init_38_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7273WEIGHTED_ROUND_ROBIN_Splitter_7276);
	FOR(int, __iter_init_39_, 0, <, 4, __iter_init_39_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_7465_7486_split[__iter_init_39_]);
	ENDFOR
// --- init: CombineDFT_7312
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7312_s.w[i] = real ; 
		CombineDFT_7312_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7313
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7313_s.w[i] = real ; 
		CombineDFT_7313_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7314
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7314_s.w[i] = real ; 
		CombineDFT_7314_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7315
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7315_s.w[i] = real ; 
		CombineDFT_7315_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7316
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7316_s.w[i] = real ; 
		CombineDFT_7316_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7317
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7317_s.w[i] = real ; 
		CombineDFT_7317_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7318
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7318_s.w[i] = real ; 
		CombineDFT_7318_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7319
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7319_s.w[i] = real ; 
		CombineDFT_7319_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7320
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7320_s.w[i] = real ; 
		CombineDFT_7320_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7321
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7321_s.w[i] = real ; 
		CombineDFT_7321_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7322
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7322_s.w[i] = real ; 
		CombineDFT_7322_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7323
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7323_s.w[i] = real ; 
		CombineDFT_7323_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7324
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7324_s.w[i] = real ; 
		CombineDFT_7324_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7325
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7325_s.w[i] = real ; 
		CombineDFT_7325_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7326
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7326_s.w[i] = real ; 
		CombineDFT_7326_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7327
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7327_s.w[i] = real ; 
		CombineDFT_7327_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7328
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7328_s.w[i] = real ; 
		CombineDFT_7328_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7331
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7331_s.w[i] = real ; 
		CombineDFT_7331_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7332
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7332_s.w[i] = real ; 
		CombineDFT_7332_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7333
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7333_s.w[i] = real ; 
		CombineDFT_7333_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7334
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7334_s.w[i] = real ; 
		CombineDFT_7334_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7335
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7335_s.w[i] = real ; 
		CombineDFT_7335_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7336
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7336_s.w[i] = real ; 
		CombineDFT_7336_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7337
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7337_s.w[i] = real ; 
		CombineDFT_7337_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7338
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7338_s.w[i] = real ; 
		CombineDFT_7338_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7339
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7339_s.w[i] = real ; 
		CombineDFT_7339_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7340
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7340_s.w[i] = real ; 
		CombineDFT_7340_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7341
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7341_s.w[i] = real ; 
		CombineDFT_7341_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7342
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7342_s.w[i] = real ; 
		CombineDFT_7342_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7343
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7343_s.w[i] = real ; 
		CombineDFT_7343_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7344
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7344_s.w[i] = real ; 
		CombineDFT_7344_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7345
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7345_s.w[i] = real ; 
		CombineDFT_7345_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7346
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7346_s.w[i] = real ; 
		CombineDFT_7346_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7349
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7349_s.w[i] = real ; 
		CombineDFT_7349_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7350
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7350_s.w[i] = real ; 
		CombineDFT_7350_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7351
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7351_s.w[i] = real ; 
		CombineDFT_7351_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7352
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7352_s.w[i] = real ; 
		CombineDFT_7352_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7353
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7353_s.w[i] = real ; 
		CombineDFT_7353_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7354
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7354_s.w[i] = real ; 
		CombineDFT_7354_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7355
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7355_s.w[i] = real ; 
		CombineDFT_7355_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7356
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7356_s.w[i] = real ; 
		CombineDFT_7356_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7359
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_7359_s.w[i] = real ; 
		CombineDFT_7359_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7360
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_7360_s.w[i] = real ; 
		CombineDFT_7360_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7361
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_7361_s.w[i] = real ; 
		CombineDFT_7361_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7362
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_7362_s.w[i] = real ; 
		CombineDFT_7362_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7365
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_7365_s.w[i] = real ; 
		CombineDFT_7365_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7366
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_7366_s.w[i] = real ; 
		CombineDFT_7366_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7247
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_7247_s.w[i] = real ; 
		CombineDFT_7247_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7407
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7407_s.w[i] = real ; 
		CombineDFT_7407_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7408
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7408_s.w[i] = real ; 
		CombineDFT_7408_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7409
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7409_s.w[i] = real ; 
		CombineDFT_7409_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7410
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7410_s.w[i] = real ; 
		CombineDFT_7410_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7411
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7411_s.w[i] = real ; 
		CombineDFT_7411_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7412
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7412_s.w[i] = real ; 
		CombineDFT_7412_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7413
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7413_s.w[i] = real ; 
		CombineDFT_7413_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7414
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7414_s.w[i] = real ; 
		CombineDFT_7414_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7415
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7415_s.w[i] = real ; 
		CombineDFT_7415_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7416
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7416_s.w[i] = real ; 
		CombineDFT_7416_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7417
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7417_s.w[i] = real ; 
		CombineDFT_7417_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7418
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7418_s.w[i] = real ; 
		CombineDFT_7418_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7419
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7419_s.w[i] = real ; 
		CombineDFT_7419_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7420
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7420_s.w[i] = real ; 
		CombineDFT_7420_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7421
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7421_s.w[i] = real ; 
		CombineDFT_7421_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7422
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7422_s.w[i] = real ; 
		CombineDFT_7422_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7423
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7423_s.w[i] = real ; 
		CombineDFT_7423_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7426
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7426_s.w[i] = real ; 
		CombineDFT_7426_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7427
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7427_s.w[i] = real ; 
		CombineDFT_7427_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7428
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7428_s.w[i] = real ; 
		CombineDFT_7428_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7429
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7429_s.w[i] = real ; 
		CombineDFT_7429_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7430
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7430_s.w[i] = real ; 
		CombineDFT_7430_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7431
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7431_s.w[i] = real ; 
		CombineDFT_7431_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7432
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7432_s.w[i] = real ; 
		CombineDFT_7432_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7433
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7433_s.w[i] = real ; 
		CombineDFT_7433_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7434
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7434_s.w[i] = real ; 
		CombineDFT_7434_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7435
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7435_s.w[i] = real ; 
		CombineDFT_7435_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7436
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7436_s.w[i] = real ; 
		CombineDFT_7436_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7437
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7437_s.w[i] = real ; 
		CombineDFT_7437_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7438
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7438_s.w[i] = real ; 
		CombineDFT_7438_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7439
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7439_s.w[i] = real ; 
		CombineDFT_7439_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7440
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7440_s.w[i] = real ; 
		CombineDFT_7440_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7441
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7441_s.w[i] = real ; 
		CombineDFT_7441_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7444
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7444_s.w[i] = real ; 
		CombineDFT_7444_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7445
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7445_s.w[i] = real ; 
		CombineDFT_7445_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7446
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7446_s.w[i] = real ; 
		CombineDFT_7446_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7447
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7447_s.w[i] = real ; 
		CombineDFT_7447_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7448
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7448_s.w[i] = real ; 
		CombineDFT_7448_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7449
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7449_s.w[i] = real ; 
		CombineDFT_7449_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7450
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7450_s.w[i] = real ; 
		CombineDFT_7450_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7451
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7451_s.w[i] = real ; 
		CombineDFT_7451_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7454
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_7454_s.w[i] = real ; 
		CombineDFT_7454_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7455
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_7455_s.w[i] = real ; 
		CombineDFT_7455_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7456
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_7456_s.w[i] = real ; 
		CombineDFT_7456_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7457
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_7457_s.w[i] = real ; 
		CombineDFT_7457_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7460
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_7460_s.w[i] = real ; 
		CombineDFT_7460_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7461
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_7461_s.w[i] = real ; 
		CombineDFT_7461_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7258
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_7258_s.w[i] = real ; 
		CombineDFT_7258_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_7268();
			FFTTestSource_7270();
			FFTTestSource_7271();
		WEIGHTED_ROUND_ROBIN_Joiner_7269();
		WEIGHTED_ROUND_ROBIN_Splitter_7260();
			FFTReorderSimple_7237();
			WEIGHTED_ROUND_ROBIN_Splitter_7272();
				FFTReorderSimple_7274();
				FFTReorderSimple_7275();
			WEIGHTED_ROUND_ROBIN_Joiner_7273();
			WEIGHTED_ROUND_ROBIN_Splitter_7276();
				FFTReorderSimple_7278();
				FFTReorderSimple_7279();
				FFTReorderSimple_7280();
				FFTReorderSimple_7281();
			WEIGHTED_ROUND_ROBIN_Joiner_7277();
			WEIGHTED_ROUND_ROBIN_Splitter_7282();
				FFTReorderSimple_7284();
				FFTReorderSimple_7285();
				FFTReorderSimple_7286();
				FFTReorderSimple_7287();
				FFTReorderSimple_7288();
				FFTReorderSimple_7289();
				FFTReorderSimple_7290();
				FFTReorderSimple_7291();
			WEIGHTED_ROUND_ROBIN_Joiner_7283();
			WEIGHTED_ROUND_ROBIN_Splitter_7292();
				FFTReorderSimple_7294();
				FFTReorderSimple_7295();
				FFTReorderSimple_7296();
				FFTReorderSimple_7297();
				FFTReorderSimple_7298();
				FFTReorderSimple_7299();
				FFTReorderSimple_7300();
				FFTReorderSimple_7301();
				FFTReorderSimple_7302();
				FFTReorderSimple_7303();
				FFTReorderSimple_7304();
				FFTReorderSimple_7305();
				FFTReorderSimple_7306();
				FFTReorderSimple_7307();
				FFTReorderSimple_7308();
				FFTReorderSimple_7309();
			WEIGHTED_ROUND_ROBIN_Joiner_7293();
			WEIGHTED_ROUND_ROBIN_Splitter_7310();
				CombineDFT_7312();
				CombineDFT_7313();
				CombineDFT_7314();
				CombineDFT_7315();
				CombineDFT_7316();
				CombineDFT_7317();
				CombineDFT_7318();
				CombineDFT_7319();
				CombineDFT_7320();
				CombineDFT_7321();
				CombineDFT_7322();
				CombineDFT_7323();
				CombineDFT_7324();
				CombineDFT_7325();
				CombineDFT_7326();
				CombineDFT_7327();
				CombineDFT_7328();
			WEIGHTED_ROUND_ROBIN_Joiner_7311();
			WEIGHTED_ROUND_ROBIN_Splitter_7329();
				CombineDFT_7331();
				CombineDFT_7332();
				CombineDFT_7333();
				CombineDFT_7334();
				CombineDFT_7335();
				CombineDFT_7336();
				CombineDFT_7337();
				CombineDFT_7338();
				CombineDFT_7339();
				CombineDFT_7340();
				CombineDFT_7341();
				CombineDFT_7342();
				CombineDFT_7343();
				CombineDFT_7344();
				CombineDFT_7345();
				CombineDFT_7346();
			WEIGHTED_ROUND_ROBIN_Joiner_7330();
			WEIGHTED_ROUND_ROBIN_Splitter_7347();
				CombineDFT_7349();
				CombineDFT_7350();
				CombineDFT_7351();
				CombineDFT_7352();
				CombineDFT_7353();
				CombineDFT_7354();
				CombineDFT_7355();
				CombineDFT_7356();
			WEIGHTED_ROUND_ROBIN_Joiner_7348();
			WEIGHTED_ROUND_ROBIN_Splitter_7357();
				CombineDFT_7359();
				CombineDFT_7360();
				CombineDFT_7361();
				CombineDFT_7362();
			WEIGHTED_ROUND_ROBIN_Joiner_7358();
			WEIGHTED_ROUND_ROBIN_Splitter_7363();
				CombineDFT_7365();
				CombineDFT_7366();
			WEIGHTED_ROUND_ROBIN_Joiner_7364();
			CombineDFT_7247();
			FFTReorderSimple_7248();
			WEIGHTED_ROUND_ROBIN_Splitter_7367();
				FFTReorderSimple_7369();
				FFTReorderSimple_7370();
			WEIGHTED_ROUND_ROBIN_Joiner_7368();
			WEIGHTED_ROUND_ROBIN_Splitter_7371();
				FFTReorderSimple_7373();
				FFTReorderSimple_7374();
				FFTReorderSimple_7375();
				FFTReorderSimple_7376();
			WEIGHTED_ROUND_ROBIN_Joiner_7372();
			WEIGHTED_ROUND_ROBIN_Splitter_7377();
				FFTReorderSimple_7379();
				FFTReorderSimple_7380();
				FFTReorderSimple_7381();
				FFTReorderSimple_7382();
				FFTReorderSimple_7383();
				FFTReorderSimple_7384();
				FFTReorderSimple_7385();
				FFTReorderSimple_7386();
			WEIGHTED_ROUND_ROBIN_Joiner_7378();
			WEIGHTED_ROUND_ROBIN_Splitter_7387();
				FFTReorderSimple_7389();
				FFTReorderSimple_7390();
				FFTReorderSimple_7391();
				FFTReorderSimple_7392();
				FFTReorderSimple_7393();
				FFTReorderSimple_7394();
				FFTReorderSimple_7395();
				FFTReorderSimple_7396();
				FFTReorderSimple_7397();
				FFTReorderSimple_7398();
				FFTReorderSimple_7399();
				FFTReorderSimple_7400();
				FFTReorderSimple_7401();
				FFTReorderSimple_7402();
				FFTReorderSimple_7403();
				FFTReorderSimple_7404();
			WEIGHTED_ROUND_ROBIN_Joiner_7388();
			WEIGHTED_ROUND_ROBIN_Splitter_7405();
				CombineDFT_7407();
				CombineDFT_7408();
				CombineDFT_7409();
				CombineDFT_7410();
				CombineDFT_7411();
				CombineDFT_7412();
				CombineDFT_7413();
				CombineDFT_7414();
				CombineDFT_7415();
				CombineDFT_7416();
				CombineDFT_7417();
				CombineDFT_7418();
				CombineDFT_7419();
				CombineDFT_7420();
				CombineDFT_7421();
				CombineDFT_7422();
				CombineDFT_7423();
			WEIGHTED_ROUND_ROBIN_Joiner_7406();
			WEIGHTED_ROUND_ROBIN_Splitter_7424();
				CombineDFT_7426();
				CombineDFT_7427();
				CombineDFT_7428();
				CombineDFT_7429();
				CombineDFT_7430();
				CombineDFT_7431();
				CombineDFT_7432();
				CombineDFT_7433();
				CombineDFT_7434();
				CombineDFT_7435();
				CombineDFT_7436();
				CombineDFT_7437();
				CombineDFT_7438();
				CombineDFT_7439();
				CombineDFT_7440();
				CombineDFT_7441();
			WEIGHTED_ROUND_ROBIN_Joiner_7425();
			WEIGHTED_ROUND_ROBIN_Splitter_7442();
				CombineDFT_7444();
				CombineDFT_7445();
				CombineDFT_7446();
				CombineDFT_7447();
				CombineDFT_7448();
				CombineDFT_7449();
				CombineDFT_7450();
				CombineDFT_7451();
			WEIGHTED_ROUND_ROBIN_Joiner_7443();
			WEIGHTED_ROUND_ROBIN_Splitter_7452();
				CombineDFT_7454();
				CombineDFT_7455();
				CombineDFT_7456();
				CombineDFT_7457();
			WEIGHTED_ROUND_ROBIN_Joiner_7453();
			WEIGHTED_ROUND_ROBIN_Splitter_7458();
				CombineDFT_7460();
				CombineDFT_7461();
			WEIGHTED_ROUND_ROBIN_Joiner_7459();
			CombineDFT_7258();
		WEIGHTED_ROUND_ROBIN_Joiner_7261();
		FloatPrinter_7259();
	ENDFOR
	return EXIT_SUCCESS;
}
