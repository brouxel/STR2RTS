#include "PEG2-FFT6_nocache.h"

buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_6014WEIGHTED_ROUND_ROBIN_Splitter_6017;
buffer_complex_t SplitJoin14_CombineDFT_Fiss_6036_6046_join[2];
buffer_complex_t SplitJoin8_CombineDFT_Fiss_6033_6043_join[2];
buffer_complex_t SplitJoin8_CombineDFT_Fiss_6033_6043_split[2];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_6034_6044_join[2];
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_6032_6042_join[2];
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_6030_6040_join[2];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_6031_6041_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5994WEIGHTED_ROUND_ROBIN_Splitter_5997;
buffer_complex_t SplitJoin16_CombineDFT_Fiss_6037_6047_split[2];
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_6032_6042_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_6010WEIGHTED_ROUND_ROBIN_Splitter_6013;
buffer_complex_t SplitJoin10_CombineDFT_Fiss_6034_6044_split[2];
buffer_complex_t CombineDFT_5990CPrinter_5991;
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_6030_6040_split[2];
buffer_complex_t FFTTestSource_5979FFTReorderSimple_5980;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_6026CombineDFT_5990;
buffer_complex_t SplitJoin12_CombineDFT_Fiss_6035_6045_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_6022WEIGHTED_ROUND_ROBIN_Splitter_6025;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_6002WEIGHTED_ROUND_ROBIN_Splitter_6005;
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_6029_6039_split[2];
buffer_complex_t FFTReorderSimple_5980WEIGHTED_ROUND_ROBIN_Splitter_5993;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_6006WEIGHTED_ROUND_ROBIN_Splitter_6009;
buffer_complex_t SplitJoin14_CombineDFT_Fiss_6036_6046_split[2];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_6031_6041_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5998WEIGHTED_ROUND_ROBIN_Splitter_6001;
buffer_complex_t SplitJoin12_CombineDFT_Fiss_6035_6045_split[2];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_6029_6039_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_6018WEIGHTED_ROUND_ROBIN_Splitter_6021;
buffer_complex_t SplitJoin16_CombineDFT_Fiss_6037_6047_join[2];


CombineDFT_6011_t CombineDFT_6011_s;
CombineDFT_6011_t CombineDFT_6012_s;
CombineDFT_6011_t CombineDFT_6015_s;
CombineDFT_6011_t CombineDFT_6016_s;
CombineDFT_6011_t CombineDFT_6019_s;
CombineDFT_6011_t CombineDFT_6020_s;
CombineDFT_6011_t CombineDFT_6023_s;
CombineDFT_6011_t CombineDFT_6024_s;
CombineDFT_6011_t CombineDFT_6027_s;
CombineDFT_6011_t CombineDFT_6028_s;
CombineDFT_6011_t CombineDFT_5990_s;

void FFTTestSource_5979() {
	complex_t c1;
	complex_t zero;
	c1.real = 1.0 ; 
	c1.imag = 0.0 ; 
	zero.real = 0.0 ; 
	zero.imag = 0.0 ; 
	push_complex(&FFTTestSource_5979FFTReorderSimple_5980, zero) ; 
	push_complex(&FFTTestSource_5979FFTReorderSimple_5980, c1) ; 
	FOR(int, i, 0,  < , 62, i++) {
		push_complex(&FFTTestSource_5979FFTReorderSimple_5980, zero) ; 
	}
	ENDFOR
}


void FFTReorderSimple_5980() {
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		complex_t __sa7 = {
			.real = 0,
			.imag = 0
		};
		__sa7 = ((complex_t) peek_complex(&FFTTestSource_5979FFTReorderSimple_5980, i)) ; 
		push_complex(&FFTReorderSimple_5980WEIGHTED_ROUND_ROBIN_Splitter_5993, __sa7) ; 
	}
	ENDFOR
	FOR(int, i, 1,  < , 64, i = (i + 2)) {
		complex_t __sa8 = {
			.real = 0,
			.imag = 0
		};
		__sa8 = ((complex_t) peek_complex(&FFTTestSource_5979FFTReorderSimple_5980, i)) ; 
		push_complex(&FFTReorderSimple_5980WEIGHTED_ROUND_ROBIN_Splitter_5993, __sa8) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 64, i++) {
		pop_complex(&FFTTestSource_5979FFTReorderSimple_5980) ; 
	}
	ENDFOR
}


void FFTReorderSimple_5995() {
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		complex_t __sa7 = {
			.real = 0,
			.imag = 0
		};
		__sa7 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_6029_6039_split[0], i)) ; 
		push_complex(&SplitJoin0_FFTReorderSimple_Fiss_6029_6039_join[0], __sa7) ; 
	}
	ENDFOR
	FOR(int, i, 1,  < , 32, i = (i + 2)) {
		complex_t __sa8 = {
			.real = 0,
			.imag = 0
		};
		__sa8 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_6029_6039_split[0], i)) ; 
		push_complex(&SplitJoin0_FFTReorderSimple_Fiss_6029_6039_join[0], __sa8) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 32, i++) {
		pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_6029_6039_split[0]) ; 
	}
	ENDFOR
}


void FFTReorderSimple_5996() {
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		complex_t __sa7 = {
			.real = 0,
			.imag = 0
		};
		__sa7 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_6029_6039_split[1], i)) ; 
		push_complex(&SplitJoin0_FFTReorderSimple_Fiss_6029_6039_join[1], __sa7) ; 
	}
	ENDFOR
	FOR(int, i, 1,  < , 32, i = (i + 2)) {
		complex_t __sa8 = {
			.real = 0,
			.imag = 0
		};
		__sa8 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_6029_6039_split[1], i)) ; 
		push_complex(&SplitJoin0_FFTReorderSimple_Fiss_6029_6039_join[1], __sa8) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 32, i++) {
		pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_6029_6039_split[1]) ; 
	}
	ENDFOR
}


void WEIGHTED_ROUND_ROBIN_Splitter_5993() {
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&SplitJoin0_FFTReorderSimple_Fiss_6029_6039_split[0], pop_complex(&FFTReorderSimple_5980WEIGHTED_ROUND_ROBIN_Splitter_5993));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&SplitJoin0_FFTReorderSimple_Fiss_6029_6039_split[1], pop_complex(&FFTReorderSimple_5980WEIGHTED_ROUND_ROBIN_Splitter_5993));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_5994() {
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5994WEIGHTED_ROUND_ROBIN_Splitter_5997, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_6029_6039_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5994WEIGHTED_ROUND_ROBIN_Splitter_5997, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_6029_6039_join[1]));
	ENDFOR
}

void FFTReorderSimple_5999(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_6030_6040_split[0], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_6030_6040_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_6030_6040_split[0], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_6030_6040_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_6030_6040_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_6000(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_6030_6040_split[1], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_6030_6040_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_6030_6040_split[1], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_6030_6040_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_6030_6040_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5997() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_6030_6040_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5994WEIGHTED_ROUND_ROBIN_Splitter_5997));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_6030_6040_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5994WEIGHTED_ROUND_ROBIN_Splitter_5997));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5998() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5998WEIGHTED_ROUND_ROBIN_Splitter_6001, pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_6030_6040_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5998WEIGHTED_ROUND_ROBIN_Splitter_6001, pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_6030_6040_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_6003(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_6031_6041_split[0], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_6031_6041_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_6031_6041_split[0], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_6031_6041_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_6031_6041_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_6004(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_6031_6041_split[1], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_6031_6041_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_6031_6041_split[1], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_6031_6041_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_6031_6041_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6001() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_6031_6041_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5998WEIGHTED_ROUND_ROBIN_Splitter_6001));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_6031_6041_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5998WEIGHTED_ROUND_ROBIN_Splitter_6001));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6002() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6002WEIGHTED_ROUND_ROBIN_Splitter_6005, pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_6031_6041_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6002WEIGHTED_ROUND_ROBIN_Splitter_6005, pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_6031_6041_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_6007(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_6032_6042_split[0], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_6032_6042_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_6032_6042_split[0], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_6032_6042_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_6032_6042_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_6008(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_6032_6042_split[1], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_6032_6042_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_6032_6042_split[1], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_6032_6042_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_6032_6042_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6005() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_6032_6042_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6002WEIGHTED_ROUND_ROBIN_Splitter_6005));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_6032_6042_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6002WEIGHTED_ROUND_ROBIN_Splitter_6005));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6006() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6006WEIGHTED_ROUND_ROBIN_Splitter_6009, pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_6032_6042_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6006WEIGHTED_ROUND_ROBIN_Splitter_6009, pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_6032_6042_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_6011(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_6033_6043_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_6033_6043_split[0], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_6011_s.wn.real) - (w.imag * CombineDFT_6011_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_6011_s.wn.imag) + (w.imag * CombineDFT_6011_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_6033_6043_split[0]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_6033_6043_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_6012(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_6033_6043_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_6033_6043_split[1], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_6012_s.wn.real) - (w.imag * CombineDFT_6012_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_6012_s.wn.imag) + (w.imag * CombineDFT_6012_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_6033_6043_split[1]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_6033_6043_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6009() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		push_complex(&SplitJoin8_CombineDFT_Fiss_6033_6043_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6006WEIGHTED_ROUND_ROBIN_Splitter_6009));
		push_complex(&SplitJoin8_CombineDFT_Fiss_6033_6043_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6006WEIGHTED_ROUND_ROBIN_Splitter_6009));
		push_complex(&SplitJoin8_CombineDFT_Fiss_6033_6043_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6006WEIGHTED_ROUND_ROBIN_Splitter_6009));
		push_complex(&SplitJoin8_CombineDFT_Fiss_6033_6043_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6006WEIGHTED_ROUND_ROBIN_Splitter_6009));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6010() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6010WEIGHTED_ROUND_ROBIN_Splitter_6013, pop_complex(&SplitJoin8_CombineDFT_Fiss_6033_6043_join[0]));
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6010WEIGHTED_ROUND_ROBIN_Splitter_6013, pop_complex(&SplitJoin8_CombineDFT_Fiss_6033_6043_join[0]));
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6010WEIGHTED_ROUND_ROBIN_Splitter_6013, pop_complex(&SplitJoin8_CombineDFT_Fiss_6033_6043_join[1]));
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6010WEIGHTED_ROUND_ROBIN_Splitter_6013, pop_complex(&SplitJoin8_CombineDFT_Fiss_6033_6043_join[1]));
	ENDFOR
}}

void CombineDFT_6015(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_6034_6044_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_6034_6044_split[0], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_6015_s.wn.real) - (w.imag * CombineDFT_6015_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_6015_s.wn.imag) + (w.imag * CombineDFT_6015_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_6034_6044_split[0]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_6034_6044_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_6016(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_6034_6044_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_6034_6044_split[1], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_6016_s.wn.real) - (w.imag * CombineDFT_6016_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_6016_s.wn.imag) + (w.imag * CombineDFT_6016_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_6034_6044_split[1]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_6034_6044_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6013() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin10_CombineDFT_Fiss_6034_6044_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6010WEIGHTED_ROUND_ROBIN_Splitter_6013));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin10_CombineDFT_Fiss_6034_6044_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6010WEIGHTED_ROUND_ROBIN_Splitter_6013));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6014() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6014WEIGHTED_ROUND_ROBIN_Splitter_6017, pop_complex(&SplitJoin10_CombineDFT_Fiss_6034_6044_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6014WEIGHTED_ROUND_ROBIN_Splitter_6017, pop_complex(&SplitJoin10_CombineDFT_Fiss_6034_6044_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_6019(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_6035_6045_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_6035_6045_split[0], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_6019_s.wn.real) - (w.imag * CombineDFT_6019_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_6019_s.wn.imag) + (w.imag * CombineDFT_6019_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_6035_6045_split[0]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_6035_6045_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_6020(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_6035_6045_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_6035_6045_split[1], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_6020_s.wn.real) - (w.imag * CombineDFT_6020_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_6020_s.wn.imag) + (w.imag * CombineDFT_6020_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_6035_6045_split[1]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_6035_6045_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6017() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin12_CombineDFT_Fiss_6035_6045_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6014WEIGHTED_ROUND_ROBIN_Splitter_6017));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin12_CombineDFT_Fiss_6035_6045_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6014WEIGHTED_ROUND_ROBIN_Splitter_6017));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6018() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6018WEIGHTED_ROUND_ROBIN_Splitter_6021, pop_complex(&SplitJoin12_CombineDFT_Fiss_6035_6045_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6018WEIGHTED_ROUND_ROBIN_Splitter_6021, pop_complex(&SplitJoin12_CombineDFT_Fiss_6035_6045_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_6023(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_6036_6046_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_6036_6046_split[0], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_6023_s.wn.real) - (w.imag * CombineDFT_6023_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_6023_s.wn.imag) + (w.imag * CombineDFT_6023_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_6036_6046_split[0]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_6036_6046_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_6024(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_6036_6046_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_6036_6046_split[1], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_6024_s.wn.real) - (w.imag * CombineDFT_6024_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_6024_s.wn.imag) + (w.imag * CombineDFT_6024_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_6036_6046_split[1]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_6036_6046_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6021() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_complex(&SplitJoin14_CombineDFT_Fiss_6036_6046_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6018WEIGHTED_ROUND_ROBIN_Splitter_6021));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_complex(&SplitJoin14_CombineDFT_Fiss_6036_6046_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6018WEIGHTED_ROUND_ROBIN_Splitter_6021));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6022() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6022WEIGHTED_ROUND_ROBIN_Splitter_6025, pop_complex(&SplitJoin14_CombineDFT_Fiss_6036_6046_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6022WEIGHTED_ROUND_ROBIN_Splitter_6025, pop_complex(&SplitJoin14_CombineDFT_Fiss_6036_6046_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_6027() {
	complex_t w;
	complex_t y0;
	complex_t y1;
	complex_t y1w;
	complex_t w_next;
	complex_t results[32];
	w.real = 1.0 ; 
	w.imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i++) {
		complex_t __sa1 = {
			.real = 0,
			.imag = 0
		};
		complex_t __sa2 = {
			.real = 0,
			.imag = 0
		};
		__sa1 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_6037_6047_split[0], i)) ; 
		y0.real = __sa1.real ; 
		y0.imag = __sa1.imag ; 
		__sa2 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_6037_6047_split[0], (16 + i))) ; 
		y1.real = __sa2.real ; 
		y1.imag = __sa2.imag ; 
		y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
		y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
		results[i].real = (y0.real + y1w.real) ; 
		results[i].imag = (y0.imag + y1w.imag) ; 
		results[(16 + i)].real = (y0.real - y1w.real) ; 
		results[(16 + i)].imag = (y0.imag - y1w.imag) ; 
		w_next.real = ((w.real * CombineDFT_6027_s.wn.real) - (w.imag * CombineDFT_6027_s.wn.imag)) ; 
		w_next.imag = ((w.real * CombineDFT_6027_s.wn.imag) + (w.imag * CombineDFT_6027_s.wn.real)) ; 
		w.real = w_next.real ; 
		w.imag = w_next.imag ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 32, i++) {
		pop_complex(&SplitJoin16_CombineDFT_Fiss_6037_6047_split[0]) ; 
		push_complex(&SplitJoin16_CombineDFT_Fiss_6037_6047_join[0], results[i]) ; 
	}
	ENDFOR
}


void CombineDFT_6028() {
	complex_t w;
	complex_t y0;
	complex_t y1;
	complex_t y1w;
	complex_t w_next;
	complex_t results[32];
	w.real = 1.0 ; 
	w.imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i++) {
		complex_t __sa1 = {
			.real = 0,
			.imag = 0
		};
		complex_t __sa2 = {
			.real = 0,
			.imag = 0
		};
		__sa1 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_6037_6047_split[1], i)) ; 
		y0.real = __sa1.real ; 
		y0.imag = __sa1.imag ; 
		__sa2 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_6037_6047_split[1], (16 + i))) ; 
		y1.real = __sa2.real ; 
		y1.imag = __sa2.imag ; 
		y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
		y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
		results[i].real = (y0.real + y1w.real) ; 
		results[i].imag = (y0.imag + y1w.imag) ; 
		results[(16 + i)].real = (y0.real - y1w.real) ; 
		results[(16 + i)].imag = (y0.imag - y1w.imag) ; 
		w_next.real = ((w.real * CombineDFT_6028_s.wn.real) - (w.imag * CombineDFT_6028_s.wn.imag)) ; 
		w_next.imag = ((w.real * CombineDFT_6028_s.wn.imag) + (w.imag * CombineDFT_6028_s.wn.real)) ; 
		w.real = w_next.real ; 
		w.imag = w_next.imag ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 32, i++) {
		pop_complex(&SplitJoin16_CombineDFT_Fiss_6037_6047_split[1]) ; 
		push_complex(&SplitJoin16_CombineDFT_Fiss_6037_6047_join[1], results[i]) ; 
	}
	ENDFOR
}


void WEIGHTED_ROUND_ROBIN_Splitter_6025() {
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&SplitJoin16_CombineDFT_Fiss_6037_6047_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6022WEIGHTED_ROUND_ROBIN_Splitter_6025));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&SplitJoin16_CombineDFT_Fiss_6037_6047_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6022WEIGHTED_ROUND_ROBIN_Splitter_6025));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_6026() {
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6026CombineDFT_5990, pop_complex(&SplitJoin16_CombineDFT_Fiss_6037_6047_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6026CombineDFT_5990, pop_complex(&SplitJoin16_CombineDFT_Fiss_6037_6047_join[1]));
	ENDFOR
}

void CombineDFT_5990() {
	complex_t w;
	complex_t y0;
	complex_t y1;
	complex_t y1w;
	complex_t w_next;
	complex_t results[64];
	w.real = 1.0 ; 
	w.imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		complex_t __sa1 = {
			.real = 0,
			.imag = 0
		};
		complex_t __sa2 = {
			.real = 0,
			.imag = 0
		};
		__sa1 = ((complex_t) peek_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6026CombineDFT_5990, i)) ; 
		y0.real = __sa1.real ; 
		y0.imag = __sa1.imag ; 
		__sa2 = ((complex_t) peek_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6026CombineDFT_5990, (32 + i))) ; 
		y1.real = __sa2.real ; 
		y1.imag = __sa2.imag ; 
		y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
		y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
		results[i].real = (y0.real + y1w.real) ; 
		results[i].imag = (y0.imag + y1w.imag) ; 
		results[(32 + i)].real = (y0.real - y1w.real) ; 
		results[(32 + i)].imag = (y0.imag - y1w.imag) ; 
		w_next.real = ((w.real * CombineDFT_5990_s.wn.real) - (w.imag * CombineDFT_5990_s.wn.imag)) ; 
		w_next.imag = ((w.real * CombineDFT_5990_s.wn.imag) + (w.imag * CombineDFT_5990_s.wn.real)) ; 
		w.real = w_next.real ; 
		w.imag = w_next.imag ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 64, i++) {
		pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6026CombineDFT_5990) ; 
		push_complex(&CombineDFT_5990CPrinter_5991, results[i]) ; 
	}
	ENDFOR
}


void CPrinter_5991(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&CombineDFT_5990CPrinter_5991));
		printf("%.10f", c.real);
		printf("\n");
		printf("%.10f", c.imag);
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6014WEIGHTED_ROUND_ROBIN_Splitter_6017);
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_6036_6046_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 2, __iter_init_1_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_6033_6043_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_6033_6043_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 2, __iter_init_3_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_6034_6044_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 2, __iter_init_4_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_6032_6042_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 2, __iter_init_5_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_6030_6040_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 2, __iter_init_6_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_6031_6041_split[__iter_init_6_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5994WEIGHTED_ROUND_ROBIN_Splitter_5997);
	FOR(int, __iter_init_7_, 0, <, 2, __iter_init_7_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_6037_6047_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_6032_6042_split[__iter_init_8_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6010WEIGHTED_ROUND_ROBIN_Splitter_6013);
	FOR(int, __iter_init_9_, 0, <, 2, __iter_init_9_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_6034_6044_split[__iter_init_9_]);
	ENDFOR
	init_buffer_complex(&CombineDFT_5990CPrinter_5991);
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_6030_6040_split[__iter_init_10_]);
	ENDFOR
	init_buffer_complex(&FFTTestSource_5979FFTReorderSimple_5980);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6026CombineDFT_5990);
	FOR(int, __iter_init_11_, 0, <, 2, __iter_init_11_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_6035_6045_join[__iter_init_11_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6022WEIGHTED_ROUND_ROBIN_Splitter_6025);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6002WEIGHTED_ROUND_ROBIN_Splitter_6005);
	FOR(int, __iter_init_12_, 0, <, 2, __iter_init_12_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_6029_6039_split[__iter_init_12_]);
	ENDFOR
	init_buffer_complex(&FFTReorderSimple_5980WEIGHTED_ROUND_ROBIN_Splitter_5993);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6006WEIGHTED_ROUND_ROBIN_Splitter_6009);
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_6036_6046_split[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_6031_6041_join[__iter_init_14_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5998WEIGHTED_ROUND_ROBIN_Splitter_6001);
	FOR(int, __iter_init_15_, 0, <, 2, __iter_init_15_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_6035_6045_split[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_6029_6039_join[__iter_init_16_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_6018WEIGHTED_ROUND_ROBIN_Splitter_6021);
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_6037_6047_join[__iter_init_17_]);
	ENDFOR
// --- init: CombineDFT_6011
	 {
	 ; 
	CombineDFT_6011_s.wn.real = -1.0 ; 
	CombineDFT_6011_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_6012
	 {
	 ; 
	CombineDFT_6012_s.wn.real = -1.0 ; 
	CombineDFT_6012_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_6015
	 {
	 ; 
	CombineDFT_6015_s.wn.real = -4.371139E-8 ; 
	CombineDFT_6015_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_6016
	 {
	 ; 
	CombineDFT_6016_s.wn.real = -4.371139E-8 ; 
	CombineDFT_6016_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_6019
	 {
	 ; 
	CombineDFT_6019_s.wn.real = 0.70710677 ; 
	CombineDFT_6019_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_6020
	 {
	 ; 
	CombineDFT_6020_s.wn.real = 0.70710677 ; 
	CombineDFT_6020_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_6023
	 {
	 ; 
	CombineDFT_6023_s.wn.real = 0.9238795 ; 
	CombineDFT_6023_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_6024
	 {
	 ; 
	CombineDFT_6024_s.wn.real = 0.9238795 ; 
	CombineDFT_6024_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_6027
	 {
	 ; 
	CombineDFT_6027_s.wn.real = 0.98078525 ; 
	CombineDFT_6027_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_6028
	 {
	 ; 
	CombineDFT_6028_s.wn.real = 0.98078525 ; 
	CombineDFT_6028_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_5990
	 {
	 ; 
	CombineDFT_5990_s.wn.real = 0.9951847 ; 
	CombineDFT_5990_s.wn.imag = -0.09801714 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FFTTestSource_5979();
		FFTReorderSimple_5980();
		WEIGHTED_ROUND_ROBIN_Splitter_5993();
			FFTReorderSimple_5995();
			FFTReorderSimple_5996();
		WEIGHTED_ROUND_ROBIN_Joiner_5994();
		WEIGHTED_ROUND_ROBIN_Splitter_5997();
			FFTReorderSimple_5999();
			FFTReorderSimple_6000();
		WEIGHTED_ROUND_ROBIN_Joiner_5998();
		WEIGHTED_ROUND_ROBIN_Splitter_6001();
			FFTReorderSimple_6003();
			FFTReorderSimple_6004();
		WEIGHTED_ROUND_ROBIN_Joiner_6002();
		WEIGHTED_ROUND_ROBIN_Splitter_6005();
			FFTReorderSimple_6007();
			FFTReorderSimple_6008();
		WEIGHTED_ROUND_ROBIN_Joiner_6006();
		WEIGHTED_ROUND_ROBIN_Splitter_6009();
			CombineDFT_6011();
			CombineDFT_6012();
		WEIGHTED_ROUND_ROBIN_Joiner_6010();
		WEIGHTED_ROUND_ROBIN_Splitter_6013();
			CombineDFT_6015();
			CombineDFT_6016();
		WEIGHTED_ROUND_ROBIN_Joiner_6014();
		WEIGHTED_ROUND_ROBIN_Splitter_6017();
			CombineDFT_6019();
			CombineDFT_6020();
		WEIGHTED_ROUND_ROBIN_Joiner_6018();
		WEIGHTED_ROUND_ROBIN_Splitter_6021();
			CombineDFT_6023();
			CombineDFT_6024();
		WEIGHTED_ROUND_ROBIN_Joiner_6022();
		WEIGHTED_ROUND_ROBIN_Splitter_6025();
			CombineDFT_6027();
			CombineDFT_6028();
		WEIGHTED_ROUND_ROBIN_Joiner_6026();
		CombineDFT_5990();
		CPrinter_5991();
	ENDFOR
	return EXIT_SUCCESS;
}
