#include "PEG10-FFT5.h"

buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5389WEIGHTED_ROUND_ROBIN_Splitter_5231;
buffer_complex_t SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_5057_5281_5406_5426_join[2];
buffer_complex_t SplitJoin20_SplitJoin14_SplitJoin14_split1_5045_5289_5410_5449_split[2];
buffer_complex_t Pre_CollapsedDataParallel_1_5217butterfly_5118;
buffer_complex_t butterfly_5117Post_CollapsedDataParallel_2_5215;
buffer_complex_t SplitJoin76_SplitJoin55_SplitJoin55_AnonFilter_a0_5073_5322_5414_5430_join[2];
buffer_complex_t butterfly_5116Post_CollapsedDataParallel_2_5212;
buffer_complex_t SplitJoin22_SplitJoin16_SplitJoin16_split2_5047_5290_5371_5450_split[4];
buffer_complex_t SplitJoin12_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child0_5366_5440_split[4];
buffer_complex_t SplitJoin52_SplitJoin33_SplitJoin33_split2_5038_5304_5368_5447_join[2];
buffer_complex_t butterfly_5118Post_CollapsedDataParallel_2_5218;
buffer_complex_t SplitJoin16_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child0_5372_5443_split[2];
buffer_complex_t SplitJoin16_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child0_5372_5443_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_5226butterfly_5121;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5274WEIGHTED_ROUND_ROBIN_Splitter_5392;
buffer_complex_t Pre_CollapsedDataParallel_1_5220butterfly_5119;
buffer_complex_t butterfly_5120Post_CollapsedDataParallel_2_5224;
buffer_complex_t butterfly_5121Post_CollapsedDataParallel_2_5227;
buffer_complex_t SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_5059_5282_5407_5427_join[2];
buffer_complex_t SplitJoin39_SplitJoin22_SplitJoin22_split2_5049_5295_5373_5451_join[4];
buffer_complex_t SplitJoin102_SplitJoin81_SplitJoin81_AnonFilter_a0_5109_5340_5421_5438_split[2];
buffer_complex_t SplitJoin39_SplitJoin22_SplitJoin22_split2_5049_5295_5373_5451_split[4];
buffer_complex_t SplitJoin24_magnitude_Fiss_5411_5452_split[10];
buffer_complex_t SplitJoin80_SplitJoin59_SplitJoin59_AnonFilter_a0_5079_5325_5415_5431_join[2];
buffer_complex_t SplitJoin20_SplitJoin14_SplitJoin14_split1_5045_5289_5410_5449_join[2];
buffer_complex_t SplitJoin92_SplitJoin71_SplitJoin71_AnonFilter_a0_5095_5333_5418_5435_join[2];
buffer_complex_t SplitJoin10_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_Hier_5408_5439_split[2];
buffer_complex_t SplitJoin74_SplitJoin53_SplitJoin53_AnonFilter_a0_5071_5321_5413_5429_split[2];
buffer_complex_t SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_5053_5279_5405_5424_join[2];
buffer_complex_t SplitJoin86_SplitJoin65_SplitJoin65_AnonFilter_a0_5087_5329_5416_5433_split[2];
buffer_complex_t SplitJoin80_SplitJoin59_SplitJoin59_AnonFilter_a0_5079_5325_5415_5431_split[2];
buffer_complex_t SplitJoin92_SplitJoin71_SplitJoin71_AnonFilter_a0_5095_5333_5418_5435_split[2];
buffer_complex_t SplitJoin18_SplitJoin12_SplitJoin12_split2_5034_5287_5365_5444_split[2];
buffer_complex_t Pre_CollapsedDataParallel_1_5208butterfly_5115;
buffer_complex_t SplitJoin50_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child1_5374_5446_join[2];
buffer_complex_t butterfly_5115Post_CollapsedDataParallel_2_5209;
buffer_complex_t Pre_CollapsedDataParallel_1_5214butterfly_5117;
buffer_complex_t Pre_CollapsedDataParallel_1_5223butterfly_5120;
buffer_complex_t SplitJoin88_SplitJoin67_SplitJoin67_AnonFilter_a0_5089_5330_5417_5434_split[2];
buffer_complex_t SplitJoin88_SplitJoin67_SplitJoin67_AnonFilter_a0_5089_5330_5417_5434_join[2];
buffer_complex_t SplitJoin14_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_Hier_5409_5442_join[2];
buffer_complex_t SplitJoin12_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child0_5366_5440_join[4];
buffer_complex_t SplitJoin18_SplitJoin12_SplitJoin12_split2_5034_5287_5365_5444_join[2];
buffer_complex_t SplitJoin46_SplitJoin29_SplitJoin29_split2_5036_5301_5367_5445_split[2];
buffer_complex_t SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_5055_5280_5363_5425_join[2];
buffer_complex_t SplitJoin84_SplitJoin63_SplitJoin63_AnonFilter_a0_5085_5328_5364_5432_split[2];
buffer_complex_t SplitJoin76_SplitJoin55_SplitJoin55_AnonFilter_a0_5073_5322_5414_5430_split[2];
buffer_complex_t SplitJoin56_SplitJoin37_SplitJoin37_split2_5040_5307_5370_5448_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5393sink_5142;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5386WEIGHTED_ROUND_ROBIN_Splitter_5273;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5380WEIGHTED_ROUND_ROBIN_Splitter_5381;
buffer_complex_t SplitJoin84_SplitJoin63_SplitJoin63_AnonFilter_a0_5085_5328_5364_5432_join[2];
buffer_complex_t SplitJoin86_SplitJoin65_SplitJoin65_AnonFilter_a0_5087_5329_5416_5433_join[2];
buffer_complex_t SplitJoin22_SplitJoin16_SplitJoin16_split2_5047_5290_5371_5450_join[4];
buffer_complex_t SplitJoin0_source_Fiss_5404_5423_split[2];
buffer_complex_t SplitJoin10_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_Hier_5408_5439_join[2];
buffer_complex_t SplitJoin14_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_Hier_5409_5442_split[2];
buffer_complex_t Pre_CollapsedDataParallel_1_5229butterfly_5122;
buffer_complex_t SplitJoin98_SplitJoin77_SplitJoin77_AnonFilter_a0_5103_5337_5420_5437_split[2];
buffer_complex_t SplitJoin70_SplitJoin49_SplitJoin49_AnonFilter_a0_5065_5318_5412_5428_split[2];
buffer_complex_t SplitJoin96_SplitJoin75_SplitJoin75_AnonFilter_a0_5101_5336_5419_5436_split[2];
buffer_float_t SplitJoin24_magnitude_Fiss_5411_5452_join[10];
buffer_complex_t SplitJoin98_SplitJoin77_SplitJoin77_AnonFilter_a0_5103_5337_5420_5437_join[2];
buffer_complex_t SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_5059_5282_5407_5427_split[2];
buffer_complex_t SplitJoin63_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child1_5369_5441_split[4];
buffer_complex_t Pre_CollapsedDataParallel_1_5211butterfly_5116;
buffer_complex_t SplitJoin70_SplitJoin49_SplitJoin49_AnonFilter_a0_5065_5318_5412_5428_join[2];
buffer_complex_t SplitJoin46_SplitJoin29_SplitJoin29_split2_5036_5301_5367_5445_join[2];
buffer_complex_t SplitJoin50_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child1_5374_5446_split[2];
buffer_complex_t SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_5053_5279_5405_5424_split[2];
buffer_complex_t SplitJoin102_SplitJoin81_SplitJoin81_AnonFilter_a0_5109_5340_5421_5438_join[2];
buffer_complex_t SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_5055_5280_5363_5425_split[2];
buffer_complex_t SplitJoin96_SplitJoin75_SplitJoin75_AnonFilter_a0_5101_5336_5419_5436_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5232WEIGHTED_ROUND_ROBIN_Splitter_5375;
buffer_complex_t SplitJoin52_SplitJoin33_SplitJoin33_split2_5038_5304_5368_5447_split[2];
buffer_complex_t SplitJoin63_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child1_5369_5441_join[4];
buffer_complex_t butterfly_5122Post_CollapsedDataParallel_2_5230;
buffer_complex_t SplitJoin56_SplitJoin37_SplitJoin37_split2_5040_5307_5370_5448_split[2];
buffer_complex_t SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_5057_5281_5406_5426_split[2];
buffer_complex_t SplitJoin0_source_Fiss_5404_5423_join[2];
buffer_complex_t SplitJoin74_SplitJoin53_SplitJoin53_AnonFilter_a0_5071_5321_5413_5429_join[2];
buffer_complex_t butterfly_5119Post_CollapsedDataParallel_2_5221;



void source(buffer_void_t *chanin, buffer_complex_t *chanout) {
		complex_t t;
		t.imag = 0.0 ; 
		t.real = 0.9501 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.2311 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.6068 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.486 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.8913 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.7621 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.4565 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.0185 ; 
		push_complex(&(*chanout), t) ; 
	}


void source_5390() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		source(&(SplitJoin0_source_Fiss_5404_5423_split[0]), &(SplitJoin0_source_Fiss_5404_5423_join[0]));
	ENDFOR
}

void source_5391() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		source(&(SplitJoin0_source_Fiss_5404_5423_split[1]), &(SplitJoin0_source_Fiss_5404_5423_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5388() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_5389() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5389WEIGHTED_ROUND_ROBIN_Splitter_5231, pop_complex(&SplitJoin0_source_Fiss_5404_5423_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5389WEIGHTED_ROUND_ROBIN_Splitter_5231, pop_complex(&SplitJoin0_source_Fiss_5404_5423_join[1]));
		ENDFOR
	ENDFOR
}}

void Identity(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t __tmp1398 = pop_complex(&(*chanin));
		push_complex(&(*chanout), __tmp1398) ; 
	}


void Identity_5061() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Identity(&(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_5059_5282_5407_5427_split[0]), &(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_5059_5282_5407_5427_join[0]));
	ENDFOR
}

void Identity_5063() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Identity(&(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_5059_5282_5407_5427_split[1]), &(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_5059_5282_5407_5427_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5237() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_5059_5282_5407_5427_split[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_5057_5281_5406_5426_split[0]));
		push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_5059_5282_5407_5427_split[1], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_5057_5281_5406_5426_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5238() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_5057_5281_5406_5426_join[0], pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_5059_5282_5407_5427_join[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_5057_5281_5406_5426_join[0], pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_5059_5282_5407_5427_join[1]));
	ENDFOR
}}

void Identity_5067() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Identity(&(SplitJoin70_SplitJoin49_SplitJoin49_AnonFilter_a0_5065_5318_5412_5428_split[0]), &(SplitJoin70_SplitJoin49_SplitJoin49_AnonFilter_a0_5065_5318_5412_5428_join[0]));
	ENDFOR
}

void Identity_5069() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Identity(&(SplitJoin70_SplitJoin49_SplitJoin49_AnonFilter_a0_5065_5318_5412_5428_split[1]), &(SplitJoin70_SplitJoin49_SplitJoin49_AnonFilter_a0_5065_5318_5412_5428_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5239() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin70_SplitJoin49_SplitJoin49_AnonFilter_a0_5065_5318_5412_5428_split[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_5057_5281_5406_5426_split[1]));
		push_complex(&SplitJoin70_SplitJoin49_SplitJoin49_AnonFilter_a0_5065_5318_5412_5428_split[1], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_5057_5281_5406_5426_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5240() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_5057_5281_5406_5426_join[1], pop_complex(&SplitJoin70_SplitJoin49_SplitJoin49_AnonFilter_a0_5065_5318_5412_5428_join[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_5057_5281_5406_5426_join[1], pop_complex(&SplitJoin70_SplitJoin49_SplitJoin49_AnonFilter_a0_5065_5318_5412_5428_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_5235() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_5057_5281_5406_5426_split[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_5055_5280_5363_5425_split[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_5057_5281_5406_5426_split[1], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_5055_5280_5363_5425_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5236() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_5055_5280_5363_5425_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_5057_5281_5406_5426_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_5055_5280_5363_5425_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_5057_5281_5406_5426_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_5055_5280_5363_5425_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_5057_5281_5406_5426_join[1]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_5055_5280_5363_5425_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_5057_5281_5406_5426_join[1]));
	ENDFOR
}}

void Identity_5075() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Identity(&(SplitJoin76_SplitJoin55_SplitJoin55_AnonFilter_a0_5073_5322_5414_5430_split[0]), &(SplitJoin76_SplitJoin55_SplitJoin55_AnonFilter_a0_5073_5322_5414_5430_join[0]));
	ENDFOR
}

void Identity_5077() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Identity(&(SplitJoin76_SplitJoin55_SplitJoin55_AnonFilter_a0_5073_5322_5414_5430_split[1]), &(SplitJoin76_SplitJoin55_SplitJoin55_AnonFilter_a0_5073_5322_5414_5430_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5243() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin76_SplitJoin55_SplitJoin55_AnonFilter_a0_5073_5322_5414_5430_split[0], pop_complex(&SplitJoin74_SplitJoin53_SplitJoin53_AnonFilter_a0_5071_5321_5413_5429_split[0]));
		push_complex(&SplitJoin76_SplitJoin55_SplitJoin55_AnonFilter_a0_5073_5322_5414_5430_split[1], pop_complex(&SplitJoin74_SplitJoin53_SplitJoin53_AnonFilter_a0_5071_5321_5413_5429_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5244() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin74_SplitJoin53_SplitJoin53_AnonFilter_a0_5071_5321_5413_5429_join[0], pop_complex(&SplitJoin76_SplitJoin55_SplitJoin55_AnonFilter_a0_5073_5322_5414_5430_join[0]));
		push_complex(&SplitJoin74_SplitJoin53_SplitJoin53_AnonFilter_a0_5071_5321_5413_5429_join[0], pop_complex(&SplitJoin76_SplitJoin55_SplitJoin55_AnonFilter_a0_5073_5322_5414_5430_join[1]));
	ENDFOR
}}

void Identity_5081() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Identity(&(SplitJoin80_SplitJoin59_SplitJoin59_AnonFilter_a0_5079_5325_5415_5431_split[0]), &(SplitJoin80_SplitJoin59_SplitJoin59_AnonFilter_a0_5079_5325_5415_5431_join[0]));
	ENDFOR
}

void Identity_5083() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Identity(&(SplitJoin80_SplitJoin59_SplitJoin59_AnonFilter_a0_5079_5325_5415_5431_split[1]), &(SplitJoin80_SplitJoin59_SplitJoin59_AnonFilter_a0_5079_5325_5415_5431_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5245() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin80_SplitJoin59_SplitJoin59_AnonFilter_a0_5079_5325_5415_5431_split[0], pop_complex(&SplitJoin74_SplitJoin53_SplitJoin53_AnonFilter_a0_5071_5321_5413_5429_split[1]));
		push_complex(&SplitJoin80_SplitJoin59_SplitJoin59_AnonFilter_a0_5079_5325_5415_5431_split[1], pop_complex(&SplitJoin74_SplitJoin53_SplitJoin53_AnonFilter_a0_5071_5321_5413_5429_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5246() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin74_SplitJoin53_SplitJoin53_AnonFilter_a0_5071_5321_5413_5429_join[1], pop_complex(&SplitJoin80_SplitJoin59_SplitJoin59_AnonFilter_a0_5079_5325_5415_5431_join[0]));
		push_complex(&SplitJoin74_SplitJoin53_SplitJoin53_AnonFilter_a0_5071_5321_5413_5429_join[1], pop_complex(&SplitJoin80_SplitJoin59_SplitJoin59_AnonFilter_a0_5079_5325_5415_5431_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_5241() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		push_complex(&SplitJoin74_SplitJoin53_SplitJoin53_AnonFilter_a0_5071_5321_5413_5429_split[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_5055_5280_5363_5425_split[1]));
		push_complex(&SplitJoin74_SplitJoin53_SplitJoin53_AnonFilter_a0_5071_5321_5413_5429_split[1], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_5055_5280_5363_5425_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5242() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_5055_5280_5363_5425_join[1], pop_complex(&SplitJoin74_SplitJoin53_SplitJoin53_AnonFilter_a0_5071_5321_5413_5429_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_5055_5280_5363_5425_join[1], pop_complex(&SplitJoin74_SplitJoin53_SplitJoin53_AnonFilter_a0_5071_5321_5413_5429_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_5055_5280_5363_5425_join[1], pop_complex(&SplitJoin74_SplitJoin53_SplitJoin53_AnonFilter_a0_5071_5321_5413_5429_join[1]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_5055_5280_5363_5425_join[1], pop_complex(&SplitJoin74_SplitJoin53_SplitJoin53_AnonFilter_a0_5071_5321_5413_5429_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_5233() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 20, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_5055_5280_5363_5425_split[0], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_5053_5279_5405_5424_split[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_5055_5280_5363_5425_split[1], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_5053_5279_5405_5424_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5234() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_5053_5279_5405_5424_join[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_5055_5280_5363_5425_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_5053_5279_5405_5424_join[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_5055_5280_5363_5425_join[1]));
		ENDFOR
	ENDFOR
}}

void Identity_5091() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Identity(&(SplitJoin88_SplitJoin67_SplitJoin67_AnonFilter_a0_5089_5330_5417_5434_split[0]), &(SplitJoin88_SplitJoin67_SplitJoin67_AnonFilter_a0_5089_5330_5417_5434_join[0]));
	ENDFOR
}

void Identity_5093() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Identity(&(SplitJoin88_SplitJoin67_SplitJoin67_AnonFilter_a0_5089_5330_5417_5434_split[1]), &(SplitJoin88_SplitJoin67_SplitJoin67_AnonFilter_a0_5089_5330_5417_5434_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5251() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin88_SplitJoin67_SplitJoin67_AnonFilter_a0_5089_5330_5417_5434_split[0], pop_complex(&SplitJoin86_SplitJoin65_SplitJoin65_AnonFilter_a0_5087_5329_5416_5433_split[0]));
		push_complex(&SplitJoin88_SplitJoin67_SplitJoin67_AnonFilter_a0_5089_5330_5417_5434_split[1], pop_complex(&SplitJoin86_SplitJoin65_SplitJoin65_AnonFilter_a0_5087_5329_5416_5433_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5252() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin86_SplitJoin65_SplitJoin65_AnonFilter_a0_5087_5329_5416_5433_join[0], pop_complex(&SplitJoin88_SplitJoin67_SplitJoin67_AnonFilter_a0_5089_5330_5417_5434_join[0]));
		push_complex(&SplitJoin86_SplitJoin65_SplitJoin65_AnonFilter_a0_5087_5329_5416_5433_join[0], pop_complex(&SplitJoin88_SplitJoin67_SplitJoin67_AnonFilter_a0_5089_5330_5417_5434_join[1]));
	ENDFOR
}}

void Identity_5097() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Identity(&(SplitJoin92_SplitJoin71_SplitJoin71_AnonFilter_a0_5095_5333_5418_5435_split[0]), &(SplitJoin92_SplitJoin71_SplitJoin71_AnonFilter_a0_5095_5333_5418_5435_join[0]));
	ENDFOR
}

void Identity_5099() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Identity(&(SplitJoin92_SplitJoin71_SplitJoin71_AnonFilter_a0_5095_5333_5418_5435_split[1]), &(SplitJoin92_SplitJoin71_SplitJoin71_AnonFilter_a0_5095_5333_5418_5435_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5253() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin92_SplitJoin71_SplitJoin71_AnonFilter_a0_5095_5333_5418_5435_split[0], pop_complex(&SplitJoin86_SplitJoin65_SplitJoin65_AnonFilter_a0_5087_5329_5416_5433_split[1]));
		push_complex(&SplitJoin92_SplitJoin71_SplitJoin71_AnonFilter_a0_5095_5333_5418_5435_split[1], pop_complex(&SplitJoin86_SplitJoin65_SplitJoin65_AnonFilter_a0_5087_5329_5416_5433_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5254() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin86_SplitJoin65_SplitJoin65_AnonFilter_a0_5087_5329_5416_5433_join[1], pop_complex(&SplitJoin92_SplitJoin71_SplitJoin71_AnonFilter_a0_5095_5333_5418_5435_join[0]));
		push_complex(&SplitJoin86_SplitJoin65_SplitJoin65_AnonFilter_a0_5087_5329_5416_5433_join[1], pop_complex(&SplitJoin92_SplitJoin71_SplitJoin71_AnonFilter_a0_5095_5333_5418_5435_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_5249() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		push_complex(&SplitJoin86_SplitJoin65_SplitJoin65_AnonFilter_a0_5087_5329_5416_5433_split[0], pop_complex(&SplitJoin84_SplitJoin63_SplitJoin63_AnonFilter_a0_5085_5328_5364_5432_split[0]));
		push_complex(&SplitJoin86_SplitJoin65_SplitJoin65_AnonFilter_a0_5087_5329_5416_5433_split[1], pop_complex(&SplitJoin84_SplitJoin63_SplitJoin63_AnonFilter_a0_5085_5328_5364_5432_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5250() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin84_SplitJoin63_SplitJoin63_AnonFilter_a0_5085_5328_5364_5432_join[0], pop_complex(&SplitJoin86_SplitJoin65_SplitJoin65_AnonFilter_a0_5087_5329_5416_5433_join[0]));
		push_complex(&SplitJoin84_SplitJoin63_SplitJoin63_AnonFilter_a0_5085_5328_5364_5432_join[0], pop_complex(&SplitJoin86_SplitJoin65_SplitJoin65_AnonFilter_a0_5087_5329_5416_5433_join[0]));
		push_complex(&SplitJoin84_SplitJoin63_SplitJoin63_AnonFilter_a0_5085_5328_5364_5432_join[0], pop_complex(&SplitJoin86_SplitJoin65_SplitJoin65_AnonFilter_a0_5087_5329_5416_5433_join[1]));
		push_complex(&SplitJoin84_SplitJoin63_SplitJoin63_AnonFilter_a0_5085_5328_5364_5432_join[0], pop_complex(&SplitJoin86_SplitJoin65_SplitJoin65_AnonFilter_a0_5087_5329_5416_5433_join[1]));
	ENDFOR
}}

void Identity_5105() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Identity(&(SplitJoin98_SplitJoin77_SplitJoin77_AnonFilter_a0_5103_5337_5420_5437_split[0]), &(SplitJoin98_SplitJoin77_SplitJoin77_AnonFilter_a0_5103_5337_5420_5437_join[0]));
	ENDFOR
}

void Identity_5107() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Identity(&(SplitJoin98_SplitJoin77_SplitJoin77_AnonFilter_a0_5103_5337_5420_5437_split[1]), &(SplitJoin98_SplitJoin77_SplitJoin77_AnonFilter_a0_5103_5337_5420_5437_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5257() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin98_SplitJoin77_SplitJoin77_AnonFilter_a0_5103_5337_5420_5437_split[0], pop_complex(&SplitJoin96_SplitJoin75_SplitJoin75_AnonFilter_a0_5101_5336_5419_5436_split[0]));
		push_complex(&SplitJoin98_SplitJoin77_SplitJoin77_AnonFilter_a0_5103_5337_5420_5437_split[1], pop_complex(&SplitJoin96_SplitJoin75_SplitJoin75_AnonFilter_a0_5101_5336_5419_5436_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5258() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin96_SplitJoin75_SplitJoin75_AnonFilter_a0_5101_5336_5419_5436_join[0], pop_complex(&SplitJoin98_SplitJoin77_SplitJoin77_AnonFilter_a0_5103_5337_5420_5437_join[0]));
		push_complex(&SplitJoin96_SplitJoin75_SplitJoin75_AnonFilter_a0_5101_5336_5419_5436_join[0], pop_complex(&SplitJoin98_SplitJoin77_SplitJoin77_AnonFilter_a0_5103_5337_5420_5437_join[1]));
	ENDFOR
}}

void Identity_5111() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Identity(&(SplitJoin102_SplitJoin81_SplitJoin81_AnonFilter_a0_5109_5340_5421_5438_split[0]), &(SplitJoin102_SplitJoin81_SplitJoin81_AnonFilter_a0_5109_5340_5421_5438_join[0]));
	ENDFOR
}

void Identity_5113() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Identity(&(SplitJoin102_SplitJoin81_SplitJoin81_AnonFilter_a0_5109_5340_5421_5438_split[1]), &(SplitJoin102_SplitJoin81_SplitJoin81_AnonFilter_a0_5109_5340_5421_5438_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5259() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin102_SplitJoin81_SplitJoin81_AnonFilter_a0_5109_5340_5421_5438_split[0], pop_complex(&SplitJoin96_SplitJoin75_SplitJoin75_AnonFilter_a0_5101_5336_5419_5436_split[1]));
		push_complex(&SplitJoin102_SplitJoin81_SplitJoin81_AnonFilter_a0_5109_5340_5421_5438_split[1], pop_complex(&SplitJoin96_SplitJoin75_SplitJoin75_AnonFilter_a0_5101_5336_5419_5436_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5260() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin96_SplitJoin75_SplitJoin75_AnonFilter_a0_5101_5336_5419_5436_join[1], pop_complex(&SplitJoin102_SplitJoin81_SplitJoin81_AnonFilter_a0_5109_5340_5421_5438_join[0]));
		push_complex(&SplitJoin96_SplitJoin75_SplitJoin75_AnonFilter_a0_5101_5336_5419_5436_join[1], pop_complex(&SplitJoin102_SplitJoin81_SplitJoin81_AnonFilter_a0_5109_5340_5421_5438_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_5255() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		push_complex(&SplitJoin96_SplitJoin75_SplitJoin75_AnonFilter_a0_5101_5336_5419_5436_split[0], pop_complex(&SplitJoin84_SplitJoin63_SplitJoin63_AnonFilter_a0_5085_5328_5364_5432_split[1]));
		push_complex(&SplitJoin96_SplitJoin75_SplitJoin75_AnonFilter_a0_5101_5336_5419_5436_split[1], pop_complex(&SplitJoin84_SplitJoin63_SplitJoin63_AnonFilter_a0_5085_5328_5364_5432_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5256() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		push_complex(&SplitJoin84_SplitJoin63_SplitJoin63_AnonFilter_a0_5085_5328_5364_5432_join[1], pop_complex(&SplitJoin96_SplitJoin75_SplitJoin75_AnonFilter_a0_5101_5336_5419_5436_join[0]));
		push_complex(&SplitJoin84_SplitJoin63_SplitJoin63_AnonFilter_a0_5085_5328_5364_5432_join[1], pop_complex(&SplitJoin96_SplitJoin75_SplitJoin75_AnonFilter_a0_5101_5336_5419_5436_join[0]));
		push_complex(&SplitJoin84_SplitJoin63_SplitJoin63_AnonFilter_a0_5085_5328_5364_5432_join[1], pop_complex(&SplitJoin96_SplitJoin75_SplitJoin75_AnonFilter_a0_5101_5336_5419_5436_join[1]));
		push_complex(&SplitJoin84_SplitJoin63_SplitJoin63_AnonFilter_a0_5085_5328_5364_5432_join[1], pop_complex(&SplitJoin96_SplitJoin75_SplitJoin75_AnonFilter_a0_5101_5336_5419_5436_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_5247() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 20, __iter_steady_++)
		push_complex(&SplitJoin84_SplitJoin63_SplitJoin63_AnonFilter_a0_5085_5328_5364_5432_split[0], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_5053_5279_5405_5424_split[1]));
		push_complex(&SplitJoin84_SplitJoin63_SplitJoin63_AnonFilter_a0_5085_5328_5364_5432_split[1], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_5053_5279_5405_5424_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5248() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_5053_5279_5405_5424_join[1], pop_complex(&SplitJoin84_SplitJoin63_SplitJoin63_AnonFilter_a0_5085_5328_5364_5432_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_5053_5279_5405_5424_join[1], pop_complex(&SplitJoin84_SplitJoin63_SplitJoin63_AnonFilter_a0_5085_5328_5364_5432_join[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_5231() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 40, __iter_steady_++)
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_5053_5279_5405_5424_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5389WEIGHTED_ROUND_ROBIN_Splitter_5231));
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_5053_5279_5405_5424_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5389WEIGHTED_ROUND_ROBIN_Splitter_5231));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5232() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5232WEIGHTED_ROUND_ROBIN_Splitter_5375, pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_5053_5279_5405_5424_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5232WEIGHTED_ROUND_ROBIN_Splitter_5375, pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_5053_5279_5405_5424_join[1]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_complex_t *chanin, buffer_complex_t *chanout) {
 {
 {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
			push_complex(&(*chanout), peek_complex(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
		}
		ENDFOR
	}
	}
	}
		pop_complex(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_5208() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child0_5366_5440_split[0]), &(Pre_CollapsedDataParallel_1_5208butterfly_5115));
	ENDFOR
}

void butterfly(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&(*chanin)));
		complex_t two = ((complex_t) pop_complex(&(*chanin)));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&(*chanout), __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&(*chanout), __sa2) ; 
	}


void butterfly_5115() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_5208butterfly_5115), &(butterfly_5115Post_CollapsedDataParallel_2_5209));
	ENDFOR
}

void Post_CollapsedDataParallel_2(buffer_complex_t *chanin, buffer_complex_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 2, _k++) {
 {
			push_complex(&(*chanout), peek_complex(&(*chanin), (_k + 0))) ; 
		}
		}
		ENDFOR
	}
	}
		pop_complex(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_5209() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_5115Post_CollapsedDataParallel_2_5209), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child0_5366_5440_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_5211() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child0_5366_5440_split[1]), &(Pre_CollapsedDataParallel_1_5211butterfly_5116));
	ENDFOR
}

void butterfly_5116() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_5211butterfly_5116), &(butterfly_5116Post_CollapsedDataParallel_2_5212));
	ENDFOR
}

void Post_CollapsedDataParallel_2_5212() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_5116Post_CollapsedDataParallel_2_5212), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child0_5366_5440_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_5214() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child0_5366_5440_split[2]), &(Pre_CollapsedDataParallel_1_5214butterfly_5117));
	ENDFOR
}

void butterfly_5117() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_5214butterfly_5117), &(butterfly_5117Post_CollapsedDataParallel_2_5215));
	ENDFOR
}

void Post_CollapsedDataParallel_2_5215() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_5117Post_CollapsedDataParallel_2_5215), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child0_5366_5440_join[2]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_5217() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child0_5366_5440_split[3]), &(Pre_CollapsedDataParallel_1_5217butterfly_5118));
	ENDFOR
}

void butterfly_5118() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_5217butterfly_5118), &(butterfly_5118Post_CollapsedDataParallel_2_5218));
	ENDFOR
}

void Post_CollapsedDataParallel_2_5218() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_5118Post_CollapsedDataParallel_2_5218), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child0_5366_5440_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5376() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child0_5366_5440_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_Hier_5408_5439_split[0]));
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child0_5366_5440_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_Hier_5408_5439_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5377() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_Hier_5408_5439_join[0], pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child0_5366_5440_join[__iter_]));
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_Hier_5408_5439_join[0], pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child0_5366_5440_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_5220() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin63_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child1_5369_5441_split[0]), &(Pre_CollapsedDataParallel_1_5220butterfly_5119));
	ENDFOR
}

void butterfly_5119() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_5220butterfly_5119), &(butterfly_5119Post_CollapsedDataParallel_2_5221));
	ENDFOR
}

void Post_CollapsedDataParallel_2_5221() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_5119Post_CollapsedDataParallel_2_5221), &(SplitJoin63_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child1_5369_5441_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_5223() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin63_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child1_5369_5441_split[1]), &(Pre_CollapsedDataParallel_1_5223butterfly_5120));
	ENDFOR
}

void butterfly_5120() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_5223butterfly_5120), &(butterfly_5120Post_CollapsedDataParallel_2_5224));
	ENDFOR
}

void Post_CollapsedDataParallel_2_5224() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_5120Post_CollapsedDataParallel_2_5224), &(SplitJoin63_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child1_5369_5441_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_5226() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin63_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child1_5369_5441_split[2]), &(Pre_CollapsedDataParallel_1_5226butterfly_5121));
	ENDFOR
}

void butterfly_5121() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_5226butterfly_5121), &(butterfly_5121Post_CollapsedDataParallel_2_5227));
	ENDFOR
}

void Post_CollapsedDataParallel_2_5227() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_5121Post_CollapsedDataParallel_2_5227), &(SplitJoin63_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child1_5369_5441_join[2]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_5229() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin63_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child1_5369_5441_split[3]), &(Pre_CollapsedDataParallel_1_5229butterfly_5122));
	ENDFOR
}

void butterfly_5122() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_5229butterfly_5122), &(butterfly_5122Post_CollapsedDataParallel_2_5230));
	ENDFOR
}

void Post_CollapsedDataParallel_2_5230() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_5122Post_CollapsedDataParallel_2_5230), &(SplitJoin63_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child1_5369_5441_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5378() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin63_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child1_5369_5441_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_Hier_5408_5439_split[1]));
			push_complex(&SplitJoin63_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child1_5369_5441_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_Hier_5408_5439_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5379() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_Hier_5408_5439_join[1], pop_complex(&SplitJoin63_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child1_5369_5441_join[__iter_]));
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_Hier_5408_5439_join[1], pop_complex(&SplitJoin63_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child1_5369_5441_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_5375() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_Hier_5408_5439_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5232WEIGHTED_ROUND_ROBIN_Splitter_5375));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_Hier_5408_5439_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5232WEIGHTED_ROUND_ROBIN_Splitter_5375));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5380() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5380WEIGHTED_ROUND_ROBIN_Splitter_5381, pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_Hier_5408_5439_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5380WEIGHTED_ROUND_ROBIN_Splitter_5381, pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_Hier_5408_5439_join[1]));
		ENDFOR
	ENDFOR
}}

void butterfly_5124() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		butterfly(&(SplitJoin18_SplitJoin12_SplitJoin12_split2_5034_5287_5365_5444_split[0]), &(SplitJoin18_SplitJoin12_SplitJoin12_split2_5034_5287_5365_5444_join[0]));
	ENDFOR
}

void butterfly_5125() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		butterfly(&(SplitJoin18_SplitJoin12_SplitJoin12_split2_5034_5287_5365_5444_split[1]), &(SplitJoin18_SplitJoin12_SplitJoin12_split2_5034_5287_5365_5444_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5265() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_5034_5287_5365_5444_split[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child0_5372_5443_split[0]));
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_5034_5287_5365_5444_split[1], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child0_5372_5443_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5266() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child0_5372_5443_join[0], pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_5034_5287_5365_5444_join[0]));
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child0_5372_5443_join[0], pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_5034_5287_5365_5444_join[1]));
	ENDFOR
}}

void butterfly_5126() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		butterfly(&(SplitJoin46_SplitJoin29_SplitJoin29_split2_5036_5301_5367_5445_split[0]), &(SplitJoin46_SplitJoin29_SplitJoin29_split2_5036_5301_5367_5445_join[0]));
	ENDFOR
}

void butterfly_5127() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		butterfly(&(SplitJoin46_SplitJoin29_SplitJoin29_split2_5036_5301_5367_5445_split[1]), &(SplitJoin46_SplitJoin29_SplitJoin29_split2_5036_5301_5367_5445_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5267() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		push_complex(&SplitJoin46_SplitJoin29_SplitJoin29_split2_5036_5301_5367_5445_split[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child0_5372_5443_split[1]));
		push_complex(&SplitJoin46_SplitJoin29_SplitJoin29_split2_5036_5301_5367_5445_split[1], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child0_5372_5443_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5268() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child0_5372_5443_join[1], pop_complex(&SplitJoin46_SplitJoin29_SplitJoin29_split2_5036_5301_5367_5445_join[0]));
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child0_5372_5443_join[1], pop_complex(&SplitJoin46_SplitJoin29_SplitJoin29_split2_5036_5301_5367_5445_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_5382() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child0_5372_5443_split[0], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_Hier_5409_5442_split[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child0_5372_5443_split[1], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_Hier_5409_5442_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5383() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_Hier_5409_5442_join[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child0_5372_5443_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_Hier_5409_5442_join[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child0_5372_5443_join[1]));
		ENDFOR
	ENDFOR
}}

void butterfly_5128() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		butterfly(&(SplitJoin52_SplitJoin33_SplitJoin33_split2_5038_5304_5368_5447_split[0]), &(SplitJoin52_SplitJoin33_SplitJoin33_split2_5038_5304_5368_5447_join[0]));
	ENDFOR
}

void butterfly_5129() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		butterfly(&(SplitJoin52_SplitJoin33_SplitJoin33_split2_5038_5304_5368_5447_split[1]), &(SplitJoin52_SplitJoin33_SplitJoin33_split2_5038_5304_5368_5447_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5269() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		push_complex(&SplitJoin52_SplitJoin33_SplitJoin33_split2_5038_5304_5368_5447_split[0], pop_complex(&SplitJoin50_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child1_5374_5446_split[0]));
		push_complex(&SplitJoin52_SplitJoin33_SplitJoin33_split2_5038_5304_5368_5447_split[1], pop_complex(&SplitJoin50_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child1_5374_5446_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5270() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		push_complex(&SplitJoin50_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child1_5374_5446_join[0], pop_complex(&SplitJoin52_SplitJoin33_SplitJoin33_split2_5038_5304_5368_5447_join[0]));
		push_complex(&SplitJoin50_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child1_5374_5446_join[0], pop_complex(&SplitJoin52_SplitJoin33_SplitJoin33_split2_5038_5304_5368_5447_join[1]));
	ENDFOR
}}

void butterfly_5130() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		butterfly(&(SplitJoin56_SplitJoin37_SplitJoin37_split2_5040_5307_5370_5448_split[0]), &(SplitJoin56_SplitJoin37_SplitJoin37_split2_5040_5307_5370_5448_join[0]));
	ENDFOR
}

void butterfly_5131() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		butterfly(&(SplitJoin56_SplitJoin37_SplitJoin37_split2_5040_5307_5370_5448_split[1]), &(SplitJoin56_SplitJoin37_SplitJoin37_split2_5040_5307_5370_5448_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5271() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		push_complex(&SplitJoin56_SplitJoin37_SplitJoin37_split2_5040_5307_5370_5448_split[0], pop_complex(&SplitJoin50_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child1_5374_5446_split[1]));
		push_complex(&SplitJoin56_SplitJoin37_SplitJoin37_split2_5040_5307_5370_5448_split[1], pop_complex(&SplitJoin50_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child1_5374_5446_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5272() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		push_complex(&SplitJoin50_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child1_5374_5446_join[1], pop_complex(&SplitJoin56_SplitJoin37_SplitJoin37_split2_5040_5307_5370_5448_join[0]));
		push_complex(&SplitJoin50_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child1_5374_5446_join[1], pop_complex(&SplitJoin56_SplitJoin37_SplitJoin37_split2_5040_5307_5370_5448_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_5384() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin50_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child1_5374_5446_split[0], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_Hier_5409_5442_split[1]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin50_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child1_5374_5446_split[1], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_Hier_5409_5442_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5385() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_Hier_5409_5442_join[1], pop_complex(&SplitJoin50_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child1_5374_5446_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_Hier_5409_5442_join[1], pop_complex(&SplitJoin50_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child1_5374_5446_join[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_5381() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_Hier_5409_5442_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5380WEIGHTED_ROUND_ROBIN_Splitter_5381));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_Hier_5409_5442_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5380WEIGHTED_ROUND_ROBIN_Splitter_5381));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5386() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5386WEIGHTED_ROUND_ROBIN_Splitter_5273, pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_Hier_5409_5442_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5386WEIGHTED_ROUND_ROBIN_Splitter_5273, pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_Hier_5409_5442_join[1]));
		ENDFOR
	ENDFOR
}}

void butterfly_5133() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_5047_5290_5371_5450_split[0]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_5047_5290_5371_5450_join[0]));
	ENDFOR
}

void butterfly_5134() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_5047_5290_5371_5450_split[1]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_5047_5290_5371_5450_join[1]));
	ENDFOR
}

void butterfly_5135() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_5047_5290_5371_5450_split[2]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_5047_5290_5371_5450_join[2]));
	ENDFOR
}

void butterfly_5136() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_5047_5290_5371_5450_split[3]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_5047_5290_5371_5450_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5275() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_5047_5290_5371_5450_split[__iter_], pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_5045_5289_5410_5449_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5276() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_5045_5289_5410_5449_join[0], pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_5047_5290_5371_5450_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void butterfly_5137() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		butterfly(&(SplitJoin39_SplitJoin22_SplitJoin22_split2_5049_5295_5373_5451_split[0]), &(SplitJoin39_SplitJoin22_SplitJoin22_split2_5049_5295_5373_5451_join[0]));
	ENDFOR
}

void butterfly_5138() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		butterfly(&(SplitJoin39_SplitJoin22_SplitJoin22_split2_5049_5295_5373_5451_split[1]), &(SplitJoin39_SplitJoin22_SplitJoin22_split2_5049_5295_5373_5451_join[1]));
	ENDFOR
}

void butterfly_5139() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		butterfly(&(SplitJoin39_SplitJoin22_SplitJoin22_split2_5049_5295_5373_5451_split[2]), &(SplitJoin39_SplitJoin22_SplitJoin22_split2_5049_5295_5373_5451_join[2]));
	ENDFOR
}

void butterfly_5140() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		butterfly(&(SplitJoin39_SplitJoin22_SplitJoin22_split2_5049_5295_5373_5451_split[3]), &(SplitJoin39_SplitJoin22_SplitJoin22_split2_5049_5295_5373_5451_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5277() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin39_SplitJoin22_SplitJoin22_split2_5049_5295_5373_5451_split[__iter_], pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_5045_5289_5410_5449_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5278() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 10, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_5045_5289_5410_5449_join[1], pop_complex(&SplitJoin39_SplitJoin22_SplitJoin22_split2_5049_5295_5373_5451_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_5273() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_5045_5289_5410_5449_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5386WEIGHTED_ROUND_ROBIN_Splitter_5273));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_5045_5289_5410_5449_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5386WEIGHTED_ROUND_ROBIN_Splitter_5273));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5274() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5274WEIGHTED_ROUND_ROBIN_Splitter_5392, pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_5045_5289_5410_5449_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5274WEIGHTED_ROUND_ROBIN_Splitter_5392, pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_5045_5289_5410_5449_join[1]));
		ENDFOR
	ENDFOR
}}

void magnitude(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		push_float(&(*chanout), ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}


void magnitude_5394() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_5411_5452_split[0]), &(SplitJoin24_magnitude_Fiss_5411_5452_join[0]));
	ENDFOR
}

void magnitude_5395() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_5411_5452_split[1]), &(SplitJoin24_magnitude_Fiss_5411_5452_join[1]));
	ENDFOR
}

void magnitude_5396() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_5411_5452_split[2]), &(SplitJoin24_magnitude_Fiss_5411_5452_join[2]));
	ENDFOR
}

void magnitude_5397() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_5411_5452_split[3]), &(SplitJoin24_magnitude_Fiss_5411_5452_join[3]));
	ENDFOR
}

void magnitude_5398() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_5411_5452_split[4]), &(SplitJoin24_magnitude_Fiss_5411_5452_join[4]));
	ENDFOR
}

void magnitude_5399() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_5411_5452_split[5]), &(SplitJoin24_magnitude_Fiss_5411_5452_join[5]));
	ENDFOR
}

void magnitude_5400() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_5411_5452_split[6]), &(SplitJoin24_magnitude_Fiss_5411_5452_join[6]));
	ENDFOR
}

void magnitude_5401() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_5411_5452_split[7]), &(SplitJoin24_magnitude_Fiss_5411_5452_join[7]));
	ENDFOR
}

void magnitude_5402() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_5411_5452_split[8]), &(SplitJoin24_magnitude_Fiss_5411_5452_join[8]));
	ENDFOR
}

void magnitude_5403() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_5411_5452_split[9]), &(SplitJoin24_magnitude_Fiss_5411_5452_join[9]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5392() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 10, __iter_++)
			push_complex(&SplitJoin24_magnitude_Fiss_5411_5452_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5274WEIGHTED_ROUND_ROBIN_Splitter_5392));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5393() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 10, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5393sink_5142, pop_float(&SplitJoin24_magnitude_Fiss_5411_5452_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void sink(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void sink_5142() {
	FOR(uint32_t, __iter_steady_, 0, <, 80, __iter_steady_++)
		sink(&(WEIGHTED_ROUND_ROBIN_Joiner_5393sink_5142));
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5389WEIGHTED_ROUND_ROBIN_Splitter_5231);
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_5057_5281_5406_5426_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 2, __iter_init_1_++)
		init_buffer_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_5045_5289_5410_5449_split[__iter_init_1_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_5217butterfly_5118);
	init_buffer_complex(&butterfly_5117Post_CollapsedDataParallel_2_5215);
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_complex(&SplitJoin76_SplitJoin55_SplitJoin55_AnonFilter_a0_5073_5322_5414_5430_join[__iter_init_2_]);
	ENDFOR
	init_buffer_complex(&butterfly_5116Post_CollapsedDataParallel_2_5212);
	FOR(int, __iter_init_3_, 0, <, 4, __iter_init_3_++)
		init_buffer_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_5047_5290_5371_5450_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 4, __iter_init_4_++)
		init_buffer_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child0_5366_5440_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 2, __iter_init_5_++)
		init_buffer_complex(&SplitJoin52_SplitJoin33_SplitJoin33_split2_5038_5304_5368_5447_join[__iter_init_5_]);
	ENDFOR
	init_buffer_complex(&butterfly_5118Post_CollapsedDataParallel_2_5218);
	FOR(int, __iter_init_6_, 0, <, 2, __iter_init_6_++)
		init_buffer_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child0_5372_5443_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 2, __iter_init_7_++)
		init_buffer_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child0_5372_5443_join[__iter_init_7_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_5226butterfly_5121);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5274WEIGHTED_ROUND_ROBIN_Splitter_5392);
	init_buffer_complex(&Pre_CollapsedDataParallel_1_5220butterfly_5119);
	init_buffer_complex(&butterfly_5120Post_CollapsedDataParallel_2_5224);
	init_buffer_complex(&butterfly_5121Post_CollapsedDataParallel_2_5227);
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_5059_5282_5407_5427_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 4, __iter_init_9_++)
		init_buffer_complex(&SplitJoin39_SplitJoin22_SplitJoin22_split2_5049_5295_5373_5451_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_complex(&SplitJoin102_SplitJoin81_SplitJoin81_AnonFilter_a0_5109_5340_5421_5438_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 4, __iter_init_11_++)
		init_buffer_complex(&SplitJoin39_SplitJoin22_SplitJoin22_split2_5049_5295_5373_5451_split[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 10, __iter_init_12_++)
		init_buffer_complex(&SplitJoin24_magnitude_Fiss_5411_5452_split[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_complex(&SplitJoin80_SplitJoin59_SplitJoin59_AnonFilter_a0_5079_5325_5415_5431_join[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_5045_5289_5410_5449_join[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 2, __iter_init_15_++)
		init_buffer_complex(&SplitJoin92_SplitJoin71_SplitJoin71_AnonFilter_a0_5095_5333_5418_5435_join[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_Hier_5408_5439_split[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_complex(&SplitJoin74_SplitJoin53_SplitJoin53_AnonFilter_a0_5071_5321_5413_5429_split[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 2, __iter_init_18_++)
		init_buffer_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_5053_5279_5405_5424_join[__iter_init_18_]);
	ENDFOR
	FOR(int, __iter_init_19_, 0, <, 2, __iter_init_19_++)
		init_buffer_complex(&SplitJoin86_SplitJoin65_SplitJoin65_AnonFilter_a0_5087_5329_5416_5433_split[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 2, __iter_init_20_++)
		init_buffer_complex(&SplitJoin80_SplitJoin59_SplitJoin59_AnonFilter_a0_5079_5325_5415_5431_split[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 2, __iter_init_21_++)
		init_buffer_complex(&SplitJoin92_SplitJoin71_SplitJoin71_AnonFilter_a0_5095_5333_5418_5435_split[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 2, __iter_init_22_++)
		init_buffer_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_5034_5287_5365_5444_split[__iter_init_22_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_5208butterfly_5115);
	FOR(int, __iter_init_23_, 0, <, 2, __iter_init_23_++)
		init_buffer_complex(&SplitJoin50_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child1_5374_5446_join[__iter_init_23_]);
	ENDFOR
	init_buffer_complex(&butterfly_5115Post_CollapsedDataParallel_2_5209);
	init_buffer_complex(&Pre_CollapsedDataParallel_1_5214butterfly_5117);
	init_buffer_complex(&Pre_CollapsedDataParallel_1_5223butterfly_5120);
	FOR(int, __iter_init_24_, 0, <, 2, __iter_init_24_++)
		init_buffer_complex(&SplitJoin88_SplitJoin67_SplitJoin67_AnonFilter_a0_5089_5330_5417_5434_split[__iter_init_24_]);
	ENDFOR
	FOR(int, __iter_init_25_, 0, <, 2, __iter_init_25_++)
		init_buffer_complex(&SplitJoin88_SplitJoin67_SplitJoin67_AnonFilter_a0_5089_5330_5417_5434_join[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 2, __iter_init_26_++)
		init_buffer_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_Hier_5409_5442_join[__iter_init_26_]);
	ENDFOR
	FOR(int, __iter_init_27_, 0, <, 4, __iter_init_27_++)
		init_buffer_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child0_5366_5440_join[__iter_init_27_]);
	ENDFOR
	FOR(int, __iter_init_28_, 0, <, 2, __iter_init_28_++)
		init_buffer_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_5034_5287_5365_5444_join[__iter_init_28_]);
	ENDFOR
	FOR(int, __iter_init_29_, 0, <, 2, __iter_init_29_++)
		init_buffer_complex(&SplitJoin46_SplitJoin29_SplitJoin29_split2_5036_5301_5367_5445_split[__iter_init_29_]);
	ENDFOR
	FOR(int, __iter_init_30_, 0, <, 2, __iter_init_30_++)
		init_buffer_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_5055_5280_5363_5425_join[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 2, __iter_init_31_++)
		init_buffer_complex(&SplitJoin84_SplitJoin63_SplitJoin63_AnonFilter_a0_5085_5328_5364_5432_split[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 2, __iter_init_32_++)
		init_buffer_complex(&SplitJoin76_SplitJoin55_SplitJoin55_AnonFilter_a0_5073_5322_5414_5430_split[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 2, __iter_init_33_++)
		init_buffer_complex(&SplitJoin56_SplitJoin37_SplitJoin37_split2_5040_5307_5370_5448_join[__iter_init_33_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5393sink_5142);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5386WEIGHTED_ROUND_ROBIN_Splitter_5273);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5380WEIGHTED_ROUND_ROBIN_Splitter_5381);
	FOR(int, __iter_init_34_, 0, <, 2, __iter_init_34_++)
		init_buffer_complex(&SplitJoin84_SplitJoin63_SplitJoin63_AnonFilter_a0_5085_5328_5364_5432_join[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 2, __iter_init_35_++)
		init_buffer_complex(&SplitJoin86_SplitJoin65_SplitJoin65_AnonFilter_a0_5087_5329_5416_5433_join[__iter_init_35_]);
	ENDFOR
	FOR(int, __iter_init_36_, 0, <, 4, __iter_init_36_++)
		init_buffer_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_5047_5290_5371_5450_join[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 2, __iter_init_37_++)
		init_buffer_complex(&SplitJoin0_source_Fiss_5404_5423_split[__iter_init_37_]);
	ENDFOR
	FOR(int, __iter_init_38_, 0, <, 2, __iter_init_38_++)
		init_buffer_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_Hier_5408_5439_join[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 2, __iter_init_39_++)
		init_buffer_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_Hier_5409_5442_split[__iter_init_39_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_5229butterfly_5122);
	FOR(int, __iter_init_40_, 0, <, 2, __iter_init_40_++)
		init_buffer_complex(&SplitJoin98_SplitJoin77_SplitJoin77_AnonFilter_a0_5103_5337_5420_5437_split[__iter_init_40_]);
	ENDFOR
	FOR(int, __iter_init_41_, 0, <, 2, __iter_init_41_++)
		init_buffer_complex(&SplitJoin70_SplitJoin49_SplitJoin49_AnonFilter_a0_5065_5318_5412_5428_split[__iter_init_41_]);
	ENDFOR
	FOR(int, __iter_init_42_, 0, <, 2, __iter_init_42_++)
		init_buffer_complex(&SplitJoin96_SplitJoin75_SplitJoin75_AnonFilter_a0_5101_5336_5419_5436_split[__iter_init_42_]);
	ENDFOR
	FOR(int, __iter_init_43_, 0, <, 10, __iter_init_43_++)
		init_buffer_float(&SplitJoin24_magnitude_Fiss_5411_5452_join[__iter_init_43_]);
	ENDFOR
	FOR(int, __iter_init_44_, 0, <, 2, __iter_init_44_++)
		init_buffer_complex(&SplitJoin98_SplitJoin77_SplitJoin77_AnonFilter_a0_5103_5337_5420_5437_join[__iter_init_44_]);
	ENDFOR
	FOR(int, __iter_init_45_, 0, <, 2, __iter_init_45_++)
		init_buffer_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_5059_5282_5407_5427_split[__iter_init_45_]);
	ENDFOR
	FOR(int, __iter_init_46_, 0, <, 4, __iter_init_46_++)
		init_buffer_complex(&SplitJoin63_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child1_5369_5441_split[__iter_init_46_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_5211butterfly_5116);
	FOR(int, __iter_init_47_, 0, <, 2, __iter_init_47_++)
		init_buffer_complex(&SplitJoin70_SplitJoin49_SplitJoin49_AnonFilter_a0_5065_5318_5412_5428_join[__iter_init_47_]);
	ENDFOR
	FOR(int, __iter_init_48_, 0, <, 2, __iter_init_48_++)
		init_buffer_complex(&SplitJoin46_SplitJoin29_SplitJoin29_split2_5036_5301_5367_5445_join[__iter_init_48_]);
	ENDFOR
	FOR(int, __iter_init_49_, 0, <, 2, __iter_init_49_++)
		init_buffer_complex(&SplitJoin50_SplitJoin10_SplitJoin10_split1_5032_5286_Hier_child1_5374_5446_split[__iter_init_49_]);
	ENDFOR
	FOR(int, __iter_init_50_, 0, <, 2, __iter_init_50_++)
		init_buffer_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_5053_5279_5405_5424_split[__iter_init_50_]);
	ENDFOR
	FOR(int, __iter_init_51_, 0, <, 2, __iter_init_51_++)
		init_buffer_complex(&SplitJoin102_SplitJoin81_SplitJoin81_AnonFilter_a0_5109_5340_5421_5438_join[__iter_init_51_]);
	ENDFOR
	FOR(int, __iter_init_52_, 0, <, 2, __iter_init_52_++)
		init_buffer_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_5055_5280_5363_5425_split[__iter_init_52_]);
	ENDFOR
	FOR(int, __iter_init_53_, 0, <, 2, __iter_init_53_++)
		init_buffer_complex(&SplitJoin96_SplitJoin75_SplitJoin75_AnonFilter_a0_5101_5336_5419_5436_join[__iter_init_53_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5232WEIGHTED_ROUND_ROBIN_Splitter_5375);
	FOR(int, __iter_init_54_, 0, <, 2, __iter_init_54_++)
		init_buffer_complex(&SplitJoin52_SplitJoin33_SplitJoin33_split2_5038_5304_5368_5447_split[__iter_init_54_]);
	ENDFOR
	FOR(int, __iter_init_55_, 0, <, 4, __iter_init_55_++)
		init_buffer_complex(&SplitJoin63_SplitJoin8_SplitJoin8_split1_5011_5284_Hier_child1_5369_5441_join[__iter_init_55_]);
	ENDFOR
	init_buffer_complex(&butterfly_5122Post_CollapsedDataParallel_2_5230);
	FOR(int, __iter_init_56_, 0, <, 2, __iter_init_56_++)
		init_buffer_complex(&SplitJoin56_SplitJoin37_SplitJoin37_split2_5040_5307_5370_5448_split[__iter_init_56_]);
	ENDFOR
	FOR(int, __iter_init_57_, 0, <, 2, __iter_init_57_++)
		init_buffer_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_5057_5281_5406_5426_split[__iter_init_57_]);
	ENDFOR
	FOR(int, __iter_init_58_, 0, <, 2, __iter_init_58_++)
		init_buffer_complex(&SplitJoin0_source_Fiss_5404_5423_join[__iter_init_58_]);
	ENDFOR
	FOR(int, __iter_init_59_, 0, <, 2, __iter_init_59_++)
		init_buffer_complex(&SplitJoin74_SplitJoin53_SplitJoin53_AnonFilter_a0_5071_5321_5413_5429_join[__iter_init_59_]);
	ENDFOR
	init_buffer_complex(&butterfly_5119Post_CollapsedDataParallel_2_5221);
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_5388();
			source_5390();
			source_5391();
		WEIGHTED_ROUND_ROBIN_Joiner_5389();
		WEIGHTED_ROUND_ROBIN_Splitter_5231();
			WEIGHTED_ROUND_ROBIN_Splitter_5233();
				WEIGHTED_ROUND_ROBIN_Splitter_5235();
					WEIGHTED_ROUND_ROBIN_Splitter_5237();
						Identity_5061();
						Identity_5063();
					WEIGHTED_ROUND_ROBIN_Joiner_5238();
					WEIGHTED_ROUND_ROBIN_Splitter_5239();
						Identity_5067();
						Identity_5069();
					WEIGHTED_ROUND_ROBIN_Joiner_5240();
				WEIGHTED_ROUND_ROBIN_Joiner_5236();
				WEIGHTED_ROUND_ROBIN_Splitter_5241();
					WEIGHTED_ROUND_ROBIN_Splitter_5243();
						Identity_5075();
						Identity_5077();
					WEIGHTED_ROUND_ROBIN_Joiner_5244();
					WEIGHTED_ROUND_ROBIN_Splitter_5245();
						Identity_5081();
						Identity_5083();
					WEIGHTED_ROUND_ROBIN_Joiner_5246();
				WEIGHTED_ROUND_ROBIN_Joiner_5242();
			WEIGHTED_ROUND_ROBIN_Joiner_5234();
			WEIGHTED_ROUND_ROBIN_Splitter_5247();
				WEIGHTED_ROUND_ROBIN_Splitter_5249();
					WEIGHTED_ROUND_ROBIN_Splitter_5251();
						Identity_5091();
						Identity_5093();
					WEIGHTED_ROUND_ROBIN_Joiner_5252();
					WEIGHTED_ROUND_ROBIN_Splitter_5253();
						Identity_5097();
						Identity_5099();
					WEIGHTED_ROUND_ROBIN_Joiner_5254();
				WEIGHTED_ROUND_ROBIN_Joiner_5250();
				WEIGHTED_ROUND_ROBIN_Splitter_5255();
					WEIGHTED_ROUND_ROBIN_Splitter_5257();
						Identity_5105();
						Identity_5107();
					WEIGHTED_ROUND_ROBIN_Joiner_5258();
					WEIGHTED_ROUND_ROBIN_Splitter_5259();
						Identity_5111();
						Identity_5113();
					WEIGHTED_ROUND_ROBIN_Joiner_5260();
				WEIGHTED_ROUND_ROBIN_Joiner_5256();
			WEIGHTED_ROUND_ROBIN_Joiner_5248();
		WEIGHTED_ROUND_ROBIN_Joiner_5232();
		WEIGHTED_ROUND_ROBIN_Splitter_5375();
			WEIGHTED_ROUND_ROBIN_Splitter_5376();
				Pre_CollapsedDataParallel_1_5208();
				butterfly_5115();
				Post_CollapsedDataParallel_2_5209();
				Pre_CollapsedDataParallel_1_5211();
				butterfly_5116();
				Post_CollapsedDataParallel_2_5212();
				Pre_CollapsedDataParallel_1_5214();
				butterfly_5117();
				Post_CollapsedDataParallel_2_5215();
				Pre_CollapsedDataParallel_1_5217();
				butterfly_5118();
				Post_CollapsedDataParallel_2_5218();
			WEIGHTED_ROUND_ROBIN_Joiner_5377();
			WEIGHTED_ROUND_ROBIN_Splitter_5378();
				Pre_CollapsedDataParallel_1_5220();
				butterfly_5119();
				Post_CollapsedDataParallel_2_5221();
				Pre_CollapsedDataParallel_1_5223();
				butterfly_5120();
				Post_CollapsedDataParallel_2_5224();
				Pre_CollapsedDataParallel_1_5226();
				butterfly_5121();
				Post_CollapsedDataParallel_2_5227();
				Pre_CollapsedDataParallel_1_5229();
				butterfly_5122();
				Post_CollapsedDataParallel_2_5230();
			WEIGHTED_ROUND_ROBIN_Joiner_5379();
		WEIGHTED_ROUND_ROBIN_Joiner_5380();
		WEIGHTED_ROUND_ROBIN_Splitter_5381();
			WEIGHTED_ROUND_ROBIN_Splitter_5382();
				WEIGHTED_ROUND_ROBIN_Splitter_5265();
					butterfly_5124();
					butterfly_5125();
				WEIGHTED_ROUND_ROBIN_Joiner_5266();
				WEIGHTED_ROUND_ROBIN_Splitter_5267();
					butterfly_5126();
					butterfly_5127();
				WEIGHTED_ROUND_ROBIN_Joiner_5268();
			WEIGHTED_ROUND_ROBIN_Joiner_5383();
			WEIGHTED_ROUND_ROBIN_Splitter_5384();
				WEIGHTED_ROUND_ROBIN_Splitter_5269();
					butterfly_5128();
					butterfly_5129();
				WEIGHTED_ROUND_ROBIN_Joiner_5270();
				WEIGHTED_ROUND_ROBIN_Splitter_5271();
					butterfly_5130();
					butterfly_5131();
				WEIGHTED_ROUND_ROBIN_Joiner_5272();
			WEIGHTED_ROUND_ROBIN_Joiner_5385();
		WEIGHTED_ROUND_ROBIN_Joiner_5386();
		WEIGHTED_ROUND_ROBIN_Splitter_5273();
			WEIGHTED_ROUND_ROBIN_Splitter_5275();
				butterfly_5133();
				butterfly_5134();
				butterfly_5135();
				butterfly_5136();
			WEIGHTED_ROUND_ROBIN_Joiner_5276();
			WEIGHTED_ROUND_ROBIN_Splitter_5277();
				butterfly_5137();
				butterfly_5138();
				butterfly_5139();
				butterfly_5140();
			WEIGHTED_ROUND_ROBIN_Joiner_5278();
		WEIGHTED_ROUND_ROBIN_Joiner_5274();
		WEIGHTED_ROUND_ROBIN_Splitter_5392();
			magnitude_5394();
			magnitude_5395();
			magnitude_5396();
			magnitude_5397();
			magnitude_5398();
			magnitude_5399();
			magnitude_5400();
			magnitude_5401();
			magnitude_5402();
			magnitude_5403();
		WEIGHTED_ROUND_ROBIN_Joiner_5393();
		sink_5142();
	ENDFOR
	return EXIT_SUCCESS;
}
