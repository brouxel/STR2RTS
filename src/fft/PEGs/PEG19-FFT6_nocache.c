#include "PEG19-FFT6_nocache.h"

buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3076WEIGHTED_ROUND_ROBIN_Splitter_3093;
buffer_complex_t FFTTestSource_3041FFTReorderSimple_3042;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3149CombineDFT_3052;
buffer_complex_t SplitJoin12_CombineDFT_Fiss_3158_3168_join[8];
buffer_complex_t FFTReorderSimple_3042WEIGHTED_ROUND_ROBIN_Splitter_3055;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3133WEIGHTED_ROUND_ROBIN_Splitter_3142;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3115WEIGHTED_ROUND_ROBIN_Splitter_3132;
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_3153_3163_join[4];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_3159_3169_split[4];
buffer_complex_t SplitJoin12_CombineDFT_Fiss_3158_3168_split[8];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_3160_3170_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3060WEIGHTED_ROUND_ROBIN_Splitter_3065;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3143WEIGHTED_ROUND_ROBIN_Splitter_3148;
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[16];
buffer_complex_t SplitJoin8_CombineDFT_Fiss_3156_3166_join[19];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_3157_3167_join[16];
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_3153_3163_split[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3066WEIGHTED_ROUND_ROBIN_Splitter_3075;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_3156_3166_split[19];
buffer_complex_t CombineDFT_3052CPrinter_3053;
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_3152_3162_split[2];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_3152_3162_join[2];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_3154_3164_join[8];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_3160_3170_join[2];
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[16];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_3157_3167_split[16];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_3159_3169_join[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3094WEIGHTED_ROUND_ROBIN_Splitter_3114;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3056WEIGHTED_ROUND_ROBIN_Splitter_3059;
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_3154_3164_split[8];


CombineDFT_3095_t CombineDFT_3095_s;
CombineDFT_3095_t CombineDFT_3096_s;
CombineDFT_3095_t CombineDFT_3097_s;
CombineDFT_3095_t CombineDFT_3098_s;
CombineDFT_3095_t CombineDFT_3099_s;
CombineDFT_3095_t CombineDFT_3100_s;
CombineDFT_3095_t CombineDFT_3101_s;
CombineDFT_3095_t CombineDFT_3102_s;
CombineDFT_3095_t CombineDFT_3103_s;
CombineDFT_3095_t CombineDFT_3104_s;
CombineDFT_3095_t CombineDFT_3105_s;
CombineDFT_3095_t CombineDFT_3106_s;
CombineDFT_3095_t CombineDFT_3107_s;
CombineDFT_3095_t CombineDFT_3108_s;
CombineDFT_3095_t CombineDFT_3109_s;
CombineDFT_3095_t CombineDFT_3110_s;
CombineDFT_3095_t CombineDFT_3111_s;
CombineDFT_3095_t CombineDFT_3112_s;
CombineDFT_3095_t CombineDFT_3113_s;
CombineDFT_3095_t CombineDFT_3116_s;
CombineDFT_3095_t CombineDFT_3117_s;
CombineDFT_3095_t CombineDFT_3118_s;
CombineDFT_3095_t CombineDFT_3119_s;
CombineDFT_3095_t CombineDFT_3120_s;
CombineDFT_3095_t CombineDFT_3121_s;
CombineDFT_3095_t CombineDFT_3122_s;
CombineDFT_3095_t CombineDFT_3123_s;
CombineDFT_3095_t CombineDFT_3124_s;
CombineDFT_3095_t CombineDFT_3125_s;
CombineDFT_3095_t CombineDFT_3126_s;
CombineDFT_3095_t CombineDFT_3127_s;
CombineDFT_3095_t CombineDFT_3128_s;
CombineDFT_3095_t CombineDFT_3129_s;
CombineDFT_3095_t CombineDFT_3130_s;
CombineDFT_3095_t CombineDFT_3131_s;
CombineDFT_3095_t CombineDFT_3134_s;
CombineDFT_3095_t CombineDFT_3135_s;
CombineDFT_3095_t CombineDFT_3136_s;
CombineDFT_3095_t CombineDFT_3137_s;
CombineDFT_3095_t CombineDFT_3138_s;
CombineDFT_3095_t CombineDFT_3139_s;
CombineDFT_3095_t CombineDFT_3140_s;
CombineDFT_3095_t CombineDFT_3141_s;
CombineDFT_3095_t CombineDFT_3144_s;
CombineDFT_3095_t CombineDFT_3145_s;
CombineDFT_3095_t CombineDFT_3146_s;
CombineDFT_3095_t CombineDFT_3147_s;
CombineDFT_3095_t CombineDFT_3150_s;
CombineDFT_3095_t CombineDFT_3151_s;
CombineDFT_3095_t CombineDFT_3052_s;

void FFTTestSource_3041(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t c1;
		complex_t zero;
		c1.real = 1.0 ; 
		c1.imag = 0.0 ; 
		zero.real = 0.0 ; 
		zero.imag = 0.0 ; 
		push_complex(&FFTTestSource_3041FFTReorderSimple_3042, zero) ; 
		push_complex(&FFTTestSource_3041FFTReorderSimple_3042, c1) ; 
		FOR(int, i, 0,  < , 62, i++) {
			push_complex(&FFTTestSource_3041FFTReorderSimple_3042, zero) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3042(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&FFTTestSource_3041FFTReorderSimple_3042, i)) ; 
			push_complex(&FFTReorderSimple_3042WEIGHTED_ROUND_ROBIN_Splitter_3055, __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 64, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&FFTTestSource_3041FFTReorderSimple_3042, i)) ; 
			push_complex(&FFTReorderSimple_3042WEIGHTED_ROUND_ROBIN_Splitter_3055, __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&FFTTestSource_3041FFTReorderSimple_3042) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3057(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_3152_3162_split[0], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_3152_3162_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 32, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_3152_3162_split[0], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_3152_3162_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_3152_3162_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3058(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_3152_3162_split[1], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_3152_3162_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 32, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_3152_3162_split[1], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_3152_3162_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_3152_3162_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3055() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_3152_3162_split[0], pop_complex(&FFTReorderSimple_3042WEIGHTED_ROUND_ROBIN_Splitter_3055));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_3152_3162_split[1], pop_complex(&FFTReorderSimple_3042WEIGHTED_ROUND_ROBIN_Splitter_3055));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3056() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3056WEIGHTED_ROUND_ROBIN_Splitter_3059, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_3152_3162_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3056WEIGHTED_ROUND_ROBIN_Splitter_3059, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_3152_3162_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_3061(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_3153_3163_split[0], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_3153_3163_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_3153_3163_split[0], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_3153_3163_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_3153_3163_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3062(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_3153_3163_split[1], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_3153_3163_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_3153_3163_split[1], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_3153_3163_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_3153_3163_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3063(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_3153_3163_split[2], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_3153_3163_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_3153_3163_split[2], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_3153_3163_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_3153_3163_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3064(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_3153_3163_split[3], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_3153_3163_join[3], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_3153_3163_split[3], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_3153_3163_join[3], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_3153_3163_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3059() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin2_FFTReorderSimple_Fiss_3153_3163_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3056WEIGHTED_ROUND_ROBIN_Splitter_3059));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3060() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3060WEIGHTED_ROUND_ROBIN_Splitter_3065, pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_3153_3163_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_3067(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_split[0], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_split[0], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3068(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_split[1], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_split[1], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3069(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_split[2], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_split[2], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3070(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_split[3], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_join[3], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_split[3], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_join[3], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3071(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_split[4], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_join[4], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_split[4], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_join[4], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3072(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_split[5], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_join[5], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_split[5], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_join[5], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3073(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_split[6], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_join[6], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_split[6], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_join[6], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3074(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_split[7], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_join[7], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_split[7], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_join[7], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3065() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3060WEIGHTED_ROUND_ROBIN_Splitter_3065));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3066() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3066WEIGHTED_ROUND_ROBIN_Splitter_3075, pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_3077(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[0], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[0], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3078(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[1], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[1], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3079(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[2], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[2], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3080(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[3], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[3], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[3], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[3], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3081(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[4], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[4], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[4], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[4], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3082(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[5], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[5], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[5], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[5], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3083(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[6], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[6], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[6], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[6], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3084(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[7], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[7], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[7], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[7], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3085(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[8], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[8], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[8], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[8], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[8]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3086(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[9], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[9], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[9], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[9], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[9]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3087(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[10], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[10], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[10], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[10], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[10]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3088(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[11], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[11], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[11], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[11], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[11]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3089(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[12], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[12], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[12], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[12], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[12]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3090(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[13], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[13], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[13], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[13], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[13]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3091(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[14], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[14], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[14], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[14], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[14]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_3092(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[15], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[15], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[15], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[15], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[15]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3075() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3066WEIGHTED_ROUND_ROBIN_Splitter_3075));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3076() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3076WEIGHTED_ROUND_ROBIN_Splitter_3093, pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_3095(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[0], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3095_s.wn.real) - (w.imag * CombineDFT_3095_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3095_s.wn.imag) + (w.imag * CombineDFT_3095_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[0]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3096(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[1], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3096_s.wn.real) - (w.imag * CombineDFT_3096_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3096_s.wn.imag) + (w.imag * CombineDFT_3096_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[1]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3097(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[2], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3097_s.wn.real) - (w.imag * CombineDFT_3097_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3097_s.wn.imag) + (w.imag * CombineDFT_3097_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[2]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3098(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[3], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3098_s.wn.real) - (w.imag * CombineDFT_3098_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3098_s.wn.imag) + (w.imag * CombineDFT_3098_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[3]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3099(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[4], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[4], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3099_s.wn.real) - (w.imag * CombineDFT_3099_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3099_s.wn.imag) + (w.imag * CombineDFT_3099_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[4]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3100(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[5], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[5], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3100_s.wn.real) - (w.imag * CombineDFT_3100_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3100_s.wn.imag) + (w.imag * CombineDFT_3100_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[5]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3101(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[6], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[6], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3101_s.wn.real) - (w.imag * CombineDFT_3101_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3101_s.wn.imag) + (w.imag * CombineDFT_3101_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[6]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3102(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[7], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[7], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3102_s.wn.real) - (w.imag * CombineDFT_3102_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3102_s.wn.imag) + (w.imag * CombineDFT_3102_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[7]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3103(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[8], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[8], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3103_s.wn.real) - (w.imag * CombineDFT_3103_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3103_s.wn.imag) + (w.imag * CombineDFT_3103_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[8]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_join[8], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3104(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[9], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[9], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3104_s.wn.real) - (w.imag * CombineDFT_3104_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3104_s.wn.imag) + (w.imag * CombineDFT_3104_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[9]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_join[9], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3105(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[10], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[10], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3105_s.wn.real) - (w.imag * CombineDFT_3105_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3105_s.wn.imag) + (w.imag * CombineDFT_3105_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[10]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_join[10], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3106(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[11], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[11], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3106_s.wn.real) - (w.imag * CombineDFT_3106_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3106_s.wn.imag) + (w.imag * CombineDFT_3106_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[11]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_join[11], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3107(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[12], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[12], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3107_s.wn.real) - (w.imag * CombineDFT_3107_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3107_s.wn.imag) + (w.imag * CombineDFT_3107_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[12]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_join[12], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3108(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[13], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[13], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3108_s.wn.real) - (w.imag * CombineDFT_3108_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3108_s.wn.imag) + (w.imag * CombineDFT_3108_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[13]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_join[13], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3109(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[14], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[14], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3109_s.wn.real) - (w.imag * CombineDFT_3109_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3109_s.wn.imag) + (w.imag * CombineDFT_3109_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[14]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_join[14], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3110(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[15], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[15], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3110_s.wn.real) - (w.imag * CombineDFT_3110_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3110_s.wn.imag) + (w.imag * CombineDFT_3110_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[15]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_join[15], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3111(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[16], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[16], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3111_s.wn.real) - (w.imag * CombineDFT_3111_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3111_s.wn.imag) + (w.imag * CombineDFT_3111_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[16]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_join[16], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3112(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[17], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[17], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3112_s.wn.real) - (w.imag * CombineDFT_3112_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3112_s.wn.imag) + (w.imag * CombineDFT_3112_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[17]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_join[17], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3113(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[18], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[18], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3113_s.wn.real) - (w.imag * CombineDFT_3113_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3113_s.wn.imag) + (w.imag * CombineDFT_3113_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[18]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_join[18], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3093() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 19, __iter_++)
			push_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3076WEIGHTED_ROUND_ROBIN_Splitter_3093));
			push_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3076WEIGHTED_ROUND_ROBIN_Splitter_3093));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3094() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 19, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3094WEIGHTED_ROUND_ROBIN_Splitter_3114, pop_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_join[__iter_]));
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3094WEIGHTED_ROUND_ROBIN_Splitter_3114, pop_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_3116(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[0], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3116_s.wn.real) - (w.imag * CombineDFT_3116_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3116_s.wn.imag) + (w.imag * CombineDFT_3116_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[0]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3117(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[1], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3117_s.wn.real) - (w.imag * CombineDFT_3117_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3117_s.wn.imag) + (w.imag * CombineDFT_3117_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[1]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3118(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[2], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3118_s.wn.real) - (w.imag * CombineDFT_3118_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3118_s.wn.imag) + (w.imag * CombineDFT_3118_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[2]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3119(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[3], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3119_s.wn.real) - (w.imag * CombineDFT_3119_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3119_s.wn.imag) + (w.imag * CombineDFT_3119_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[3]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3120(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[4], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[4], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3120_s.wn.real) - (w.imag * CombineDFT_3120_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3120_s.wn.imag) + (w.imag * CombineDFT_3120_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[4]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3121(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[5], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[5], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3121_s.wn.real) - (w.imag * CombineDFT_3121_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3121_s.wn.imag) + (w.imag * CombineDFT_3121_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[5]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3122(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[6], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[6], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3122_s.wn.real) - (w.imag * CombineDFT_3122_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3122_s.wn.imag) + (w.imag * CombineDFT_3122_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[6]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3123(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[7], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[7], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3123_s.wn.real) - (w.imag * CombineDFT_3123_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3123_s.wn.imag) + (w.imag * CombineDFT_3123_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[7]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3124(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[8], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[8], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3124_s.wn.real) - (w.imag * CombineDFT_3124_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3124_s.wn.imag) + (w.imag * CombineDFT_3124_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[8]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_join[8], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3125(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[9], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[9], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3125_s.wn.real) - (w.imag * CombineDFT_3125_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3125_s.wn.imag) + (w.imag * CombineDFT_3125_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[9]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_join[9], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3126(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[10], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[10], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3126_s.wn.real) - (w.imag * CombineDFT_3126_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3126_s.wn.imag) + (w.imag * CombineDFT_3126_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[10]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_join[10], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3127(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[11], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[11], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3127_s.wn.real) - (w.imag * CombineDFT_3127_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3127_s.wn.imag) + (w.imag * CombineDFT_3127_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[11]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_join[11], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3128(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[12], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[12], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3128_s.wn.real) - (w.imag * CombineDFT_3128_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3128_s.wn.imag) + (w.imag * CombineDFT_3128_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[12]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_join[12], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3129(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[13], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[13], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3129_s.wn.real) - (w.imag * CombineDFT_3129_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3129_s.wn.imag) + (w.imag * CombineDFT_3129_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[13]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_join[13], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3130(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[14], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[14], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3130_s.wn.real) - (w.imag * CombineDFT_3130_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3130_s.wn.imag) + (w.imag * CombineDFT_3130_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[14]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_join[14], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3131(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[15], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[15], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3131_s.wn.real) - (w.imag * CombineDFT_3131_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3131_s.wn.imag) + (w.imag * CombineDFT_3131_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[15]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_join[15], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3114() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3094WEIGHTED_ROUND_ROBIN_Splitter_3114));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3115() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3115WEIGHTED_ROUND_ROBIN_Splitter_3132, pop_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_3134(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_split[0], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3134_s.wn.real) - (w.imag * CombineDFT_3134_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3134_s.wn.imag) + (w.imag * CombineDFT_3134_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_split[0]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3135(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_split[1], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3135_s.wn.real) - (w.imag * CombineDFT_3135_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3135_s.wn.imag) + (w.imag * CombineDFT_3135_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_split[1]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3136(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_split[2], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3136_s.wn.real) - (w.imag * CombineDFT_3136_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3136_s.wn.imag) + (w.imag * CombineDFT_3136_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_split[2]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3137(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_split[3], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3137_s.wn.real) - (w.imag * CombineDFT_3137_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3137_s.wn.imag) + (w.imag * CombineDFT_3137_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_split[3]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3138(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_split[4], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_split[4], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3138_s.wn.real) - (w.imag * CombineDFT_3138_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3138_s.wn.imag) + (w.imag * CombineDFT_3138_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_split[4]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3139(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_split[5], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_split[5], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3139_s.wn.real) - (w.imag * CombineDFT_3139_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3139_s.wn.imag) + (w.imag * CombineDFT_3139_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_split[5]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3140(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_split[6], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_split[6], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3140_s.wn.real) - (w.imag * CombineDFT_3140_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3140_s.wn.imag) + (w.imag * CombineDFT_3140_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_split[6]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3141(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_split[7], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_split[7], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3141_s.wn.real) - (w.imag * CombineDFT_3141_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3141_s.wn.imag) + (w.imag * CombineDFT_3141_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_split[7]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3132() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3115WEIGHTED_ROUND_ROBIN_Splitter_3132));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3133() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3133WEIGHTED_ROUND_ROBIN_Splitter_3142, pop_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_3144(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_3159_3169_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_3159_3169_split[0], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3144_s.wn.real) - (w.imag * CombineDFT_3144_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3144_s.wn.imag) + (w.imag * CombineDFT_3144_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_3159_3169_split[0]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_3159_3169_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3145(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_3159_3169_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_3159_3169_split[1], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3145_s.wn.real) - (w.imag * CombineDFT_3145_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3145_s.wn.imag) + (w.imag * CombineDFT_3145_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_3159_3169_split[1]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_3159_3169_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3146(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_3159_3169_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_3159_3169_split[2], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3146_s.wn.real) - (w.imag * CombineDFT_3146_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3146_s.wn.imag) + (w.imag * CombineDFT_3146_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_3159_3169_split[2]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_3159_3169_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3147(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_3159_3169_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_3159_3169_split[3], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3147_s.wn.real) - (w.imag * CombineDFT_3147_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3147_s.wn.imag) + (w.imag * CombineDFT_3147_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_3159_3169_split[3]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_3159_3169_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3142() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin14_CombineDFT_Fiss_3159_3169_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3133WEIGHTED_ROUND_ROBIN_Splitter_3142));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3143() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3143WEIGHTED_ROUND_ROBIN_Splitter_3148, pop_complex(&SplitJoin14_CombineDFT_Fiss_3159_3169_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_3150(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[32];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 16, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_3160_3170_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_3160_3170_split[0], (16 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(16 + i)].real = (y0.real - y1w.real) ; 
			results[(16 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3150_s.wn.real) - (w.imag * CombineDFT_3150_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3150_s.wn.imag) + (w.imag * CombineDFT_3150_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin16_CombineDFT_Fiss_3160_3170_split[0]) ; 
			push_complex(&SplitJoin16_CombineDFT_Fiss_3160_3170_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_3151(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[32];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 16, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_3160_3170_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_3160_3170_split[1], (16 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(16 + i)].real = (y0.real - y1w.real) ; 
			results[(16 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3151_s.wn.real) - (w.imag * CombineDFT_3151_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3151_s.wn.imag) + (w.imag * CombineDFT_3151_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin16_CombineDFT_Fiss_3160_3170_split[1]) ; 
			push_complex(&SplitJoin16_CombineDFT_Fiss_3160_3170_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3148() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_3160_3170_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3143WEIGHTED_ROUND_ROBIN_Splitter_3148));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_3160_3170_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3143WEIGHTED_ROUND_ROBIN_Splitter_3148));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3149() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3149CombineDFT_3052, pop_complex(&SplitJoin16_CombineDFT_Fiss_3160_3170_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3149CombineDFT_3052, pop_complex(&SplitJoin16_CombineDFT_Fiss_3160_3170_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_3052(){
	FOR(uint32_t, __iter_steady_, 0, <, 19, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[64];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3149CombineDFT_3052, i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3149CombineDFT_3052, (32 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(32 + i)].real = (y0.real - y1w.real) ; 
			results[(32 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_3052_s.wn.real) - (w.imag * CombineDFT_3052_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_3052_s.wn.imag) + (w.imag * CombineDFT_3052_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3149CombineDFT_3052) ; 
			push_complex(&CombineDFT_3052CPrinter_3053, results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CPrinter_3053(){
	FOR(uint32_t, __iter_steady_, 0, <, 1216, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&CombineDFT_3052CPrinter_3053));
		printf("%.10f", c.real);
		printf("\n");
		printf("%.10f", c.imag);
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3076WEIGHTED_ROUND_ROBIN_Splitter_3093);
	init_buffer_complex(&FFTTestSource_3041FFTReorderSimple_3042);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3149CombineDFT_3052);
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_join[__iter_init_0_]);
	ENDFOR
	init_buffer_complex(&FFTReorderSimple_3042WEIGHTED_ROUND_ROBIN_Splitter_3055);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3133WEIGHTED_ROUND_ROBIN_Splitter_3142);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3115WEIGHTED_ROUND_ROBIN_Splitter_3132);
	FOR(int, __iter_init_1_, 0, <, 4, __iter_init_1_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_3153_3163_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 4, __iter_init_2_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_3159_3169_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_3158_3168_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 2, __iter_init_4_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_3160_3170_split[__iter_init_4_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3060WEIGHTED_ROUND_ROBIN_Splitter_3065);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3143WEIGHTED_ROUND_ROBIN_Splitter_3148);
	FOR(int, __iter_init_5_, 0, <, 16, __iter_init_5_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 19, __iter_init_6_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 16, __iter_init_7_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 4, __iter_init_8_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_3153_3163_split[__iter_init_8_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3066WEIGHTED_ROUND_ROBIN_Splitter_3075);
	FOR(int, __iter_init_9_, 0, <, 19, __iter_init_9_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_3156_3166_split[__iter_init_9_]);
	ENDFOR
	init_buffer_complex(&CombineDFT_3052CPrinter_3053);
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_3152_3162_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 2, __iter_init_11_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_3152_3162_join[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 8, __iter_init_12_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_join[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_3160_3170_join[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 16, __iter_init_14_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_3155_3165_split[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 16, __iter_init_15_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_3157_3167_split[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 4, __iter_init_16_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_3159_3169_join[__iter_init_16_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3094WEIGHTED_ROUND_ROBIN_Splitter_3114);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3056WEIGHTED_ROUND_ROBIN_Splitter_3059);
	FOR(int, __iter_init_17_, 0, <, 8, __iter_init_17_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_3154_3164_split[__iter_init_17_]);
	ENDFOR
// --- init: CombineDFT_3095
	 {
	 ; 
	CombineDFT_3095_s.wn.real = -1.0 ; 
	CombineDFT_3095_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3096
	 {
	 ; 
	CombineDFT_3096_s.wn.real = -1.0 ; 
	CombineDFT_3096_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3097
	 {
	 ; 
	CombineDFT_3097_s.wn.real = -1.0 ; 
	CombineDFT_3097_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3098
	 {
	 ; 
	CombineDFT_3098_s.wn.real = -1.0 ; 
	CombineDFT_3098_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3099
	 {
	 ; 
	CombineDFT_3099_s.wn.real = -1.0 ; 
	CombineDFT_3099_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3100
	 {
	 ; 
	CombineDFT_3100_s.wn.real = -1.0 ; 
	CombineDFT_3100_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3101
	 {
	 ; 
	CombineDFT_3101_s.wn.real = -1.0 ; 
	CombineDFT_3101_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3102
	 {
	 ; 
	CombineDFT_3102_s.wn.real = -1.0 ; 
	CombineDFT_3102_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3103
	 {
	 ; 
	CombineDFT_3103_s.wn.real = -1.0 ; 
	CombineDFT_3103_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3104
	 {
	 ; 
	CombineDFT_3104_s.wn.real = -1.0 ; 
	CombineDFT_3104_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3105
	 {
	 ; 
	CombineDFT_3105_s.wn.real = -1.0 ; 
	CombineDFT_3105_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3106
	 {
	 ; 
	CombineDFT_3106_s.wn.real = -1.0 ; 
	CombineDFT_3106_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3107
	 {
	 ; 
	CombineDFT_3107_s.wn.real = -1.0 ; 
	CombineDFT_3107_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3108
	 {
	 ; 
	CombineDFT_3108_s.wn.real = -1.0 ; 
	CombineDFT_3108_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3109
	 {
	 ; 
	CombineDFT_3109_s.wn.real = -1.0 ; 
	CombineDFT_3109_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3110
	 {
	 ; 
	CombineDFT_3110_s.wn.real = -1.0 ; 
	CombineDFT_3110_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3111
	 {
	 ; 
	CombineDFT_3111_s.wn.real = -1.0 ; 
	CombineDFT_3111_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3112
	 {
	 ; 
	CombineDFT_3112_s.wn.real = -1.0 ; 
	CombineDFT_3112_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3113
	 {
	 ; 
	CombineDFT_3113_s.wn.real = -1.0 ; 
	CombineDFT_3113_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_3116
	 {
	 ; 
	CombineDFT_3116_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3116_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3117
	 {
	 ; 
	CombineDFT_3117_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3117_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3118
	 {
	 ; 
	CombineDFT_3118_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3118_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3119
	 {
	 ; 
	CombineDFT_3119_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3119_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3120
	 {
	 ; 
	CombineDFT_3120_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3120_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3121
	 {
	 ; 
	CombineDFT_3121_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3121_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3122
	 {
	 ; 
	CombineDFT_3122_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3122_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3123
	 {
	 ; 
	CombineDFT_3123_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3123_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3124
	 {
	 ; 
	CombineDFT_3124_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3124_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3125
	 {
	 ; 
	CombineDFT_3125_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3125_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3126
	 {
	 ; 
	CombineDFT_3126_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3126_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3127
	 {
	 ; 
	CombineDFT_3127_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3127_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3128
	 {
	 ; 
	CombineDFT_3128_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3128_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3129
	 {
	 ; 
	CombineDFT_3129_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3129_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3130
	 {
	 ; 
	CombineDFT_3130_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3130_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3131
	 {
	 ; 
	CombineDFT_3131_s.wn.real = -4.371139E-8 ; 
	CombineDFT_3131_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_3134
	 {
	 ; 
	CombineDFT_3134_s.wn.real = 0.70710677 ; 
	CombineDFT_3134_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3135
	 {
	 ; 
	CombineDFT_3135_s.wn.real = 0.70710677 ; 
	CombineDFT_3135_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3136
	 {
	 ; 
	CombineDFT_3136_s.wn.real = 0.70710677 ; 
	CombineDFT_3136_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3137
	 {
	 ; 
	CombineDFT_3137_s.wn.real = 0.70710677 ; 
	CombineDFT_3137_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3138
	 {
	 ; 
	CombineDFT_3138_s.wn.real = 0.70710677 ; 
	CombineDFT_3138_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3139
	 {
	 ; 
	CombineDFT_3139_s.wn.real = 0.70710677 ; 
	CombineDFT_3139_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3140
	 {
	 ; 
	CombineDFT_3140_s.wn.real = 0.70710677 ; 
	CombineDFT_3140_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3141
	 {
	 ; 
	CombineDFT_3141_s.wn.real = 0.70710677 ; 
	CombineDFT_3141_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_3144
	 {
	 ; 
	CombineDFT_3144_s.wn.real = 0.9238795 ; 
	CombineDFT_3144_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_3145
	 {
	 ; 
	CombineDFT_3145_s.wn.real = 0.9238795 ; 
	CombineDFT_3145_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_3146
	 {
	 ; 
	CombineDFT_3146_s.wn.real = 0.9238795 ; 
	CombineDFT_3146_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_3147
	 {
	 ; 
	CombineDFT_3147_s.wn.real = 0.9238795 ; 
	CombineDFT_3147_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_3150
	 {
	 ; 
	CombineDFT_3150_s.wn.real = 0.98078525 ; 
	CombineDFT_3150_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_3151
	 {
	 ; 
	CombineDFT_3151_s.wn.real = 0.98078525 ; 
	CombineDFT_3151_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_3052
	 {
	 ; 
	CombineDFT_3052_s.wn.real = 0.9951847 ; 
	CombineDFT_3052_s.wn.imag = -0.09801714 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FFTTestSource_3041();
		FFTReorderSimple_3042();
		WEIGHTED_ROUND_ROBIN_Splitter_3055();
			FFTReorderSimple_3057();
			FFTReorderSimple_3058();
		WEIGHTED_ROUND_ROBIN_Joiner_3056();
		WEIGHTED_ROUND_ROBIN_Splitter_3059();
			FFTReorderSimple_3061();
			FFTReorderSimple_3062();
			FFTReorderSimple_3063();
			FFTReorderSimple_3064();
		WEIGHTED_ROUND_ROBIN_Joiner_3060();
		WEIGHTED_ROUND_ROBIN_Splitter_3065();
			FFTReorderSimple_3067();
			FFTReorderSimple_3068();
			FFTReorderSimple_3069();
			FFTReorderSimple_3070();
			FFTReorderSimple_3071();
			FFTReorderSimple_3072();
			FFTReorderSimple_3073();
			FFTReorderSimple_3074();
		WEIGHTED_ROUND_ROBIN_Joiner_3066();
		WEIGHTED_ROUND_ROBIN_Splitter_3075();
			FFTReorderSimple_3077();
			FFTReorderSimple_3078();
			FFTReorderSimple_3079();
			FFTReorderSimple_3080();
			FFTReorderSimple_3081();
			FFTReorderSimple_3082();
			FFTReorderSimple_3083();
			FFTReorderSimple_3084();
			FFTReorderSimple_3085();
			FFTReorderSimple_3086();
			FFTReorderSimple_3087();
			FFTReorderSimple_3088();
			FFTReorderSimple_3089();
			FFTReorderSimple_3090();
			FFTReorderSimple_3091();
			FFTReorderSimple_3092();
		WEIGHTED_ROUND_ROBIN_Joiner_3076();
		WEIGHTED_ROUND_ROBIN_Splitter_3093();
			CombineDFT_3095();
			CombineDFT_3096();
			CombineDFT_3097();
			CombineDFT_3098();
			CombineDFT_3099();
			CombineDFT_3100();
			CombineDFT_3101();
			CombineDFT_3102();
			CombineDFT_3103();
			CombineDFT_3104();
			CombineDFT_3105();
			CombineDFT_3106();
			CombineDFT_3107();
			CombineDFT_3108();
			CombineDFT_3109();
			CombineDFT_3110();
			CombineDFT_3111();
			CombineDFT_3112();
			CombineDFT_3113();
		WEIGHTED_ROUND_ROBIN_Joiner_3094();
		WEIGHTED_ROUND_ROBIN_Splitter_3114();
			CombineDFT_3116();
			CombineDFT_3117();
			CombineDFT_3118();
			CombineDFT_3119();
			CombineDFT_3120();
			CombineDFT_3121();
			CombineDFT_3122();
			CombineDFT_3123();
			CombineDFT_3124();
			CombineDFT_3125();
			CombineDFT_3126();
			CombineDFT_3127();
			CombineDFT_3128();
			CombineDFT_3129();
			CombineDFT_3130();
			CombineDFT_3131();
		WEIGHTED_ROUND_ROBIN_Joiner_3115();
		WEIGHTED_ROUND_ROBIN_Splitter_3132();
			CombineDFT_3134();
			CombineDFT_3135();
			CombineDFT_3136();
			CombineDFT_3137();
			CombineDFT_3138();
			CombineDFT_3139();
			CombineDFT_3140();
			CombineDFT_3141();
		WEIGHTED_ROUND_ROBIN_Joiner_3133();
		WEIGHTED_ROUND_ROBIN_Splitter_3142();
			CombineDFT_3144();
			CombineDFT_3145();
			CombineDFT_3146();
			CombineDFT_3147();
		WEIGHTED_ROUND_ROBIN_Joiner_3143();
		WEIGHTED_ROUND_ROBIN_Splitter_3148();
			CombineDFT_3150();
			CombineDFT_3151();
		WEIGHTED_ROUND_ROBIN_Joiner_3149();
		CombineDFT_3052();
		CPrinter_3053();
	ENDFOR
	return EXIT_SUCCESS;
}
