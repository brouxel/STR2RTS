#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=10752 on the compile command line
#else
#if BUF_SIZEMAX < 10752
#error BUF_SIZEMAX too small, it must be at least 10752
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float w[2];
} CombineDFT_5516_t;

typedef struct {
	float w[4];
} CombineDFT_5539_t;

typedef struct {
	float w[8];
} CombineDFT_5557_t;

typedef struct {
	float w[16];
} CombineDFT_5567_t;

typedef struct {
	float w[32];
} CombineDFT_5573_t;

typedef struct {
	float w[64];
} CombineDFT_5451_t;
void WEIGHTED_ROUND_ROBIN_Splitter_5472();
void FFTTestSource_5474();
void FFTTestSource_5475();
void WEIGHTED_ROUND_ROBIN_Joiner_5473();
void WEIGHTED_ROUND_ROBIN_Splitter_5464();
void FFTReorderSimple_5441();
void WEIGHTED_ROUND_ROBIN_Splitter_5476();
void FFTReorderSimple_5478();
void FFTReorderSimple_5479();
void WEIGHTED_ROUND_ROBIN_Joiner_5477();
void WEIGHTED_ROUND_ROBIN_Splitter_5480();
void FFTReorderSimple_5482();
void FFTReorderSimple_5483();
void FFTReorderSimple_5484();
void FFTReorderSimple_5485();
void WEIGHTED_ROUND_ROBIN_Joiner_5481();
void WEIGHTED_ROUND_ROBIN_Splitter_5486();
void FFTReorderSimple_5488();
void FFTReorderSimple_5489();
void FFTReorderSimple_5490();
void FFTReorderSimple_5491();
void FFTReorderSimple_5492();
void FFTReorderSimple_5493();
void FFTReorderSimple_5494();
void FFTReorderSimple_5495();
void WEIGHTED_ROUND_ROBIN_Joiner_5487();
void WEIGHTED_ROUND_ROBIN_Splitter_5496();
void FFTReorderSimple_5498();
void FFTReorderSimple_5499();
void FFTReorderSimple_5500();
void FFTReorderSimple_5501();
void FFTReorderSimple_5502();
void FFTReorderSimple_5503();
void FFTReorderSimple_5504();
void FFTReorderSimple_5505();
void FFTReorderSimple_5506();
void FFTReorderSimple_5507();
void FFTReorderSimple_5508();
void FFTReorderSimple_5509();
void FFTReorderSimple_5510();
void FFTReorderSimple_5511();
void FFTReorderSimple_5512();
void FFTReorderSimple_5513();
void WEIGHTED_ROUND_ROBIN_Joiner_5497();
void WEIGHTED_ROUND_ROBIN_Splitter_5514();
void CombineDFT_5516();
void CombineDFT_5517();
void CombineDFT_5518();
void CombineDFT_5519();
void CombineDFT_5520();
void CombineDFT_5521();
void CombineDFT_5522();
void CombineDFT_5523();
void CombineDFT_5524();
void CombineDFT_5525();
void CombineDFT_5526();
void CombineDFT_5527();
void CombineDFT_5528();
void CombineDFT_5529();
void CombineDFT_5530();
void CombineDFT_5531();
void CombineDFT_5532();
void CombineDFT_5533();
void CombineDFT_5534();
void CombineDFT_5535();
void CombineDFT_5536();
void WEIGHTED_ROUND_ROBIN_Joiner_5515();
void WEIGHTED_ROUND_ROBIN_Splitter_5537();
void CombineDFT_5539();
void CombineDFT_5540();
void CombineDFT_5541();
void CombineDFT_5542();
void CombineDFT_5543();
void CombineDFT_5544();
void CombineDFT_5545();
void CombineDFT_5546();
void CombineDFT_5547();
void CombineDFT_5548();
void CombineDFT_5549();
void CombineDFT_5550();
void CombineDFT_5551();
void CombineDFT_5552();
void CombineDFT_5553();
void CombineDFT_5554();
void WEIGHTED_ROUND_ROBIN_Joiner_5538();
void WEIGHTED_ROUND_ROBIN_Splitter_5555();
void CombineDFT_5557();
void CombineDFT_5558();
void CombineDFT_5559();
void CombineDFT_5560();
void CombineDFT_5561();
void CombineDFT_5562();
void CombineDFT_5563();
void CombineDFT_5564();
void WEIGHTED_ROUND_ROBIN_Joiner_5556();
void WEIGHTED_ROUND_ROBIN_Splitter_5565();
void CombineDFT_5567();
void CombineDFT_5568();
void CombineDFT_5569();
void CombineDFT_5570();
void WEIGHTED_ROUND_ROBIN_Joiner_5566();
void WEIGHTED_ROUND_ROBIN_Splitter_5571();
void CombineDFT_5573();
void CombineDFT_5574();
void WEIGHTED_ROUND_ROBIN_Joiner_5572();
void CombineDFT_5451();
void FFTReorderSimple_5452();
void WEIGHTED_ROUND_ROBIN_Splitter_5575();
void FFTReorderSimple_5577();
void FFTReorderSimple_5578();
void WEIGHTED_ROUND_ROBIN_Joiner_5576();
void WEIGHTED_ROUND_ROBIN_Splitter_5579();
void FFTReorderSimple_5581();
void FFTReorderSimple_5582();
void FFTReorderSimple_5583();
void FFTReorderSimple_5584();
void WEIGHTED_ROUND_ROBIN_Joiner_5580();
void WEIGHTED_ROUND_ROBIN_Splitter_5585();
void FFTReorderSimple_5587();
void FFTReorderSimple_5588();
void FFTReorderSimple_5589();
void FFTReorderSimple_5590();
void FFTReorderSimple_5591();
void FFTReorderSimple_5592();
void FFTReorderSimple_5593();
void FFTReorderSimple_5594();
void WEIGHTED_ROUND_ROBIN_Joiner_5586();
void WEIGHTED_ROUND_ROBIN_Splitter_5595();
void FFTReorderSimple_5597();
void FFTReorderSimple_5598();
void FFTReorderSimple_5599();
void FFTReorderSimple_5600();
void FFTReorderSimple_5601();
void FFTReorderSimple_5602();
void FFTReorderSimple_5603();
void FFTReorderSimple_5604();
void FFTReorderSimple_5605();
void FFTReorderSimple_5606();
void FFTReorderSimple_5607();
void FFTReorderSimple_5608();
void FFTReorderSimple_5609();
void FFTReorderSimple_5610();
void FFTReorderSimple_5611();
void FFTReorderSimple_5612();
void WEIGHTED_ROUND_ROBIN_Joiner_5596();
void WEIGHTED_ROUND_ROBIN_Splitter_5613();
void CombineDFT_5615();
void CombineDFT_5616();
void CombineDFT_5617();
void CombineDFT_5618();
void CombineDFT_5619();
void CombineDFT_5620();
void CombineDFT_5621();
void CombineDFT_5622();
void CombineDFT_5623();
void CombineDFT_5624();
void CombineDFT_5625();
void CombineDFT_5626();
void CombineDFT_5627();
void CombineDFT_5628();
void CombineDFT_5629();
void CombineDFT_5630();
void CombineDFT_5631();
void CombineDFT_5632();
void CombineDFT_5633();
void CombineDFT_5634();
void CombineDFT_5635();
void WEIGHTED_ROUND_ROBIN_Joiner_5614();
void WEIGHTED_ROUND_ROBIN_Splitter_5636();
void CombineDFT_5638();
void CombineDFT_5639();
void CombineDFT_5640();
void CombineDFT_5641();
void CombineDFT_5642();
void CombineDFT_5643();
void CombineDFT_5644();
void CombineDFT_5645();
void CombineDFT_5646();
void CombineDFT_5647();
void CombineDFT_5648();
void CombineDFT_5649();
void CombineDFT_5650();
void CombineDFT_5651();
void CombineDFT_5652();
void CombineDFT_5653();
void WEIGHTED_ROUND_ROBIN_Joiner_5637();
void WEIGHTED_ROUND_ROBIN_Splitter_5654();
void CombineDFT_5656();
void CombineDFT_5657();
void CombineDFT_5658();
void CombineDFT_5659();
void CombineDFT_5660();
void CombineDFT_5661();
void CombineDFT_5662();
void CombineDFT_5663();
void WEIGHTED_ROUND_ROBIN_Joiner_5655();
void WEIGHTED_ROUND_ROBIN_Splitter_5664();
void CombineDFT_5666();
void CombineDFT_5667();
void CombineDFT_5668();
void CombineDFT_5669();
void WEIGHTED_ROUND_ROBIN_Joiner_5665();
void WEIGHTED_ROUND_ROBIN_Splitter_5670();
void CombineDFT_5672();
void CombineDFT_5673();
void WEIGHTED_ROUND_ROBIN_Joiner_5671();
void CombineDFT_5462();
void WEIGHTED_ROUND_ROBIN_Joiner_5465();
void FloatPrinter_5463();

#ifdef __cplusplus
}
#endif
#endif
