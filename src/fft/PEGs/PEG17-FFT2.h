#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=8704 on the compile command line
#else
#if BUF_SIZEMAX < 8704
#error BUF_SIZEMAX too small, it must be at least 8704
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float w[2];
} CombineDFT_7312_t;

typedef struct {
	float w[4];
} CombineDFT_7331_t;

typedef struct {
	float w[8];
} CombineDFT_7349_t;

typedef struct {
	float w[16];
} CombineDFT_7359_t;

typedef struct {
	float w[32];
} CombineDFT_7365_t;

typedef struct {
	float w[64];
} CombineDFT_7247_t;
void WEIGHTED_ROUND_ROBIN_Splitter_7268();
void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout);
void FFTTestSource_7270();
void FFTTestSource_7271();
void WEIGHTED_ROUND_ROBIN_Joiner_7269();
void WEIGHTED_ROUND_ROBIN_Splitter_7260();
void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout);
void FFTReorderSimple_7237();
void WEIGHTED_ROUND_ROBIN_Splitter_7272();
void FFTReorderSimple_7274();
void FFTReorderSimple_7275();
void WEIGHTED_ROUND_ROBIN_Joiner_7273();
void WEIGHTED_ROUND_ROBIN_Splitter_7276();
void FFTReorderSimple_7278();
void FFTReorderSimple_7279();
void FFTReorderSimple_7280();
void FFTReorderSimple_7281();
void WEIGHTED_ROUND_ROBIN_Joiner_7277();
void WEIGHTED_ROUND_ROBIN_Splitter_7282();
void FFTReorderSimple_7284();
void FFTReorderSimple_7285();
void FFTReorderSimple_7286();
void FFTReorderSimple_7287();
void FFTReorderSimple_7288();
void FFTReorderSimple_7289();
void FFTReorderSimple_7290();
void FFTReorderSimple_7291();
void WEIGHTED_ROUND_ROBIN_Joiner_7283();
void WEIGHTED_ROUND_ROBIN_Splitter_7292();
void FFTReorderSimple_7294();
void FFTReorderSimple_7295();
void FFTReorderSimple_7296();
void FFTReorderSimple_7297();
void FFTReorderSimple_7298();
void FFTReorderSimple_7299();
void FFTReorderSimple_7300();
void FFTReorderSimple_7301();
void FFTReorderSimple_7302();
void FFTReorderSimple_7303();
void FFTReorderSimple_7304();
void FFTReorderSimple_7305();
void FFTReorderSimple_7306();
void FFTReorderSimple_7307();
void FFTReorderSimple_7308();
void FFTReorderSimple_7309();
void WEIGHTED_ROUND_ROBIN_Joiner_7293();
void WEIGHTED_ROUND_ROBIN_Splitter_7310();
void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout);
void CombineDFT_7312();
void CombineDFT_7313();
void CombineDFT_7314();
void CombineDFT_7315();
void CombineDFT_7316();
void CombineDFT_7317();
void CombineDFT_7318();
void CombineDFT_7319();
void CombineDFT_7320();
void CombineDFT_7321();
void CombineDFT_7322();
void CombineDFT_7323();
void CombineDFT_7324();
void CombineDFT_7325();
void CombineDFT_7326();
void CombineDFT_7327();
void CombineDFT_7328();
void WEIGHTED_ROUND_ROBIN_Joiner_7311();
void WEIGHTED_ROUND_ROBIN_Splitter_7329();
void CombineDFT_7331();
void CombineDFT_7332();
void CombineDFT_7333();
void CombineDFT_7334();
void CombineDFT_7335();
void CombineDFT_7336();
void CombineDFT_7337();
void CombineDFT_7338();
void CombineDFT_7339();
void CombineDFT_7340();
void CombineDFT_7341();
void CombineDFT_7342();
void CombineDFT_7343();
void CombineDFT_7344();
void CombineDFT_7345();
void CombineDFT_7346();
void WEIGHTED_ROUND_ROBIN_Joiner_7330();
void WEIGHTED_ROUND_ROBIN_Splitter_7347();
void CombineDFT_7349();
void CombineDFT_7350();
void CombineDFT_7351();
void CombineDFT_7352();
void CombineDFT_7353();
void CombineDFT_7354();
void CombineDFT_7355();
void CombineDFT_7356();
void WEIGHTED_ROUND_ROBIN_Joiner_7348();
void WEIGHTED_ROUND_ROBIN_Splitter_7357();
void CombineDFT_7359();
void CombineDFT_7360();
void CombineDFT_7361();
void CombineDFT_7362();
void WEIGHTED_ROUND_ROBIN_Joiner_7358();
void WEIGHTED_ROUND_ROBIN_Splitter_7363();
void CombineDFT_7365();
void CombineDFT_7366();
void WEIGHTED_ROUND_ROBIN_Joiner_7364();
void CombineDFT_7247();
void FFTReorderSimple_7248();
void WEIGHTED_ROUND_ROBIN_Splitter_7367();
void FFTReorderSimple_7369();
void FFTReorderSimple_7370();
void WEIGHTED_ROUND_ROBIN_Joiner_7368();
void WEIGHTED_ROUND_ROBIN_Splitter_7371();
void FFTReorderSimple_7373();
void FFTReorderSimple_7374();
void FFTReorderSimple_7375();
void FFTReorderSimple_7376();
void WEIGHTED_ROUND_ROBIN_Joiner_7372();
void WEIGHTED_ROUND_ROBIN_Splitter_7377();
void FFTReorderSimple_7379();
void FFTReorderSimple_7380();
void FFTReorderSimple_7381();
void FFTReorderSimple_7382();
void FFTReorderSimple_7383();
void FFTReorderSimple_7384();
void FFTReorderSimple_7385();
void FFTReorderSimple_7386();
void WEIGHTED_ROUND_ROBIN_Joiner_7378();
void WEIGHTED_ROUND_ROBIN_Splitter_7387();
void FFTReorderSimple_7389();
void FFTReorderSimple_7390();
void FFTReorderSimple_7391();
void FFTReorderSimple_7392();
void FFTReorderSimple_7393();
void FFTReorderSimple_7394();
void FFTReorderSimple_7395();
void FFTReorderSimple_7396();
void FFTReorderSimple_7397();
void FFTReorderSimple_7398();
void FFTReorderSimple_7399();
void FFTReorderSimple_7400();
void FFTReorderSimple_7401();
void FFTReorderSimple_7402();
void FFTReorderSimple_7403();
void FFTReorderSimple_7404();
void WEIGHTED_ROUND_ROBIN_Joiner_7388();
void WEIGHTED_ROUND_ROBIN_Splitter_7405();
void CombineDFT_7407();
void CombineDFT_7408();
void CombineDFT_7409();
void CombineDFT_7410();
void CombineDFT_7411();
void CombineDFT_7412();
void CombineDFT_7413();
void CombineDFT_7414();
void CombineDFT_7415();
void CombineDFT_7416();
void CombineDFT_7417();
void CombineDFT_7418();
void CombineDFT_7419();
void CombineDFT_7420();
void CombineDFT_7421();
void CombineDFT_7422();
void CombineDFT_7423();
void WEIGHTED_ROUND_ROBIN_Joiner_7406();
void WEIGHTED_ROUND_ROBIN_Splitter_7424();
void CombineDFT_7426();
void CombineDFT_7427();
void CombineDFT_7428();
void CombineDFT_7429();
void CombineDFT_7430();
void CombineDFT_7431();
void CombineDFT_7432();
void CombineDFT_7433();
void CombineDFT_7434();
void CombineDFT_7435();
void CombineDFT_7436();
void CombineDFT_7437();
void CombineDFT_7438();
void CombineDFT_7439();
void CombineDFT_7440();
void CombineDFT_7441();
void WEIGHTED_ROUND_ROBIN_Joiner_7425();
void WEIGHTED_ROUND_ROBIN_Splitter_7442();
void CombineDFT_7444();
void CombineDFT_7445();
void CombineDFT_7446();
void CombineDFT_7447();
void CombineDFT_7448();
void CombineDFT_7449();
void CombineDFT_7450();
void CombineDFT_7451();
void WEIGHTED_ROUND_ROBIN_Joiner_7443();
void WEIGHTED_ROUND_ROBIN_Splitter_7452();
void CombineDFT_7454();
void CombineDFT_7455();
void CombineDFT_7456();
void CombineDFT_7457();
void WEIGHTED_ROUND_ROBIN_Joiner_7453();
void WEIGHTED_ROUND_ROBIN_Splitter_7458();
void CombineDFT_7460();
void CombineDFT_7461();
void WEIGHTED_ROUND_ROBIN_Joiner_7459();
void CombineDFT_7258();
void WEIGHTED_ROUND_ROBIN_Joiner_7261();
void FloatPrinter(buffer_float_t *chanin);
void FloatPrinter_7259();

#ifdef __cplusplus
}
#endif
#endif
