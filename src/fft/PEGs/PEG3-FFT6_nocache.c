#include "PEG3-FFT6_nocache.h"

buffer_complex_t SplitJoin12_CombineDFT_Fiss_5936_5946_join[3];
buffer_complex_t SplitJoin8_CombineDFT_Fiss_5934_5944_split[3];
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_5931_5941_split[3];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_5930_5940_split[2];
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_5933_5943_join[3];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5917WEIGHTED_ROUND_ROBIN_Splitter_5921;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5927CombineDFT_5884;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5907WEIGHTED_ROUND_ROBIN_Splitter_5911;
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_5930_5940_join[2];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_5935_5945_split[3];
buffer_complex_t CombineDFT_5884CPrinter_5885;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_5934_5944_join[3];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5897WEIGHTED_ROUND_ROBIN_Splitter_5901;
buffer_complex_t SplitJoin16_CombineDFT_Fiss_5938_5948_join[2];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_5932_5942_split[3];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_5938_5948_split[2];
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_5933_5943_split[3];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_5935_5945_join[3];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5888WEIGHTED_ROUND_ROBIN_Splitter_5891;
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_5932_5942_join[3];
buffer_complex_t SplitJoin12_CombineDFT_Fiss_5936_5946_split[3];
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_5931_5941_join[3];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5902WEIGHTED_ROUND_ROBIN_Splitter_5906;
buffer_complex_t FFTTestSource_5873FFTReorderSimple_5874;
buffer_complex_t FFTReorderSimple_5874WEIGHTED_ROUND_ROBIN_Splitter_5887;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5922WEIGHTED_ROUND_ROBIN_Splitter_5926;
buffer_complex_t SplitJoin14_CombineDFT_Fiss_5937_5947_split[3];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5892WEIGHTED_ROUND_ROBIN_Splitter_5896;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5912WEIGHTED_ROUND_ROBIN_Splitter_5916;
buffer_complex_t SplitJoin14_CombineDFT_Fiss_5937_5947_join[3];


CombineDFT_5908_t CombineDFT_5908_s;
CombineDFT_5908_t CombineDFT_5909_s;
CombineDFT_5908_t CombineDFT_5910_s;
CombineDFT_5908_t CombineDFT_5913_s;
CombineDFT_5908_t CombineDFT_5914_s;
CombineDFT_5908_t CombineDFT_5915_s;
CombineDFT_5908_t CombineDFT_5918_s;
CombineDFT_5908_t CombineDFT_5919_s;
CombineDFT_5908_t CombineDFT_5920_s;
CombineDFT_5908_t CombineDFT_5923_s;
CombineDFT_5908_t CombineDFT_5924_s;
CombineDFT_5908_t CombineDFT_5925_s;
CombineDFT_5908_t CombineDFT_5928_s;
CombineDFT_5908_t CombineDFT_5929_s;
CombineDFT_5908_t CombineDFT_5884_s;

void FFTTestSource_5873(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		complex_t c1;
		complex_t zero;
		c1.real = 1.0 ; 
		c1.imag = 0.0 ; 
		zero.real = 0.0 ; 
		zero.imag = 0.0 ; 
		push_complex(&FFTTestSource_5873FFTReorderSimple_5874, zero) ; 
		push_complex(&FFTTestSource_5873FFTReorderSimple_5874, c1) ; 
		FOR(int, i, 0,  < , 62, i++) {
			push_complex(&FFTTestSource_5873FFTReorderSimple_5874, zero) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5874(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&FFTTestSource_5873FFTReorderSimple_5874, i)) ; 
			push_complex(&FFTReorderSimple_5874WEIGHTED_ROUND_ROBIN_Splitter_5887, __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 64, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&FFTTestSource_5873FFTReorderSimple_5874, i)) ; 
			push_complex(&FFTReorderSimple_5874WEIGHTED_ROUND_ROBIN_Splitter_5887, __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&FFTTestSource_5873FFTReorderSimple_5874) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5889(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_5930_5940_split[0], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_5930_5940_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 32, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_5930_5940_split[0], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_5930_5940_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_5930_5940_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5890(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_5930_5940_split[1], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_5930_5940_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 32, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_5930_5940_split[1], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_5930_5940_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_5930_5940_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5887() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_5930_5940_split[0], pop_complex(&FFTReorderSimple_5874WEIGHTED_ROUND_ROBIN_Splitter_5887));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_5930_5940_split[1], pop_complex(&FFTReorderSimple_5874WEIGHTED_ROUND_ROBIN_Splitter_5887));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5888() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5888WEIGHTED_ROUND_ROBIN_Splitter_5891, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_5930_5940_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5888WEIGHTED_ROUND_ROBIN_Splitter_5891, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_5930_5940_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_5893(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_5931_5941_split[0], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5931_5941_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_5931_5941_split[0], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5931_5941_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_5931_5941_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5894(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_5931_5941_split[1], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5931_5941_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_5931_5941_split[1], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5931_5941_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_5931_5941_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5895(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_5931_5941_split[2], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5931_5941_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_5931_5941_split[2], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5931_5941_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_5931_5941_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5891() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5931_5941_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5888WEIGHTED_ROUND_ROBIN_Splitter_5891));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5892() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5892WEIGHTED_ROUND_ROBIN_Splitter_5896, pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_5931_5941_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_5898(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5932_5942_split[0], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5932_5942_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5932_5942_split[0], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5932_5942_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_5932_5942_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5899(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5932_5942_split[1], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5932_5942_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5932_5942_split[1], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5932_5942_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_5932_5942_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5900(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5932_5942_split[2], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5932_5942_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5932_5942_split[2], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5932_5942_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_5932_5942_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5896() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5932_5942_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5892WEIGHTED_ROUND_ROBIN_Splitter_5896));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5897() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5897WEIGHTED_ROUND_ROBIN_Splitter_5901, pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_5932_5942_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_5903(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5933_5943_split[0], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5933_5943_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5933_5943_split[0], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5933_5943_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_5933_5943_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5904(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5933_5943_split[1], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5933_5943_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5933_5943_split[1], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5933_5943_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_5933_5943_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5905(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5933_5943_split[2], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5933_5943_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5933_5943_split[2], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5933_5943_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_5933_5943_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5901() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5933_5943_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5897WEIGHTED_ROUND_ROBIN_Splitter_5901));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5902() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5902WEIGHTED_ROUND_ROBIN_Splitter_5906, pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_5933_5943_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5908(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5934_5944_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5934_5944_split[0], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5908_s.wn.real) - (w.imag * CombineDFT_5908_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5908_s.wn.imag) + (w.imag * CombineDFT_5908_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_5934_5944_split[0]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_5934_5944_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5909(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5934_5944_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5934_5944_split[1], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5909_s.wn.real) - (w.imag * CombineDFT_5909_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5909_s.wn.imag) + (w.imag * CombineDFT_5909_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_5934_5944_split[1]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_5934_5944_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5910(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5934_5944_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5934_5944_split[2], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5910_s.wn.real) - (w.imag * CombineDFT_5910_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5910_s.wn.imag) + (w.imag * CombineDFT_5910_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_5934_5944_split[2]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_5934_5944_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5906() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_complex(&SplitJoin8_CombineDFT_Fiss_5934_5944_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5902WEIGHTED_ROUND_ROBIN_Splitter_5906));
			push_complex(&SplitJoin8_CombineDFT_Fiss_5934_5944_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5902WEIGHTED_ROUND_ROBIN_Splitter_5906));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5907() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5907WEIGHTED_ROUND_ROBIN_Splitter_5911, pop_complex(&SplitJoin8_CombineDFT_Fiss_5934_5944_join[__iter_]));
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5907WEIGHTED_ROUND_ROBIN_Splitter_5911, pop_complex(&SplitJoin8_CombineDFT_Fiss_5934_5944_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_5913(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5935_5945_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5935_5945_split[0], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5913_s.wn.real) - (w.imag * CombineDFT_5913_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5913_s.wn.imag) + (w.imag * CombineDFT_5913_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_5935_5945_split[0]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_5935_5945_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5914(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5935_5945_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5935_5945_split[1], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5914_s.wn.real) - (w.imag * CombineDFT_5914_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5914_s.wn.imag) + (w.imag * CombineDFT_5914_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_5935_5945_split[1]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_5935_5945_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5915(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5935_5945_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5935_5945_split[2], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5915_s.wn.real) - (w.imag * CombineDFT_5915_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5915_s.wn.imag) + (w.imag * CombineDFT_5915_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_5935_5945_split[2]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_5935_5945_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5911() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin10_CombineDFT_Fiss_5935_5945_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5907WEIGHTED_ROUND_ROBIN_Splitter_5911));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5912() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5912WEIGHTED_ROUND_ROBIN_Splitter_5916, pop_complex(&SplitJoin10_CombineDFT_Fiss_5935_5945_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5918(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5936_5946_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5936_5946_split[0], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5918_s.wn.real) - (w.imag * CombineDFT_5918_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5918_s.wn.imag) + (w.imag * CombineDFT_5918_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_5936_5946_split[0]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_5936_5946_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5919(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5936_5946_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5936_5946_split[1], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5919_s.wn.real) - (w.imag * CombineDFT_5919_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5919_s.wn.imag) + (w.imag * CombineDFT_5919_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_5936_5946_split[1]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_5936_5946_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5920(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5936_5946_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5936_5946_split[2], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5920_s.wn.real) - (w.imag * CombineDFT_5920_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5920_s.wn.imag) + (w.imag * CombineDFT_5920_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_5936_5946_split[2]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_5936_5946_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5916() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin12_CombineDFT_Fiss_5936_5946_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5912WEIGHTED_ROUND_ROBIN_Splitter_5916));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5917() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5917WEIGHTED_ROUND_ROBIN_Splitter_5921, pop_complex(&SplitJoin12_CombineDFT_Fiss_5936_5946_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5923(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_5937_5947_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_5937_5947_split[0], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5923_s.wn.real) - (w.imag * CombineDFT_5923_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5923_s.wn.imag) + (w.imag * CombineDFT_5923_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_5937_5947_split[0]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_5937_5947_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5924(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_5937_5947_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_5937_5947_split[1], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5924_s.wn.real) - (w.imag * CombineDFT_5924_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5924_s.wn.imag) + (w.imag * CombineDFT_5924_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_5937_5947_split[1]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_5937_5947_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5925(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_5937_5947_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_5937_5947_split[2], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5925_s.wn.real) - (w.imag * CombineDFT_5925_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5925_s.wn.imag) + (w.imag * CombineDFT_5925_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_5937_5947_split[2]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_5937_5947_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5921() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin14_CombineDFT_Fiss_5937_5947_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5917WEIGHTED_ROUND_ROBIN_Splitter_5921));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5922() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5922WEIGHTED_ROUND_ROBIN_Splitter_5926, pop_complex(&SplitJoin14_CombineDFT_Fiss_5937_5947_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5928(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[32];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 16, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_5938_5948_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_5938_5948_split[0], (16 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(16 + i)].real = (y0.real - y1w.real) ; 
			results[(16 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5928_s.wn.real) - (w.imag * CombineDFT_5928_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5928_s.wn.imag) + (w.imag * CombineDFT_5928_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin16_CombineDFT_Fiss_5938_5948_split[0]) ; 
			push_complex(&SplitJoin16_CombineDFT_Fiss_5938_5948_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5929(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[32];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 16, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_5938_5948_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_5938_5948_split[1], (16 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(16 + i)].real = (y0.real - y1w.real) ; 
			results[(16 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5929_s.wn.real) - (w.imag * CombineDFT_5929_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5929_s.wn.imag) + (w.imag * CombineDFT_5929_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin16_CombineDFT_Fiss_5938_5948_split[1]) ; 
			push_complex(&SplitJoin16_CombineDFT_Fiss_5938_5948_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5926() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_5938_5948_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5922WEIGHTED_ROUND_ROBIN_Splitter_5926));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_5938_5948_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5922WEIGHTED_ROUND_ROBIN_Splitter_5926));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5927() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5927CombineDFT_5884, pop_complex(&SplitJoin16_CombineDFT_Fiss_5938_5948_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5927CombineDFT_5884, pop_complex(&SplitJoin16_CombineDFT_Fiss_5938_5948_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_5884(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[64];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5927CombineDFT_5884, i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5927CombineDFT_5884, (32 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(32 + i)].real = (y0.real - y1w.real) ; 
			results[(32 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5884_s.wn.real) - (w.imag * CombineDFT_5884_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5884_s.wn.imag) + (w.imag * CombineDFT_5884_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5927CombineDFT_5884) ; 
			push_complex(&CombineDFT_5884CPrinter_5885, results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CPrinter_5885(){
	FOR(uint32_t, __iter_steady_, 0, <, 192, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&CombineDFT_5884CPrinter_5885));
		printf("%.10f", c.real);
		printf("\n");
		printf("%.10f", c.imag);
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 3, __iter_init_0_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_5936_5946_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 3, __iter_init_1_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_5934_5944_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 3, __iter_init_2_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_5931_5941_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 2, __iter_init_3_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_5930_5940_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 3, __iter_init_4_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_5933_5943_join[__iter_init_4_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5917WEIGHTED_ROUND_ROBIN_Splitter_5921);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5927CombineDFT_5884);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5907WEIGHTED_ROUND_ROBIN_Splitter_5911);
	FOR(int, __iter_init_5_, 0, <, 2, __iter_init_5_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_5930_5940_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 3, __iter_init_6_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_5935_5945_split[__iter_init_6_]);
	ENDFOR
	init_buffer_complex(&CombineDFT_5884CPrinter_5885);
	FOR(int, __iter_init_7_, 0, <, 3, __iter_init_7_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_5934_5944_join[__iter_init_7_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5897WEIGHTED_ROUND_ROBIN_Splitter_5901);
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_5938_5948_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 3, __iter_init_9_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_5932_5942_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_5938_5948_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 3, __iter_init_11_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_5933_5943_split[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 3, __iter_init_12_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_5935_5945_join[__iter_init_12_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5888WEIGHTED_ROUND_ROBIN_Splitter_5891);
	FOR(int, __iter_init_13_, 0, <, 3, __iter_init_13_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_5932_5942_join[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 3, __iter_init_14_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_5936_5946_split[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 3, __iter_init_15_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_5931_5941_join[__iter_init_15_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5902WEIGHTED_ROUND_ROBIN_Splitter_5906);
	init_buffer_complex(&FFTTestSource_5873FFTReorderSimple_5874);
	init_buffer_complex(&FFTReorderSimple_5874WEIGHTED_ROUND_ROBIN_Splitter_5887);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5922WEIGHTED_ROUND_ROBIN_Splitter_5926);
	FOR(int, __iter_init_16_, 0, <, 3, __iter_init_16_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_5937_5947_split[__iter_init_16_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5892WEIGHTED_ROUND_ROBIN_Splitter_5896);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5912WEIGHTED_ROUND_ROBIN_Splitter_5916);
	FOR(int, __iter_init_17_, 0, <, 3, __iter_init_17_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_5937_5947_join[__iter_init_17_]);
	ENDFOR
// --- init: CombineDFT_5908
	 {
	 ; 
	CombineDFT_5908_s.wn.real = -1.0 ; 
	CombineDFT_5908_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5909
	 {
	 ; 
	CombineDFT_5909_s.wn.real = -1.0 ; 
	CombineDFT_5909_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5910
	 {
	 ; 
	CombineDFT_5910_s.wn.real = -1.0 ; 
	CombineDFT_5910_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5913
	 {
	 ; 
	CombineDFT_5913_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5913_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5914
	 {
	 ; 
	CombineDFT_5914_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5914_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5915
	 {
	 ; 
	CombineDFT_5915_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5915_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5918
	 {
	 ; 
	CombineDFT_5918_s.wn.real = 0.70710677 ; 
	CombineDFT_5918_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5919
	 {
	 ; 
	CombineDFT_5919_s.wn.real = 0.70710677 ; 
	CombineDFT_5919_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5920
	 {
	 ; 
	CombineDFT_5920_s.wn.real = 0.70710677 ; 
	CombineDFT_5920_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5923
	 {
	 ; 
	CombineDFT_5923_s.wn.real = 0.9238795 ; 
	CombineDFT_5923_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_5924
	 {
	 ; 
	CombineDFT_5924_s.wn.real = 0.9238795 ; 
	CombineDFT_5924_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_5925
	 {
	 ; 
	CombineDFT_5925_s.wn.real = 0.9238795 ; 
	CombineDFT_5925_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_5928
	 {
	 ; 
	CombineDFT_5928_s.wn.real = 0.98078525 ; 
	CombineDFT_5928_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_5929
	 {
	 ; 
	CombineDFT_5929_s.wn.real = 0.98078525 ; 
	CombineDFT_5929_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_5884
	 {
	 ; 
	CombineDFT_5884_s.wn.real = 0.9951847 ; 
	CombineDFT_5884_s.wn.imag = -0.09801714 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FFTTestSource_5873();
		FFTReorderSimple_5874();
		WEIGHTED_ROUND_ROBIN_Splitter_5887();
			FFTReorderSimple_5889();
			FFTReorderSimple_5890();
		WEIGHTED_ROUND_ROBIN_Joiner_5888();
		WEIGHTED_ROUND_ROBIN_Splitter_5891();
			FFTReorderSimple_5893();
			FFTReorderSimple_5894();
			FFTReorderSimple_5895();
		WEIGHTED_ROUND_ROBIN_Joiner_5892();
		WEIGHTED_ROUND_ROBIN_Splitter_5896();
			FFTReorderSimple_5898();
			FFTReorderSimple_5899();
			FFTReorderSimple_5900();
		WEIGHTED_ROUND_ROBIN_Joiner_5897();
		WEIGHTED_ROUND_ROBIN_Splitter_5901();
			FFTReorderSimple_5903();
			FFTReorderSimple_5904();
			FFTReorderSimple_5905();
		WEIGHTED_ROUND_ROBIN_Joiner_5902();
		WEIGHTED_ROUND_ROBIN_Splitter_5906();
			CombineDFT_5908();
			CombineDFT_5909();
			CombineDFT_5910();
		WEIGHTED_ROUND_ROBIN_Joiner_5907();
		WEIGHTED_ROUND_ROBIN_Splitter_5911();
			CombineDFT_5913();
			CombineDFT_5914();
			CombineDFT_5915();
		WEIGHTED_ROUND_ROBIN_Joiner_5912();
		WEIGHTED_ROUND_ROBIN_Splitter_5916();
			CombineDFT_5918();
			CombineDFT_5919();
			CombineDFT_5920();
		WEIGHTED_ROUND_ROBIN_Joiner_5917();
		WEIGHTED_ROUND_ROBIN_Splitter_5921();
			CombineDFT_5923();
			CombineDFT_5924();
			CombineDFT_5925();
		WEIGHTED_ROUND_ROBIN_Joiner_5922();
		WEIGHTED_ROUND_ROBIN_Splitter_5926();
			CombineDFT_5928();
			CombineDFT_5929();
		WEIGHTED_ROUND_ROBIN_Joiner_5927();
		CombineDFT_5884();
		CPrinter_5885();
	ENDFOR
	return EXIT_SUCCESS;
}
