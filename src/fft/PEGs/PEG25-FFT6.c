#include "PEG25-FFT6.h"

buffer_complex_t SplitJoin8_CombineDFT_Fiss_1836_1846_join[25];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1795WEIGHTED_ROUND_ROBIN_Splitter_1812;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1740WEIGHTED_ROUND_ROBIN_Splitter_1749;
buffer_complex_t SplitJoin14_CombineDFT_Fiss_1839_1849_join[4];
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_1835_1845_join[16];
buffer_complex_t FFTReorderSimple_1716WEIGHTED_ROUND_ROBIN_Splitter_1729;
buffer_complex_t SplitJoin14_CombineDFT_Fiss_1839_1849_split[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1823WEIGHTED_ROUND_ROBIN_Splitter_1828;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1829CombineDFT_1726;
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_1833_1843_join[4];
buffer_complex_t SplitJoin12_CombineDFT_Fiss_1838_1848_split[8];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1730WEIGHTED_ROUND_ROBIN_Splitter_1733;
buffer_complex_t CombineDFT_1726CPrinter_1727;
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_1832_1842_split[2];
buffer_complex_t SplitJoin8_CombineDFT_Fiss_1836_1846_split[25];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1734WEIGHTED_ROUND_ROBIN_Splitter_1739;
buffer_complex_t SplitJoin16_CombineDFT_Fiss_1840_1850_split[2];
buffer_complex_t SplitJoin12_CombineDFT_Fiss_1838_1848_join[8];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_1832_1842_join[2];
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_1835_1845_split[16];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_1834_1844_join[8];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_1840_1850_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1768WEIGHTED_ROUND_ROBIN_Splitter_1794;
buffer_complex_t SplitJoin10_CombineDFT_Fiss_1837_1847_join[16];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1750WEIGHTED_ROUND_ROBIN_Splitter_1767;
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_1833_1843_split[4];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_1834_1844_split[8];
buffer_complex_t FFTTestSource_1715FFTReorderSimple_1716;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1813WEIGHTED_ROUND_ROBIN_Splitter_1822;
buffer_complex_t SplitJoin10_CombineDFT_Fiss_1837_1847_split[16];


CombineDFT_1769_t CombineDFT_1769_s;
CombineDFT_1769_t CombineDFT_1770_s;
CombineDFT_1769_t CombineDFT_1771_s;
CombineDFT_1769_t CombineDFT_1772_s;
CombineDFT_1769_t CombineDFT_1773_s;
CombineDFT_1769_t CombineDFT_1774_s;
CombineDFT_1769_t CombineDFT_1775_s;
CombineDFT_1769_t CombineDFT_1776_s;
CombineDFT_1769_t CombineDFT_1777_s;
CombineDFT_1769_t CombineDFT_1778_s;
CombineDFT_1769_t CombineDFT_1779_s;
CombineDFT_1769_t CombineDFT_1780_s;
CombineDFT_1769_t CombineDFT_1781_s;
CombineDFT_1769_t CombineDFT_1782_s;
CombineDFT_1769_t CombineDFT_1783_s;
CombineDFT_1769_t CombineDFT_1784_s;
CombineDFT_1769_t CombineDFT_1785_s;
CombineDFT_1769_t CombineDFT_1786_s;
CombineDFT_1769_t CombineDFT_1787_s;
CombineDFT_1769_t CombineDFT_1788_s;
CombineDFT_1769_t CombineDFT_1789_s;
CombineDFT_1769_t CombineDFT_1790_s;
CombineDFT_1769_t CombineDFT_1791_s;
CombineDFT_1769_t CombineDFT_1792_s;
CombineDFT_1769_t CombineDFT_1793_s;
CombineDFT_1769_t CombineDFT_1796_s;
CombineDFT_1769_t CombineDFT_1797_s;
CombineDFT_1769_t CombineDFT_1798_s;
CombineDFT_1769_t CombineDFT_1799_s;
CombineDFT_1769_t CombineDFT_1800_s;
CombineDFT_1769_t CombineDFT_1801_s;
CombineDFT_1769_t CombineDFT_1802_s;
CombineDFT_1769_t CombineDFT_1803_s;
CombineDFT_1769_t CombineDFT_1804_s;
CombineDFT_1769_t CombineDFT_1805_s;
CombineDFT_1769_t CombineDFT_1806_s;
CombineDFT_1769_t CombineDFT_1807_s;
CombineDFT_1769_t CombineDFT_1808_s;
CombineDFT_1769_t CombineDFT_1809_s;
CombineDFT_1769_t CombineDFT_1810_s;
CombineDFT_1769_t CombineDFT_1811_s;
CombineDFT_1769_t CombineDFT_1814_s;
CombineDFT_1769_t CombineDFT_1815_s;
CombineDFT_1769_t CombineDFT_1816_s;
CombineDFT_1769_t CombineDFT_1817_s;
CombineDFT_1769_t CombineDFT_1818_s;
CombineDFT_1769_t CombineDFT_1819_s;
CombineDFT_1769_t CombineDFT_1820_s;
CombineDFT_1769_t CombineDFT_1821_s;
CombineDFT_1769_t CombineDFT_1824_s;
CombineDFT_1769_t CombineDFT_1825_s;
CombineDFT_1769_t CombineDFT_1826_s;
CombineDFT_1769_t CombineDFT_1827_s;
CombineDFT_1769_t CombineDFT_1830_s;
CombineDFT_1769_t CombineDFT_1831_s;
CombineDFT_1769_t CombineDFT_1726_s;

void FFTTestSource(buffer_complex_t *chanout) {
		complex_t c1;
		complex_t zero;
		c1.real = 1.0 ; 
		c1.imag = 0.0 ; 
		zero.real = 0.0 ; 
		zero.imag = 0.0 ; 
		push_complex(&(*chanout), zero) ; 
		push_complex(&(*chanout), c1) ; 
		FOR(int, i, 0,  < , 62, i++) {
			push_complex(&(*chanout), zero) ; 
		}
		ENDFOR
	}


void FFTTestSource_1715() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTTestSource(&(FFTTestSource_1715FFTReorderSimple_1716));
	ENDFOR
}

void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 64, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&(*chanin)) ; 
		}
		ENDFOR
	}


void FFTReorderSimple_1716() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTReorderSimple(&(FFTTestSource_1715FFTReorderSimple_1716), &(FFTReorderSimple_1716WEIGHTED_ROUND_ROBIN_Splitter_1729));
	ENDFOR
}

void FFTReorderSimple_1731() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_1832_1842_split[0]), &(SplitJoin0_FFTReorderSimple_Fiss_1832_1842_join[0]));
	ENDFOR
}

void FFTReorderSimple_1732() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_1832_1842_split[1]), &(SplitJoin0_FFTReorderSimple_Fiss_1832_1842_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1729() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_1832_1842_split[0], pop_complex(&FFTReorderSimple_1716WEIGHTED_ROUND_ROBIN_Splitter_1729));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_1832_1842_split[1], pop_complex(&FFTReorderSimple_1716WEIGHTED_ROUND_ROBIN_Splitter_1729));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1730() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1730WEIGHTED_ROUND_ROBIN_Splitter_1733, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_1832_1842_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1730WEIGHTED_ROUND_ROBIN_Splitter_1733, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_1832_1842_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_1735() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_1833_1843_split[0]), &(SplitJoin2_FFTReorderSimple_Fiss_1833_1843_join[0]));
	ENDFOR
}

void FFTReorderSimple_1736() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_1833_1843_split[1]), &(SplitJoin2_FFTReorderSimple_Fiss_1833_1843_join[1]));
	ENDFOR
}

void FFTReorderSimple_1737() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_1833_1843_split[2]), &(SplitJoin2_FFTReorderSimple_Fiss_1833_1843_join[2]));
	ENDFOR
}

void FFTReorderSimple_1738() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_1833_1843_split[3]), &(SplitJoin2_FFTReorderSimple_Fiss_1833_1843_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1733() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin2_FFTReorderSimple_Fiss_1833_1843_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1730WEIGHTED_ROUND_ROBIN_Splitter_1733));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1734() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1734WEIGHTED_ROUND_ROBIN_Splitter_1739, pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_1833_1843_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_1741() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_1834_1844_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_1834_1844_join[0]));
	ENDFOR
}

void FFTReorderSimple_1742() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_1834_1844_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_1834_1844_join[1]));
	ENDFOR
}

void FFTReorderSimple_1743() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_1834_1844_split[2]), &(SplitJoin4_FFTReorderSimple_Fiss_1834_1844_join[2]));
	ENDFOR
}

void FFTReorderSimple_1744() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_1834_1844_split[3]), &(SplitJoin4_FFTReorderSimple_Fiss_1834_1844_join[3]));
	ENDFOR
}

void FFTReorderSimple_1745() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_1834_1844_split[4]), &(SplitJoin4_FFTReorderSimple_Fiss_1834_1844_join[4]));
	ENDFOR
}

void FFTReorderSimple_1746() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_1834_1844_split[5]), &(SplitJoin4_FFTReorderSimple_Fiss_1834_1844_join[5]));
	ENDFOR
}

void FFTReorderSimple_1747() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_1834_1844_split[6]), &(SplitJoin4_FFTReorderSimple_Fiss_1834_1844_join[6]));
	ENDFOR
}

void FFTReorderSimple_1748() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_1834_1844_split[7]), &(SplitJoin4_FFTReorderSimple_Fiss_1834_1844_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1739() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin4_FFTReorderSimple_Fiss_1834_1844_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1734WEIGHTED_ROUND_ROBIN_Splitter_1739));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1740() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1740WEIGHTED_ROUND_ROBIN_Splitter_1749, pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_1834_1844_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_1751() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_join[0]));
	ENDFOR
}

void FFTReorderSimple_1752() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_join[1]));
	ENDFOR
}

void FFTReorderSimple_1753() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_join[2]));
	ENDFOR
}

void FFTReorderSimple_1754() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_join[3]));
	ENDFOR
}

void FFTReorderSimple_1755() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_split[4]), &(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_join[4]));
	ENDFOR
}

void FFTReorderSimple_1756() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_split[5]), &(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_join[5]));
	ENDFOR
}

void FFTReorderSimple_1757() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_split[6]), &(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_join[6]));
	ENDFOR
}

void FFTReorderSimple_1758() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_split[7]), &(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_join[7]));
	ENDFOR
}

void FFTReorderSimple_1759() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_split[8]), &(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_join[8]));
	ENDFOR
}

void FFTReorderSimple_1760() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_split[9]), &(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_join[9]));
	ENDFOR
}

void FFTReorderSimple_1761() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_split[10]), &(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_join[10]));
	ENDFOR
}

void FFTReorderSimple_1762() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_split[11]), &(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_join[11]));
	ENDFOR
}

void FFTReorderSimple_1763() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_split[12]), &(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_join[12]));
	ENDFOR
}

void FFTReorderSimple_1764() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_split[13]), &(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_join[13]));
	ENDFOR
}

void FFTReorderSimple_1765() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_split[14]), &(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_join[14]));
	ENDFOR
}

void FFTReorderSimple_1766() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_split[15]), &(SplitJoin6_FFTReorderSimple_Fiss_1835_1845_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1749() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1835_1845_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1740WEIGHTED_ROUND_ROBIN_Splitter_1749));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1750() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1750WEIGHTED_ROUND_ROBIN_Splitter_1767, pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_1835_1845_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&(*chanin), (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1769_s.wn.real) - (w.imag * CombineDFT_1769_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1769_s.wn.imag) + (w.imag * CombineDFT_1769_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&(*chanin)) ; 
			push_complex(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_1769() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1836_1846_split[0]), &(SplitJoin8_CombineDFT_Fiss_1836_1846_join[0]));
	ENDFOR
}

void CombineDFT_1770() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1836_1846_split[1]), &(SplitJoin8_CombineDFT_Fiss_1836_1846_join[1]));
	ENDFOR
}

void CombineDFT_1771() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1836_1846_split[2]), &(SplitJoin8_CombineDFT_Fiss_1836_1846_join[2]));
	ENDFOR
}

void CombineDFT_1772() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1836_1846_split[3]), &(SplitJoin8_CombineDFT_Fiss_1836_1846_join[3]));
	ENDFOR
}

void CombineDFT_1773() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1836_1846_split[4]), &(SplitJoin8_CombineDFT_Fiss_1836_1846_join[4]));
	ENDFOR
}

void CombineDFT_1774() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1836_1846_split[5]), &(SplitJoin8_CombineDFT_Fiss_1836_1846_join[5]));
	ENDFOR
}

void CombineDFT_1775() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1836_1846_split[6]), &(SplitJoin8_CombineDFT_Fiss_1836_1846_join[6]));
	ENDFOR
}

void CombineDFT_1776() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1836_1846_split[7]), &(SplitJoin8_CombineDFT_Fiss_1836_1846_join[7]));
	ENDFOR
}

void CombineDFT_1777() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1836_1846_split[8]), &(SplitJoin8_CombineDFT_Fiss_1836_1846_join[8]));
	ENDFOR
}

void CombineDFT_1778() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1836_1846_split[9]), &(SplitJoin8_CombineDFT_Fiss_1836_1846_join[9]));
	ENDFOR
}

void CombineDFT_1779() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1836_1846_split[10]), &(SplitJoin8_CombineDFT_Fiss_1836_1846_join[10]));
	ENDFOR
}

void CombineDFT_1780() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1836_1846_split[11]), &(SplitJoin8_CombineDFT_Fiss_1836_1846_join[11]));
	ENDFOR
}

void CombineDFT_1781() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1836_1846_split[12]), &(SplitJoin8_CombineDFT_Fiss_1836_1846_join[12]));
	ENDFOR
}

void CombineDFT_1782() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1836_1846_split[13]), &(SplitJoin8_CombineDFT_Fiss_1836_1846_join[13]));
	ENDFOR
}

void CombineDFT_1783() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1836_1846_split[14]), &(SplitJoin8_CombineDFT_Fiss_1836_1846_join[14]));
	ENDFOR
}

void CombineDFT_1784() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1836_1846_split[15]), &(SplitJoin8_CombineDFT_Fiss_1836_1846_join[15]));
	ENDFOR
}

void CombineDFT_1785() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1836_1846_split[16]), &(SplitJoin8_CombineDFT_Fiss_1836_1846_join[16]));
	ENDFOR
}

void CombineDFT_1786() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1836_1846_split[17]), &(SplitJoin8_CombineDFT_Fiss_1836_1846_join[17]));
	ENDFOR
}

void CombineDFT_1787() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1836_1846_split[18]), &(SplitJoin8_CombineDFT_Fiss_1836_1846_join[18]));
	ENDFOR
}

void CombineDFT_1788() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1836_1846_split[19]), &(SplitJoin8_CombineDFT_Fiss_1836_1846_join[19]));
	ENDFOR
}

void CombineDFT_1789() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1836_1846_split[20]), &(SplitJoin8_CombineDFT_Fiss_1836_1846_join[20]));
	ENDFOR
}

void CombineDFT_1790() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1836_1846_split[21]), &(SplitJoin8_CombineDFT_Fiss_1836_1846_join[21]));
	ENDFOR
}

void CombineDFT_1791() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1836_1846_split[22]), &(SplitJoin8_CombineDFT_Fiss_1836_1846_join[22]));
	ENDFOR
}

void CombineDFT_1792() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1836_1846_split[23]), &(SplitJoin8_CombineDFT_Fiss_1836_1846_join[23]));
	ENDFOR
}

void CombineDFT_1793() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1836_1846_split[24]), &(SplitJoin8_CombineDFT_Fiss_1836_1846_join[24]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1767() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 25, __iter_++)
			push_complex(&SplitJoin8_CombineDFT_Fiss_1836_1846_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1750WEIGHTED_ROUND_ROBIN_Splitter_1767));
			push_complex(&SplitJoin8_CombineDFT_Fiss_1836_1846_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1750WEIGHTED_ROUND_ROBIN_Splitter_1767));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1768() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 25, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1768WEIGHTED_ROUND_ROBIN_Splitter_1794, pop_complex(&SplitJoin8_CombineDFT_Fiss_1836_1846_join[__iter_]));
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1768WEIGHTED_ROUND_ROBIN_Splitter_1794, pop_complex(&SplitJoin8_CombineDFT_Fiss_1836_1846_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_1796() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1837_1847_split[0]), &(SplitJoin10_CombineDFT_Fiss_1837_1847_join[0]));
	ENDFOR
}

void CombineDFT_1797() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1837_1847_split[1]), &(SplitJoin10_CombineDFT_Fiss_1837_1847_join[1]));
	ENDFOR
}

void CombineDFT_1798() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1837_1847_split[2]), &(SplitJoin10_CombineDFT_Fiss_1837_1847_join[2]));
	ENDFOR
}

void CombineDFT_1799() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1837_1847_split[3]), &(SplitJoin10_CombineDFT_Fiss_1837_1847_join[3]));
	ENDFOR
}

void CombineDFT_1800() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1837_1847_split[4]), &(SplitJoin10_CombineDFT_Fiss_1837_1847_join[4]));
	ENDFOR
}

void CombineDFT_1801() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1837_1847_split[5]), &(SplitJoin10_CombineDFT_Fiss_1837_1847_join[5]));
	ENDFOR
}

void CombineDFT_1802() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1837_1847_split[6]), &(SplitJoin10_CombineDFT_Fiss_1837_1847_join[6]));
	ENDFOR
}

void CombineDFT_1803() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1837_1847_split[7]), &(SplitJoin10_CombineDFT_Fiss_1837_1847_join[7]));
	ENDFOR
}

void CombineDFT_1804() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1837_1847_split[8]), &(SplitJoin10_CombineDFT_Fiss_1837_1847_join[8]));
	ENDFOR
}

void CombineDFT_1805() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1837_1847_split[9]), &(SplitJoin10_CombineDFT_Fiss_1837_1847_join[9]));
	ENDFOR
}

void CombineDFT_1806() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1837_1847_split[10]), &(SplitJoin10_CombineDFT_Fiss_1837_1847_join[10]));
	ENDFOR
}

void CombineDFT_1807() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1837_1847_split[11]), &(SplitJoin10_CombineDFT_Fiss_1837_1847_join[11]));
	ENDFOR
}

void CombineDFT_1808() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1837_1847_split[12]), &(SplitJoin10_CombineDFT_Fiss_1837_1847_join[12]));
	ENDFOR
}

void CombineDFT_1809() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1837_1847_split[13]), &(SplitJoin10_CombineDFT_Fiss_1837_1847_join[13]));
	ENDFOR
}

void CombineDFT_1810() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1837_1847_split[14]), &(SplitJoin10_CombineDFT_Fiss_1837_1847_join[14]));
	ENDFOR
}

void CombineDFT_1811() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1837_1847_split[15]), &(SplitJoin10_CombineDFT_Fiss_1837_1847_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1794() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin10_CombineDFT_Fiss_1837_1847_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1768WEIGHTED_ROUND_ROBIN_Splitter_1794));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1795() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1795WEIGHTED_ROUND_ROBIN_Splitter_1812, pop_complex(&SplitJoin10_CombineDFT_Fiss_1837_1847_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_1814() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1838_1848_split[0]), &(SplitJoin12_CombineDFT_Fiss_1838_1848_join[0]));
	ENDFOR
}

void CombineDFT_1815() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1838_1848_split[1]), &(SplitJoin12_CombineDFT_Fiss_1838_1848_join[1]));
	ENDFOR
}

void CombineDFT_1816() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1838_1848_split[2]), &(SplitJoin12_CombineDFT_Fiss_1838_1848_join[2]));
	ENDFOR
}

void CombineDFT_1817() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1838_1848_split[3]), &(SplitJoin12_CombineDFT_Fiss_1838_1848_join[3]));
	ENDFOR
}

void CombineDFT_1818() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1838_1848_split[4]), &(SplitJoin12_CombineDFT_Fiss_1838_1848_join[4]));
	ENDFOR
}

void CombineDFT_1819() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1838_1848_split[5]), &(SplitJoin12_CombineDFT_Fiss_1838_1848_join[5]));
	ENDFOR
}

void CombineDFT_1820() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1838_1848_split[6]), &(SplitJoin12_CombineDFT_Fiss_1838_1848_join[6]));
	ENDFOR
}

void CombineDFT_1821() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1838_1848_split[7]), &(SplitJoin12_CombineDFT_Fiss_1838_1848_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1812() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin12_CombineDFT_Fiss_1838_1848_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1795WEIGHTED_ROUND_ROBIN_Splitter_1812));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1813() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1813WEIGHTED_ROUND_ROBIN_Splitter_1822, pop_complex(&SplitJoin12_CombineDFT_Fiss_1838_1848_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_1824() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_1839_1849_split[0]), &(SplitJoin14_CombineDFT_Fiss_1839_1849_join[0]));
	ENDFOR
}

void CombineDFT_1825() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_1839_1849_split[1]), &(SplitJoin14_CombineDFT_Fiss_1839_1849_join[1]));
	ENDFOR
}

void CombineDFT_1826() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_1839_1849_split[2]), &(SplitJoin14_CombineDFT_Fiss_1839_1849_join[2]));
	ENDFOR
}

void CombineDFT_1827() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_1839_1849_split[3]), &(SplitJoin14_CombineDFT_Fiss_1839_1849_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1822() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin14_CombineDFT_Fiss_1839_1849_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1813WEIGHTED_ROUND_ROBIN_Splitter_1822));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1823() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1823WEIGHTED_ROUND_ROBIN_Splitter_1828, pop_complex(&SplitJoin14_CombineDFT_Fiss_1839_1849_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_1830() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_1840_1850_split[0]), &(SplitJoin16_CombineDFT_Fiss_1840_1850_join[0]));
	ENDFOR
}

void CombineDFT_1831() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_1840_1850_split[1]), &(SplitJoin16_CombineDFT_Fiss_1840_1850_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1828() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_1840_1850_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1823WEIGHTED_ROUND_ROBIN_Splitter_1828));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_1840_1850_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1823WEIGHTED_ROUND_ROBIN_Splitter_1828));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1829() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1829CombineDFT_1726, pop_complex(&SplitJoin16_CombineDFT_Fiss_1840_1850_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1829CombineDFT_1726, pop_complex(&SplitJoin16_CombineDFT_Fiss_1840_1850_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_1726() {
	FOR(uint32_t, __iter_steady_, 0, <, 25, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_1829CombineDFT_1726), &(CombineDFT_1726CPrinter_1727));
	ENDFOR
}

void CPrinter(buffer_complex_t *chanin) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		printf("%.10f", c.real);
		printf("\n");
		printf("%.10f", c.imag);
		printf("\n");
	}


void CPrinter_1727() {
	FOR(uint32_t, __iter_steady_, 0, <, 1600, __iter_steady_++)
		CPrinter(&(CombineDFT_1726CPrinter_1727));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 25, __iter_init_0_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_1836_1846_join[__iter_init_0_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1795WEIGHTED_ROUND_ROBIN_Splitter_1812);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1740WEIGHTED_ROUND_ROBIN_Splitter_1749);
	FOR(int, __iter_init_1_, 0, <, 4, __iter_init_1_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_1839_1849_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 16, __iter_init_2_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_1835_1845_join[__iter_init_2_]);
	ENDFOR
	init_buffer_complex(&FFTReorderSimple_1716WEIGHTED_ROUND_ROBIN_Splitter_1729);
	FOR(int, __iter_init_3_, 0, <, 4, __iter_init_3_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_1839_1849_split[__iter_init_3_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1823WEIGHTED_ROUND_ROBIN_Splitter_1828);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1829CombineDFT_1726);
	FOR(int, __iter_init_4_, 0, <, 4, __iter_init_4_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_1833_1843_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_1838_1848_split[__iter_init_5_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1730WEIGHTED_ROUND_ROBIN_Splitter_1733);
	init_buffer_complex(&CombineDFT_1726CPrinter_1727);
	FOR(int, __iter_init_6_, 0, <, 2, __iter_init_6_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_1832_1842_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 25, __iter_init_7_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_1836_1846_split[__iter_init_7_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1734WEIGHTED_ROUND_ROBIN_Splitter_1739);
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_1840_1850_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_1838_1848_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_1832_1842_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 16, __iter_init_11_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_1835_1845_split[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 8, __iter_init_12_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_1834_1844_join[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_1840_1850_join[__iter_init_13_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1768WEIGHTED_ROUND_ROBIN_Splitter_1794);
	FOR(int, __iter_init_14_, 0, <, 16, __iter_init_14_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_1837_1847_join[__iter_init_14_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1750WEIGHTED_ROUND_ROBIN_Splitter_1767);
	FOR(int, __iter_init_15_, 0, <, 4, __iter_init_15_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_1833_1843_split[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 8, __iter_init_16_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_1834_1844_split[__iter_init_16_]);
	ENDFOR
	init_buffer_complex(&FFTTestSource_1715FFTReorderSimple_1716);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1813WEIGHTED_ROUND_ROBIN_Splitter_1822);
	FOR(int, __iter_init_17_, 0, <, 16, __iter_init_17_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_1837_1847_split[__iter_init_17_]);
	ENDFOR
// --- init: CombineDFT_1769
	 {
	 ; 
	CombineDFT_1769_s.wn.real = -1.0 ; 
	CombineDFT_1769_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1770
	 {
	CombineDFT_1770_s.wn.real = -1.0 ; 
	CombineDFT_1770_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1771
	 {
	CombineDFT_1771_s.wn.real = -1.0 ; 
	CombineDFT_1771_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1772
	 {
	CombineDFT_1772_s.wn.real = -1.0 ; 
	CombineDFT_1772_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1773
	 {
	CombineDFT_1773_s.wn.real = -1.0 ; 
	CombineDFT_1773_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1774
	 {
	CombineDFT_1774_s.wn.real = -1.0 ; 
	CombineDFT_1774_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1775
	 {
	CombineDFT_1775_s.wn.real = -1.0 ; 
	CombineDFT_1775_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1776
	 {
	CombineDFT_1776_s.wn.real = -1.0 ; 
	CombineDFT_1776_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1777
	 {
	CombineDFT_1777_s.wn.real = -1.0 ; 
	CombineDFT_1777_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1778
	 {
	CombineDFT_1778_s.wn.real = -1.0 ; 
	CombineDFT_1778_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1779
	 {
	CombineDFT_1779_s.wn.real = -1.0 ; 
	CombineDFT_1779_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1780
	 {
	CombineDFT_1780_s.wn.real = -1.0 ; 
	CombineDFT_1780_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1781
	 {
	CombineDFT_1781_s.wn.real = -1.0 ; 
	CombineDFT_1781_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1782
	 {
	CombineDFT_1782_s.wn.real = -1.0 ; 
	CombineDFT_1782_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1783
	 {
	CombineDFT_1783_s.wn.real = -1.0 ; 
	CombineDFT_1783_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1784
	 {
	CombineDFT_1784_s.wn.real = -1.0 ; 
	CombineDFT_1784_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1785
	 {
	CombineDFT_1785_s.wn.real = -1.0 ; 
	CombineDFT_1785_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1786
	 {
	CombineDFT_1786_s.wn.real = -1.0 ; 
	CombineDFT_1786_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1787
	 {
	CombineDFT_1787_s.wn.real = -1.0 ; 
	CombineDFT_1787_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1788
	 {
	CombineDFT_1788_s.wn.real = -1.0 ; 
	CombineDFT_1788_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1789
	 {
	CombineDFT_1789_s.wn.real = -1.0 ; 
	CombineDFT_1789_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1790
	 {
	CombineDFT_1790_s.wn.real = -1.0 ; 
	CombineDFT_1790_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1791
	 {
	CombineDFT_1791_s.wn.real = -1.0 ; 
	CombineDFT_1791_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1792
	 {
	CombineDFT_1792_s.wn.real = -1.0 ; 
	CombineDFT_1792_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1793
	 {
	CombineDFT_1793_s.wn.real = -1.0 ; 
	CombineDFT_1793_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1796
	 {
	CombineDFT_1796_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1796_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1797
	 {
	CombineDFT_1797_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1797_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1798
	 {
	CombineDFT_1798_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1798_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1799
	 {
	CombineDFT_1799_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1799_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1800
	 {
	CombineDFT_1800_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1800_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1801
	 {
	CombineDFT_1801_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1801_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1802
	 {
	CombineDFT_1802_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1802_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1803
	 {
	CombineDFT_1803_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1803_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1804
	 {
	CombineDFT_1804_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1804_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1805
	 {
	CombineDFT_1805_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1805_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1806
	 {
	CombineDFT_1806_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1806_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1807
	 {
	CombineDFT_1807_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1807_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1808
	 {
	CombineDFT_1808_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1808_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1809
	 {
	CombineDFT_1809_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1809_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1810
	 {
	CombineDFT_1810_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1810_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1811
	 {
	CombineDFT_1811_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1811_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1814
	 {
	CombineDFT_1814_s.wn.real = 0.70710677 ; 
	CombineDFT_1814_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1815
	 {
	CombineDFT_1815_s.wn.real = 0.70710677 ; 
	CombineDFT_1815_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1816
	 {
	CombineDFT_1816_s.wn.real = 0.70710677 ; 
	CombineDFT_1816_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1817
	 {
	CombineDFT_1817_s.wn.real = 0.70710677 ; 
	CombineDFT_1817_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1818
	 {
	CombineDFT_1818_s.wn.real = 0.70710677 ; 
	CombineDFT_1818_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1819
	 {
	CombineDFT_1819_s.wn.real = 0.70710677 ; 
	CombineDFT_1819_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1820
	 {
	CombineDFT_1820_s.wn.real = 0.70710677 ; 
	CombineDFT_1820_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1821
	 {
	CombineDFT_1821_s.wn.real = 0.70710677 ; 
	CombineDFT_1821_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1824
	 {
	CombineDFT_1824_s.wn.real = 0.9238795 ; 
	CombineDFT_1824_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_1825
	 {
	CombineDFT_1825_s.wn.real = 0.9238795 ; 
	CombineDFT_1825_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_1826
	 {
	CombineDFT_1826_s.wn.real = 0.9238795 ; 
	CombineDFT_1826_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_1827
	 {
	CombineDFT_1827_s.wn.real = 0.9238795 ; 
	CombineDFT_1827_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_1830
	 {
	CombineDFT_1830_s.wn.real = 0.98078525 ; 
	CombineDFT_1830_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_1831
	 {
	CombineDFT_1831_s.wn.real = 0.98078525 ; 
	CombineDFT_1831_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_1726
	 {
	 ; 
	CombineDFT_1726_s.wn.real = 0.9951847 ; 
	CombineDFT_1726_s.wn.imag = -0.09801714 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FFTTestSource_1715();
		FFTReorderSimple_1716();
		WEIGHTED_ROUND_ROBIN_Splitter_1729();
			FFTReorderSimple_1731();
			FFTReorderSimple_1732();
		WEIGHTED_ROUND_ROBIN_Joiner_1730();
		WEIGHTED_ROUND_ROBIN_Splitter_1733();
			FFTReorderSimple_1735();
			FFTReorderSimple_1736();
			FFTReorderSimple_1737();
			FFTReorderSimple_1738();
		WEIGHTED_ROUND_ROBIN_Joiner_1734();
		WEIGHTED_ROUND_ROBIN_Splitter_1739();
			FFTReorderSimple_1741();
			FFTReorderSimple_1742();
			FFTReorderSimple_1743();
			FFTReorderSimple_1744();
			FFTReorderSimple_1745();
			FFTReorderSimple_1746();
			FFTReorderSimple_1747();
			FFTReorderSimple_1748();
		WEIGHTED_ROUND_ROBIN_Joiner_1740();
		WEIGHTED_ROUND_ROBIN_Splitter_1749();
			FFTReorderSimple_1751();
			FFTReorderSimple_1752();
			FFTReorderSimple_1753();
			FFTReorderSimple_1754();
			FFTReorderSimple_1755();
			FFTReorderSimple_1756();
			FFTReorderSimple_1757();
			FFTReorderSimple_1758();
			FFTReorderSimple_1759();
			FFTReorderSimple_1760();
			FFTReorderSimple_1761();
			FFTReorderSimple_1762();
			FFTReorderSimple_1763();
			FFTReorderSimple_1764();
			FFTReorderSimple_1765();
			FFTReorderSimple_1766();
		WEIGHTED_ROUND_ROBIN_Joiner_1750();
		WEIGHTED_ROUND_ROBIN_Splitter_1767();
			CombineDFT_1769();
			CombineDFT_1770();
			CombineDFT_1771();
			CombineDFT_1772();
			CombineDFT_1773();
			CombineDFT_1774();
			CombineDFT_1775();
			CombineDFT_1776();
			CombineDFT_1777();
			CombineDFT_1778();
			CombineDFT_1779();
			CombineDFT_1780();
			CombineDFT_1781();
			CombineDFT_1782();
			CombineDFT_1783();
			CombineDFT_1784();
			CombineDFT_1785();
			CombineDFT_1786();
			CombineDFT_1787();
			CombineDFT_1788();
			CombineDFT_1789();
			CombineDFT_1790();
			CombineDFT_1791();
			CombineDFT_1792();
			CombineDFT_1793();
		WEIGHTED_ROUND_ROBIN_Joiner_1768();
		WEIGHTED_ROUND_ROBIN_Splitter_1794();
			CombineDFT_1796();
			CombineDFT_1797();
			CombineDFT_1798();
			CombineDFT_1799();
			CombineDFT_1800();
			CombineDFT_1801();
			CombineDFT_1802();
			CombineDFT_1803();
			CombineDFT_1804();
			CombineDFT_1805();
			CombineDFT_1806();
			CombineDFT_1807();
			CombineDFT_1808();
			CombineDFT_1809();
			CombineDFT_1810();
			CombineDFT_1811();
		WEIGHTED_ROUND_ROBIN_Joiner_1795();
		WEIGHTED_ROUND_ROBIN_Splitter_1812();
			CombineDFT_1814();
			CombineDFT_1815();
			CombineDFT_1816();
			CombineDFT_1817();
			CombineDFT_1818();
			CombineDFT_1819();
			CombineDFT_1820();
			CombineDFT_1821();
		WEIGHTED_ROUND_ROBIN_Joiner_1813();
		WEIGHTED_ROUND_ROBIN_Splitter_1822();
			CombineDFT_1824();
			CombineDFT_1825();
			CombineDFT_1826();
			CombineDFT_1827();
		WEIGHTED_ROUND_ROBIN_Joiner_1823();
		WEIGHTED_ROUND_ROBIN_Splitter_1828();
			CombineDFT_1830();
			CombineDFT_1831();
		WEIGHTED_ROUND_ROBIN_Joiner_1829();
		CombineDFT_1726();
		CPrinter_1727();
	ENDFOR
	return EXIT_SUCCESS;
}
