#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=3456 on the compile command line
#else
#if BUF_SIZEMAX < 3456
#error BUF_SIZEMAX too small, it must be at least 3456
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	complex_t wn;
} CombineDFT_1311_t;
void FFTTestSource_1257();
void FFTReorderSimple_1258();
void WEIGHTED_ROUND_ROBIN_Splitter_1271();
void FFTReorderSimple_1273();
void FFTReorderSimple_1274();
void WEIGHTED_ROUND_ROBIN_Joiner_1272();
void WEIGHTED_ROUND_ROBIN_Splitter_1275();
void FFTReorderSimple_1277();
void FFTReorderSimple_1278();
void FFTReorderSimple_1279();
void FFTReorderSimple_1280();
void WEIGHTED_ROUND_ROBIN_Joiner_1276();
void WEIGHTED_ROUND_ROBIN_Splitter_1281();
void FFTReorderSimple_1283();
void FFTReorderSimple_1284();
void FFTReorderSimple_1285();
void FFTReorderSimple_1286();
void FFTReorderSimple_1287();
void FFTReorderSimple_1288();
void FFTReorderSimple_1289();
void FFTReorderSimple_1290();
void WEIGHTED_ROUND_ROBIN_Joiner_1282();
void WEIGHTED_ROUND_ROBIN_Splitter_1291();
void FFTReorderSimple_1293();
void FFTReorderSimple_1294();
void FFTReorderSimple_1295();
void FFTReorderSimple_1296();
void FFTReorderSimple_1297();
void FFTReorderSimple_1298();
void FFTReorderSimple_1299();
void FFTReorderSimple_1300();
void FFTReorderSimple_1301();
void FFTReorderSimple_1302();
void FFTReorderSimple_1303();
void FFTReorderSimple_1304();
void FFTReorderSimple_1305();
void FFTReorderSimple_1306();
void FFTReorderSimple_1307();
void FFTReorderSimple_1308();
void WEIGHTED_ROUND_ROBIN_Joiner_1292();
void WEIGHTED_ROUND_ROBIN_Splitter_1309();
void CombineDFT_1311();
void CombineDFT_1312();
void CombineDFT_1313();
void CombineDFT_1314();
void CombineDFT_1315();
void CombineDFT_1316();
void CombineDFT_1317();
void CombineDFT_1318();
void CombineDFT_1319();
void CombineDFT_1320();
void CombineDFT_1321();
void CombineDFT_1322();
void CombineDFT_1323();
void CombineDFT_1324();
void CombineDFT_1325();
void CombineDFT_1326();
void CombineDFT_1327();
void CombineDFT_1328();
void CombineDFT_1329();
void CombineDFT_1330();
void CombineDFT_1331();
void CombineDFT_1332();
void CombineDFT_1333();
void CombineDFT_1334();
void CombineDFT_1335();
void CombineDFT_1336();
void CombineDFT_1337();
void WEIGHTED_ROUND_ROBIN_Joiner_1310();
void WEIGHTED_ROUND_ROBIN_Splitter_1338();
void CombineDFT_1340();
void CombineDFT_1341();
void CombineDFT_1342();
void CombineDFT_1343();
void CombineDFT_1344();
void CombineDFT_1345();
void CombineDFT_1346();
void CombineDFT_1347();
void CombineDFT_1348();
void CombineDFT_1349();
void CombineDFT_1350();
void CombineDFT_1351();
void CombineDFT_1352();
void CombineDFT_1353();
void CombineDFT_1354();
void CombineDFT_1355();
void WEIGHTED_ROUND_ROBIN_Joiner_1339();
void WEIGHTED_ROUND_ROBIN_Splitter_1356();
void CombineDFT_1358();
void CombineDFT_1359();
void CombineDFT_1360();
void CombineDFT_1361();
void CombineDFT_1362();
void CombineDFT_1363();
void CombineDFT_1364();
void CombineDFT_1365();
void WEIGHTED_ROUND_ROBIN_Joiner_1357();
void WEIGHTED_ROUND_ROBIN_Splitter_1366();
void CombineDFT_1368();
void CombineDFT_1369();
void CombineDFT_1370();
void CombineDFT_1371();
void WEIGHTED_ROUND_ROBIN_Joiner_1367();
void WEIGHTED_ROUND_ROBIN_Splitter_1372();
void CombineDFT_1374();
void CombineDFT_1375();
void WEIGHTED_ROUND_ROBIN_Joiner_1373();
void CombineDFT_1268();
void CPrinter_1269();

#ifdef __cplusplus
}
#endif
#endif
