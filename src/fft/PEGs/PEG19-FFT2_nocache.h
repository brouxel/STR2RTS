#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=9728 on the compile command line
#else
#if BUF_SIZEMAX < 9728
#error BUF_SIZEMAX too small, it must be at least 9728
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float w[2];
} CombineDFT_6422_t;

typedef struct {
	float w[4];
} CombineDFT_6443_t;

typedef struct {
	float w[8];
} CombineDFT_6461_t;

typedef struct {
	float w[16];
} CombineDFT_6471_t;

typedef struct {
	float w[32];
} CombineDFT_6477_t;

typedef struct {
	float w[64];
} CombineDFT_6357_t;
void WEIGHTED_ROUND_ROBIN_Splitter_6378();
void FFTTestSource_6380();
void FFTTestSource_6381();
void WEIGHTED_ROUND_ROBIN_Joiner_6379();
void WEIGHTED_ROUND_ROBIN_Splitter_6370();
void FFTReorderSimple_6347();
void WEIGHTED_ROUND_ROBIN_Splitter_6382();
void FFTReorderSimple_6384();
void FFTReorderSimple_6385();
void WEIGHTED_ROUND_ROBIN_Joiner_6383();
void WEIGHTED_ROUND_ROBIN_Splitter_6386();
void FFTReorderSimple_6388();
void FFTReorderSimple_6389();
void FFTReorderSimple_6390();
void FFTReorderSimple_6391();
void WEIGHTED_ROUND_ROBIN_Joiner_6387();
void WEIGHTED_ROUND_ROBIN_Splitter_6392();
void FFTReorderSimple_6394();
void FFTReorderSimple_6395();
void FFTReorderSimple_6396();
void FFTReorderSimple_6397();
void FFTReorderSimple_6398();
void FFTReorderSimple_6399();
void FFTReorderSimple_6400();
void FFTReorderSimple_6401();
void WEIGHTED_ROUND_ROBIN_Joiner_6393();
void WEIGHTED_ROUND_ROBIN_Splitter_6402();
void FFTReorderSimple_6404();
void FFTReorderSimple_6405();
void FFTReorderSimple_6406();
void FFTReorderSimple_6407();
void FFTReorderSimple_6408();
void FFTReorderSimple_6409();
void FFTReorderSimple_6410();
void FFTReorderSimple_6411();
void FFTReorderSimple_6412();
void FFTReorderSimple_6413();
void FFTReorderSimple_6414();
void FFTReorderSimple_6415();
void FFTReorderSimple_6416();
void FFTReorderSimple_6417();
void FFTReorderSimple_6418();
void FFTReorderSimple_6419();
void WEIGHTED_ROUND_ROBIN_Joiner_6403();
void WEIGHTED_ROUND_ROBIN_Splitter_6420();
void CombineDFT_6422();
void CombineDFT_6423();
void CombineDFT_6424();
void CombineDFT_6425();
void CombineDFT_6426();
void CombineDFT_6427();
void CombineDFT_6428();
void CombineDFT_6429();
void CombineDFT_6430();
void CombineDFT_6431();
void CombineDFT_6432();
void CombineDFT_6433();
void CombineDFT_6434();
void CombineDFT_6435();
void CombineDFT_6436();
void CombineDFT_6437();
void CombineDFT_6438();
void CombineDFT_6439();
void CombineDFT_6440();
void WEIGHTED_ROUND_ROBIN_Joiner_6421();
void WEIGHTED_ROUND_ROBIN_Splitter_6441();
void CombineDFT_6443();
void CombineDFT_6444();
void CombineDFT_6445();
void CombineDFT_6446();
void CombineDFT_6447();
void CombineDFT_6448();
void CombineDFT_6449();
void CombineDFT_6450();
void CombineDFT_6451();
void CombineDFT_6452();
void CombineDFT_6453();
void CombineDFT_6454();
void CombineDFT_6455();
void CombineDFT_6456();
void CombineDFT_6457();
void CombineDFT_6458();
void WEIGHTED_ROUND_ROBIN_Joiner_6442();
void WEIGHTED_ROUND_ROBIN_Splitter_6459();
void CombineDFT_6461();
void CombineDFT_6462();
void CombineDFT_6463();
void CombineDFT_6464();
void CombineDFT_6465();
void CombineDFT_6466();
void CombineDFT_6467();
void CombineDFT_6468();
void WEIGHTED_ROUND_ROBIN_Joiner_6460();
void WEIGHTED_ROUND_ROBIN_Splitter_6469();
void CombineDFT_6471();
void CombineDFT_6472();
void CombineDFT_6473();
void CombineDFT_6474();
void WEIGHTED_ROUND_ROBIN_Joiner_6470();
void WEIGHTED_ROUND_ROBIN_Splitter_6475();
void CombineDFT_6477();
void CombineDFT_6478();
void WEIGHTED_ROUND_ROBIN_Joiner_6476();
void CombineDFT_6357();
void FFTReorderSimple_6358();
void WEIGHTED_ROUND_ROBIN_Splitter_6479();
void FFTReorderSimple_6481();
void FFTReorderSimple_6482();
void WEIGHTED_ROUND_ROBIN_Joiner_6480();
void WEIGHTED_ROUND_ROBIN_Splitter_6483();
void FFTReorderSimple_6485();
void FFTReorderSimple_6486();
void FFTReorderSimple_6487();
void FFTReorderSimple_6488();
void WEIGHTED_ROUND_ROBIN_Joiner_6484();
void WEIGHTED_ROUND_ROBIN_Splitter_6489();
void FFTReorderSimple_6491();
void FFTReorderSimple_6492();
void FFTReorderSimple_6493();
void FFTReorderSimple_6494();
void FFTReorderSimple_6495();
void FFTReorderSimple_6496();
void FFTReorderSimple_6497();
void FFTReorderSimple_6498();
void WEIGHTED_ROUND_ROBIN_Joiner_6490();
void WEIGHTED_ROUND_ROBIN_Splitter_6499();
void FFTReorderSimple_6501();
void FFTReorderSimple_6502();
void FFTReorderSimple_6503();
void FFTReorderSimple_6504();
void FFTReorderSimple_6505();
void FFTReorderSimple_6506();
void FFTReorderSimple_6507();
void FFTReorderSimple_6508();
void FFTReorderSimple_6509();
void FFTReorderSimple_6510();
void FFTReorderSimple_6511();
void FFTReorderSimple_6512();
void FFTReorderSimple_6513();
void FFTReorderSimple_6514();
void FFTReorderSimple_6515();
void FFTReorderSimple_6516();
void WEIGHTED_ROUND_ROBIN_Joiner_6500();
void WEIGHTED_ROUND_ROBIN_Splitter_6517();
void CombineDFT_6519();
void CombineDFT_6520();
void CombineDFT_6521();
void CombineDFT_6522();
void CombineDFT_6523();
void CombineDFT_6524();
void CombineDFT_6525();
void CombineDFT_6526();
void CombineDFT_6527();
void CombineDFT_6528();
void CombineDFT_6529();
void CombineDFT_6530();
void CombineDFT_6531();
void CombineDFT_6532();
void CombineDFT_6533();
void CombineDFT_6534();
void CombineDFT_6535();
void CombineDFT_6536();
void CombineDFT_6537();
void WEIGHTED_ROUND_ROBIN_Joiner_6518();
void WEIGHTED_ROUND_ROBIN_Splitter_6538();
void CombineDFT_6540();
void CombineDFT_6541();
void CombineDFT_6542();
void CombineDFT_6543();
void CombineDFT_6544();
void CombineDFT_6545();
void CombineDFT_6546();
void CombineDFT_6547();
void CombineDFT_6548();
void CombineDFT_6549();
void CombineDFT_6550();
void CombineDFT_6551();
void CombineDFT_6552();
void CombineDFT_6553();
void CombineDFT_6554();
void CombineDFT_6555();
void WEIGHTED_ROUND_ROBIN_Joiner_6539();
void WEIGHTED_ROUND_ROBIN_Splitter_6556();
void CombineDFT_6558();
void CombineDFT_6559();
void CombineDFT_6560();
void CombineDFT_6561();
void CombineDFT_6562();
void CombineDFT_6563();
void CombineDFT_6564();
void CombineDFT_6565();
void WEIGHTED_ROUND_ROBIN_Joiner_6557();
void WEIGHTED_ROUND_ROBIN_Splitter_6566();
void CombineDFT_6568();
void CombineDFT_6569();
void CombineDFT_6570();
void CombineDFT_6571();
void WEIGHTED_ROUND_ROBIN_Joiner_6567();
void WEIGHTED_ROUND_ROBIN_Splitter_6572();
void CombineDFT_6574();
void CombineDFT_6575();
void WEIGHTED_ROUND_ROBIN_Joiner_6573();
void CombineDFT_6368();
void WEIGHTED_ROUND_ROBIN_Joiner_6371();
void FloatPrinter_6369();

#ifdef __cplusplus
}
#endif
#endif
