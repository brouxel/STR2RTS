#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=640 on the compile command line
#else
#if BUF_SIZEMAX < 640
#error BUF_SIZEMAX too small, it must be at least 640
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float A_re[32];
	float A_im[32];
	int idx;
} FloatSource_5864_t;
void FloatSource(buffer_float_t *chanout);
void FloatSource_5864();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_6165();
void WEIGHTED_ROUND_ROBIN_Splitter_6307();
void Butterfly(buffer_float_t *chanin, buffer_float_t *chanout);
void Butterfly_6309();
void Butterfly_6310();
void Butterfly_6311();
void Butterfly_6312();
void Butterfly_6313();
void Butterfly_6314();
void Butterfly_6315();
void Butterfly_6316();
void Butterfly_6317();
void Butterfly_6318();
void WEIGHTED_ROUND_ROBIN_Joiner_6308();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_6166();
void WEIGHTED_ROUND_ROBIN_Splitter_6209();
void Pre_CollapsedDataParallel_1_6168();
void WEIGHTED_ROUND_ROBIN_Splitter_6319();
void Butterfly_6321();
void Butterfly_6322();
void Butterfly_6323();
void Butterfly_6324();
void Butterfly_6325();
void Butterfly_6326();
void Butterfly_6327();
void Butterfly_6328();
void WEIGHTED_ROUND_ROBIN_Joiner_6320();
void Post_CollapsedDataParallel_2_6169();
void WEIGHTED_ROUND_ROBIN_Splitter_6289();
void Pre_CollapsedDataParallel_1_6174();
void WEIGHTED_ROUND_ROBIN_Splitter_6329();
void Butterfly_6331();
void Butterfly_6332();
void Butterfly_6333();
void Butterfly_6334();
void WEIGHTED_ROUND_ROBIN_Joiner_6330();
void Post_CollapsedDataParallel_2_6175();
void Pre_CollapsedDataParallel_1_6177();
void WEIGHTED_ROUND_ROBIN_Splitter_6335();
void Butterfly_6337();
void Butterfly_6338();
void Butterfly_6339();
void Butterfly_6340();
void WEIGHTED_ROUND_ROBIN_Joiner_6336();
void Post_CollapsedDataParallel_2_6178();
void WEIGHTED_ROUND_ROBIN_Joiner_6290();
void Pre_CollapsedDataParallel_1_6171();
void WEIGHTED_ROUND_ROBIN_Splitter_6341();
void Butterfly_6343();
void Butterfly_6344();
void Butterfly_6345();
void Butterfly_6346();
void Butterfly_6347();
void Butterfly_6348();
void Butterfly_6349();
void Butterfly_6350();
void WEIGHTED_ROUND_ROBIN_Joiner_6342();
void Post_CollapsedDataParallel_2_6172();
void WEIGHTED_ROUND_ROBIN_Splitter_6291();
void Pre_CollapsedDataParallel_1_6180();
void WEIGHTED_ROUND_ROBIN_Splitter_6351();
void Butterfly_6353();
void Butterfly_6354();
void Butterfly_6355();
void Butterfly_6356();
void WEIGHTED_ROUND_ROBIN_Joiner_6352();
void Post_CollapsedDataParallel_2_6181();
void Pre_CollapsedDataParallel_1_6183();
void WEIGHTED_ROUND_ROBIN_Splitter_6357();
void Butterfly_6359();
void Butterfly_6360();
void Butterfly_6361();
void Butterfly_6362();
void WEIGHTED_ROUND_ROBIN_Joiner_6358();
void Post_CollapsedDataParallel_2_6184();
void WEIGHTED_ROUND_ROBIN_Joiner_6292();
void WEIGHTED_ROUND_ROBIN_Joiner_6293();
void WEIGHTED_ROUND_ROBIN_Splitter_6294();
void WEIGHTED_ROUND_ROBIN_Splitter_6295();
void Pre_CollapsedDataParallel_1_6186();
void WEIGHTED_ROUND_ROBIN_Splitter_6363();
void Butterfly_6365();
void Butterfly_6366();
void WEIGHTED_ROUND_ROBIN_Joiner_6364();
void Post_CollapsedDataParallel_2_6187();
void Pre_CollapsedDataParallel_1_6189();
void WEIGHTED_ROUND_ROBIN_Splitter_6367();
void Butterfly_6369();
void Butterfly_6370();
void WEIGHTED_ROUND_ROBIN_Joiner_6368();
void Post_CollapsedDataParallel_2_6190();
void Pre_CollapsedDataParallel_1_6192();
void WEIGHTED_ROUND_ROBIN_Splitter_6371();
void Butterfly_6373();
void Butterfly_6374();
void WEIGHTED_ROUND_ROBIN_Joiner_6372();
void Post_CollapsedDataParallel_2_6193();
void Pre_CollapsedDataParallel_1_6195();
void WEIGHTED_ROUND_ROBIN_Splitter_6375();
void Butterfly_6377();
void Butterfly_6378();
void WEIGHTED_ROUND_ROBIN_Joiner_6376();
void Post_CollapsedDataParallel_2_6196();
void WEIGHTED_ROUND_ROBIN_Joiner_6296();
void WEIGHTED_ROUND_ROBIN_Splitter_6297();
void Pre_CollapsedDataParallel_1_6198();
void WEIGHTED_ROUND_ROBIN_Splitter_6379();
void Butterfly_6381();
void Butterfly_6382();
void WEIGHTED_ROUND_ROBIN_Joiner_6380();
void Post_CollapsedDataParallel_2_6199();
void Pre_CollapsedDataParallel_1_6201();
void WEIGHTED_ROUND_ROBIN_Splitter_6383();
void Butterfly_6385();
void Butterfly_6386();
void WEIGHTED_ROUND_ROBIN_Joiner_6384();
void Post_CollapsedDataParallel_2_6202();
void Pre_CollapsedDataParallel_1_6204();
void WEIGHTED_ROUND_ROBIN_Splitter_6387();
void Butterfly_6389();
void Butterfly_6390();
void WEIGHTED_ROUND_ROBIN_Joiner_6388();
void Post_CollapsedDataParallel_2_6205();
void Pre_CollapsedDataParallel_1_6207();
void WEIGHTED_ROUND_ROBIN_Splitter_6391();
void Butterfly_6393();
void Butterfly_6394();
void WEIGHTED_ROUND_ROBIN_Joiner_6392();
void Post_CollapsedDataParallel_2_6208();
void WEIGHTED_ROUND_ROBIN_Joiner_6298();
void WEIGHTED_ROUND_ROBIN_Joiner_6299();
void WEIGHTED_ROUND_ROBIN_Splitter_6300();
void WEIGHTED_ROUND_ROBIN_Splitter_6301();
void Butterfly_5929();
void Butterfly_5930();
void Butterfly_5931();
void Butterfly_5932();
void Butterfly_5933();
void Butterfly_5934();
void Butterfly_5935();
void Butterfly_5936();
void WEIGHTED_ROUND_ROBIN_Joiner_6302();
void WEIGHTED_ROUND_ROBIN_Splitter_6303();
void Butterfly_5937();
void Butterfly_5938();
void Butterfly_5939();
void Butterfly_5940();
void Butterfly_5941();
void Butterfly_5942();
void Butterfly_5943();
void Butterfly_5944();
void WEIGHTED_ROUND_ROBIN_Joiner_6304();
void WEIGHTED_ROUND_ROBIN_Joiner_6305();
int BitReverse_5945_bitrev(int inp, int numbits);
void BitReverse(buffer_float_t *chanin, buffer_float_t *chanout);
void BitReverse_5945();
void FloatPrinter(buffer_float_t *chanin);
void FloatPrinter_5946();

#ifdef __cplusplus
}
#endif
#endif
