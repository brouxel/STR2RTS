#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=14848 on the compile command line
#else
#if BUF_SIZEMAX < 14848
#error BUF_SIZEMAX too small, it must be at least 14848
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float w[2];
} CombineDFT_1732_t;

typedef struct {
	float w[4];
} CombineDFT_1763_t;

typedef struct {
	float w[8];
} CombineDFT_1781_t;

typedef struct {
	float w[16];
} CombineDFT_1791_t;

typedef struct {
	float w[32];
} CombineDFT_1797_t;

typedef struct {
	float w[64];
} CombineDFT_1667_t;
void WEIGHTED_ROUND_ROBIN_Splitter_1688();
void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout);
void FFTTestSource_1690();
void FFTTestSource_1691();
void WEIGHTED_ROUND_ROBIN_Joiner_1689();
void WEIGHTED_ROUND_ROBIN_Splitter_1680();
void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout);
void FFTReorderSimple_1657();
void WEIGHTED_ROUND_ROBIN_Splitter_1692();
void FFTReorderSimple_1694();
void FFTReorderSimple_1695();
void WEIGHTED_ROUND_ROBIN_Joiner_1693();
void WEIGHTED_ROUND_ROBIN_Splitter_1696();
void FFTReorderSimple_1698();
void FFTReorderSimple_1699();
void FFTReorderSimple_1700();
void FFTReorderSimple_1701();
void WEIGHTED_ROUND_ROBIN_Joiner_1697();
void WEIGHTED_ROUND_ROBIN_Splitter_1702();
void FFTReorderSimple_1704();
void FFTReorderSimple_1705();
void FFTReorderSimple_1706();
void FFTReorderSimple_1707();
void FFTReorderSimple_1708();
void FFTReorderSimple_1709();
void FFTReorderSimple_1710();
void FFTReorderSimple_1711();
void WEIGHTED_ROUND_ROBIN_Joiner_1703();
void WEIGHTED_ROUND_ROBIN_Splitter_1712();
void FFTReorderSimple_1714();
void FFTReorderSimple_1715();
void FFTReorderSimple_1716();
void FFTReorderSimple_1717();
void FFTReorderSimple_1718();
void FFTReorderSimple_1719();
void FFTReorderSimple_1720();
void FFTReorderSimple_1721();
void FFTReorderSimple_1722();
void FFTReorderSimple_1723();
void FFTReorderSimple_1724();
void FFTReorderSimple_1725();
void FFTReorderSimple_1726();
void FFTReorderSimple_1727();
void FFTReorderSimple_1728();
void FFTReorderSimple_1729();
void WEIGHTED_ROUND_ROBIN_Joiner_1713();
void WEIGHTED_ROUND_ROBIN_Splitter_1730();
void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout);
void CombineDFT_1732();
void CombineDFT_1733();
void CombineDFT_1734();
void CombineDFT_1735();
void CombineDFT_1736();
void CombineDFT_1737();
void CombineDFT_1738();
void CombineDFT_1739();
void CombineDFT_1740();
void CombineDFT_1741();
void CombineDFT_1742();
void CombineDFT_1743();
void CombineDFT_1744();
void CombineDFT_1745();
void CombineDFT_1746();
void CombineDFT_1747();
void CombineDFT_1748();
void CombineDFT_1749();
void CombineDFT_1750();
void CombineDFT_1751();
void CombineDFT_1752();
void CombineDFT_1753();
void CombineDFT_1754();
void CombineDFT_1755();
void CombineDFT_1756();
void CombineDFT_1757();
void CombineDFT_1758();
void CombineDFT_1759();
void CombineDFT_1760();
void WEIGHTED_ROUND_ROBIN_Joiner_1731();
void WEIGHTED_ROUND_ROBIN_Splitter_1761();
void CombineDFT_1763();
void CombineDFT_1764();
void CombineDFT_1765();
void CombineDFT_1766();
void CombineDFT_1767();
void CombineDFT_1768();
void CombineDFT_1769();
void CombineDFT_1770();
void CombineDFT_1771();
void CombineDFT_1772();
void CombineDFT_1773();
void CombineDFT_1774();
void CombineDFT_1775();
void CombineDFT_1776();
void CombineDFT_1777();
void CombineDFT_1778();
void WEIGHTED_ROUND_ROBIN_Joiner_1762();
void WEIGHTED_ROUND_ROBIN_Splitter_1779();
void CombineDFT_1781();
void CombineDFT_1782();
void CombineDFT_1783();
void CombineDFT_1784();
void CombineDFT_1785();
void CombineDFT_1786();
void CombineDFT_1787();
void CombineDFT_1788();
void WEIGHTED_ROUND_ROBIN_Joiner_1780();
void WEIGHTED_ROUND_ROBIN_Splitter_1789();
void CombineDFT_1791();
void CombineDFT_1792();
void CombineDFT_1793();
void CombineDFT_1794();
void WEIGHTED_ROUND_ROBIN_Joiner_1790();
void WEIGHTED_ROUND_ROBIN_Splitter_1795();
void CombineDFT_1797();
void CombineDFT_1798();
void WEIGHTED_ROUND_ROBIN_Joiner_1796();
void CombineDFT_1667();
void FFTReorderSimple_1668();
void WEIGHTED_ROUND_ROBIN_Splitter_1799();
void FFTReorderSimple_1801();
void FFTReorderSimple_1802();
void WEIGHTED_ROUND_ROBIN_Joiner_1800();
void WEIGHTED_ROUND_ROBIN_Splitter_1803();
void FFTReorderSimple_1805();
void FFTReorderSimple_1806();
void FFTReorderSimple_1807();
void FFTReorderSimple_1808();
void WEIGHTED_ROUND_ROBIN_Joiner_1804();
void WEIGHTED_ROUND_ROBIN_Splitter_1809();
void FFTReorderSimple_1811();
void FFTReorderSimple_1812();
void FFTReorderSimple_1813();
void FFTReorderSimple_1814();
void FFTReorderSimple_1815();
void FFTReorderSimple_1816();
void FFTReorderSimple_1817();
void FFTReorderSimple_1818();
void WEIGHTED_ROUND_ROBIN_Joiner_1810();
void WEIGHTED_ROUND_ROBIN_Splitter_1819();
void FFTReorderSimple_1821();
void FFTReorderSimple_1822();
void FFTReorderSimple_1823();
void FFTReorderSimple_1824();
void FFTReorderSimple_1825();
void FFTReorderSimple_1826();
void FFTReorderSimple_1827();
void FFTReorderSimple_1828();
void FFTReorderSimple_1829();
void FFTReorderSimple_1830();
void FFTReorderSimple_1831();
void FFTReorderSimple_1832();
void FFTReorderSimple_1833();
void FFTReorderSimple_1834();
void FFTReorderSimple_1835();
void FFTReorderSimple_1836();
void WEIGHTED_ROUND_ROBIN_Joiner_1820();
void WEIGHTED_ROUND_ROBIN_Splitter_1837();
void CombineDFT_1839();
void CombineDFT_1840();
void CombineDFT_1841();
void CombineDFT_1842();
void CombineDFT_1843();
void CombineDFT_1844();
void CombineDFT_1845();
void CombineDFT_1846();
void CombineDFT_1847();
void CombineDFT_1848();
void CombineDFT_1849();
void CombineDFT_1850();
void CombineDFT_1851();
void CombineDFT_1852();
void CombineDFT_1853();
void CombineDFT_1854();
void CombineDFT_1855();
void CombineDFT_1856();
void CombineDFT_1857();
void CombineDFT_1858();
void CombineDFT_1859();
void CombineDFT_1860();
void CombineDFT_1861();
void CombineDFT_1862();
void CombineDFT_1863();
void CombineDFT_1864();
void CombineDFT_1865();
void CombineDFT_1866();
void CombineDFT_1867();
void WEIGHTED_ROUND_ROBIN_Joiner_1838();
void WEIGHTED_ROUND_ROBIN_Splitter_1868();
void CombineDFT_1870();
void CombineDFT_1871();
void CombineDFT_1872();
void CombineDFT_1873();
void CombineDFT_1874();
void CombineDFT_1875();
void CombineDFT_1876();
void CombineDFT_1877();
void CombineDFT_1878();
void CombineDFT_1879();
void CombineDFT_1880();
void CombineDFT_1881();
void CombineDFT_1882();
void CombineDFT_1883();
void CombineDFT_1884();
void CombineDFT_1885();
void WEIGHTED_ROUND_ROBIN_Joiner_1869();
void WEIGHTED_ROUND_ROBIN_Splitter_1886();
void CombineDFT_1888();
void CombineDFT_1889();
void CombineDFT_1890();
void CombineDFT_1891();
void CombineDFT_1892();
void CombineDFT_1893();
void CombineDFT_1894();
void CombineDFT_1895();
void WEIGHTED_ROUND_ROBIN_Joiner_1887();
void WEIGHTED_ROUND_ROBIN_Splitter_1896();
void CombineDFT_1898();
void CombineDFT_1899();
void CombineDFT_1900();
void CombineDFT_1901();
void WEIGHTED_ROUND_ROBIN_Joiner_1897();
void WEIGHTED_ROUND_ROBIN_Splitter_1902();
void CombineDFT_1904();
void CombineDFT_1905();
void WEIGHTED_ROUND_ROBIN_Joiner_1903();
void CombineDFT_1678();
void WEIGHTED_ROUND_ROBIN_Joiner_1681();
void FloatPrinter(buffer_float_t *chanin);
void FloatPrinter_1679();

#ifdef __cplusplus
}
#endif
#endif
