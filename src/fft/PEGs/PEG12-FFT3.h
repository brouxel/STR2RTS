#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=384 on the compile command line
#else
#if BUF_SIZEMAX < 384
#error BUF_SIZEMAX too small, it must be at least 384
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float A_re[32];
	float A_im[32];
	int idx;
} FloatSource_4410_t;
void FloatSource(buffer_float_t *chanout);
void FloatSource_4410();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_4711();
void WEIGHTED_ROUND_ROBIN_Splitter_4853();
void Butterfly(buffer_float_t *chanin, buffer_float_t *chanout);
void Butterfly_4855();
void Butterfly_4856();
void Butterfly_4857();
void Butterfly_4858();
void Butterfly_4859();
void Butterfly_4860();
void Butterfly_4861();
void Butterfly_4862();
void Butterfly_4863();
void Butterfly_4864();
void Butterfly_4865();
void Butterfly_4866();
void WEIGHTED_ROUND_ROBIN_Joiner_4854();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_4712();
void WEIGHTED_ROUND_ROBIN_Splitter_4755();
void Pre_CollapsedDataParallel_1_4714();
void WEIGHTED_ROUND_ROBIN_Splitter_4867();
void Butterfly_4869();
void Butterfly_4870();
void Butterfly_4871();
void Butterfly_4872();
void Butterfly_4873();
void Butterfly_4874();
void Butterfly_4875();
void Butterfly_4876();
void WEIGHTED_ROUND_ROBIN_Joiner_4868();
void Post_CollapsedDataParallel_2_4715();
void WEIGHTED_ROUND_ROBIN_Splitter_4835();
void Pre_CollapsedDataParallel_1_4720();
void WEIGHTED_ROUND_ROBIN_Splitter_4877();
void Butterfly_4879();
void Butterfly_4880();
void Butterfly_4881();
void Butterfly_4882();
void WEIGHTED_ROUND_ROBIN_Joiner_4878();
void Post_CollapsedDataParallel_2_4721();
void Pre_CollapsedDataParallel_1_4723();
void WEIGHTED_ROUND_ROBIN_Splitter_4883();
void Butterfly_4885();
void Butterfly_4886();
void Butterfly_4887();
void Butterfly_4888();
void WEIGHTED_ROUND_ROBIN_Joiner_4884();
void Post_CollapsedDataParallel_2_4724();
void WEIGHTED_ROUND_ROBIN_Joiner_4836();
void Pre_CollapsedDataParallel_1_4717();
void WEIGHTED_ROUND_ROBIN_Splitter_4889();
void Butterfly_4891();
void Butterfly_4892();
void Butterfly_4893();
void Butterfly_4894();
void Butterfly_4895();
void Butterfly_4896();
void Butterfly_4897();
void Butterfly_4898();
void WEIGHTED_ROUND_ROBIN_Joiner_4890();
void Post_CollapsedDataParallel_2_4718();
void WEIGHTED_ROUND_ROBIN_Splitter_4837();
void Pre_CollapsedDataParallel_1_4726();
void WEIGHTED_ROUND_ROBIN_Splitter_4899();
void Butterfly_4901();
void Butterfly_4902();
void Butterfly_4903();
void Butterfly_4904();
void WEIGHTED_ROUND_ROBIN_Joiner_4900();
void Post_CollapsedDataParallel_2_4727();
void Pre_CollapsedDataParallel_1_4729();
void WEIGHTED_ROUND_ROBIN_Splitter_4905();
void Butterfly_4907();
void Butterfly_4908();
void Butterfly_4909();
void Butterfly_4910();
void WEIGHTED_ROUND_ROBIN_Joiner_4906();
void Post_CollapsedDataParallel_2_4730();
void WEIGHTED_ROUND_ROBIN_Joiner_4838();
void WEIGHTED_ROUND_ROBIN_Joiner_4839();
void WEIGHTED_ROUND_ROBIN_Splitter_4840();
void WEIGHTED_ROUND_ROBIN_Splitter_4841();
void Pre_CollapsedDataParallel_1_4732();
void WEIGHTED_ROUND_ROBIN_Splitter_4911();
void Butterfly_4913();
void Butterfly_4914();
void WEIGHTED_ROUND_ROBIN_Joiner_4912();
void Post_CollapsedDataParallel_2_4733();
void Pre_CollapsedDataParallel_1_4735();
void WEIGHTED_ROUND_ROBIN_Splitter_4915();
void Butterfly_4917();
void Butterfly_4918();
void WEIGHTED_ROUND_ROBIN_Joiner_4916();
void Post_CollapsedDataParallel_2_4736();
void Pre_CollapsedDataParallel_1_4738();
void WEIGHTED_ROUND_ROBIN_Splitter_4919();
void Butterfly_4921();
void Butterfly_4922();
void WEIGHTED_ROUND_ROBIN_Joiner_4920();
void Post_CollapsedDataParallel_2_4739();
void Pre_CollapsedDataParallel_1_4741();
void WEIGHTED_ROUND_ROBIN_Splitter_4923();
void Butterfly_4925();
void Butterfly_4926();
void WEIGHTED_ROUND_ROBIN_Joiner_4924();
void Post_CollapsedDataParallel_2_4742();
void WEIGHTED_ROUND_ROBIN_Joiner_4842();
void WEIGHTED_ROUND_ROBIN_Splitter_4843();
void Pre_CollapsedDataParallel_1_4744();
void WEIGHTED_ROUND_ROBIN_Splitter_4927();
void Butterfly_4929();
void Butterfly_4930();
void WEIGHTED_ROUND_ROBIN_Joiner_4928();
void Post_CollapsedDataParallel_2_4745();
void Pre_CollapsedDataParallel_1_4747();
void WEIGHTED_ROUND_ROBIN_Splitter_4931();
void Butterfly_4933();
void Butterfly_4934();
void WEIGHTED_ROUND_ROBIN_Joiner_4932();
void Post_CollapsedDataParallel_2_4748();
void Pre_CollapsedDataParallel_1_4750();
void WEIGHTED_ROUND_ROBIN_Splitter_4935();
void Butterfly_4937();
void Butterfly_4938();
void WEIGHTED_ROUND_ROBIN_Joiner_4936();
void Post_CollapsedDataParallel_2_4751();
void Pre_CollapsedDataParallel_1_4753();
void WEIGHTED_ROUND_ROBIN_Splitter_4939();
void Butterfly_4941();
void Butterfly_4942();
void WEIGHTED_ROUND_ROBIN_Joiner_4940();
void Post_CollapsedDataParallel_2_4754();
void WEIGHTED_ROUND_ROBIN_Joiner_4844();
void WEIGHTED_ROUND_ROBIN_Joiner_4845();
void WEIGHTED_ROUND_ROBIN_Splitter_4846();
void WEIGHTED_ROUND_ROBIN_Splitter_4847();
void Butterfly_4475();
void Butterfly_4476();
void Butterfly_4477();
void Butterfly_4478();
void Butterfly_4479();
void Butterfly_4480();
void Butterfly_4481();
void Butterfly_4482();
void WEIGHTED_ROUND_ROBIN_Joiner_4848();
void WEIGHTED_ROUND_ROBIN_Splitter_4849();
void Butterfly_4483();
void Butterfly_4484();
void Butterfly_4485();
void Butterfly_4486();
void Butterfly_4487();
void Butterfly_4488();
void Butterfly_4489();
void Butterfly_4490();
void WEIGHTED_ROUND_ROBIN_Joiner_4850();
void WEIGHTED_ROUND_ROBIN_Joiner_4851();
int BitReverse_4491_bitrev(int inp, int numbits);
void BitReverse(buffer_float_t *chanin, buffer_float_t *chanout);
void BitReverse_4491();
void FloatPrinter(buffer_float_t *chanin);
void FloatPrinter_4492();

#ifdef __cplusplus
}
#endif
#endif
