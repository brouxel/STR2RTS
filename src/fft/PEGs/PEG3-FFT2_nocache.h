#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=1536 on the compile command line
#else
#if BUF_SIZEMAX < 1536
#error BUF_SIZEMAX too small, it must be at least 1536
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float w[2];
} CombineDFT_12371_t;

typedef struct {
	float w[4];
} CombineDFT_12376_t;

typedef struct {
	float w[8];
} CombineDFT_12381_t;

typedef struct {
	float w[16];
} CombineDFT_12386_t;

typedef struct {
	float w[32];
} CombineDFT_12391_t;

typedef struct {
	float w[64];
} CombineDFT_12325_t;
void WEIGHTED_ROUND_ROBIN_Splitter_12346();
void FFTTestSource_12348();
void FFTTestSource_12349();
void WEIGHTED_ROUND_ROBIN_Joiner_12347();
void WEIGHTED_ROUND_ROBIN_Splitter_12338();
void FFTReorderSimple_12315();
void WEIGHTED_ROUND_ROBIN_Splitter_12350();
void FFTReorderSimple_12352();
void FFTReorderSimple_12353();
void WEIGHTED_ROUND_ROBIN_Joiner_12351();
void WEIGHTED_ROUND_ROBIN_Splitter_12354();
void FFTReorderSimple_12356();
void FFTReorderSimple_12357();
void FFTReorderSimple_12358();
void WEIGHTED_ROUND_ROBIN_Joiner_12355();
void WEIGHTED_ROUND_ROBIN_Splitter_12359();
void FFTReorderSimple_12361();
void FFTReorderSimple_12362();
void FFTReorderSimple_12363();
void WEIGHTED_ROUND_ROBIN_Joiner_12360();
void WEIGHTED_ROUND_ROBIN_Splitter_12364();
void FFTReorderSimple_12366();
void FFTReorderSimple_12367();
void FFTReorderSimple_12368();
void WEIGHTED_ROUND_ROBIN_Joiner_12365();
void WEIGHTED_ROUND_ROBIN_Splitter_12369();
void CombineDFT_12371();
void CombineDFT_12372();
void CombineDFT_12373();
void WEIGHTED_ROUND_ROBIN_Joiner_12370();
void WEIGHTED_ROUND_ROBIN_Splitter_12374();
void CombineDFT_12376();
void CombineDFT_12377();
void CombineDFT_12378();
void WEIGHTED_ROUND_ROBIN_Joiner_12375();
void WEIGHTED_ROUND_ROBIN_Splitter_12379();
void CombineDFT_12381();
void CombineDFT_12382();
void CombineDFT_12383();
void WEIGHTED_ROUND_ROBIN_Joiner_12380();
void WEIGHTED_ROUND_ROBIN_Splitter_12384();
void CombineDFT_12386();
void CombineDFT_12387();
void CombineDFT_12388();
void WEIGHTED_ROUND_ROBIN_Joiner_12385();
void WEIGHTED_ROUND_ROBIN_Splitter_12389();
void CombineDFT_12391();
void CombineDFT_12392();
void WEIGHTED_ROUND_ROBIN_Joiner_12390();
void CombineDFT_12325();
void FFTReorderSimple_12326();
void WEIGHTED_ROUND_ROBIN_Splitter_12393();
void FFTReorderSimple_12395();
void FFTReorderSimple_12396();
void WEIGHTED_ROUND_ROBIN_Joiner_12394();
void WEIGHTED_ROUND_ROBIN_Splitter_12397();
void FFTReorderSimple_12399();
void FFTReorderSimple_12400();
void FFTReorderSimple_12401();
void WEIGHTED_ROUND_ROBIN_Joiner_12398();
void WEIGHTED_ROUND_ROBIN_Splitter_12402();
void FFTReorderSimple_12404();
void FFTReorderSimple_12405();
void FFTReorderSimple_12406();
void WEIGHTED_ROUND_ROBIN_Joiner_12403();
void WEIGHTED_ROUND_ROBIN_Splitter_12407();
void FFTReorderSimple_12409();
void FFTReorderSimple_12410();
void FFTReorderSimple_12411();
void WEIGHTED_ROUND_ROBIN_Joiner_12408();
void WEIGHTED_ROUND_ROBIN_Splitter_12412();
void CombineDFT_12414();
void CombineDFT_12415();
void CombineDFT_12416();
void WEIGHTED_ROUND_ROBIN_Joiner_12413();
void WEIGHTED_ROUND_ROBIN_Splitter_12417();
void CombineDFT_12419();
void CombineDFT_12420();
void CombineDFT_12421();
void WEIGHTED_ROUND_ROBIN_Joiner_12418();
void WEIGHTED_ROUND_ROBIN_Splitter_12422();
void CombineDFT_12424();
void CombineDFT_12425();
void CombineDFT_12426();
void WEIGHTED_ROUND_ROBIN_Joiner_12423();
void WEIGHTED_ROUND_ROBIN_Splitter_12427();
void CombineDFT_12429();
void CombineDFT_12430();
void CombineDFT_12431();
void WEIGHTED_ROUND_ROBIN_Joiner_12428();
void WEIGHTED_ROUND_ROBIN_Splitter_12432();
void CombineDFT_12434();
void CombineDFT_12435();
void WEIGHTED_ROUND_ROBIN_Joiner_12433();
void CombineDFT_12336();
void WEIGHTED_ROUND_ROBIN_Joiner_12339();
void FloatPrinter_12337();

#ifdef __cplusplus
}
#endif
#endif
