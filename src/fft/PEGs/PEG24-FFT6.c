#include "PEG24-FFT6.h"

buffer_complex_t SplitJoin16_CombineDFT_Fiss_2065_2075_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2054CombineDFT_1952;
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_2059_2069_join[8];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_2065_2075_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1966WEIGHTED_ROUND_ROBIN_Splitter_1975;
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_2057_2067_join[2];
buffer_complex_t SplitJoin12_CombineDFT_Fiss_2063_2073_split[8];
buffer_complex_t FFTReorderSimple_1942WEIGHTED_ROUND_ROBIN_Splitter_1955;
buffer_complex_t FFTTestSource_1941FFTReorderSimple_1942;
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_2058_2068_split[4];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_2057_2067_split[2];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_2062_2072_split[16];
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_2060_2070_split[16];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_2062_2072_join[16];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1956WEIGHTED_ROUND_ROBIN_Splitter_1959;
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_2060_2070_join[16];
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_2058_2068_join[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1994WEIGHTED_ROUND_ROBIN_Splitter_2019;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1960WEIGHTED_ROUND_ROBIN_Splitter_1965;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_2061_2071_join[24];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_2064_2074_join[4];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_2059_2069_split[8];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2020WEIGHTED_ROUND_ROBIN_Splitter_2037;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2038WEIGHTED_ROUND_ROBIN_Splitter_2047;
buffer_complex_t SplitJoin12_CombineDFT_Fiss_2063_2073_join[8];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_2064_2074_split[4];
buffer_complex_t CombineDFT_1952CPrinter_1953;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1976WEIGHTED_ROUND_ROBIN_Splitter_1993;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2048WEIGHTED_ROUND_ROBIN_Splitter_2053;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_2061_2071_split[24];


CombineDFT_1995_t CombineDFT_1995_s;
CombineDFT_1995_t CombineDFT_1996_s;
CombineDFT_1995_t CombineDFT_1997_s;
CombineDFT_1995_t CombineDFT_1998_s;
CombineDFT_1995_t CombineDFT_1999_s;
CombineDFT_1995_t CombineDFT_2000_s;
CombineDFT_1995_t CombineDFT_2001_s;
CombineDFT_1995_t CombineDFT_2002_s;
CombineDFT_1995_t CombineDFT_2003_s;
CombineDFT_1995_t CombineDFT_2004_s;
CombineDFT_1995_t CombineDFT_2005_s;
CombineDFT_1995_t CombineDFT_2006_s;
CombineDFT_1995_t CombineDFT_2007_s;
CombineDFT_1995_t CombineDFT_2008_s;
CombineDFT_1995_t CombineDFT_2009_s;
CombineDFT_1995_t CombineDFT_2010_s;
CombineDFT_1995_t CombineDFT_2011_s;
CombineDFT_1995_t CombineDFT_2012_s;
CombineDFT_1995_t CombineDFT_2013_s;
CombineDFT_1995_t CombineDFT_2014_s;
CombineDFT_1995_t CombineDFT_2015_s;
CombineDFT_1995_t CombineDFT_2016_s;
CombineDFT_1995_t CombineDFT_2017_s;
CombineDFT_1995_t CombineDFT_2018_s;
CombineDFT_1995_t CombineDFT_2021_s;
CombineDFT_1995_t CombineDFT_2022_s;
CombineDFT_1995_t CombineDFT_2023_s;
CombineDFT_1995_t CombineDFT_2024_s;
CombineDFT_1995_t CombineDFT_2025_s;
CombineDFT_1995_t CombineDFT_2026_s;
CombineDFT_1995_t CombineDFT_2027_s;
CombineDFT_1995_t CombineDFT_2028_s;
CombineDFT_1995_t CombineDFT_2029_s;
CombineDFT_1995_t CombineDFT_2030_s;
CombineDFT_1995_t CombineDFT_2031_s;
CombineDFT_1995_t CombineDFT_2032_s;
CombineDFT_1995_t CombineDFT_2033_s;
CombineDFT_1995_t CombineDFT_2034_s;
CombineDFT_1995_t CombineDFT_2035_s;
CombineDFT_1995_t CombineDFT_2036_s;
CombineDFT_1995_t CombineDFT_2039_s;
CombineDFT_1995_t CombineDFT_2040_s;
CombineDFT_1995_t CombineDFT_2041_s;
CombineDFT_1995_t CombineDFT_2042_s;
CombineDFT_1995_t CombineDFT_2043_s;
CombineDFT_1995_t CombineDFT_2044_s;
CombineDFT_1995_t CombineDFT_2045_s;
CombineDFT_1995_t CombineDFT_2046_s;
CombineDFT_1995_t CombineDFT_2049_s;
CombineDFT_1995_t CombineDFT_2050_s;
CombineDFT_1995_t CombineDFT_2051_s;
CombineDFT_1995_t CombineDFT_2052_s;
CombineDFT_1995_t CombineDFT_2055_s;
CombineDFT_1995_t CombineDFT_2056_s;
CombineDFT_1995_t CombineDFT_1952_s;

void FFTTestSource(buffer_complex_t *chanout) {
		complex_t c1;
		complex_t zero;
		c1.real = 1.0 ; 
		c1.imag = 0.0 ; 
		zero.real = 0.0 ; 
		zero.imag = 0.0 ; 
		push_complex(&(*chanout), zero) ; 
		push_complex(&(*chanout), c1) ; 
		FOR(int, i, 0,  < , 62, i++) {
			push_complex(&(*chanout), zero) ; 
		}
		ENDFOR
	}


void FFTTestSource_1941() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTTestSource(&(FFTTestSource_1941FFTReorderSimple_1942));
	ENDFOR
}

void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 64, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&(*chanin)) ; 
		}
		ENDFOR
	}


void FFTReorderSimple_1942() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(FFTTestSource_1941FFTReorderSimple_1942), &(FFTReorderSimple_1942WEIGHTED_ROUND_ROBIN_Splitter_1955));
	ENDFOR
}

void FFTReorderSimple_1957() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_2057_2067_split[0]), &(SplitJoin0_FFTReorderSimple_Fiss_2057_2067_join[0]));
	ENDFOR
}

void FFTReorderSimple_1958() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_2057_2067_split[1]), &(SplitJoin0_FFTReorderSimple_Fiss_2057_2067_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1955() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_2057_2067_split[0], pop_complex(&FFTReorderSimple_1942WEIGHTED_ROUND_ROBIN_Splitter_1955));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_2057_2067_split[1], pop_complex(&FFTReorderSimple_1942WEIGHTED_ROUND_ROBIN_Splitter_1955));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1956() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1956WEIGHTED_ROUND_ROBIN_Splitter_1959, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_2057_2067_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1956WEIGHTED_ROUND_ROBIN_Splitter_1959, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_2057_2067_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_1961() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_2058_2068_split[0]), &(SplitJoin2_FFTReorderSimple_Fiss_2058_2068_join[0]));
	ENDFOR
}

void FFTReorderSimple_1962() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_2058_2068_split[1]), &(SplitJoin2_FFTReorderSimple_Fiss_2058_2068_join[1]));
	ENDFOR
}

void FFTReorderSimple_1963() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_2058_2068_split[2]), &(SplitJoin2_FFTReorderSimple_Fiss_2058_2068_join[2]));
	ENDFOR
}

void FFTReorderSimple_1964() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_2058_2068_split[3]), &(SplitJoin2_FFTReorderSimple_Fiss_2058_2068_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1959() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin2_FFTReorderSimple_Fiss_2058_2068_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1956WEIGHTED_ROUND_ROBIN_Splitter_1959));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1960() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1960WEIGHTED_ROUND_ROBIN_Splitter_1965, pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_2058_2068_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_1967() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_2059_2069_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_2059_2069_join[0]));
	ENDFOR
}

void FFTReorderSimple_1968() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_2059_2069_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_2059_2069_join[1]));
	ENDFOR
}

void FFTReorderSimple_1969() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_2059_2069_split[2]), &(SplitJoin4_FFTReorderSimple_Fiss_2059_2069_join[2]));
	ENDFOR
}

void FFTReorderSimple_1970() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_2059_2069_split[3]), &(SplitJoin4_FFTReorderSimple_Fiss_2059_2069_join[3]));
	ENDFOR
}

void FFTReorderSimple_1971() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_2059_2069_split[4]), &(SplitJoin4_FFTReorderSimple_Fiss_2059_2069_join[4]));
	ENDFOR
}

void FFTReorderSimple_1972() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_2059_2069_split[5]), &(SplitJoin4_FFTReorderSimple_Fiss_2059_2069_join[5]));
	ENDFOR
}

void FFTReorderSimple_1973() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_2059_2069_split[6]), &(SplitJoin4_FFTReorderSimple_Fiss_2059_2069_join[6]));
	ENDFOR
}

void FFTReorderSimple_1974() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_2059_2069_split[7]), &(SplitJoin4_FFTReorderSimple_Fiss_2059_2069_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1965() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2059_2069_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1960WEIGHTED_ROUND_ROBIN_Splitter_1965));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1966() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1966WEIGHTED_ROUND_ROBIN_Splitter_1975, pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_2059_2069_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_1977() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_join[0]));
	ENDFOR
}

void FFTReorderSimple_1978() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_join[1]));
	ENDFOR
}

void FFTReorderSimple_1979() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_join[2]));
	ENDFOR
}

void FFTReorderSimple_1980() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_join[3]));
	ENDFOR
}

void FFTReorderSimple_1981() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_split[4]), &(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_join[4]));
	ENDFOR
}

void FFTReorderSimple_1982() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_split[5]), &(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_join[5]));
	ENDFOR
}

void FFTReorderSimple_1983() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_split[6]), &(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_join[6]));
	ENDFOR
}

void FFTReorderSimple_1984() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_split[7]), &(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_join[7]));
	ENDFOR
}

void FFTReorderSimple_1985() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_split[8]), &(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_join[8]));
	ENDFOR
}

void FFTReorderSimple_1986() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_split[9]), &(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_join[9]));
	ENDFOR
}

void FFTReorderSimple_1987() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_split[10]), &(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_join[10]));
	ENDFOR
}

void FFTReorderSimple_1988() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_split[11]), &(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_join[11]));
	ENDFOR
}

void FFTReorderSimple_1989() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_split[12]), &(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_join[12]));
	ENDFOR
}

void FFTReorderSimple_1990() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_split[13]), &(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_join[13]));
	ENDFOR
}

void FFTReorderSimple_1991() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_split[14]), &(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_join[14]));
	ENDFOR
}

void FFTReorderSimple_1992() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_split[15]), &(SplitJoin6_FFTReorderSimple_Fiss_2060_2070_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1975() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2060_2070_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1966WEIGHTED_ROUND_ROBIN_Splitter_1975));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1976() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1976WEIGHTED_ROUND_ROBIN_Splitter_1993, pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2060_2070_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&(*chanin), (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1995_s.wn.real) - (w.imag * CombineDFT_1995_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1995_s.wn.imag) + (w.imag * CombineDFT_1995_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&(*chanin)) ; 
			push_complex(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_1995() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2061_2071_split[0]), &(SplitJoin8_CombineDFT_Fiss_2061_2071_join[0]));
	ENDFOR
}

void CombineDFT_1996() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2061_2071_split[1]), &(SplitJoin8_CombineDFT_Fiss_2061_2071_join[1]));
	ENDFOR
}

void CombineDFT_1997() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2061_2071_split[2]), &(SplitJoin8_CombineDFT_Fiss_2061_2071_join[2]));
	ENDFOR
}

void CombineDFT_1998() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2061_2071_split[3]), &(SplitJoin8_CombineDFT_Fiss_2061_2071_join[3]));
	ENDFOR
}

void CombineDFT_1999() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2061_2071_split[4]), &(SplitJoin8_CombineDFT_Fiss_2061_2071_join[4]));
	ENDFOR
}

void CombineDFT_2000() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2061_2071_split[5]), &(SplitJoin8_CombineDFT_Fiss_2061_2071_join[5]));
	ENDFOR
}

void CombineDFT_2001() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2061_2071_split[6]), &(SplitJoin8_CombineDFT_Fiss_2061_2071_join[6]));
	ENDFOR
}

void CombineDFT_2002() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2061_2071_split[7]), &(SplitJoin8_CombineDFT_Fiss_2061_2071_join[7]));
	ENDFOR
}

void CombineDFT_2003() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2061_2071_split[8]), &(SplitJoin8_CombineDFT_Fiss_2061_2071_join[8]));
	ENDFOR
}

void CombineDFT_2004() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2061_2071_split[9]), &(SplitJoin8_CombineDFT_Fiss_2061_2071_join[9]));
	ENDFOR
}

void CombineDFT_2005() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2061_2071_split[10]), &(SplitJoin8_CombineDFT_Fiss_2061_2071_join[10]));
	ENDFOR
}

void CombineDFT_2006() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2061_2071_split[11]), &(SplitJoin8_CombineDFT_Fiss_2061_2071_join[11]));
	ENDFOR
}

void CombineDFT_2007() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2061_2071_split[12]), &(SplitJoin8_CombineDFT_Fiss_2061_2071_join[12]));
	ENDFOR
}

void CombineDFT_2008() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2061_2071_split[13]), &(SplitJoin8_CombineDFT_Fiss_2061_2071_join[13]));
	ENDFOR
}

void CombineDFT_2009() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2061_2071_split[14]), &(SplitJoin8_CombineDFT_Fiss_2061_2071_join[14]));
	ENDFOR
}

void CombineDFT_2010() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2061_2071_split[15]), &(SplitJoin8_CombineDFT_Fiss_2061_2071_join[15]));
	ENDFOR
}

void CombineDFT_2011() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2061_2071_split[16]), &(SplitJoin8_CombineDFT_Fiss_2061_2071_join[16]));
	ENDFOR
}

void CombineDFT_2012() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2061_2071_split[17]), &(SplitJoin8_CombineDFT_Fiss_2061_2071_join[17]));
	ENDFOR
}

void CombineDFT_2013() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2061_2071_split[18]), &(SplitJoin8_CombineDFT_Fiss_2061_2071_join[18]));
	ENDFOR
}

void CombineDFT_2014() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2061_2071_split[19]), &(SplitJoin8_CombineDFT_Fiss_2061_2071_join[19]));
	ENDFOR
}

void CombineDFT_2015() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2061_2071_split[20]), &(SplitJoin8_CombineDFT_Fiss_2061_2071_join[20]));
	ENDFOR
}

void CombineDFT_2016() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2061_2071_split[21]), &(SplitJoin8_CombineDFT_Fiss_2061_2071_join[21]));
	ENDFOR
}

void CombineDFT_2017() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2061_2071_split[22]), &(SplitJoin8_CombineDFT_Fiss_2061_2071_join[22]));
	ENDFOR
}

void CombineDFT_2018() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2061_2071_split[23]), &(SplitJoin8_CombineDFT_Fiss_2061_2071_join[23]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1993() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 24, __iter_++)
			push_complex(&SplitJoin8_CombineDFT_Fiss_2061_2071_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1976WEIGHTED_ROUND_ROBIN_Splitter_1993));
			push_complex(&SplitJoin8_CombineDFT_Fiss_2061_2071_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1976WEIGHTED_ROUND_ROBIN_Splitter_1993));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1994() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 24, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1994WEIGHTED_ROUND_ROBIN_Splitter_2019, pop_complex(&SplitJoin8_CombineDFT_Fiss_2061_2071_join[__iter_]));
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1994WEIGHTED_ROUND_ROBIN_Splitter_2019, pop_complex(&SplitJoin8_CombineDFT_Fiss_2061_2071_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_2021() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2062_2072_split[0]), &(SplitJoin10_CombineDFT_Fiss_2062_2072_join[0]));
	ENDFOR
}

void CombineDFT_2022() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2062_2072_split[1]), &(SplitJoin10_CombineDFT_Fiss_2062_2072_join[1]));
	ENDFOR
}

void CombineDFT_2023() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2062_2072_split[2]), &(SplitJoin10_CombineDFT_Fiss_2062_2072_join[2]));
	ENDFOR
}

void CombineDFT_2024() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2062_2072_split[3]), &(SplitJoin10_CombineDFT_Fiss_2062_2072_join[3]));
	ENDFOR
}

void CombineDFT_2025() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2062_2072_split[4]), &(SplitJoin10_CombineDFT_Fiss_2062_2072_join[4]));
	ENDFOR
}

void CombineDFT_2026() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2062_2072_split[5]), &(SplitJoin10_CombineDFT_Fiss_2062_2072_join[5]));
	ENDFOR
}

void CombineDFT_2027() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2062_2072_split[6]), &(SplitJoin10_CombineDFT_Fiss_2062_2072_join[6]));
	ENDFOR
}

void CombineDFT_2028() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2062_2072_split[7]), &(SplitJoin10_CombineDFT_Fiss_2062_2072_join[7]));
	ENDFOR
}

void CombineDFT_2029() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2062_2072_split[8]), &(SplitJoin10_CombineDFT_Fiss_2062_2072_join[8]));
	ENDFOR
}

void CombineDFT_2030() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2062_2072_split[9]), &(SplitJoin10_CombineDFT_Fiss_2062_2072_join[9]));
	ENDFOR
}

void CombineDFT_2031() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2062_2072_split[10]), &(SplitJoin10_CombineDFT_Fiss_2062_2072_join[10]));
	ENDFOR
}

void CombineDFT_2032() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2062_2072_split[11]), &(SplitJoin10_CombineDFT_Fiss_2062_2072_join[11]));
	ENDFOR
}

void CombineDFT_2033() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2062_2072_split[12]), &(SplitJoin10_CombineDFT_Fiss_2062_2072_join[12]));
	ENDFOR
}

void CombineDFT_2034() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2062_2072_split[13]), &(SplitJoin10_CombineDFT_Fiss_2062_2072_join[13]));
	ENDFOR
}

void CombineDFT_2035() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2062_2072_split[14]), &(SplitJoin10_CombineDFT_Fiss_2062_2072_join[14]));
	ENDFOR
}

void CombineDFT_2036() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2062_2072_split[15]), &(SplitJoin10_CombineDFT_Fiss_2062_2072_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2019() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin10_CombineDFT_Fiss_2062_2072_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1994WEIGHTED_ROUND_ROBIN_Splitter_2019));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2020() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2020WEIGHTED_ROUND_ROBIN_Splitter_2037, pop_complex(&SplitJoin10_CombineDFT_Fiss_2062_2072_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2039() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2063_2073_split[0]), &(SplitJoin12_CombineDFT_Fiss_2063_2073_join[0]));
	ENDFOR
}

void CombineDFT_2040() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2063_2073_split[1]), &(SplitJoin12_CombineDFT_Fiss_2063_2073_join[1]));
	ENDFOR
}

void CombineDFT_2041() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2063_2073_split[2]), &(SplitJoin12_CombineDFT_Fiss_2063_2073_join[2]));
	ENDFOR
}

void CombineDFT_2042() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2063_2073_split[3]), &(SplitJoin12_CombineDFT_Fiss_2063_2073_join[3]));
	ENDFOR
}

void CombineDFT_2043() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2063_2073_split[4]), &(SplitJoin12_CombineDFT_Fiss_2063_2073_join[4]));
	ENDFOR
}

void CombineDFT_2044() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2063_2073_split[5]), &(SplitJoin12_CombineDFT_Fiss_2063_2073_join[5]));
	ENDFOR
}

void CombineDFT_2045() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2063_2073_split[6]), &(SplitJoin12_CombineDFT_Fiss_2063_2073_join[6]));
	ENDFOR
}

void CombineDFT_2046() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2063_2073_split[7]), &(SplitJoin12_CombineDFT_Fiss_2063_2073_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2037() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin12_CombineDFT_Fiss_2063_2073_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2020WEIGHTED_ROUND_ROBIN_Splitter_2037));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2038() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2038WEIGHTED_ROUND_ROBIN_Splitter_2047, pop_complex(&SplitJoin12_CombineDFT_Fiss_2063_2073_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2049() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2064_2074_split[0]), &(SplitJoin14_CombineDFT_Fiss_2064_2074_join[0]));
	ENDFOR
}

void CombineDFT_2050() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2064_2074_split[1]), &(SplitJoin14_CombineDFT_Fiss_2064_2074_join[1]));
	ENDFOR
}

void CombineDFT_2051() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2064_2074_split[2]), &(SplitJoin14_CombineDFT_Fiss_2064_2074_join[2]));
	ENDFOR
}

void CombineDFT_2052() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2064_2074_split[3]), &(SplitJoin14_CombineDFT_Fiss_2064_2074_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2047() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin14_CombineDFT_Fiss_2064_2074_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2038WEIGHTED_ROUND_ROBIN_Splitter_2047));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2048() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2048WEIGHTED_ROUND_ROBIN_Splitter_2053, pop_complex(&SplitJoin14_CombineDFT_Fiss_2064_2074_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2055() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_2065_2075_split[0]), &(SplitJoin16_CombineDFT_Fiss_2065_2075_join[0]));
	ENDFOR
}

void CombineDFT_2056() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_2065_2075_split[1]), &(SplitJoin16_CombineDFT_Fiss_2065_2075_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2053() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_2065_2075_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2048WEIGHTED_ROUND_ROBIN_Splitter_2053));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_2065_2075_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2048WEIGHTED_ROUND_ROBIN_Splitter_2053));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2054() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2054CombineDFT_1952, pop_complex(&SplitJoin16_CombineDFT_Fiss_2065_2075_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2054CombineDFT_1952, pop_complex(&SplitJoin16_CombineDFT_Fiss_2065_2075_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_1952() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_2054CombineDFT_1952), &(CombineDFT_1952CPrinter_1953));
	ENDFOR
}

void CPrinter(buffer_complex_t *chanin) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		printf("%.10f", c.real);
		printf("\n");
		printf("%.10f", c.imag);
		printf("\n");
	}


void CPrinter_1953() {
	FOR(uint32_t, __iter_steady_, 0, <, 192, __iter_steady_++)
		CPrinter(&(CombineDFT_1952CPrinter_1953));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_2065_2075_split[__iter_init_0_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2054CombineDFT_1952);
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_2059_2069_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_2065_2075_join[__iter_init_2_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1966WEIGHTED_ROUND_ROBIN_Splitter_1975);
	FOR(int, __iter_init_3_, 0, <, 2, __iter_init_3_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_2057_2067_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_2063_2073_split[__iter_init_4_]);
	ENDFOR
	init_buffer_complex(&FFTReorderSimple_1942WEIGHTED_ROUND_ROBIN_Splitter_1955);
	init_buffer_complex(&FFTTestSource_1941FFTReorderSimple_1942);
	FOR(int, __iter_init_5_, 0, <, 4, __iter_init_5_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_2058_2068_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 2, __iter_init_6_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_2057_2067_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 16, __iter_init_7_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_2062_2072_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 16, __iter_init_8_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_2060_2070_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 16, __iter_init_9_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_2062_2072_join[__iter_init_9_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1956WEIGHTED_ROUND_ROBIN_Splitter_1959);
	FOR(int, __iter_init_10_, 0, <, 16, __iter_init_10_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_2060_2070_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 4, __iter_init_11_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_2058_2068_join[__iter_init_11_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1994WEIGHTED_ROUND_ROBIN_Splitter_2019);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1960WEIGHTED_ROUND_ROBIN_Splitter_1965);
	FOR(int, __iter_init_12_, 0, <, 24, __iter_init_12_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_2061_2071_join[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 4, __iter_init_13_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_2064_2074_join[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 8, __iter_init_14_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_2059_2069_split[__iter_init_14_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2020WEIGHTED_ROUND_ROBIN_Splitter_2037);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2038WEIGHTED_ROUND_ROBIN_Splitter_2047);
	FOR(int, __iter_init_15_, 0, <, 8, __iter_init_15_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_2063_2073_join[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 4, __iter_init_16_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_2064_2074_split[__iter_init_16_]);
	ENDFOR
	init_buffer_complex(&CombineDFT_1952CPrinter_1953);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1976WEIGHTED_ROUND_ROBIN_Splitter_1993);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2048WEIGHTED_ROUND_ROBIN_Splitter_2053);
	FOR(int, __iter_init_17_, 0, <, 24, __iter_init_17_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_2061_2071_split[__iter_init_17_]);
	ENDFOR
// --- init: CombineDFT_1995
	 {
	 ; 
	CombineDFT_1995_s.wn.real = -1.0 ; 
	CombineDFT_1995_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1996
	 {
	CombineDFT_1996_s.wn.real = -1.0 ; 
	CombineDFT_1996_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1997
	 {
	CombineDFT_1997_s.wn.real = -1.0 ; 
	CombineDFT_1997_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1998
	 {
	CombineDFT_1998_s.wn.real = -1.0 ; 
	CombineDFT_1998_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1999
	 {
	CombineDFT_1999_s.wn.real = -1.0 ; 
	CombineDFT_1999_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2000
	 {
	CombineDFT_2000_s.wn.real = -1.0 ; 
	CombineDFT_2000_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2001
	 {
	CombineDFT_2001_s.wn.real = -1.0 ; 
	CombineDFT_2001_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2002
	 {
	CombineDFT_2002_s.wn.real = -1.0 ; 
	CombineDFT_2002_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2003
	 {
	CombineDFT_2003_s.wn.real = -1.0 ; 
	CombineDFT_2003_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2004
	 {
	CombineDFT_2004_s.wn.real = -1.0 ; 
	CombineDFT_2004_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2005
	 {
	CombineDFT_2005_s.wn.real = -1.0 ; 
	CombineDFT_2005_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2006
	 {
	CombineDFT_2006_s.wn.real = -1.0 ; 
	CombineDFT_2006_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2007
	 {
	CombineDFT_2007_s.wn.real = -1.0 ; 
	CombineDFT_2007_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2008
	 {
	CombineDFT_2008_s.wn.real = -1.0 ; 
	CombineDFT_2008_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2009
	 {
	CombineDFT_2009_s.wn.real = -1.0 ; 
	CombineDFT_2009_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2010
	 {
	CombineDFT_2010_s.wn.real = -1.0 ; 
	CombineDFT_2010_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2011
	 {
	CombineDFT_2011_s.wn.real = -1.0 ; 
	CombineDFT_2011_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2012
	 {
	CombineDFT_2012_s.wn.real = -1.0 ; 
	CombineDFT_2012_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2013
	 {
	CombineDFT_2013_s.wn.real = -1.0 ; 
	CombineDFT_2013_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2014
	 {
	CombineDFT_2014_s.wn.real = -1.0 ; 
	CombineDFT_2014_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2015
	 {
	CombineDFT_2015_s.wn.real = -1.0 ; 
	CombineDFT_2015_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2016
	 {
	CombineDFT_2016_s.wn.real = -1.0 ; 
	CombineDFT_2016_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2017
	 {
	CombineDFT_2017_s.wn.real = -1.0 ; 
	CombineDFT_2017_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2018
	 {
	CombineDFT_2018_s.wn.real = -1.0 ; 
	CombineDFT_2018_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2021
	 {
	CombineDFT_2021_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2021_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2022
	 {
	CombineDFT_2022_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2022_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2023
	 {
	CombineDFT_2023_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2023_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2024
	 {
	CombineDFT_2024_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2024_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2025
	 {
	CombineDFT_2025_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2025_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2026
	 {
	CombineDFT_2026_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2026_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2027
	 {
	CombineDFT_2027_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2027_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2028
	 {
	CombineDFT_2028_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2028_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2029
	 {
	CombineDFT_2029_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2029_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2030
	 {
	CombineDFT_2030_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2030_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2031
	 {
	CombineDFT_2031_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2031_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2032
	 {
	CombineDFT_2032_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2032_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2033
	 {
	CombineDFT_2033_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2033_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2034
	 {
	CombineDFT_2034_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2034_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2035
	 {
	CombineDFT_2035_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2035_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2036
	 {
	CombineDFT_2036_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2036_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2039
	 {
	CombineDFT_2039_s.wn.real = 0.70710677 ; 
	CombineDFT_2039_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2040
	 {
	CombineDFT_2040_s.wn.real = 0.70710677 ; 
	CombineDFT_2040_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2041
	 {
	CombineDFT_2041_s.wn.real = 0.70710677 ; 
	CombineDFT_2041_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2042
	 {
	CombineDFT_2042_s.wn.real = 0.70710677 ; 
	CombineDFT_2042_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2043
	 {
	CombineDFT_2043_s.wn.real = 0.70710677 ; 
	CombineDFT_2043_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2044
	 {
	CombineDFT_2044_s.wn.real = 0.70710677 ; 
	CombineDFT_2044_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2045
	 {
	CombineDFT_2045_s.wn.real = 0.70710677 ; 
	CombineDFT_2045_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2046
	 {
	CombineDFT_2046_s.wn.real = 0.70710677 ; 
	CombineDFT_2046_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2049
	 {
	CombineDFT_2049_s.wn.real = 0.9238795 ; 
	CombineDFT_2049_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_2050
	 {
	CombineDFT_2050_s.wn.real = 0.9238795 ; 
	CombineDFT_2050_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_2051
	 {
	CombineDFT_2051_s.wn.real = 0.9238795 ; 
	CombineDFT_2051_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_2052
	 {
	CombineDFT_2052_s.wn.real = 0.9238795 ; 
	CombineDFT_2052_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_2055
	 {
	CombineDFT_2055_s.wn.real = 0.98078525 ; 
	CombineDFT_2055_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_2056
	 {
	CombineDFT_2056_s.wn.real = 0.98078525 ; 
	CombineDFT_2056_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_1952
	 {
	 ; 
	CombineDFT_1952_s.wn.real = 0.9951847 ; 
	CombineDFT_1952_s.wn.imag = -0.09801714 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FFTTestSource_1941();
		FFTReorderSimple_1942();
		WEIGHTED_ROUND_ROBIN_Splitter_1955();
			FFTReorderSimple_1957();
			FFTReorderSimple_1958();
		WEIGHTED_ROUND_ROBIN_Joiner_1956();
		WEIGHTED_ROUND_ROBIN_Splitter_1959();
			FFTReorderSimple_1961();
			FFTReorderSimple_1962();
			FFTReorderSimple_1963();
			FFTReorderSimple_1964();
		WEIGHTED_ROUND_ROBIN_Joiner_1960();
		WEIGHTED_ROUND_ROBIN_Splitter_1965();
			FFTReorderSimple_1967();
			FFTReorderSimple_1968();
			FFTReorderSimple_1969();
			FFTReorderSimple_1970();
			FFTReorderSimple_1971();
			FFTReorderSimple_1972();
			FFTReorderSimple_1973();
			FFTReorderSimple_1974();
		WEIGHTED_ROUND_ROBIN_Joiner_1966();
		WEIGHTED_ROUND_ROBIN_Splitter_1975();
			FFTReorderSimple_1977();
			FFTReorderSimple_1978();
			FFTReorderSimple_1979();
			FFTReorderSimple_1980();
			FFTReorderSimple_1981();
			FFTReorderSimple_1982();
			FFTReorderSimple_1983();
			FFTReorderSimple_1984();
			FFTReorderSimple_1985();
			FFTReorderSimple_1986();
			FFTReorderSimple_1987();
			FFTReorderSimple_1988();
			FFTReorderSimple_1989();
			FFTReorderSimple_1990();
			FFTReorderSimple_1991();
			FFTReorderSimple_1992();
		WEIGHTED_ROUND_ROBIN_Joiner_1976();
		WEIGHTED_ROUND_ROBIN_Splitter_1993();
			CombineDFT_1995();
			CombineDFT_1996();
			CombineDFT_1997();
			CombineDFT_1998();
			CombineDFT_1999();
			CombineDFT_2000();
			CombineDFT_2001();
			CombineDFT_2002();
			CombineDFT_2003();
			CombineDFT_2004();
			CombineDFT_2005();
			CombineDFT_2006();
			CombineDFT_2007();
			CombineDFT_2008();
			CombineDFT_2009();
			CombineDFT_2010();
			CombineDFT_2011();
			CombineDFT_2012();
			CombineDFT_2013();
			CombineDFT_2014();
			CombineDFT_2015();
			CombineDFT_2016();
			CombineDFT_2017();
			CombineDFT_2018();
		WEIGHTED_ROUND_ROBIN_Joiner_1994();
		WEIGHTED_ROUND_ROBIN_Splitter_2019();
			CombineDFT_2021();
			CombineDFT_2022();
			CombineDFT_2023();
			CombineDFT_2024();
			CombineDFT_2025();
			CombineDFT_2026();
			CombineDFT_2027();
			CombineDFT_2028();
			CombineDFT_2029();
			CombineDFT_2030();
			CombineDFT_2031();
			CombineDFT_2032();
			CombineDFT_2033();
			CombineDFT_2034();
			CombineDFT_2035();
			CombineDFT_2036();
		WEIGHTED_ROUND_ROBIN_Joiner_2020();
		WEIGHTED_ROUND_ROBIN_Splitter_2037();
			CombineDFT_2039();
			CombineDFT_2040();
			CombineDFT_2041();
			CombineDFT_2042();
			CombineDFT_2043();
			CombineDFT_2044();
			CombineDFT_2045();
			CombineDFT_2046();
		WEIGHTED_ROUND_ROBIN_Joiner_2038();
		WEIGHTED_ROUND_ROBIN_Splitter_2047();
			CombineDFT_2049();
			CombineDFT_2050();
			CombineDFT_2051();
			CombineDFT_2052();
		WEIGHTED_ROUND_ROBIN_Joiner_2048();
		WEIGHTED_ROUND_ROBIN_Splitter_2053();
			CombineDFT_2055();
			CombineDFT_2056();
		WEIGHTED_ROUND_ROBIN_Joiner_2054();
		CombineDFT_1952();
		CPrinter_1953();
	ENDFOR
	return EXIT_SUCCESS;
}
