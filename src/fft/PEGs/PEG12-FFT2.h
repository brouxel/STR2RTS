#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=1536 on the compile command line
#else
#if BUF_SIZEMAX < 1536
#error BUF_SIZEMAX too small, it must be at least 1536
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float w[2];
} CombineDFT_9415_t;

typedef struct {
	float w[4];
} CombineDFT_9429_t;

typedef struct {
	float w[8];
} CombineDFT_9443_t;

typedef struct {
	float w[16];
} CombineDFT_9453_t;

typedef struct {
	float w[32];
} CombineDFT_9459_t;

typedef struct {
	float w[64];
} CombineDFT_9354_t;
void WEIGHTED_ROUND_ROBIN_Splitter_9375();
void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout);
void FFTTestSource_9377();
void FFTTestSource_9378();
void WEIGHTED_ROUND_ROBIN_Joiner_9376();
void WEIGHTED_ROUND_ROBIN_Splitter_9367();
void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout);
void FFTReorderSimple_9344();
void WEIGHTED_ROUND_ROBIN_Splitter_9379();
void FFTReorderSimple_9381();
void FFTReorderSimple_9382();
void WEIGHTED_ROUND_ROBIN_Joiner_9380();
void WEIGHTED_ROUND_ROBIN_Splitter_9383();
void FFTReorderSimple_9385();
void FFTReorderSimple_9386();
void FFTReorderSimple_9387();
void FFTReorderSimple_9388();
void WEIGHTED_ROUND_ROBIN_Joiner_9384();
void WEIGHTED_ROUND_ROBIN_Splitter_9389();
void FFTReorderSimple_9391();
void FFTReorderSimple_9392();
void FFTReorderSimple_9393();
void FFTReorderSimple_9394();
void FFTReorderSimple_9395();
void FFTReorderSimple_9396();
void FFTReorderSimple_9397();
void FFTReorderSimple_9398();
void WEIGHTED_ROUND_ROBIN_Joiner_9390();
void WEIGHTED_ROUND_ROBIN_Splitter_9399();
void FFTReorderSimple_9401();
void FFTReorderSimple_9402();
void FFTReorderSimple_9403();
void FFTReorderSimple_9404();
void FFTReorderSimple_9405();
void FFTReorderSimple_9406();
void FFTReorderSimple_9407();
void FFTReorderSimple_9408();
void FFTReorderSimple_9409();
void FFTReorderSimple_9410();
void FFTReorderSimple_9411();
void FFTReorderSimple_9412();
void WEIGHTED_ROUND_ROBIN_Joiner_9400();
void WEIGHTED_ROUND_ROBIN_Splitter_9413();
void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout);
void CombineDFT_9415();
void CombineDFT_9416();
void CombineDFT_9417();
void CombineDFT_9418();
void CombineDFT_9419();
void CombineDFT_9420();
void CombineDFT_9421();
void CombineDFT_9422();
void CombineDFT_9423();
void CombineDFT_9424();
void CombineDFT_9425();
void CombineDFT_9426();
void WEIGHTED_ROUND_ROBIN_Joiner_9414();
void WEIGHTED_ROUND_ROBIN_Splitter_9427();
void CombineDFT_9429();
void CombineDFT_9430();
void CombineDFT_9431();
void CombineDFT_9432();
void CombineDFT_9433();
void CombineDFT_9434();
void CombineDFT_9435();
void CombineDFT_9436();
void CombineDFT_9437();
void CombineDFT_9438();
void CombineDFT_9439();
void CombineDFT_9440();
void WEIGHTED_ROUND_ROBIN_Joiner_9428();
void WEIGHTED_ROUND_ROBIN_Splitter_9441();
void CombineDFT_9443();
void CombineDFT_9444();
void CombineDFT_9445();
void CombineDFT_9446();
void CombineDFT_9447();
void CombineDFT_9448();
void CombineDFT_9449();
void CombineDFT_9450();
void WEIGHTED_ROUND_ROBIN_Joiner_9442();
void WEIGHTED_ROUND_ROBIN_Splitter_9451();
void CombineDFT_9453();
void CombineDFT_9454();
void CombineDFT_9455();
void CombineDFT_9456();
void WEIGHTED_ROUND_ROBIN_Joiner_9452();
void WEIGHTED_ROUND_ROBIN_Splitter_9457();
void CombineDFT_9459();
void CombineDFT_9460();
void WEIGHTED_ROUND_ROBIN_Joiner_9458();
void CombineDFT_9354();
void FFTReorderSimple_9355();
void WEIGHTED_ROUND_ROBIN_Splitter_9461();
void FFTReorderSimple_9463();
void FFTReorderSimple_9464();
void WEIGHTED_ROUND_ROBIN_Joiner_9462();
void WEIGHTED_ROUND_ROBIN_Splitter_9465();
void FFTReorderSimple_9467();
void FFTReorderSimple_9468();
void FFTReorderSimple_9469();
void FFTReorderSimple_9470();
void WEIGHTED_ROUND_ROBIN_Joiner_9466();
void WEIGHTED_ROUND_ROBIN_Splitter_9471();
void FFTReorderSimple_9473();
void FFTReorderSimple_9474();
void FFTReorderSimple_9475();
void FFTReorderSimple_9476();
void FFTReorderSimple_9477();
void FFTReorderSimple_9478();
void FFTReorderSimple_9479();
void FFTReorderSimple_9480();
void WEIGHTED_ROUND_ROBIN_Joiner_9472();
void WEIGHTED_ROUND_ROBIN_Splitter_9481();
void FFTReorderSimple_9483();
void FFTReorderSimple_9484();
void FFTReorderSimple_9485();
void FFTReorderSimple_9486();
void FFTReorderSimple_9487();
void FFTReorderSimple_9488();
void FFTReorderSimple_9489();
void FFTReorderSimple_9490();
void FFTReorderSimple_9491();
void FFTReorderSimple_9492();
void FFTReorderSimple_9493();
void FFTReorderSimple_9494();
void WEIGHTED_ROUND_ROBIN_Joiner_9482();
void WEIGHTED_ROUND_ROBIN_Splitter_9495();
void CombineDFT_9497();
void CombineDFT_9498();
void CombineDFT_9499();
void CombineDFT_9500();
void CombineDFT_9501();
void CombineDFT_9502();
void CombineDFT_9503();
void CombineDFT_9504();
void CombineDFT_9505();
void CombineDFT_9506();
void CombineDFT_9507();
void CombineDFT_9508();
void WEIGHTED_ROUND_ROBIN_Joiner_9496();
void WEIGHTED_ROUND_ROBIN_Splitter_9509();
void CombineDFT_9511();
void CombineDFT_9512();
void CombineDFT_9513();
void CombineDFT_9514();
void CombineDFT_9515();
void CombineDFT_9516();
void CombineDFT_9517();
void CombineDFT_9518();
void CombineDFT_9519();
void CombineDFT_9520();
void CombineDFT_9521();
void CombineDFT_9522();
void WEIGHTED_ROUND_ROBIN_Joiner_9510();
void WEIGHTED_ROUND_ROBIN_Splitter_9523();
void CombineDFT_9525();
void CombineDFT_9526();
void CombineDFT_9527();
void CombineDFT_9528();
void CombineDFT_9529();
void CombineDFT_9530();
void CombineDFT_9531();
void CombineDFT_9532();
void WEIGHTED_ROUND_ROBIN_Joiner_9524();
void WEIGHTED_ROUND_ROBIN_Splitter_9533();
void CombineDFT_9535();
void CombineDFT_9536();
void CombineDFT_9537();
void CombineDFT_9538();
void WEIGHTED_ROUND_ROBIN_Joiner_9534();
void WEIGHTED_ROUND_ROBIN_Splitter_9539();
void CombineDFT_9541();
void CombineDFT_9542();
void WEIGHTED_ROUND_ROBIN_Joiner_9540();
void CombineDFT_9365();
void WEIGHTED_ROUND_ROBIN_Joiner_9368();
void FloatPrinter(buffer_float_t *chanin);
void FloatPrinter_9366();

#ifdef __cplusplus
}
#endif
#endif
