#include "PEG10-FFT2.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10250WEIGHTED_ROUND_ROBIN_Splitter_10261;
buffer_float_t SplitJoin78_FFTReorderSimple_Fiss_10307_10328_split[10];
buffer_float_t FFTReorderSimple_10117WEIGHTED_ROUND_ROBIN_Splitter_10217;
buffer_float_t SplitJoin74_FFTReorderSimple_Fiss_10305_10326_join[4];
buffer_float_t SplitJoin82_CombineDFT_Fiss_10309_10330_split[10];
buffer_float_t SplitJoin84_CombineDFT_Fiss_10310_10331_join[8];
buffer_float_t SplitJoin18_CombineDFT_Fiss_10302_10323_join[4];
buffer_float_t SplitJoin86_CombineDFT_Fiss_10311_10332_join[4];
buffer_float_t SplitJoin12_CombineDFT_Fiss_10299_10320_split[10];
buffer_float_t SplitJoin16_CombineDFT_Fiss_10301_10322_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10274WEIGHTED_ROUND_ROBIN_Splitter_10283;
buffer_float_t SplitJoin80_CombineDFT_Fiss_10308_10329_split[10];
buffer_float_t SplitJoin20_CombineDFT_Fiss_10303_10324_join[2];
buffer_float_t SplitJoin0_FFTTestSource_Fiss_10293_10314_split[2];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10095_10131_10294_10315_join[2];
buffer_float_t SplitJoin84_CombineDFT_Fiss_10310_10331_split[8];
buffer_float_t FFTReorderSimple_10106WEIGHTED_ROUND_ROBIN_Splitter_10141;
buffer_float_t SplitJoin80_CombineDFT_Fiss_10308_10329_join[10];
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_10298_10319_split[10];
buffer_float_t SplitJoin72_FFTReorderSimple_Fiss_10304_10325_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10142WEIGHTED_ROUND_ROBIN_Splitter_10145;
buffer_float_t SplitJoin12_CombineDFT_Fiss_10299_10320_join[10];
buffer_float_t SplitJoin76_FFTReorderSimple_Fiss_10306_10327_split[8];
buffer_float_t SplitJoin14_CombineDFT_Fiss_10300_10321_split[10];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10208WEIGHTED_ROUND_ROBIN_Splitter_10213;
buffer_float_t SplitJoin16_CombineDFT_Fiss_10301_10322_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10130FloatPrinter_10128;
buffer_float_t SplitJoin86_CombineDFT_Fiss_10311_10332_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10228WEIGHTED_ROUND_ROBIN_Splitter_10237;
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_10296_10317_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10262WEIGHTED_ROUND_ROBIN_Splitter_10273;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10214CombineDFT_10116;
buffer_float_t SplitJoin74_FFTReorderSimple_Fiss_10305_10326_split[4];
buffer_float_t SplitJoin14_CombineDFT_Fiss_10300_10321_join[10];
buffer_float_t SplitJoin18_CombineDFT_Fiss_10302_10323_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10222WEIGHTED_ROUND_ROBIN_Splitter_10227;
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_10297_10318_join[8];
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_10296_10317_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10284WEIGHTED_ROUND_ROBIN_Splitter_10289;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10152WEIGHTED_ROUND_ROBIN_Splitter_10161;
buffer_float_t SplitJoin78_FFTReorderSimple_Fiss_10307_10328_join[10];
buffer_float_t SplitJoin0_FFTTestSource_Fiss_10293_10314_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10138WEIGHTED_ROUND_ROBIN_Splitter_10129;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10146WEIGHTED_ROUND_ROBIN_Splitter_10151;
buffer_float_t SplitJoin88_CombineDFT_Fiss_10312_10333_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10186WEIGHTED_ROUND_ROBIN_Splitter_10197;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10218WEIGHTED_ROUND_ROBIN_Splitter_10221;
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_10295_10316_split[2];
buffer_float_t SplitJoin76_FFTReorderSimple_Fiss_10306_10327_join[8];
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_10295_10316_join[2];
buffer_float_t SplitJoin82_CombineDFT_Fiss_10309_10330_join[10];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10238WEIGHTED_ROUND_ROBIN_Splitter_10249;
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10095_10131_10294_10315_split[2];
buffer_float_t SplitJoin88_CombineDFT_Fiss_10312_10333_join[2];
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_10297_10318_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10290CombineDFT_10127;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10198WEIGHTED_ROUND_ROBIN_Splitter_10207;
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_10298_10319_join[10];
buffer_float_t SplitJoin72_FFTReorderSimple_Fiss_10304_10325_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10162WEIGHTED_ROUND_ROBIN_Splitter_10173;
buffer_float_t SplitJoin20_CombineDFT_Fiss_10303_10324_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10174WEIGHTED_ROUND_ROBIN_Splitter_10185;


CombineDFT_10175_t CombineDFT_10175_s;
CombineDFT_10175_t CombineDFT_10176_s;
CombineDFT_10175_t CombineDFT_10177_s;
CombineDFT_10175_t CombineDFT_10178_s;
CombineDFT_10175_t CombineDFT_10179_s;
CombineDFT_10175_t CombineDFT_10180_s;
CombineDFT_10175_t CombineDFT_10181_s;
CombineDFT_10175_t CombineDFT_10182_s;
CombineDFT_10175_t CombineDFT_10183_s;
CombineDFT_10175_t CombineDFT_10184_s;
CombineDFT_10187_t CombineDFT_10187_s;
CombineDFT_10187_t CombineDFT_10188_s;
CombineDFT_10187_t CombineDFT_10189_s;
CombineDFT_10187_t CombineDFT_10190_s;
CombineDFT_10187_t CombineDFT_10191_s;
CombineDFT_10187_t CombineDFT_10192_s;
CombineDFT_10187_t CombineDFT_10193_s;
CombineDFT_10187_t CombineDFT_10194_s;
CombineDFT_10187_t CombineDFT_10195_s;
CombineDFT_10187_t CombineDFT_10196_s;
CombineDFT_10199_t CombineDFT_10199_s;
CombineDFT_10199_t CombineDFT_10200_s;
CombineDFT_10199_t CombineDFT_10201_s;
CombineDFT_10199_t CombineDFT_10202_s;
CombineDFT_10199_t CombineDFT_10203_s;
CombineDFT_10199_t CombineDFT_10204_s;
CombineDFT_10199_t CombineDFT_10205_s;
CombineDFT_10199_t CombineDFT_10206_s;
CombineDFT_10209_t CombineDFT_10209_s;
CombineDFT_10209_t CombineDFT_10210_s;
CombineDFT_10209_t CombineDFT_10211_s;
CombineDFT_10209_t CombineDFT_10212_s;
CombineDFT_10215_t CombineDFT_10215_s;
CombineDFT_10215_t CombineDFT_10216_s;
CombineDFT_10116_t CombineDFT_10116_s;
CombineDFT_10175_t CombineDFT_10251_s;
CombineDFT_10175_t CombineDFT_10252_s;
CombineDFT_10175_t CombineDFT_10253_s;
CombineDFT_10175_t CombineDFT_10254_s;
CombineDFT_10175_t CombineDFT_10255_s;
CombineDFT_10175_t CombineDFT_10256_s;
CombineDFT_10175_t CombineDFT_10257_s;
CombineDFT_10175_t CombineDFT_10258_s;
CombineDFT_10175_t CombineDFT_10259_s;
CombineDFT_10175_t CombineDFT_10260_s;
CombineDFT_10187_t CombineDFT_10263_s;
CombineDFT_10187_t CombineDFT_10264_s;
CombineDFT_10187_t CombineDFT_10265_s;
CombineDFT_10187_t CombineDFT_10266_s;
CombineDFT_10187_t CombineDFT_10267_s;
CombineDFT_10187_t CombineDFT_10268_s;
CombineDFT_10187_t CombineDFT_10269_s;
CombineDFT_10187_t CombineDFT_10270_s;
CombineDFT_10187_t CombineDFT_10271_s;
CombineDFT_10187_t CombineDFT_10272_s;
CombineDFT_10199_t CombineDFT_10275_s;
CombineDFT_10199_t CombineDFT_10276_s;
CombineDFT_10199_t CombineDFT_10277_s;
CombineDFT_10199_t CombineDFT_10278_s;
CombineDFT_10199_t CombineDFT_10279_s;
CombineDFT_10199_t CombineDFT_10280_s;
CombineDFT_10199_t CombineDFT_10281_s;
CombineDFT_10199_t CombineDFT_10282_s;
CombineDFT_10209_t CombineDFT_10285_s;
CombineDFT_10209_t CombineDFT_10286_s;
CombineDFT_10209_t CombineDFT_10287_s;
CombineDFT_10209_t CombineDFT_10288_s;
CombineDFT_10215_t CombineDFT_10291_s;
CombineDFT_10215_t CombineDFT_10292_s;
CombineDFT_10116_t CombineDFT_10127_s;

void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), 0.0) ; 
		push_float(&(*chanout), 0.0) ; 
		push_float(&(*chanout), 1.0) ; 
		push_float(&(*chanout), 0.0) ; 
		FOR(int, i, 0,  < , 124, i++) {
			push_float(&(*chanout), 0.0) ; 
		}
		ENDFOR
	}


void FFTTestSource_10139() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_10293_10314_split[0]), &(SplitJoin0_FFTTestSource_Fiss_10293_10314_join[0]));
	ENDFOR
}

void FFTTestSource_10140() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_10293_10314_split[1]), &(SplitJoin0_FFTTestSource_Fiss_10293_10314_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10137() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_10138() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10138WEIGHTED_ROUND_ROBIN_Splitter_10129, pop_float(&SplitJoin0_FFTTestSource_Fiss_10293_10314_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10138WEIGHTED_ROUND_ROBIN_Splitter_10129, pop_float(&SplitJoin0_FFTTestSource_Fiss_10293_10314_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, i, 0,  < , 128, i = (i + 4)) {
			push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 128, i = (i + 4)) {
			push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_float(&(*chanin)) ; 
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void FFTReorderSimple_10106() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10095_10131_10294_10315_split[0]), &(FFTReorderSimple_10106WEIGHTED_ROUND_ROBIN_Splitter_10141));
	ENDFOR
}

void FFTReorderSimple_10143() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_10295_10316_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_10295_10316_join[0]));
	ENDFOR
}

void FFTReorderSimple_10144() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_10295_10316_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_10295_10316_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10141() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_10295_10316_split[0], pop_float(&FFTReorderSimple_10106WEIGHTED_ROUND_ROBIN_Splitter_10141));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_10295_10316_split[1], pop_float(&FFTReorderSimple_10106WEIGHTED_ROUND_ROBIN_Splitter_10141));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10142() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10142WEIGHTED_ROUND_ROBIN_Splitter_10145, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_10295_10316_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10142WEIGHTED_ROUND_ROBIN_Splitter_10145, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_10295_10316_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_10147() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_10296_10317_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_10296_10317_join[0]));
	ENDFOR
}

void FFTReorderSimple_10148() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_10296_10317_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_10296_10317_join[1]));
	ENDFOR
}

void FFTReorderSimple_10149() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_10296_10317_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_10296_10317_join[2]));
	ENDFOR
}

void FFTReorderSimple_10150() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_10296_10317_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_10296_10317_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10145() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin6_FFTReorderSimple_Fiss_10296_10317_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10142WEIGHTED_ROUND_ROBIN_Splitter_10145));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10146() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10146WEIGHTED_ROUND_ROBIN_Splitter_10151, pop_float(&SplitJoin6_FFTReorderSimple_Fiss_10296_10317_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_10153() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_10297_10318_split[0]), &(SplitJoin8_FFTReorderSimple_Fiss_10297_10318_join[0]));
	ENDFOR
}

void FFTReorderSimple_10154() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_10297_10318_split[1]), &(SplitJoin8_FFTReorderSimple_Fiss_10297_10318_join[1]));
	ENDFOR
}

void FFTReorderSimple_10155() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_10297_10318_split[2]), &(SplitJoin8_FFTReorderSimple_Fiss_10297_10318_join[2]));
	ENDFOR
}

void FFTReorderSimple_10156() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_10297_10318_split[3]), &(SplitJoin8_FFTReorderSimple_Fiss_10297_10318_join[3]));
	ENDFOR
}

void FFTReorderSimple_10157() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_10297_10318_split[4]), &(SplitJoin8_FFTReorderSimple_Fiss_10297_10318_join[4]));
	ENDFOR
}

void FFTReorderSimple_10158() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_10297_10318_split[5]), &(SplitJoin8_FFTReorderSimple_Fiss_10297_10318_join[5]));
	ENDFOR
}

void FFTReorderSimple_10159() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_10297_10318_split[6]), &(SplitJoin8_FFTReorderSimple_Fiss_10297_10318_join[6]));
	ENDFOR
}

void FFTReorderSimple_10160() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_10297_10318_split[7]), &(SplitJoin8_FFTReorderSimple_Fiss_10297_10318_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10151() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin8_FFTReorderSimple_Fiss_10297_10318_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10146WEIGHTED_ROUND_ROBIN_Splitter_10151));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10152() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10152WEIGHTED_ROUND_ROBIN_Splitter_10161, pop_float(&SplitJoin8_FFTReorderSimple_Fiss_10297_10318_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_10163() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_10298_10319_split[0]), &(SplitJoin10_FFTReorderSimple_Fiss_10298_10319_join[0]));
	ENDFOR
}

void FFTReorderSimple_10164() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_10298_10319_split[1]), &(SplitJoin10_FFTReorderSimple_Fiss_10298_10319_join[1]));
	ENDFOR
}

void FFTReorderSimple_10165() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_10298_10319_split[2]), &(SplitJoin10_FFTReorderSimple_Fiss_10298_10319_join[2]));
	ENDFOR
}

void FFTReorderSimple_10166() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_10298_10319_split[3]), &(SplitJoin10_FFTReorderSimple_Fiss_10298_10319_join[3]));
	ENDFOR
}

void FFTReorderSimple_10167() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_10298_10319_split[4]), &(SplitJoin10_FFTReorderSimple_Fiss_10298_10319_join[4]));
	ENDFOR
}

void FFTReorderSimple_10168() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_10298_10319_split[5]), &(SplitJoin10_FFTReorderSimple_Fiss_10298_10319_join[5]));
	ENDFOR
}

void FFTReorderSimple_10169() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_10298_10319_split[6]), &(SplitJoin10_FFTReorderSimple_Fiss_10298_10319_join[6]));
	ENDFOR
}

void FFTReorderSimple_10170() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_10298_10319_split[7]), &(SplitJoin10_FFTReorderSimple_Fiss_10298_10319_join[7]));
	ENDFOR
}

void FFTReorderSimple_10171() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_10298_10319_split[8]), &(SplitJoin10_FFTReorderSimple_Fiss_10298_10319_join[8]));
	ENDFOR
}

void FFTReorderSimple_10172() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_10298_10319_split[9]), &(SplitJoin10_FFTReorderSimple_Fiss_10298_10319_join[9]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10161() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 10, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_FFTReorderSimple_Fiss_10298_10319_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10152WEIGHTED_ROUND_ROBIN_Splitter_10161));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10162() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 10, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10162WEIGHTED_ROUND_ROBIN_Splitter_10173, pop_float(&SplitJoin10_FFTReorderSimple_Fiss_10298_10319_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&(*chanin), i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&(*chanin), i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&(*chanin), (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&(*chanin), (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_10175_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_10175_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&(*chanin)) ; 
			push_float(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_10175() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_10299_10320_split[0]), &(SplitJoin12_CombineDFT_Fiss_10299_10320_join[0]));
	ENDFOR
}

void CombineDFT_10176() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_10299_10320_split[1]), &(SplitJoin12_CombineDFT_Fiss_10299_10320_join[1]));
	ENDFOR
}

void CombineDFT_10177() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_10299_10320_split[2]), &(SplitJoin12_CombineDFT_Fiss_10299_10320_join[2]));
	ENDFOR
}

void CombineDFT_10178() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_10299_10320_split[3]), &(SplitJoin12_CombineDFT_Fiss_10299_10320_join[3]));
	ENDFOR
}

void CombineDFT_10179() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_10299_10320_split[4]), &(SplitJoin12_CombineDFT_Fiss_10299_10320_join[4]));
	ENDFOR
}

void CombineDFT_10180() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_10299_10320_split[5]), &(SplitJoin12_CombineDFT_Fiss_10299_10320_join[5]));
	ENDFOR
}

void CombineDFT_10181() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_10299_10320_split[6]), &(SplitJoin12_CombineDFT_Fiss_10299_10320_join[6]));
	ENDFOR
}

void CombineDFT_10182() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_10299_10320_split[7]), &(SplitJoin12_CombineDFT_Fiss_10299_10320_join[7]));
	ENDFOR
}

void CombineDFT_10183() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_10299_10320_split[8]), &(SplitJoin12_CombineDFT_Fiss_10299_10320_join[8]));
	ENDFOR
}

void CombineDFT_10184() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_10299_10320_split[9]), &(SplitJoin12_CombineDFT_Fiss_10299_10320_join[9]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10173() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 10, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin12_CombineDFT_Fiss_10299_10320_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10162WEIGHTED_ROUND_ROBIN_Splitter_10173));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10174() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 10, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10174WEIGHTED_ROUND_ROBIN_Splitter_10185, pop_float(&SplitJoin12_CombineDFT_Fiss_10299_10320_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_10187() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_10300_10321_split[0]), &(SplitJoin14_CombineDFT_Fiss_10300_10321_join[0]));
	ENDFOR
}

void CombineDFT_10188() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_10300_10321_split[1]), &(SplitJoin14_CombineDFT_Fiss_10300_10321_join[1]));
	ENDFOR
}

void CombineDFT_10189() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_10300_10321_split[2]), &(SplitJoin14_CombineDFT_Fiss_10300_10321_join[2]));
	ENDFOR
}

void CombineDFT_10190() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_10300_10321_split[3]), &(SplitJoin14_CombineDFT_Fiss_10300_10321_join[3]));
	ENDFOR
}

void CombineDFT_10191() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_10300_10321_split[4]), &(SplitJoin14_CombineDFT_Fiss_10300_10321_join[4]));
	ENDFOR
}

void CombineDFT_10192() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_10300_10321_split[5]), &(SplitJoin14_CombineDFT_Fiss_10300_10321_join[5]));
	ENDFOR
}

void CombineDFT_10193() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_10300_10321_split[6]), &(SplitJoin14_CombineDFT_Fiss_10300_10321_join[6]));
	ENDFOR
}

void CombineDFT_10194() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_10300_10321_split[7]), &(SplitJoin14_CombineDFT_Fiss_10300_10321_join[7]));
	ENDFOR
}

void CombineDFT_10195() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_10300_10321_split[8]), &(SplitJoin14_CombineDFT_Fiss_10300_10321_join[8]));
	ENDFOR
}

void CombineDFT_10196() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_10300_10321_split[9]), &(SplitJoin14_CombineDFT_Fiss_10300_10321_join[9]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10185() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 10, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin14_CombineDFT_Fiss_10300_10321_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10174WEIGHTED_ROUND_ROBIN_Splitter_10185));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10186() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 10, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10186WEIGHTED_ROUND_ROBIN_Splitter_10197, pop_float(&SplitJoin14_CombineDFT_Fiss_10300_10321_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_10199() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_10301_10322_split[0]), &(SplitJoin16_CombineDFT_Fiss_10301_10322_join[0]));
	ENDFOR
}

void CombineDFT_10200() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_10301_10322_split[1]), &(SplitJoin16_CombineDFT_Fiss_10301_10322_join[1]));
	ENDFOR
}

void CombineDFT_10201() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_10301_10322_split[2]), &(SplitJoin16_CombineDFT_Fiss_10301_10322_join[2]));
	ENDFOR
}

void CombineDFT_10202() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_10301_10322_split[3]), &(SplitJoin16_CombineDFT_Fiss_10301_10322_join[3]));
	ENDFOR
}

void CombineDFT_10203() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_10301_10322_split[4]), &(SplitJoin16_CombineDFT_Fiss_10301_10322_join[4]));
	ENDFOR
}

void CombineDFT_10204() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_10301_10322_split[5]), &(SplitJoin16_CombineDFT_Fiss_10301_10322_join[5]));
	ENDFOR
}

void CombineDFT_10205() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_10301_10322_split[6]), &(SplitJoin16_CombineDFT_Fiss_10301_10322_join[6]));
	ENDFOR
}

void CombineDFT_10206() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_10301_10322_split[7]), &(SplitJoin16_CombineDFT_Fiss_10301_10322_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10197() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin16_CombineDFT_Fiss_10301_10322_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10186WEIGHTED_ROUND_ROBIN_Splitter_10197));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10198() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10198WEIGHTED_ROUND_ROBIN_Splitter_10207, pop_float(&SplitJoin16_CombineDFT_Fiss_10301_10322_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_10209() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_10302_10323_split[0]), &(SplitJoin18_CombineDFT_Fiss_10302_10323_join[0]));
	ENDFOR
}

void CombineDFT_10210() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_10302_10323_split[1]), &(SplitJoin18_CombineDFT_Fiss_10302_10323_join[1]));
	ENDFOR
}

void CombineDFT_10211() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_10302_10323_split[2]), &(SplitJoin18_CombineDFT_Fiss_10302_10323_join[2]));
	ENDFOR
}

void CombineDFT_10212() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_10302_10323_split[3]), &(SplitJoin18_CombineDFT_Fiss_10302_10323_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10207() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin18_CombineDFT_Fiss_10302_10323_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10198WEIGHTED_ROUND_ROBIN_Splitter_10207));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10208() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10208WEIGHTED_ROUND_ROBIN_Splitter_10213, pop_float(&SplitJoin18_CombineDFT_Fiss_10302_10323_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_10215() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin20_CombineDFT_Fiss_10303_10324_split[0]), &(SplitJoin20_CombineDFT_Fiss_10303_10324_join[0]));
	ENDFOR
}

void CombineDFT_10216() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin20_CombineDFT_Fiss_10303_10324_split[1]), &(SplitJoin20_CombineDFT_Fiss_10303_10324_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10213() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_10303_10324_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10208WEIGHTED_ROUND_ROBIN_Splitter_10213));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_10303_10324_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10208WEIGHTED_ROUND_ROBIN_Splitter_10213));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10214() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10214CombineDFT_10116, pop_float(&SplitJoin20_CombineDFT_Fiss_10303_10324_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10214CombineDFT_10116, pop_float(&SplitJoin20_CombineDFT_Fiss_10303_10324_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_10116() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_10214CombineDFT_10116), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10095_10131_10294_10315_join[0]));
	ENDFOR
}

void FFTReorderSimple_10117() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10095_10131_10294_10315_split[1]), &(FFTReorderSimple_10117WEIGHTED_ROUND_ROBIN_Splitter_10217));
	ENDFOR
}

void FFTReorderSimple_10219() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin72_FFTReorderSimple_Fiss_10304_10325_split[0]), &(SplitJoin72_FFTReorderSimple_Fiss_10304_10325_join[0]));
	ENDFOR
}

void FFTReorderSimple_10220() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin72_FFTReorderSimple_Fiss_10304_10325_split[1]), &(SplitJoin72_FFTReorderSimple_Fiss_10304_10325_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10217() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_10304_10325_split[0], pop_float(&FFTReorderSimple_10117WEIGHTED_ROUND_ROBIN_Splitter_10217));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin72_FFTReorderSimple_Fiss_10304_10325_split[1], pop_float(&FFTReorderSimple_10117WEIGHTED_ROUND_ROBIN_Splitter_10217));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10218() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10218WEIGHTED_ROUND_ROBIN_Splitter_10221, pop_float(&SplitJoin72_FFTReorderSimple_Fiss_10304_10325_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10218WEIGHTED_ROUND_ROBIN_Splitter_10221, pop_float(&SplitJoin72_FFTReorderSimple_Fiss_10304_10325_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_10223() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin74_FFTReorderSimple_Fiss_10305_10326_split[0]), &(SplitJoin74_FFTReorderSimple_Fiss_10305_10326_join[0]));
	ENDFOR
}

void FFTReorderSimple_10224() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin74_FFTReorderSimple_Fiss_10305_10326_split[1]), &(SplitJoin74_FFTReorderSimple_Fiss_10305_10326_join[1]));
	ENDFOR
}

void FFTReorderSimple_10225() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin74_FFTReorderSimple_Fiss_10305_10326_split[2]), &(SplitJoin74_FFTReorderSimple_Fiss_10305_10326_join[2]));
	ENDFOR
}

void FFTReorderSimple_10226() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin74_FFTReorderSimple_Fiss_10305_10326_split[3]), &(SplitJoin74_FFTReorderSimple_Fiss_10305_10326_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10221() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin74_FFTReorderSimple_Fiss_10305_10326_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10218WEIGHTED_ROUND_ROBIN_Splitter_10221));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10222() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10222WEIGHTED_ROUND_ROBIN_Splitter_10227, pop_float(&SplitJoin74_FFTReorderSimple_Fiss_10305_10326_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_10229() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin76_FFTReorderSimple_Fiss_10306_10327_split[0]), &(SplitJoin76_FFTReorderSimple_Fiss_10306_10327_join[0]));
	ENDFOR
}

void FFTReorderSimple_10230() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin76_FFTReorderSimple_Fiss_10306_10327_split[1]), &(SplitJoin76_FFTReorderSimple_Fiss_10306_10327_join[1]));
	ENDFOR
}

void FFTReorderSimple_10231() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin76_FFTReorderSimple_Fiss_10306_10327_split[2]), &(SplitJoin76_FFTReorderSimple_Fiss_10306_10327_join[2]));
	ENDFOR
}

void FFTReorderSimple_10232() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin76_FFTReorderSimple_Fiss_10306_10327_split[3]), &(SplitJoin76_FFTReorderSimple_Fiss_10306_10327_join[3]));
	ENDFOR
}

void FFTReorderSimple_10233() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin76_FFTReorderSimple_Fiss_10306_10327_split[4]), &(SplitJoin76_FFTReorderSimple_Fiss_10306_10327_join[4]));
	ENDFOR
}

void FFTReorderSimple_10234() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin76_FFTReorderSimple_Fiss_10306_10327_split[5]), &(SplitJoin76_FFTReorderSimple_Fiss_10306_10327_join[5]));
	ENDFOR
}

void FFTReorderSimple_10235() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin76_FFTReorderSimple_Fiss_10306_10327_split[6]), &(SplitJoin76_FFTReorderSimple_Fiss_10306_10327_join[6]));
	ENDFOR
}

void FFTReorderSimple_10236() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin76_FFTReorderSimple_Fiss_10306_10327_split[7]), &(SplitJoin76_FFTReorderSimple_Fiss_10306_10327_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10227() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin76_FFTReorderSimple_Fiss_10306_10327_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10222WEIGHTED_ROUND_ROBIN_Splitter_10227));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10228() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10228WEIGHTED_ROUND_ROBIN_Splitter_10237, pop_float(&SplitJoin76_FFTReorderSimple_Fiss_10306_10327_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_10239() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin78_FFTReorderSimple_Fiss_10307_10328_split[0]), &(SplitJoin78_FFTReorderSimple_Fiss_10307_10328_join[0]));
	ENDFOR
}

void FFTReorderSimple_10240() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin78_FFTReorderSimple_Fiss_10307_10328_split[1]), &(SplitJoin78_FFTReorderSimple_Fiss_10307_10328_join[1]));
	ENDFOR
}

void FFTReorderSimple_10241() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin78_FFTReorderSimple_Fiss_10307_10328_split[2]), &(SplitJoin78_FFTReorderSimple_Fiss_10307_10328_join[2]));
	ENDFOR
}

void FFTReorderSimple_10242() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin78_FFTReorderSimple_Fiss_10307_10328_split[3]), &(SplitJoin78_FFTReorderSimple_Fiss_10307_10328_join[3]));
	ENDFOR
}

void FFTReorderSimple_10243() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin78_FFTReorderSimple_Fiss_10307_10328_split[4]), &(SplitJoin78_FFTReorderSimple_Fiss_10307_10328_join[4]));
	ENDFOR
}

void FFTReorderSimple_10244() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin78_FFTReorderSimple_Fiss_10307_10328_split[5]), &(SplitJoin78_FFTReorderSimple_Fiss_10307_10328_join[5]));
	ENDFOR
}

void FFTReorderSimple_10245() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin78_FFTReorderSimple_Fiss_10307_10328_split[6]), &(SplitJoin78_FFTReorderSimple_Fiss_10307_10328_join[6]));
	ENDFOR
}

void FFTReorderSimple_10246() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin78_FFTReorderSimple_Fiss_10307_10328_split[7]), &(SplitJoin78_FFTReorderSimple_Fiss_10307_10328_join[7]));
	ENDFOR
}

void FFTReorderSimple_10247() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin78_FFTReorderSimple_Fiss_10307_10328_split[8]), &(SplitJoin78_FFTReorderSimple_Fiss_10307_10328_join[8]));
	ENDFOR
}

void FFTReorderSimple_10248() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin78_FFTReorderSimple_Fiss_10307_10328_split[9]), &(SplitJoin78_FFTReorderSimple_Fiss_10307_10328_join[9]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10237() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 10, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin78_FFTReorderSimple_Fiss_10307_10328_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10228WEIGHTED_ROUND_ROBIN_Splitter_10237));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10238() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 10, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10238WEIGHTED_ROUND_ROBIN_Splitter_10249, pop_float(&SplitJoin78_FFTReorderSimple_Fiss_10307_10328_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_10251() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin80_CombineDFT_Fiss_10308_10329_split[0]), &(SplitJoin80_CombineDFT_Fiss_10308_10329_join[0]));
	ENDFOR
}

void CombineDFT_10252() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin80_CombineDFT_Fiss_10308_10329_split[1]), &(SplitJoin80_CombineDFT_Fiss_10308_10329_join[1]));
	ENDFOR
}

void CombineDFT_10253() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin80_CombineDFT_Fiss_10308_10329_split[2]), &(SplitJoin80_CombineDFT_Fiss_10308_10329_join[2]));
	ENDFOR
}

void CombineDFT_10254() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin80_CombineDFT_Fiss_10308_10329_split[3]), &(SplitJoin80_CombineDFT_Fiss_10308_10329_join[3]));
	ENDFOR
}

void CombineDFT_10255() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin80_CombineDFT_Fiss_10308_10329_split[4]), &(SplitJoin80_CombineDFT_Fiss_10308_10329_join[4]));
	ENDFOR
}

void CombineDFT_10256() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin80_CombineDFT_Fiss_10308_10329_split[5]), &(SplitJoin80_CombineDFT_Fiss_10308_10329_join[5]));
	ENDFOR
}

void CombineDFT_10257() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin80_CombineDFT_Fiss_10308_10329_split[6]), &(SplitJoin80_CombineDFT_Fiss_10308_10329_join[6]));
	ENDFOR
}

void CombineDFT_10258() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin80_CombineDFT_Fiss_10308_10329_split[7]), &(SplitJoin80_CombineDFT_Fiss_10308_10329_join[7]));
	ENDFOR
}

void CombineDFT_10259() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin80_CombineDFT_Fiss_10308_10329_split[8]), &(SplitJoin80_CombineDFT_Fiss_10308_10329_join[8]));
	ENDFOR
}

void CombineDFT_10260() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin80_CombineDFT_Fiss_10308_10329_split[9]), &(SplitJoin80_CombineDFT_Fiss_10308_10329_join[9]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10249() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 10, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin80_CombineDFT_Fiss_10308_10329_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10238WEIGHTED_ROUND_ROBIN_Splitter_10249));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10250() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 10, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10250WEIGHTED_ROUND_ROBIN_Splitter_10261, pop_float(&SplitJoin80_CombineDFT_Fiss_10308_10329_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_10263() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin82_CombineDFT_Fiss_10309_10330_split[0]), &(SplitJoin82_CombineDFT_Fiss_10309_10330_join[0]));
	ENDFOR
}

void CombineDFT_10264() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin82_CombineDFT_Fiss_10309_10330_split[1]), &(SplitJoin82_CombineDFT_Fiss_10309_10330_join[1]));
	ENDFOR
}

void CombineDFT_10265() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin82_CombineDFT_Fiss_10309_10330_split[2]), &(SplitJoin82_CombineDFT_Fiss_10309_10330_join[2]));
	ENDFOR
}

void CombineDFT_10266() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin82_CombineDFT_Fiss_10309_10330_split[3]), &(SplitJoin82_CombineDFT_Fiss_10309_10330_join[3]));
	ENDFOR
}

void CombineDFT_10267() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin82_CombineDFT_Fiss_10309_10330_split[4]), &(SplitJoin82_CombineDFT_Fiss_10309_10330_join[4]));
	ENDFOR
}

void CombineDFT_10268() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin82_CombineDFT_Fiss_10309_10330_split[5]), &(SplitJoin82_CombineDFT_Fiss_10309_10330_join[5]));
	ENDFOR
}

void CombineDFT_10269() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin82_CombineDFT_Fiss_10309_10330_split[6]), &(SplitJoin82_CombineDFT_Fiss_10309_10330_join[6]));
	ENDFOR
}

void CombineDFT_10270() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin82_CombineDFT_Fiss_10309_10330_split[7]), &(SplitJoin82_CombineDFT_Fiss_10309_10330_join[7]));
	ENDFOR
}

void CombineDFT_10271() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin82_CombineDFT_Fiss_10309_10330_split[8]), &(SplitJoin82_CombineDFT_Fiss_10309_10330_join[8]));
	ENDFOR
}

void CombineDFT_10272() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin82_CombineDFT_Fiss_10309_10330_split[9]), &(SplitJoin82_CombineDFT_Fiss_10309_10330_join[9]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10261() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 10, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin82_CombineDFT_Fiss_10309_10330_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10250WEIGHTED_ROUND_ROBIN_Splitter_10261));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10262() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 10, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10262WEIGHTED_ROUND_ROBIN_Splitter_10273, pop_float(&SplitJoin82_CombineDFT_Fiss_10309_10330_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_10275() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin84_CombineDFT_Fiss_10310_10331_split[0]), &(SplitJoin84_CombineDFT_Fiss_10310_10331_join[0]));
	ENDFOR
}

void CombineDFT_10276() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin84_CombineDFT_Fiss_10310_10331_split[1]), &(SplitJoin84_CombineDFT_Fiss_10310_10331_join[1]));
	ENDFOR
}

void CombineDFT_10277() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin84_CombineDFT_Fiss_10310_10331_split[2]), &(SplitJoin84_CombineDFT_Fiss_10310_10331_join[2]));
	ENDFOR
}

void CombineDFT_10278() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin84_CombineDFT_Fiss_10310_10331_split[3]), &(SplitJoin84_CombineDFT_Fiss_10310_10331_join[3]));
	ENDFOR
}

void CombineDFT_10279() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin84_CombineDFT_Fiss_10310_10331_split[4]), &(SplitJoin84_CombineDFT_Fiss_10310_10331_join[4]));
	ENDFOR
}

void CombineDFT_10280() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin84_CombineDFT_Fiss_10310_10331_split[5]), &(SplitJoin84_CombineDFT_Fiss_10310_10331_join[5]));
	ENDFOR
}

void CombineDFT_10281() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin84_CombineDFT_Fiss_10310_10331_split[6]), &(SplitJoin84_CombineDFT_Fiss_10310_10331_join[6]));
	ENDFOR
}

void CombineDFT_10282() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin84_CombineDFT_Fiss_10310_10331_split[7]), &(SplitJoin84_CombineDFT_Fiss_10310_10331_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10273() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin84_CombineDFT_Fiss_10310_10331_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10262WEIGHTED_ROUND_ROBIN_Splitter_10273));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10274() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10274WEIGHTED_ROUND_ROBIN_Splitter_10283, pop_float(&SplitJoin84_CombineDFT_Fiss_10310_10331_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_10285() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin86_CombineDFT_Fiss_10311_10332_split[0]), &(SplitJoin86_CombineDFT_Fiss_10311_10332_join[0]));
	ENDFOR
}

void CombineDFT_10286() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin86_CombineDFT_Fiss_10311_10332_split[1]), &(SplitJoin86_CombineDFT_Fiss_10311_10332_join[1]));
	ENDFOR
}

void CombineDFT_10287() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin86_CombineDFT_Fiss_10311_10332_split[2]), &(SplitJoin86_CombineDFT_Fiss_10311_10332_join[2]));
	ENDFOR
}

void CombineDFT_10288() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin86_CombineDFT_Fiss_10311_10332_split[3]), &(SplitJoin86_CombineDFT_Fiss_10311_10332_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10283() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin86_CombineDFT_Fiss_10311_10332_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10274WEIGHTED_ROUND_ROBIN_Splitter_10283));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10284() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10284WEIGHTED_ROUND_ROBIN_Splitter_10289, pop_float(&SplitJoin86_CombineDFT_Fiss_10311_10332_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_10291() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin88_CombineDFT_Fiss_10312_10333_split[0]), &(SplitJoin88_CombineDFT_Fiss_10312_10333_join[0]));
	ENDFOR
}

void CombineDFT_10292() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin88_CombineDFT_Fiss_10312_10333_split[1]), &(SplitJoin88_CombineDFT_Fiss_10312_10333_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10289() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin88_CombineDFT_Fiss_10312_10333_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10284WEIGHTED_ROUND_ROBIN_Splitter_10289));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin88_CombineDFT_Fiss_10312_10333_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10284WEIGHTED_ROUND_ROBIN_Splitter_10289));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10290() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10290CombineDFT_10127, pop_float(&SplitJoin88_CombineDFT_Fiss_10312_10333_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10290CombineDFT_10127, pop_float(&SplitJoin88_CombineDFT_Fiss_10312_10333_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_10127() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_10290CombineDFT_10127), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10095_10131_10294_10315_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10129() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10095_10131_10294_10315_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10138WEIGHTED_ROUND_ROBIN_Splitter_10129));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10095_10131_10294_10315_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10138WEIGHTED_ROUND_ROBIN_Splitter_10129));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10130() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10130FloatPrinter_10128, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10095_10131_10294_10315_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10130FloatPrinter_10128, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10095_10131_10294_10315_join[1]));
		ENDFOR
	ENDFOR
}}

void FloatPrinter(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void FloatPrinter_10128() {
	FOR(uint32_t, __iter_steady_, 0, <, 1280, __iter_steady_++)
		FloatPrinter(&(WEIGHTED_ROUND_ROBIN_Joiner_10130FloatPrinter_10128));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10250WEIGHTED_ROUND_ROBIN_Splitter_10261);
	FOR(int, __iter_init_0_, 0, <, 10, __iter_init_0_++)
		init_buffer_float(&SplitJoin78_FFTReorderSimple_Fiss_10307_10328_split[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&FFTReorderSimple_10117WEIGHTED_ROUND_ROBIN_Splitter_10217);
	FOR(int, __iter_init_1_, 0, <, 4, __iter_init_1_++)
		init_buffer_float(&SplitJoin74_FFTReorderSimple_Fiss_10305_10326_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 10, __iter_init_2_++)
		init_buffer_float(&SplitJoin82_CombineDFT_Fiss_10309_10330_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_float(&SplitJoin84_CombineDFT_Fiss_10310_10331_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 4, __iter_init_4_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_10302_10323_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 4, __iter_init_5_++)
		init_buffer_float(&SplitJoin86_CombineDFT_Fiss_10311_10332_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 10, __iter_init_6_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_10299_10320_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_10301_10322_split[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10274WEIGHTED_ROUND_ROBIN_Splitter_10283);
	FOR(int, __iter_init_8_, 0, <, 10, __iter_init_8_++)
		init_buffer_float(&SplitJoin80_CombineDFT_Fiss_10308_10329_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 2, __iter_init_9_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_10303_10324_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_10293_10314_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 2, __iter_init_11_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10095_10131_10294_10315_join[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 8, __iter_init_12_++)
		init_buffer_float(&SplitJoin84_CombineDFT_Fiss_10310_10331_split[__iter_init_12_]);
	ENDFOR
	init_buffer_float(&FFTReorderSimple_10106WEIGHTED_ROUND_ROBIN_Splitter_10141);
	FOR(int, __iter_init_13_, 0, <, 10, __iter_init_13_++)
		init_buffer_float(&SplitJoin80_CombineDFT_Fiss_10308_10329_join[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 10, __iter_init_14_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_10298_10319_split[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 2, __iter_init_15_++)
		init_buffer_float(&SplitJoin72_FFTReorderSimple_Fiss_10304_10325_join[__iter_init_15_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10142WEIGHTED_ROUND_ROBIN_Splitter_10145);
	FOR(int, __iter_init_16_, 0, <, 10, __iter_init_16_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_10299_10320_join[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 8, __iter_init_17_++)
		init_buffer_float(&SplitJoin76_FFTReorderSimple_Fiss_10306_10327_split[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 10, __iter_init_18_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_10300_10321_split[__iter_init_18_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10208WEIGHTED_ROUND_ROBIN_Splitter_10213);
	FOR(int, __iter_init_19_, 0, <, 8, __iter_init_19_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_10301_10322_join[__iter_init_19_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10130FloatPrinter_10128);
	FOR(int, __iter_init_20_, 0, <, 4, __iter_init_20_++)
		init_buffer_float(&SplitJoin86_CombineDFT_Fiss_10311_10332_split[__iter_init_20_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10228WEIGHTED_ROUND_ROBIN_Splitter_10237);
	FOR(int, __iter_init_21_, 0, <, 4, __iter_init_21_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_10296_10317_split[__iter_init_21_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10262WEIGHTED_ROUND_ROBIN_Splitter_10273);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10214CombineDFT_10116);
	FOR(int, __iter_init_22_, 0, <, 4, __iter_init_22_++)
		init_buffer_float(&SplitJoin74_FFTReorderSimple_Fiss_10305_10326_split[__iter_init_22_]);
	ENDFOR
	FOR(int, __iter_init_23_, 0, <, 10, __iter_init_23_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_10300_10321_join[__iter_init_23_]);
	ENDFOR
	FOR(int, __iter_init_24_, 0, <, 4, __iter_init_24_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_10302_10323_split[__iter_init_24_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10222WEIGHTED_ROUND_ROBIN_Splitter_10227);
	FOR(int, __iter_init_25_, 0, <, 8, __iter_init_25_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_10297_10318_join[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 4, __iter_init_26_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_10296_10317_join[__iter_init_26_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10284WEIGHTED_ROUND_ROBIN_Splitter_10289);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10152WEIGHTED_ROUND_ROBIN_Splitter_10161);
	FOR(int, __iter_init_27_, 0, <, 10, __iter_init_27_++)
		init_buffer_float(&SplitJoin78_FFTReorderSimple_Fiss_10307_10328_join[__iter_init_27_]);
	ENDFOR
	FOR(int, __iter_init_28_, 0, <, 2, __iter_init_28_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_10293_10314_join[__iter_init_28_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10138WEIGHTED_ROUND_ROBIN_Splitter_10129);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10146WEIGHTED_ROUND_ROBIN_Splitter_10151);
	FOR(int, __iter_init_29_, 0, <, 2, __iter_init_29_++)
		init_buffer_float(&SplitJoin88_CombineDFT_Fiss_10312_10333_split[__iter_init_29_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10186WEIGHTED_ROUND_ROBIN_Splitter_10197);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10218WEIGHTED_ROUND_ROBIN_Splitter_10221);
	FOR(int, __iter_init_30_, 0, <, 2, __iter_init_30_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_10295_10316_split[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 8, __iter_init_31_++)
		init_buffer_float(&SplitJoin76_FFTReorderSimple_Fiss_10306_10327_join[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 2, __iter_init_32_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_10295_10316_join[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 10, __iter_init_33_++)
		init_buffer_float(&SplitJoin82_CombineDFT_Fiss_10309_10330_join[__iter_init_33_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10238WEIGHTED_ROUND_ROBIN_Splitter_10249);
	FOR(int, __iter_init_34_, 0, <, 2, __iter_init_34_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_10095_10131_10294_10315_split[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 2, __iter_init_35_++)
		init_buffer_float(&SplitJoin88_CombineDFT_Fiss_10312_10333_join[__iter_init_35_]);
	ENDFOR
	FOR(int, __iter_init_36_, 0, <, 8, __iter_init_36_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_10297_10318_split[__iter_init_36_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10290CombineDFT_10127);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10198WEIGHTED_ROUND_ROBIN_Splitter_10207);
	FOR(int, __iter_init_37_, 0, <, 10, __iter_init_37_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_10298_10319_join[__iter_init_37_]);
	ENDFOR
	FOR(int, __iter_init_38_, 0, <, 2, __iter_init_38_++)
		init_buffer_float(&SplitJoin72_FFTReorderSimple_Fiss_10304_10325_split[__iter_init_38_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10162WEIGHTED_ROUND_ROBIN_Splitter_10173);
	FOR(int, __iter_init_39_, 0, <, 2, __iter_init_39_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_10303_10324_split[__iter_init_39_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10174WEIGHTED_ROUND_ROBIN_Splitter_10185);
// --- init: CombineDFT_10175
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10175_s.w[i] = real ; 
		CombineDFT_10175_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10176
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10176_s.w[i] = real ; 
		CombineDFT_10176_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10177
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10177_s.w[i] = real ; 
		CombineDFT_10177_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10178
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10178_s.w[i] = real ; 
		CombineDFT_10178_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10179
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10179_s.w[i] = real ; 
		CombineDFT_10179_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10180
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10180_s.w[i] = real ; 
		CombineDFT_10180_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10181
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10181_s.w[i] = real ; 
		CombineDFT_10181_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10182
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10182_s.w[i] = real ; 
		CombineDFT_10182_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10183
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10183_s.w[i] = real ; 
		CombineDFT_10183_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10184
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10184_s.w[i] = real ; 
		CombineDFT_10184_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10187
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10187_s.w[i] = real ; 
		CombineDFT_10187_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10188
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10188_s.w[i] = real ; 
		CombineDFT_10188_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10189
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10189_s.w[i] = real ; 
		CombineDFT_10189_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10190
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10190_s.w[i] = real ; 
		CombineDFT_10190_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10191
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10191_s.w[i] = real ; 
		CombineDFT_10191_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10192
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10192_s.w[i] = real ; 
		CombineDFT_10192_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10193
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10193_s.w[i] = real ; 
		CombineDFT_10193_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10194
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10194_s.w[i] = real ; 
		CombineDFT_10194_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10195
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10195_s.w[i] = real ; 
		CombineDFT_10195_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10196
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10196_s.w[i] = real ; 
		CombineDFT_10196_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10199
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10199_s.w[i] = real ; 
		CombineDFT_10199_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10200
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10200_s.w[i] = real ; 
		CombineDFT_10200_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10201
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10201_s.w[i] = real ; 
		CombineDFT_10201_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10202
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10202_s.w[i] = real ; 
		CombineDFT_10202_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10203
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10203_s.w[i] = real ; 
		CombineDFT_10203_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10204
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10204_s.w[i] = real ; 
		CombineDFT_10204_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10205
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10205_s.w[i] = real ; 
		CombineDFT_10205_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10206
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10206_s.w[i] = real ; 
		CombineDFT_10206_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10209
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_10209_s.w[i] = real ; 
		CombineDFT_10209_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10210
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_10210_s.w[i] = real ; 
		CombineDFT_10210_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10211
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_10211_s.w[i] = real ; 
		CombineDFT_10211_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10212
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_10212_s.w[i] = real ; 
		CombineDFT_10212_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10215
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_10215_s.w[i] = real ; 
		CombineDFT_10215_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10216
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_10216_s.w[i] = real ; 
		CombineDFT_10216_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10116
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_10116_s.w[i] = real ; 
		CombineDFT_10116_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10251
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10251_s.w[i] = real ; 
		CombineDFT_10251_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10252
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10252_s.w[i] = real ; 
		CombineDFT_10252_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10253
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10253_s.w[i] = real ; 
		CombineDFT_10253_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10254
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10254_s.w[i] = real ; 
		CombineDFT_10254_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10255
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10255_s.w[i] = real ; 
		CombineDFT_10255_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10256
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10256_s.w[i] = real ; 
		CombineDFT_10256_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10257
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10257_s.w[i] = real ; 
		CombineDFT_10257_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10258
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10258_s.w[i] = real ; 
		CombineDFT_10258_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10259
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10259_s.w[i] = real ; 
		CombineDFT_10259_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10260
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_10260_s.w[i] = real ; 
		CombineDFT_10260_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10263
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10263_s.w[i] = real ; 
		CombineDFT_10263_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10264
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10264_s.w[i] = real ; 
		CombineDFT_10264_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10265
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10265_s.w[i] = real ; 
		CombineDFT_10265_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10266
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10266_s.w[i] = real ; 
		CombineDFT_10266_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10267
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10267_s.w[i] = real ; 
		CombineDFT_10267_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10268
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10268_s.w[i] = real ; 
		CombineDFT_10268_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10269
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10269_s.w[i] = real ; 
		CombineDFT_10269_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10270
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10270_s.w[i] = real ; 
		CombineDFT_10270_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10271
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10271_s.w[i] = real ; 
		CombineDFT_10271_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10272
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_10272_s.w[i] = real ; 
		CombineDFT_10272_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10275
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10275_s.w[i] = real ; 
		CombineDFT_10275_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10276
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10276_s.w[i] = real ; 
		CombineDFT_10276_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10277
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10277_s.w[i] = real ; 
		CombineDFT_10277_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10278
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10278_s.w[i] = real ; 
		CombineDFT_10278_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10279
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10279_s.w[i] = real ; 
		CombineDFT_10279_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10280
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10280_s.w[i] = real ; 
		CombineDFT_10280_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10281
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10281_s.w[i] = real ; 
		CombineDFT_10281_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10282
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_10282_s.w[i] = real ; 
		CombineDFT_10282_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10285
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_10285_s.w[i] = real ; 
		CombineDFT_10285_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10286
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_10286_s.w[i] = real ; 
		CombineDFT_10286_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10287
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_10287_s.w[i] = real ; 
		CombineDFT_10287_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10288
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_10288_s.w[i] = real ; 
		CombineDFT_10288_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10291
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_10291_s.w[i] = real ; 
		CombineDFT_10291_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10292
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_10292_s.w[i] = real ; 
		CombineDFT_10292_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_10127
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_10127_s.w[i] = real ; 
		CombineDFT_10127_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_10137();
			FFTTestSource_10139();
			FFTTestSource_10140();
		WEIGHTED_ROUND_ROBIN_Joiner_10138();
		WEIGHTED_ROUND_ROBIN_Splitter_10129();
			FFTReorderSimple_10106();
			WEIGHTED_ROUND_ROBIN_Splitter_10141();
				FFTReorderSimple_10143();
				FFTReorderSimple_10144();
			WEIGHTED_ROUND_ROBIN_Joiner_10142();
			WEIGHTED_ROUND_ROBIN_Splitter_10145();
				FFTReorderSimple_10147();
				FFTReorderSimple_10148();
				FFTReorderSimple_10149();
				FFTReorderSimple_10150();
			WEIGHTED_ROUND_ROBIN_Joiner_10146();
			WEIGHTED_ROUND_ROBIN_Splitter_10151();
				FFTReorderSimple_10153();
				FFTReorderSimple_10154();
				FFTReorderSimple_10155();
				FFTReorderSimple_10156();
				FFTReorderSimple_10157();
				FFTReorderSimple_10158();
				FFTReorderSimple_10159();
				FFTReorderSimple_10160();
			WEIGHTED_ROUND_ROBIN_Joiner_10152();
			WEIGHTED_ROUND_ROBIN_Splitter_10161();
				FFTReorderSimple_10163();
				FFTReorderSimple_10164();
				FFTReorderSimple_10165();
				FFTReorderSimple_10166();
				FFTReorderSimple_10167();
				FFTReorderSimple_10168();
				FFTReorderSimple_10169();
				FFTReorderSimple_10170();
				FFTReorderSimple_10171();
				FFTReorderSimple_10172();
			WEIGHTED_ROUND_ROBIN_Joiner_10162();
			WEIGHTED_ROUND_ROBIN_Splitter_10173();
				CombineDFT_10175();
				CombineDFT_10176();
				CombineDFT_10177();
				CombineDFT_10178();
				CombineDFT_10179();
				CombineDFT_10180();
				CombineDFT_10181();
				CombineDFT_10182();
				CombineDFT_10183();
				CombineDFT_10184();
			WEIGHTED_ROUND_ROBIN_Joiner_10174();
			WEIGHTED_ROUND_ROBIN_Splitter_10185();
				CombineDFT_10187();
				CombineDFT_10188();
				CombineDFT_10189();
				CombineDFT_10190();
				CombineDFT_10191();
				CombineDFT_10192();
				CombineDFT_10193();
				CombineDFT_10194();
				CombineDFT_10195();
				CombineDFT_10196();
			WEIGHTED_ROUND_ROBIN_Joiner_10186();
			WEIGHTED_ROUND_ROBIN_Splitter_10197();
				CombineDFT_10199();
				CombineDFT_10200();
				CombineDFT_10201();
				CombineDFT_10202();
				CombineDFT_10203();
				CombineDFT_10204();
				CombineDFT_10205();
				CombineDFT_10206();
			WEIGHTED_ROUND_ROBIN_Joiner_10198();
			WEIGHTED_ROUND_ROBIN_Splitter_10207();
				CombineDFT_10209();
				CombineDFT_10210();
				CombineDFT_10211();
				CombineDFT_10212();
			WEIGHTED_ROUND_ROBIN_Joiner_10208();
			WEIGHTED_ROUND_ROBIN_Splitter_10213();
				CombineDFT_10215();
				CombineDFT_10216();
			WEIGHTED_ROUND_ROBIN_Joiner_10214();
			CombineDFT_10116();
			FFTReorderSimple_10117();
			WEIGHTED_ROUND_ROBIN_Splitter_10217();
				FFTReorderSimple_10219();
				FFTReorderSimple_10220();
			WEIGHTED_ROUND_ROBIN_Joiner_10218();
			WEIGHTED_ROUND_ROBIN_Splitter_10221();
				FFTReorderSimple_10223();
				FFTReorderSimple_10224();
				FFTReorderSimple_10225();
				FFTReorderSimple_10226();
			WEIGHTED_ROUND_ROBIN_Joiner_10222();
			WEIGHTED_ROUND_ROBIN_Splitter_10227();
				FFTReorderSimple_10229();
				FFTReorderSimple_10230();
				FFTReorderSimple_10231();
				FFTReorderSimple_10232();
				FFTReorderSimple_10233();
				FFTReorderSimple_10234();
				FFTReorderSimple_10235();
				FFTReorderSimple_10236();
			WEIGHTED_ROUND_ROBIN_Joiner_10228();
			WEIGHTED_ROUND_ROBIN_Splitter_10237();
				FFTReorderSimple_10239();
				FFTReorderSimple_10240();
				FFTReorderSimple_10241();
				FFTReorderSimple_10242();
				FFTReorderSimple_10243();
				FFTReorderSimple_10244();
				FFTReorderSimple_10245();
				FFTReorderSimple_10246();
				FFTReorderSimple_10247();
				FFTReorderSimple_10248();
			WEIGHTED_ROUND_ROBIN_Joiner_10238();
			WEIGHTED_ROUND_ROBIN_Splitter_10249();
				CombineDFT_10251();
				CombineDFT_10252();
				CombineDFT_10253();
				CombineDFT_10254();
				CombineDFT_10255();
				CombineDFT_10256();
				CombineDFT_10257();
				CombineDFT_10258();
				CombineDFT_10259();
				CombineDFT_10260();
			WEIGHTED_ROUND_ROBIN_Joiner_10250();
			WEIGHTED_ROUND_ROBIN_Splitter_10261();
				CombineDFT_10263();
				CombineDFT_10264();
				CombineDFT_10265();
				CombineDFT_10266();
				CombineDFT_10267();
				CombineDFT_10268();
				CombineDFT_10269();
				CombineDFT_10270();
				CombineDFT_10271();
				CombineDFT_10272();
			WEIGHTED_ROUND_ROBIN_Joiner_10262();
			WEIGHTED_ROUND_ROBIN_Splitter_10273();
				CombineDFT_10275();
				CombineDFT_10276();
				CombineDFT_10277();
				CombineDFT_10278();
				CombineDFT_10279();
				CombineDFT_10280();
				CombineDFT_10281();
				CombineDFT_10282();
			WEIGHTED_ROUND_ROBIN_Joiner_10274();
			WEIGHTED_ROUND_ROBIN_Splitter_10283();
				CombineDFT_10285();
				CombineDFT_10286();
				CombineDFT_10287();
				CombineDFT_10288();
			WEIGHTED_ROUND_ROBIN_Joiner_10284();
			WEIGHTED_ROUND_ROBIN_Splitter_10289();
				CombineDFT_10291();
				CombineDFT_10292();
			WEIGHTED_ROUND_ROBIN_Joiner_10290();
			CombineDFT_10127();
		WEIGHTED_ROUND_ROBIN_Joiner_10130();
		FloatPrinter_10128();
	ENDFOR
	return EXIT_SUCCESS;
}
