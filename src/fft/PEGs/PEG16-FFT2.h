#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=512 on the compile command line
#else
#if BUF_SIZEMAX < 512
#error BUF_SIZEMAX too small, it must be at least 512
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float w[2];
} CombineDFT_7751_t;

typedef struct {
	float w[4];
} CombineDFT_7769_t;

typedef struct {
	float w[8];
} CombineDFT_7787_t;

typedef struct {
	float w[16];
} CombineDFT_7797_t;

typedef struct {
	float w[32];
} CombineDFT_7803_t;

typedef struct {
	float w[64];
} CombineDFT_7686_t;
void WEIGHTED_ROUND_ROBIN_Splitter_7707();
void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout);
void FFTTestSource_7709();
void FFTTestSource_7710();
void WEIGHTED_ROUND_ROBIN_Joiner_7708();
void WEIGHTED_ROUND_ROBIN_Splitter_7699();
void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout);
void FFTReorderSimple_7676();
void WEIGHTED_ROUND_ROBIN_Splitter_7711();
void FFTReorderSimple_7713();
void FFTReorderSimple_7714();
void WEIGHTED_ROUND_ROBIN_Joiner_7712();
void WEIGHTED_ROUND_ROBIN_Splitter_7715();
void FFTReorderSimple_7717();
void FFTReorderSimple_7718();
void FFTReorderSimple_7719();
void FFTReorderSimple_7720();
void WEIGHTED_ROUND_ROBIN_Joiner_7716();
void WEIGHTED_ROUND_ROBIN_Splitter_7721();
void FFTReorderSimple_7723();
void FFTReorderSimple_7724();
void FFTReorderSimple_7725();
void FFTReorderSimple_7726();
void FFTReorderSimple_7727();
void FFTReorderSimple_7728();
void FFTReorderSimple_7729();
void FFTReorderSimple_7730();
void WEIGHTED_ROUND_ROBIN_Joiner_7722();
void WEIGHTED_ROUND_ROBIN_Splitter_7731();
void FFTReorderSimple_7733();
void FFTReorderSimple_7734();
void FFTReorderSimple_7735();
void FFTReorderSimple_7736();
void FFTReorderSimple_7737();
void FFTReorderSimple_7738();
void FFTReorderSimple_7739();
void FFTReorderSimple_7740();
void FFTReorderSimple_7741();
void FFTReorderSimple_7742();
void FFTReorderSimple_7743();
void FFTReorderSimple_7744();
void FFTReorderSimple_7745();
void FFTReorderSimple_7746();
void FFTReorderSimple_7747();
void FFTReorderSimple_7748();
void WEIGHTED_ROUND_ROBIN_Joiner_7732();
void WEIGHTED_ROUND_ROBIN_Splitter_7749();
void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout);
void CombineDFT_7751();
void CombineDFT_7752();
void CombineDFT_7753();
void CombineDFT_7754();
void CombineDFT_7755();
void CombineDFT_7756();
void CombineDFT_7757();
void CombineDFT_7758();
void CombineDFT_7759();
void CombineDFT_7760();
void CombineDFT_7761();
void CombineDFT_7762();
void CombineDFT_7763();
void CombineDFT_7764();
void CombineDFT_7765();
void CombineDFT_7766();
void WEIGHTED_ROUND_ROBIN_Joiner_7750();
void WEIGHTED_ROUND_ROBIN_Splitter_7767();
void CombineDFT_7769();
void CombineDFT_7770();
void CombineDFT_7771();
void CombineDFT_7772();
void CombineDFT_7773();
void CombineDFT_7774();
void CombineDFT_7775();
void CombineDFT_7776();
void CombineDFT_7777();
void CombineDFT_7778();
void CombineDFT_7779();
void CombineDFT_7780();
void CombineDFT_7781();
void CombineDFT_7782();
void CombineDFT_7783();
void CombineDFT_7784();
void WEIGHTED_ROUND_ROBIN_Joiner_7768();
void WEIGHTED_ROUND_ROBIN_Splitter_7785();
void CombineDFT_7787();
void CombineDFT_7788();
void CombineDFT_7789();
void CombineDFT_7790();
void CombineDFT_7791();
void CombineDFT_7792();
void CombineDFT_7793();
void CombineDFT_7794();
void WEIGHTED_ROUND_ROBIN_Joiner_7786();
void WEIGHTED_ROUND_ROBIN_Splitter_7795();
void CombineDFT_7797();
void CombineDFT_7798();
void CombineDFT_7799();
void CombineDFT_7800();
void WEIGHTED_ROUND_ROBIN_Joiner_7796();
void WEIGHTED_ROUND_ROBIN_Splitter_7801();
void CombineDFT_7803();
void CombineDFT_7804();
void WEIGHTED_ROUND_ROBIN_Joiner_7802();
void CombineDFT_7686();
void FFTReorderSimple_7687();
void WEIGHTED_ROUND_ROBIN_Splitter_7805();
void FFTReorderSimple_7807();
void FFTReorderSimple_7808();
void WEIGHTED_ROUND_ROBIN_Joiner_7806();
void WEIGHTED_ROUND_ROBIN_Splitter_7809();
void FFTReorderSimple_7811();
void FFTReorderSimple_7812();
void FFTReorderSimple_7813();
void FFTReorderSimple_7814();
void WEIGHTED_ROUND_ROBIN_Joiner_7810();
void WEIGHTED_ROUND_ROBIN_Splitter_7815();
void FFTReorderSimple_7817();
void FFTReorderSimple_7818();
void FFTReorderSimple_7819();
void FFTReorderSimple_7820();
void FFTReorderSimple_7821();
void FFTReorderSimple_7822();
void FFTReorderSimple_7823();
void FFTReorderSimple_7824();
void WEIGHTED_ROUND_ROBIN_Joiner_7816();
void WEIGHTED_ROUND_ROBIN_Splitter_7825();
void FFTReorderSimple_7827();
void FFTReorderSimple_7828();
void FFTReorderSimple_7829();
void FFTReorderSimple_7830();
void FFTReorderSimple_7831();
void FFTReorderSimple_7832();
void FFTReorderSimple_7833();
void FFTReorderSimple_7834();
void FFTReorderSimple_7835();
void FFTReorderSimple_7836();
void FFTReorderSimple_7837();
void FFTReorderSimple_7838();
void FFTReorderSimple_7839();
void FFTReorderSimple_7840();
void FFTReorderSimple_7841();
void FFTReorderSimple_7842();
void WEIGHTED_ROUND_ROBIN_Joiner_7826();
void WEIGHTED_ROUND_ROBIN_Splitter_7843();
void CombineDFT_7845();
void CombineDFT_7846();
void CombineDFT_7847();
void CombineDFT_7848();
void CombineDFT_7849();
void CombineDFT_7850();
void CombineDFT_7851();
void CombineDFT_7852();
void CombineDFT_7853();
void CombineDFT_7854();
void CombineDFT_7855();
void CombineDFT_7856();
void CombineDFT_7857();
void CombineDFT_7858();
void CombineDFT_7859();
void CombineDFT_7860();
void WEIGHTED_ROUND_ROBIN_Joiner_7844();
void WEIGHTED_ROUND_ROBIN_Splitter_7861();
void CombineDFT_7863();
void CombineDFT_7864();
void CombineDFT_7865();
void CombineDFT_7866();
void CombineDFT_7867();
void CombineDFT_7868();
void CombineDFT_7869();
void CombineDFT_7870();
void CombineDFT_7871();
void CombineDFT_7872();
void CombineDFT_7873();
void CombineDFT_7874();
void CombineDFT_7875();
void CombineDFT_7876();
void CombineDFT_7877();
void CombineDFT_7878();
void WEIGHTED_ROUND_ROBIN_Joiner_7862();
void WEIGHTED_ROUND_ROBIN_Splitter_7879();
void CombineDFT_7881();
void CombineDFT_7882();
void CombineDFT_7883();
void CombineDFT_7884();
void CombineDFT_7885();
void CombineDFT_7886();
void CombineDFT_7887();
void CombineDFT_7888();
void WEIGHTED_ROUND_ROBIN_Joiner_7880();
void WEIGHTED_ROUND_ROBIN_Splitter_7889();
void CombineDFT_7891();
void CombineDFT_7892();
void CombineDFT_7893();
void CombineDFT_7894();
void WEIGHTED_ROUND_ROBIN_Joiner_7890();
void WEIGHTED_ROUND_ROBIN_Splitter_7895();
void CombineDFT_7897();
void CombineDFT_7898();
void WEIGHTED_ROUND_ROBIN_Joiner_7896();
void CombineDFT_7697();
void WEIGHTED_ROUND_ROBIN_Joiner_7700();
void FloatPrinter(buffer_float_t *chanin);
void FloatPrinter_7698();

#ifdef __cplusplus
}
#endif
#endif
