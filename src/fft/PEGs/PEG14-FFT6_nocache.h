#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=896 on the compile command line
#else
#if BUF_SIZEMAX < 896
#error BUF_SIZEMAX too small, it must be at least 896
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	complex_t wn;
} CombineDFT_4139_t;
void FFTTestSource_4087();
void FFTReorderSimple_4088();
void WEIGHTED_ROUND_ROBIN_Splitter_4101();
void FFTReorderSimple_4103();
void FFTReorderSimple_4104();
void WEIGHTED_ROUND_ROBIN_Joiner_4102();
void WEIGHTED_ROUND_ROBIN_Splitter_4105();
void FFTReorderSimple_4107();
void FFTReorderSimple_4108();
void FFTReorderSimple_4109();
void FFTReorderSimple_4110();
void WEIGHTED_ROUND_ROBIN_Joiner_4106();
void WEIGHTED_ROUND_ROBIN_Splitter_4111();
void FFTReorderSimple_4113();
void FFTReorderSimple_4114();
void FFTReorderSimple_4115();
void FFTReorderSimple_4116();
void FFTReorderSimple_4117();
void FFTReorderSimple_4118();
void FFTReorderSimple_4119();
void FFTReorderSimple_4120();
void WEIGHTED_ROUND_ROBIN_Joiner_4112();
void WEIGHTED_ROUND_ROBIN_Splitter_4121();
void FFTReorderSimple_4123();
void FFTReorderSimple_4124();
void FFTReorderSimple_4125();
void FFTReorderSimple_4126();
void FFTReorderSimple_4127();
void FFTReorderSimple_4128();
void FFTReorderSimple_4129();
void FFTReorderSimple_4130();
void FFTReorderSimple_4131();
void FFTReorderSimple_4132();
void FFTReorderSimple_4133();
void FFTReorderSimple_4134();
void FFTReorderSimple_4135();
void FFTReorderSimple_4136();
void WEIGHTED_ROUND_ROBIN_Joiner_4122();
void WEIGHTED_ROUND_ROBIN_Splitter_4137();
void CombineDFT_4139();
void CombineDFT_4140();
void CombineDFT_4141();
void CombineDFT_4142();
void CombineDFT_4143();
void CombineDFT_4144();
void CombineDFT_4145();
void CombineDFT_4146();
void CombineDFT_4147();
void CombineDFT_4148();
void CombineDFT_4149();
void CombineDFT_4150();
void CombineDFT_4151();
void CombineDFT_4152();
void WEIGHTED_ROUND_ROBIN_Joiner_4138();
void WEIGHTED_ROUND_ROBIN_Splitter_4153();
void CombineDFT_4155();
void CombineDFT_4156();
void CombineDFT_4157();
void CombineDFT_4158();
void CombineDFT_4159();
void CombineDFT_4160();
void CombineDFT_4161();
void CombineDFT_4162();
void CombineDFT_4163();
void CombineDFT_4164();
void CombineDFT_4165();
void CombineDFT_4166();
void CombineDFT_4167();
void CombineDFT_4168();
void WEIGHTED_ROUND_ROBIN_Joiner_4154();
void WEIGHTED_ROUND_ROBIN_Splitter_4169();
void CombineDFT_4171();
void CombineDFT_4172();
void CombineDFT_4173();
void CombineDFT_4174();
void CombineDFT_4175();
void CombineDFT_4176();
void CombineDFT_4177();
void CombineDFT_4178();
void WEIGHTED_ROUND_ROBIN_Joiner_4170();
void WEIGHTED_ROUND_ROBIN_Splitter_4179();
void CombineDFT_4181();
void CombineDFT_4182();
void CombineDFT_4183();
void CombineDFT_4184();
void WEIGHTED_ROUND_ROBIN_Joiner_4180();
void WEIGHTED_ROUND_ROBIN_Splitter_4185();
void CombineDFT_4187();
void CombineDFT_4188();
void WEIGHTED_ROUND_ROBIN_Joiner_4186();
void CombineDFT_4098();
void CPrinter_4099();

#ifdef __cplusplus
}
#endif
#endif
