#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=2176 on the compile command line
#else
#if BUF_SIZEMAX < 2176
#error BUF_SIZEMAX too small, it must be at least 2176
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	complex_t wn;
} CombineDFT_3521_t;
void FFTTestSource_3467();
void FFTReorderSimple_3468();
void WEIGHTED_ROUND_ROBIN_Splitter_3481();
void FFTReorderSimple_3483();
void FFTReorderSimple_3484();
void WEIGHTED_ROUND_ROBIN_Joiner_3482();
void WEIGHTED_ROUND_ROBIN_Splitter_3485();
void FFTReorderSimple_3487();
void FFTReorderSimple_3488();
void FFTReorderSimple_3489();
void FFTReorderSimple_3490();
void WEIGHTED_ROUND_ROBIN_Joiner_3486();
void WEIGHTED_ROUND_ROBIN_Splitter_3491();
void FFTReorderSimple_3493();
void FFTReorderSimple_3494();
void FFTReorderSimple_3495();
void FFTReorderSimple_3496();
void FFTReorderSimple_3497();
void FFTReorderSimple_3498();
void FFTReorderSimple_3499();
void FFTReorderSimple_3500();
void WEIGHTED_ROUND_ROBIN_Joiner_3492();
void WEIGHTED_ROUND_ROBIN_Splitter_3501();
void FFTReorderSimple_3503();
void FFTReorderSimple_3504();
void FFTReorderSimple_3505();
void FFTReorderSimple_3506();
void FFTReorderSimple_3507();
void FFTReorderSimple_3508();
void FFTReorderSimple_3509();
void FFTReorderSimple_3510();
void FFTReorderSimple_3511();
void FFTReorderSimple_3512();
void FFTReorderSimple_3513();
void FFTReorderSimple_3514();
void FFTReorderSimple_3515();
void FFTReorderSimple_3516();
void FFTReorderSimple_3517();
void FFTReorderSimple_3518();
void WEIGHTED_ROUND_ROBIN_Joiner_3502();
void WEIGHTED_ROUND_ROBIN_Splitter_3519();
void CombineDFT_3521();
void CombineDFT_3522();
void CombineDFT_3523();
void CombineDFT_3524();
void CombineDFT_3525();
void CombineDFT_3526();
void CombineDFT_3527();
void CombineDFT_3528();
void CombineDFT_3529();
void CombineDFT_3530();
void CombineDFT_3531();
void CombineDFT_3532();
void CombineDFT_3533();
void CombineDFT_3534();
void CombineDFT_3535();
void CombineDFT_3536();
void CombineDFT_3537();
void WEIGHTED_ROUND_ROBIN_Joiner_3520();
void WEIGHTED_ROUND_ROBIN_Splitter_3538();
void CombineDFT_3540();
void CombineDFT_3541();
void CombineDFT_3542();
void CombineDFT_3543();
void CombineDFT_3544();
void CombineDFT_3545();
void CombineDFT_3546();
void CombineDFT_3547();
void CombineDFT_3548();
void CombineDFT_3549();
void CombineDFT_3550();
void CombineDFT_3551();
void CombineDFT_3552();
void CombineDFT_3553();
void CombineDFT_3554();
void CombineDFT_3555();
void WEIGHTED_ROUND_ROBIN_Joiner_3539();
void WEIGHTED_ROUND_ROBIN_Splitter_3556();
void CombineDFT_3558();
void CombineDFT_3559();
void CombineDFT_3560();
void CombineDFT_3561();
void CombineDFT_3562();
void CombineDFT_3563();
void CombineDFT_3564();
void CombineDFT_3565();
void WEIGHTED_ROUND_ROBIN_Joiner_3557();
void WEIGHTED_ROUND_ROBIN_Splitter_3566();
void CombineDFT_3568();
void CombineDFT_3569();
void CombineDFT_3570();
void CombineDFT_3571();
void WEIGHTED_ROUND_ROBIN_Joiner_3567();
void WEIGHTED_ROUND_ROBIN_Splitter_3572();
void CombineDFT_3574();
void CombineDFT_3575();
void WEIGHTED_ROUND_ROBIN_Joiner_3573();
void CombineDFT_3478();
void CPrinter_3479();

#ifdef __cplusplus
}
#endif
#endif
