#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=896 on the compile command line
#else
#if BUF_SIZEMAX < 896
#error BUF_SIZEMAX too small, it must be at least 896
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float A_re[32];
	float A_im[32];
	int idx;
} FloatSource_8030_t;
void FloatSource_8030();
void Pre_CollapsedDataParallel_1_8331();
void WEIGHTED_ROUND_ROBIN_Splitter_8473();
void Butterfly_8475();
void Butterfly_8476();
void Butterfly_8477();
void Butterfly_8478();
void Butterfly_8479();
void Butterfly_8480();
void Butterfly_8481();
void WEIGHTED_ROUND_ROBIN_Joiner_8474();
void Post_CollapsedDataParallel_2_8332();
void WEIGHTED_ROUND_ROBIN_Splitter_8375();
void Pre_CollapsedDataParallel_1_8334();
void WEIGHTED_ROUND_ROBIN_Splitter_8482();
void Butterfly_8484();
void Butterfly_8485();
void Butterfly_8486();
void Butterfly_8487();
void Butterfly_8488();
void Butterfly_8489();
void Butterfly_8490();
void WEIGHTED_ROUND_ROBIN_Joiner_8483();
void Post_CollapsedDataParallel_2_8335();
void WEIGHTED_ROUND_ROBIN_Splitter_8455();
void Pre_CollapsedDataParallel_1_8340();
void WEIGHTED_ROUND_ROBIN_Splitter_8491();
void Butterfly_8493();
void Butterfly_8494();
void Butterfly_8495();
void Butterfly_8496();
void WEIGHTED_ROUND_ROBIN_Joiner_8492();
void Post_CollapsedDataParallel_2_8341();
void Pre_CollapsedDataParallel_1_8343();
void WEIGHTED_ROUND_ROBIN_Splitter_8497();
void Butterfly_8499();
void Butterfly_8500();
void Butterfly_8501();
void Butterfly_8502();
void WEIGHTED_ROUND_ROBIN_Joiner_8498();
void Post_CollapsedDataParallel_2_8344();
void WEIGHTED_ROUND_ROBIN_Joiner_8456();
void Pre_CollapsedDataParallel_1_8337();
void WEIGHTED_ROUND_ROBIN_Splitter_8503();
void Butterfly_8505();
void Butterfly_8506();
void Butterfly_8507();
void Butterfly_8508();
void Butterfly_8509();
void Butterfly_8510();
void Butterfly_8511();
void WEIGHTED_ROUND_ROBIN_Joiner_8504();
void Post_CollapsedDataParallel_2_8338();
void WEIGHTED_ROUND_ROBIN_Splitter_8457();
void Pre_CollapsedDataParallel_1_8346();
void WEIGHTED_ROUND_ROBIN_Splitter_8512();
void Butterfly_8514();
void Butterfly_8515();
void Butterfly_8516();
void Butterfly_8517();
void WEIGHTED_ROUND_ROBIN_Joiner_8513();
void Post_CollapsedDataParallel_2_8347();
void Pre_CollapsedDataParallel_1_8349();
void WEIGHTED_ROUND_ROBIN_Splitter_8518();
void Butterfly_8520();
void Butterfly_8521();
void Butterfly_8522();
void Butterfly_8523();
void WEIGHTED_ROUND_ROBIN_Joiner_8519();
void Post_CollapsedDataParallel_2_8350();
void WEIGHTED_ROUND_ROBIN_Joiner_8458();
void WEIGHTED_ROUND_ROBIN_Joiner_8459();
void WEIGHTED_ROUND_ROBIN_Splitter_8460();
void WEIGHTED_ROUND_ROBIN_Splitter_8461();
void Pre_CollapsedDataParallel_1_8352();
void WEIGHTED_ROUND_ROBIN_Splitter_8524();
void Butterfly_8526();
void Butterfly_8527();
void WEIGHTED_ROUND_ROBIN_Joiner_8525();
void Post_CollapsedDataParallel_2_8353();
void Pre_CollapsedDataParallel_1_8355();
void WEIGHTED_ROUND_ROBIN_Splitter_8528();
void Butterfly_8530();
void Butterfly_8531();
void WEIGHTED_ROUND_ROBIN_Joiner_8529();
void Post_CollapsedDataParallel_2_8356();
void Pre_CollapsedDataParallel_1_8358();
void WEIGHTED_ROUND_ROBIN_Splitter_8532();
void Butterfly_8534();
void Butterfly_8535();
void WEIGHTED_ROUND_ROBIN_Joiner_8533();
void Post_CollapsedDataParallel_2_8359();
void Pre_CollapsedDataParallel_1_8361();
void WEIGHTED_ROUND_ROBIN_Splitter_8536();
void Butterfly_8538();
void Butterfly_8539();
void WEIGHTED_ROUND_ROBIN_Joiner_8537();
void Post_CollapsedDataParallel_2_8362();
void WEIGHTED_ROUND_ROBIN_Joiner_8462();
void WEIGHTED_ROUND_ROBIN_Splitter_8463();
void Pre_CollapsedDataParallel_1_8364();
void WEIGHTED_ROUND_ROBIN_Splitter_8540();
void Butterfly_8542();
void Butterfly_8543();
void WEIGHTED_ROUND_ROBIN_Joiner_8541();
void Post_CollapsedDataParallel_2_8365();
void Pre_CollapsedDataParallel_1_8367();
void WEIGHTED_ROUND_ROBIN_Splitter_8544();
void Butterfly_8546();
void Butterfly_8547();
void WEIGHTED_ROUND_ROBIN_Joiner_8545();
void Post_CollapsedDataParallel_2_8368();
void Pre_CollapsedDataParallel_1_8370();
void WEIGHTED_ROUND_ROBIN_Splitter_8548();
void Butterfly_8550();
void Butterfly_8551();
void WEIGHTED_ROUND_ROBIN_Joiner_8549();
void Post_CollapsedDataParallel_2_8371();
void Pre_CollapsedDataParallel_1_8373();
void WEIGHTED_ROUND_ROBIN_Splitter_8552();
void Butterfly_8554();
void Butterfly_8555();
void WEIGHTED_ROUND_ROBIN_Joiner_8553();
void Post_CollapsedDataParallel_2_8374();
void WEIGHTED_ROUND_ROBIN_Joiner_8464();
void WEIGHTED_ROUND_ROBIN_Joiner_8465();
void WEIGHTED_ROUND_ROBIN_Splitter_8466();
void WEIGHTED_ROUND_ROBIN_Splitter_8467();
void Butterfly_8095();
void Butterfly_8096();
void Butterfly_8097();
void Butterfly_8098();
void Butterfly_8099();
void Butterfly_8100();
void Butterfly_8101();
void Butterfly_8102();
void WEIGHTED_ROUND_ROBIN_Joiner_8468();
void WEIGHTED_ROUND_ROBIN_Splitter_8469();
void Butterfly_8103();
void Butterfly_8104();
void Butterfly_8105();
void Butterfly_8106();
void Butterfly_8107();
void Butterfly_8108();
void Butterfly_8109();
void Butterfly_8110();
void WEIGHTED_ROUND_ROBIN_Joiner_8470();
void WEIGHTED_ROUND_ROBIN_Joiner_8471();
int BitReverse_8111_bitrev(int inp, int numbits);
void BitReverse_8111();
void FloatPrinter_8112();

#ifdef __cplusplus
}
#endif
#endif
