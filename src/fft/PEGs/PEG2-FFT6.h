#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=128 on the compile command line
#else
#if BUF_SIZEMAX < 128
#error BUF_SIZEMAX too small, it must be at least 128
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	complex_t wn;
} CombineDFT_6011_t;
void FFTTestSource(buffer_complex_t *chanout);
void FFTTestSource_5979();
void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout);
void FFTReorderSimple_5980();
void WEIGHTED_ROUND_ROBIN_Splitter_5993();
void FFTReorderSimple_5995();
void FFTReorderSimple_5996();
void WEIGHTED_ROUND_ROBIN_Joiner_5994();
void WEIGHTED_ROUND_ROBIN_Splitter_5997();
void FFTReorderSimple_5999();
void FFTReorderSimple_6000();
void WEIGHTED_ROUND_ROBIN_Joiner_5998();
void WEIGHTED_ROUND_ROBIN_Splitter_6001();
void FFTReorderSimple_6003();
void FFTReorderSimple_6004();
void WEIGHTED_ROUND_ROBIN_Joiner_6002();
void WEIGHTED_ROUND_ROBIN_Splitter_6005();
void FFTReorderSimple_6007();
void FFTReorderSimple_6008();
void WEIGHTED_ROUND_ROBIN_Joiner_6006();
void WEIGHTED_ROUND_ROBIN_Splitter_6009();
void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout);
void CombineDFT_6011();
void CombineDFT_6012();
void WEIGHTED_ROUND_ROBIN_Joiner_6010();
void WEIGHTED_ROUND_ROBIN_Splitter_6013();
void CombineDFT_6015();
void CombineDFT_6016();
void WEIGHTED_ROUND_ROBIN_Joiner_6014();
void WEIGHTED_ROUND_ROBIN_Splitter_6017();
void CombineDFT_6019();
void CombineDFT_6020();
void WEIGHTED_ROUND_ROBIN_Joiner_6018();
void WEIGHTED_ROUND_ROBIN_Splitter_6021();
void CombineDFT_6023();
void CombineDFT_6024();
void WEIGHTED_ROUND_ROBIN_Joiner_6022();
void WEIGHTED_ROUND_ROBIN_Splitter_6025();
void CombineDFT_6027();
void CombineDFT_6028();
void WEIGHTED_ROUND_ROBIN_Joiner_6026();
void CombineDFT_5990();
void CPrinter(buffer_complex_t *chanin);
void CPrinter_5991();

#ifdef __cplusplus
}
#endif
#endif
