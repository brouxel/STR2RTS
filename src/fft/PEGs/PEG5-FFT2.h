#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=2560 on the compile command line
#else
#if BUF_SIZEMAX < 2560
#error BUF_SIZEMAX too small, it must be at least 2560
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float w[2];
} CombineDFT_11838_t;

typedef struct {
	float w[4];
} CombineDFT_11845_t;

typedef struct {
	float w[8];
} CombineDFT_11852_t;

typedef struct {
	float w[16];
} CombineDFT_11859_t;

typedef struct {
	float w[32];
} CombineDFT_11865_t;

typedef struct {
	float w[64];
} CombineDFT_11787_t;
void WEIGHTED_ROUND_ROBIN_Splitter_11808();
void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout);
void FFTTestSource_11810();
void FFTTestSource_11811();
void WEIGHTED_ROUND_ROBIN_Joiner_11809();
void WEIGHTED_ROUND_ROBIN_Splitter_11800();
void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout);
void FFTReorderSimple_11777();
void WEIGHTED_ROUND_ROBIN_Splitter_11812();
void FFTReorderSimple_11814();
void FFTReorderSimple_11815();
void WEIGHTED_ROUND_ROBIN_Joiner_11813();
void WEIGHTED_ROUND_ROBIN_Splitter_11816();
void FFTReorderSimple_11818();
void FFTReorderSimple_11819();
void FFTReorderSimple_11820();
void FFTReorderSimple_11821();
void WEIGHTED_ROUND_ROBIN_Joiner_11817();
void WEIGHTED_ROUND_ROBIN_Splitter_11822();
void FFTReorderSimple_11824();
void FFTReorderSimple_11825();
void FFTReorderSimple_11826();
void FFTReorderSimple_11827();
void FFTReorderSimple_11828();
void WEIGHTED_ROUND_ROBIN_Joiner_11823();
void WEIGHTED_ROUND_ROBIN_Splitter_11829();
void FFTReorderSimple_11831();
void FFTReorderSimple_11832();
void FFTReorderSimple_11833();
void FFTReorderSimple_11834();
void FFTReorderSimple_11835();
void WEIGHTED_ROUND_ROBIN_Joiner_11830();
void WEIGHTED_ROUND_ROBIN_Splitter_11836();
void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout);
void CombineDFT_11838();
void CombineDFT_11839();
void CombineDFT_11840();
void CombineDFT_11841();
void CombineDFT_11842();
void WEIGHTED_ROUND_ROBIN_Joiner_11837();
void WEIGHTED_ROUND_ROBIN_Splitter_11843();
void CombineDFT_11845();
void CombineDFT_11846();
void CombineDFT_11847();
void CombineDFT_11848();
void CombineDFT_11849();
void WEIGHTED_ROUND_ROBIN_Joiner_11844();
void WEIGHTED_ROUND_ROBIN_Splitter_11850();
void CombineDFT_11852();
void CombineDFT_11853();
void CombineDFT_11854();
void CombineDFT_11855();
void CombineDFT_11856();
void WEIGHTED_ROUND_ROBIN_Joiner_11851();
void WEIGHTED_ROUND_ROBIN_Splitter_11857();
void CombineDFT_11859();
void CombineDFT_11860();
void CombineDFT_11861();
void CombineDFT_11862();
void WEIGHTED_ROUND_ROBIN_Joiner_11858();
void WEIGHTED_ROUND_ROBIN_Splitter_11863();
void CombineDFT_11865();
void CombineDFT_11866();
void WEIGHTED_ROUND_ROBIN_Joiner_11864();
void CombineDFT_11787();
void FFTReorderSimple_11788();
void WEIGHTED_ROUND_ROBIN_Splitter_11867();
void FFTReorderSimple_11869();
void FFTReorderSimple_11870();
void WEIGHTED_ROUND_ROBIN_Joiner_11868();
void WEIGHTED_ROUND_ROBIN_Splitter_11871();
void FFTReorderSimple_11873();
void FFTReorderSimple_11874();
void FFTReorderSimple_11875();
void FFTReorderSimple_11876();
void WEIGHTED_ROUND_ROBIN_Joiner_11872();
void WEIGHTED_ROUND_ROBIN_Splitter_11877();
void FFTReorderSimple_11879();
void FFTReorderSimple_11880();
void FFTReorderSimple_11881();
void FFTReorderSimple_11882();
void FFTReorderSimple_11883();
void WEIGHTED_ROUND_ROBIN_Joiner_11878();
void WEIGHTED_ROUND_ROBIN_Splitter_11884();
void FFTReorderSimple_11886();
void FFTReorderSimple_11887();
void FFTReorderSimple_11888();
void FFTReorderSimple_11889();
void FFTReorderSimple_11890();
void WEIGHTED_ROUND_ROBIN_Joiner_11885();
void WEIGHTED_ROUND_ROBIN_Splitter_11891();
void CombineDFT_11893();
void CombineDFT_11894();
void CombineDFT_11895();
void CombineDFT_11896();
void CombineDFT_11897();
void WEIGHTED_ROUND_ROBIN_Joiner_11892();
void WEIGHTED_ROUND_ROBIN_Splitter_11898();
void CombineDFT_11900();
void CombineDFT_11901();
void CombineDFT_11902();
void CombineDFT_11903();
void CombineDFT_11904();
void WEIGHTED_ROUND_ROBIN_Joiner_11899();
void WEIGHTED_ROUND_ROBIN_Splitter_11905();
void CombineDFT_11907();
void CombineDFT_11908();
void CombineDFT_11909();
void CombineDFT_11910();
void CombineDFT_11911();
void WEIGHTED_ROUND_ROBIN_Joiner_11906();
void WEIGHTED_ROUND_ROBIN_Splitter_11912();
void CombineDFT_11914();
void CombineDFT_11915();
void CombineDFT_11916();
void CombineDFT_11917();
void WEIGHTED_ROUND_ROBIN_Joiner_11913();
void WEIGHTED_ROUND_ROBIN_Splitter_11918();
void CombineDFT_11920();
void CombineDFT_11921();
void WEIGHTED_ROUND_ROBIN_Joiner_11919();
void CombineDFT_11798();
void WEIGHTED_ROUND_ROBIN_Joiner_11801();
void FloatPrinter(buffer_float_t *chanin);
void FloatPrinter_11799();

#ifdef __cplusplus
}
#endif
#endif
