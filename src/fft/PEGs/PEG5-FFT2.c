#include "PEG5-FFT2.h"

buffer_float_t SplitJoin18_CombineDFT_Fiss_11931_11952_join[4];
buffer_float_t SplitJoin63_CombineDFT_Fiss_11939_11960_join[5];
buffer_float_t SplitJoin57_FFTReorderSimple_Fiss_11936_11957_split[5];
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_11924_11945_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11872WEIGHTED_ROUND_ROBIN_Splitter_11877;
buffer_float_t SplitJoin67_CombineDFT_Fiss_11941_11962_split[2];
buffer_float_t FFTReorderSimple_11777WEIGHTED_ROUND_ROBIN_Splitter_11812;
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11766_11802_11923_11944_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11899WEIGHTED_ROUND_ROBIN_Splitter_11905;
buffer_float_t SplitJoin59_CombineDFT_Fiss_11937_11958_split[5];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11864CombineDFT_11787;
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_11925_11946_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11858WEIGHTED_ROUND_ROBIN_Splitter_11863;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11817WEIGHTED_ROUND_ROBIN_Splitter_11822;
buffer_float_t SplitJoin51_FFTReorderSimple_Fiss_11933_11954_join[2];
buffer_float_t SplitJoin16_CombineDFT_Fiss_11930_11951_split[5];
buffer_float_t SplitJoin16_CombineDFT_Fiss_11930_11951_join[5];
buffer_float_t SplitJoin14_CombineDFT_Fiss_11929_11950_join[5];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11844WEIGHTED_ROUND_ROBIN_Splitter_11850;
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_11926_11947_split[5];
buffer_float_t SplitJoin55_FFTReorderSimple_Fiss_11935_11956_split[5];
buffer_float_t FFTReorderSimple_11788WEIGHTED_ROUND_ROBIN_Splitter_11867;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11885WEIGHTED_ROUND_ROBIN_Splitter_11891;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11830WEIGHTED_ROUND_ROBIN_Splitter_11836;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11837WEIGHTED_ROUND_ROBIN_Splitter_11843;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11913WEIGHTED_ROUND_ROBIN_Splitter_11918;
buffer_float_t SplitJoin14_CombineDFT_Fiss_11929_11950_split[5];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11868WEIGHTED_ROUND_ROBIN_Splitter_11871;
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_11927_11948_join[5];
buffer_float_t SplitJoin18_CombineDFT_Fiss_11931_11952_split[4];
buffer_float_t SplitJoin53_FFTReorderSimple_Fiss_11934_11955_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11892WEIGHTED_ROUND_ROBIN_Splitter_11898;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11919CombineDFT_11798;
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_11927_11948_split[5];
buffer_float_t SplitJoin57_FFTReorderSimple_Fiss_11936_11957_join[5];
buffer_float_t SplitJoin61_CombineDFT_Fiss_11938_11959_split[5];
buffer_float_t SplitJoin20_CombineDFT_Fiss_11932_11953_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11801FloatPrinter_11799;
buffer_float_t SplitJoin53_FFTReorderSimple_Fiss_11934_11955_split[4];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11766_11802_11923_11944_split[2];
buffer_float_t SplitJoin12_CombineDFT_Fiss_11928_11949_split[5];
buffer_float_t SplitJoin0_FFTTestSource_Fiss_11922_11943_split[2];
buffer_float_t SplitJoin12_CombineDFT_Fiss_11928_11949_join[5];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11813WEIGHTED_ROUND_ROBIN_Splitter_11816;
buffer_float_t SplitJoin55_FFTReorderSimple_Fiss_11935_11956_join[5];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11823WEIGHTED_ROUND_ROBIN_Splitter_11829;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11878WEIGHTED_ROUND_ROBIN_Splitter_11884;
buffer_float_t SplitJoin0_FFTTestSource_Fiss_11922_11943_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11906WEIGHTED_ROUND_ROBIN_Splitter_11912;
buffer_float_t SplitJoin63_CombineDFT_Fiss_11939_11960_split[5];
buffer_float_t SplitJoin65_CombineDFT_Fiss_11940_11961_split[4];
buffer_float_t SplitJoin67_CombineDFT_Fiss_11941_11962_join[2];
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_11925_11946_split[4];
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_11926_11947_join[5];
buffer_float_t SplitJoin51_FFTReorderSimple_Fiss_11933_11954_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11809WEIGHTED_ROUND_ROBIN_Splitter_11800;
buffer_float_t SplitJoin61_CombineDFT_Fiss_11938_11959_join[5];
buffer_float_t SplitJoin65_CombineDFT_Fiss_11940_11961_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11851WEIGHTED_ROUND_ROBIN_Splitter_11857;
buffer_float_t SplitJoin20_CombineDFT_Fiss_11932_11953_join[2];
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_11924_11945_split[2];
buffer_float_t SplitJoin59_CombineDFT_Fiss_11937_11958_join[5];


CombineDFT_11838_t CombineDFT_11838_s;
CombineDFT_11838_t CombineDFT_11839_s;
CombineDFT_11838_t CombineDFT_11840_s;
CombineDFT_11838_t CombineDFT_11841_s;
CombineDFT_11838_t CombineDFT_11842_s;
CombineDFT_11845_t CombineDFT_11845_s;
CombineDFT_11845_t CombineDFT_11846_s;
CombineDFT_11845_t CombineDFT_11847_s;
CombineDFT_11845_t CombineDFT_11848_s;
CombineDFT_11845_t CombineDFT_11849_s;
CombineDFT_11852_t CombineDFT_11852_s;
CombineDFT_11852_t CombineDFT_11853_s;
CombineDFT_11852_t CombineDFT_11854_s;
CombineDFT_11852_t CombineDFT_11855_s;
CombineDFT_11852_t CombineDFT_11856_s;
CombineDFT_11859_t CombineDFT_11859_s;
CombineDFT_11859_t CombineDFT_11860_s;
CombineDFT_11859_t CombineDFT_11861_s;
CombineDFT_11859_t CombineDFT_11862_s;
CombineDFT_11865_t CombineDFT_11865_s;
CombineDFT_11865_t CombineDFT_11866_s;
CombineDFT_11787_t CombineDFT_11787_s;
CombineDFT_11838_t CombineDFT_11893_s;
CombineDFT_11838_t CombineDFT_11894_s;
CombineDFT_11838_t CombineDFT_11895_s;
CombineDFT_11838_t CombineDFT_11896_s;
CombineDFT_11838_t CombineDFT_11897_s;
CombineDFT_11845_t CombineDFT_11900_s;
CombineDFT_11845_t CombineDFT_11901_s;
CombineDFT_11845_t CombineDFT_11902_s;
CombineDFT_11845_t CombineDFT_11903_s;
CombineDFT_11845_t CombineDFT_11904_s;
CombineDFT_11852_t CombineDFT_11907_s;
CombineDFT_11852_t CombineDFT_11908_s;
CombineDFT_11852_t CombineDFT_11909_s;
CombineDFT_11852_t CombineDFT_11910_s;
CombineDFT_11852_t CombineDFT_11911_s;
CombineDFT_11859_t CombineDFT_11914_s;
CombineDFT_11859_t CombineDFT_11915_s;
CombineDFT_11859_t CombineDFT_11916_s;
CombineDFT_11859_t CombineDFT_11917_s;
CombineDFT_11865_t CombineDFT_11920_s;
CombineDFT_11865_t CombineDFT_11921_s;
CombineDFT_11787_t CombineDFT_11798_s;

void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), 0.0) ; 
		push_float(&(*chanout), 0.0) ; 
		push_float(&(*chanout), 1.0) ; 
		push_float(&(*chanout), 0.0) ; 
		FOR(int, i, 0,  < , 124, i++) {
			push_float(&(*chanout), 0.0) ; 
		}
		ENDFOR
	}


void FFTTestSource_11810() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_11922_11943_split[0]), &(SplitJoin0_FFTTestSource_Fiss_11922_11943_join[0]));
	ENDFOR
}

void FFTTestSource_11811() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_11922_11943_split[1]), &(SplitJoin0_FFTTestSource_Fiss_11922_11943_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11808() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_11809() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11809WEIGHTED_ROUND_ROBIN_Splitter_11800, pop_float(&SplitJoin0_FFTTestSource_Fiss_11922_11943_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11809WEIGHTED_ROUND_ROBIN_Splitter_11800, pop_float(&SplitJoin0_FFTTestSource_Fiss_11922_11943_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, i, 0,  < , 128, i = (i + 4)) {
			push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 128, i = (i + 4)) {
			push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_float(&(*chanin)) ; 
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void FFTReorderSimple_11777() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11766_11802_11923_11944_split[0]), &(FFTReorderSimple_11777WEIGHTED_ROUND_ROBIN_Splitter_11812));
	ENDFOR
}

void FFTReorderSimple_11814() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_11924_11945_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_11924_11945_join[0]));
	ENDFOR
}

void FFTReorderSimple_11815() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_11924_11945_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_11924_11945_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11812() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_11924_11945_split[0], pop_float(&FFTReorderSimple_11777WEIGHTED_ROUND_ROBIN_Splitter_11812));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_11924_11945_split[1], pop_float(&FFTReorderSimple_11777WEIGHTED_ROUND_ROBIN_Splitter_11812));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11813() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11813WEIGHTED_ROUND_ROBIN_Splitter_11816, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_11924_11945_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11813WEIGHTED_ROUND_ROBIN_Splitter_11816, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_11924_11945_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_11818() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_11925_11946_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_11925_11946_join[0]));
	ENDFOR
}

void FFTReorderSimple_11819() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_11925_11946_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_11925_11946_join[1]));
	ENDFOR
}

void FFTReorderSimple_11820() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_11925_11946_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_11925_11946_join[2]));
	ENDFOR
}

void FFTReorderSimple_11821() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_11925_11946_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_11925_11946_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11816() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin6_FFTReorderSimple_Fiss_11925_11946_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11813WEIGHTED_ROUND_ROBIN_Splitter_11816));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11817() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11817WEIGHTED_ROUND_ROBIN_Splitter_11822, pop_float(&SplitJoin6_FFTReorderSimple_Fiss_11925_11946_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_11824() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_11926_11947_split[0]), &(SplitJoin8_FFTReorderSimple_Fiss_11926_11947_join[0]));
	ENDFOR
}

void FFTReorderSimple_11825() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_11926_11947_split[1]), &(SplitJoin8_FFTReorderSimple_Fiss_11926_11947_join[1]));
	ENDFOR
}

void FFTReorderSimple_11826() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_11926_11947_split[2]), &(SplitJoin8_FFTReorderSimple_Fiss_11926_11947_join[2]));
	ENDFOR
}

void FFTReorderSimple_11827() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_11926_11947_split[3]), &(SplitJoin8_FFTReorderSimple_Fiss_11926_11947_join[3]));
	ENDFOR
}

void FFTReorderSimple_11828() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_11926_11947_split[4]), &(SplitJoin8_FFTReorderSimple_Fiss_11926_11947_join[4]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11822() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin8_FFTReorderSimple_Fiss_11926_11947_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11817WEIGHTED_ROUND_ROBIN_Splitter_11822));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11823() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11823WEIGHTED_ROUND_ROBIN_Splitter_11829, pop_float(&SplitJoin8_FFTReorderSimple_Fiss_11926_11947_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_11831() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_11927_11948_split[0]), &(SplitJoin10_FFTReorderSimple_Fiss_11927_11948_join[0]));
	ENDFOR
}

void FFTReorderSimple_11832() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_11927_11948_split[1]), &(SplitJoin10_FFTReorderSimple_Fiss_11927_11948_join[1]));
	ENDFOR
}

void FFTReorderSimple_11833() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_11927_11948_split[2]), &(SplitJoin10_FFTReorderSimple_Fiss_11927_11948_join[2]));
	ENDFOR
}

void FFTReorderSimple_11834() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_11927_11948_split[3]), &(SplitJoin10_FFTReorderSimple_Fiss_11927_11948_join[3]));
	ENDFOR
}

void FFTReorderSimple_11835() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_11927_11948_split[4]), &(SplitJoin10_FFTReorderSimple_Fiss_11927_11948_join[4]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11829() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_FFTReorderSimple_Fiss_11927_11948_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11823WEIGHTED_ROUND_ROBIN_Splitter_11829));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11830() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11830WEIGHTED_ROUND_ROBIN_Splitter_11836, pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11927_11948_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&(*chanin), i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&(*chanin), i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&(*chanin), (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&(*chanin), (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11838_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11838_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&(*chanin)) ; 
			push_float(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_11838() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_11928_11949_split[0]), &(SplitJoin12_CombineDFT_Fiss_11928_11949_join[0]));
	ENDFOR
}

void CombineDFT_11839() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_11928_11949_split[1]), &(SplitJoin12_CombineDFT_Fiss_11928_11949_join[1]));
	ENDFOR
}

void CombineDFT_11840() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_11928_11949_split[2]), &(SplitJoin12_CombineDFT_Fiss_11928_11949_join[2]));
	ENDFOR
}

void CombineDFT_11841() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_11928_11949_split[3]), &(SplitJoin12_CombineDFT_Fiss_11928_11949_join[3]));
	ENDFOR
}

void CombineDFT_11842() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_11928_11949_split[4]), &(SplitJoin12_CombineDFT_Fiss_11928_11949_join[4]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11836() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin12_CombineDFT_Fiss_11928_11949_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11830WEIGHTED_ROUND_ROBIN_Splitter_11836));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11837() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11837WEIGHTED_ROUND_ROBIN_Splitter_11843, pop_float(&SplitJoin12_CombineDFT_Fiss_11928_11949_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_11845() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_11929_11950_split[0]), &(SplitJoin14_CombineDFT_Fiss_11929_11950_join[0]));
	ENDFOR
}

void CombineDFT_11846() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_11929_11950_split[1]), &(SplitJoin14_CombineDFT_Fiss_11929_11950_join[1]));
	ENDFOR
}

void CombineDFT_11847() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_11929_11950_split[2]), &(SplitJoin14_CombineDFT_Fiss_11929_11950_join[2]));
	ENDFOR
}

void CombineDFT_11848() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_11929_11950_split[3]), &(SplitJoin14_CombineDFT_Fiss_11929_11950_join[3]));
	ENDFOR
}

void CombineDFT_11849() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_11929_11950_split[4]), &(SplitJoin14_CombineDFT_Fiss_11929_11950_join[4]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11843() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin14_CombineDFT_Fiss_11929_11950_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11837WEIGHTED_ROUND_ROBIN_Splitter_11843));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11844() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11844WEIGHTED_ROUND_ROBIN_Splitter_11850, pop_float(&SplitJoin14_CombineDFT_Fiss_11929_11950_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_11852() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_11930_11951_split[0]), &(SplitJoin16_CombineDFT_Fiss_11930_11951_join[0]));
	ENDFOR
}

void CombineDFT_11853() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_11930_11951_split[1]), &(SplitJoin16_CombineDFT_Fiss_11930_11951_join[1]));
	ENDFOR
}

void CombineDFT_11854() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_11930_11951_split[2]), &(SplitJoin16_CombineDFT_Fiss_11930_11951_join[2]));
	ENDFOR
}

void CombineDFT_11855() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_11930_11951_split[3]), &(SplitJoin16_CombineDFT_Fiss_11930_11951_join[3]));
	ENDFOR
}

void CombineDFT_11856() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_11930_11951_split[4]), &(SplitJoin16_CombineDFT_Fiss_11930_11951_join[4]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11850() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin16_CombineDFT_Fiss_11930_11951_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11844WEIGHTED_ROUND_ROBIN_Splitter_11850));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11851() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11851WEIGHTED_ROUND_ROBIN_Splitter_11857, pop_float(&SplitJoin16_CombineDFT_Fiss_11930_11951_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_11859() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_11931_11952_split[0]), &(SplitJoin18_CombineDFT_Fiss_11931_11952_join[0]));
	ENDFOR
}

void CombineDFT_11860() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_11931_11952_split[1]), &(SplitJoin18_CombineDFT_Fiss_11931_11952_join[1]));
	ENDFOR
}

void CombineDFT_11861() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_11931_11952_split[2]), &(SplitJoin18_CombineDFT_Fiss_11931_11952_join[2]));
	ENDFOR
}

void CombineDFT_11862() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_11931_11952_split[3]), &(SplitJoin18_CombineDFT_Fiss_11931_11952_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11857() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin18_CombineDFT_Fiss_11931_11952_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11851WEIGHTED_ROUND_ROBIN_Splitter_11857));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11858() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11858WEIGHTED_ROUND_ROBIN_Splitter_11863, pop_float(&SplitJoin18_CombineDFT_Fiss_11931_11952_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_11865() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin20_CombineDFT_Fiss_11932_11953_split[0]), &(SplitJoin20_CombineDFT_Fiss_11932_11953_join[0]));
	ENDFOR
}

void CombineDFT_11866() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin20_CombineDFT_Fiss_11932_11953_split[1]), &(SplitJoin20_CombineDFT_Fiss_11932_11953_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11863() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_11932_11953_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11858WEIGHTED_ROUND_ROBIN_Splitter_11863));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_11932_11953_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11858WEIGHTED_ROUND_ROBIN_Splitter_11863));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11864() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11864CombineDFT_11787, pop_float(&SplitJoin20_CombineDFT_Fiss_11932_11953_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11864CombineDFT_11787, pop_float(&SplitJoin20_CombineDFT_Fiss_11932_11953_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_11787() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_11864CombineDFT_11787), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11766_11802_11923_11944_join[0]));
	ENDFOR
}

void FFTReorderSimple_11788() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11766_11802_11923_11944_split[1]), &(FFTReorderSimple_11788WEIGHTED_ROUND_ROBIN_Splitter_11867));
	ENDFOR
}

void FFTReorderSimple_11869() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin51_FFTReorderSimple_Fiss_11933_11954_split[0]), &(SplitJoin51_FFTReorderSimple_Fiss_11933_11954_join[0]));
	ENDFOR
}

void FFTReorderSimple_11870() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin51_FFTReorderSimple_Fiss_11933_11954_split[1]), &(SplitJoin51_FFTReorderSimple_Fiss_11933_11954_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11867() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin51_FFTReorderSimple_Fiss_11933_11954_split[0], pop_float(&FFTReorderSimple_11788WEIGHTED_ROUND_ROBIN_Splitter_11867));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin51_FFTReorderSimple_Fiss_11933_11954_split[1], pop_float(&FFTReorderSimple_11788WEIGHTED_ROUND_ROBIN_Splitter_11867));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11868() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11868WEIGHTED_ROUND_ROBIN_Splitter_11871, pop_float(&SplitJoin51_FFTReorderSimple_Fiss_11933_11954_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11868WEIGHTED_ROUND_ROBIN_Splitter_11871, pop_float(&SplitJoin51_FFTReorderSimple_Fiss_11933_11954_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_11873() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin53_FFTReorderSimple_Fiss_11934_11955_split[0]), &(SplitJoin53_FFTReorderSimple_Fiss_11934_11955_join[0]));
	ENDFOR
}

void FFTReorderSimple_11874() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin53_FFTReorderSimple_Fiss_11934_11955_split[1]), &(SplitJoin53_FFTReorderSimple_Fiss_11934_11955_join[1]));
	ENDFOR
}

void FFTReorderSimple_11875() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin53_FFTReorderSimple_Fiss_11934_11955_split[2]), &(SplitJoin53_FFTReorderSimple_Fiss_11934_11955_join[2]));
	ENDFOR
}

void FFTReorderSimple_11876() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin53_FFTReorderSimple_Fiss_11934_11955_split[3]), &(SplitJoin53_FFTReorderSimple_Fiss_11934_11955_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11871() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin53_FFTReorderSimple_Fiss_11934_11955_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11868WEIGHTED_ROUND_ROBIN_Splitter_11871));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11872() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11872WEIGHTED_ROUND_ROBIN_Splitter_11877, pop_float(&SplitJoin53_FFTReorderSimple_Fiss_11934_11955_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_11879() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin55_FFTReorderSimple_Fiss_11935_11956_split[0]), &(SplitJoin55_FFTReorderSimple_Fiss_11935_11956_join[0]));
	ENDFOR
}

void FFTReorderSimple_11880() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin55_FFTReorderSimple_Fiss_11935_11956_split[1]), &(SplitJoin55_FFTReorderSimple_Fiss_11935_11956_join[1]));
	ENDFOR
}

void FFTReorderSimple_11881() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin55_FFTReorderSimple_Fiss_11935_11956_split[2]), &(SplitJoin55_FFTReorderSimple_Fiss_11935_11956_join[2]));
	ENDFOR
}

void FFTReorderSimple_11882() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin55_FFTReorderSimple_Fiss_11935_11956_split[3]), &(SplitJoin55_FFTReorderSimple_Fiss_11935_11956_join[3]));
	ENDFOR
}

void FFTReorderSimple_11883() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin55_FFTReorderSimple_Fiss_11935_11956_split[4]), &(SplitJoin55_FFTReorderSimple_Fiss_11935_11956_join[4]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11877() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin55_FFTReorderSimple_Fiss_11935_11956_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11872WEIGHTED_ROUND_ROBIN_Splitter_11877));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11878() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11878WEIGHTED_ROUND_ROBIN_Splitter_11884, pop_float(&SplitJoin55_FFTReorderSimple_Fiss_11935_11956_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_11886() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin57_FFTReorderSimple_Fiss_11936_11957_split[0]), &(SplitJoin57_FFTReorderSimple_Fiss_11936_11957_join[0]));
	ENDFOR
}

void FFTReorderSimple_11887() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin57_FFTReorderSimple_Fiss_11936_11957_split[1]), &(SplitJoin57_FFTReorderSimple_Fiss_11936_11957_join[1]));
	ENDFOR
}

void FFTReorderSimple_11888() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin57_FFTReorderSimple_Fiss_11936_11957_split[2]), &(SplitJoin57_FFTReorderSimple_Fiss_11936_11957_join[2]));
	ENDFOR
}

void FFTReorderSimple_11889() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin57_FFTReorderSimple_Fiss_11936_11957_split[3]), &(SplitJoin57_FFTReorderSimple_Fiss_11936_11957_join[3]));
	ENDFOR
}

void FFTReorderSimple_11890() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin57_FFTReorderSimple_Fiss_11936_11957_split[4]), &(SplitJoin57_FFTReorderSimple_Fiss_11936_11957_join[4]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11884() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin57_FFTReorderSimple_Fiss_11936_11957_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11878WEIGHTED_ROUND_ROBIN_Splitter_11884));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11885() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11885WEIGHTED_ROUND_ROBIN_Splitter_11891, pop_float(&SplitJoin57_FFTReorderSimple_Fiss_11936_11957_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_11893() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin59_CombineDFT_Fiss_11937_11958_split[0]), &(SplitJoin59_CombineDFT_Fiss_11937_11958_join[0]));
	ENDFOR
}

void CombineDFT_11894() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin59_CombineDFT_Fiss_11937_11958_split[1]), &(SplitJoin59_CombineDFT_Fiss_11937_11958_join[1]));
	ENDFOR
}

void CombineDFT_11895() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin59_CombineDFT_Fiss_11937_11958_split[2]), &(SplitJoin59_CombineDFT_Fiss_11937_11958_join[2]));
	ENDFOR
}

void CombineDFT_11896() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin59_CombineDFT_Fiss_11937_11958_split[3]), &(SplitJoin59_CombineDFT_Fiss_11937_11958_join[3]));
	ENDFOR
}

void CombineDFT_11897() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin59_CombineDFT_Fiss_11937_11958_split[4]), &(SplitJoin59_CombineDFT_Fiss_11937_11958_join[4]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11891() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin59_CombineDFT_Fiss_11937_11958_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11885WEIGHTED_ROUND_ROBIN_Splitter_11891));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11892() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11892WEIGHTED_ROUND_ROBIN_Splitter_11898, pop_float(&SplitJoin59_CombineDFT_Fiss_11937_11958_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_11900() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin61_CombineDFT_Fiss_11938_11959_split[0]), &(SplitJoin61_CombineDFT_Fiss_11938_11959_join[0]));
	ENDFOR
}

void CombineDFT_11901() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin61_CombineDFT_Fiss_11938_11959_split[1]), &(SplitJoin61_CombineDFT_Fiss_11938_11959_join[1]));
	ENDFOR
}

void CombineDFT_11902() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin61_CombineDFT_Fiss_11938_11959_split[2]), &(SplitJoin61_CombineDFT_Fiss_11938_11959_join[2]));
	ENDFOR
}

void CombineDFT_11903() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin61_CombineDFT_Fiss_11938_11959_split[3]), &(SplitJoin61_CombineDFT_Fiss_11938_11959_join[3]));
	ENDFOR
}

void CombineDFT_11904() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin61_CombineDFT_Fiss_11938_11959_split[4]), &(SplitJoin61_CombineDFT_Fiss_11938_11959_join[4]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11898() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin61_CombineDFT_Fiss_11938_11959_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11892WEIGHTED_ROUND_ROBIN_Splitter_11898));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11899() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11899WEIGHTED_ROUND_ROBIN_Splitter_11905, pop_float(&SplitJoin61_CombineDFT_Fiss_11938_11959_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_11907() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin63_CombineDFT_Fiss_11939_11960_split[0]), &(SplitJoin63_CombineDFT_Fiss_11939_11960_join[0]));
	ENDFOR
}

void CombineDFT_11908() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin63_CombineDFT_Fiss_11939_11960_split[1]), &(SplitJoin63_CombineDFT_Fiss_11939_11960_join[1]));
	ENDFOR
}

void CombineDFT_11909() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin63_CombineDFT_Fiss_11939_11960_split[2]), &(SplitJoin63_CombineDFT_Fiss_11939_11960_join[2]));
	ENDFOR
}

void CombineDFT_11910() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin63_CombineDFT_Fiss_11939_11960_split[3]), &(SplitJoin63_CombineDFT_Fiss_11939_11960_join[3]));
	ENDFOR
}

void CombineDFT_11911() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin63_CombineDFT_Fiss_11939_11960_split[4]), &(SplitJoin63_CombineDFT_Fiss_11939_11960_join[4]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11905() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin63_CombineDFT_Fiss_11939_11960_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11899WEIGHTED_ROUND_ROBIN_Splitter_11905));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11906() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 5, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11906WEIGHTED_ROUND_ROBIN_Splitter_11912, pop_float(&SplitJoin63_CombineDFT_Fiss_11939_11960_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_11914() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin65_CombineDFT_Fiss_11940_11961_split[0]), &(SplitJoin65_CombineDFT_Fiss_11940_11961_join[0]));
	ENDFOR
}

void CombineDFT_11915() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin65_CombineDFT_Fiss_11940_11961_split[1]), &(SplitJoin65_CombineDFT_Fiss_11940_11961_join[1]));
	ENDFOR
}

void CombineDFT_11916() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin65_CombineDFT_Fiss_11940_11961_split[2]), &(SplitJoin65_CombineDFT_Fiss_11940_11961_join[2]));
	ENDFOR
}

void CombineDFT_11917() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin65_CombineDFT_Fiss_11940_11961_split[3]), &(SplitJoin65_CombineDFT_Fiss_11940_11961_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11912() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin65_CombineDFT_Fiss_11940_11961_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11906WEIGHTED_ROUND_ROBIN_Splitter_11912));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11913() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11913WEIGHTED_ROUND_ROBIN_Splitter_11918, pop_float(&SplitJoin65_CombineDFT_Fiss_11940_11961_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_11920() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin67_CombineDFT_Fiss_11941_11962_split[0]), &(SplitJoin67_CombineDFT_Fiss_11941_11962_join[0]));
	ENDFOR
}

void CombineDFT_11921() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin67_CombineDFT_Fiss_11941_11962_split[1]), &(SplitJoin67_CombineDFT_Fiss_11941_11962_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11918() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin67_CombineDFT_Fiss_11941_11962_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11913WEIGHTED_ROUND_ROBIN_Splitter_11918));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin67_CombineDFT_Fiss_11941_11962_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11913WEIGHTED_ROUND_ROBIN_Splitter_11918));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11919() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11919CombineDFT_11798, pop_float(&SplitJoin67_CombineDFT_Fiss_11941_11962_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11919CombineDFT_11798, pop_float(&SplitJoin67_CombineDFT_Fiss_11941_11962_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_11798() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_11919CombineDFT_11798), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11766_11802_11923_11944_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11800() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11766_11802_11923_11944_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11809WEIGHTED_ROUND_ROBIN_Splitter_11800));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11766_11802_11923_11944_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11809WEIGHTED_ROUND_ROBIN_Splitter_11800));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11801() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11801FloatPrinter_11799, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11766_11802_11923_11944_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11801FloatPrinter_11799, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11766_11802_11923_11944_join[1]));
		ENDFOR
	ENDFOR
}}

void FloatPrinter(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void FloatPrinter_11799() {
	FOR(uint32_t, __iter_steady_, 0, <, 1280, __iter_steady_++)
		FloatPrinter(&(WEIGHTED_ROUND_ROBIN_Joiner_11801FloatPrinter_11799));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 4, __iter_init_0_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_11931_11952_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 5, __iter_init_1_++)
		init_buffer_float(&SplitJoin63_CombineDFT_Fiss_11939_11960_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 5, __iter_init_2_++)
		init_buffer_float(&SplitJoin57_FFTReorderSimple_Fiss_11936_11957_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 2, __iter_init_3_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_11924_11945_join[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11872WEIGHTED_ROUND_ROBIN_Splitter_11877);
	FOR(int, __iter_init_4_, 0, <, 2, __iter_init_4_++)
		init_buffer_float(&SplitJoin67_CombineDFT_Fiss_11941_11962_split[__iter_init_4_]);
	ENDFOR
	init_buffer_float(&FFTReorderSimple_11777WEIGHTED_ROUND_ROBIN_Splitter_11812);
	FOR(int, __iter_init_5_, 0, <, 2, __iter_init_5_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11766_11802_11923_11944_join[__iter_init_5_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11899WEIGHTED_ROUND_ROBIN_Splitter_11905);
	FOR(int, __iter_init_6_, 0, <, 5, __iter_init_6_++)
		init_buffer_float(&SplitJoin59_CombineDFT_Fiss_11937_11958_split[__iter_init_6_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11864CombineDFT_11787);
	FOR(int, __iter_init_7_, 0, <, 4, __iter_init_7_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_11925_11946_join[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11858WEIGHTED_ROUND_ROBIN_Splitter_11863);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11817WEIGHTED_ROUND_ROBIN_Splitter_11822);
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_float(&SplitJoin51_FFTReorderSimple_Fiss_11933_11954_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 5, __iter_init_9_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_11930_11951_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 5, __iter_init_10_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_11930_11951_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 5, __iter_init_11_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_11929_11950_join[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11844WEIGHTED_ROUND_ROBIN_Splitter_11850);
	FOR(int, __iter_init_12_, 0, <, 5, __iter_init_12_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_11926_11947_split[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 5, __iter_init_13_++)
		init_buffer_float(&SplitJoin55_FFTReorderSimple_Fiss_11935_11956_split[__iter_init_13_]);
	ENDFOR
	init_buffer_float(&FFTReorderSimple_11788WEIGHTED_ROUND_ROBIN_Splitter_11867);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11885WEIGHTED_ROUND_ROBIN_Splitter_11891);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11830WEIGHTED_ROUND_ROBIN_Splitter_11836);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11837WEIGHTED_ROUND_ROBIN_Splitter_11843);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11913WEIGHTED_ROUND_ROBIN_Splitter_11918);
	FOR(int, __iter_init_14_, 0, <, 5, __iter_init_14_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_11929_11950_split[__iter_init_14_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11868WEIGHTED_ROUND_ROBIN_Splitter_11871);
	FOR(int, __iter_init_15_, 0, <, 5, __iter_init_15_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_11927_11948_join[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 4, __iter_init_16_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_11931_11952_split[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 4, __iter_init_17_++)
		init_buffer_float(&SplitJoin53_FFTReorderSimple_Fiss_11934_11955_join[__iter_init_17_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11892WEIGHTED_ROUND_ROBIN_Splitter_11898);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11919CombineDFT_11798);
	FOR(int, __iter_init_18_, 0, <, 5, __iter_init_18_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_11927_11948_split[__iter_init_18_]);
	ENDFOR
	FOR(int, __iter_init_19_, 0, <, 5, __iter_init_19_++)
		init_buffer_float(&SplitJoin57_FFTReorderSimple_Fiss_11936_11957_join[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 5, __iter_init_20_++)
		init_buffer_float(&SplitJoin61_CombineDFT_Fiss_11938_11959_split[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 2, __iter_init_21_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_11932_11953_split[__iter_init_21_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11801FloatPrinter_11799);
	FOR(int, __iter_init_22_, 0, <, 4, __iter_init_22_++)
		init_buffer_float(&SplitJoin53_FFTReorderSimple_Fiss_11934_11955_split[__iter_init_22_]);
	ENDFOR
	FOR(int, __iter_init_23_, 0, <, 2, __iter_init_23_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11766_11802_11923_11944_split[__iter_init_23_]);
	ENDFOR
	FOR(int, __iter_init_24_, 0, <, 5, __iter_init_24_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_11928_11949_split[__iter_init_24_]);
	ENDFOR
	FOR(int, __iter_init_25_, 0, <, 2, __iter_init_25_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_11922_11943_split[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 5, __iter_init_26_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_11928_11949_join[__iter_init_26_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11813WEIGHTED_ROUND_ROBIN_Splitter_11816);
	FOR(int, __iter_init_27_, 0, <, 5, __iter_init_27_++)
		init_buffer_float(&SplitJoin55_FFTReorderSimple_Fiss_11935_11956_join[__iter_init_27_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11823WEIGHTED_ROUND_ROBIN_Splitter_11829);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11878WEIGHTED_ROUND_ROBIN_Splitter_11884);
	FOR(int, __iter_init_28_, 0, <, 2, __iter_init_28_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_11922_11943_join[__iter_init_28_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11906WEIGHTED_ROUND_ROBIN_Splitter_11912);
	FOR(int, __iter_init_29_, 0, <, 5, __iter_init_29_++)
		init_buffer_float(&SplitJoin63_CombineDFT_Fiss_11939_11960_split[__iter_init_29_]);
	ENDFOR
	FOR(int, __iter_init_30_, 0, <, 4, __iter_init_30_++)
		init_buffer_float(&SplitJoin65_CombineDFT_Fiss_11940_11961_split[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 2, __iter_init_31_++)
		init_buffer_float(&SplitJoin67_CombineDFT_Fiss_11941_11962_join[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 4, __iter_init_32_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_11925_11946_split[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 5, __iter_init_33_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_11926_11947_join[__iter_init_33_]);
	ENDFOR
	FOR(int, __iter_init_34_, 0, <, 2, __iter_init_34_++)
		init_buffer_float(&SplitJoin51_FFTReorderSimple_Fiss_11933_11954_split[__iter_init_34_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11809WEIGHTED_ROUND_ROBIN_Splitter_11800);
	FOR(int, __iter_init_35_, 0, <, 5, __iter_init_35_++)
		init_buffer_float(&SplitJoin61_CombineDFT_Fiss_11938_11959_join[__iter_init_35_]);
	ENDFOR
	FOR(int, __iter_init_36_, 0, <, 4, __iter_init_36_++)
		init_buffer_float(&SplitJoin65_CombineDFT_Fiss_11940_11961_join[__iter_init_36_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11851WEIGHTED_ROUND_ROBIN_Splitter_11857);
	FOR(int, __iter_init_37_, 0, <, 2, __iter_init_37_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_11932_11953_join[__iter_init_37_]);
	ENDFOR
	FOR(int, __iter_init_38_, 0, <, 2, __iter_init_38_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_11924_11945_split[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 5, __iter_init_39_++)
		init_buffer_float(&SplitJoin59_CombineDFT_Fiss_11937_11958_join[__iter_init_39_]);
	ENDFOR
// --- init: CombineDFT_11838
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11838_s.w[i] = real ; 
		CombineDFT_11838_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11839
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11839_s.w[i] = real ; 
		CombineDFT_11839_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11840
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11840_s.w[i] = real ; 
		CombineDFT_11840_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11841
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11841_s.w[i] = real ; 
		CombineDFT_11841_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11842
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11842_s.w[i] = real ; 
		CombineDFT_11842_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11845
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11845_s.w[i] = real ; 
		CombineDFT_11845_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11846
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11846_s.w[i] = real ; 
		CombineDFT_11846_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11847
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11847_s.w[i] = real ; 
		CombineDFT_11847_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11848
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11848_s.w[i] = real ; 
		CombineDFT_11848_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11849
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11849_s.w[i] = real ; 
		CombineDFT_11849_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11852
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11852_s.w[i] = real ; 
		CombineDFT_11852_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11853
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11853_s.w[i] = real ; 
		CombineDFT_11853_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11854
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11854_s.w[i] = real ; 
		CombineDFT_11854_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11855
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11855_s.w[i] = real ; 
		CombineDFT_11855_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11856
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11856_s.w[i] = real ; 
		CombineDFT_11856_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11859
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_11859_s.w[i] = real ; 
		CombineDFT_11859_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11860
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_11860_s.w[i] = real ; 
		CombineDFT_11860_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11861
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_11861_s.w[i] = real ; 
		CombineDFT_11861_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11862
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_11862_s.w[i] = real ; 
		CombineDFT_11862_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11865
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_11865_s.w[i] = real ; 
		CombineDFT_11865_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11866
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_11866_s.w[i] = real ; 
		CombineDFT_11866_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11787
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_11787_s.w[i] = real ; 
		CombineDFT_11787_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11893
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11893_s.w[i] = real ; 
		CombineDFT_11893_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11894
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11894_s.w[i] = real ; 
		CombineDFT_11894_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11895
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11895_s.w[i] = real ; 
		CombineDFT_11895_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11896
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11896_s.w[i] = real ; 
		CombineDFT_11896_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11897
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11897_s.w[i] = real ; 
		CombineDFT_11897_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11900
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11900_s.w[i] = real ; 
		CombineDFT_11900_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11901
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11901_s.w[i] = real ; 
		CombineDFT_11901_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11902
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11902_s.w[i] = real ; 
		CombineDFT_11902_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11903
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11903_s.w[i] = real ; 
		CombineDFT_11903_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11904
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11904_s.w[i] = real ; 
		CombineDFT_11904_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11907
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11907_s.w[i] = real ; 
		CombineDFT_11907_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11908
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11908_s.w[i] = real ; 
		CombineDFT_11908_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11909
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11909_s.w[i] = real ; 
		CombineDFT_11909_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11910
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11910_s.w[i] = real ; 
		CombineDFT_11910_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11911
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11911_s.w[i] = real ; 
		CombineDFT_11911_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11914
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_11914_s.w[i] = real ; 
		CombineDFT_11914_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11915
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_11915_s.w[i] = real ; 
		CombineDFT_11915_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11916
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_11916_s.w[i] = real ; 
		CombineDFT_11916_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11917
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_11917_s.w[i] = real ; 
		CombineDFT_11917_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11920
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_11920_s.w[i] = real ; 
		CombineDFT_11920_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11921
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_11921_s.w[i] = real ; 
		CombineDFT_11921_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11798
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_11798_s.w[i] = real ; 
		CombineDFT_11798_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_11808();
			FFTTestSource_11810();
			FFTTestSource_11811();
		WEIGHTED_ROUND_ROBIN_Joiner_11809();
		WEIGHTED_ROUND_ROBIN_Splitter_11800();
			FFTReorderSimple_11777();
			WEIGHTED_ROUND_ROBIN_Splitter_11812();
				FFTReorderSimple_11814();
				FFTReorderSimple_11815();
			WEIGHTED_ROUND_ROBIN_Joiner_11813();
			WEIGHTED_ROUND_ROBIN_Splitter_11816();
				FFTReorderSimple_11818();
				FFTReorderSimple_11819();
				FFTReorderSimple_11820();
				FFTReorderSimple_11821();
			WEIGHTED_ROUND_ROBIN_Joiner_11817();
			WEIGHTED_ROUND_ROBIN_Splitter_11822();
				FFTReorderSimple_11824();
				FFTReorderSimple_11825();
				FFTReorderSimple_11826();
				FFTReorderSimple_11827();
				FFTReorderSimple_11828();
			WEIGHTED_ROUND_ROBIN_Joiner_11823();
			WEIGHTED_ROUND_ROBIN_Splitter_11829();
				FFTReorderSimple_11831();
				FFTReorderSimple_11832();
				FFTReorderSimple_11833();
				FFTReorderSimple_11834();
				FFTReorderSimple_11835();
			WEIGHTED_ROUND_ROBIN_Joiner_11830();
			WEIGHTED_ROUND_ROBIN_Splitter_11836();
				CombineDFT_11838();
				CombineDFT_11839();
				CombineDFT_11840();
				CombineDFT_11841();
				CombineDFT_11842();
			WEIGHTED_ROUND_ROBIN_Joiner_11837();
			WEIGHTED_ROUND_ROBIN_Splitter_11843();
				CombineDFT_11845();
				CombineDFT_11846();
				CombineDFT_11847();
				CombineDFT_11848();
				CombineDFT_11849();
			WEIGHTED_ROUND_ROBIN_Joiner_11844();
			WEIGHTED_ROUND_ROBIN_Splitter_11850();
				CombineDFT_11852();
				CombineDFT_11853();
				CombineDFT_11854();
				CombineDFT_11855();
				CombineDFT_11856();
			WEIGHTED_ROUND_ROBIN_Joiner_11851();
			WEIGHTED_ROUND_ROBIN_Splitter_11857();
				CombineDFT_11859();
				CombineDFT_11860();
				CombineDFT_11861();
				CombineDFT_11862();
			WEIGHTED_ROUND_ROBIN_Joiner_11858();
			WEIGHTED_ROUND_ROBIN_Splitter_11863();
				CombineDFT_11865();
				CombineDFT_11866();
			WEIGHTED_ROUND_ROBIN_Joiner_11864();
			CombineDFT_11787();
			FFTReorderSimple_11788();
			WEIGHTED_ROUND_ROBIN_Splitter_11867();
				FFTReorderSimple_11869();
				FFTReorderSimple_11870();
			WEIGHTED_ROUND_ROBIN_Joiner_11868();
			WEIGHTED_ROUND_ROBIN_Splitter_11871();
				FFTReorderSimple_11873();
				FFTReorderSimple_11874();
				FFTReorderSimple_11875();
				FFTReorderSimple_11876();
			WEIGHTED_ROUND_ROBIN_Joiner_11872();
			WEIGHTED_ROUND_ROBIN_Splitter_11877();
				FFTReorderSimple_11879();
				FFTReorderSimple_11880();
				FFTReorderSimple_11881();
				FFTReorderSimple_11882();
				FFTReorderSimple_11883();
			WEIGHTED_ROUND_ROBIN_Joiner_11878();
			WEIGHTED_ROUND_ROBIN_Splitter_11884();
				FFTReorderSimple_11886();
				FFTReorderSimple_11887();
				FFTReorderSimple_11888();
				FFTReorderSimple_11889();
				FFTReorderSimple_11890();
			WEIGHTED_ROUND_ROBIN_Joiner_11885();
			WEIGHTED_ROUND_ROBIN_Splitter_11891();
				CombineDFT_11893();
				CombineDFT_11894();
				CombineDFT_11895();
				CombineDFT_11896();
				CombineDFT_11897();
			WEIGHTED_ROUND_ROBIN_Joiner_11892();
			WEIGHTED_ROUND_ROBIN_Splitter_11898();
				CombineDFT_11900();
				CombineDFT_11901();
				CombineDFT_11902();
				CombineDFT_11903();
				CombineDFT_11904();
			WEIGHTED_ROUND_ROBIN_Joiner_11899();
			WEIGHTED_ROUND_ROBIN_Splitter_11905();
				CombineDFT_11907();
				CombineDFT_11908();
				CombineDFT_11909();
				CombineDFT_11910();
				CombineDFT_11911();
			WEIGHTED_ROUND_ROBIN_Joiner_11906();
			WEIGHTED_ROUND_ROBIN_Splitter_11912();
				CombineDFT_11914();
				CombineDFT_11915();
				CombineDFT_11916();
				CombineDFT_11917();
			WEIGHTED_ROUND_ROBIN_Joiner_11913();
			WEIGHTED_ROUND_ROBIN_Splitter_11918();
				CombineDFT_11920();
				CombineDFT_11921();
			WEIGHTED_ROUND_ROBIN_Joiner_11919();
			CombineDFT_11798();
		WEIGHTED_ROUND_ROBIN_Joiner_11801();
		FloatPrinter_11799();
	ENDFOR
	return EXIT_SUCCESS;
}
