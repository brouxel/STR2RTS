#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=352 on the compile command line
#else
#if BUF_SIZEMAX < 352
#error BUF_SIZEMAX too small, it must be at least 352
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif


void WEIGHTED_ROUND_ROBIN_Splitter_4793();
void source_4795();
void source_4796();
void WEIGHTED_ROUND_ROBIN_Joiner_4794();
void WEIGHTED_ROUND_ROBIN_Splitter_4636();
void WEIGHTED_ROUND_ROBIN_Splitter_4638();
void WEIGHTED_ROUND_ROBIN_Splitter_4640();
void WEIGHTED_ROUND_ROBIN_Splitter_4642();
void Identity_4466();
void Identity_4468();
void WEIGHTED_ROUND_ROBIN_Joiner_4643();
void WEIGHTED_ROUND_ROBIN_Splitter_4644();
void Identity_4472();
void Identity_4474();
void WEIGHTED_ROUND_ROBIN_Joiner_4645();
void WEIGHTED_ROUND_ROBIN_Joiner_4641();
void WEIGHTED_ROUND_ROBIN_Splitter_4646();
void WEIGHTED_ROUND_ROBIN_Splitter_4648();
void Identity_4480();
void Identity_4482();
void WEIGHTED_ROUND_ROBIN_Joiner_4649();
void WEIGHTED_ROUND_ROBIN_Splitter_4650();
void Identity_4486();
void Identity_4488();
void WEIGHTED_ROUND_ROBIN_Joiner_4651();
void WEIGHTED_ROUND_ROBIN_Joiner_4647();
void WEIGHTED_ROUND_ROBIN_Joiner_4639();
void WEIGHTED_ROUND_ROBIN_Splitter_4652();
void WEIGHTED_ROUND_ROBIN_Splitter_4654();
void WEIGHTED_ROUND_ROBIN_Splitter_4656();
void Identity_4496();
void Identity_4498();
void WEIGHTED_ROUND_ROBIN_Joiner_4657();
void WEIGHTED_ROUND_ROBIN_Splitter_4658();
void Identity_4502();
void Identity_4504();
void WEIGHTED_ROUND_ROBIN_Joiner_4659();
void WEIGHTED_ROUND_ROBIN_Joiner_4655();
void WEIGHTED_ROUND_ROBIN_Splitter_4660();
void WEIGHTED_ROUND_ROBIN_Splitter_4662();
void Identity_4510();
void Identity_4512();
void WEIGHTED_ROUND_ROBIN_Joiner_4663();
void WEIGHTED_ROUND_ROBIN_Splitter_4664();
void Identity_4516();
void Identity_4518();
void WEIGHTED_ROUND_ROBIN_Joiner_4665();
void WEIGHTED_ROUND_ROBIN_Joiner_4661();
void WEIGHTED_ROUND_ROBIN_Joiner_4653();
void WEIGHTED_ROUND_ROBIN_Joiner_4637();
void WEIGHTED_ROUND_ROBIN_Splitter_4780();
void WEIGHTED_ROUND_ROBIN_Splitter_4781();
void Pre_CollapsedDataParallel_1_4613();
void butterfly_4520();
void Post_CollapsedDataParallel_2_4614();
void Pre_CollapsedDataParallel_1_4616();
void butterfly_4521();
void Post_CollapsedDataParallel_2_4617();
void Pre_CollapsedDataParallel_1_4619();
void butterfly_4522();
void Post_CollapsedDataParallel_2_4620();
void Pre_CollapsedDataParallel_1_4622();
void butterfly_4523();
void Post_CollapsedDataParallel_2_4623();
void WEIGHTED_ROUND_ROBIN_Joiner_4782();
void WEIGHTED_ROUND_ROBIN_Splitter_4783();
void Pre_CollapsedDataParallel_1_4625();
void butterfly_4524();
void Post_CollapsedDataParallel_2_4626();
void Pre_CollapsedDataParallel_1_4628();
void butterfly_4525();
void Post_CollapsedDataParallel_2_4629();
void Pre_CollapsedDataParallel_1_4631();
void butterfly_4526();
void Post_CollapsedDataParallel_2_4632();
void Pre_CollapsedDataParallel_1_4634();
void butterfly_4527();
void Post_CollapsedDataParallel_2_4635();
void WEIGHTED_ROUND_ROBIN_Joiner_4784();
void WEIGHTED_ROUND_ROBIN_Joiner_4785();
void WEIGHTED_ROUND_ROBIN_Splitter_4786();
void WEIGHTED_ROUND_ROBIN_Splitter_4787();
void WEIGHTED_ROUND_ROBIN_Splitter_4670();
void butterfly_4529();
void butterfly_4530();
void WEIGHTED_ROUND_ROBIN_Joiner_4671();
void WEIGHTED_ROUND_ROBIN_Splitter_4672();
void butterfly_4531();
void butterfly_4532();
void WEIGHTED_ROUND_ROBIN_Joiner_4673();
void WEIGHTED_ROUND_ROBIN_Joiner_4788();
void WEIGHTED_ROUND_ROBIN_Splitter_4789();
void WEIGHTED_ROUND_ROBIN_Splitter_4674();
void butterfly_4533();
void butterfly_4534();
void WEIGHTED_ROUND_ROBIN_Joiner_4675();
void WEIGHTED_ROUND_ROBIN_Splitter_4676();
void butterfly_4535();
void butterfly_4536();
void WEIGHTED_ROUND_ROBIN_Joiner_4677();
void WEIGHTED_ROUND_ROBIN_Joiner_4790();
void WEIGHTED_ROUND_ROBIN_Joiner_4791();
void WEIGHTED_ROUND_ROBIN_Splitter_4678();
void WEIGHTED_ROUND_ROBIN_Splitter_4680();
void butterfly_4538();
void butterfly_4539();
void butterfly_4540();
void butterfly_4541();
void WEIGHTED_ROUND_ROBIN_Joiner_4681();
void WEIGHTED_ROUND_ROBIN_Splitter_4682();
void butterfly_4542();
void butterfly_4543();
void butterfly_4544();
void butterfly_4545();
void WEIGHTED_ROUND_ROBIN_Joiner_4683();
void WEIGHTED_ROUND_ROBIN_Joiner_4679();
void WEIGHTED_ROUND_ROBIN_Splitter_4797();
void magnitude_4799();
void magnitude_4800();
void magnitude_4801();
void magnitude_4802();
void magnitude_4803();
void magnitude_4804();
void magnitude_4805();
void magnitude_4806();
void magnitude_4807();
void magnitude_4808();
void magnitude_4809();
void WEIGHTED_ROUND_ROBIN_Joiner_4798();
void sink_4547();

#ifdef __cplusplus
}
#endif
#endif
