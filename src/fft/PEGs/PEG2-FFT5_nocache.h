#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=32 on the compile command line
#else
#if BUF_SIZEMAX < 32
#error BUF_SIZEMAX too small, it must be at least 32
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif


void WEIGHTED_ROUND_ROBIN_Splitter_10076();
void source_10078();
void source_10079();
void WEIGHTED_ROUND_ROBIN_Joiner_10077();
void WEIGHTED_ROUND_ROBIN_Splitter_9919();
void WEIGHTED_ROUND_ROBIN_Splitter_9921();
void WEIGHTED_ROUND_ROBIN_Splitter_9923();
void WEIGHTED_ROUND_ROBIN_Splitter_9925();
void Identity_9749();
void Identity_9751();
void WEIGHTED_ROUND_ROBIN_Joiner_9926();
void WEIGHTED_ROUND_ROBIN_Splitter_9927();
void Identity_9755();
void Identity_9757();
void WEIGHTED_ROUND_ROBIN_Joiner_9928();
void WEIGHTED_ROUND_ROBIN_Joiner_9924();
void WEIGHTED_ROUND_ROBIN_Splitter_9929();
void WEIGHTED_ROUND_ROBIN_Splitter_9931();
void Identity_9763();
void Identity_9765();
void WEIGHTED_ROUND_ROBIN_Joiner_9932();
void WEIGHTED_ROUND_ROBIN_Splitter_9933();
void Identity_9769();
void Identity_9771();
void WEIGHTED_ROUND_ROBIN_Joiner_9934();
void WEIGHTED_ROUND_ROBIN_Joiner_9930();
void WEIGHTED_ROUND_ROBIN_Joiner_9922();
void WEIGHTED_ROUND_ROBIN_Splitter_9935();
void WEIGHTED_ROUND_ROBIN_Splitter_9937();
void WEIGHTED_ROUND_ROBIN_Splitter_9939();
void Identity_9779();
void Identity_9781();
void WEIGHTED_ROUND_ROBIN_Joiner_9940();
void WEIGHTED_ROUND_ROBIN_Splitter_9941();
void Identity_9785();
void Identity_9787();
void WEIGHTED_ROUND_ROBIN_Joiner_9942();
void WEIGHTED_ROUND_ROBIN_Joiner_9938();
void WEIGHTED_ROUND_ROBIN_Splitter_9943();
void WEIGHTED_ROUND_ROBIN_Splitter_9945();
void Identity_9793();
void Identity_9795();
void WEIGHTED_ROUND_ROBIN_Joiner_9946();
void WEIGHTED_ROUND_ROBIN_Splitter_9947();
void Identity_9799();
void Identity_9801();
void WEIGHTED_ROUND_ROBIN_Joiner_9948();
void WEIGHTED_ROUND_ROBIN_Joiner_9944();
void WEIGHTED_ROUND_ROBIN_Joiner_9936();
void WEIGHTED_ROUND_ROBIN_Joiner_9920();
void WEIGHTED_ROUND_ROBIN_Splitter_10063();
void WEIGHTED_ROUND_ROBIN_Splitter_10064();
void Pre_CollapsedDataParallel_1_9896();
void butterfly_9803();
void Post_CollapsedDataParallel_2_9897();
void Pre_CollapsedDataParallel_1_9899();
void butterfly_9804();
void Post_CollapsedDataParallel_2_9900();
void Pre_CollapsedDataParallel_1_9902();
void butterfly_9805();
void Post_CollapsedDataParallel_2_9903();
void Pre_CollapsedDataParallel_1_9905();
void butterfly_9806();
void Post_CollapsedDataParallel_2_9906();
void WEIGHTED_ROUND_ROBIN_Joiner_10065();
void WEIGHTED_ROUND_ROBIN_Splitter_10066();
void Pre_CollapsedDataParallel_1_9908();
void butterfly_9807();
void Post_CollapsedDataParallel_2_9909();
void Pre_CollapsedDataParallel_1_9911();
void butterfly_9808();
void Post_CollapsedDataParallel_2_9912();
void Pre_CollapsedDataParallel_1_9914();
void butterfly_9809();
void Post_CollapsedDataParallel_2_9915();
void Pre_CollapsedDataParallel_1_9917();
void butterfly_9810();
void Post_CollapsedDataParallel_2_9918();
void WEIGHTED_ROUND_ROBIN_Joiner_10067();
void WEIGHTED_ROUND_ROBIN_Joiner_10068();
void WEIGHTED_ROUND_ROBIN_Splitter_10069();
void WEIGHTED_ROUND_ROBIN_Splitter_10070();
void WEIGHTED_ROUND_ROBIN_Splitter_9953();
void butterfly_9812();
void butterfly_9813();
void WEIGHTED_ROUND_ROBIN_Joiner_9954();
void WEIGHTED_ROUND_ROBIN_Splitter_9955();
void butterfly_9814();
void butterfly_9815();
void WEIGHTED_ROUND_ROBIN_Joiner_9956();
void WEIGHTED_ROUND_ROBIN_Joiner_10071();
void WEIGHTED_ROUND_ROBIN_Splitter_10072();
void WEIGHTED_ROUND_ROBIN_Splitter_9957();
void butterfly_9816();
void butterfly_9817();
void WEIGHTED_ROUND_ROBIN_Joiner_9958();
void WEIGHTED_ROUND_ROBIN_Splitter_9959();
void butterfly_9818();
void butterfly_9819();
void WEIGHTED_ROUND_ROBIN_Joiner_9960();
void WEIGHTED_ROUND_ROBIN_Joiner_10073();
void WEIGHTED_ROUND_ROBIN_Joiner_10074();
void WEIGHTED_ROUND_ROBIN_Splitter_9961();
void WEIGHTED_ROUND_ROBIN_Splitter_9963();
void butterfly_9821();
void butterfly_9822();
void butterfly_9823();
void butterfly_9824();
void WEIGHTED_ROUND_ROBIN_Joiner_9964();
void WEIGHTED_ROUND_ROBIN_Splitter_9965();
void butterfly_9825();
void butterfly_9826();
void butterfly_9827();
void butterfly_9828();
void WEIGHTED_ROUND_ROBIN_Joiner_9966();
void WEIGHTED_ROUND_ROBIN_Joiner_9962();
void WEIGHTED_ROUND_ROBIN_Splitter_10080();
void magnitude_10082();
void magnitude_10083();
void WEIGHTED_ROUND_ROBIN_Joiner_10081();
void sink_9830();

#ifdef __cplusplus
}
#endif
#endif
