#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=224 on the compile command line
#else
#if BUF_SIZEMAX < 224
#error BUF_SIZEMAX too small, it must be at least 224
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif


void WEIGHTED_ROUND_ROBIN_Splitter_7161();
void source_7163();
void source_7164();
void WEIGHTED_ROUND_ROBIN_Joiner_7162();
void WEIGHTED_ROUND_ROBIN_Splitter_7004();
void WEIGHTED_ROUND_ROBIN_Splitter_7006();
void WEIGHTED_ROUND_ROBIN_Splitter_7008();
void WEIGHTED_ROUND_ROBIN_Splitter_7010();
void Identity_6834();
void Identity_6836();
void WEIGHTED_ROUND_ROBIN_Joiner_7011();
void WEIGHTED_ROUND_ROBIN_Splitter_7012();
void Identity_6840();
void Identity_6842();
void WEIGHTED_ROUND_ROBIN_Joiner_7013();
void WEIGHTED_ROUND_ROBIN_Joiner_7009();
void WEIGHTED_ROUND_ROBIN_Splitter_7014();
void WEIGHTED_ROUND_ROBIN_Splitter_7016();
void Identity_6848();
void Identity_6850();
void WEIGHTED_ROUND_ROBIN_Joiner_7017();
void WEIGHTED_ROUND_ROBIN_Splitter_7018();
void Identity_6854();
void Identity_6856();
void WEIGHTED_ROUND_ROBIN_Joiner_7019();
void WEIGHTED_ROUND_ROBIN_Joiner_7015();
void WEIGHTED_ROUND_ROBIN_Joiner_7007();
void WEIGHTED_ROUND_ROBIN_Splitter_7020();
void WEIGHTED_ROUND_ROBIN_Splitter_7022();
void WEIGHTED_ROUND_ROBIN_Splitter_7024();
void Identity_6864();
void Identity_6866();
void WEIGHTED_ROUND_ROBIN_Joiner_7025();
void WEIGHTED_ROUND_ROBIN_Splitter_7026();
void Identity_6870();
void Identity_6872();
void WEIGHTED_ROUND_ROBIN_Joiner_7027();
void WEIGHTED_ROUND_ROBIN_Joiner_7023();
void WEIGHTED_ROUND_ROBIN_Splitter_7028();
void WEIGHTED_ROUND_ROBIN_Splitter_7030();
void Identity_6878();
void Identity_6880();
void WEIGHTED_ROUND_ROBIN_Joiner_7031();
void WEIGHTED_ROUND_ROBIN_Splitter_7032();
void Identity_6884();
void Identity_6886();
void WEIGHTED_ROUND_ROBIN_Joiner_7033();
void WEIGHTED_ROUND_ROBIN_Joiner_7029();
void WEIGHTED_ROUND_ROBIN_Joiner_7021();
void WEIGHTED_ROUND_ROBIN_Joiner_7005();
void WEIGHTED_ROUND_ROBIN_Splitter_7148();
void WEIGHTED_ROUND_ROBIN_Splitter_7149();
void Pre_CollapsedDataParallel_1_6981();
void butterfly_6888();
void Post_CollapsedDataParallel_2_6982();
void Pre_CollapsedDataParallel_1_6984();
void butterfly_6889();
void Post_CollapsedDataParallel_2_6985();
void Pre_CollapsedDataParallel_1_6987();
void butterfly_6890();
void Post_CollapsedDataParallel_2_6988();
void Pre_CollapsedDataParallel_1_6990();
void butterfly_6891();
void Post_CollapsedDataParallel_2_6991();
void WEIGHTED_ROUND_ROBIN_Joiner_7150();
void WEIGHTED_ROUND_ROBIN_Splitter_7151();
void Pre_CollapsedDataParallel_1_6993();
void butterfly_6892();
void Post_CollapsedDataParallel_2_6994();
void Pre_CollapsedDataParallel_1_6996();
void butterfly_6893();
void Post_CollapsedDataParallel_2_6997();
void Pre_CollapsedDataParallel_1_6999();
void butterfly_6894();
void Post_CollapsedDataParallel_2_7000();
void Pre_CollapsedDataParallel_1_7002();
void butterfly_6895();
void Post_CollapsedDataParallel_2_7003();
void WEIGHTED_ROUND_ROBIN_Joiner_7152();
void WEIGHTED_ROUND_ROBIN_Joiner_7153();
void WEIGHTED_ROUND_ROBIN_Splitter_7154();
void WEIGHTED_ROUND_ROBIN_Splitter_7155();
void WEIGHTED_ROUND_ROBIN_Splitter_7038();
void butterfly_6897();
void butterfly_6898();
void WEIGHTED_ROUND_ROBIN_Joiner_7039();
void WEIGHTED_ROUND_ROBIN_Splitter_7040();
void butterfly_6899();
void butterfly_6900();
void WEIGHTED_ROUND_ROBIN_Joiner_7041();
void WEIGHTED_ROUND_ROBIN_Joiner_7156();
void WEIGHTED_ROUND_ROBIN_Splitter_7157();
void WEIGHTED_ROUND_ROBIN_Splitter_7042();
void butterfly_6901();
void butterfly_6902();
void WEIGHTED_ROUND_ROBIN_Joiner_7043();
void WEIGHTED_ROUND_ROBIN_Splitter_7044();
void butterfly_6903();
void butterfly_6904();
void WEIGHTED_ROUND_ROBIN_Joiner_7045();
void WEIGHTED_ROUND_ROBIN_Joiner_7158();
void WEIGHTED_ROUND_ROBIN_Joiner_7159();
void WEIGHTED_ROUND_ROBIN_Splitter_7046();
void WEIGHTED_ROUND_ROBIN_Splitter_7048();
void butterfly_6906();
void butterfly_6907();
void butterfly_6908();
void butterfly_6909();
void WEIGHTED_ROUND_ROBIN_Joiner_7049();
void WEIGHTED_ROUND_ROBIN_Splitter_7050();
void butterfly_6910();
void butterfly_6911();
void butterfly_6912();
void butterfly_6913();
void WEIGHTED_ROUND_ROBIN_Joiner_7051();
void WEIGHTED_ROUND_ROBIN_Joiner_7047();
void WEIGHTED_ROUND_ROBIN_Splitter_7165();
void magnitude_7167();
void magnitude_7168();
void magnitude_7169();
void magnitude_7170();
void magnitude_7171();
void magnitude_7172();
void magnitude_7173();
void WEIGHTED_ROUND_ROBIN_Joiner_7166();
void sink_6915();

#ifdef __cplusplus
}
#endif
#endif
