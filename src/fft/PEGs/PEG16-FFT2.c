#include "PEG16-FFT2.h"

buffer_float_t SplitJoin102_CombineDFT_Fiss_7916_7937_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7862WEIGHTED_ROUND_ROBIN_Splitter_7879;
buffer_float_t SplitJoin20_CombineDFT_Fiss_7909_7930_join[2];
buffer_float_t SplitJoin106_CombineDFT_Fiss_7918_7939_join[2];
buffer_float_t SplitJoin0_FFTTestSource_Fiss_7899_7920_join[2];
buffer_float_t FFTReorderSimple_7676WEIGHTED_ROUND_ROBIN_Splitter_7711;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7786WEIGHTED_ROUND_ROBIN_Splitter_7795;
buffer_float_t SplitJoin94_FFTReorderSimple_Fiss_7912_7933_join[8];
buffer_float_t SplitJoin98_CombineDFT_Fiss_7914_7935_join[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7768WEIGHTED_ROUND_ROBIN_Splitter_7785;
buffer_float_t SplitJoin96_FFTReorderSimple_Fiss_7913_7934_split[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7806WEIGHTED_ROUND_ROBIN_Splitter_7809;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7844WEIGHTED_ROUND_ROBIN_Splitter_7861;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7880WEIGHTED_ROUND_ROBIN_Splitter_7889;
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_7904_7925_split[16];
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_7903_7924_join[8];
buffer_float_t SplitJoin94_FFTReorderSimple_Fiss_7912_7933_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7722WEIGHTED_ROUND_ROBIN_Splitter_7731;
buffer_float_t SplitJoin14_CombineDFT_Fiss_7906_7927_join[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7896CombineDFT_7697;
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_7665_7701_7900_7921_join[2];
buffer_float_t SplitJoin92_FFTReorderSimple_Fiss_7911_7932_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7802CombineDFT_7686;
buffer_float_t SplitJoin92_FFTReorderSimple_Fiss_7911_7932_join[4];
buffer_float_t SplitJoin16_CombineDFT_Fiss_7907_7928_split[8];
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_7901_7922_split[2];
buffer_float_t SplitJoin104_CombineDFT_Fiss_7917_7938_join[4];
buffer_float_t SplitJoin0_FFTTestSource_Fiss_7899_7920_split[2];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_7665_7701_7900_7921_split[2];
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_7902_7923_join[4];
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_7903_7924_split[8];
buffer_float_t SplitJoin16_CombineDFT_Fiss_7907_7928_join[8];
buffer_float_t SplitJoin18_CombineDFT_Fiss_7908_7929_split[4];
buffer_float_t SplitJoin106_CombineDFT_Fiss_7918_7939_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7708WEIGHTED_ROUND_ROBIN_Splitter_7699;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7810WEIGHTED_ROUND_ROBIN_Splitter_7815;
buffer_float_t SplitJoin14_CombineDFT_Fiss_7906_7927_split[16];
buffer_float_t SplitJoin102_CombineDFT_Fiss_7916_7937_join[8];
buffer_float_t SplitJoin98_CombineDFT_Fiss_7914_7935_split[16];
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_7901_7922_join[2];
buffer_float_t SplitJoin90_FFTReorderSimple_Fiss_7910_7931_join[2];
buffer_float_t SplitJoin18_CombineDFT_Fiss_7908_7929_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7826WEIGHTED_ROUND_ROBIN_Splitter_7843;
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_7904_7925_join[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7732WEIGHTED_ROUND_ROBIN_Splitter_7749;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7890WEIGHTED_ROUND_ROBIN_Splitter_7895;
buffer_float_t SplitJoin90_FFTReorderSimple_Fiss_7910_7931_split[2];
buffer_float_t SplitJoin104_CombineDFT_Fiss_7917_7938_split[4];
buffer_float_t SplitJoin96_FFTReorderSimple_Fiss_7913_7934_join[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7700FloatPrinter_7698;
buffer_float_t SplitJoin20_CombineDFT_Fiss_7909_7930_split[2];
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_7902_7923_split[4];
buffer_float_t SplitJoin12_CombineDFT_Fiss_7905_7926_split[16];
buffer_float_t SplitJoin100_CombineDFT_Fiss_7915_7936_split[16];
buffer_float_t SplitJoin12_CombineDFT_Fiss_7905_7926_join[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7750WEIGHTED_ROUND_ROBIN_Splitter_7767;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7816WEIGHTED_ROUND_ROBIN_Splitter_7825;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7712WEIGHTED_ROUND_ROBIN_Splitter_7715;
buffer_float_t FFTReorderSimple_7687WEIGHTED_ROUND_ROBIN_Splitter_7805;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7716WEIGHTED_ROUND_ROBIN_Splitter_7721;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7796WEIGHTED_ROUND_ROBIN_Splitter_7801;
buffer_float_t SplitJoin100_CombineDFT_Fiss_7915_7936_join[16];


CombineDFT_7751_t CombineDFT_7751_s;
CombineDFT_7751_t CombineDFT_7752_s;
CombineDFT_7751_t CombineDFT_7753_s;
CombineDFT_7751_t CombineDFT_7754_s;
CombineDFT_7751_t CombineDFT_7755_s;
CombineDFT_7751_t CombineDFT_7756_s;
CombineDFT_7751_t CombineDFT_7757_s;
CombineDFT_7751_t CombineDFT_7758_s;
CombineDFT_7751_t CombineDFT_7759_s;
CombineDFT_7751_t CombineDFT_7760_s;
CombineDFT_7751_t CombineDFT_7761_s;
CombineDFT_7751_t CombineDFT_7762_s;
CombineDFT_7751_t CombineDFT_7763_s;
CombineDFT_7751_t CombineDFT_7764_s;
CombineDFT_7751_t CombineDFT_7765_s;
CombineDFT_7751_t CombineDFT_7766_s;
CombineDFT_7769_t CombineDFT_7769_s;
CombineDFT_7769_t CombineDFT_7770_s;
CombineDFT_7769_t CombineDFT_7771_s;
CombineDFT_7769_t CombineDFT_7772_s;
CombineDFT_7769_t CombineDFT_7773_s;
CombineDFT_7769_t CombineDFT_7774_s;
CombineDFT_7769_t CombineDFT_7775_s;
CombineDFT_7769_t CombineDFT_7776_s;
CombineDFT_7769_t CombineDFT_7777_s;
CombineDFT_7769_t CombineDFT_7778_s;
CombineDFT_7769_t CombineDFT_7779_s;
CombineDFT_7769_t CombineDFT_7780_s;
CombineDFT_7769_t CombineDFT_7781_s;
CombineDFT_7769_t CombineDFT_7782_s;
CombineDFT_7769_t CombineDFT_7783_s;
CombineDFT_7769_t CombineDFT_7784_s;
CombineDFT_7787_t CombineDFT_7787_s;
CombineDFT_7787_t CombineDFT_7788_s;
CombineDFT_7787_t CombineDFT_7789_s;
CombineDFT_7787_t CombineDFT_7790_s;
CombineDFT_7787_t CombineDFT_7791_s;
CombineDFT_7787_t CombineDFT_7792_s;
CombineDFT_7787_t CombineDFT_7793_s;
CombineDFT_7787_t CombineDFT_7794_s;
CombineDFT_7797_t CombineDFT_7797_s;
CombineDFT_7797_t CombineDFT_7798_s;
CombineDFT_7797_t CombineDFT_7799_s;
CombineDFT_7797_t CombineDFT_7800_s;
CombineDFT_7803_t CombineDFT_7803_s;
CombineDFT_7803_t CombineDFT_7804_s;
CombineDFT_7686_t CombineDFT_7686_s;
CombineDFT_7751_t CombineDFT_7845_s;
CombineDFT_7751_t CombineDFT_7846_s;
CombineDFT_7751_t CombineDFT_7847_s;
CombineDFT_7751_t CombineDFT_7848_s;
CombineDFT_7751_t CombineDFT_7849_s;
CombineDFT_7751_t CombineDFT_7850_s;
CombineDFT_7751_t CombineDFT_7851_s;
CombineDFT_7751_t CombineDFT_7852_s;
CombineDFT_7751_t CombineDFT_7853_s;
CombineDFT_7751_t CombineDFT_7854_s;
CombineDFT_7751_t CombineDFT_7855_s;
CombineDFT_7751_t CombineDFT_7856_s;
CombineDFT_7751_t CombineDFT_7857_s;
CombineDFT_7751_t CombineDFT_7858_s;
CombineDFT_7751_t CombineDFT_7859_s;
CombineDFT_7751_t CombineDFT_7860_s;
CombineDFT_7769_t CombineDFT_7863_s;
CombineDFT_7769_t CombineDFT_7864_s;
CombineDFT_7769_t CombineDFT_7865_s;
CombineDFT_7769_t CombineDFT_7866_s;
CombineDFT_7769_t CombineDFT_7867_s;
CombineDFT_7769_t CombineDFT_7868_s;
CombineDFT_7769_t CombineDFT_7869_s;
CombineDFT_7769_t CombineDFT_7870_s;
CombineDFT_7769_t CombineDFT_7871_s;
CombineDFT_7769_t CombineDFT_7872_s;
CombineDFT_7769_t CombineDFT_7873_s;
CombineDFT_7769_t CombineDFT_7874_s;
CombineDFT_7769_t CombineDFT_7875_s;
CombineDFT_7769_t CombineDFT_7876_s;
CombineDFT_7769_t CombineDFT_7877_s;
CombineDFT_7769_t CombineDFT_7878_s;
CombineDFT_7787_t CombineDFT_7881_s;
CombineDFT_7787_t CombineDFT_7882_s;
CombineDFT_7787_t CombineDFT_7883_s;
CombineDFT_7787_t CombineDFT_7884_s;
CombineDFT_7787_t CombineDFT_7885_s;
CombineDFT_7787_t CombineDFT_7886_s;
CombineDFT_7787_t CombineDFT_7887_s;
CombineDFT_7787_t CombineDFT_7888_s;
CombineDFT_7797_t CombineDFT_7891_s;
CombineDFT_7797_t CombineDFT_7892_s;
CombineDFT_7797_t CombineDFT_7893_s;
CombineDFT_7797_t CombineDFT_7894_s;
CombineDFT_7803_t CombineDFT_7897_s;
CombineDFT_7803_t CombineDFT_7898_s;
CombineDFT_7686_t CombineDFT_7697_s;

void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout) {
	push_float(&(*chanout), 0.0) ; 
	push_float(&(*chanout), 0.0) ; 
	push_float(&(*chanout), 1.0) ; 
	push_float(&(*chanout), 0.0) ; 
	FOR(int, i, 0,  < , 124, i++) {
		push_float(&(*chanout), 0.0) ; 
	}
	ENDFOR
}


void FFTTestSource_7709() {
	FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_7899_7920_split[0]), &(SplitJoin0_FFTTestSource_Fiss_7899_7920_join[0]));
}

void FFTTestSource_7710() {
	FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_7899_7920_split[1]), &(SplitJoin0_FFTTestSource_Fiss_7899_7920_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7707() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_7708() {
	FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7708WEIGHTED_ROUND_ROBIN_Splitter_7699, pop_float(&SplitJoin0_FFTTestSource_Fiss_7899_7920_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7708WEIGHTED_ROUND_ROBIN_Splitter_7699, pop_float(&SplitJoin0_FFTTestSource_Fiss_7899_7920_join[1]));
	ENDFOR
}

void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout) {
	FOR(int, i, 0,  < , 128, i = (i + 4)) {
		push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
		push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 2,  < , 128, i = (i + 4)) {
		push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
		push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 64, i++) {
		pop_float(&(*chanin)) ; 
		pop_float(&(*chanin)) ; 
	}
	ENDFOR
}


void FFTReorderSimple_7676() {
	FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_7665_7701_7900_7921_split[0]), &(FFTReorderSimple_7676WEIGHTED_ROUND_ROBIN_Splitter_7711));
}

void FFTReorderSimple_7713() {
	FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_7901_7922_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_7901_7922_join[0]));
}

void FFTReorderSimple_7714() {
	FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_7901_7922_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_7901_7922_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7711() {
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&SplitJoin4_FFTReorderSimple_Fiss_7901_7922_split[0], pop_float(&FFTReorderSimple_7676WEIGHTED_ROUND_ROBIN_Splitter_7711));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&SplitJoin4_FFTReorderSimple_Fiss_7901_7922_split[1], pop_float(&FFTReorderSimple_7676WEIGHTED_ROUND_ROBIN_Splitter_7711));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7712() {
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7712WEIGHTED_ROUND_ROBIN_Splitter_7715, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_7901_7922_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7712WEIGHTED_ROUND_ROBIN_Splitter_7715, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_7901_7922_join[1]));
	ENDFOR
}

void FFTReorderSimple_7717() {
	FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_7902_7923_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_7902_7923_join[0]));
}

void FFTReorderSimple_7718() {
	FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_7902_7923_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_7902_7923_join[1]));
}

void FFTReorderSimple_7719() {
	FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_7902_7923_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_7902_7923_join[2]));
}

void FFTReorderSimple_7720() {
	FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_7902_7923_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_7902_7923_join[3]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7715() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
			push_float(&SplitJoin6_FFTReorderSimple_Fiss_7902_7923_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7712WEIGHTED_ROUND_ROBIN_Splitter_7715));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7716() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7716WEIGHTED_ROUND_ROBIN_Splitter_7721, pop_float(&SplitJoin6_FFTReorderSimple_Fiss_7902_7923_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void FFTReorderSimple_7723() {
	FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_7903_7924_split[0]), &(SplitJoin8_FFTReorderSimple_Fiss_7903_7924_join[0]));
}

void FFTReorderSimple_7724() {
	FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_7903_7924_split[1]), &(SplitJoin8_FFTReorderSimple_Fiss_7903_7924_join[1]));
}

void FFTReorderSimple_7725() {
	FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_7903_7924_split[2]), &(SplitJoin8_FFTReorderSimple_Fiss_7903_7924_join[2]));
}

void FFTReorderSimple_7726() {
	FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_7903_7924_split[3]), &(SplitJoin8_FFTReorderSimple_Fiss_7903_7924_join[3]));
}

void FFTReorderSimple_7727() {
	FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_7903_7924_split[4]), &(SplitJoin8_FFTReorderSimple_Fiss_7903_7924_join[4]));
}

void FFTReorderSimple_7728() {
	FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_7903_7924_split[5]), &(SplitJoin8_FFTReorderSimple_Fiss_7903_7924_join[5]));
}

void FFTReorderSimple_7729() {
	FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_7903_7924_split[6]), &(SplitJoin8_FFTReorderSimple_Fiss_7903_7924_join[6]));
}

void FFTReorderSimple_7730() {
	FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_7903_7924_split[7]), &(SplitJoin8_FFTReorderSimple_Fiss_7903_7924_join[7]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7721() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
			push_float(&SplitJoin8_FFTReorderSimple_Fiss_7903_7924_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7716WEIGHTED_ROUND_ROBIN_Splitter_7721));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7722() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7722WEIGHTED_ROUND_ROBIN_Splitter_7731, pop_float(&SplitJoin8_FFTReorderSimple_Fiss_7903_7924_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void FFTReorderSimple_7733() {
	FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_split[0]), &(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_join[0]));
}

void FFTReorderSimple_7734() {
	FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_split[1]), &(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_join[1]));
}

void FFTReorderSimple_7735() {
	FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_split[2]), &(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_join[2]));
}

void FFTReorderSimple_7736() {
	FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_split[3]), &(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_join[3]));
}

void FFTReorderSimple_7737() {
	FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_split[4]), &(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_join[4]));
}

void FFTReorderSimple_7738() {
	FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_split[5]), &(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_join[5]));
}

void FFTReorderSimple_7739() {
	FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_split[6]), &(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_join[6]));
}

void FFTReorderSimple_7740() {
	FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_split[7]), &(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_join[7]));
}

void FFTReorderSimple_7741() {
	FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_split[8]), &(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_join[8]));
}

void FFTReorderSimple_7742() {
	FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_split[9]), &(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_join[9]));
}

void FFTReorderSimple_7743() {
	FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_split[10]), &(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_join[10]));
}

void FFTReorderSimple_7744() {
	FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_split[11]), &(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_join[11]));
}

void FFTReorderSimple_7745() {
	FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_split[12]), &(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_join[12]));
}

void FFTReorderSimple_7746() {
	FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_split[13]), &(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_join[13]));
}

void FFTReorderSimple_7747() {
	FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_split[14]), &(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_join[14]));
}

void FFTReorderSimple_7748() {
	FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_split[15]), &(SplitJoin10_FFTReorderSimple_Fiss_7904_7925_join[15]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7731() {
	FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_float(&SplitJoin10_FFTReorderSimple_Fiss_7904_7925_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7722WEIGHTED_ROUND_ROBIN_Splitter_7731));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7732() {
	FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7732WEIGHTED_ROUND_ROBIN_Splitter_7749, pop_float(&SplitJoin10_FFTReorderSimple_Fiss_7904_7925_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&(*chanin), i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&(*chanin), i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&(*chanin), (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&(*chanin), (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_7751_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_7751_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&(*chanin)) ; 
			push_float(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_7751() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7905_7926_split[0]), &(SplitJoin12_CombineDFT_Fiss_7905_7926_join[0]));
	ENDFOR
}

void CombineDFT_7752() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7905_7926_split[1]), &(SplitJoin12_CombineDFT_Fiss_7905_7926_join[1]));
	ENDFOR
}

void CombineDFT_7753() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7905_7926_split[2]), &(SplitJoin12_CombineDFT_Fiss_7905_7926_join[2]));
	ENDFOR
}

void CombineDFT_7754() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7905_7926_split[3]), &(SplitJoin12_CombineDFT_Fiss_7905_7926_join[3]));
	ENDFOR
}

void CombineDFT_7755() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7905_7926_split[4]), &(SplitJoin12_CombineDFT_Fiss_7905_7926_join[4]));
	ENDFOR
}

void CombineDFT_7756() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7905_7926_split[5]), &(SplitJoin12_CombineDFT_Fiss_7905_7926_join[5]));
	ENDFOR
}

void CombineDFT_7757() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7905_7926_split[6]), &(SplitJoin12_CombineDFT_Fiss_7905_7926_join[6]));
	ENDFOR
}

void CombineDFT_7758() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7905_7926_split[7]), &(SplitJoin12_CombineDFT_Fiss_7905_7926_join[7]));
	ENDFOR
}

void CombineDFT_7759() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7905_7926_split[8]), &(SplitJoin12_CombineDFT_Fiss_7905_7926_join[8]));
	ENDFOR
}

void CombineDFT_7760() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7905_7926_split[9]), &(SplitJoin12_CombineDFT_Fiss_7905_7926_join[9]));
	ENDFOR
}

void CombineDFT_7761() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7905_7926_split[10]), &(SplitJoin12_CombineDFT_Fiss_7905_7926_join[10]));
	ENDFOR
}

void CombineDFT_7762() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7905_7926_split[11]), &(SplitJoin12_CombineDFT_Fiss_7905_7926_join[11]));
	ENDFOR
}

void CombineDFT_7763() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7905_7926_split[12]), &(SplitJoin12_CombineDFT_Fiss_7905_7926_join[12]));
	ENDFOR
}

void CombineDFT_7764() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7905_7926_split[13]), &(SplitJoin12_CombineDFT_Fiss_7905_7926_join[13]));
	ENDFOR
}

void CombineDFT_7765() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7905_7926_split[14]), &(SplitJoin12_CombineDFT_Fiss_7905_7926_join[14]));
	ENDFOR
}

void CombineDFT_7766() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_7905_7926_split[15]), &(SplitJoin12_CombineDFT_Fiss_7905_7926_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7749() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin12_CombineDFT_Fiss_7905_7926_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7732WEIGHTED_ROUND_ROBIN_Splitter_7749));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7750() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7750WEIGHTED_ROUND_ROBIN_Splitter_7767, pop_float(&SplitJoin12_CombineDFT_Fiss_7905_7926_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_7769() {
	CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7906_7927_split[0]), &(SplitJoin14_CombineDFT_Fiss_7906_7927_join[0]));
}

void CombineDFT_7770() {
	CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7906_7927_split[1]), &(SplitJoin14_CombineDFT_Fiss_7906_7927_join[1]));
}

void CombineDFT_7771() {
	CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7906_7927_split[2]), &(SplitJoin14_CombineDFT_Fiss_7906_7927_join[2]));
}

void CombineDFT_7772() {
	CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7906_7927_split[3]), &(SplitJoin14_CombineDFT_Fiss_7906_7927_join[3]));
}

void CombineDFT_7773() {
	CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7906_7927_split[4]), &(SplitJoin14_CombineDFT_Fiss_7906_7927_join[4]));
}

void CombineDFT_7774() {
	CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7906_7927_split[5]), &(SplitJoin14_CombineDFT_Fiss_7906_7927_join[5]));
}

void CombineDFT_7775() {
	CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7906_7927_split[6]), &(SplitJoin14_CombineDFT_Fiss_7906_7927_join[6]));
}

void CombineDFT_7776() {
	CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7906_7927_split[7]), &(SplitJoin14_CombineDFT_Fiss_7906_7927_join[7]));
}

void CombineDFT_7777() {
	CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7906_7927_split[8]), &(SplitJoin14_CombineDFT_Fiss_7906_7927_join[8]));
}

void CombineDFT_7778() {
	CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7906_7927_split[9]), &(SplitJoin14_CombineDFT_Fiss_7906_7927_join[9]));
}

void CombineDFT_7779() {
	CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7906_7927_split[10]), &(SplitJoin14_CombineDFT_Fiss_7906_7927_join[10]));
}

void CombineDFT_7780() {
	CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7906_7927_split[11]), &(SplitJoin14_CombineDFT_Fiss_7906_7927_join[11]));
}

void CombineDFT_7781() {
	CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7906_7927_split[12]), &(SplitJoin14_CombineDFT_Fiss_7906_7927_join[12]));
}

void CombineDFT_7782() {
	CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7906_7927_split[13]), &(SplitJoin14_CombineDFT_Fiss_7906_7927_join[13]));
}

void CombineDFT_7783() {
	CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7906_7927_split[14]), &(SplitJoin14_CombineDFT_Fiss_7906_7927_join[14]));
}

void CombineDFT_7784() {
	CombineDFT(&(SplitJoin14_CombineDFT_Fiss_7906_7927_split[15]), &(SplitJoin14_CombineDFT_Fiss_7906_7927_join[15]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7767() {
	FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_float(&SplitJoin14_CombineDFT_Fiss_7906_7927_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7750WEIGHTED_ROUND_ROBIN_Splitter_7767));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7768() {
	FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7768WEIGHTED_ROUND_ROBIN_Splitter_7785, pop_float(&SplitJoin14_CombineDFT_Fiss_7906_7927_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void CombineDFT_7787() {
	CombineDFT(&(SplitJoin16_CombineDFT_Fiss_7907_7928_split[0]), &(SplitJoin16_CombineDFT_Fiss_7907_7928_join[0]));
}

void CombineDFT_7788() {
	CombineDFT(&(SplitJoin16_CombineDFT_Fiss_7907_7928_split[1]), &(SplitJoin16_CombineDFT_Fiss_7907_7928_join[1]));
}

void CombineDFT_7789() {
	CombineDFT(&(SplitJoin16_CombineDFT_Fiss_7907_7928_split[2]), &(SplitJoin16_CombineDFT_Fiss_7907_7928_join[2]));
}

void CombineDFT_7790() {
	CombineDFT(&(SplitJoin16_CombineDFT_Fiss_7907_7928_split[3]), &(SplitJoin16_CombineDFT_Fiss_7907_7928_join[3]));
}

void CombineDFT_7791() {
	CombineDFT(&(SplitJoin16_CombineDFT_Fiss_7907_7928_split[4]), &(SplitJoin16_CombineDFT_Fiss_7907_7928_join[4]));
}

void CombineDFT_7792() {
	CombineDFT(&(SplitJoin16_CombineDFT_Fiss_7907_7928_split[5]), &(SplitJoin16_CombineDFT_Fiss_7907_7928_join[5]));
}

void CombineDFT_7793() {
	CombineDFT(&(SplitJoin16_CombineDFT_Fiss_7907_7928_split[6]), &(SplitJoin16_CombineDFT_Fiss_7907_7928_join[6]));
}

void CombineDFT_7794() {
	CombineDFT(&(SplitJoin16_CombineDFT_Fiss_7907_7928_split[7]), &(SplitJoin16_CombineDFT_Fiss_7907_7928_join[7]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7785() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
			push_float(&SplitJoin16_CombineDFT_Fiss_7907_7928_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7768WEIGHTED_ROUND_ROBIN_Splitter_7785));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7786() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7786WEIGHTED_ROUND_ROBIN_Splitter_7795, pop_float(&SplitJoin16_CombineDFT_Fiss_7907_7928_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void CombineDFT_7797() {
	CombineDFT(&(SplitJoin18_CombineDFT_Fiss_7908_7929_split[0]), &(SplitJoin18_CombineDFT_Fiss_7908_7929_join[0]));
}

void CombineDFT_7798() {
	CombineDFT(&(SplitJoin18_CombineDFT_Fiss_7908_7929_split[1]), &(SplitJoin18_CombineDFT_Fiss_7908_7929_join[1]));
}

void CombineDFT_7799() {
	CombineDFT(&(SplitJoin18_CombineDFT_Fiss_7908_7929_split[2]), &(SplitJoin18_CombineDFT_Fiss_7908_7929_join[2]));
}

void CombineDFT_7800() {
	CombineDFT(&(SplitJoin18_CombineDFT_Fiss_7908_7929_split[3]), &(SplitJoin18_CombineDFT_Fiss_7908_7929_join[3]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7795() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
			push_float(&SplitJoin18_CombineDFT_Fiss_7908_7929_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7786WEIGHTED_ROUND_ROBIN_Splitter_7795));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7796() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7796WEIGHTED_ROUND_ROBIN_Splitter_7801, pop_float(&SplitJoin18_CombineDFT_Fiss_7908_7929_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void CombineDFT_7803() {
	CombineDFT(&(SplitJoin20_CombineDFT_Fiss_7909_7930_split[0]), &(SplitJoin20_CombineDFT_Fiss_7909_7930_join[0]));
}

void CombineDFT_7804() {
	CombineDFT(&(SplitJoin20_CombineDFT_Fiss_7909_7930_split[1]), &(SplitJoin20_CombineDFT_Fiss_7909_7930_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7801() {
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&SplitJoin20_CombineDFT_Fiss_7909_7930_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7796WEIGHTED_ROUND_ROBIN_Splitter_7801));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&SplitJoin20_CombineDFT_Fiss_7909_7930_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7796WEIGHTED_ROUND_ROBIN_Splitter_7801));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7802() {
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7802CombineDFT_7686, pop_float(&SplitJoin20_CombineDFT_Fiss_7909_7930_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7802CombineDFT_7686, pop_float(&SplitJoin20_CombineDFT_Fiss_7909_7930_join[1]));
	ENDFOR
}

void CombineDFT_7686() {
	CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_7802CombineDFT_7686), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_7665_7701_7900_7921_join[0]));
}

void FFTReorderSimple_7687() {
	FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_7665_7701_7900_7921_split[1]), &(FFTReorderSimple_7687WEIGHTED_ROUND_ROBIN_Splitter_7805));
}

void FFTReorderSimple_7807() {
	FFTReorderSimple(&(SplitJoin90_FFTReorderSimple_Fiss_7910_7931_split[0]), &(SplitJoin90_FFTReorderSimple_Fiss_7910_7931_join[0]));
}

void FFTReorderSimple_7808() {
	FFTReorderSimple(&(SplitJoin90_FFTReorderSimple_Fiss_7910_7931_split[1]), &(SplitJoin90_FFTReorderSimple_Fiss_7910_7931_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7805() {
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&SplitJoin90_FFTReorderSimple_Fiss_7910_7931_split[0], pop_float(&FFTReorderSimple_7687WEIGHTED_ROUND_ROBIN_Splitter_7805));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&SplitJoin90_FFTReorderSimple_Fiss_7910_7931_split[1], pop_float(&FFTReorderSimple_7687WEIGHTED_ROUND_ROBIN_Splitter_7805));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7806() {
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7806WEIGHTED_ROUND_ROBIN_Splitter_7809, pop_float(&SplitJoin90_FFTReorderSimple_Fiss_7910_7931_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7806WEIGHTED_ROUND_ROBIN_Splitter_7809, pop_float(&SplitJoin90_FFTReorderSimple_Fiss_7910_7931_join[1]));
	ENDFOR
}

void FFTReorderSimple_7811() {
	FFTReorderSimple(&(SplitJoin92_FFTReorderSimple_Fiss_7911_7932_split[0]), &(SplitJoin92_FFTReorderSimple_Fiss_7911_7932_join[0]));
}

void FFTReorderSimple_7812() {
	FFTReorderSimple(&(SplitJoin92_FFTReorderSimple_Fiss_7911_7932_split[1]), &(SplitJoin92_FFTReorderSimple_Fiss_7911_7932_join[1]));
}

void FFTReorderSimple_7813() {
	FFTReorderSimple(&(SplitJoin92_FFTReorderSimple_Fiss_7911_7932_split[2]), &(SplitJoin92_FFTReorderSimple_Fiss_7911_7932_join[2]));
}

void FFTReorderSimple_7814() {
	FFTReorderSimple(&(SplitJoin92_FFTReorderSimple_Fiss_7911_7932_split[3]), &(SplitJoin92_FFTReorderSimple_Fiss_7911_7932_join[3]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7809() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
			push_float(&SplitJoin92_FFTReorderSimple_Fiss_7911_7932_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7806WEIGHTED_ROUND_ROBIN_Splitter_7809));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7810() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7810WEIGHTED_ROUND_ROBIN_Splitter_7815, pop_float(&SplitJoin92_FFTReorderSimple_Fiss_7911_7932_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void FFTReorderSimple_7817() {
	FFTReorderSimple(&(SplitJoin94_FFTReorderSimple_Fiss_7912_7933_split[0]), &(SplitJoin94_FFTReorderSimple_Fiss_7912_7933_join[0]));
}

void FFTReorderSimple_7818() {
	FFTReorderSimple(&(SplitJoin94_FFTReorderSimple_Fiss_7912_7933_split[1]), &(SplitJoin94_FFTReorderSimple_Fiss_7912_7933_join[1]));
}

void FFTReorderSimple_7819() {
	FFTReorderSimple(&(SplitJoin94_FFTReorderSimple_Fiss_7912_7933_split[2]), &(SplitJoin94_FFTReorderSimple_Fiss_7912_7933_join[2]));
}

void FFTReorderSimple_7820() {
	FFTReorderSimple(&(SplitJoin94_FFTReorderSimple_Fiss_7912_7933_split[3]), &(SplitJoin94_FFTReorderSimple_Fiss_7912_7933_join[3]));
}

void FFTReorderSimple_7821() {
	FFTReorderSimple(&(SplitJoin94_FFTReorderSimple_Fiss_7912_7933_split[4]), &(SplitJoin94_FFTReorderSimple_Fiss_7912_7933_join[4]));
}

void FFTReorderSimple_7822() {
	FFTReorderSimple(&(SplitJoin94_FFTReorderSimple_Fiss_7912_7933_split[5]), &(SplitJoin94_FFTReorderSimple_Fiss_7912_7933_join[5]));
}

void FFTReorderSimple_7823() {
	FFTReorderSimple(&(SplitJoin94_FFTReorderSimple_Fiss_7912_7933_split[6]), &(SplitJoin94_FFTReorderSimple_Fiss_7912_7933_join[6]));
}

void FFTReorderSimple_7824() {
	FFTReorderSimple(&(SplitJoin94_FFTReorderSimple_Fiss_7912_7933_split[7]), &(SplitJoin94_FFTReorderSimple_Fiss_7912_7933_join[7]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7815() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
			push_float(&SplitJoin94_FFTReorderSimple_Fiss_7912_7933_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7810WEIGHTED_ROUND_ROBIN_Splitter_7815));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7816() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7816WEIGHTED_ROUND_ROBIN_Splitter_7825, pop_float(&SplitJoin94_FFTReorderSimple_Fiss_7912_7933_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void FFTReorderSimple_7827() {
	FFTReorderSimple(&(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_split[0]), &(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_join[0]));
}

void FFTReorderSimple_7828() {
	FFTReorderSimple(&(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_split[1]), &(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_join[1]));
}

void FFTReorderSimple_7829() {
	FFTReorderSimple(&(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_split[2]), &(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_join[2]));
}

void FFTReorderSimple_7830() {
	FFTReorderSimple(&(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_split[3]), &(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_join[3]));
}

void FFTReorderSimple_7831() {
	FFTReorderSimple(&(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_split[4]), &(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_join[4]));
}

void FFTReorderSimple_7832() {
	FFTReorderSimple(&(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_split[5]), &(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_join[5]));
}

void FFTReorderSimple_7833() {
	FFTReorderSimple(&(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_split[6]), &(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_join[6]));
}

void FFTReorderSimple_7834() {
	FFTReorderSimple(&(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_split[7]), &(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_join[7]));
}

void FFTReorderSimple_7835() {
	FFTReorderSimple(&(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_split[8]), &(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_join[8]));
}

void FFTReorderSimple_7836() {
	FFTReorderSimple(&(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_split[9]), &(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_join[9]));
}

void FFTReorderSimple_7837() {
	FFTReorderSimple(&(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_split[10]), &(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_join[10]));
}

void FFTReorderSimple_7838() {
	FFTReorderSimple(&(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_split[11]), &(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_join[11]));
}

void FFTReorderSimple_7839() {
	FFTReorderSimple(&(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_split[12]), &(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_join[12]));
}

void FFTReorderSimple_7840() {
	FFTReorderSimple(&(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_split[13]), &(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_join[13]));
}

void FFTReorderSimple_7841() {
	FFTReorderSimple(&(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_split[14]), &(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_join[14]));
}

void FFTReorderSimple_7842() {
	FFTReorderSimple(&(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_split[15]), &(SplitJoin96_FFTReorderSimple_Fiss_7913_7934_join[15]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7825() {
	FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_float(&SplitJoin96_FFTReorderSimple_Fiss_7913_7934_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7816WEIGHTED_ROUND_ROBIN_Splitter_7825));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7826() {
	FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7826WEIGHTED_ROUND_ROBIN_Splitter_7843, pop_float(&SplitJoin96_FFTReorderSimple_Fiss_7913_7934_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void CombineDFT_7845() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin98_CombineDFT_Fiss_7914_7935_split[0]), &(SplitJoin98_CombineDFT_Fiss_7914_7935_join[0]));
	ENDFOR
}

void CombineDFT_7846() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin98_CombineDFT_Fiss_7914_7935_split[1]), &(SplitJoin98_CombineDFT_Fiss_7914_7935_join[1]));
	ENDFOR
}

void CombineDFT_7847() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin98_CombineDFT_Fiss_7914_7935_split[2]), &(SplitJoin98_CombineDFT_Fiss_7914_7935_join[2]));
	ENDFOR
}

void CombineDFT_7848() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin98_CombineDFT_Fiss_7914_7935_split[3]), &(SplitJoin98_CombineDFT_Fiss_7914_7935_join[3]));
	ENDFOR
}

void CombineDFT_7849() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin98_CombineDFT_Fiss_7914_7935_split[4]), &(SplitJoin98_CombineDFT_Fiss_7914_7935_join[4]));
	ENDFOR
}

void CombineDFT_7850() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin98_CombineDFT_Fiss_7914_7935_split[5]), &(SplitJoin98_CombineDFT_Fiss_7914_7935_join[5]));
	ENDFOR
}

void CombineDFT_7851() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin98_CombineDFT_Fiss_7914_7935_split[6]), &(SplitJoin98_CombineDFT_Fiss_7914_7935_join[6]));
	ENDFOR
}

void CombineDFT_7852() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin98_CombineDFT_Fiss_7914_7935_split[7]), &(SplitJoin98_CombineDFT_Fiss_7914_7935_join[7]));
	ENDFOR
}

void CombineDFT_7853() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin98_CombineDFT_Fiss_7914_7935_split[8]), &(SplitJoin98_CombineDFT_Fiss_7914_7935_join[8]));
	ENDFOR
}

void CombineDFT_7854() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin98_CombineDFT_Fiss_7914_7935_split[9]), &(SplitJoin98_CombineDFT_Fiss_7914_7935_join[9]));
	ENDFOR
}

void CombineDFT_7855() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin98_CombineDFT_Fiss_7914_7935_split[10]), &(SplitJoin98_CombineDFT_Fiss_7914_7935_join[10]));
	ENDFOR
}

void CombineDFT_7856() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin98_CombineDFT_Fiss_7914_7935_split[11]), &(SplitJoin98_CombineDFT_Fiss_7914_7935_join[11]));
	ENDFOR
}

void CombineDFT_7857() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin98_CombineDFT_Fiss_7914_7935_split[12]), &(SplitJoin98_CombineDFT_Fiss_7914_7935_join[12]));
	ENDFOR
}

void CombineDFT_7858() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin98_CombineDFT_Fiss_7914_7935_split[13]), &(SplitJoin98_CombineDFT_Fiss_7914_7935_join[13]));
	ENDFOR
}

void CombineDFT_7859() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin98_CombineDFT_Fiss_7914_7935_split[14]), &(SplitJoin98_CombineDFT_Fiss_7914_7935_join[14]));
	ENDFOR
}

void CombineDFT_7860() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		CombineDFT(&(SplitJoin98_CombineDFT_Fiss_7914_7935_split[15]), &(SplitJoin98_CombineDFT_Fiss_7914_7935_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7843() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin98_CombineDFT_Fiss_7914_7935_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7826WEIGHTED_ROUND_ROBIN_Splitter_7843));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7844() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7844WEIGHTED_ROUND_ROBIN_Splitter_7861, pop_float(&SplitJoin98_CombineDFT_Fiss_7914_7935_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_7863() {
	CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7915_7936_split[0]), &(SplitJoin100_CombineDFT_Fiss_7915_7936_join[0]));
}

void CombineDFT_7864() {
	CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7915_7936_split[1]), &(SplitJoin100_CombineDFT_Fiss_7915_7936_join[1]));
}

void CombineDFT_7865() {
	CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7915_7936_split[2]), &(SplitJoin100_CombineDFT_Fiss_7915_7936_join[2]));
}

void CombineDFT_7866() {
	CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7915_7936_split[3]), &(SplitJoin100_CombineDFT_Fiss_7915_7936_join[3]));
}

void CombineDFT_7867() {
	CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7915_7936_split[4]), &(SplitJoin100_CombineDFT_Fiss_7915_7936_join[4]));
}

void CombineDFT_7868() {
	CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7915_7936_split[5]), &(SplitJoin100_CombineDFT_Fiss_7915_7936_join[5]));
}

void CombineDFT_7869() {
	CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7915_7936_split[6]), &(SplitJoin100_CombineDFT_Fiss_7915_7936_join[6]));
}

void CombineDFT_7870() {
	CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7915_7936_split[7]), &(SplitJoin100_CombineDFT_Fiss_7915_7936_join[7]));
}

void CombineDFT_7871() {
	CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7915_7936_split[8]), &(SplitJoin100_CombineDFT_Fiss_7915_7936_join[8]));
}

void CombineDFT_7872() {
	CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7915_7936_split[9]), &(SplitJoin100_CombineDFT_Fiss_7915_7936_join[9]));
}

void CombineDFT_7873() {
	CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7915_7936_split[10]), &(SplitJoin100_CombineDFT_Fiss_7915_7936_join[10]));
}

void CombineDFT_7874() {
	CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7915_7936_split[11]), &(SplitJoin100_CombineDFT_Fiss_7915_7936_join[11]));
}

void CombineDFT_7875() {
	CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7915_7936_split[12]), &(SplitJoin100_CombineDFT_Fiss_7915_7936_join[12]));
}

void CombineDFT_7876() {
	CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7915_7936_split[13]), &(SplitJoin100_CombineDFT_Fiss_7915_7936_join[13]));
}

void CombineDFT_7877() {
	CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7915_7936_split[14]), &(SplitJoin100_CombineDFT_Fiss_7915_7936_join[14]));
}

void CombineDFT_7878() {
	CombineDFT(&(SplitJoin100_CombineDFT_Fiss_7915_7936_split[15]), &(SplitJoin100_CombineDFT_Fiss_7915_7936_join[15]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7861() {
	FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_float(&SplitJoin100_CombineDFT_Fiss_7915_7936_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7844WEIGHTED_ROUND_ROBIN_Splitter_7861));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7862() {
	FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7862WEIGHTED_ROUND_ROBIN_Splitter_7879, pop_float(&SplitJoin100_CombineDFT_Fiss_7915_7936_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void CombineDFT_7881() {
	CombineDFT(&(SplitJoin102_CombineDFT_Fiss_7916_7937_split[0]), &(SplitJoin102_CombineDFT_Fiss_7916_7937_join[0]));
}

void CombineDFT_7882() {
	CombineDFT(&(SplitJoin102_CombineDFT_Fiss_7916_7937_split[1]), &(SplitJoin102_CombineDFT_Fiss_7916_7937_join[1]));
}

void CombineDFT_7883() {
	CombineDFT(&(SplitJoin102_CombineDFT_Fiss_7916_7937_split[2]), &(SplitJoin102_CombineDFT_Fiss_7916_7937_join[2]));
}

void CombineDFT_7884() {
	CombineDFT(&(SplitJoin102_CombineDFT_Fiss_7916_7937_split[3]), &(SplitJoin102_CombineDFT_Fiss_7916_7937_join[3]));
}

void CombineDFT_7885() {
	CombineDFT(&(SplitJoin102_CombineDFT_Fiss_7916_7937_split[4]), &(SplitJoin102_CombineDFT_Fiss_7916_7937_join[4]));
}

void CombineDFT_7886() {
	CombineDFT(&(SplitJoin102_CombineDFT_Fiss_7916_7937_split[5]), &(SplitJoin102_CombineDFT_Fiss_7916_7937_join[5]));
}

void CombineDFT_7887() {
	CombineDFT(&(SplitJoin102_CombineDFT_Fiss_7916_7937_split[6]), &(SplitJoin102_CombineDFT_Fiss_7916_7937_join[6]));
}

void CombineDFT_7888() {
	CombineDFT(&(SplitJoin102_CombineDFT_Fiss_7916_7937_split[7]), &(SplitJoin102_CombineDFT_Fiss_7916_7937_join[7]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7879() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
			push_float(&SplitJoin102_CombineDFT_Fiss_7916_7937_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7862WEIGHTED_ROUND_ROBIN_Splitter_7879));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7880() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7880WEIGHTED_ROUND_ROBIN_Splitter_7889, pop_float(&SplitJoin102_CombineDFT_Fiss_7916_7937_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void CombineDFT_7891() {
	CombineDFT(&(SplitJoin104_CombineDFT_Fiss_7917_7938_split[0]), &(SplitJoin104_CombineDFT_Fiss_7917_7938_join[0]));
}

void CombineDFT_7892() {
	CombineDFT(&(SplitJoin104_CombineDFT_Fiss_7917_7938_split[1]), &(SplitJoin104_CombineDFT_Fiss_7917_7938_join[1]));
}

void CombineDFT_7893() {
	CombineDFT(&(SplitJoin104_CombineDFT_Fiss_7917_7938_split[2]), &(SplitJoin104_CombineDFT_Fiss_7917_7938_join[2]));
}

void CombineDFT_7894() {
	CombineDFT(&(SplitJoin104_CombineDFT_Fiss_7917_7938_split[3]), &(SplitJoin104_CombineDFT_Fiss_7917_7938_join[3]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7889() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
			push_float(&SplitJoin104_CombineDFT_Fiss_7917_7938_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7880WEIGHTED_ROUND_ROBIN_Splitter_7889));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7890() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7890WEIGHTED_ROUND_ROBIN_Splitter_7895, pop_float(&SplitJoin104_CombineDFT_Fiss_7917_7938_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void CombineDFT_7897() {
	CombineDFT(&(SplitJoin106_CombineDFT_Fiss_7918_7939_split[0]), &(SplitJoin106_CombineDFT_Fiss_7918_7939_join[0]));
}

void CombineDFT_7898() {
	CombineDFT(&(SplitJoin106_CombineDFT_Fiss_7918_7939_split[1]), &(SplitJoin106_CombineDFT_Fiss_7918_7939_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7895() {
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&SplitJoin106_CombineDFT_Fiss_7918_7939_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7890WEIGHTED_ROUND_ROBIN_Splitter_7895));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&SplitJoin106_CombineDFT_Fiss_7918_7939_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7890WEIGHTED_ROUND_ROBIN_Splitter_7895));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7896() {
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7896CombineDFT_7697, pop_float(&SplitJoin106_CombineDFT_Fiss_7918_7939_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7896CombineDFT_7697, pop_float(&SplitJoin106_CombineDFT_Fiss_7918_7939_join[1]));
	ENDFOR
}

void CombineDFT_7697() {
	CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_7896CombineDFT_7697), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_7665_7701_7900_7921_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_7699() {
	FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
		push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_7665_7701_7900_7921_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7708WEIGHTED_ROUND_ROBIN_Splitter_7699));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
		push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_7665_7701_7900_7921_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_7708WEIGHTED_ROUND_ROBIN_Splitter_7699));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_7700() {
	FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7700FloatPrinter_7698, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_7665_7701_7900_7921_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7700FloatPrinter_7698, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_7665_7701_7900_7921_join[1]));
	ENDFOR
}

void FloatPrinter(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void FloatPrinter_7698() {
	FOR(uint32_t, __iter_steady_, 0, <, 256, __iter_steady_++)
		FloatPrinter(&(WEIGHTED_ROUND_ROBIN_Joiner_7700FloatPrinter_7698));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_float(&SplitJoin102_CombineDFT_Fiss_7916_7937_split[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7862WEIGHTED_ROUND_ROBIN_Splitter_7879);
	FOR(int, __iter_init_1_, 0, <, 2, __iter_init_1_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_7909_7930_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_float(&SplitJoin106_CombineDFT_Fiss_7918_7939_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 2, __iter_init_3_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_7899_7920_join[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&FFTReorderSimple_7676WEIGHTED_ROUND_ROBIN_Splitter_7711);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7786WEIGHTED_ROUND_ROBIN_Splitter_7795);
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_float(&SplitJoin94_FFTReorderSimple_Fiss_7912_7933_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 16, __iter_init_5_++)
		init_buffer_float(&SplitJoin98_CombineDFT_Fiss_7914_7935_join[__iter_init_5_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7768WEIGHTED_ROUND_ROBIN_Splitter_7785);
	FOR(int, __iter_init_6_, 0, <, 16, __iter_init_6_++)
		init_buffer_float(&SplitJoin96_FFTReorderSimple_Fiss_7913_7934_split[__iter_init_6_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7806WEIGHTED_ROUND_ROBIN_Splitter_7809);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7844WEIGHTED_ROUND_ROBIN_Splitter_7861);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7880WEIGHTED_ROUND_ROBIN_Splitter_7889);
	FOR(int, __iter_init_7_, 0, <, 16, __iter_init_7_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_7904_7925_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_7903_7924_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_float(&SplitJoin94_FFTReorderSimple_Fiss_7912_7933_split[__iter_init_9_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7722WEIGHTED_ROUND_ROBIN_Splitter_7731);
	FOR(int, __iter_init_10_, 0, <, 16, __iter_init_10_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_7906_7927_join[__iter_init_10_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7896CombineDFT_7697);
	FOR(int, __iter_init_11_, 0, <, 2, __iter_init_11_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_7665_7701_7900_7921_join[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 4, __iter_init_12_++)
		init_buffer_float(&SplitJoin92_FFTReorderSimple_Fiss_7911_7932_split[__iter_init_12_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7802CombineDFT_7686);
	FOR(int, __iter_init_13_, 0, <, 4, __iter_init_13_++)
		init_buffer_float(&SplitJoin92_FFTReorderSimple_Fiss_7911_7932_join[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 8, __iter_init_14_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_7907_7928_split[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 2, __iter_init_15_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_7901_7922_split[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 4, __iter_init_16_++)
		init_buffer_float(&SplitJoin104_CombineDFT_Fiss_7917_7938_join[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_7899_7920_split[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 2, __iter_init_18_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_7665_7701_7900_7921_split[__iter_init_18_]);
	ENDFOR
	FOR(int, __iter_init_19_, 0, <, 4, __iter_init_19_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_7902_7923_join[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 8, __iter_init_20_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_7903_7924_split[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 8, __iter_init_21_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_7907_7928_join[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 4, __iter_init_22_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_7908_7929_split[__iter_init_22_]);
	ENDFOR
	FOR(int, __iter_init_23_, 0, <, 2, __iter_init_23_++)
		init_buffer_float(&SplitJoin106_CombineDFT_Fiss_7918_7939_split[__iter_init_23_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7708WEIGHTED_ROUND_ROBIN_Splitter_7699);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7810WEIGHTED_ROUND_ROBIN_Splitter_7815);
	FOR(int, __iter_init_24_, 0, <, 16, __iter_init_24_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_7906_7927_split[__iter_init_24_]);
	ENDFOR
	FOR(int, __iter_init_25_, 0, <, 8, __iter_init_25_++)
		init_buffer_float(&SplitJoin102_CombineDFT_Fiss_7916_7937_join[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 16, __iter_init_26_++)
		init_buffer_float(&SplitJoin98_CombineDFT_Fiss_7914_7935_split[__iter_init_26_]);
	ENDFOR
	FOR(int, __iter_init_27_, 0, <, 2, __iter_init_27_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_7901_7922_join[__iter_init_27_]);
	ENDFOR
	FOR(int, __iter_init_28_, 0, <, 2, __iter_init_28_++)
		init_buffer_float(&SplitJoin90_FFTReorderSimple_Fiss_7910_7931_join[__iter_init_28_]);
	ENDFOR
	FOR(int, __iter_init_29_, 0, <, 4, __iter_init_29_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_7908_7929_join[__iter_init_29_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7826WEIGHTED_ROUND_ROBIN_Splitter_7843);
	FOR(int, __iter_init_30_, 0, <, 16, __iter_init_30_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_7904_7925_join[__iter_init_30_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7732WEIGHTED_ROUND_ROBIN_Splitter_7749);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7890WEIGHTED_ROUND_ROBIN_Splitter_7895);
	FOR(int, __iter_init_31_, 0, <, 2, __iter_init_31_++)
		init_buffer_float(&SplitJoin90_FFTReorderSimple_Fiss_7910_7931_split[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 4, __iter_init_32_++)
		init_buffer_float(&SplitJoin104_CombineDFT_Fiss_7917_7938_split[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 16, __iter_init_33_++)
		init_buffer_float(&SplitJoin96_FFTReorderSimple_Fiss_7913_7934_join[__iter_init_33_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7700FloatPrinter_7698);
	FOR(int, __iter_init_34_, 0, <, 2, __iter_init_34_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_7909_7930_split[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 4, __iter_init_35_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_7902_7923_split[__iter_init_35_]);
	ENDFOR
	FOR(int, __iter_init_36_, 0, <, 16, __iter_init_36_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_7905_7926_split[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 16, __iter_init_37_++)
		init_buffer_float(&SplitJoin100_CombineDFT_Fiss_7915_7936_split[__iter_init_37_]);
	ENDFOR
	FOR(int, __iter_init_38_, 0, <, 16, __iter_init_38_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_7905_7926_join[__iter_init_38_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7750WEIGHTED_ROUND_ROBIN_Splitter_7767);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7816WEIGHTED_ROUND_ROBIN_Splitter_7825);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7712WEIGHTED_ROUND_ROBIN_Splitter_7715);
	init_buffer_float(&FFTReorderSimple_7687WEIGHTED_ROUND_ROBIN_Splitter_7805);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7716WEIGHTED_ROUND_ROBIN_Splitter_7721);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7796WEIGHTED_ROUND_ROBIN_Splitter_7801);
	FOR(int, __iter_init_39_, 0, <, 16, __iter_init_39_++)
		init_buffer_float(&SplitJoin100_CombineDFT_Fiss_7915_7936_join[__iter_init_39_]);
	ENDFOR
// --- init: CombineDFT_7751
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7751_s.w[i] = real ; 
		CombineDFT_7751_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7752
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7752_s.w[i] = real ; 
		CombineDFT_7752_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7753
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7753_s.w[i] = real ; 
		CombineDFT_7753_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7754
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7754_s.w[i] = real ; 
		CombineDFT_7754_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7755
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7755_s.w[i] = real ; 
		CombineDFT_7755_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7756
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7756_s.w[i] = real ; 
		CombineDFT_7756_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7757
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7757_s.w[i] = real ; 
		CombineDFT_7757_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7758
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7758_s.w[i] = real ; 
		CombineDFT_7758_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7759
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7759_s.w[i] = real ; 
		CombineDFT_7759_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7760
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7760_s.w[i] = real ; 
		CombineDFT_7760_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7761
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7761_s.w[i] = real ; 
		CombineDFT_7761_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7762
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7762_s.w[i] = real ; 
		CombineDFT_7762_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7763
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7763_s.w[i] = real ; 
		CombineDFT_7763_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7764
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7764_s.w[i] = real ; 
		CombineDFT_7764_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7765
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7765_s.w[i] = real ; 
		CombineDFT_7765_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7766
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7766_s.w[i] = real ; 
		CombineDFT_7766_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7769
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7769_s.w[i] = real ; 
		CombineDFT_7769_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7770
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7770_s.w[i] = real ; 
		CombineDFT_7770_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7771
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7771_s.w[i] = real ; 
		CombineDFT_7771_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7772
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7772_s.w[i] = real ; 
		CombineDFT_7772_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7773
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7773_s.w[i] = real ; 
		CombineDFT_7773_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7774
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7774_s.w[i] = real ; 
		CombineDFT_7774_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7775
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7775_s.w[i] = real ; 
		CombineDFT_7775_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7776
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7776_s.w[i] = real ; 
		CombineDFT_7776_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7777
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7777_s.w[i] = real ; 
		CombineDFT_7777_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7778
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7778_s.w[i] = real ; 
		CombineDFT_7778_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7779
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7779_s.w[i] = real ; 
		CombineDFT_7779_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7780
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7780_s.w[i] = real ; 
		CombineDFT_7780_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7781
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7781_s.w[i] = real ; 
		CombineDFT_7781_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7782
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7782_s.w[i] = real ; 
		CombineDFT_7782_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7783
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7783_s.w[i] = real ; 
		CombineDFT_7783_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7784
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7784_s.w[i] = real ; 
		CombineDFT_7784_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7787
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7787_s.w[i] = real ; 
		CombineDFT_7787_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7788
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7788_s.w[i] = real ; 
		CombineDFT_7788_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7789
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7789_s.w[i] = real ; 
		CombineDFT_7789_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7790
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7790_s.w[i] = real ; 
		CombineDFT_7790_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7791
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7791_s.w[i] = real ; 
		CombineDFT_7791_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7792
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7792_s.w[i] = real ; 
		CombineDFT_7792_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7793
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7793_s.w[i] = real ; 
		CombineDFT_7793_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7794
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7794_s.w[i] = real ; 
		CombineDFT_7794_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7797
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_7797_s.w[i] = real ; 
		CombineDFT_7797_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7798
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_7798_s.w[i] = real ; 
		CombineDFT_7798_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7799
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_7799_s.w[i] = real ; 
		CombineDFT_7799_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7800
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_7800_s.w[i] = real ; 
		CombineDFT_7800_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7803
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_7803_s.w[i] = real ; 
		CombineDFT_7803_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7804
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_7804_s.w[i] = real ; 
		CombineDFT_7804_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7686
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_7686_s.w[i] = real ; 
		CombineDFT_7686_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7845
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7845_s.w[i] = real ; 
		CombineDFT_7845_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7846
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7846_s.w[i] = real ; 
		CombineDFT_7846_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7847
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7847_s.w[i] = real ; 
		CombineDFT_7847_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7848
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7848_s.w[i] = real ; 
		CombineDFT_7848_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7849
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7849_s.w[i] = real ; 
		CombineDFT_7849_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7850
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7850_s.w[i] = real ; 
		CombineDFT_7850_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7851
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7851_s.w[i] = real ; 
		CombineDFT_7851_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7852
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7852_s.w[i] = real ; 
		CombineDFT_7852_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7853
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7853_s.w[i] = real ; 
		CombineDFT_7853_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7854
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7854_s.w[i] = real ; 
		CombineDFT_7854_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7855
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7855_s.w[i] = real ; 
		CombineDFT_7855_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7856
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7856_s.w[i] = real ; 
		CombineDFT_7856_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7857
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7857_s.w[i] = real ; 
		CombineDFT_7857_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7858
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7858_s.w[i] = real ; 
		CombineDFT_7858_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7859
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7859_s.w[i] = real ; 
		CombineDFT_7859_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7860
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_7860_s.w[i] = real ; 
		CombineDFT_7860_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7863
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7863_s.w[i] = real ; 
		CombineDFT_7863_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7864
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7864_s.w[i] = real ; 
		CombineDFT_7864_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7865
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7865_s.w[i] = real ; 
		CombineDFT_7865_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7866
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7866_s.w[i] = real ; 
		CombineDFT_7866_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7867
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7867_s.w[i] = real ; 
		CombineDFT_7867_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7868
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7868_s.w[i] = real ; 
		CombineDFT_7868_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7869
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7869_s.w[i] = real ; 
		CombineDFT_7869_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7870
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7870_s.w[i] = real ; 
		CombineDFT_7870_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7871
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7871_s.w[i] = real ; 
		CombineDFT_7871_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7872
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7872_s.w[i] = real ; 
		CombineDFT_7872_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7873
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7873_s.w[i] = real ; 
		CombineDFT_7873_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7874
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7874_s.w[i] = real ; 
		CombineDFT_7874_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7875
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7875_s.w[i] = real ; 
		CombineDFT_7875_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7876
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7876_s.w[i] = real ; 
		CombineDFT_7876_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7877
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7877_s.w[i] = real ; 
		CombineDFT_7877_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7878
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_7878_s.w[i] = real ; 
		CombineDFT_7878_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7881
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7881_s.w[i] = real ; 
		CombineDFT_7881_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7882
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7882_s.w[i] = real ; 
		CombineDFT_7882_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7883
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7883_s.w[i] = real ; 
		CombineDFT_7883_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7884
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7884_s.w[i] = real ; 
		CombineDFT_7884_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7885
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7885_s.w[i] = real ; 
		CombineDFT_7885_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7886
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7886_s.w[i] = real ; 
		CombineDFT_7886_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7887
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7887_s.w[i] = real ; 
		CombineDFT_7887_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7888
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_7888_s.w[i] = real ; 
		CombineDFT_7888_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7891
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_7891_s.w[i] = real ; 
		CombineDFT_7891_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7892
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_7892_s.w[i] = real ; 
		CombineDFT_7892_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7893
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_7893_s.w[i] = real ; 
		CombineDFT_7893_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7894
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_7894_s.w[i] = real ; 
		CombineDFT_7894_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7897
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_7897_s.w[i] = real ; 
		CombineDFT_7897_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7898
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_7898_s.w[i] = real ; 
		CombineDFT_7898_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_7697
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_7697_s.w[i] = real ; 
		CombineDFT_7697_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_7707();
			FFTTestSource_7709();
			FFTTestSource_7710();
		WEIGHTED_ROUND_ROBIN_Joiner_7708();
		WEIGHTED_ROUND_ROBIN_Splitter_7699();
			FFTReorderSimple_7676();
			WEIGHTED_ROUND_ROBIN_Splitter_7711();
				FFTReorderSimple_7713();
				FFTReorderSimple_7714();
			WEIGHTED_ROUND_ROBIN_Joiner_7712();
			WEIGHTED_ROUND_ROBIN_Splitter_7715();
				FFTReorderSimple_7717();
				FFTReorderSimple_7718();
				FFTReorderSimple_7719();
				FFTReorderSimple_7720();
			WEIGHTED_ROUND_ROBIN_Joiner_7716();
			WEIGHTED_ROUND_ROBIN_Splitter_7721();
				FFTReorderSimple_7723();
				FFTReorderSimple_7724();
				FFTReorderSimple_7725();
				FFTReorderSimple_7726();
				FFTReorderSimple_7727();
				FFTReorderSimple_7728();
				FFTReorderSimple_7729();
				FFTReorderSimple_7730();
			WEIGHTED_ROUND_ROBIN_Joiner_7722();
			WEIGHTED_ROUND_ROBIN_Splitter_7731();
				FFTReorderSimple_7733();
				FFTReorderSimple_7734();
				FFTReorderSimple_7735();
				FFTReorderSimple_7736();
				FFTReorderSimple_7737();
				FFTReorderSimple_7738();
				FFTReorderSimple_7739();
				FFTReorderSimple_7740();
				FFTReorderSimple_7741();
				FFTReorderSimple_7742();
				FFTReorderSimple_7743();
				FFTReorderSimple_7744();
				FFTReorderSimple_7745();
				FFTReorderSimple_7746();
				FFTReorderSimple_7747();
				FFTReorderSimple_7748();
			WEIGHTED_ROUND_ROBIN_Joiner_7732();
			WEIGHTED_ROUND_ROBIN_Splitter_7749();
				CombineDFT_7751();
				CombineDFT_7752();
				CombineDFT_7753();
				CombineDFT_7754();
				CombineDFT_7755();
				CombineDFT_7756();
				CombineDFT_7757();
				CombineDFT_7758();
				CombineDFT_7759();
				CombineDFT_7760();
				CombineDFT_7761();
				CombineDFT_7762();
				CombineDFT_7763();
				CombineDFT_7764();
				CombineDFT_7765();
				CombineDFT_7766();
			WEIGHTED_ROUND_ROBIN_Joiner_7750();
			WEIGHTED_ROUND_ROBIN_Splitter_7767();
				CombineDFT_7769();
				CombineDFT_7770();
				CombineDFT_7771();
				CombineDFT_7772();
				CombineDFT_7773();
				CombineDFT_7774();
				CombineDFT_7775();
				CombineDFT_7776();
				CombineDFT_7777();
				CombineDFT_7778();
				CombineDFT_7779();
				CombineDFT_7780();
				CombineDFT_7781();
				CombineDFT_7782();
				CombineDFT_7783();
				CombineDFT_7784();
			WEIGHTED_ROUND_ROBIN_Joiner_7768();
			WEIGHTED_ROUND_ROBIN_Splitter_7785();
				CombineDFT_7787();
				CombineDFT_7788();
				CombineDFT_7789();
				CombineDFT_7790();
				CombineDFT_7791();
				CombineDFT_7792();
				CombineDFT_7793();
				CombineDFT_7794();
			WEIGHTED_ROUND_ROBIN_Joiner_7786();
			WEIGHTED_ROUND_ROBIN_Splitter_7795();
				CombineDFT_7797();
				CombineDFT_7798();
				CombineDFT_7799();
				CombineDFT_7800();
			WEIGHTED_ROUND_ROBIN_Joiner_7796();
			WEIGHTED_ROUND_ROBIN_Splitter_7801();
				CombineDFT_7803();
				CombineDFT_7804();
			WEIGHTED_ROUND_ROBIN_Joiner_7802();
			CombineDFT_7686();
			FFTReorderSimple_7687();
			WEIGHTED_ROUND_ROBIN_Splitter_7805();
				FFTReorderSimple_7807();
				FFTReorderSimple_7808();
			WEIGHTED_ROUND_ROBIN_Joiner_7806();
			WEIGHTED_ROUND_ROBIN_Splitter_7809();
				FFTReorderSimple_7811();
				FFTReorderSimple_7812();
				FFTReorderSimple_7813();
				FFTReorderSimple_7814();
			WEIGHTED_ROUND_ROBIN_Joiner_7810();
			WEIGHTED_ROUND_ROBIN_Splitter_7815();
				FFTReorderSimple_7817();
				FFTReorderSimple_7818();
				FFTReorderSimple_7819();
				FFTReorderSimple_7820();
				FFTReorderSimple_7821();
				FFTReorderSimple_7822();
				FFTReorderSimple_7823();
				FFTReorderSimple_7824();
			WEIGHTED_ROUND_ROBIN_Joiner_7816();
			WEIGHTED_ROUND_ROBIN_Splitter_7825();
				FFTReorderSimple_7827();
				FFTReorderSimple_7828();
				FFTReorderSimple_7829();
				FFTReorderSimple_7830();
				FFTReorderSimple_7831();
				FFTReorderSimple_7832();
				FFTReorderSimple_7833();
				FFTReorderSimple_7834();
				FFTReorderSimple_7835();
				FFTReorderSimple_7836();
				FFTReorderSimple_7837();
				FFTReorderSimple_7838();
				FFTReorderSimple_7839();
				FFTReorderSimple_7840();
				FFTReorderSimple_7841();
				FFTReorderSimple_7842();
			WEIGHTED_ROUND_ROBIN_Joiner_7826();
			WEIGHTED_ROUND_ROBIN_Splitter_7843();
				CombineDFT_7845();
				CombineDFT_7846();
				CombineDFT_7847();
				CombineDFT_7848();
				CombineDFT_7849();
				CombineDFT_7850();
				CombineDFT_7851();
				CombineDFT_7852();
				CombineDFT_7853();
				CombineDFT_7854();
				CombineDFT_7855();
				CombineDFT_7856();
				CombineDFT_7857();
				CombineDFT_7858();
				CombineDFT_7859();
				CombineDFT_7860();
			WEIGHTED_ROUND_ROBIN_Joiner_7844();
			WEIGHTED_ROUND_ROBIN_Splitter_7861();
				CombineDFT_7863();
				CombineDFT_7864();
				CombineDFT_7865();
				CombineDFT_7866();
				CombineDFT_7867();
				CombineDFT_7868();
				CombineDFT_7869();
				CombineDFT_7870();
				CombineDFT_7871();
				CombineDFT_7872();
				CombineDFT_7873();
				CombineDFT_7874();
				CombineDFT_7875();
				CombineDFT_7876();
				CombineDFT_7877();
				CombineDFT_7878();
			WEIGHTED_ROUND_ROBIN_Joiner_7862();
			WEIGHTED_ROUND_ROBIN_Splitter_7879();
				CombineDFT_7881();
				CombineDFT_7882();
				CombineDFT_7883();
				CombineDFT_7884();
				CombineDFT_7885();
				CombineDFT_7886();
				CombineDFT_7887();
				CombineDFT_7888();
			WEIGHTED_ROUND_ROBIN_Joiner_7880();
			WEIGHTED_ROUND_ROBIN_Splitter_7889();
				CombineDFT_7891();
				CombineDFT_7892();
				CombineDFT_7893();
				CombineDFT_7894();
			WEIGHTED_ROUND_ROBIN_Joiner_7890();
			WEIGHTED_ROUND_ROBIN_Splitter_7895();
				CombineDFT_7897();
				CombineDFT_7898();
			WEIGHTED_ROUND_ROBIN_Joiner_7896();
			CombineDFT_7697();
		WEIGHTED_ROUND_ROBIN_Joiner_7700();
		FloatPrinter_7698();
	ENDFOR
	return EXIT_SUCCESS;
}
