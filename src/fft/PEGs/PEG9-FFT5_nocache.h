#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=288 on the compile command line
#else
#if BUF_SIZEMAX < 288
#error BUF_SIZEMAX too small, it must be at least 288
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif


void WEIGHTED_ROUND_ROBIN_Splitter_5981();
void source_5983();
void source_5984();
void WEIGHTED_ROUND_ROBIN_Joiner_5982();
void WEIGHTED_ROUND_ROBIN_Splitter_5824();
void WEIGHTED_ROUND_ROBIN_Splitter_5826();
void WEIGHTED_ROUND_ROBIN_Splitter_5828();
void WEIGHTED_ROUND_ROBIN_Splitter_5830();
void Identity_5654();
void Identity_5656();
void WEIGHTED_ROUND_ROBIN_Joiner_5831();
void WEIGHTED_ROUND_ROBIN_Splitter_5832();
void Identity_5660();
void Identity_5662();
void WEIGHTED_ROUND_ROBIN_Joiner_5833();
void WEIGHTED_ROUND_ROBIN_Joiner_5829();
void WEIGHTED_ROUND_ROBIN_Splitter_5834();
void WEIGHTED_ROUND_ROBIN_Splitter_5836();
void Identity_5668();
void Identity_5670();
void WEIGHTED_ROUND_ROBIN_Joiner_5837();
void WEIGHTED_ROUND_ROBIN_Splitter_5838();
void Identity_5674();
void Identity_5676();
void WEIGHTED_ROUND_ROBIN_Joiner_5839();
void WEIGHTED_ROUND_ROBIN_Joiner_5835();
void WEIGHTED_ROUND_ROBIN_Joiner_5827();
void WEIGHTED_ROUND_ROBIN_Splitter_5840();
void WEIGHTED_ROUND_ROBIN_Splitter_5842();
void WEIGHTED_ROUND_ROBIN_Splitter_5844();
void Identity_5684();
void Identity_5686();
void WEIGHTED_ROUND_ROBIN_Joiner_5845();
void WEIGHTED_ROUND_ROBIN_Splitter_5846();
void Identity_5690();
void Identity_5692();
void WEIGHTED_ROUND_ROBIN_Joiner_5847();
void WEIGHTED_ROUND_ROBIN_Joiner_5843();
void WEIGHTED_ROUND_ROBIN_Splitter_5848();
void WEIGHTED_ROUND_ROBIN_Splitter_5850();
void Identity_5698();
void Identity_5700();
void WEIGHTED_ROUND_ROBIN_Joiner_5851();
void WEIGHTED_ROUND_ROBIN_Splitter_5852();
void Identity_5704();
void Identity_5706();
void WEIGHTED_ROUND_ROBIN_Joiner_5853();
void WEIGHTED_ROUND_ROBIN_Joiner_5849();
void WEIGHTED_ROUND_ROBIN_Joiner_5841();
void WEIGHTED_ROUND_ROBIN_Joiner_5825();
void WEIGHTED_ROUND_ROBIN_Splitter_5968();
void WEIGHTED_ROUND_ROBIN_Splitter_5969();
void Pre_CollapsedDataParallel_1_5801();
void butterfly_5708();
void Post_CollapsedDataParallel_2_5802();
void Pre_CollapsedDataParallel_1_5804();
void butterfly_5709();
void Post_CollapsedDataParallel_2_5805();
void Pre_CollapsedDataParallel_1_5807();
void butterfly_5710();
void Post_CollapsedDataParallel_2_5808();
void Pre_CollapsedDataParallel_1_5810();
void butterfly_5711();
void Post_CollapsedDataParallel_2_5811();
void WEIGHTED_ROUND_ROBIN_Joiner_5970();
void WEIGHTED_ROUND_ROBIN_Splitter_5971();
void Pre_CollapsedDataParallel_1_5813();
void butterfly_5712();
void Post_CollapsedDataParallel_2_5814();
void Pre_CollapsedDataParallel_1_5816();
void butterfly_5713();
void Post_CollapsedDataParallel_2_5817();
void Pre_CollapsedDataParallel_1_5819();
void butterfly_5714();
void Post_CollapsedDataParallel_2_5820();
void Pre_CollapsedDataParallel_1_5822();
void butterfly_5715();
void Post_CollapsedDataParallel_2_5823();
void WEIGHTED_ROUND_ROBIN_Joiner_5972();
void WEIGHTED_ROUND_ROBIN_Joiner_5973();
void WEIGHTED_ROUND_ROBIN_Splitter_5974();
void WEIGHTED_ROUND_ROBIN_Splitter_5975();
void WEIGHTED_ROUND_ROBIN_Splitter_5858();
void butterfly_5717();
void butterfly_5718();
void WEIGHTED_ROUND_ROBIN_Joiner_5859();
void WEIGHTED_ROUND_ROBIN_Splitter_5860();
void butterfly_5719();
void butterfly_5720();
void WEIGHTED_ROUND_ROBIN_Joiner_5861();
void WEIGHTED_ROUND_ROBIN_Joiner_5976();
void WEIGHTED_ROUND_ROBIN_Splitter_5977();
void WEIGHTED_ROUND_ROBIN_Splitter_5862();
void butterfly_5721();
void butterfly_5722();
void WEIGHTED_ROUND_ROBIN_Joiner_5863();
void WEIGHTED_ROUND_ROBIN_Splitter_5864();
void butterfly_5723();
void butterfly_5724();
void WEIGHTED_ROUND_ROBIN_Joiner_5865();
void WEIGHTED_ROUND_ROBIN_Joiner_5978();
void WEIGHTED_ROUND_ROBIN_Joiner_5979();
void WEIGHTED_ROUND_ROBIN_Splitter_5866();
void WEIGHTED_ROUND_ROBIN_Splitter_5868();
void butterfly_5726();
void butterfly_5727();
void butterfly_5728();
void butterfly_5729();
void WEIGHTED_ROUND_ROBIN_Joiner_5869();
void WEIGHTED_ROUND_ROBIN_Splitter_5870();
void butterfly_5730();
void butterfly_5731();
void butterfly_5732();
void butterfly_5733();
void WEIGHTED_ROUND_ROBIN_Joiner_5871();
void WEIGHTED_ROUND_ROBIN_Joiner_5867();
void WEIGHTED_ROUND_ROBIN_Splitter_5985();
void magnitude_5987();
void magnitude_5988();
void magnitude_5989();
void magnitude_5990();
void magnitude_5991();
void magnitude_5992();
void magnitude_5993();
void magnitude_5994();
void magnitude_5995();
void WEIGHTED_ROUND_ROBIN_Joiner_5986();
void sink_5735();

#ifdef __cplusplus
}
#endif
#endif
