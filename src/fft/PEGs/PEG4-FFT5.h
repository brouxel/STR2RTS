#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=32 on the compile command line
#else
#if BUF_SIZEMAX < 32
#error BUF_SIZEMAX too small, it must be at least 32
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif


void WEIGHTED_ROUND_ROBIN_Splitter_8916();
void source(buffer_void_t *chanin, buffer_complex_t *chanout);
void source_8918();
void source_8919();
void WEIGHTED_ROUND_ROBIN_Joiner_8917();
void WEIGHTED_ROUND_ROBIN_Splitter_8759();
void WEIGHTED_ROUND_ROBIN_Splitter_8761();
void WEIGHTED_ROUND_ROBIN_Splitter_8763();
void WEIGHTED_ROUND_ROBIN_Splitter_8765();
void Identity(buffer_complex_t *chanin, buffer_complex_t *chanout);
void Identity_8589();
void Identity_8591();
void WEIGHTED_ROUND_ROBIN_Joiner_8766();
void WEIGHTED_ROUND_ROBIN_Splitter_8767();
void Identity_8595();
void Identity_8597();
void WEIGHTED_ROUND_ROBIN_Joiner_8768();
void WEIGHTED_ROUND_ROBIN_Joiner_8764();
void WEIGHTED_ROUND_ROBIN_Splitter_8769();
void WEIGHTED_ROUND_ROBIN_Splitter_8771();
void Identity_8603();
void Identity_8605();
void WEIGHTED_ROUND_ROBIN_Joiner_8772();
void WEIGHTED_ROUND_ROBIN_Splitter_8773();
void Identity_8609();
void Identity_8611();
void WEIGHTED_ROUND_ROBIN_Joiner_8774();
void WEIGHTED_ROUND_ROBIN_Joiner_8770();
void WEIGHTED_ROUND_ROBIN_Joiner_8762();
void WEIGHTED_ROUND_ROBIN_Splitter_8775();
void WEIGHTED_ROUND_ROBIN_Splitter_8777();
void WEIGHTED_ROUND_ROBIN_Splitter_8779();
void Identity_8619();
void Identity_8621();
void WEIGHTED_ROUND_ROBIN_Joiner_8780();
void WEIGHTED_ROUND_ROBIN_Splitter_8781();
void Identity_8625();
void Identity_8627();
void WEIGHTED_ROUND_ROBIN_Joiner_8782();
void WEIGHTED_ROUND_ROBIN_Joiner_8778();
void WEIGHTED_ROUND_ROBIN_Splitter_8783();
void WEIGHTED_ROUND_ROBIN_Splitter_8785();
void Identity_8633();
void Identity_8635();
void WEIGHTED_ROUND_ROBIN_Joiner_8786();
void WEIGHTED_ROUND_ROBIN_Splitter_8787();
void Identity_8639();
void Identity_8641();
void WEIGHTED_ROUND_ROBIN_Joiner_8788();
void WEIGHTED_ROUND_ROBIN_Joiner_8784();
void WEIGHTED_ROUND_ROBIN_Joiner_8776();
void WEIGHTED_ROUND_ROBIN_Joiner_8760();
void WEIGHTED_ROUND_ROBIN_Splitter_8903();
void WEIGHTED_ROUND_ROBIN_Splitter_8904();
void Pre_CollapsedDataParallel_1(buffer_complex_t *chanin, buffer_complex_t *chanout);
void Pre_CollapsedDataParallel_1_8736();
void butterfly(buffer_complex_t *chanin, buffer_complex_t *chanout);
void butterfly_8643();
void Post_CollapsedDataParallel_2(buffer_complex_t *chanin, buffer_complex_t *chanout);
void Post_CollapsedDataParallel_2_8737();
void Pre_CollapsedDataParallel_1_8739();
void butterfly_8644();
void Post_CollapsedDataParallel_2_8740();
void Pre_CollapsedDataParallel_1_8742();
void butterfly_8645();
void Post_CollapsedDataParallel_2_8743();
void Pre_CollapsedDataParallel_1_8745();
void butterfly_8646();
void Post_CollapsedDataParallel_2_8746();
void WEIGHTED_ROUND_ROBIN_Joiner_8905();
void WEIGHTED_ROUND_ROBIN_Splitter_8906();
void Pre_CollapsedDataParallel_1_8748();
void butterfly_8647();
void Post_CollapsedDataParallel_2_8749();
void Pre_CollapsedDataParallel_1_8751();
void butterfly_8648();
void Post_CollapsedDataParallel_2_8752();
void Pre_CollapsedDataParallel_1_8754();
void butterfly_8649();
void Post_CollapsedDataParallel_2_8755();
void Pre_CollapsedDataParallel_1_8757();
void butterfly_8650();
void Post_CollapsedDataParallel_2_8758();
void WEIGHTED_ROUND_ROBIN_Joiner_8907();
void WEIGHTED_ROUND_ROBIN_Joiner_8908();
void WEIGHTED_ROUND_ROBIN_Splitter_8909();
void WEIGHTED_ROUND_ROBIN_Splitter_8910();
void WEIGHTED_ROUND_ROBIN_Splitter_8793();
void butterfly_8652();
void butterfly_8653();
void WEIGHTED_ROUND_ROBIN_Joiner_8794();
void WEIGHTED_ROUND_ROBIN_Splitter_8795();
void butterfly_8654();
void butterfly_8655();
void WEIGHTED_ROUND_ROBIN_Joiner_8796();
void WEIGHTED_ROUND_ROBIN_Joiner_8911();
void WEIGHTED_ROUND_ROBIN_Splitter_8912();
void WEIGHTED_ROUND_ROBIN_Splitter_8797();
void butterfly_8656();
void butterfly_8657();
void WEIGHTED_ROUND_ROBIN_Joiner_8798();
void WEIGHTED_ROUND_ROBIN_Splitter_8799();
void butterfly_8658();
void butterfly_8659();
void WEIGHTED_ROUND_ROBIN_Joiner_8800();
void WEIGHTED_ROUND_ROBIN_Joiner_8913();
void WEIGHTED_ROUND_ROBIN_Joiner_8914();
void WEIGHTED_ROUND_ROBIN_Splitter_8801();
void WEIGHTED_ROUND_ROBIN_Splitter_8803();
void butterfly_8661();
void butterfly_8662();
void butterfly_8663();
void butterfly_8664();
void WEIGHTED_ROUND_ROBIN_Joiner_8804();
void WEIGHTED_ROUND_ROBIN_Splitter_8805();
void butterfly_8665();
void butterfly_8666();
void butterfly_8667();
void butterfly_8668();
void WEIGHTED_ROUND_ROBIN_Joiner_8806();
void WEIGHTED_ROUND_ROBIN_Joiner_8802();
void WEIGHTED_ROUND_ROBIN_Splitter_8920();
void magnitude(buffer_complex_t *chanin, buffer_float_t *chanout);
void magnitude_8922();
void magnitude_8923();
void magnitude_8924();
void magnitude_8925();
void WEIGHTED_ROUND_ROBIN_Joiner_8921();
void sink(buffer_float_t *chanin);
void sink_8670();

#ifdef __cplusplus
}
#endif
#endif
