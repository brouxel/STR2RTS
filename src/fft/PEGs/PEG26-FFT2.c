#include "PEG26-FFT2.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3142WEIGHTED_ROUND_ROBIN_Splitter_3145;
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_3351_3372_join[2];
buffer_float_t SplitJoin18_CombineDFT_Fiss_3358_3379_split[4];
buffer_float_t SplitJoin116_CombineDFT_Fiss_3368_3389_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3250WEIGHTED_ROUND_ROBIN_Splitter_3255;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3180WEIGHTED_ROUND_ROBIN_Splitter_3207;
buffer_float_t SplitJoin16_CombineDFT_Fiss_3357_3378_join[8];
buffer_float_t SplitJoin110_CombineDFT_Fiss_3365_3386_join[16];
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_3354_3375_split[16];
buffer_float_t SplitJoin0_FFTTestSource_Fiss_3349_3370_split[2];
buffer_float_t SplitJoin100_FFTReorderSimple_Fiss_3360_3381_split[2];
buffer_float_t SplitJoin112_CombineDFT_Fiss_3366_3387_split[8];
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_3354_3375_join[16];
buffer_float_t SplitJoin100_FFTReorderSimple_Fiss_3360_3381_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3340WEIGHTED_ROUND_ROBIN_Splitter_3345;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3152WEIGHTED_ROUND_ROBIN_Splitter_3161;
buffer_float_t SplitJoin106_FFTReorderSimple_Fiss_3363_3384_split[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3236WEIGHTED_ROUND_ROBIN_Splitter_3241;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3256WEIGHTED_ROUND_ROBIN_Splitter_3265;
buffer_float_t SplitJoin106_FFTReorderSimple_Fiss_3363_3384_join[16];
buffer_float_t SplitJoin114_CombineDFT_Fiss_3367_3388_split[4];
buffer_float_t SplitJoin12_CombineDFT_Fiss_3355_3376_split[26];
buffer_float_t SplitJoin102_FFTReorderSimple_Fiss_3361_3382_split[4];
buffer_float_t SplitJoin108_CombineDFT_Fiss_3364_3385_split[26];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3208WEIGHTED_ROUND_ROBIN_Splitter_3225;
buffer_float_t SplitJoin18_CombineDFT_Fiss_3358_3379_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3312WEIGHTED_ROUND_ROBIN_Splitter_3329;
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_3352_3373_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3138WEIGHTED_ROUND_ROBIN_Splitter_3129;
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_3352_3373_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3246WEIGHTED_ROUND_ROBIN_Splitter_3249;
buffer_float_t SplitJoin116_CombineDFT_Fiss_3368_3389_split[2];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_3095_3131_3350_3371_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3242CombineDFT_3116;
buffer_float_t SplitJoin104_FFTReorderSimple_Fiss_3362_3383_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3284WEIGHTED_ROUND_ROBIN_Splitter_3311;
buffer_float_t SplitJoin14_CombineDFT_Fiss_3356_3377_join[16];
buffer_float_t SplitJoin20_CombineDFT_Fiss_3359_3380_split[2];
buffer_float_t SplitJoin14_CombineDFT_Fiss_3356_3377_split[16];
buffer_float_t SplitJoin112_CombineDFT_Fiss_3366_3387_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3130FloatPrinter_3128;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3330WEIGHTED_ROUND_ROBIN_Splitter_3339;
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_3353_3374_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3162WEIGHTED_ROUND_ROBIN_Splitter_3179;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3226WEIGHTED_ROUND_ROBIN_Splitter_3235;
buffer_float_t SplitJoin104_FFTReorderSimple_Fiss_3362_3383_split[8];
buffer_float_t FFTReorderSimple_3117WEIGHTED_ROUND_ROBIN_Splitter_3245;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3346CombineDFT_3127;
buffer_float_t FFTReorderSimple_3106WEIGHTED_ROUND_ROBIN_Splitter_3141;
buffer_float_t SplitJoin108_CombineDFT_Fiss_3364_3385_join[26];
buffer_float_t SplitJoin0_FFTTestSource_Fiss_3349_3370_join[2];
buffer_float_t SplitJoin20_CombineDFT_Fiss_3359_3380_join[2];
buffer_float_t SplitJoin102_FFTReorderSimple_Fiss_3361_3382_join[4];
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_3353_3374_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3266WEIGHTED_ROUND_ROBIN_Splitter_3283;
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_3351_3372_split[2];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_3095_3131_3350_3371_join[2];
buffer_float_t SplitJoin16_CombineDFT_Fiss_3357_3378_split[8];
buffer_float_t SplitJoin12_CombineDFT_Fiss_3355_3376_join[26];
buffer_float_t SplitJoin110_CombineDFT_Fiss_3365_3386_split[16];
buffer_float_t SplitJoin114_CombineDFT_Fiss_3367_3388_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3146WEIGHTED_ROUND_ROBIN_Splitter_3151;


CombineDFT_3181_t CombineDFT_3181_s;
CombineDFT_3181_t CombineDFT_3182_s;
CombineDFT_3181_t CombineDFT_3183_s;
CombineDFT_3181_t CombineDFT_3184_s;
CombineDFT_3181_t CombineDFT_3185_s;
CombineDFT_3181_t CombineDFT_3186_s;
CombineDFT_3181_t CombineDFT_3187_s;
CombineDFT_3181_t CombineDFT_3188_s;
CombineDFT_3181_t CombineDFT_3189_s;
CombineDFT_3181_t CombineDFT_3190_s;
CombineDFT_3181_t CombineDFT_3191_s;
CombineDFT_3181_t CombineDFT_3192_s;
CombineDFT_3181_t CombineDFT_3193_s;
CombineDFT_3181_t CombineDFT_3194_s;
CombineDFT_3181_t CombineDFT_3195_s;
CombineDFT_3181_t CombineDFT_3196_s;
CombineDFT_3181_t CombineDFT_3197_s;
CombineDFT_3181_t CombineDFT_3198_s;
CombineDFT_3181_t CombineDFT_3199_s;
CombineDFT_3181_t CombineDFT_3200_s;
CombineDFT_3181_t CombineDFT_3201_s;
CombineDFT_3181_t CombineDFT_3202_s;
CombineDFT_3181_t CombineDFT_3203_s;
CombineDFT_3181_t CombineDFT_3204_s;
CombineDFT_3181_t CombineDFT_3205_s;
CombineDFT_3181_t CombineDFT_3206_s;
CombineDFT_3209_t CombineDFT_3209_s;
CombineDFT_3209_t CombineDFT_3210_s;
CombineDFT_3209_t CombineDFT_3211_s;
CombineDFT_3209_t CombineDFT_3212_s;
CombineDFT_3209_t CombineDFT_3213_s;
CombineDFT_3209_t CombineDFT_3214_s;
CombineDFT_3209_t CombineDFT_3215_s;
CombineDFT_3209_t CombineDFT_3216_s;
CombineDFT_3209_t CombineDFT_3217_s;
CombineDFT_3209_t CombineDFT_3218_s;
CombineDFT_3209_t CombineDFT_3219_s;
CombineDFT_3209_t CombineDFT_3220_s;
CombineDFT_3209_t CombineDFT_3221_s;
CombineDFT_3209_t CombineDFT_3222_s;
CombineDFT_3209_t CombineDFT_3223_s;
CombineDFT_3209_t CombineDFT_3224_s;
CombineDFT_3227_t CombineDFT_3227_s;
CombineDFT_3227_t CombineDFT_3228_s;
CombineDFT_3227_t CombineDFT_3229_s;
CombineDFT_3227_t CombineDFT_3230_s;
CombineDFT_3227_t CombineDFT_3231_s;
CombineDFT_3227_t CombineDFT_3232_s;
CombineDFT_3227_t CombineDFT_3233_s;
CombineDFT_3227_t CombineDFT_3234_s;
CombineDFT_3237_t CombineDFT_3237_s;
CombineDFT_3237_t CombineDFT_3238_s;
CombineDFT_3237_t CombineDFT_3239_s;
CombineDFT_3237_t CombineDFT_3240_s;
CombineDFT_3243_t CombineDFT_3243_s;
CombineDFT_3243_t CombineDFT_3244_s;
CombineDFT_3116_t CombineDFT_3116_s;
CombineDFT_3181_t CombineDFT_3285_s;
CombineDFT_3181_t CombineDFT_3286_s;
CombineDFT_3181_t CombineDFT_3287_s;
CombineDFT_3181_t CombineDFT_3288_s;
CombineDFT_3181_t CombineDFT_3289_s;
CombineDFT_3181_t CombineDFT_3290_s;
CombineDFT_3181_t CombineDFT_3291_s;
CombineDFT_3181_t CombineDFT_3292_s;
CombineDFT_3181_t CombineDFT_3293_s;
CombineDFT_3181_t CombineDFT_3294_s;
CombineDFT_3181_t CombineDFT_3295_s;
CombineDFT_3181_t CombineDFT_3296_s;
CombineDFT_3181_t CombineDFT_3297_s;
CombineDFT_3181_t CombineDFT_3298_s;
CombineDFT_3181_t CombineDFT_3299_s;
CombineDFT_3181_t CombineDFT_3300_s;
CombineDFT_3181_t CombineDFT_3301_s;
CombineDFT_3181_t CombineDFT_3302_s;
CombineDFT_3181_t CombineDFT_3303_s;
CombineDFT_3181_t CombineDFT_3304_s;
CombineDFT_3181_t CombineDFT_3305_s;
CombineDFT_3181_t CombineDFT_3306_s;
CombineDFT_3181_t CombineDFT_3307_s;
CombineDFT_3181_t CombineDFT_3308_s;
CombineDFT_3181_t CombineDFT_3309_s;
CombineDFT_3181_t CombineDFT_3310_s;
CombineDFT_3209_t CombineDFT_3313_s;
CombineDFT_3209_t CombineDFT_3314_s;
CombineDFT_3209_t CombineDFT_3315_s;
CombineDFT_3209_t CombineDFT_3316_s;
CombineDFT_3209_t CombineDFT_3317_s;
CombineDFT_3209_t CombineDFT_3318_s;
CombineDFT_3209_t CombineDFT_3319_s;
CombineDFT_3209_t CombineDFT_3320_s;
CombineDFT_3209_t CombineDFT_3321_s;
CombineDFT_3209_t CombineDFT_3322_s;
CombineDFT_3209_t CombineDFT_3323_s;
CombineDFT_3209_t CombineDFT_3324_s;
CombineDFT_3209_t CombineDFT_3325_s;
CombineDFT_3209_t CombineDFT_3326_s;
CombineDFT_3209_t CombineDFT_3327_s;
CombineDFT_3209_t CombineDFT_3328_s;
CombineDFT_3227_t CombineDFT_3331_s;
CombineDFT_3227_t CombineDFT_3332_s;
CombineDFT_3227_t CombineDFT_3333_s;
CombineDFT_3227_t CombineDFT_3334_s;
CombineDFT_3227_t CombineDFT_3335_s;
CombineDFT_3227_t CombineDFT_3336_s;
CombineDFT_3227_t CombineDFT_3337_s;
CombineDFT_3227_t CombineDFT_3338_s;
CombineDFT_3237_t CombineDFT_3341_s;
CombineDFT_3237_t CombineDFT_3342_s;
CombineDFT_3237_t CombineDFT_3343_s;
CombineDFT_3237_t CombineDFT_3344_s;
CombineDFT_3243_t CombineDFT_3347_s;
CombineDFT_3243_t CombineDFT_3348_s;
CombineDFT_3116_t CombineDFT_3127_s;

void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), 0.0) ; 
		push_float(&(*chanout), 0.0) ; 
		push_float(&(*chanout), 1.0) ; 
		push_float(&(*chanout), 0.0) ; 
		FOR(int, i, 0,  < , 124, i++) {
			push_float(&(*chanout), 0.0) ; 
		}
		ENDFOR
	}


void FFTTestSource_3139() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_3349_3370_split[0]), &(SplitJoin0_FFTTestSource_Fiss_3349_3370_join[0]));
	ENDFOR
}

void FFTTestSource_3140() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_3349_3370_split[1]), &(SplitJoin0_FFTTestSource_Fiss_3349_3370_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3137() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_3138() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3138WEIGHTED_ROUND_ROBIN_Splitter_3129, pop_float(&SplitJoin0_FFTTestSource_Fiss_3349_3370_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3138WEIGHTED_ROUND_ROBIN_Splitter_3129, pop_float(&SplitJoin0_FFTTestSource_Fiss_3349_3370_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, i, 0,  < , 128, i = (i + 4)) {
			push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 128, i = (i + 4)) {
			push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_float(&(*chanin)) ; 
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void FFTReorderSimple_3106() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_3095_3131_3350_3371_split[0]), &(FFTReorderSimple_3106WEIGHTED_ROUND_ROBIN_Splitter_3141));
	ENDFOR
}

void FFTReorderSimple_3143() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_3351_3372_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_3351_3372_join[0]));
	ENDFOR
}

void FFTReorderSimple_3144() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_3351_3372_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_3351_3372_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3141() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_3351_3372_split[0], pop_float(&FFTReorderSimple_3106WEIGHTED_ROUND_ROBIN_Splitter_3141));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_3351_3372_split[1], pop_float(&FFTReorderSimple_3106WEIGHTED_ROUND_ROBIN_Splitter_3141));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3142() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3142WEIGHTED_ROUND_ROBIN_Splitter_3145, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_3351_3372_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3142WEIGHTED_ROUND_ROBIN_Splitter_3145, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_3351_3372_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_3147() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3352_3373_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_3352_3373_join[0]));
	ENDFOR
}

void FFTReorderSimple_3148() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3352_3373_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_3352_3373_join[1]));
	ENDFOR
}

void FFTReorderSimple_3149() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3352_3373_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_3352_3373_join[2]));
	ENDFOR
}

void FFTReorderSimple_3150() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_3352_3373_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_3352_3373_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3145() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin6_FFTReorderSimple_Fiss_3352_3373_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3142WEIGHTED_ROUND_ROBIN_Splitter_3145));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3146() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3146WEIGHTED_ROUND_ROBIN_Splitter_3151, pop_float(&SplitJoin6_FFTReorderSimple_Fiss_3352_3373_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_3153() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_3353_3374_split[0]), &(SplitJoin8_FFTReorderSimple_Fiss_3353_3374_join[0]));
	ENDFOR
}

void FFTReorderSimple_3154() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_3353_3374_split[1]), &(SplitJoin8_FFTReorderSimple_Fiss_3353_3374_join[1]));
	ENDFOR
}

void FFTReorderSimple_3155() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_3353_3374_split[2]), &(SplitJoin8_FFTReorderSimple_Fiss_3353_3374_join[2]));
	ENDFOR
}

void FFTReorderSimple_3156() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_3353_3374_split[3]), &(SplitJoin8_FFTReorderSimple_Fiss_3353_3374_join[3]));
	ENDFOR
}

void FFTReorderSimple_3157() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_3353_3374_split[4]), &(SplitJoin8_FFTReorderSimple_Fiss_3353_3374_join[4]));
	ENDFOR
}

void FFTReorderSimple_3158() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_3353_3374_split[5]), &(SplitJoin8_FFTReorderSimple_Fiss_3353_3374_join[5]));
	ENDFOR
}

void FFTReorderSimple_3159() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_3353_3374_split[6]), &(SplitJoin8_FFTReorderSimple_Fiss_3353_3374_join[6]));
	ENDFOR
}

void FFTReorderSimple_3160() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_3353_3374_split[7]), &(SplitJoin8_FFTReorderSimple_Fiss_3353_3374_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3151() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin8_FFTReorderSimple_Fiss_3353_3374_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3146WEIGHTED_ROUND_ROBIN_Splitter_3151));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3152() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3152WEIGHTED_ROUND_ROBIN_Splitter_3161, pop_float(&SplitJoin8_FFTReorderSimple_Fiss_3353_3374_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_3163() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_split[0]), &(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_join[0]));
	ENDFOR
}

void FFTReorderSimple_3164() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_split[1]), &(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_join[1]));
	ENDFOR
}

void FFTReorderSimple_3165() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_split[2]), &(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_join[2]));
	ENDFOR
}

void FFTReorderSimple_3166() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_split[3]), &(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_join[3]));
	ENDFOR
}

void FFTReorderSimple_3167() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_split[4]), &(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_join[4]));
	ENDFOR
}

void FFTReorderSimple_3168() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_split[5]), &(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_join[5]));
	ENDFOR
}

void FFTReorderSimple_3169() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_split[6]), &(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_join[6]));
	ENDFOR
}

void FFTReorderSimple_3170() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_split[7]), &(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_join[7]));
	ENDFOR
}

void FFTReorderSimple_3171() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_split[8]), &(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_join[8]));
	ENDFOR
}

void FFTReorderSimple_3172() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_split[9]), &(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_join[9]));
	ENDFOR
}

void FFTReorderSimple_3173() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_split[10]), &(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_join[10]));
	ENDFOR
}

void FFTReorderSimple_3174() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_split[11]), &(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_join[11]));
	ENDFOR
}

void FFTReorderSimple_3175() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_split[12]), &(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_join[12]));
	ENDFOR
}

void FFTReorderSimple_3176() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_split[13]), &(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_join[13]));
	ENDFOR
}

void FFTReorderSimple_3177() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_split[14]), &(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_join[14]));
	ENDFOR
}

void FFTReorderSimple_3178() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_split[15]), &(SplitJoin10_FFTReorderSimple_Fiss_3354_3375_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3161() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_FFTReorderSimple_Fiss_3354_3375_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3152WEIGHTED_ROUND_ROBIN_Splitter_3161));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3162() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3162WEIGHTED_ROUND_ROBIN_Splitter_3179, pop_float(&SplitJoin10_FFTReorderSimple_Fiss_3354_3375_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&(*chanin), i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&(*chanin), i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&(*chanin), (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&(*chanin), (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_3181_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_3181_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&(*chanin)) ; 
			push_float(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_3181() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3355_3376_split[0]), &(SplitJoin12_CombineDFT_Fiss_3355_3376_join[0]));
	ENDFOR
}

void CombineDFT_3182() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3355_3376_split[1]), &(SplitJoin12_CombineDFT_Fiss_3355_3376_join[1]));
	ENDFOR
}

void CombineDFT_3183() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3355_3376_split[2]), &(SplitJoin12_CombineDFT_Fiss_3355_3376_join[2]));
	ENDFOR
}

void CombineDFT_3184() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3355_3376_split[3]), &(SplitJoin12_CombineDFT_Fiss_3355_3376_join[3]));
	ENDFOR
}

void CombineDFT_3185() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3355_3376_split[4]), &(SplitJoin12_CombineDFT_Fiss_3355_3376_join[4]));
	ENDFOR
}

void CombineDFT_3186() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3355_3376_split[5]), &(SplitJoin12_CombineDFT_Fiss_3355_3376_join[5]));
	ENDFOR
}

void CombineDFT_3187() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3355_3376_split[6]), &(SplitJoin12_CombineDFT_Fiss_3355_3376_join[6]));
	ENDFOR
}

void CombineDFT_3188() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3355_3376_split[7]), &(SplitJoin12_CombineDFT_Fiss_3355_3376_join[7]));
	ENDFOR
}

void CombineDFT_3189() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3355_3376_split[8]), &(SplitJoin12_CombineDFT_Fiss_3355_3376_join[8]));
	ENDFOR
}

void CombineDFT_3190() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3355_3376_split[9]), &(SplitJoin12_CombineDFT_Fiss_3355_3376_join[9]));
	ENDFOR
}

void CombineDFT_3191() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3355_3376_split[10]), &(SplitJoin12_CombineDFT_Fiss_3355_3376_join[10]));
	ENDFOR
}

void CombineDFT_3192() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3355_3376_split[11]), &(SplitJoin12_CombineDFT_Fiss_3355_3376_join[11]));
	ENDFOR
}

void CombineDFT_3193() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3355_3376_split[12]), &(SplitJoin12_CombineDFT_Fiss_3355_3376_join[12]));
	ENDFOR
}

void CombineDFT_3194() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3355_3376_split[13]), &(SplitJoin12_CombineDFT_Fiss_3355_3376_join[13]));
	ENDFOR
}

void CombineDFT_3195() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3355_3376_split[14]), &(SplitJoin12_CombineDFT_Fiss_3355_3376_join[14]));
	ENDFOR
}

void CombineDFT_3196() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3355_3376_split[15]), &(SplitJoin12_CombineDFT_Fiss_3355_3376_join[15]));
	ENDFOR
}

void CombineDFT_3197() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3355_3376_split[16]), &(SplitJoin12_CombineDFT_Fiss_3355_3376_join[16]));
	ENDFOR
}

void CombineDFT_3198() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3355_3376_split[17]), &(SplitJoin12_CombineDFT_Fiss_3355_3376_join[17]));
	ENDFOR
}

void CombineDFT_3199() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3355_3376_split[18]), &(SplitJoin12_CombineDFT_Fiss_3355_3376_join[18]));
	ENDFOR
}

void CombineDFT_3200() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3355_3376_split[19]), &(SplitJoin12_CombineDFT_Fiss_3355_3376_join[19]));
	ENDFOR
}

void CombineDFT_3201() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3355_3376_split[20]), &(SplitJoin12_CombineDFT_Fiss_3355_3376_join[20]));
	ENDFOR
}

void CombineDFT_3202() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3355_3376_split[21]), &(SplitJoin12_CombineDFT_Fiss_3355_3376_join[21]));
	ENDFOR
}

void CombineDFT_3203() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3355_3376_split[22]), &(SplitJoin12_CombineDFT_Fiss_3355_3376_join[22]));
	ENDFOR
}

void CombineDFT_3204() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3355_3376_split[23]), &(SplitJoin12_CombineDFT_Fiss_3355_3376_join[23]));
	ENDFOR
}

void CombineDFT_3205() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3355_3376_split[24]), &(SplitJoin12_CombineDFT_Fiss_3355_3376_join[24]));
	ENDFOR
}

void CombineDFT_3206() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_3355_3376_split[25]), &(SplitJoin12_CombineDFT_Fiss_3355_3376_join[25]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3179() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 26, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin12_CombineDFT_Fiss_3355_3376_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3162WEIGHTED_ROUND_ROBIN_Splitter_3179));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3180() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 26, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3180WEIGHTED_ROUND_ROBIN_Splitter_3207, pop_float(&SplitJoin12_CombineDFT_Fiss_3355_3376_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_3209() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_3356_3377_split[0]), &(SplitJoin14_CombineDFT_Fiss_3356_3377_join[0]));
	ENDFOR
}

void CombineDFT_3210() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_3356_3377_split[1]), &(SplitJoin14_CombineDFT_Fiss_3356_3377_join[1]));
	ENDFOR
}

void CombineDFT_3211() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_3356_3377_split[2]), &(SplitJoin14_CombineDFT_Fiss_3356_3377_join[2]));
	ENDFOR
}

void CombineDFT_3212() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_3356_3377_split[3]), &(SplitJoin14_CombineDFT_Fiss_3356_3377_join[3]));
	ENDFOR
}

void CombineDFT_3213() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_3356_3377_split[4]), &(SplitJoin14_CombineDFT_Fiss_3356_3377_join[4]));
	ENDFOR
}

void CombineDFT_3214() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_3356_3377_split[5]), &(SplitJoin14_CombineDFT_Fiss_3356_3377_join[5]));
	ENDFOR
}

void CombineDFT_3215() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_3356_3377_split[6]), &(SplitJoin14_CombineDFT_Fiss_3356_3377_join[6]));
	ENDFOR
}

void CombineDFT_3216() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_3356_3377_split[7]), &(SplitJoin14_CombineDFT_Fiss_3356_3377_join[7]));
	ENDFOR
}

void CombineDFT_3217() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_3356_3377_split[8]), &(SplitJoin14_CombineDFT_Fiss_3356_3377_join[8]));
	ENDFOR
}

void CombineDFT_3218() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_3356_3377_split[9]), &(SplitJoin14_CombineDFT_Fiss_3356_3377_join[9]));
	ENDFOR
}

void CombineDFT_3219() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_3356_3377_split[10]), &(SplitJoin14_CombineDFT_Fiss_3356_3377_join[10]));
	ENDFOR
}

void CombineDFT_3220() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_3356_3377_split[11]), &(SplitJoin14_CombineDFT_Fiss_3356_3377_join[11]));
	ENDFOR
}

void CombineDFT_3221() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_3356_3377_split[12]), &(SplitJoin14_CombineDFT_Fiss_3356_3377_join[12]));
	ENDFOR
}

void CombineDFT_3222() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_3356_3377_split[13]), &(SplitJoin14_CombineDFT_Fiss_3356_3377_join[13]));
	ENDFOR
}

void CombineDFT_3223() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_3356_3377_split[14]), &(SplitJoin14_CombineDFT_Fiss_3356_3377_join[14]));
	ENDFOR
}

void CombineDFT_3224() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_3356_3377_split[15]), &(SplitJoin14_CombineDFT_Fiss_3356_3377_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3207() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin14_CombineDFT_Fiss_3356_3377_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3180WEIGHTED_ROUND_ROBIN_Splitter_3207));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3208() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3208WEIGHTED_ROUND_ROBIN_Splitter_3225, pop_float(&SplitJoin14_CombineDFT_Fiss_3356_3377_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_3227() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_3357_3378_split[0]), &(SplitJoin16_CombineDFT_Fiss_3357_3378_join[0]));
	ENDFOR
}

void CombineDFT_3228() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_3357_3378_split[1]), &(SplitJoin16_CombineDFT_Fiss_3357_3378_join[1]));
	ENDFOR
}

void CombineDFT_3229() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_3357_3378_split[2]), &(SplitJoin16_CombineDFT_Fiss_3357_3378_join[2]));
	ENDFOR
}

void CombineDFT_3230() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_3357_3378_split[3]), &(SplitJoin16_CombineDFT_Fiss_3357_3378_join[3]));
	ENDFOR
}

void CombineDFT_3231() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_3357_3378_split[4]), &(SplitJoin16_CombineDFT_Fiss_3357_3378_join[4]));
	ENDFOR
}

void CombineDFT_3232() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_3357_3378_split[5]), &(SplitJoin16_CombineDFT_Fiss_3357_3378_join[5]));
	ENDFOR
}

void CombineDFT_3233() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_3357_3378_split[6]), &(SplitJoin16_CombineDFT_Fiss_3357_3378_join[6]));
	ENDFOR
}

void CombineDFT_3234() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_3357_3378_split[7]), &(SplitJoin16_CombineDFT_Fiss_3357_3378_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3225() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin16_CombineDFT_Fiss_3357_3378_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3208WEIGHTED_ROUND_ROBIN_Splitter_3225));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3226() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3226WEIGHTED_ROUND_ROBIN_Splitter_3235, pop_float(&SplitJoin16_CombineDFT_Fiss_3357_3378_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_3237() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_3358_3379_split[0]), &(SplitJoin18_CombineDFT_Fiss_3358_3379_join[0]));
	ENDFOR
}

void CombineDFT_3238() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_3358_3379_split[1]), &(SplitJoin18_CombineDFT_Fiss_3358_3379_join[1]));
	ENDFOR
}

void CombineDFT_3239() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_3358_3379_split[2]), &(SplitJoin18_CombineDFT_Fiss_3358_3379_join[2]));
	ENDFOR
}

void CombineDFT_3240() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_3358_3379_split[3]), &(SplitJoin18_CombineDFT_Fiss_3358_3379_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3235() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin18_CombineDFT_Fiss_3358_3379_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3226WEIGHTED_ROUND_ROBIN_Splitter_3235));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3236() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3236WEIGHTED_ROUND_ROBIN_Splitter_3241, pop_float(&SplitJoin18_CombineDFT_Fiss_3358_3379_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_3243() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin20_CombineDFT_Fiss_3359_3380_split[0]), &(SplitJoin20_CombineDFT_Fiss_3359_3380_join[0]));
	ENDFOR
}

void CombineDFT_3244() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin20_CombineDFT_Fiss_3359_3380_split[1]), &(SplitJoin20_CombineDFT_Fiss_3359_3380_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3241() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_3359_3380_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3236WEIGHTED_ROUND_ROBIN_Splitter_3241));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_3359_3380_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3236WEIGHTED_ROUND_ROBIN_Splitter_3241));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3242() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3242CombineDFT_3116, pop_float(&SplitJoin20_CombineDFT_Fiss_3359_3380_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3242CombineDFT_3116, pop_float(&SplitJoin20_CombineDFT_Fiss_3359_3380_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_3116() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_3242CombineDFT_3116), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_3095_3131_3350_3371_join[0]));
	ENDFOR
}

void FFTReorderSimple_3117() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_3095_3131_3350_3371_split[1]), &(FFTReorderSimple_3117WEIGHTED_ROUND_ROBIN_Splitter_3245));
	ENDFOR
}

void FFTReorderSimple_3247() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin100_FFTReorderSimple_Fiss_3360_3381_split[0]), &(SplitJoin100_FFTReorderSimple_Fiss_3360_3381_join[0]));
	ENDFOR
}

void FFTReorderSimple_3248() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin100_FFTReorderSimple_Fiss_3360_3381_split[1]), &(SplitJoin100_FFTReorderSimple_Fiss_3360_3381_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3245() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_3360_3381_split[0], pop_float(&FFTReorderSimple_3117WEIGHTED_ROUND_ROBIN_Splitter_3245));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin100_FFTReorderSimple_Fiss_3360_3381_split[1], pop_float(&FFTReorderSimple_3117WEIGHTED_ROUND_ROBIN_Splitter_3245));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3246() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3246WEIGHTED_ROUND_ROBIN_Splitter_3249, pop_float(&SplitJoin100_FFTReorderSimple_Fiss_3360_3381_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3246WEIGHTED_ROUND_ROBIN_Splitter_3249, pop_float(&SplitJoin100_FFTReorderSimple_Fiss_3360_3381_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_3251() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin102_FFTReorderSimple_Fiss_3361_3382_split[0]), &(SplitJoin102_FFTReorderSimple_Fiss_3361_3382_join[0]));
	ENDFOR
}

void FFTReorderSimple_3252() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin102_FFTReorderSimple_Fiss_3361_3382_split[1]), &(SplitJoin102_FFTReorderSimple_Fiss_3361_3382_join[1]));
	ENDFOR
}

void FFTReorderSimple_3253() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin102_FFTReorderSimple_Fiss_3361_3382_split[2]), &(SplitJoin102_FFTReorderSimple_Fiss_3361_3382_join[2]));
	ENDFOR
}

void FFTReorderSimple_3254() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin102_FFTReorderSimple_Fiss_3361_3382_split[3]), &(SplitJoin102_FFTReorderSimple_Fiss_3361_3382_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3249() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin102_FFTReorderSimple_Fiss_3361_3382_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3246WEIGHTED_ROUND_ROBIN_Splitter_3249));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3250() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3250WEIGHTED_ROUND_ROBIN_Splitter_3255, pop_float(&SplitJoin102_FFTReorderSimple_Fiss_3361_3382_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_3257() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin104_FFTReorderSimple_Fiss_3362_3383_split[0]), &(SplitJoin104_FFTReorderSimple_Fiss_3362_3383_join[0]));
	ENDFOR
}

void FFTReorderSimple_3258() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin104_FFTReorderSimple_Fiss_3362_3383_split[1]), &(SplitJoin104_FFTReorderSimple_Fiss_3362_3383_join[1]));
	ENDFOR
}

void FFTReorderSimple_3259() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin104_FFTReorderSimple_Fiss_3362_3383_split[2]), &(SplitJoin104_FFTReorderSimple_Fiss_3362_3383_join[2]));
	ENDFOR
}

void FFTReorderSimple_3260() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin104_FFTReorderSimple_Fiss_3362_3383_split[3]), &(SplitJoin104_FFTReorderSimple_Fiss_3362_3383_join[3]));
	ENDFOR
}

void FFTReorderSimple_3261() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin104_FFTReorderSimple_Fiss_3362_3383_split[4]), &(SplitJoin104_FFTReorderSimple_Fiss_3362_3383_join[4]));
	ENDFOR
}

void FFTReorderSimple_3262() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin104_FFTReorderSimple_Fiss_3362_3383_split[5]), &(SplitJoin104_FFTReorderSimple_Fiss_3362_3383_join[5]));
	ENDFOR
}

void FFTReorderSimple_3263() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin104_FFTReorderSimple_Fiss_3362_3383_split[6]), &(SplitJoin104_FFTReorderSimple_Fiss_3362_3383_join[6]));
	ENDFOR
}

void FFTReorderSimple_3264() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin104_FFTReorderSimple_Fiss_3362_3383_split[7]), &(SplitJoin104_FFTReorderSimple_Fiss_3362_3383_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3255() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin104_FFTReorderSimple_Fiss_3362_3383_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3250WEIGHTED_ROUND_ROBIN_Splitter_3255));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3256() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3256WEIGHTED_ROUND_ROBIN_Splitter_3265, pop_float(&SplitJoin104_FFTReorderSimple_Fiss_3362_3383_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_3267() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_split[0]), &(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_join[0]));
	ENDFOR
}

void FFTReorderSimple_3268() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_split[1]), &(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_join[1]));
	ENDFOR
}

void FFTReorderSimple_3269() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_split[2]), &(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_join[2]));
	ENDFOR
}

void FFTReorderSimple_3270() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_split[3]), &(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_join[3]));
	ENDFOR
}

void FFTReorderSimple_3271() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_split[4]), &(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_join[4]));
	ENDFOR
}

void FFTReorderSimple_3272() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_split[5]), &(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_join[5]));
	ENDFOR
}

void FFTReorderSimple_3273() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_split[6]), &(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_join[6]));
	ENDFOR
}

void FFTReorderSimple_3274() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_split[7]), &(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_join[7]));
	ENDFOR
}

void FFTReorderSimple_3275() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_split[8]), &(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_join[8]));
	ENDFOR
}

void FFTReorderSimple_3276() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_split[9]), &(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_join[9]));
	ENDFOR
}

void FFTReorderSimple_3277() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_split[10]), &(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_join[10]));
	ENDFOR
}

void FFTReorderSimple_3278() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_split[11]), &(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_join[11]));
	ENDFOR
}

void FFTReorderSimple_3279() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_split[12]), &(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_join[12]));
	ENDFOR
}

void FFTReorderSimple_3280() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_split[13]), &(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_join[13]));
	ENDFOR
}

void FFTReorderSimple_3281() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_split[14]), &(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_join[14]));
	ENDFOR
}

void FFTReorderSimple_3282() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_split[15]), &(SplitJoin106_FFTReorderSimple_Fiss_3363_3384_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3265() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin106_FFTReorderSimple_Fiss_3363_3384_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3256WEIGHTED_ROUND_ROBIN_Splitter_3265));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3266() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3266WEIGHTED_ROUND_ROBIN_Splitter_3283, pop_float(&SplitJoin106_FFTReorderSimple_Fiss_3363_3384_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_3285() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_3364_3385_split[0]), &(SplitJoin108_CombineDFT_Fiss_3364_3385_join[0]));
	ENDFOR
}

void CombineDFT_3286() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_3364_3385_split[1]), &(SplitJoin108_CombineDFT_Fiss_3364_3385_join[1]));
	ENDFOR
}

void CombineDFT_3287() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_3364_3385_split[2]), &(SplitJoin108_CombineDFT_Fiss_3364_3385_join[2]));
	ENDFOR
}

void CombineDFT_3288() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_3364_3385_split[3]), &(SplitJoin108_CombineDFT_Fiss_3364_3385_join[3]));
	ENDFOR
}

void CombineDFT_3289() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_3364_3385_split[4]), &(SplitJoin108_CombineDFT_Fiss_3364_3385_join[4]));
	ENDFOR
}

void CombineDFT_3290() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_3364_3385_split[5]), &(SplitJoin108_CombineDFT_Fiss_3364_3385_join[5]));
	ENDFOR
}

void CombineDFT_3291() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_3364_3385_split[6]), &(SplitJoin108_CombineDFT_Fiss_3364_3385_join[6]));
	ENDFOR
}

void CombineDFT_3292() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_3364_3385_split[7]), &(SplitJoin108_CombineDFT_Fiss_3364_3385_join[7]));
	ENDFOR
}

void CombineDFT_3293() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_3364_3385_split[8]), &(SplitJoin108_CombineDFT_Fiss_3364_3385_join[8]));
	ENDFOR
}

void CombineDFT_3294() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_3364_3385_split[9]), &(SplitJoin108_CombineDFT_Fiss_3364_3385_join[9]));
	ENDFOR
}

void CombineDFT_3295() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_3364_3385_split[10]), &(SplitJoin108_CombineDFT_Fiss_3364_3385_join[10]));
	ENDFOR
}

void CombineDFT_3296() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_3364_3385_split[11]), &(SplitJoin108_CombineDFT_Fiss_3364_3385_join[11]));
	ENDFOR
}

void CombineDFT_3297() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_3364_3385_split[12]), &(SplitJoin108_CombineDFT_Fiss_3364_3385_join[12]));
	ENDFOR
}

void CombineDFT_3298() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_3364_3385_split[13]), &(SplitJoin108_CombineDFT_Fiss_3364_3385_join[13]));
	ENDFOR
}

void CombineDFT_3299() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_3364_3385_split[14]), &(SplitJoin108_CombineDFT_Fiss_3364_3385_join[14]));
	ENDFOR
}

void CombineDFT_3300() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_3364_3385_split[15]), &(SplitJoin108_CombineDFT_Fiss_3364_3385_join[15]));
	ENDFOR
}

void CombineDFT_3301() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_3364_3385_split[16]), &(SplitJoin108_CombineDFT_Fiss_3364_3385_join[16]));
	ENDFOR
}

void CombineDFT_3302() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_3364_3385_split[17]), &(SplitJoin108_CombineDFT_Fiss_3364_3385_join[17]));
	ENDFOR
}

void CombineDFT_3303() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_3364_3385_split[18]), &(SplitJoin108_CombineDFT_Fiss_3364_3385_join[18]));
	ENDFOR
}

void CombineDFT_3304() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_3364_3385_split[19]), &(SplitJoin108_CombineDFT_Fiss_3364_3385_join[19]));
	ENDFOR
}

void CombineDFT_3305() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_3364_3385_split[20]), &(SplitJoin108_CombineDFT_Fiss_3364_3385_join[20]));
	ENDFOR
}

void CombineDFT_3306() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_3364_3385_split[21]), &(SplitJoin108_CombineDFT_Fiss_3364_3385_join[21]));
	ENDFOR
}

void CombineDFT_3307() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_3364_3385_split[22]), &(SplitJoin108_CombineDFT_Fiss_3364_3385_join[22]));
	ENDFOR
}

void CombineDFT_3308() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_3364_3385_split[23]), &(SplitJoin108_CombineDFT_Fiss_3364_3385_join[23]));
	ENDFOR
}

void CombineDFT_3309() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_3364_3385_split[24]), &(SplitJoin108_CombineDFT_Fiss_3364_3385_join[24]));
	ENDFOR
}

void CombineDFT_3310() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_3364_3385_split[25]), &(SplitJoin108_CombineDFT_Fiss_3364_3385_join[25]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3283() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 26, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin108_CombineDFT_Fiss_3364_3385_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3266WEIGHTED_ROUND_ROBIN_Splitter_3283));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3284() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 26, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3284WEIGHTED_ROUND_ROBIN_Splitter_3311, pop_float(&SplitJoin108_CombineDFT_Fiss_3364_3385_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_3313() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_3365_3386_split[0]), &(SplitJoin110_CombineDFT_Fiss_3365_3386_join[0]));
	ENDFOR
}

void CombineDFT_3314() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_3365_3386_split[1]), &(SplitJoin110_CombineDFT_Fiss_3365_3386_join[1]));
	ENDFOR
}

void CombineDFT_3315() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_3365_3386_split[2]), &(SplitJoin110_CombineDFT_Fiss_3365_3386_join[2]));
	ENDFOR
}

void CombineDFT_3316() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_3365_3386_split[3]), &(SplitJoin110_CombineDFT_Fiss_3365_3386_join[3]));
	ENDFOR
}

void CombineDFT_3317() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_3365_3386_split[4]), &(SplitJoin110_CombineDFT_Fiss_3365_3386_join[4]));
	ENDFOR
}

void CombineDFT_3318() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_3365_3386_split[5]), &(SplitJoin110_CombineDFT_Fiss_3365_3386_join[5]));
	ENDFOR
}

void CombineDFT_3319() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_3365_3386_split[6]), &(SplitJoin110_CombineDFT_Fiss_3365_3386_join[6]));
	ENDFOR
}

void CombineDFT_3320() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_3365_3386_split[7]), &(SplitJoin110_CombineDFT_Fiss_3365_3386_join[7]));
	ENDFOR
}

void CombineDFT_3321() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_3365_3386_split[8]), &(SplitJoin110_CombineDFT_Fiss_3365_3386_join[8]));
	ENDFOR
}

void CombineDFT_3322() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_3365_3386_split[9]), &(SplitJoin110_CombineDFT_Fiss_3365_3386_join[9]));
	ENDFOR
}

void CombineDFT_3323() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_3365_3386_split[10]), &(SplitJoin110_CombineDFT_Fiss_3365_3386_join[10]));
	ENDFOR
}

void CombineDFT_3324() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_3365_3386_split[11]), &(SplitJoin110_CombineDFT_Fiss_3365_3386_join[11]));
	ENDFOR
}

void CombineDFT_3325() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_3365_3386_split[12]), &(SplitJoin110_CombineDFT_Fiss_3365_3386_join[12]));
	ENDFOR
}

void CombineDFT_3326() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_3365_3386_split[13]), &(SplitJoin110_CombineDFT_Fiss_3365_3386_join[13]));
	ENDFOR
}

void CombineDFT_3327() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_3365_3386_split[14]), &(SplitJoin110_CombineDFT_Fiss_3365_3386_join[14]));
	ENDFOR
}

void CombineDFT_3328() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_3365_3386_split[15]), &(SplitJoin110_CombineDFT_Fiss_3365_3386_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3311() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin110_CombineDFT_Fiss_3365_3386_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3284WEIGHTED_ROUND_ROBIN_Splitter_3311));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3312() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3312WEIGHTED_ROUND_ROBIN_Splitter_3329, pop_float(&SplitJoin110_CombineDFT_Fiss_3365_3386_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_3331() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_3366_3387_split[0]), &(SplitJoin112_CombineDFT_Fiss_3366_3387_join[0]));
	ENDFOR
}

void CombineDFT_3332() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_3366_3387_split[1]), &(SplitJoin112_CombineDFT_Fiss_3366_3387_join[1]));
	ENDFOR
}

void CombineDFT_3333() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_3366_3387_split[2]), &(SplitJoin112_CombineDFT_Fiss_3366_3387_join[2]));
	ENDFOR
}

void CombineDFT_3334() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_3366_3387_split[3]), &(SplitJoin112_CombineDFT_Fiss_3366_3387_join[3]));
	ENDFOR
}

void CombineDFT_3335() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_3366_3387_split[4]), &(SplitJoin112_CombineDFT_Fiss_3366_3387_join[4]));
	ENDFOR
}

void CombineDFT_3336() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_3366_3387_split[5]), &(SplitJoin112_CombineDFT_Fiss_3366_3387_join[5]));
	ENDFOR
}

void CombineDFT_3337() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_3366_3387_split[6]), &(SplitJoin112_CombineDFT_Fiss_3366_3387_join[6]));
	ENDFOR
}

void CombineDFT_3338() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin112_CombineDFT_Fiss_3366_3387_split[7]), &(SplitJoin112_CombineDFT_Fiss_3366_3387_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3329() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin112_CombineDFT_Fiss_3366_3387_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3312WEIGHTED_ROUND_ROBIN_Splitter_3329));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3330() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3330WEIGHTED_ROUND_ROBIN_Splitter_3339, pop_float(&SplitJoin112_CombineDFT_Fiss_3366_3387_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_3341() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin114_CombineDFT_Fiss_3367_3388_split[0]), &(SplitJoin114_CombineDFT_Fiss_3367_3388_join[0]));
	ENDFOR
}

void CombineDFT_3342() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin114_CombineDFT_Fiss_3367_3388_split[1]), &(SplitJoin114_CombineDFT_Fiss_3367_3388_join[1]));
	ENDFOR
}

void CombineDFT_3343() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin114_CombineDFT_Fiss_3367_3388_split[2]), &(SplitJoin114_CombineDFT_Fiss_3367_3388_join[2]));
	ENDFOR
}

void CombineDFT_3344() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin114_CombineDFT_Fiss_3367_3388_split[3]), &(SplitJoin114_CombineDFT_Fiss_3367_3388_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3339() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin114_CombineDFT_Fiss_3367_3388_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3330WEIGHTED_ROUND_ROBIN_Splitter_3339));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3340() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3340WEIGHTED_ROUND_ROBIN_Splitter_3345, pop_float(&SplitJoin114_CombineDFT_Fiss_3367_3388_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_3347() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin116_CombineDFT_Fiss_3368_3389_split[0]), &(SplitJoin116_CombineDFT_Fiss_3368_3389_join[0]));
	ENDFOR
}

void CombineDFT_3348() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin116_CombineDFT_Fiss_3368_3389_split[1]), &(SplitJoin116_CombineDFT_Fiss_3368_3389_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3345() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin116_CombineDFT_Fiss_3368_3389_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3340WEIGHTED_ROUND_ROBIN_Splitter_3345));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin116_CombineDFT_Fiss_3368_3389_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3340WEIGHTED_ROUND_ROBIN_Splitter_3345));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3346() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3346CombineDFT_3127, pop_float(&SplitJoin116_CombineDFT_Fiss_3368_3389_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3346CombineDFT_3127, pop_float(&SplitJoin116_CombineDFT_Fiss_3368_3389_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_3127() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_3346CombineDFT_3127), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_3095_3131_3350_3371_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3129() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_3095_3131_3350_3371_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3138WEIGHTED_ROUND_ROBIN_Splitter_3129));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_3095_3131_3350_3371_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3138WEIGHTED_ROUND_ROBIN_Splitter_3129));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3130() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3130FloatPrinter_3128, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_3095_3131_3350_3371_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3130FloatPrinter_3128, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_3095_3131_3350_3371_join[1]));
		ENDFOR
	ENDFOR
}}

void FloatPrinter(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void FloatPrinter_3128() {
	FOR(uint32_t, __iter_steady_, 0, <, 3328, __iter_steady_++)
		FloatPrinter(&(WEIGHTED_ROUND_ROBIN_Joiner_3130FloatPrinter_3128));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3142WEIGHTED_ROUND_ROBIN_Splitter_3145);
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_3351_3372_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 4, __iter_init_1_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_3358_3379_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_float(&SplitJoin116_CombineDFT_Fiss_3368_3389_join[__iter_init_2_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3250WEIGHTED_ROUND_ROBIN_Splitter_3255);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3180WEIGHTED_ROUND_ROBIN_Splitter_3207);
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_3357_3378_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 16, __iter_init_4_++)
		init_buffer_float(&SplitJoin110_CombineDFT_Fiss_3365_3386_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 16, __iter_init_5_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_3354_3375_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 2, __iter_init_6_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_3349_3370_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 2, __iter_init_7_++)
		init_buffer_float(&SplitJoin100_FFTReorderSimple_Fiss_3360_3381_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_float(&SplitJoin112_CombineDFT_Fiss_3366_3387_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 16, __iter_init_9_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_3354_3375_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_float(&SplitJoin100_FFTReorderSimple_Fiss_3360_3381_join[__iter_init_10_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3340WEIGHTED_ROUND_ROBIN_Splitter_3345);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3152WEIGHTED_ROUND_ROBIN_Splitter_3161);
	FOR(int, __iter_init_11_, 0, <, 16, __iter_init_11_++)
		init_buffer_float(&SplitJoin106_FFTReorderSimple_Fiss_3363_3384_split[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3236WEIGHTED_ROUND_ROBIN_Splitter_3241);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3256WEIGHTED_ROUND_ROBIN_Splitter_3265);
	FOR(int, __iter_init_12_, 0, <, 16, __iter_init_12_++)
		init_buffer_float(&SplitJoin106_FFTReorderSimple_Fiss_3363_3384_join[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 4, __iter_init_13_++)
		init_buffer_float(&SplitJoin114_CombineDFT_Fiss_3367_3388_split[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 26, __iter_init_14_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_3355_3376_split[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 4, __iter_init_15_++)
		init_buffer_float(&SplitJoin102_FFTReorderSimple_Fiss_3361_3382_split[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 26, __iter_init_16_++)
		init_buffer_float(&SplitJoin108_CombineDFT_Fiss_3364_3385_split[__iter_init_16_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3208WEIGHTED_ROUND_ROBIN_Splitter_3225);
	FOR(int, __iter_init_17_, 0, <, 4, __iter_init_17_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_3358_3379_join[__iter_init_17_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3312WEIGHTED_ROUND_ROBIN_Splitter_3329);
	FOR(int, __iter_init_18_, 0, <, 4, __iter_init_18_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_3352_3373_split[__iter_init_18_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3138WEIGHTED_ROUND_ROBIN_Splitter_3129);
	FOR(int, __iter_init_19_, 0, <, 4, __iter_init_19_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_3352_3373_join[__iter_init_19_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3246WEIGHTED_ROUND_ROBIN_Splitter_3249);
	FOR(int, __iter_init_20_, 0, <, 2, __iter_init_20_++)
		init_buffer_float(&SplitJoin116_CombineDFT_Fiss_3368_3389_split[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 2, __iter_init_21_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_3095_3131_3350_3371_split[__iter_init_21_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3242CombineDFT_3116);
	FOR(int, __iter_init_22_, 0, <, 8, __iter_init_22_++)
		init_buffer_float(&SplitJoin104_FFTReorderSimple_Fiss_3362_3383_join[__iter_init_22_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3284WEIGHTED_ROUND_ROBIN_Splitter_3311);
	FOR(int, __iter_init_23_, 0, <, 16, __iter_init_23_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_3356_3377_join[__iter_init_23_]);
	ENDFOR
	FOR(int, __iter_init_24_, 0, <, 2, __iter_init_24_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_3359_3380_split[__iter_init_24_]);
	ENDFOR
	FOR(int, __iter_init_25_, 0, <, 16, __iter_init_25_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_3356_3377_split[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 8, __iter_init_26_++)
		init_buffer_float(&SplitJoin112_CombineDFT_Fiss_3366_3387_join[__iter_init_26_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3130FloatPrinter_3128);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3330WEIGHTED_ROUND_ROBIN_Splitter_3339);
	FOR(int, __iter_init_27_, 0, <, 8, __iter_init_27_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_3353_3374_join[__iter_init_27_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3162WEIGHTED_ROUND_ROBIN_Splitter_3179);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3226WEIGHTED_ROUND_ROBIN_Splitter_3235);
	FOR(int, __iter_init_28_, 0, <, 8, __iter_init_28_++)
		init_buffer_float(&SplitJoin104_FFTReorderSimple_Fiss_3362_3383_split[__iter_init_28_]);
	ENDFOR
	init_buffer_float(&FFTReorderSimple_3117WEIGHTED_ROUND_ROBIN_Splitter_3245);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3346CombineDFT_3127);
	init_buffer_float(&FFTReorderSimple_3106WEIGHTED_ROUND_ROBIN_Splitter_3141);
	FOR(int, __iter_init_29_, 0, <, 26, __iter_init_29_++)
		init_buffer_float(&SplitJoin108_CombineDFT_Fiss_3364_3385_join[__iter_init_29_]);
	ENDFOR
	FOR(int, __iter_init_30_, 0, <, 2, __iter_init_30_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_3349_3370_join[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 2, __iter_init_31_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_3359_3380_join[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 4, __iter_init_32_++)
		init_buffer_float(&SplitJoin102_FFTReorderSimple_Fiss_3361_3382_join[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 8, __iter_init_33_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_3353_3374_split[__iter_init_33_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3266WEIGHTED_ROUND_ROBIN_Splitter_3283);
	FOR(int, __iter_init_34_, 0, <, 2, __iter_init_34_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_3351_3372_split[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 2, __iter_init_35_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_3095_3131_3350_3371_join[__iter_init_35_]);
	ENDFOR
	FOR(int, __iter_init_36_, 0, <, 8, __iter_init_36_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_3357_3378_split[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 26, __iter_init_37_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_3355_3376_join[__iter_init_37_]);
	ENDFOR
	FOR(int, __iter_init_38_, 0, <, 16, __iter_init_38_++)
		init_buffer_float(&SplitJoin110_CombineDFT_Fiss_3365_3386_split[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 4, __iter_init_39_++)
		init_buffer_float(&SplitJoin114_CombineDFT_Fiss_3367_3388_join[__iter_init_39_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3146WEIGHTED_ROUND_ROBIN_Splitter_3151);
// --- init: CombineDFT_3181
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3181_s.w[i] = real ; 
		CombineDFT_3181_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3182
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3182_s.w[i] = real ; 
		CombineDFT_3182_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3183
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3183_s.w[i] = real ; 
		CombineDFT_3183_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3184
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3184_s.w[i] = real ; 
		CombineDFT_3184_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3185
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3185_s.w[i] = real ; 
		CombineDFT_3185_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3186
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3186_s.w[i] = real ; 
		CombineDFT_3186_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3187
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3187_s.w[i] = real ; 
		CombineDFT_3187_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3188
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3188_s.w[i] = real ; 
		CombineDFT_3188_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3189
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3189_s.w[i] = real ; 
		CombineDFT_3189_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3190
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3190_s.w[i] = real ; 
		CombineDFT_3190_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3191
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3191_s.w[i] = real ; 
		CombineDFT_3191_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3192
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3192_s.w[i] = real ; 
		CombineDFT_3192_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3193
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3193_s.w[i] = real ; 
		CombineDFT_3193_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3194
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3194_s.w[i] = real ; 
		CombineDFT_3194_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3195
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3195_s.w[i] = real ; 
		CombineDFT_3195_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3196
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3196_s.w[i] = real ; 
		CombineDFT_3196_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3197
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3197_s.w[i] = real ; 
		CombineDFT_3197_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3198
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3198_s.w[i] = real ; 
		CombineDFT_3198_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3199
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3199_s.w[i] = real ; 
		CombineDFT_3199_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3200
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3200_s.w[i] = real ; 
		CombineDFT_3200_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3201
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3201_s.w[i] = real ; 
		CombineDFT_3201_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3202
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3202_s.w[i] = real ; 
		CombineDFT_3202_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3203
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3203_s.w[i] = real ; 
		CombineDFT_3203_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3204
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3204_s.w[i] = real ; 
		CombineDFT_3204_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3205
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3205_s.w[i] = real ; 
		CombineDFT_3205_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3206
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3206_s.w[i] = real ; 
		CombineDFT_3206_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3209
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3209_s.w[i] = real ; 
		CombineDFT_3209_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3210
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3210_s.w[i] = real ; 
		CombineDFT_3210_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3211
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3211_s.w[i] = real ; 
		CombineDFT_3211_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3212
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3212_s.w[i] = real ; 
		CombineDFT_3212_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3213
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3213_s.w[i] = real ; 
		CombineDFT_3213_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3214
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3214_s.w[i] = real ; 
		CombineDFT_3214_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3215
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3215_s.w[i] = real ; 
		CombineDFT_3215_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3216
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3216_s.w[i] = real ; 
		CombineDFT_3216_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3217
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3217_s.w[i] = real ; 
		CombineDFT_3217_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3218
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3218_s.w[i] = real ; 
		CombineDFT_3218_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3219
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3219_s.w[i] = real ; 
		CombineDFT_3219_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3220
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3220_s.w[i] = real ; 
		CombineDFT_3220_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3221
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3221_s.w[i] = real ; 
		CombineDFT_3221_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3222
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3222_s.w[i] = real ; 
		CombineDFT_3222_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3223
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3223_s.w[i] = real ; 
		CombineDFT_3223_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3224
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3224_s.w[i] = real ; 
		CombineDFT_3224_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3227
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_3227_s.w[i] = real ; 
		CombineDFT_3227_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3228
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_3228_s.w[i] = real ; 
		CombineDFT_3228_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3229
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_3229_s.w[i] = real ; 
		CombineDFT_3229_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3230
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_3230_s.w[i] = real ; 
		CombineDFT_3230_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3231
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_3231_s.w[i] = real ; 
		CombineDFT_3231_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3232
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_3232_s.w[i] = real ; 
		CombineDFT_3232_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3233
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_3233_s.w[i] = real ; 
		CombineDFT_3233_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3234
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_3234_s.w[i] = real ; 
		CombineDFT_3234_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3237
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_3237_s.w[i] = real ; 
		CombineDFT_3237_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3238
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_3238_s.w[i] = real ; 
		CombineDFT_3238_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3239
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_3239_s.w[i] = real ; 
		CombineDFT_3239_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3240
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_3240_s.w[i] = real ; 
		CombineDFT_3240_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3243
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_3243_s.w[i] = real ; 
		CombineDFT_3243_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3244
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_3244_s.w[i] = real ; 
		CombineDFT_3244_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3116
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_3116_s.w[i] = real ; 
		CombineDFT_3116_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3285
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3285_s.w[i] = real ; 
		CombineDFT_3285_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3286
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3286_s.w[i] = real ; 
		CombineDFT_3286_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3287
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3287_s.w[i] = real ; 
		CombineDFT_3287_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3288
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3288_s.w[i] = real ; 
		CombineDFT_3288_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3289
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3289_s.w[i] = real ; 
		CombineDFT_3289_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3290
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3290_s.w[i] = real ; 
		CombineDFT_3290_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3291
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3291_s.w[i] = real ; 
		CombineDFT_3291_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3292
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3292_s.w[i] = real ; 
		CombineDFT_3292_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3293
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3293_s.w[i] = real ; 
		CombineDFT_3293_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3294
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3294_s.w[i] = real ; 
		CombineDFT_3294_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3295
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3295_s.w[i] = real ; 
		CombineDFT_3295_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3296
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3296_s.w[i] = real ; 
		CombineDFT_3296_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3297
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3297_s.w[i] = real ; 
		CombineDFT_3297_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3298
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3298_s.w[i] = real ; 
		CombineDFT_3298_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3299
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3299_s.w[i] = real ; 
		CombineDFT_3299_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3300
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3300_s.w[i] = real ; 
		CombineDFT_3300_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3301
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3301_s.w[i] = real ; 
		CombineDFT_3301_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3302
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3302_s.w[i] = real ; 
		CombineDFT_3302_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3303
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3303_s.w[i] = real ; 
		CombineDFT_3303_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3304
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3304_s.w[i] = real ; 
		CombineDFT_3304_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3305
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3305_s.w[i] = real ; 
		CombineDFT_3305_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3306
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3306_s.w[i] = real ; 
		CombineDFT_3306_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3307
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3307_s.w[i] = real ; 
		CombineDFT_3307_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3308
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3308_s.w[i] = real ; 
		CombineDFT_3308_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3309
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3309_s.w[i] = real ; 
		CombineDFT_3309_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3310
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_3310_s.w[i] = real ; 
		CombineDFT_3310_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3313
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3313_s.w[i] = real ; 
		CombineDFT_3313_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3314
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3314_s.w[i] = real ; 
		CombineDFT_3314_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3315
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3315_s.w[i] = real ; 
		CombineDFT_3315_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3316
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3316_s.w[i] = real ; 
		CombineDFT_3316_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3317
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3317_s.w[i] = real ; 
		CombineDFT_3317_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3318
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3318_s.w[i] = real ; 
		CombineDFT_3318_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3319
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3319_s.w[i] = real ; 
		CombineDFT_3319_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3320
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3320_s.w[i] = real ; 
		CombineDFT_3320_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3321
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3321_s.w[i] = real ; 
		CombineDFT_3321_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3322
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3322_s.w[i] = real ; 
		CombineDFT_3322_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3323
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3323_s.w[i] = real ; 
		CombineDFT_3323_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3324
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3324_s.w[i] = real ; 
		CombineDFT_3324_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3325
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3325_s.w[i] = real ; 
		CombineDFT_3325_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3326
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3326_s.w[i] = real ; 
		CombineDFT_3326_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3327
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3327_s.w[i] = real ; 
		CombineDFT_3327_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3328
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_3328_s.w[i] = real ; 
		CombineDFT_3328_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3331
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_3331_s.w[i] = real ; 
		CombineDFT_3331_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3332
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_3332_s.w[i] = real ; 
		CombineDFT_3332_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3333
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_3333_s.w[i] = real ; 
		CombineDFT_3333_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3334
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_3334_s.w[i] = real ; 
		CombineDFT_3334_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3335
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_3335_s.w[i] = real ; 
		CombineDFT_3335_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3336
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_3336_s.w[i] = real ; 
		CombineDFT_3336_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3337
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_3337_s.w[i] = real ; 
		CombineDFT_3337_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3338
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_3338_s.w[i] = real ; 
		CombineDFT_3338_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3341
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_3341_s.w[i] = real ; 
		CombineDFT_3341_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3342
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_3342_s.w[i] = real ; 
		CombineDFT_3342_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3343
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_3343_s.w[i] = real ; 
		CombineDFT_3343_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3344
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_3344_s.w[i] = real ; 
		CombineDFT_3344_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3347
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_3347_s.w[i] = real ; 
		CombineDFT_3347_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3348
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_3348_s.w[i] = real ; 
		CombineDFT_3348_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_3127
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_3127_s.w[i] = real ; 
		CombineDFT_3127_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_3137();
			FFTTestSource_3139();
			FFTTestSource_3140();
		WEIGHTED_ROUND_ROBIN_Joiner_3138();
		WEIGHTED_ROUND_ROBIN_Splitter_3129();
			FFTReorderSimple_3106();
			WEIGHTED_ROUND_ROBIN_Splitter_3141();
				FFTReorderSimple_3143();
				FFTReorderSimple_3144();
			WEIGHTED_ROUND_ROBIN_Joiner_3142();
			WEIGHTED_ROUND_ROBIN_Splitter_3145();
				FFTReorderSimple_3147();
				FFTReorderSimple_3148();
				FFTReorderSimple_3149();
				FFTReorderSimple_3150();
			WEIGHTED_ROUND_ROBIN_Joiner_3146();
			WEIGHTED_ROUND_ROBIN_Splitter_3151();
				FFTReorderSimple_3153();
				FFTReorderSimple_3154();
				FFTReorderSimple_3155();
				FFTReorderSimple_3156();
				FFTReorderSimple_3157();
				FFTReorderSimple_3158();
				FFTReorderSimple_3159();
				FFTReorderSimple_3160();
			WEIGHTED_ROUND_ROBIN_Joiner_3152();
			WEIGHTED_ROUND_ROBIN_Splitter_3161();
				FFTReorderSimple_3163();
				FFTReorderSimple_3164();
				FFTReorderSimple_3165();
				FFTReorderSimple_3166();
				FFTReorderSimple_3167();
				FFTReorderSimple_3168();
				FFTReorderSimple_3169();
				FFTReorderSimple_3170();
				FFTReorderSimple_3171();
				FFTReorderSimple_3172();
				FFTReorderSimple_3173();
				FFTReorderSimple_3174();
				FFTReorderSimple_3175();
				FFTReorderSimple_3176();
				FFTReorderSimple_3177();
				FFTReorderSimple_3178();
			WEIGHTED_ROUND_ROBIN_Joiner_3162();
			WEIGHTED_ROUND_ROBIN_Splitter_3179();
				CombineDFT_3181();
				CombineDFT_3182();
				CombineDFT_3183();
				CombineDFT_3184();
				CombineDFT_3185();
				CombineDFT_3186();
				CombineDFT_3187();
				CombineDFT_3188();
				CombineDFT_3189();
				CombineDFT_3190();
				CombineDFT_3191();
				CombineDFT_3192();
				CombineDFT_3193();
				CombineDFT_3194();
				CombineDFT_3195();
				CombineDFT_3196();
				CombineDFT_3197();
				CombineDFT_3198();
				CombineDFT_3199();
				CombineDFT_3200();
				CombineDFT_3201();
				CombineDFT_3202();
				CombineDFT_3203();
				CombineDFT_3204();
				CombineDFT_3205();
				CombineDFT_3206();
			WEIGHTED_ROUND_ROBIN_Joiner_3180();
			WEIGHTED_ROUND_ROBIN_Splitter_3207();
				CombineDFT_3209();
				CombineDFT_3210();
				CombineDFT_3211();
				CombineDFT_3212();
				CombineDFT_3213();
				CombineDFT_3214();
				CombineDFT_3215();
				CombineDFT_3216();
				CombineDFT_3217();
				CombineDFT_3218();
				CombineDFT_3219();
				CombineDFT_3220();
				CombineDFT_3221();
				CombineDFT_3222();
				CombineDFT_3223();
				CombineDFT_3224();
			WEIGHTED_ROUND_ROBIN_Joiner_3208();
			WEIGHTED_ROUND_ROBIN_Splitter_3225();
				CombineDFT_3227();
				CombineDFT_3228();
				CombineDFT_3229();
				CombineDFT_3230();
				CombineDFT_3231();
				CombineDFT_3232();
				CombineDFT_3233();
				CombineDFT_3234();
			WEIGHTED_ROUND_ROBIN_Joiner_3226();
			WEIGHTED_ROUND_ROBIN_Splitter_3235();
				CombineDFT_3237();
				CombineDFT_3238();
				CombineDFT_3239();
				CombineDFT_3240();
			WEIGHTED_ROUND_ROBIN_Joiner_3236();
			WEIGHTED_ROUND_ROBIN_Splitter_3241();
				CombineDFT_3243();
				CombineDFT_3244();
			WEIGHTED_ROUND_ROBIN_Joiner_3242();
			CombineDFT_3116();
			FFTReorderSimple_3117();
			WEIGHTED_ROUND_ROBIN_Splitter_3245();
				FFTReorderSimple_3247();
				FFTReorderSimple_3248();
			WEIGHTED_ROUND_ROBIN_Joiner_3246();
			WEIGHTED_ROUND_ROBIN_Splitter_3249();
				FFTReorderSimple_3251();
				FFTReorderSimple_3252();
				FFTReorderSimple_3253();
				FFTReorderSimple_3254();
			WEIGHTED_ROUND_ROBIN_Joiner_3250();
			WEIGHTED_ROUND_ROBIN_Splitter_3255();
				FFTReorderSimple_3257();
				FFTReorderSimple_3258();
				FFTReorderSimple_3259();
				FFTReorderSimple_3260();
				FFTReorderSimple_3261();
				FFTReorderSimple_3262();
				FFTReorderSimple_3263();
				FFTReorderSimple_3264();
			WEIGHTED_ROUND_ROBIN_Joiner_3256();
			WEIGHTED_ROUND_ROBIN_Splitter_3265();
				FFTReorderSimple_3267();
				FFTReorderSimple_3268();
				FFTReorderSimple_3269();
				FFTReorderSimple_3270();
				FFTReorderSimple_3271();
				FFTReorderSimple_3272();
				FFTReorderSimple_3273();
				FFTReorderSimple_3274();
				FFTReorderSimple_3275();
				FFTReorderSimple_3276();
				FFTReorderSimple_3277();
				FFTReorderSimple_3278();
				FFTReorderSimple_3279();
				FFTReorderSimple_3280();
				FFTReorderSimple_3281();
				FFTReorderSimple_3282();
			WEIGHTED_ROUND_ROBIN_Joiner_3266();
			WEIGHTED_ROUND_ROBIN_Splitter_3283();
				CombineDFT_3285();
				CombineDFT_3286();
				CombineDFT_3287();
				CombineDFT_3288();
				CombineDFT_3289();
				CombineDFT_3290();
				CombineDFT_3291();
				CombineDFT_3292();
				CombineDFT_3293();
				CombineDFT_3294();
				CombineDFT_3295();
				CombineDFT_3296();
				CombineDFT_3297();
				CombineDFT_3298();
				CombineDFT_3299();
				CombineDFT_3300();
				CombineDFT_3301();
				CombineDFT_3302();
				CombineDFT_3303();
				CombineDFT_3304();
				CombineDFT_3305();
				CombineDFT_3306();
				CombineDFT_3307();
				CombineDFT_3308();
				CombineDFT_3309();
				CombineDFT_3310();
			WEIGHTED_ROUND_ROBIN_Joiner_3284();
			WEIGHTED_ROUND_ROBIN_Splitter_3311();
				CombineDFT_3313();
				CombineDFT_3314();
				CombineDFT_3315();
				CombineDFT_3316();
				CombineDFT_3317();
				CombineDFT_3318();
				CombineDFT_3319();
				CombineDFT_3320();
				CombineDFT_3321();
				CombineDFT_3322();
				CombineDFT_3323();
				CombineDFT_3324();
				CombineDFT_3325();
				CombineDFT_3326();
				CombineDFT_3327();
				CombineDFT_3328();
			WEIGHTED_ROUND_ROBIN_Joiner_3312();
			WEIGHTED_ROUND_ROBIN_Splitter_3329();
				CombineDFT_3331();
				CombineDFT_3332();
				CombineDFT_3333();
				CombineDFT_3334();
				CombineDFT_3335();
				CombineDFT_3336();
				CombineDFT_3337();
				CombineDFT_3338();
			WEIGHTED_ROUND_ROBIN_Joiner_3330();
			WEIGHTED_ROUND_ROBIN_Splitter_3339();
				CombineDFT_3341();
				CombineDFT_3342();
				CombineDFT_3343();
				CombineDFT_3344();
			WEIGHTED_ROUND_ROBIN_Joiner_3340();
			WEIGHTED_ROUND_ROBIN_Splitter_3345();
				CombineDFT_3347();
				CombineDFT_3348();
			WEIGHTED_ROUND_ROBIN_Joiner_3346();
			CombineDFT_3127();
		WEIGHTED_ROUND_ROBIN_Joiner_3130();
		FloatPrinter_3128();
	ENDFOR
	return EXIT_SUCCESS;
}
