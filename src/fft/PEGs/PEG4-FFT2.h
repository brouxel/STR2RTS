#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=512 on the compile command line
#else
#if BUF_SIZEMAX < 512
#error BUF_SIZEMAX too small, it must be at least 512
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float w[2];
} CombineDFT_12115_t;

typedef struct {
	float w[4];
} CombineDFT_12121_t;

typedef struct {
	float w[8];
} CombineDFT_12127_t;

typedef struct {
	float w[16];
} CombineDFT_12133_t;

typedef struct {
	float w[32];
} CombineDFT_12139_t;

typedef struct {
	float w[64];
} CombineDFT_12066_t;
void WEIGHTED_ROUND_ROBIN_Splitter_12087();
void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout);
void FFTTestSource_12089();
void FFTTestSource_12090();
void WEIGHTED_ROUND_ROBIN_Joiner_12088();
void WEIGHTED_ROUND_ROBIN_Splitter_12079();
void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout);
void FFTReorderSimple_12056();
void WEIGHTED_ROUND_ROBIN_Splitter_12091();
void FFTReorderSimple_12093();
void FFTReorderSimple_12094();
void WEIGHTED_ROUND_ROBIN_Joiner_12092();
void WEIGHTED_ROUND_ROBIN_Splitter_12095();
void FFTReorderSimple_12097();
void FFTReorderSimple_12098();
void FFTReorderSimple_12099();
void FFTReorderSimple_12100();
void WEIGHTED_ROUND_ROBIN_Joiner_12096();
void WEIGHTED_ROUND_ROBIN_Splitter_12101();
void FFTReorderSimple_12103();
void FFTReorderSimple_12104();
void FFTReorderSimple_12105();
void FFTReorderSimple_12106();
void WEIGHTED_ROUND_ROBIN_Joiner_12102();
void WEIGHTED_ROUND_ROBIN_Splitter_12107();
void FFTReorderSimple_12109();
void FFTReorderSimple_12110();
void FFTReorderSimple_12111();
void FFTReorderSimple_12112();
void WEIGHTED_ROUND_ROBIN_Joiner_12108();
void WEIGHTED_ROUND_ROBIN_Splitter_12113();
void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout);
void CombineDFT_12115();
void CombineDFT_12116();
void CombineDFT_12117();
void CombineDFT_12118();
void WEIGHTED_ROUND_ROBIN_Joiner_12114();
void WEIGHTED_ROUND_ROBIN_Splitter_12119();
void CombineDFT_12121();
void CombineDFT_12122();
void CombineDFT_12123();
void CombineDFT_12124();
void WEIGHTED_ROUND_ROBIN_Joiner_12120();
void WEIGHTED_ROUND_ROBIN_Splitter_12125();
void CombineDFT_12127();
void CombineDFT_12128();
void CombineDFT_12129();
void CombineDFT_12130();
void WEIGHTED_ROUND_ROBIN_Joiner_12126();
void WEIGHTED_ROUND_ROBIN_Splitter_12131();
void CombineDFT_12133();
void CombineDFT_12134();
void CombineDFT_12135();
void CombineDFT_12136();
void WEIGHTED_ROUND_ROBIN_Joiner_12132();
void WEIGHTED_ROUND_ROBIN_Splitter_12137();
void CombineDFT_12139();
void CombineDFT_12140();
void WEIGHTED_ROUND_ROBIN_Joiner_12138();
void CombineDFT_12066();
void FFTReorderSimple_12067();
void WEIGHTED_ROUND_ROBIN_Splitter_12141();
void FFTReorderSimple_12143();
void FFTReorderSimple_12144();
void WEIGHTED_ROUND_ROBIN_Joiner_12142();
void WEIGHTED_ROUND_ROBIN_Splitter_12145();
void FFTReorderSimple_12147();
void FFTReorderSimple_12148();
void FFTReorderSimple_12149();
void FFTReorderSimple_12150();
void WEIGHTED_ROUND_ROBIN_Joiner_12146();
void WEIGHTED_ROUND_ROBIN_Splitter_12151();
void FFTReorderSimple_12153();
void FFTReorderSimple_12154();
void FFTReorderSimple_12155();
void FFTReorderSimple_12156();
void WEIGHTED_ROUND_ROBIN_Joiner_12152();
void WEIGHTED_ROUND_ROBIN_Splitter_12157();
void FFTReorderSimple_12159();
void FFTReorderSimple_12160();
void FFTReorderSimple_12161();
void FFTReorderSimple_12162();
void WEIGHTED_ROUND_ROBIN_Joiner_12158();
void WEIGHTED_ROUND_ROBIN_Splitter_12163();
void CombineDFT_12165();
void CombineDFT_12166();
void CombineDFT_12167();
void CombineDFT_12168();
void WEIGHTED_ROUND_ROBIN_Joiner_12164();
void WEIGHTED_ROUND_ROBIN_Splitter_12169();
void CombineDFT_12171();
void CombineDFT_12172();
void CombineDFT_12173();
void CombineDFT_12174();
void WEIGHTED_ROUND_ROBIN_Joiner_12170();
void WEIGHTED_ROUND_ROBIN_Splitter_12175();
void CombineDFT_12177();
void CombineDFT_12178();
void CombineDFT_12179();
void CombineDFT_12180();
void WEIGHTED_ROUND_ROBIN_Joiner_12176();
void WEIGHTED_ROUND_ROBIN_Splitter_12181();
void CombineDFT_12183();
void CombineDFT_12184();
void CombineDFT_12185();
void CombineDFT_12186();
void WEIGHTED_ROUND_ROBIN_Joiner_12182();
void WEIGHTED_ROUND_ROBIN_Splitter_12187();
void CombineDFT_12189();
void CombineDFT_12190();
void WEIGHTED_ROUND_ROBIN_Joiner_12188();
void CombineDFT_12077();
void WEIGHTED_ROUND_ROBIN_Joiner_12080();
void FloatPrinter(buffer_float_t *chanin);
void FloatPrinter_12078();

#ifdef __cplusplus
}
#endif
#endif
