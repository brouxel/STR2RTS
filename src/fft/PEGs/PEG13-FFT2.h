#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=6656 on the compile command line
#else
#if BUF_SIZEMAX < 6656
#error BUF_SIZEMAX too small, it must be at least 6656
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float w[2];
} CombineDFT_9017_t;

typedef struct {
	float w[4];
} CombineDFT_9032_t;

typedef struct {
	float w[8];
} CombineDFT_9047_t;

typedef struct {
	float w[16];
} CombineDFT_9057_t;

typedef struct {
	float w[32];
} CombineDFT_9063_t;

typedef struct {
	float w[64];
} CombineDFT_8955_t;
void WEIGHTED_ROUND_ROBIN_Splitter_8976();
void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout);
void FFTTestSource_8978();
void FFTTestSource_8979();
void WEIGHTED_ROUND_ROBIN_Joiner_8977();
void WEIGHTED_ROUND_ROBIN_Splitter_8968();
void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout);
void FFTReorderSimple_8945();
void WEIGHTED_ROUND_ROBIN_Splitter_8980();
void FFTReorderSimple_8982();
void FFTReorderSimple_8983();
void WEIGHTED_ROUND_ROBIN_Joiner_8981();
void WEIGHTED_ROUND_ROBIN_Splitter_8984();
void FFTReorderSimple_8986();
void FFTReorderSimple_8987();
void FFTReorderSimple_8988();
void FFTReorderSimple_8989();
void WEIGHTED_ROUND_ROBIN_Joiner_8985();
void WEIGHTED_ROUND_ROBIN_Splitter_8990();
void FFTReorderSimple_8992();
void FFTReorderSimple_8993();
void FFTReorderSimple_8994();
void FFTReorderSimple_8995();
void FFTReorderSimple_8996();
void FFTReorderSimple_8997();
void FFTReorderSimple_8998();
void FFTReorderSimple_8999();
void WEIGHTED_ROUND_ROBIN_Joiner_8991();
void WEIGHTED_ROUND_ROBIN_Splitter_9000();
void FFTReorderSimple_9002();
void FFTReorderSimple_9003();
void FFTReorderSimple_9004();
void FFTReorderSimple_9005();
void FFTReorderSimple_9006();
void FFTReorderSimple_9007();
void FFTReorderSimple_9008();
void FFTReorderSimple_9009();
void FFTReorderSimple_9010();
void FFTReorderSimple_9011();
void FFTReorderSimple_9012();
void FFTReorderSimple_9013();
void FFTReorderSimple_9014();
void WEIGHTED_ROUND_ROBIN_Joiner_9001();
void WEIGHTED_ROUND_ROBIN_Splitter_9015();
void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout);
void CombineDFT_9017();
void CombineDFT_9018();
void CombineDFT_9019();
void CombineDFT_9020();
void CombineDFT_9021();
void CombineDFT_9022();
void CombineDFT_9023();
void CombineDFT_9024();
void CombineDFT_9025();
void CombineDFT_9026();
void CombineDFT_9027();
void CombineDFT_9028();
void CombineDFT_9029();
void WEIGHTED_ROUND_ROBIN_Joiner_9016();
void WEIGHTED_ROUND_ROBIN_Splitter_9030();
void CombineDFT_9032();
void CombineDFT_9033();
void CombineDFT_9034();
void CombineDFT_9035();
void CombineDFT_9036();
void CombineDFT_9037();
void CombineDFT_9038();
void CombineDFT_9039();
void CombineDFT_9040();
void CombineDFT_9041();
void CombineDFT_9042();
void CombineDFT_9043();
void CombineDFT_9044();
void WEIGHTED_ROUND_ROBIN_Joiner_9031();
void WEIGHTED_ROUND_ROBIN_Splitter_9045();
void CombineDFT_9047();
void CombineDFT_9048();
void CombineDFT_9049();
void CombineDFT_9050();
void CombineDFT_9051();
void CombineDFT_9052();
void CombineDFT_9053();
void CombineDFT_9054();
void WEIGHTED_ROUND_ROBIN_Joiner_9046();
void WEIGHTED_ROUND_ROBIN_Splitter_9055();
void CombineDFT_9057();
void CombineDFT_9058();
void CombineDFT_9059();
void CombineDFT_9060();
void WEIGHTED_ROUND_ROBIN_Joiner_9056();
void WEIGHTED_ROUND_ROBIN_Splitter_9061();
void CombineDFT_9063();
void CombineDFT_9064();
void WEIGHTED_ROUND_ROBIN_Joiner_9062();
void CombineDFT_8955();
void FFTReorderSimple_8956();
void WEIGHTED_ROUND_ROBIN_Splitter_9065();
void FFTReorderSimple_9067();
void FFTReorderSimple_9068();
void WEIGHTED_ROUND_ROBIN_Joiner_9066();
void WEIGHTED_ROUND_ROBIN_Splitter_9069();
void FFTReorderSimple_9071();
void FFTReorderSimple_9072();
void FFTReorderSimple_9073();
void FFTReorderSimple_9074();
void WEIGHTED_ROUND_ROBIN_Joiner_9070();
void WEIGHTED_ROUND_ROBIN_Splitter_9075();
void FFTReorderSimple_9077();
void FFTReorderSimple_9078();
void FFTReorderSimple_9079();
void FFTReorderSimple_9080();
void FFTReorderSimple_9081();
void FFTReorderSimple_9082();
void FFTReorderSimple_9083();
void FFTReorderSimple_9084();
void WEIGHTED_ROUND_ROBIN_Joiner_9076();
void WEIGHTED_ROUND_ROBIN_Splitter_9085();
void FFTReorderSimple_9087();
void FFTReorderSimple_9088();
void FFTReorderSimple_9089();
void FFTReorderSimple_9090();
void FFTReorderSimple_9091();
void FFTReorderSimple_9092();
void FFTReorderSimple_9093();
void FFTReorderSimple_9094();
void FFTReorderSimple_9095();
void FFTReorderSimple_9096();
void FFTReorderSimple_9097();
void FFTReorderSimple_9098();
void FFTReorderSimple_9099();
void WEIGHTED_ROUND_ROBIN_Joiner_9086();
void WEIGHTED_ROUND_ROBIN_Splitter_9100();
void CombineDFT_9102();
void CombineDFT_9103();
void CombineDFT_9104();
void CombineDFT_9105();
void CombineDFT_9106();
void CombineDFT_9107();
void CombineDFT_9108();
void CombineDFT_9109();
void CombineDFT_9110();
void CombineDFT_9111();
void CombineDFT_9112();
void CombineDFT_9113();
void CombineDFT_9114();
void WEIGHTED_ROUND_ROBIN_Joiner_9101();
void WEIGHTED_ROUND_ROBIN_Splitter_9115();
void CombineDFT_9117();
void CombineDFT_9118();
void CombineDFT_9119();
void CombineDFT_9120();
void CombineDFT_9121();
void CombineDFT_9122();
void CombineDFT_9123();
void CombineDFT_9124();
void CombineDFT_9125();
void CombineDFT_9126();
void CombineDFT_9127();
void CombineDFT_9128();
void CombineDFT_9129();
void WEIGHTED_ROUND_ROBIN_Joiner_9116();
void WEIGHTED_ROUND_ROBIN_Splitter_9130();
void CombineDFT_9132();
void CombineDFT_9133();
void CombineDFT_9134();
void CombineDFT_9135();
void CombineDFT_9136();
void CombineDFT_9137();
void CombineDFT_9138();
void CombineDFT_9139();
void WEIGHTED_ROUND_ROBIN_Joiner_9131();
void WEIGHTED_ROUND_ROBIN_Splitter_9140();
void CombineDFT_9142();
void CombineDFT_9143();
void CombineDFT_9144();
void CombineDFT_9145();
void WEIGHTED_ROUND_ROBIN_Joiner_9141();
void WEIGHTED_ROUND_ROBIN_Splitter_9146();
void CombineDFT_9148();
void CombineDFT_9149();
void WEIGHTED_ROUND_ROBIN_Joiner_9147();
void CombineDFT_8966();
void WEIGHTED_ROUND_ROBIN_Joiner_8969();
void FloatPrinter(buffer_float_t *chanin);
void FloatPrinter_8967();

#ifdef __cplusplus
}
#endif
#endif
