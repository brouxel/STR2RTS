#include "PEG4-FFT3_nocache.h"

buffer_float_t SplitJoin43_Butterfly_Fiss_10681_10706_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10664Post_CollapsedDataParallel_2_10495;
buffer_float_t SplitJoin57_Butterfly_Fiss_10684_10710_join[2];
buffer_float_t SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_Hier_10675_10702_join[2];
buffer_float_t SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child0_10577_10703_split[4];
buffer_float_t SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_Hier_10677_10713_join[2];
buffer_float_t Pre_CollapsedDataParallel_1_10485WEIGHTED_ROUND_ROBIN_Splitter_10651;
buffer_float_t SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_Hier_10677_10713_split[2];
buffer_float_t SplitJoin61_Butterfly_Fiss_10685_10711_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10652Post_CollapsedDataParallel_2_10486;
buffer_float_t SplitJoin43_Butterfly_Fiss_10681_10706_join[2];
buffer_float_t SplitJoin8_Butterfly_Fiss_10674_10696_split[4];
buffer_float_t Pre_CollapsedDataParallel_1_10467WEIGHTED_ROUND_ROBIN_Splitter_10615;
buffer_float_t SplitJoin91_Butterfly_Fiss_10690_10701_split[4];
buffer_float_t SplitJoin72_Butterfly_Fiss_10687_10697_join[4];
buffer_float_t SplitJoin4_Butterfly_Fiss_10673_10694_split[4];
buffer_float_t Pre_CollapsedDataParallel_1_10479WEIGHTED_ROUND_ROBIN_Splitter_10643;
buffer_float_t SplitJoin83_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_child1_10553_10699_join[2];
buffer_float_t SplitJoin83_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_child1_10553_10699_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10628Post_CollapsedDataParallel_2_10471;
buffer_float_t SplitJoin61_Butterfly_Fiss_10685_10711_join[2];
buffer_float_t Post_CollapsedDataParallel_2_10456WEIGHTED_ROUND_ROBIN_Splitter_10499;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10610Post_CollapsedDataParallel_2_10465;
buffer_float_t Pre_CollapsedDataParallel_1_10491WEIGHTED_ROUND_ROBIN_Splitter_10659;
buffer_float_t SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[8];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_10112_10507_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_Hier_10672_10693_join[2];
buffer_float_t SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10660Post_CollapsedDataParallel_2_10492;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10616Post_CollapsedDataParallel_2_10468;
buffer_float_t SplitJoin65_Butterfly_Fiss_10686_10712_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10640Post_CollapsedDataParallel_2_10477;
buffer_float_t Pre_CollapsedDataParallel_1_10473WEIGHTED_ROUND_ROBIN_Splitter_10633;
buffer_float_t Pre_CollapsedDataParallel_1_10476WEIGHTED_ROUND_ROBIN_Splitter_10639;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10644Post_CollapsedDataParallel_2_10480;
buffer_float_t Pre_CollapsedDataParallel_1_10488WEIGHTED_ROUND_ROBIN_Splitter_10655;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10598Post_CollapsedDataParallel_2_10456;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10583WEIGHTED_ROUND_ROBIN_Splitter_10584;
buffer_float_t SplitJoin81_Butterfly_Fiss_10688_10698_join[4];
buffer_float_t Pre_CollapsedDataParallel_1_10461WEIGHTED_ROUND_ROBIN_Splitter_10621;
buffer_float_t SplitJoin72_Butterfly_Fiss_10687_10697_split[4];
buffer_float_t SplitJoin81_Butterfly_Fiss_10688_10698_split[4];
buffer_float_t SplitJoin8_Butterfly_Fiss_10674_10696_join[4];
buffer_float_t Pre_CollapsedDataParallel_1_10497WEIGHTED_ROUND_ROBIN_Splitter_10667;
buffer_float_t Post_CollapsedDataParallel_2_10459WEIGHTED_ROUND_ROBIN_Splitter_10579;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10648Post_CollapsedDataParallel_2_10483;
buffer_float_t Pre_CollapsedDataParallel_1_10458WEIGHTED_ROUND_ROBIN_Splitter_10603;
buffer_float_t SplitJoin14_Butterfly_Fiss_10676_10704_split[2];
buffer_float_t Pre_CollapsedDataParallel_1_10455WEIGHTED_ROUND_ROBIN_Splitter_10597;
buffer_float_t SplitJoin0_Butterfly_Fiss_10671_10692_split[4];
buffer_float_t Post_CollapsedDataParallel_2_10462WEIGHTED_ROUND_ROBIN_Splitter_10581;
buffer_float_t SplitJoin14_Butterfly_Fiss_10676_10704_join[2];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_10112_10507_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_Hier_10672_10693_split[2];
buffer_float_t SplitJoin85_Butterfly_Fiss_10689_10700_join[4];
buffer_float_t BitReverse_10235FloatPrinter_10236;
buffer_float_t SplitJoin39_Butterfly_Fiss_10680_10705_join[2];
buffer_float_t Pre_CollapsedDataParallel_1_10470WEIGHTED_ROUND_ROBIN_Splitter_10627;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10595BitReverse_10235;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10604Post_CollapsedDataParallel_2_10459;
buffer_float_t SplitJoin4_Butterfly_Fiss_10673_10694_join[4];
buffer_float_t SplitJoin57_Butterfly_Fiss_10684_10710_split[2];
buffer_float_t SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_child0_10548_10695_join[2];
buffer_float_t SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_child0_10548_10695_split[2];
buffer_float_t SplitJoin39_Butterfly_Fiss_10680_10705_split[2];
buffer_float_t SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10589WEIGHTED_ROUND_ROBIN_Splitter_10590;
buffer_float_t Pre_CollapsedDataParallel_1_10464WEIGHTED_ROUND_ROBIN_Splitter_10609;
buffer_float_t SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child1_10578_10708_join[4];
buffer_float_t Pre_CollapsedDataParallel_1_10494WEIGHTED_ROUND_ROBIN_Splitter_10663;
buffer_float_t SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_Hier_10675_10702_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10634Post_CollapsedDataParallel_2_10474;
buffer_float_t FloatSource_10154Pre_CollapsedDataParallel_1_10455;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10656Post_CollapsedDataParallel_2_10489;
buffer_float_t SplitJoin47_Butterfly_Fiss_10682_10707_split[2];
buffer_float_t SplitJoin47_Butterfly_Fiss_10682_10707_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10622Post_CollapsedDataParallel_2_10462;
buffer_float_t SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[8];
buffer_float_t SplitJoin53_Butterfly_Fiss_10683_10709_split[2];
buffer_float_t SplitJoin53_Butterfly_Fiss_10683_10709_join[2];
buffer_float_t SplitJoin85_Butterfly_Fiss_10689_10700_split[4];
buffer_float_t SplitJoin0_Butterfly_Fiss_10671_10692_join[4];
buffer_float_t Pre_CollapsedDataParallel_1_10482WEIGHTED_ROUND_ROBIN_Splitter_10647;
buffer_float_t SplitJoin65_Butterfly_Fiss_10686_10712_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_10668Post_CollapsedDataParallel_2_10498;
buffer_float_t SplitJoin91_Butterfly_Fiss_10690_10701_join[4];
buffer_float_t SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child0_10577_10703_join[4];
buffer_float_t SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child1_10578_10708_split[4];


FloatSource_10154_t FloatSource_10154_s;

void FloatSource_10154(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&FloatSource_10154Pre_CollapsedDataParallel_1_10455, FloatSource_10154_s.A_re[FloatSource_10154_s.idx]) ; 
		push_float(&FloatSource_10154Pre_CollapsedDataParallel_1_10455, FloatSource_10154_s.A_im[FloatSource_10154_s.idx]) ; 
		FloatSource_10154_s.idx++ ; 
		if((FloatSource_10154_s.idx >= 32)) {
			FloatSource_10154_s.idx = 0 ; 
		}
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_10455() {
 {
	int partialSum_k = 0;
 {
	FOR(int, _k, 0,  < , 16, _k++) {
		int iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&Pre_CollapsedDataParallel_1_10455WEIGHTED_ROUND_ROBIN_Splitter_10597, peek_float(&FloatSource_10154Pre_CollapsedDataParallel_1_10455, (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
			}
			ENDFOR
		}
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 32) ; 
		}
		ENDFOR
	}
		partialSum_k = (partialSum_k + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&FloatSource_10154Pre_CollapsedDataParallel_1_10455) ; 
}


void Butterfly_10599(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin0_Butterfly_Fiss_10671_10692_split[0]) ; 
		u_im = pop_float(&SplitJoin0_Butterfly_Fiss_10671_10692_split[0]) ; 
		t_re = pop_float(&SplitJoin0_Butterfly_Fiss_10671_10692_split[0]) ; 
		t_im = pop_float(&SplitJoin0_Butterfly_Fiss_10671_10692_split[0]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_10671_10692_join[0], u_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_10671_10692_join[0], u_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_10671_10692_join[0], t_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_10671_10692_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_10600(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin0_Butterfly_Fiss_10671_10692_split[1]) ; 
		u_im = pop_float(&SplitJoin0_Butterfly_Fiss_10671_10692_split[1]) ; 
		t_re = pop_float(&SplitJoin0_Butterfly_Fiss_10671_10692_split[1]) ; 
		t_im = pop_float(&SplitJoin0_Butterfly_Fiss_10671_10692_split[1]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_10671_10692_join[1], u_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_10671_10692_join[1], u_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_10671_10692_join[1], t_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_10671_10692_join[1], t_im) ; 
	}
	ENDFOR
}

void Butterfly_10601(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin0_Butterfly_Fiss_10671_10692_split[2]) ; 
		u_im = pop_float(&SplitJoin0_Butterfly_Fiss_10671_10692_split[2]) ; 
		t_re = pop_float(&SplitJoin0_Butterfly_Fiss_10671_10692_split[2]) ; 
		t_im = pop_float(&SplitJoin0_Butterfly_Fiss_10671_10692_split[2]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_10671_10692_join[2], u_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_10671_10692_join[2], u_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_10671_10692_join[2], t_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_10671_10692_join[2], t_im) ; 
	}
	ENDFOR
}

void Butterfly_10602(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin0_Butterfly_Fiss_10671_10692_split[3]) ; 
		u_im = pop_float(&SplitJoin0_Butterfly_Fiss_10671_10692_split[3]) ; 
		t_re = pop_float(&SplitJoin0_Butterfly_Fiss_10671_10692_split[3]) ; 
		t_im = pop_float(&SplitJoin0_Butterfly_Fiss_10671_10692_split[3]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_10671_10692_join[3], u_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_10671_10692_join[3], u_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_10671_10692_join[3], t_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_10671_10692_join[3], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10597() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin0_Butterfly_Fiss_10671_10692_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_10455WEIGHTED_ROUND_ROBIN_Splitter_10597));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10598() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10598Post_CollapsedDataParallel_2_10456, pop_float(&SplitJoin0_Butterfly_Fiss_10671_10692_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_10456() {
 {
	int kTimesWeights_i = 0;
 {
	FOR(int, _k, 0,  < , 2, _k++) {
		int partialSum_i = 0;
 {
		FOR(int, _i, 0,  < , 16, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&Post_CollapsedDataParallel_2_10456WEIGHTED_ROUND_ROBIN_Splitter_10499, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_10598Post_CollapsedDataParallel_2_10456, (kTimesWeights_i + (partialSum_i + _j)))) ; 
			}
			ENDFOR
		}
			partialSum_i = (partialSum_i + 4) ; 
		}
		ENDFOR
	}
		kTimesWeights_i = (kTimesWeights_i + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10598Post_CollapsedDataParallel_2_10456) ; 
}


void Pre_CollapsedDataParallel_1_10458() {
 {
	int partialSum_k = 0;
 {
	FOR(int, _k, 0,  < , 8, _k++) {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
		iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
		FOR(int, _i, 0,  < , 2, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&Pre_CollapsedDataParallel_1_10458WEIGHTED_ROUND_ROBIN_Splitter_10603, peek_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_10112_10507_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_Hier_10672_10693_split[0], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
			}
			ENDFOR
		}
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 16) ; 
		}
		ENDFOR
	}
		partialSum_k = (partialSum_k + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_10112_10507_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_Hier_10672_10693_split[0]) ; 
}


void Butterfly_10605(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin4_Butterfly_Fiss_10673_10694_split[0]) ; 
		u_im = pop_float(&SplitJoin4_Butterfly_Fiss_10673_10694_split[0]) ; 
		t_re = pop_float(&SplitJoin4_Butterfly_Fiss_10673_10694_split[0]) ; 
		t_im = pop_float(&SplitJoin4_Butterfly_Fiss_10673_10694_split[0]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_10673_10694_join[0], u_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_10673_10694_join[0], u_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_10673_10694_join[0], t_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_10673_10694_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_10606(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin4_Butterfly_Fiss_10673_10694_split[1]) ; 
		u_im = pop_float(&SplitJoin4_Butterfly_Fiss_10673_10694_split[1]) ; 
		t_re = pop_float(&SplitJoin4_Butterfly_Fiss_10673_10694_split[1]) ; 
		t_im = pop_float(&SplitJoin4_Butterfly_Fiss_10673_10694_split[1]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_10673_10694_join[1], u_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_10673_10694_join[1], u_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_10673_10694_join[1], t_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_10673_10694_join[1], t_im) ; 
	}
	ENDFOR
}

void Butterfly_10607(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin4_Butterfly_Fiss_10673_10694_split[2]) ; 
		u_im = pop_float(&SplitJoin4_Butterfly_Fiss_10673_10694_split[2]) ; 
		t_re = pop_float(&SplitJoin4_Butterfly_Fiss_10673_10694_split[2]) ; 
		t_im = pop_float(&SplitJoin4_Butterfly_Fiss_10673_10694_split[2]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_10673_10694_join[2], u_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_10673_10694_join[2], u_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_10673_10694_join[2], t_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_10673_10694_join[2], t_im) ; 
	}
	ENDFOR
}

void Butterfly_10608(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin4_Butterfly_Fiss_10673_10694_split[3]) ; 
		u_im = pop_float(&SplitJoin4_Butterfly_Fiss_10673_10694_split[3]) ; 
		t_re = pop_float(&SplitJoin4_Butterfly_Fiss_10673_10694_split[3]) ; 
		t_im = pop_float(&SplitJoin4_Butterfly_Fiss_10673_10694_split[3]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_10673_10694_join[3], u_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_10673_10694_join[3], u_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_10673_10694_join[3], t_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_10673_10694_join[3], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10603() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin4_Butterfly_Fiss_10673_10694_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_10458WEIGHTED_ROUND_ROBIN_Splitter_10603));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10604() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10604Post_CollapsedDataParallel_2_10459, pop_float(&SplitJoin4_Butterfly_Fiss_10673_10694_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_10459() {
 {
	int kTimesWeights_i = 0;
 {
	FOR(int, _k, 0,  < , 2, _k++) {
		int partialSum_i = 0;
 {
		FOR(int, _i, 0,  < , 8, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&Post_CollapsedDataParallel_2_10459WEIGHTED_ROUND_ROBIN_Splitter_10579, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_10604Post_CollapsedDataParallel_2_10459, (kTimesWeights_i + (partialSum_i + _j)))) ; 
			}
			ENDFOR
		}
			partialSum_i = (partialSum_i + 4) ; 
		}
		ENDFOR
	}
		kTimesWeights_i = (kTimesWeights_i + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10604Post_CollapsedDataParallel_2_10459) ; 
}


void Pre_CollapsedDataParallel_1_10464() {
 {
	int partialSum_k = 0;
	partialSum_k = 0 ; 
 {
	FOR(int, _k, 0,  < , 4, _k++) {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
		iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
		FOR(int, _i, 0,  < , 2, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&Pre_CollapsedDataParallel_1_10464WEIGHTED_ROUND_ROBIN_Splitter_10609, peek_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_child0_10548_10695_split[0], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
			}
			ENDFOR
		}
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
		}
		ENDFOR
	}
		partialSum_k = (partialSum_k + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_child0_10548_10695_split[0]) ; 
}


void Butterfly_10611() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = 0.0 ; 
	u_im = 0.0 ; 
	t_re = 0.0 ; 
	t_im = 0.0 ; 
	wt_re = 0.0 ; 
	wt_im = 0.0 ; 
	u_re = pop_float(&SplitJoin8_Butterfly_Fiss_10674_10696_split[0]) ; 
	u_im = pop_float(&SplitJoin8_Butterfly_Fiss_10674_10696_split[0]) ; 
	t_re = pop_float(&SplitJoin8_Butterfly_Fiss_10674_10696_split[0]) ; 
	t_im = pop_float(&SplitJoin8_Butterfly_Fiss_10674_10696_split[0]) ; 
	wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
	wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin8_Butterfly_Fiss_10674_10696_join[0], u_re) ; 
	push_float(&SplitJoin8_Butterfly_Fiss_10674_10696_join[0], u_im) ; 
	push_float(&SplitJoin8_Butterfly_Fiss_10674_10696_join[0], t_re) ; 
	push_float(&SplitJoin8_Butterfly_Fiss_10674_10696_join[0], t_im) ; 
}


void Butterfly_10612() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = 0.0 ; 
	u_im = 0.0 ; 
	t_re = 0.0 ; 
	t_im = 0.0 ; 
	wt_re = 0.0 ; 
	wt_im = 0.0 ; 
	u_re = pop_float(&SplitJoin8_Butterfly_Fiss_10674_10696_split[1]) ; 
	u_im = pop_float(&SplitJoin8_Butterfly_Fiss_10674_10696_split[1]) ; 
	t_re = pop_float(&SplitJoin8_Butterfly_Fiss_10674_10696_split[1]) ; 
	t_im = pop_float(&SplitJoin8_Butterfly_Fiss_10674_10696_split[1]) ; 
	wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
	wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin8_Butterfly_Fiss_10674_10696_join[1], u_re) ; 
	push_float(&SplitJoin8_Butterfly_Fiss_10674_10696_join[1], u_im) ; 
	push_float(&SplitJoin8_Butterfly_Fiss_10674_10696_join[1], t_re) ; 
	push_float(&SplitJoin8_Butterfly_Fiss_10674_10696_join[1], t_im) ; 
}


void Butterfly_10613() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = 0.0 ; 
	u_im = 0.0 ; 
	t_re = 0.0 ; 
	t_im = 0.0 ; 
	wt_re = 0.0 ; 
	wt_im = 0.0 ; 
	u_re = pop_float(&SplitJoin8_Butterfly_Fiss_10674_10696_split[2]) ; 
	u_im = pop_float(&SplitJoin8_Butterfly_Fiss_10674_10696_split[2]) ; 
	t_re = pop_float(&SplitJoin8_Butterfly_Fiss_10674_10696_split[2]) ; 
	t_im = pop_float(&SplitJoin8_Butterfly_Fiss_10674_10696_split[2]) ; 
	wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
	wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin8_Butterfly_Fiss_10674_10696_join[2], u_re) ; 
	push_float(&SplitJoin8_Butterfly_Fiss_10674_10696_join[2], u_im) ; 
	push_float(&SplitJoin8_Butterfly_Fiss_10674_10696_join[2], t_re) ; 
	push_float(&SplitJoin8_Butterfly_Fiss_10674_10696_join[2], t_im) ; 
}


void Butterfly_10614() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = 0.0 ; 
	u_im = 0.0 ; 
	t_re = 0.0 ; 
	t_im = 0.0 ; 
	wt_re = 0.0 ; 
	wt_im = 0.0 ; 
	u_re = pop_float(&SplitJoin8_Butterfly_Fiss_10674_10696_split[3]) ; 
	u_im = pop_float(&SplitJoin8_Butterfly_Fiss_10674_10696_split[3]) ; 
	t_re = pop_float(&SplitJoin8_Butterfly_Fiss_10674_10696_split[3]) ; 
	t_im = pop_float(&SplitJoin8_Butterfly_Fiss_10674_10696_split[3]) ; 
	wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
	wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin8_Butterfly_Fiss_10674_10696_join[3], u_re) ; 
	push_float(&SplitJoin8_Butterfly_Fiss_10674_10696_join[3], u_im) ; 
	push_float(&SplitJoin8_Butterfly_Fiss_10674_10696_join[3], t_re) ; 
	push_float(&SplitJoin8_Butterfly_Fiss_10674_10696_join[3], t_im) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_10609() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_float(&SplitJoin8_Butterfly_Fiss_10674_10696_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_10464WEIGHTED_ROUND_ROBIN_Splitter_10609));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10610() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10610Post_CollapsedDataParallel_2_10465, pop_float(&SplitJoin8_Butterfly_Fiss_10674_10696_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void Post_CollapsedDataParallel_2_10465() {
 {
	int kTimesWeights_i = 0;
	kTimesWeights_i = 0 ; 
 {
	FOR(int, _k, 0,  < , 2, _k++) {
		int partialSum_i = 0;
		partialSum_i = 0 ; 
 {
		FOR(int, _i, 0,  < , 4, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_child0_10548_10695_join[0], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_10610Post_CollapsedDataParallel_2_10465, (kTimesWeights_i + (partialSum_i + _j)))) ; 
			}
			ENDFOR
		}
			partialSum_i = (partialSum_i + 4) ; 
		}
		ENDFOR
	}
		kTimesWeights_i = (kTimesWeights_i + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10610Post_CollapsedDataParallel_2_10465) ; 
}


void Pre_CollapsedDataParallel_1_10467() {
 {
	int partialSum_k = 0;
	partialSum_k = 0 ; 
 {
	FOR(int, _k, 0,  < , 4, _k++) {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
		iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
		FOR(int, _i, 0,  < , 2, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&Pre_CollapsedDataParallel_1_10467WEIGHTED_ROUND_ROBIN_Splitter_10615, peek_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_child0_10548_10695_split[1], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
			}
			ENDFOR
		}
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
		}
		ENDFOR
	}
		partialSum_k = (partialSum_k + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_child0_10548_10695_split[1]) ; 
}


void Butterfly_10617() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = 0.0 ; 
	u_im = 0.0 ; 
	t_re = 0.0 ; 
	t_im = 0.0 ; 
	wt_re = 0.0 ; 
	wt_im = 0.0 ; 
	u_re = pop_float(&SplitJoin72_Butterfly_Fiss_10687_10697_split[0]) ; 
	u_im = pop_float(&SplitJoin72_Butterfly_Fiss_10687_10697_split[0]) ; 
	t_re = pop_float(&SplitJoin72_Butterfly_Fiss_10687_10697_split[0]) ; 
	t_im = pop_float(&SplitJoin72_Butterfly_Fiss_10687_10697_split[0]) ; 
	wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
	wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin72_Butterfly_Fiss_10687_10697_join[0], u_re) ; 
	push_float(&SplitJoin72_Butterfly_Fiss_10687_10697_join[0], u_im) ; 
	push_float(&SplitJoin72_Butterfly_Fiss_10687_10697_join[0], t_re) ; 
	push_float(&SplitJoin72_Butterfly_Fiss_10687_10697_join[0], t_im) ; 
}


void Butterfly_10618() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = 0.0 ; 
	u_im = 0.0 ; 
	t_re = 0.0 ; 
	t_im = 0.0 ; 
	wt_re = 0.0 ; 
	wt_im = 0.0 ; 
	u_re = pop_float(&SplitJoin72_Butterfly_Fiss_10687_10697_split[1]) ; 
	u_im = pop_float(&SplitJoin72_Butterfly_Fiss_10687_10697_split[1]) ; 
	t_re = pop_float(&SplitJoin72_Butterfly_Fiss_10687_10697_split[1]) ; 
	t_im = pop_float(&SplitJoin72_Butterfly_Fiss_10687_10697_split[1]) ; 
	wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
	wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin72_Butterfly_Fiss_10687_10697_join[1], u_re) ; 
	push_float(&SplitJoin72_Butterfly_Fiss_10687_10697_join[1], u_im) ; 
	push_float(&SplitJoin72_Butterfly_Fiss_10687_10697_join[1], t_re) ; 
	push_float(&SplitJoin72_Butterfly_Fiss_10687_10697_join[1], t_im) ; 
}


void Butterfly_10619() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = 0.0 ; 
	u_im = 0.0 ; 
	t_re = 0.0 ; 
	t_im = 0.0 ; 
	wt_re = 0.0 ; 
	wt_im = 0.0 ; 
	u_re = pop_float(&SplitJoin72_Butterfly_Fiss_10687_10697_split[2]) ; 
	u_im = pop_float(&SplitJoin72_Butterfly_Fiss_10687_10697_split[2]) ; 
	t_re = pop_float(&SplitJoin72_Butterfly_Fiss_10687_10697_split[2]) ; 
	t_im = pop_float(&SplitJoin72_Butterfly_Fiss_10687_10697_split[2]) ; 
	wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
	wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin72_Butterfly_Fiss_10687_10697_join[2], u_re) ; 
	push_float(&SplitJoin72_Butterfly_Fiss_10687_10697_join[2], u_im) ; 
	push_float(&SplitJoin72_Butterfly_Fiss_10687_10697_join[2], t_re) ; 
	push_float(&SplitJoin72_Butterfly_Fiss_10687_10697_join[2], t_im) ; 
}


void Butterfly_10620() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = 0.0 ; 
	u_im = 0.0 ; 
	t_re = 0.0 ; 
	t_im = 0.0 ; 
	wt_re = 0.0 ; 
	wt_im = 0.0 ; 
	u_re = pop_float(&SplitJoin72_Butterfly_Fiss_10687_10697_split[3]) ; 
	u_im = pop_float(&SplitJoin72_Butterfly_Fiss_10687_10697_split[3]) ; 
	t_re = pop_float(&SplitJoin72_Butterfly_Fiss_10687_10697_split[3]) ; 
	t_im = pop_float(&SplitJoin72_Butterfly_Fiss_10687_10697_split[3]) ; 
	wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
	wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin72_Butterfly_Fiss_10687_10697_join[3], u_re) ; 
	push_float(&SplitJoin72_Butterfly_Fiss_10687_10697_join[3], u_im) ; 
	push_float(&SplitJoin72_Butterfly_Fiss_10687_10697_join[3], t_re) ; 
	push_float(&SplitJoin72_Butterfly_Fiss_10687_10697_join[3], t_im) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_10615() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_float(&SplitJoin72_Butterfly_Fiss_10687_10697_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_10467WEIGHTED_ROUND_ROBIN_Splitter_10615));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10616() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10616Post_CollapsedDataParallel_2_10468, pop_float(&SplitJoin72_Butterfly_Fiss_10687_10697_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void Post_CollapsedDataParallel_2_10468() {
 {
	int kTimesWeights_i = 0;
	kTimesWeights_i = 0 ; 
 {
	FOR(int, _k, 0,  < , 2, _k++) {
		int partialSum_i = 0;
		partialSum_i = 0 ; 
 {
		FOR(int, _i, 0,  < , 4, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_child0_10548_10695_join[1], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_10616Post_CollapsedDataParallel_2_10468, (kTimesWeights_i + (partialSum_i + _j)))) ; 
			}
			ENDFOR
		}
			partialSum_i = (partialSum_i + 4) ; 
		}
		ENDFOR
	}
		kTimesWeights_i = (kTimesWeights_i + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10616Post_CollapsedDataParallel_2_10468) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_10579() {
	FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
		push_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_child0_10548_10695_split[0], pop_float(&Post_CollapsedDataParallel_2_10459WEIGHTED_ROUND_ROBIN_Splitter_10579));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
		push_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_child0_10548_10695_split[1], pop_float(&Post_CollapsedDataParallel_2_10459WEIGHTED_ROUND_ROBIN_Splitter_10579));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10580() {
	FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
		push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_10112_10507_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_Hier_10672_10693_join[0], pop_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_child0_10548_10695_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
		push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_10112_10507_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_Hier_10672_10693_join[0], pop_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_child0_10548_10695_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_10461() {
 {
	int partialSum_k = 0;
 {
	FOR(int, _k, 0,  < , 8, _k++) {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
		iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
		FOR(int, _i, 0,  < , 2, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&Pre_CollapsedDataParallel_1_10461WEIGHTED_ROUND_ROBIN_Splitter_10621, peek_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_10112_10507_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_Hier_10672_10693_split[1], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
			}
			ENDFOR
		}
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 16) ; 
		}
		ENDFOR
	}
		partialSum_k = (partialSum_k + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_10112_10507_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_Hier_10672_10693_split[1]) ; 
}


void Butterfly_10623(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin81_Butterfly_Fiss_10688_10698_split[0]) ; 
		u_im = pop_float(&SplitJoin81_Butterfly_Fiss_10688_10698_split[0]) ; 
		t_re = pop_float(&SplitJoin81_Butterfly_Fiss_10688_10698_split[0]) ; 
		t_im = pop_float(&SplitJoin81_Butterfly_Fiss_10688_10698_split[0]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin81_Butterfly_Fiss_10688_10698_join[0], u_re) ; 
		push_float(&SplitJoin81_Butterfly_Fiss_10688_10698_join[0], u_im) ; 
		push_float(&SplitJoin81_Butterfly_Fiss_10688_10698_join[0], t_re) ; 
		push_float(&SplitJoin81_Butterfly_Fiss_10688_10698_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_10624(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin81_Butterfly_Fiss_10688_10698_split[1]) ; 
		u_im = pop_float(&SplitJoin81_Butterfly_Fiss_10688_10698_split[1]) ; 
		t_re = pop_float(&SplitJoin81_Butterfly_Fiss_10688_10698_split[1]) ; 
		t_im = pop_float(&SplitJoin81_Butterfly_Fiss_10688_10698_split[1]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin81_Butterfly_Fiss_10688_10698_join[1], u_re) ; 
		push_float(&SplitJoin81_Butterfly_Fiss_10688_10698_join[1], u_im) ; 
		push_float(&SplitJoin81_Butterfly_Fiss_10688_10698_join[1], t_re) ; 
		push_float(&SplitJoin81_Butterfly_Fiss_10688_10698_join[1], t_im) ; 
	}
	ENDFOR
}

void Butterfly_10625(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin81_Butterfly_Fiss_10688_10698_split[2]) ; 
		u_im = pop_float(&SplitJoin81_Butterfly_Fiss_10688_10698_split[2]) ; 
		t_re = pop_float(&SplitJoin81_Butterfly_Fiss_10688_10698_split[2]) ; 
		t_im = pop_float(&SplitJoin81_Butterfly_Fiss_10688_10698_split[2]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin81_Butterfly_Fiss_10688_10698_join[2], u_re) ; 
		push_float(&SplitJoin81_Butterfly_Fiss_10688_10698_join[2], u_im) ; 
		push_float(&SplitJoin81_Butterfly_Fiss_10688_10698_join[2], t_re) ; 
		push_float(&SplitJoin81_Butterfly_Fiss_10688_10698_join[2], t_im) ; 
	}
	ENDFOR
}

void Butterfly_10626(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin81_Butterfly_Fiss_10688_10698_split[3]) ; 
		u_im = pop_float(&SplitJoin81_Butterfly_Fiss_10688_10698_split[3]) ; 
		t_re = pop_float(&SplitJoin81_Butterfly_Fiss_10688_10698_split[3]) ; 
		t_im = pop_float(&SplitJoin81_Butterfly_Fiss_10688_10698_split[3]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin81_Butterfly_Fiss_10688_10698_join[3], u_re) ; 
		push_float(&SplitJoin81_Butterfly_Fiss_10688_10698_join[3], u_im) ; 
		push_float(&SplitJoin81_Butterfly_Fiss_10688_10698_join[3], t_re) ; 
		push_float(&SplitJoin81_Butterfly_Fiss_10688_10698_join[3], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10621() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin81_Butterfly_Fiss_10688_10698_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_10461WEIGHTED_ROUND_ROBIN_Splitter_10621));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_10622() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10622Post_CollapsedDataParallel_2_10462, pop_float(&SplitJoin81_Butterfly_Fiss_10688_10698_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_10462() {
 {
	int kTimesWeights_i = 0;
 {
	FOR(int, _k, 0,  < , 2, _k++) {
		int partialSum_i = 0;
 {
		FOR(int, _i, 0,  < , 8, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&Post_CollapsedDataParallel_2_10462WEIGHTED_ROUND_ROBIN_Splitter_10581, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_10622Post_CollapsedDataParallel_2_10462, (kTimesWeights_i + (partialSum_i + _j)))) ; 
			}
			ENDFOR
		}
			partialSum_i = (partialSum_i + 4) ; 
		}
		ENDFOR
	}
		kTimesWeights_i = (kTimesWeights_i + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10622Post_CollapsedDataParallel_2_10462) ; 
}


void Pre_CollapsedDataParallel_1_10470() {
 {
	int partialSum_k = 0;
	partialSum_k = 0 ; 
 {
	FOR(int, _k, 0,  < , 4, _k++) {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
		iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
		FOR(int, _i, 0,  < , 2, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&Pre_CollapsedDataParallel_1_10470WEIGHTED_ROUND_ROBIN_Splitter_10627, peek_float(&SplitJoin83_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_child1_10553_10699_split[0], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
			}
			ENDFOR
		}
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
		}
		ENDFOR
	}
		partialSum_k = (partialSum_k + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&SplitJoin83_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_child1_10553_10699_split[0]) ; 
}


void Butterfly_10629() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = 0.0 ; 
	u_im = 0.0 ; 
	t_re = 0.0 ; 
	t_im = 0.0 ; 
	wt_re = 0.0 ; 
	wt_im = 0.0 ; 
	u_re = pop_float(&SplitJoin85_Butterfly_Fiss_10689_10700_split[0]) ; 
	u_im = pop_float(&SplitJoin85_Butterfly_Fiss_10689_10700_split[0]) ; 
	t_re = pop_float(&SplitJoin85_Butterfly_Fiss_10689_10700_split[0]) ; 
	t_im = pop_float(&SplitJoin85_Butterfly_Fiss_10689_10700_split[0]) ; 
	wt_re = ((0.70710677 * t_re) - (0.70710677 * t_im)) ; 
	wt_im = ((0.70710677 * t_im) + (0.70710677 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin85_Butterfly_Fiss_10689_10700_join[0], u_re) ; 
	push_float(&SplitJoin85_Butterfly_Fiss_10689_10700_join[0], u_im) ; 
	push_float(&SplitJoin85_Butterfly_Fiss_10689_10700_join[0], t_re) ; 
	push_float(&SplitJoin85_Butterfly_Fiss_10689_10700_join[0], t_im) ; 
}


void Butterfly_10630() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = 0.0 ; 
	u_im = 0.0 ; 
	t_re = 0.0 ; 
	t_im = 0.0 ; 
	wt_re = 0.0 ; 
	wt_im = 0.0 ; 
	u_re = pop_float(&SplitJoin85_Butterfly_Fiss_10689_10700_split[1]) ; 
	u_im = pop_float(&SplitJoin85_Butterfly_Fiss_10689_10700_split[1]) ; 
	t_re = pop_float(&SplitJoin85_Butterfly_Fiss_10689_10700_split[1]) ; 
	t_im = pop_float(&SplitJoin85_Butterfly_Fiss_10689_10700_split[1]) ; 
	wt_re = ((0.70710677 * t_re) - (0.70710677 * t_im)) ; 
	wt_im = ((0.70710677 * t_im) + (0.70710677 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin85_Butterfly_Fiss_10689_10700_join[1], u_re) ; 
	push_float(&SplitJoin85_Butterfly_Fiss_10689_10700_join[1], u_im) ; 
	push_float(&SplitJoin85_Butterfly_Fiss_10689_10700_join[1], t_re) ; 
	push_float(&SplitJoin85_Butterfly_Fiss_10689_10700_join[1], t_im) ; 
}


void Butterfly_10631() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = 0.0 ; 
	u_im = 0.0 ; 
	t_re = 0.0 ; 
	t_im = 0.0 ; 
	wt_re = 0.0 ; 
	wt_im = 0.0 ; 
	u_re = pop_float(&SplitJoin85_Butterfly_Fiss_10689_10700_split[2]) ; 
	u_im = pop_float(&SplitJoin85_Butterfly_Fiss_10689_10700_split[2]) ; 
	t_re = pop_float(&SplitJoin85_Butterfly_Fiss_10689_10700_split[2]) ; 
	t_im = pop_float(&SplitJoin85_Butterfly_Fiss_10689_10700_split[2]) ; 
	wt_re = ((0.70710677 * t_re) - (0.70710677 * t_im)) ; 
	wt_im = ((0.70710677 * t_im) + (0.70710677 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin85_Butterfly_Fiss_10689_10700_join[2], u_re) ; 
	push_float(&SplitJoin85_Butterfly_Fiss_10689_10700_join[2], u_im) ; 
	push_float(&SplitJoin85_Butterfly_Fiss_10689_10700_join[2], t_re) ; 
	push_float(&SplitJoin85_Butterfly_Fiss_10689_10700_join[2], t_im) ; 
}


void Butterfly_10632() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = 0.0 ; 
	u_im = 0.0 ; 
	t_re = 0.0 ; 
	t_im = 0.0 ; 
	wt_re = 0.0 ; 
	wt_im = 0.0 ; 
	u_re = pop_float(&SplitJoin85_Butterfly_Fiss_10689_10700_split[3]) ; 
	u_im = pop_float(&SplitJoin85_Butterfly_Fiss_10689_10700_split[3]) ; 
	t_re = pop_float(&SplitJoin85_Butterfly_Fiss_10689_10700_split[3]) ; 
	t_im = pop_float(&SplitJoin85_Butterfly_Fiss_10689_10700_split[3]) ; 
	wt_re = ((0.70710677 * t_re) - (0.70710677 * t_im)) ; 
	wt_im = ((0.70710677 * t_im) + (0.70710677 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin85_Butterfly_Fiss_10689_10700_join[3], u_re) ; 
	push_float(&SplitJoin85_Butterfly_Fiss_10689_10700_join[3], u_im) ; 
	push_float(&SplitJoin85_Butterfly_Fiss_10689_10700_join[3], t_re) ; 
	push_float(&SplitJoin85_Butterfly_Fiss_10689_10700_join[3], t_im) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_10627() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_float(&SplitJoin85_Butterfly_Fiss_10689_10700_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_10470WEIGHTED_ROUND_ROBIN_Splitter_10627));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10628() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10628Post_CollapsedDataParallel_2_10471, pop_float(&SplitJoin85_Butterfly_Fiss_10689_10700_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void Post_CollapsedDataParallel_2_10471() {
 {
	int kTimesWeights_i = 0;
	kTimesWeights_i = 0 ; 
 {
	FOR(int, _k, 0,  < , 2, _k++) {
		int partialSum_i = 0;
		partialSum_i = 0 ; 
 {
		FOR(int, _i, 0,  < , 4, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&SplitJoin83_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_child1_10553_10699_join[0], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_10628Post_CollapsedDataParallel_2_10471, (kTimesWeights_i + (partialSum_i + _j)))) ; 
			}
			ENDFOR
		}
			partialSum_i = (partialSum_i + 4) ; 
		}
		ENDFOR
	}
		kTimesWeights_i = (kTimesWeights_i + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10628Post_CollapsedDataParallel_2_10471) ; 
}


void Pre_CollapsedDataParallel_1_10473() {
 {
	int partialSum_k = 0;
	partialSum_k = 0 ; 
 {
	FOR(int, _k, 0,  < , 4, _k++) {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
		iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
		FOR(int, _i, 0,  < , 2, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&Pre_CollapsedDataParallel_1_10473WEIGHTED_ROUND_ROBIN_Splitter_10633, peek_float(&SplitJoin83_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_child1_10553_10699_split[1], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
			}
			ENDFOR
		}
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
		}
		ENDFOR
	}
		partialSum_k = (partialSum_k + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&SplitJoin83_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_child1_10553_10699_split[1]) ; 
}


void Butterfly_10635() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = 0.0 ; 
	u_im = 0.0 ; 
	t_re = 0.0 ; 
	t_im = 0.0 ; 
	wt_re = 0.0 ; 
	wt_im = 0.0 ; 
	u_re = pop_float(&SplitJoin91_Butterfly_Fiss_10690_10701_split[0]) ; 
	u_im = pop_float(&SplitJoin91_Butterfly_Fiss_10690_10701_split[0]) ; 
	t_re = pop_float(&SplitJoin91_Butterfly_Fiss_10690_10701_split[0]) ; 
	t_im = pop_float(&SplitJoin91_Butterfly_Fiss_10690_10701_split[0]) ; 
	wt_re = ((-0.70710677 * t_re) - (0.70710677 * t_im)) ; 
	wt_im = ((-0.70710677 * t_im) + (0.70710677 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin91_Butterfly_Fiss_10690_10701_join[0], u_re) ; 
	push_float(&SplitJoin91_Butterfly_Fiss_10690_10701_join[0], u_im) ; 
	push_float(&SplitJoin91_Butterfly_Fiss_10690_10701_join[0], t_re) ; 
	push_float(&SplitJoin91_Butterfly_Fiss_10690_10701_join[0], t_im) ; 
}


void Butterfly_10636() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = 0.0 ; 
	u_im = 0.0 ; 
	t_re = 0.0 ; 
	t_im = 0.0 ; 
	wt_re = 0.0 ; 
	wt_im = 0.0 ; 
	u_re = pop_float(&SplitJoin91_Butterfly_Fiss_10690_10701_split[1]) ; 
	u_im = pop_float(&SplitJoin91_Butterfly_Fiss_10690_10701_split[1]) ; 
	t_re = pop_float(&SplitJoin91_Butterfly_Fiss_10690_10701_split[1]) ; 
	t_im = pop_float(&SplitJoin91_Butterfly_Fiss_10690_10701_split[1]) ; 
	wt_re = ((-0.70710677 * t_re) - (0.70710677 * t_im)) ; 
	wt_im = ((-0.70710677 * t_im) + (0.70710677 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin91_Butterfly_Fiss_10690_10701_join[1], u_re) ; 
	push_float(&SplitJoin91_Butterfly_Fiss_10690_10701_join[1], u_im) ; 
	push_float(&SplitJoin91_Butterfly_Fiss_10690_10701_join[1], t_re) ; 
	push_float(&SplitJoin91_Butterfly_Fiss_10690_10701_join[1], t_im) ; 
}


void Butterfly_10637() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = 0.0 ; 
	u_im = 0.0 ; 
	t_re = 0.0 ; 
	t_im = 0.0 ; 
	wt_re = 0.0 ; 
	wt_im = 0.0 ; 
	u_re = pop_float(&SplitJoin91_Butterfly_Fiss_10690_10701_split[2]) ; 
	u_im = pop_float(&SplitJoin91_Butterfly_Fiss_10690_10701_split[2]) ; 
	t_re = pop_float(&SplitJoin91_Butterfly_Fiss_10690_10701_split[2]) ; 
	t_im = pop_float(&SplitJoin91_Butterfly_Fiss_10690_10701_split[2]) ; 
	wt_re = ((-0.70710677 * t_re) - (0.70710677 * t_im)) ; 
	wt_im = ((-0.70710677 * t_im) + (0.70710677 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin91_Butterfly_Fiss_10690_10701_join[2], u_re) ; 
	push_float(&SplitJoin91_Butterfly_Fiss_10690_10701_join[2], u_im) ; 
	push_float(&SplitJoin91_Butterfly_Fiss_10690_10701_join[2], t_re) ; 
	push_float(&SplitJoin91_Butterfly_Fiss_10690_10701_join[2], t_im) ; 
}


void Butterfly_10638() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = 0.0 ; 
	u_im = 0.0 ; 
	t_re = 0.0 ; 
	t_im = 0.0 ; 
	wt_re = 0.0 ; 
	wt_im = 0.0 ; 
	u_re = pop_float(&SplitJoin91_Butterfly_Fiss_10690_10701_split[3]) ; 
	u_im = pop_float(&SplitJoin91_Butterfly_Fiss_10690_10701_split[3]) ; 
	t_re = pop_float(&SplitJoin91_Butterfly_Fiss_10690_10701_split[3]) ; 
	t_im = pop_float(&SplitJoin91_Butterfly_Fiss_10690_10701_split[3]) ; 
	wt_re = ((-0.70710677 * t_re) - (0.70710677 * t_im)) ; 
	wt_im = ((-0.70710677 * t_im) + (0.70710677 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin91_Butterfly_Fiss_10690_10701_join[3], u_re) ; 
	push_float(&SplitJoin91_Butterfly_Fiss_10690_10701_join[3], u_im) ; 
	push_float(&SplitJoin91_Butterfly_Fiss_10690_10701_join[3], t_re) ; 
	push_float(&SplitJoin91_Butterfly_Fiss_10690_10701_join[3], t_im) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_10633() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_float(&SplitJoin91_Butterfly_Fiss_10690_10701_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_10473WEIGHTED_ROUND_ROBIN_Splitter_10633));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10634() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10634Post_CollapsedDataParallel_2_10474, pop_float(&SplitJoin91_Butterfly_Fiss_10690_10701_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void Post_CollapsedDataParallel_2_10474() {
 {
	int kTimesWeights_i = 0;
	kTimesWeights_i = 0 ; 
 {
	FOR(int, _k, 0,  < , 2, _k++) {
		int partialSum_i = 0;
		partialSum_i = 0 ; 
 {
		FOR(int, _i, 0,  < , 4, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&SplitJoin83_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_child1_10553_10699_join[1], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_10634Post_CollapsedDataParallel_2_10474, (kTimesWeights_i + (partialSum_i + _j)))) ; 
			}
			ENDFOR
		}
			partialSum_i = (partialSum_i + 4) ; 
		}
		ENDFOR
	}
		kTimesWeights_i = (kTimesWeights_i + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10634Post_CollapsedDataParallel_2_10474) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_10581() {
	FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
		push_float(&SplitJoin83_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_child1_10553_10699_split[0], pop_float(&Post_CollapsedDataParallel_2_10462WEIGHTED_ROUND_ROBIN_Splitter_10581));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
		push_float(&SplitJoin83_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_child1_10553_10699_split[1], pop_float(&Post_CollapsedDataParallel_2_10462WEIGHTED_ROUND_ROBIN_Splitter_10581));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10582() {
	FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
		push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_10112_10507_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_Hier_10672_10693_join[1], pop_float(&SplitJoin83_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_child1_10553_10699_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
		push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_10112_10507_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_Hier_10672_10693_join[1], pop_float(&SplitJoin83_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_child1_10553_10699_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10499() {
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_10112_10507_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_Hier_10672_10693_split[0], pop_float(&Post_CollapsedDataParallel_2_10456WEIGHTED_ROUND_ROBIN_Splitter_10499));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_10112_10507_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_Hier_10672_10693_split[1], pop_float(&Post_CollapsedDataParallel_2_10456WEIGHTED_ROUND_ROBIN_Splitter_10499));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10583() {
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10583WEIGHTED_ROUND_ROBIN_Splitter_10584, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_10112_10507_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_Hier_10672_10693_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10583WEIGHTED_ROUND_ROBIN_Splitter_10584, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_10112_10507_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_Hier_10672_10693_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_10476() {
 {
	int partialSum_k = 0;
 {
	FOR(int, _k, 0,  < , 2, _k++) {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
		iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
		FOR(int, _i, 0,  < , 2, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&Pre_CollapsedDataParallel_1_10476WEIGHTED_ROUND_ROBIN_Splitter_10639, peek_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child0_10577_10703_split[0], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
			}
			ENDFOR
		}
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 4) ; 
		}
		ENDFOR
	}
		partialSum_k = (partialSum_k + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child0_10577_10703_split[0]) ; 
}


void Butterfly_10641() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin14_Butterfly_Fiss_10676_10704_split[0]) ; 
	u_im = pop_float(&SplitJoin14_Butterfly_Fiss_10676_10704_split[0]) ; 
	t_re = pop_float(&SplitJoin14_Butterfly_Fiss_10676_10704_split[0]) ; 
	t_im = pop_float(&SplitJoin14_Butterfly_Fiss_10676_10704_split[0]) ; 
	wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
	wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin14_Butterfly_Fiss_10676_10704_join[0], u_re) ; 
	push_float(&SplitJoin14_Butterfly_Fiss_10676_10704_join[0], u_im) ; 
	push_float(&SplitJoin14_Butterfly_Fiss_10676_10704_join[0], t_re) ; 
	push_float(&SplitJoin14_Butterfly_Fiss_10676_10704_join[0], t_im) ; 
}


void Butterfly_10642() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin14_Butterfly_Fiss_10676_10704_split[1]) ; 
	u_im = pop_float(&SplitJoin14_Butterfly_Fiss_10676_10704_split[1]) ; 
	t_re = pop_float(&SplitJoin14_Butterfly_Fiss_10676_10704_split[1]) ; 
	t_im = pop_float(&SplitJoin14_Butterfly_Fiss_10676_10704_split[1]) ; 
	wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
	wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin14_Butterfly_Fiss_10676_10704_join[1], u_re) ; 
	push_float(&SplitJoin14_Butterfly_Fiss_10676_10704_join[1], u_im) ; 
	push_float(&SplitJoin14_Butterfly_Fiss_10676_10704_join[1], t_re) ; 
	push_float(&SplitJoin14_Butterfly_Fiss_10676_10704_join[1], t_im) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_10639() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin14_Butterfly_Fiss_10676_10704_split[0], pop_float(&Pre_CollapsedDataParallel_1_10476WEIGHTED_ROUND_ROBIN_Splitter_10639));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin14_Butterfly_Fiss_10676_10704_split[1], pop_float(&Pre_CollapsedDataParallel_1_10476WEIGHTED_ROUND_ROBIN_Splitter_10639));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10640() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10640Post_CollapsedDataParallel_2_10477, pop_float(&SplitJoin14_Butterfly_Fiss_10676_10704_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10640Post_CollapsedDataParallel_2_10477, pop_float(&SplitJoin14_Butterfly_Fiss_10676_10704_join[1]));
	ENDFOR
}

void Post_CollapsedDataParallel_2_10477() {
 {
	int kTimesWeights_i = 0;
 {
	FOR(int, _k, 0,  < , 2, _k++) {
		int partialSum_i = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child0_10577_10703_join[0], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_10640Post_CollapsedDataParallel_2_10477, (kTimesWeights_i + (partialSum_i + _j)))) ; 
			}
			ENDFOR
		}
			partialSum_i = (partialSum_i + 4) ; 
		}
		ENDFOR
	}
		kTimesWeights_i = (kTimesWeights_i + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10640Post_CollapsedDataParallel_2_10477) ; 
}


void Pre_CollapsedDataParallel_1_10479() {
 {
	int partialSum_k = 0;
 {
	FOR(int, _k, 0,  < , 2, _k++) {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
		iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
		FOR(int, _i, 0,  < , 2, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&Pre_CollapsedDataParallel_1_10479WEIGHTED_ROUND_ROBIN_Splitter_10643, peek_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child0_10577_10703_split[1], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
			}
			ENDFOR
		}
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 4) ; 
		}
		ENDFOR
	}
		partialSum_k = (partialSum_k + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child0_10577_10703_split[1]) ; 
}


void Butterfly_10645() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin39_Butterfly_Fiss_10680_10705_split[0]) ; 
	u_im = pop_float(&SplitJoin39_Butterfly_Fiss_10680_10705_split[0]) ; 
	t_re = pop_float(&SplitJoin39_Butterfly_Fiss_10680_10705_split[0]) ; 
	t_im = pop_float(&SplitJoin39_Butterfly_Fiss_10680_10705_split[0]) ; 
	wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
	wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin39_Butterfly_Fiss_10680_10705_join[0], u_re) ; 
	push_float(&SplitJoin39_Butterfly_Fiss_10680_10705_join[0], u_im) ; 
	push_float(&SplitJoin39_Butterfly_Fiss_10680_10705_join[0], t_re) ; 
	push_float(&SplitJoin39_Butterfly_Fiss_10680_10705_join[0], t_im) ; 
}


void Butterfly_10646() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin39_Butterfly_Fiss_10680_10705_split[1]) ; 
	u_im = pop_float(&SplitJoin39_Butterfly_Fiss_10680_10705_split[1]) ; 
	t_re = pop_float(&SplitJoin39_Butterfly_Fiss_10680_10705_split[1]) ; 
	t_im = pop_float(&SplitJoin39_Butterfly_Fiss_10680_10705_split[1]) ; 
	wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
	wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin39_Butterfly_Fiss_10680_10705_join[1], u_re) ; 
	push_float(&SplitJoin39_Butterfly_Fiss_10680_10705_join[1], u_im) ; 
	push_float(&SplitJoin39_Butterfly_Fiss_10680_10705_join[1], t_re) ; 
	push_float(&SplitJoin39_Butterfly_Fiss_10680_10705_join[1], t_im) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_10643() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin39_Butterfly_Fiss_10680_10705_split[0], pop_float(&Pre_CollapsedDataParallel_1_10479WEIGHTED_ROUND_ROBIN_Splitter_10643));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin39_Butterfly_Fiss_10680_10705_split[1], pop_float(&Pre_CollapsedDataParallel_1_10479WEIGHTED_ROUND_ROBIN_Splitter_10643));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10644() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10644Post_CollapsedDataParallel_2_10480, pop_float(&SplitJoin39_Butterfly_Fiss_10680_10705_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10644Post_CollapsedDataParallel_2_10480, pop_float(&SplitJoin39_Butterfly_Fiss_10680_10705_join[1]));
	ENDFOR
}

void Post_CollapsedDataParallel_2_10480() {
 {
	int kTimesWeights_i = 0;
 {
	FOR(int, _k, 0,  < , 2, _k++) {
		int partialSum_i = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child0_10577_10703_join[1], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_10644Post_CollapsedDataParallel_2_10480, (kTimesWeights_i + (partialSum_i + _j)))) ; 
			}
			ENDFOR
		}
			partialSum_i = (partialSum_i + 4) ; 
		}
		ENDFOR
	}
		kTimesWeights_i = (kTimesWeights_i + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10644Post_CollapsedDataParallel_2_10480) ; 
}


void Pre_CollapsedDataParallel_1_10482() {
 {
	int partialSum_k = 0;
 {
	FOR(int, _k, 0,  < , 2, _k++) {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
		iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
		FOR(int, _i, 0,  < , 2, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&Pre_CollapsedDataParallel_1_10482WEIGHTED_ROUND_ROBIN_Splitter_10647, peek_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child0_10577_10703_split[2], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
			}
			ENDFOR
		}
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 4) ; 
		}
		ENDFOR
	}
		partialSum_k = (partialSum_k + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child0_10577_10703_split[2]) ; 
}


void Butterfly_10649() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin43_Butterfly_Fiss_10681_10706_split[0]) ; 
	u_im = pop_float(&SplitJoin43_Butterfly_Fiss_10681_10706_split[0]) ; 
	t_re = pop_float(&SplitJoin43_Butterfly_Fiss_10681_10706_split[0]) ; 
	t_im = pop_float(&SplitJoin43_Butterfly_Fiss_10681_10706_split[0]) ; 
	wt_re = ((0.70710677 * t_re) - (0.70710677 * t_im)) ; 
	wt_im = ((0.70710677 * t_im) + (0.70710677 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin43_Butterfly_Fiss_10681_10706_join[0], u_re) ; 
	push_float(&SplitJoin43_Butterfly_Fiss_10681_10706_join[0], u_im) ; 
	push_float(&SplitJoin43_Butterfly_Fiss_10681_10706_join[0], t_re) ; 
	push_float(&SplitJoin43_Butterfly_Fiss_10681_10706_join[0], t_im) ; 
}


void Butterfly_10650() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin43_Butterfly_Fiss_10681_10706_split[1]) ; 
	u_im = pop_float(&SplitJoin43_Butterfly_Fiss_10681_10706_split[1]) ; 
	t_re = pop_float(&SplitJoin43_Butterfly_Fiss_10681_10706_split[1]) ; 
	t_im = pop_float(&SplitJoin43_Butterfly_Fiss_10681_10706_split[1]) ; 
	wt_re = ((0.70710677 * t_re) - (0.70710677 * t_im)) ; 
	wt_im = ((0.70710677 * t_im) + (0.70710677 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin43_Butterfly_Fiss_10681_10706_join[1], u_re) ; 
	push_float(&SplitJoin43_Butterfly_Fiss_10681_10706_join[1], u_im) ; 
	push_float(&SplitJoin43_Butterfly_Fiss_10681_10706_join[1], t_re) ; 
	push_float(&SplitJoin43_Butterfly_Fiss_10681_10706_join[1], t_im) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_10647() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin43_Butterfly_Fiss_10681_10706_split[0], pop_float(&Pre_CollapsedDataParallel_1_10482WEIGHTED_ROUND_ROBIN_Splitter_10647));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin43_Butterfly_Fiss_10681_10706_split[1], pop_float(&Pre_CollapsedDataParallel_1_10482WEIGHTED_ROUND_ROBIN_Splitter_10647));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10648() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10648Post_CollapsedDataParallel_2_10483, pop_float(&SplitJoin43_Butterfly_Fiss_10681_10706_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10648Post_CollapsedDataParallel_2_10483, pop_float(&SplitJoin43_Butterfly_Fiss_10681_10706_join[1]));
	ENDFOR
}

void Post_CollapsedDataParallel_2_10483() {
 {
	int kTimesWeights_i = 0;
 {
	FOR(int, _k, 0,  < , 2, _k++) {
		int partialSum_i = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child0_10577_10703_join[2], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_10648Post_CollapsedDataParallel_2_10483, (kTimesWeights_i + (partialSum_i + _j)))) ; 
			}
			ENDFOR
		}
			partialSum_i = (partialSum_i + 4) ; 
		}
		ENDFOR
	}
		kTimesWeights_i = (kTimesWeights_i + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10648Post_CollapsedDataParallel_2_10483) ; 
}


void Pre_CollapsedDataParallel_1_10485() {
 {
	int partialSum_k = 0;
 {
	FOR(int, _k, 0,  < , 2, _k++) {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
		iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
		FOR(int, _i, 0,  < , 2, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&Pre_CollapsedDataParallel_1_10485WEIGHTED_ROUND_ROBIN_Splitter_10651, peek_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child0_10577_10703_split[3], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
			}
			ENDFOR
		}
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 4) ; 
		}
		ENDFOR
	}
		partialSum_k = (partialSum_k + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child0_10577_10703_split[3]) ; 
}


void Butterfly_10653() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin47_Butterfly_Fiss_10682_10707_split[0]) ; 
	u_im = pop_float(&SplitJoin47_Butterfly_Fiss_10682_10707_split[0]) ; 
	t_re = pop_float(&SplitJoin47_Butterfly_Fiss_10682_10707_split[0]) ; 
	t_im = pop_float(&SplitJoin47_Butterfly_Fiss_10682_10707_split[0]) ; 
	wt_re = ((-0.70710677 * t_re) - (0.70710677 * t_im)) ; 
	wt_im = ((-0.70710677 * t_im) + (0.70710677 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin47_Butterfly_Fiss_10682_10707_join[0], u_re) ; 
	push_float(&SplitJoin47_Butterfly_Fiss_10682_10707_join[0], u_im) ; 
	push_float(&SplitJoin47_Butterfly_Fiss_10682_10707_join[0], t_re) ; 
	push_float(&SplitJoin47_Butterfly_Fiss_10682_10707_join[0], t_im) ; 
}


void Butterfly_10654() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin47_Butterfly_Fiss_10682_10707_split[1]) ; 
	u_im = pop_float(&SplitJoin47_Butterfly_Fiss_10682_10707_split[1]) ; 
	t_re = pop_float(&SplitJoin47_Butterfly_Fiss_10682_10707_split[1]) ; 
	t_im = pop_float(&SplitJoin47_Butterfly_Fiss_10682_10707_split[1]) ; 
	wt_re = ((-0.70710677 * t_re) - (0.70710677 * t_im)) ; 
	wt_im = ((-0.70710677 * t_im) + (0.70710677 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin47_Butterfly_Fiss_10682_10707_join[1], u_re) ; 
	push_float(&SplitJoin47_Butterfly_Fiss_10682_10707_join[1], u_im) ; 
	push_float(&SplitJoin47_Butterfly_Fiss_10682_10707_join[1], t_re) ; 
	push_float(&SplitJoin47_Butterfly_Fiss_10682_10707_join[1], t_im) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_10651() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin47_Butterfly_Fiss_10682_10707_split[0], pop_float(&Pre_CollapsedDataParallel_1_10485WEIGHTED_ROUND_ROBIN_Splitter_10651));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin47_Butterfly_Fiss_10682_10707_split[1], pop_float(&Pre_CollapsedDataParallel_1_10485WEIGHTED_ROUND_ROBIN_Splitter_10651));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10652() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10652Post_CollapsedDataParallel_2_10486, pop_float(&SplitJoin47_Butterfly_Fiss_10682_10707_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10652Post_CollapsedDataParallel_2_10486, pop_float(&SplitJoin47_Butterfly_Fiss_10682_10707_join[1]));
	ENDFOR
}

void Post_CollapsedDataParallel_2_10486() {
 {
	int kTimesWeights_i = 0;
 {
	FOR(int, _k, 0,  < , 2, _k++) {
		int partialSum_i = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child0_10577_10703_join[3], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_10652Post_CollapsedDataParallel_2_10486, (kTimesWeights_i + (partialSum_i + _j)))) ; 
			}
			ENDFOR
		}
			partialSum_i = (partialSum_i + 4) ; 
		}
		ENDFOR
	}
		kTimesWeights_i = (kTimesWeights_i + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10652Post_CollapsedDataParallel_2_10486) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_10585() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child0_10577_10703_split[__iter_dec_], pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_Hier_10675_10702_split[0]));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10586() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_Hier_10675_10702_join[0], pop_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child0_10577_10703_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void Pre_CollapsedDataParallel_1_10488() {
 {
	int partialSum_k = 0;
 {
	FOR(int, _k, 0,  < , 2, _k++) {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
		iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
		FOR(int, _i, 0,  < , 2, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&Pre_CollapsedDataParallel_1_10488WEIGHTED_ROUND_ROBIN_Splitter_10655, peek_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child1_10578_10708_split[0], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
			}
			ENDFOR
		}
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 4) ; 
		}
		ENDFOR
	}
		partialSum_k = (partialSum_k + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child1_10578_10708_split[0]) ; 
}


void Butterfly_10657() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin53_Butterfly_Fiss_10683_10709_split[0]) ; 
	u_im = pop_float(&SplitJoin53_Butterfly_Fiss_10683_10709_split[0]) ; 
	t_re = pop_float(&SplitJoin53_Butterfly_Fiss_10683_10709_split[0]) ; 
	t_im = pop_float(&SplitJoin53_Butterfly_Fiss_10683_10709_split[0]) ; 
	wt_re = ((0.9238795 * t_re) - (0.38268346 * t_im)) ; 
	wt_im = ((0.9238795 * t_im) + (0.38268346 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin53_Butterfly_Fiss_10683_10709_join[0], u_re) ; 
	push_float(&SplitJoin53_Butterfly_Fiss_10683_10709_join[0], u_im) ; 
	push_float(&SplitJoin53_Butterfly_Fiss_10683_10709_join[0], t_re) ; 
	push_float(&SplitJoin53_Butterfly_Fiss_10683_10709_join[0], t_im) ; 
}


void Butterfly_10658() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin53_Butterfly_Fiss_10683_10709_split[1]) ; 
	u_im = pop_float(&SplitJoin53_Butterfly_Fiss_10683_10709_split[1]) ; 
	t_re = pop_float(&SplitJoin53_Butterfly_Fiss_10683_10709_split[1]) ; 
	t_im = pop_float(&SplitJoin53_Butterfly_Fiss_10683_10709_split[1]) ; 
	wt_re = ((0.9238795 * t_re) - (0.38268346 * t_im)) ; 
	wt_im = ((0.9238795 * t_im) + (0.38268346 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin53_Butterfly_Fiss_10683_10709_join[1], u_re) ; 
	push_float(&SplitJoin53_Butterfly_Fiss_10683_10709_join[1], u_im) ; 
	push_float(&SplitJoin53_Butterfly_Fiss_10683_10709_join[1], t_re) ; 
	push_float(&SplitJoin53_Butterfly_Fiss_10683_10709_join[1], t_im) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_10655() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin53_Butterfly_Fiss_10683_10709_split[0], pop_float(&Pre_CollapsedDataParallel_1_10488WEIGHTED_ROUND_ROBIN_Splitter_10655));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin53_Butterfly_Fiss_10683_10709_split[1], pop_float(&Pre_CollapsedDataParallel_1_10488WEIGHTED_ROUND_ROBIN_Splitter_10655));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10656() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10656Post_CollapsedDataParallel_2_10489, pop_float(&SplitJoin53_Butterfly_Fiss_10683_10709_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10656Post_CollapsedDataParallel_2_10489, pop_float(&SplitJoin53_Butterfly_Fiss_10683_10709_join[1]));
	ENDFOR
}

void Post_CollapsedDataParallel_2_10489() {
 {
	int kTimesWeights_i = 0;
 {
	FOR(int, _k, 0,  < , 2, _k++) {
		int partialSum_i = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child1_10578_10708_join[0], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_10656Post_CollapsedDataParallel_2_10489, (kTimesWeights_i + (partialSum_i + _j)))) ; 
			}
			ENDFOR
		}
			partialSum_i = (partialSum_i + 4) ; 
		}
		ENDFOR
	}
		kTimesWeights_i = (kTimesWeights_i + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10656Post_CollapsedDataParallel_2_10489) ; 
}


void Pre_CollapsedDataParallel_1_10491() {
 {
	int partialSum_k = 0;
 {
	FOR(int, _k, 0,  < , 2, _k++) {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
		iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
		FOR(int, _i, 0,  < , 2, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&Pre_CollapsedDataParallel_1_10491WEIGHTED_ROUND_ROBIN_Splitter_10659, peek_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child1_10578_10708_split[1], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
			}
			ENDFOR
		}
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 4) ; 
		}
		ENDFOR
	}
		partialSum_k = (partialSum_k + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child1_10578_10708_split[1]) ; 
}


void Butterfly_10661() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin57_Butterfly_Fiss_10684_10710_split[0]) ; 
	u_im = pop_float(&SplitJoin57_Butterfly_Fiss_10684_10710_split[0]) ; 
	t_re = pop_float(&SplitJoin57_Butterfly_Fiss_10684_10710_split[0]) ; 
	t_im = pop_float(&SplitJoin57_Butterfly_Fiss_10684_10710_split[0]) ; 
	wt_re = ((-0.38268352 * t_re) - (0.9238795 * t_im)) ; 
	wt_im = ((-0.38268352 * t_im) + (0.9238795 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin57_Butterfly_Fiss_10684_10710_join[0], u_re) ; 
	push_float(&SplitJoin57_Butterfly_Fiss_10684_10710_join[0], u_im) ; 
	push_float(&SplitJoin57_Butterfly_Fiss_10684_10710_join[0], t_re) ; 
	push_float(&SplitJoin57_Butterfly_Fiss_10684_10710_join[0], t_im) ; 
}


void Butterfly_10662() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin57_Butterfly_Fiss_10684_10710_split[1]) ; 
	u_im = pop_float(&SplitJoin57_Butterfly_Fiss_10684_10710_split[1]) ; 
	t_re = pop_float(&SplitJoin57_Butterfly_Fiss_10684_10710_split[1]) ; 
	t_im = pop_float(&SplitJoin57_Butterfly_Fiss_10684_10710_split[1]) ; 
	wt_re = ((-0.38268352 * t_re) - (0.9238795 * t_im)) ; 
	wt_im = ((-0.38268352 * t_im) + (0.9238795 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin57_Butterfly_Fiss_10684_10710_join[1], u_re) ; 
	push_float(&SplitJoin57_Butterfly_Fiss_10684_10710_join[1], u_im) ; 
	push_float(&SplitJoin57_Butterfly_Fiss_10684_10710_join[1], t_re) ; 
	push_float(&SplitJoin57_Butterfly_Fiss_10684_10710_join[1], t_im) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_10659() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin57_Butterfly_Fiss_10684_10710_split[0], pop_float(&Pre_CollapsedDataParallel_1_10491WEIGHTED_ROUND_ROBIN_Splitter_10659));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin57_Butterfly_Fiss_10684_10710_split[1], pop_float(&Pre_CollapsedDataParallel_1_10491WEIGHTED_ROUND_ROBIN_Splitter_10659));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10660() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10660Post_CollapsedDataParallel_2_10492, pop_float(&SplitJoin57_Butterfly_Fiss_10684_10710_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10660Post_CollapsedDataParallel_2_10492, pop_float(&SplitJoin57_Butterfly_Fiss_10684_10710_join[1]));
	ENDFOR
}

void Post_CollapsedDataParallel_2_10492() {
 {
	int kTimesWeights_i = 0;
 {
	FOR(int, _k, 0,  < , 2, _k++) {
		int partialSum_i = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child1_10578_10708_join[1], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_10660Post_CollapsedDataParallel_2_10492, (kTimesWeights_i + (partialSum_i + _j)))) ; 
			}
			ENDFOR
		}
			partialSum_i = (partialSum_i + 4) ; 
		}
		ENDFOR
	}
		kTimesWeights_i = (kTimesWeights_i + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10660Post_CollapsedDataParallel_2_10492) ; 
}


void Pre_CollapsedDataParallel_1_10494() {
 {
	int partialSum_k = 0;
 {
	FOR(int, _k, 0,  < , 2, _k++) {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
		iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
		FOR(int, _i, 0,  < , 2, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&Pre_CollapsedDataParallel_1_10494WEIGHTED_ROUND_ROBIN_Splitter_10663, peek_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child1_10578_10708_split[2], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
			}
			ENDFOR
		}
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 4) ; 
		}
		ENDFOR
	}
		partialSum_k = (partialSum_k + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child1_10578_10708_split[2]) ; 
}


void Butterfly_10665() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin61_Butterfly_Fiss_10685_10711_split[0]) ; 
	u_im = pop_float(&SplitJoin61_Butterfly_Fiss_10685_10711_split[0]) ; 
	t_re = pop_float(&SplitJoin61_Butterfly_Fiss_10685_10711_split[0]) ; 
	t_im = pop_float(&SplitJoin61_Butterfly_Fiss_10685_10711_split[0]) ; 
	wt_re = ((0.38268343 * t_re) - (0.9238795 * t_im)) ; 
	wt_im = ((0.38268343 * t_im) + (0.9238795 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin61_Butterfly_Fiss_10685_10711_join[0], u_re) ; 
	push_float(&SplitJoin61_Butterfly_Fiss_10685_10711_join[0], u_im) ; 
	push_float(&SplitJoin61_Butterfly_Fiss_10685_10711_join[0], t_re) ; 
	push_float(&SplitJoin61_Butterfly_Fiss_10685_10711_join[0], t_im) ; 
}


void Butterfly_10666() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin61_Butterfly_Fiss_10685_10711_split[1]) ; 
	u_im = pop_float(&SplitJoin61_Butterfly_Fiss_10685_10711_split[1]) ; 
	t_re = pop_float(&SplitJoin61_Butterfly_Fiss_10685_10711_split[1]) ; 
	t_im = pop_float(&SplitJoin61_Butterfly_Fiss_10685_10711_split[1]) ; 
	wt_re = ((0.38268343 * t_re) - (0.9238795 * t_im)) ; 
	wt_im = ((0.38268343 * t_im) + (0.9238795 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin61_Butterfly_Fiss_10685_10711_join[1], u_re) ; 
	push_float(&SplitJoin61_Butterfly_Fiss_10685_10711_join[1], u_im) ; 
	push_float(&SplitJoin61_Butterfly_Fiss_10685_10711_join[1], t_re) ; 
	push_float(&SplitJoin61_Butterfly_Fiss_10685_10711_join[1], t_im) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_10663() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin61_Butterfly_Fiss_10685_10711_split[0], pop_float(&Pre_CollapsedDataParallel_1_10494WEIGHTED_ROUND_ROBIN_Splitter_10663));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin61_Butterfly_Fiss_10685_10711_split[1], pop_float(&Pre_CollapsedDataParallel_1_10494WEIGHTED_ROUND_ROBIN_Splitter_10663));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10664() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10664Post_CollapsedDataParallel_2_10495, pop_float(&SplitJoin61_Butterfly_Fiss_10685_10711_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10664Post_CollapsedDataParallel_2_10495, pop_float(&SplitJoin61_Butterfly_Fiss_10685_10711_join[1]));
	ENDFOR
}

void Post_CollapsedDataParallel_2_10495() {
 {
	int kTimesWeights_i = 0;
 {
	FOR(int, _k, 0,  < , 2, _k++) {
		int partialSum_i = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child1_10578_10708_join[2], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_10664Post_CollapsedDataParallel_2_10495, (kTimesWeights_i + (partialSum_i + _j)))) ; 
			}
			ENDFOR
		}
			partialSum_i = (partialSum_i + 4) ; 
		}
		ENDFOR
	}
		kTimesWeights_i = (kTimesWeights_i + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10664Post_CollapsedDataParallel_2_10495) ; 
}


void Pre_CollapsedDataParallel_1_10497() {
 {
	int partialSum_k = 0;
 {
	FOR(int, _k, 0,  < , 2, _k++) {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
		iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
		FOR(int, _i, 0,  < , 2, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&Pre_CollapsedDataParallel_1_10497WEIGHTED_ROUND_ROBIN_Splitter_10667, peek_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child1_10578_10708_split[3], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
			}
			ENDFOR
		}
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 4) ; 
		}
		ENDFOR
	}
		partialSum_k = (partialSum_k + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child1_10578_10708_split[3]) ; 
}


void Butterfly_10669() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin65_Butterfly_Fiss_10686_10712_split[0]) ; 
	u_im = pop_float(&SplitJoin65_Butterfly_Fiss_10686_10712_split[0]) ; 
	t_re = pop_float(&SplitJoin65_Butterfly_Fiss_10686_10712_split[0]) ; 
	t_im = pop_float(&SplitJoin65_Butterfly_Fiss_10686_10712_split[0]) ; 
	wt_re = ((-0.9238796 * t_re) - (0.38268328 * t_im)) ; 
	wt_im = ((-0.9238796 * t_im) + (0.38268328 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin65_Butterfly_Fiss_10686_10712_join[0], u_re) ; 
	push_float(&SplitJoin65_Butterfly_Fiss_10686_10712_join[0], u_im) ; 
	push_float(&SplitJoin65_Butterfly_Fiss_10686_10712_join[0], t_re) ; 
	push_float(&SplitJoin65_Butterfly_Fiss_10686_10712_join[0], t_im) ; 
}


void Butterfly_10670() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin65_Butterfly_Fiss_10686_10712_split[1]) ; 
	u_im = pop_float(&SplitJoin65_Butterfly_Fiss_10686_10712_split[1]) ; 
	t_re = pop_float(&SplitJoin65_Butterfly_Fiss_10686_10712_split[1]) ; 
	t_im = pop_float(&SplitJoin65_Butterfly_Fiss_10686_10712_split[1]) ; 
	wt_re = ((-0.9238796 * t_re) - (0.38268328 * t_im)) ; 
	wt_im = ((-0.9238796 * t_im) + (0.38268328 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin65_Butterfly_Fiss_10686_10712_join[1], u_re) ; 
	push_float(&SplitJoin65_Butterfly_Fiss_10686_10712_join[1], u_im) ; 
	push_float(&SplitJoin65_Butterfly_Fiss_10686_10712_join[1], t_re) ; 
	push_float(&SplitJoin65_Butterfly_Fiss_10686_10712_join[1], t_im) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_10667() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin65_Butterfly_Fiss_10686_10712_split[0], pop_float(&Pre_CollapsedDataParallel_1_10497WEIGHTED_ROUND_ROBIN_Splitter_10667));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin65_Butterfly_Fiss_10686_10712_split[1], pop_float(&Pre_CollapsedDataParallel_1_10497WEIGHTED_ROUND_ROBIN_Splitter_10667));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10668() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10668Post_CollapsedDataParallel_2_10498, pop_float(&SplitJoin65_Butterfly_Fiss_10686_10712_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10668Post_CollapsedDataParallel_2_10498, pop_float(&SplitJoin65_Butterfly_Fiss_10686_10712_join[1]));
	ENDFOR
}

void Post_CollapsedDataParallel_2_10498() {
 {
	int kTimesWeights_i = 0;
 {
	FOR(int, _k, 0,  < , 2, _k++) {
		int partialSum_i = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
 {
			FOR(int, _j, 0,  < , 2, _j++) {
				push_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child1_10578_10708_join[3], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_10668Post_CollapsedDataParallel_2_10498, (kTimesWeights_i + (partialSum_i + _j)))) ; 
			}
			ENDFOR
		}
			partialSum_i = (partialSum_i + 4) ; 
		}
		ENDFOR
	}
		kTimesWeights_i = (kTimesWeights_i + 2) ; 
	}
	ENDFOR
}
}
	pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10668Post_CollapsedDataParallel_2_10498) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_10587() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child1_10578_10708_split[__iter_dec_], pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_Hier_10675_10702_split[1]));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10588() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_Hier_10675_10702_join[1], pop_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child1_10578_10708_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10584() {
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_Hier_10675_10702_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10583WEIGHTED_ROUND_ROBIN_Splitter_10584));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_Hier_10675_10702_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10583WEIGHTED_ROUND_ROBIN_Splitter_10584));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10589() {
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10589WEIGHTED_ROUND_ROBIN_Splitter_10590, pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_Hier_10675_10702_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10589WEIGHTED_ROUND_ROBIN_Splitter_10590, pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_Hier_10675_10702_join[1]));
	ENDFOR
}

void Butterfly_10219() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[0]) ; 
	u_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[0]) ; 
	t_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[0]) ; 
	t_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[0]) ; 
	wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
	wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[0], u_re) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[0], u_im) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[0], t_re) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[0], t_im) ; 
}


void Butterfly_10220() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[1]) ; 
	u_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[1]) ; 
	t_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[1]) ; 
	t_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[1]) ; 
	wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
	wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[1], u_re) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[1], u_im) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[1], t_re) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[1], t_im) ; 
}


void Butterfly_10221() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[2]) ; 
	u_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[2]) ; 
	t_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[2]) ; 
	t_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[2]) ; 
	wt_re = ((0.70710677 * t_re) - (0.70710677 * t_im)) ; 
	wt_im = ((0.70710677 * t_im) + (0.70710677 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[2], u_re) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[2], u_im) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[2], t_re) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[2], t_im) ; 
}


void Butterfly_10222() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[3]) ; 
	u_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[3]) ; 
	t_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[3]) ; 
	t_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[3]) ; 
	wt_re = ((-0.70710677 * t_re) - (0.70710677 * t_im)) ; 
	wt_im = ((-0.70710677 * t_im) + (0.70710677 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[3], u_re) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[3], u_im) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[3], t_re) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[3], t_im) ; 
}


void Butterfly_10223() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[4]) ; 
	u_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[4]) ; 
	t_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[4]) ; 
	t_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[4]) ; 
	wt_re = ((0.9238795 * t_re) - (0.38268346 * t_im)) ; 
	wt_im = ((0.9238795 * t_im) + (0.38268346 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[4], u_re) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[4], u_im) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[4], t_re) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[4], t_im) ; 
}


void Butterfly_10224() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[5]) ; 
	u_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[5]) ; 
	t_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[5]) ; 
	t_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[5]) ; 
	wt_re = ((-0.38268352 * t_re) - (0.9238795 * t_im)) ; 
	wt_im = ((-0.38268352 * t_im) + (0.9238795 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[5], u_re) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[5], u_im) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[5], t_re) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[5], t_im) ; 
}


void Butterfly_10225() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[6]) ; 
	u_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[6]) ; 
	t_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[6]) ; 
	t_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[6]) ; 
	wt_re = ((0.38268343 * t_re) - (0.9238795 * t_im)) ; 
	wt_im = ((0.38268343 * t_im) + (0.9238795 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[6], u_re) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[6], u_im) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[6], t_re) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[6], t_im) ; 
}


void Butterfly_10226() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[7]) ; 
	u_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[7]) ; 
	t_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[7]) ; 
	t_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[7]) ; 
	wt_re = ((-0.9238796 * t_re) - (0.38268328 * t_im)) ; 
	wt_im = ((-0.9238796 * t_im) + (0.38268328 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[7], u_re) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[7], u_im) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[7], t_re) ; 
	push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[7], t_im) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_10591() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[__iter_dec_], pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_Hier_10677_10713_split[0]));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10592() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_Hier_10677_10713_join[0], pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void Butterfly_10227() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[0]) ; 
	u_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[0]) ; 
	t_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[0]) ; 
	t_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[0]) ; 
	wt_re = ((0.98078525 * t_re) - (0.19509032 * t_im)) ; 
	wt_im = ((0.98078525 * t_im) + (0.19509032 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[0], u_re) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[0], u_im) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[0], t_re) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[0], t_im) ; 
}


void Butterfly_10228() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[1]) ; 
	u_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[1]) ; 
	t_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[1]) ; 
	t_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[1]) ; 
	wt_re = ((-0.19509032 * t_re) - (0.98078525 * t_im)) ; 
	wt_im = ((-0.19509032 * t_im) + (0.98078525 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[1], u_re) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[1], u_im) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[1], t_re) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[1], t_im) ; 
}


void Butterfly_10229() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[2]) ; 
	u_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[2]) ; 
	t_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[2]) ; 
	t_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[2]) ; 
	wt_re = ((0.5555702 * t_re) - (0.83146966 * t_im)) ; 
	wt_im = ((0.5555702 * t_im) + (0.83146966 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[2], u_re) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[2], u_im) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[2], t_re) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[2], t_im) ; 
}


void Butterfly_10230() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[3]) ; 
	u_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[3]) ; 
	t_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[3]) ; 
	t_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[3]) ; 
	wt_re = ((-0.83146966 * t_re) - (0.5555702 * t_im)) ; 
	wt_im = ((-0.83146966 * t_im) + (0.5555702 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[3], u_re) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[3], u_im) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[3], t_re) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[3], t_im) ; 
}


void Butterfly_10231() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[4]) ; 
	u_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[4]) ; 
	t_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[4]) ; 
	t_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[4]) ; 
	wt_re = ((0.8314696 * t_re) - (0.55557024 * t_im)) ; 
	wt_im = ((0.8314696 * t_im) + (0.55557024 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[4], u_re) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[4], u_im) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[4], t_re) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[4], t_im) ; 
}


void Butterfly_10232() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[5]) ; 
	u_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[5]) ; 
	t_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[5]) ; 
	t_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[5]) ; 
	wt_re = ((-0.55557036 * t_re) - (0.83146954 * t_im)) ; 
	wt_im = ((-0.55557036 * t_im) + (0.83146954 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[5], u_re) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[5], u_im) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[5], t_re) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[5], t_im) ; 
}


void Butterfly_10233() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[6]) ; 
	u_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[6]) ; 
	t_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[6]) ; 
	t_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[6]) ; 
	wt_re = ((0.19509023 * t_re) - (0.9807853 * t_im)) ; 
	wt_im = ((0.19509023 * t_im) + (0.9807853 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[6], u_re) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[6], u_im) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[6], t_re) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[6], t_im) ; 
}


void Butterfly_10234() {
	float u_re = 0.0;
	float u_im = 0.0;
	float t_re = 0.0;
	float t_im = 0.0;
	float wt_re = 0.0;
	float wt_im = 0.0;
	u_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[7]) ; 
	u_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[7]) ; 
	t_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[7]) ; 
	t_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[7]) ; 
	wt_re = ((-0.9807853 * t_re) - (0.19509031 * t_im)) ; 
	wt_im = ((-0.9807853 * t_im) + (0.19509031 * t_re)) ; 
	t_re = (u_re - wt_re) ; 
	t_im = (u_im - wt_im) ; 
	u_re = (u_re + wt_re) ; 
	u_im = (u_im + wt_im) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[7], u_re) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[7], u_im) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[7], t_re) ; 
	push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[7], t_im) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_10593() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[__iter_dec_], pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_Hier_10677_10713_split[1]));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10594() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
			push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_Hier_10677_10713_join[1], pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_10590() {
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_Hier_10677_10713_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10589WEIGHTED_ROUND_ROBIN_Splitter_10590));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_Hier_10677_10713_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10589WEIGHTED_ROUND_ROBIN_Splitter_10590));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_10595() {
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10595BitReverse_10235, pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_Hier_10677_10713_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_10595BitReverse_10235, pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_Hier_10677_10713_join[1]));
	ENDFOR
}

int BitReverse_10235_bitrev(int inp, int numbits) {
	int rev = 0;
	FOR(int, i, 0,  < , numbits, i++) {
		rev = ((rev * 2) | (inp & 1)) ; 
		inp = (inp / 2) ; 
	}
	ENDFOR
	return rev;
}
void BitReverse_10235() {
	FOR(int, i__conflict__0, 0,  < , 32, i__conflict__0++) {
		int br = 0;
		br = BitReverse_10235_bitrev(i__conflict__0, 5) ; 
		push_float(&BitReverse_10235FloatPrinter_10236, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_10595BitReverse_10235, (2 * br))) ; 
		push_float(&BitReverse_10235FloatPrinter_10236, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_10595BitReverse_10235, ((2 * br) + 1))) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 32, i++) {
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10595BitReverse_10235) ; 
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_10595BitReverse_10235) ; 
	}
	ENDFOR
}


void FloatPrinter_10236(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		printf("%.10f", pop_float(&BitReverse_10235FloatPrinter_10236));
		printf("\n");
		printf("%.10f", pop_float(&BitReverse_10235FloatPrinter_10236));
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_float(&SplitJoin43_Butterfly_Fiss_10681_10706_split[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10664Post_CollapsedDataParallel_2_10495);
	FOR(int, __iter_init_1_, 0, <, 2, __iter_init_1_++)
		init_buffer_float(&SplitJoin57_Butterfly_Fiss_10684_10710_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_Hier_10675_10702_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 4, __iter_init_3_++)
		init_buffer_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child0_10577_10703_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 2, __iter_init_4_++)
		init_buffer_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_Hier_10677_10713_join[__iter_init_4_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_10485WEIGHTED_ROUND_ROBIN_Splitter_10651);
	FOR(int, __iter_init_5_, 0, <, 2, __iter_init_5_++)
		init_buffer_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_Hier_10677_10713_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 2, __iter_init_6_++)
		init_buffer_float(&SplitJoin61_Butterfly_Fiss_10685_10711_split[__iter_init_6_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10652Post_CollapsedDataParallel_2_10486);
	FOR(int, __iter_init_7_, 0, <, 2, __iter_init_7_++)
		init_buffer_float(&SplitJoin43_Butterfly_Fiss_10681_10706_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 4, __iter_init_8_++)
		init_buffer_float(&SplitJoin8_Butterfly_Fiss_10674_10696_split[__iter_init_8_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_10467WEIGHTED_ROUND_ROBIN_Splitter_10615);
	FOR(int, __iter_init_9_, 0, <, 4, __iter_init_9_++)
		init_buffer_float(&SplitJoin91_Butterfly_Fiss_10690_10701_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 4, __iter_init_10_++)
		init_buffer_float(&SplitJoin72_Butterfly_Fiss_10687_10697_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 4, __iter_init_11_++)
		init_buffer_float(&SplitJoin4_Butterfly_Fiss_10673_10694_split[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_10479WEIGHTED_ROUND_ROBIN_Splitter_10643);
	FOR(int, __iter_init_12_, 0, <, 2, __iter_init_12_++)
		init_buffer_float(&SplitJoin83_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_child1_10553_10699_join[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_float(&SplitJoin83_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_child1_10553_10699_split[__iter_init_13_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10628Post_CollapsedDataParallel_2_10471);
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_float(&SplitJoin61_Butterfly_Fiss_10685_10711_join[__iter_init_14_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_10456WEIGHTED_ROUND_ROBIN_Splitter_10499);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10610Post_CollapsedDataParallel_2_10465);
	init_buffer_float(&Pre_CollapsedDataParallel_1_10491WEIGHTED_ROUND_ROBIN_Splitter_10659);
	FOR(int, __iter_init_15_, 0, <, 8, __iter_init_15_++)
		init_buffer_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_join[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_10112_10507_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_Hier_10672_10693_join[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 8, __iter_init_17_++)
		init_buffer_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child1_10679_10715_split[__iter_init_17_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10660Post_CollapsedDataParallel_2_10492);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10616Post_CollapsedDataParallel_2_10468);
	FOR(int, __iter_init_18_, 0, <, 2, __iter_init_18_++)
		init_buffer_float(&SplitJoin65_Butterfly_Fiss_10686_10712_split[__iter_init_18_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10640Post_CollapsedDataParallel_2_10477);
	init_buffer_float(&Pre_CollapsedDataParallel_1_10473WEIGHTED_ROUND_ROBIN_Splitter_10633);
	init_buffer_float(&Pre_CollapsedDataParallel_1_10476WEIGHTED_ROUND_ROBIN_Splitter_10639);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10644Post_CollapsedDataParallel_2_10480);
	init_buffer_float(&Pre_CollapsedDataParallel_1_10488WEIGHTED_ROUND_ROBIN_Splitter_10655);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10598Post_CollapsedDataParallel_2_10456);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10583WEIGHTED_ROUND_ROBIN_Splitter_10584);
	FOR(int, __iter_init_19_, 0, <, 4, __iter_init_19_++)
		init_buffer_float(&SplitJoin81_Butterfly_Fiss_10688_10698_join[__iter_init_19_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_10461WEIGHTED_ROUND_ROBIN_Splitter_10621);
	FOR(int, __iter_init_20_, 0, <, 4, __iter_init_20_++)
		init_buffer_float(&SplitJoin72_Butterfly_Fiss_10687_10697_split[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 4, __iter_init_21_++)
		init_buffer_float(&SplitJoin81_Butterfly_Fiss_10688_10698_split[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 4, __iter_init_22_++)
		init_buffer_float(&SplitJoin8_Butterfly_Fiss_10674_10696_join[__iter_init_22_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_10497WEIGHTED_ROUND_ROBIN_Splitter_10667);
	init_buffer_float(&Post_CollapsedDataParallel_2_10459WEIGHTED_ROUND_ROBIN_Splitter_10579);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10648Post_CollapsedDataParallel_2_10483);
	init_buffer_float(&Pre_CollapsedDataParallel_1_10458WEIGHTED_ROUND_ROBIN_Splitter_10603);
	FOR(int, __iter_init_23_, 0, <, 2, __iter_init_23_++)
		init_buffer_float(&SplitJoin14_Butterfly_Fiss_10676_10704_split[__iter_init_23_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_10455WEIGHTED_ROUND_ROBIN_Splitter_10597);
	FOR(int, __iter_init_24_, 0, <, 4, __iter_init_24_++)
		init_buffer_float(&SplitJoin0_Butterfly_Fiss_10671_10692_split[__iter_init_24_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_10462WEIGHTED_ROUND_ROBIN_Splitter_10581);
	FOR(int, __iter_init_25_, 0, <, 2, __iter_init_25_++)
		init_buffer_float(&SplitJoin14_Butterfly_Fiss_10676_10704_join[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 2, __iter_init_26_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_10112_10507_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_Hier_10672_10693_split[__iter_init_26_]);
	ENDFOR
	FOR(int, __iter_init_27_, 0, <, 4, __iter_init_27_++)
		init_buffer_float(&SplitJoin85_Butterfly_Fiss_10689_10700_join[__iter_init_27_]);
	ENDFOR
	init_buffer_float(&BitReverse_10235FloatPrinter_10236);
	FOR(int, __iter_init_28_, 0, <, 2, __iter_init_28_++)
		init_buffer_float(&SplitJoin39_Butterfly_Fiss_10680_10705_join[__iter_init_28_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_10470WEIGHTED_ROUND_ROBIN_Splitter_10627);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10595BitReverse_10235);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10604Post_CollapsedDataParallel_2_10459);
	FOR(int, __iter_init_29_, 0, <, 4, __iter_init_29_++)
		init_buffer_float(&SplitJoin4_Butterfly_Fiss_10673_10694_join[__iter_init_29_]);
	ENDFOR
	FOR(int, __iter_init_30_, 0, <, 2, __iter_init_30_++)
		init_buffer_float(&SplitJoin57_Butterfly_Fiss_10684_10710_split[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 2, __iter_init_31_++)
		init_buffer_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_child0_10548_10695_join[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 2, __iter_init_32_++)
		init_buffer_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_10121_10509_child0_10548_10695_split[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 2, __iter_init_33_++)
		init_buffer_float(&SplitJoin39_Butterfly_Fiss_10680_10705_split[__iter_init_33_]);
	ENDFOR
	FOR(int, __iter_init_34_, 0, <, 8, __iter_init_34_++)
		init_buffer_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_join[__iter_init_34_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10589WEIGHTED_ROUND_ROBIN_Splitter_10590);
	init_buffer_float(&Pre_CollapsedDataParallel_1_10464WEIGHTED_ROUND_ROBIN_Splitter_10609);
	FOR(int, __iter_init_35_, 0, <, 4, __iter_init_35_++)
		init_buffer_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child1_10578_10708_join[__iter_init_35_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_10494WEIGHTED_ROUND_ROBIN_Splitter_10663);
	FOR(int, __iter_init_36_, 0, <, 2, __iter_init_36_++)
		init_buffer_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_Hier_10675_10702_split[__iter_init_36_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10634Post_CollapsedDataParallel_2_10474);
	init_buffer_float(&FloatSource_10154Pre_CollapsedDataParallel_1_10455);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10656Post_CollapsedDataParallel_2_10489);
	FOR(int, __iter_init_37_, 0, <, 2, __iter_init_37_++)
		init_buffer_float(&SplitJoin47_Butterfly_Fiss_10682_10707_split[__iter_init_37_]);
	ENDFOR
	FOR(int, __iter_init_38_, 0, <, 2, __iter_init_38_++)
		init_buffer_float(&SplitJoin47_Butterfly_Fiss_10682_10707_join[__iter_init_38_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10622Post_CollapsedDataParallel_2_10462);
	FOR(int, __iter_init_39_, 0, <, 8, __iter_init_39_++)
		init_buffer_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10152_10513_Hier_child0_10678_10714_split[__iter_init_39_]);
	ENDFOR
	FOR(int, __iter_init_40_, 0, <, 2, __iter_init_40_++)
		init_buffer_float(&SplitJoin53_Butterfly_Fiss_10683_10709_split[__iter_init_40_]);
	ENDFOR
	FOR(int, __iter_init_41_, 0, <, 2, __iter_init_41_++)
		init_buffer_float(&SplitJoin53_Butterfly_Fiss_10683_10709_join[__iter_init_41_]);
	ENDFOR
	FOR(int, __iter_init_42_, 0, <, 4, __iter_init_42_++)
		init_buffer_float(&SplitJoin85_Butterfly_Fiss_10689_10700_split[__iter_init_42_]);
	ENDFOR
	FOR(int, __iter_init_43_, 0, <, 4, __iter_init_43_++)
		init_buffer_float(&SplitJoin0_Butterfly_Fiss_10671_10692_join[__iter_init_43_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_10482WEIGHTED_ROUND_ROBIN_Splitter_10647);
	FOR(int, __iter_init_44_, 0, <, 2, __iter_init_44_++)
		init_buffer_float(&SplitJoin65_Butterfly_Fiss_10686_10712_join[__iter_init_44_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_10668Post_CollapsedDataParallel_2_10498);
	FOR(int, __iter_init_45_, 0, <, 4, __iter_init_45_++)
		init_buffer_float(&SplitJoin91_Butterfly_Fiss_10690_10701_join[__iter_init_45_]);
	ENDFOR
	FOR(int, __iter_init_46_, 0, <, 4, __iter_init_46_++)
		init_buffer_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child0_10577_10703_join[__iter_init_46_]);
	ENDFOR
	FOR(int, __iter_init_47_, 0, <, 4, __iter_init_47_++)
		init_buffer_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10134_10511_Hier_child1_10578_10708_split[__iter_init_47_]);
	ENDFOR
// --- init: FloatSource_10154
	 {
	FOR(int, i, 0,  < , 32, i++) {
		FloatSource_10154_s.A_re[i] = 0.0 ; 
		FloatSource_10154_s.A_im[i] = 0.0 ; 
	}
	ENDFOR
	FloatSource_10154_s.A_re[1] = 1.0 ; 
	FloatSource_10154_s.idx = 0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FloatSource_10154();
		Pre_CollapsedDataParallel_1_10455();
		WEIGHTED_ROUND_ROBIN_Splitter_10597();
			Butterfly_10599();
			Butterfly_10600();
			Butterfly_10601();
			Butterfly_10602();
		WEIGHTED_ROUND_ROBIN_Joiner_10598();
		Post_CollapsedDataParallel_2_10456();
		WEIGHTED_ROUND_ROBIN_Splitter_10499();
			Pre_CollapsedDataParallel_1_10458();
			WEIGHTED_ROUND_ROBIN_Splitter_10603();
				Butterfly_10605();
				Butterfly_10606();
				Butterfly_10607();
				Butterfly_10608();
			WEIGHTED_ROUND_ROBIN_Joiner_10604();
			Post_CollapsedDataParallel_2_10459();
			WEIGHTED_ROUND_ROBIN_Splitter_10579();
				Pre_CollapsedDataParallel_1_10464();
				WEIGHTED_ROUND_ROBIN_Splitter_10609();
					Butterfly_10611();
					Butterfly_10612();
					Butterfly_10613();
					Butterfly_10614();
				WEIGHTED_ROUND_ROBIN_Joiner_10610();
				Post_CollapsedDataParallel_2_10465();
				Pre_CollapsedDataParallel_1_10467();
				WEIGHTED_ROUND_ROBIN_Splitter_10615();
					Butterfly_10617();
					Butterfly_10618();
					Butterfly_10619();
					Butterfly_10620();
				WEIGHTED_ROUND_ROBIN_Joiner_10616();
				Post_CollapsedDataParallel_2_10468();
			WEIGHTED_ROUND_ROBIN_Joiner_10580();
			Pre_CollapsedDataParallel_1_10461();
			WEIGHTED_ROUND_ROBIN_Splitter_10621();
				Butterfly_10623();
				Butterfly_10624();
				Butterfly_10625();
				Butterfly_10626();
			WEIGHTED_ROUND_ROBIN_Joiner_10622();
			Post_CollapsedDataParallel_2_10462();
			WEIGHTED_ROUND_ROBIN_Splitter_10581();
				Pre_CollapsedDataParallel_1_10470();
				WEIGHTED_ROUND_ROBIN_Splitter_10627();
					Butterfly_10629();
					Butterfly_10630();
					Butterfly_10631();
					Butterfly_10632();
				WEIGHTED_ROUND_ROBIN_Joiner_10628();
				Post_CollapsedDataParallel_2_10471();
				Pre_CollapsedDataParallel_1_10473();
				WEIGHTED_ROUND_ROBIN_Splitter_10633();
					Butterfly_10635();
					Butterfly_10636();
					Butterfly_10637();
					Butterfly_10638();
				WEIGHTED_ROUND_ROBIN_Joiner_10634();
				Post_CollapsedDataParallel_2_10474();
			WEIGHTED_ROUND_ROBIN_Joiner_10582();
		WEIGHTED_ROUND_ROBIN_Joiner_10583();
		WEIGHTED_ROUND_ROBIN_Splitter_10584();
			WEIGHTED_ROUND_ROBIN_Splitter_10585();
				Pre_CollapsedDataParallel_1_10476();
				WEIGHTED_ROUND_ROBIN_Splitter_10639();
					Butterfly_10641();
					Butterfly_10642();
				WEIGHTED_ROUND_ROBIN_Joiner_10640();
				Post_CollapsedDataParallel_2_10477();
				Pre_CollapsedDataParallel_1_10479();
				WEIGHTED_ROUND_ROBIN_Splitter_10643();
					Butterfly_10645();
					Butterfly_10646();
				WEIGHTED_ROUND_ROBIN_Joiner_10644();
				Post_CollapsedDataParallel_2_10480();
				Pre_CollapsedDataParallel_1_10482();
				WEIGHTED_ROUND_ROBIN_Splitter_10647();
					Butterfly_10649();
					Butterfly_10650();
				WEIGHTED_ROUND_ROBIN_Joiner_10648();
				Post_CollapsedDataParallel_2_10483();
				Pre_CollapsedDataParallel_1_10485();
				WEIGHTED_ROUND_ROBIN_Splitter_10651();
					Butterfly_10653();
					Butterfly_10654();
				WEIGHTED_ROUND_ROBIN_Joiner_10652();
				Post_CollapsedDataParallel_2_10486();
			WEIGHTED_ROUND_ROBIN_Joiner_10586();
			WEIGHTED_ROUND_ROBIN_Splitter_10587();
				Pre_CollapsedDataParallel_1_10488();
				WEIGHTED_ROUND_ROBIN_Splitter_10655();
					Butterfly_10657();
					Butterfly_10658();
				WEIGHTED_ROUND_ROBIN_Joiner_10656();
				Post_CollapsedDataParallel_2_10489();
				Pre_CollapsedDataParallel_1_10491();
				WEIGHTED_ROUND_ROBIN_Splitter_10659();
					Butterfly_10661();
					Butterfly_10662();
				WEIGHTED_ROUND_ROBIN_Joiner_10660();
				Post_CollapsedDataParallel_2_10492();
				Pre_CollapsedDataParallel_1_10494();
				WEIGHTED_ROUND_ROBIN_Splitter_10663();
					Butterfly_10665();
					Butterfly_10666();
				WEIGHTED_ROUND_ROBIN_Joiner_10664();
				Post_CollapsedDataParallel_2_10495();
				Pre_CollapsedDataParallel_1_10497();
				WEIGHTED_ROUND_ROBIN_Splitter_10667();
					Butterfly_10669();
					Butterfly_10670();
				WEIGHTED_ROUND_ROBIN_Joiner_10668();
				Post_CollapsedDataParallel_2_10498();
			WEIGHTED_ROUND_ROBIN_Joiner_10588();
		WEIGHTED_ROUND_ROBIN_Joiner_10589();
		WEIGHTED_ROUND_ROBIN_Splitter_10590();
			WEIGHTED_ROUND_ROBIN_Splitter_10591();
				Butterfly_10219();
				Butterfly_10220();
				Butterfly_10221();
				Butterfly_10222();
				Butterfly_10223();
				Butterfly_10224();
				Butterfly_10225();
				Butterfly_10226();
			WEIGHTED_ROUND_ROBIN_Joiner_10592();
			WEIGHTED_ROUND_ROBIN_Splitter_10593();
				Butterfly_10227();
				Butterfly_10228();
				Butterfly_10229();
				Butterfly_10230();
				Butterfly_10231();
				Butterfly_10232();
				Butterfly_10233();
				Butterfly_10234();
			WEIGHTED_ROUND_ROBIN_Joiner_10594();
		WEIGHTED_ROUND_ROBIN_Joiner_10595();
		BitReverse_10235();
		FloatPrinter_10236();
	ENDFOR
	return EXIT_SUCCESS;
}
