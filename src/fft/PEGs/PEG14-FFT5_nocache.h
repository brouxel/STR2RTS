#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=224 on the compile command line
#else
#if BUF_SIZEMAX < 224
#error BUF_SIZEMAX too small, it must be at least 224
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif


void WEIGHTED_ROUND_ROBIN_Splitter_2996();
void source_2998();
void source_2999();
void WEIGHTED_ROUND_ROBIN_Joiner_2997();
void WEIGHTED_ROUND_ROBIN_Splitter_2839();
void WEIGHTED_ROUND_ROBIN_Splitter_2841();
void WEIGHTED_ROUND_ROBIN_Splitter_2843();
void WEIGHTED_ROUND_ROBIN_Splitter_2845();
void Identity_2669();
void Identity_2671();
void WEIGHTED_ROUND_ROBIN_Joiner_2846();
void WEIGHTED_ROUND_ROBIN_Splitter_2847();
void Identity_2675();
void Identity_2677();
void WEIGHTED_ROUND_ROBIN_Joiner_2848();
void WEIGHTED_ROUND_ROBIN_Joiner_2844();
void WEIGHTED_ROUND_ROBIN_Splitter_2849();
void WEIGHTED_ROUND_ROBIN_Splitter_2851();
void Identity_2683();
void Identity_2685();
void WEIGHTED_ROUND_ROBIN_Joiner_2852();
void WEIGHTED_ROUND_ROBIN_Splitter_2853();
void Identity_2689();
void Identity_2691();
void WEIGHTED_ROUND_ROBIN_Joiner_2854();
void WEIGHTED_ROUND_ROBIN_Joiner_2850();
void WEIGHTED_ROUND_ROBIN_Joiner_2842();
void WEIGHTED_ROUND_ROBIN_Splitter_2855();
void WEIGHTED_ROUND_ROBIN_Splitter_2857();
void WEIGHTED_ROUND_ROBIN_Splitter_2859();
void Identity_2699();
void Identity_2701();
void WEIGHTED_ROUND_ROBIN_Joiner_2860();
void WEIGHTED_ROUND_ROBIN_Splitter_2861();
void Identity_2705();
void Identity_2707();
void WEIGHTED_ROUND_ROBIN_Joiner_2862();
void WEIGHTED_ROUND_ROBIN_Joiner_2858();
void WEIGHTED_ROUND_ROBIN_Splitter_2863();
void WEIGHTED_ROUND_ROBIN_Splitter_2865();
void Identity_2713();
void Identity_2715();
void WEIGHTED_ROUND_ROBIN_Joiner_2866();
void WEIGHTED_ROUND_ROBIN_Splitter_2867();
void Identity_2719();
void Identity_2721();
void WEIGHTED_ROUND_ROBIN_Joiner_2868();
void WEIGHTED_ROUND_ROBIN_Joiner_2864();
void WEIGHTED_ROUND_ROBIN_Joiner_2856();
void WEIGHTED_ROUND_ROBIN_Joiner_2840();
void WEIGHTED_ROUND_ROBIN_Splitter_2983();
void WEIGHTED_ROUND_ROBIN_Splitter_2984();
void Pre_CollapsedDataParallel_1_2816();
void butterfly_2723();
void Post_CollapsedDataParallel_2_2817();
void Pre_CollapsedDataParallel_1_2819();
void butterfly_2724();
void Post_CollapsedDataParallel_2_2820();
void Pre_CollapsedDataParallel_1_2822();
void butterfly_2725();
void Post_CollapsedDataParallel_2_2823();
void Pre_CollapsedDataParallel_1_2825();
void butterfly_2726();
void Post_CollapsedDataParallel_2_2826();
void WEIGHTED_ROUND_ROBIN_Joiner_2985();
void WEIGHTED_ROUND_ROBIN_Splitter_2986();
void Pre_CollapsedDataParallel_1_2828();
void butterfly_2727();
void Post_CollapsedDataParallel_2_2829();
void Pre_CollapsedDataParallel_1_2831();
void butterfly_2728();
void Post_CollapsedDataParallel_2_2832();
void Pre_CollapsedDataParallel_1_2834();
void butterfly_2729();
void Post_CollapsedDataParallel_2_2835();
void Pre_CollapsedDataParallel_1_2837();
void butterfly_2730();
void Post_CollapsedDataParallel_2_2838();
void WEIGHTED_ROUND_ROBIN_Joiner_2987();
void WEIGHTED_ROUND_ROBIN_Joiner_2988();
void WEIGHTED_ROUND_ROBIN_Splitter_2989();
void WEIGHTED_ROUND_ROBIN_Splitter_2990();
void WEIGHTED_ROUND_ROBIN_Splitter_2873();
void butterfly_2732();
void butterfly_2733();
void WEIGHTED_ROUND_ROBIN_Joiner_2874();
void WEIGHTED_ROUND_ROBIN_Splitter_2875();
void butterfly_2734();
void butterfly_2735();
void WEIGHTED_ROUND_ROBIN_Joiner_2876();
void WEIGHTED_ROUND_ROBIN_Joiner_2991();
void WEIGHTED_ROUND_ROBIN_Splitter_2992();
void WEIGHTED_ROUND_ROBIN_Splitter_2877();
void butterfly_2736();
void butterfly_2737();
void WEIGHTED_ROUND_ROBIN_Joiner_2878();
void WEIGHTED_ROUND_ROBIN_Splitter_2879();
void butterfly_2738();
void butterfly_2739();
void WEIGHTED_ROUND_ROBIN_Joiner_2880();
void WEIGHTED_ROUND_ROBIN_Joiner_2993();
void WEIGHTED_ROUND_ROBIN_Joiner_2994();
void WEIGHTED_ROUND_ROBIN_Splitter_2881();
void WEIGHTED_ROUND_ROBIN_Splitter_2883();
void butterfly_2741();
void butterfly_2742();
void butterfly_2743();
void butterfly_2744();
void WEIGHTED_ROUND_ROBIN_Joiner_2884();
void WEIGHTED_ROUND_ROBIN_Splitter_2885();
void butterfly_2745();
void butterfly_2746();
void butterfly_2747();
void butterfly_2748();
void WEIGHTED_ROUND_ROBIN_Joiner_2886();
void WEIGHTED_ROUND_ROBIN_Joiner_2882();
void WEIGHTED_ROUND_ROBIN_Splitter_3000();
void magnitude_3002();
void magnitude_3003();
void magnitude_3004();
void magnitude_3005();
void magnitude_3006();
void magnitude_3007();
void magnitude_3008();
void magnitude_3009();
void magnitude_3010();
void magnitude_3011();
void magnitude_3012();
void magnitude_3013();
void magnitude_3014();
void magnitude_3015();
void WEIGHTED_ROUND_ROBIN_Joiner_3001();
void sink_2750();

#ifdef __cplusplus
}
#endif
#endif
