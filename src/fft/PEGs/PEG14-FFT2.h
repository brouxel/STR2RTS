#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=3584 on the compile command line
#else
#if BUF_SIZEMAX < 3584
#error BUF_SIZEMAX too small, it must be at least 3584
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float w[2];
} CombineDFT_8607_t;

typedef struct {
	float w[4];
} CombineDFT_8623_t;

typedef struct {
	float w[8];
} CombineDFT_8639_t;

typedef struct {
	float w[16];
} CombineDFT_8649_t;

typedef struct {
	float w[32];
} CombineDFT_8655_t;

typedef struct {
	float w[64];
} CombineDFT_8544_t;
void WEIGHTED_ROUND_ROBIN_Splitter_8565();
void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout);
void FFTTestSource_8567();
void FFTTestSource_8568();
void WEIGHTED_ROUND_ROBIN_Joiner_8566();
void WEIGHTED_ROUND_ROBIN_Splitter_8557();
void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout);
void FFTReorderSimple_8534();
void WEIGHTED_ROUND_ROBIN_Splitter_8569();
void FFTReorderSimple_8571();
void FFTReorderSimple_8572();
void WEIGHTED_ROUND_ROBIN_Joiner_8570();
void WEIGHTED_ROUND_ROBIN_Splitter_8573();
void FFTReorderSimple_8575();
void FFTReorderSimple_8576();
void FFTReorderSimple_8577();
void FFTReorderSimple_8578();
void WEIGHTED_ROUND_ROBIN_Joiner_8574();
void WEIGHTED_ROUND_ROBIN_Splitter_8579();
void FFTReorderSimple_8581();
void FFTReorderSimple_8582();
void FFTReorderSimple_8583();
void FFTReorderSimple_8584();
void FFTReorderSimple_8585();
void FFTReorderSimple_8586();
void FFTReorderSimple_8587();
void FFTReorderSimple_8588();
void WEIGHTED_ROUND_ROBIN_Joiner_8580();
void WEIGHTED_ROUND_ROBIN_Splitter_8589();
void FFTReorderSimple_8591();
void FFTReorderSimple_8592();
void FFTReorderSimple_8593();
void FFTReorderSimple_8594();
void FFTReorderSimple_8595();
void FFTReorderSimple_8596();
void FFTReorderSimple_8597();
void FFTReorderSimple_8598();
void FFTReorderSimple_8599();
void FFTReorderSimple_8600();
void FFTReorderSimple_8601();
void FFTReorderSimple_8602();
void FFTReorderSimple_8603();
void FFTReorderSimple_8604();
void WEIGHTED_ROUND_ROBIN_Joiner_8590();
void WEIGHTED_ROUND_ROBIN_Splitter_8605();
void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout);
void CombineDFT_8607();
void CombineDFT_8608();
void CombineDFT_8609();
void CombineDFT_8610();
void CombineDFT_8611();
void CombineDFT_8612();
void CombineDFT_8613();
void CombineDFT_8614();
void CombineDFT_8615();
void CombineDFT_8616();
void CombineDFT_8617();
void CombineDFT_8618();
void CombineDFT_8619();
void CombineDFT_8620();
void WEIGHTED_ROUND_ROBIN_Joiner_8606();
void WEIGHTED_ROUND_ROBIN_Splitter_8621();
void CombineDFT_8623();
void CombineDFT_8624();
void CombineDFT_8625();
void CombineDFT_8626();
void CombineDFT_8627();
void CombineDFT_8628();
void CombineDFT_8629();
void CombineDFT_8630();
void CombineDFT_8631();
void CombineDFT_8632();
void CombineDFT_8633();
void CombineDFT_8634();
void CombineDFT_8635();
void CombineDFT_8636();
void WEIGHTED_ROUND_ROBIN_Joiner_8622();
void WEIGHTED_ROUND_ROBIN_Splitter_8637();
void CombineDFT_8639();
void CombineDFT_8640();
void CombineDFT_8641();
void CombineDFT_8642();
void CombineDFT_8643();
void CombineDFT_8644();
void CombineDFT_8645();
void CombineDFT_8646();
void WEIGHTED_ROUND_ROBIN_Joiner_8638();
void WEIGHTED_ROUND_ROBIN_Splitter_8647();
void CombineDFT_8649();
void CombineDFT_8650();
void CombineDFT_8651();
void CombineDFT_8652();
void WEIGHTED_ROUND_ROBIN_Joiner_8648();
void WEIGHTED_ROUND_ROBIN_Splitter_8653();
void CombineDFT_8655();
void CombineDFT_8656();
void WEIGHTED_ROUND_ROBIN_Joiner_8654();
void CombineDFT_8544();
void FFTReorderSimple_8545();
void WEIGHTED_ROUND_ROBIN_Splitter_8657();
void FFTReorderSimple_8659();
void FFTReorderSimple_8660();
void WEIGHTED_ROUND_ROBIN_Joiner_8658();
void WEIGHTED_ROUND_ROBIN_Splitter_8661();
void FFTReorderSimple_8663();
void FFTReorderSimple_8664();
void FFTReorderSimple_8665();
void FFTReorderSimple_8666();
void WEIGHTED_ROUND_ROBIN_Joiner_8662();
void WEIGHTED_ROUND_ROBIN_Splitter_8667();
void FFTReorderSimple_8669();
void FFTReorderSimple_8670();
void FFTReorderSimple_8671();
void FFTReorderSimple_8672();
void FFTReorderSimple_8673();
void FFTReorderSimple_8674();
void FFTReorderSimple_8675();
void FFTReorderSimple_8676();
void WEIGHTED_ROUND_ROBIN_Joiner_8668();
void WEIGHTED_ROUND_ROBIN_Splitter_8677();
void FFTReorderSimple_8679();
void FFTReorderSimple_8680();
void FFTReorderSimple_8681();
void FFTReorderSimple_8682();
void FFTReorderSimple_8683();
void FFTReorderSimple_8684();
void FFTReorderSimple_8685();
void FFTReorderSimple_8686();
void FFTReorderSimple_8687();
void FFTReorderSimple_8688();
void FFTReorderSimple_8689();
void FFTReorderSimple_8690();
void FFTReorderSimple_8691();
void FFTReorderSimple_8692();
void WEIGHTED_ROUND_ROBIN_Joiner_8678();
void WEIGHTED_ROUND_ROBIN_Splitter_8693();
void CombineDFT_8695();
void CombineDFT_8696();
void CombineDFT_8697();
void CombineDFT_8698();
void CombineDFT_8699();
void CombineDFT_8700();
void CombineDFT_8701();
void CombineDFT_8702();
void CombineDFT_8703();
void CombineDFT_8704();
void CombineDFT_8705();
void CombineDFT_8706();
void CombineDFT_8707();
void CombineDFT_8708();
void WEIGHTED_ROUND_ROBIN_Joiner_8694();
void WEIGHTED_ROUND_ROBIN_Splitter_8709();
void CombineDFT_8711();
void CombineDFT_8712();
void CombineDFT_8713();
void CombineDFT_8714();
void CombineDFT_8715();
void CombineDFT_8716();
void CombineDFT_8717();
void CombineDFT_8718();
void CombineDFT_8719();
void CombineDFT_8720();
void CombineDFT_8721();
void CombineDFT_8722();
void CombineDFT_8723();
void CombineDFT_8724();
void WEIGHTED_ROUND_ROBIN_Joiner_8710();
void WEIGHTED_ROUND_ROBIN_Splitter_8725();
void CombineDFT_8727();
void CombineDFT_8728();
void CombineDFT_8729();
void CombineDFT_8730();
void CombineDFT_8731();
void CombineDFT_8732();
void CombineDFT_8733();
void CombineDFT_8734();
void WEIGHTED_ROUND_ROBIN_Joiner_8726();
void WEIGHTED_ROUND_ROBIN_Splitter_8735();
void CombineDFT_8737();
void CombineDFT_8738();
void CombineDFT_8739();
void CombineDFT_8740();
void WEIGHTED_ROUND_ROBIN_Joiner_8736();
void WEIGHTED_ROUND_ROBIN_Splitter_8741();
void CombineDFT_8743();
void CombineDFT_8744();
void WEIGHTED_ROUND_ROBIN_Joiner_8742();
void CombineDFT_8555();
void WEIGHTED_ROUND_ROBIN_Joiner_8558();
void FloatPrinter(buffer_float_t *chanin);
void FloatPrinter_8556();

#ifdef __cplusplus
}
#endif
#endif
