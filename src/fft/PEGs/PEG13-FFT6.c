#include "PEG13-FFT6.h"

buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_4385_4395_join[13];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_4387_4397_split[13];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_4389_4399_split[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4373WEIGHTED_ROUND_ROBIN_Splitter_4378;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4308WEIGHTED_ROUND_ROBIN_Splitter_4317;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_4386_4396_join[13];
buffer_complex_t CombineDFT_4294CPrinter_4295;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_4386_4396_split[13];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_4389_4399_join[4];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_4390_4400_split[2];
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_4383_4393_split[4];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_4384_4394_split[8];
buffer_complex_t SplitJoin12_CombineDFT_Fiss_4388_4398_join[8];
buffer_complex_t FFTReorderSimple_4284WEIGHTED_ROUND_ROBIN_Splitter_4297;
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_4382_4392_split[2];
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_4383_4393_join[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4333WEIGHTED_ROUND_ROBIN_Splitter_4347;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4363WEIGHTED_ROUND_ROBIN_Splitter_4372;
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_4382_4392_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4298WEIGHTED_ROUND_ROBIN_Splitter_4301;
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_4385_4395_split[13];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4302WEIGHTED_ROUND_ROBIN_Splitter_4307;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4318WEIGHTED_ROUND_ROBIN_Splitter_4332;
buffer_complex_t SplitJoin16_CombineDFT_Fiss_4390_4400_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4348WEIGHTED_ROUND_ROBIN_Splitter_4362;
buffer_complex_t FFTTestSource_4283FFTReorderSimple_4284;
buffer_complex_t SplitJoin12_CombineDFT_Fiss_4388_4398_split[8];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4379CombineDFT_4294;
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_4384_4394_join[8];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_4387_4397_join[13];


CombineDFT_4334_t CombineDFT_4334_s;
CombineDFT_4334_t CombineDFT_4335_s;
CombineDFT_4334_t CombineDFT_4336_s;
CombineDFT_4334_t CombineDFT_4337_s;
CombineDFT_4334_t CombineDFT_4338_s;
CombineDFT_4334_t CombineDFT_4339_s;
CombineDFT_4334_t CombineDFT_4340_s;
CombineDFT_4334_t CombineDFT_4341_s;
CombineDFT_4334_t CombineDFT_4342_s;
CombineDFT_4334_t CombineDFT_4343_s;
CombineDFT_4334_t CombineDFT_4344_s;
CombineDFT_4334_t CombineDFT_4345_s;
CombineDFT_4334_t CombineDFT_4346_s;
CombineDFT_4334_t CombineDFT_4349_s;
CombineDFT_4334_t CombineDFT_4350_s;
CombineDFT_4334_t CombineDFT_4351_s;
CombineDFT_4334_t CombineDFT_4352_s;
CombineDFT_4334_t CombineDFT_4353_s;
CombineDFT_4334_t CombineDFT_4354_s;
CombineDFT_4334_t CombineDFT_4355_s;
CombineDFT_4334_t CombineDFT_4356_s;
CombineDFT_4334_t CombineDFT_4357_s;
CombineDFT_4334_t CombineDFT_4358_s;
CombineDFT_4334_t CombineDFT_4359_s;
CombineDFT_4334_t CombineDFT_4360_s;
CombineDFT_4334_t CombineDFT_4361_s;
CombineDFT_4334_t CombineDFT_4364_s;
CombineDFT_4334_t CombineDFT_4365_s;
CombineDFT_4334_t CombineDFT_4366_s;
CombineDFT_4334_t CombineDFT_4367_s;
CombineDFT_4334_t CombineDFT_4368_s;
CombineDFT_4334_t CombineDFT_4369_s;
CombineDFT_4334_t CombineDFT_4370_s;
CombineDFT_4334_t CombineDFT_4371_s;
CombineDFT_4334_t CombineDFT_4374_s;
CombineDFT_4334_t CombineDFT_4375_s;
CombineDFT_4334_t CombineDFT_4376_s;
CombineDFT_4334_t CombineDFT_4377_s;
CombineDFT_4334_t CombineDFT_4380_s;
CombineDFT_4334_t CombineDFT_4381_s;
CombineDFT_4334_t CombineDFT_4294_s;

void FFTTestSource(buffer_complex_t *chanout) {
		complex_t c1;
		complex_t zero;
		c1.real = 1.0 ; 
		c1.imag = 0.0 ; 
		zero.real = 0.0 ; 
		zero.imag = 0.0 ; 
		push_complex(&(*chanout), zero) ; 
		push_complex(&(*chanout), c1) ; 
		FOR(int, i, 0,  < , 62, i++) {
			push_complex(&(*chanout), zero) ; 
		}
		ENDFOR
	}


void FFTTestSource_4283() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTTestSource(&(FFTTestSource_4283FFTReorderSimple_4284));
	ENDFOR
}

void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 64, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&(*chanin)) ; 
		}
		ENDFOR
	}


void FFTReorderSimple_4284() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(FFTTestSource_4283FFTReorderSimple_4284), &(FFTReorderSimple_4284WEIGHTED_ROUND_ROBIN_Splitter_4297));
	ENDFOR
}

void FFTReorderSimple_4299() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_4382_4392_split[0]), &(SplitJoin0_FFTReorderSimple_Fiss_4382_4392_join[0]));
	ENDFOR
}

void FFTReorderSimple_4300() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_4382_4392_split[1]), &(SplitJoin0_FFTReorderSimple_Fiss_4382_4392_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4297() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_4382_4392_split[0], pop_complex(&FFTReorderSimple_4284WEIGHTED_ROUND_ROBIN_Splitter_4297));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_4382_4392_split[1], pop_complex(&FFTReorderSimple_4284WEIGHTED_ROUND_ROBIN_Splitter_4297));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4298() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4298WEIGHTED_ROUND_ROBIN_Splitter_4301, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_4382_4392_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4298WEIGHTED_ROUND_ROBIN_Splitter_4301, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_4382_4392_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_4303() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_4383_4393_split[0]), &(SplitJoin2_FFTReorderSimple_Fiss_4383_4393_join[0]));
	ENDFOR
}

void FFTReorderSimple_4304() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_4383_4393_split[1]), &(SplitJoin2_FFTReorderSimple_Fiss_4383_4393_join[1]));
	ENDFOR
}

void FFTReorderSimple_4305() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_4383_4393_split[2]), &(SplitJoin2_FFTReorderSimple_Fiss_4383_4393_join[2]));
	ENDFOR
}

void FFTReorderSimple_4306() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_4383_4393_split[3]), &(SplitJoin2_FFTReorderSimple_Fiss_4383_4393_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4301() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin2_FFTReorderSimple_Fiss_4383_4393_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4298WEIGHTED_ROUND_ROBIN_Splitter_4301));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4302() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4302WEIGHTED_ROUND_ROBIN_Splitter_4307, pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_4383_4393_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_4309() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_4384_4394_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_4384_4394_join[0]));
	ENDFOR
}

void FFTReorderSimple_4310() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_4384_4394_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_4384_4394_join[1]));
	ENDFOR
}

void FFTReorderSimple_4311() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_4384_4394_split[2]), &(SplitJoin4_FFTReorderSimple_Fiss_4384_4394_join[2]));
	ENDFOR
}

void FFTReorderSimple_4312() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_4384_4394_split[3]), &(SplitJoin4_FFTReorderSimple_Fiss_4384_4394_join[3]));
	ENDFOR
}

void FFTReorderSimple_4313() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_4384_4394_split[4]), &(SplitJoin4_FFTReorderSimple_Fiss_4384_4394_join[4]));
	ENDFOR
}

void FFTReorderSimple_4314() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_4384_4394_split[5]), &(SplitJoin4_FFTReorderSimple_Fiss_4384_4394_join[5]));
	ENDFOR
}

void FFTReorderSimple_4315() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_4384_4394_split[6]), &(SplitJoin4_FFTReorderSimple_Fiss_4384_4394_join[6]));
	ENDFOR
}

void FFTReorderSimple_4316() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_4384_4394_split[7]), &(SplitJoin4_FFTReorderSimple_Fiss_4384_4394_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4307() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4384_4394_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4302WEIGHTED_ROUND_ROBIN_Splitter_4307));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4308() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4308WEIGHTED_ROUND_ROBIN_Splitter_4317, pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_4384_4394_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_4319() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4385_4395_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_4385_4395_join[0]));
	ENDFOR
}

void FFTReorderSimple_4320() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4385_4395_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_4385_4395_join[1]));
	ENDFOR
}

void FFTReorderSimple_4321() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4385_4395_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_4385_4395_join[2]));
	ENDFOR
}

void FFTReorderSimple_4322() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4385_4395_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_4385_4395_join[3]));
	ENDFOR
}

void FFTReorderSimple_4323() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4385_4395_split[4]), &(SplitJoin6_FFTReorderSimple_Fiss_4385_4395_join[4]));
	ENDFOR
}

void FFTReorderSimple_4324() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4385_4395_split[5]), &(SplitJoin6_FFTReorderSimple_Fiss_4385_4395_join[5]));
	ENDFOR
}

void FFTReorderSimple_4325() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4385_4395_split[6]), &(SplitJoin6_FFTReorderSimple_Fiss_4385_4395_join[6]));
	ENDFOR
}

void FFTReorderSimple_4326() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4385_4395_split[7]), &(SplitJoin6_FFTReorderSimple_Fiss_4385_4395_join[7]));
	ENDFOR
}

void FFTReorderSimple_4327() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4385_4395_split[8]), &(SplitJoin6_FFTReorderSimple_Fiss_4385_4395_join[8]));
	ENDFOR
}

void FFTReorderSimple_4328() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4385_4395_split[9]), &(SplitJoin6_FFTReorderSimple_Fiss_4385_4395_join[9]));
	ENDFOR
}

void FFTReorderSimple_4329() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4385_4395_split[10]), &(SplitJoin6_FFTReorderSimple_Fiss_4385_4395_join[10]));
	ENDFOR
}

void FFTReorderSimple_4330() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4385_4395_split[11]), &(SplitJoin6_FFTReorderSimple_Fiss_4385_4395_join[11]));
	ENDFOR
}

void FFTReorderSimple_4331() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4385_4395_split[12]), &(SplitJoin6_FFTReorderSimple_Fiss_4385_4395_join[12]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4317() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 13, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4385_4395_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4308WEIGHTED_ROUND_ROBIN_Splitter_4317));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4318() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 13, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4318WEIGHTED_ROUND_ROBIN_Splitter_4332, pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_4385_4395_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&(*chanin), (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4334_s.wn.real) - (w.imag * CombineDFT_4334_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4334_s.wn.imag) + (w.imag * CombineDFT_4334_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&(*chanin)) ; 
			push_complex(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_4334() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4386_4396_split[0]), &(SplitJoin8_CombineDFT_Fiss_4386_4396_join[0]));
	ENDFOR
}

void CombineDFT_4335() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4386_4396_split[1]), &(SplitJoin8_CombineDFT_Fiss_4386_4396_join[1]));
	ENDFOR
}

void CombineDFT_4336() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4386_4396_split[2]), &(SplitJoin8_CombineDFT_Fiss_4386_4396_join[2]));
	ENDFOR
}

void CombineDFT_4337() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4386_4396_split[3]), &(SplitJoin8_CombineDFT_Fiss_4386_4396_join[3]));
	ENDFOR
}

void CombineDFT_4338() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4386_4396_split[4]), &(SplitJoin8_CombineDFT_Fiss_4386_4396_join[4]));
	ENDFOR
}

void CombineDFT_4339() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4386_4396_split[5]), &(SplitJoin8_CombineDFT_Fiss_4386_4396_join[5]));
	ENDFOR
}

void CombineDFT_4340() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4386_4396_split[6]), &(SplitJoin8_CombineDFT_Fiss_4386_4396_join[6]));
	ENDFOR
}

void CombineDFT_4341() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4386_4396_split[7]), &(SplitJoin8_CombineDFT_Fiss_4386_4396_join[7]));
	ENDFOR
}

void CombineDFT_4342() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4386_4396_split[8]), &(SplitJoin8_CombineDFT_Fiss_4386_4396_join[8]));
	ENDFOR
}

void CombineDFT_4343() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4386_4396_split[9]), &(SplitJoin8_CombineDFT_Fiss_4386_4396_join[9]));
	ENDFOR
}

void CombineDFT_4344() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4386_4396_split[10]), &(SplitJoin8_CombineDFT_Fiss_4386_4396_join[10]));
	ENDFOR
}

void CombineDFT_4345() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4386_4396_split[11]), &(SplitJoin8_CombineDFT_Fiss_4386_4396_join[11]));
	ENDFOR
}

void CombineDFT_4346() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4386_4396_split[12]), &(SplitJoin8_CombineDFT_Fiss_4386_4396_join[12]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4332() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 13, __iter_++)
			push_complex(&SplitJoin8_CombineDFT_Fiss_4386_4396_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4318WEIGHTED_ROUND_ROBIN_Splitter_4332));
			push_complex(&SplitJoin8_CombineDFT_Fiss_4386_4396_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4318WEIGHTED_ROUND_ROBIN_Splitter_4332));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4333() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 13, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4333WEIGHTED_ROUND_ROBIN_Splitter_4347, pop_complex(&SplitJoin8_CombineDFT_Fiss_4386_4396_join[__iter_]));
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4333WEIGHTED_ROUND_ROBIN_Splitter_4347, pop_complex(&SplitJoin8_CombineDFT_Fiss_4386_4396_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_4349() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4387_4397_split[0]), &(SplitJoin10_CombineDFT_Fiss_4387_4397_join[0]));
	ENDFOR
}

void CombineDFT_4350() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4387_4397_split[1]), &(SplitJoin10_CombineDFT_Fiss_4387_4397_join[1]));
	ENDFOR
}

void CombineDFT_4351() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4387_4397_split[2]), &(SplitJoin10_CombineDFT_Fiss_4387_4397_join[2]));
	ENDFOR
}

void CombineDFT_4352() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4387_4397_split[3]), &(SplitJoin10_CombineDFT_Fiss_4387_4397_join[3]));
	ENDFOR
}

void CombineDFT_4353() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4387_4397_split[4]), &(SplitJoin10_CombineDFT_Fiss_4387_4397_join[4]));
	ENDFOR
}

void CombineDFT_4354() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4387_4397_split[5]), &(SplitJoin10_CombineDFT_Fiss_4387_4397_join[5]));
	ENDFOR
}

void CombineDFT_4355() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4387_4397_split[6]), &(SplitJoin10_CombineDFT_Fiss_4387_4397_join[6]));
	ENDFOR
}

void CombineDFT_4356() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4387_4397_split[7]), &(SplitJoin10_CombineDFT_Fiss_4387_4397_join[7]));
	ENDFOR
}

void CombineDFT_4357() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4387_4397_split[8]), &(SplitJoin10_CombineDFT_Fiss_4387_4397_join[8]));
	ENDFOR
}

void CombineDFT_4358() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4387_4397_split[9]), &(SplitJoin10_CombineDFT_Fiss_4387_4397_join[9]));
	ENDFOR
}

void CombineDFT_4359() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4387_4397_split[10]), &(SplitJoin10_CombineDFT_Fiss_4387_4397_join[10]));
	ENDFOR
}

void CombineDFT_4360() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4387_4397_split[11]), &(SplitJoin10_CombineDFT_Fiss_4387_4397_join[11]));
	ENDFOR
}

void CombineDFT_4361() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4387_4397_split[12]), &(SplitJoin10_CombineDFT_Fiss_4387_4397_join[12]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4347() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 13, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin10_CombineDFT_Fiss_4387_4397_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4333WEIGHTED_ROUND_ROBIN_Splitter_4347));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4348() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 13, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4348WEIGHTED_ROUND_ROBIN_Splitter_4362, pop_complex(&SplitJoin10_CombineDFT_Fiss_4387_4397_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_4364() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_4388_4398_split[0]), &(SplitJoin12_CombineDFT_Fiss_4388_4398_join[0]));
	ENDFOR
}

void CombineDFT_4365() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_4388_4398_split[1]), &(SplitJoin12_CombineDFT_Fiss_4388_4398_join[1]));
	ENDFOR
}

void CombineDFT_4366() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_4388_4398_split[2]), &(SplitJoin12_CombineDFT_Fiss_4388_4398_join[2]));
	ENDFOR
}

void CombineDFT_4367() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_4388_4398_split[3]), &(SplitJoin12_CombineDFT_Fiss_4388_4398_join[3]));
	ENDFOR
}

void CombineDFT_4368() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_4388_4398_split[4]), &(SplitJoin12_CombineDFT_Fiss_4388_4398_join[4]));
	ENDFOR
}

void CombineDFT_4369() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_4388_4398_split[5]), &(SplitJoin12_CombineDFT_Fiss_4388_4398_join[5]));
	ENDFOR
}

void CombineDFT_4370() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_4388_4398_split[6]), &(SplitJoin12_CombineDFT_Fiss_4388_4398_join[6]));
	ENDFOR
}

void CombineDFT_4371() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_4388_4398_split[7]), &(SplitJoin12_CombineDFT_Fiss_4388_4398_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4362() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin12_CombineDFT_Fiss_4388_4398_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4348WEIGHTED_ROUND_ROBIN_Splitter_4362));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4363() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4363WEIGHTED_ROUND_ROBIN_Splitter_4372, pop_complex(&SplitJoin12_CombineDFT_Fiss_4388_4398_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_4374() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_4389_4399_split[0]), &(SplitJoin14_CombineDFT_Fiss_4389_4399_join[0]));
	ENDFOR
}

void CombineDFT_4375() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_4389_4399_split[1]), &(SplitJoin14_CombineDFT_Fiss_4389_4399_join[1]));
	ENDFOR
}

void CombineDFT_4376() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_4389_4399_split[2]), &(SplitJoin14_CombineDFT_Fiss_4389_4399_join[2]));
	ENDFOR
}

void CombineDFT_4377() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_4389_4399_split[3]), &(SplitJoin14_CombineDFT_Fiss_4389_4399_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4372() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin14_CombineDFT_Fiss_4389_4399_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4363WEIGHTED_ROUND_ROBIN_Splitter_4372));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4373() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4373WEIGHTED_ROUND_ROBIN_Splitter_4378, pop_complex(&SplitJoin14_CombineDFT_Fiss_4389_4399_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_4380() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_4390_4400_split[0]), &(SplitJoin16_CombineDFT_Fiss_4390_4400_join[0]));
	ENDFOR
}

void CombineDFT_4381() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_4390_4400_split[1]), &(SplitJoin16_CombineDFT_Fiss_4390_4400_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4378() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_4390_4400_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4373WEIGHTED_ROUND_ROBIN_Splitter_4378));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_4390_4400_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4373WEIGHTED_ROUND_ROBIN_Splitter_4378));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4379() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4379CombineDFT_4294, pop_complex(&SplitJoin16_CombineDFT_Fiss_4390_4400_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4379CombineDFT_4294, pop_complex(&SplitJoin16_CombineDFT_Fiss_4390_4400_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_4294() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_4379CombineDFT_4294), &(CombineDFT_4294CPrinter_4295));
	ENDFOR
}

void CPrinter(buffer_complex_t *chanin) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		printf("%.10f", c.real);
		printf("\n");
		printf("%.10f", c.imag);
		printf("\n");
	}


void CPrinter_4295() {
	FOR(uint32_t, __iter_steady_, 0, <, 832, __iter_steady_++)
		CPrinter(&(CombineDFT_4294CPrinter_4295));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 13, __iter_init_0_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_4385_4395_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 13, __iter_init_1_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_4387_4397_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 4, __iter_init_2_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_4389_4399_split[__iter_init_2_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4373WEIGHTED_ROUND_ROBIN_Splitter_4378);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4308WEIGHTED_ROUND_ROBIN_Splitter_4317);
	FOR(int, __iter_init_3_, 0, <, 13, __iter_init_3_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_4386_4396_join[__iter_init_3_]);
	ENDFOR
	init_buffer_complex(&CombineDFT_4294CPrinter_4295);
	FOR(int, __iter_init_4_, 0, <, 13, __iter_init_4_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_4386_4396_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 4, __iter_init_5_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_4389_4399_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 2, __iter_init_6_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_4390_4400_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 4, __iter_init_7_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_4383_4393_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_4384_4394_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_4388_4398_join[__iter_init_9_]);
	ENDFOR
	init_buffer_complex(&FFTReorderSimple_4284WEIGHTED_ROUND_ROBIN_Splitter_4297);
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_4382_4392_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 4, __iter_init_11_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_4383_4393_join[__iter_init_11_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4333WEIGHTED_ROUND_ROBIN_Splitter_4347);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4363WEIGHTED_ROUND_ROBIN_Splitter_4372);
	FOR(int, __iter_init_12_, 0, <, 2, __iter_init_12_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_4382_4392_join[__iter_init_12_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4298WEIGHTED_ROUND_ROBIN_Splitter_4301);
	FOR(int, __iter_init_13_, 0, <, 13, __iter_init_13_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_4385_4395_split[__iter_init_13_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4302WEIGHTED_ROUND_ROBIN_Splitter_4307);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4318WEIGHTED_ROUND_ROBIN_Splitter_4332);
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_4390_4400_join[__iter_init_14_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4348WEIGHTED_ROUND_ROBIN_Splitter_4362);
	init_buffer_complex(&FFTTestSource_4283FFTReorderSimple_4284);
	FOR(int, __iter_init_15_, 0, <, 8, __iter_init_15_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_4388_4398_split[__iter_init_15_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4379CombineDFT_4294);
	FOR(int, __iter_init_16_, 0, <, 8, __iter_init_16_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_4384_4394_join[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 13, __iter_init_17_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_4387_4397_join[__iter_init_17_]);
	ENDFOR
// --- init: CombineDFT_4334
	 {
	 ; 
	CombineDFT_4334_s.wn.real = -1.0 ; 
	CombineDFT_4334_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4335
	 {
	CombineDFT_4335_s.wn.real = -1.0 ; 
	CombineDFT_4335_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4336
	 {
	CombineDFT_4336_s.wn.real = -1.0 ; 
	CombineDFT_4336_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4337
	 {
	CombineDFT_4337_s.wn.real = -1.0 ; 
	CombineDFT_4337_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4338
	 {
	CombineDFT_4338_s.wn.real = -1.0 ; 
	CombineDFT_4338_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4339
	 {
	CombineDFT_4339_s.wn.real = -1.0 ; 
	CombineDFT_4339_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4340
	 {
	CombineDFT_4340_s.wn.real = -1.0 ; 
	CombineDFT_4340_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4341
	 {
	CombineDFT_4341_s.wn.real = -1.0 ; 
	CombineDFT_4341_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4342
	 {
	CombineDFT_4342_s.wn.real = -1.0 ; 
	CombineDFT_4342_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4343
	 {
	CombineDFT_4343_s.wn.real = -1.0 ; 
	CombineDFT_4343_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4344
	 {
	CombineDFT_4344_s.wn.real = -1.0 ; 
	CombineDFT_4344_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4345
	 {
	CombineDFT_4345_s.wn.real = -1.0 ; 
	CombineDFT_4345_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4346
	 {
	CombineDFT_4346_s.wn.real = -1.0 ; 
	CombineDFT_4346_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4349
	 {
	CombineDFT_4349_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4349_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4350
	 {
	CombineDFT_4350_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4350_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4351
	 {
	CombineDFT_4351_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4351_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4352
	 {
	CombineDFT_4352_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4352_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4353
	 {
	CombineDFT_4353_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4353_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4354
	 {
	CombineDFT_4354_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4354_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4355
	 {
	CombineDFT_4355_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4355_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4356
	 {
	CombineDFT_4356_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4356_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4357
	 {
	CombineDFT_4357_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4357_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4358
	 {
	CombineDFT_4358_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4358_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4359
	 {
	CombineDFT_4359_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4359_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4360
	 {
	CombineDFT_4360_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4360_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4361
	 {
	CombineDFT_4361_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4361_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4364
	 {
	CombineDFT_4364_s.wn.real = 0.70710677 ; 
	CombineDFT_4364_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4365
	 {
	CombineDFT_4365_s.wn.real = 0.70710677 ; 
	CombineDFT_4365_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4366
	 {
	CombineDFT_4366_s.wn.real = 0.70710677 ; 
	CombineDFT_4366_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4367
	 {
	CombineDFT_4367_s.wn.real = 0.70710677 ; 
	CombineDFT_4367_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4368
	 {
	CombineDFT_4368_s.wn.real = 0.70710677 ; 
	CombineDFT_4368_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4369
	 {
	CombineDFT_4369_s.wn.real = 0.70710677 ; 
	CombineDFT_4369_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4370
	 {
	CombineDFT_4370_s.wn.real = 0.70710677 ; 
	CombineDFT_4370_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4371
	 {
	CombineDFT_4371_s.wn.real = 0.70710677 ; 
	CombineDFT_4371_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4374
	 {
	CombineDFT_4374_s.wn.real = 0.9238795 ; 
	CombineDFT_4374_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_4375
	 {
	CombineDFT_4375_s.wn.real = 0.9238795 ; 
	CombineDFT_4375_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_4376
	 {
	CombineDFT_4376_s.wn.real = 0.9238795 ; 
	CombineDFT_4376_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_4377
	 {
	CombineDFT_4377_s.wn.real = 0.9238795 ; 
	CombineDFT_4377_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_4380
	 {
	CombineDFT_4380_s.wn.real = 0.98078525 ; 
	CombineDFT_4380_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_4381
	 {
	CombineDFT_4381_s.wn.real = 0.98078525 ; 
	CombineDFT_4381_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_4294
	 {
	 ; 
	CombineDFT_4294_s.wn.real = 0.9951847 ; 
	CombineDFT_4294_s.wn.imag = -0.09801714 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FFTTestSource_4283();
		FFTReorderSimple_4284();
		WEIGHTED_ROUND_ROBIN_Splitter_4297();
			FFTReorderSimple_4299();
			FFTReorderSimple_4300();
		WEIGHTED_ROUND_ROBIN_Joiner_4298();
		WEIGHTED_ROUND_ROBIN_Splitter_4301();
			FFTReorderSimple_4303();
			FFTReorderSimple_4304();
			FFTReorderSimple_4305();
			FFTReorderSimple_4306();
		WEIGHTED_ROUND_ROBIN_Joiner_4302();
		WEIGHTED_ROUND_ROBIN_Splitter_4307();
			FFTReorderSimple_4309();
			FFTReorderSimple_4310();
			FFTReorderSimple_4311();
			FFTReorderSimple_4312();
			FFTReorderSimple_4313();
			FFTReorderSimple_4314();
			FFTReorderSimple_4315();
			FFTReorderSimple_4316();
		WEIGHTED_ROUND_ROBIN_Joiner_4308();
		WEIGHTED_ROUND_ROBIN_Splitter_4317();
			FFTReorderSimple_4319();
			FFTReorderSimple_4320();
			FFTReorderSimple_4321();
			FFTReorderSimple_4322();
			FFTReorderSimple_4323();
			FFTReorderSimple_4324();
			FFTReorderSimple_4325();
			FFTReorderSimple_4326();
			FFTReorderSimple_4327();
			FFTReorderSimple_4328();
			FFTReorderSimple_4329();
			FFTReorderSimple_4330();
			FFTReorderSimple_4331();
		WEIGHTED_ROUND_ROBIN_Joiner_4318();
		WEIGHTED_ROUND_ROBIN_Splitter_4332();
			CombineDFT_4334();
			CombineDFT_4335();
			CombineDFT_4336();
			CombineDFT_4337();
			CombineDFT_4338();
			CombineDFT_4339();
			CombineDFT_4340();
			CombineDFT_4341();
			CombineDFT_4342();
			CombineDFT_4343();
			CombineDFT_4344();
			CombineDFT_4345();
			CombineDFT_4346();
		WEIGHTED_ROUND_ROBIN_Joiner_4333();
		WEIGHTED_ROUND_ROBIN_Splitter_4347();
			CombineDFT_4349();
			CombineDFT_4350();
			CombineDFT_4351();
			CombineDFT_4352();
			CombineDFT_4353();
			CombineDFT_4354();
			CombineDFT_4355();
			CombineDFT_4356();
			CombineDFT_4357();
			CombineDFT_4358();
			CombineDFT_4359();
			CombineDFT_4360();
			CombineDFT_4361();
		WEIGHTED_ROUND_ROBIN_Joiner_4348();
		WEIGHTED_ROUND_ROBIN_Splitter_4362();
			CombineDFT_4364();
			CombineDFT_4365();
			CombineDFT_4366();
			CombineDFT_4367();
			CombineDFT_4368();
			CombineDFT_4369();
			CombineDFT_4370();
			CombineDFT_4371();
		WEIGHTED_ROUND_ROBIN_Joiner_4363();
		WEIGHTED_ROUND_ROBIN_Splitter_4372();
			CombineDFT_4374();
			CombineDFT_4375();
			CombineDFT_4376();
			CombineDFT_4377();
		WEIGHTED_ROUND_ROBIN_Joiner_4373();
		WEIGHTED_ROUND_ROBIN_Splitter_4378();
			CombineDFT_4380();
			CombineDFT_4381();
		WEIGHTED_ROUND_ROBIN_Joiner_4379();
		CombineDFT_4294();
		CPrinter_4295();
	ENDFOR
	return EXIT_SUCCESS;
}
