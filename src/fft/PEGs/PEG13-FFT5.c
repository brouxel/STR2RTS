#include "PEG13-FFT5.h"

buffer_complex_t SplitJoin20_SplitJoin14_SplitJoin14_split1_3254_3498_3622_3661_split[2];
buffer_complex_t SplitJoin79_SplitJoin55_SplitJoin55_AnonFilter_a0_3282_3531_3626_3642_split[2];
buffer_complex_t SplitJoin22_SplitJoin16_SplitJoin16_split2_3256_3499_3580_3662_join[4];
buffer_complex_t SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3262_3488_3617_3636_join[2];
buffer_complex_t butterfly_3329Post_CollapsedDataParallel_2_3433;
buffer_complex_t SplitJoin24_magnitude_Fiss_3623_3664_split[13];
buffer_complex_t SplitJoin20_SplitJoin14_SplitJoin14_split1_3254_3498_3622_3661_join[2];
buffer_complex_t SplitJoin87_SplitJoin63_SplitJoin63_AnonFilter_a0_3294_3537_3573_3644_split[2];
buffer_complex_t SplitJoin53_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child1_3583_3658_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_3429butterfly_3328;
buffer_complex_t butterfly_3331Post_CollapsedDataParallel_2_3439;
buffer_complex_t SplitJoin91_SplitJoin67_SplitJoin67_AnonFilter_a0_3298_3539_3629_3646_join[2];
buffer_complex_t SplitJoin49_SplitJoin29_SplitJoin29_split2_3245_3510_3576_3657_join[2];
buffer_complex_t butterfly_3328Post_CollapsedDataParallel_2_3430;
buffer_complex_t SplitJoin95_SplitJoin71_SplitJoin71_AnonFilter_a0_3304_3542_3630_3647_split[2];
buffer_complex_t SplitJoin101_SplitJoin77_SplitJoin77_AnonFilter_a0_3312_3546_3632_3649_join[2];
buffer_complex_t SplitJoin95_SplitJoin71_SplitJoin71_AnonFilter_a0_3304_3542_3630_3647_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_3420butterfly_3325;
buffer_complex_t SplitJoin66_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child1_3578_3653_split[4];
buffer_complex_t butterfly_3327Post_CollapsedDataParallel_2_3427;
buffer_complex_t Pre_CollapsedDataParallel_1_3435butterfly_3330;
buffer_complex_t Pre_CollapsedDataParallel_1_3426butterfly_3327;
buffer_complex_t SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3266_3490_3618_3638_join[2];
buffer_complex_t SplitJoin77_SplitJoin53_SplitJoin53_AnonFilter_a0_3280_3530_3625_3641_join[2];
buffer_complex_t SplitJoin10_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_Hier_3620_3651_split[2];
buffer_complex_t SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3266_3490_3618_3638_split[2];
buffer_complex_t SplitJoin12_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child0_3575_3652_join[4];
buffer_complex_t SplitJoin22_SplitJoin16_SplitJoin16_split2_3256_3499_3580_3662_split[4];
buffer_complex_t SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3262_3488_3617_3636_split[2];
buffer_complex_t SplitJoin73_SplitJoin49_SplitJoin49_AnonFilter_a0_3274_3527_3624_3640_split[2];
buffer_complex_t butterfly_3325Post_CollapsedDataParallel_2_3421;
buffer_complex_t butterfly_3330Post_CollapsedDataParallel_2_3436;
buffer_complex_t SplitJoin55_SplitJoin33_SplitJoin33_split2_3247_3513_3577_3659_split[2];
buffer_complex_t Pre_CollapsedDataParallel_1_3432butterfly_3329;
buffer_complex_t SplitJoin79_SplitJoin55_SplitJoin55_AnonFilter_a0_3282_3531_3626_3642_join[2];
buffer_complex_t SplitJoin105_SplitJoin81_SplitJoin81_AnonFilter_a0_3318_3549_3633_3650_split[2];
buffer_complex_t SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_3268_3491_3619_3639_split[2];
buffer_complex_t SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_3268_3491_3619_3639_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3595WEIGHTED_ROUND_ROBIN_Splitter_3482;
buffer_complex_t SplitJoin83_SplitJoin59_SplitJoin59_AnonFilter_a0_3288_3534_3627_3643_join[2];
buffer_complex_t SplitJoin10_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_Hier_3620_3651_join[2];
buffer_complex_t SplitJoin83_SplitJoin59_SplitJoin59_AnonFilter_a0_3288_3534_3627_3643_split[2];
buffer_complex_t SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3264_3489_3572_3637_join[2];
buffer_complex_t SplitJoin89_SplitJoin65_SplitJoin65_AnonFilter_a0_3296_3538_3628_3645_split[2];
buffer_complex_t SplitJoin49_SplitJoin29_SplitJoin29_split2_3245_3510_3576_3657_split[2];
buffer_complex_t SplitJoin0_source_Fiss_3616_3635_split[2];
buffer_complex_t SplitJoin16_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child0_3581_3655_join[2];
buffer_complex_t butterfly_3324Post_CollapsedDataParallel_2_3418;
buffer_float_t SplitJoin24_magnitude_Fiss_3623_3664_join[13];
buffer_complex_t SplitJoin0_source_Fiss_3616_3635_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3598WEIGHTED_ROUND_ROBIN_Splitter_3440;
buffer_complex_t SplitJoin99_SplitJoin75_SplitJoin75_AnonFilter_a0_3310_3545_3631_3648_split[2];
buffer_complex_t SplitJoin59_SplitJoin37_SplitJoin37_split2_3249_3516_3579_3660_join[2];
buffer_complex_t SplitJoin89_SplitJoin65_SplitJoin65_AnonFilter_a0_3296_3538_3628_3645_join[2];
buffer_complex_t SplitJoin91_SplitJoin67_SplitJoin67_AnonFilter_a0_3298_3539_3629_3646_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3483WEIGHTED_ROUND_ROBIN_Splitter_3601;
buffer_complex_t SplitJoin73_SplitJoin49_SplitJoin49_AnonFilter_a0_3274_3527_3624_3640_join[2];
buffer_complex_t SplitJoin55_SplitJoin33_SplitJoin33_split2_3247_3513_3577_3659_join[2];
buffer_complex_t SplitJoin42_SplitJoin22_SplitJoin22_split2_3258_3504_3582_3663_join[4];
buffer_complex_t SplitJoin18_SplitJoin12_SplitJoin12_split2_3243_3496_3574_3656_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3441WEIGHTED_ROUND_ROBIN_Splitter_3584;
buffer_complex_t SplitJoin77_SplitJoin53_SplitJoin53_AnonFilter_a0_3280_3530_3625_3641_split[2];
buffer_complex_t SplitJoin101_SplitJoin77_SplitJoin77_AnonFilter_a0_3312_3546_3632_3649_split[2];
buffer_complex_t SplitJoin66_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child1_3578_3653_join[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_3589WEIGHTED_ROUND_ROBIN_Splitter_3590;
buffer_complex_t SplitJoin14_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_Hier_3621_3654_split[2];
buffer_complex_t SplitJoin12_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child0_3575_3652_split[4];
buffer_complex_t SplitJoin105_SplitJoin81_SplitJoin81_AnonFilter_a0_3318_3549_3633_3650_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_3438butterfly_3331;
buffer_complex_t butterfly_3326Post_CollapsedDataParallel_2_3424;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3602sink_3351;
buffer_complex_t Pre_CollapsedDataParallel_1_3417butterfly_3324;
buffer_complex_t SplitJoin18_SplitJoin12_SplitJoin12_split2_3243_3496_3574_3656_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_3423butterfly_3326;
buffer_complex_t SplitJoin16_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child0_3581_3655_split[2];
buffer_complex_t SplitJoin59_SplitJoin37_SplitJoin37_split2_3249_3516_3579_3660_split[2];
buffer_complex_t SplitJoin42_SplitJoin22_SplitJoin22_split2_3258_3504_3582_3663_split[4];
buffer_complex_t SplitJoin99_SplitJoin75_SplitJoin75_AnonFilter_a0_3310_3545_3631_3648_join[2];
buffer_complex_t SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3264_3489_3572_3637_split[2];
buffer_complex_t SplitJoin53_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child1_3583_3658_split[2];
buffer_complex_t SplitJoin87_SplitJoin63_SplitJoin63_AnonFilter_a0_3294_3537_3573_3644_join[2];
buffer_complex_t SplitJoin14_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_Hier_3621_3654_join[2];



void source(buffer_void_t *chanin, buffer_complex_t *chanout) {
		complex_t t;
		t.imag = 0.0 ; 
		t.real = 0.9501 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.2311 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.6068 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.486 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.8913 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.7621 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.4565 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.0185 ; 
		push_complex(&(*chanout), t) ; 
	}


void source_3599() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		source(&(SplitJoin0_source_Fiss_3616_3635_split[0]), &(SplitJoin0_source_Fiss_3616_3635_join[0]));
	ENDFOR
}

void source_3600() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		source(&(SplitJoin0_source_Fiss_3616_3635_split[1]), &(SplitJoin0_source_Fiss_3616_3635_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3597() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_3598() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3598WEIGHTED_ROUND_ROBIN_Splitter_3440, pop_complex(&SplitJoin0_source_Fiss_3616_3635_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3598WEIGHTED_ROUND_ROBIN_Splitter_3440, pop_complex(&SplitJoin0_source_Fiss_3616_3635_join[1]));
		ENDFOR
	ENDFOR
}}

void Identity(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t __tmp942 = pop_complex(&(*chanin));
		push_complex(&(*chanout), __tmp942) ; 
	}


void Identity_3270() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Identity(&(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_3268_3491_3619_3639_split[0]), &(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_3268_3491_3619_3639_join[0]));
	ENDFOR
}

void Identity_3272() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Identity(&(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_3268_3491_3619_3639_split[1]), &(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_3268_3491_3619_3639_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3446() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_3268_3491_3619_3639_split[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3266_3490_3618_3638_split[0]));
		push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_3268_3491_3619_3639_split[1], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3266_3490_3618_3638_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3447() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3266_3490_3618_3638_join[0], pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_3268_3491_3619_3639_join[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3266_3490_3618_3638_join[0], pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_3268_3491_3619_3639_join[1]));
	ENDFOR
}}

void Identity_3276() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Identity(&(SplitJoin73_SplitJoin49_SplitJoin49_AnonFilter_a0_3274_3527_3624_3640_split[0]), &(SplitJoin73_SplitJoin49_SplitJoin49_AnonFilter_a0_3274_3527_3624_3640_join[0]));
	ENDFOR
}

void Identity_3278() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Identity(&(SplitJoin73_SplitJoin49_SplitJoin49_AnonFilter_a0_3274_3527_3624_3640_split[1]), &(SplitJoin73_SplitJoin49_SplitJoin49_AnonFilter_a0_3274_3527_3624_3640_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3448() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		push_complex(&SplitJoin73_SplitJoin49_SplitJoin49_AnonFilter_a0_3274_3527_3624_3640_split[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3266_3490_3618_3638_split[1]));
		push_complex(&SplitJoin73_SplitJoin49_SplitJoin49_AnonFilter_a0_3274_3527_3624_3640_split[1], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3266_3490_3618_3638_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3449() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3266_3490_3618_3638_join[1], pop_complex(&SplitJoin73_SplitJoin49_SplitJoin49_AnonFilter_a0_3274_3527_3624_3640_join[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3266_3490_3618_3638_join[1], pop_complex(&SplitJoin73_SplitJoin49_SplitJoin49_AnonFilter_a0_3274_3527_3624_3640_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_3444() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 26, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3266_3490_3618_3638_split[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3264_3489_3572_3637_split[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3266_3490_3618_3638_split[1], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3264_3489_3572_3637_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3445() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3264_3489_3572_3637_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3266_3490_3618_3638_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3264_3489_3572_3637_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3266_3490_3618_3638_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3264_3489_3572_3637_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3266_3490_3618_3638_join[1]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3264_3489_3572_3637_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3266_3490_3618_3638_join[1]));
	ENDFOR
}}

void Identity_3284() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Identity(&(SplitJoin79_SplitJoin55_SplitJoin55_AnonFilter_a0_3282_3531_3626_3642_split[0]), &(SplitJoin79_SplitJoin55_SplitJoin55_AnonFilter_a0_3282_3531_3626_3642_join[0]));
	ENDFOR
}

void Identity_3286() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Identity(&(SplitJoin79_SplitJoin55_SplitJoin55_AnonFilter_a0_3282_3531_3626_3642_split[1]), &(SplitJoin79_SplitJoin55_SplitJoin55_AnonFilter_a0_3282_3531_3626_3642_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3452() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		push_complex(&SplitJoin79_SplitJoin55_SplitJoin55_AnonFilter_a0_3282_3531_3626_3642_split[0], pop_complex(&SplitJoin77_SplitJoin53_SplitJoin53_AnonFilter_a0_3280_3530_3625_3641_split[0]));
		push_complex(&SplitJoin79_SplitJoin55_SplitJoin55_AnonFilter_a0_3282_3531_3626_3642_split[1], pop_complex(&SplitJoin77_SplitJoin53_SplitJoin53_AnonFilter_a0_3280_3530_3625_3641_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3453() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		push_complex(&SplitJoin77_SplitJoin53_SplitJoin53_AnonFilter_a0_3280_3530_3625_3641_join[0], pop_complex(&SplitJoin79_SplitJoin55_SplitJoin55_AnonFilter_a0_3282_3531_3626_3642_join[0]));
		push_complex(&SplitJoin77_SplitJoin53_SplitJoin53_AnonFilter_a0_3280_3530_3625_3641_join[0], pop_complex(&SplitJoin79_SplitJoin55_SplitJoin55_AnonFilter_a0_3282_3531_3626_3642_join[1]));
	ENDFOR
}}

void Identity_3290() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Identity(&(SplitJoin83_SplitJoin59_SplitJoin59_AnonFilter_a0_3288_3534_3627_3643_split[0]), &(SplitJoin83_SplitJoin59_SplitJoin59_AnonFilter_a0_3288_3534_3627_3643_join[0]));
	ENDFOR
}

void Identity_3292() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Identity(&(SplitJoin83_SplitJoin59_SplitJoin59_AnonFilter_a0_3288_3534_3627_3643_split[1]), &(SplitJoin83_SplitJoin59_SplitJoin59_AnonFilter_a0_3288_3534_3627_3643_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3454() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		push_complex(&SplitJoin83_SplitJoin59_SplitJoin59_AnonFilter_a0_3288_3534_3627_3643_split[0], pop_complex(&SplitJoin77_SplitJoin53_SplitJoin53_AnonFilter_a0_3280_3530_3625_3641_split[1]));
		push_complex(&SplitJoin83_SplitJoin59_SplitJoin59_AnonFilter_a0_3288_3534_3627_3643_split[1], pop_complex(&SplitJoin77_SplitJoin53_SplitJoin53_AnonFilter_a0_3280_3530_3625_3641_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3455() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		push_complex(&SplitJoin77_SplitJoin53_SplitJoin53_AnonFilter_a0_3280_3530_3625_3641_join[1], pop_complex(&SplitJoin83_SplitJoin59_SplitJoin59_AnonFilter_a0_3288_3534_3627_3643_join[0]));
		push_complex(&SplitJoin77_SplitJoin53_SplitJoin53_AnonFilter_a0_3280_3530_3625_3641_join[1], pop_complex(&SplitJoin83_SplitJoin59_SplitJoin59_AnonFilter_a0_3288_3534_3627_3643_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_3450() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 26, __iter_steady_++)
		push_complex(&SplitJoin77_SplitJoin53_SplitJoin53_AnonFilter_a0_3280_3530_3625_3641_split[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3264_3489_3572_3637_split[1]));
		push_complex(&SplitJoin77_SplitJoin53_SplitJoin53_AnonFilter_a0_3280_3530_3625_3641_split[1], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3264_3489_3572_3637_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3451() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3264_3489_3572_3637_join[1], pop_complex(&SplitJoin77_SplitJoin53_SplitJoin53_AnonFilter_a0_3280_3530_3625_3641_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3264_3489_3572_3637_join[1], pop_complex(&SplitJoin77_SplitJoin53_SplitJoin53_AnonFilter_a0_3280_3530_3625_3641_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3264_3489_3572_3637_join[1], pop_complex(&SplitJoin77_SplitJoin53_SplitJoin53_AnonFilter_a0_3280_3530_3625_3641_join[1]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3264_3489_3572_3637_join[1], pop_complex(&SplitJoin77_SplitJoin53_SplitJoin53_AnonFilter_a0_3280_3530_3625_3641_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_3442() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 52, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3264_3489_3572_3637_split[0], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3262_3488_3617_3636_split[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3264_3489_3572_3637_split[1], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3262_3488_3617_3636_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3443() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3262_3488_3617_3636_join[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3264_3489_3572_3637_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3262_3488_3617_3636_join[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3264_3489_3572_3637_join[1]));
		ENDFOR
	ENDFOR
}}

void Identity_3300() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Identity(&(SplitJoin91_SplitJoin67_SplitJoin67_AnonFilter_a0_3298_3539_3629_3646_split[0]), &(SplitJoin91_SplitJoin67_SplitJoin67_AnonFilter_a0_3298_3539_3629_3646_join[0]));
	ENDFOR
}

void Identity_3302() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Identity(&(SplitJoin91_SplitJoin67_SplitJoin67_AnonFilter_a0_3298_3539_3629_3646_split[1]), &(SplitJoin91_SplitJoin67_SplitJoin67_AnonFilter_a0_3298_3539_3629_3646_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3460() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		push_complex(&SplitJoin91_SplitJoin67_SplitJoin67_AnonFilter_a0_3298_3539_3629_3646_split[0], pop_complex(&SplitJoin89_SplitJoin65_SplitJoin65_AnonFilter_a0_3296_3538_3628_3645_split[0]));
		push_complex(&SplitJoin91_SplitJoin67_SplitJoin67_AnonFilter_a0_3298_3539_3629_3646_split[1], pop_complex(&SplitJoin89_SplitJoin65_SplitJoin65_AnonFilter_a0_3296_3538_3628_3645_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3461() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		push_complex(&SplitJoin89_SplitJoin65_SplitJoin65_AnonFilter_a0_3296_3538_3628_3645_join[0], pop_complex(&SplitJoin91_SplitJoin67_SplitJoin67_AnonFilter_a0_3298_3539_3629_3646_join[0]));
		push_complex(&SplitJoin89_SplitJoin65_SplitJoin65_AnonFilter_a0_3296_3538_3628_3645_join[0], pop_complex(&SplitJoin91_SplitJoin67_SplitJoin67_AnonFilter_a0_3298_3539_3629_3646_join[1]));
	ENDFOR
}}

void Identity_3306() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Identity(&(SplitJoin95_SplitJoin71_SplitJoin71_AnonFilter_a0_3304_3542_3630_3647_split[0]), &(SplitJoin95_SplitJoin71_SplitJoin71_AnonFilter_a0_3304_3542_3630_3647_join[0]));
	ENDFOR
}

void Identity_3308() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Identity(&(SplitJoin95_SplitJoin71_SplitJoin71_AnonFilter_a0_3304_3542_3630_3647_split[1]), &(SplitJoin95_SplitJoin71_SplitJoin71_AnonFilter_a0_3304_3542_3630_3647_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3462() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		push_complex(&SplitJoin95_SplitJoin71_SplitJoin71_AnonFilter_a0_3304_3542_3630_3647_split[0], pop_complex(&SplitJoin89_SplitJoin65_SplitJoin65_AnonFilter_a0_3296_3538_3628_3645_split[1]));
		push_complex(&SplitJoin95_SplitJoin71_SplitJoin71_AnonFilter_a0_3304_3542_3630_3647_split[1], pop_complex(&SplitJoin89_SplitJoin65_SplitJoin65_AnonFilter_a0_3296_3538_3628_3645_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3463() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		push_complex(&SplitJoin89_SplitJoin65_SplitJoin65_AnonFilter_a0_3296_3538_3628_3645_join[1], pop_complex(&SplitJoin95_SplitJoin71_SplitJoin71_AnonFilter_a0_3304_3542_3630_3647_join[0]));
		push_complex(&SplitJoin89_SplitJoin65_SplitJoin65_AnonFilter_a0_3296_3538_3628_3645_join[1], pop_complex(&SplitJoin95_SplitJoin71_SplitJoin71_AnonFilter_a0_3304_3542_3630_3647_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_3458() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 26, __iter_steady_++)
		push_complex(&SplitJoin89_SplitJoin65_SplitJoin65_AnonFilter_a0_3296_3538_3628_3645_split[0], pop_complex(&SplitJoin87_SplitJoin63_SplitJoin63_AnonFilter_a0_3294_3537_3573_3644_split[0]));
		push_complex(&SplitJoin89_SplitJoin65_SplitJoin65_AnonFilter_a0_3296_3538_3628_3645_split[1], pop_complex(&SplitJoin87_SplitJoin63_SplitJoin63_AnonFilter_a0_3294_3537_3573_3644_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3459() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		push_complex(&SplitJoin87_SplitJoin63_SplitJoin63_AnonFilter_a0_3294_3537_3573_3644_join[0], pop_complex(&SplitJoin89_SplitJoin65_SplitJoin65_AnonFilter_a0_3296_3538_3628_3645_join[0]));
		push_complex(&SplitJoin87_SplitJoin63_SplitJoin63_AnonFilter_a0_3294_3537_3573_3644_join[0], pop_complex(&SplitJoin89_SplitJoin65_SplitJoin65_AnonFilter_a0_3296_3538_3628_3645_join[0]));
		push_complex(&SplitJoin87_SplitJoin63_SplitJoin63_AnonFilter_a0_3294_3537_3573_3644_join[0], pop_complex(&SplitJoin89_SplitJoin65_SplitJoin65_AnonFilter_a0_3296_3538_3628_3645_join[1]));
		push_complex(&SplitJoin87_SplitJoin63_SplitJoin63_AnonFilter_a0_3294_3537_3573_3644_join[0], pop_complex(&SplitJoin89_SplitJoin65_SplitJoin65_AnonFilter_a0_3296_3538_3628_3645_join[1]));
	ENDFOR
}}

void Identity_3314() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Identity(&(SplitJoin101_SplitJoin77_SplitJoin77_AnonFilter_a0_3312_3546_3632_3649_split[0]), &(SplitJoin101_SplitJoin77_SplitJoin77_AnonFilter_a0_3312_3546_3632_3649_join[0]));
	ENDFOR
}

void Identity_3316() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Identity(&(SplitJoin101_SplitJoin77_SplitJoin77_AnonFilter_a0_3312_3546_3632_3649_split[1]), &(SplitJoin101_SplitJoin77_SplitJoin77_AnonFilter_a0_3312_3546_3632_3649_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3466() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		push_complex(&SplitJoin101_SplitJoin77_SplitJoin77_AnonFilter_a0_3312_3546_3632_3649_split[0], pop_complex(&SplitJoin99_SplitJoin75_SplitJoin75_AnonFilter_a0_3310_3545_3631_3648_split[0]));
		push_complex(&SplitJoin101_SplitJoin77_SplitJoin77_AnonFilter_a0_3312_3546_3632_3649_split[1], pop_complex(&SplitJoin99_SplitJoin75_SplitJoin75_AnonFilter_a0_3310_3545_3631_3648_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3467() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		push_complex(&SplitJoin99_SplitJoin75_SplitJoin75_AnonFilter_a0_3310_3545_3631_3648_join[0], pop_complex(&SplitJoin101_SplitJoin77_SplitJoin77_AnonFilter_a0_3312_3546_3632_3649_join[0]));
		push_complex(&SplitJoin99_SplitJoin75_SplitJoin75_AnonFilter_a0_3310_3545_3631_3648_join[0], pop_complex(&SplitJoin101_SplitJoin77_SplitJoin77_AnonFilter_a0_3312_3546_3632_3649_join[1]));
	ENDFOR
}}

void Identity_3320() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Identity(&(SplitJoin105_SplitJoin81_SplitJoin81_AnonFilter_a0_3318_3549_3633_3650_split[0]), &(SplitJoin105_SplitJoin81_SplitJoin81_AnonFilter_a0_3318_3549_3633_3650_join[0]));
	ENDFOR
}

void Identity_3322() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Identity(&(SplitJoin105_SplitJoin81_SplitJoin81_AnonFilter_a0_3318_3549_3633_3650_split[1]), &(SplitJoin105_SplitJoin81_SplitJoin81_AnonFilter_a0_3318_3549_3633_3650_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3468() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		push_complex(&SplitJoin105_SplitJoin81_SplitJoin81_AnonFilter_a0_3318_3549_3633_3650_split[0], pop_complex(&SplitJoin99_SplitJoin75_SplitJoin75_AnonFilter_a0_3310_3545_3631_3648_split[1]));
		push_complex(&SplitJoin105_SplitJoin81_SplitJoin81_AnonFilter_a0_3318_3549_3633_3650_split[1], pop_complex(&SplitJoin99_SplitJoin75_SplitJoin75_AnonFilter_a0_3310_3545_3631_3648_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3469() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		push_complex(&SplitJoin99_SplitJoin75_SplitJoin75_AnonFilter_a0_3310_3545_3631_3648_join[1], pop_complex(&SplitJoin105_SplitJoin81_SplitJoin81_AnonFilter_a0_3318_3549_3633_3650_join[0]));
		push_complex(&SplitJoin99_SplitJoin75_SplitJoin75_AnonFilter_a0_3310_3545_3631_3648_join[1], pop_complex(&SplitJoin105_SplitJoin81_SplitJoin81_AnonFilter_a0_3318_3549_3633_3650_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_3464() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 26, __iter_steady_++)
		push_complex(&SplitJoin99_SplitJoin75_SplitJoin75_AnonFilter_a0_3310_3545_3631_3648_split[0], pop_complex(&SplitJoin87_SplitJoin63_SplitJoin63_AnonFilter_a0_3294_3537_3573_3644_split[1]));
		push_complex(&SplitJoin99_SplitJoin75_SplitJoin75_AnonFilter_a0_3310_3545_3631_3648_split[1], pop_complex(&SplitJoin87_SplitJoin63_SplitJoin63_AnonFilter_a0_3294_3537_3573_3644_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3465() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		push_complex(&SplitJoin87_SplitJoin63_SplitJoin63_AnonFilter_a0_3294_3537_3573_3644_join[1], pop_complex(&SplitJoin99_SplitJoin75_SplitJoin75_AnonFilter_a0_3310_3545_3631_3648_join[0]));
		push_complex(&SplitJoin87_SplitJoin63_SplitJoin63_AnonFilter_a0_3294_3537_3573_3644_join[1], pop_complex(&SplitJoin99_SplitJoin75_SplitJoin75_AnonFilter_a0_3310_3545_3631_3648_join[0]));
		push_complex(&SplitJoin87_SplitJoin63_SplitJoin63_AnonFilter_a0_3294_3537_3573_3644_join[1], pop_complex(&SplitJoin99_SplitJoin75_SplitJoin75_AnonFilter_a0_3310_3545_3631_3648_join[1]));
		push_complex(&SplitJoin87_SplitJoin63_SplitJoin63_AnonFilter_a0_3294_3537_3573_3644_join[1], pop_complex(&SplitJoin99_SplitJoin75_SplitJoin75_AnonFilter_a0_3310_3545_3631_3648_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_3456() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 52, __iter_steady_++)
		push_complex(&SplitJoin87_SplitJoin63_SplitJoin63_AnonFilter_a0_3294_3537_3573_3644_split[0], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3262_3488_3617_3636_split[1]));
		push_complex(&SplitJoin87_SplitJoin63_SplitJoin63_AnonFilter_a0_3294_3537_3573_3644_split[1], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3262_3488_3617_3636_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3457() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3262_3488_3617_3636_join[1], pop_complex(&SplitJoin87_SplitJoin63_SplitJoin63_AnonFilter_a0_3294_3537_3573_3644_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3262_3488_3617_3636_join[1], pop_complex(&SplitJoin87_SplitJoin63_SplitJoin63_AnonFilter_a0_3294_3537_3573_3644_join[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_3440() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 104, __iter_steady_++)
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3262_3488_3617_3636_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3598WEIGHTED_ROUND_ROBIN_Splitter_3440));
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3262_3488_3617_3636_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3598WEIGHTED_ROUND_ROBIN_Splitter_3440));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3441() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3441WEIGHTED_ROUND_ROBIN_Splitter_3584, pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3262_3488_3617_3636_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3441WEIGHTED_ROUND_ROBIN_Splitter_3584, pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3262_3488_3617_3636_join[1]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_complex_t *chanin, buffer_complex_t *chanout) {
 {
 {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
			push_complex(&(*chanout), peek_complex(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
		}
		ENDFOR
	}
	}
	}
		pop_complex(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_3417() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child0_3575_3652_split[0]), &(Pre_CollapsedDataParallel_1_3417butterfly_3324));
	ENDFOR
}

void butterfly(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&(*chanin)));
		complex_t two = ((complex_t) pop_complex(&(*chanin)));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&(*chanout), __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&(*chanout), __sa2) ; 
	}


void butterfly_3324() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_3417butterfly_3324), &(butterfly_3324Post_CollapsedDataParallel_2_3418));
	ENDFOR
}

void Post_CollapsedDataParallel_2(buffer_complex_t *chanin, buffer_complex_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 2, _k++) {
 {
			push_complex(&(*chanout), peek_complex(&(*chanin), (_k + 0))) ; 
		}
		}
		ENDFOR
	}
	}
		pop_complex(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_3418() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_3324Post_CollapsedDataParallel_2_3418), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child0_3575_3652_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_3420() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child0_3575_3652_split[1]), &(Pre_CollapsedDataParallel_1_3420butterfly_3325));
	ENDFOR
}

void butterfly_3325() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_3420butterfly_3325), &(butterfly_3325Post_CollapsedDataParallel_2_3421));
	ENDFOR
}

void Post_CollapsedDataParallel_2_3421() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_3325Post_CollapsedDataParallel_2_3421), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child0_3575_3652_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_3423() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child0_3575_3652_split[2]), &(Pre_CollapsedDataParallel_1_3423butterfly_3326));
	ENDFOR
}

void butterfly_3326() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_3423butterfly_3326), &(butterfly_3326Post_CollapsedDataParallel_2_3424));
	ENDFOR
}

void Post_CollapsedDataParallel_2_3424() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_3326Post_CollapsedDataParallel_2_3424), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child0_3575_3652_join[2]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_3426() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child0_3575_3652_split[3]), &(Pre_CollapsedDataParallel_1_3426butterfly_3327));
	ENDFOR
}

void butterfly_3327() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_3426butterfly_3327), &(butterfly_3327Post_CollapsedDataParallel_2_3427));
	ENDFOR
}

void Post_CollapsedDataParallel_2_3427() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_3327Post_CollapsedDataParallel_2_3427), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child0_3575_3652_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3585() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child0_3575_3652_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_Hier_3620_3651_split[0]));
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child0_3575_3652_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_Hier_3620_3651_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3586() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_Hier_3620_3651_join[0], pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child0_3575_3652_join[__iter_]));
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_Hier_3620_3651_join[0], pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child0_3575_3652_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_3429() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin66_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child1_3578_3653_split[0]), &(Pre_CollapsedDataParallel_1_3429butterfly_3328));
	ENDFOR
}

void butterfly_3328() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_3429butterfly_3328), &(butterfly_3328Post_CollapsedDataParallel_2_3430));
	ENDFOR
}

void Post_CollapsedDataParallel_2_3430() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_3328Post_CollapsedDataParallel_2_3430), &(SplitJoin66_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child1_3578_3653_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_3432() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin66_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child1_3578_3653_split[1]), &(Pre_CollapsedDataParallel_1_3432butterfly_3329));
	ENDFOR
}

void butterfly_3329() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_3432butterfly_3329), &(butterfly_3329Post_CollapsedDataParallel_2_3433));
	ENDFOR
}

void Post_CollapsedDataParallel_2_3433() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_3329Post_CollapsedDataParallel_2_3433), &(SplitJoin66_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child1_3578_3653_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_3435() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin66_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child1_3578_3653_split[2]), &(Pre_CollapsedDataParallel_1_3435butterfly_3330));
	ENDFOR
}

void butterfly_3330() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_3435butterfly_3330), &(butterfly_3330Post_CollapsedDataParallel_2_3436));
	ENDFOR
}

void Post_CollapsedDataParallel_2_3436() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_3330Post_CollapsedDataParallel_2_3436), &(SplitJoin66_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child1_3578_3653_join[2]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_3438() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin66_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child1_3578_3653_split[3]), &(Pre_CollapsedDataParallel_1_3438butterfly_3331));
	ENDFOR
}

void butterfly_3331() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_3438butterfly_3331), &(butterfly_3331Post_CollapsedDataParallel_2_3439));
	ENDFOR
}

void Post_CollapsedDataParallel_2_3439() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_3331Post_CollapsedDataParallel_2_3439), &(SplitJoin66_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child1_3578_3653_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3587() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin66_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child1_3578_3653_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_Hier_3620_3651_split[1]));
			push_complex(&SplitJoin66_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child1_3578_3653_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_Hier_3620_3651_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3588() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_Hier_3620_3651_join[1], pop_complex(&SplitJoin66_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child1_3578_3653_join[__iter_]));
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_Hier_3620_3651_join[1], pop_complex(&SplitJoin66_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child1_3578_3653_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_3584() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_Hier_3620_3651_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3441WEIGHTED_ROUND_ROBIN_Splitter_3584));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_Hier_3620_3651_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3441WEIGHTED_ROUND_ROBIN_Splitter_3584));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3589() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3589WEIGHTED_ROUND_ROBIN_Splitter_3590, pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_Hier_3620_3651_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3589WEIGHTED_ROUND_ROBIN_Splitter_3590, pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_Hier_3620_3651_join[1]));
		ENDFOR
	ENDFOR
}}

void butterfly_3333() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		butterfly(&(SplitJoin18_SplitJoin12_SplitJoin12_split2_3243_3496_3574_3656_split[0]), &(SplitJoin18_SplitJoin12_SplitJoin12_split2_3243_3496_3574_3656_join[0]));
	ENDFOR
}

void butterfly_3334() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		butterfly(&(SplitJoin18_SplitJoin12_SplitJoin12_split2_3243_3496_3574_3656_split[1]), &(SplitJoin18_SplitJoin12_SplitJoin12_split2_3243_3496_3574_3656_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3474() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 26, __iter_steady_++)
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_3243_3496_3574_3656_split[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child0_3581_3655_split[0]));
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_3243_3496_3574_3656_split[1], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child0_3581_3655_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3475() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 26, __iter_steady_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child0_3581_3655_join[0], pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_3243_3496_3574_3656_join[0]));
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child0_3581_3655_join[0], pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_3243_3496_3574_3656_join[1]));
	ENDFOR
}}

void butterfly_3335() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		butterfly(&(SplitJoin49_SplitJoin29_SplitJoin29_split2_3245_3510_3576_3657_split[0]), &(SplitJoin49_SplitJoin29_SplitJoin29_split2_3245_3510_3576_3657_join[0]));
	ENDFOR
}

void butterfly_3336() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		butterfly(&(SplitJoin49_SplitJoin29_SplitJoin29_split2_3245_3510_3576_3657_split[1]), &(SplitJoin49_SplitJoin29_SplitJoin29_split2_3245_3510_3576_3657_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3476() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 26, __iter_steady_++)
		push_complex(&SplitJoin49_SplitJoin29_SplitJoin29_split2_3245_3510_3576_3657_split[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child0_3581_3655_split[1]));
		push_complex(&SplitJoin49_SplitJoin29_SplitJoin29_split2_3245_3510_3576_3657_split[1], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child0_3581_3655_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3477() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 26, __iter_steady_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child0_3581_3655_join[1], pop_complex(&SplitJoin49_SplitJoin29_SplitJoin29_split2_3245_3510_3576_3657_join[0]));
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child0_3581_3655_join[1], pop_complex(&SplitJoin49_SplitJoin29_SplitJoin29_split2_3245_3510_3576_3657_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_3591() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child0_3581_3655_split[0], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_Hier_3621_3654_split[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child0_3581_3655_split[1], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_Hier_3621_3654_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3592() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_Hier_3621_3654_join[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child0_3581_3655_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_Hier_3621_3654_join[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child0_3581_3655_join[1]));
		ENDFOR
	ENDFOR
}}

void butterfly_3337() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		butterfly(&(SplitJoin55_SplitJoin33_SplitJoin33_split2_3247_3513_3577_3659_split[0]), &(SplitJoin55_SplitJoin33_SplitJoin33_split2_3247_3513_3577_3659_join[0]));
	ENDFOR
}

void butterfly_3338() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		butterfly(&(SplitJoin55_SplitJoin33_SplitJoin33_split2_3247_3513_3577_3659_split[1]), &(SplitJoin55_SplitJoin33_SplitJoin33_split2_3247_3513_3577_3659_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3478() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 26, __iter_steady_++)
		push_complex(&SplitJoin55_SplitJoin33_SplitJoin33_split2_3247_3513_3577_3659_split[0], pop_complex(&SplitJoin53_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child1_3583_3658_split[0]));
		push_complex(&SplitJoin55_SplitJoin33_SplitJoin33_split2_3247_3513_3577_3659_split[1], pop_complex(&SplitJoin53_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child1_3583_3658_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3479() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 26, __iter_steady_++)
		push_complex(&SplitJoin53_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child1_3583_3658_join[0], pop_complex(&SplitJoin55_SplitJoin33_SplitJoin33_split2_3247_3513_3577_3659_join[0]));
		push_complex(&SplitJoin53_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child1_3583_3658_join[0], pop_complex(&SplitJoin55_SplitJoin33_SplitJoin33_split2_3247_3513_3577_3659_join[1]));
	ENDFOR
}}

void butterfly_3339() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		butterfly(&(SplitJoin59_SplitJoin37_SplitJoin37_split2_3249_3516_3579_3660_split[0]), &(SplitJoin59_SplitJoin37_SplitJoin37_split2_3249_3516_3579_3660_join[0]));
	ENDFOR
}

void butterfly_3340() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		butterfly(&(SplitJoin59_SplitJoin37_SplitJoin37_split2_3249_3516_3579_3660_split[1]), &(SplitJoin59_SplitJoin37_SplitJoin37_split2_3249_3516_3579_3660_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3480() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 26, __iter_steady_++)
		push_complex(&SplitJoin59_SplitJoin37_SplitJoin37_split2_3249_3516_3579_3660_split[0], pop_complex(&SplitJoin53_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child1_3583_3658_split[1]));
		push_complex(&SplitJoin59_SplitJoin37_SplitJoin37_split2_3249_3516_3579_3660_split[1], pop_complex(&SplitJoin53_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child1_3583_3658_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3481() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 26, __iter_steady_++)
		push_complex(&SplitJoin53_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child1_3583_3658_join[1], pop_complex(&SplitJoin59_SplitJoin37_SplitJoin37_split2_3249_3516_3579_3660_join[0]));
		push_complex(&SplitJoin53_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child1_3583_3658_join[1], pop_complex(&SplitJoin59_SplitJoin37_SplitJoin37_split2_3249_3516_3579_3660_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_3593() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin53_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child1_3583_3658_split[0], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_Hier_3621_3654_split[1]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin53_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child1_3583_3658_split[1], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_Hier_3621_3654_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3594() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_Hier_3621_3654_join[1], pop_complex(&SplitJoin53_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child1_3583_3658_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_Hier_3621_3654_join[1], pop_complex(&SplitJoin53_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child1_3583_3658_join[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_3590() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_Hier_3621_3654_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3589WEIGHTED_ROUND_ROBIN_Splitter_3590));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_Hier_3621_3654_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3589WEIGHTED_ROUND_ROBIN_Splitter_3590));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3595() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3595WEIGHTED_ROUND_ROBIN_Splitter_3482, pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_Hier_3621_3654_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3595WEIGHTED_ROUND_ROBIN_Splitter_3482, pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_Hier_3621_3654_join[1]));
		ENDFOR
	ENDFOR
}}

void butterfly_3342() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_3256_3499_3580_3662_split[0]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_3256_3499_3580_3662_join[0]));
	ENDFOR
}

void butterfly_3343() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_3256_3499_3580_3662_split[1]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_3256_3499_3580_3662_join[1]));
	ENDFOR
}

void butterfly_3344() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_3256_3499_3580_3662_split[2]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_3256_3499_3580_3662_join[2]));
	ENDFOR
}

void butterfly_3345() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_3256_3499_3580_3662_split[3]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_3256_3499_3580_3662_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3484() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 26, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_3256_3499_3580_3662_split[__iter_], pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_3254_3498_3622_3661_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3485() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 26, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_3254_3498_3622_3661_join[0], pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_3256_3499_3580_3662_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void butterfly_3346() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		butterfly(&(SplitJoin42_SplitJoin22_SplitJoin22_split2_3258_3504_3582_3663_split[0]), &(SplitJoin42_SplitJoin22_SplitJoin22_split2_3258_3504_3582_3663_join[0]));
	ENDFOR
}

void butterfly_3347() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		butterfly(&(SplitJoin42_SplitJoin22_SplitJoin22_split2_3258_3504_3582_3663_split[1]), &(SplitJoin42_SplitJoin22_SplitJoin22_split2_3258_3504_3582_3663_join[1]));
	ENDFOR
}

void butterfly_3348() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		butterfly(&(SplitJoin42_SplitJoin22_SplitJoin22_split2_3258_3504_3582_3663_split[2]), &(SplitJoin42_SplitJoin22_SplitJoin22_split2_3258_3504_3582_3663_join[2]));
	ENDFOR
}

void butterfly_3349() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		butterfly(&(SplitJoin42_SplitJoin22_SplitJoin22_split2_3258_3504_3582_3663_split[3]), &(SplitJoin42_SplitJoin22_SplitJoin22_split2_3258_3504_3582_3663_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3486() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 26, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin42_SplitJoin22_SplitJoin22_split2_3258_3504_3582_3663_split[__iter_], pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_3254_3498_3622_3661_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3487() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 26, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_3254_3498_3622_3661_join[1], pop_complex(&SplitJoin42_SplitJoin22_SplitJoin22_split2_3258_3504_3582_3663_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_3482() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_3254_3498_3622_3661_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3595WEIGHTED_ROUND_ROBIN_Splitter_3482));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_3254_3498_3622_3661_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3595WEIGHTED_ROUND_ROBIN_Splitter_3482));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3483() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3483WEIGHTED_ROUND_ROBIN_Splitter_3601, pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_3254_3498_3622_3661_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3483WEIGHTED_ROUND_ROBIN_Splitter_3601, pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_3254_3498_3622_3661_join[1]));
		ENDFOR
	ENDFOR
}}

void magnitude(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		push_float(&(*chanout), ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}


void magnitude_3603() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_3623_3664_split[0]), &(SplitJoin24_magnitude_Fiss_3623_3664_join[0]));
	ENDFOR
}

void magnitude_3604() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_3623_3664_split[1]), &(SplitJoin24_magnitude_Fiss_3623_3664_join[1]));
	ENDFOR
}

void magnitude_3605() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_3623_3664_split[2]), &(SplitJoin24_magnitude_Fiss_3623_3664_join[2]));
	ENDFOR
}

void magnitude_3606() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_3623_3664_split[3]), &(SplitJoin24_magnitude_Fiss_3623_3664_join[3]));
	ENDFOR
}

void magnitude_3607() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_3623_3664_split[4]), &(SplitJoin24_magnitude_Fiss_3623_3664_join[4]));
	ENDFOR
}

void magnitude_3608() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_3623_3664_split[5]), &(SplitJoin24_magnitude_Fiss_3623_3664_join[5]));
	ENDFOR
}

void magnitude_3609() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_3623_3664_split[6]), &(SplitJoin24_magnitude_Fiss_3623_3664_join[6]));
	ENDFOR
}

void magnitude_3610() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_3623_3664_split[7]), &(SplitJoin24_magnitude_Fiss_3623_3664_join[7]));
	ENDFOR
}

void magnitude_3611() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_3623_3664_split[8]), &(SplitJoin24_magnitude_Fiss_3623_3664_join[8]));
	ENDFOR
}

void magnitude_3612() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_3623_3664_split[9]), &(SplitJoin24_magnitude_Fiss_3623_3664_join[9]));
	ENDFOR
}

void magnitude_3613() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_3623_3664_split[10]), &(SplitJoin24_magnitude_Fiss_3623_3664_join[10]));
	ENDFOR
}

void magnitude_3614() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_3623_3664_split[11]), &(SplitJoin24_magnitude_Fiss_3623_3664_join[11]));
	ENDFOR
}

void magnitude_3615() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_3623_3664_split[12]), &(SplitJoin24_magnitude_Fiss_3623_3664_join[12]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3601() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 13, __iter_++)
			push_complex(&SplitJoin24_magnitude_Fiss_3623_3664_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3483WEIGHTED_ROUND_ROBIN_Splitter_3601));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3602() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 13, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3602sink_3351, pop_float(&SplitJoin24_magnitude_Fiss_3623_3664_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void sink(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void sink_3351() {
	FOR(uint32_t, __iter_steady_, 0, <, 208, __iter_steady_++)
		sink(&(WEIGHTED_ROUND_ROBIN_Joiner_3602sink_3351));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_3254_3498_3622_3661_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 2, __iter_init_1_++)
		init_buffer_complex(&SplitJoin79_SplitJoin55_SplitJoin55_AnonFilter_a0_3282_3531_3626_3642_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 4, __iter_init_2_++)
		init_buffer_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_3256_3499_3580_3662_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 2, __iter_init_3_++)
		init_buffer_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3262_3488_3617_3636_join[__iter_init_3_]);
	ENDFOR
	init_buffer_complex(&butterfly_3329Post_CollapsedDataParallel_2_3433);
	FOR(int, __iter_init_4_, 0, <, 13, __iter_init_4_++)
		init_buffer_complex(&SplitJoin24_magnitude_Fiss_3623_3664_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 2, __iter_init_5_++)
		init_buffer_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_3254_3498_3622_3661_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 2, __iter_init_6_++)
		init_buffer_complex(&SplitJoin87_SplitJoin63_SplitJoin63_AnonFilter_a0_3294_3537_3573_3644_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 2, __iter_init_7_++)
		init_buffer_complex(&SplitJoin53_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child1_3583_3658_join[__iter_init_7_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_3429butterfly_3328);
	init_buffer_complex(&butterfly_3331Post_CollapsedDataParallel_2_3439);
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_complex(&SplitJoin91_SplitJoin67_SplitJoin67_AnonFilter_a0_3298_3539_3629_3646_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 2, __iter_init_9_++)
		init_buffer_complex(&SplitJoin49_SplitJoin29_SplitJoin29_split2_3245_3510_3576_3657_join[__iter_init_9_]);
	ENDFOR
	init_buffer_complex(&butterfly_3328Post_CollapsedDataParallel_2_3430);
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_complex(&SplitJoin95_SplitJoin71_SplitJoin71_AnonFilter_a0_3304_3542_3630_3647_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 2, __iter_init_11_++)
		init_buffer_complex(&SplitJoin101_SplitJoin77_SplitJoin77_AnonFilter_a0_3312_3546_3632_3649_join[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 2, __iter_init_12_++)
		init_buffer_complex(&SplitJoin95_SplitJoin71_SplitJoin71_AnonFilter_a0_3304_3542_3630_3647_join[__iter_init_12_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_3420butterfly_3325);
	FOR(int, __iter_init_13_, 0, <, 4, __iter_init_13_++)
		init_buffer_complex(&SplitJoin66_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child1_3578_3653_split[__iter_init_13_]);
	ENDFOR
	init_buffer_complex(&butterfly_3327Post_CollapsedDataParallel_2_3427);
	init_buffer_complex(&Pre_CollapsedDataParallel_1_3435butterfly_3330);
	init_buffer_complex(&Pre_CollapsedDataParallel_1_3426butterfly_3327);
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3266_3490_3618_3638_join[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 2, __iter_init_15_++)
		init_buffer_complex(&SplitJoin77_SplitJoin53_SplitJoin53_AnonFilter_a0_3280_3530_3625_3641_join[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_Hier_3620_3651_split[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_3266_3490_3618_3638_split[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 4, __iter_init_18_++)
		init_buffer_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child0_3575_3652_join[__iter_init_18_]);
	ENDFOR
	FOR(int, __iter_init_19_, 0, <, 4, __iter_init_19_++)
		init_buffer_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_3256_3499_3580_3662_split[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 2, __iter_init_20_++)
		init_buffer_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_3262_3488_3617_3636_split[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 2, __iter_init_21_++)
		init_buffer_complex(&SplitJoin73_SplitJoin49_SplitJoin49_AnonFilter_a0_3274_3527_3624_3640_split[__iter_init_21_]);
	ENDFOR
	init_buffer_complex(&butterfly_3325Post_CollapsedDataParallel_2_3421);
	init_buffer_complex(&butterfly_3330Post_CollapsedDataParallel_2_3436);
	FOR(int, __iter_init_22_, 0, <, 2, __iter_init_22_++)
		init_buffer_complex(&SplitJoin55_SplitJoin33_SplitJoin33_split2_3247_3513_3577_3659_split[__iter_init_22_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_3432butterfly_3329);
	FOR(int, __iter_init_23_, 0, <, 2, __iter_init_23_++)
		init_buffer_complex(&SplitJoin79_SplitJoin55_SplitJoin55_AnonFilter_a0_3282_3531_3626_3642_join[__iter_init_23_]);
	ENDFOR
	FOR(int, __iter_init_24_, 0, <, 2, __iter_init_24_++)
		init_buffer_complex(&SplitJoin105_SplitJoin81_SplitJoin81_AnonFilter_a0_3318_3549_3633_3650_split[__iter_init_24_]);
	ENDFOR
	FOR(int, __iter_init_25_, 0, <, 2, __iter_init_25_++)
		init_buffer_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_3268_3491_3619_3639_split[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 2, __iter_init_26_++)
		init_buffer_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_3268_3491_3619_3639_join[__iter_init_26_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3595WEIGHTED_ROUND_ROBIN_Splitter_3482);
	FOR(int, __iter_init_27_, 0, <, 2, __iter_init_27_++)
		init_buffer_complex(&SplitJoin83_SplitJoin59_SplitJoin59_AnonFilter_a0_3288_3534_3627_3643_join[__iter_init_27_]);
	ENDFOR
	FOR(int, __iter_init_28_, 0, <, 2, __iter_init_28_++)
		init_buffer_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_Hier_3620_3651_join[__iter_init_28_]);
	ENDFOR
	FOR(int, __iter_init_29_, 0, <, 2, __iter_init_29_++)
		init_buffer_complex(&SplitJoin83_SplitJoin59_SplitJoin59_AnonFilter_a0_3288_3534_3627_3643_split[__iter_init_29_]);
	ENDFOR
	FOR(int, __iter_init_30_, 0, <, 2, __iter_init_30_++)
		init_buffer_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3264_3489_3572_3637_join[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 2, __iter_init_31_++)
		init_buffer_complex(&SplitJoin89_SplitJoin65_SplitJoin65_AnonFilter_a0_3296_3538_3628_3645_split[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 2, __iter_init_32_++)
		init_buffer_complex(&SplitJoin49_SplitJoin29_SplitJoin29_split2_3245_3510_3576_3657_split[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 2, __iter_init_33_++)
		init_buffer_complex(&SplitJoin0_source_Fiss_3616_3635_split[__iter_init_33_]);
	ENDFOR
	FOR(int, __iter_init_34_, 0, <, 2, __iter_init_34_++)
		init_buffer_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child0_3581_3655_join[__iter_init_34_]);
	ENDFOR
	init_buffer_complex(&butterfly_3324Post_CollapsedDataParallel_2_3418);
	FOR(int, __iter_init_35_, 0, <, 13, __iter_init_35_++)
		init_buffer_float(&SplitJoin24_magnitude_Fiss_3623_3664_join[__iter_init_35_]);
	ENDFOR
	FOR(int, __iter_init_36_, 0, <, 2, __iter_init_36_++)
		init_buffer_complex(&SplitJoin0_source_Fiss_3616_3635_join[__iter_init_36_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3598WEIGHTED_ROUND_ROBIN_Splitter_3440);
	FOR(int, __iter_init_37_, 0, <, 2, __iter_init_37_++)
		init_buffer_complex(&SplitJoin99_SplitJoin75_SplitJoin75_AnonFilter_a0_3310_3545_3631_3648_split[__iter_init_37_]);
	ENDFOR
	FOR(int, __iter_init_38_, 0, <, 2, __iter_init_38_++)
		init_buffer_complex(&SplitJoin59_SplitJoin37_SplitJoin37_split2_3249_3516_3579_3660_join[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 2, __iter_init_39_++)
		init_buffer_complex(&SplitJoin89_SplitJoin65_SplitJoin65_AnonFilter_a0_3296_3538_3628_3645_join[__iter_init_39_]);
	ENDFOR
	FOR(int, __iter_init_40_, 0, <, 2, __iter_init_40_++)
		init_buffer_complex(&SplitJoin91_SplitJoin67_SplitJoin67_AnonFilter_a0_3298_3539_3629_3646_split[__iter_init_40_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3483WEIGHTED_ROUND_ROBIN_Splitter_3601);
	FOR(int, __iter_init_41_, 0, <, 2, __iter_init_41_++)
		init_buffer_complex(&SplitJoin73_SplitJoin49_SplitJoin49_AnonFilter_a0_3274_3527_3624_3640_join[__iter_init_41_]);
	ENDFOR
	FOR(int, __iter_init_42_, 0, <, 2, __iter_init_42_++)
		init_buffer_complex(&SplitJoin55_SplitJoin33_SplitJoin33_split2_3247_3513_3577_3659_join[__iter_init_42_]);
	ENDFOR
	FOR(int, __iter_init_43_, 0, <, 4, __iter_init_43_++)
		init_buffer_complex(&SplitJoin42_SplitJoin22_SplitJoin22_split2_3258_3504_3582_3663_join[__iter_init_43_]);
	ENDFOR
	FOR(int, __iter_init_44_, 0, <, 2, __iter_init_44_++)
		init_buffer_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_3243_3496_3574_3656_split[__iter_init_44_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3441WEIGHTED_ROUND_ROBIN_Splitter_3584);
	FOR(int, __iter_init_45_, 0, <, 2, __iter_init_45_++)
		init_buffer_complex(&SplitJoin77_SplitJoin53_SplitJoin53_AnonFilter_a0_3280_3530_3625_3641_split[__iter_init_45_]);
	ENDFOR
	FOR(int, __iter_init_46_, 0, <, 2, __iter_init_46_++)
		init_buffer_complex(&SplitJoin101_SplitJoin77_SplitJoin77_AnonFilter_a0_3312_3546_3632_3649_split[__iter_init_46_]);
	ENDFOR
	FOR(int, __iter_init_47_, 0, <, 4, __iter_init_47_++)
		init_buffer_complex(&SplitJoin66_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child1_3578_3653_join[__iter_init_47_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_3589WEIGHTED_ROUND_ROBIN_Splitter_3590);
	FOR(int, __iter_init_48_, 0, <, 2, __iter_init_48_++)
		init_buffer_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_Hier_3621_3654_split[__iter_init_48_]);
	ENDFOR
	FOR(int, __iter_init_49_, 0, <, 4, __iter_init_49_++)
		init_buffer_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_3220_3493_Hier_child0_3575_3652_split[__iter_init_49_]);
	ENDFOR
	FOR(int, __iter_init_50_, 0, <, 2, __iter_init_50_++)
		init_buffer_complex(&SplitJoin105_SplitJoin81_SplitJoin81_AnonFilter_a0_3318_3549_3633_3650_join[__iter_init_50_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_3438butterfly_3331);
	init_buffer_complex(&butterfly_3326Post_CollapsedDataParallel_2_3424);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3602sink_3351);
	init_buffer_complex(&Pre_CollapsedDataParallel_1_3417butterfly_3324);
	FOR(int, __iter_init_51_, 0, <, 2, __iter_init_51_++)
		init_buffer_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_3243_3496_3574_3656_join[__iter_init_51_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_3423butterfly_3326);
	FOR(int, __iter_init_52_, 0, <, 2, __iter_init_52_++)
		init_buffer_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child0_3581_3655_split[__iter_init_52_]);
	ENDFOR
	FOR(int, __iter_init_53_, 0, <, 2, __iter_init_53_++)
		init_buffer_complex(&SplitJoin59_SplitJoin37_SplitJoin37_split2_3249_3516_3579_3660_split[__iter_init_53_]);
	ENDFOR
	FOR(int, __iter_init_54_, 0, <, 4, __iter_init_54_++)
		init_buffer_complex(&SplitJoin42_SplitJoin22_SplitJoin22_split2_3258_3504_3582_3663_split[__iter_init_54_]);
	ENDFOR
	FOR(int, __iter_init_55_, 0, <, 2, __iter_init_55_++)
		init_buffer_complex(&SplitJoin99_SplitJoin75_SplitJoin75_AnonFilter_a0_3310_3545_3631_3648_join[__iter_init_55_]);
	ENDFOR
	FOR(int, __iter_init_56_, 0, <, 2, __iter_init_56_++)
		init_buffer_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_3264_3489_3572_3637_split[__iter_init_56_]);
	ENDFOR
	FOR(int, __iter_init_57_, 0, <, 2, __iter_init_57_++)
		init_buffer_complex(&SplitJoin53_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_child1_3583_3658_split[__iter_init_57_]);
	ENDFOR
	FOR(int, __iter_init_58_, 0, <, 2, __iter_init_58_++)
		init_buffer_complex(&SplitJoin87_SplitJoin63_SplitJoin63_AnonFilter_a0_3294_3537_3573_3644_join[__iter_init_58_]);
	ENDFOR
	FOR(int, __iter_init_59_, 0, <, 2, __iter_init_59_++)
		init_buffer_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_3241_3495_Hier_Hier_3621_3654_join[__iter_init_59_]);
	ENDFOR
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_3597();
			source_3599();
			source_3600();
		WEIGHTED_ROUND_ROBIN_Joiner_3598();
		WEIGHTED_ROUND_ROBIN_Splitter_3440();
			WEIGHTED_ROUND_ROBIN_Splitter_3442();
				WEIGHTED_ROUND_ROBIN_Splitter_3444();
					WEIGHTED_ROUND_ROBIN_Splitter_3446();
						Identity_3270();
						Identity_3272();
					WEIGHTED_ROUND_ROBIN_Joiner_3447();
					WEIGHTED_ROUND_ROBIN_Splitter_3448();
						Identity_3276();
						Identity_3278();
					WEIGHTED_ROUND_ROBIN_Joiner_3449();
				WEIGHTED_ROUND_ROBIN_Joiner_3445();
				WEIGHTED_ROUND_ROBIN_Splitter_3450();
					WEIGHTED_ROUND_ROBIN_Splitter_3452();
						Identity_3284();
						Identity_3286();
					WEIGHTED_ROUND_ROBIN_Joiner_3453();
					WEIGHTED_ROUND_ROBIN_Splitter_3454();
						Identity_3290();
						Identity_3292();
					WEIGHTED_ROUND_ROBIN_Joiner_3455();
				WEIGHTED_ROUND_ROBIN_Joiner_3451();
			WEIGHTED_ROUND_ROBIN_Joiner_3443();
			WEIGHTED_ROUND_ROBIN_Splitter_3456();
				WEIGHTED_ROUND_ROBIN_Splitter_3458();
					WEIGHTED_ROUND_ROBIN_Splitter_3460();
						Identity_3300();
						Identity_3302();
					WEIGHTED_ROUND_ROBIN_Joiner_3461();
					WEIGHTED_ROUND_ROBIN_Splitter_3462();
						Identity_3306();
						Identity_3308();
					WEIGHTED_ROUND_ROBIN_Joiner_3463();
				WEIGHTED_ROUND_ROBIN_Joiner_3459();
				WEIGHTED_ROUND_ROBIN_Splitter_3464();
					WEIGHTED_ROUND_ROBIN_Splitter_3466();
						Identity_3314();
						Identity_3316();
					WEIGHTED_ROUND_ROBIN_Joiner_3467();
					WEIGHTED_ROUND_ROBIN_Splitter_3468();
						Identity_3320();
						Identity_3322();
					WEIGHTED_ROUND_ROBIN_Joiner_3469();
				WEIGHTED_ROUND_ROBIN_Joiner_3465();
			WEIGHTED_ROUND_ROBIN_Joiner_3457();
		WEIGHTED_ROUND_ROBIN_Joiner_3441();
		WEIGHTED_ROUND_ROBIN_Splitter_3584();
			WEIGHTED_ROUND_ROBIN_Splitter_3585();
				Pre_CollapsedDataParallel_1_3417();
				butterfly_3324();
				Post_CollapsedDataParallel_2_3418();
				Pre_CollapsedDataParallel_1_3420();
				butterfly_3325();
				Post_CollapsedDataParallel_2_3421();
				Pre_CollapsedDataParallel_1_3423();
				butterfly_3326();
				Post_CollapsedDataParallel_2_3424();
				Pre_CollapsedDataParallel_1_3426();
				butterfly_3327();
				Post_CollapsedDataParallel_2_3427();
			WEIGHTED_ROUND_ROBIN_Joiner_3586();
			WEIGHTED_ROUND_ROBIN_Splitter_3587();
				Pre_CollapsedDataParallel_1_3429();
				butterfly_3328();
				Post_CollapsedDataParallel_2_3430();
				Pre_CollapsedDataParallel_1_3432();
				butterfly_3329();
				Post_CollapsedDataParallel_2_3433();
				Pre_CollapsedDataParallel_1_3435();
				butterfly_3330();
				Post_CollapsedDataParallel_2_3436();
				Pre_CollapsedDataParallel_1_3438();
				butterfly_3331();
				Post_CollapsedDataParallel_2_3439();
			WEIGHTED_ROUND_ROBIN_Joiner_3588();
		WEIGHTED_ROUND_ROBIN_Joiner_3589();
		WEIGHTED_ROUND_ROBIN_Splitter_3590();
			WEIGHTED_ROUND_ROBIN_Splitter_3591();
				WEIGHTED_ROUND_ROBIN_Splitter_3474();
					butterfly_3333();
					butterfly_3334();
				WEIGHTED_ROUND_ROBIN_Joiner_3475();
				WEIGHTED_ROUND_ROBIN_Splitter_3476();
					butterfly_3335();
					butterfly_3336();
				WEIGHTED_ROUND_ROBIN_Joiner_3477();
			WEIGHTED_ROUND_ROBIN_Joiner_3592();
			WEIGHTED_ROUND_ROBIN_Splitter_3593();
				WEIGHTED_ROUND_ROBIN_Splitter_3478();
					butterfly_3337();
					butterfly_3338();
				WEIGHTED_ROUND_ROBIN_Joiner_3479();
				WEIGHTED_ROUND_ROBIN_Splitter_3480();
					butterfly_3339();
					butterfly_3340();
				WEIGHTED_ROUND_ROBIN_Joiner_3481();
			WEIGHTED_ROUND_ROBIN_Joiner_3594();
		WEIGHTED_ROUND_ROBIN_Joiner_3595();
		WEIGHTED_ROUND_ROBIN_Splitter_3482();
			WEIGHTED_ROUND_ROBIN_Splitter_3484();
				butterfly_3342();
				butterfly_3343();
				butterfly_3344();
				butterfly_3345();
			WEIGHTED_ROUND_ROBIN_Joiner_3485();
			WEIGHTED_ROUND_ROBIN_Splitter_3486();
				butterfly_3346();
				butterfly_3347();
				butterfly_3348();
				butterfly_3349();
			WEIGHTED_ROUND_ROBIN_Joiner_3487();
		WEIGHTED_ROUND_ROBIN_Joiner_3483();
		WEIGHTED_ROUND_ROBIN_Splitter_3601();
			magnitude_3603();
			magnitude_3604();
			magnitude_3605();
			magnitude_3606();
			magnitude_3607();
			magnitude_3608();
			magnitude_3609();
			magnitude_3610();
			magnitude_3611();
			magnitude_3612();
			magnitude_3613();
			magnitude_3614();
			magnitude_3615();
		WEIGHTED_ROUND_ROBIN_Joiner_3602();
		sink_3351();
	ENDFOR
	return EXIT_SUCCESS;
}
