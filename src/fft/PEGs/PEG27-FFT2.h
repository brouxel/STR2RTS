#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=13824 on the compile command line
#else
#if BUF_SIZEMAX < 13824
#error BUF_SIZEMAX too small, it must be at least 13824
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float w[2];
} CombineDFT_2702_t;

typedef struct {
	float w[4];
} CombineDFT_2731_t;

typedef struct {
	float w[8];
} CombineDFT_2749_t;

typedef struct {
	float w[16];
} CombineDFT_2759_t;

typedef struct {
	float w[32];
} CombineDFT_2765_t;

typedef struct {
	float w[64];
} CombineDFT_2637_t;
void WEIGHTED_ROUND_ROBIN_Splitter_2658();
void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout);
void FFTTestSource_2660();
void FFTTestSource_2661();
void WEIGHTED_ROUND_ROBIN_Joiner_2659();
void WEIGHTED_ROUND_ROBIN_Splitter_2650();
void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout);
void FFTReorderSimple_2627();
void WEIGHTED_ROUND_ROBIN_Splitter_2662();
void FFTReorderSimple_2664();
void FFTReorderSimple_2665();
void WEIGHTED_ROUND_ROBIN_Joiner_2663();
void WEIGHTED_ROUND_ROBIN_Splitter_2666();
void FFTReorderSimple_2668();
void FFTReorderSimple_2669();
void FFTReorderSimple_2670();
void FFTReorderSimple_2671();
void WEIGHTED_ROUND_ROBIN_Joiner_2667();
void WEIGHTED_ROUND_ROBIN_Splitter_2672();
void FFTReorderSimple_2674();
void FFTReorderSimple_2675();
void FFTReorderSimple_2676();
void FFTReorderSimple_2677();
void FFTReorderSimple_2678();
void FFTReorderSimple_2679();
void FFTReorderSimple_2680();
void FFTReorderSimple_2681();
void WEIGHTED_ROUND_ROBIN_Joiner_2673();
void WEIGHTED_ROUND_ROBIN_Splitter_2682();
void FFTReorderSimple_2684();
void FFTReorderSimple_2685();
void FFTReorderSimple_2686();
void FFTReorderSimple_2687();
void FFTReorderSimple_2688();
void FFTReorderSimple_2689();
void FFTReorderSimple_2690();
void FFTReorderSimple_2691();
void FFTReorderSimple_2692();
void FFTReorderSimple_2693();
void FFTReorderSimple_2694();
void FFTReorderSimple_2695();
void FFTReorderSimple_2696();
void FFTReorderSimple_2697();
void FFTReorderSimple_2698();
void FFTReorderSimple_2699();
void WEIGHTED_ROUND_ROBIN_Joiner_2683();
void WEIGHTED_ROUND_ROBIN_Splitter_2700();
void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout);
void CombineDFT_2702();
void CombineDFT_2703();
void CombineDFT_2704();
void CombineDFT_2705();
void CombineDFT_2706();
void CombineDFT_2707();
void CombineDFT_2708();
void CombineDFT_2709();
void CombineDFT_2710();
void CombineDFT_2711();
void CombineDFT_2712();
void CombineDFT_2713();
void CombineDFT_2714();
void CombineDFT_2715();
void CombineDFT_2716();
void CombineDFT_2717();
void CombineDFT_2718();
void CombineDFT_2719();
void CombineDFT_2720();
void CombineDFT_2721();
void CombineDFT_2722();
void CombineDFT_2723();
void CombineDFT_2724();
void CombineDFT_2725();
void CombineDFT_2726();
void CombineDFT_2727();
void CombineDFT_2728();
void WEIGHTED_ROUND_ROBIN_Joiner_2701();
void WEIGHTED_ROUND_ROBIN_Splitter_2729();
void CombineDFT_2731();
void CombineDFT_2732();
void CombineDFT_2733();
void CombineDFT_2734();
void CombineDFT_2735();
void CombineDFT_2736();
void CombineDFT_2737();
void CombineDFT_2738();
void CombineDFT_2739();
void CombineDFT_2740();
void CombineDFT_2741();
void CombineDFT_2742();
void CombineDFT_2743();
void CombineDFT_2744();
void CombineDFT_2745();
void CombineDFT_2746();
void WEIGHTED_ROUND_ROBIN_Joiner_2730();
void WEIGHTED_ROUND_ROBIN_Splitter_2747();
void CombineDFT_2749();
void CombineDFT_2750();
void CombineDFT_2751();
void CombineDFT_2752();
void CombineDFT_2753();
void CombineDFT_2754();
void CombineDFT_2755();
void CombineDFT_2756();
void WEIGHTED_ROUND_ROBIN_Joiner_2748();
void WEIGHTED_ROUND_ROBIN_Splitter_2757();
void CombineDFT_2759();
void CombineDFT_2760();
void CombineDFT_2761();
void CombineDFT_2762();
void WEIGHTED_ROUND_ROBIN_Joiner_2758();
void WEIGHTED_ROUND_ROBIN_Splitter_2763();
void CombineDFT_2765();
void CombineDFT_2766();
void WEIGHTED_ROUND_ROBIN_Joiner_2764();
void CombineDFT_2637();
void FFTReorderSimple_2638();
void WEIGHTED_ROUND_ROBIN_Splitter_2767();
void FFTReorderSimple_2769();
void FFTReorderSimple_2770();
void WEIGHTED_ROUND_ROBIN_Joiner_2768();
void WEIGHTED_ROUND_ROBIN_Splitter_2771();
void FFTReorderSimple_2773();
void FFTReorderSimple_2774();
void FFTReorderSimple_2775();
void FFTReorderSimple_2776();
void WEIGHTED_ROUND_ROBIN_Joiner_2772();
void WEIGHTED_ROUND_ROBIN_Splitter_2777();
void FFTReorderSimple_2779();
void FFTReorderSimple_2780();
void FFTReorderSimple_2781();
void FFTReorderSimple_2782();
void FFTReorderSimple_2783();
void FFTReorderSimple_2784();
void FFTReorderSimple_2785();
void FFTReorderSimple_2786();
void WEIGHTED_ROUND_ROBIN_Joiner_2778();
void WEIGHTED_ROUND_ROBIN_Splitter_2787();
void FFTReorderSimple_2789();
void FFTReorderSimple_2790();
void FFTReorderSimple_2791();
void FFTReorderSimple_2792();
void FFTReorderSimple_2793();
void FFTReorderSimple_2794();
void FFTReorderSimple_2795();
void FFTReorderSimple_2796();
void FFTReorderSimple_2797();
void FFTReorderSimple_2798();
void FFTReorderSimple_2799();
void FFTReorderSimple_2800();
void FFTReorderSimple_2801();
void FFTReorderSimple_2802();
void FFTReorderSimple_2803();
void FFTReorderSimple_2804();
void WEIGHTED_ROUND_ROBIN_Joiner_2788();
void WEIGHTED_ROUND_ROBIN_Splitter_2805();
void CombineDFT_2807();
void CombineDFT_2808();
void CombineDFT_2809();
void CombineDFT_2810();
void CombineDFT_2811();
void CombineDFT_2812();
void CombineDFT_2813();
void CombineDFT_2814();
void CombineDFT_2815();
void CombineDFT_2816();
void CombineDFT_2817();
void CombineDFT_2818();
void CombineDFT_2819();
void CombineDFT_2820();
void CombineDFT_2821();
void CombineDFT_2822();
void CombineDFT_2823();
void CombineDFT_2824();
void CombineDFT_2825();
void CombineDFT_2826();
void CombineDFT_2827();
void CombineDFT_2828();
void CombineDFT_2829();
void CombineDFT_2830();
void CombineDFT_2831();
void CombineDFT_2832();
void CombineDFT_2833();
void WEIGHTED_ROUND_ROBIN_Joiner_2806();
void WEIGHTED_ROUND_ROBIN_Splitter_2834();
void CombineDFT_2836();
void CombineDFT_2837();
void CombineDFT_2838();
void CombineDFT_2839();
void CombineDFT_2840();
void CombineDFT_2841();
void CombineDFT_2842();
void CombineDFT_2843();
void CombineDFT_2844();
void CombineDFT_2845();
void CombineDFT_2846();
void CombineDFT_2847();
void CombineDFT_2848();
void CombineDFT_2849();
void CombineDFT_2850();
void CombineDFT_2851();
void WEIGHTED_ROUND_ROBIN_Joiner_2835();
void WEIGHTED_ROUND_ROBIN_Splitter_2852();
void CombineDFT_2854();
void CombineDFT_2855();
void CombineDFT_2856();
void CombineDFT_2857();
void CombineDFT_2858();
void CombineDFT_2859();
void CombineDFT_2860();
void CombineDFT_2861();
void WEIGHTED_ROUND_ROBIN_Joiner_2853();
void WEIGHTED_ROUND_ROBIN_Splitter_2862();
void CombineDFT_2864();
void CombineDFT_2865();
void CombineDFT_2866();
void CombineDFT_2867();
void WEIGHTED_ROUND_ROBIN_Joiner_2863();
void WEIGHTED_ROUND_ROBIN_Splitter_2868();
void CombineDFT_2870();
void CombineDFT_2871();
void WEIGHTED_ROUND_ROBIN_Joiner_2869();
void CombineDFT_2648();
void WEIGHTED_ROUND_ROBIN_Joiner_2651();
void FloatPrinter(buffer_float_t *chanin);
void FloatPrinter_2649();

#ifdef __cplusplus
}
#endif
#endif
