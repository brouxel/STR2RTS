#include "PEG20-FFT2.h"

buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_6132_6153_join[16];
buffer_float_t SplitJoin104_CombineDFT_Fiss_6143_6164_split[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5928WEIGHTED_ROUND_ROBIN_Splitter_5919;
buffer_float_t FFTReorderSimple_5907WEIGHTED_ROUND_ROBIN_Splitter_6029;
buffer_float_t SplitJoin14_CombineDFT_Fiss_6134_6155_split[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6050WEIGHTED_ROUND_ROBIN_Splitter_6067;
buffer_float_t SplitJoin104_CombineDFT_Fiss_6143_6164_join[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6090WEIGHTED_ROUND_ROBIN_Splitter_6107;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5952WEIGHTED_ROUND_ROBIN_Splitter_5969;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5920FloatPrinter_5918;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6026CombineDFT_5906;
buffer_float_t SplitJoin96_FFTReorderSimple_Fiss_6139_6160_join[4];
buffer_float_t SplitJoin12_CombineDFT_Fiss_6133_6154_join[20];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6020WEIGHTED_ROUND_ROBIN_Splitter_6025;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6068WEIGHTED_ROUND_ROBIN_Splitter_6089;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5992WEIGHTED_ROUND_ROBIN_Splitter_6009;
buffer_float_t SplitJoin18_CombineDFT_Fiss_6136_6157_join[4];
buffer_float_t SplitJoin94_FFTReorderSimple_Fiss_6138_6159_split[2];
buffer_float_t SplitJoin98_FFTReorderSimple_Fiss_6140_6161_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6124CombineDFT_5917;
buffer_float_t FFTReorderSimple_5896WEIGHTED_ROUND_ROBIN_Splitter_5931;
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_5885_5921_6128_6149_join[2];
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_6129_6150_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6040WEIGHTED_ROUND_ROBIN_Splitter_6049;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6108WEIGHTED_ROUND_ROBIN_Splitter_6117;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5932WEIGHTED_ROUND_ROBIN_Splitter_5935;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5936WEIGHTED_ROUND_ROBIN_Splitter_5941;
buffer_float_t SplitJoin102_CombineDFT_Fiss_6142_6163_split[20];
buffer_float_t SplitJoin0_FFTTestSource_Fiss_6127_6148_split[2];
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_6130_6151_split[4];
buffer_float_t SplitJoin100_FFTReorderSimple_Fiss_6141_6162_split[16];
buffer_float_t SplitJoin20_CombineDFT_Fiss_6137_6158_split[2];
buffer_float_t SplitJoin110_CombineDFT_Fiss_6146_6167_split[2];
buffer_float_t SplitJoin108_CombineDFT_Fiss_6145_6166_join[4];
buffer_float_t SplitJoin12_CombineDFT_Fiss_6133_6154_split[20];
buffer_float_t SplitJoin100_FFTReorderSimple_Fiss_6141_6162_join[16];
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_6130_6151_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5942WEIGHTED_ROUND_ROBIN_Splitter_5951;
buffer_float_t SplitJoin110_CombineDFT_Fiss_6146_6167_join[2];
buffer_float_t SplitJoin0_FFTTestSource_Fiss_6127_6148_join[2];
buffer_float_t SplitJoin14_CombineDFT_Fiss_6134_6155_join[16];
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_6129_6150_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5970WEIGHTED_ROUND_ROBIN_Splitter_5991;
buffer_float_t SplitJoin16_CombineDFT_Fiss_6135_6156_split[8];
buffer_float_t SplitJoin106_CombineDFT_Fiss_6144_6165_join[8];
buffer_float_t SplitJoin98_FFTReorderSimple_Fiss_6140_6161_join[8];
buffer_float_t SplitJoin106_CombineDFT_Fiss_6144_6165_split[8];
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_6132_6153_split[16];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6010WEIGHTED_ROUND_ROBIN_Splitter_6019;
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_6131_6152_join[8];
buffer_float_t SplitJoin102_CombineDFT_Fiss_6142_6163_join[20];
buffer_float_t SplitJoin96_FFTReorderSimple_Fiss_6139_6160_split[4];
buffer_float_t SplitJoin16_CombineDFT_Fiss_6135_6156_join[8];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_5885_5921_6128_6149_split[2];
buffer_float_t SplitJoin94_FFTReorderSimple_Fiss_6138_6159_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6030WEIGHTED_ROUND_ROBIN_Splitter_6033;
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_6131_6152_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6118WEIGHTED_ROUND_ROBIN_Splitter_6123;
buffer_float_t SplitJoin108_CombineDFT_Fiss_6145_6166_split[4];
buffer_float_t SplitJoin20_CombineDFT_Fiss_6137_6158_join[2];
buffer_float_t SplitJoin18_CombineDFT_Fiss_6136_6157_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6034WEIGHTED_ROUND_ROBIN_Splitter_6039;


CombineDFT_5971_t CombineDFT_5971_s;
CombineDFT_5971_t CombineDFT_5972_s;
CombineDFT_5971_t CombineDFT_5973_s;
CombineDFT_5971_t CombineDFT_5974_s;
CombineDFT_5971_t CombineDFT_5975_s;
CombineDFT_5971_t CombineDFT_5976_s;
CombineDFT_5971_t CombineDFT_5977_s;
CombineDFT_5971_t CombineDFT_5978_s;
CombineDFT_5971_t CombineDFT_5979_s;
CombineDFT_5971_t CombineDFT_5980_s;
CombineDFT_5971_t CombineDFT_5981_s;
CombineDFT_5971_t CombineDFT_5982_s;
CombineDFT_5971_t CombineDFT_5983_s;
CombineDFT_5971_t CombineDFT_5984_s;
CombineDFT_5971_t CombineDFT_5985_s;
CombineDFT_5971_t CombineDFT_5986_s;
CombineDFT_5971_t CombineDFT_5987_s;
CombineDFT_5971_t CombineDFT_5988_s;
CombineDFT_5971_t CombineDFT_5989_s;
CombineDFT_5971_t CombineDFT_5990_s;
CombineDFT_5993_t CombineDFT_5993_s;
CombineDFT_5993_t CombineDFT_5994_s;
CombineDFT_5993_t CombineDFT_5995_s;
CombineDFT_5993_t CombineDFT_5996_s;
CombineDFT_5993_t CombineDFT_5997_s;
CombineDFT_5993_t CombineDFT_5998_s;
CombineDFT_5993_t CombineDFT_5999_s;
CombineDFT_5993_t CombineDFT_6000_s;
CombineDFT_5993_t CombineDFT_6001_s;
CombineDFT_5993_t CombineDFT_6002_s;
CombineDFT_5993_t CombineDFT_6003_s;
CombineDFT_5993_t CombineDFT_6004_s;
CombineDFT_5993_t CombineDFT_6005_s;
CombineDFT_5993_t CombineDFT_6006_s;
CombineDFT_5993_t CombineDFT_6007_s;
CombineDFT_5993_t CombineDFT_6008_s;
CombineDFT_6011_t CombineDFT_6011_s;
CombineDFT_6011_t CombineDFT_6012_s;
CombineDFT_6011_t CombineDFT_6013_s;
CombineDFT_6011_t CombineDFT_6014_s;
CombineDFT_6011_t CombineDFT_6015_s;
CombineDFT_6011_t CombineDFT_6016_s;
CombineDFT_6011_t CombineDFT_6017_s;
CombineDFT_6011_t CombineDFT_6018_s;
CombineDFT_6021_t CombineDFT_6021_s;
CombineDFT_6021_t CombineDFT_6022_s;
CombineDFT_6021_t CombineDFT_6023_s;
CombineDFT_6021_t CombineDFT_6024_s;
CombineDFT_6027_t CombineDFT_6027_s;
CombineDFT_6027_t CombineDFT_6028_s;
CombineDFT_5906_t CombineDFT_5906_s;
CombineDFT_5971_t CombineDFT_6069_s;
CombineDFT_5971_t CombineDFT_6070_s;
CombineDFT_5971_t CombineDFT_6071_s;
CombineDFT_5971_t CombineDFT_6072_s;
CombineDFT_5971_t CombineDFT_6073_s;
CombineDFT_5971_t CombineDFT_6074_s;
CombineDFT_5971_t CombineDFT_6075_s;
CombineDFT_5971_t CombineDFT_6076_s;
CombineDFT_5971_t CombineDFT_6077_s;
CombineDFT_5971_t CombineDFT_6078_s;
CombineDFT_5971_t CombineDFT_6079_s;
CombineDFT_5971_t CombineDFT_6080_s;
CombineDFT_5971_t CombineDFT_6081_s;
CombineDFT_5971_t CombineDFT_6082_s;
CombineDFT_5971_t CombineDFT_6083_s;
CombineDFT_5971_t CombineDFT_6084_s;
CombineDFT_5971_t CombineDFT_6085_s;
CombineDFT_5971_t CombineDFT_6086_s;
CombineDFT_5971_t CombineDFT_6087_s;
CombineDFT_5971_t CombineDFT_6088_s;
CombineDFT_5993_t CombineDFT_6091_s;
CombineDFT_5993_t CombineDFT_6092_s;
CombineDFT_5993_t CombineDFT_6093_s;
CombineDFT_5993_t CombineDFT_6094_s;
CombineDFT_5993_t CombineDFT_6095_s;
CombineDFT_5993_t CombineDFT_6096_s;
CombineDFT_5993_t CombineDFT_6097_s;
CombineDFT_5993_t CombineDFT_6098_s;
CombineDFT_5993_t CombineDFT_6099_s;
CombineDFT_5993_t CombineDFT_6100_s;
CombineDFT_5993_t CombineDFT_6101_s;
CombineDFT_5993_t CombineDFT_6102_s;
CombineDFT_5993_t CombineDFT_6103_s;
CombineDFT_5993_t CombineDFT_6104_s;
CombineDFT_5993_t CombineDFT_6105_s;
CombineDFT_5993_t CombineDFT_6106_s;
CombineDFT_6011_t CombineDFT_6109_s;
CombineDFT_6011_t CombineDFT_6110_s;
CombineDFT_6011_t CombineDFT_6111_s;
CombineDFT_6011_t CombineDFT_6112_s;
CombineDFT_6011_t CombineDFT_6113_s;
CombineDFT_6011_t CombineDFT_6114_s;
CombineDFT_6011_t CombineDFT_6115_s;
CombineDFT_6011_t CombineDFT_6116_s;
CombineDFT_6021_t CombineDFT_6119_s;
CombineDFT_6021_t CombineDFT_6120_s;
CombineDFT_6021_t CombineDFT_6121_s;
CombineDFT_6021_t CombineDFT_6122_s;
CombineDFT_6027_t CombineDFT_6125_s;
CombineDFT_6027_t CombineDFT_6126_s;
CombineDFT_5906_t CombineDFT_5917_s;

void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), 0.0) ; 
		push_float(&(*chanout), 0.0) ; 
		push_float(&(*chanout), 1.0) ; 
		push_float(&(*chanout), 0.0) ; 
		FOR(int, i, 0,  < , 124, i++) {
			push_float(&(*chanout), 0.0) ; 
		}
		ENDFOR
	}


void FFTTestSource_5929() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_6127_6148_split[0]), &(SplitJoin0_FFTTestSource_Fiss_6127_6148_join[0]));
	ENDFOR
}

void FFTTestSource_5930() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_6127_6148_split[1]), &(SplitJoin0_FFTTestSource_Fiss_6127_6148_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5927() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_5928() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5928WEIGHTED_ROUND_ROBIN_Splitter_5919, pop_float(&SplitJoin0_FFTTestSource_Fiss_6127_6148_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5928WEIGHTED_ROUND_ROBIN_Splitter_5919, pop_float(&SplitJoin0_FFTTestSource_Fiss_6127_6148_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, i, 0,  < , 128, i = (i + 4)) {
			push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 128, i = (i + 4)) {
			push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_float(&(*chanin)) ; 
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void FFTReorderSimple_5896() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_5885_5921_6128_6149_split[0]), &(FFTReorderSimple_5896WEIGHTED_ROUND_ROBIN_Splitter_5931));
	ENDFOR
}

void FFTReorderSimple_5933() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_6129_6150_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_6129_6150_join[0]));
	ENDFOR
}

void FFTReorderSimple_5934() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_6129_6150_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_6129_6150_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5931() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_6129_6150_split[0], pop_float(&FFTReorderSimple_5896WEIGHTED_ROUND_ROBIN_Splitter_5931));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_6129_6150_split[1], pop_float(&FFTReorderSimple_5896WEIGHTED_ROUND_ROBIN_Splitter_5931));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5932() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5932WEIGHTED_ROUND_ROBIN_Splitter_5935, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_6129_6150_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5932WEIGHTED_ROUND_ROBIN_Splitter_5935, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_6129_6150_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_5937() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_6130_6151_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_6130_6151_join[0]));
	ENDFOR
}

void FFTReorderSimple_5938() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_6130_6151_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_6130_6151_join[1]));
	ENDFOR
}

void FFTReorderSimple_5939() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_6130_6151_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_6130_6151_join[2]));
	ENDFOR
}

void FFTReorderSimple_5940() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_6130_6151_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_6130_6151_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5935() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin6_FFTReorderSimple_Fiss_6130_6151_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5932WEIGHTED_ROUND_ROBIN_Splitter_5935));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5936() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5936WEIGHTED_ROUND_ROBIN_Splitter_5941, pop_float(&SplitJoin6_FFTReorderSimple_Fiss_6130_6151_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_5943() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_6131_6152_split[0]), &(SplitJoin8_FFTReorderSimple_Fiss_6131_6152_join[0]));
	ENDFOR
}

void FFTReorderSimple_5944() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_6131_6152_split[1]), &(SplitJoin8_FFTReorderSimple_Fiss_6131_6152_join[1]));
	ENDFOR
}

void FFTReorderSimple_5945() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_6131_6152_split[2]), &(SplitJoin8_FFTReorderSimple_Fiss_6131_6152_join[2]));
	ENDFOR
}

void FFTReorderSimple_5946() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_6131_6152_split[3]), &(SplitJoin8_FFTReorderSimple_Fiss_6131_6152_join[3]));
	ENDFOR
}

void FFTReorderSimple_5947() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_6131_6152_split[4]), &(SplitJoin8_FFTReorderSimple_Fiss_6131_6152_join[4]));
	ENDFOR
}

void FFTReorderSimple_5948() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_6131_6152_split[5]), &(SplitJoin8_FFTReorderSimple_Fiss_6131_6152_join[5]));
	ENDFOR
}

void FFTReorderSimple_5949() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_6131_6152_split[6]), &(SplitJoin8_FFTReorderSimple_Fiss_6131_6152_join[6]));
	ENDFOR
}

void FFTReorderSimple_5950() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_6131_6152_split[7]), &(SplitJoin8_FFTReorderSimple_Fiss_6131_6152_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5941() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin8_FFTReorderSimple_Fiss_6131_6152_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5936WEIGHTED_ROUND_ROBIN_Splitter_5941));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5942() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5942WEIGHTED_ROUND_ROBIN_Splitter_5951, pop_float(&SplitJoin8_FFTReorderSimple_Fiss_6131_6152_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_5953() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_split[0]), &(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_join[0]));
	ENDFOR
}

void FFTReorderSimple_5954() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_split[1]), &(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_join[1]));
	ENDFOR
}

void FFTReorderSimple_5955() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_split[2]), &(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_join[2]));
	ENDFOR
}

void FFTReorderSimple_5956() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_split[3]), &(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_join[3]));
	ENDFOR
}

void FFTReorderSimple_5957() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_split[4]), &(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_join[4]));
	ENDFOR
}

void FFTReorderSimple_5958() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_split[5]), &(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_join[5]));
	ENDFOR
}

void FFTReorderSimple_5959() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_split[6]), &(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_join[6]));
	ENDFOR
}

void FFTReorderSimple_5960() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_split[7]), &(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_join[7]));
	ENDFOR
}

void FFTReorderSimple_5961() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_split[8]), &(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_join[8]));
	ENDFOR
}

void FFTReorderSimple_5962() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_split[9]), &(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_join[9]));
	ENDFOR
}

void FFTReorderSimple_5963() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_split[10]), &(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_join[10]));
	ENDFOR
}

void FFTReorderSimple_5964() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_split[11]), &(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_join[11]));
	ENDFOR
}

void FFTReorderSimple_5965() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_split[12]), &(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_join[12]));
	ENDFOR
}

void FFTReorderSimple_5966() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_split[13]), &(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_join[13]));
	ENDFOR
}

void FFTReorderSimple_5967() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_split[14]), &(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_join[14]));
	ENDFOR
}

void FFTReorderSimple_5968() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_split[15]), &(SplitJoin10_FFTReorderSimple_Fiss_6132_6153_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5951() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_FFTReorderSimple_Fiss_6132_6153_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5942WEIGHTED_ROUND_ROBIN_Splitter_5951));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5952() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5952WEIGHTED_ROUND_ROBIN_Splitter_5969, pop_float(&SplitJoin10_FFTReorderSimple_Fiss_6132_6153_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&(*chanin), i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&(*chanin), i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&(*chanin), (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&(*chanin), (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_5971_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_5971_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&(*chanin)) ; 
			push_float(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_5971() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_6133_6154_split[0]), &(SplitJoin12_CombineDFT_Fiss_6133_6154_join[0]));
	ENDFOR
}

void CombineDFT_5972() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_6133_6154_split[1]), &(SplitJoin12_CombineDFT_Fiss_6133_6154_join[1]));
	ENDFOR
}

void CombineDFT_5973() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_6133_6154_split[2]), &(SplitJoin12_CombineDFT_Fiss_6133_6154_join[2]));
	ENDFOR
}

void CombineDFT_5974() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_6133_6154_split[3]), &(SplitJoin12_CombineDFT_Fiss_6133_6154_join[3]));
	ENDFOR
}

void CombineDFT_5975() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_6133_6154_split[4]), &(SplitJoin12_CombineDFT_Fiss_6133_6154_join[4]));
	ENDFOR
}

void CombineDFT_5976() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_6133_6154_split[5]), &(SplitJoin12_CombineDFT_Fiss_6133_6154_join[5]));
	ENDFOR
}

void CombineDFT_5977() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_6133_6154_split[6]), &(SplitJoin12_CombineDFT_Fiss_6133_6154_join[6]));
	ENDFOR
}

void CombineDFT_5978() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_6133_6154_split[7]), &(SplitJoin12_CombineDFT_Fiss_6133_6154_join[7]));
	ENDFOR
}

void CombineDFT_5979() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_6133_6154_split[8]), &(SplitJoin12_CombineDFT_Fiss_6133_6154_join[8]));
	ENDFOR
}

void CombineDFT_5980() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_6133_6154_split[9]), &(SplitJoin12_CombineDFT_Fiss_6133_6154_join[9]));
	ENDFOR
}

void CombineDFT_5981() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_6133_6154_split[10]), &(SplitJoin12_CombineDFT_Fiss_6133_6154_join[10]));
	ENDFOR
}

void CombineDFT_5982() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_6133_6154_split[11]), &(SplitJoin12_CombineDFT_Fiss_6133_6154_join[11]));
	ENDFOR
}

void CombineDFT_5983() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_6133_6154_split[12]), &(SplitJoin12_CombineDFT_Fiss_6133_6154_join[12]));
	ENDFOR
}

void CombineDFT_5984() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_6133_6154_split[13]), &(SplitJoin12_CombineDFT_Fiss_6133_6154_join[13]));
	ENDFOR
}

void CombineDFT_5985() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_6133_6154_split[14]), &(SplitJoin12_CombineDFT_Fiss_6133_6154_join[14]));
	ENDFOR
}

void CombineDFT_5986() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_6133_6154_split[15]), &(SplitJoin12_CombineDFT_Fiss_6133_6154_join[15]));
	ENDFOR
}

void CombineDFT_5987() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_6133_6154_split[16]), &(SplitJoin12_CombineDFT_Fiss_6133_6154_join[16]));
	ENDFOR
}

void CombineDFT_5988() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_6133_6154_split[17]), &(SplitJoin12_CombineDFT_Fiss_6133_6154_join[17]));
	ENDFOR
}

void CombineDFT_5989() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_6133_6154_split[18]), &(SplitJoin12_CombineDFT_Fiss_6133_6154_join[18]));
	ENDFOR
}

void CombineDFT_5990() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_6133_6154_split[19]), &(SplitJoin12_CombineDFT_Fiss_6133_6154_join[19]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5969() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 20, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin12_CombineDFT_Fiss_6133_6154_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5952WEIGHTED_ROUND_ROBIN_Splitter_5969));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5970() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 20, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5970WEIGHTED_ROUND_ROBIN_Splitter_5991, pop_float(&SplitJoin12_CombineDFT_Fiss_6133_6154_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5993() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_6134_6155_split[0]), &(SplitJoin14_CombineDFT_Fiss_6134_6155_join[0]));
	ENDFOR
}

void CombineDFT_5994() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_6134_6155_split[1]), &(SplitJoin14_CombineDFT_Fiss_6134_6155_join[1]));
	ENDFOR
}

void CombineDFT_5995() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_6134_6155_split[2]), &(SplitJoin14_CombineDFT_Fiss_6134_6155_join[2]));
	ENDFOR
}

void CombineDFT_5996() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_6134_6155_split[3]), &(SplitJoin14_CombineDFT_Fiss_6134_6155_join[3]));
	ENDFOR
}

void CombineDFT_5997() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_6134_6155_split[4]), &(SplitJoin14_CombineDFT_Fiss_6134_6155_join[4]));
	ENDFOR
}

void CombineDFT_5998() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_6134_6155_split[5]), &(SplitJoin14_CombineDFT_Fiss_6134_6155_join[5]));
	ENDFOR
}

void CombineDFT_5999() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_6134_6155_split[6]), &(SplitJoin14_CombineDFT_Fiss_6134_6155_join[6]));
	ENDFOR
}

void CombineDFT_6000() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_6134_6155_split[7]), &(SplitJoin14_CombineDFT_Fiss_6134_6155_join[7]));
	ENDFOR
}

void CombineDFT_6001() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_6134_6155_split[8]), &(SplitJoin14_CombineDFT_Fiss_6134_6155_join[8]));
	ENDFOR
}

void CombineDFT_6002() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_6134_6155_split[9]), &(SplitJoin14_CombineDFT_Fiss_6134_6155_join[9]));
	ENDFOR
}

void CombineDFT_6003() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_6134_6155_split[10]), &(SplitJoin14_CombineDFT_Fiss_6134_6155_join[10]));
	ENDFOR
}

void CombineDFT_6004() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_6134_6155_split[11]), &(SplitJoin14_CombineDFT_Fiss_6134_6155_join[11]));
	ENDFOR
}

void CombineDFT_6005() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_6134_6155_split[12]), &(SplitJoin14_CombineDFT_Fiss_6134_6155_join[12]));
	ENDFOR
}

void CombineDFT_6006() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_6134_6155_split[13]), &(SplitJoin14_CombineDFT_Fiss_6134_6155_join[13]));
	ENDFOR
}

void CombineDFT_6007() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_6134_6155_split[14]), &(SplitJoin14_CombineDFT_Fiss_6134_6155_join[14]));
	ENDFOR
}

void CombineDFT_6008() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_6134_6155_split[15]), &(SplitJoin14_CombineDFT_Fiss_6134_6155_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5991() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin14_CombineDFT_Fiss_6134_6155_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5970WEIGHTED_ROUND_ROBIN_Splitter_5991));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5992() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5992WEIGHTED_ROUND_ROBIN_Splitter_6009, pop_float(&SplitJoin14_CombineDFT_Fiss_6134_6155_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_6011() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_6135_6156_split[0]), &(SplitJoin16_CombineDFT_Fiss_6135_6156_join[0]));
	ENDFOR
}

void CombineDFT_6012() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_6135_6156_split[1]), &(SplitJoin16_CombineDFT_Fiss_6135_6156_join[1]));
	ENDFOR
}

void CombineDFT_6013() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_6135_6156_split[2]), &(SplitJoin16_CombineDFT_Fiss_6135_6156_join[2]));
	ENDFOR
}

void CombineDFT_6014() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_6135_6156_split[3]), &(SplitJoin16_CombineDFT_Fiss_6135_6156_join[3]));
	ENDFOR
}

void CombineDFT_6015() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_6135_6156_split[4]), &(SplitJoin16_CombineDFT_Fiss_6135_6156_join[4]));
	ENDFOR
}

void CombineDFT_6016() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_6135_6156_split[5]), &(SplitJoin16_CombineDFT_Fiss_6135_6156_join[5]));
	ENDFOR
}

void CombineDFT_6017() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_6135_6156_split[6]), &(SplitJoin16_CombineDFT_Fiss_6135_6156_join[6]));
	ENDFOR
}

void CombineDFT_6018() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_6135_6156_split[7]), &(SplitJoin16_CombineDFT_Fiss_6135_6156_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6009() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin16_CombineDFT_Fiss_6135_6156_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5992WEIGHTED_ROUND_ROBIN_Splitter_6009));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6010() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6010WEIGHTED_ROUND_ROBIN_Splitter_6019, pop_float(&SplitJoin16_CombineDFT_Fiss_6135_6156_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_6021() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_6136_6157_split[0]), &(SplitJoin18_CombineDFT_Fiss_6136_6157_join[0]));
	ENDFOR
}

void CombineDFT_6022() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_6136_6157_split[1]), &(SplitJoin18_CombineDFT_Fiss_6136_6157_join[1]));
	ENDFOR
}

void CombineDFT_6023() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_6136_6157_split[2]), &(SplitJoin18_CombineDFT_Fiss_6136_6157_join[2]));
	ENDFOR
}

void CombineDFT_6024() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_6136_6157_split[3]), &(SplitJoin18_CombineDFT_Fiss_6136_6157_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6019() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin18_CombineDFT_Fiss_6136_6157_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6010WEIGHTED_ROUND_ROBIN_Splitter_6019));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6020() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6020WEIGHTED_ROUND_ROBIN_Splitter_6025, pop_float(&SplitJoin18_CombineDFT_Fiss_6136_6157_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_6027() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin20_CombineDFT_Fiss_6137_6158_split[0]), &(SplitJoin20_CombineDFT_Fiss_6137_6158_join[0]));
	ENDFOR
}

void CombineDFT_6028() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin20_CombineDFT_Fiss_6137_6158_split[1]), &(SplitJoin20_CombineDFT_Fiss_6137_6158_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6025() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_6137_6158_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6020WEIGHTED_ROUND_ROBIN_Splitter_6025));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_6137_6158_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6020WEIGHTED_ROUND_ROBIN_Splitter_6025));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6026() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6026CombineDFT_5906, pop_float(&SplitJoin20_CombineDFT_Fiss_6137_6158_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6026CombineDFT_5906, pop_float(&SplitJoin20_CombineDFT_Fiss_6137_6158_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_5906() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_6026CombineDFT_5906), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_5885_5921_6128_6149_join[0]));
	ENDFOR
}

void FFTReorderSimple_5907() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_5885_5921_6128_6149_split[1]), &(FFTReorderSimple_5907WEIGHTED_ROUND_ROBIN_Splitter_6029));
	ENDFOR
}

void FFTReorderSimple_6031() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin94_FFTReorderSimple_Fiss_6138_6159_split[0]), &(SplitJoin94_FFTReorderSimple_Fiss_6138_6159_join[0]));
	ENDFOR
}

void FFTReorderSimple_6032() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin94_FFTReorderSimple_Fiss_6138_6159_split[1]), &(SplitJoin94_FFTReorderSimple_Fiss_6138_6159_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6029() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin94_FFTReorderSimple_Fiss_6138_6159_split[0], pop_float(&FFTReorderSimple_5907WEIGHTED_ROUND_ROBIN_Splitter_6029));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin94_FFTReorderSimple_Fiss_6138_6159_split[1], pop_float(&FFTReorderSimple_5907WEIGHTED_ROUND_ROBIN_Splitter_6029));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6030() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6030WEIGHTED_ROUND_ROBIN_Splitter_6033, pop_float(&SplitJoin94_FFTReorderSimple_Fiss_6138_6159_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6030WEIGHTED_ROUND_ROBIN_Splitter_6033, pop_float(&SplitJoin94_FFTReorderSimple_Fiss_6138_6159_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_6035() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin96_FFTReorderSimple_Fiss_6139_6160_split[0]), &(SplitJoin96_FFTReorderSimple_Fiss_6139_6160_join[0]));
	ENDFOR
}

void FFTReorderSimple_6036() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin96_FFTReorderSimple_Fiss_6139_6160_split[1]), &(SplitJoin96_FFTReorderSimple_Fiss_6139_6160_join[1]));
	ENDFOR
}

void FFTReorderSimple_6037() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin96_FFTReorderSimple_Fiss_6139_6160_split[2]), &(SplitJoin96_FFTReorderSimple_Fiss_6139_6160_join[2]));
	ENDFOR
}

void FFTReorderSimple_6038() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin96_FFTReorderSimple_Fiss_6139_6160_split[3]), &(SplitJoin96_FFTReorderSimple_Fiss_6139_6160_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6033() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin96_FFTReorderSimple_Fiss_6139_6160_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6030WEIGHTED_ROUND_ROBIN_Splitter_6033));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6034() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6034WEIGHTED_ROUND_ROBIN_Splitter_6039, pop_float(&SplitJoin96_FFTReorderSimple_Fiss_6139_6160_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_6041() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin98_FFTReorderSimple_Fiss_6140_6161_split[0]), &(SplitJoin98_FFTReorderSimple_Fiss_6140_6161_join[0]));
	ENDFOR
}

void FFTReorderSimple_6042() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin98_FFTReorderSimple_Fiss_6140_6161_split[1]), &(SplitJoin98_FFTReorderSimple_Fiss_6140_6161_join[1]));
	ENDFOR
}

void FFTReorderSimple_6043() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin98_FFTReorderSimple_Fiss_6140_6161_split[2]), &(SplitJoin98_FFTReorderSimple_Fiss_6140_6161_join[2]));
	ENDFOR
}

void FFTReorderSimple_6044() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin98_FFTReorderSimple_Fiss_6140_6161_split[3]), &(SplitJoin98_FFTReorderSimple_Fiss_6140_6161_join[3]));
	ENDFOR
}

void FFTReorderSimple_6045() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin98_FFTReorderSimple_Fiss_6140_6161_split[4]), &(SplitJoin98_FFTReorderSimple_Fiss_6140_6161_join[4]));
	ENDFOR
}

void FFTReorderSimple_6046() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin98_FFTReorderSimple_Fiss_6140_6161_split[5]), &(SplitJoin98_FFTReorderSimple_Fiss_6140_6161_join[5]));
	ENDFOR
}

void FFTReorderSimple_6047() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin98_FFTReorderSimple_Fiss_6140_6161_split[6]), &(SplitJoin98_FFTReorderSimple_Fiss_6140_6161_join[6]));
	ENDFOR
}

void FFTReorderSimple_6048() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin98_FFTReorderSimple_Fiss_6140_6161_split[7]), &(SplitJoin98_FFTReorderSimple_Fiss_6140_6161_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6039() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin98_FFTReorderSimple_Fiss_6140_6161_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6034WEIGHTED_ROUND_ROBIN_Splitter_6039));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6040() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6040WEIGHTED_ROUND_ROBIN_Splitter_6049, pop_float(&SplitJoin98_FFTReorderSimple_Fiss_6140_6161_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_6051() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_split[0]), &(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_join[0]));
	ENDFOR
}

void FFTReorderSimple_6052() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_split[1]), &(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_join[1]));
	ENDFOR
}

void FFTReorderSimple_6053() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_split[2]), &(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_join[2]));
	ENDFOR
}

void FFTReorderSimple_6054() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_split[3]), &(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_join[3]));
	ENDFOR
}

void FFTReorderSimple_6055() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_split[4]), &(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_join[4]));
	ENDFOR
}

void FFTReorderSimple_6056() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_split[5]), &(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_join[5]));
	ENDFOR
}

void FFTReorderSimple_6057() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_split[6]), &(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_join[6]));
	ENDFOR
}

void FFTReorderSimple_6058() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_split[7]), &(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_join[7]));
	ENDFOR
}

void FFTReorderSimple_6059() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_split[8]), &(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_join[8]));
	ENDFOR
}

void FFTReorderSimple_6060() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_split[9]), &(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_join[9]));
	ENDFOR
}

void FFTReorderSimple_6061() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_split[10]), &(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_join[10]));
	ENDFOR
}

void FFTReorderSimple_6062() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_split[11]), &(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_join[11]));
	ENDFOR
}

void FFTReorderSimple_6063() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_split[12]), &(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_join[12]));
	ENDFOR
}

void FFTReorderSimple_6064() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_split[13]), &(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_join[13]));
	ENDFOR
}

void FFTReorderSimple_6065() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_split[14]), &(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_join[14]));
	ENDFOR
}

void FFTReorderSimple_6066() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_split[15]), &(SplitJoin100_FFTReorderSimple_Fiss_6141_6162_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6049() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin100_FFTReorderSimple_Fiss_6141_6162_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6040WEIGHTED_ROUND_ROBIN_Splitter_6049));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6050() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6050WEIGHTED_ROUND_ROBIN_Splitter_6067, pop_float(&SplitJoin100_FFTReorderSimple_Fiss_6141_6162_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_6069() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_6142_6163_split[0]), &(SplitJoin102_CombineDFT_Fiss_6142_6163_join[0]));
	ENDFOR
}

void CombineDFT_6070() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_6142_6163_split[1]), &(SplitJoin102_CombineDFT_Fiss_6142_6163_join[1]));
	ENDFOR
}

void CombineDFT_6071() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_6142_6163_split[2]), &(SplitJoin102_CombineDFT_Fiss_6142_6163_join[2]));
	ENDFOR
}

void CombineDFT_6072() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_6142_6163_split[3]), &(SplitJoin102_CombineDFT_Fiss_6142_6163_join[3]));
	ENDFOR
}

void CombineDFT_6073() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_6142_6163_split[4]), &(SplitJoin102_CombineDFT_Fiss_6142_6163_join[4]));
	ENDFOR
}

void CombineDFT_6074() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_6142_6163_split[5]), &(SplitJoin102_CombineDFT_Fiss_6142_6163_join[5]));
	ENDFOR
}

void CombineDFT_6075() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_6142_6163_split[6]), &(SplitJoin102_CombineDFT_Fiss_6142_6163_join[6]));
	ENDFOR
}

void CombineDFT_6076() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_6142_6163_split[7]), &(SplitJoin102_CombineDFT_Fiss_6142_6163_join[7]));
	ENDFOR
}

void CombineDFT_6077() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_6142_6163_split[8]), &(SplitJoin102_CombineDFT_Fiss_6142_6163_join[8]));
	ENDFOR
}

void CombineDFT_6078() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_6142_6163_split[9]), &(SplitJoin102_CombineDFT_Fiss_6142_6163_join[9]));
	ENDFOR
}

void CombineDFT_6079() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_6142_6163_split[10]), &(SplitJoin102_CombineDFT_Fiss_6142_6163_join[10]));
	ENDFOR
}

void CombineDFT_6080() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_6142_6163_split[11]), &(SplitJoin102_CombineDFT_Fiss_6142_6163_join[11]));
	ENDFOR
}

void CombineDFT_6081() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_6142_6163_split[12]), &(SplitJoin102_CombineDFT_Fiss_6142_6163_join[12]));
	ENDFOR
}

void CombineDFT_6082() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_6142_6163_split[13]), &(SplitJoin102_CombineDFT_Fiss_6142_6163_join[13]));
	ENDFOR
}

void CombineDFT_6083() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_6142_6163_split[14]), &(SplitJoin102_CombineDFT_Fiss_6142_6163_join[14]));
	ENDFOR
}

void CombineDFT_6084() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_6142_6163_split[15]), &(SplitJoin102_CombineDFT_Fiss_6142_6163_join[15]));
	ENDFOR
}

void CombineDFT_6085() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_6142_6163_split[16]), &(SplitJoin102_CombineDFT_Fiss_6142_6163_join[16]));
	ENDFOR
}

void CombineDFT_6086() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_6142_6163_split[17]), &(SplitJoin102_CombineDFT_Fiss_6142_6163_join[17]));
	ENDFOR
}

void CombineDFT_6087() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_6142_6163_split[18]), &(SplitJoin102_CombineDFT_Fiss_6142_6163_join[18]));
	ENDFOR
}

void CombineDFT_6088() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin102_CombineDFT_Fiss_6142_6163_split[19]), &(SplitJoin102_CombineDFT_Fiss_6142_6163_join[19]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6067() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 20, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin102_CombineDFT_Fiss_6142_6163_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6050WEIGHTED_ROUND_ROBIN_Splitter_6067));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6068() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 20, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6068WEIGHTED_ROUND_ROBIN_Splitter_6089, pop_float(&SplitJoin102_CombineDFT_Fiss_6142_6163_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_6091() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin104_CombineDFT_Fiss_6143_6164_split[0]), &(SplitJoin104_CombineDFT_Fiss_6143_6164_join[0]));
	ENDFOR
}

void CombineDFT_6092() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin104_CombineDFT_Fiss_6143_6164_split[1]), &(SplitJoin104_CombineDFT_Fiss_6143_6164_join[1]));
	ENDFOR
}

void CombineDFT_6093() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin104_CombineDFT_Fiss_6143_6164_split[2]), &(SplitJoin104_CombineDFT_Fiss_6143_6164_join[2]));
	ENDFOR
}

void CombineDFT_6094() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin104_CombineDFT_Fiss_6143_6164_split[3]), &(SplitJoin104_CombineDFT_Fiss_6143_6164_join[3]));
	ENDFOR
}

void CombineDFT_6095() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin104_CombineDFT_Fiss_6143_6164_split[4]), &(SplitJoin104_CombineDFT_Fiss_6143_6164_join[4]));
	ENDFOR
}

void CombineDFT_6096() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin104_CombineDFT_Fiss_6143_6164_split[5]), &(SplitJoin104_CombineDFT_Fiss_6143_6164_join[5]));
	ENDFOR
}

void CombineDFT_6097() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin104_CombineDFT_Fiss_6143_6164_split[6]), &(SplitJoin104_CombineDFT_Fiss_6143_6164_join[6]));
	ENDFOR
}

void CombineDFT_6098() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin104_CombineDFT_Fiss_6143_6164_split[7]), &(SplitJoin104_CombineDFT_Fiss_6143_6164_join[7]));
	ENDFOR
}

void CombineDFT_6099() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin104_CombineDFT_Fiss_6143_6164_split[8]), &(SplitJoin104_CombineDFT_Fiss_6143_6164_join[8]));
	ENDFOR
}

void CombineDFT_6100() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin104_CombineDFT_Fiss_6143_6164_split[9]), &(SplitJoin104_CombineDFT_Fiss_6143_6164_join[9]));
	ENDFOR
}

void CombineDFT_6101() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin104_CombineDFT_Fiss_6143_6164_split[10]), &(SplitJoin104_CombineDFT_Fiss_6143_6164_join[10]));
	ENDFOR
}

void CombineDFT_6102() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin104_CombineDFT_Fiss_6143_6164_split[11]), &(SplitJoin104_CombineDFT_Fiss_6143_6164_join[11]));
	ENDFOR
}

void CombineDFT_6103() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin104_CombineDFT_Fiss_6143_6164_split[12]), &(SplitJoin104_CombineDFT_Fiss_6143_6164_join[12]));
	ENDFOR
}

void CombineDFT_6104() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin104_CombineDFT_Fiss_6143_6164_split[13]), &(SplitJoin104_CombineDFT_Fiss_6143_6164_join[13]));
	ENDFOR
}

void CombineDFT_6105() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin104_CombineDFT_Fiss_6143_6164_split[14]), &(SplitJoin104_CombineDFT_Fiss_6143_6164_join[14]));
	ENDFOR
}

void CombineDFT_6106() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin104_CombineDFT_Fiss_6143_6164_split[15]), &(SplitJoin104_CombineDFT_Fiss_6143_6164_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6089() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin104_CombineDFT_Fiss_6143_6164_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6068WEIGHTED_ROUND_ROBIN_Splitter_6089));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6090() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6090WEIGHTED_ROUND_ROBIN_Splitter_6107, pop_float(&SplitJoin104_CombineDFT_Fiss_6143_6164_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_6109() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin106_CombineDFT_Fiss_6144_6165_split[0]), &(SplitJoin106_CombineDFT_Fiss_6144_6165_join[0]));
	ENDFOR
}

void CombineDFT_6110() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin106_CombineDFT_Fiss_6144_6165_split[1]), &(SplitJoin106_CombineDFT_Fiss_6144_6165_join[1]));
	ENDFOR
}

void CombineDFT_6111() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin106_CombineDFT_Fiss_6144_6165_split[2]), &(SplitJoin106_CombineDFT_Fiss_6144_6165_join[2]));
	ENDFOR
}

void CombineDFT_6112() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin106_CombineDFT_Fiss_6144_6165_split[3]), &(SplitJoin106_CombineDFT_Fiss_6144_6165_join[3]));
	ENDFOR
}

void CombineDFT_6113() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin106_CombineDFT_Fiss_6144_6165_split[4]), &(SplitJoin106_CombineDFT_Fiss_6144_6165_join[4]));
	ENDFOR
}

void CombineDFT_6114() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin106_CombineDFT_Fiss_6144_6165_split[5]), &(SplitJoin106_CombineDFT_Fiss_6144_6165_join[5]));
	ENDFOR
}

void CombineDFT_6115() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin106_CombineDFT_Fiss_6144_6165_split[6]), &(SplitJoin106_CombineDFT_Fiss_6144_6165_join[6]));
	ENDFOR
}

void CombineDFT_6116() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin106_CombineDFT_Fiss_6144_6165_split[7]), &(SplitJoin106_CombineDFT_Fiss_6144_6165_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6107() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin106_CombineDFT_Fiss_6144_6165_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6090WEIGHTED_ROUND_ROBIN_Splitter_6107));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6108() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6108WEIGHTED_ROUND_ROBIN_Splitter_6117, pop_float(&SplitJoin106_CombineDFT_Fiss_6144_6165_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_6119() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_6145_6166_split[0]), &(SplitJoin108_CombineDFT_Fiss_6145_6166_join[0]));
	ENDFOR
}

void CombineDFT_6120() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_6145_6166_split[1]), &(SplitJoin108_CombineDFT_Fiss_6145_6166_join[1]));
	ENDFOR
}

void CombineDFT_6121() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_6145_6166_split[2]), &(SplitJoin108_CombineDFT_Fiss_6145_6166_join[2]));
	ENDFOR
}

void CombineDFT_6122() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin108_CombineDFT_Fiss_6145_6166_split[3]), &(SplitJoin108_CombineDFT_Fiss_6145_6166_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6117() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin108_CombineDFT_Fiss_6145_6166_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6108WEIGHTED_ROUND_ROBIN_Splitter_6117));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6118() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6118WEIGHTED_ROUND_ROBIN_Splitter_6123, pop_float(&SplitJoin108_CombineDFT_Fiss_6145_6166_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_6125() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_6146_6167_split[0]), &(SplitJoin110_CombineDFT_Fiss_6146_6167_join[0]));
	ENDFOR
}

void CombineDFT_6126() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(SplitJoin110_CombineDFT_Fiss_6146_6167_split[1]), &(SplitJoin110_CombineDFT_Fiss_6146_6167_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6123() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin110_CombineDFT_Fiss_6146_6167_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6118WEIGHTED_ROUND_ROBIN_Splitter_6123));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin110_CombineDFT_Fiss_6146_6167_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6118WEIGHTED_ROUND_ROBIN_Splitter_6123));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6124() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6124CombineDFT_5917, pop_float(&SplitJoin110_CombineDFT_Fiss_6146_6167_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6124CombineDFT_5917, pop_float(&SplitJoin110_CombineDFT_Fiss_6146_6167_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_5917() {
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_6124CombineDFT_5917), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_5885_5921_6128_6149_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5919() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_5885_5921_6128_6149_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5928WEIGHTED_ROUND_ROBIN_Splitter_5919));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_5885_5921_6128_6149_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5928WEIGHTED_ROUND_ROBIN_Splitter_5919));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5920() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5920FloatPrinter_5918, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_5885_5921_6128_6149_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5920FloatPrinter_5918, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_5885_5921_6128_6149_join[1]));
		ENDFOR
	ENDFOR
}}

void FloatPrinter(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void FloatPrinter_5918() {
	FOR(uint32_t, __iter_steady_, 0, <, 1280, __iter_steady_++)
		FloatPrinter(&(WEIGHTED_ROUND_ROBIN_Joiner_5920FloatPrinter_5918));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 16, __iter_init_0_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_6132_6153_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 16, __iter_init_1_++)
		init_buffer_float(&SplitJoin104_CombineDFT_Fiss_6143_6164_split[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5928WEIGHTED_ROUND_ROBIN_Splitter_5919);
	init_buffer_float(&FFTReorderSimple_5907WEIGHTED_ROUND_ROBIN_Splitter_6029);
	FOR(int, __iter_init_2_, 0, <, 16, __iter_init_2_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_6134_6155_split[__iter_init_2_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6050WEIGHTED_ROUND_ROBIN_Splitter_6067);
	FOR(int, __iter_init_3_, 0, <, 16, __iter_init_3_++)
		init_buffer_float(&SplitJoin104_CombineDFT_Fiss_6143_6164_join[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6090WEIGHTED_ROUND_ROBIN_Splitter_6107);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5952WEIGHTED_ROUND_ROBIN_Splitter_5969);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5920FloatPrinter_5918);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6026CombineDFT_5906);
	FOR(int, __iter_init_4_, 0, <, 4, __iter_init_4_++)
		init_buffer_float(&SplitJoin96_FFTReorderSimple_Fiss_6139_6160_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 20, __iter_init_5_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_6133_6154_join[__iter_init_5_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6020WEIGHTED_ROUND_ROBIN_Splitter_6025);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6068WEIGHTED_ROUND_ROBIN_Splitter_6089);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5992WEIGHTED_ROUND_ROBIN_Splitter_6009);
	FOR(int, __iter_init_6_, 0, <, 4, __iter_init_6_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_6136_6157_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 2, __iter_init_7_++)
		init_buffer_float(&SplitJoin94_FFTReorderSimple_Fiss_6138_6159_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_float(&SplitJoin98_FFTReorderSimple_Fiss_6140_6161_split[__iter_init_8_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6124CombineDFT_5917);
	init_buffer_float(&FFTReorderSimple_5896WEIGHTED_ROUND_ROBIN_Splitter_5931);
	FOR(int, __iter_init_9_, 0, <, 2, __iter_init_9_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_5885_5921_6128_6149_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_6129_6150_split[__iter_init_10_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6040WEIGHTED_ROUND_ROBIN_Splitter_6049);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6108WEIGHTED_ROUND_ROBIN_Splitter_6117);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5932WEIGHTED_ROUND_ROBIN_Splitter_5935);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5936WEIGHTED_ROUND_ROBIN_Splitter_5941);
	FOR(int, __iter_init_11_, 0, <, 20, __iter_init_11_++)
		init_buffer_float(&SplitJoin102_CombineDFT_Fiss_6142_6163_split[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 2, __iter_init_12_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_6127_6148_split[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 4, __iter_init_13_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_6130_6151_split[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 16, __iter_init_14_++)
		init_buffer_float(&SplitJoin100_FFTReorderSimple_Fiss_6141_6162_split[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 2, __iter_init_15_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_6137_6158_split[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_float(&SplitJoin110_CombineDFT_Fiss_6146_6167_split[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 4, __iter_init_17_++)
		init_buffer_float(&SplitJoin108_CombineDFT_Fiss_6145_6166_join[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 20, __iter_init_18_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_6133_6154_split[__iter_init_18_]);
	ENDFOR
	FOR(int, __iter_init_19_, 0, <, 16, __iter_init_19_++)
		init_buffer_float(&SplitJoin100_FFTReorderSimple_Fiss_6141_6162_join[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 4, __iter_init_20_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_6130_6151_join[__iter_init_20_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5942WEIGHTED_ROUND_ROBIN_Splitter_5951);
	FOR(int, __iter_init_21_, 0, <, 2, __iter_init_21_++)
		init_buffer_float(&SplitJoin110_CombineDFT_Fiss_6146_6167_join[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 2, __iter_init_22_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_6127_6148_join[__iter_init_22_]);
	ENDFOR
	FOR(int, __iter_init_23_, 0, <, 16, __iter_init_23_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_6134_6155_join[__iter_init_23_]);
	ENDFOR
	FOR(int, __iter_init_24_, 0, <, 2, __iter_init_24_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_6129_6150_join[__iter_init_24_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5970WEIGHTED_ROUND_ROBIN_Splitter_5991);
	FOR(int, __iter_init_25_, 0, <, 8, __iter_init_25_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_6135_6156_split[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 8, __iter_init_26_++)
		init_buffer_float(&SplitJoin106_CombineDFT_Fiss_6144_6165_join[__iter_init_26_]);
	ENDFOR
	FOR(int, __iter_init_27_, 0, <, 8, __iter_init_27_++)
		init_buffer_float(&SplitJoin98_FFTReorderSimple_Fiss_6140_6161_join[__iter_init_27_]);
	ENDFOR
	FOR(int, __iter_init_28_, 0, <, 8, __iter_init_28_++)
		init_buffer_float(&SplitJoin106_CombineDFT_Fiss_6144_6165_split[__iter_init_28_]);
	ENDFOR
	FOR(int, __iter_init_29_, 0, <, 16, __iter_init_29_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_6132_6153_split[__iter_init_29_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6010WEIGHTED_ROUND_ROBIN_Splitter_6019);
	FOR(int, __iter_init_30_, 0, <, 8, __iter_init_30_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_6131_6152_join[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 20, __iter_init_31_++)
		init_buffer_float(&SplitJoin102_CombineDFT_Fiss_6142_6163_join[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 4, __iter_init_32_++)
		init_buffer_float(&SplitJoin96_FFTReorderSimple_Fiss_6139_6160_split[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 8, __iter_init_33_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_6135_6156_join[__iter_init_33_]);
	ENDFOR
	FOR(int, __iter_init_34_, 0, <, 2, __iter_init_34_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_5885_5921_6128_6149_split[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 2, __iter_init_35_++)
		init_buffer_float(&SplitJoin94_FFTReorderSimple_Fiss_6138_6159_join[__iter_init_35_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6030WEIGHTED_ROUND_ROBIN_Splitter_6033);
	FOR(int, __iter_init_36_, 0, <, 8, __iter_init_36_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_6131_6152_split[__iter_init_36_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6118WEIGHTED_ROUND_ROBIN_Splitter_6123);
	FOR(int, __iter_init_37_, 0, <, 4, __iter_init_37_++)
		init_buffer_float(&SplitJoin108_CombineDFT_Fiss_6145_6166_split[__iter_init_37_]);
	ENDFOR
	FOR(int, __iter_init_38_, 0, <, 2, __iter_init_38_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_6137_6158_join[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 4, __iter_init_39_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_6136_6157_split[__iter_init_39_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6034WEIGHTED_ROUND_ROBIN_Splitter_6039);
// --- init: CombineDFT_5971
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5971_s.w[i] = real ; 
		CombineDFT_5971_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5972
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5972_s.w[i] = real ; 
		CombineDFT_5972_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5973
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5973_s.w[i] = real ; 
		CombineDFT_5973_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5974
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5974_s.w[i] = real ; 
		CombineDFT_5974_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5975
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5975_s.w[i] = real ; 
		CombineDFT_5975_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5976
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5976_s.w[i] = real ; 
		CombineDFT_5976_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5977
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5977_s.w[i] = real ; 
		CombineDFT_5977_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5978
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5978_s.w[i] = real ; 
		CombineDFT_5978_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5979
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5979_s.w[i] = real ; 
		CombineDFT_5979_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5980
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5980_s.w[i] = real ; 
		CombineDFT_5980_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5981
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5981_s.w[i] = real ; 
		CombineDFT_5981_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5982
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5982_s.w[i] = real ; 
		CombineDFT_5982_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5983
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5983_s.w[i] = real ; 
		CombineDFT_5983_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5984
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5984_s.w[i] = real ; 
		CombineDFT_5984_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5985
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5985_s.w[i] = real ; 
		CombineDFT_5985_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5986
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5986_s.w[i] = real ; 
		CombineDFT_5986_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5987
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5987_s.w[i] = real ; 
		CombineDFT_5987_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5988
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5988_s.w[i] = real ; 
		CombineDFT_5988_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5989
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5989_s.w[i] = real ; 
		CombineDFT_5989_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5990
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_5990_s.w[i] = real ; 
		CombineDFT_5990_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5993
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5993_s.w[i] = real ; 
		CombineDFT_5993_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5994
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5994_s.w[i] = real ; 
		CombineDFT_5994_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5995
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5995_s.w[i] = real ; 
		CombineDFT_5995_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5996
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5996_s.w[i] = real ; 
		CombineDFT_5996_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5997
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5997_s.w[i] = real ; 
		CombineDFT_5997_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5998
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5998_s.w[i] = real ; 
		CombineDFT_5998_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5999
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_5999_s.w[i] = real ; 
		CombineDFT_5999_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6000
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6000_s.w[i] = real ; 
		CombineDFT_6000_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6001
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6001_s.w[i] = real ; 
		CombineDFT_6001_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6002
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6002_s.w[i] = real ; 
		CombineDFT_6002_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6003
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6003_s.w[i] = real ; 
		CombineDFT_6003_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6004
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6004_s.w[i] = real ; 
		CombineDFT_6004_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6005
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6005_s.w[i] = real ; 
		CombineDFT_6005_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6006
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6006_s.w[i] = real ; 
		CombineDFT_6006_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6007
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6007_s.w[i] = real ; 
		CombineDFT_6007_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6008
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6008_s.w[i] = real ; 
		CombineDFT_6008_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6011
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_6011_s.w[i] = real ; 
		CombineDFT_6011_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6012
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_6012_s.w[i] = real ; 
		CombineDFT_6012_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6013
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_6013_s.w[i] = real ; 
		CombineDFT_6013_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6014
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_6014_s.w[i] = real ; 
		CombineDFT_6014_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6015
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_6015_s.w[i] = real ; 
		CombineDFT_6015_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6016
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_6016_s.w[i] = real ; 
		CombineDFT_6016_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6017
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_6017_s.w[i] = real ; 
		CombineDFT_6017_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6018
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_6018_s.w[i] = real ; 
		CombineDFT_6018_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6021
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_6021_s.w[i] = real ; 
		CombineDFT_6021_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6022
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_6022_s.w[i] = real ; 
		CombineDFT_6022_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6023
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_6023_s.w[i] = real ; 
		CombineDFT_6023_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6024
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_6024_s.w[i] = real ; 
		CombineDFT_6024_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6027
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_6027_s.w[i] = real ; 
		CombineDFT_6027_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6028
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_6028_s.w[i] = real ; 
		CombineDFT_6028_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5906
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_5906_s.w[i] = real ; 
		CombineDFT_5906_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6069
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6069_s.w[i] = real ; 
		CombineDFT_6069_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6070
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6070_s.w[i] = real ; 
		CombineDFT_6070_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6071
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6071_s.w[i] = real ; 
		CombineDFT_6071_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6072
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6072_s.w[i] = real ; 
		CombineDFT_6072_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6073
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6073_s.w[i] = real ; 
		CombineDFT_6073_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6074
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6074_s.w[i] = real ; 
		CombineDFT_6074_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6075
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6075_s.w[i] = real ; 
		CombineDFT_6075_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6076
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6076_s.w[i] = real ; 
		CombineDFT_6076_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6077
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6077_s.w[i] = real ; 
		CombineDFT_6077_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6078
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6078_s.w[i] = real ; 
		CombineDFT_6078_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6079
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6079_s.w[i] = real ; 
		CombineDFT_6079_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6080
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6080_s.w[i] = real ; 
		CombineDFT_6080_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6081
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6081_s.w[i] = real ; 
		CombineDFT_6081_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6082
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6082_s.w[i] = real ; 
		CombineDFT_6082_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6083
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6083_s.w[i] = real ; 
		CombineDFT_6083_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6084
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6084_s.w[i] = real ; 
		CombineDFT_6084_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6085
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6085_s.w[i] = real ; 
		CombineDFT_6085_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6086
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6086_s.w[i] = real ; 
		CombineDFT_6086_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6087
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6087_s.w[i] = real ; 
		CombineDFT_6087_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6088
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_6088_s.w[i] = real ; 
		CombineDFT_6088_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6091
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6091_s.w[i] = real ; 
		CombineDFT_6091_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6092
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6092_s.w[i] = real ; 
		CombineDFT_6092_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6093
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6093_s.w[i] = real ; 
		CombineDFT_6093_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6094
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6094_s.w[i] = real ; 
		CombineDFT_6094_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6095
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6095_s.w[i] = real ; 
		CombineDFT_6095_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6096
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6096_s.w[i] = real ; 
		CombineDFT_6096_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6097
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6097_s.w[i] = real ; 
		CombineDFT_6097_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6098
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6098_s.w[i] = real ; 
		CombineDFT_6098_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6099
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6099_s.w[i] = real ; 
		CombineDFT_6099_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6100
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6100_s.w[i] = real ; 
		CombineDFT_6100_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6101
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6101_s.w[i] = real ; 
		CombineDFT_6101_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6102
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6102_s.w[i] = real ; 
		CombineDFT_6102_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6103
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6103_s.w[i] = real ; 
		CombineDFT_6103_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6104
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6104_s.w[i] = real ; 
		CombineDFT_6104_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6105
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6105_s.w[i] = real ; 
		CombineDFT_6105_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6106
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_6106_s.w[i] = real ; 
		CombineDFT_6106_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6109
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_6109_s.w[i] = real ; 
		CombineDFT_6109_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6110
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_6110_s.w[i] = real ; 
		CombineDFT_6110_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6111
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_6111_s.w[i] = real ; 
		CombineDFT_6111_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6112
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_6112_s.w[i] = real ; 
		CombineDFT_6112_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6113
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_6113_s.w[i] = real ; 
		CombineDFT_6113_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6114
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_6114_s.w[i] = real ; 
		CombineDFT_6114_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6115
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_6115_s.w[i] = real ; 
		CombineDFT_6115_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6116
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_6116_s.w[i] = real ; 
		CombineDFT_6116_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6119
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_6119_s.w[i] = real ; 
		CombineDFT_6119_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6120
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_6120_s.w[i] = real ; 
		CombineDFT_6120_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6121
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_6121_s.w[i] = real ; 
		CombineDFT_6121_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6122
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_6122_s.w[i] = real ; 
		CombineDFT_6122_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6125
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_6125_s.w[i] = real ; 
		CombineDFT_6125_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_6126
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_6126_s.w[i] = real ; 
		CombineDFT_6126_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_5917
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_5917_s.w[i] = real ; 
		CombineDFT_5917_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_5927();
			FFTTestSource_5929();
			FFTTestSource_5930();
		WEIGHTED_ROUND_ROBIN_Joiner_5928();
		WEIGHTED_ROUND_ROBIN_Splitter_5919();
			FFTReorderSimple_5896();
			WEIGHTED_ROUND_ROBIN_Splitter_5931();
				FFTReorderSimple_5933();
				FFTReorderSimple_5934();
			WEIGHTED_ROUND_ROBIN_Joiner_5932();
			WEIGHTED_ROUND_ROBIN_Splitter_5935();
				FFTReorderSimple_5937();
				FFTReorderSimple_5938();
				FFTReorderSimple_5939();
				FFTReorderSimple_5940();
			WEIGHTED_ROUND_ROBIN_Joiner_5936();
			WEIGHTED_ROUND_ROBIN_Splitter_5941();
				FFTReorderSimple_5943();
				FFTReorderSimple_5944();
				FFTReorderSimple_5945();
				FFTReorderSimple_5946();
				FFTReorderSimple_5947();
				FFTReorderSimple_5948();
				FFTReorderSimple_5949();
				FFTReorderSimple_5950();
			WEIGHTED_ROUND_ROBIN_Joiner_5942();
			WEIGHTED_ROUND_ROBIN_Splitter_5951();
				FFTReorderSimple_5953();
				FFTReorderSimple_5954();
				FFTReorderSimple_5955();
				FFTReorderSimple_5956();
				FFTReorderSimple_5957();
				FFTReorderSimple_5958();
				FFTReorderSimple_5959();
				FFTReorderSimple_5960();
				FFTReorderSimple_5961();
				FFTReorderSimple_5962();
				FFTReorderSimple_5963();
				FFTReorderSimple_5964();
				FFTReorderSimple_5965();
				FFTReorderSimple_5966();
				FFTReorderSimple_5967();
				FFTReorderSimple_5968();
			WEIGHTED_ROUND_ROBIN_Joiner_5952();
			WEIGHTED_ROUND_ROBIN_Splitter_5969();
				CombineDFT_5971();
				CombineDFT_5972();
				CombineDFT_5973();
				CombineDFT_5974();
				CombineDFT_5975();
				CombineDFT_5976();
				CombineDFT_5977();
				CombineDFT_5978();
				CombineDFT_5979();
				CombineDFT_5980();
				CombineDFT_5981();
				CombineDFT_5982();
				CombineDFT_5983();
				CombineDFT_5984();
				CombineDFT_5985();
				CombineDFT_5986();
				CombineDFT_5987();
				CombineDFT_5988();
				CombineDFT_5989();
				CombineDFT_5990();
			WEIGHTED_ROUND_ROBIN_Joiner_5970();
			WEIGHTED_ROUND_ROBIN_Splitter_5991();
				CombineDFT_5993();
				CombineDFT_5994();
				CombineDFT_5995();
				CombineDFT_5996();
				CombineDFT_5997();
				CombineDFT_5998();
				CombineDFT_5999();
				CombineDFT_6000();
				CombineDFT_6001();
				CombineDFT_6002();
				CombineDFT_6003();
				CombineDFT_6004();
				CombineDFT_6005();
				CombineDFT_6006();
				CombineDFT_6007();
				CombineDFT_6008();
			WEIGHTED_ROUND_ROBIN_Joiner_5992();
			WEIGHTED_ROUND_ROBIN_Splitter_6009();
				CombineDFT_6011();
				CombineDFT_6012();
				CombineDFT_6013();
				CombineDFT_6014();
				CombineDFT_6015();
				CombineDFT_6016();
				CombineDFT_6017();
				CombineDFT_6018();
			WEIGHTED_ROUND_ROBIN_Joiner_6010();
			WEIGHTED_ROUND_ROBIN_Splitter_6019();
				CombineDFT_6021();
				CombineDFT_6022();
				CombineDFT_6023();
				CombineDFT_6024();
			WEIGHTED_ROUND_ROBIN_Joiner_6020();
			WEIGHTED_ROUND_ROBIN_Splitter_6025();
				CombineDFT_6027();
				CombineDFT_6028();
			WEIGHTED_ROUND_ROBIN_Joiner_6026();
			CombineDFT_5906();
			FFTReorderSimple_5907();
			WEIGHTED_ROUND_ROBIN_Splitter_6029();
				FFTReorderSimple_6031();
				FFTReorderSimple_6032();
			WEIGHTED_ROUND_ROBIN_Joiner_6030();
			WEIGHTED_ROUND_ROBIN_Splitter_6033();
				FFTReorderSimple_6035();
				FFTReorderSimple_6036();
				FFTReorderSimple_6037();
				FFTReorderSimple_6038();
			WEIGHTED_ROUND_ROBIN_Joiner_6034();
			WEIGHTED_ROUND_ROBIN_Splitter_6039();
				FFTReorderSimple_6041();
				FFTReorderSimple_6042();
				FFTReorderSimple_6043();
				FFTReorderSimple_6044();
				FFTReorderSimple_6045();
				FFTReorderSimple_6046();
				FFTReorderSimple_6047();
				FFTReorderSimple_6048();
			WEIGHTED_ROUND_ROBIN_Joiner_6040();
			WEIGHTED_ROUND_ROBIN_Splitter_6049();
				FFTReorderSimple_6051();
				FFTReorderSimple_6052();
				FFTReorderSimple_6053();
				FFTReorderSimple_6054();
				FFTReorderSimple_6055();
				FFTReorderSimple_6056();
				FFTReorderSimple_6057();
				FFTReorderSimple_6058();
				FFTReorderSimple_6059();
				FFTReorderSimple_6060();
				FFTReorderSimple_6061();
				FFTReorderSimple_6062();
				FFTReorderSimple_6063();
				FFTReorderSimple_6064();
				FFTReorderSimple_6065();
				FFTReorderSimple_6066();
			WEIGHTED_ROUND_ROBIN_Joiner_6050();
			WEIGHTED_ROUND_ROBIN_Splitter_6067();
				CombineDFT_6069();
				CombineDFT_6070();
				CombineDFT_6071();
				CombineDFT_6072();
				CombineDFT_6073();
				CombineDFT_6074();
				CombineDFT_6075();
				CombineDFT_6076();
				CombineDFT_6077();
				CombineDFT_6078();
				CombineDFT_6079();
				CombineDFT_6080();
				CombineDFT_6081();
				CombineDFT_6082();
				CombineDFT_6083();
				CombineDFT_6084();
				CombineDFT_6085();
				CombineDFT_6086();
				CombineDFT_6087();
				CombineDFT_6088();
			WEIGHTED_ROUND_ROBIN_Joiner_6068();
			WEIGHTED_ROUND_ROBIN_Splitter_6089();
				CombineDFT_6091();
				CombineDFT_6092();
				CombineDFT_6093();
				CombineDFT_6094();
				CombineDFT_6095();
				CombineDFT_6096();
				CombineDFT_6097();
				CombineDFT_6098();
				CombineDFT_6099();
				CombineDFT_6100();
				CombineDFT_6101();
				CombineDFT_6102();
				CombineDFT_6103();
				CombineDFT_6104();
				CombineDFT_6105();
				CombineDFT_6106();
			WEIGHTED_ROUND_ROBIN_Joiner_6090();
			WEIGHTED_ROUND_ROBIN_Splitter_6107();
				CombineDFT_6109();
				CombineDFT_6110();
				CombineDFT_6111();
				CombineDFT_6112();
				CombineDFT_6113();
				CombineDFT_6114();
				CombineDFT_6115();
				CombineDFT_6116();
			WEIGHTED_ROUND_ROBIN_Joiner_6108();
			WEIGHTED_ROUND_ROBIN_Splitter_6117();
				CombineDFT_6119();
				CombineDFT_6120();
				CombineDFT_6121();
				CombineDFT_6122();
			WEIGHTED_ROUND_ROBIN_Joiner_6118();
			WEIGHTED_ROUND_ROBIN_Splitter_6123();
				CombineDFT_6125();
				CombineDFT_6126();
			WEIGHTED_ROUND_ROBIN_Joiner_6124();
			CombineDFT_5917();
		WEIGHTED_ROUND_ROBIN_Joiner_5920();
		FloatPrinter_5918();
	ENDFOR
	return EXIT_SUCCESS;
}
