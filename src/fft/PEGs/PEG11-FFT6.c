#include "PEG11-FFT6.h"

buffer_complex_t SplitJoin14_CombineDFT_Fiss_4757_4767_join[4];
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_4751_4761_split[4];
buffer_complex_t CombineDFT_4668CPrinter_4669;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_4754_4764_split[11];
buffer_complex_t SplitJoin8_CombineDFT_Fiss_4754_4764_join[11];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_4755_4765_join[11];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_4750_4760_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4731WEIGHTED_ROUND_ROBIN_Splitter_4740;
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_4753_4763_join[11];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4682WEIGHTED_ROUND_ROBIN_Splitter_4691;
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_4753_4763_split[11];
buffer_complex_t FFTReorderSimple_4658WEIGHTED_ROUND_ROBIN_Splitter_4671;
buffer_complex_t FFTTestSource_4657FFTReorderSimple_4658;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4672WEIGHTED_ROUND_ROBIN_Splitter_4675;
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_4751_4761_join[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4676WEIGHTED_ROUND_ROBIN_Splitter_4681;
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_4752_4762_split[8];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4718WEIGHTED_ROUND_ROBIN_Splitter_4730;
buffer_complex_t SplitJoin10_CombineDFT_Fiss_4755_4765_split[11];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_4758_4768_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4741WEIGHTED_ROUND_ROBIN_Splitter_4746;
buffer_complex_t SplitJoin12_CombineDFT_Fiss_4756_4766_join[8];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4747CombineDFT_4668;
buffer_complex_t SplitJoin12_CombineDFT_Fiss_4756_4766_split[8];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_4750_4760_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4692WEIGHTED_ROUND_ROBIN_Splitter_4704;
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_4752_4762_join[8];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_4757_4767_split[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4705WEIGHTED_ROUND_ROBIN_Splitter_4717;
buffer_complex_t SplitJoin16_CombineDFT_Fiss_4758_4768_join[2];


CombineDFT_4706_t CombineDFT_4706_s;
CombineDFT_4706_t CombineDFT_4707_s;
CombineDFT_4706_t CombineDFT_4708_s;
CombineDFT_4706_t CombineDFT_4709_s;
CombineDFT_4706_t CombineDFT_4710_s;
CombineDFT_4706_t CombineDFT_4711_s;
CombineDFT_4706_t CombineDFT_4712_s;
CombineDFT_4706_t CombineDFT_4713_s;
CombineDFT_4706_t CombineDFT_4714_s;
CombineDFT_4706_t CombineDFT_4715_s;
CombineDFT_4706_t CombineDFT_4716_s;
CombineDFT_4706_t CombineDFT_4719_s;
CombineDFT_4706_t CombineDFT_4720_s;
CombineDFT_4706_t CombineDFT_4721_s;
CombineDFT_4706_t CombineDFT_4722_s;
CombineDFT_4706_t CombineDFT_4723_s;
CombineDFT_4706_t CombineDFT_4724_s;
CombineDFT_4706_t CombineDFT_4725_s;
CombineDFT_4706_t CombineDFT_4726_s;
CombineDFT_4706_t CombineDFT_4727_s;
CombineDFT_4706_t CombineDFT_4728_s;
CombineDFT_4706_t CombineDFT_4729_s;
CombineDFT_4706_t CombineDFT_4732_s;
CombineDFT_4706_t CombineDFT_4733_s;
CombineDFT_4706_t CombineDFT_4734_s;
CombineDFT_4706_t CombineDFT_4735_s;
CombineDFT_4706_t CombineDFT_4736_s;
CombineDFT_4706_t CombineDFT_4737_s;
CombineDFT_4706_t CombineDFT_4738_s;
CombineDFT_4706_t CombineDFT_4739_s;
CombineDFT_4706_t CombineDFT_4742_s;
CombineDFT_4706_t CombineDFT_4743_s;
CombineDFT_4706_t CombineDFT_4744_s;
CombineDFT_4706_t CombineDFT_4745_s;
CombineDFT_4706_t CombineDFT_4748_s;
CombineDFT_4706_t CombineDFT_4749_s;
CombineDFT_4706_t CombineDFT_4668_s;

void FFTTestSource(buffer_complex_t *chanout) {
		complex_t c1;
		complex_t zero;
		c1.real = 1.0 ; 
		c1.imag = 0.0 ; 
		zero.real = 0.0 ; 
		zero.imag = 0.0 ; 
		push_complex(&(*chanout), zero) ; 
		push_complex(&(*chanout), c1) ; 
		FOR(int, i, 0,  < , 62, i++) {
			push_complex(&(*chanout), zero) ; 
		}
		ENDFOR
	}


void FFTTestSource_4657() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTTestSource(&(FFTTestSource_4657FFTReorderSimple_4658));
	ENDFOR
}

void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 64, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&(*chanin)) ; 
		}
		ENDFOR
	}


void FFTReorderSimple_4658() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(FFTTestSource_4657FFTReorderSimple_4658), &(FFTReorderSimple_4658WEIGHTED_ROUND_ROBIN_Splitter_4671));
	ENDFOR
}

void FFTReorderSimple_4673() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_4750_4760_split[0]), &(SplitJoin0_FFTReorderSimple_Fiss_4750_4760_join[0]));
	ENDFOR
}

void FFTReorderSimple_4674() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_4750_4760_split[1]), &(SplitJoin0_FFTReorderSimple_Fiss_4750_4760_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4671() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_4750_4760_split[0], pop_complex(&FFTReorderSimple_4658WEIGHTED_ROUND_ROBIN_Splitter_4671));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_4750_4760_split[1], pop_complex(&FFTReorderSimple_4658WEIGHTED_ROUND_ROBIN_Splitter_4671));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4672() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4672WEIGHTED_ROUND_ROBIN_Splitter_4675, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_4750_4760_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4672WEIGHTED_ROUND_ROBIN_Splitter_4675, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_4750_4760_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_4677() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_4751_4761_split[0]), &(SplitJoin2_FFTReorderSimple_Fiss_4751_4761_join[0]));
	ENDFOR
}

void FFTReorderSimple_4678() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_4751_4761_split[1]), &(SplitJoin2_FFTReorderSimple_Fiss_4751_4761_join[1]));
	ENDFOR
}

void FFTReorderSimple_4679() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_4751_4761_split[2]), &(SplitJoin2_FFTReorderSimple_Fiss_4751_4761_join[2]));
	ENDFOR
}

void FFTReorderSimple_4680() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_4751_4761_split[3]), &(SplitJoin2_FFTReorderSimple_Fiss_4751_4761_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4675() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin2_FFTReorderSimple_Fiss_4751_4761_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4672WEIGHTED_ROUND_ROBIN_Splitter_4675));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4676() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4676WEIGHTED_ROUND_ROBIN_Splitter_4681, pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_4751_4761_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_4683() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_4752_4762_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_4752_4762_join[0]));
	ENDFOR
}

void FFTReorderSimple_4684() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_4752_4762_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_4752_4762_join[1]));
	ENDFOR
}

void FFTReorderSimple_4685() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_4752_4762_split[2]), &(SplitJoin4_FFTReorderSimple_Fiss_4752_4762_join[2]));
	ENDFOR
}

void FFTReorderSimple_4686() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_4752_4762_split[3]), &(SplitJoin4_FFTReorderSimple_Fiss_4752_4762_join[3]));
	ENDFOR
}

void FFTReorderSimple_4687() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_4752_4762_split[4]), &(SplitJoin4_FFTReorderSimple_Fiss_4752_4762_join[4]));
	ENDFOR
}

void FFTReorderSimple_4688() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_4752_4762_split[5]), &(SplitJoin4_FFTReorderSimple_Fiss_4752_4762_join[5]));
	ENDFOR
}

void FFTReorderSimple_4689() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_4752_4762_split[6]), &(SplitJoin4_FFTReorderSimple_Fiss_4752_4762_join[6]));
	ENDFOR
}

void FFTReorderSimple_4690() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_4752_4762_split[7]), &(SplitJoin4_FFTReorderSimple_Fiss_4752_4762_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4681() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4752_4762_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4676WEIGHTED_ROUND_ROBIN_Splitter_4681));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4682() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4682WEIGHTED_ROUND_ROBIN_Splitter_4691, pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_4752_4762_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_4693() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4753_4763_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_4753_4763_join[0]));
	ENDFOR
}

void FFTReorderSimple_4694() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4753_4763_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_4753_4763_join[1]));
	ENDFOR
}

void FFTReorderSimple_4695() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4753_4763_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_4753_4763_join[2]));
	ENDFOR
}

void FFTReorderSimple_4696() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4753_4763_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_4753_4763_join[3]));
	ENDFOR
}

void FFTReorderSimple_4697() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4753_4763_split[4]), &(SplitJoin6_FFTReorderSimple_Fiss_4753_4763_join[4]));
	ENDFOR
}

void FFTReorderSimple_4698() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4753_4763_split[5]), &(SplitJoin6_FFTReorderSimple_Fiss_4753_4763_join[5]));
	ENDFOR
}

void FFTReorderSimple_4699() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4753_4763_split[6]), &(SplitJoin6_FFTReorderSimple_Fiss_4753_4763_join[6]));
	ENDFOR
}

void FFTReorderSimple_4700() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4753_4763_split[7]), &(SplitJoin6_FFTReorderSimple_Fiss_4753_4763_join[7]));
	ENDFOR
}

void FFTReorderSimple_4701() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4753_4763_split[8]), &(SplitJoin6_FFTReorderSimple_Fiss_4753_4763_join[8]));
	ENDFOR
}

void FFTReorderSimple_4702() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4753_4763_split[9]), &(SplitJoin6_FFTReorderSimple_Fiss_4753_4763_join[9]));
	ENDFOR
}

void FFTReorderSimple_4703() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_4753_4763_split[10]), &(SplitJoin6_FFTReorderSimple_Fiss_4753_4763_join[10]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4691() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 11, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4753_4763_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4682WEIGHTED_ROUND_ROBIN_Splitter_4691));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4692() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 11, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4692WEIGHTED_ROUND_ROBIN_Splitter_4704, pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_4753_4763_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&(*chanin), (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4706_s.wn.real) - (w.imag * CombineDFT_4706_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4706_s.wn.imag) + (w.imag * CombineDFT_4706_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&(*chanin)) ; 
			push_complex(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_4706() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4754_4764_split[0]), &(SplitJoin8_CombineDFT_Fiss_4754_4764_join[0]));
	ENDFOR
}

void CombineDFT_4707() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4754_4764_split[1]), &(SplitJoin8_CombineDFT_Fiss_4754_4764_join[1]));
	ENDFOR
}

void CombineDFT_4708() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4754_4764_split[2]), &(SplitJoin8_CombineDFT_Fiss_4754_4764_join[2]));
	ENDFOR
}

void CombineDFT_4709() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4754_4764_split[3]), &(SplitJoin8_CombineDFT_Fiss_4754_4764_join[3]));
	ENDFOR
}

void CombineDFT_4710() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4754_4764_split[4]), &(SplitJoin8_CombineDFT_Fiss_4754_4764_join[4]));
	ENDFOR
}

void CombineDFT_4711() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4754_4764_split[5]), &(SplitJoin8_CombineDFT_Fiss_4754_4764_join[5]));
	ENDFOR
}

void CombineDFT_4712() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4754_4764_split[6]), &(SplitJoin8_CombineDFT_Fiss_4754_4764_join[6]));
	ENDFOR
}

void CombineDFT_4713() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4754_4764_split[7]), &(SplitJoin8_CombineDFT_Fiss_4754_4764_join[7]));
	ENDFOR
}

void CombineDFT_4714() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4754_4764_split[8]), &(SplitJoin8_CombineDFT_Fiss_4754_4764_join[8]));
	ENDFOR
}

void CombineDFT_4715() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4754_4764_split[9]), &(SplitJoin8_CombineDFT_Fiss_4754_4764_join[9]));
	ENDFOR
}

void CombineDFT_4716() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_4754_4764_split[10]), &(SplitJoin8_CombineDFT_Fiss_4754_4764_join[10]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4704() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 11, __iter_++)
			push_complex(&SplitJoin8_CombineDFT_Fiss_4754_4764_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4692WEIGHTED_ROUND_ROBIN_Splitter_4704));
			push_complex(&SplitJoin8_CombineDFT_Fiss_4754_4764_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4692WEIGHTED_ROUND_ROBIN_Splitter_4704));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4705() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 11, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4705WEIGHTED_ROUND_ROBIN_Splitter_4717, pop_complex(&SplitJoin8_CombineDFT_Fiss_4754_4764_join[__iter_]));
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4705WEIGHTED_ROUND_ROBIN_Splitter_4717, pop_complex(&SplitJoin8_CombineDFT_Fiss_4754_4764_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_4719() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4755_4765_split[0]), &(SplitJoin10_CombineDFT_Fiss_4755_4765_join[0]));
	ENDFOR
}

void CombineDFT_4720() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4755_4765_split[1]), &(SplitJoin10_CombineDFT_Fiss_4755_4765_join[1]));
	ENDFOR
}

void CombineDFT_4721() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4755_4765_split[2]), &(SplitJoin10_CombineDFT_Fiss_4755_4765_join[2]));
	ENDFOR
}

void CombineDFT_4722() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4755_4765_split[3]), &(SplitJoin10_CombineDFT_Fiss_4755_4765_join[3]));
	ENDFOR
}

void CombineDFT_4723() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4755_4765_split[4]), &(SplitJoin10_CombineDFT_Fiss_4755_4765_join[4]));
	ENDFOR
}

void CombineDFT_4724() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4755_4765_split[5]), &(SplitJoin10_CombineDFT_Fiss_4755_4765_join[5]));
	ENDFOR
}

void CombineDFT_4725() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4755_4765_split[6]), &(SplitJoin10_CombineDFT_Fiss_4755_4765_join[6]));
	ENDFOR
}

void CombineDFT_4726() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4755_4765_split[7]), &(SplitJoin10_CombineDFT_Fiss_4755_4765_join[7]));
	ENDFOR
}

void CombineDFT_4727() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4755_4765_split[8]), &(SplitJoin10_CombineDFT_Fiss_4755_4765_join[8]));
	ENDFOR
}

void CombineDFT_4728() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4755_4765_split[9]), &(SplitJoin10_CombineDFT_Fiss_4755_4765_join[9]));
	ENDFOR
}

void CombineDFT_4729() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_4755_4765_split[10]), &(SplitJoin10_CombineDFT_Fiss_4755_4765_join[10]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4717() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 11, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin10_CombineDFT_Fiss_4755_4765_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4705WEIGHTED_ROUND_ROBIN_Splitter_4717));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4718() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 11, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4718WEIGHTED_ROUND_ROBIN_Splitter_4730, pop_complex(&SplitJoin10_CombineDFT_Fiss_4755_4765_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_4732() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_4756_4766_split[0]), &(SplitJoin12_CombineDFT_Fiss_4756_4766_join[0]));
	ENDFOR
}

void CombineDFT_4733() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_4756_4766_split[1]), &(SplitJoin12_CombineDFT_Fiss_4756_4766_join[1]));
	ENDFOR
}

void CombineDFT_4734() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_4756_4766_split[2]), &(SplitJoin12_CombineDFT_Fiss_4756_4766_join[2]));
	ENDFOR
}

void CombineDFT_4735() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_4756_4766_split[3]), &(SplitJoin12_CombineDFT_Fiss_4756_4766_join[3]));
	ENDFOR
}

void CombineDFT_4736() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_4756_4766_split[4]), &(SplitJoin12_CombineDFT_Fiss_4756_4766_join[4]));
	ENDFOR
}

void CombineDFT_4737() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_4756_4766_split[5]), &(SplitJoin12_CombineDFT_Fiss_4756_4766_join[5]));
	ENDFOR
}

void CombineDFT_4738() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_4756_4766_split[6]), &(SplitJoin12_CombineDFT_Fiss_4756_4766_join[6]));
	ENDFOR
}

void CombineDFT_4739() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_4756_4766_split[7]), &(SplitJoin12_CombineDFT_Fiss_4756_4766_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4730() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin12_CombineDFT_Fiss_4756_4766_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4718WEIGHTED_ROUND_ROBIN_Splitter_4730));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4731() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4731WEIGHTED_ROUND_ROBIN_Splitter_4740, pop_complex(&SplitJoin12_CombineDFT_Fiss_4756_4766_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_4742() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_4757_4767_split[0]), &(SplitJoin14_CombineDFT_Fiss_4757_4767_join[0]));
	ENDFOR
}

void CombineDFT_4743() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_4757_4767_split[1]), &(SplitJoin14_CombineDFT_Fiss_4757_4767_join[1]));
	ENDFOR
}

void CombineDFT_4744() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_4757_4767_split[2]), &(SplitJoin14_CombineDFT_Fiss_4757_4767_join[2]));
	ENDFOR
}

void CombineDFT_4745() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_4757_4767_split[3]), &(SplitJoin14_CombineDFT_Fiss_4757_4767_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4740() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin14_CombineDFT_Fiss_4757_4767_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4731WEIGHTED_ROUND_ROBIN_Splitter_4740));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4741() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4741WEIGHTED_ROUND_ROBIN_Splitter_4746, pop_complex(&SplitJoin14_CombineDFT_Fiss_4757_4767_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_4748() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_4758_4768_split[0]), &(SplitJoin16_CombineDFT_Fiss_4758_4768_join[0]));
	ENDFOR
}

void CombineDFT_4749() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_4758_4768_split[1]), &(SplitJoin16_CombineDFT_Fiss_4758_4768_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4746() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_4758_4768_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4741WEIGHTED_ROUND_ROBIN_Splitter_4746));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_4758_4768_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4741WEIGHTED_ROUND_ROBIN_Splitter_4746));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4747() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4747CombineDFT_4668, pop_complex(&SplitJoin16_CombineDFT_Fiss_4758_4768_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4747CombineDFT_4668, pop_complex(&SplitJoin16_CombineDFT_Fiss_4758_4768_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_4668() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_4747CombineDFT_4668), &(CombineDFT_4668CPrinter_4669));
	ENDFOR
}

void CPrinter(buffer_complex_t *chanin) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		printf("%.10f", c.real);
		printf("\n");
		printf("%.10f", c.imag);
		printf("\n");
	}


void CPrinter_4669() {
	FOR(uint32_t, __iter_steady_, 0, <, 704, __iter_steady_++)
		CPrinter(&(CombineDFT_4668CPrinter_4669));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 4, __iter_init_0_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_4757_4767_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 4, __iter_init_1_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_4751_4761_split[__iter_init_1_]);
	ENDFOR
	init_buffer_complex(&CombineDFT_4668CPrinter_4669);
	FOR(int, __iter_init_2_, 0, <, 11, __iter_init_2_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_4754_4764_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 11, __iter_init_3_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_4754_4764_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 11, __iter_init_4_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_4755_4765_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 2, __iter_init_5_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_4750_4760_split[__iter_init_5_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4731WEIGHTED_ROUND_ROBIN_Splitter_4740);
	FOR(int, __iter_init_6_, 0, <, 11, __iter_init_6_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_4753_4763_join[__iter_init_6_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4682WEIGHTED_ROUND_ROBIN_Splitter_4691);
	FOR(int, __iter_init_7_, 0, <, 11, __iter_init_7_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_4753_4763_split[__iter_init_7_]);
	ENDFOR
	init_buffer_complex(&FFTReorderSimple_4658WEIGHTED_ROUND_ROBIN_Splitter_4671);
	init_buffer_complex(&FFTTestSource_4657FFTReorderSimple_4658);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4672WEIGHTED_ROUND_ROBIN_Splitter_4675);
	FOR(int, __iter_init_8_, 0, <, 4, __iter_init_8_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_4751_4761_join[__iter_init_8_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4676WEIGHTED_ROUND_ROBIN_Splitter_4681);
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_4752_4762_split[__iter_init_9_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4718WEIGHTED_ROUND_ROBIN_Splitter_4730);
	FOR(int, __iter_init_10_, 0, <, 11, __iter_init_10_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_4755_4765_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 2, __iter_init_11_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_4758_4768_split[__iter_init_11_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4741WEIGHTED_ROUND_ROBIN_Splitter_4746);
	FOR(int, __iter_init_12_, 0, <, 8, __iter_init_12_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_4756_4766_join[__iter_init_12_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4747CombineDFT_4668);
	FOR(int, __iter_init_13_, 0, <, 8, __iter_init_13_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_4756_4766_split[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_4750_4760_join[__iter_init_14_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4692WEIGHTED_ROUND_ROBIN_Splitter_4704);
	FOR(int, __iter_init_15_, 0, <, 8, __iter_init_15_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_4752_4762_join[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 4, __iter_init_16_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_4757_4767_split[__iter_init_16_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4705WEIGHTED_ROUND_ROBIN_Splitter_4717);
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_4758_4768_join[__iter_init_17_]);
	ENDFOR
// --- init: CombineDFT_4706
	 {
	 ; 
	CombineDFT_4706_s.wn.real = -1.0 ; 
	CombineDFT_4706_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4707
	 {
	CombineDFT_4707_s.wn.real = -1.0 ; 
	CombineDFT_4707_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4708
	 {
	CombineDFT_4708_s.wn.real = -1.0 ; 
	CombineDFT_4708_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4709
	 {
	CombineDFT_4709_s.wn.real = -1.0 ; 
	CombineDFT_4709_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4710
	 {
	CombineDFT_4710_s.wn.real = -1.0 ; 
	CombineDFT_4710_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4711
	 {
	CombineDFT_4711_s.wn.real = -1.0 ; 
	CombineDFT_4711_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4712
	 {
	CombineDFT_4712_s.wn.real = -1.0 ; 
	CombineDFT_4712_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4713
	 {
	CombineDFT_4713_s.wn.real = -1.0 ; 
	CombineDFT_4713_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4714
	 {
	CombineDFT_4714_s.wn.real = -1.0 ; 
	CombineDFT_4714_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4715
	 {
	CombineDFT_4715_s.wn.real = -1.0 ; 
	CombineDFT_4715_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4716
	 {
	CombineDFT_4716_s.wn.real = -1.0 ; 
	CombineDFT_4716_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4719
	 {
	CombineDFT_4719_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4719_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4720
	 {
	CombineDFT_4720_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4720_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4721
	 {
	CombineDFT_4721_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4721_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4722
	 {
	CombineDFT_4722_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4722_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4723
	 {
	CombineDFT_4723_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4723_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4724
	 {
	CombineDFT_4724_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4724_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4725
	 {
	CombineDFT_4725_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4725_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4726
	 {
	CombineDFT_4726_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4726_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4727
	 {
	CombineDFT_4727_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4727_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4728
	 {
	CombineDFT_4728_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4728_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4729
	 {
	CombineDFT_4729_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4729_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4732
	 {
	CombineDFT_4732_s.wn.real = 0.70710677 ; 
	CombineDFT_4732_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4733
	 {
	CombineDFT_4733_s.wn.real = 0.70710677 ; 
	CombineDFT_4733_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4734
	 {
	CombineDFT_4734_s.wn.real = 0.70710677 ; 
	CombineDFT_4734_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4735
	 {
	CombineDFT_4735_s.wn.real = 0.70710677 ; 
	CombineDFT_4735_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4736
	 {
	CombineDFT_4736_s.wn.real = 0.70710677 ; 
	CombineDFT_4736_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4737
	 {
	CombineDFT_4737_s.wn.real = 0.70710677 ; 
	CombineDFT_4737_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4738
	 {
	CombineDFT_4738_s.wn.real = 0.70710677 ; 
	CombineDFT_4738_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4739
	 {
	CombineDFT_4739_s.wn.real = 0.70710677 ; 
	CombineDFT_4739_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4742
	 {
	CombineDFT_4742_s.wn.real = 0.9238795 ; 
	CombineDFT_4742_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_4743
	 {
	CombineDFT_4743_s.wn.real = 0.9238795 ; 
	CombineDFT_4743_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_4744
	 {
	CombineDFT_4744_s.wn.real = 0.9238795 ; 
	CombineDFT_4744_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_4745
	 {
	CombineDFT_4745_s.wn.real = 0.9238795 ; 
	CombineDFT_4745_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_4748
	 {
	CombineDFT_4748_s.wn.real = 0.98078525 ; 
	CombineDFT_4748_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_4749
	 {
	CombineDFT_4749_s.wn.real = 0.98078525 ; 
	CombineDFT_4749_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_4668
	 {
	 ; 
	CombineDFT_4668_s.wn.real = 0.9951847 ; 
	CombineDFT_4668_s.wn.imag = -0.09801714 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FFTTestSource_4657();
		FFTReorderSimple_4658();
		WEIGHTED_ROUND_ROBIN_Splitter_4671();
			FFTReorderSimple_4673();
			FFTReorderSimple_4674();
		WEIGHTED_ROUND_ROBIN_Joiner_4672();
		WEIGHTED_ROUND_ROBIN_Splitter_4675();
			FFTReorderSimple_4677();
			FFTReorderSimple_4678();
			FFTReorderSimple_4679();
			FFTReorderSimple_4680();
		WEIGHTED_ROUND_ROBIN_Joiner_4676();
		WEIGHTED_ROUND_ROBIN_Splitter_4681();
			FFTReorderSimple_4683();
			FFTReorderSimple_4684();
			FFTReorderSimple_4685();
			FFTReorderSimple_4686();
			FFTReorderSimple_4687();
			FFTReorderSimple_4688();
			FFTReorderSimple_4689();
			FFTReorderSimple_4690();
		WEIGHTED_ROUND_ROBIN_Joiner_4682();
		WEIGHTED_ROUND_ROBIN_Splitter_4691();
			FFTReorderSimple_4693();
			FFTReorderSimple_4694();
			FFTReorderSimple_4695();
			FFTReorderSimple_4696();
			FFTReorderSimple_4697();
			FFTReorderSimple_4698();
			FFTReorderSimple_4699();
			FFTReorderSimple_4700();
			FFTReorderSimple_4701();
			FFTReorderSimple_4702();
			FFTReorderSimple_4703();
		WEIGHTED_ROUND_ROBIN_Joiner_4692();
		WEIGHTED_ROUND_ROBIN_Splitter_4704();
			CombineDFT_4706();
			CombineDFT_4707();
			CombineDFT_4708();
			CombineDFT_4709();
			CombineDFT_4710();
			CombineDFT_4711();
			CombineDFT_4712();
			CombineDFT_4713();
			CombineDFT_4714();
			CombineDFT_4715();
			CombineDFT_4716();
		WEIGHTED_ROUND_ROBIN_Joiner_4705();
		WEIGHTED_ROUND_ROBIN_Splitter_4717();
			CombineDFT_4719();
			CombineDFT_4720();
			CombineDFT_4721();
			CombineDFT_4722();
			CombineDFT_4723();
			CombineDFT_4724();
			CombineDFT_4725();
			CombineDFT_4726();
			CombineDFT_4727();
			CombineDFT_4728();
			CombineDFT_4729();
		WEIGHTED_ROUND_ROBIN_Joiner_4718();
		WEIGHTED_ROUND_ROBIN_Splitter_4730();
			CombineDFT_4732();
			CombineDFT_4733();
			CombineDFT_4734();
			CombineDFT_4735();
			CombineDFT_4736();
			CombineDFT_4737();
			CombineDFT_4738();
			CombineDFT_4739();
		WEIGHTED_ROUND_ROBIN_Joiner_4731();
		WEIGHTED_ROUND_ROBIN_Splitter_4740();
			CombineDFT_4742();
			CombineDFT_4743();
			CombineDFT_4744();
			CombineDFT_4745();
		WEIGHTED_ROUND_ROBIN_Joiner_4741();
		WEIGHTED_ROUND_ROBIN_Splitter_4746();
			CombineDFT_4748();
			CombineDFT_4749();
		WEIGHTED_ROUND_ROBIN_Joiner_4747();
		CombineDFT_4668();
		CPrinter_4669();
	ENDFOR
	return EXIT_SUCCESS;
}
