#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=32 on the compile command line
#else
#if BUF_SIZEMAX < 32
#error BUF_SIZEMAX too small, it must be at least 32
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif


void WEIGHTED_ROUND_ROBIN_Splitter_6572();
void source_6574();
void source_6575();
void WEIGHTED_ROUND_ROBIN_Joiner_6573();
void WEIGHTED_ROUND_ROBIN_Splitter_6415();
void WEIGHTED_ROUND_ROBIN_Splitter_6417();
void WEIGHTED_ROUND_ROBIN_Splitter_6419();
void WEIGHTED_ROUND_ROBIN_Splitter_6421();
void Identity_6245();
void Identity_6247();
void WEIGHTED_ROUND_ROBIN_Joiner_6422();
void WEIGHTED_ROUND_ROBIN_Splitter_6423();
void Identity_6251();
void Identity_6253();
void WEIGHTED_ROUND_ROBIN_Joiner_6424();
void WEIGHTED_ROUND_ROBIN_Joiner_6420();
void WEIGHTED_ROUND_ROBIN_Splitter_6425();
void WEIGHTED_ROUND_ROBIN_Splitter_6427();
void Identity_6259();
void Identity_6261();
void WEIGHTED_ROUND_ROBIN_Joiner_6428();
void WEIGHTED_ROUND_ROBIN_Splitter_6429();
void Identity_6265();
void Identity_6267();
void WEIGHTED_ROUND_ROBIN_Joiner_6430();
void WEIGHTED_ROUND_ROBIN_Joiner_6426();
void WEIGHTED_ROUND_ROBIN_Joiner_6418();
void WEIGHTED_ROUND_ROBIN_Splitter_6431();
void WEIGHTED_ROUND_ROBIN_Splitter_6433();
void WEIGHTED_ROUND_ROBIN_Splitter_6435();
void Identity_6275();
void Identity_6277();
void WEIGHTED_ROUND_ROBIN_Joiner_6436();
void WEIGHTED_ROUND_ROBIN_Splitter_6437();
void Identity_6281();
void Identity_6283();
void WEIGHTED_ROUND_ROBIN_Joiner_6438();
void WEIGHTED_ROUND_ROBIN_Joiner_6434();
void WEIGHTED_ROUND_ROBIN_Splitter_6439();
void WEIGHTED_ROUND_ROBIN_Splitter_6441();
void Identity_6289();
void Identity_6291();
void WEIGHTED_ROUND_ROBIN_Joiner_6442();
void WEIGHTED_ROUND_ROBIN_Splitter_6443();
void Identity_6295();
void Identity_6297();
void WEIGHTED_ROUND_ROBIN_Joiner_6444();
void WEIGHTED_ROUND_ROBIN_Joiner_6440();
void WEIGHTED_ROUND_ROBIN_Joiner_6432();
void WEIGHTED_ROUND_ROBIN_Joiner_6416();
void WEIGHTED_ROUND_ROBIN_Splitter_6559();
void WEIGHTED_ROUND_ROBIN_Splitter_6560();
void Pre_CollapsedDataParallel_1_6392();
void butterfly_6299();
void Post_CollapsedDataParallel_2_6393();
void Pre_CollapsedDataParallel_1_6395();
void butterfly_6300();
void Post_CollapsedDataParallel_2_6396();
void Pre_CollapsedDataParallel_1_6398();
void butterfly_6301();
void Post_CollapsedDataParallel_2_6399();
void Pre_CollapsedDataParallel_1_6401();
void butterfly_6302();
void Post_CollapsedDataParallel_2_6402();
void WEIGHTED_ROUND_ROBIN_Joiner_6561();
void WEIGHTED_ROUND_ROBIN_Splitter_6562();
void Pre_CollapsedDataParallel_1_6404();
void butterfly_6303();
void Post_CollapsedDataParallel_2_6405();
void Pre_CollapsedDataParallel_1_6407();
void butterfly_6304();
void Post_CollapsedDataParallel_2_6408();
void Pre_CollapsedDataParallel_1_6410();
void butterfly_6305();
void Post_CollapsedDataParallel_2_6411();
void Pre_CollapsedDataParallel_1_6413();
void butterfly_6306();
void Post_CollapsedDataParallel_2_6414();
void WEIGHTED_ROUND_ROBIN_Joiner_6563();
void WEIGHTED_ROUND_ROBIN_Joiner_6564();
void WEIGHTED_ROUND_ROBIN_Splitter_6565();
void WEIGHTED_ROUND_ROBIN_Splitter_6566();
void WEIGHTED_ROUND_ROBIN_Splitter_6449();
void butterfly_6308();
void butterfly_6309();
void WEIGHTED_ROUND_ROBIN_Joiner_6450();
void WEIGHTED_ROUND_ROBIN_Splitter_6451();
void butterfly_6310();
void butterfly_6311();
void WEIGHTED_ROUND_ROBIN_Joiner_6452();
void WEIGHTED_ROUND_ROBIN_Joiner_6567();
void WEIGHTED_ROUND_ROBIN_Splitter_6568();
void WEIGHTED_ROUND_ROBIN_Splitter_6453();
void butterfly_6312();
void butterfly_6313();
void WEIGHTED_ROUND_ROBIN_Joiner_6454();
void WEIGHTED_ROUND_ROBIN_Splitter_6455();
void butterfly_6314();
void butterfly_6315();
void WEIGHTED_ROUND_ROBIN_Joiner_6456();
void WEIGHTED_ROUND_ROBIN_Joiner_6569();
void WEIGHTED_ROUND_ROBIN_Joiner_6570();
void WEIGHTED_ROUND_ROBIN_Splitter_6457();
void WEIGHTED_ROUND_ROBIN_Splitter_6459();
void butterfly_6317();
void butterfly_6318();
void butterfly_6319();
void butterfly_6320();
void WEIGHTED_ROUND_ROBIN_Joiner_6460();
void WEIGHTED_ROUND_ROBIN_Splitter_6461();
void butterfly_6321();
void butterfly_6322();
void butterfly_6323();
void butterfly_6324();
void WEIGHTED_ROUND_ROBIN_Joiner_6462();
void WEIGHTED_ROUND_ROBIN_Joiner_6458();
void WEIGHTED_ROUND_ROBIN_Splitter_6576();
void magnitude_6578();
void magnitude_6579();
void magnitude_6580();
void magnitude_6581();
void magnitude_6582();
void magnitude_6583();
void magnitude_6584();
void magnitude_6585();
void WEIGHTED_ROUND_ROBIN_Joiner_6577();
void sink_6326();

#ifdef __cplusplus
}
#endif
#endif
