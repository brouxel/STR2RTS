#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=5632 on the compile command line
#else
#if BUF_SIZEMAX < 5632
#error BUF_SIZEMAX too small, it must be at least 5632
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float w[2];
} CombineDFT_9801_t;

typedef struct {
	float w[4];
} CombineDFT_9814_t;

typedef struct {
	float w[8];
} CombineDFT_9827_t;

typedef struct {
	float w[16];
} CombineDFT_9837_t;

typedef struct {
	float w[32];
} CombineDFT_9843_t;

typedef struct {
	float w[64];
} CombineDFT_9741_t;
void WEIGHTED_ROUND_ROBIN_Splitter_9762();
void FFTTestSource_9764();
void FFTTestSource_9765();
void WEIGHTED_ROUND_ROBIN_Joiner_9763();
void WEIGHTED_ROUND_ROBIN_Splitter_9754();
void FFTReorderSimple_9731();
void WEIGHTED_ROUND_ROBIN_Splitter_9766();
void FFTReorderSimple_9768();
void FFTReorderSimple_9769();
void WEIGHTED_ROUND_ROBIN_Joiner_9767();
void WEIGHTED_ROUND_ROBIN_Splitter_9770();
void FFTReorderSimple_9772();
void FFTReorderSimple_9773();
void FFTReorderSimple_9774();
void FFTReorderSimple_9775();
void WEIGHTED_ROUND_ROBIN_Joiner_9771();
void WEIGHTED_ROUND_ROBIN_Splitter_9776();
void FFTReorderSimple_9778();
void FFTReorderSimple_9779();
void FFTReorderSimple_9780();
void FFTReorderSimple_9781();
void FFTReorderSimple_9782();
void FFTReorderSimple_9783();
void FFTReorderSimple_9784();
void FFTReorderSimple_9785();
void WEIGHTED_ROUND_ROBIN_Joiner_9777();
void WEIGHTED_ROUND_ROBIN_Splitter_9786();
void FFTReorderSimple_9788();
void FFTReorderSimple_9789();
void FFTReorderSimple_9790();
void FFTReorderSimple_9791();
void FFTReorderSimple_9792();
void FFTReorderSimple_9793();
void FFTReorderSimple_9794();
void FFTReorderSimple_9795();
void FFTReorderSimple_9796();
void FFTReorderSimple_9797();
void FFTReorderSimple_9798();
void WEIGHTED_ROUND_ROBIN_Joiner_9787();
void WEIGHTED_ROUND_ROBIN_Splitter_9799();
void CombineDFT_9801();
void CombineDFT_9802();
void CombineDFT_9803();
void CombineDFT_9804();
void CombineDFT_9805();
void CombineDFT_9806();
void CombineDFT_9807();
void CombineDFT_9808();
void CombineDFT_9809();
void CombineDFT_9810();
void CombineDFT_9811();
void WEIGHTED_ROUND_ROBIN_Joiner_9800();
void WEIGHTED_ROUND_ROBIN_Splitter_9812();
void CombineDFT_9814();
void CombineDFT_9815();
void CombineDFT_9816();
void CombineDFT_9817();
void CombineDFT_9818();
void CombineDFT_9819();
void CombineDFT_9820();
void CombineDFT_9821();
void CombineDFT_9822();
void CombineDFT_9823();
void CombineDFT_9824();
void WEIGHTED_ROUND_ROBIN_Joiner_9813();
void WEIGHTED_ROUND_ROBIN_Splitter_9825();
void CombineDFT_9827();
void CombineDFT_9828();
void CombineDFT_9829();
void CombineDFT_9830();
void CombineDFT_9831();
void CombineDFT_9832();
void CombineDFT_9833();
void CombineDFT_9834();
void WEIGHTED_ROUND_ROBIN_Joiner_9826();
void WEIGHTED_ROUND_ROBIN_Splitter_9835();
void CombineDFT_9837();
void CombineDFT_9838();
void CombineDFT_9839();
void CombineDFT_9840();
void WEIGHTED_ROUND_ROBIN_Joiner_9836();
void WEIGHTED_ROUND_ROBIN_Splitter_9841();
void CombineDFT_9843();
void CombineDFT_9844();
void WEIGHTED_ROUND_ROBIN_Joiner_9842();
void CombineDFT_9741();
void FFTReorderSimple_9742();
void WEIGHTED_ROUND_ROBIN_Splitter_9845();
void FFTReorderSimple_9847();
void FFTReorderSimple_9848();
void WEIGHTED_ROUND_ROBIN_Joiner_9846();
void WEIGHTED_ROUND_ROBIN_Splitter_9849();
void FFTReorderSimple_9851();
void FFTReorderSimple_9852();
void FFTReorderSimple_9853();
void FFTReorderSimple_9854();
void WEIGHTED_ROUND_ROBIN_Joiner_9850();
void WEIGHTED_ROUND_ROBIN_Splitter_9855();
void FFTReorderSimple_9857();
void FFTReorderSimple_9858();
void FFTReorderSimple_9859();
void FFTReorderSimple_9860();
void FFTReorderSimple_9861();
void FFTReorderSimple_9862();
void FFTReorderSimple_9863();
void FFTReorderSimple_9864();
void WEIGHTED_ROUND_ROBIN_Joiner_9856();
void WEIGHTED_ROUND_ROBIN_Splitter_9865();
void FFTReorderSimple_9867();
void FFTReorderSimple_9868();
void FFTReorderSimple_9869();
void FFTReorderSimple_9870();
void FFTReorderSimple_9871();
void FFTReorderSimple_9872();
void FFTReorderSimple_9873();
void FFTReorderSimple_9874();
void FFTReorderSimple_9875();
void FFTReorderSimple_9876();
void FFTReorderSimple_9877();
void WEIGHTED_ROUND_ROBIN_Joiner_9866();
void WEIGHTED_ROUND_ROBIN_Splitter_9878();
void CombineDFT_9880();
void CombineDFT_9881();
void CombineDFT_9882();
void CombineDFT_9883();
void CombineDFT_9884();
void CombineDFT_9885();
void CombineDFT_9886();
void CombineDFT_9887();
void CombineDFT_9888();
void CombineDFT_9889();
void CombineDFT_9890();
void WEIGHTED_ROUND_ROBIN_Joiner_9879();
void WEIGHTED_ROUND_ROBIN_Splitter_9891();
void CombineDFT_9893();
void CombineDFT_9894();
void CombineDFT_9895();
void CombineDFT_9896();
void CombineDFT_9897();
void CombineDFT_9898();
void CombineDFT_9899();
void CombineDFT_9900();
void CombineDFT_9901();
void CombineDFT_9902();
void CombineDFT_9903();
void WEIGHTED_ROUND_ROBIN_Joiner_9892();
void WEIGHTED_ROUND_ROBIN_Splitter_9904();
void CombineDFT_9906();
void CombineDFT_9907();
void CombineDFT_9908();
void CombineDFT_9909();
void CombineDFT_9910();
void CombineDFT_9911();
void CombineDFT_9912();
void CombineDFT_9913();
void WEIGHTED_ROUND_ROBIN_Joiner_9905();
void WEIGHTED_ROUND_ROBIN_Splitter_9914();
void CombineDFT_9916();
void CombineDFT_9917();
void CombineDFT_9918();
void CombineDFT_9919();
void WEIGHTED_ROUND_ROBIN_Joiner_9915();
void WEIGHTED_ROUND_ROBIN_Splitter_9920();
void CombineDFT_9922();
void CombineDFT_9923();
void WEIGHTED_ROUND_ROBIN_Joiner_9921();
void CombineDFT_9752();
void WEIGHTED_ROUND_ROBIN_Joiner_9755();
void FloatPrinter_9753();

#ifdef __cplusplus
}
#endif
#endif
