#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=128 on the compile command line
#else
#if BUF_SIZEMAX < 128
#error BUF_SIZEMAX too small, it must be at least 128
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float A_re[32];
	float A_im[32];
	int idx;
} FloatSource_11532_t;
void FloatSource(buffer_float_t *chanout);
void FloatSource_11532();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_11833();
void WEIGHTED_ROUND_ROBIN_Splitter_11975();
void Butterfly(buffer_float_t *chanin, buffer_float_t *chanout);
void Butterfly_11977();
void Butterfly_11978();
void WEIGHTED_ROUND_ROBIN_Joiner_11976();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_11834();
void WEIGHTED_ROUND_ROBIN_Splitter_11877();
void Pre_CollapsedDataParallel_1_11836();
void WEIGHTED_ROUND_ROBIN_Splitter_11979();
void Butterfly_11981();
void Butterfly_11982();
void WEIGHTED_ROUND_ROBIN_Joiner_11980();
void Post_CollapsedDataParallel_2_11837();
void WEIGHTED_ROUND_ROBIN_Splitter_11957();
void Pre_CollapsedDataParallel_1_11842();
void WEIGHTED_ROUND_ROBIN_Splitter_11983();
void Butterfly_11985();
void Butterfly_11986();
void WEIGHTED_ROUND_ROBIN_Joiner_11984();
void Post_CollapsedDataParallel_2_11843();
void Pre_CollapsedDataParallel_1_11845();
void WEIGHTED_ROUND_ROBIN_Splitter_11987();
void Butterfly_11989();
void Butterfly_11990();
void WEIGHTED_ROUND_ROBIN_Joiner_11988();
void Post_CollapsedDataParallel_2_11846();
void WEIGHTED_ROUND_ROBIN_Joiner_11958();
void Pre_CollapsedDataParallel_1_11839();
void WEIGHTED_ROUND_ROBIN_Splitter_11991();
void Butterfly_11993();
void Butterfly_11994();
void WEIGHTED_ROUND_ROBIN_Joiner_11992();
void Post_CollapsedDataParallel_2_11840();
void WEIGHTED_ROUND_ROBIN_Splitter_11959();
void Pre_CollapsedDataParallel_1_11848();
void WEIGHTED_ROUND_ROBIN_Splitter_11995();
void Butterfly_11997();
void Butterfly_11998();
void WEIGHTED_ROUND_ROBIN_Joiner_11996();
void Post_CollapsedDataParallel_2_11849();
void Pre_CollapsedDataParallel_1_11851();
void WEIGHTED_ROUND_ROBIN_Splitter_11999();
void Butterfly_12001();
void Butterfly_12002();
void WEIGHTED_ROUND_ROBIN_Joiner_12000();
void Post_CollapsedDataParallel_2_11852();
void WEIGHTED_ROUND_ROBIN_Joiner_11960();
void WEIGHTED_ROUND_ROBIN_Joiner_11961();
void WEIGHTED_ROUND_ROBIN_Splitter_11962();
void WEIGHTED_ROUND_ROBIN_Splitter_11963();
void Pre_CollapsedDataParallel_1_11854();
void WEIGHTED_ROUND_ROBIN_Splitter_12003();
void Butterfly_12005();
void Butterfly_12006();
void WEIGHTED_ROUND_ROBIN_Joiner_12004();
void Post_CollapsedDataParallel_2_11855();
void Pre_CollapsedDataParallel_1_11857();
void WEIGHTED_ROUND_ROBIN_Splitter_12007();
void Butterfly_12009();
void Butterfly_12010();
void WEIGHTED_ROUND_ROBIN_Joiner_12008();
void Post_CollapsedDataParallel_2_11858();
void Pre_CollapsedDataParallel_1_11860();
void WEIGHTED_ROUND_ROBIN_Splitter_12011();
void Butterfly_12013();
void Butterfly_12014();
void WEIGHTED_ROUND_ROBIN_Joiner_12012();
void Post_CollapsedDataParallel_2_11861();
void Pre_CollapsedDataParallel_1_11863();
void WEIGHTED_ROUND_ROBIN_Splitter_12015();
void Butterfly_12017();
void Butterfly_12018();
void WEIGHTED_ROUND_ROBIN_Joiner_12016();
void Post_CollapsedDataParallel_2_11864();
void WEIGHTED_ROUND_ROBIN_Joiner_11964();
void WEIGHTED_ROUND_ROBIN_Splitter_11965();
void Pre_CollapsedDataParallel_1_11866();
void WEIGHTED_ROUND_ROBIN_Splitter_12019();
void Butterfly_12021();
void Butterfly_12022();
void WEIGHTED_ROUND_ROBIN_Joiner_12020();
void Post_CollapsedDataParallel_2_11867();
void Pre_CollapsedDataParallel_1_11869();
void WEIGHTED_ROUND_ROBIN_Splitter_12023();
void Butterfly_12025();
void Butterfly_12026();
void WEIGHTED_ROUND_ROBIN_Joiner_12024();
void Post_CollapsedDataParallel_2_11870();
void Pre_CollapsedDataParallel_1_11872();
void WEIGHTED_ROUND_ROBIN_Splitter_12027();
void Butterfly_12029();
void Butterfly_12030();
void WEIGHTED_ROUND_ROBIN_Joiner_12028();
void Post_CollapsedDataParallel_2_11873();
void Pre_CollapsedDataParallel_1_11875();
void WEIGHTED_ROUND_ROBIN_Splitter_12031();
void Butterfly_12033();
void Butterfly_12034();
void WEIGHTED_ROUND_ROBIN_Joiner_12032();
void Post_CollapsedDataParallel_2_11876();
void WEIGHTED_ROUND_ROBIN_Joiner_11966();
void WEIGHTED_ROUND_ROBIN_Joiner_11967();
void WEIGHTED_ROUND_ROBIN_Splitter_11968();
void WEIGHTED_ROUND_ROBIN_Splitter_11969();
void Butterfly_11597();
void Butterfly_11598();
void Butterfly_11599();
void Butterfly_11600();
void Butterfly_11601();
void Butterfly_11602();
void Butterfly_11603();
void Butterfly_11604();
void WEIGHTED_ROUND_ROBIN_Joiner_11970();
void WEIGHTED_ROUND_ROBIN_Splitter_11971();
void Butterfly_11605();
void Butterfly_11606();
void Butterfly_11607();
void Butterfly_11608();
void Butterfly_11609();
void Butterfly_11610();
void Butterfly_11611();
void Butterfly_11612();
void WEIGHTED_ROUND_ROBIN_Joiner_11972();
void WEIGHTED_ROUND_ROBIN_Joiner_11973();
int BitReverse_11613_bitrev(int inp, int numbits);
void BitReverse(buffer_float_t *chanin, buffer_float_t *chanout);
void BitReverse_11613();
void FloatPrinter(buffer_float_t *chanin);
void FloatPrinter_11614();

#ifdef __cplusplus
}
#endif
#endif
