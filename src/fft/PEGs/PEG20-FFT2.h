#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=2560 on the compile command line
#else
#if BUF_SIZEMAX < 2560
#error BUF_SIZEMAX too small, it must be at least 2560
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float w[2];
} CombineDFT_5971_t;

typedef struct {
	float w[4];
} CombineDFT_5993_t;

typedef struct {
	float w[8];
} CombineDFT_6011_t;

typedef struct {
	float w[16];
} CombineDFT_6021_t;

typedef struct {
	float w[32];
} CombineDFT_6027_t;

typedef struct {
	float w[64];
} CombineDFT_5906_t;
void WEIGHTED_ROUND_ROBIN_Splitter_5927();
void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout);
void FFTTestSource_5929();
void FFTTestSource_5930();
void WEIGHTED_ROUND_ROBIN_Joiner_5928();
void WEIGHTED_ROUND_ROBIN_Splitter_5919();
void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout);
void FFTReorderSimple_5896();
void WEIGHTED_ROUND_ROBIN_Splitter_5931();
void FFTReorderSimple_5933();
void FFTReorderSimple_5934();
void WEIGHTED_ROUND_ROBIN_Joiner_5932();
void WEIGHTED_ROUND_ROBIN_Splitter_5935();
void FFTReorderSimple_5937();
void FFTReorderSimple_5938();
void FFTReorderSimple_5939();
void FFTReorderSimple_5940();
void WEIGHTED_ROUND_ROBIN_Joiner_5936();
void WEIGHTED_ROUND_ROBIN_Splitter_5941();
void FFTReorderSimple_5943();
void FFTReorderSimple_5944();
void FFTReorderSimple_5945();
void FFTReorderSimple_5946();
void FFTReorderSimple_5947();
void FFTReorderSimple_5948();
void FFTReorderSimple_5949();
void FFTReorderSimple_5950();
void WEIGHTED_ROUND_ROBIN_Joiner_5942();
void WEIGHTED_ROUND_ROBIN_Splitter_5951();
void FFTReorderSimple_5953();
void FFTReorderSimple_5954();
void FFTReorderSimple_5955();
void FFTReorderSimple_5956();
void FFTReorderSimple_5957();
void FFTReorderSimple_5958();
void FFTReorderSimple_5959();
void FFTReorderSimple_5960();
void FFTReorderSimple_5961();
void FFTReorderSimple_5962();
void FFTReorderSimple_5963();
void FFTReorderSimple_5964();
void FFTReorderSimple_5965();
void FFTReorderSimple_5966();
void FFTReorderSimple_5967();
void FFTReorderSimple_5968();
void WEIGHTED_ROUND_ROBIN_Joiner_5952();
void WEIGHTED_ROUND_ROBIN_Splitter_5969();
void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout);
void CombineDFT_5971();
void CombineDFT_5972();
void CombineDFT_5973();
void CombineDFT_5974();
void CombineDFT_5975();
void CombineDFT_5976();
void CombineDFT_5977();
void CombineDFT_5978();
void CombineDFT_5979();
void CombineDFT_5980();
void CombineDFT_5981();
void CombineDFT_5982();
void CombineDFT_5983();
void CombineDFT_5984();
void CombineDFT_5985();
void CombineDFT_5986();
void CombineDFT_5987();
void CombineDFT_5988();
void CombineDFT_5989();
void CombineDFT_5990();
void WEIGHTED_ROUND_ROBIN_Joiner_5970();
void WEIGHTED_ROUND_ROBIN_Splitter_5991();
void CombineDFT_5993();
void CombineDFT_5994();
void CombineDFT_5995();
void CombineDFT_5996();
void CombineDFT_5997();
void CombineDFT_5998();
void CombineDFT_5999();
void CombineDFT_6000();
void CombineDFT_6001();
void CombineDFT_6002();
void CombineDFT_6003();
void CombineDFT_6004();
void CombineDFT_6005();
void CombineDFT_6006();
void CombineDFT_6007();
void CombineDFT_6008();
void WEIGHTED_ROUND_ROBIN_Joiner_5992();
void WEIGHTED_ROUND_ROBIN_Splitter_6009();
void CombineDFT_6011();
void CombineDFT_6012();
void CombineDFT_6013();
void CombineDFT_6014();
void CombineDFT_6015();
void CombineDFT_6016();
void CombineDFT_6017();
void CombineDFT_6018();
void WEIGHTED_ROUND_ROBIN_Joiner_6010();
void WEIGHTED_ROUND_ROBIN_Splitter_6019();
void CombineDFT_6021();
void CombineDFT_6022();
void CombineDFT_6023();
void CombineDFT_6024();
void WEIGHTED_ROUND_ROBIN_Joiner_6020();
void WEIGHTED_ROUND_ROBIN_Splitter_6025();
void CombineDFT_6027();
void CombineDFT_6028();
void WEIGHTED_ROUND_ROBIN_Joiner_6026();
void CombineDFT_5906();
void FFTReorderSimple_5907();
void WEIGHTED_ROUND_ROBIN_Splitter_6029();
void FFTReorderSimple_6031();
void FFTReorderSimple_6032();
void WEIGHTED_ROUND_ROBIN_Joiner_6030();
void WEIGHTED_ROUND_ROBIN_Splitter_6033();
void FFTReorderSimple_6035();
void FFTReorderSimple_6036();
void FFTReorderSimple_6037();
void FFTReorderSimple_6038();
void WEIGHTED_ROUND_ROBIN_Joiner_6034();
void WEIGHTED_ROUND_ROBIN_Splitter_6039();
void FFTReorderSimple_6041();
void FFTReorderSimple_6042();
void FFTReorderSimple_6043();
void FFTReorderSimple_6044();
void FFTReorderSimple_6045();
void FFTReorderSimple_6046();
void FFTReorderSimple_6047();
void FFTReorderSimple_6048();
void WEIGHTED_ROUND_ROBIN_Joiner_6040();
void WEIGHTED_ROUND_ROBIN_Splitter_6049();
void FFTReorderSimple_6051();
void FFTReorderSimple_6052();
void FFTReorderSimple_6053();
void FFTReorderSimple_6054();
void FFTReorderSimple_6055();
void FFTReorderSimple_6056();
void FFTReorderSimple_6057();
void FFTReorderSimple_6058();
void FFTReorderSimple_6059();
void FFTReorderSimple_6060();
void FFTReorderSimple_6061();
void FFTReorderSimple_6062();
void FFTReorderSimple_6063();
void FFTReorderSimple_6064();
void FFTReorderSimple_6065();
void FFTReorderSimple_6066();
void WEIGHTED_ROUND_ROBIN_Joiner_6050();
void WEIGHTED_ROUND_ROBIN_Splitter_6067();
void CombineDFT_6069();
void CombineDFT_6070();
void CombineDFT_6071();
void CombineDFT_6072();
void CombineDFT_6073();
void CombineDFT_6074();
void CombineDFT_6075();
void CombineDFT_6076();
void CombineDFT_6077();
void CombineDFT_6078();
void CombineDFT_6079();
void CombineDFT_6080();
void CombineDFT_6081();
void CombineDFT_6082();
void CombineDFT_6083();
void CombineDFT_6084();
void CombineDFT_6085();
void CombineDFT_6086();
void CombineDFT_6087();
void CombineDFT_6088();
void WEIGHTED_ROUND_ROBIN_Joiner_6068();
void WEIGHTED_ROUND_ROBIN_Splitter_6089();
void CombineDFT_6091();
void CombineDFT_6092();
void CombineDFT_6093();
void CombineDFT_6094();
void CombineDFT_6095();
void CombineDFT_6096();
void CombineDFT_6097();
void CombineDFT_6098();
void CombineDFT_6099();
void CombineDFT_6100();
void CombineDFT_6101();
void CombineDFT_6102();
void CombineDFT_6103();
void CombineDFT_6104();
void CombineDFT_6105();
void CombineDFT_6106();
void WEIGHTED_ROUND_ROBIN_Joiner_6090();
void WEIGHTED_ROUND_ROBIN_Splitter_6107();
void CombineDFT_6109();
void CombineDFT_6110();
void CombineDFT_6111();
void CombineDFT_6112();
void CombineDFT_6113();
void CombineDFT_6114();
void CombineDFT_6115();
void CombineDFT_6116();
void WEIGHTED_ROUND_ROBIN_Joiner_6108();
void WEIGHTED_ROUND_ROBIN_Splitter_6117();
void CombineDFT_6119();
void CombineDFT_6120();
void CombineDFT_6121();
void CombineDFT_6122();
void WEIGHTED_ROUND_ROBIN_Joiner_6118();
void WEIGHTED_ROUND_ROBIN_Splitter_6123();
void CombineDFT_6125();
void CombineDFT_6126();
void WEIGHTED_ROUND_ROBIN_Joiner_6124();
void CombineDFT_5917();
void WEIGHTED_ROUND_ROBIN_Joiner_5920();
void FloatPrinter(buffer_float_t *chanin);
void FloatPrinter_5918();

#ifdef __cplusplus
}
#endif
#endif
