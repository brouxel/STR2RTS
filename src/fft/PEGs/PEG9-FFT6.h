#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=1152 on the compile command line
#else
#if BUF_SIZEMAX < 1152
#error BUF_SIZEMAX too small, it must be at least 1152
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	complex_t wn;
} CombineDFT_5054_t;
void FFTTestSource(buffer_complex_t *chanout);
void FFTTestSource_5007();
void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout);
void FFTReorderSimple_5008();
void WEIGHTED_ROUND_ROBIN_Splitter_5021();
void FFTReorderSimple_5023();
void FFTReorderSimple_5024();
void WEIGHTED_ROUND_ROBIN_Joiner_5022();
void WEIGHTED_ROUND_ROBIN_Splitter_5025();
void FFTReorderSimple_5027();
void FFTReorderSimple_5028();
void FFTReorderSimple_5029();
void FFTReorderSimple_5030();
void WEIGHTED_ROUND_ROBIN_Joiner_5026();
void WEIGHTED_ROUND_ROBIN_Splitter_5031();
void FFTReorderSimple_5033();
void FFTReorderSimple_5034();
void FFTReorderSimple_5035();
void FFTReorderSimple_5036();
void FFTReorderSimple_5037();
void FFTReorderSimple_5038();
void FFTReorderSimple_5039();
void FFTReorderSimple_5040();
void WEIGHTED_ROUND_ROBIN_Joiner_5032();
void WEIGHTED_ROUND_ROBIN_Splitter_5041();
void FFTReorderSimple_5043();
void FFTReorderSimple_5044();
void FFTReorderSimple_5045();
void FFTReorderSimple_5046();
void FFTReorderSimple_5047();
void FFTReorderSimple_5048();
void FFTReorderSimple_5049();
void FFTReorderSimple_5050();
void FFTReorderSimple_5051();
void WEIGHTED_ROUND_ROBIN_Joiner_5042();
void WEIGHTED_ROUND_ROBIN_Splitter_5052();
void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout);
void CombineDFT_5054();
void CombineDFT_5055();
void CombineDFT_5056();
void CombineDFT_5057();
void CombineDFT_5058();
void CombineDFT_5059();
void CombineDFT_5060();
void CombineDFT_5061();
void CombineDFT_5062();
void WEIGHTED_ROUND_ROBIN_Joiner_5053();
void WEIGHTED_ROUND_ROBIN_Splitter_5063();
void CombineDFT_5065();
void CombineDFT_5066();
void CombineDFT_5067();
void CombineDFT_5068();
void CombineDFT_5069();
void CombineDFT_5070();
void CombineDFT_5071();
void CombineDFT_5072();
void CombineDFT_5073();
void WEIGHTED_ROUND_ROBIN_Joiner_5064();
void WEIGHTED_ROUND_ROBIN_Splitter_5074();
void CombineDFT_5076();
void CombineDFT_5077();
void CombineDFT_5078();
void CombineDFT_5079();
void CombineDFT_5080();
void CombineDFT_5081();
void CombineDFT_5082();
void CombineDFT_5083();
void WEIGHTED_ROUND_ROBIN_Joiner_5075();
void WEIGHTED_ROUND_ROBIN_Splitter_5084();
void CombineDFT_5086();
void CombineDFT_5087();
void CombineDFT_5088();
void CombineDFT_5089();
void WEIGHTED_ROUND_ROBIN_Joiner_5085();
void WEIGHTED_ROUND_ROBIN_Splitter_5090();
void CombineDFT_5092();
void CombineDFT_5093();
void WEIGHTED_ROUND_ROBIN_Joiner_5091();
void CombineDFT_5018();
void CPrinter(buffer_complex_t *chanin);
void CPrinter_5019();

#ifdef __cplusplus
}
#endif
#endif
