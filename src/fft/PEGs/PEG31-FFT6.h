#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=3968 on the compile command line
#else
#if BUF_SIZEMAX < 3968
#error BUF_SIZEMAX too small, it must be at least 3968
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	complex_t wn;
} CombineDFT_371_t;
void FFTTestSource(buffer_complex_t *chanout);
void FFTTestSource_317();
void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout);
void FFTReorderSimple_318();
void WEIGHTED_ROUND_ROBIN_Splitter_331();
void FFTReorderSimple_333();
void FFTReorderSimple_334();
void WEIGHTED_ROUND_ROBIN_Joiner_332();
void WEIGHTED_ROUND_ROBIN_Splitter_335();
void FFTReorderSimple_337();
void FFTReorderSimple_338();
void FFTReorderSimple_339();
void FFTReorderSimple_340();
void WEIGHTED_ROUND_ROBIN_Joiner_336();
void WEIGHTED_ROUND_ROBIN_Splitter_341();
void FFTReorderSimple_343();
void FFTReorderSimple_344();
void FFTReorderSimple_345();
void FFTReorderSimple_346();
void FFTReorderSimple_347();
void FFTReorderSimple_348();
void FFTReorderSimple_349();
void FFTReorderSimple_350();
void WEIGHTED_ROUND_ROBIN_Joiner_342();
void WEIGHTED_ROUND_ROBIN_Splitter_351();
void FFTReorderSimple_353();
void FFTReorderSimple_354();
void FFTReorderSimple_355();
void FFTReorderSimple_356();
void FFTReorderSimple_357();
void FFTReorderSimple_358();
void FFTReorderSimple_359();
void FFTReorderSimple_360();
void FFTReorderSimple_361();
void FFTReorderSimple_362();
void FFTReorderSimple_363();
void FFTReorderSimple_364();
void FFTReorderSimple_365();
void FFTReorderSimple_366();
void FFTReorderSimple_367();
void FFTReorderSimple_368();
void WEIGHTED_ROUND_ROBIN_Joiner_352();
void WEIGHTED_ROUND_ROBIN_Splitter_369();
void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout);
void CombineDFT_371();
void CombineDFT_372();
void CombineDFT_373();
void CombineDFT_374();
void CombineDFT_375();
void CombineDFT_376();
void CombineDFT_377();
void CombineDFT_378();
void CombineDFT_379();
void CombineDFT_380();
void CombineDFT_381();
void CombineDFT_382();
void CombineDFT_383();
void CombineDFT_384();
void CombineDFT_385();
void CombineDFT_386();
void CombineDFT_387();
void CombineDFT_388();
void CombineDFT_389();
void CombineDFT_390();
void CombineDFT_391();
void CombineDFT_392();
void CombineDFT_393();
void CombineDFT_394();
void CombineDFT_395();
void CombineDFT_396();
void CombineDFT_397();
void CombineDFT_398();
void CombineDFT_399();
void CombineDFT_400();
void CombineDFT_401();
void WEIGHTED_ROUND_ROBIN_Joiner_370();
void WEIGHTED_ROUND_ROBIN_Splitter_402();
void CombineDFT_404();
void CombineDFT_405();
void CombineDFT_406();
void CombineDFT_407();
void CombineDFT_408();
void CombineDFT_409();
void CombineDFT_410();
void CombineDFT_411();
void CombineDFT_412();
void CombineDFT_413();
void CombineDFT_414();
void CombineDFT_415();
void CombineDFT_416();
void CombineDFT_417();
void CombineDFT_418();
void CombineDFT_419();
void WEIGHTED_ROUND_ROBIN_Joiner_403();
void WEIGHTED_ROUND_ROBIN_Splitter_420();
void CombineDFT_422();
void CombineDFT_423();
void CombineDFT_424();
void CombineDFT_425();
void CombineDFT_426();
void CombineDFT_427();
void CombineDFT_428();
void CombineDFT_429();
void WEIGHTED_ROUND_ROBIN_Joiner_421();
void WEIGHTED_ROUND_ROBIN_Splitter_430();
void CombineDFT_432();
void CombineDFT_433();
void CombineDFT_434();
void CombineDFT_435();
void WEIGHTED_ROUND_ROBIN_Joiner_431();
void WEIGHTED_ROUND_ROBIN_Splitter_436();
void CombineDFT_438();
void CombineDFT_439();
void WEIGHTED_ROUND_ROBIN_Joiner_437();
void CombineDFT_328();
void CPrinter(buffer_complex_t *chanin);
void CPrinter_329();

#ifdef __cplusplus
}
#endif
#endif
