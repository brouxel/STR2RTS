#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=160 on the compile command line
#else
#if BUF_SIZEMAX < 160
#error BUF_SIZEMAX too small, it must be at least 160
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif


void WEIGHTED_ROUND_ROBIN_Splitter_5388();
void source(buffer_void_t *chanin, buffer_complex_t *chanout);
void source_5390();
void source_5391();
void WEIGHTED_ROUND_ROBIN_Joiner_5389();
void WEIGHTED_ROUND_ROBIN_Splitter_5231();
void WEIGHTED_ROUND_ROBIN_Splitter_5233();
void WEIGHTED_ROUND_ROBIN_Splitter_5235();
void WEIGHTED_ROUND_ROBIN_Splitter_5237();
void Identity(buffer_complex_t *chanin, buffer_complex_t *chanout);
void Identity_5061();
void Identity_5063();
void WEIGHTED_ROUND_ROBIN_Joiner_5238();
void WEIGHTED_ROUND_ROBIN_Splitter_5239();
void Identity_5067();
void Identity_5069();
void WEIGHTED_ROUND_ROBIN_Joiner_5240();
void WEIGHTED_ROUND_ROBIN_Joiner_5236();
void WEIGHTED_ROUND_ROBIN_Splitter_5241();
void WEIGHTED_ROUND_ROBIN_Splitter_5243();
void Identity_5075();
void Identity_5077();
void WEIGHTED_ROUND_ROBIN_Joiner_5244();
void WEIGHTED_ROUND_ROBIN_Splitter_5245();
void Identity_5081();
void Identity_5083();
void WEIGHTED_ROUND_ROBIN_Joiner_5246();
void WEIGHTED_ROUND_ROBIN_Joiner_5242();
void WEIGHTED_ROUND_ROBIN_Joiner_5234();
void WEIGHTED_ROUND_ROBIN_Splitter_5247();
void WEIGHTED_ROUND_ROBIN_Splitter_5249();
void WEIGHTED_ROUND_ROBIN_Splitter_5251();
void Identity_5091();
void Identity_5093();
void WEIGHTED_ROUND_ROBIN_Joiner_5252();
void WEIGHTED_ROUND_ROBIN_Splitter_5253();
void Identity_5097();
void Identity_5099();
void WEIGHTED_ROUND_ROBIN_Joiner_5254();
void WEIGHTED_ROUND_ROBIN_Joiner_5250();
void WEIGHTED_ROUND_ROBIN_Splitter_5255();
void WEIGHTED_ROUND_ROBIN_Splitter_5257();
void Identity_5105();
void Identity_5107();
void WEIGHTED_ROUND_ROBIN_Joiner_5258();
void WEIGHTED_ROUND_ROBIN_Splitter_5259();
void Identity_5111();
void Identity_5113();
void WEIGHTED_ROUND_ROBIN_Joiner_5260();
void WEIGHTED_ROUND_ROBIN_Joiner_5256();
void WEIGHTED_ROUND_ROBIN_Joiner_5248();
void WEIGHTED_ROUND_ROBIN_Joiner_5232();
void WEIGHTED_ROUND_ROBIN_Splitter_5375();
void WEIGHTED_ROUND_ROBIN_Splitter_5376();
void Pre_CollapsedDataParallel_1(buffer_complex_t *chanin, buffer_complex_t *chanout);
void Pre_CollapsedDataParallel_1_5208();
void butterfly(buffer_complex_t *chanin, buffer_complex_t *chanout);
void butterfly_5115();
void Post_CollapsedDataParallel_2(buffer_complex_t *chanin, buffer_complex_t *chanout);
void Post_CollapsedDataParallel_2_5209();
void Pre_CollapsedDataParallel_1_5211();
void butterfly_5116();
void Post_CollapsedDataParallel_2_5212();
void Pre_CollapsedDataParallel_1_5214();
void butterfly_5117();
void Post_CollapsedDataParallel_2_5215();
void Pre_CollapsedDataParallel_1_5217();
void butterfly_5118();
void Post_CollapsedDataParallel_2_5218();
void WEIGHTED_ROUND_ROBIN_Joiner_5377();
void WEIGHTED_ROUND_ROBIN_Splitter_5378();
void Pre_CollapsedDataParallel_1_5220();
void butterfly_5119();
void Post_CollapsedDataParallel_2_5221();
void Pre_CollapsedDataParallel_1_5223();
void butterfly_5120();
void Post_CollapsedDataParallel_2_5224();
void Pre_CollapsedDataParallel_1_5226();
void butterfly_5121();
void Post_CollapsedDataParallel_2_5227();
void Pre_CollapsedDataParallel_1_5229();
void butterfly_5122();
void Post_CollapsedDataParallel_2_5230();
void WEIGHTED_ROUND_ROBIN_Joiner_5379();
void WEIGHTED_ROUND_ROBIN_Joiner_5380();
void WEIGHTED_ROUND_ROBIN_Splitter_5381();
void WEIGHTED_ROUND_ROBIN_Splitter_5382();
void WEIGHTED_ROUND_ROBIN_Splitter_5265();
void butterfly_5124();
void butterfly_5125();
void WEIGHTED_ROUND_ROBIN_Joiner_5266();
void WEIGHTED_ROUND_ROBIN_Splitter_5267();
void butterfly_5126();
void butterfly_5127();
void WEIGHTED_ROUND_ROBIN_Joiner_5268();
void WEIGHTED_ROUND_ROBIN_Joiner_5383();
void WEIGHTED_ROUND_ROBIN_Splitter_5384();
void WEIGHTED_ROUND_ROBIN_Splitter_5269();
void butterfly_5128();
void butterfly_5129();
void WEIGHTED_ROUND_ROBIN_Joiner_5270();
void WEIGHTED_ROUND_ROBIN_Splitter_5271();
void butterfly_5130();
void butterfly_5131();
void WEIGHTED_ROUND_ROBIN_Joiner_5272();
void WEIGHTED_ROUND_ROBIN_Joiner_5385();
void WEIGHTED_ROUND_ROBIN_Joiner_5386();
void WEIGHTED_ROUND_ROBIN_Splitter_5273();
void WEIGHTED_ROUND_ROBIN_Splitter_5275();
void butterfly_5133();
void butterfly_5134();
void butterfly_5135();
void butterfly_5136();
void WEIGHTED_ROUND_ROBIN_Joiner_5276();
void WEIGHTED_ROUND_ROBIN_Splitter_5277();
void butterfly_5137();
void butterfly_5138();
void butterfly_5139();
void butterfly_5140();
void WEIGHTED_ROUND_ROBIN_Joiner_5278();
void WEIGHTED_ROUND_ROBIN_Joiner_5274();
void WEIGHTED_ROUND_ROBIN_Splitter_5392();
void magnitude(buffer_complex_t *chanin, buffer_float_t *chanout);
void magnitude_5394();
void magnitude_5395();
void magnitude_5396();
void magnitude_5397();
void magnitude_5398();
void magnitude_5399();
void magnitude_5400();
void magnitude_5401();
void magnitude_5402();
void magnitude_5403();
void WEIGHTED_ROUND_ROBIN_Joiner_5393();
void sink(buffer_float_t *chanin);
void sink_5142();

#ifdef __cplusplus
}
#endif
#endif
