#include "PEG7-FFT2.h"

buffer_float_t SplitJoin69_CombineDFT_Fiss_11339_11360_join[7];
buffer_float_t SplitJoin65_FFTReorderSimple_Fiss_11337_11358_split[7];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11264WEIGHTED_ROUND_ROBIN_Splitter_11269;
buffer_float_t SplitJoin69_CombineDFT_Fiss_11339_11360_split[7];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11250WEIGHTED_ROUND_ROBIN_Splitter_11255;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11195WEIGHTED_ROUND_ROBIN_Splitter_11198;
buffer_float_t SplitJoin12_CombineDFT_Fiss_11330_11351_split[7];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11260WEIGHTED_ROUND_ROBIN_Splitter_11263;
buffer_float_t SplitJoin14_CombineDFT_Fiss_11331_11352_join[7];
buffer_float_t FFTReorderSimple_11159WEIGHTED_ROUND_ROBIN_Splitter_11194;
buffer_float_t SplitJoin12_CombineDFT_Fiss_11330_11351_join[7];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11288WEIGHTED_ROUND_ROBIN_Splitter_11296;
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_11328_11349_split[7];
buffer_float_t SplitJoin0_FFTTestSource_Fiss_11324_11345_split[2];
buffer_float_t SplitJoin61_FFTReorderSimple_Fiss_11335_11356_join[2];
buffer_float_t SplitJoin77_CombineDFT_Fiss_11343_11364_split[2];
buffer_float_t SplitJoin77_CombineDFT_Fiss_11343_11364_join[2];
buffer_float_t SplitJoin75_CombineDFT_Fiss_11342_11363_split[4];
buffer_float_t SplitJoin63_FFTReorderSimple_Fiss_11336_11357_join[4];
buffer_float_t SplitJoin67_FFTReorderSimple_Fiss_11338_11359_split[7];
buffer_float_t FFTReorderSimple_11170WEIGHTED_ROUND_ROBIN_Splitter_11259;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11199WEIGHTED_ROUND_ROBIN_Splitter_11204;
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_11328_11349_join[7];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11256CombineDFT_11169;
buffer_float_t SplitJoin18_CombineDFT_Fiss_11333_11354_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11297WEIGHTED_ROUND_ROBIN_Splitter_11305;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11306WEIGHTED_ROUND_ROBIN_Splitter_11314;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11223WEIGHTED_ROUND_ROBIN_Splitter_11231;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11270WEIGHTED_ROUND_ROBIN_Splitter_11278;
buffer_float_t SplitJoin0_FFTTestSource_Fiss_11324_11345_join[2];
buffer_float_t SplitJoin20_CombineDFT_Fiss_11334_11355_split[2];
buffer_float_t SplitJoin71_CombineDFT_Fiss_11340_11361_join[7];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11321CombineDFT_11180;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11183FloatPrinter_11181;
buffer_float_t SplitJoin61_FFTReorderSimple_Fiss_11335_11356_split[2];
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_11326_11347_split[2];
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_11327_11348_join[4];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11148_11184_11325_11346_split[2];
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_11326_11347_join[2];
buffer_float_t SplitJoin18_CombineDFT_Fiss_11333_11354_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11279WEIGHTED_ROUND_ROBIN_Splitter_11287;
buffer_float_t SplitJoin16_CombineDFT_Fiss_11332_11353_split[7];
buffer_float_t SplitJoin73_CombineDFT_Fiss_11341_11362_split[7];
buffer_float_t SplitJoin73_CombineDFT_Fiss_11341_11362_join[7];
buffer_float_t SplitJoin20_CombineDFT_Fiss_11334_11355_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11214WEIGHTED_ROUND_ROBIN_Splitter_11222;
buffer_float_t SplitJoin65_FFTReorderSimple_Fiss_11337_11358_join[7];
buffer_float_t SplitJoin16_CombineDFT_Fiss_11332_11353_join[7];
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_11329_11350_split[7];
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_11329_11350_join[7];
buffer_float_t SplitJoin14_CombineDFT_Fiss_11331_11352_split[7];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11148_11184_11325_11346_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11205WEIGHTED_ROUND_ROBIN_Splitter_11213;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11315WEIGHTED_ROUND_ROBIN_Splitter_11320;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11191WEIGHTED_ROUND_ROBIN_Splitter_11182;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11232WEIGHTED_ROUND_ROBIN_Splitter_11240;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11241WEIGHTED_ROUND_ROBIN_Splitter_11249;
buffer_float_t SplitJoin71_CombineDFT_Fiss_11340_11361_split[7];
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_11327_11348_split[4];
buffer_float_t SplitJoin67_FFTReorderSimple_Fiss_11338_11359_join[7];
buffer_float_t SplitJoin75_CombineDFT_Fiss_11342_11363_join[4];
buffer_float_t SplitJoin63_FFTReorderSimple_Fiss_11336_11357_split[4];


CombineDFT_11224_t CombineDFT_11224_s;
CombineDFT_11224_t CombineDFT_11225_s;
CombineDFT_11224_t CombineDFT_11226_s;
CombineDFT_11224_t CombineDFT_11227_s;
CombineDFT_11224_t CombineDFT_11228_s;
CombineDFT_11224_t CombineDFT_11229_s;
CombineDFT_11224_t CombineDFT_11230_s;
CombineDFT_11233_t CombineDFT_11233_s;
CombineDFT_11233_t CombineDFT_11234_s;
CombineDFT_11233_t CombineDFT_11235_s;
CombineDFT_11233_t CombineDFT_11236_s;
CombineDFT_11233_t CombineDFT_11237_s;
CombineDFT_11233_t CombineDFT_11238_s;
CombineDFT_11233_t CombineDFT_11239_s;
CombineDFT_11242_t CombineDFT_11242_s;
CombineDFT_11242_t CombineDFT_11243_s;
CombineDFT_11242_t CombineDFT_11244_s;
CombineDFT_11242_t CombineDFT_11245_s;
CombineDFT_11242_t CombineDFT_11246_s;
CombineDFT_11242_t CombineDFT_11247_s;
CombineDFT_11242_t CombineDFT_11248_s;
CombineDFT_11251_t CombineDFT_11251_s;
CombineDFT_11251_t CombineDFT_11252_s;
CombineDFT_11251_t CombineDFT_11253_s;
CombineDFT_11251_t CombineDFT_11254_s;
CombineDFT_11257_t CombineDFT_11257_s;
CombineDFT_11257_t CombineDFT_11258_s;
CombineDFT_11169_t CombineDFT_11169_s;
CombineDFT_11224_t CombineDFT_11289_s;
CombineDFT_11224_t CombineDFT_11290_s;
CombineDFT_11224_t CombineDFT_11291_s;
CombineDFT_11224_t CombineDFT_11292_s;
CombineDFT_11224_t CombineDFT_11293_s;
CombineDFT_11224_t CombineDFT_11294_s;
CombineDFT_11224_t CombineDFT_11295_s;
CombineDFT_11233_t CombineDFT_11298_s;
CombineDFT_11233_t CombineDFT_11299_s;
CombineDFT_11233_t CombineDFT_11300_s;
CombineDFT_11233_t CombineDFT_11301_s;
CombineDFT_11233_t CombineDFT_11302_s;
CombineDFT_11233_t CombineDFT_11303_s;
CombineDFT_11233_t CombineDFT_11304_s;
CombineDFT_11242_t CombineDFT_11307_s;
CombineDFT_11242_t CombineDFT_11308_s;
CombineDFT_11242_t CombineDFT_11309_s;
CombineDFT_11242_t CombineDFT_11310_s;
CombineDFT_11242_t CombineDFT_11311_s;
CombineDFT_11242_t CombineDFT_11312_s;
CombineDFT_11242_t CombineDFT_11313_s;
CombineDFT_11251_t CombineDFT_11316_s;
CombineDFT_11251_t CombineDFT_11317_s;
CombineDFT_11251_t CombineDFT_11318_s;
CombineDFT_11251_t CombineDFT_11319_s;
CombineDFT_11257_t CombineDFT_11322_s;
CombineDFT_11257_t CombineDFT_11323_s;
CombineDFT_11169_t CombineDFT_11180_s;

void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), 0.0) ; 
		push_float(&(*chanout), 0.0) ; 
		push_float(&(*chanout), 1.0) ; 
		push_float(&(*chanout), 0.0) ; 
		FOR(int, i, 0,  < , 124, i++) {
			push_float(&(*chanout), 0.0) ; 
		}
		ENDFOR
	}


void FFTTestSource_11192() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_11324_11345_split[0]), &(SplitJoin0_FFTTestSource_Fiss_11324_11345_join[0]));
	ENDFOR
}

void FFTTestSource_11193() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_11324_11345_split[1]), &(SplitJoin0_FFTTestSource_Fiss_11324_11345_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11190() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_11191() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11191WEIGHTED_ROUND_ROBIN_Splitter_11182, pop_float(&SplitJoin0_FFTTestSource_Fiss_11324_11345_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11191WEIGHTED_ROUND_ROBIN_Splitter_11182, pop_float(&SplitJoin0_FFTTestSource_Fiss_11324_11345_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, i, 0,  < , 128, i = (i + 4)) {
			push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 128, i = (i + 4)) {
			push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_float(&(*chanin)) ; 
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void FFTReorderSimple_11159() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11148_11184_11325_11346_split[0]), &(FFTReorderSimple_11159WEIGHTED_ROUND_ROBIN_Splitter_11194));
	ENDFOR
}

void FFTReorderSimple_11196() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_11326_11347_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_11326_11347_join[0]));
	ENDFOR
}

void FFTReorderSimple_11197() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_11326_11347_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_11326_11347_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11194() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_11326_11347_split[0], pop_float(&FFTReorderSimple_11159WEIGHTED_ROUND_ROBIN_Splitter_11194));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_11326_11347_split[1], pop_float(&FFTReorderSimple_11159WEIGHTED_ROUND_ROBIN_Splitter_11194));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11195() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11195WEIGHTED_ROUND_ROBIN_Splitter_11198, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_11326_11347_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11195WEIGHTED_ROUND_ROBIN_Splitter_11198, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_11326_11347_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_11200() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_11327_11348_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_11327_11348_join[0]));
	ENDFOR
}

void FFTReorderSimple_11201() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_11327_11348_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_11327_11348_join[1]));
	ENDFOR
}

void FFTReorderSimple_11202() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_11327_11348_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_11327_11348_join[2]));
	ENDFOR
}

void FFTReorderSimple_11203() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_11327_11348_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_11327_11348_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11198() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin6_FFTReorderSimple_Fiss_11327_11348_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11195WEIGHTED_ROUND_ROBIN_Splitter_11198));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11199() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11199WEIGHTED_ROUND_ROBIN_Splitter_11204, pop_float(&SplitJoin6_FFTReorderSimple_Fiss_11327_11348_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_11206() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_11328_11349_split[0]), &(SplitJoin8_FFTReorderSimple_Fiss_11328_11349_join[0]));
	ENDFOR
}

void FFTReorderSimple_11207() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_11328_11349_split[1]), &(SplitJoin8_FFTReorderSimple_Fiss_11328_11349_join[1]));
	ENDFOR
}

void FFTReorderSimple_11208() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_11328_11349_split[2]), &(SplitJoin8_FFTReorderSimple_Fiss_11328_11349_join[2]));
	ENDFOR
}

void FFTReorderSimple_11209() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_11328_11349_split[3]), &(SplitJoin8_FFTReorderSimple_Fiss_11328_11349_join[3]));
	ENDFOR
}

void FFTReorderSimple_11210() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_11328_11349_split[4]), &(SplitJoin8_FFTReorderSimple_Fiss_11328_11349_join[4]));
	ENDFOR
}

void FFTReorderSimple_11211() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_11328_11349_split[5]), &(SplitJoin8_FFTReorderSimple_Fiss_11328_11349_join[5]));
	ENDFOR
}

void FFTReorderSimple_11212() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_11328_11349_split[6]), &(SplitJoin8_FFTReorderSimple_Fiss_11328_11349_join[6]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11204() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin8_FFTReorderSimple_Fiss_11328_11349_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11199WEIGHTED_ROUND_ROBIN_Splitter_11204));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11205() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11205WEIGHTED_ROUND_ROBIN_Splitter_11213, pop_float(&SplitJoin8_FFTReorderSimple_Fiss_11328_11349_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_11215() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_11329_11350_split[0]), &(SplitJoin10_FFTReorderSimple_Fiss_11329_11350_join[0]));
	ENDFOR
}

void FFTReorderSimple_11216() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_11329_11350_split[1]), &(SplitJoin10_FFTReorderSimple_Fiss_11329_11350_join[1]));
	ENDFOR
}

void FFTReorderSimple_11217() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_11329_11350_split[2]), &(SplitJoin10_FFTReorderSimple_Fiss_11329_11350_join[2]));
	ENDFOR
}

void FFTReorderSimple_11218() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_11329_11350_split[3]), &(SplitJoin10_FFTReorderSimple_Fiss_11329_11350_join[3]));
	ENDFOR
}

void FFTReorderSimple_11219() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_11329_11350_split[4]), &(SplitJoin10_FFTReorderSimple_Fiss_11329_11350_join[4]));
	ENDFOR
}

void FFTReorderSimple_11220() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_11329_11350_split[5]), &(SplitJoin10_FFTReorderSimple_Fiss_11329_11350_join[5]));
	ENDFOR
}

void FFTReorderSimple_11221() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_11329_11350_split[6]), &(SplitJoin10_FFTReorderSimple_Fiss_11329_11350_join[6]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11213() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_FFTReorderSimple_Fiss_11329_11350_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11205WEIGHTED_ROUND_ROBIN_Splitter_11213));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11214() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11214WEIGHTED_ROUND_ROBIN_Splitter_11222, pop_float(&SplitJoin10_FFTReorderSimple_Fiss_11329_11350_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&(*chanin), i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&(*chanin), i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&(*chanin), (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&(*chanin), (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_11224_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_11224_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&(*chanin)) ; 
			push_float(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_11224() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_11330_11351_split[0]), &(SplitJoin12_CombineDFT_Fiss_11330_11351_join[0]));
	ENDFOR
}

void CombineDFT_11225() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_11330_11351_split[1]), &(SplitJoin12_CombineDFT_Fiss_11330_11351_join[1]));
	ENDFOR
}

void CombineDFT_11226() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_11330_11351_split[2]), &(SplitJoin12_CombineDFT_Fiss_11330_11351_join[2]));
	ENDFOR
}

void CombineDFT_11227() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_11330_11351_split[3]), &(SplitJoin12_CombineDFT_Fiss_11330_11351_join[3]));
	ENDFOR
}

void CombineDFT_11228() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_11330_11351_split[4]), &(SplitJoin12_CombineDFT_Fiss_11330_11351_join[4]));
	ENDFOR
}

void CombineDFT_11229() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_11330_11351_split[5]), &(SplitJoin12_CombineDFT_Fiss_11330_11351_join[5]));
	ENDFOR
}

void CombineDFT_11230() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_11330_11351_split[6]), &(SplitJoin12_CombineDFT_Fiss_11330_11351_join[6]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11222() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin12_CombineDFT_Fiss_11330_11351_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11214WEIGHTED_ROUND_ROBIN_Splitter_11222));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11223() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11223WEIGHTED_ROUND_ROBIN_Splitter_11231, pop_float(&SplitJoin12_CombineDFT_Fiss_11330_11351_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_11233() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_11331_11352_split[0]), &(SplitJoin14_CombineDFT_Fiss_11331_11352_join[0]));
	ENDFOR
}

void CombineDFT_11234() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_11331_11352_split[1]), &(SplitJoin14_CombineDFT_Fiss_11331_11352_join[1]));
	ENDFOR
}

void CombineDFT_11235() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_11331_11352_split[2]), &(SplitJoin14_CombineDFT_Fiss_11331_11352_join[2]));
	ENDFOR
}

void CombineDFT_11236() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_11331_11352_split[3]), &(SplitJoin14_CombineDFT_Fiss_11331_11352_join[3]));
	ENDFOR
}

void CombineDFT_11237() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_11331_11352_split[4]), &(SplitJoin14_CombineDFT_Fiss_11331_11352_join[4]));
	ENDFOR
}

void CombineDFT_11238() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_11331_11352_split[5]), &(SplitJoin14_CombineDFT_Fiss_11331_11352_join[5]));
	ENDFOR
}

void CombineDFT_11239() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_11331_11352_split[6]), &(SplitJoin14_CombineDFT_Fiss_11331_11352_join[6]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11231() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin14_CombineDFT_Fiss_11331_11352_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11223WEIGHTED_ROUND_ROBIN_Splitter_11231));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11232() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11232WEIGHTED_ROUND_ROBIN_Splitter_11240, pop_float(&SplitJoin14_CombineDFT_Fiss_11331_11352_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_11242() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_11332_11353_split[0]), &(SplitJoin16_CombineDFT_Fiss_11332_11353_join[0]));
	ENDFOR
}

void CombineDFT_11243() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_11332_11353_split[1]), &(SplitJoin16_CombineDFT_Fiss_11332_11353_join[1]));
	ENDFOR
}

void CombineDFT_11244() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_11332_11353_split[2]), &(SplitJoin16_CombineDFT_Fiss_11332_11353_join[2]));
	ENDFOR
}

void CombineDFT_11245() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_11332_11353_split[3]), &(SplitJoin16_CombineDFT_Fiss_11332_11353_join[3]));
	ENDFOR
}

void CombineDFT_11246() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_11332_11353_split[4]), &(SplitJoin16_CombineDFT_Fiss_11332_11353_join[4]));
	ENDFOR
}

void CombineDFT_11247() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_11332_11353_split[5]), &(SplitJoin16_CombineDFT_Fiss_11332_11353_join[5]));
	ENDFOR
}

void CombineDFT_11248() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_11332_11353_split[6]), &(SplitJoin16_CombineDFT_Fiss_11332_11353_join[6]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11240() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin16_CombineDFT_Fiss_11332_11353_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11232WEIGHTED_ROUND_ROBIN_Splitter_11240));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11241() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11241WEIGHTED_ROUND_ROBIN_Splitter_11249, pop_float(&SplitJoin16_CombineDFT_Fiss_11332_11353_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_11251() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_11333_11354_split[0]), &(SplitJoin18_CombineDFT_Fiss_11333_11354_join[0]));
	ENDFOR
}

void CombineDFT_11252() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_11333_11354_split[1]), &(SplitJoin18_CombineDFT_Fiss_11333_11354_join[1]));
	ENDFOR
}

void CombineDFT_11253() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_11333_11354_split[2]), &(SplitJoin18_CombineDFT_Fiss_11333_11354_join[2]));
	ENDFOR
}

void CombineDFT_11254() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_11333_11354_split[3]), &(SplitJoin18_CombineDFT_Fiss_11333_11354_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11249() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin18_CombineDFT_Fiss_11333_11354_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11241WEIGHTED_ROUND_ROBIN_Splitter_11249));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11250() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11250WEIGHTED_ROUND_ROBIN_Splitter_11255, pop_float(&SplitJoin18_CombineDFT_Fiss_11333_11354_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_11257() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin20_CombineDFT_Fiss_11334_11355_split[0]), &(SplitJoin20_CombineDFT_Fiss_11334_11355_join[0]));
	ENDFOR
}

void CombineDFT_11258() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin20_CombineDFT_Fiss_11334_11355_split[1]), &(SplitJoin20_CombineDFT_Fiss_11334_11355_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11255() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_11334_11355_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11250WEIGHTED_ROUND_ROBIN_Splitter_11255));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_11334_11355_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11250WEIGHTED_ROUND_ROBIN_Splitter_11255));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11256() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11256CombineDFT_11169, pop_float(&SplitJoin20_CombineDFT_Fiss_11334_11355_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11256CombineDFT_11169, pop_float(&SplitJoin20_CombineDFT_Fiss_11334_11355_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_11169() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_11256CombineDFT_11169), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11148_11184_11325_11346_join[0]));
	ENDFOR
}

void FFTReorderSimple_11170() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11148_11184_11325_11346_split[1]), &(FFTReorderSimple_11170WEIGHTED_ROUND_ROBIN_Splitter_11259));
	ENDFOR
}

void FFTReorderSimple_11261() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin61_FFTReorderSimple_Fiss_11335_11356_split[0]), &(SplitJoin61_FFTReorderSimple_Fiss_11335_11356_join[0]));
	ENDFOR
}

void FFTReorderSimple_11262() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin61_FFTReorderSimple_Fiss_11335_11356_split[1]), &(SplitJoin61_FFTReorderSimple_Fiss_11335_11356_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11259() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin61_FFTReorderSimple_Fiss_11335_11356_split[0], pop_float(&FFTReorderSimple_11170WEIGHTED_ROUND_ROBIN_Splitter_11259));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin61_FFTReorderSimple_Fiss_11335_11356_split[1], pop_float(&FFTReorderSimple_11170WEIGHTED_ROUND_ROBIN_Splitter_11259));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11260() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11260WEIGHTED_ROUND_ROBIN_Splitter_11263, pop_float(&SplitJoin61_FFTReorderSimple_Fiss_11335_11356_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11260WEIGHTED_ROUND_ROBIN_Splitter_11263, pop_float(&SplitJoin61_FFTReorderSimple_Fiss_11335_11356_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_11265() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin63_FFTReorderSimple_Fiss_11336_11357_split[0]), &(SplitJoin63_FFTReorderSimple_Fiss_11336_11357_join[0]));
	ENDFOR
}

void FFTReorderSimple_11266() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin63_FFTReorderSimple_Fiss_11336_11357_split[1]), &(SplitJoin63_FFTReorderSimple_Fiss_11336_11357_join[1]));
	ENDFOR
}

void FFTReorderSimple_11267() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin63_FFTReorderSimple_Fiss_11336_11357_split[2]), &(SplitJoin63_FFTReorderSimple_Fiss_11336_11357_join[2]));
	ENDFOR
}

void FFTReorderSimple_11268() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin63_FFTReorderSimple_Fiss_11336_11357_split[3]), &(SplitJoin63_FFTReorderSimple_Fiss_11336_11357_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11263() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin63_FFTReorderSimple_Fiss_11336_11357_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11260WEIGHTED_ROUND_ROBIN_Splitter_11263));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11264() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11264WEIGHTED_ROUND_ROBIN_Splitter_11269, pop_float(&SplitJoin63_FFTReorderSimple_Fiss_11336_11357_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_11271() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin65_FFTReorderSimple_Fiss_11337_11358_split[0]), &(SplitJoin65_FFTReorderSimple_Fiss_11337_11358_join[0]));
	ENDFOR
}

void FFTReorderSimple_11272() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin65_FFTReorderSimple_Fiss_11337_11358_split[1]), &(SplitJoin65_FFTReorderSimple_Fiss_11337_11358_join[1]));
	ENDFOR
}

void FFTReorderSimple_11273() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin65_FFTReorderSimple_Fiss_11337_11358_split[2]), &(SplitJoin65_FFTReorderSimple_Fiss_11337_11358_join[2]));
	ENDFOR
}

void FFTReorderSimple_11274() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin65_FFTReorderSimple_Fiss_11337_11358_split[3]), &(SplitJoin65_FFTReorderSimple_Fiss_11337_11358_join[3]));
	ENDFOR
}

void FFTReorderSimple_11275() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin65_FFTReorderSimple_Fiss_11337_11358_split[4]), &(SplitJoin65_FFTReorderSimple_Fiss_11337_11358_join[4]));
	ENDFOR
}

void FFTReorderSimple_11276() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin65_FFTReorderSimple_Fiss_11337_11358_split[5]), &(SplitJoin65_FFTReorderSimple_Fiss_11337_11358_join[5]));
	ENDFOR
}

void FFTReorderSimple_11277() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin65_FFTReorderSimple_Fiss_11337_11358_split[6]), &(SplitJoin65_FFTReorderSimple_Fiss_11337_11358_join[6]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11269() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin65_FFTReorderSimple_Fiss_11337_11358_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11264WEIGHTED_ROUND_ROBIN_Splitter_11269));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11270() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11270WEIGHTED_ROUND_ROBIN_Splitter_11278, pop_float(&SplitJoin65_FFTReorderSimple_Fiss_11337_11358_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_11280() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin67_FFTReorderSimple_Fiss_11338_11359_split[0]), &(SplitJoin67_FFTReorderSimple_Fiss_11338_11359_join[0]));
	ENDFOR
}

void FFTReorderSimple_11281() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin67_FFTReorderSimple_Fiss_11338_11359_split[1]), &(SplitJoin67_FFTReorderSimple_Fiss_11338_11359_join[1]));
	ENDFOR
}

void FFTReorderSimple_11282() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin67_FFTReorderSimple_Fiss_11338_11359_split[2]), &(SplitJoin67_FFTReorderSimple_Fiss_11338_11359_join[2]));
	ENDFOR
}

void FFTReorderSimple_11283() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin67_FFTReorderSimple_Fiss_11338_11359_split[3]), &(SplitJoin67_FFTReorderSimple_Fiss_11338_11359_join[3]));
	ENDFOR
}

void FFTReorderSimple_11284() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin67_FFTReorderSimple_Fiss_11338_11359_split[4]), &(SplitJoin67_FFTReorderSimple_Fiss_11338_11359_join[4]));
	ENDFOR
}

void FFTReorderSimple_11285() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin67_FFTReorderSimple_Fiss_11338_11359_split[5]), &(SplitJoin67_FFTReorderSimple_Fiss_11338_11359_join[5]));
	ENDFOR
}

void FFTReorderSimple_11286() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin67_FFTReorderSimple_Fiss_11338_11359_split[6]), &(SplitJoin67_FFTReorderSimple_Fiss_11338_11359_join[6]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11278() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin67_FFTReorderSimple_Fiss_11338_11359_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11270WEIGHTED_ROUND_ROBIN_Splitter_11278));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11279() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11279WEIGHTED_ROUND_ROBIN_Splitter_11287, pop_float(&SplitJoin67_FFTReorderSimple_Fiss_11338_11359_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_11289() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin69_CombineDFT_Fiss_11339_11360_split[0]), &(SplitJoin69_CombineDFT_Fiss_11339_11360_join[0]));
	ENDFOR
}

void CombineDFT_11290() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin69_CombineDFT_Fiss_11339_11360_split[1]), &(SplitJoin69_CombineDFT_Fiss_11339_11360_join[1]));
	ENDFOR
}

void CombineDFT_11291() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin69_CombineDFT_Fiss_11339_11360_split[2]), &(SplitJoin69_CombineDFT_Fiss_11339_11360_join[2]));
	ENDFOR
}

void CombineDFT_11292() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin69_CombineDFT_Fiss_11339_11360_split[3]), &(SplitJoin69_CombineDFT_Fiss_11339_11360_join[3]));
	ENDFOR
}

void CombineDFT_11293() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin69_CombineDFT_Fiss_11339_11360_split[4]), &(SplitJoin69_CombineDFT_Fiss_11339_11360_join[4]));
	ENDFOR
}

void CombineDFT_11294() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin69_CombineDFT_Fiss_11339_11360_split[5]), &(SplitJoin69_CombineDFT_Fiss_11339_11360_join[5]));
	ENDFOR
}

void CombineDFT_11295() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin69_CombineDFT_Fiss_11339_11360_split[6]), &(SplitJoin69_CombineDFT_Fiss_11339_11360_join[6]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11287() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin69_CombineDFT_Fiss_11339_11360_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11279WEIGHTED_ROUND_ROBIN_Splitter_11287));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11288() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11288WEIGHTED_ROUND_ROBIN_Splitter_11296, pop_float(&SplitJoin69_CombineDFT_Fiss_11339_11360_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_11298() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin71_CombineDFT_Fiss_11340_11361_split[0]), &(SplitJoin71_CombineDFT_Fiss_11340_11361_join[0]));
	ENDFOR
}

void CombineDFT_11299() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin71_CombineDFT_Fiss_11340_11361_split[1]), &(SplitJoin71_CombineDFT_Fiss_11340_11361_join[1]));
	ENDFOR
}

void CombineDFT_11300() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin71_CombineDFT_Fiss_11340_11361_split[2]), &(SplitJoin71_CombineDFT_Fiss_11340_11361_join[2]));
	ENDFOR
}

void CombineDFT_11301() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin71_CombineDFT_Fiss_11340_11361_split[3]), &(SplitJoin71_CombineDFT_Fiss_11340_11361_join[3]));
	ENDFOR
}

void CombineDFT_11302() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin71_CombineDFT_Fiss_11340_11361_split[4]), &(SplitJoin71_CombineDFT_Fiss_11340_11361_join[4]));
	ENDFOR
}

void CombineDFT_11303() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin71_CombineDFT_Fiss_11340_11361_split[5]), &(SplitJoin71_CombineDFT_Fiss_11340_11361_join[5]));
	ENDFOR
}

void CombineDFT_11304() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin71_CombineDFT_Fiss_11340_11361_split[6]), &(SplitJoin71_CombineDFT_Fiss_11340_11361_join[6]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11296() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin71_CombineDFT_Fiss_11340_11361_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11288WEIGHTED_ROUND_ROBIN_Splitter_11296));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11297() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11297WEIGHTED_ROUND_ROBIN_Splitter_11305, pop_float(&SplitJoin71_CombineDFT_Fiss_11340_11361_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_11307() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin73_CombineDFT_Fiss_11341_11362_split[0]), &(SplitJoin73_CombineDFT_Fiss_11341_11362_join[0]));
	ENDFOR
}

void CombineDFT_11308() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin73_CombineDFT_Fiss_11341_11362_split[1]), &(SplitJoin73_CombineDFT_Fiss_11341_11362_join[1]));
	ENDFOR
}

void CombineDFT_11309() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin73_CombineDFT_Fiss_11341_11362_split[2]), &(SplitJoin73_CombineDFT_Fiss_11341_11362_join[2]));
	ENDFOR
}

void CombineDFT_11310() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin73_CombineDFT_Fiss_11341_11362_split[3]), &(SplitJoin73_CombineDFT_Fiss_11341_11362_join[3]));
	ENDFOR
}

void CombineDFT_11311() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin73_CombineDFT_Fiss_11341_11362_split[4]), &(SplitJoin73_CombineDFT_Fiss_11341_11362_join[4]));
	ENDFOR
}

void CombineDFT_11312() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin73_CombineDFT_Fiss_11341_11362_split[5]), &(SplitJoin73_CombineDFT_Fiss_11341_11362_join[5]));
	ENDFOR
}

void CombineDFT_11313() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin73_CombineDFT_Fiss_11341_11362_split[6]), &(SplitJoin73_CombineDFT_Fiss_11341_11362_join[6]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11305() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin73_CombineDFT_Fiss_11341_11362_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11297WEIGHTED_ROUND_ROBIN_Splitter_11305));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11306() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11306WEIGHTED_ROUND_ROBIN_Splitter_11314, pop_float(&SplitJoin73_CombineDFT_Fiss_11341_11362_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_11316() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin75_CombineDFT_Fiss_11342_11363_split[0]), &(SplitJoin75_CombineDFT_Fiss_11342_11363_join[0]));
	ENDFOR
}

void CombineDFT_11317() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin75_CombineDFT_Fiss_11342_11363_split[1]), &(SplitJoin75_CombineDFT_Fiss_11342_11363_join[1]));
	ENDFOR
}

void CombineDFT_11318() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin75_CombineDFT_Fiss_11342_11363_split[2]), &(SplitJoin75_CombineDFT_Fiss_11342_11363_join[2]));
	ENDFOR
}

void CombineDFT_11319() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin75_CombineDFT_Fiss_11342_11363_split[3]), &(SplitJoin75_CombineDFT_Fiss_11342_11363_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11314() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin75_CombineDFT_Fiss_11342_11363_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11306WEIGHTED_ROUND_ROBIN_Splitter_11314));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11315() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11315WEIGHTED_ROUND_ROBIN_Splitter_11320, pop_float(&SplitJoin75_CombineDFT_Fiss_11342_11363_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_11322() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin77_CombineDFT_Fiss_11343_11364_split[0]), &(SplitJoin77_CombineDFT_Fiss_11343_11364_join[0]));
	ENDFOR
}

void CombineDFT_11323() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin77_CombineDFT_Fiss_11343_11364_split[1]), &(SplitJoin77_CombineDFT_Fiss_11343_11364_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11320() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin77_CombineDFT_Fiss_11343_11364_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11315WEIGHTED_ROUND_ROBIN_Splitter_11320));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin77_CombineDFT_Fiss_11343_11364_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11315WEIGHTED_ROUND_ROBIN_Splitter_11320));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11321() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11321CombineDFT_11180, pop_float(&SplitJoin77_CombineDFT_Fiss_11343_11364_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11321CombineDFT_11180, pop_float(&SplitJoin77_CombineDFT_Fiss_11343_11364_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_11180() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_11321CombineDFT_11180), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11148_11184_11325_11346_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11182() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11148_11184_11325_11346_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11191WEIGHTED_ROUND_ROBIN_Splitter_11182));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11148_11184_11325_11346_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11191WEIGHTED_ROUND_ROBIN_Splitter_11182));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11183() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11183FloatPrinter_11181, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11148_11184_11325_11346_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11183FloatPrinter_11181, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11148_11184_11325_11346_join[1]));
		ENDFOR
	ENDFOR
}}

void FloatPrinter(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void FloatPrinter_11181() {
	FOR(uint32_t, __iter_steady_, 0, <, 1792, __iter_steady_++)
		FloatPrinter(&(WEIGHTED_ROUND_ROBIN_Joiner_11183FloatPrinter_11181));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 7, __iter_init_0_++)
		init_buffer_float(&SplitJoin69_CombineDFT_Fiss_11339_11360_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 7, __iter_init_1_++)
		init_buffer_float(&SplitJoin65_FFTReorderSimple_Fiss_11337_11358_split[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11264WEIGHTED_ROUND_ROBIN_Splitter_11269);
	FOR(int, __iter_init_2_, 0, <, 7, __iter_init_2_++)
		init_buffer_float(&SplitJoin69_CombineDFT_Fiss_11339_11360_split[__iter_init_2_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11250WEIGHTED_ROUND_ROBIN_Splitter_11255);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11195WEIGHTED_ROUND_ROBIN_Splitter_11198);
	FOR(int, __iter_init_3_, 0, <, 7, __iter_init_3_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_11330_11351_split[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11260WEIGHTED_ROUND_ROBIN_Splitter_11263);
	FOR(int, __iter_init_4_, 0, <, 7, __iter_init_4_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_11331_11352_join[__iter_init_4_]);
	ENDFOR
	init_buffer_float(&FFTReorderSimple_11159WEIGHTED_ROUND_ROBIN_Splitter_11194);
	FOR(int, __iter_init_5_, 0, <, 7, __iter_init_5_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_11330_11351_join[__iter_init_5_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11288WEIGHTED_ROUND_ROBIN_Splitter_11296);
	FOR(int, __iter_init_6_, 0, <, 7, __iter_init_6_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_11328_11349_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 2, __iter_init_7_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_11324_11345_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_float(&SplitJoin61_FFTReorderSimple_Fiss_11335_11356_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 2, __iter_init_9_++)
		init_buffer_float(&SplitJoin77_CombineDFT_Fiss_11343_11364_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_float(&SplitJoin77_CombineDFT_Fiss_11343_11364_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 4, __iter_init_11_++)
		init_buffer_float(&SplitJoin75_CombineDFT_Fiss_11342_11363_split[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 4, __iter_init_12_++)
		init_buffer_float(&SplitJoin63_FFTReorderSimple_Fiss_11336_11357_join[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 7, __iter_init_13_++)
		init_buffer_float(&SplitJoin67_FFTReorderSimple_Fiss_11338_11359_split[__iter_init_13_]);
	ENDFOR
	init_buffer_float(&FFTReorderSimple_11170WEIGHTED_ROUND_ROBIN_Splitter_11259);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11199WEIGHTED_ROUND_ROBIN_Splitter_11204);
	FOR(int, __iter_init_14_, 0, <, 7, __iter_init_14_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_11328_11349_join[__iter_init_14_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11256CombineDFT_11169);
	FOR(int, __iter_init_15_, 0, <, 4, __iter_init_15_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_11333_11354_split[__iter_init_15_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11297WEIGHTED_ROUND_ROBIN_Splitter_11305);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11306WEIGHTED_ROUND_ROBIN_Splitter_11314);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11223WEIGHTED_ROUND_ROBIN_Splitter_11231);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11270WEIGHTED_ROUND_ROBIN_Splitter_11278);
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_11324_11345_join[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_11334_11355_split[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 7, __iter_init_18_++)
		init_buffer_float(&SplitJoin71_CombineDFT_Fiss_11340_11361_join[__iter_init_18_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11321CombineDFT_11180);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11183FloatPrinter_11181);
	FOR(int, __iter_init_19_, 0, <, 2, __iter_init_19_++)
		init_buffer_float(&SplitJoin61_FFTReorderSimple_Fiss_11335_11356_split[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 2, __iter_init_20_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_11326_11347_split[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 4, __iter_init_21_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_11327_11348_join[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 2, __iter_init_22_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11148_11184_11325_11346_split[__iter_init_22_]);
	ENDFOR
	FOR(int, __iter_init_23_, 0, <, 2, __iter_init_23_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_11326_11347_join[__iter_init_23_]);
	ENDFOR
	FOR(int, __iter_init_24_, 0, <, 4, __iter_init_24_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_11333_11354_join[__iter_init_24_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11279WEIGHTED_ROUND_ROBIN_Splitter_11287);
	FOR(int, __iter_init_25_, 0, <, 7, __iter_init_25_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_11332_11353_split[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 7, __iter_init_26_++)
		init_buffer_float(&SplitJoin73_CombineDFT_Fiss_11341_11362_split[__iter_init_26_]);
	ENDFOR
	FOR(int, __iter_init_27_, 0, <, 7, __iter_init_27_++)
		init_buffer_float(&SplitJoin73_CombineDFT_Fiss_11341_11362_join[__iter_init_27_]);
	ENDFOR
	FOR(int, __iter_init_28_, 0, <, 2, __iter_init_28_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_11334_11355_join[__iter_init_28_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11214WEIGHTED_ROUND_ROBIN_Splitter_11222);
	FOR(int, __iter_init_29_, 0, <, 7, __iter_init_29_++)
		init_buffer_float(&SplitJoin65_FFTReorderSimple_Fiss_11337_11358_join[__iter_init_29_]);
	ENDFOR
	FOR(int, __iter_init_30_, 0, <, 7, __iter_init_30_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_11332_11353_join[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 7, __iter_init_31_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_11329_11350_split[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 7, __iter_init_32_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_11329_11350_join[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 7, __iter_init_33_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_11331_11352_split[__iter_init_33_]);
	ENDFOR
	FOR(int, __iter_init_34_, 0, <, 2, __iter_init_34_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_11148_11184_11325_11346_join[__iter_init_34_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11205WEIGHTED_ROUND_ROBIN_Splitter_11213);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11315WEIGHTED_ROUND_ROBIN_Splitter_11320);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11191WEIGHTED_ROUND_ROBIN_Splitter_11182);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11232WEIGHTED_ROUND_ROBIN_Splitter_11240);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11241WEIGHTED_ROUND_ROBIN_Splitter_11249);
	FOR(int, __iter_init_35_, 0, <, 7, __iter_init_35_++)
		init_buffer_float(&SplitJoin71_CombineDFT_Fiss_11340_11361_split[__iter_init_35_]);
	ENDFOR
	FOR(int, __iter_init_36_, 0, <, 4, __iter_init_36_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_11327_11348_split[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 7, __iter_init_37_++)
		init_buffer_float(&SplitJoin67_FFTReorderSimple_Fiss_11338_11359_join[__iter_init_37_]);
	ENDFOR
	FOR(int, __iter_init_38_, 0, <, 4, __iter_init_38_++)
		init_buffer_float(&SplitJoin75_CombineDFT_Fiss_11342_11363_join[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 4, __iter_init_39_++)
		init_buffer_float(&SplitJoin63_FFTReorderSimple_Fiss_11336_11357_split[__iter_init_39_]);
	ENDFOR
// --- init: CombineDFT_11224
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11224_s.w[i] = real ; 
		CombineDFT_11224_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11225
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11225_s.w[i] = real ; 
		CombineDFT_11225_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11226
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11226_s.w[i] = real ; 
		CombineDFT_11226_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11227
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11227_s.w[i] = real ; 
		CombineDFT_11227_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11228
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11228_s.w[i] = real ; 
		CombineDFT_11228_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11229
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11229_s.w[i] = real ; 
		CombineDFT_11229_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11230
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11230_s.w[i] = real ; 
		CombineDFT_11230_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11233
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11233_s.w[i] = real ; 
		CombineDFT_11233_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11234
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11234_s.w[i] = real ; 
		CombineDFT_11234_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11235
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11235_s.w[i] = real ; 
		CombineDFT_11235_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11236
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11236_s.w[i] = real ; 
		CombineDFT_11236_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11237
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11237_s.w[i] = real ; 
		CombineDFT_11237_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11238
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11238_s.w[i] = real ; 
		CombineDFT_11238_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11239
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11239_s.w[i] = real ; 
		CombineDFT_11239_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11242
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11242_s.w[i] = real ; 
		CombineDFT_11242_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11243
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11243_s.w[i] = real ; 
		CombineDFT_11243_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11244
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11244_s.w[i] = real ; 
		CombineDFT_11244_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11245
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11245_s.w[i] = real ; 
		CombineDFT_11245_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11246
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11246_s.w[i] = real ; 
		CombineDFT_11246_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11247
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11247_s.w[i] = real ; 
		CombineDFT_11247_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11248
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11248_s.w[i] = real ; 
		CombineDFT_11248_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11251
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_11251_s.w[i] = real ; 
		CombineDFT_11251_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11252
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_11252_s.w[i] = real ; 
		CombineDFT_11252_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11253
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_11253_s.w[i] = real ; 
		CombineDFT_11253_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11254
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_11254_s.w[i] = real ; 
		CombineDFT_11254_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11257
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_11257_s.w[i] = real ; 
		CombineDFT_11257_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11258
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_11258_s.w[i] = real ; 
		CombineDFT_11258_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11169
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_11169_s.w[i] = real ; 
		CombineDFT_11169_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11289
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11289_s.w[i] = real ; 
		CombineDFT_11289_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11290
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11290_s.w[i] = real ; 
		CombineDFT_11290_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11291
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11291_s.w[i] = real ; 
		CombineDFT_11291_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11292
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11292_s.w[i] = real ; 
		CombineDFT_11292_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11293
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11293_s.w[i] = real ; 
		CombineDFT_11293_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11294
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11294_s.w[i] = real ; 
		CombineDFT_11294_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11295
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_11295_s.w[i] = real ; 
		CombineDFT_11295_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11298
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11298_s.w[i] = real ; 
		CombineDFT_11298_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11299
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11299_s.w[i] = real ; 
		CombineDFT_11299_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11300
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11300_s.w[i] = real ; 
		CombineDFT_11300_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11301
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11301_s.w[i] = real ; 
		CombineDFT_11301_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11302
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11302_s.w[i] = real ; 
		CombineDFT_11302_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11303
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11303_s.w[i] = real ; 
		CombineDFT_11303_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11304
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_11304_s.w[i] = real ; 
		CombineDFT_11304_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11307
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11307_s.w[i] = real ; 
		CombineDFT_11307_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11308
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11308_s.w[i] = real ; 
		CombineDFT_11308_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11309
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11309_s.w[i] = real ; 
		CombineDFT_11309_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11310
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11310_s.w[i] = real ; 
		CombineDFT_11310_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11311
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11311_s.w[i] = real ; 
		CombineDFT_11311_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11312
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11312_s.w[i] = real ; 
		CombineDFT_11312_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11313
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_11313_s.w[i] = real ; 
		CombineDFT_11313_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11316
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_11316_s.w[i] = real ; 
		CombineDFT_11316_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11317
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_11317_s.w[i] = real ; 
		CombineDFT_11317_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11318
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_11318_s.w[i] = real ; 
		CombineDFT_11318_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11319
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_11319_s.w[i] = real ; 
		CombineDFT_11319_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11322
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_11322_s.w[i] = real ; 
		CombineDFT_11322_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11323
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_11323_s.w[i] = real ; 
		CombineDFT_11323_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_11180
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_11180_s.w[i] = real ; 
		CombineDFT_11180_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_11190();
			FFTTestSource_11192();
			FFTTestSource_11193();
		WEIGHTED_ROUND_ROBIN_Joiner_11191();
		WEIGHTED_ROUND_ROBIN_Splitter_11182();
			FFTReorderSimple_11159();
			WEIGHTED_ROUND_ROBIN_Splitter_11194();
				FFTReorderSimple_11196();
				FFTReorderSimple_11197();
			WEIGHTED_ROUND_ROBIN_Joiner_11195();
			WEIGHTED_ROUND_ROBIN_Splitter_11198();
				FFTReorderSimple_11200();
				FFTReorderSimple_11201();
				FFTReorderSimple_11202();
				FFTReorderSimple_11203();
			WEIGHTED_ROUND_ROBIN_Joiner_11199();
			WEIGHTED_ROUND_ROBIN_Splitter_11204();
				FFTReorderSimple_11206();
				FFTReorderSimple_11207();
				FFTReorderSimple_11208();
				FFTReorderSimple_11209();
				FFTReorderSimple_11210();
				FFTReorderSimple_11211();
				FFTReorderSimple_11212();
			WEIGHTED_ROUND_ROBIN_Joiner_11205();
			WEIGHTED_ROUND_ROBIN_Splitter_11213();
				FFTReorderSimple_11215();
				FFTReorderSimple_11216();
				FFTReorderSimple_11217();
				FFTReorderSimple_11218();
				FFTReorderSimple_11219();
				FFTReorderSimple_11220();
				FFTReorderSimple_11221();
			WEIGHTED_ROUND_ROBIN_Joiner_11214();
			WEIGHTED_ROUND_ROBIN_Splitter_11222();
				CombineDFT_11224();
				CombineDFT_11225();
				CombineDFT_11226();
				CombineDFT_11227();
				CombineDFT_11228();
				CombineDFT_11229();
				CombineDFT_11230();
			WEIGHTED_ROUND_ROBIN_Joiner_11223();
			WEIGHTED_ROUND_ROBIN_Splitter_11231();
				CombineDFT_11233();
				CombineDFT_11234();
				CombineDFT_11235();
				CombineDFT_11236();
				CombineDFT_11237();
				CombineDFT_11238();
				CombineDFT_11239();
			WEIGHTED_ROUND_ROBIN_Joiner_11232();
			WEIGHTED_ROUND_ROBIN_Splitter_11240();
				CombineDFT_11242();
				CombineDFT_11243();
				CombineDFT_11244();
				CombineDFT_11245();
				CombineDFT_11246();
				CombineDFT_11247();
				CombineDFT_11248();
			WEIGHTED_ROUND_ROBIN_Joiner_11241();
			WEIGHTED_ROUND_ROBIN_Splitter_11249();
				CombineDFT_11251();
				CombineDFT_11252();
				CombineDFT_11253();
				CombineDFT_11254();
			WEIGHTED_ROUND_ROBIN_Joiner_11250();
			WEIGHTED_ROUND_ROBIN_Splitter_11255();
				CombineDFT_11257();
				CombineDFT_11258();
			WEIGHTED_ROUND_ROBIN_Joiner_11256();
			CombineDFT_11169();
			FFTReorderSimple_11170();
			WEIGHTED_ROUND_ROBIN_Splitter_11259();
				FFTReorderSimple_11261();
				FFTReorderSimple_11262();
			WEIGHTED_ROUND_ROBIN_Joiner_11260();
			WEIGHTED_ROUND_ROBIN_Splitter_11263();
				FFTReorderSimple_11265();
				FFTReorderSimple_11266();
				FFTReorderSimple_11267();
				FFTReorderSimple_11268();
			WEIGHTED_ROUND_ROBIN_Joiner_11264();
			WEIGHTED_ROUND_ROBIN_Splitter_11269();
				FFTReorderSimple_11271();
				FFTReorderSimple_11272();
				FFTReorderSimple_11273();
				FFTReorderSimple_11274();
				FFTReorderSimple_11275();
				FFTReorderSimple_11276();
				FFTReorderSimple_11277();
			WEIGHTED_ROUND_ROBIN_Joiner_11270();
			WEIGHTED_ROUND_ROBIN_Splitter_11278();
				FFTReorderSimple_11280();
				FFTReorderSimple_11281();
				FFTReorderSimple_11282();
				FFTReorderSimple_11283();
				FFTReorderSimple_11284();
				FFTReorderSimple_11285();
				FFTReorderSimple_11286();
			WEIGHTED_ROUND_ROBIN_Joiner_11279();
			WEIGHTED_ROUND_ROBIN_Splitter_11287();
				CombineDFT_11289();
				CombineDFT_11290();
				CombineDFT_11291();
				CombineDFT_11292();
				CombineDFT_11293();
				CombineDFT_11294();
				CombineDFT_11295();
			WEIGHTED_ROUND_ROBIN_Joiner_11288();
			WEIGHTED_ROUND_ROBIN_Splitter_11296();
				CombineDFT_11298();
				CombineDFT_11299();
				CombineDFT_11300();
				CombineDFT_11301();
				CombineDFT_11302();
				CombineDFT_11303();
				CombineDFT_11304();
			WEIGHTED_ROUND_ROBIN_Joiner_11297();
			WEIGHTED_ROUND_ROBIN_Splitter_11305();
				CombineDFT_11307();
				CombineDFT_11308();
				CombineDFT_11309();
				CombineDFT_11310();
				CombineDFT_11311();
				CombineDFT_11312();
				CombineDFT_11313();
			WEIGHTED_ROUND_ROBIN_Joiner_11306();
			WEIGHTED_ROUND_ROBIN_Splitter_11314();
				CombineDFT_11316();
				CombineDFT_11317();
				CombineDFT_11318();
				CombineDFT_11319();
			WEIGHTED_ROUND_ROBIN_Joiner_11315();
			WEIGHTED_ROUND_ROBIN_Splitter_11320();
				CombineDFT_11322();
				CombineDFT_11323();
			WEIGHTED_ROUND_ROBIN_Joiner_11321();
			CombineDFT_11180();
		WEIGHTED_ROUND_ROBIN_Joiner_11183();
		FloatPrinter_11181();
	ENDFOR
	return EXIT_SUCCESS;
}
