#include "PEG23-FFT6_nocache.h"

buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_2280_2290_split[2];
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_2281_2291_join[4];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_2285_2295_split[16];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2180WEIGHTED_ROUND_ROBIN_Splitter_2183;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2218WEIGHTED_ROUND_ROBIN_Splitter_2242;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2271WEIGHTED_ROUND_ROBIN_Splitter_2276;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_2284_2294_split[23];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_2287_2297_split[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2190WEIGHTED_ROUND_ROBIN_Splitter_2199;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2200WEIGHTED_ROUND_ROBIN_Splitter_2217;
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_2282_2292_split[8];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2243WEIGHTED_ROUND_ROBIN_Splitter_2260;
buffer_complex_t SplitJoin12_CombineDFT_Fiss_2286_2296_join[8];
buffer_complex_t FFTReorderSimple_2166WEIGHTED_ROUND_ROBIN_Splitter_2179;
buffer_complex_t CombineDFT_2176CPrinter_2177;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_2284_2294_join[23];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_2288_2298_split[2];
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_2281_2291_split[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2277CombineDFT_2176;
buffer_complex_t FFTTestSource_2165FFTReorderSimple_2166;
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[16];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_2280_2290_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2261WEIGHTED_ROUND_ROBIN_Splitter_2270;
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_2282_2292_join[8];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2184WEIGHTED_ROUND_ROBIN_Splitter_2189;
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[16];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_2288_2298_join[2];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_2287_2297_join[4];
buffer_complex_t SplitJoin12_CombineDFT_Fiss_2286_2296_split[8];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_2285_2295_join[16];


CombineDFT_2219_t CombineDFT_2219_s;
CombineDFT_2219_t CombineDFT_2220_s;
CombineDFT_2219_t CombineDFT_2221_s;
CombineDFT_2219_t CombineDFT_2222_s;
CombineDFT_2219_t CombineDFT_2223_s;
CombineDFT_2219_t CombineDFT_2224_s;
CombineDFT_2219_t CombineDFT_2225_s;
CombineDFT_2219_t CombineDFT_2226_s;
CombineDFT_2219_t CombineDFT_2227_s;
CombineDFT_2219_t CombineDFT_2228_s;
CombineDFT_2219_t CombineDFT_2229_s;
CombineDFT_2219_t CombineDFT_2230_s;
CombineDFT_2219_t CombineDFT_2231_s;
CombineDFT_2219_t CombineDFT_2232_s;
CombineDFT_2219_t CombineDFT_2233_s;
CombineDFT_2219_t CombineDFT_2234_s;
CombineDFT_2219_t CombineDFT_2235_s;
CombineDFT_2219_t CombineDFT_2236_s;
CombineDFT_2219_t CombineDFT_2237_s;
CombineDFT_2219_t CombineDFT_2238_s;
CombineDFT_2219_t CombineDFT_2239_s;
CombineDFT_2219_t CombineDFT_2240_s;
CombineDFT_2219_t CombineDFT_2241_s;
CombineDFT_2219_t CombineDFT_2244_s;
CombineDFT_2219_t CombineDFT_2245_s;
CombineDFT_2219_t CombineDFT_2246_s;
CombineDFT_2219_t CombineDFT_2247_s;
CombineDFT_2219_t CombineDFT_2248_s;
CombineDFT_2219_t CombineDFT_2249_s;
CombineDFT_2219_t CombineDFT_2250_s;
CombineDFT_2219_t CombineDFT_2251_s;
CombineDFT_2219_t CombineDFT_2252_s;
CombineDFT_2219_t CombineDFT_2253_s;
CombineDFT_2219_t CombineDFT_2254_s;
CombineDFT_2219_t CombineDFT_2255_s;
CombineDFT_2219_t CombineDFT_2256_s;
CombineDFT_2219_t CombineDFT_2257_s;
CombineDFT_2219_t CombineDFT_2258_s;
CombineDFT_2219_t CombineDFT_2259_s;
CombineDFT_2219_t CombineDFT_2262_s;
CombineDFT_2219_t CombineDFT_2263_s;
CombineDFT_2219_t CombineDFT_2264_s;
CombineDFT_2219_t CombineDFT_2265_s;
CombineDFT_2219_t CombineDFT_2266_s;
CombineDFT_2219_t CombineDFT_2267_s;
CombineDFT_2219_t CombineDFT_2268_s;
CombineDFT_2219_t CombineDFT_2269_s;
CombineDFT_2219_t CombineDFT_2272_s;
CombineDFT_2219_t CombineDFT_2273_s;
CombineDFT_2219_t CombineDFT_2274_s;
CombineDFT_2219_t CombineDFT_2275_s;
CombineDFT_2219_t CombineDFT_2278_s;
CombineDFT_2219_t CombineDFT_2279_s;
CombineDFT_2219_t CombineDFT_2176_s;

void FFTTestSource_2165(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t c1;
		complex_t zero;
		c1.real = 1.0 ; 
		c1.imag = 0.0 ; 
		zero.real = 0.0 ; 
		zero.imag = 0.0 ; 
		push_complex(&FFTTestSource_2165FFTReorderSimple_2166, zero) ; 
		push_complex(&FFTTestSource_2165FFTReorderSimple_2166, c1) ; 
		FOR(int, i, 0,  < , 62, i++) {
			push_complex(&FFTTestSource_2165FFTReorderSimple_2166, zero) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2166(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&FFTTestSource_2165FFTReorderSimple_2166, i)) ; 
			push_complex(&FFTReorderSimple_2166WEIGHTED_ROUND_ROBIN_Splitter_2179, __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 64, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&FFTTestSource_2165FFTReorderSimple_2166, i)) ; 
			push_complex(&FFTReorderSimple_2166WEIGHTED_ROUND_ROBIN_Splitter_2179, __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&FFTTestSource_2165FFTReorderSimple_2166) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2181(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_2280_2290_split[0], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_2280_2290_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 32, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_2280_2290_split[0], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_2280_2290_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_2280_2290_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2182(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_2280_2290_split[1], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_2280_2290_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 32, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_2280_2290_split[1], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_2280_2290_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_2280_2290_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2179() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_2280_2290_split[0], pop_complex(&FFTReorderSimple_2166WEIGHTED_ROUND_ROBIN_Splitter_2179));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_2280_2290_split[1], pop_complex(&FFTReorderSimple_2166WEIGHTED_ROUND_ROBIN_Splitter_2179));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2180() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2180WEIGHTED_ROUND_ROBIN_Splitter_2183, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_2280_2290_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2180WEIGHTED_ROUND_ROBIN_Splitter_2183, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_2280_2290_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_2185(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_2281_2291_split[0], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_2281_2291_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_2281_2291_split[0], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_2281_2291_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_2281_2291_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2186(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_2281_2291_split[1], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_2281_2291_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_2281_2291_split[1], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_2281_2291_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_2281_2291_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2187(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_2281_2291_split[2], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_2281_2291_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_2281_2291_split[2], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_2281_2291_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_2281_2291_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2188(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_2281_2291_split[3], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_2281_2291_join[3], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_2281_2291_split[3], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_2281_2291_join[3], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_2281_2291_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2183() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin2_FFTReorderSimple_Fiss_2281_2291_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2180WEIGHTED_ROUND_ROBIN_Splitter_2183));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2184() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2184WEIGHTED_ROUND_ROBIN_Splitter_2189, pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_2281_2291_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_2191(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_split[0], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_split[0], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2192(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_split[1], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_split[1], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2193(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_split[2], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_split[2], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2194(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_split[3], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_join[3], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_split[3], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_join[3], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2195(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_split[4], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_join[4], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_split[4], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_join[4], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2196(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_split[5], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_join[5], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_split[5], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_join[5], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2197(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_split[6], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_join[6], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_split[6], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_join[6], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2198(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_split[7], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_join[7], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_split[7], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_join[7], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2189() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2184WEIGHTED_ROUND_ROBIN_Splitter_2189));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2190() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2190WEIGHTED_ROUND_ROBIN_Splitter_2199, pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_2201(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[0], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[0], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2202(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[1], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[1], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2203(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[2], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[2], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2204(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[3], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[3], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[3], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[3], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2205(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[4], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[4], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[4], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[4], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2206(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[5], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[5], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[5], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[5], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2207(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[6], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[6], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[6], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[6], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2208(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[7], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[7], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[7], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[7], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2209(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[8], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[8], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[8], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[8], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[8]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2210(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[9], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[9], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[9], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[9], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[9]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2211(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[10], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[10], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[10], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[10], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[10]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2212(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[11], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[11], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[11], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[11], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[11]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2213(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[12], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[12], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[12], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[12], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[12]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2214(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[13], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[13], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[13], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[13], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[13]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2215(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[14], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[14], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[14], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[14], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[14]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_2216(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[15], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[15], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[15], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[15], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[15]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2199() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2190WEIGHTED_ROUND_ROBIN_Splitter_2199));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2200() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2200WEIGHTED_ROUND_ROBIN_Splitter_2217, pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2219(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[0], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2219_s.wn.real) - (w.imag * CombineDFT_2219_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2219_s.wn.imag) + (w.imag * CombineDFT_2219_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[0]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2220(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[1], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2220_s.wn.real) - (w.imag * CombineDFT_2220_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2220_s.wn.imag) + (w.imag * CombineDFT_2220_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[1]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2221(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[2], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2221_s.wn.real) - (w.imag * CombineDFT_2221_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2221_s.wn.imag) + (w.imag * CombineDFT_2221_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[2]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2222(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[3], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2222_s.wn.real) - (w.imag * CombineDFT_2222_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2222_s.wn.imag) + (w.imag * CombineDFT_2222_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[3]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2223(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[4], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[4], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2223_s.wn.real) - (w.imag * CombineDFT_2223_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2223_s.wn.imag) + (w.imag * CombineDFT_2223_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[4]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2224(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[5], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[5], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2224_s.wn.real) - (w.imag * CombineDFT_2224_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2224_s.wn.imag) + (w.imag * CombineDFT_2224_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[5]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2225(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[6], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[6], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2225_s.wn.real) - (w.imag * CombineDFT_2225_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2225_s.wn.imag) + (w.imag * CombineDFT_2225_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[6]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2226(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[7], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[7], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2226_s.wn.real) - (w.imag * CombineDFT_2226_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2226_s.wn.imag) + (w.imag * CombineDFT_2226_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[7]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2227(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[8], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[8], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2227_s.wn.real) - (w.imag * CombineDFT_2227_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2227_s.wn.imag) + (w.imag * CombineDFT_2227_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[8]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_join[8], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2228(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[9], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[9], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2228_s.wn.real) - (w.imag * CombineDFT_2228_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2228_s.wn.imag) + (w.imag * CombineDFT_2228_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[9]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_join[9], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2229(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[10], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[10], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2229_s.wn.real) - (w.imag * CombineDFT_2229_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2229_s.wn.imag) + (w.imag * CombineDFT_2229_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[10]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_join[10], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2230(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[11], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[11], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2230_s.wn.real) - (w.imag * CombineDFT_2230_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2230_s.wn.imag) + (w.imag * CombineDFT_2230_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[11]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_join[11], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2231(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[12], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[12], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2231_s.wn.real) - (w.imag * CombineDFT_2231_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2231_s.wn.imag) + (w.imag * CombineDFT_2231_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[12]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_join[12], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2232(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[13], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[13], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2232_s.wn.real) - (w.imag * CombineDFT_2232_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2232_s.wn.imag) + (w.imag * CombineDFT_2232_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[13]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_join[13], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2233(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[14], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[14], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2233_s.wn.real) - (w.imag * CombineDFT_2233_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2233_s.wn.imag) + (w.imag * CombineDFT_2233_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[14]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_join[14], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2234(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[15], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[15], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2234_s.wn.real) - (w.imag * CombineDFT_2234_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2234_s.wn.imag) + (w.imag * CombineDFT_2234_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[15]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_join[15], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2235(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[16], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[16], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2235_s.wn.real) - (w.imag * CombineDFT_2235_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2235_s.wn.imag) + (w.imag * CombineDFT_2235_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[16]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_join[16], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2236(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[17], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[17], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2236_s.wn.real) - (w.imag * CombineDFT_2236_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2236_s.wn.imag) + (w.imag * CombineDFT_2236_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[17]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_join[17], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2237(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[18], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[18], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2237_s.wn.real) - (w.imag * CombineDFT_2237_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2237_s.wn.imag) + (w.imag * CombineDFT_2237_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[18]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_join[18], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2238(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[19], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[19], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2238_s.wn.real) - (w.imag * CombineDFT_2238_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2238_s.wn.imag) + (w.imag * CombineDFT_2238_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[19]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_join[19], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2239(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[20], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[20], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2239_s.wn.real) - (w.imag * CombineDFT_2239_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2239_s.wn.imag) + (w.imag * CombineDFT_2239_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[20]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_join[20], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2240(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[21], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[21], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2240_s.wn.real) - (w.imag * CombineDFT_2240_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2240_s.wn.imag) + (w.imag * CombineDFT_2240_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[21]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_join[21], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2241(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[22], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[22], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2241_s.wn.real) - (w.imag * CombineDFT_2241_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2241_s.wn.imag) + (w.imag * CombineDFT_2241_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[22]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_join[22], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2217() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 23, __iter_++)
			push_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2200WEIGHTED_ROUND_ROBIN_Splitter_2217));
			push_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2200WEIGHTED_ROUND_ROBIN_Splitter_2217));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2218() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 23, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2218WEIGHTED_ROUND_ROBIN_Splitter_2242, pop_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_join[__iter_]));
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2218WEIGHTED_ROUND_ROBIN_Splitter_2242, pop_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_2244(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[0], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2244_s.wn.real) - (w.imag * CombineDFT_2244_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2244_s.wn.imag) + (w.imag * CombineDFT_2244_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[0]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2245(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[1], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2245_s.wn.real) - (w.imag * CombineDFT_2245_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2245_s.wn.imag) + (w.imag * CombineDFT_2245_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[1]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2246(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[2], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2246_s.wn.real) - (w.imag * CombineDFT_2246_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2246_s.wn.imag) + (w.imag * CombineDFT_2246_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[2]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2247(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[3], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2247_s.wn.real) - (w.imag * CombineDFT_2247_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2247_s.wn.imag) + (w.imag * CombineDFT_2247_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[3]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2248(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[4], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[4], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2248_s.wn.real) - (w.imag * CombineDFT_2248_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2248_s.wn.imag) + (w.imag * CombineDFT_2248_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[4]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2249(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[5], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[5], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2249_s.wn.real) - (w.imag * CombineDFT_2249_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2249_s.wn.imag) + (w.imag * CombineDFT_2249_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[5]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2250(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[6], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[6], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2250_s.wn.real) - (w.imag * CombineDFT_2250_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2250_s.wn.imag) + (w.imag * CombineDFT_2250_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[6]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2251(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[7], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[7], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2251_s.wn.real) - (w.imag * CombineDFT_2251_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2251_s.wn.imag) + (w.imag * CombineDFT_2251_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[7]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2252(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[8], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[8], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2252_s.wn.real) - (w.imag * CombineDFT_2252_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2252_s.wn.imag) + (w.imag * CombineDFT_2252_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[8]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_join[8], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2253(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[9], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[9], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2253_s.wn.real) - (w.imag * CombineDFT_2253_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2253_s.wn.imag) + (w.imag * CombineDFT_2253_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[9]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_join[9], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2254(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[10], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[10], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2254_s.wn.real) - (w.imag * CombineDFT_2254_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2254_s.wn.imag) + (w.imag * CombineDFT_2254_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[10]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_join[10], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2255(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[11], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[11], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2255_s.wn.real) - (w.imag * CombineDFT_2255_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2255_s.wn.imag) + (w.imag * CombineDFT_2255_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[11]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_join[11], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2256(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[12], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[12], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2256_s.wn.real) - (w.imag * CombineDFT_2256_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2256_s.wn.imag) + (w.imag * CombineDFT_2256_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[12]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_join[12], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2257(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[13], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[13], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2257_s.wn.real) - (w.imag * CombineDFT_2257_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2257_s.wn.imag) + (w.imag * CombineDFT_2257_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[13]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_join[13], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2258(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[14], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[14], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2258_s.wn.real) - (w.imag * CombineDFT_2258_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2258_s.wn.imag) + (w.imag * CombineDFT_2258_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[14]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_join[14], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2259(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[15], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[15], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2259_s.wn.real) - (w.imag * CombineDFT_2259_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2259_s.wn.imag) + (w.imag * CombineDFT_2259_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[15]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_join[15], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2242() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2218WEIGHTED_ROUND_ROBIN_Splitter_2242));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2243() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2243WEIGHTED_ROUND_ROBIN_Splitter_2260, pop_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2262(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_split[0], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2262_s.wn.real) - (w.imag * CombineDFT_2262_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2262_s.wn.imag) + (w.imag * CombineDFT_2262_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_split[0]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2263(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_split[1], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2263_s.wn.real) - (w.imag * CombineDFT_2263_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2263_s.wn.imag) + (w.imag * CombineDFT_2263_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_split[1]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2264(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_split[2], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2264_s.wn.real) - (w.imag * CombineDFT_2264_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2264_s.wn.imag) + (w.imag * CombineDFT_2264_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_split[2]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2265(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_split[3], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2265_s.wn.real) - (w.imag * CombineDFT_2265_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2265_s.wn.imag) + (w.imag * CombineDFT_2265_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_split[3]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2266(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_split[4], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_split[4], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2266_s.wn.real) - (w.imag * CombineDFT_2266_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2266_s.wn.imag) + (w.imag * CombineDFT_2266_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_split[4]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2267(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_split[5], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_split[5], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2267_s.wn.real) - (w.imag * CombineDFT_2267_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2267_s.wn.imag) + (w.imag * CombineDFT_2267_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_split[5]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2268(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_split[6], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_split[6], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2268_s.wn.real) - (w.imag * CombineDFT_2268_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2268_s.wn.imag) + (w.imag * CombineDFT_2268_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_split[6]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2269(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_split[7], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_split[7], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2269_s.wn.real) - (w.imag * CombineDFT_2269_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2269_s.wn.imag) + (w.imag * CombineDFT_2269_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_split[7]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2260() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2243WEIGHTED_ROUND_ROBIN_Splitter_2260));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2261() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2261WEIGHTED_ROUND_ROBIN_Splitter_2270, pop_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2272(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_2287_2297_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_2287_2297_split[0], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2272_s.wn.real) - (w.imag * CombineDFT_2272_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2272_s.wn.imag) + (w.imag * CombineDFT_2272_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_2287_2297_split[0]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_2287_2297_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2273(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_2287_2297_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_2287_2297_split[1], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2273_s.wn.real) - (w.imag * CombineDFT_2273_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2273_s.wn.imag) + (w.imag * CombineDFT_2273_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_2287_2297_split[1]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_2287_2297_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2274(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_2287_2297_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_2287_2297_split[2], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2274_s.wn.real) - (w.imag * CombineDFT_2274_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2274_s.wn.imag) + (w.imag * CombineDFT_2274_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_2287_2297_split[2]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_2287_2297_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2275(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_2287_2297_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_2287_2297_split[3], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2275_s.wn.real) - (w.imag * CombineDFT_2275_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2275_s.wn.imag) + (w.imag * CombineDFT_2275_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_2287_2297_split[3]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_2287_2297_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2270() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin14_CombineDFT_Fiss_2287_2297_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2261WEIGHTED_ROUND_ROBIN_Splitter_2270));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2271() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2271WEIGHTED_ROUND_ROBIN_Splitter_2276, pop_complex(&SplitJoin14_CombineDFT_Fiss_2287_2297_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2278(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[32];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 16, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_2288_2298_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_2288_2298_split[0], (16 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(16 + i)].real = (y0.real - y1w.real) ; 
			results[(16 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2278_s.wn.real) - (w.imag * CombineDFT_2278_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2278_s.wn.imag) + (w.imag * CombineDFT_2278_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin16_CombineDFT_Fiss_2288_2298_split[0]) ; 
			push_complex(&SplitJoin16_CombineDFT_Fiss_2288_2298_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_2279(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[32];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 16, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_2288_2298_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_2288_2298_split[1], (16 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(16 + i)].real = (y0.real - y1w.real) ; 
			results[(16 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2279_s.wn.real) - (w.imag * CombineDFT_2279_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2279_s.wn.imag) + (w.imag * CombineDFT_2279_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin16_CombineDFT_Fiss_2288_2298_split[1]) ; 
			push_complex(&SplitJoin16_CombineDFT_Fiss_2288_2298_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2276() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_2288_2298_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2271WEIGHTED_ROUND_ROBIN_Splitter_2276));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_2288_2298_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2271WEIGHTED_ROUND_ROBIN_Splitter_2276));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2277() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2277CombineDFT_2176, pop_complex(&SplitJoin16_CombineDFT_Fiss_2288_2298_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2277CombineDFT_2176, pop_complex(&SplitJoin16_CombineDFT_Fiss_2288_2298_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_2176(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[64];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2277CombineDFT_2176, i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2277CombineDFT_2176, (32 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(32 + i)].real = (y0.real - y1w.real) ; 
			results[(32 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2176_s.wn.real) - (w.imag * CombineDFT_2176_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2176_s.wn.imag) + (w.imag * CombineDFT_2176_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2277CombineDFT_2176) ; 
			push_complex(&CombineDFT_2176CPrinter_2177, results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CPrinter_2177(){
	FOR(uint32_t, __iter_steady_, 0, <, 1472, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&CombineDFT_2176CPrinter_2177));
		printf("%.10f", c.real);
		printf("\n");
		printf("%.10f", c.imag);
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_2280_2290_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 4, __iter_init_1_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_2281_2291_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 16, __iter_init_2_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_split[__iter_init_2_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2180WEIGHTED_ROUND_ROBIN_Splitter_2183);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2218WEIGHTED_ROUND_ROBIN_Splitter_2242);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2271WEIGHTED_ROUND_ROBIN_Splitter_2276);
	FOR(int, __iter_init_3_, 0, <, 23, __iter_init_3_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 4, __iter_init_4_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_2287_2297_split[__iter_init_4_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2190WEIGHTED_ROUND_ROBIN_Splitter_2199);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2200WEIGHTED_ROUND_ROBIN_Splitter_2217);
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_split[__iter_init_5_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2243WEIGHTED_ROUND_ROBIN_Splitter_2260);
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_join[__iter_init_6_]);
	ENDFOR
	init_buffer_complex(&FFTReorderSimple_2166WEIGHTED_ROUND_ROBIN_Splitter_2179);
	init_buffer_complex(&CombineDFT_2176CPrinter_2177);
	FOR(int, __iter_init_7_, 0, <, 23, __iter_init_7_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_2284_2294_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_2288_2298_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 4, __iter_init_9_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_2281_2291_split[__iter_init_9_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2277CombineDFT_2176);
	init_buffer_complex(&FFTTestSource_2165FFTReorderSimple_2166);
	FOR(int, __iter_init_10_, 0, <, 16, __iter_init_10_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 2, __iter_init_11_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_2280_2290_join[__iter_init_11_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2261WEIGHTED_ROUND_ROBIN_Splitter_2270);
	FOR(int, __iter_init_12_, 0, <, 8, __iter_init_12_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_2282_2292_join[__iter_init_12_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2184WEIGHTED_ROUND_ROBIN_Splitter_2189);
	FOR(int, __iter_init_13_, 0, <, 16, __iter_init_13_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_2283_2293_split[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_2288_2298_join[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 4, __iter_init_15_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_2287_2297_join[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 8, __iter_init_16_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_2286_2296_split[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 16, __iter_init_17_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_2285_2295_join[__iter_init_17_]);
	ENDFOR
// --- init: CombineDFT_2219
	 {
	 ; 
	CombineDFT_2219_s.wn.real = -1.0 ; 
	CombineDFT_2219_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2220
	 {
	 ; 
	CombineDFT_2220_s.wn.real = -1.0 ; 
	CombineDFT_2220_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2221
	 {
	 ; 
	CombineDFT_2221_s.wn.real = -1.0 ; 
	CombineDFT_2221_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2222
	 {
	 ; 
	CombineDFT_2222_s.wn.real = -1.0 ; 
	CombineDFT_2222_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2223
	 {
	 ; 
	CombineDFT_2223_s.wn.real = -1.0 ; 
	CombineDFT_2223_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2224
	 {
	 ; 
	CombineDFT_2224_s.wn.real = -1.0 ; 
	CombineDFT_2224_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2225
	 {
	 ; 
	CombineDFT_2225_s.wn.real = -1.0 ; 
	CombineDFT_2225_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2226
	 {
	 ; 
	CombineDFT_2226_s.wn.real = -1.0 ; 
	CombineDFT_2226_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2227
	 {
	 ; 
	CombineDFT_2227_s.wn.real = -1.0 ; 
	CombineDFT_2227_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2228
	 {
	 ; 
	CombineDFT_2228_s.wn.real = -1.0 ; 
	CombineDFT_2228_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2229
	 {
	 ; 
	CombineDFT_2229_s.wn.real = -1.0 ; 
	CombineDFT_2229_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2230
	 {
	 ; 
	CombineDFT_2230_s.wn.real = -1.0 ; 
	CombineDFT_2230_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2231
	 {
	 ; 
	CombineDFT_2231_s.wn.real = -1.0 ; 
	CombineDFT_2231_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2232
	 {
	 ; 
	CombineDFT_2232_s.wn.real = -1.0 ; 
	CombineDFT_2232_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2233
	 {
	 ; 
	CombineDFT_2233_s.wn.real = -1.0 ; 
	CombineDFT_2233_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2234
	 {
	 ; 
	CombineDFT_2234_s.wn.real = -1.0 ; 
	CombineDFT_2234_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2235
	 {
	 ; 
	CombineDFT_2235_s.wn.real = -1.0 ; 
	CombineDFT_2235_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2236
	 {
	 ; 
	CombineDFT_2236_s.wn.real = -1.0 ; 
	CombineDFT_2236_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2237
	 {
	 ; 
	CombineDFT_2237_s.wn.real = -1.0 ; 
	CombineDFT_2237_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2238
	 {
	 ; 
	CombineDFT_2238_s.wn.real = -1.0 ; 
	CombineDFT_2238_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2239
	 {
	 ; 
	CombineDFT_2239_s.wn.real = -1.0 ; 
	CombineDFT_2239_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2240
	 {
	 ; 
	CombineDFT_2240_s.wn.real = -1.0 ; 
	CombineDFT_2240_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2241
	 {
	 ; 
	CombineDFT_2241_s.wn.real = -1.0 ; 
	CombineDFT_2241_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2244
	 {
	 ; 
	CombineDFT_2244_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2244_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2245
	 {
	 ; 
	CombineDFT_2245_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2245_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2246
	 {
	 ; 
	CombineDFT_2246_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2246_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2247
	 {
	 ; 
	CombineDFT_2247_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2247_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2248
	 {
	 ; 
	CombineDFT_2248_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2248_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2249
	 {
	 ; 
	CombineDFT_2249_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2249_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2250
	 {
	 ; 
	CombineDFT_2250_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2250_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2251
	 {
	 ; 
	CombineDFT_2251_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2251_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2252
	 {
	 ; 
	CombineDFT_2252_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2252_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2253
	 {
	 ; 
	CombineDFT_2253_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2253_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2254
	 {
	 ; 
	CombineDFT_2254_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2254_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2255
	 {
	 ; 
	CombineDFT_2255_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2255_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2256
	 {
	 ; 
	CombineDFT_2256_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2256_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2257
	 {
	 ; 
	CombineDFT_2257_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2257_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2258
	 {
	 ; 
	CombineDFT_2258_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2258_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2259
	 {
	 ; 
	CombineDFT_2259_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2259_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2262
	 {
	 ; 
	CombineDFT_2262_s.wn.real = 0.70710677 ; 
	CombineDFT_2262_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2263
	 {
	 ; 
	CombineDFT_2263_s.wn.real = 0.70710677 ; 
	CombineDFT_2263_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2264
	 {
	 ; 
	CombineDFT_2264_s.wn.real = 0.70710677 ; 
	CombineDFT_2264_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2265
	 {
	 ; 
	CombineDFT_2265_s.wn.real = 0.70710677 ; 
	CombineDFT_2265_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2266
	 {
	 ; 
	CombineDFT_2266_s.wn.real = 0.70710677 ; 
	CombineDFT_2266_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2267
	 {
	 ; 
	CombineDFT_2267_s.wn.real = 0.70710677 ; 
	CombineDFT_2267_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2268
	 {
	 ; 
	CombineDFT_2268_s.wn.real = 0.70710677 ; 
	CombineDFT_2268_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2269
	 {
	 ; 
	CombineDFT_2269_s.wn.real = 0.70710677 ; 
	CombineDFT_2269_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2272
	 {
	 ; 
	CombineDFT_2272_s.wn.real = 0.9238795 ; 
	CombineDFT_2272_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_2273
	 {
	 ; 
	CombineDFT_2273_s.wn.real = 0.9238795 ; 
	CombineDFT_2273_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_2274
	 {
	 ; 
	CombineDFT_2274_s.wn.real = 0.9238795 ; 
	CombineDFT_2274_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_2275
	 {
	 ; 
	CombineDFT_2275_s.wn.real = 0.9238795 ; 
	CombineDFT_2275_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_2278
	 {
	 ; 
	CombineDFT_2278_s.wn.real = 0.98078525 ; 
	CombineDFT_2278_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_2279
	 {
	 ; 
	CombineDFT_2279_s.wn.real = 0.98078525 ; 
	CombineDFT_2279_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_2176
	 {
	 ; 
	CombineDFT_2176_s.wn.real = 0.9951847 ; 
	CombineDFT_2176_s.wn.imag = -0.09801714 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FFTTestSource_2165();
		FFTReorderSimple_2166();
		WEIGHTED_ROUND_ROBIN_Splitter_2179();
			FFTReorderSimple_2181();
			FFTReorderSimple_2182();
		WEIGHTED_ROUND_ROBIN_Joiner_2180();
		WEIGHTED_ROUND_ROBIN_Splitter_2183();
			FFTReorderSimple_2185();
			FFTReorderSimple_2186();
			FFTReorderSimple_2187();
			FFTReorderSimple_2188();
		WEIGHTED_ROUND_ROBIN_Joiner_2184();
		WEIGHTED_ROUND_ROBIN_Splitter_2189();
			FFTReorderSimple_2191();
			FFTReorderSimple_2192();
			FFTReorderSimple_2193();
			FFTReorderSimple_2194();
			FFTReorderSimple_2195();
			FFTReorderSimple_2196();
			FFTReorderSimple_2197();
			FFTReorderSimple_2198();
		WEIGHTED_ROUND_ROBIN_Joiner_2190();
		WEIGHTED_ROUND_ROBIN_Splitter_2199();
			FFTReorderSimple_2201();
			FFTReorderSimple_2202();
			FFTReorderSimple_2203();
			FFTReorderSimple_2204();
			FFTReorderSimple_2205();
			FFTReorderSimple_2206();
			FFTReorderSimple_2207();
			FFTReorderSimple_2208();
			FFTReorderSimple_2209();
			FFTReorderSimple_2210();
			FFTReorderSimple_2211();
			FFTReorderSimple_2212();
			FFTReorderSimple_2213();
			FFTReorderSimple_2214();
			FFTReorderSimple_2215();
			FFTReorderSimple_2216();
		WEIGHTED_ROUND_ROBIN_Joiner_2200();
		WEIGHTED_ROUND_ROBIN_Splitter_2217();
			CombineDFT_2219();
			CombineDFT_2220();
			CombineDFT_2221();
			CombineDFT_2222();
			CombineDFT_2223();
			CombineDFT_2224();
			CombineDFT_2225();
			CombineDFT_2226();
			CombineDFT_2227();
			CombineDFT_2228();
			CombineDFT_2229();
			CombineDFT_2230();
			CombineDFT_2231();
			CombineDFT_2232();
			CombineDFT_2233();
			CombineDFT_2234();
			CombineDFT_2235();
			CombineDFT_2236();
			CombineDFT_2237();
			CombineDFT_2238();
			CombineDFT_2239();
			CombineDFT_2240();
			CombineDFT_2241();
		WEIGHTED_ROUND_ROBIN_Joiner_2218();
		WEIGHTED_ROUND_ROBIN_Splitter_2242();
			CombineDFT_2244();
			CombineDFT_2245();
			CombineDFT_2246();
			CombineDFT_2247();
			CombineDFT_2248();
			CombineDFT_2249();
			CombineDFT_2250();
			CombineDFT_2251();
			CombineDFT_2252();
			CombineDFT_2253();
			CombineDFT_2254();
			CombineDFT_2255();
			CombineDFT_2256();
			CombineDFT_2257();
			CombineDFT_2258();
			CombineDFT_2259();
		WEIGHTED_ROUND_ROBIN_Joiner_2243();
		WEIGHTED_ROUND_ROBIN_Splitter_2260();
			CombineDFT_2262();
			CombineDFT_2263();
			CombineDFT_2264();
			CombineDFT_2265();
			CombineDFT_2266();
			CombineDFT_2267();
			CombineDFT_2268();
			CombineDFT_2269();
		WEIGHTED_ROUND_ROBIN_Joiner_2261();
		WEIGHTED_ROUND_ROBIN_Splitter_2270();
			CombineDFT_2272();
			CombineDFT_2273();
			CombineDFT_2274();
			CombineDFT_2275();
		WEIGHTED_ROUND_ROBIN_Joiner_2271();
		WEIGHTED_ROUND_ROBIN_Splitter_2276();
			CombineDFT_2278();
			CombineDFT_2279();
		WEIGHTED_ROUND_ROBIN_Joiner_2277();
		CombineDFT_2176();
		CPrinter_2177();
	ENDFOR
	return EXIT_SUCCESS;
}
