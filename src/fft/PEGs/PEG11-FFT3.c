#include "PEG11-FFT3.h"

buffer_float_t SplitJoin65_Butterfly_Fiss_5685_5711_split[2];
buffer_float_t SplitJoin4_Butterfly_Fiss_5672_5693_split[8];
buffer_float_t SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_child0_5561_5702_split[4];
buffer_float_t Post_CollapsedDataParallel_2_5443WEIGHTED_ROUND_ROBIN_Splitter_5563;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5582Post_CollapsedDataParallel_2_5440;
buffer_float_t SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_Hier_5676_5712_join[2];
buffer_float_t FloatSource_5138Pre_CollapsedDataParallel_1_5439;
buffer_float_t Pre_CollapsedDataParallel_1_5454WEIGHTED_ROUND_ROBIN_Splitter_5626;
buffer_float_t Pre_CollapsedDataParallel_1_5463WEIGHTED_ROUND_ROBIN_Splitter_5642;
buffer_float_t SplitJoin57_Butterfly_Fiss_5683_5709_join[2];
buffer_float_t SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_child0_5532_5694_split[2];
buffer_float_t SplitJoin85_Butterfly_Fiss_5687_5697_split[8];
buffer_float_t SplitJoin61_Butterfly_Fiss_5684_5710_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5595Post_CollapsedDataParallel_2_5443;
buffer_float_t SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_child0_5561_5702_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5611Post_CollapsedDataParallel_2_5452;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5605Post_CollapsedDataParallel_2_5449;
buffer_float_t SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_child1_5537_5698_join[2];
buffer_float_t SplitJoin89_Butterfly_Fiss_5688_5699_split[4];
buffer_float_t SplitJoin47_Butterfly_Fiss_5681_5706_split[2];
buffer_float_t SplitJoin4_Butterfly_Fiss_5672_5693_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5627Post_CollapsedDataParallel_2_5455;
buffer_float_t Pre_CollapsedDataParallel_1_5478WEIGHTED_ROUND_ROBIN_Splitter_5662;
buffer_float_t SplitJoin8_Butterfly_Fiss_5673_5695_split[4];
buffer_float_t SplitJoin43_Butterfly_Fiss_5680_5705_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5651Post_CollapsedDataParallel_2_5470;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5659Post_CollapsedDataParallel_2_5476;
buffer_float_t BitReverse_5219FloatPrinter_5220;
buffer_float_t SplitJoin57_Butterfly_Fiss_5683_5709_split[2];
buffer_float_t Pre_CollapsedDataParallel_1_5475WEIGHTED_ROUND_ROBIN_Splitter_5658;
buffer_float_t Pre_CollapsedDataParallel_1_5439WEIGHTED_ROUND_ROBIN_Splitter_5581;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5655Post_CollapsedDataParallel_2_5473;
buffer_float_t SplitJoin43_Butterfly_Fiss_5680_5705_join[2];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_5096_5491_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_Hier_5671_5692_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5567WEIGHTED_ROUND_ROBIN_Splitter_5568;
buffer_float_t SplitJoin39_Butterfly_Fiss_5679_5704_split[2];
buffer_float_t SplitJoin53_Butterfly_Fiss_5682_5708_split[2];
buffer_float_t SplitJoin65_Butterfly_Fiss_5685_5711_join[2];
buffer_float_t SplitJoin14_Butterfly_Fiss_5675_5703_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5663Post_CollapsedDataParallel_2_5479;
buffer_float_t SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_Hier_5676_5712_split[2];
buffer_float_t SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_child0_5532_5694_join[2];
buffer_float_t SplitJoin14_Butterfly_Fiss_5675_5703_split[2];
buffer_float_t SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child0_5677_5713_join[8];
buffer_float_t Post_CollapsedDataParallel_2_5440WEIGHTED_ROUND_ROBIN_Splitter_5483;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5579BitReverse_5219;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5633Post_CollapsedDataParallel_2_5458;
buffer_float_t Pre_CollapsedDataParallel_1_5445WEIGHTED_ROUND_ROBIN_Splitter_5616;
buffer_float_t SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child1_5678_5714_split[8];
buffer_float_t SplitJoin89_Butterfly_Fiss_5688_5699_join[4];
buffer_float_t SplitJoin72_Butterfly_Fiss_5686_5696_split[4];
buffer_float_t SplitJoin53_Butterfly_Fiss_5682_5708_join[2];
buffer_float_t SplitJoin47_Butterfly_Fiss_5681_5706_join[2];
buffer_float_t Pre_CollapsedDataParallel_1_5481WEIGHTED_ROUND_ROBIN_Splitter_5666;
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_5096_5491_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_Hier_5671_5692_join[2];
buffer_float_t Pre_CollapsedDataParallel_1_5442WEIGHTED_ROUND_ROBIN_Splitter_5594;
buffer_float_t SplitJoin39_Butterfly_Fiss_5679_5704_join[2];
buffer_float_t SplitJoin8_Butterfly_Fiss_5673_5695_join[4];
buffer_float_t Post_CollapsedDataParallel_2_5446WEIGHTED_ROUND_ROBIN_Splitter_5565;
buffer_float_t Pre_CollapsedDataParallel_1_5460WEIGHTED_ROUND_ROBIN_Splitter_5638;
buffer_float_t Pre_CollapsedDataParallel_1_5448WEIGHTED_ROUND_ROBIN_Splitter_5604;
buffer_float_t SplitJoin0_Butterfly_Fiss_5670_5691_join[11];
buffer_float_t SplitJoin61_Butterfly_Fiss_5684_5710_split[2];
buffer_float_t SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child0_5677_5713_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5639Post_CollapsedDataParallel_2_5461;
buffer_float_t Pre_CollapsedDataParallel_1_5451WEIGHTED_ROUND_ROBIN_Splitter_5610;
buffer_float_t Pre_CollapsedDataParallel_1_5466WEIGHTED_ROUND_ROBIN_Splitter_5646;
buffer_float_t Pre_CollapsedDataParallel_1_5457WEIGHTED_ROUND_ROBIN_Splitter_5632;
buffer_float_t SplitJoin85_Butterfly_Fiss_5687_5697_join[8];
buffer_float_t SplitJoin72_Butterfly_Fiss_5686_5696_join[4];
buffer_float_t SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_child1_5537_5698_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5573WEIGHTED_ROUND_ROBIN_Splitter_5574;
buffer_float_t SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_child1_5562_5707_split[4];
buffer_float_t Pre_CollapsedDataParallel_1_5472WEIGHTED_ROUND_ROBIN_Splitter_5654;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5667Post_CollapsedDataParallel_2_5482;
buffer_float_t SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child1_5678_5714_join[8];
buffer_float_t SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_Hier_5674_5701_join[2];
buffer_float_t SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_Hier_5674_5701_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5617Post_CollapsedDataParallel_2_5446;
buffer_float_t SplitJoin0_Butterfly_Fiss_5670_5691_split[11];
buffer_float_t Pre_CollapsedDataParallel_1_5469WEIGHTED_ROUND_ROBIN_Splitter_5650;
buffer_float_t SplitJoin95_Butterfly_Fiss_5689_5700_join[4];
buffer_float_t SplitJoin95_Butterfly_Fiss_5689_5700_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5643Post_CollapsedDataParallel_2_5464;
buffer_float_t SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_child1_5562_5707_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_5647Post_CollapsedDataParallel_2_5467;


FloatSource_5138_t FloatSource_5138_s;

void FloatSource(buffer_float_t *chanout) {
		push_float(&(*chanout), FloatSource_5138_s.A_re[FloatSource_5138_s.idx]) ; 
		push_float(&(*chanout), FloatSource_5138_s.A_im[FloatSource_5138_s.idx]) ; 
		FloatSource_5138_s.idx++ ; 
		if((FloatSource_5138_s.idx >= 32)) {
			FloatSource_5138_s.idx = 0 ; 
		}
	}


void FloatSource_5138() {
	FOR(uint32_t, __iter_steady_, 0, <, 352, __iter_steady_++)
		FloatSource(&(FloatSource_5138Pre_CollapsedDataParallel_1_5439));
	ENDFOR
}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
		int partialSum_k = 0;
 {
		FOR(int, _k, 0,  < , 16, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k;
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 32) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_5439() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(FloatSource_5138Pre_CollapsedDataParallel_1_5439), &(Pre_CollapsedDataParallel_1_5439WEIGHTED_ROUND_ROBIN_Splitter_5581));
	ENDFOR
}

void Butterfly(buffer_float_t *chanin, buffer_float_t *chanout) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&(*chanin)) ; 
		u_im = pop_float(&(*chanin)) ; 
		t_re = pop_float(&(*chanin)) ; 
		t_im = pop_float(&(*chanin)) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&(*chanout), u_re) ; 
		push_float(&(*chanout), u_im) ; 
		push_float(&(*chanout), t_re) ; 
		push_float(&(*chanout), t_im) ; 
	}


void Butterfly_5583() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_5670_5691_split[0]), &(SplitJoin0_Butterfly_Fiss_5670_5691_join[0]));
	ENDFOR
}

void Butterfly_5584() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_5670_5691_split[1]), &(SplitJoin0_Butterfly_Fiss_5670_5691_join[1]));
	ENDFOR
}

void Butterfly_5585() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_5670_5691_split[2]), &(SplitJoin0_Butterfly_Fiss_5670_5691_join[2]));
	ENDFOR
}

void Butterfly_5586() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_5670_5691_split[3]), &(SplitJoin0_Butterfly_Fiss_5670_5691_join[3]));
	ENDFOR
}

void Butterfly_5587() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_5670_5691_split[4]), &(SplitJoin0_Butterfly_Fiss_5670_5691_join[4]));
	ENDFOR
}

void Butterfly_5588() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_5670_5691_split[5]), &(SplitJoin0_Butterfly_Fiss_5670_5691_join[5]));
	ENDFOR
}

void Butterfly_5589() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_5670_5691_split[6]), &(SplitJoin0_Butterfly_Fiss_5670_5691_join[6]));
	ENDFOR
}

void Butterfly_5590() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_5670_5691_split[7]), &(SplitJoin0_Butterfly_Fiss_5670_5691_join[7]));
	ENDFOR
}

void Butterfly_5591() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_5670_5691_split[8]), &(SplitJoin0_Butterfly_Fiss_5670_5691_join[8]));
	ENDFOR
}

void Butterfly_5592() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_5670_5691_split[9]), &(SplitJoin0_Butterfly_Fiss_5670_5691_join[9]));
	ENDFOR
}

void Butterfly_5593() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_5670_5691_split[10]), &(SplitJoin0_Butterfly_Fiss_5670_5691_join[10]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5581() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 11, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin0_Butterfly_Fiss_5670_5691_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_5439WEIGHTED_ROUND_ROBIN_Splitter_5581));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5582() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 11, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5582Post_CollapsedDataParallel_2_5440, pop_float(&SplitJoin0_Butterfly_Fiss_5670_5691_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
		int kTimesWeights_i = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 16, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&(*chanout), peek_float(&(*chanin), (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_5440() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_5582Post_CollapsedDataParallel_2_5440), &(Post_CollapsedDataParallel_2_5440WEIGHTED_ROUND_ROBIN_Splitter_5483));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_5442() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_5096_5491_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_Hier_5671_5692_split[0]), &(Pre_CollapsedDataParallel_1_5442WEIGHTED_ROUND_ROBIN_Splitter_5594));
	ENDFOR
}

void Butterfly_5596() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_5672_5693_split[0]), &(SplitJoin4_Butterfly_Fiss_5672_5693_join[0]));
	ENDFOR
}

void Butterfly_5597() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_5672_5693_split[1]), &(SplitJoin4_Butterfly_Fiss_5672_5693_join[1]));
	ENDFOR
}

void Butterfly_5598() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_5672_5693_split[2]), &(SplitJoin4_Butterfly_Fiss_5672_5693_join[2]));
	ENDFOR
}

void Butterfly_5599() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_5672_5693_split[3]), &(SplitJoin4_Butterfly_Fiss_5672_5693_join[3]));
	ENDFOR
}

void Butterfly_5600() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_5672_5693_split[4]), &(SplitJoin4_Butterfly_Fiss_5672_5693_join[4]));
	ENDFOR
}

void Butterfly_5601() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_5672_5693_split[5]), &(SplitJoin4_Butterfly_Fiss_5672_5693_join[5]));
	ENDFOR
}

void Butterfly_5602() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_5672_5693_split[6]), &(SplitJoin4_Butterfly_Fiss_5672_5693_join[6]));
	ENDFOR
}

void Butterfly_5603() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_5672_5693_split[7]), &(SplitJoin4_Butterfly_Fiss_5672_5693_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5594() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin4_Butterfly_Fiss_5672_5693_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_5442WEIGHTED_ROUND_ROBIN_Splitter_5594));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5595() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5595Post_CollapsedDataParallel_2_5443, pop_float(&SplitJoin4_Butterfly_Fiss_5672_5693_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_5443() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_5595Post_CollapsedDataParallel_2_5443), &(Post_CollapsedDataParallel_2_5443WEIGHTED_ROUND_ROBIN_Splitter_5563));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_5448() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_child0_5532_5694_split[0]), &(Pre_CollapsedDataParallel_1_5448WEIGHTED_ROUND_ROBIN_Splitter_5604));
	ENDFOR
}

void Butterfly_5606() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin8_Butterfly_Fiss_5673_5695_split[0]), &(SplitJoin8_Butterfly_Fiss_5673_5695_join[0]));
	ENDFOR
}

void Butterfly_5607() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin8_Butterfly_Fiss_5673_5695_split[1]), &(SplitJoin8_Butterfly_Fiss_5673_5695_join[1]));
	ENDFOR
}

void Butterfly_5608() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin8_Butterfly_Fiss_5673_5695_split[2]), &(SplitJoin8_Butterfly_Fiss_5673_5695_join[2]));
	ENDFOR
}

void Butterfly_5609() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin8_Butterfly_Fiss_5673_5695_split[3]), &(SplitJoin8_Butterfly_Fiss_5673_5695_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5604() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin8_Butterfly_Fiss_5673_5695_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_5448WEIGHTED_ROUND_ROBIN_Splitter_5604));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5605() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5605Post_CollapsedDataParallel_2_5449, pop_float(&SplitJoin8_Butterfly_Fiss_5673_5695_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_5449() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_5605Post_CollapsedDataParallel_2_5449), &(SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_child0_5532_5694_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_5451() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_child0_5532_5694_split[1]), &(Pre_CollapsedDataParallel_1_5451WEIGHTED_ROUND_ROBIN_Splitter_5610));
	ENDFOR
}

void Butterfly_5612() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin72_Butterfly_Fiss_5686_5696_split[0]), &(SplitJoin72_Butterfly_Fiss_5686_5696_join[0]));
	ENDFOR
}

void Butterfly_5613() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin72_Butterfly_Fiss_5686_5696_split[1]), &(SplitJoin72_Butterfly_Fiss_5686_5696_join[1]));
	ENDFOR
}

void Butterfly_5614() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin72_Butterfly_Fiss_5686_5696_split[2]), &(SplitJoin72_Butterfly_Fiss_5686_5696_join[2]));
	ENDFOR
}

void Butterfly_5615() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin72_Butterfly_Fiss_5686_5696_split[3]), &(SplitJoin72_Butterfly_Fiss_5686_5696_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5610() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin72_Butterfly_Fiss_5686_5696_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_5451WEIGHTED_ROUND_ROBIN_Splitter_5610));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5611() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5611Post_CollapsedDataParallel_2_5452, pop_float(&SplitJoin72_Butterfly_Fiss_5686_5696_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_5452() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_5611Post_CollapsedDataParallel_2_5452), &(SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_child0_5532_5694_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5563() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_child0_5532_5694_split[0], pop_float(&Post_CollapsedDataParallel_2_5443WEIGHTED_ROUND_ROBIN_Splitter_5563));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_child0_5532_5694_split[1], pop_float(&Post_CollapsedDataParallel_2_5443WEIGHTED_ROUND_ROBIN_Splitter_5563));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5564() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_5096_5491_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_Hier_5671_5692_join[0], pop_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_child0_5532_5694_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_5096_5491_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_Hier_5671_5692_join[0], pop_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_child0_5532_5694_join[1]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_5445() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_5096_5491_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_Hier_5671_5692_split[1]), &(Pre_CollapsedDataParallel_1_5445WEIGHTED_ROUND_ROBIN_Splitter_5616));
	ENDFOR
}

void Butterfly_5618() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_5687_5697_split[0]), &(SplitJoin85_Butterfly_Fiss_5687_5697_join[0]));
	ENDFOR
}

void Butterfly_5619() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_5687_5697_split[1]), &(SplitJoin85_Butterfly_Fiss_5687_5697_join[1]));
	ENDFOR
}

void Butterfly_5620() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_5687_5697_split[2]), &(SplitJoin85_Butterfly_Fiss_5687_5697_join[2]));
	ENDFOR
}

void Butterfly_5621() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_5687_5697_split[3]), &(SplitJoin85_Butterfly_Fiss_5687_5697_join[3]));
	ENDFOR
}

void Butterfly_5622() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_5687_5697_split[4]), &(SplitJoin85_Butterfly_Fiss_5687_5697_join[4]));
	ENDFOR
}

void Butterfly_5623() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_5687_5697_split[5]), &(SplitJoin85_Butterfly_Fiss_5687_5697_join[5]));
	ENDFOR
}

void Butterfly_5624() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_5687_5697_split[6]), &(SplitJoin85_Butterfly_Fiss_5687_5697_join[6]));
	ENDFOR
}

void Butterfly_5625() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_5687_5697_split[7]), &(SplitJoin85_Butterfly_Fiss_5687_5697_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5616() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin85_Butterfly_Fiss_5687_5697_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_5445WEIGHTED_ROUND_ROBIN_Splitter_5616));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5617() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5617Post_CollapsedDataParallel_2_5446, pop_float(&SplitJoin85_Butterfly_Fiss_5687_5697_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_5446() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_5617Post_CollapsedDataParallel_2_5446), &(Post_CollapsedDataParallel_2_5446WEIGHTED_ROUND_ROBIN_Splitter_5565));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_5454() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_child1_5537_5698_split[0]), &(Pre_CollapsedDataParallel_1_5454WEIGHTED_ROUND_ROBIN_Splitter_5626));
	ENDFOR
}

void Butterfly_5628() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin89_Butterfly_Fiss_5688_5699_split[0]), &(SplitJoin89_Butterfly_Fiss_5688_5699_join[0]));
	ENDFOR
}

void Butterfly_5629() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin89_Butterfly_Fiss_5688_5699_split[1]), &(SplitJoin89_Butterfly_Fiss_5688_5699_join[1]));
	ENDFOR
}

void Butterfly_5630() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin89_Butterfly_Fiss_5688_5699_split[2]), &(SplitJoin89_Butterfly_Fiss_5688_5699_join[2]));
	ENDFOR
}

void Butterfly_5631() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin89_Butterfly_Fiss_5688_5699_split[3]), &(SplitJoin89_Butterfly_Fiss_5688_5699_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5626() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin89_Butterfly_Fiss_5688_5699_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_5454WEIGHTED_ROUND_ROBIN_Splitter_5626));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5627() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5627Post_CollapsedDataParallel_2_5455, pop_float(&SplitJoin89_Butterfly_Fiss_5688_5699_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_5455() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_5627Post_CollapsedDataParallel_2_5455), &(SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_child1_5537_5698_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_5457() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_child1_5537_5698_split[1]), &(Pre_CollapsedDataParallel_1_5457WEIGHTED_ROUND_ROBIN_Splitter_5632));
	ENDFOR
}

void Butterfly_5634() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin95_Butterfly_Fiss_5689_5700_split[0]), &(SplitJoin95_Butterfly_Fiss_5689_5700_join[0]));
	ENDFOR
}

void Butterfly_5635() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin95_Butterfly_Fiss_5689_5700_split[1]), &(SplitJoin95_Butterfly_Fiss_5689_5700_join[1]));
	ENDFOR
}

void Butterfly_5636() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin95_Butterfly_Fiss_5689_5700_split[2]), &(SplitJoin95_Butterfly_Fiss_5689_5700_join[2]));
	ENDFOR
}

void Butterfly_5637() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin95_Butterfly_Fiss_5689_5700_split[3]), &(SplitJoin95_Butterfly_Fiss_5689_5700_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5632() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin95_Butterfly_Fiss_5689_5700_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_5457WEIGHTED_ROUND_ROBIN_Splitter_5632));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5633() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5633Post_CollapsedDataParallel_2_5458, pop_float(&SplitJoin95_Butterfly_Fiss_5689_5700_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_5458() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_5633Post_CollapsedDataParallel_2_5458), &(SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_child1_5537_5698_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5565() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_child1_5537_5698_split[0], pop_float(&Post_CollapsedDataParallel_2_5446WEIGHTED_ROUND_ROBIN_Splitter_5565));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_child1_5537_5698_split[1], pop_float(&Post_CollapsedDataParallel_2_5446WEIGHTED_ROUND_ROBIN_Splitter_5565));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5566() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_5096_5491_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_Hier_5671_5692_join[1], pop_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_child1_5537_5698_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_5096_5491_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_Hier_5671_5692_join[1], pop_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_child1_5537_5698_join[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_5483() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_5096_5491_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_Hier_5671_5692_split[0], pop_float(&Post_CollapsedDataParallel_2_5440WEIGHTED_ROUND_ROBIN_Splitter_5483));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_5096_5491_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_Hier_5671_5692_split[1], pop_float(&Post_CollapsedDataParallel_2_5440WEIGHTED_ROUND_ROBIN_Splitter_5483));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5567() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5567WEIGHTED_ROUND_ROBIN_Splitter_5568, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_5096_5491_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_Hier_5671_5692_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5567WEIGHTED_ROUND_ROBIN_Splitter_5568, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_5096_5491_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_Hier_5671_5692_join[1]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_5460() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_child0_5561_5702_split[0]), &(Pre_CollapsedDataParallel_1_5460WEIGHTED_ROUND_ROBIN_Splitter_5638));
	ENDFOR
}

void Butterfly_5640() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin14_Butterfly_Fiss_5675_5703_split[0]), &(SplitJoin14_Butterfly_Fiss_5675_5703_join[0]));
	ENDFOR
}

void Butterfly_5641() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin14_Butterfly_Fiss_5675_5703_split[1]), &(SplitJoin14_Butterfly_Fiss_5675_5703_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5638() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin14_Butterfly_Fiss_5675_5703_split[0], pop_float(&Pre_CollapsedDataParallel_1_5460WEIGHTED_ROUND_ROBIN_Splitter_5638));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin14_Butterfly_Fiss_5675_5703_split[1], pop_float(&Pre_CollapsedDataParallel_1_5460WEIGHTED_ROUND_ROBIN_Splitter_5638));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5639() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5639Post_CollapsedDataParallel_2_5461, pop_float(&SplitJoin14_Butterfly_Fiss_5675_5703_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5639Post_CollapsedDataParallel_2_5461, pop_float(&SplitJoin14_Butterfly_Fiss_5675_5703_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_5461() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_5639Post_CollapsedDataParallel_2_5461), &(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_child0_5561_5702_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_5463() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_child0_5561_5702_split[1]), &(Pre_CollapsedDataParallel_1_5463WEIGHTED_ROUND_ROBIN_Splitter_5642));
	ENDFOR
}

void Butterfly_5644() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin39_Butterfly_Fiss_5679_5704_split[0]), &(SplitJoin39_Butterfly_Fiss_5679_5704_join[0]));
	ENDFOR
}

void Butterfly_5645() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin39_Butterfly_Fiss_5679_5704_split[1]), &(SplitJoin39_Butterfly_Fiss_5679_5704_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5642() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin39_Butterfly_Fiss_5679_5704_split[0], pop_float(&Pre_CollapsedDataParallel_1_5463WEIGHTED_ROUND_ROBIN_Splitter_5642));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin39_Butterfly_Fiss_5679_5704_split[1], pop_float(&Pre_CollapsedDataParallel_1_5463WEIGHTED_ROUND_ROBIN_Splitter_5642));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5643() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5643Post_CollapsedDataParallel_2_5464, pop_float(&SplitJoin39_Butterfly_Fiss_5679_5704_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5643Post_CollapsedDataParallel_2_5464, pop_float(&SplitJoin39_Butterfly_Fiss_5679_5704_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_5464() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_5643Post_CollapsedDataParallel_2_5464), &(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_child0_5561_5702_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_5466() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_child0_5561_5702_split[2]), &(Pre_CollapsedDataParallel_1_5466WEIGHTED_ROUND_ROBIN_Splitter_5646));
	ENDFOR
}

void Butterfly_5648() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin43_Butterfly_Fiss_5680_5705_split[0]), &(SplitJoin43_Butterfly_Fiss_5680_5705_join[0]));
	ENDFOR
}

void Butterfly_5649() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin43_Butterfly_Fiss_5680_5705_split[1]), &(SplitJoin43_Butterfly_Fiss_5680_5705_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5646() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin43_Butterfly_Fiss_5680_5705_split[0], pop_float(&Pre_CollapsedDataParallel_1_5466WEIGHTED_ROUND_ROBIN_Splitter_5646));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin43_Butterfly_Fiss_5680_5705_split[1], pop_float(&Pre_CollapsedDataParallel_1_5466WEIGHTED_ROUND_ROBIN_Splitter_5646));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5647() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5647Post_CollapsedDataParallel_2_5467, pop_float(&SplitJoin43_Butterfly_Fiss_5680_5705_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5647Post_CollapsedDataParallel_2_5467, pop_float(&SplitJoin43_Butterfly_Fiss_5680_5705_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_5467() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_5647Post_CollapsedDataParallel_2_5467), &(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_child0_5561_5702_join[2]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_5469() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_child0_5561_5702_split[3]), &(Pre_CollapsedDataParallel_1_5469WEIGHTED_ROUND_ROBIN_Splitter_5650));
	ENDFOR
}

void Butterfly_5652() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin47_Butterfly_Fiss_5681_5706_split[0]), &(SplitJoin47_Butterfly_Fiss_5681_5706_join[0]));
	ENDFOR
}

void Butterfly_5653() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin47_Butterfly_Fiss_5681_5706_split[1]), &(SplitJoin47_Butterfly_Fiss_5681_5706_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5650() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin47_Butterfly_Fiss_5681_5706_split[0], pop_float(&Pre_CollapsedDataParallel_1_5469WEIGHTED_ROUND_ROBIN_Splitter_5650));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin47_Butterfly_Fiss_5681_5706_split[1], pop_float(&Pre_CollapsedDataParallel_1_5469WEIGHTED_ROUND_ROBIN_Splitter_5650));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5651() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5651Post_CollapsedDataParallel_2_5470, pop_float(&SplitJoin47_Butterfly_Fiss_5681_5706_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5651Post_CollapsedDataParallel_2_5470, pop_float(&SplitJoin47_Butterfly_Fiss_5681_5706_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_5470() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_5651Post_CollapsedDataParallel_2_5470), &(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_child0_5561_5702_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5569() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_child0_5561_5702_split[__iter_dec_], pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_Hier_5674_5701_split[0]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5570() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_Hier_5674_5701_join[0], pop_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_child0_5561_5702_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_5472() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_child1_5562_5707_split[0]), &(Pre_CollapsedDataParallel_1_5472WEIGHTED_ROUND_ROBIN_Splitter_5654));
	ENDFOR
}

void Butterfly_5656() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin53_Butterfly_Fiss_5682_5708_split[0]), &(SplitJoin53_Butterfly_Fiss_5682_5708_join[0]));
	ENDFOR
}

void Butterfly_5657() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin53_Butterfly_Fiss_5682_5708_split[1]), &(SplitJoin53_Butterfly_Fiss_5682_5708_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5654() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin53_Butterfly_Fiss_5682_5708_split[0], pop_float(&Pre_CollapsedDataParallel_1_5472WEIGHTED_ROUND_ROBIN_Splitter_5654));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin53_Butterfly_Fiss_5682_5708_split[1], pop_float(&Pre_CollapsedDataParallel_1_5472WEIGHTED_ROUND_ROBIN_Splitter_5654));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5655() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5655Post_CollapsedDataParallel_2_5473, pop_float(&SplitJoin53_Butterfly_Fiss_5682_5708_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5655Post_CollapsedDataParallel_2_5473, pop_float(&SplitJoin53_Butterfly_Fiss_5682_5708_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_5473() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_5655Post_CollapsedDataParallel_2_5473), &(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_child1_5562_5707_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_5475() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_child1_5562_5707_split[1]), &(Pre_CollapsedDataParallel_1_5475WEIGHTED_ROUND_ROBIN_Splitter_5658));
	ENDFOR
}

void Butterfly_5660() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin57_Butterfly_Fiss_5683_5709_split[0]), &(SplitJoin57_Butterfly_Fiss_5683_5709_join[0]));
	ENDFOR
}

void Butterfly_5661() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin57_Butterfly_Fiss_5683_5709_split[1]), &(SplitJoin57_Butterfly_Fiss_5683_5709_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5658() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin57_Butterfly_Fiss_5683_5709_split[0], pop_float(&Pre_CollapsedDataParallel_1_5475WEIGHTED_ROUND_ROBIN_Splitter_5658));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin57_Butterfly_Fiss_5683_5709_split[1], pop_float(&Pre_CollapsedDataParallel_1_5475WEIGHTED_ROUND_ROBIN_Splitter_5658));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5659() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5659Post_CollapsedDataParallel_2_5476, pop_float(&SplitJoin57_Butterfly_Fiss_5683_5709_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5659Post_CollapsedDataParallel_2_5476, pop_float(&SplitJoin57_Butterfly_Fiss_5683_5709_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_5476() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_5659Post_CollapsedDataParallel_2_5476), &(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_child1_5562_5707_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_5478() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_child1_5562_5707_split[2]), &(Pre_CollapsedDataParallel_1_5478WEIGHTED_ROUND_ROBIN_Splitter_5662));
	ENDFOR
}

void Butterfly_5664() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin61_Butterfly_Fiss_5684_5710_split[0]), &(SplitJoin61_Butterfly_Fiss_5684_5710_join[0]));
	ENDFOR
}

void Butterfly_5665() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin61_Butterfly_Fiss_5684_5710_split[1]), &(SplitJoin61_Butterfly_Fiss_5684_5710_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5662() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin61_Butterfly_Fiss_5684_5710_split[0], pop_float(&Pre_CollapsedDataParallel_1_5478WEIGHTED_ROUND_ROBIN_Splitter_5662));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin61_Butterfly_Fiss_5684_5710_split[1], pop_float(&Pre_CollapsedDataParallel_1_5478WEIGHTED_ROUND_ROBIN_Splitter_5662));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5663() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5663Post_CollapsedDataParallel_2_5479, pop_float(&SplitJoin61_Butterfly_Fiss_5684_5710_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5663Post_CollapsedDataParallel_2_5479, pop_float(&SplitJoin61_Butterfly_Fiss_5684_5710_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_5479() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_5663Post_CollapsedDataParallel_2_5479), &(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_child1_5562_5707_join[2]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_5481() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_child1_5562_5707_split[3]), &(Pre_CollapsedDataParallel_1_5481WEIGHTED_ROUND_ROBIN_Splitter_5666));
	ENDFOR
}

void Butterfly_5668() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin65_Butterfly_Fiss_5685_5711_split[0]), &(SplitJoin65_Butterfly_Fiss_5685_5711_join[0]));
	ENDFOR
}

void Butterfly_5669() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin65_Butterfly_Fiss_5685_5711_split[1]), &(SplitJoin65_Butterfly_Fiss_5685_5711_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5666() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin65_Butterfly_Fiss_5685_5711_split[0], pop_float(&Pre_CollapsedDataParallel_1_5481WEIGHTED_ROUND_ROBIN_Splitter_5666));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin65_Butterfly_Fiss_5685_5711_split[1], pop_float(&Pre_CollapsedDataParallel_1_5481WEIGHTED_ROUND_ROBIN_Splitter_5666));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5667() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5667Post_CollapsedDataParallel_2_5482, pop_float(&SplitJoin65_Butterfly_Fiss_5685_5711_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5667Post_CollapsedDataParallel_2_5482, pop_float(&SplitJoin65_Butterfly_Fiss_5685_5711_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_5482() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_5667Post_CollapsedDataParallel_2_5482), &(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_child1_5562_5707_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5571() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_child1_5562_5707_split[__iter_dec_], pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_Hier_5674_5701_split[1]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5572() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_Hier_5674_5701_join[1], pop_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_child1_5562_5707_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_5568() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_Hier_5674_5701_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5567WEIGHTED_ROUND_ROBIN_Splitter_5568));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_Hier_5674_5701_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5567WEIGHTED_ROUND_ROBIN_Splitter_5568));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5573() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5573WEIGHTED_ROUND_ROBIN_Splitter_5574, pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_Hier_5674_5701_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5573WEIGHTED_ROUND_ROBIN_Splitter_5574, pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_Hier_5674_5701_join[1]));
		ENDFOR
	ENDFOR
}}

void Butterfly_5203() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child0_5677_5713_split[0]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child0_5677_5713_join[0]));
	ENDFOR
}

void Butterfly_5204() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child0_5677_5713_split[1]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child0_5677_5713_join[1]));
	ENDFOR
}

void Butterfly_5205() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child0_5677_5713_split[2]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child0_5677_5713_join[2]));
	ENDFOR
}

void Butterfly_5206() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child0_5677_5713_split[3]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child0_5677_5713_join[3]));
	ENDFOR
}

void Butterfly_5207() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child0_5677_5713_split[4]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child0_5677_5713_join[4]));
	ENDFOR
}

void Butterfly_5208() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child0_5677_5713_split[5]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child0_5677_5713_join[5]));
	ENDFOR
}

void Butterfly_5209() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child0_5677_5713_split[6]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child0_5677_5713_join[6]));
	ENDFOR
}

void Butterfly_5210() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child0_5677_5713_split[7]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child0_5677_5713_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5575() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child0_5677_5713_split[__iter_dec_], pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_Hier_5676_5712_split[0]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5576() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_Hier_5676_5712_join[0], pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child0_5677_5713_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Butterfly_5211() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child1_5678_5714_split[0]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child1_5678_5714_join[0]));
	ENDFOR
}

void Butterfly_5212() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child1_5678_5714_split[1]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child1_5678_5714_join[1]));
	ENDFOR
}

void Butterfly_5213() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child1_5678_5714_split[2]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child1_5678_5714_join[2]));
	ENDFOR
}

void Butterfly_5214() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child1_5678_5714_split[3]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child1_5678_5714_join[3]));
	ENDFOR
}

void Butterfly_5215() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child1_5678_5714_split[4]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child1_5678_5714_join[4]));
	ENDFOR
}

void Butterfly_5216() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child1_5678_5714_split[5]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child1_5678_5714_join[5]));
	ENDFOR
}

void Butterfly_5217() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child1_5678_5714_split[6]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child1_5678_5714_join[6]));
	ENDFOR
}

void Butterfly_5218() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child1_5678_5714_split[7]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child1_5678_5714_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5577() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child1_5678_5714_split[__iter_dec_], pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_Hier_5676_5712_split[1]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5578() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_Hier_5676_5712_join[1], pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child1_5678_5714_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_5574() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_Hier_5676_5712_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5573WEIGHTED_ROUND_ROBIN_Splitter_5574));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_Hier_5676_5712_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_5573WEIGHTED_ROUND_ROBIN_Splitter_5574));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5579() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5579BitReverse_5219, pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_Hier_5676_5712_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_5579BitReverse_5219, pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_Hier_5676_5712_join[1]));
		ENDFOR
	ENDFOR
}}

int BitReverse_5219_bitrev(int inp, int numbits) {
	int rev = 0;
	FOR(int, i, 0,  < , numbits, i++) {
		rev = ((rev * 2) | (inp & 1)) ; 
		inp = (inp / 2) ; 
	}
	ENDFOR
	return rev;
}
void BitReverse(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, i__conflict__0, 0,  < , 32, i__conflict__0++) {
			int br = 0;
			br = BitReverse_5219_bitrev(i__conflict__0, 5) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (2 * br))) ; 
			push_float(&(*chanout), peek_float(&(*chanin), ((2 * br) + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&(*chanin)) ; 
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void BitReverse_5219() {
	FOR(uint32_t, __iter_steady_, 0, <, 11, __iter_steady_++)
		BitReverse(&(WEIGHTED_ROUND_ROBIN_Joiner_5579BitReverse_5219), &(BitReverse_5219FloatPrinter_5220));
	ENDFOR
}

void FloatPrinter(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void FloatPrinter_5220() {
	FOR(uint32_t, __iter_steady_, 0, <, 352, __iter_steady_++)
		FloatPrinter(&(BitReverse_5219FloatPrinter_5220));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_float(&SplitJoin65_Butterfly_Fiss_5685_5711_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_float(&SplitJoin4_Butterfly_Fiss_5672_5693_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 4, __iter_init_2_++)
		init_buffer_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_child0_5561_5702_split[__iter_init_2_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_5443WEIGHTED_ROUND_ROBIN_Splitter_5563);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5582Post_CollapsedDataParallel_2_5440);
	FOR(int, __iter_init_3_, 0, <, 2, __iter_init_3_++)
		init_buffer_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_Hier_5676_5712_join[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&FloatSource_5138Pre_CollapsedDataParallel_1_5439);
	init_buffer_float(&Pre_CollapsedDataParallel_1_5454WEIGHTED_ROUND_ROBIN_Splitter_5626);
	init_buffer_float(&Pre_CollapsedDataParallel_1_5463WEIGHTED_ROUND_ROBIN_Splitter_5642);
	FOR(int, __iter_init_4_, 0, <, 2, __iter_init_4_++)
		init_buffer_float(&SplitJoin57_Butterfly_Fiss_5683_5709_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 2, __iter_init_5_++)
		init_buffer_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_child0_5532_5694_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_float(&SplitJoin85_Butterfly_Fiss_5687_5697_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 2, __iter_init_7_++)
		init_buffer_float(&SplitJoin61_Butterfly_Fiss_5684_5710_join[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5595Post_CollapsedDataParallel_2_5443);
	FOR(int, __iter_init_8_, 0, <, 4, __iter_init_8_++)
		init_buffer_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_child0_5561_5702_join[__iter_init_8_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5611Post_CollapsedDataParallel_2_5452);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5605Post_CollapsedDataParallel_2_5449);
	FOR(int, __iter_init_9_, 0, <, 2, __iter_init_9_++)
		init_buffer_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_child1_5537_5698_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 4, __iter_init_10_++)
		init_buffer_float(&SplitJoin89_Butterfly_Fiss_5688_5699_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 2, __iter_init_11_++)
		init_buffer_float(&SplitJoin47_Butterfly_Fiss_5681_5706_split[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 8, __iter_init_12_++)
		init_buffer_float(&SplitJoin4_Butterfly_Fiss_5672_5693_join[__iter_init_12_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5627Post_CollapsedDataParallel_2_5455);
	init_buffer_float(&Pre_CollapsedDataParallel_1_5478WEIGHTED_ROUND_ROBIN_Splitter_5662);
	FOR(int, __iter_init_13_, 0, <, 4, __iter_init_13_++)
		init_buffer_float(&SplitJoin8_Butterfly_Fiss_5673_5695_split[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_float(&SplitJoin43_Butterfly_Fiss_5680_5705_split[__iter_init_14_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5651Post_CollapsedDataParallel_2_5470);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5659Post_CollapsedDataParallel_2_5476);
	init_buffer_float(&BitReverse_5219FloatPrinter_5220);
	FOR(int, __iter_init_15_, 0, <, 2, __iter_init_15_++)
		init_buffer_float(&SplitJoin57_Butterfly_Fiss_5683_5709_split[__iter_init_15_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_5475WEIGHTED_ROUND_ROBIN_Splitter_5658);
	init_buffer_float(&Pre_CollapsedDataParallel_1_5439WEIGHTED_ROUND_ROBIN_Splitter_5581);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5655Post_CollapsedDataParallel_2_5473);
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_float(&SplitJoin43_Butterfly_Fiss_5680_5705_join[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_5096_5491_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_Hier_5671_5692_split[__iter_init_17_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5567WEIGHTED_ROUND_ROBIN_Splitter_5568);
	FOR(int, __iter_init_18_, 0, <, 2, __iter_init_18_++)
		init_buffer_float(&SplitJoin39_Butterfly_Fiss_5679_5704_split[__iter_init_18_]);
	ENDFOR
	FOR(int, __iter_init_19_, 0, <, 2, __iter_init_19_++)
		init_buffer_float(&SplitJoin53_Butterfly_Fiss_5682_5708_split[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 2, __iter_init_20_++)
		init_buffer_float(&SplitJoin65_Butterfly_Fiss_5685_5711_join[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 2, __iter_init_21_++)
		init_buffer_float(&SplitJoin14_Butterfly_Fiss_5675_5703_join[__iter_init_21_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5663Post_CollapsedDataParallel_2_5479);
	FOR(int, __iter_init_22_, 0, <, 2, __iter_init_22_++)
		init_buffer_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_Hier_5676_5712_split[__iter_init_22_]);
	ENDFOR
	FOR(int, __iter_init_23_, 0, <, 2, __iter_init_23_++)
		init_buffer_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_child0_5532_5694_join[__iter_init_23_]);
	ENDFOR
	FOR(int, __iter_init_24_, 0, <, 2, __iter_init_24_++)
		init_buffer_float(&SplitJoin14_Butterfly_Fiss_5675_5703_split[__iter_init_24_]);
	ENDFOR
	FOR(int, __iter_init_25_, 0, <, 8, __iter_init_25_++)
		init_buffer_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child0_5677_5713_join[__iter_init_25_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_5440WEIGHTED_ROUND_ROBIN_Splitter_5483);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5579BitReverse_5219);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5633Post_CollapsedDataParallel_2_5458);
	init_buffer_float(&Pre_CollapsedDataParallel_1_5445WEIGHTED_ROUND_ROBIN_Splitter_5616);
	FOR(int, __iter_init_26_, 0, <, 8, __iter_init_26_++)
		init_buffer_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child1_5678_5714_split[__iter_init_26_]);
	ENDFOR
	FOR(int, __iter_init_27_, 0, <, 4, __iter_init_27_++)
		init_buffer_float(&SplitJoin89_Butterfly_Fiss_5688_5699_join[__iter_init_27_]);
	ENDFOR
	FOR(int, __iter_init_28_, 0, <, 4, __iter_init_28_++)
		init_buffer_float(&SplitJoin72_Butterfly_Fiss_5686_5696_split[__iter_init_28_]);
	ENDFOR
	FOR(int, __iter_init_29_, 0, <, 2, __iter_init_29_++)
		init_buffer_float(&SplitJoin53_Butterfly_Fiss_5682_5708_join[__iter_init_29_]);
	ENDFOR
	FOR(int, __iter_init_30_, 0, <, 2, __iter_init_30_++)
		init_buffer_float(&SplitJoin47_Butterfly_Fiss_5681_5706_join[__iter_init_30_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_5481WEIGHTED_ROUND_ROBIN_Splitter_5666);
	FOR(int, __iter_init_31_, 0, <, 2, __iter_init_31_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_5096_5491_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_Hier_5671_5692_join[__iter_init_31_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_5442WEIGHTED_ROUND_ROBIN_Splitter_5594);
	FOR(int, __iter_init_32_, 0, <, 2, __iter_init_32_++)
		init_buffer_float(&SplitJoin39_Butterfly_Fiss_5679_5704_join[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 4, __iter_init_33_++)
		init_buffer_float(&SplitJoin8_Butterfly_Fiss_5673_5695_join[__iter_init_33_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_5446WEIGHTED_ROUND_ROBIN_Splitter_5565);
	init_buffer_float(&Pre_CollapsedDataParallel_1_5460WEIGHTED_ROUND_ROBIN_Splitter_5638);
	init_buffer_float(&Pre_CollapsedDataParallel_1_5448WEIGHTED_ROUND_ROBIN_Splitter_5604);
	FOR(int, __iter_init_34_, 0, <, 11, __iter_init_34_++)
		init_buffer_float(&SplitJoin0_Butterfly_Fiss_5670_5691_join[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 2, __iter_init_35_++)
		init_buffer_float(&SplitJoin61_Butterfly_Fiss_5684_5710_split[__iter_init_35_]);
	ENDFOR
	FOR(int, __iter_init_36_, 0, <, 8, __iter_init_36_++)
		init_buffer_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child0_5677_5713_split[__iter_init_36_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5639Post_CollapsedDataParallel_2_5461);
	init_buffer_float(&Pre_CollapsedDataParallel_1_5451WEIGHTED_ROUND_ROBIN_Splitter_5610);
	init_buffer_float(&Pre_CollapsedDataParallel_1_5466WEIGHTED_ROUND_ROBIN_Splitter_5646);
	init_buffer_float(&Pre_CollapsedDataParallel_1_5457WEIGHTED_ROUND_ROBIN_Splitter_5632);
	FOR(int, __iter_init_37_, 0, <, 8, __iter_init_37_++)
		init_buffer_float(&SplitJoin85_Butterfly_Fiss_5687_5697_join[__iter_init_37_]);
	ENDFOR
	FOR(int, __iter_init_38_, 0, <, 4, __iter_init_38_++)
		init_buffer_float(&SplitJoin72_Butterfly_Fiss_5686_5696_join[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 2, __iter_init_39_++)
		init_buffer_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_5105_5493_child1_5537_5698_split[__iter_init_39_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5573WEIGHTED_ROUND_ROBIN_Splitter_5574);
	FOR(int, __iter_init_40_, 0, <, 4, __iter_init_40_++)
		init_buffer_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_child1_5562_5707_split[__iter_init_40_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_5472WEIGHTED_ROUND_ROBIN_Splitter_5654);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5667Post_CollapsedDataParallel_2_5482);
	FOR(int, __iter_init_41_, 0, <, 8, __iter_init_41_++)
		init_buffer_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_5136_5497_Hier_child1_5678_5714_join[__iter_init_41_]);
	ENDFOR
	FOR(int, __iter_init_42_, 0, <, 2, __iter_init_42_++)
		init_buffer_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_Hier_5674_5701_join[__iter_init_42_]);
	ENDFOR
	FOR(int, __iter_init_43_, 0, <, 2, __iter_init_43_++)
		init_buffer_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_Hier_5674_5701_split[__iter_init_43_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5617Post_CollapsedDataParallel_2_5446);
	FOR(int, __iter_init_44_, 0, <, 11, __iter_init_44_++)
		init_buffer_float(&SplitJoin0_Butterfly_Fiss_5670_5691_split[__iter_init_44_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_5469WEIGHTED_ROUND_ROBIN_Splitter_5650);
	FOR(int, __iter_init_45_, 0, <, 4, __iter_init_45_++)
		init_buffer_float(&SplitJoin95_Butterfly_Fiss_5689_5700_join[__iter_init_45_]);
	ENDFOR
	FOR(int, __iter_init_46_, 0, <, 4, __iter_init_46_++)
		init_buffer_float(&SplitJoin95_Butterfly_Fiss_5689_5700_split[__iter_init_46_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5643Post_CollapsedDataParallel_2_5464);
	FOR(int, __iter_init_47_, 0, <, 4, __iter_init_47_++)
		init_buffer_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_5118_5495_Hier_child1_5562_5707_join[__iter_init_47_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_5647Post_CollapsedDataParallel_2_5467);
// --- init: FloatSource_5138
	 {
	FOR(int, i, 0,  < , 32, i++) {
		FloatSource_5138_s.A_re[i] = 0.0 ; 
		FloatSource_5138_s.A_im[i] = 0.0 ; 
	}
	ENDFOR
	FloatSource_5138_s.A_re[1] = 1.0 ; 
	FloatSource_5138_s.idx = 0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FloatSource_5138();
		Pre_CollapsedDataParallel_1_5439();
		WEIGHTED_ROUND_ROBIN_Splitter_5581();
			Butterfly_5583();
			Butterfly_5584();
			Butterfly_5585();
			Butterfly_5586();
			Butterfly_5587();
			Butterfly_5588();
			Butterfly_5589();
			Butterfly_5590();
			Butterfly_5591();
			Butterfly_5592();
			Butterfly_5593();
		WEIGHTED_ROUND_ROBIN_Joiner_5582();
		Post_CollapsedDataParallel_2_5440();
		WEIGHTED_ROUND_ROBIN_Splitter_5483();
			Pre_CollapsedDataParallel_1_5442();
			WEIGHTED_ROUND_ROBIN_Splitter_5594();
				Butterfly_5596();
				Butterfly_5597();
				Butterfly_5598();
				Butterfly_5599();
				Butterfly_5600();
				Butterfly_5601();
				Butterfly_5602();
				Butterfly_5603();
			WEIGHTED_ROUND_ROBIN_Joiner_5595();
			Post_CollapsedDataParallel_2_5443();
			WEIGHTED_ROUND_ROBIN_Splitter_5563();
				Pre_CollapsedDataParallel_1_5448();
				WEIGHTED_ROUND_ROBIN_Splitter_5604();
					Butterfly_5606();
					Butterfly_5607();
					Butterfly_5608();
					Butterfly_5609();
				WEIGHTED_ROUND_ROBIN_Joiner_5605();
				Post_CollapsedDataParallel_2_5449();
				Pre_CollapsedDataParallel_1_5451();
				WEIGHTED_ROUND_ROBIN_Splitter_5610();
					Butterfly_5612();
					Butterfly_5613();
					Butterfly_5614();
					Butterfly_5615();
				WEIGHTED_ROUND_ROBIN_Joiner_5611();
				Post_CollapsedDataParallel_2_5452();
			WEIGHTED_ROUND_ROBIN_Joiner_5564();
			Pre_CollapsedDataParallel_1_5445();
			WEIGHTED_ROUND_ROBIN_Splitter_5616();
				Butterfly_5618();
				Butterfly_5619();
				Butterfly_5620();
				Butterfly_5621();
				Butterfly_5622();
				Butterfly_5623();
				Butterfly_5624();
				Butterfly_5625();
			WEIGHTED_ROUND_ROBIN_Joiner_5617();
			Post_CollapsedDataParallel_2_5446();
			WEIGHTED_ROUND_ROBIN_Splitter_5565();
				Pre_CollapsedDataParallel_1_5454();
				WEIGHTED_ROUND_ROBIN_Splitter_5626();
					Butterfly_5628();
					Butterfly_5629();
					Butterfly_5630();
					Butterfly_5631();
				WEIGHTED_ROUND_ROBIN_Joiner_5627();
				Post_CollapsedDataParallel_2_5455();
				Pre_CollapsedDataParallel_1_5457();
				WEIGHTED_ROUND_ROBIN_Splitter_5632();
					Butterfly_5634();
					Butterfly_5635();
					Butterfly_5636();
					Butterfly_5637();
				WEIGHTED_ROUND_ROBIN_Joiner_5633();
				Post_CollapsedDataParallel_2_5458();
			WEIGHTED_ROUND_ROBIN_Joiner_5566();
		WEIGHTED_ROUND_ROBIN_Joiner_5567();
		WEIGHTED_ROUND_ROBIN_Splitter_5568();
			WEIGHTED_ROUND_ROBIN_Splitter_5569();
				Pre_CollapsedDataParallel_1_5460();
				WEIGHTED_ROUND_ROBIN_Splitter_5638();
					Butterfly_5640();
					Butterfly_5641();
				WEIGHTED_ROUND_ROBIN_Joiner_5639();
				Post_CollapsedDataParallel_2_5461();
				Pre_CollapsedDataParallel_1_5463();
				WEIGHTED_ROUND_ROBIN_Splitter_5642();
					Butterfly_5644();
					Butterfly_5645();
				WEIGHTED_ROUND_ROBIN_Joiner_5643();
				Post_CollapsedDataParallel_2_5464();
				Pre_CollapsedDataParallel_1_5466();
				WEIGHTED_ROUND_ROBIN_Splitter_5646();
					Butterfly_5648();
					Butterfly_5649();
				WEIGHTED_ROUND_ROBIN_Joiner_5647();
				Post_CollapsedDataParallel_2_5467();
				Pre_CollapsedDataParallel_1_5469();
				WEIGHTED_ROUND_ROBIN_Splitter_5650();
					Butterfly_5652();
					Butterfly_5653();
				WEIGHTED_ROUND_ROBIN_Joiner_5651();
				Post_CollapsedDataParallel_2_5470();
			WEIGHTED_ROUND_ROBIN_Joiner_5570();
			WEIGHTED_ROUND_ROBIN_Splitter_5571();
				Pre_CollapsedDataParallel_1_5472();
				WEIGHTED_ROUND_ROBIN_Splitter_5654();
					Butterfly_5656();
					Butterfly_5657();
				WEIGHTED_ROUND_ROBIN_Joiner_5655();
				Post_CollapsedDataParallel_2_5473();
				Pre_CollapsedDataParallel_1_5475();
				WEIGHTED_ROUND_ROBIN_Splitter_5658();
					Butterfly_5660();
					Butterfly_5661();
				WEIGHTED_ROUND_ROBIN_Joiner_5659();
				Post_CollapsedDataParallel_2_5476();
				Pre_CollapsedDataParallel_1_5478();
				WEIGHTED_ROUND_ROBIN_Splitter_5662();
					Butterfly_5664();
					Butterfly_5665();
				WEIGHTED_ROUND_ROBIN_Joiner_5663();
				Post_CollapsedDataParallel_2_5479();
				Pre_CollapsedDataParallel_1_5481();
				WEIGHTED_ROUND_ROBIN_Splitter_5666();
					Butterfly_5668();
					Butterfly_5669();
				WEIGHTED_ROUND_ROBIN_Joiner_5667();
				Post_CollapsedDataParallel_2_5482();
			WEIGHTED_ROUND_ROBIN_Joiner_5572();
		WEIGHTED_ROUND_ROBIN_Joiner_5573();
		WEIGHTED_ROUND_ROBIN_Splitter_5574();
			WEIGHTED_ROUND_ROBIN_Splitter_5575();
				Butterfly_5203();
				Butterfly_5204();
				Butterfly_5205();
				Butterfly_5206();
				Butterfly_5207();
				Butterfly_5208();
				Butterfly_5209();
				Butterfly_5210();
			WEIGHTED_ROUND_ROBIN_Joiner_5576();
			WEIGHTED_ROUND_ROBIN_Splitter_5577();
				Butterfly_5211();
				Butterfly_5212();
				Butterfly_5213();
				Butterfly_5214();
				Butterfly_5215();
				Butterfly_5216();
				Butterfly_5217();
				Butterfly_5218();
			WEIGHTED_ROUND_ROBIN_Joiner_5578();
		WEIGHTED_ROUND_ROBIN_Joiner_5579();
		BitReverse_5219();
		FloatPrinter_5220();
	ENDFOR
	return EXIT_SUCCESS;
}
