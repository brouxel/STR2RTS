#include "PEG21-FFT6.h"

buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2683WEIGHTED_ROUND_ROBIN_Splitter_2700;
buffer_complex_t CombineDFT_2618CPrinter_2619;
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_2721_2731_join[4];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_2725_2735_split[16];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_2728_2738_join[2];
buffer_complex_t SplitJoin8_CombineDFT_Fiss_2724_2734_join[21];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_2720_2730_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2711WEIGHTED_ROUND_ROBIN_Splitter_2716;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_2724_2734_split[21];
buffer_complex_t SplitJoin12_CombineDFT_Fiss_2726_2736_join[8];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2717CombineDFT_2618;
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_2723_2733_join[16];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2632WEIGHTED_ROUND_ROBIN_Splitter_2641;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2642WEIGHTED_ROUND_ROBIN_Splitter_2659;
buffer_complex_t SplitJoin14_CombineDFT_Fiss_2727_2737_split[4];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_2722_2732_split[8];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_2725_2735_join[16];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_2728_2738_split[2];
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_2723_2733_split[16];
buffer_complex_t SplitJoin12_CombineDFT_Fiss_2726_2736_split[8];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_2722_2732_join[8];
buffer_complex_t FFTTestSource_2607FFTReorderSimple_2608;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2701WEIGHTED_ROUND_ROBIN_Splitter_2710;
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_2720_2730_join[2];
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_2721_2731_split[4];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_2727_2737_join[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2626WEIGHTED_ROUND_ROBIN_Splitter_2631;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2622WEIGHTED_ROUND_ROBIN_Splitter_2625;
buffer_complex_t FFTReorderSimple_2608WEIGHTED_ROUND_ROBIN_Splitter_2621;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_2660WEIGHTED_ROUND_ROBIN_Splitter_2682;


CombineDFT_2661_t CombineDFT_2661_s;
CombineDFT_2661_t CombineDFT_2662_s;
CombineDFT_2661_t CombineDFT_2663_s;
CombineDFT_2661_t CombineDFT_2664_s;
CombineDFT_2661_t CombineDFT_2665_s;
CombineDFT_2661_t CombineDFT_2666_s;
CombineDFT_2661_t CombineDFT_2667_s;
CombineDFT_2661_t CombineDFT_2668_s;
CombineDFT_2661_t CombineDFT_2669_s;
CombineDFT_2661_t CombineDFT_2670_s;
CombineDFT_2661_t CombineDFT_2671_s;
CombineDFT_2661_t CombineDFT_2672_s;
CombineDFT_2661_t CombineDFT_2673_s;
CombineDFT_2661_t CombineDFT_2674_s;
CombineDFT_2661_t CombineDFT_2675_s;
CombineDFT_2661_t CombineDFT_2676_s;
CombineDFT_2661_t CombineDFT_2677_s;
CombineDFT_2661_t CombineDFT_2678_s;
CombineDFT_2661_t CombineDFT_2679_s;
CombineDFT_2661_t CombineDFT_2680_s;
CombineDFT_2661_t CombineDFT_2681_s;
CombineDFT_2661_t CombineDFT_2684_s;
CombineDFT_2661_t CombineDFT_2685_s;
CombineDFT_2661_t CombineDFT_2686_s;
CombineDFT_2661_t CombineDFT_2687_s;
CombineDFT_2661_t CombineDFT_2688_s;
CombineDFT_2661_t CombineDFT_2689_s;
CombineDFT_2661_t CombineDFT_2690_s;
CombineDFT_2661_t CombineDFT_2691_s;
CombineDFT_2661_t CombineDFT_2692_s;
CombineDFT_2661_t CombineDFT_2693_s;
CombineDFT_2661_t CombineDFT_2694_s;
CombineDFT_2661_t CombineDFT_2695_s;
CombineDFT_2661_t CombineDFT_2696_s;
CombineDFT_2661_t CombineDFT_2697_s;
CombineDFT_2661_t CombineDFT_2698_s;
CombineDFT_2661_t CombineDFT_2699_s;
CombineDFT_2661_t CombineDFT_2702_s;
CombineDFT_2661_t CombineDFT_2703_s;
CombineDFT_2661_t CombineDFT_2704_s;
CombineDFT_2661_t CombineDFT_2705_s;
CombineDFT_2661_t CombineDFT_2706_s;
CombineDFT_2661_t CombineDFT_2707_s;
CombineDFT_2661_t CombineDFT_2708_s;
CombineDFT_2661_t CombineDFT_2709_s;
CombineDFT_2661_t CombineDFT_2712_s;
CombineDFT_2661_t CombineDFT_2713_s;
CombineDFT_2661_t CombineDFT_2714_s;
CombineDFT_2661_t CombineDFT_2715_s;
CombineDFT_2661_t CombineDFT_2718_s;
CombineDFT_2661_t CombineDFT_2719_s;
CombineDFT_2661_t CombineDFT_2618_s;

void FFTTestSource(buffer_complex_t *chanout) {
		complex_t c1;
		complex_t zero;
		c1.real = 1.0 ; 
		c1.imag = 0.0 ; 
		zero.real = 0.0 ; 
		zero.imag = 0.0 ; 
		push_complex(&(*chanout), zero) ; 
		push_complex(&(*chanout), c1) ; 
		FOR(int, i, 0,  < , 62, i++) {
			push_complex(&(*chanout), zero) ; 
		}
		ENDFOR
	}


void FFTTestSource_2607() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTTestSource(&(FFTTestSource_2607FFTReorderSimple_2608));
	ENDFOR
}

void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 64, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&(*chanin)) ; 
		}
		ENDFOR
	}


void FFTReorderSimple_2608() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(FFTTestSource_2607FFTReorderSimple_2608), &(FFTReorderSimple_2608WEIGHTED_ROUND_ROBIN_Splitter_2621));
	ENDFOR
}

void FFTReorderSimple_2623() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_2720_2730_split[0]), &(SplitJoin0_FFTReorderSimple_Fiss_2720_2730_join[0]));
	ENDFOR
}

void FFTReorderSimple_2624() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_2720_2730_split[1]), &(SplitJoin0_FFTReorderSimple_Fiss_2720_2730_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2621() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_2720_2730_split[0], pop_complex(&FFTReorderSimple_2608WEIGHTED_ROUND_ROBIN_Splitter_2621));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_2720_2730_split[1], pop_complex(&FFTReorderSimple_2608WEIGHTED_ROUND_ROBIN_Splitter_2621));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2622() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2622WEIGHTED_ROUND_ROBIN_Splitter_2625, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_2720_2730_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2622WEIGHTED_ROUND_ROBIN_Splitter_2625, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_2720_2730_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_2627() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_2721_2731_split[0]), &(SplitJoin2_FFTReorderSimple_Fiss_2721_2731_join[0]));
	ENDFOR
}

void FFTReorderSimple_2628() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_2721_2731_split[1]), &(SplitJoin2_FFTReorderSimple_Fiss_2721_2731_join[1]));
	ENDFOR
}

void FFTReorderSimple_2629() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_2721_2731_split[2]), &(SplitJoin2_FFTReorderSimple_Fiss_2721_2731_join[2]));
	ENDFOR
}

void FFTReorderSimple_2630() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_2721_2731_split[3]), &(SplitJoin2_FFTReorderSimple_Fiss_2721_2731_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2625() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin2_FFTReorderSimple_Fiss_2721_2731_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2622WEIGHTED_ROUND_ROBIN_Splitter_2625));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2626() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2626WEIGHTED_ROUND_ROBIN_Splitter_2631, pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_2721_2731_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_2633() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_2722_2732_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_2722_2732_join[0]));
	ENDFOR
}

void FFTReorderSimple_2634() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_2722_2732_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_2722_2732_join[1]));
	ENDFOR
}

void FFTReorderSimple_2635() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_2722_2732_split[2]), &(SplitJoin4_FFTReorderSimple_Fiss_2722_2732_join[2]));
	ENDFOR
}

void FFTReorderSimple_2636() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_2722_2732_split[3]), &(SplitJoin4_FFTReorderSimple_Fiss_2722_2732_join[3]));
	ENDFOR
}

void FFTReorderSimple_2637() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_2722_2732_split[4]), &(SplitJoin4_FFTReorderSimple_Fiss_2722_2732_join[4]));
	ENDFOR
}

void FFTReorderSimple_2638() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_2722_2732_split[5]), &(SplitJoin4_FFTReorderSimple_Fiss_2722_2732_join[5]));
	ENDFOR
}

void FFTReorderSimple_2639() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_2722_2732_split[6]), &(SplitJoin4_FFTReorderSimple_Fiss_2722_2732_join[6]));
	ENDFOR
}

void FFTReorderSimple_2640() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_2722_2732_split[7]), &(SplitJoin4_FFTReorderSimple_Fiss_2722_2732_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2631() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin4_FFTReorderSimple_Fiss_2722_2732_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2626WEIGHTED_ROUND_ROBIN_Splitter_2631));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2632() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2632WEIGHTED_ROUND_ROBIN_Splitter_2641, pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_2722_2732_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_2643() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_join[0]));
	ENDFOR
}

void FFTReorderSimple_2644() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_join[1]));
	ENDFOR
}

void FFTReorderSimple_2645() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_join[2]));
	ENDFOR
}

void FFTReorderSimple_2646() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_join[3]));
	ENDFOR
}

void FFTReorderSimple_2647() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_split[4]), &(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_join[4]));
	ENDFOR
}

void FFTReorderSimple_2648() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_split[5]), &(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_join[5]));
	ENDFOR
}

void FFTReorderSimple_2649() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_split[6]), &(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_join[6]));
	ENDFOR
}

void FFTReorderSimple_2650() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_split[7]), &(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_join[7]));
	ENDFOR
}

void FFTReorderSimple_2651() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_split[8]), &(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_join[8]));
	ENDFOR
}

void FFTReorderSimple_2652() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_split[9]), &(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_join[9]));
	ENDFOR
}

void FFTReorderSimple_2653() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_split[10]), &(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_join[10]));
	ENDFOR
}

void FFTReorderSimple_2654() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_split[11]), &(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_join[11]));
	ENDFOR
}

void FFTReorderSimple_2655() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_split[12]), &(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_join[12]));
	ENDFOR
}

void FFTReorderSimple_2656() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_split[13]), &(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_join[13]));
	ENDFOR
}

void FFTReorderSimple_2657() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_split[14]), &(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_join[14]));
	ENDFOR
}

void FFTReorderSimple_2658() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_split[15]), &(SplitJoin6_FFTReorderSimple_Fiss_2723_2733_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2641() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin6_FFTReorderSimple_Fiss_2723_2733_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2632WEIGHTED_ROUND_ROBIN_Splitter_2641));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2642() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2642WEIGHTED_ROUND_ROBIN_Splitter_2659, pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_2723_2733_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&(*chanin), (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_2661_s.wn.real) - (w.imag * CombineDFT_2661_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_2661_s.wn.imag) + (w.imag * CombineDFT_2661_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&(*chanin)) ; 
			push_complex(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_2661() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2724_2734_split[0]), &(SplitJoin8_CombineDFT_Fiss_2724_2734_join[0]));
	ENDFOR
}

void CombineDFT_2662() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2724_2734_split[1]), &(SplitJoin8_CombineDFT_Fiss_2724_2734_join[1]));
	ENDFOR
}

void CombineDFT_2663() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2724_2734_split[2]), &(SplitJoin8_CombineDFT_Fiss_2724_2734_join[2]));
	ENDFOR
}

void CombineDFT_2664() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2724_2734_split[3]), &(SplitJoin8_CombineDFT_Fiss_2724_2734_join[3]));
	ENDFOR
}

void CombineDFT_2665() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2724_2734_split[4]), &(SplitJoin8_CombineDFT_Fiss_2724_2734_join[4]));
	ENDFOR
}

void CombineDFT_2666() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2724_2734_split[5]), &(SplitJoin8_CombineDFT_Fiss_2724_2734_join[5]));
	ENDFOR
}

void CombineDFT_2667() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2724_2734_split[6]), &(SplitJoin8_CombineDFT_Fiss_2724_2734_join[6]));
	ENDFOR
}

void CombineDFT_2668() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2724_2734_split[7]), &(SplitJoin8_CombineDFT_Fiss_2724_2734_join[7]));
	ENDFOR
}

void CombineDFT_2669() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2724_2734_split[8]), &(SplitJoin8_CombineDFT_Fiss_2724_2734_join[8]));
	ENDFOR
}

void CombineDFT_2670() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2724_2734_split[9]), &(SplitJoin8_CombineDFT_Fiss_2724_2734_join[9]));
	ENDFOR
}

void CombineDFT_2671() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2724_2734_split[10]), &(SplitJoin8_CombineDFT_Fiss_2724_2734_join[10]));
	ENDFOR
}

void CombineDFT_2672() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2724_2734_split[11]), &(SplitJoin8_CombineDFT_Fiss_2724_2734_join[11]));
	ENDFOR
}

void CombineDFT_2673() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2724_2734_split[12]), &(SplitJoin8_CombineDFT_Fiss_2724_2734_join[12]));
	ENDFOR
}

void CombineDFT_2674() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2724_2734_split[13]), &(SplitJoin8_CombineDFT_Fiss_2724_2734_join[13]));
	ENDFOR
}

void CombineDFT_2675() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2724_2734_split[14]), &(SplitJoin8_CombineDFT_Fiss_2724_2734_join[14]));
	ENDFOR
}

void CombineDFT_2676() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2724_2734_split[15]), &(SplitJoin8_CombineDFT_Fiss_2724_2734_join[15]));
	ENDFOR
}

void CombineDFT_2677() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2724_2734_split[16]), &(SplitJoin8_CombineDFT_Fiss_2724_2734_join[16]));
	ENDFOR
}

void CombineDFT_2678() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2724_2734_split[17]), &(SplitJoin8_CombineDFT_Fiss_2724_2734_join[17]));
	ENDFOR
}

void CombineDFT_2679() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2724_2734_split[18]), &(SplitJoin8_CombineDFT_Fiss_2724_2734_join[18]));
	ENDFOR
}

void CombineDFT_2680() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2724_2734_split[19]), &(SplitJoin8_CombineDFT_Fiss_2724_2734_join[19]));
	ENDFOR
}

void CombineDFT_2681() {
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_2724_2734_split[20]), &(SplitJoin8_CombineDFT_Fiss_2724_2734_join[20]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2659() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 21, __iter_++)
			push_complex(&SplitJoin8_CombineDFT_Fiss_2724_2734_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2642WEIGHTED_ROUND_ROBIN_Splitter_2659));
			push_complex(&SplitJoin8_CombineDFT_Fiss_2724_2734_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2642WEIGHTED_ROUND_ROBIN_Splitter_2659));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2660() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 21, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2660WEIGHTED_ROUND_ROBIN_Splitter_2682, pop_complex(&SplitJoin8_CombineDFT_Fiss_2724_2734_join[__iter_]));
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2660WEIGHTED_ROUND_ROBIN_Splitter_2682, pop_complex(&SplitJoin8_CombineDFT_Fiss_2724_2734_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_2684() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2725_2735_split[0]), &(SplitJoin10_CombineDFT_Fiss_2725_2735_join[0]));
	ENDFOR
}

void CombineDFT_2685() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2725_2735_split[1]), &(SplitJoin10_CombineDFT_Fiss_2725_2735_join[1]));
	ENDFOR
}

void CombineDFT_2686() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2725_2735_split[2]), &(SplitJoin10_CombineDFT_Fiss_2725_2735_join[2]));
	ENDFOR
}

void CombineDFT_2687() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2725_2735_split[3]), &(SplitJoin10_CombineDFT_Fiss_2725_2735_join[3]));
	ENDFOR
}

void CombineDFT_2688() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2725_2735_split[4]), &(SplitJoin10_CombineDFT_Fiss_2725_2735_join[4]));
	ENDFOR
}

void CombineDFT_2689() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2725_2735_split[5]), &(SplitJoin10_CombineDFT_Fiss_2725_2735_join[5]));
	ENDFOR
}

void CombineDFT_2690() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2725_2735_split[6]), &(SplitJoin10_CombineDFT_Fiss_2725_2735_join[6]));
	ENDFOR
}

void CombineDFT_2691() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2725_2735_split[7]), &(SplitJoin10_CombineDFT_Fiss_2725_2735_join[7]));
	ENDFOR
}

void CombineDFT_2692() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2725_2735_split[8]), &(SplitJoin10_CombineDFT_Fiss_2725_2735_join[8]));
	ENDFOR
}

void CombineDFT_2693() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2725_2735_split[9]), &(SplitJoin10_CombineDFT_Fiss_2725_2735_join[9]));
	ENDFOR
}

void CombineDFT_2694() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2725_2735_split[10]), &(SplitJoin10_CombineDFT_Fiss_2725_2735_join[10]));
	ENDFOR
}

void CombineDFT_2695() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2725_2735_split[11]), &(SplitJoin10_CombineDFT_Fiss_2725_2735_join[11]));
	ENDFOR
}

void CombineDFT_2696() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2725_2735_split[12]), &(SplitJoin10_CombineDFT_Fiss_2725_2735_join[12]));
	ENDFOR
}

void CombineDFT_2697() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2725_2735_split[13]), &(SplitJoin10_CombineDFT_Fiss_2725_2735_join[13]));
	ENDFOR
}

void CombineDFT_2698() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2725_2735_split[14]), &(SplitJoin10_CombineDFT_Fiss_2725_2735_join[14]));
	ENDFOR
}

void CombineDFT_2699() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_2725_2735_split[15]), &(SplitJoin10_CombineDFT_Fiss_2725_2735_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2682() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin10_CombineDFT_Fiss_2725_2735_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2660WEIGHTED_ROUND_ROBIN_Splitter_2682));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2683() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2683WEIGHTED_ROUND_ROBIN_Splitter_2700, pop_complex(&SplitJoin10_CombineDFT_Fiss_2725_2735_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2702() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2726_2736_split[0]), &(SplitJoin12_CombineDFT_Fiss_2726_2736_join[0]));
	ENDFOR
}

void CombineDFT_2703() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2726_2736_split[1]), &(SplitJoin12_CombineDFT_Fiss_2726_2736_join[1]));
	ENDFOR
}

void CombineDFT_2704() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2726_2736_split[2]), &(SplitJoin12_CombineDFT_Fiss_2726_2736_join[2]));
	ENDFOR
}

void CombineDFT_2705() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2726_2736_split[3]), &(SplitJoin12_CombineDFT_Fiss_2726_2736_join[3]));
	ENDFOR
}

void CombineDFT_2706() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2726_2736_split[4]), &(SplitJoin12_CombineDFT_Fiss_2726_2736_join[4]));
	ENDFOR
}

void CombineDFT_2707() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2726_2736_split[5]), &(SplitJoin12_CombineDFT_Fiss_2726_2736_join[5]));
	ENDFOR
}

void CombineDFT_2708() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2726_2736_split[6]), &(SplitJoin12_CombineDFT_Fiss_2726_2736_join[6]));
	ENDFOR
}

void CombineDFT_2709() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_2726_2736_split[7]), &(SplitJoin12_CombineDFT_Fiss_2726_2736_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2700() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin12_CombineDFT_Fiss_2726_2736_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2683WEIGHTED_ROUND_ROBIN_Splitter_2700));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2701() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2701WEIGHTED_ROUND_ROBIN_Splitter_2710, pop_complex(&SplitJoin12_CombineDFT_Fiss_2726_2736_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2712() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2727_2737_split[0]), &(SplitJoin14_CombineDFT_Fiss_2727_2737_join[0]));
	ENDFOR
}

void CombineDFT_2713() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2727_2737_split[1]), &(SplitJoin14_CombineDFT_Fiss_2727_2737_join[1]));
	ENDFOR
}

void CombineDFT_2714() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2727_2737_split[2]), &(SplitJoin14_CombineDFT_Fiss_2727_2737_join[2]));
	ENDFOR
}

void CombineDFT_2715() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_2727_2737_split[3]), &(SplitJoin14_CombineDFT_Fiss_2727_2737_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2710() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin14_CombineDFT_Fiss_2727_2737_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2701WEIGHTED_ROUND_ROBIN_Splitter_2710));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2711() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2711WEIGHTED_ROUND_ROBIN_Splitter_2716, pop_complex(&SplitJoin14_CombineDFT_Fiss_2727_2737_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_2718() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_2728_2738_split[0]), &(SplitJoin16_CombineDFT_Fiss_2728_2738_join[0]));
	ENDFOR
}

void CombineDFT_2719() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_2728_2738_split[1]), &(SplitJoin16_CombineDFT_Fiss_2728_2738_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_2716() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_2728_2738_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2711WEIGHTED_ROUND_ROBIN_Splitter_2716));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_2728_2738_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2711WEIGHTED_ROUND_ROBIN_Splitter_2716));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_2717() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2717CombineDFT_2618, pop_complex(&SplitJoin16_CombineDFT_Fiss_2728_2738_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2717CombineDFT_2618, pop_complex(&SplitJoin16_CombineDFT_Fiss_2728_2738_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_2618() {
	FOR(uint32_t, __iter_steady_, 0, <, 21, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_2717CombineDFT_2618), &(CombineDFT_2618CPrinter_2619));
	ENDFOR
}

void CPrinter(buffer_complex_t *chanin) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		printf("%.10f", c.real);
		printf("\n");
		printf("%.10f", c.imag);
		printf("\n");
	}


void CPrinter_2619() {
	FOR(uint32_t, __iter_steady_, 0, <, 1344, __iter_steady_++)
		CPrinter(&(CombineDFT_2618CPrinter_2619));
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2683WEIGHTED_ROUND_ROBIN_Splitter_2700);
	init_buffer_complex(&CombineDFT_2618CPrinter_2619);
	FOR(int, __iter_init_0_, 0, <, 4, __iter_init_0_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_2721_2731_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 16, __iter_init_1_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_2725_2735_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_2728_2738_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 21, __iter_init_3_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_2724_2734_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 2, __iter_init_4_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_2720_2730_split[__iter_init_4_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2711WEIGHTED_ROUND_ROBIN_Splitter_2716);
	FOR(int, __iter_init_5_, 0, <, 21, __iter_init_5_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_2724_2734_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_2726_2736_join[__iter_init_6_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2717CombineDFT_2618);
	FOR(int, __iter_init_7_, 0, <, 16, __iter_init_7_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_2723_2733_join[__iter_init_7_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2632WEIGHTED_ROUND_ROBIN_Splitter_2641);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2642WEIGHTED_ROUND_ROBIN_Splitter_2659);
	FOR(int, __iter_init_8_, 0, <, 4, __iter_init_8_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_2727_2737_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_2722_2732_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 16, __iter_init_10_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_2725_2735_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 2, __iter_init_11_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_2728_2738_split[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 16, __iter_init_12_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_2723_2733_split[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 8, __iter_init_13_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_2726_2736_split[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 8, __iter_init_14_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_2722_2732_join[__iter_init_14_]);
	ENDFOR
	init_buffer_complex(&FFTTestSource_2607FFTReorderSimple_2608);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2701WEIGHTED_ROUND_ROBIN_Splitter_2710);
	FOR(int, __iter_init_15_, 0, <, 2, __iter_init_15_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_2720_2730_join[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 4, __iter_init_16_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_2721_2731_split[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 4, __iter_init_17_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_2727_2737_join[__iter_init_17_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2626WEIGHTED_ROUND_ROBIN_Splitter_2631);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2622WEIGHTED_ROUND_ROBIN_Splitter_2625);
	init_buffer_complex(&FFTReorderSimple_2608WEIGHTED_ROUND_ROBIN_Splitter_2621);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_2660WEIGHTED_ROUND_ROBIN_Splitter_2682);
// --- init: CombineDFT_2661
	 {
	 ; 
	CombineDFT_2661_s.wn.real = -1.0 ; 
	CombineDFT_2661_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2662
	 {
	CombineDFT_2662_s.wn.real = -1.0 ; 
	CombineDFT_2662_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2663
	 {
	CombineDFT_2663_s.wn.real = -1.0 ; 
	CombineDFT_2663_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2664
	 {
	CombineDFT_2664_s.wn.real = -1.0 ; 
	CombineDFT_2664_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2665
	 {
	CombineDFT_2665_s.wn.real = -1.0 ; 
	CombineDFT_2665_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2666
	 {
	CombineDFT_2666_s.wn.real = -1.0 ; 
	CombineDFT_2666_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2667
	 {
	CombineDFT_2667_s.wn.real = -1.0 ; 
	CombineDFT_2667_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2668
	 {
	CombineDFT_2668_s.wn.real = -1.0 ; 
	CombineDFT_2668_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2669
	 {
	CombineDFT_2669_s.wn.real = -1.0 ; 
	CombineDFT_2669_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2670
	 {
	CombineDFT_2670_s.wn.real = -1.0 ; 
	CombineDFT_2670_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2671
	 {
	CombineDFT_2671_s.wn.real = -1.0 ; 
	CombineDFT_2671_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2672
	 {
	CombineDFT_2672_s.wn.real = -1.0 ; 
	CombineDFT_2672_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2673
	 {
	CombineDFT_2673_s.wn.real = -1.0 ; 
	CombineDFT_2673_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2674
	 {
	CombineDFT_2674_s.wn.real = -1.0 ; 
	CombineDFT_2674_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2675
	 {
	CombineDFT_2675_s.wn.real = -1.0 ; 
	CombineDFT_2675_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2676
	 {
	CombineDFT_2676_s.wn.real = -1.0 ; 
	CombineDFT_2676_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2677
	 {
	CombineDFT_2677_s.wn.real = -1.0 ; 
	CombineDFT_2677_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2678
	 {
	CombineDFT_2678_s.wn.real = -1.0 ; 
	CombineDFT_2678_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2679
	 {
	CombineDFT_2679_s.wn.real = -1.0 ; 
	CombineDFT_2679_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2680
	 {
	CombineDFT_2680_s.wn.real = -1.0 ; 
	CombineDFT_2680_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2681
	 {
	CombineDFT_2681_s.wn.real = -1.0 ; 
	CombineDFT_2681_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_2684
	 {
	CombineDFT_2684_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2684_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2685
	 {
	CombineDFT_2685_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2685_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2686
	 {
	CombineDFT_2686_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2686_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2687
	 {
	CombineDFT_2687_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2687_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2688
	 {
	CombineDFT_2688_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2688_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2689
	 {
	CombineDFT_2689_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2689_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2690
	 {
	CombineDFT_2690_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2690_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2691
	 {
	CombineDFT_2691_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2691_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2692
	 {
	CombineDFT_2692_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2692_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2693
	 {
	CombineDFT_2693_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2693_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2694
	 {
	CombineDFT_2694_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2694_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2695
	 {
	CombineDFT_2695_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2695_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2696
	 {
	CombineDFT_2696_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2696_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2697
	 {
	CombineDFT_2697_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2697_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2698
	 {
	CombineDFT_2698_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2698_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2699
	 {
	CombineDFT_2699_s.wn.real = -4.371139E-8 ; 
	CombineDFT_2699_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_2702
	 {
	CombineDFT_2702_s.wn.real = 0.70710677 ; 
	CombineDFT_2702_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2703
	 {
	CombineDFT_2703_s.wn.real = 0.70710677 ; 
	CombineDFT_2703_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2704
	 {
	CombineDFT_2704_s.wn.real = 0.70710677 ; 
	CombineDFT_2704_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2705
	 {
	CombineDFT_2705_s.wn.real = 0.70710677 ; 
	CombineDFT_2705_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2706
	 {
	CombineDFT_2706_s.wn.real = 0.70710677 ; 
	CombineDFT_2706_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2707
	 {
	CombineDFT_2707_s.wn.real = 0.70710677 ; 
	CombineDFT_2707_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2708
	 {
	CombineDFT_2708_s.wn.real = 0.70710677 ; 
	CombineDFT_2708_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2709
	 {
	CombineDFT_2709_s.wn.real = 0.70710677 ; 
	CombineDFT_2709_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_2712
	 {
	CombineDFT_2712_s.wn.real = 0.9238795 ; 
	CombineDFT_2712_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_2713
	 {
	CombineDFT_2713_s.wn.real = 0.9238795 ; 
	CombineDFT_2713_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_2714
	 {
	CombineDFT_2714_s.wn.real = 0.9238795 ; 
	CombineDFT_2714_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_2715
	 {
	CombineDFT_2715_s.wn.real = 0.9238795 ; 
	CombineDFT_2715_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_2718
	 {
	CombineDFT_2718_s.wn.real = 0.98078525 ; 
	CombineDFT_2718_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_2719
	 {
	CombineDFT_2719_s.wn.real = 0.98078525 ; 
	CombineDFT_2719_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_2618
	 {
	 ; 
	CombineDFT_2618_s.wn.real = 0.9951847 ; 
	CombineDFT_2618_s.wn.imag = -0.09801714 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FFTTestSource_2607();
		FFTReorderSimple_2608();
		WEIGHTED_ROUND_ROBIN_Splitter_2621();
			FFTReorderSimple_2623();
			FFTReorderSimple_2624();
		WEIGHTED_ROUND_ROBIN_Joiner_2622();
		WEIGHTED_ROUND_ROBIN_Splitter_2625();
			FFTReorderSimple_2627();
			FFTReorderSimple_2628();
			FFTReorderSimple_2629();
			FFTReorderSimple_2630();
		WEIGHTED_ROUND_ROBIN_Joiner_2626();
		WEIGHTED_ROUND_ROBIN_Splitter_2631();
			FFTReorderSimple_2633();
			FFTReorderSimple_2634();
			FFTReorderSimple_2635();
			FFTReorderSimple_2636();
			FFTReorderSimple_2637();
			FFTReorderSimple_2638();
			FFTReorderSimple_2639();
			FFTReorderSimple_2640();
		WEIGHTED_ROUND_ROBIN_Joiner_2632();
		WEIGHTED_ROUND_ROBIN_Splitter_2641();
			FFTReorderSimple_2643();
			FFTReorderSimple_2644();
			FFTReorderSimple_2645();
			FFTReorderSimple_2646();
			FFTReorderSimple_2647();
			FFTReorderSimple_2648();
			FFTReorderSimple_2649();
			FFTReorderSimple_2650();
			FFTReorderSimple_2651();
			FFTReorderSimple_2652();
			FFTReorderSimple_2653();
			FFTReorderSimple_2654();
			FFTReorderSimple_2655();
			FFTReorderSimple_2656();
			FFTReorderSimple_2657();
			FFTReorderSimple_2658();
		WEIGHTED_ROUND_ROBIN_Joiner_2642();
		WEIGHTED_ROUND_ROBIN_Splitter_2659();
			CombineDFT_2661();
			CombineDFT_2662();
			CombineDFT_2663();
			CombineDFT_2664();
			CombineDFT_2665();
			CombineDFT_2666();
			CombineDFT_2667();
			CombineDFT_2668();
			CombineDFT_2669();
			CombineDFT_2670();
			CombineDFT_2671();
			CombineDFT_2672();
			CombineDFT_2673();
			CombineDFT_2674();
			CombineDFT_2675();
			CombineDFT_2676();
			CombineDFT_2677();
			CombineDFT_2678();
			CombineDFT_2679();
			CombineDFT_2680();
			CombineDFT_2681();
		WEIGHTED_ROUND_ROBIN_Joiner_2660();
		WEIGHTED_ROUND_ROBIN_Splitter_2682();
			CombineDFT_2684();
			CombineDFT_2685();
			CombineDFT_2686();
			CombineDFT_2687();
			CombineDFT_2688();
			CombineDFT_2689();
			CombineDFT_2690();
			CombineDFT_2691();
			CombineDFT_2692();
			CombineDFT_2693();
			CombineDFT_2694();
			CombineDFT_2695();
			CombineDFT_2696();
			CombineDFT_2697();
			CombineDFT_2698();
			CombineDFT_2699();
		WEIGHTED_ROUND_ROBIN_Joiner_2683();
		WEIGHTED_ROUND_ROBIN_Splitter_2700();
			CombineDFT_2702();
			CombineDFT_2703();
			CombineDFT_2704();
			CombineDFT_2705();
			CombineDFT_2706();
			CombineDFT_2707();
			CombineDFT_2708();
			CombineDFT_2709();
		WEIGHTED_ROUND_ROBIN_Joiner_2701();
		WEIGHTED_ROUND_ROBIN_Splitter_2710();
			CombineDFT_2712();
			CombineDFT_2713();
			CombineDFT_2714();
			CombineDFT_2715();
		WEIGHTED_ROUND_ROBIN_Joiner_2711();
		WEIGHTED_ROUND_ROBIN_Splitter_2716();
			CombineDFT_2718();
			CombineDFT_2719();
		WEIGHTED_ROUND_ROBIN_Joiner_2717();
		CombineDFT_2618();
		CPrinter_2619();
	ENDFOR
	return EXIT_SUCCESS;
}
