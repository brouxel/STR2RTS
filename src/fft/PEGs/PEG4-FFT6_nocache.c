#include "PEG4-FFT6_nocache.h"

buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5778WEIGHTED_ROUND_ROBIN_Splitter_5783;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5768WEIGHTED_ROUND_ROBIN_Splitter_5771;
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_5818_5828_split[4];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_5822_5832_join[4];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_5817_5827_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5784WEIGHTED_ROUND_ROBIN_Splitter_5789;
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_5819_5829_join[4];
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_5820_5830_split[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5772WEIGHTED_ROUND_ROBIN_Splitter_5777;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5796WEIGHTED_ROUND_ROBIN_Splitter_5801;
buffer_complex_t SplitJoin14_CombineDFT_Fiss_5824_5834_join[4];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_5819_5829_split[4];
buffer_complex_t SplitJoin12_CombineDFT_Fiss_5823_5833_split[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5790WEIGHTED_ROUND_ROBIN_Splitter_5795;
buffer_complex_t SplitJoin10_CombineDFT_Fiss_5822_5832_split[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5808WEIGHTED_ROUND_ROBIN_Splitter_5813;
buffer_complex_t SplitJoin14_CombineDFT_Fiss_5824_5834_split[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5814CombineDFT_5764;
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_5818_5828_join[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5802WEIGHTED_ROUND_ROBIN_Splitter_5807;
buffer_complex_t FFTReorderSimple_5754WEIGHTED_ROUND_ROBIN_Splitter_5767;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_5821_5831_join[4];
buffer_complex_t SplitJoin8_CombineDFT_Fiss_5821_5831_split[4];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_5825_5835_join[2];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_5825_5835_split[2];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_5817_5827_join[2];
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_5820_5830_join[4];
buffer_complex_t FFTTestSource_5753FFTReorderSimple_5754;
buffer_complex_t CombineDFT_5764CPrinter_5765;
buffer_complex_t SplitJoin12_CombineDFT_Fiss_5823_5833_join[4];


CombineDFT_5791_t CombineDFT_5791_s;
CombineDFT_5791_t CombineDFT_5792_s;
CombineDFT_5791_t CombineDFT_5793_s;
CombineDFT_5791_t CombineDFT_5794_s;
CombineDFT_5791_t CombineDFT_5797_s;
CombineDFT_5791_t CombineDFT_5798_s;
CombineDFT_5791_t CombineDFT_5799_s;
CombineDFT_5791_t CombineDFT_5800_s;
CombineDFT_5791_t CombineDFT_5803_s;
CombineDFT_5791_t CombineDFT_5804_s;
CombineDFT_5791_t CombineDFT_5805_s;
CombineDFT_5791_t CombineDFT_5806_s;
CombineDFT_5791_t CombineDFT_5809_s;
CombineDFT_5791_t CombineDFT_5810_s;
CombineDFT_5791_t CombineDFT_5811_s;
CombineDFT_5791_t CombineDFT_5812_s;
CombineDFT_5791_t CombineDFT_5815_s;
CombineDFT_5791_t CombineDFT_5816_s;
CombineDFT_5791_t CombineDFT_5764_s;

void FFTTestSource_5753() {
	complex_t c1;
	complex_t zero;
	c1.real = 1.0 ; 
	c1.imag = 0.0 ; 
	zero.real = 0.0 ; 
	zero.imag = 0.0 ; 
	push_complex(&FFTTestSource_5753FFTReorderSimple_5754, zero) ; 
	push_complex(&FFTTestSource_5753FFTReorderSimple_5754, c1) ; 
	FOR(int, i, 0,  < , 62, i++) {
		push_complex(&FFTTestSource_5753FFTReorderSimple_5754, zero) ; 
	}
	ENDFOR
}


void FFTReorderSimple_5754() {
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		complex_t __sa7 = {
			.real = 0,
			.imag = 0
		};
		__sa7 = ((complex_t) peek_complex(&FFTTestSource_5753FFTReorderSimple_5754, i)) ; 
		push_complex(&FFTReorderSimple_5754WEIGHTED_ROUND_ROBIN_Splitter_5767, __sa7) ; 
	}
	ENDFOR
	FOR(int, i, 1,  < , 64, i = (i + 2)) {
		complex_t __sa8 = {
			.real = 0,
			.imag = 0
		};
		__sa8 = ((complex_t) peek_complex(&FFTTestSource_5753FFTReorderSimple_5754, i)) ; 
		push_complex(&FFTReorderSimple_5754WEIGHTED_ROUND_ROBIN_Splitter_5767, __sa8) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 64, i++) {
		pop_complex(&FFTTestSource_5753FFTReorderSimple_5754) ; 
	}
	ENDFOR
}


void FFTReorderSimple_5769() {
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		complex_t __sa7 = {
			.real = 0,
			.imag = 0
		};
		__sa7 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_5817_5827_split[0], i)) ; 
		push_complex(&SplitJoin0_FFTReorderSimple_Fiss_5817_5827_join[0], __sa7) ; 
	}
	ENDFOR
	FOR(int, i, 1,  < , 32, i = (i + 2)) {
		complex_t __sa8 = {
			.real = 0,
			.imag = 0
		};
		__sa8 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_5817_5827_split[0], i)) ; 
		push_complex(&SplitJoin0_FFTReorderSimple_Fiss_5817_5827_join[0], __sa8) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 32, i++) {
		pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_5817_5827_split[0]) ; 
	}
	ENDFOR
}


void FFTReorderSimple_5770() {
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		complex_t __sa7 = {
			.real = 0,
			.imag = 0
		};
		__sa7 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_5817_5827_split[1], i)) ; 
		push_complex(&SplitJoin0_FFTReorderSimple_Fiss_5817_5827_join[1], __sa7) ; 
	}
	ENDFOR
	FOR(int, i, 1,  < , 32, i = (i + 2)) {
		complex_t __sa8 = {
			.real = 0,
			.imag = 0
		};
		__sa8 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_5817_5827_split[1], i)) ; 
		push_complex(&SplitJoin0_FFTReorderSimple_Fiss_5817_5827_join[1], __sa8) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 32, i++) {
		pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_5817_5827_split[1]) ; 
	}
	ENDFOR
}


void WEIGHTED_ROUND_ROBIN_Splitter_5767() {
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&SplitJoin0_FFTReorderSimple_Fiss_5817_5827_split[0], pop_complex(&FFTReorderSimple_5754WEIGHTED_ROUND_ROBIN_Splitter_5767));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&SplitJoin0_FFTReorderSimple_Fiss_5817_5827_split[1], pop_complex(&FFTReorderSimple_5754WEIGHTED_ROUND_ROBIN_Splitter_5767));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_5768() {
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5768WEIGHTED_ROUND_ROBIN_Splitter_5771, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_5817_5827_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5768WEIGHTED_ROUND_ROBIN_Splitter_5771, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_5817_5827_join[1]));
	ENDFOR
}

void FFTReorderSimple_5773() {
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		complex_t __sa7 = {
			.real = 0,
			.imag = 0
		};
		__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_5818_5828_split[0], i)) ; 
		push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5818_5828_join[0], __sa7) ; 
	}
	ENDFOR
	FOR(int, i, 1,  < , 16, i = (i + 2)) {
		complex_t __sa8 = {
			.real = 0,
			.imag = 0
		};
		__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_5818_5828_split[0], i)) ; 
		push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5818_5828_join[0], __sa8) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_5818_5828_split[0]) ; 
	}
	ENDFOR
}


void FFTReorderSimple_5774() {
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		complex_t __sa7 = {
			.real = 0,
			.imag = 0
		};
		__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_5818_5828_split[1], i)) ; 
		push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5818_5828_join[1], __sa7) ; 
	}
	ENDFOR
	FOR(int, i, 1,  < , 16, i = (i + 2)) {
		complex_t __sa8 = {
			.real = 0,
			.imag = 0
		};
		__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_5818_5828_split[1], i)) ; 
		push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5818_5828_join[1], __sa8) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_5818_5828_split[1]) ; 
	}
	ENDFOR
}


void FFTReorderSimple_5775() {
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		complex_t __sa7 = {
			.real = 0,
			.imag = 0
		};
		__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_5818_5828_split[2], i)) ; 
		push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5818_5828_join[2], __sa7) ; 
	}
	ENDFOR
	FOR(int, i, 1,  < , 16, i = (i + 2)) {
		complex_t __sa8 = {
			.real = 0,
			.imag = 0
		};
		__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_5818_5828_split[2], i)) ; 
		push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5818_5828_join[2], __sa8) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_5818_5828_split[2]) ; 
	}
	ENDFOR
}


void FFTReorderSimple_5776() {
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		complex_t __sa7 = {
			.real = 0,
			.imag = 0
		};
		__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_5818_5828_split[3], i)) ; 
		push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5818_5828_join[3], __sa7) ; 
	}
	ENDFOR
	FOR(int, i, 1,  < , 16, i = (i + 2)) {
		complex_t __sa8 = {
			.real = 0,
			.imag = 0
		};
		__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_5818_5828_split[3], i)) ; 
		push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5818_5828_join[3], __sa8) ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_5818_5828_split[3]) ; 
	}
	ENDFOR
}


void WEIGHTED_ROUND_ROBIN_Splitter_5771() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5818_5828_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5768WEIGHTED_ROUND_ROBIN_Splitter_5771));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_5772() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5772WEIGHTED_ROUND_ROBIN_Splitter_5777, pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_5818_5828_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void FFTReorderSimple_5779(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5819_5829_split[0], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5819_5829_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5819_5829_split[0], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5819_5829_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_5819_5829_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5780(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5819_5829_split[1], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5819_5829_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5819_5829_split[1], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5819_5829_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_5819_5829_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5781(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5819_5829_split[2], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5819_5829_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5819_5829_split[2], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5819_5829_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_5819_5829_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5782(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5819_5829_split[3], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5819_5829_join[3], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5819_5829_split[3], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5819_5829_join[3], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_5819_5829_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5777() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5819_5829_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5772WEIGHTED_ROUND_ROBIN_Splitter_5777));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5778() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5778WEIGHTED_ROUND_ROBIN_Splitter_5783, pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_5819_5829_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_5785(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5820_5830_split[0], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5820_5830_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5820_5830_split[0], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5820_5830_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_5820_5830_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5786(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5820_5830_split[1], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5820_5830_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5820_5830_split[1], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5820_5830_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_5820_5830_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5787(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5820_5830_split[2], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5820_5830_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5820_5830_split[2], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5820_5830_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_5820_5830_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5788(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5820_5830_split[3], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5820_5830_join[3], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5820_5830_split[3], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5820_5830_join[3], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_5820_5830_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5783() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5820_5830_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5778WEIGHTED_ROUND_ROBIN_Splitter_5783));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5784() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5784WEIGHTED_ROUND_ROBIN_Splitter_5789, pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_5820_5830_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5791(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5821_5831_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5821_5831_split[0], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5791_s.wn.real) - (w.imag * CombineDFT_5791_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5791_s.wn.imag) + (w.imag * CombineDFT_5791_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_5821_5831_split[0]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_5821_5831_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5792(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5821_5831_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5821_5831_split[1], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5792_s.wn.real) - (w.imag * CombineDFT_5792_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5792_s.wn.imag) + (w.imag * CombineDFT_5792_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_5821_5831_split[1]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_5821_5831_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5793(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5821_5831_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5821_5831_split[2], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5793_s.wn.real) - (w.imag * CombineDFT_5793_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5793_s.wn.imag) + (w.imag * CombineDFT_5793_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_5821_5831_split[2]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_5821_5831_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5794(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5821_5831_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5821_5831_split[3], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5794_s.wn.real) - (w.imag * CombineDFT_5794_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5794_s.wn.imag) + (w.imag * CombineDFT_5794_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_5821_5831_split[3]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_5821_5831_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5789() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin8_CombineDFT_Fiss_5821_5831_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5784WEIGHTED_ROUND_ROBIN_Splitter_5789));
			push_complex(&SplitJoin8_CombineDFT_Fiss_5821_5831_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5784WEIGHTED_ROUND_ROBIN_Splitter_5789));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5790() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5790WEIGHTED_ROUND_ROBIN_Splitter_5795, pop_complex(&SplitJoin8_CombineDFT_Fiss_5821_5831_join[__iter_]));
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5790WEIGHTED_ROUND_ROBIN_Splitter_5795, pop_complex(&SplitJoin8_CombineDFT_Fiss_5821_5831_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_5797(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5822_5832_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5822_5832_split[0], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5797_s.wn.real) - (w.imag * CombineDFT_5797_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5797_s.wn.imag) + (w.imag * CombineDFT_5797_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_5822_5832_split[0]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_5822_5832_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5798(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5822_5832_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5822_5832_split[1], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5798_s.wn.real) - (w.imag * CombineDFT_5798_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5798_s.wn.imag) + (w.imag * CombineDFT_5798_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_5822_5832_split[1]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_5822_5832_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5799(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5822_5832_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5822_5832_split[2], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5799_s.wn.real) - (w.imag * CombineDFT_5799_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5799_s.wn.imag) + (w.imag * CombineDFT_5799_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_5822_5832_split[2]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_5822_5832_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5800(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5822_5832_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5822_5832_split[3], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5800_s.wn.real) - (w.imag * CombineDFT_5800_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5800_s.wn.imag) + (w.imag * CombineDFT_5800_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_5822_5832_split[3]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_5822_5832_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5795() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin10_CombineDFT_Fiss_5822_5832_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5790WEIGHTED_ROUND_ROBIN_Splitter_5795));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5796() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5796WEIGHTED_ROUND_ROBIN_Splitter_5801, pop_complex(&SplitJoin10_CombineDFT_Fiss_5822_5832_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5803(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5823_5833_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5823_5833_split[0], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5803_s.wn.real) - (w.imag * CombineDFT_5803_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5803_s.wn.imag) + (w.imag * CombineDFT_5803_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_5823_5833_split[0]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_5823_5833_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5804(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5823_5833_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5823_5833_split[1], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5804_s.wn.real) - (w.imag * CombineDFT_5804_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5804_s.wn.imag) + (w.imag * CombineDFT_5804_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_5823_5833_split[1]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_5823_5833_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5805(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5823_5833_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5823_5833_split[2], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5805_s.wn.real) - (w.imag * CombineDFT_5805_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5805_s.wn.imag) + (w.imag * CombineDFT_5805_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_5823_5833_split[2]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_5823_5833_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5806(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5823_5833_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5823_5833_split[3], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5806_s.wn.real) - (w.imag * CombineDFT_5806_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5806_s.wn.imag) + (w.imag * CombineDFT_5806_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_5823_5833_split[3]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_5823_5833_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5801() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin12_CombineDFT_Fiss_5823_5833_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5796WEIGHTED_ROUND_ROBIN_Splitter_5801));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5802() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5802WEIGHTED_ROUND_ROBIN_Splitter_5807, pop_complex(&SplitJoin12_CombineDFT_Fiss_5823_5833_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5809() {
	complex_t w;
	complex_t y0;
	complex_t y1;
	complex_t y1w;
	complex_t w_next;
	complex_t results[16];
	w.real = 1.0 ; 
	w.imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i++) {
		complex_t __sa1 = {
			.real = 0,
			.imag = 0
		};
		complex_t __sa2 = {
			.real = 0,
			.imag = 0
		};
		__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_5824_5834_split[0], i)) ; 
		y0.real = __sa1.real ; 
		y0.imag = __sa1.imag ; 
		__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_5824_5834_split[0], (8 + i))) ; 
		y1.real = __sa2.real ; 
		y1.imag = __sa2.imag ; 
		y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
		y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
		results[i].real = (y0.real + y1w.real) ; 
		results[i].imag = (y0.imag + y1w.imag) ; 
		results[(8 + i)].real = (y0.real - y1w.real) ; 
		results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
		w_next.real = ((w.real * CombineDFT_5809_s.wn.real) - (w.imag * CombineDFT_5809_s.wn.imag)) ; 
		w_next.imag = ((w.real * CombineDFT_5809_s.wn.imag) + (w.imag * CombineDFT_5809_s.wn.real)) ; 
		w.real = w_next.real ; 
		w.imag = w_next.imag ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_complex(&SplitJoin14_CombineDFT_Fiss_5824_5834_split[0]) ; 
		push_complex(&SplitJoin14_CombineDFT_Fiss_5824_5834_join[0], results[i]) ; 
	}
	ENDFOR
}


void CombineDFT_5810() {
	complex_t w;
	complex_t y0;
	complex_t y1;
	complex_t y1w;
	complex_t w_next;
	complex_t results[16];
	w.real = 1.0 ; 
	w.imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i++) {
		complex_t __sa1 = {
			.real = 0,
			.imag = 0
		};
		complex_t __sa2 = {
			.real = 0,
			.imag = 0
		};
		__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_5824_5834_split[1], i)) ; 
		y0.real = __sa1.real ; 
		y0.imag = __sa1.imag ; 
		__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_5824_5834_split[1], (8 + i))) ; 
		y1.real = __sa2.real ; 
		y1.imag = __sa2.imag ; 
		y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
		y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
		results[i].real = (y0.real + y1w.real) ; 
		results[i].imag = (y0.imag + y1w.imag) ; 
		results[(8 + i)].real = (y0.real - y1w.real) ; 
		results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
		w_next.real = ((w.real * CombineDFT_5810_s.wn.real) - (w.imag * CombineDFT_5810_s.wn.imag)) ; 
		w_next.imag = ((w.real * CombineDFT_5810_s.wn.imag) + (w.imag * CombineDFT_5810_s.wn.real)) ; 
		w.real = w_next.real ; 
		w.imag = w_next.imag ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_complex(&SplitJoin14_CombineDFT_Fiss_5824_5834_split[1]) ; 
		push_complex(&SplitJoin14_CombineDFT_Fiss_5824_5834_join[1], results[i]) ; 
	}
	ENDFOR
}


void CombineDFT_5811() {
	complex_t w;
	complex_t y0;
	complex_t y1;
	complex_t y1w;
	complex_t w_next;
	complex_t results[16];
	w.real = 1.0 ; 
	w.imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i++) {
		complex_t __sa1 = {
			.real = 0,
			.imag = 0
		};
		complex_t __sa2 = {
			.real = 0,
			.imag = 0
		};
		__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_5824_5834_split[2], i)) ; 
		y0.real = __sa1.real ; 
		y0.imag = __sa1.imag ; 
		__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_5824_5834_split[2], (8 + i))) ; 
		y1.real = __sa2.real ; 
		y1.imag = __sa2.imag ; 
		y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
		y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
		results[i].real = (y0.real + y1w.real) ; 
		results[i].imag = (y0.imag + y1w.imag) ; 
		results[(8 + i)].real = (y0.real - y1w.real) ; 
		results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
		w_next.real = ((w.real * CombineDFT_5811_s.wn.real) - (w.imag * CombineDFT_5811_s.wn.imag)) ; 
		w_next.imag = ((w.real * CombineDFT_5811_s.wn.imag) + (w.imag * CombineDFT_5811_s.wn.real)) ; 
		w.real = w_next.real ; 
		w.imag = w_next.imag ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_complex(&SplitJoin14_CombineDFT_Fiss_5824_5834_split[2]) ; 
		push_complex(&SplitJoin14_CombineDFT_Fiss_5824_5834_join[2], results[i]) ; 
	}
	ENDFOR
}


void CombineDFT_5812() {
	complex_t w;
	complex_t y0;
	complex_t y1;
	complex_t y1w;
	complex_t w_next;
	complex_t results[16];
	w.real = 1.0 ; 
	w.imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i++) {
		complex_t __sa1 = {
			.real = 0,
			.imag = 0
		};
		complex_t __sa2 = {
			.real = 0,
			.imag = 0
		};
		__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_5824_5834_split[3], i)) ; 
		y0.real = __sa1.real ; 
		y0.imag = __sa1.imag ; 
		__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_5824_5834_split[3], (8 + i))) ; 
		y1.real = __sa2.real ; 
		y1.imag = __sa2.imag ; 
		y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
		y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
		results[i].real = (y0.real + y1w.real) ; 
		results[i].imag = (y0.imag + y1w.imag) ; 
		results[(8 + i)].real = (y0.real - y1w.real) ; 
		results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
		w_next.real = ((w.real * CombineDFT_5812_s.wn.real) - (w.imag * CombineDFT_5812_s.wn.imag)) ; 
		w_next.imag = ((w.real * CombineDFT_5812_s.wn.imag) + (w.imag * CombineDFT_5812_s.wn.real)) ; 
		w.real = w_next.real ; 
		w.imag = w_next.imag ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 16, i++) {
		pop_complex(&SplitJoin14_CombineDFT_Fiss_5824_5834_split[3]) ; 
		push_complex(&SplitJoin14_CombineDFT_Fiss_5824_5834_join[3], results[i]) ; 
	}
	ENDFOR
}


void WEIGHTED_ROUND_ROBIN_Splitter_5807() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
			push_complex(&SplitJoin14_CombineDFT_Fiss_5824_5834_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5802WEIGHTED_ROUND_ROBIN_Splitter_5807));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_5808() {
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5808WEIGHTED_ROUND_ROBIN_Splitter_5813, pop_complex(&SplitJoin14_CombineDFT_Fiss_5824_5834_join[__iter_dec_]));
		ENDFOR
	ENDFOR
}

void CombineDFT_5815() {
	complex_t w;
	complex_t y0;
	complex_t y1;
	complex_t y1w;
	complex_t w_next;
	complex_t results[32];
	w.real = 1.0 ; 
	w.imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i++) {
		complex_t __sa1 = {
			.real = 0,
			.imag = 0
		};
		complex_t __sa2 = {
			.real = 0,
			.imag = 0
		};
		__sa1 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_5825_5835_split[0], i)) ; 
		y0.real = __sa1.real ; 
		y0.imag = __sa1.imag ; 
		__sa2 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_5825_5835_split[0], (16 + i))) ; 
		y1.real = __sa2.real ; 
		y1.imag = __sa2.imag ; 
		y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
		y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
		results[i].real = (y0.real + y1w.real) ; 
		results[i].imag = (y0.imag + y1w.imag) ; 
		results[(16 + i)].real = (y0.real - y1w.real) ; 
		results[(16 + i)].imag = (y0.imag - y1w.imag) ; 
		w_next.real = ((w.real * CombineDFT_5815_s.wn.real) - (w.imag * CombineDFT_5815_s.wn.imag)) ; 
		w_next.imag = ((w.real * CombineDFT_5815_s.wn.imag) + (w.imag * CombineDFT_5815_s.wn.real)) ; 
		w.real = w_next.real ; 
		w.imag = w_next.imag ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 32, i++) {
		pop_complex(&SplitJoin16_CombineDFT_Fiss_5825_5835_split[0]) ; 
		push_complex(&SplitJoin16_CombineDFT_Fiss_5825_5835_join[0], results[i]) ; 
	}
	ENDFOR
}


void CombineDFT_5816() {
	complex_t w;
	complex_t y0;
	complex_t y1;
	complex_t y1w;
	complex_t w_next;
	complex_t results[32];
	w.real = 1.0 ; 
	w.imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i++) {
		complex_t __sa1 = {
			.real = 0,
			.imag = 0
		};
		complex_t __sa2 = {
			.real = 0,
			.imag = 0
		};
		__sa1 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_5825_5835_split[1], i)) ; 
		y0.real = __sa1.real ; 
		y0.imag = __sa1.imag ; 
		__sa2 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_5825_5835_split[1], (16 + i))) ; 
		y1.real = __sa2.real ; 
		y1.imag = __sa2.imag ; 
		y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
		y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
		results[i].real = (y0.real + y1w.real) ; 
		results[i].imag = (y0.imag + y1w.imag) ; 
		results[(16 + i)].real = (y0.real - y1w.real) ; 
		results[(16 + i)].imag = (y0.imag - y1w.imag) ; 
		w_next.real = ((w.real * CombineDFT_5816_s.wn.real) - (w.imag * CombineDFT_5816_s.wn.imag)) ; 
		w_next.imag = ((w.real * CombineDFT_5816_s.wn.imag) + (w.imag * CombineDFT_5816_s.wn.real)) ; 
		w.real = w_next.real ; 
		w.imag = w_next.imag ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 32, i++) {
		pop_complex(&SplitJoin16_CombineDFT_Fiss_5825_5835_split[1]) ; 
		push_complex(&SplitJoin16_CombineDFT_Fiss_5825_5835_join[1], results[i]) ; 
	}
	ENDFOR
}


void WEIGHTED_ROUND_ROBIN_Splitter_5813() {
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&SplitJoin16_CombineDFT_Fiss_5825_5835_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5808WEIGHTED_ROUND_ROBIN_Splitter_5813));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&SplitJoin16_CombineDFT_Fiss_5825_5835_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5808WEIGHTED_ROUND_ROBIN_Splitter_5813));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_5814() {
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5814CombineDFT_5764, pop_complex(&SplitJoin16_CombineDFT_Fiss_5825_5835_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5814CombineDFT_5764, pop_complex(&SplitJoin16_CombineDFT_Fiss_5825_5835_join[1]));
	ENDFOR
}

void CombineDFT_5764() {
	complex_t w;
	complex_t y0;
	complex_t y1;
	complex_t y1w;
	complex_t w_next;
	complex_t results[64];
	w.real = 1.0 ; 
	w.imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		complex_t __sa1 = {
			.real = 0,
			.imag = 0
		};
		complex_t __sa2 = {
			.real = 0,
			.imag = 0
		};
		__sa1 = ((complex_t) peek_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5814CombineDFT_5764, i)) ; 
		y0.real = __sa1.real ; 
		y0.imag = __sa1.imag ; 
		__sa2 = ((complex_t) peek_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5814CombineDFT_5764, (32 + i))) ; 
		y1.real = __sa2.real ; 
		y1.imag = __sa2.imag ; 
		y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
		y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
		results[i].real = (y0.real + y1w.real) ; 
		results[i].imag = (y0.imag + y1w.imag) ; 
		results[(32 + i)].real = (y0.real - y1w.real) ; 
		results[(32 + i)].imag = (y0.imag - y1w.imag) ; 
		w_next.real = ((w.real * CombineDFT_5764_s.wn.real) - (w.imag * CombineDFT_5764_s.wn.imag)) ; 
		w_next.imag = ((w.real * CombineDFT_5764_s.wn.imag) + (w.imag * CombineDFT_5764_s.wn.real)) ; 
		w.real = w_next.real ; 
		w.imag = w_next.imag ; 
	}
	ENDFOR
	FOR(int, i, 0,  < , 64, i++) {
		pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5814CombineDFT_5764) ; 
		push_complex(&CombineDFT_5764CPrinter_5765, results[i]) ; 
	}
	ENDFOR
}


void CPrinter_5765(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&CombineDFT_5764CPrinter_5765));
		printf("%.10f", c.real);
		printf("\n");
		printf("%.10f", c.imag);
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5778WEIGHTED_ROUND_ROBIN_Splitter_5783);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5768WEIGHTED_ROUND_ROBIN_Splitter_5771);
	FOR(int, __iter_init_0_, 0, <, 4, __iter_init_0_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_5818_5828_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 4, __iter_init_1_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_5822_5832_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_5817_5827_split[__iter_init_2_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5784WEIGHTED_ROUND_ROBIN_Splitter_5789);
	FOR(int, __iter_init_3_, 0, <, 4, __iter_init_3_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_5819_5829_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 4, __iter_init_4_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_5820_5830_split[__iter_init_4_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5772WEIGHTED_ROUND_ROBIN_Splitter_5777);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5796WEIGHTED_ROUND_ROBIN_Splitter_5801);
	FOR(int, __iter_init_5_, 0, <, 4, __iter_init_5_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_5824_5834_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 4, __iter_init_6_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_5819_5829_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 4, __iter_init_7_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_5823_5833_split[__iter_init_7_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5790WEIGHTED_ROUND_ROBIN_Splitter_5795);
	FOR(int, __iter_init_8_, 0, <, 4, __iter_init_8_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_5822_5832_split[__iter_init_8_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5808WEIGHTED_ROUND_ROBIN_Splitter_5813);
	FOR(int, __iter_init_9_, 0, <, 4, __iter_init_9_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_5824_5834_split[__iter_init_9_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5814CombineDFT_5764);
	FOR(int, __iter_init_10_, 0, <, 4, __iter_init_10_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_5818_5828_join[__iter_init_10_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5802WEIGHTED_ROUND_ROBIN_Splitter_5807);
	init_buffer_complex(&FFTReorderSimple_5754WEIGHTED_ROUND_ROBIN_Splitter_5767);
	FOR(int, __iter_init_11_, 0, <, 4, __iter_init_11_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_5821_5831_join[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 4, __iter_init_12_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_5821_5831_split[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_5825_5835_join[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_5825_5835_split[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 2, __iter_init_15_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_5817_5827_join[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 4, __iter_init_16_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_5820_5830_join[__iter_init_16_]);
	ENDFOR
	init_buffer_complex(&FFTTestSource_5753FFTReorderSimple_5754);
	init_buffer_complex(&CombineDFT_5764CPrinter_5765);
	FOR(int, __iter_init_17_, 0, <, 4, __iter_init_17_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_5823_5833_join[__iter_init_17_]);
	ENDFOR
// --- init: CombineDFT_5791
	 {
	 ; 
	CombineDFT_5791_s.wn.real = -1.0 ; 
	CombineDFT_5791_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5792
	 {
	 ; 
	CombineDFT_5792_s.wn.real = -1.0 ; 
	CombineDFT_5792_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5793
	 {
	 ; 
	CombineDFT_5793_s.wn.real = -1.0 ; 
	CombineDFT_5793_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5794
	 {
	 ; 
	CombineDFT_5794_s.wn.real = -1.0 ; 
	CombineDFT_5794_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5797
	 {
	 ; 
	CombineDFT_5797_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5797_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5798
	 {
	 ; 
	CombineDFT_5798_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5798_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5799
	 {
	 ; 
	CombineDFT_5799_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5799_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5800
	 {
	 ; 
	CombineDFT_5800_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5800_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5803
	 {
	 ; 
	CombineDFT_5803_s.wn.real = 0.70710677 ; 
	CombineDFT_5803_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5804
	 {
	 ; 
	CombineDFT_5804_s.wn.real = 0.70710677 ; 
	CombineDFT_5804_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5805
	 {
	 ; 
	CombineDFT_5805_s.wn.real = 0.70710677 ; 
	CombineDFT_5805_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5806
	 {
	 ; 
	CombineDFT_5806_s.wn.real = 0.70710677 ; 
	CombineDFT_5806_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5809
	 {
	 ; 
	CombineDFT_5809_s.wn.real = 0.9238795 ; 
	CombineDFT_5809_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_5810
	 {
	 ; 
	CombineDFT_5810_s.wn.real = 0.9238795 ; 
	CombineDFT_5810_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_5811
	 {
	 ; 
	CombineDFT_5811_s.wn.real = 0.9238795 ; 
	CombineDFT_5811_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_5812
	 {
	 ; 
	CombineDFT_5812_s.wn.real = 0.9238795 ; 
	CombineDFT_5812_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_5815
	 {
	 ; 
	CombineDFT_5815_s.wn.real = 0.98078525 ; 
	CombineDFT_5815_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_5816
	 {
	 ; 
	CombineDFT_5816_s.wn.real = 0.98078525 ; 
	CombineDFT_5816_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_5764
	 {
	 ; 
	CombineDFT_5764_s.wn.real = 0.9951847 ; 
	CombineDFT_5764_s.wn.imag = -0.09801714 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FFTTestSource_5753();
		FFTReorderSimple_5754();
		WEIGHTED_ROUND_ROBIN_Splitter_5767();
			FFTReorderSimple_5769();
			FFTReorderSimple_5770();
		WEIGHTED_ROUND_ROBIN_Joiner_5768();
		WEIGHTED_ROUND_ROBIN_Splitter_5771();
			FFTReorderSimple_5773();
			FFTReorderSimple_5774();
			FFTReorderSimple_5775();
			FFTReorderSimple_5776();
		WEIGHTED_ROUND_ROBIN_Joiner_5772();
		WEIGHTED_ROUND_ROBIN_Splitter_5777();
			FFTReorderSimple_5779();
			FFTReorderSimple_5780();
			FFTReorderSimple_5781();
			FFTReorderSimple_5782();
		WEIGHTED_ROUND_ROBIN_Joiner_5778();
		WEIGHTED_ROUND_ROBIN_Splitter_5783();
			FFTReorderSimple_5785();
			FFTReorderSimple_5786();
			FFTReorderSimple_5787();
			FFTReorderSimple_5788();
		WEIGHTED_ROUND_ROBIN_Joiner_5784();
		WEIGHTED_ROUND_ROBIN_Splitter_5789();
			CombineDFT_5791();
			CombineDFT_5792();
			CombineDFT_5793();
			CombineDFT_5794();
		WEIGHTED_ROUND_ROBIN_Joiner_5790();
		WEIGHTED_ROUND_ROBIN_Splitter_5795();
			CombineDFT_5797();
			CombineDFT_5798();
			CombineDFT_5799();
			CombineDFT_5800();
		WEIGHTED_ROUND_ROBIN_Joiner_5796();
		WEIGHTED_ROUND_ROBIN_Splitter_5801();
			CombineDFT_5803();
			CombineDFT_5804();
			CombineDFT_5805();
			CombineDFT_5806();
		WEIGHTED_ROUND_ROBIN_Joiner_5802();
		WEIGHTED_ROUND_ROBIN_Splitter_5807();
			CombineDFT_5809();
			CombineDFT_5810();
			CombineDFT_5811();
			CombineDFT_5812();
		WEIGHTED_ROUND_ROBIN_Joiner_5808();
		WEIGHTED_ROUND_ROBIN_Splitter_5813();
			CombineDFT_5815();
			CombineDFT_5816();
		WEIGHTED_ROUND_ROBIN_Joiner_5814();
		CombineDFT_5764();
		CPrinter_5765();
	ENDFOR
	return EXIT_SUCCESS;
}
