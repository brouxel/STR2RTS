#include "PEG7-FFT6_nocache.h"

buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_5415_5425_join[7];
buffer_complex_t FFTTestSource_5333FFTReorderSimple_5334;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_5416_5426_join[7];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5352WEIGHTED_ROUND_ROBIN_Splitter_5357;
buffer_complex_t FFTReorderSimple_5334WEIGHTED_ROUND_ROBIN_Splitter_5347;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5376WEIGHTED_ROUND_ROBIN_Splitter_5384;
buffer_complex_t SplitJoin14_CombineDFT_Fiss_5419_5429_join[4];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_5419_5429_split[4];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_5420_5430_split[2];
buffer_complex_t SplitJoin12_CombineDFT_Fiss_5418_5428_split[7];
buffer_complex_t SplitJoin12_CombineDFT_Fiss_5418_5428_join[7];
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_5413_5423_join[4];
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_5413_5423_split[4];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_5412_5422_join[2];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_5412_5422_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5409CombineDFT_5344;
buffer_complex_t SplitJoin8_CombineDFT_Fiss_5416_5426_split[7];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5348WEIGHTED_ROUND_ROBIN_Splitter_5351;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5358WEIGHTED_ROUND_ROBIN_Splitter_5366;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5385WEIGHTED_ROUND_ROBIN_Splitter_5393;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5367WEIGHTED_ROUND_ROBIN_Splitter_5375;
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_5415_5425_split[7];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5403WEIGHTED_ROUND_ROBIN_Splitter_5408;
buffer_complex_t SplitJoin10_CombineDFT_Fiss_5417_5427_split[7];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_5394WEIGHTED_ROUND_ROBIN_Splitter_5402;
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_5414_5424_join[7];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_5414_5424_split[7];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_5420_5430_join[2];
buffer_complex_t CombineDFT_5344CPrinter_5345;
buffer_complex_t SplitJoin10_CombineDFT_Fiss_5417_5427_join[7];


CombineDFT_5377_t CombineDFT_5377_s;
CombineDFT_5377_t CombineDFT_5378_s;
CombineDFT_5377_t CombineDFT_5379_s;
CombineDFT_5377_t CombineDFT_5380_s;
CombineDFT_5377_t CombineDFT_5381_s;
CombineDFT_5377_t CombineDFT_5382_s;
CombineDFT_5377_t CombineDFT_5383_s;
CombineDFT_5377_t CombineDFT_5386_s;
CombineDFT_5377_t CombineDFT_5387_s;
CombineDFT_5377_t CombineDFT_5388_s;
CombineDFT_5377_t CombineDFT_5389_s;
CombineDFT_5377_t CombineDFT_5390_s;
CombineDFT_5377_t CombineDFT_5391_s;
CombineDFT_5377_t CombineDFT_5392_s;
CombineDFT_5377_t CombineDFT_5395_s;
CombineDFT_5377_t CombineDFT_5396_s;
CombineDFT_5377_t CombineDFT_5397_s;
CombineDFT_5377_t CombineDFT_5398_s;
CombineDFT_5377_t CombineDFT_5399_s;
CombineDFT_5377_t CombineDFT_5400_s;
CombineDFT_5377_t CombineDFT_5401_s;
CombineDFT_5377_t CombineDFT_5404_s;
CombineDFT_5377_t CombineDFT_5405_s;
CombineDFT_5377_t CombineDFT_5406_s;
CombineDFT_5377_t CombineDFT_5407_s;
CombineDFT_5377_t CombineDFT_5410_s;
CombineDFT_5377_t CombineDFT_5411_s;
CombineDFT_5377_t CombineDFT_5344_s;

void FFTTestSource_5333(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t c1;
		complex_t zero;
		c1.real = 1.0 ; 
		c1.imag = 0.0 ; 
		zero.real = 0.0 ; 
		zero.imag = 0.0 ; 
		push_complex(&FFTTestSource_5333FFTReorderSimple_5334, zero) ; 
		push_complex(&FFTTestSource_5333FFTReorderSimple_5334, c1) ; 
		FOR(int, i, 0,  < , 62, i++) {
			push_complex(&FFTTestSource_5333FFTReorderSimple_5334, zero) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5334(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&FFTTestSource_5333FFTReorderSimple_5334, i)) ; 
			push_complex(&FFTReorderSimple_5334WEIGHTED_ROUND_ROBIN_Splitter_5347, __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 64, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&FFTTestSource_5333FFTReorderSimple_5334, i)) ; 
			push_complex(&FFTReorderSimple_5334WEIGHTED_ROUND_ROBIN_Splitter_5347, __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&FFTTestSource_5333FFTReorderSimple_5334) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5349(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_5412_5422_split[0], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_5412_5422_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 32, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_5412_5422_split[0], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_5412_5422_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_5412_5422_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5350(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_5412_5422_split[1], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_5412_5422_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 32, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_5412_5422_split[1], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_5412_5422_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_5412_5422_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5347() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_5412_5422_split[0], pop_complex(&FFTReorderSimple_5334WEIGHTED_ROUND_ROBIN_Splitter_5347));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_5412_5422_split[1], pop_complex(&FFTReorderSimple_5334WEIGHTED_ROUND_ROBIN_Splitter_5347));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5348() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5348WEIGHTED_ROUND_ROBIN_Splitter_5351, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_5412_5422_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5348WEIGHTED_ROUND_ROBIN_Splitter_5351, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_5412_5422_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_5353(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_5413_5423_split[0], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5413_5423_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_5413_5423_split[0], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5413_5423_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_5413_5423_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5354(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_5413_5423_split[1], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5413_5423_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_5413_5423_split[1], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5413_5423_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_5413_5423_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5355(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_5413_5423_split[2], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5413_5423_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_5413_5423_split[2], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5413_5423_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_5413_5423_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5356(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_5413_5423_split[3], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5413_5423_join[3], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_5413_5423_split[3], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5413_5423_join[3], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_5413_5423_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5351() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin2_FFTReorderSimple_Fiss_5413_5423_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5348WEIGHTED_ROUND_ROBIN_Splitter_5351));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5352() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5352WEIGHTED_ROUND_ROBIN_Splitter_5357, pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_5413_5423_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_5359(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_split[0], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_split[0], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5360(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_split[1], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_split[1], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5361(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_split[2], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_split[2], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5362(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_split[3], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_join[3], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_split[3], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_join[3], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5363(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_split[4], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_join[4], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_split[4], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_join[4], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5364(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_split[5], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_join[5], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_split[5], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_join[5], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5365(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_split[6], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_join[6], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_split[6], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_join[6], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5357() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5352WEIGHTED_ROUND_ROBIN_Splitter_5357));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5358() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5358WEIGHTED_ROUND_ROBIN_Splitter_5366, pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_5368(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_split[0], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_split[0], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5369(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_split[1], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_split[1], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5370(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_split[2], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_split[2], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5371(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_split[3], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_join[3], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_split[3], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_join[3], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5372(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_split[4], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_join[4], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_split[4], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_join[4], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5373(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_split[5], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_join[5], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_split[5], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_join[5], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_5374(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_split[6], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_join[6], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_split[6], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_join[6], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5366() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5358WEIGHTED_ROUND_ROBIN_Splitter_5366));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5367() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5367WEIGHTED_ROUND_ROBIN_Splitter_5375, pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5377(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_split[0], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5377_s.wn.real) - (w.imag * CombineDFT_5377_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5377_s.wn.imag) + (w.imag * CombineDFT_5377_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_split[0]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5378(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_split[1], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5378_s.wn.real) - (w.imag * CombineDFT_5378_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5378_s.wn.imag) + (w.imag * CombineDFT_5378_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_split[1]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5379(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_split[2], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5379_s.wn.real) - (w.imag * CombineDFT_5379_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5379_s.wn.imag) + (w.imag * CombineDFT_5379_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_split[2]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5380(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_split[3], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5380_s.wn.real) - (w.imag * CombineDFT_5380_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5380_s.wn.imag) + (w.imag * CombineDFT_5380_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_split[3]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5381(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_split[4], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_split[4], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5381_s.wn.real) - (w.imag * CombineDFT_5381_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5381_s.wn.imag) + (w.imag * CombineDFT_5381_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_split[4]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5382(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_split[5], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_split[5], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5382_s.wn.real) - (w.imag * CombineDFT_5382_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5382_s.wn.imag) + (w.imag * CombineDFT_5382_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_split[5]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5383(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_split[6], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_split[6], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5383_s.wn.real) - (w.imag * CombineDFT_5383_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5383_s.wn.imag) + (w.imag * CombineDFT_5383_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_split[6]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5375() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5367WEIGHTED_ROUND_ROBIN_Splitter_5375));
			push_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5367WEIGHTED_ROUND_ROBIN_Splitter_5375));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5376() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5376WEIGHTED_ROUND_ROBIN_Splitter_5384, pop_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_join[__iter_]));
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5376WEIGHTED_ROUND_ROBIN_Splitter_5384, pop_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_5386(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_split[0], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5386_s.wn.real) - (w.imag * CombineDFT_5386_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5386_s.wn.imag) + (w.imag * CombineDFT_5386_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_split[0]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5387(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_split[1], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5387_s.wn.real) - (w.imag * CombineDFT_5387_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5387_s.wn.imag) + (w.imag * CombineDFT_5387_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_split[1]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5388(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_split[2], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5388_s.wn.real) - (w.imag * CombineDFT_5388_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5388_s.wn.imag) + (w.imag * CombineDFT_5388_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_split[2]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5389(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_split[3], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5389_s.wn.real) - (w.imag * CombineDFT_5389_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5389_s.wn.imag) + (w.imag * CombineDFT_5389_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_split[3]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5390(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_split[4], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_split[4], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5390_s.wn.real) - (w.imag * CombineDFT_5390_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5390_s.wn.imag) + (w.imag * CombineDFT_5390_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_split[4]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5391(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_split[5], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_split[5], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5391_s.wn.real) - (w.imag * CombineDFT_5391_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5391_s.wn.imag) + (w.imag * CombineDFT_5391_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_split[5]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5392(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_split[6], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_split[6], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5392_s.wn.real) - (w.imag * CombineDFT_5392_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5392_s.wn.imag) + (w.imag * CombineDFT_5392_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_split[6]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5384() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5376WEIGHTED_ROUND_ROBIN_Splitter_5384));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5385() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5385WEIGHTED_ROUND_ROBIN_Splitter_5393, pop_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5395(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_split[0], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5395_s.wn.real) - (w.imag * CombineDFT_5395_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5395_s.wn.imag) + (w.imag * CombineDFT_5395_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_split[0]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5396(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_split[1], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5396_s.wn.real) - (w.imag * CombineDFT_5396_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5396_s.wn.imag) + (w.imag * CombineDFT_5396_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_split[1]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5397(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_split[2], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5397_s.wn.real) - (w.imag * CombineDFT_5397_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5397_s.wn.imag) + (w.imag * CombineDFT_5397_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_split[2]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5398(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_split[3], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5398_s.wn.real) - (w.imag * CombineDFT_5398_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5398_s.wn.imag) + (w.imag * CombineDFT_5398_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_split[3]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5399(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_split[4], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_split[4], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5399_s.wn.real) - (w.imag * CombineDFT_5399_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5399_s.wn.imag) + (w.imag * CombineDFT_5399_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_split[4]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5400(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_split[5], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_split[5], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5400_s.wn.real) - (w.imag * CombineDFT_5400_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5400_s.wn.imag) + (w.imag * CombineDFT_5400_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_split[5]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5401(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_split[6], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_split[6], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5401_s.wn.real) - (w.imag * CombineDFT_5401_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5401_s.wn.imag) + (w.imag * CombineDFT_5401_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_split[6]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5393() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5385WEIGHTED_ROUND_ROBIN_Splitter_5393));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5394() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5394WEIGHTED_ROUND_ROBIN_Splitter_5402, pop_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5404(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_5419_5429_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_5419_5429_split[0], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5404_s.wn.real) - (w.imag * CombineDFT_5404_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5404_s.wn.imag) + (w.imag * CombineDFT_5404_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_5419_5429_split[0]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_5419_5429_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5405(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_5419_5429_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_5419_5429_split[1], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5405_s.wn.real) - (w.imag * CombineDFT_5405_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5405_s.wn.imag) + (w.imag * CombineDFT_5405_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_5419_5429_split[1]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_5419_5429_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5406(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_5419_5429_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_5419_5429_split[2], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5406_s.wn.real) - (w.imag * CombineDFT_5406_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5406_s.wn.imag) + (w.imag * CombineDFT_5406_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_5419_5429_split[2]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_5419_5429_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5407(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_5419_5429_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_5419_5429_split[3], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5407_s.wn.real) - (w.imag * CombineDFT_5407_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5407_s.wn.imag) + (w.imag * CombineDFT_5407_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_5419_5429_split[3]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_5419_5429_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5402() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin14_CombineDFT_Fiss_5419_5429_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5394WEIGHTED_ROUND_ROBIN_Splitter_5402));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5403() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5403WEIGHTED_ROUND_ROBIN_Splitter_5408, pop_complex(&SplitJoin14_CombineDFT_Fiss_5419_5429_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_5410(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[32];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 16, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_5420_5430_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_5420_5430_split[0], (16 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(16 + i)].real = (y0.real - y1w.real) ; 
			results[(16 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5410_s.wn.real) - (w.imag * CombineDFT_5410_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5410_s.wn.imag) + (w.imag * CombineDFT_5410_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin16_CombineDFT_Fiss_5420_5430_split[0]) ; 
			push_complex(&SplitJoin16_CombineDFT_Fiss_5420_5430_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_5411(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[32];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 16, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_5420_5430_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_5420_5430_split[1], (16 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(16 + i)].real = (y0.real - y1w.real) ; 
			results[(16 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5411_s.wn.real) - (w.imag * CombineDFT_5411_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5411_s.wn.imag) + (w.imag * CombineDFT_5411_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin16_CombineDFT_Fiss_5420_5430_split[1]) ; 
			push_complex(&SplitJoin16_CombineDFT_Fiss_5420_5430_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_5408() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_5420_5430_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5403WEIGHTED_ROUND_ROBIN_Splitter_5408));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_5420_5430_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5403WEIGHTED_ROUND_ROBIN_Splitter_5408));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_5409() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5409CombineDFT_5344, pop_complex(&SplitJoin16_CombineDFT_Fiss_5420_5430_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5409CombineDFT_5344, pop_complex(&SplitJoin16_CombineDFT_Fiss_5420_5430_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_5344(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[64];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5409CombineDFT_5344, i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5409CombineDFT_5344, (32 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(32 + i)].real = (y0.real - y1w.real) ; 
			results[(32 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_5344_s.wn.real) - (w.imag * CombineDFT_5344_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_5344_s.wn.imag) + (w.imag * CombineDFT_5344_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5409CombineDFT_5344) ; 
			push_complex(&CombineDFT_5344CPrinter_5345, results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CPrinter_5345(){
	FOR(uint32_t, __iter_steady_, 0, <, 448, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&CombineDFT_5344CPrinter_5345));
		printf("%.10f", c.real);
		printf("\n");
		printf("%.10f", c.imag);
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 7, __iter_init_0_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_join[__iter_init_0_]);
	ENDFOR
	init_buffer_complex(&FFTTestSource_5333FFTReorderSimple_5334);
	FOR(int, __iter_init_1_, 0, <, 7, __iter_init_1_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_join[__iter_init_1_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5352WEIGHTED_ROUND_ROBIN_Splitter_5357);
	init_buffer_complex(&FFTReorderSimple_5334WEIGHTED_ROUND_ROBIN_Splitter_5347);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5376WEIGHTED_ROUND_ROBIN_Splitter_5384);
	FOR(int, __iter_init_2_, 0, <, 4, __iter_init_2_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_5419_5429_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 4, __iter_init_3_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_5419_5429_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 2, __iter_init_4_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_5420_5430_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 7, __iter_init_5_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 7, __iter_init_6_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_5418_5428_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 4, __iter_init_7_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_5413_5423_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 4, __iter_init_8_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_5413_5423_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 2, __iter_init_9_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_5412_5422_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_5412_5422_split[__iter_init_10_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5409CombineDFT_5344);
	FOR(int, __iter_init_11_, 0, <, 7, __iter_init_11_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_5416_5426_split[__iter_init_11_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5348WEIGHTED_ROUND_ROBIN_Splitter_5351);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5358WEIGHTED_ROUND_ROBIN_Splitter_5366);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5385WEIGHTED_ROUND_ROBIN_Splitter_5393);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5367WEIGHTED_ROUND_ROBIN_Splitter_5375);
	FOR(int, __iter_init_12_, 0, <, 7, __iter_init_12_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_5415_5425_split[__iter_init_12_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5403WEIGHTED_ROUND_ROBIN_Splitter_5408);
	FOR(int, __iter_init_13_, 0, <, 7, __iter_init_13_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_split[__iter_init_13_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_5394WEIGHTED_ROUND_ROBIN_Splitter_5402);
	FOR(int, __iter_init_14_, 0, <, 7, __iter_init_14_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_join[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 7, __iter_init_15_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_5414_5424_split[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_5420_5430_join[__iter_init_16_]);
	ENDFOR
	init_buffer_complex(&CombineDFT_5344CPrinter_5345);
	FOR(int, __iter_init_17_, 0, <, 7, __iter_init_17_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_5417_5427_join[__iter_init_17_]);
	ENDFOR
// --- init: CombineDFT_5377
	 {
	 ; 
	CombineDFT_5377_s.wn.real = -1.0 ; 
	CombineDFT_5377_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5378
	 {
	 ; 
	CombineDFT_5378_s.wn.real = -1.0 ; 
	CombineDFT_5378_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5379
	 {
	 ; 
	CombineDFT_5379_s.wn.real = -1.0 ; 
	CombineDFT_5379_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5380
	 {
	 ; 
	CombineDFT_5380_s.wn.real = -1.0 ; 
	CombineDFT_5380_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5381
	 {
	 ; 
	CombineDFT_5381_s.wn.real = -1.0 ; 
	CombineDFT_5381_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5382
	 {
	 ; 
	CombineDFT_5382_s.wn.real = -1.0 ; 
	CombineDFT_5382_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5383
	 {
	 ; 
	CombineDFT_5383_s.wn.real = -1.0 ; 
	CombineDFT_5383_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_5386
	 {
	 ; 
	CombineDFT_5386_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5386_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5387
	 {
	 ; 
	CombineDFT_5387_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5387_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5388
	 {
	 ; 
	CombineDFT_5388_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5388_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5389
	 {
	 ; 
	CombineDFT_5389_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5389_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5390
	 {
	 ; 
	CombineDFT_5390_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5390_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5391
	 {
	 ; 
	CombineDFT_5391_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5391_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5392
	 {
	 ; 
	CombineDFT_5392_s.wn.real = -4.371139E-8 ; 
	CombineDFT_5392_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_5395
	 {
	 ; 
	CombineDFT_5395_s.wn.real = 0.70710677 ; 
	CombineDFT_5395_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5396
	 {
	 ; 
	CombineDFT_5396_s.wn.real = 0.70710677 ; 
	CombineDFT_5396_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5397
	 {
	 ; 
	CombineDFT_5397_s.wn.real = 0.70710677 ; 
	CombineDFT_5397_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5398
	 {
	 ; 
	CombineDFT_5398_s.wn.real = 0.70710677 ; 
	CombineDFT_5398_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5399
	 {
	 ; 
	CombineDFT_5399_s.wn.real = 0.70710677 ; 
	CombineDFT_5399_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5400
	 {
	 ; 
	CombineDFT_5400_s.wn.real = 0.70710677 ; 
	CombineDFT_5400_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5401
	 {
	 ; 
	CombineDFT_5401_s.wn.real = 0.70710677 ; 
	CombineDFT_5401_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_5404
	 {
	 ; 
	CombineDFT_5404_s.wn.real = 0.9238795 ; 
	CombineDFT_5404_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_5405
	 {
	 ; 
	CombineDFT_5405_s.wn.real = 0.9238795 ; 
	CombineDFT_5405_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_5406
	 {
	 ; 
	CombineDFT_5406_s.wn.real = 0.9238795 ; 
	CombineDFT_5406_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_5407
	 {
	 ; 
	CombineDFT_5407_s.wn.real = 0.9238795 ; 
	CombineDFT_5407_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_5410
	 {
	 ; 
	CombineDFT_5410_s.wn.real = 0.98078525 ; 
	CombineDFT_5410_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_5411
	 {
	 ; 
	CombineDFT_5411_s.wn.real = 0.98078525 ; 
	CombineDFT_5411_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_5344
	 {
	 ; 
	CombineDFT_5344_s.wn.real = 0.9951847 ; 
	CombineDFT_5344_s.wn.imag = -0.09801714 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FFTTestSource_5333();
		FFTReorderSimple_5334();
		WEIGHTED_ROUND_ROBIN_Splitter_5347();
			FFTReorderSimple_5349();
			FFTReorderSimple_5350();
		WEIGHTED_ROUND_ROBIN_Joiner_5348();
		WEIGHTED_ROUND_ROBIN_Splitter_5351();
			FFTReorderSimple_5353();
			FFTReorderSimple_5354();
			FFTReorderSimple_5355();
			FFTReorderSimple_5356();
		WEIGHTED_ROUND_ROBIN_Joiner_5352();
		WEIGHTED_ROUND_ROBIN_Splitter_5357();
			FFTReorderSimple_5359();
			FFTReorderSimple_5360();
			FFTReorderSimple_5361();
			FFTReorderSimple_5362();
			FFTReorderSimple_5363();
			FFTReorderSimple_5364();
			FFTReorderSimple_5365();
		WEIGHTED_ROUND_ROBIN_Joiner_5358();
		WEIGHTED_ROUND_ROBIN_Splitter_5366();
			FFTReorderSimple_5368();
			FFTReorderSimple_5369();
			FFTReorderSimple_5370();
			FFTReorderSimple_5371();
			FFTReorderSimple_5372();
			FFTReorderSimple_5373();
			FFTReorderSimple_5374();
		WEIGHTED_ROUND_ROBIN_Joiner_5367();
		WEIGHTED_ROUND_ROBIN_Splitter_5375();
			CombineDFT_5377();
			CombineDFT_5378();
			CombineDFT_5379();
			CombineDFT_5380();
			CombineDFT_5381();
			CombineDFT_5382();
			CombineDFT_5383();
		WEIGHTED_ROUND_ROBIN_Joiner_5376();
		WEIGHTED_ROUND_ROBIN_Splitter_5384();
			CombineDFT_5386();
			CombineDFT_5387();
			CombineDFT_5388();
			CombineDFT_5389();
			CombineDFT_5390();
			CombineDFT_5391();
			CombineDFT_5392();
		WEIGHTED_ROUND_ROBIN_Joiner_5385();
		WEIGHTED_ROUND_ROBIN_Splitter_5393();
			CombineDFT_5395();
			CombineDFT_5396();
			CombineDFT_5397();
			CombineDFT_5398();
			CombineDFT_5399();
			CombineDFT_5400();
			CombineDFT_5401();
		WEIGHTED_ROUND_ROBIN_Joiner_5394();
		WEIGHTED_ROUND_ROBIN_Splitter_5402();
			CombineDFT_5404();
			CombineDFT_5405();
			CombineDFT_5406();
			CombineDFT_5407();
		WEIGHTED_ROUND_ROBIN_Joiner_5403();
		WEIGHTED_ROUND_ROBIN_Splitter_5408();
			CombineDFT_5410();
			CombineDFT_5411();
		WEIGHTED_ROUND_ROBIN_Joiner_5409();
		CombineDFT_5344();
		CPrinter_5345();
	ENDFOR
	return EXIT_SUCCESS;
}
