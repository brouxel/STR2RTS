#include "PEG28-FFT6.h"

buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_1145_1155_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1040WEIGHTED_ROUND_ROBIN_Splitter_1043;
buffer_complex_t SplitJoin12_CombineDFT_Fiss_1151_1161_split[8];
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_1148_1158_join[16];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_1152_1162_join[4];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_1153_1163_split[2];
buffer_complex_t SplitJoin8_CombineDFT_Fiss_1149_1159_split[28];
buffer_complex_t SplitJoin8_CombineDFT_Fiss_1149_1159_join[28];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1142CombineDFT_1036;
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_1147_1157_split[8];
buffer_complex_t FFTReorderSimple_1026WEIGHTED_ROUND_ROBIN_Splitter_1039;
buffer_complex_t FFTTestSource_1025FFTReorderSimple_1026;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1126WEIGHTED_ROUND_ROBIN_Splitter_1135;
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_1146_1156_split[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1136WEIGHTED_ROUND_ROBIN_Splitter_1141;
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_1146_1156_join[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1060WEIGHTED_ROUND_ROBIN_Splitter_1077;
buffer_complex_t SplitJoin14_CombineDFT_Fiss_1152_1162_split[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1044WEIGHTED_ROUND_ROBIN_Splitter_1049;
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_1148_1158_split[16];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1078WEIGHTED_ROUND_ROBIN_Splitter_1107;
buffer_complex_t SplitJoin10_CombineDFT_Fiss_1150_1160_split[16];
buffer_complex_t SplitJoin12_CombineDFT_Fiss_1151_1161_join[8];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_1150_1160_join[16];
buffer_complex_t CombineDFT_1036CPrinter_1037;
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_1145_1155_join[2];
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_1147_1157_join[8];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1050WEIGHTED_ROUND_ROBIN_Splitter_1059;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_1108WEIGHTED_ROUND_ROBIN_Splitter_1125;
buffer_complex_t SplitJoin16_CombineDFT_Fiss_1153_1163_join[2];


CombineDFT_1079_t CombineDFT_1079_s;
CombineDFT_1079_t CombineDFT_1080_s;
CombineDFT_1079_t CombineDFT_1081_s;
CombineDFT_1079_t CombineDFT_1082_s;
CombineDFT_1079_t CombineDFT_1083_s;
CombineDFT_1079_t CombineDFT_1084_s;
CombineDFT_1079_t CombineDFT_1085_s;
CombineDFT_1079_t CombineDFT_1086_s;
CombineDFT_1079_t CombineDFT_1087_s;
CombineDFT_1079_t CombineDFT_1088_s;
CombineDFT_1079_t CombineDFT_1089_s;
CombineDFT_1079_t CombineDFT_1090_s;
CombineDFT_1079_t CombineDFT_1091_s;
CombineDFT_1079_t CombineDFT_1092_s;
CombineDFT_1079_t CombineDFT_1093_s;
CombineDFT_1079_t CombineDFT_1094_s;
CombineDFT_1079_t CombineDFT_1095_s;
CombineDFT_1079_t CombineDFT_1096_s;
CombineDFT_1079_t CombineDFT_1097_s;
CombineDFT_1079_t CombineDFT_1098_s;
CombineDFT_1079_t CombineDFT_1099_s;
CombineDFT_1079_t CombineDFT_1100_s;
CombineDFT_1079_t CombineDFT_1101_s;
CombineDFT_1079_t CombineDFT_1102_s;
CombineDFT_1079_t CombineDFT_1103_s;
CombineDFT_1079_t CombineDFT_1104_s;
CombineDFT_1079_t CombineDFT_1105_s;
CombineDFT_1079_t CombineDFT_1106_s;
CombineDFT_1079_t CombineDFT_1109_s;
CombineDFT_1079_t CombineDFT_1110_s;
CombineDFT_1079_t CombineDFT_1111_s;
CombineDFT_1079_t CombineDFT_1112_s;
CombineDFT_1079_t CombineDFT_1113_s;
CombineDFT_1079_t CombineDFT_1114_s;
CombineDFT_1079_t CombineDFT_1115_s;
CombineDFT_1079_t CombineDFT_1116_s;
CombineDFT_1079_t CombineDFT_1117_s;
CombineDFT_1079_t CombineDFT_1118_s;
CombineDFT_1079_t CombineDFT_1119_s;
CombineDFT_1079_t CombineDFT_1120_s;
CombineDFT_1079_t CombineDFT_1121_s;
CombineDFT_1079_t CombineDFT_1122_s;
CombineDFT_1079_t CombineDFT_1123_s;
CombineDFT_1079_t CombineDFT_1124_s;
CombineDFT_1079_t CombineDFT_1127_s;
CombineDFT_1079_t CombineDFT_1128_s;
CombineDFT_1079_t CombineDFT_1129_s;
CombineDFT_1079_t CombineDFT_1130_s;
CombineDFT_1079_t CombineDFT_1131_s;
CombineDFT_1079_t CombineDFT_1132_s;
CombineDFT_1079_t CombineDFT_1133_s;
CombineDFT_1079_t CombineDFT_1134_s;
CombineDFT_1079_t CombineDFT_1137_s;
CombineDFT_1079_t CombineDFT_1138_s;
CombineDFT_1079_t CombineDFT_1139_s;
CombineDFT_1079_t CombineDFT_1140_s;
CombineDFT_1079_t CombineDFT_1143_s;
CombineDFT_1079_t CombineDFT_1144_s;
CombineDFT_1079_t CombineDFT_1036_s;

void FFTTestSource(buffer_complex_t *chanout) {
		complex_t c1;
		complex_t zero;
		c1.real = 1.0 ; 
		c1.imag = 0.0 ; 
		zero.real = 0.0 ; 
		zero.imag = 0.0 ; 
		push_complex(&(*chanout), zero) ; 
		push_complex(&(*chanout), c1) ; 
		FOR(int, i, 0,  < , 62, i++) {
			push_complex(&(*chanout), zero) ; 
		}
		ENDFOR
	}


void FFTTestSource_1025() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTTestSource(&(FFTTestSource_1025FFTReorderSimple_1026));
	ENDFOR
}

void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		FOR(int, i, 0,  < , 64, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 64, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			push_complex(&(*chanout), __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&(*chanin)) ; 
		}
		ENDFOR
	}


void FFTReorderSimple_1026() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(FFTTestSource_1025FFTReorderSimple_1026), &(FFTReorderSimple_1026WEIGHTED_ROUND_ROBIN_Splitter_1039));
	ENDFOR
}

void FFTReorderSimple_1041() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_1145_1155_split[0]), &(SplitJoin0_FFTReorderSimple_Fiss_1145_1155_join[0]));
	ENDFOR
}

void FFTReorderSimple_1042() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin0_FFTReorderSimple_Fiss_1145_1155_split[1]), &(SplitJoin0_FFTReorderSimple_Fiss_1145_1155_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1039() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_1145_1155_split[0], pop_complex(&FFTReorderSimple_1026WEIGHTED_ROUND_ROBIN_Splitter_1039));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_1145_1155_split[1], pop_complex(&FFTReorderSimple_1026WEIGHTED_ROUND_ROBIN_Splitter_1039));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1040() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1040WEIGHTED_ROUND_ROBIN_Splitter_1043, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_1145_1155_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1040WEIGHTED_ROUND_ROBIN_Splitter_1043, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_1145_1155_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_1045() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_1146_1156_split[0]), &(SplitJoin2_FFTReorderSimple_Fiss_1146_1156_join[0]));
	ENDFOR
}

void FFTReorderSimple_1046() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_1146_1156_split[1]), &(SplitJoin2_FFTReorderSimple_Fiss_1146_1156_join[1]));
	ENDFOR
}

void FFTReorderSimple_1047() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_1146_1156_split[2]), &(SplitJoin2_FFTReorderSimple_Fiss_1146_1156_join[2]));
	ENDFOR
}

void FFTReorderSimple_1048() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_FFTReorderSimple_Fiss_1146_1156_split[3]), &(SplitJoin2_FFTReorderSimple_Fiss_1146_1156_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1043() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin2_FFTReorderSimple_Fiss_1146_1156_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1040WEIGHTED_ROUND_ROBIN_Splitter_1043));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1044() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1044WEIGHTED_ROUND_ROBIN_Splitter_1049, pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_1146_1156_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_1051() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_1147_1157_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_1147_1157_join[0]));
	ENDFOR
}

void FFTReorderSimple_1052() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_1147_1157_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_1147_1157_join[1]));
	ENDFOR
}

void FFTReorderSimple_1053() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_1147_1157_split[2]), &(SplitJoin4_FFTReorderSimple_Fiss_1147_1157_join[2]));
	ENDFOR
}

void FFTReorderSimple_1054() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_1147_1157_split[3]), &(SplitJoin4_FFTReorderSimple_Fiss_1147_1157_join[3]));
	ENDFOR
}

void FFTReorderSimple_1055() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_1147_1157_split[4]), &(SplitJoin4_FFTReorderSimple_Fiss_1147_1157_join[4]));
	ENDFOR
}

void FFTReorderSimple_1056() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_1147_1157_split[5]), &(SplitJoin4_FFTReorderSimple_Fiss_1147_1157_join[5]));
	ENDFOR
}

void FFTReorderSimple_1057() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_1147_1157_split[6]), &(SplitJoin4_FFTReorderSimple_Fiss_1147_1157_join[6]));
	ENDFOR
}

void FFTReorderSimple_1058() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_1147_1157_split[7]), &(SplitJoin4_FFTReorderSimple_Fiss_1147_1157_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1049() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin4_FFTReorderSimple_Fiss_1147_1157_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1044WEIGHTED_ROUND_ROBIN_Splitter_1049));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1050() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1050WEIGHTED_ROUND_ROBIN_Splitter_1059, pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_1147_1157_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_1061() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_join[0]));
	ENDFOR
}

void FFTReorderSimple_1062() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_join[1]));
	ENDFOR
}

void FFTReorderSimple_1063() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_join[2]));
	ENDFOR
}

void FFTReorderSimple_1064() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_join[3]));
	ENDFOR
}

void FFTReorderSimple_1065() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_split[4]), &(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_join[4]));
	ENDFOR
}

void FFTReorderSimple_1066() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_split[5]), &(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_join[5]));
	ENDFOR
}

void FFTReorderSimple_1067() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_split[6]), &(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_join[6]));
	ENDFOR
}

void FFTReorderSimple_1068() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_split[7]), &(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_join[7]));
	ENDFOR
}

void FFTReorderSimple_1069() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_split[8]), &(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_join[8]));
	ENDFOR
}

void FFTReorderSimple_1070() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_split[9]), &(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_join[9]));
	ENDFOR
}

void FFTReorderSimple_1071() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_split[10]), &(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_join[10]));
	ENDFOR
}

void FFTReorderSimple_1072() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_split[11]), &(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_join[11]));
	ENDFOR
}

void FFTReorderSimple_1073() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_split[12]), &(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_join[12]));
	ENDFOR
}

void FFTReorderSimple_1074() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_split[13]), &(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_join[13]));
	ENDFOR
}

void FFTReorderSimple_1075() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_split[14]), &(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_join[14]));
	ENDFOR
}

void FFTReorderSimple_1076() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_split[15]), &(SplitJoin6_FFTReorderSimple_Fiss_1148_1158_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1059() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin6_FFTReorderSimple_Fiss_1148_1158_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1050WEIGHTED_ROUND_ROBIN_Splitter_1059));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1060() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1060WEIGHTED_ROUND_ROBIN_Splitter_1077, pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_1148_1158_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&(*chanin), i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&(*chanin), (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_1079_s.wn.real) - (w.imag * CombineDFT_1079_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_1079_s.wn.imag) + (w.imag * CombineDFT_1079_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&(*chanin)) ; 
			push_complex(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_1079() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1149_1159_split[0]), &(SplitJoin8_CombineDFT_Fiss_1149_1159_join[0]));
	ENDFOR
}

void CombineDFT_1080() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1149_1159_split[1]), &(SplitJoin8_CombineDFT_Fiss_1149_1159_join[1]));
	ENDFOR
}

void CombineDFT_1081() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1149_1159_split[2]), &(SplitJoin8_CombineDFT_Fiss_1149_1159_join[2]));
	ENDFOR
}

void CombineDFT_1082() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1149_1159_split[3]), &(SplitJoin8_CombineDFT_Fiss_1149_1159_join[3]));
	ENDFOR
}

void CombineDFT_1083() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1149_1159_split[4]), &(SplitJoin8_CombineDFT_Fiss_1149_1159_join[4]));
	ENDFOR
}

void CombineDFT_1084() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1149_1159_split[5]), &(SplitJoin8_CombineDFT_Fiss_1149_1159_join[5]));
	ENDFOR
}

void CombineDFT_1085() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1149_1159_split[6]), &(SplitJoin8_CombineDFT_Fiss_1149_1159_join[6]));
	ENDFOR
}

void CombineDFT_1086() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1149_1159_split[7]), &(SplitJoin8_CombineDFT_Fiss_1149_1159_join[7]));
	ENDFOR
}

void CombineDFT_1087() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1149_1159_split[8]), &(SplitJoin8_CombineDFT_Fiss_1149_1159_join[8]));
	ENDFOR
}

void CombineDFT_1088() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1149_1159_split[9]), &(SplitJoin8_CombineDFT_Fiss_1149_1159_join[9]));
	ENDFOR
}

void CombineDFT_1089() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1149_1159_split[10]), &(SplitJoin8_CombineDFT_Fiss_1149_1159_join[10]));
	ENDFOR
}

void CombineDFT_1090() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1149_1159_split[11]), &(SplitJoin8_CombineDFT_Fiss_1149_1159_join[11]));
	ENDFOR
}

void CombineDFT_1091() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1149_1159_split[12]), &(SplitJoin8_CombineDFT_Fiss_1149_1159_join[12]));
	ENDFOR
}

void CombineDFT_1092() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1149_1159_split[13]), &(SplitJoin8_CombineDFT_Fiss_1149_1159_join[13]));
	ENDFOR
}

void CombineDFT_1093() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1149_1159_split[14]), &(SplitJoin8_CombineDFT_Fiss_1149_1159_join[14]));
	ENDFOR
}

void CombineDFT_1094() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1149_1159_split[15]), &(SplitJoin8_CombineDFT_Fiss_1149_1159_join[15]));
	ENDFOR
}

void CombineDFT_1095() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1149_1159_split[16]), &(SplitJoin8_CombineDFT_Fiss_1149_1159_join[16]));
	ENDFOR
}

void CombineDFT_1096() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1149_1159_split[17]), &(SplitJoin8_CombineDFT_Fiss_1149_1159_join[17]));
	ENDFOR
}

void CombineDFT_1097() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1149_1159_split[18]), &(SplitJoin8_CombineDFT_Fiss_1149_1159_join[18]));
	ENDFOR
}

void CombineDFT_1098() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1149_1159_split[19]), &(SplitJoin8_CombineDFT_Fiss_1149_1159_join[19]));
	ENDFOR
}

void CombineDFT_1099() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1149_1159_split[20]), &(SplitJoin8_CombineDFT_Fiss_1149_1159_join[20]));
	ENDFOR
}

void CombineDFT_1100() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1149_1159_split[21]), &(SplitJoin8_CombineDFT_Fiss_1149_1159_join[21]));
	ENDFOR
}

void CombineDFT_1101() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1149_1159_split[22]), &(SplitJoin8_CombineDFT_Fiss_1149_1159_join[22]));
	ENDFOR
}

void CombineDFT_1102() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1149_1159_split[23]), &(SplitJoin8_CombineDFT_Fiss_1149_1159_join[23]));
	ENDFOR
}

void CombineDFT_1103() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1149_1159_split[24]), &(SplitJoin8_CombineDFT_Fiss_1149_1159_join[24]));
	ENDFOR
}

void CombineDFT_1104() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1149_1159_split[25]), &(SplitJoin8_CombineDFT_Fiss_1149_1159_join[25]));
	ENDFOR
}

void CombineDFT_1105() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1149_1159_split[26]), &(SplitJoin8_CombineDFT_Fiss_1149_1159_join[26]));
	ENDFOR
}

void CombineDFT_1106() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin8_CombineDFT_Fiss_1149_1159_split[27]), &(SplitJoin8_CombineDFT_Fiss_1149_1159_join[27]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1077() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 28, __iter_++)
			push_complex(&SplitJoin8_CombineDFT_Fiss_1149_1159_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1060WEIGHTED_ROUND_ROBIN_Splitter_1077));
			push_complex(&SplitJoin8_CombineDFT_Fiss_1149_1159_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1060WEIGHTED_ROUND_ROBIN_Splitter_1077));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1078() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 28, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1078WEIGHTED_ROUND_ROBIN_Splitter_1107, pop_complex(&SplitJoin8_CombineDFT_Fiss_1149_1159_join[__iter_]));
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1078WEIGHTED_ROUND_ROBIN_Splitter_1107, pop_complex(&SplitJoin8_CombineDFT_Fiss_1149_1159_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_1109() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1150_1160_split[0]), &(SplitJoin10_CombineDFT_Fiss_1150_1160_join[0]));
	ENDFOR
}

void CombineDFT_1110() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1150_1160_split[1]), &(SplitJoin10_CombineDFT_Fiss_1150_1160_join[1]));
	ENDFOR
}

void CombineDFT_1111() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1150_1160_split[2]), &(SplitJoin10_CombineDFT_Fiss_1150_1160_join[2]));
	ENDFOR
}

void CombineDFT_1112() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1150_1160_split[3]), &(SplitJoin10_CombineDFT_Fiss_1150_1160_join[3]));
	ENDFOR
}

void CombineDFT_1113() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1150_1160_split[4]), &(SplitJoin10_CombineDFT_Fiss_1150_1160_join[4]));
	ENDFOR
}

void CombineDFT_1114() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1150_1160_split[5]), &(SplitJoin10_CombineDFT_Fiss_1150_1160_join[5]));
	ENDFOR
}

void CombineDFT_1115() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1150_1160_split[6]), &(SplitJoin10_CombineDFT_Fiss_1150_1160_join[6]));
	ENDFOR
}

void CombineDFT_1116() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1150_1160_split[7]), &(SplitJoin10_CombineDFT_Fiss_1150_1160_join[7]));
	ENDFOR
}

void CombineDFT_1117() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1150_1160_split[8]), &(SplitJoin10_CombineDFT_Fiss_1150_1160_join[8]));
	ENDFOR
}

void CombineDFT_1118() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1150_1160_split[9]), &(SplitJoin10_CombineDFT_Fiss_1150_1160_join[9]));
	ENDFOR
}

void CombineDFT_1119() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1150_1160_split[10]), &(SplitJoin10_CombineDFT_Fiss_1150_1160_join[10]));
	ENDFOR
}

void CombineDFT_1120() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1150_1160_split[11]), &(SplitJoin10_CombineDFT_Fiss_1150_1160_join[11]));
	ENDFOR
}

void CombineDFT_1121() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1150_1160_split[12]), &(SplitJoin10_CombineDFT_Fiss_1150_1160_join[12]));
	ENDFOR
}

void CombineDFT_1122() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1150_1160_split[13]), &(SplitJoin10_CombineDFT_Fiss_1150_1160_join[13]));
	ENDFOR
}

void CombineDFT_1123() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1150_1160_split[14]), &(SplitJoin10_CombineDFT_Fiss_1150_1160_join[14]));
	ENDFOR
}

void CombineDFT_1124() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin10_CombineDFT_Fiss_1150_1160_split[15]), &(SplitJoin10_CombineDFT_Fiss_1150_1160_join[15]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1107() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin10_CombineDFT_Fiss_1150_1160_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1078WEIGHTED_ROUND_ROBIN_Splitter_1107));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1108() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 16, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1108WEIGHTED_ROUND_ROBIN_Splitter_1125, pop_complex(&SplitJoin10_CombineDFT_Fiss_1150_1160_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_1127() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1151_1161_split[0]), &(SplitJoin12_CombineDFT_Fiss_1151_1161_join[0]));
	ENDFOR
}

void CombineDFT_1128() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1151_1161_split[1]), &(SplitJoin12_CombineDFT_Fiss_1151_1161_join[1]));
	ENDFOR
}

void CombineDFT_1129() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1151_1161_split[2]), &(SplitJoin12_CombineDFT_Fiss_1151_1161_join[2]));
	ENDFOR
}

void CombineDFT_1130() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1151_1161_split[3]), &(SplitJoin12_CombineDFT_Fiss_1151_1161_join[3]));
	ENDFOR
}

void CombineDFT_1131() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1151_1161_split[4]), &(SplitJoin12_CombineDFT_Fiss_1151_1161_join[4]));
	ENDFOR
}

void CombineDFT_1132() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1151_1161_split[5]), &(SplitJoin12_CombineDFT_Fiss_1151_1161_join[5]));
	ENDFOR
}

void CombineDFT_1133() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1151_1161_split[6]), &(SplitJoin12_CombineDFT_Fiss_1151_1161_join[6]));
	ENDFOR
}

void CombineDFT_1134() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_1151_1161_split[7]), &(SplitJoin12_CombineDFT_Fiss_1151_1161_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1125() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin12_CombineDFT_Fiss_1151_1161_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1108WEIGHTED_ROUND_ROBIN_Splitter_1125));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1126() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1126WEIGHTED_ROUND_ROBIN_Splitter_1135, pop_complex(&SplitJoin12_CombineDFT_Fiss_1151_1161_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_1137() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_1152_1162_split[0]), &(SplitJoin14_CombineDFT_Fiss_1152_1162_join[0]));
	ENDFOR
}

void CombineDFT_1138() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_1152_1162_split[1]), &(SplitJoin14_CombineDFT_Fiss_1152_1162_join[1]));
	ENDFOR
}

void CombineDFT_1139() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_1152_1162_split[2]), &(SplitJoin14_CombineDFT_Fiss_1152_1162_join[2]));
	ENDFOR
}

void CombineDFT_1140() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_1152_1162_split[3]), &(SplitJoin14_CombineDFT_Fiss_1152_1162_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1135() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin14_CombineDFT_Fiss_1152_1162_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1126WEIGHTED_ROUND_ROBIN_Splitter_1135));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1136() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1136WEIGHTED_ROUND_ROBIN_Splitter_1141, pop_complex(&SplitJoin14_CombineDFT_Fiss_1152_1162_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_1143() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_1153_1163_split[0]), &(SplitJoin16_CombineDFT_Fiss_1153_1163_join[0]));
	ENDFOR
}

void CombineDFT_1144() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_1153_1163_split[1]), &(SplitJoin16_CombineDFT_Fiss_1153_1163_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1141() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_1153_1163_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1136WEIGHTED_ROUND_ROBIN_Splitter_1141));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_1153_1163_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1136WEIGHTED_ROUND_ROBIN_Splitter_1141));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1142() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1142CombineDFT_1036, pop_complex(&SplitJoin16_CombineDFT_Fiss_1153_1163_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1142CombineDFT_1036, pop_complex(&SplitJoin16_CombineDFT_Fiss_1153_1163_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_1036() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_1142CombineDFT_1036), &(CombineDFT_1036CPrinter_1037));
	ENDFOR
}

void CPrinter(buffer_complex_t *chanin) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		printf("%.10f", c.real);
		printf("\n");
		printf("%.10f", c.imag);
		printf("\n");
	}


void CPrinter_1037() {
	FOR(uint32_t, __iter_steady_, 0, <, 448, __iter_steady_++)
		CPrinter(&(CombineDFT_1036CPrinter_1037));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_1145_1155_split[__iter_init_0_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1040WEIGHTED_ROUND_ROBIN_Splitter_1043);
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_1151_1161_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 16, __iter_init_2_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_1148_1158_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 4, __iter_init_3_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_1152_1162_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 2, __iter_init_4_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_1153_1163_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 28, __iter_init_5_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_1149_1159_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 28, __iter_init_6_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_1149_1159_join[__iter_init_6_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1142CombineDFT_1036);
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_1147_1157_split[__iter_init_7_]);
	ENDFOR
	init_buffer_complex(&FFTReorderSimple_1026WEIGHTED_ROUND_ROBIN_Splitter_1039);
	init_buffer_complex(&FFTTestSource_1025FFTReorderSimple_1026);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1126WEIGHTED_ROUND_ROBIN_Splitter_1135);
	FOR(int, __iter_init_8_, 0, <, 4, __iter_init_8_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_1146_1156_split[__iter_init_8_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1136WEIGHTED_ROUND_ROBIN_Splitter_1141);
	FOR(int, __iter_init_9_, 0, <, 4, __iter_init_9_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_1146_1156_join[__iter_init_9_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1060WEIGHTED_ROUND_ROBIN_Splitter_1077);
	FOR(int, __iter_init_10_, 0, <, 4, __iter_init_10_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_1152_1162_split[__iter_init_10_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1044WEIGHTED_ROUND_ROBIN_Splitter_1049);
	FOR(int, __iter_init_11_, 0, <, 16, __iter_init_11_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_1148_1158_split[__iter_init_11_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1078WEIGHTED_ROUND_ROBIN_Splitter_1107);
	FOR(int, __iter_init_12_, 0, <, 16, __iter_init_12_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_1150_1160_split[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 8, __iter_init_13_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_1151_1161_join[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 16, __iter_init_14_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_1150_1160_join[__iter_init_14_]);
	ENDFOR
	init_buffer_complex(&CombineDFT_1036CPrinter_1037);
	FOR(int, __iter_init_15_, 0, <, 2, __iter_init_15_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_1145_1155_join[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 8, __iter_init_16_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_1147_1157_join[__iter_init_16_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1050WEIGHTED_ROUND_ROBIN_Splitter_1059);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_1108WEIGHTED_ROUND_ROBIN_Splitter_1125);
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_1153_1163_join[__iter_init_17_]);
	ENDFOR
// --- init: CombineDFT_1079
	 {
	 ; 
	CombineDFT_1079_s.wn.real = -1.0 ; 
	CombineDFT_1079_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1080
	 {
	CombineDFT_1080_s.wn.real = -1.0 ; 
	CombineDFT_1080_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1081
	 {
	CombineDFT_1081_s.wn.real = -1.0 ; 
	CombineDFT_1081_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1082
	 {
	CombineDFT_1082_s.wn.real = -1.0 ; 
	CombineDFT_1082_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1083
	 {
	CombineDFT_1083_s.wn.real = -1.0 ; 
	CombineDFT_1083_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1084
	 {
	CombineDFT_1084_s.wn.real = -1.0 ; 
	CombineDFT_1084_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1085
	 {
	CombineDFT_1085_s.wn.real = -1.0 ; 
	CombineDFT_1085_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1086
	 {
	CombineDFT_1086_s.wn.real = -1.0 ; 
	CombineDFT_1086_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1087
	 {
	CombineDFT_1087_s.wn.real = -1.0 ; 
	CombineDFT_1087_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1088
	 {
	CombineDFT_1088_s.wn.real = -1.0 ; 
	CombineDFT_1088_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1089
	 {
	CombineDFT_1089_s.wn.real = -1.0 ; 
	CombineDFT_1089_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1090
	 {
	CombineDFT_1090_s.wn.real = -1.0 ; 
	CombineDFT_1090_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1091
	 {
	CombineDFT_1091_s.wn.real = -1.0 ; 
	CombineDFT_1091_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1092
	 {
	CombineDFT_1092_s.wn.real = -1.0 ; 
	CombineDFT_1092_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1093
	 {
	CombineDFT_1093_s.wn.real = -1.0 ; 
	CombineDFT_1093_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1094
	 {
	CombineDFT_1094_s.wn.real = -1.0 ; 
	CombineDFT_1094_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1095
	 {
	CombineDFT_1095_s.wn.real = -1.0 ; 
	CombineDFT_1095_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1096
	 {
	CombineDFT_1096_s.wn.real = -1.0 ; 
	CombineDFT_1096_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1097
	 {
	CombineDFT_1097_s.wn.real = -1.0 ; 
	CombineDFT_1097_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1098
	 {
	CombineDFT_1098_s.wn.real = -1.0 ; 
	CombineDFT_1098_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1099
	 {
	CombineDFT_1099_s.wn.real = -1.0 ; 
	CombineDFT_1099_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1100
	 {
	CombineDFT_1100_s.wn.real = -1.0 ; 
	CombineDFT_1100_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1101
	 {
	CombineDFT_1101_s.wn.real = -1.0 ; 
	CombineDFT_1101_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1102
	 {
	CombineDFT_1102_s.wn.real = -1.0 ; 
	CombineDFT_1102_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1103
	 {
	CombineDFT_1103_s.wn.real = -1.0 ; 
	CombineDFT_1103_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1104
	 {
	CombineDFT_1104_s.wn.real = -1.0 ; 
	CombineDFT_1104_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1105
	 {
	CombineDFT_1105_s.wn.real = -1.0 ; 
	CombineDFT_1105_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1106
	 {
	CombineDFT_1106_s.wn.real = -1.0 ; 
	CombineDFT_1106_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_1109
	 {
	CombineDFT_1109_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1109_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1110
	 {
	CombineDFT_1110_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1110_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1111
	 {
	CombineDFT_1111_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1111_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1112
	 {
	CombineDFT_1112_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1112_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1113
	 {
	CombineDFT_1113_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1113_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1114
	 {
	CombineDFT_1114_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1114_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1115
	 {
	CombineDFT_1115_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1115_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1116
	 {
	CombineDFT_1116_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1116_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1117
	 {
	CombineDFT_1117_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1117_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1118
	 {
	CombineDFT_1118_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1118_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1119
	 {
	CombineDFT_1119_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1119_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1120
	 {
	CombineDFT_1120_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1120_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1121
	 {
	CombineDFT_1121_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1121_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1122
	 {
	CombineDFT_1122_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1122_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1123
	 {
	CombineDFT_1123_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1123_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1124
	 {
	CombineDFT_1124_s.wn.real = -4.371139E-8 ; 
	CombineDFT_1124_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_1127
	 {
	CombineDFT_1127_s.wn.real = 0.70710677 ; 
	CombineDFT_1127_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1128
	 {
	CombineDFT_1128_s.wn.real = 0.70710677 ; 
	CombineDFT_1128_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1129
	 {
	CombineDFT_1129_s.wn.real = 0.70710677 ; 
	CombineDFT_1129_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1130
	 {
	CombineDFT_1130_s.wn.real = 0.70710677 ; 
	CombineDFT_1130_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1131
	 {
	CombineDFT_1131_s.wn.real = 0.70710677 ; 
	CombineDFT_1131_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1132
	 {
	CombineDFT_1132_s.wn.real = 0.70710677 ; 
	CombineDFT_1132_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1133
	 {
	CombineDFT_1133_s.wn.real = 0.70710677 ; 
	CombineDFT_1133_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1134
	 {
	CombineDFT_1134_s.wn.real = 0.70710677 ; 
	CombineDFT_1134_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_1137
	 {
	CombineDFT_1137_s.wn.real = 0.9238795 ; 
	CombineDFT_1137_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_1138
	 {
	CombineDFT_1138_s.wn.real = 0.9238795 ; 
	CombineDFT_1138_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_1139
	 {
	CombineDFT_1139_s.wn.real = 0.9238795 ; 
	CombineDFT_1139_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_1140
	 {
	CombineDFT_1140_s.wn.real = 0.9238795 ; 
	CombineDFT_1140_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_1143
	 {
	CombineDFT_1143_s.wn.real = 0.98078525 ; 
	CombineDFT_1143_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_1144
	 {
	CombineDFT_1144_s.wn.real = 0.98078525 ; 
	CombineDFT_1144_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_1036
	 {
	 ; 
	CombineDFT_1036_s.wn.real = 0.9951847 ; 
	CombineDFT_1036_s.wn.imag = -0.09801714 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FFTTestSource_1025();
		FFTReorderSimple_1026();
		WEIGHTED_ROUND_ROBIN_Splitter_1039();
			FFTReorderSimple_1041();
			FFTReorderSimple_1042();
		WEIGHTED_ROUND_ROBIN_Joiner_1040();
		WEIGHTED_ROUND_ROBIN_Splitter_1043();
			FFTReorderSimple_1045();
			FFTReorderSimple_1046();
			FFTReorderSimple_1047();
			FFTReorderSimple_1048();
		WEIGHTED_ROUND_ROBIN_Joiner_1044();
		WEIGHTED_ROUND_ROBIN_Splitter_1049();
			FFTReorderSimple_1051();
			FFTReorderSimple_1052();
			FFTReorderSimple_1053();
			FFTReorderSimple_1054();
			FFTReorderSimple_1055();
			FFTReorderSimple_1056();
			FFTReorderSimple_1057();
			FFTReorderSimple_1058();
		WEIGHTED_ROUND_ROBIN_Joiner_1050();
		WEIGHTED_ROUND_ROBIN_Splitter_1059();
			FFTReorderSimple_1061();
			FFTReorderSimple_1062();
			FFTReorderSimple_1063();
			FFTReorderSimple_1064();
			FFTReorderSimple_1065();
			FFTReorderSimple_1066();
			FFTReorderSimple_1067();
			FFTReorderSimple_1068();
			FFTReorderSimple_1069();
			FFTReorderSimple_1070();
			FFTReorderSimple_1071();
			FFTReorderSimple_1072();
			FFTReorderSimple_1073();
			FFTReorderSimple_1074();
			FFTReorderSimple_1075();
			FFTReorderSimple_1076();
		WEIGHTED_ROUND_ROBIN_Joiner_1060();
		WEIGHTED_ROUND_ROBIN_Splitter_1077();
			CombineDFT_1079();
			CombineDFT_1080();
			CombineDFT_1081();
			CombineDFT_1082();
			CombineDFT_1083();
			CombineDFT_1084();
			CombineDFT_1085();
			CombineDFT_1086();
			CombineDFT_1087();
			CombineDFT_1088();
			CombineDFT_1089();
			CombineDFT_1090();
			CombineDFT_1091();
			CombineDFT_1092();
			CombineDFT_1093();
			CombineDFT_1094();
			CombineDFT_1095();
			CombineDFT_1096();
			CombineDFT_1097();
			CombineDFT_1098();
			CombineDFT_1099();
			CombineDFT_1100();
			CombineDFT_1101();
			CombineDFT_1102();
			CombineDFT_1103();
			CombineDFT_1104();
			CombineDFT_1105();
			CombineDFT_1106();
		WEIGHTED_ROUND_ROBIN_Joiner_1078();
		WEIGHTED_ROUND_ROBIN_Splitter_1107();
			CombineDFT_1109();
			CombineDFT_1110();
			CombineDFT_1111();
			CombineDFT_1112();
			CombineDFT_1113();
			CombineDFT_1114();
			CombineDFT_1115();
			CombineDFT_1116();
			CombineDFT_1117();
			CombineDFT_1118();
			CombineDFT_1119();
			CombineDFT_1120();
			CombineDFT_1121();
			CombineDFT_1122();
			CombineDFT_1123();
			CombineDFT_1124();
		WEIGHTED_ROUND_ROBIN_Joiner_1108();
		WEIGHTED_ROUND_ROBIN_Splitter_1125();
			CombineDFT_1127();
			CombineDFT_1128();
			CombineDFT_1129();
			CombineDFT_1130();
			CombineDFT_1131();
			CombineDFT_1132();
			CombineDFT_1133();
			CombineDFT_1134();
		WEIGHTED_ROUND_ROBIN_Joiner_1126();
		WEIGHTED_ROUND_ROBIN_Splitter_1135();
			CombineDFT_1137();
			CombineDFT_1138();
			CombineDFT_1139();
			CombineDFT_1140();
		WEIGHTED_ROUND_ROBIN_Joiner_1136();
		WEIGHTED_ROUND_ROBIN_Splitter_1141();
			CombineDFT_1143();
			CombineDFT_1144();
		WEIGHTED_ROUND_ROBIN_Joiner_1142();
		CombineDFT_1036();
		CPrinter_1037();
	ENDFOR
	return EXIT_SUCCESS;
}
