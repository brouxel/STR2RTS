#include "PEG10-FFT6_nocache.h"

buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[10];
buffer_complex_t SplitJoin8_CombineDFT_Fiss_4929_4939_join[10];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4882WEIGHTED_ROUND_ROBIN_Splitter_4893;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4916WEIGHTED_ROUND_ROBIN_Splitter_4921;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4922CombineDFT_4846;
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_4927_4937_split[8];
buffer_complex_t SplitJoin6_FFTReorderSimple_Fiss_4928_4938_join[10];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4906WEIGHTED_ROUND_ROBIN_Splitter_4915;
buffer_complex_t SplitJoin10_CombineDFT_Fiss_4930_4940_join[10];
buffer_complex_t CombineDFT_4846CPrinter_4847;
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_4926_4936_join[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4854WEIGHTED_ROUND_ROBIN_Splitter_4859;
buffer_complex_t SplitJoin14_CombineDFT_Fiss_4932_4942_split[4];
buffer_complex_t SplitJoin14_CombineDFT_Fiss_4932_4942_join[4];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_4933_4943_split[2];
buffer_complex_t FFTReorderSimple_4836WEIGHTED_ROUND_ROBIN_Splitter_4849;
buffer_complex_t FFTTestSource_4835FFTReorderSimple_4836;
buffer_complex_t SplitJoin12_CombineDFT_Fiss_4931_4941_join[8];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4870WEIGHTED_ROUND_ROBIN_Splitter_4881;
buffer_complex_t SplitJoin12_CombineDFT_Fiss_4931_4941_split[8];
buffer_complex_t SplitJoin2_FFTReorderSimple_Fiss_4926_4936_split[4];
buffer_complex_t SplitJoin10_CombineDFT_Fiss_4930_4940_split[10];
buffer_complex_t SplitJoin8_CombineDFT_Fiss_4929_4939_split[10];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_4925_4935_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4850WEIGHTED_ROUND_ROBIN_Splitter_4853;
buffer_complex_t SplitJoin4_FFTReorderSimple_Fiss_4927_4937_join[8];
buffer_complex_t SplitJoin16_CombineDFT_Fiss_4933_4943_join[2];
buffer_complex_t SplitJoin0_FFTReorderSimple_Fiss_4925_4935_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4894WEIGHTED_ROUND_ROBIN_Splitter_4905;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_4860WEIGHTED_ROUND_ROBIN_Splitter_4869;


CombineDFT_4883_t CombineDFT_4883_s;
CombineDFT_4883_t CombineDFT_4884_s;
CombineDFT_4883_t CombineDFT_4885_s;
CombineDFT_4883_t CombineDFT_4886_s;
CombineDFT_4883_t CombineDFT_4887_s;
CombineDFT_4883_t CombineDFT_4888_s;
CombineDFT_4883_t CombineDFT_4889_s;
CombineDFT_4883_t CombineDFT_4890_s;
CombineDFT_4883_t CombineDFT_4891_s;
CombineDFT_4883_t CombineDFT_4892_s;
CombineDFT_4883_t CombineDFT_4895_s;
CombineDFT_4883_t CombineDFT_4896_s;
CombineDFT_4883_t CombineDFT_4897_s;
CombineDFT_4883_t CombineDFT_4898_s;
CombineDFT_4883_t CombineDFT_4899_s;
CombineDFT_4883_t CombineDFT_4900_s;
CombineDFT_4883_t CombineDFT_4901_s;
CombineDFT_4883_t CombineDFT_4902_s;
CombineDFT_4883_t CombineDFT_4903_s;
CombineDFT_4883_t CombineDFT_4904_s;
CombineDFT_4883_t CombineDFT_4907_s;
CombineDFT_4883_t CombineDFT_4908_s;
CombineDFT_4883_t CombineDFT_4909_s;
CombineDFT_4883_t CombineDFT_4910_s;
CombineDFT_4883_t CombineDFT_4911_s;
CombineDFT_4883_t CombineDFT_4912_s;
CombineDFT_4883_t CombineDFT_4913_s;
CombineDFT_4883_t CombineDFT_4914_s;
CombineDFT_4883_t CombineDFT_4917_s;
CombineDFT_4883_t CombineDFT_4918_s;
CombineDFT_4883_t CombineDFT_4919_s;
CombineDFT_4883_t CombineDFT_4920_s;
CombineDFT_4883_t CombineDFT_4923_s;
CombineDFT_4883_t CombineDFT_4924_s;
CombineDFT_4883_t CombineDFT_4846_s;

void FFTTestSource_4835(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t c1;
		complex_t zero;
		c1.real = 1.0 ; 
		c1.imag = 0.0 ; 
		zero.real = 0.0 ; 
		zero.imag = 0.0 ; 
		push_complex(&FFTTestSource_4835FFTReorderSimple_4836, zero) ; 
		push_complex(&FFTTestSource_4835FFTReorderSimple_4836, c1) ; 
		FOR(int, i, 0,  < , 62, i++) {
			push_complex(&FFTTestSource_4835FFTReorderSimple_4836, zero) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4836(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&FFTTestSource_4835FFTReorderSimple_4836, i)) ; 
			push_complex(&FFTReorderSimple_4836WEIGHTED_ROUND_ROBIN_Splitter_4849, __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 64, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&FFTTestSource_4835FFTReorderSimple_4836, i)) ; 
			push_complex(&FFTReorderSimple_4836WEIGHTED_ROUND_ROBIN_Splitter_4849, __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&FFTTestSource_4835FFTReorderSimple_4836) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4851(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_4925_4935_split[0], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_4925_4935_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 32, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_4925_4935_split[0], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_4925_4935_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_4925_4935_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4852(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 32, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_4925_4935_split[1], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_4925_4935_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 32, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin0_FFTReorderSimple_Fiss_4925_4935_split[1], i)) ; 
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_4925_4935_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_4925_4935_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4849() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_4925_4935_split[0], pop_complex(&FFTReorderSimple_4836WEIGHTED_ROUND_ROBIN_Splitter_4849));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin0_FFTReorderSimple_Fiss_4925_4935_split[1], pop_complex(&FFTReorderSimple_4836WEIGHTED_ROUND_ROBIN_Splitter_4849));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4850() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4850WEIGHTED_ROUND_ROBIN_Splitter_4853, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_4925_4935_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4850WEIGHTED_ROUND_ROBIN_Splitter_4853, pop_complex(&SplitJoin0_FFTReorderSimple_Fiss_4925_4935_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_4855(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_4926_4936_split[0], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_4926_4936_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_4926_4936_split[0], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_4926_4936_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_4926_4936_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4856(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_4926_4936_split[1], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_4926_4936_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_4926_4936_split[1], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_4926_4936_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_4926_4936_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4857(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_4926_4936_split[2], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_4926_4936_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_4926_4936_split[2], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_4926_4936_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_4926_4936_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4858(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 16, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_4926_4936_split[3], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_4926_4936_join[3], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 16, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin2_FFTReorderSimple_Fiss_4926_4936_split[3], i)) ; 
			push_complex(&SplitJoin2_FFTReorderSimple_Fiss_4926_4936_join[3], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_4926_4936_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4853() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin2_FFTReorderSimple_Fiss_4926_4936_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4850WEIGHTED_ROUND_ROBIN_Splitter_4853));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4854() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4854WEIGHTED_ROUND_ROBIN_Splitter_4859, pop_complex(&SplitJoin2_FFTReorderSimple_Fiss_4926_4936_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_4861(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_split[0], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_split[0], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4862(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_split[1], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_split[1], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4863(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_split[2], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_split[2], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4864(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_split[3], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_join[3], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_split[3], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_join[3], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4865(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_split[4], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_join[4], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_split[4], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_join[4], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4866(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_split[5], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_join[5], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_split[5], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_join[5], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4867(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_split[6], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_join[6], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_split[6], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_join[6], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4868(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 8, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_split[7], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_join[7], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 8, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_split[7], i)) ; 
			push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_join[7], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4859() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4854WEIGHTED_ROUND_ROBIN_Splitter_4859));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4860() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4860WEIGHTED_ROUND_ROBIN_Splitter_4869, pop_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_4871(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[0], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_join[0], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[0], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_join[0], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4872(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[1], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_join[1], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[1], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_join[1], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4873(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[2], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_join[2], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[2], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_join[2], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4874(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[3], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_join[3], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[3], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_join[3], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4875(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[4], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_join[4], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[4], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_join[4], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4876(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[5], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_join[5], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[5], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_join[5], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4877(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[6], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_join[6], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[6], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_join[6], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4878(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[7], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_join[7], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[7], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_join[7], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4879(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[8], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_join[8], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[8], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_join[8], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[8]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FFTReorderSimple_4880(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		FOR(int, i, 0,  < , 4, i = (i + 2)) {
			complex_t __sa7 = {
				.real = 0,
				.imag = 0
			};
			__sa7 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[9], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_join[9], __sa7) ; 
		}
		ENDFOR
		FOR(int, i, 1,  < , 4, i = (i + 2)) {
			complex_t __sa8 = {
				.real = 0,
				.imag = 0
			};
			__sa8 = ((complex_t) peek_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[9], i)) ; 
			push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_join[9], __sa8) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[9]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4869() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 10, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4860WEIGHTED_ROUND_ROBIN_Splitter_4869));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4870() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 10, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4870WEIGHTED_ROUND_ROBIN_Splitter_4881, pop_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_4883(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[0], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4883_s.wn.real) - (w.imag * CombineDFT_4883_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4883_s.wn.imag) + (w.imag * CombineDFT_4883_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[0]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4884(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[1], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4884_s.wn.real) - (w.imag * CombineDFT_4884_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4884_s.wn.imag) + (w.imag * CombineDFT_4884_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[1]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4885(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[2], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4885_s.wn.real) - (w.imag * CombineDFT_4885_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4885_s.wn.imag) + (w.imag * CombineDFT_4885_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[2]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4886(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[3], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4886_s.wn.real) - (w.imag * CombineDFT_4886_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4886_s.wn.imag) + (w.imag * CombineDFT_4886_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[3]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4887(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[4], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[4], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4887_s.wn.real) - (w.imag * CombineDFT_4887_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4887_s.wn.imag) + (w.imag * CombineDFT_4887_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[4]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4888(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[5], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[5], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4888_s.wn.real) - (w.imag * CombineDFT_4888_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4888_s.wn.imag) + (w.imag * CombineDFT_4888_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[5]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4889(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[6], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[6], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4889_s.wn.real) - (w.imag * CombineDFT_4889_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4889_s.wn.imag) + (w.imag * CombineDFT_4889_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[6]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4890(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[7], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[7], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4890_s.wn.real) - (w.imag * CombineDFT_4890_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4890_s.wn.imag) + (w.imag * CombineDFT_4890_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[7]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4891(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[8], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[8], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4891_s.wn.real) - (w.imag * CombineDFT_4891_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4891_s.wn.imag) + (w.imag * CombineDFT_4891_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[8]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_join[8], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4892(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[2];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 1, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[9], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[9], (1 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(1 + i)].real = (y0.real - y1w.real) ; 
			results[(1 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4892_s.wn.real) - (w.imag * CombineDFT_4892_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4892_s.wn.imag) + (w.imag * CombineDFT_4892_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 2, i++) {
			pop_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[9]) ; 
			push_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_join[9], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4881() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 10, __iter_++)
			push_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4870WEIGHTED_ROUND_ROBIN_Splitter_4881));
			push_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4870WEIGHTED_ROUND_ROBIN_Splitter_4881));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4882() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 10, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4882WEIGHTED_ROUND_ROBIN_Splitter_4893, pop_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_join[__iter_]));
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4882WEIGHTED_ROUND_ROBIN_Splitter_4893, pop_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_4895(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[0], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4895_s.wn.real) - (w.imag * CombineDFT_4895_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4895_s.wn.imag) + (w.imag * CombineDFT_4895_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[0]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4896(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[1], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4896_s.wn.real) - (w.imag * CombineDFT_4896_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4896_s.wn.imag) + (w.imag * CombineDFT_4896_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[1]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4897(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[2], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4897_s.wn.real) - (w.imag * CombineDFT_4897_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4897_s.wn.imag) + (w.imag * CombineDFT_4897_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[2]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4898(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[3], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4898_s.wn.real) - (w.imag * CombineDFT_4898_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4898_s.wn.imag) + (w.imag * CombineDFT_4898_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[3]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4899(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[4], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[4], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4899_s.wn.real) - (w.imag * CombineDFT_4899_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4899_s.wn.imag) + (w.imag * CombineDFT_4899_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[4]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4900(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[5], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[5], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4900_s.wn.real) - (w.imag * CombineDFT_4900_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4900_s.wn.imag) + (w.imag * CombineDFT_4900_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[5]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4901(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[6], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[6], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4901_s.wn.real) - (w.imag * CombineDFT_4901_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4901_s.wn.imag) + (w.imag * CombineDFT_4901_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[6]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4902(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[7], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[7], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4902_s.wn.real) - (w.imag * CombineDFT_4902_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4902_s.wn.imag) + (w.imag * CombineDFT_4902_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[7]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4903(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[8], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[8], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4903_s.wn.real) - (w.imag * CombineDFT_4903_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4903_s.wn.imag) + (w.imag * CombineDFT_4903_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[8]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_join[8], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4904(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[4];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 2, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[9], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[9], (2 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(2 + i)].real = (y0.real - y1w.real) ; 
			results[(2 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4904_s.wn.real) - (w.imag * CombineDFT_4904_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4904_s.wn.imag) + (w.imag * CombineDFT_4904_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[9]) ; 
			push_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_join[9], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4893() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 10, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4882WEIGHTED_ROUND_ROBIN_Splitter_4893));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4894() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 10, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4894WEIGHTED_ROUND_ROBIN_Splitter_4905, pop_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_4907(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_split[0], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4907_s.wn.real) - (w.imag * CombineDFT_4907_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4907_s.wn.imag) + (w.imag * CombineDFT_4907_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_split[0]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4908(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_split[1], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4908_s.wn.real) - (w.imag * CombineDFT_4908_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4908_s.wn.imag) + (w.imag * CombineDFT_4908_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_split[1]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4909(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_split[2], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4909_s.wn.real) - (w.imag * CombineDFT_4909_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4909_s.wn.imag) + (w.imag * CombineDFT_4909_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_split[2]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4910(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_split[3], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4910_s.wn.real) - (w.imag * CombineDFT_4910_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4910_s.wn.imag) + (w.imag * CombineDFT_4910_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_split[3]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4911(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_split[4], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_split[4], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4911_s.wn.real) - (w.imag * CombineDFT_4911_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4911_s.wn.imag) + (w.imag * CombineDFT_4911_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_split[4]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_join[4], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4912(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_split[5], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_split[5], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4912_s.wn.real) - (w.imag * CombineDFT_4912_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4912_s.wn.imag) + (w.imag * CombineDFT_4912_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_split[5]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_join[5], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4913(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_split[6], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_split[6], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4913_s.wn.real) - (w.imag * CombineDFT_4913_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4913_s.wn.imag) + (w.imag * CombineDFT_4913_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_split[6]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_join[6], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4914(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[8];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 4, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_split[7], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_split[7], (4 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(4 + i)].real = (y0.real - y1w.real) ; 
			results[(4 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4914_s.wn.real) - (w.imag * CombineDFT_4914_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4914_s.wn.imag) + (w.imag * CombineDFT_4914_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			pop_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_split[7]) ; 
			push_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_join[7], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4905() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4894WEIGHTED_ROUND_ROBIN_Splitter_4905));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4906() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4906WEIGHTED_ROUND_ROBIN_Splitter_4915, pop_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_4917(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_4932_4942_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_4932_4942_split[0], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4917_s.wn.real) - (w.imag * CombineDFT_4917_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4917_s.wn.imag) + (w.imag * CombineDFT_4917_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_4932_4942_split[0]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_4932_4942_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4918(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_4932_4942_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_4932_4942_split[1], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4918_s.wn.real) - (w.imag * CombineDFT_4918_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4918_s.wn.imag) + (w.imag * CombineDFT_4918_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_4932_4942_split[1]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_4932_4942_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4919(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_4932_4942_split[2], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_4932_4942_split[2], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4919_s.wn.real) - (w.imag * CombineDFT_4919_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4919_s.wn.imag) + (w.imag * CombineDFT_4919_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_4932_4942_split[2]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_4932_4942_join[2], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4920(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[16];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 8, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_4932_4942_split[3], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin14_CombineDFT_Fiss_4932_4942_split[3], (8 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(8 + i)].real = (y0.real - y1w.real) ; 
			results[(8 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4920_s.wn.real) - (w.imag * CombineDFT_4920_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4920_s.wn.imag) + (w.imag * CombineDFT_4920_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 16, i++) {
			pop_complex(&SplitJoin14_CombineDFT_Fiss_4932_4942_split[3]) ; 
			push_complex(&SplitJoin14_CombineDFT_Fiss_4932_4942_join[3], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4915() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&SplitJoin14_CombineDFT_Fiss_4932_4942_split[__iter_dec_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4906WEIGHTED_ROUND_ROBIN_Splitter_4915));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4916() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4916WEIGHTED_ROUND_ROBIN_Splitter_4921, pop_complex(&SplitJoin14_CombineDFT_Fiss_4932_4942_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_4923(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[32];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 16, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_4933_4943_split[0], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_4933_4943_split[0], (16 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(16 + i)].real = (y0.real - y1w.real) ; 
			results[(16 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4923_s.wn.real) - (w.imag * CombineDFT_4923_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4923_s.wn.imag) + (w.imag * CombineDFT_4923_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin16_CombineDFT_Fiss_4933_4943_split[0]) ; 
			push_complex(&SplitJoin16_CombineDFT_Fiss_4933_4943_join[0], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CombineDFT_4924(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[32];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 16, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_4933_4943_split[1], i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&SplitJoin16_CombineDFT_Fiss_4933_4943_split[1], (16 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(16 + i)].real = (y0.real - y1w.real) ; 
			results[(16 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4924_s.wn.real) - (w.imag * CombineDFT_4924_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4924_s.wn.imag) + (w.imag * CombineDFT_4924_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_complex(&SplitJoin16_CombineDFT_Fiss_4933_4943_split[1]) ; 
			push_complex(&SplitJoin16_CombineDFT_Fiss_4933_4943_join[1], results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4921() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_4933_4943_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4916WEIGHTED_ROUND_ROBIN_Splitter_4921));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&SplitJoin16_CombineDFT_Fiss_4933_4943_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4916WEIGHTED_ROUND_ROBIN_Splitter_4921));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4922() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4922CombineDFT_4846, pop_complex(&SplitJoin16_CombineDFT_Fiss_4933_4943_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4922CombineDFT_4846, pop_complex(&SplitJoin16_CombineDFT_Fiss_4933_4943_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_4846(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		complex_t w;
		complex_t y0;
		complex_t y1;
		complex_t y1w;
		complex_t w_next;
		complex_t results[64];
		w.real = 1.0 ; 
		w.imag = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			complex_t __sa1 = {
				.real = 0,
				.imag = 0
			};
			complex_t __sa2 = {
				.real = 0,
				.imag = 0
			};
			__sa1 = ((complex_t) peek_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4922CombineDFT_4846, i)) ; 
			y0.real = __sa1.real ; 
			y0.imag = __sa1.imag ; 
			__sa2 = ((complex_t) peek_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4922CombineDFT_4846, (32 + i))) ; 
			y1.real = __sa2.real ; 
			y1.imag = __sa2.imag ; 
			y1w.real = ((y1.real * w.real) - (y1.imag * w.imag)) ; 
			y1w.imag = ((y1.real * w.imag) + (y1.imag * w.real)) ; 
			results[i].real = (y0.real + y1w.real) ; 
			results[i].imag = (y0.imag + y1w.imag) ; 
			results[(32 + i)].real = (y0.real - y1w.real) ; 
			results[(32 + i)].imag = (y0.imag - y1w.imag) ; 
			w_next.real = ((w.real * CombineDFT_4846_s.wn.real) - (w.imag * CombineDFT_4846_s.wn.imag)) ; 
			w_next.imag = ((w.real * CombineDFT_4846_s.wn.imag) + (w.imag * CombineDFT_4846_s.wn.real)) ; 
			w.real = w_next.real ; 
			w.imag = w_next.imag ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4922CombineDFT_4846) ; 
			push_complex(&CombineDFT_4846CPrinter_4847, results[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void CPrinter_4847(){
	FOR(uint32_t, __iter_steady_, 0, <, 320, __iter_steady_++) {
		complex_t c = ((complex_t) pop_complex(&CombineDFT_4846CPrinter_4847));
		printf("%.10f", c.real);
		printf("\n");
		printf("%.10f", c.imag);
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 10, __iter_init_0_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 10, __iter_init_1_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_join[__iter_init_1_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4882WEIGHTED_ROUND_ROBIN_Splitter_4893);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4916WEIGHTED_ROUND_ROBIN_Splitter_4921);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4922CombineDFT_4846);
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 10, __iter_init_3_++)
		init_buffer_complex(&SplitJoin6_FFTReorderSimple_Fiss_4928_4938_join[__iter_init_3_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4906WEIGHTED_ROUND_ROBIN_Splitter_4915);
	FOR(int, __iter_init_4_, 0, <, 10, __iter_init_4_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_join[__iter_init_4_]);
	ENDFOR
	init_buffer_complex(&CombineDFT_4846CPrinter_4847);
	FOR(int, __iter_init_5_, 0, <, 4, __iter_init_5_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_4926_4936_join[__iter_init_5_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4854WEIGHTED_ROUND_ROBIN_Splitter_4859);
	FOR(int, __iter_init_6_, 0, <, 4, __iter_init_6_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_4932_4942_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 4, __iter_init_7_++)
		init_buffer_complex(&SplitJoin14_CombineDFT_Fiss_4932_4942_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_4933_4943_split[__iter_init_8_]);
	ENDFOR
	init_buffer_complex(&FFTReorderSimple_4836WEIGHTED_ROUND_ROBIN_Splitter_4849);
	init_buffer_complex(&FFTTestSource_4835FFTReorderSimple_4836);
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_join[__iter_init_9_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4870WEIGHTED_ROUND_ROBIN_Splitter_4881);
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_complex(&SplitJoin12_CombineDFT_Fiss_4931_4941_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 4, __iter_init_11_++)
		init_buffer_complex(&SplitJoin2_FFTReorderSimple_Fiss_4926_4936_split[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 10, __iter_init_12_++)
		init_buffer_complex(&SplitJoin10_CombineDFT_Fiss_4930_4940_split[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 10, __iter_init_13_++)
		init_buffer_complex(&SplitJoin8_CombineDFT_Fiss_4929_4939_split[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_4925_4935_join[__iter_init_14_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4850WEIGHTED_ROUND_ROBIN_Splitter_4853);
	FOR(int, __iter_init_15_, 0, <, 8, __iter_init_15_++)
		init_buffer_complex(&SplitJoin4_FFTReorderSimple_Fiss_4927_4937_join[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_complex(&SplitJoin16_CombineDFT_Fiss_4933_4943_join[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_complex(&SplitJoin0_FFTReorderSimple_Fiss_4925_4935_split[__iter_init_17_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4894WEIGHTED_ROUND_ROBIN_Splitter_4905);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_4860WEIGHTED_ROUND_ROBIN_Splitter_4869);
// --- init: CombineDFT_4883
	 {
	 ; 
	CombineDFT_4883_s.wn.real = -1.0 ; 
	CombineDFT_4883_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4884
	 {
	 ; 
	CombineDFT_4884_s.wn.real = -1.0 ; 
	CombineDFT_4884_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4885
	 {
	 ; 
	CombineDFT_4885_s.wn.real = -1.0 ; 
	CombineDFT_4885_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4886
	 {
	 ; 
	CombineDFT_4886_s.wn.real = -1.0 ; 
	CombineDFT_4886_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4887
	 {
	 ; 
	CombineDFT_4887_s.wn.real = -1.0 ; 
	CombineDFT_4887_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4888
	 {
	 ; 
	CombineDFT_4888_s.wn.real = -1.0 ; 
	CombineDFT_4888_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4889
	 {
	 ; 
	CombineDFT_4889_s.wn.real = -1.0 ; 
	CombineDFT_4889_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4890
	 {
	 ; 
	CombineDFT_4890_s.wn.real = -1.0 ; 
	CombineDFT_4890_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4891
	 {
	 ; 
	CombineDFT_4891_s.wn.real = -1.0 ; 
	CombineDFT_4891_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4892
	 {
	 ; 
	CombineDFT_4892_s.wn.real = -1.0 ; 
	CombineDFT_4892_s.wn.imag = 8.742278E-8 ; 
}
//--------------------------------
// --- init: CombineDFT_4895
	 {
	 ; 
	CombineDFT_4895_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4895_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4896
	 {
	 ; 
	CombineDFT_4896_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4896_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4897
	 {
	 ; 
	CombineDFT_4897_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4897_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4898
	 {
	 ; 
	CombineDFT_4898_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4898_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4899
	 {
	 ; 
	CombineDFT_4899_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4899_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4900
	 {
	 ; 
	CombineDFT_4900_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4900_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4901
	 {
	 ; 
	CombineDFT_4901_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4901_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4902
	 {
	 ; 
	CombineDFT_4902_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4902_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4903
	 {
	 ; 
	CombineDFT_4903_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4903_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4904
	 {
	 ; 
	CombineDFT_4904_s.wn.real = -4.371139E-8 ; 
	CombineDFT_4904_s.wn.imag = -1.0 ; 
}
//--------------------------------
// --- init: CombineDFT_4907
	 {
	 ; 
	CombineDFT_4907_s.wn.real = 0.70710677 ; 
	CombineDFT_4907_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4908
	 {
	 ; 
	CombineDFT_4908_s.wn.real = 0.70710677 ; 
	CombineDFT_4908_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4909
	 {
	 ; 
	CombineDFT_4909_s.wn.real = 0.70710677 ; 
	CombineDFT_4909_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4910
	 {
	 ; 
	CombineDFT_4910_s.wn.real = 0.70710677 ; 
	CombineDFT_4910_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4911
	 {
	 ; 
	CombineDFT_4911_s.wn.real = 0.70710677 ; 
	CombineDFT_4911_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4912
	 {
	 ; 
	CombineDFT_4912_s.wn.real = 0.70710677 ; 
	CombineDFT_4912_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4913
	 {
	 ; 
	CombineDFT_4913_s.wn.real = 0.70710677 ; 
	CombineDFT_4913_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4914
	 {
	 ; 
	CombineDFT_4914_s.wn.real = 0.70710677 ; 
	CombineDFT_4914_s.wn.imag = -0.70710677 ; 
}
//--------------------------------
// --- init: CombineDFT_4917
	 {
	 ; 
	CombineDFT_4917_s.wn.real = 0.9238795 ; 
	CombineDFT_4917_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_4918
	 {
	 ; 
	CombineDFT_4918_s.wn.real = 0.9238795 ; 
	CombineDFT_4918_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_4919
	 {
	 ; 
	CombineDFT_4919_s.wn.real = 0.9238795 ; 
	CombineDFT_4919_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_4920
	 {
	 ; 
	CombineDFT_4920_s.wn.real = 0.9238795 ; 
	CombineDFT_4920_s.wn.imag = -0.38268346 ; 
}
//--------------------------------
// --- init: CombineDFT_4923
	 {
	 ; 
	CombineDFT_4923_s.wn.real = 0.98078525 ; 
	CombineDFT_4923_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_4924
	 {
	 ; 
	CombineDFT_4924_s.wn.real = 0.98078525 ; 
	CombineDFT_4924_s.wn.imag = -0.19509032 ; 
}
//--------------------------------
// --- init: CombineDFT_4846
	 {
	 ; 
	CombineDFT_4846_s.wn.real = 0.9951847 ; 
	CombineDFT_4846_s.wn.imag = -0.09801714 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FFTTestSource_4835();
		FFTReorderSimple_4836();
		WEIGHTED_ROUND_ROBIN_Splitter_4849();
			FFTReorderSimple_4851();
			FFTReorderSimple_4852();
		WEIGHTED_ROUND_ROBIN_Joiner_4850();
		WEIGHTED_ROUND_ROBIN_Splitter_4853();
			FFTReorderSimple_4855();
			FFTReorderSimple_4856();
			FFTReorderSimple_4857();
			FFTReorderSimple_4858();
		WEIGHTED_ROUND_ROBIN_Joiner_4854();
		WEIGHTED_ROUND_ROBIN_Splitter_4859();
			FFTReorderSimple_4861();
			FFTReorderSimple_4862();
			FFTReorderSimple_4863();
			FFTReorderSimple_4864();
			FFTReorderSimple_4865();
			FFTReorderSimple_4866();
			FFTReorderSimple_4867();
			FFTReorderSimple_4868();
		WEIGHTED_ROUND_ROBIN_Joiner_4860();
		WEIGHTED_ROUND_ROBIN_Splitter_4869();
			FFTReorderSimple_4871();
			FFTReorderSimple_4872();
			FFTReorderSimple_4873();
			FFTReorderSimple_4874();
			FFTReorderSimple_4875();
			FFTReorderSimple_4876();
			FFTReorderSimple_4877();
			FFTReorderSimple_4878();
			FFTReorderSimple_4879();
			FFTReorderSimple_4880();
		WEIGHTED_ROUND_ROBIN_Joiner_4870();
		WEIGHTED_ROUND_ROBIN_Splitter_4881();
			CombineDFT_4883();
			CombineDFT_4884();
			CombineDFT_4885();
			CombineDFT_4886();
			CombineDFT_4887();
			CombineDFT_4888();
			CombineDFT_4889();
			CombineDFT_4890();
			CombineDFT_4891();
			CombineDFT_4892();
		WEIGHTED_ROUND_ROBIN_Joiner_4882();
		WEIGHTED_ROUND_ROBIN_Splitter_4893();
			CombineDFT_4895();
			CombineDFT_4896();
			CombineDFT_4897();
			CombineDFT_4898();
			CombineDFT_4899();
			CombineDFT_4900();
			CombineDFT_4901();
			CombineDFT_4902();
			CombineDFT_4903();
			CombineDFT_4904();
		WEIGHTED_ROUND_ROBIN_Joiner_4894();
		WEIGHTED_ROUND_ROBIN_Splitter_4905();
			CombineDFT_4907();
			CombineDFT_4908();
			CombineDFT_4909();
			CombineDFT_4910();
			CombineDFT_4911();
			CombineDFT_4912();
			CombineDFT_4913();
			CombineDFT_4914();
		WEIGHTED_ROUND_ROBIN_Joiner_4906();
		WEIGHTED_ROUND_ROBIN_Splitter_4915();
			CombineDFT_4917();
			CombineDFT_4918();
			CombineDFT_4919();
			CombineDFT_4920();
		WEIGHTED_ROUND_ROBIN_Joiner_4916();
		WEIGHTED_ROUND_ROBIN_Splitter_4921();
			CombineDFT_4923();
			CombineDFT_4924();
		WEIGHTED_ROUND_ROBIN_Joiner_4922();
		CombineDFT_4846();
		CPrinter_4847();
	ENDFOR
	return EXIT_SUCCESS;
}
