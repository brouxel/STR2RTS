#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=2432 on the compile command line
#else
#if BUF_SIZEMAX < 2432
#error BUF_SIZEMAX too small, it must be at least 2432
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	complex_t wn;
} CombineDFT_3095_t;
void FFTTestSource(buffer_complex_t *chanout);
void FFTTestSource_3041();
void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout);
void FFTReorderSimple_3042();
void WEIGHTED_ROUND_ROBIN_Splitter_3055();
void FFTReorderSimple_3057();
void FFTReorderSimple_3058();
void WEIGHTED_ROUND_ROBIN_Joiner_3056();
void WEIGHTED_ROUND_ROBIN_Splitter_3059();
void FFTReorderSimple_3061();
void FFTReorderSimple_3062();
void FFTReorderSimple_3063();
void FFTReorderSimple_3064();
void WEIGHTED_ROUND_ROBIN_Joiner_3060();
void WEIGHTED_ROUND_ROBIN_Splitter_3065();
void FFTReorderSimple_3067();
void FFTReorderSimple_3068();
void FFTReorderSimple_3069();
void FFTReorderSimple_3070();
void FFTReorderSimple_3071();
void FFTReorderSimple_3072();
void FFTReorderSimple_3073();
void FFTReorderSimple_3074();
void WEIGHTED_ROUND_ROBIN_Joiner_3066();
void WEIGHTED_ROUND_ROBIN_Splitter_3075();
void FFTReorderSimple_3077();
void FFTReorderSimple_3078();
void FFTReorderSimple_3079();
void FFTReorderSimple_3080();
void FFTReorderSimple_3081();
void FFTReorderSimple_3082();
void FFTReorderSimple_3083();
void FFTReorderSimple_3084();
void FFTReorderSimple_3085();
void FFTReorderSimple_3086();
void FFTReorderSimple_3087();
void FFTReorderSimple_3088();
void FFTReorderSimple_3089();
void FFTReorderSimple_3090();
void FFTReorderSimple_3091();
void FFTReorderSimple_3092();
void WEIGHTED_ROUND_ROBIN_Joiner_3076();
void WEIGHTED_ROUND_ROBIN_Splitter_3093();
void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout);
void CombineDFT_3095();
void CombineDFT_3096();
void CombineDFT_3097();
void CombineDFT_3098();
void CombineDFT_3099();
void CombineDFT_3100();
void CombineDFT_3101();
void CombineDFT_3102();
void CombineDFT_3103();
void CombineDFT_3104();
void CombineDFT_3105();
void CombineDFT_3106();
void CombineDFT_3107();
void CombineDFT_3108();
void CombineDFT_3109();
void CombineDFT_3110();
void CombineDFT_3111();
void CombineDFT_3112();
void CombineDFT_3113();
void WEIGHTED_ROUND_ROBIN_Joiner_3094();
void WEIGHTED_ROUND_ROBIN_Splitter_3114();
void CombineDFT_3116();
void CombineDFT_3117();
void CombineDFT_3118();
void CombineDFT_3119();
void CombineDFT_3120();
void CombineDFT_3121();
void CombineDFT_3122();
void CombineDFT_3123();
void CombineDFT_3124();
void CombineDFT_3125();
void CombineDFT_3126();
void CombineDFT_3127();
void CombineDFT_3128();
void CombineDFT_3129();
void CombineDFT_3130();
void CombineDFT_3131();
void WEIGHTED_ROUND_ROBIN_Joiner_3115();
void WEIGHTED_ROUND_ROBIN_Splitter_3132();
void CombineDFT_3134();
void CombineDFT_3135();
void CombineDFT_3136();
void CombineDFT_3137();
void CombineDFT_3138();
void CombineDFT_3139();
void CombineDFT_3140();
void CombineDFT_3141();
void WEIGHTED_ROUND_ROBIN_Joiner_3133();
void WEIGHTED_ROUND_ROBIN_Splitter_3142();
void CombineDFT_3144();
void CombineDFT_3145();
void CombineDFT_3146();
void CombineDFT_3147();
void WEIGHTED_ROUND_ROBIN_Joiner_3143();
void WEIGHTED_ROUND_ROBIN_Splitter_3148();
void CombineDFT_3150();
void CombineDFT_3151();
void WEIGHTED_ROUND_ROBIN_Joiner_3149();
void CombineDFT_3052();
void CPrinter(buffer_complex_t *chanin);
void CPrinter_3053();

#ifdef __cplusplus
}
#endif
#endif
