#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=1408 on the compile command line
#else
#if BUF_SIZEMAX < 1408
#error BUF_SIZEMAX too small, it must be at least 1408
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	complex_t wn;
} CombineDFT_2441_t;
void FFTTestSource(buffer_complex_t *chanout);
void FFTTestSource_2387();
void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout);
void FFTReorderSimple_2388();
void WEIGHTED_ROUND_ROBIN_Splitter_2401();
void FFTReorderSimple_2403();
void FFTReorderSimple_2404();
void WEIGHTED_ROUND_ROBIN_Joiner_2402();
void WEIGHTED_ROUND_ROBIN_Splitter_2405();
void FFTReorderSimple_2407();
void FFTReorderSimple_2408();
void FFTReorderSimple_2409();
void FFTReorderSimple_2410();
void WEIGHTED_ROUND_ROBIN_Joiner_2406();
void WEIGHTED_ROUND_ROBIN_Splitter_2411();
void FFTReorderSimple_2413();
void FFTReorderSimple_2414();
void FFTReorderSimple_2415();
void FFTReorderSimple_2416();
void FFTReorderSimple_2417();
void FFTReorderSimple_2418();
void FFTReorderSimple_2419();
void FFTReorderSimple_2420();
void WEIGHTED_ROUND_ROBIN_Joiner_2412();
void WEIGHTED_ROUND_ROBIN_Splitter_2421();
void FFTReorderSimple_2423();
void FFTReorderSimple_2424();
void FFTReorderSimple_2425();
void FFTReorderSimple_2426();
void FFTReorderSimple_2427();
void FFTReorderSimple_2428();
void FFTReorderSimple_2429();
void FFTReorderSimple_2430();
void FFTReorderSimple_2431();
void FFTReorderSimple_2432();
void FFTReorderSimple_2433();
void FFTReorderSimple_2434();
void FFTReorderSimple_2435();
void FFTReorderSimple_2436();
void FFTReorderSimple_2437();
void FFTReorderSimple_2438();
void WEIGHTED_ROUND_ROBIN_Joiner_2422();
void WEIGHTED_ROUND_ROBIN_Splitter_2439();
void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout);
void CombineDFT_2441();
void CombineDFT_2442();
void CombineDFT_2443();
void CombineDFT_2444();
void CombineDFT_2445();
void CombineDFT_2446();
void CombineDFT_2447();
void CombineDFT_2448();
void CombineDFT_2449();
void CombineDFT_2450();
void CombineDFT_2451();
void CombineDFT_2452();
void CombineDFT_2453();
void CombineDFT_2454();
void CombineDFT_2455();
void CombineDFT_2456();
void CombineDFT_2457();
void CombineDFT_2458();
void CombineDFT_2459();
void CombineDFT_2460();
void CombineDFT_2461();
void CombineDFT_2462();
void WEIGHTED_ROUND_ROBIN_Joiner_2440();
void WEIGHTED_ROUND_ROBIN_Splitter_2463();
void CombineDFT_2465();
void CombineDFT_2466();
void CombineDFT_2467();
void CombineDFT_2468();
void CombineDFT_2469();
void CombineDFT_2470();
void CombineDFT_2471();
void CombineDFT_2472();
void CombineDFT_2473();
void CombineDFT_2474();
void CombineDFT_2475();
void CombineDFT_2476();
void CombineDFT_2477();
void CombineDFT_2478();
void CombineDFT_2479();
void CombineDFT_2480();
void WEIGHTED_ROUND_ROBIN_Joiner_2464();
void WEIGHTED_ROUND_ROBIN_Splitter_2481();
void CombineDFT_2483();
void CombineDFT_2484();
void CombineDFT_2485();
void CombineDFT_2486();
void CombineDFT_2487();
void CombineDFT_2488();
void CombineDFT_2489();
void CombineDFT_2490();
void WEIGHTED_ROUND_ROBIN_Joiner_2482();
void WEIGHTED_ROUND_ROBIN_Splitter_2491();
void CombineDFT_2493();
void CombineDFT_2494();
void CombineDFT_2495();
void CombineDFT_2496();
void WEIGHTED_ROUND_ROBIN_Joiner_2492();
void WEIGHTED_ROUND_ROBIN_Splitter_2497();
void CombineDFT_2499();
void CombineDFT_2500();
void WEIGHTED_ROUND_ROBIN_Joiner_2498();
void CombineDFT_2398();
void CPrinter(buffer_complex_t *chanin);
void CPrinter_2399();

#ifdef __cplusplus
}
#endif
#endif
