#include "PEG13-FFT3.h"

buffer_float_t Pre_CollapsedDataParallel_1_3993WEIGHTED_ROUND_ROBIN_Splitter_4154;
buffer_float_t Pre_CollapsedDataParallel_1_4002WEIGHTED_ROUND_ROBIN_Splitter_4182;
buffer_float_t SplitJoin4_Butterfly_Fiss_4216_4237_split[8];
buffer_float_t SplitJoin89_Butterfly_Fiss_4232_4243_join[4];
buffer_float_t SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_child1_4079_4242_split[2];
buffer_float_t SplitJoin95_Butterfly_Fiss_4233_4244_join[4];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_3638_4033_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_Hier_4215_4236_split[2];
buffer_float_t SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_child0_4103_4246_split[4];
buffer_float_t SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_child0_4103_4246_join[4];
buffer_float_t Pre_CollapsedDataParallel_1_3984WEIGHTED_ROUND_ROBIN_Splitter_4138;
buffer_float_t SplitJoin39_Butterfly_Fiss_4223_4248_split[2];
buffer_float_t Pre_CollapsedDataParallel_1_4017WEIGHTED_ROUND_ROBIN_Splitter_4202;
buffer_float_t SplitJoin61_Butterfly_Fiss_4228_4254_split[2];
buffer_float_t Pre_CollapsedDataParallel_1_3996WEIGHTED_ROUND_ROBIN_Splitter_4170;
buffer_float_t SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child1_4222_4258_join[8];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_3638_4033_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_Hier_4215_4236_join[2];
buffer_float_t SplitJoin72_Butterfly_Fiss_4230_4240_split[4];
buffer_float_t Post_CollapsedDataParallel_2_3988WEIGHTED_ROUND_ROBIN_Splitter_4107;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4155Post_CollapsedDataParallel_2_3994;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4109WEIGHTED_ROUND_ROBIN_Splitter_4110;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4211Post_CollapsedDataParallel_2_4024;
buffer_float_t SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_child0_4074_4238_join[2];
buffer_float_t SplitJoin85_Butterfly_Fiss_4231_4241_join[8];
buffer_float_t SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_Hier_4218_4245_join[2];
buffer_float_t SplitJoin61_Butterfly_Fiss_4228_4254_join[2];
buffer_float_t Post_CollapsedDataParallel_2_3985WEIGHTED_ROUND_ROBIN_Splitter_4105;
buffer_float_t SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child1_4222_4258_split[8];
buffer_float_t SplitJoin72_Butterfly_Fiss_4230_4240_join[4];
buffer_float_t SplitJoin14_Butterfly_Fiss_4219_4247_split[2];
buffer_float_t SplitJoin47_Butterfly_Fiss_4225_4250_join[2];
buffer_float_t SplitJoin43_Butterfly_Fiss_4224_4249_join[2];
buffer_float_t SplitJoin43_Butterfly_Fiss_4224_4249_split[2];
buffer_float_t Pre_CollapsedDataParallel_1_4014WEIGHTED_ROUND_ROBIN_Splitter_4198;
buffer_float_t SplitJoin39_Butterfly_Fiss_4223_4248_join[2];
buffer_float_t SplitJoin14_Butterfly_Fiss_4219_4247_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4121BitReverse_3761;
buffer_float_t SplitJoin0_Butterfly_Fiss_4214_4235_split[13];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4199Post_CollapsedDataParallel_2_4015;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4161Post_CollapsedDataParallel_2_3988;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4195Post_CollapsedDataParallel_2_4012;
buffer_float_t SplitJoin65_Butterfly_Fiss_4229_4255_join[2];
buffer_float_t BitReverse_3761FloatPrinter_3762;
buffer_float_t SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_child0_4074_4238_split[2];
buffer_float_t Pre_CollapsedDataParallel_1_4020WEIGHTED_ROUND_ROBIN_Splitter_4206;
buffer_float_t SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_Hier_4218_4245_split[2];
buffer_float_t SplitJoin95_Butterfly_Fiss_4233_4244_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4191Post_CollapsedDataParallel_2_4009;
buffer_float_t Pre_CollapsedDataParallel_1_4005WEIGHTED_ROUND_ROBIN_Splitter_4186;
buffer_float_t SplitJoin47_Butterfly_Fiss_4225_4250_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4124Post_CollapsedDataParallel_2_3982;
buffer_float_t SplitJoin0_Butterfly_Fiss_4214_4235_join[13];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4149Post_CollapsedDataParallel_2_3991;
buffer_float_t SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_Hier_4220_4256_join[2];
buffer_float_t Pre_CollapsedDataParallel_1_3999WEIGHTED_ROUND_ROBIN_Splitter_4176;
buffer_float_t SplitJoin85_Butterfly_Fiss_4231_4241_split[8];
buffer_float_t SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_Hier_4220_4256_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4139Post_CollapsedDataParallel_2_3985;
buffer_float_t Pre_CollapsedDataParallel_1_3990WEIGHTED_ROUND_ROBIN_Splitter_4148;
buffer_float_t Pre_CollapsedDataParallel_1_4011WEIGHTED_ROUND_ROBIN_Splitter_4194;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4115WEIGHTED_ROUND_ROBIN_Splitter_4116;
buffer_float_t SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child0_4221_4257_join[8];
buffer_float_t SplitJoin57_Butterfly_Fiss_4227_4253_join[2];
buffer_float_t Pre_CollapsedDataParallel_1_3981WEIGHTED_ROUND_ROBIN_Splitter_4123;
buffer_float_t SplitJoin57_Butterfly_Fiss_4227_4253_split[2];
buffer_float_t FloatSource_3680Pre_CollapsedDataParallel_1_3981;
buffer_float_t SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child0_4221_4257_split[8];
buffer_float_t SplitJoin4_Butterfly_Fiss_4216_4237_join[8];
buffer_float_t SplitJoin8_Butterfly_Fiss_4217_4239_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4177Post_CollapsedDataParallel_2_4000;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4183Post_CollapsedDataParallel_2_4003;
buffer_float_t SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_child1_4104_4251_split[4];
buffer_float_t SplitJoin53_Butterfly_Fiss_4226_4252_split[2];
buffer_float_t Pre_CollapsedDataParallel_1_3987WEIGHTED_ROUND_ROBIN_Splitter_4160;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4207Post_CollapsedDataParallel_2_4021;
buffer_float_t SplitJoin8_Butterfly_Fiss_4217_4239_join[4];
buffer_float_t SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_child1_4079_4242_join[2];
buffer_float_t SplitJoin89_Butterfly_Fiss_4232_4243_split[4];
buffer_float_t SplitJoin53_Butterfly_Fiss_4226_4252_join[2];
buffer_float_t Pre_CollapsedDataParallel_1_4008WEIGHTED_ROUND_ROBIN_Splitter_4190;
buffer_float_t SplitJoin65_Butterfly_Fiss_4229_4255_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4187Post_CollapsedDataParallel_2_4006;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4171Post_CollapsedDataParallel_2_3997;
buffer_float_t Post_CollapsedDataParallel_2_3982WEIGHTED_ROUND_ROBIN_Splitter_4025;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4203Post_CollapsedDataParallel_2_4018;
buffer_float_t Pre_CollapsedDataParallel_1_4023WEIGHTED_ROUND_ROBIN_Splitter_4210;
buffer_float_t SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_child1_4104_4251_join[4];


FloatSource_3680_t FloatSource_3680_s;

void FloatSource(buffer_float_t *chanout) {
		push_float(&(*chanout), FloatSource_3680_s.A_re[FloatSource_3680_s.idx]) ; 
		push_float(&(*chanout), FloatSource_3680_s.A_im[FloatSource_3680_s.idx]) ; 
		FloatSource_3680_s.idx++ ; 
		if((FloatSource_3680_s.idx >= 32)) {
			FloatSource_3680_s.idx = 0 ; 
		}
	}


void FloatSource_3680() {
	FOR(uint32_t, __iter_steady_, 0, <, 416, __iter_steady_++)
		FloatSource(&(FloatSource_3680Pre_CollapsedDataParallel_1_3981));
	ENDFOR
}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
		int partialSum_k = 0;
 {
		FOR(int, _k, 0,  < , 16, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k;
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 32) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_3981() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(FloatSource_3680Pre_CollapsedDataParallel_1_3981), &(Pre_CollapsedDataParallel_1_3981WEIGHTED_ROUND_ROBIN_Splitter_4123));
	ENDFOR
}

void Butterfly(buffer_float_t *chanin, buffer_float_t *chanout) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&(*chanin)) ; 
		u_im = pop_float(&(*chanin)) ; 
		t_re = pop_float(&(*chanin)) ; 
		t_im = pop_float(&(*chanin)) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&(*chanout), u_re) ; 
		push_float(&(*chanout), u_im) ; 
		push_float(&(*chanout), t_re) ; 
		push_float(&(*chanout), t_im) ; 
	}


void Butterfly_4125() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_4214_4235_split[0]), &(SplitJoin0_Butterfly_Fiss_4214_4235_join[0]));
	ENDFOR
}

void Butterfly_4126() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_4214_4235_split[1]), &(SplitJoin0_Butterfly_Fiss_4214_4235_join[1]));
	ENDFOR
}

void Butterfly_4127() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_4214_4235_split[2]), &(SplitJoin0_Butterfly_Fiss_4214_4235_join[2]));
	ENDFOR
}

void Butterfly_4128() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_4214_4235_split[3]), &(SplitJoin0_Butterfly_Fiss_4214_4235_join[3]));
	ENDFOR
}

void Butterfly_4129() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_4214_4235_split[4]), &(SplitJoin0_Butterfly_Fiss_4214_4235_join[4]));
	ENDFOR
}

void Butterfly_4130() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_4214_4235_split[5]), &(SplitJoin0_Butterfly_Fiss_4214_4235_join[5]));
	ENDFOR
}

void Butterfly_4131() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_4214_4235_split[6]), &(SplitJoin0_Butterfly_Fiss_4214_4235_join[6]));
	ENDFOR
}

void Butterfly_4132() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_4214_4235_split[7]), &(SplitJoin0_Butterfly_Fiss_4214_4235_join[7]));
	ENDFOR
}

void Butterfly_4133() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_4214_4235_split[8]), &(SplitJoin0_Butterfly_Fiss_4214_4235_join[8]));
	ENDFOR
}

void Butterfly_4134() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_4214_4235_split[9]), &(SplitJoin0_Butterfly_Fiss_4214_4235_join[9]));
	ENDFOR
}

void Butterfly_4135() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_4214_4235_split[10]), &(SplitJoin0_Butterfly_Fiss_4214_4235_join[10]));
	ENDFOR
}

void Butterfly_4136() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_4214_4235_split[11]), &(SplitJoin0_Butterfly_Fiss_4214_4235_join[11]));
	ENDFOR
}

void Butterfly_4137() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_4214_4235_split[12]), &(SplitJoin0_Butterfly_Fiss_4214_4235_join[12]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4123() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 13, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin0_Butterfly_Fiss_4214_4235_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_3981WEIGHTED_ROUND_ROBIN_Splitter_4123));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4124() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 13, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4124Post_CollapsedDataParallel_2_3982, pop_float(&SplitJoin0_Butterfly_Fiss_4214_4235_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
		int kTimesWeights_i = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 16, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&(*chanout), peek_float(&(*chanin), (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_3982() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4124Post_CollapsedDataParallel_2_3982), &(Post_CollapsedDataParallel_2_3982WEIGHTED_ROUND_ROBIN_Splitter_4025));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_3984() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_3638_4033_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_Hier_4215_4236_split[0]), &(Pre_CollapsedDataParallel_1_3984WEIGHTED_ROUND_ROBIN_Splitter_4138));
	ENDFOR
}

void Butterfly_4140() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_4216_4237_split[0]), &(SplitJoin4_Butterfly_Fiss_4216_4237_join[0]));
	ENDFOR
}

void Butterfly_4141() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_4216_4237_split[1]), &(SplitJoin4_Butterfly_Fiss_4216_4237_join[1]));
	ENDFOR
}

void Butterfly_4142() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_4216_4237_split[2]), &(SplitJoin4_Butterfly_Fiss_4216_4237_join[2]));
	ENDFOR
}

void Butterfly_4143() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_4216_4237_split[3]), &(SplitJoin4_Butterfly_Fiss_4216_4237_join[3]));
	ENDFOR
}

void Butterfly_4144() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_4216_4237_split[4]), &(SplitJoin4_Butterfly_Fiss_4216_4237_join[4]));
	ENDFOR
}

void Butterfly_4145() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_4216_4237_split[5]), &(SplitJoin4_Butterfly_Fiss_4216_4237_join[5]));
	ENDFOR
}

void Butterfly_4146() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_4216_4237_split[6]), &(SplitJoin4_Butterfly_Fiss_4216_4237_join[6]));
	ENDFOR
}

void Butterfly_4147() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_4216_4237_split[7]), &(SplitJoin4_Butterfly_Fiss_4216_4237_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4138() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin4_Butterfly_Fiss_4216_4237_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_3984WEIGHTED_ROUND_ROBIN_Splitter_4138));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4139() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4139Post_CollapsedDataParallel_2_3985, pop_float(&SplitJoin4_Butterfly_Fiss_4216_4237_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_3985() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4139Post_CollapsedDataParallel_2_3985), &(Post_CollapsedDataParallel_2_3985WEIGHTED_ROUND_ROBIN_Splitter_4105));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_3990() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_child0_4074_4238_split[0]), &(Pre_CollapsedDataParallel_1_3990WEIGHTED_ROUND_ROBIN_Splitter_4148));
	ENDFOR
}

void Butterfly_4150() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin8_Butterfly_Fiss_4217_4239_split[0]), &(SplitJoin8_Butterfly_Fiss_4217_4239_join[0]));
	ENDFOR
}

void Butterfly_4151() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin8_Butterfly_Fiss_4217_4239_split[1]), &(SplitJoin8_Butterfly_Fiss_4217_4239_join[1]));
	ENDFOR
}

void Butterfly_4152() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin8_Butterfly_Fiss_4217_4239_split[2]), &(SplitJoin8_Butterfly_Fiss_4217_4239_join[2]));
	ENDFOR
}

void Butterfly_4153() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin8_Butterfly_Fiss_4217_4239_split[3]), &(SplitJoin8_Butterfly_Fiss_4217_4239_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4148() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin8_Butterfly_Fiss_4217_4239_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_3990WEIGHTED_ROUND_ROBIN_Splitter_4148));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4149() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4149Post_CollapsedDataParallel_2_3991, pop_float(&SplitJoin8_Butterfly_Fiss_4217_4239_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_3991() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4149Post_CollapsedDataParallel_2_3991), &(SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_child0_4074_4238_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_3993() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_child0_4074_4238_split[1]), &(Pre_CollapsedDataParallel_1_3993WEIGHTED_ROUND_ROBIN_Splitter_4154));
	ENDFOR
}

void Butterfly_4156() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin72_Butterfly_Fiss_4230_4240_split[0]), &(SplitJoin72_Butterfly_Fiss_4230_4240_join[0]));
	ENDFOR
}

void Butterfly_4157() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin72_Butterfly_Fiss_4230_4240_split[1]), &(SplitJoin72_Butterfly_Fiss_4230_4240_join[1]));
	ENDFOR
}

void Butterfly_4158() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin72_Butterfly_Fiss_4230_4240_split[2]), &(SplitJoin72_Butterfly_Fiss_4230_4240_join[2]));
	ENDFOR
}

void Butterfly_4159() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin72_Butterfly_Fiss_4230_4240_split[3]), &(SplitJoin72_Butterfly_Fiss_4230_4240_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4154() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin72_Butterfly_Fiss_4230_4240_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_3993WEIGHTED_ROUND_ROBIN_Splitter_4154));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4155() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4155Post_CollapsedDataParallel_2_3994, pop_float(&SplitJoin72_Butterfly_Fiss_4230_4240_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_3994() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4155Post_CollapsedDataParallel_2_3994), &(SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_child0_4074_4238_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4105() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_child0_4074_4238_split[0], pop_float(&Post_CollapsedDataParallel_2_3985WEIGHTED_ROUND_ROBIN_Splitter_4105));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_child0_4074_4238_split[1], pop_float(&Post_CollapsedDataParallel_2_3985WEIGHTED_ROUND_ROBIN_Splitter_4105));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4106() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_3638_4033_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_Hier_4215_4236_join[0], pop_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_child0_4074_4238_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_3638_4033_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_Hier_4215_4236_join[0], pop_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_child0_4074_4238_join[1]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_3987() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_3638_4033_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_Hier_4215_4236_split[1]), &(Pre_CollapsedDataParallel_1_3987WEIGHTED_ROUND_ROBIN_Splitter_4160));
	ENDFOR
}

void Butterfly_4162() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_4231_4241_split[0]), &(SplitJoin85_Butterfly_Fiss_4231_4241_join[0]));
	ENDFOR
}

void Butterfly_4163() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_4231_4241_split[1]), &(SplitJoin85_Butterfly_Fiss_4231_4241_join[1]));
	ENDFOR
}

void Butterfly_4164() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_4231_4241_split[2]), &(SplitJoin85_Butterfly_Fiss_4231_4241_join[2]));
	ENDFOR
}

void Butterfly_4165() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_4231_4241_split[3]), &(SplitJoin85_Butterfly_Fiss_4231_4241_join[3]));
	ENDFOR
}

void Butterfly_4166() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_4231_4241_split[4]), &(SplitJoin85_Butterfly_Fiss_4231_4241_join[4]));
	ENDFOR
}

void Butterfly_4167() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_4231_4241_split[5]), &(SplitJoin85_Butterfly_Fiss_4231_4241_join[5]));
	ENDFOR
}

void Butterfly_4168() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_4231_4241_split[6]), &(SplitJoin85_Butterfly_Fiss_4231_4241_join[6]));
	ENDFOR
}

void Butterfly_4169() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_4231_4241_split[7]), &(SplitJoin85_Butterfly_Fiss_4231_4241_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4160() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin85_Butterfly_Fiss_4231_4241_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_3987WEIGHTED_ROUND_ROBIN_Splitter_4160));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4161() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4161Post_CollapsedDataParallel_2_3988, pop_float(&SplitJoin85_Butterfly_Fiss_4231_4241_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_3988() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4161Post_CollapsedDataParallel_2_3988), &(Post_CollapsedDataParallel_2_3988WEIGHTED_ROUND_ROBIN_Splitter_4107));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_3996() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_child1_4079_4242_split[0]), &(Pre_CollapsedDataParallel_1_3996WEIGHTED_ROUND_ROBIN_Splitter_4170));
	ENDFOR
}

void Butterfly_4172() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin89_Butterfly_Fiss_4232_4243_split[0]), &(SplitJoin89_Butterfly_Fiss_4232_4243_join[0]));
	ENDFOR
}

void Butterfly_4173() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin89_Butterfly_Fiss_4232_4243_split[1]), &(SplitJoin89_Butterfly_Fiss_4232_4243_join[1]));
	ENDFOR
}

void Butterfly_4174() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin89_Butterfly_Fiss_4232_4243_split[2]), &(SplitJoin89_Butterfly_Fiss_4232_4243_join[2]));
	ENDFOR
}

void Butterfly_4175() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin89_Butterfly_Fiss_4232_4243_split[3]), &(SplitJoin89_Butterfly_Fiss_4232_4243_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4170() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin89_Butterfly_Fiss_4232_4243_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_3996WEIGHTED_ROUND_ROBIN_Splitter_4170));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4171() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4171Post_CollapsedDataParallel_2_3997, pop_float(&SplitJoin89_Butterfly_Fiss_4232_4243_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_3997() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4171Post_CollapsedDataParallel_2_3997), &(SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_child1_4079_4242_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_3999() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_child1_4079_4242_split[1]), &(Pre_CollapsedDataParallel_1_3999WEIGHTED_ROUND_ROBIN_Splitter_4176));
	ENDFOR
}

void Butterfly_4178() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin95_Butterfly_Fiss_4233_4244_split[0]), &(SplitJoin95_Butterfly_Fiss_4233_4244_join[0]));
	ENDFOR
}

void Butterfly_4179() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin95_Butterfly_Fiss_4233_4244_split[1]), &(SplitJoin95_Butterfly_Fiss_4233_4244_join[1]));
	ENDFOR
}

void Butterfly_4180() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin95_Butterfly_Fiss_4233_4244_split[2]), &(SplitJoin95_Butterfly_Fiss_4233_4244_join[2]));
	ENDFOR
}

void Butterfly_4181() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin95_Butterfly_Fiss_4233_4244_split[3]), &(SplitJoin95_Butterfly_Fiss_4233_4244_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4176() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin95_Butterfly_Fiss_4233_4244_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_3999WEIGHTED_ROUND_ROBIN_Splitter_4176));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4177() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4177Post_CollapsedDataParallel_2_4000, pop_float(&SplitJoin95_Butterfly_Fiss_4233_4244_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_4000() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4177Post_CollapsedDataParallel_2_4000), &(SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_child1_4079_4242_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4107() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_child1_4079_4242_split[0], pop_float(&Post_CollapsedDataParallel_2_3988WEIGHTED_ROUND_ROBIN_Splitter_4107));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_child1_4079_4242_split[1], pop_float(&Post_CollapsedDataParallel_2_3988WEIGHTED_ROUND_ROBIN_Splitter_4107));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4108() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_3638_4033_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_Hier_4215_4236_join[1], pop_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_child1_4079_4242_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_3638_4033_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_Hier_4215_4236_join[1], pop_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_child1_4079_4242_join[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_4025() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_3638_4033_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_Hier_4215_4236_split[0], pop_float(&Post_CollapsedDataParallel_2_3982WEIGHTED_ROUND_ROBIN_Splitter_4025));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_3638_4033_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_Hier_4215_4236_split[1], pop_float(&Post_CollapsedDataParallel_2_3982WEIGHTED_ROUND_ROBIN_Splitter_4025));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4109() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4109WEIGHTED_ROUND_ROBIN_Splitter_4110, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_3638_4033_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_Hier_4215_4236_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4109WEIGHTED_ROUND_ROBIN_Splitter_4110, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_3638_4033_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_Hier_4215_4236_join[1]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_4002() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_child0_4103_4246_split[0]), &(Pre_CollapsedDataParallel_1_4002WEIGHTED_ROUND_ROBIN_Splitter_4182));
	ENDFOR
}

void Butterfly_4184() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin14_Butterfly_Fiss_4219_4247_split[0]), &(SplitJoin14_Butterfly_Fiss_4219_4247_join[0]));
	ENDFOR
}

void Butterfly_4185() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin14_Butterfly_Fiss_4219_4247_split[1]), &(SplitJoin14_Butterfly_Fiss_4219_4247_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4182() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin14_Butterfly_Fiss_4219_4247_split[0], pop_float(&Pre_CollapsedDataParallel_1_4002WEIGHTED_ROUND_ROBIN_Splitter_4182));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin14_Butterfly_Fiss_4219_4247_split[1], pop_float(&Pre_CollapsedDataParallel_1_4002WEIGHTED_ROUND_ROBIN_Splitter_4182));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4183() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4183Post_CollapsedDataParallel_2_4003, pop_float(&SplitJoin14_Butterfly_Fiss_4219_4247_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4183Post_CollapsedDataParallel_2_4003, pop_float(&SplitJoin14_Butterfly_Fiss_4219_4247_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_4003() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4183Post_CollapsedDataParallel_2_4003), &(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_child0_4103_4246_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_4005() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_child0_4103_4246_split[1]), &(Pre_CollapsedDataParallel_1_4005WEIGHTED_ROUND_ROBIN_Splitter_4186));
	ENDFOR
}

void Butterfly_4188() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin39_Butterfly_Fiss_4223_4248_split[0]), &(SplitJoin39_Butterfly_Fiss_4223_4248_join[0]));
	ENDFOR
}

void Butterfly_4189() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin39_Butterfly_Fiss_4223_4248_split[1]), &(SplitJoin39_Butterfly_Fiss_4223_4248_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4186() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin39_Butterfly_Fiss_4223_4248_split[0], pop_float(&Pre_CollapsedDataParallel_1_4005WEIGHTED_ROUND_ROBIN_Splitter_4186));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin39_Butterfly_Fiss_4223_4248_split[1], pop_float(&Pre_CollapsedDataParallel_1_4005WEIGHTED_ROUND_ROBIN_Splitter_4186));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4187() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4187Post_CollapsedDataParallel_2_4006, pop_float(&SplitJoin39_Butterfly_Fiss_4223_4248_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4187Post_CollapsedDataParallel_2_4006, pop_float(&SplitJoin39_Butterfly_Fiss_4223_4248_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_4006() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4187Post_CollapsedDataParallel_2_4006), &(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_child0_4103_4246_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_4008() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_child0_4103_4246_split[2]), &(Pre_CollapsedDataParallel_1_4008WEIGHTED_ROUND_ROBIN_Splitter_4190));
	ENDFOR
}

void Butterfly_4192() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin43_Butterfly_Fiss_4224_4249_split[0]), &(SplitJoin43_Butterfly_Fiss_4224_4249_join[0]));
	ENDFOR
}

void Butterfly_4193() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin43_Butterfly_Fiss_4224_4249_split[1]), &(SplitJoin43_Butterfly_Fiss_4224_4249_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4190() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin43_Butterfly_Fiss_4224_4249_split[0], pop_float(&Pre_CollapsedDataParallel_1_4008WEIGHTED_ROUND_ROBIN_Splitter_4190));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin43_Butterfly_Fiss_4224_4249_split[1], pop_float(&Pre_CollapsedDataParallel_1_4008WEIGHTED_ROUND_ROBIN_Splitter_4190));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4191() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4191Post_CollapsedDataParallel_2_4009, pop_float(&SplitJoin43_Butterfly_Fiss_4224_4249_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4191Post_CollapsedDataParallel_2_4009, pop_float(&SplitJoin43_Butterfly_Fiss_4224_4249_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_4009() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4191Post_CollapsedDataParallel_2_4009), &(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_child0_4103_4246_join[2]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_4011() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_child0_4103_4246_split[3]), &(Pre_CollapsedDataParallel_1_4011WEIGHTED_ROUND_ROBIN_Splitter_4194));
	ENDFOR
}

void Butterfly_4196() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin47_Butterfly_Fiss_4225_4250_split[0]), &(SplitJoin47_Butterfly_Fiss_4225_4250_join[0]));
	ENDFOR
}

void Butterfly_4197() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin47_Butterfly_Fiss_4225_4250_split[1]), &(SplitJoin47_Butterfly_Fiss_4225_4250_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4194() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin47_Butterfly_Fiss_4225_4250_split[0], pop_float(&Pre_CollapsedDataParallel_1_4011WEIGHTED_ROUND_ROBIN_Splitter_4194));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin47_Butterfly_Fiss_4225_4250_split[1], pop_float(&Pre_CollapsedDataParallel_1_4011WEIGHTED_ROUND_ROBIN_Splitter_4194));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4195() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4195Post_CollapsedDataParallel_2_4012, pop_float(&SplitJoin47_Butterfly_Fiss_4225_4250_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4195Post_CollapsedDataParallel_2_4012, pop_float(&SplitJoin47_Butterfly_Fiss_4225_4250_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_4012() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4195Post_CollapsedDataParallel_2_4012), &(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_child0_4103_4246_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4111() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_child0_4103_4246_split[__iter_dec_], pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_Hier_4218_4245_split[0]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4112() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_Hier_4218_4245_join[0], pop_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_child0_4103_4246_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_4014() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_child1_4104_4251_split[0]), &(Pre_CollapsedDataParallel_1_4014WEIGHTED_ROUND_ROBIN_Splitter_4198));
	ENDFOR
}

void Butterfly_4200() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin53_Butterfly_Fiss_4226_4252_split[0]), &(SplitJoin53_Butterfly_Fiss_4226_4252_join[0]));
	ENDFOR
}

void Butterfly_4201() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin53_Butterfly_Fiss_4226_4252_split[1]), &(SplitJoin53_Butterfly_Fiss_4226_4252_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4198() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin53_Butterfly_Fiss_4226_4252_split[0], pop_float(&Pre_CollapsedDataParallel_1_4014WEIGHTED_ROUND_ROBIN_Splitter_4198));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin53_Butterfly_Fiss_4226_4252_split[1], pop_float(&Pre_CollapsedDataParallel_1_4014WEIGHTED_ROUND_ROBIN_Splitter_4198));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4199() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4199Post_CollapsedDataParallel_2_4015, pop_float(&SplitJoin53_Butterfly_Fiss_4226_4252_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4199Post_CollapsedDataParallel_2_4015, pop_float(&SplitJoin53_Butterfly_Fiss_4226_4252_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_4015() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4199Post_CollapsedDataParallel_2_4015), &(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_child1_4104_4251_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_4017() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_child1_4104_4251_split[1]), &(Pre_CollapsedDataParallel_1_4017WEIGHTED_ROUND_ROBIN_Splitter_4202));
	ENDFOR
}

void Butterfly_4204() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin57_Butterfly_Fiss_4227_4253_split[0]), &(SplitJoin57_Butterfly_Fiss_4227_4253_join[0]));
	ENDFOR
}

void Butterfly_4205() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin57_Butterfly_Fiss_4227_4253_split[1]), &(SplitJoin57_Butterfly_Fiss_4227_4253_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4202() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin57_Butterfly_Fiss_4227_4253_split[0], pop_float(&Pre_CollapsedDataParallel_1_4017WEIGHTED_ROUND_ROBIN_Splitter_4202));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin57_Butterfly_Fiss_4227_4253_split[1], pop_float(&Pre_CollapsedDataParallel_1_4017WEIGHTED_ROUND_ROBIN_Splitter_4202));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4203() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4203Post_CollapsedDataParallel_2_4018, pop_float(&SplitJoin57_Butterfly_Fiss_4227_4253_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4203Post_CollapsedDataParallel_2_4018, pop_float(&SplitJoin57_Butterfly_Fiss_4227_4253_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_4018() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4203Post_CollapsedDataParallel_2_4018), &(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_child1_4104_4251_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_4020() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_child1_4104_4251_split[2]), &(Pre_CollapsedDataParallel_1_4020WEIGHTED_ROUND_ROBIN_Splitter_4206));
	ENDFOR
}

void Butterfly_4208() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin61_Butterfly_Fiss_4228_4254_split[0]), &(SplitJoin61_Butterfly_Fiss_4228_4254_join[0]));
	ENDFOR
}

void Butterfly_4209() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin61_Butterfly_Fiss_4228_4254_split[1]), &(SplitJoin61_Butterfly_Fiss_4228_4254_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4206() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin61_Butterfly_Fiss_4228_4254_split[0], pop_float(&Pre_CollapsedDataParallel_1_4020WEIGHTED_ROUND_ROBIN_Splitter_4206));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin61_Butterfly_Fiss_4228_4254_split[1], pop_float(&Pre_CollapsedDataParallel_1_4020WEIGHTED_ROUND_ROBIN_Splitter_4206));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4207() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4207Post_CollapsedDataParallel_2_4021, pop_float(&SplitJoin61_Butterfly_Fiss_4228_4254_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4207Post_CollapsedDataParallel_2_4021, pop_float(&SplitJoin61_Butterfly_Fiss_4228_4254_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_4021() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4207Post_CollapsedDataParallel_2_4021), &(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_child1_4104_4251_join[2]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_4023() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_child1_4104_4251_split[3]), &(Pre_CollapsedDataParallel_1_4023WEIGHTED_ROUND_ROBIN_Splitter_4210));
	ENDFOR
}

void Butterfly_4212() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin65_Butterfly_Fiss_4229_4255_split[0]), &(SplitJoin65_Butterfly_Fiss_4229_4255_join[0]));
	ENDFOR
}

void Butterfly_4213() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin65_Butterfly_Fiss_4229_4255_split[1]), &(SplitJoin65_Butterfly_Fiss_4229_4255_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4210() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin65_Butterfly_Fiss_4229_4255_split[0], pop_float(&Pre_CollapsedDataParallel_1_4023WEIGHTED_ROUND_ROBIN_Splitter_4210));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin65_Butterfly_Fiss_4229_4255_split[1], pop_float(&Pre_CollapsedDataParallel_1_4023WEIGHTED_ROUND_ROBIN_Splitter_4210));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4211() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4211Post_CollapsedDataParallel_2_4024, pop_float(&SplitJoin65_Butterfly_Fiss_4229_4255_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4211Post_CollapsedDataParallel_2_4024, pop_float(&SplitJoin65_Butterfly_Fiss_4229_4255_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_4024() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4211Post_CollapsedDataParallel_2_4024), &(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_child1_4104_4251_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4113() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_child1_4104_4251_split[__iter_dec_], pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_Hier_4218_4245_split[1]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4114() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_Hier_4218_4245_join[1], pop_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_child1_4104_4251_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_4110() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_Hier_4218_4245_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4109WEIGHTED_ROUND_ROBIN_Splitter_4110));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_Hier_4218_4245_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4109WEIGHTED_ROUND_ROBIN_Splitter_4110));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4115() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4115WEIGHTED_ROUND_ROBIN_Splitter_4116, pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_Hier_4218_4245_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4115WEIGHTED_ROUND_ROBIN_Splitter_4116, pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_Hier_4218_4245_join[1]));
		ENDFOR
	ENDFOR
}}

void Butterfly_3745() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child0_4221_4257_split[0]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child0_4221_4257_join[0]));
	ENDFOR
}

void Butterfly_3746() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child0_4221_4257_split[1]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child0_4221_4257_join[1]));
	ENDFOR
}

void Butterfly_3747() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child0_4221_4257_split[2]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child0_4221_4257_join[2]));
	ENDFOR
}

void Butterfly_3748() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child0_4221_4257_split[3]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child0_4221_4257_join[3]));
	ENDFOR
}

void Butterfly_3749() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child0_4221_4257_split[4]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child0_4221_4257_join[4]));
	ENDFOR
}

void Butterfly_3750() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child0_4221_4257_split[5]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child0_4221_4257_join[5]));
	ENDFOR
}

void Butterfly_3751() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child0_4221_4257_split[6]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child0_4221_4257_join[6]));
	ENDFOR
}

void Butterfly_3752() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child0_4221_4257_split[7]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child0_4221_4257_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4117() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child0_4221_4257_split[__iter_dec_], pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_Hier_4220_4256_split[0]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4118() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_Hier_4220_4256_join[0], pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child0_4221_4257_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Butterfly_3753() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child1_4222_4258_split[0]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child1_4222_4258_join[0]));
	ENDFOR
}

void Butterfly_3754() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child1_4222_4258_split[1]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child1_4222_4258_join[1]));
	ENDFOR
}

void Butterfly_3755() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child1_4222_4258_split[2]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child1_4222_4258_join[2]));
	ENDFOR
}

void Butterfly_3756() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child1_4222_4258_split[3]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child1_4222_4258_join[3]));
	ENDFOR
}

void Butterfly_3757() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child1_4222_4258_split[4]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child1_4222_4258_join[4]));
	ENDFOR
}

void Butterfly_3758() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child1_4222_4258_split[5]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child1_4222_4258_join[5]));
	ENDFOR
}

void Butterfly_3759() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child1_4222_4258_split[6]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child1_4222_4258_join[6]));
	ENDFOR
}

void Butterfly_3760() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child1_4222_4258_split[7]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child1_4222_4258_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4119() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child1_4222_4258_split[__iter_dec_], pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_Hier_4220_4256_split[1]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4120() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_Hier_4220_4256_join[1], pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child1_4222_4258_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_4116() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_Hier_4220_4256_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4115WEIGHTED_ROUND_ROBIN_Splitter_4116));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_Hier_4220_4256_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4115WEIGHTED_ROUND_ROBIN_Splitter_4116));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4121() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4121BitReverse_3761, pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_Hier_4220_4256_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4121BitReverse_3761, pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_Hier_4220_4256_join[1]));
		ENDFOR
	ENDFOR
}}

int BitReverse_3761_bitrev(int inp, int numbits) {
	int rev = 0;
	FOR(int, i, 0,  < , numbits, i++) {
		rev = ((rev * 2) | (inp & 1)) ; 
		inp = (inp / 2) ; 
	}
	ENDFOR
	return rev;
}
void BitReverse(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, i__conflict__0, 0,  < , 32, i__conflict__0++) {
			int br = 0;
			br = BitReverse_3761_bitrev(i__conflict__0, 5) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (2 * br))) ; 
			push_float(&(*chanout), peek_float(&(*chanin), ((2 * br) + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&(*chanin)) ; 
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void BitReverse_3761() {
	FOR(uint32_t, __iter_steady_, 0, <, 13, __iter_steady_++)
		BitReverse(&(WEIGHTED_ROUND_ROBIN_Joiner_4121BitReverse_3761), &(BitReverse_3761FloatPrinter_3762));
	ENDFOR
}

void FloatPrinter(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void FloatPrinter_3762() {
	FOR(uint32_t, __iter_steady_, 0, <, 416, __iter_steady_++)
		FloatPrinter(&(BitReverse_3761FloatPrinter_3762));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&Pre_CollapsedDataParallel_1_3993WEIGHTED_ROUND_ROBIN_Splitter_4154);
	init_buffer_float(&Pre_CollapsedDataParallel_1_4002WEIGHTED_ROUND_ROBIN_Splitter_4182);
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_float(&SplitJoin4_Butterfly_Fiss_4216_4237_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 4, __iter_init_1_++)
		init_buffer_float(&SplitJoin89_Butterfly_Fiss_4232_4243_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_child1_4079_4242_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 4, __iter_init_3_++)
		init_buffer_float(&SplitJoin95_Butterfly_Fiss_4233_4244_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 2, __iter_init_4_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_3638_4033_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_Hier_4215_4236_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 4, __iter_init_5_++)
		init_buffer_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_child0_4103_4246_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 4, __iter_init_6_++)
		init_buffer_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_child0_4103_4246_join[__iter_init_6_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_3984WEIGHTED_ROUND_ROBIN_Splitter_4138);
	FOR(int, __iter_init_7_, 0, <, 2, __iter_init_7_++)
		init_buffer_float(&SplitJoin39_Butterfly_Fiss_4223_4248_split[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_4017WEIGHTED_ROUND_ROBIN_Splitter_4202);
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_float(&SplitJoin61_Butterfly_Fiss_4228_4254_split[__iter_init_8_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_3996WEIGHTED_ROUND_ROBIN_Splitter_4170);
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child1_4222_4258_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_3638_4033_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_Hier_4215_4236_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 4, __iter_init_11_++)
		init_buffer_float(&SplitJoin72_Butterfly_Fiss_4230_4240_split[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_3988WEIGHTED_ROUND_ROBIN_Splitter_4107);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4155Post_CollapsedDataParallel_2_3994);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4109WEIGHTED_ROUND_ROBIN_Splitter_4110);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4211Post_CollapsedDataParallel_2_4024);
	FOR(int, __iter_init_12_, 0, <, 2, __iter_init_12_++)
		init_buffer_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_child0_4074_4238_join[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 8, __iter_init_13_++)
		init_buffer_float(&SplitJoin85_Butterfly_Fiss_4231_4241_join[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_Hier_4218_4245_join[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 2, __iter_init_15_++)
		init_buffer_float(&SplitJoin61_Butterfly_Fiss_4228_4254_join[__iter_init_15_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_3985WEIGHTED_ROUND_ROBIN_Splitter_4105);
	FOR(int, __iter_init_16_, 0, <, 8, __iter_init_16_++)
		init_buffer_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child1_4222_4258_split[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 4, __iter_init_17_++)
		init_buffer_float(&SplitJoin72_Butterfly_Fiss_4230_4240_join[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 2, __iter_init_18_++)
		init_buffer_float(&SplitJoin14_Butterfly_Fiss_4219_4247_split[__iter_init_18_]);
	ENDFOR
	FOR(int, __iter_init_19_, 0, <, 2, __iter_init_19_++)
		init_buffer_float(&SplitJoin47_Butterfly_Fiss_4225_4250_join[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 2, __iter_init_20_++)
		init_buffer_float(&SplitJoin43_Butterfly_Fiss_4224_4249_join[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 2, __iter_init_21_++)
		init_buffer_float(&SplitJoin43_Butterfly_Fiss_4224_4249_split[__iter_init_21_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_4014WEIGHTED_ROUND_ROBIN_Splitter_4198);
	FOR(int, __iter_init_22_, 0, <, 2, __iter_init_22_++)
		init_buffer_float(&SplitJoin39_Butterfly_Fiss_4223_4248_join[__iter_init_22_]);
	ENDFOR
	FOR(int, __iter_init_23_, 0, <, 2, __iter_init_23_++)
		init_buffer_float(&SplitJoin14_Butterfly_Fiss_4219_4247_join[__iter_init_23_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4121BitReverse_3761);
	FOR(int, __iter_init_24_, 0, <, 13, __iter_init_24_++)
		init_buffer_float(&SplitJoin0_Butterfly_Fiss_4214_4235_split[__iter_init_24_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4199Post_CollapsedDataParallel_2_4015);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4161Post_CollapsedDataParallel_2_3988);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4195Post_CollapsedDataParallel_2_4012);
	FOR(int, __iter_init_25_, 0, <, 2, __iter_init_25_++)
		init_buffer_float(&SplitJoin65_Butterfly_Fiss_4229_4255_join[__iter_init_25_]);
	ENDFOR
	init_buffer_float(&BitReverse_3761FloatPrinter_3762);
	FOR(int, __iter_init_26_, 0, <, 2, __iter_init_26_++)
		init_buffer_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_child0_4074_4238_split[__iter_init_26_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_4020WEIGHTED_ROUND_ROBIN_Splitter_4206);
	FOR(int, __iter_init_27_, 0, <, 2, __iter_init_27_++)
		init_buffer_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_Hier_4218_4245_split[__iter_init_27_]);
	ENDFOR
	FOR(int, __iter_init_28_, 0, <, 4, __iter_init_28_++)
		init_buffer_float(&SplitJoin95_Butterfly_Fiss_4233_4244_split[__iter_init_28_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4191Post_CollapsedDataParallel_2_4009);
	init_buffer_float(&Pre_CollapsedDataParallel_1_4005WEIGHTED_ROUND_ROBIN_Splitter_4186);
	FOR(int, __iter_init_29_, 0, <, 2, __iter_init_29_++)
		init_buffer_float(&SplitJoin47_Butterfly_Fiss_4225_4250_split[__iter_init_29_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4124Post_CollapsedDataParallel_2_3982);
	FOR(int, __iter_init_30_, 0, <, 13, __iter_init_30_++)
		init_buffer_float(&SplitJoin0_Butterfly_Fiss_4214_4235_join[__iter_init_30_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4149Post_CollapsedDataParallel_2_3991);
	FOR(int, __iter_init_31_, 0, <, 2, __iter_init_31_++)
		init_buffer_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_Hier_4220_4256_join[__iter_init_31_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_3999WEIGHTED_ROUND_ROBIN_Splitter_4176);
	FOR(int, __iter_init_32_, 0, <, 8, __iter_init_32_++)
		init_buffer_float(&SplitJoin85_Butterfly_Fiss_4231_4241_split[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 2, __iter_init_33_++)
		init_buffer_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_Hier_4220_4256_split[__iter_init_33_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4139Post_CollapsedDataParallel_2_3985);
	init_buffer_float(&Pre_CollapsedDataParallel_1_3990WEIGHTED_ROUND_ROBIN_Splitter_4148);
	init_buffer_float(&Pre_CollapsedDataParallel_1_4011WEIGHTED_ROUND_ROBIN_Splitter_4194);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4115WEIGHTED_ROUND_ROBIN_Splitter_4116);
	FOR(int, __iter_init_34_, 0, <, 8, __iter_init_34_++)
		init_buffer_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child0_4221_4257_join[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 2, __iter_init_35_++)
		init_buffer_float(&SplitJoin57_Butterfly_Fiss_4227_4253_join[__iter_init_35_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_3981WEIGHTED_ROUND_ROBIN_Splitter_4123);
	FOR(int, __iter_init_36_, 0, <, 2, __iter_init_36_++)
		init_buffer_float(&SplitJoin57_Butterfly_Fiss_4227_4253_split[__iter_init_36_]);
	ENDFOR
	init_buffer_float(&FloatSource_3680Pre_CollapsedDataParallel_1_3981);
	FOR(int, __iter_init_37_, 0, <, 8, __iter_init_37_++)
		init_buffer_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_3678_4039_Hier_child0_4221_4257_split[__iter_init_37_]);
	ENDFOR
	FOR(int, __iter_init_38_, 0, <, 8, __iter_init_38_++)
		init_buffer_float(&SplitJoin4_Butterfly_Fiss_4216_4237_join[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 4, __iter_init_39_++)
		init_buffer_float(&SplitJoin8_Butterfly_Fiss_4217_4239_split[__iter_init_39_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4177Post_CollapsedDataParallel_2_4000);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4183Post_CollapsedDataParallel_2_4003);
	FOR(int, __iter_init_40_, 0, <, 4, __iter_init_40_++)
		init_buffer_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_child1_4104_4251_split[__iter_init_40_]);
	ENDFOR
	FOR(int, __iter_init_41_, 0, <, 2, __iter_init_41_++)
		init_buffer_float(&SplitJoin53_Butterfly_Fiss_4226_4252_split[__iter_init_41_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_3987WEIGHTED_ROUND_ROBIN_Splitter_4160);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4207Post_CollapsedDataParallel_2_4021);
	FOR(int, __iter_init_42_, 0, <, 4, __iter_init_42_++)
		init_buffer_float(&SplitJoin8_Butterfly_Fiss_4217_4239_join[__iter_init_42_]);
	ENDFOR
	FOR(int, __iter_init_43_, 0, <, 2, __iter_init_43_++)
		init_buffer_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_3647_4035_child1_4079_4242_join[__iter_init_43_]);
	ENDFOR
	FOR(int, __iter_init_44_, 0, <, 4, __iter_init_44_++)
		init_buffer_float(&SplitJoin89_Butterfly_Fiss_4232_4243_split[__iter_init_44_]);
	ENDFOR
	FOR(int, __iter_init_45_, 0, <, 2, __iter_init_45_++)
		init_buffer_float(&SplitJoin53_Butterfly_Fiss_4226_4252_join[__iter_init_45_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_4008WEIGHTED_ROUND_ROBIN_Splitter_4190);
	FOR(int, __iter_init_46_, 0, <, 2, __iter_init_46_++)
		init_buffer_float(&SplitJoin65_Butterfly_Fiss_4229_4255_split[__iter_init_46_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4187Post_CollapsedDataParallel_2_4006);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4171Post_CollapsedDataParallel_2_3997);
	init_buffer_float(&Post_CollapsedDataParallel_2_3982WEIGHTED_ROUND_ROBIN_Splitter_4025);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4203Post_CollapsedDataParallel_2_4018);
	init_buffer_float(&Pre_CollapsedDataParallel_1_4023WEIGHTED_ROUND_ROBIN_Splitter_4210);
	FOR(int, __iter_init_47_, 0, <, 4, __iter_init_47_++)
		init_buffer_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_3660_4037_Hier_child1_4104_4251_join[__iter_init_47_]);
	ENDFOR
// --- init: FloatSource_3680
	 {
	FOR(int, i, 0,  < , 32, i++) {
		FloatSource_3680_s.A_re[i] = 0.0 ; 
		FloatSource_3680_s.A_im[i] = 0.0 ; 
	}
	ENDFOR
	FloatSource_3680_s.A_re[1] = 1.0 ; 
	FloatSource_3680_s.idx = 0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FloatSource_3680();
		Pre_CollapsedDataParallel_1_3981();
		WEIGHTED_ROUND_ROBIN_Splitter_4123();
			Butterfly_4125();
			Butterfly_4126();
			Butterfly_4127();
			Butterfly_4128();
			Butterfly_4129();
			Butterfly_4130();
			Butterfly_4131();
			Butterfly_4132();
			Butterfly_4133();
			Butterfly_4134();
			Butterfly_4135();
			Butterfly_4136();
			Butterfly_4137();
		WEIGHTED_ROUND_ROBIN_Joiner_4124();
		Post_CollapsedDataParallel_2_3982();
		WEIGHTED_ROUND_ROBIN_Splitter_4025();
			Pre_CollapsedDataParallel_1_3984();
			WEIGHTED_ROUND_ROBIN_Splitter_4138();
				Butterfly_4140();
				Butterfly_4141();
				Butterfly_4142();
				Butterfly_4143();
				Butterfly_4144();
				Butterfly_4145();
				Butterfly_4146();
				Butterfly_4147();
			WEIGHTED_ROUND_ROBIN_Joiner_4139();
			Post_CollapsedDataParallel_2_3985();
			WEIGHTED_ROUND_ROBIN_Splitter_4105();
				Pre_CollapsedDataParallel_1_3990();
				WEIGHTED_ROUND_ROBIN_Splitter_4148();
					Butterfly_4150();
					Butterfly_4151();
					Butterfly_4152();
					Butterfly_4153();
				WEIGHTED_ROUND_ROBIN_Joiner_4149();
				Post_CollapsedDataParallel_2_3991();
				Pre_CollapsedDataParallel_1_3993();
				WEIGHTED_ROUND_ROBIN_Splitter_4154();
					Butterfly_4156();
					Butterfly_4157();
					Butterfly_4158();
					Butterfly_4159();
				WEIGHTED_ROUND_ROBIN_Joiner_4155();
				Post_CollapsedDataParallel_2_3994();
			WEIGHTED_ROUND_ROBIN_Joiner_4106();
			Pre_CollapsedDataParallel_1_3987();
			WEIGHTED_ROUND_ROBIN_Splitter_4160();
				Butterfly_4162();
				Butterfly_4163();
				Butterfly_4164();
				Butterfly_4165();
				Butterfly_4166();
				Butterfly_4167();
				Butterfly_4168();
				Butterfly_4169();
			WEIGHTED_ROUND_ROBIN_Joiner_4161();
			Post_CollapsedDataParallel_2_3988();
			WEIGHTED_ROUND_ROBIN_Splitter_4107();
				Pre_CollapsedDataParallel_1_3996();
				WEIGHTED_ROUND_ROBIN_Splitter_4170();
					Butterfly_4172();
					Butterfly_4173();
					Butterfly_4174();
					Butterfly_4175();
				WEIGHTED_ROUND_ROBIN_Joiner_4171();
				Post_CollapsedDataParallel_2_3997();
				Pre_CollapsedDataParallel_1_3999();
				WEIGHTED_ROUND_ROBIN_Splitter_4176();
					Butterfly_4178();
					Butterfly_4179();
					Butterfly_4180();
					Butterfly_4181();
				WEIGHTED_ROUND_ROBIN_Joiner_4177();
				Post_CollapsedDataParallel_2_4000();
			WEIGHTED_ROUND_ROBIN_Joiner_4108();
		WEIGHTED_ROUND_ROBIN_Joiner_4109();
		WEIGHTED_ROUND_ROBIN_Splitter_4110();
			WEIGHTED_ROUND_ROBIN_Splitter_4111();
				Pre_CollapsedDataParallel_1_4002();
				WEIGHTED_ROUND_ROBIN_Splitter_4182();
					Butterfly_4184();
					Butterfly_4185();
				WEIGHTED_ROUND_ROBIN_Joiner_4183();
				Post_CollapsedDataParallel_2_4003();
				Pre_CollapsedDataParallel_1_4005();
				WEIGHTED_ROUND_ROBIN_Splitter_4186();
					Butterfly_4188();
					Butterfly_4189();
				WEIGHTED_ROUND_ROBIN_Joiner_4187();
				Post_CollapsedDataParallel_2_4006();
				Pre_CollapsedDataParallel_1_4008();
				WEIGHTED_ROUND_ROBIN_Splitter_4190();
					Butterfly_4192();
					Butterfly_4193();
				WEIGHTED_ROUND_ROBIN_Joiner_4191();
				Post_CollapsedDataParallel_2_4009();
				Pre_CollapsedDataParallel_1_4011();
				WEIGHTED_ROUND_ROBIN_Splitter_4194();
					Butterfly_4196();
					Butterfly_4197();
				WEIGHTED_ROUND_ROBIN_Joiner_4195();
				Post_CollapsedDataParallel_2_4012();
			WEIGHTED_ROUND_ROBIN_Joiner_4112();
			WEIGHTED_ROUND_ROBIN_Splitter_4113();
				Pre_CollapsedDataParallel_1_4014();
				WEIGHTED_ROUND_ROBIN_Splitter_4198();
					Butterfly_4200();
					Butterfly_4201();
				WEIGHTED_ROUND_ROBIN_Joiner_4199();
				Post_CollapsedDataParallel_2_4015();
				Pre_CollapsedDataParallel_1_4017();
				WEIGHTED_ROUND_ROBIN_Splitter_4202();
					Butterfly_4204();
					Butterfly_4205();
				WEIGHTED_ROUND_ROBIN_Joiner_4203();
				Post_CollapsedDataParallel_2_4018();
				Pre_CollapsedDataParallel_1_4020();
				WEIGHTED_ROUND_ROBIN_Splitter_4206();
					Butterfly_4208();
					Butterfly_4209();
				WEIGHTED_ROUND_ROBIN_Joiner_4207();
				Post_CollapsedDataParallel_2_4021();
				Pre_CollapsedDataParallel_1_4023();
				WEIGHTED_ROUND_ROBIN_Splitter_4210();
					Butterfly_4212();
					Butterfly_4213();
				WEIGHTED_ROUND_ROBIN_Joiner_4211();
				Post_CollapsedDataParallel_2_4024();
			WEIGHTED_ROUND_ROBIN_Joiner_4114();
		WEIGHTED_ROUND_ROBIN_Joiner_4115();
		WEIGHTED_ROUND_ROBIN_Splitter_4116();
			WEIGHTED_ROUND_ROBIN_Splitter_4117();
				Butterfly_3745();
				Butterfly_3746();
				Butterfly_3747();
				Butterfly_3748();
				Butterfly_3749();
				Butterfly_3750();
				Butterfly_3751();
				Butterfly_3752();
			WEIGHTED_ROUND_ROBIN_Joiner_4118();
			WEIGHTED_ROUND_ROBIN_Splitter_4119();
				Butterfly_3753();
				Butterfly_3754();
				Butterfly_3755();
				Butterfly_3756();
				Butterfly_3757();
				Butterfly_3758();
				Butterfly_3759();
				Butterfly_3760();
			WEIGHTED_ROUND_ROBIN_Joiner_4120();
		WEIGHTED_ROUND_ROBIN_Joiner_4121();
		BitReverse_3761();
		FloatPrinter_3762();
	ENDFOR
	return EXIT_SUCCESS;
}
