#include "PEG6-FFT5.h"

buffer_complex_t SplitJoin0_source_Fiss_7760_7779_join[2];
buffer_complex_t SplitJoin66_SplitJoin49_SplitJoin49_AnonFilter_a0_7425_7678_7768_7784_join[2];
buffer_complex_t SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7413_7639_7761_7780_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_7574butterfly_7477;
buffer_complex_t SplitJoin82_SplitJoin65_SplitJoin65_AnonFilter_a0_7447_7689_7772_7789_join[2];
buffer_complex_t SplitJoin80_SplitJoin63_SplitJoin63_AnonFilter_a0_7445_7688_7724_7788_split[2];
buffer_complex_t SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_7415_7640_7723_7781_join[2];
buffer_complex_t SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_7419_7642_7763_7783_split[2];
buffer_complex_t SplitJoin14_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_Hier_7765_7798_join[2];
buffer_complex_t SplitJoin84_SplitJoin67_SplitJoin67_AnonFilter_a0_7449_7690_7773_7790_join[2];
buffer_complex_t SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_7415_7640_7723_7781_split[2];
buffer_complex_t SplitJoin12_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child0_7726_7796_join[4];
buffer_complex_t SplitJoin94_SplitJoin77_SplitJoin77_AnonFilter_a0_7463_7697_7776_7793_split[2];
buffer_complex_t SplitJoin42_SplitJoin29_SplitJoin29_split2_7396_7661_7727_7801_join[2];
buffer_complex_t SplitJoin76_SplitJoin59_SplitJoin59_AnonFilter_a0_7439_7685_7771_7787_join[2];
buffer_complex_t SplitJoin22_SplitJoin16_SplitJoin16_split2_7407_7650_7731_7806_split[4];
buffer_complex_t SplitJoin35_SplitJoin22_SplitJoin22_split2_7409_7655_7733_7807_split[4];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_7592WEIGHTED_ROUND_ROBIN_Splitter_7735;
buffer_complex_t SplitJoin94_SplitJoin77_SplitJoin77_AnonFilter_a0_7463_7697_7776_7793_join[2];
buffer_complex_t SplitJoin18_SplitJoin12_SplitJoin12_split2_7394_7647_7725_7800_split[2];
buffer_complex_t SplitJoin20_SplitJoin14_SplitJoin14_split1_7405_7649_7766_7805_join[2];
buffer_complex_t SplitJoin46_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child1_7734_7802_split[2];
buffer_complex_t SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_7417_7641_7762_7782_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_7589butterfly_7482;
buffer_complex_t SplitJoin52_SplitJoin37_SplitJoin37_split2_7400_7667_7730_7804_split[2];
buffer_complex_t SplitJoin72_SplitJoin55_SplitJoin55_AnonFilter_a0_7433_7682_7770_7786_split[2];
buffer_complex_t SplitJoin59_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child1_7729_7797_split[4];
buffer_complex_t butterfly_7475Post_CollapsedDataParallel_2_7569;
buffer_complex_t SplitJoin59_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child1_7729_7797_join[4];
buffer_complex_t SplitJoin48_SplitJoin33_SplitJoin33_split2_7398_7664_7728_7803_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_7586butterfly_7481;
buffer_complex_t SplitJoin76_SplitJoin59_SplitJoin59_AnonFilter_a0_7439_7685_7771_7787_split[2];
buffer_complex_t SplitJoin92_SplitJoin75_SplitJoin75_AnonFilter_a0_7461_7696_7775_7792_split[2];
buffer_complex_t SplitJoin46_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child1_7734_7802_join[2];
buffer_complex_t SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_7419_7642_7763_7783_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_7746WEIGHTED_ROUND_ROBIN_Splitter_7633;
buffer_complex_t SplitJoin20_SplitJoin14_SplitJoin14_split1_7405_7649_7766_7805_split[2];
buffer_complex_t Pre_CollapsedDataParallel_1_7571butterfly_7476;
buffer_complex_t SplitJoin80_SplitJoin63_SplitJoin63_AnonFilter_a0_7445_7688_7724_7788_join[2];
buffer_complex_t SplitJoin35_SplitJoin22_SplitJoin22_split2_7409_7655_7733_7807_join[4];
buffer_complex_t SplitJoin84_SplitJoin67_SplitJoin67_AnonFilter_a0_7449_7690_7773_7790_split[2];
buffer_complex_t Pre_CollapsedDataParallel_1_7568butterfly_7475;
buffer_complex_t SplitJoin70_SplitJoin53_SplitJoin53_AnonFilter_a0_7431_7681_7769_7785_split[2];
buffer_complex_t butterfly_7476Post_CollapsedDataParallel_2_7572;
buffer_complex_t SplitJoin24_magnitude_Fiss_7767_7808_split[6];
buffer_complex_t butterfly_7477Post_CollapsedDataParallel_2_7575;
buffer_complex_t SplitJoin70_SplitJoin53_SplitJoin53_AnonFilter_a0_7431_7681_7769_7785_join[2];
buffer_complex_t SplitJoin10_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_Hier_7764_7795_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_7634WEIGHTED_ROUND_ROBIN_Splitter_7752;
buffer_complex_t SplitJoin14_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_Hier_7765_7798_split[2];
buffer_complex_t SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_7417_7641_7762_7782_split[2];
buffer_complex_t butterfly_7478Post_CollapsedDataParallel_2_7578;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_7753sink_7502;
buffer_complex_t SplitJoin18_SplitJoin12_SplitJoin12_split2_7394_7647_7725_7800_join[2];
buffer_complex_t butterfly_7481Post_CollapsedDataParallel_2_7587;
buffer_complex_t Pre_CollapsedDataParallel_1_7580butterfly_7479;
buffer_complex_t SplitJoin10_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_Hier_7764_7795_split[2];
buffer_complex_t butterfly_7480Post_CollapsedDataParallel_2_7584;
buffer_complex_t SplitJoin88_SplitJoin71_SplitJoin71_AnonFilter_a0_7455_7693_7774_7791_split[2];
buffer_complex_t SplitJoin98_SplitJoin81_SplitJoin81_AnonFilter_a0_7469_7700_7777_7794_split[2];
buffer_complex_t Pre_CollapsedDataParallel_1_7577butterfly_7478;
buffer_float_t SplitJoin24_magnitude_Fiss_7767_7808_join[6];
buffer_complex_t SplitJoin82_SplitJoin65_SplitJoin65_AnonFilter_a0_7447_7689_7772_7789_split[2];
buffer_complex_t SplitJoin72_SplitJoin55_SplitJoin55_AnonFilter_a0_7433_7682_7770_7786_join[2];
buffer_complex_t SplitJoin16_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child0_7732_7799_split[2];
buffer_complex_t SplitJoin48_SplitJoin33_SplitJoin33_split2_7398_7664_7728_7803_split[2];
buffer_complex_t SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7413_7639_7761_7780_split[2];
buffer_complex_t SplitJoin12_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child0_7726_7796_split[4];
buffer_complex_t SplitJoin92_SplitJoin75_SplitJoin75_AnonFilter_a0_7461_7696_7775_7792_join[2];
buffer_complex_t SplitJoin66_SplitJoin49_SplitJoin49_AnonFilter_a0_7425_7678_7768_7784_split[2];
buffer_complex_t SplitJoin42_SplitJoin29_SplitJoin29_split2_7396_7661_7727_7801_split[2];
buffer_complex_t SplitJoin98_SplitJoin81_SplitJoin81_AnonFilter_a0_7469_7700_7777_7794_join[2];
buffer_complex_t SplitJoin52_SplitJoin37_SplitJoin37_split2_7400_7667_7730_7804_join[2];
buffer_complex_t SplitJoin0_source_Fiss_7760_7779_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_7740WEIGHTED_ROUND_ROBIN_Splitter_7741;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_7749WEIGHTED_ROUND_ROBIN_Splitter_7591;
buffer_complex_t Pre_CollapsedDataParallel_1_7583butterfly_7480;
buffer_complex_t SplitJoin16_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child0_7732_7799_join[2];
buffer_complex_t butterfly_7479Post_CollapsedDataParallel_2_7581;
buffer_complex_t butterfly_7482Post_CollapsedDataParallel_2_7590;
buffer_complex_t SplitJoin22_SplitJoin16_SplitJoin16_split2_7407_7650_7731_7806_join[4];
buffer_complex_t SplitJoin88_SplitJoin71_SplitJoin71_AnonFilter_a0_7455_7693_7774_7791_join[2];



void source(buffer_void_t *chanin, buffer_complex_t *chanout) {
		complex_t t;
		t.imag = 0.0 ; 
		t.real = 0.9501 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.2311 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.6068 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.486 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.8913 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.7621 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.4565 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.0185 ; 
		push_complex(&(*chanout), t) ; 
	}


void source_7750() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		source(&(SplitJoin0_source_Fiss_7760_7779_split[0]), &(SplitJoin0_source_Fiss_7760_7779_join[0]));
	ENDFOR
}

void source_7751() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		source(&(SplitJoin0_source_Fiss_7760_7779_split[1]), &(SplitJoin0_source_Fiss_7760_7779_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7748() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_7749() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7749WEIGHTED_ROUND_ROBIN_Splitter_7591, pop_complex(&SplitJoin0_source_Fiss_7760_7779_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7749WEIGHTED_ROUND_ROBIN_Splitter_7591, pop_complex(&SplitJoin0_source_Fiss_7760_7779_join[1]));
		ENDFOR
	ENDFOR
}}

void Identity(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t __tmp2006 = pop_complex(&(*chanin));
		push_complex(&(*chanout), __tmp2006) ; 
	}


void Identity_7421() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_7419_7642_7763_7783_split[0]), &(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_7419_7642_7763_7783_join[0]));
	ENDFOR
}

void Identity_7423() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_7419_7642_7763_7783_split[1]), &(SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_7419_7642_7763_7783_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7597() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_7419_7642_7763_7783_split[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_7417_7641_7762_7782_split[0]));
		push_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_7419_7642_7763_7783_split[1], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_7417_7641_7762_7782_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7598() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_7417_7641_7762_7782_join[0], pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_7419_7642_7763_7783_join[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_7417_7641_7762_7782_join[0], pop_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_7419_7642_7763_7783_join[1]));
	ENDFOR
}}

void Identity_7427() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin66_SplitJoin49_SplitJoin49_AnonFilter_a0_7425_7678_7768_7784_split[0]), &(SplitJoin66_SplitJoin49_SplitJoin49_AnonFilter_a0_7425_7678_7768_7784_join[0]));
	ENDFOR
}

void Identity_7429() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin66_SplitJoin49_SplitJoin49_AnonFilter_a0_7425_7678_7768_7784_split[1]), &(SplitJoin66_SplitJoin49_SplitJoin49_AnonFilter_a0_7425_7678_7768_7784_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7599() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin66_SplitJoin49_SplitJoin49_AnonFilter_a0_7425_7678_7768_7784_split[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_7417_7641_7762_7782_split[1]));
		push_complex(&SplitJoin66_SplitJoin49_SplitJoin49_AnonFilter_a0_7425_7678_7768_7784_split[1], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_7417_7641_7762_7782_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7600() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_7417_7641_7762_7782_join[1], pop_complex(&SplitJoin66_SplitJoin49_SplitJoin49_AnonFilter_a0_7425_7678_7768_7784_join[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_7417_7641_7762_7782_join[1], pop_complex(&SplitJoin66_SplitJoin49_SplitJoin49_AnonFilter_a0_7425_7678_7768_7784_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_7595() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_7417_7641_7762_7782_split[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_7415_7640_7723_7781_split[0]));
		push_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_7417_7641_7762_7782_split[1], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_7415_7640_7723_7781_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7596() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_7415_7640_7723_7781_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_7417_7641_7762_7782_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_7415_7640_7723_7781_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_7417_7641_7762_7782_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_7415_7640_7723_7781_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_7417_7641_7762_7782_join[1]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_7415_7640_7723_7781_join[0], pop_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_7417_7641_7762_7782_join[1]));
	ENDFOR
}}

void Identity_7435() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin72_SplitJoin55_SplitJoin55_AnonFilter_a0_7433_7682_7770_7786_split[0]), &(SplitJoin72_SplitJoin55_SplitJoin55_AnonFilter_a0_7433_7682_7770_7786_join[0]));
	ENDFOR
}

void Identity_7437() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin72_SplitJoin55_SplitJoin55_AnonFilter_a0_7433_7682_7770_7786_split[1]), &(SplitJoin72_SplitJoin55_SplitJoin55_AnonFilter_a0_7433_7682_7770_7786_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7603() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin72_SplitJoin55_SplitJoin55_AnonFilter_a0_7433_7682_7770_7786_split[0], pop_complex(&SplitJoin70_SplitJoin53_SplitJoin53_AnonFilter_a0_7431_7681_7769_7785_split[0]));
		push_complex(&SplitJoin72_SplitJoin55_SplitJoin55_AnonFilter_a0_7433_7682_7770_7786_split[1], pop_complex(&SplitJoin70_SplitJoin53_SplitJoin53_AnonFilter_a0_7431_7681_7769_7785_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7604() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin70_SplitJoin53_SplitJoin53_AnonFilter_a0_7431_7681_7769_7785_join[0], pop_complex(&SplitJoin72_SplitJoin55_SplitJoin55_AnonFilter_a0_7433_7682_7770_7786_join[0]));
		push_complex(&SplitJoin70_SplitJoin53_SplitJoin53_AnonFilter_a0_7431_7681_7769_7785_join[0], pop_complex(&SplitJoin72_SplitJoin55_SplitJoin55_AnonFilter_a0_7433_7682_7770_7786_join[1]));
	ENDFOR
}}

void Identity_7441() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin76_SplitJoin59_SplitJoin59_AnonFilter_a0_7439_7685_7771_7787_split[0]), &(SplitJoin76_SplitJoin59_SplitJoin59_AnonFilter_a0_7439_7685_7771_7787_join[0]));
	ENDFOR
}

void Identity_7443() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin76_SplitJoin59_SplitJoin59_AnonFilter_a0_7439_7685_7771_7787_split[1]), &(SplitJoin76_SplitJoin59_SplitJoin59_AnonFilter_a0_7439_7685_7771_7787_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7605() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin76_SplitJoin59_SplitJoin59_AnonFilter_a0_7439_7685_7771_7787_split[0], pop_complex(&SplitJoin70_SplitJoin53_SplitJoin53_AnonFilter_a0_7431_7681_7769_7785_split[1]));
		push_complex(&SplitJoin76_SplitJoin59_SplitJoin59_AnonFilter_a0_7439_7685_7771_7787_split[1], pop_complex(&SplitJoin70_SplitJoin53_SplitJoin53_AnonFilter_a0_7431_7681_7769_7785_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7606() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin70_SplitJoin53_SplitJoin53_AnonFilter_a0_7431_7681_7769_7785_join[1], pop_complex(&SplitJoin76_SplitJoin59_SplitJoin59_AnonFilter_a0_7439_7685_7771_7787_join[0]));
		push_complex(&SplitJoin70_SplitJoin53_SplitJoin53_AnonFilter_a0_7431_7681_7769_7785_join[1], pop_complex(&SplitJoin76_SplitJoin59_SplitJoin59_AnonFilter_a0_7439_7685_7771_7787_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_7601() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin70_SplitJoin53_SplitJoin53_AnonFilter_a0_7431_7681_7769_7785_split[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_7415_7640_7723_7781_split[1]));
		push_complex(&SplitJoin70_SplitJoin53_SplitJoin53_AnonFilter_a0_7431_7681_7769_7785_split[1], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_7415_7640_7723_7781_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7602() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_7415_7640_7723_7781_join[1], pop_complex(&SplitJoin70_SplitJoin53_SplitJoin53_AnonFilter_a0_7431_7681_7769_7785_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_7415_7640_7723_7781_join[1], pop_complex(&SplitJoin70_SplitJoin53_SplitJoin53_AnonFilter_a0_7431_7681_7769_7785_join[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_7415_7640_7723_7781_join[1], pop_complex(&SplitJoin70_SplitJoin53_SplitJoin53_AnonFilter_a0_7431_7681_7769_7785_join[1]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_7415_7640_7723_7781_join[1], pop_complex(&SplitJoin70_SplitJoin53_SplitJoin53_AnonFilter_a0_7431_7681_7769_7785_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_7593() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 12, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_7415_7640_7723_7781_split[0], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7413_7639_7761_7780_split[0]));
		push_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_7415_7640_7723_7781_split[1], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7413_7639_7761_7780_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7594() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7413_7639_7761_7780_join[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_7415_7640_7723_7781_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7413_7639_7761_7780_join[0], pop_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_7415_7640_7723_7781_join[1]));
		ENDFOR
	ENDFOR
}}

void Identity_7451() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin84_SplitJoin67_SplitJoin67_AnonFilter_a0_7449_7690_7773_7790_split[0]), &(SplitJoin84_SplitJoin67_SplitJoin67_AnonFilter_a0_7449_7690_7773_7790_join[0]));
	ENDFOR
}

void Identity_7453() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin84_SplitJoin67_SplitJoin67_AnonFilter_a0_7449_7690_7773_7790_split[1]), &(SplitJoin84_SplitJoin67_SplitJoin67_AnonFilter_a0_7449_7690_7773_7790_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7611() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin84_SplitJoin67_SplitJoin67_AnonFilter_a0_7449_7690_7773_7790_split[0], pop_complex(&SplitJoin82_SplitJoin65_SplitJoin65_AnonFilter_a0_7447_7689_7772_7789_split[0]));
		push_complex(&SplitJoin84_SplitJoin67_SplitJoin67_AnonFilter_a0_7449_7690_7773_7790_split[1], pop_complex(&SplitJoin82_SplitJoin65_SplitJoin65_AnonFilter_a0_7447_7689_7772_7789_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7612() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin82_SplitJoin65_SplitJoin65_AnonFilter_a0_7447_7689_7772_7789_join[0], pop_complex(&SplitJoin84_SplitJoin67_SplitJoin67_AnonFilter_a0_7449_7690_7773_7790_join[0]));
		push_complex(&SplitJoin82_SplitJoin65_SplitJoin65_AnonFilter_a0_7447_7689_7772_7789_join[0], pop_complex(&SplitJoin84_SplitJoin67_SplitJoin67_AnonFilter_a0_7449_7690_7773_7790_join[1]));
	ENDFOR
}}

void Identity_7457() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin88_SplitJoin71_SplitJoin71_AnonFilter_a0_7455_7693_7774_7791_split[0]), &(SplitJoin88_SplitJoin71_SplitJoin71_AnonFilter_a0_7455_7693_7774_7791_join[0]));
	ENDFOR
}

void Identity_7459() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin88_SplitJoin71_SplitJoin71_AnonFilter_a0_7455_7693_7774_7791_split[1]), &(SplitJoin88_SplitJoin71_SplitJoin71_AnonFilter_a0_7455_7693_7774_7791_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7613() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin88_SplitJoin71_SplitJoin71_AnonFilter_a0_7455_7693_7774_7791_split[0], pop_complex(&SplitJoin82_SplitJoin65_SplitJoin65_AnonFilter_a0_7447_7689_7772_7789_split[1]));
		push_complex(&SplitJoin88_SplitJoin71_SplitJoin71_AnonFilter_a0_7455_7693_7774_7791_split[1], pop_complex(&SplitJoin82_SplitJoin65_SplitJoin65_AnonFilter_a0_7447_7689_7772_7789_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7614() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin82_SplitJoin65_SplitJoin65_AnonFilter_a0_7447_7689_7772_7789_join[1], pop_complex(&SplitJoin88_SplitJoin71_SplitJoin71_AnonFilter_a0_7455_7693_7774_7791_join[0]));
		push_complex(&SplitJoin82_SplitJoin65_SplitJoin65_AnonFilter_a0_7447_7689_7772_7789_join[1], pop_complex(&SplitJoin88_SplitJoin71_SplitJoin71_AnonFilter_a0_7455_7693_7774_7791_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_7609() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin82_SplitJoin65_SplitJoin65_AnonFilter_a0_7447_7689_7772_7789_split[0], pop_complex(&SplitJoin80_SplitJoin63_SplitJoin63_AnonFilter_a0_7445_7688_7724_7788_split[0]));
		push_complex(&SplitJoin82_SplitJoin65_SplitJoin65_AnonFilter_a0_7447_7689_7772_7789_split[1], pop_complex(&SplitJoin80_SplitJoin63_SplitJoin63_AnonFilter_a0_7445_7688_7724_7788_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7610() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin80_SplitJoin63_SplitJoin63_AnonFilter_a0_7445_7688_7724_7788_join[0], pop_complex(&SplitJoin82_SplitJoin65_SplitJoin65_AnonFilter_a0_7447_7689_7772_7789_join[0]));
		push_complex(&SplitJoin80_SplitJoin63_SplitJoin63_AnonFilter_a0_7445_7688_7724_7788_join[0], pop_complex(&SplitJoin82_SplitJoin65_SplitJoin65_AnonFilter_a0_7447_7689_7772_7789_join[0]));
		push_complex(&SplitJoin80_SplitJoin63_SplitJoin63_AnonFilter_a0_7445_7688_7724_7788_join[0], pop_complex(&SplitJoin82_SplitJoin65_SplitJoin65_AnonFilter_a0_7447_7689_7772_7789_join[1]));
		push_complex(&SplitJoin80_SplitJoin63_SplitJoin63_AnonFilter_a0_7445_7688_7724_7788_join[0], pop_complex(&SplitJoin82_SplitJoin65_SplitJoin65_AnonFilter_a0_7447_7689_7772_7789_join[1]));
	ENDFOR
}}

void Identity_7465() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin94_SplitJoin77_SplitJoin77_AnonFilter_a0_7463_7697_7776_7793_split[0]), &(SplitJoin94_SplitJoin77_SplitJoin77_AnonFilter_a0_7463_7697_7776_7793_join[0]));
	ENDFOR
}

void Identity_7467() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin94_SplitJoin77_SplitJoin77_AnonFilter_a0_7463_7697_7776_7793_split[1]), &(SplitJoin94_SplitJoin77_SplitJoin77_AnonFilter_a0_7463_7697_7776_7793_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7617() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin94_SplitJoin77_SplitJoin77_AnonFilter_a0_7463_7697_7776_7793_split[0], pop_complex(&SplitJoin92_SplitJoin75_SplitJoin75_AnonFilter_a0_7461_7696_7775_7792_split[0]));
		push_complex(&SplitJoin94_SplitJoin77_SplitJoin77_AnonFilter_a0_7463_7697_7776_7793_split[1], pop_complex(&SplitJoin92_SplitJoin75_SplitJoin75_AnonFilter_a0_7461_7696_7775_7792_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7618() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin92_SplitJoin75_SplitJoin75_AnonFilter_a0_7461_7696_7775_7792_join[0], pop_complex(&SplitJoin94_SplitJoin77_SplitJoin77_AnonFilter_a0_7463_7697_7776_7793_join[0]));
		push_complex(&SplitJoin92_SplitJoin75_SplitJoin75_AnonFilter_a0_7461_7696_7775_7792_join[0], pop_complex(&SplitJoin94_SplitJoin77_SplitJoin77_AnonFilter_a0_7463_7697_7776_7793_join[1]));
	ENDFOR
}}

void Identity_7471() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin98_SplitJoin81_SplitJoin81_AnonFilter_a0_7469_7700_7777_7794_split[0]), &(SplitJoin98_SplitJoin81_SplitJoin81_AnonFilter_a0_7469_7700_7777_7794_join[0]));
	ENDFOR
}

void Identity_7473() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Identity(&(SplitJoin98_SplitJoin81_SplitJoin81_AnonFilter_a0_7469_7700_7777_7794_split[1]), &(SplitJoin98_SplitJoin81_SplitJoin81_AnonFilter_a0_7469_7700_7777_7794_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7619() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin98_SplitJoin81_SplitJoin81_AnonFilter_a0_7469_7700_7777_7794_split[0], pop_complex(&SplitJoin92_SplitJoin75_SplitJoin75_AnonFilter_a0_7461_7696_7775_7792_split[1]));
		push_complex(&SplitJoin98_SplitJoin81_SplitJoin81_AnonFilter_a0_7469_7700_7777_7794_split[1], pop_complex(&SplitJoin92_SplitJoin75_SplitJoin75_AnonFilter_a0_7461_7696_7775_7792_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7620() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin92_SplitJoin75_SplitJoin75_AnonFilter_a0_7461_7696_7775_7792_join[1], pop_complex(&SplitJoin98_SplitJoin81_SplitJoin81_AnonFilter_a0_7469_7700_7777_7794_join[0]));
		push_complex(&SplitJoin92_SplitJoin75_SplitJoin75_AnonFilter_a0_7461_7696_7775_7792_join[1], pop_complex(&SplitJoin98_SplitJoin81_SplitJoin81_AnonFilter_a0_7469_7700_7777_7794_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_7615() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin92_SplitJoin75_SplitJoin75_AnonFilter_a0_7461_7696_7775_7792_split[0], pop_complex(&SplitJoin80_SplitJoin63_SplitJoin63_AnonFilter_a0_7445_7688_7724_7788_split[1]));
		push_complex(&SplitJoin92_SplitJoin75_SplitJoin75_AnonFilter_a0_7461_7696_7775_7792_split[1], pop_complex(&SplitJoin80_SplitJoin63_SplitJoin63_AnonFilter_a0_7445_7688_7724_7788_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7616() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		push_complex(&SplitJoin80_SplitJoin63_SplitJoin63_AnonFilter_a0_7445_7688_7724_7788_join[1], pop_complex(&SplitJoin92_SplitJoin75_SplitJoin75_AnonFilter_a0_7461_7696_7775_7792_join[0]));
		push_complex(&SplitJoin80_SplitJoin63_SplitJoin63_AnonFilter_a0_7445_7688_7724_7788_join[1], pop_complex(&SplitJoin92_SplitJoin75_SplitJoin75_AnonFilter_a0_7461_7696_7775_7792_join[0]));
		push_complex(&SplitJoin80_SplitJoin63_SplitJoin63_AnonFilter_a0_7445_7688_7724_7788_join[1], pop_complex(&SplitJoin92_SplitJoin75_SplitJoin75_AnonFilter_a0_7461_7696_7775_7792_join[1]));
		push_complex(&SplitJoin80_SplitJoin63_SplitJoin63_AnonFilter_a0_7445_7688_7724_7788_join[1], pop_complex(&SplitJoin92_SplitJoin75_SplitJoin75_AnonFilter_a0_7461_7696_7775_7792_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_7607() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 12, __iter_steady_++)
		push_complex(&SplitJoin80_SplitJoin63_SplitJoin63_AnonFilter_a0_7445_7688_7724_7788_split[0], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7413_7639_7761_7780_split[1]));
		push_complex(&SplitJoin80_SplitJoin63_SplitJoin63_AnonFilter_a0_7445_7688_7724_7788_split[1], pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7413_7639_7761_7780_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7608() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7413_7639_7761_7780_join[1], pop_complex(&SplitJoin80_SplitJoin63_SplitJoin63_AnonFilter_a0_7445_7688_7724_7788_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7413_7639_7761_7780_join[1], pop_complex(&SplitJoin80_SplitJoin63_SplitJoin63_AnonFilter_a0_7445_7688_7724_7788_join[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_7591() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 24, __iter_steady_++)
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7413_7639_7761_7780_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7749WEIGHTED_ROUND_ROBIN_Splitter_7591));
		push_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7413_7639_7761_7780_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7749WEIGHTED_ROUND_ROBIN_Splitter_7591));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7592() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7592WEIGHTED_ROUND_ROBIN_Splitter_7735, pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7413_7639_7761_7780_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7592WEIGHTED_ROUND_ROBIN_Splitter_7735, pop_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7413_7639_7761_7780_join[1]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_complex_t *chanin, buffer_complex_t *chanout) {
 {
 {
		int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
		FOR(int, _i, 0,  < , 2, _i++) {
			push_complex(&(*chanout), peek_complex(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
			iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
		}
		ENDFOR
	}
	}
	}
		pop_complex(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_7568() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child0_7726_7796_split[0]), &(Pre_CollapsedDataParallel_1_7568butterfly_7475));
	ENDFOR
}

void butterfly(buffer_complex_t *chanin, buffer_complex_t *chanout) {
		complex_t WN1;
		complex_t WN2;
		complex_t one = ((complex_t) pop_complex(&(*chanin)));
		complex_t two = ((complex_t) pop_complex(&(*chanin)));
		complex_t __sa1;
		complex_t __sa2;
		WN1.real = 1.0 ; 
		WN1.imag = -0.0 ; 
		WN2.real = -1.0 ; 
		WN2.imag = 8.742278E-8 ; 
		__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
		__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
		push_complex(&(*chanout), __sa1) ; 
		__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
		__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
		push_complex(&(*chanout), __sa2) ; 
	}


void butterfly_7475() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_7568butterfly_7475), &(butterfly_7475Post_CollapsedDataParallel_2_7569));
	ENDFOR
}

void Post_CollapsedDataParallel_2(buffer_complex_t *chanin, buffer_complex_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 2, _k++) {
 {
			push_complex(&(*chanout), peek_complex(&(*chanin), (_k + 0))) ; 
		}
		}
		ENDFOR
	}
	}
		pop_complex(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_7569() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_7475Post_CollapsedDataParallel_2_7569), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child0_7726_7796_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_7571() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child0_7726_7796_split[1]), &(Pre_CollapsedDataParallel_1_7571butterfly_7476));
	ENDFOR
}

void butterfly_7476() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_7571butterfly_7476), &(butterfly_7476Post_CollapsedDataParallel_2_7572));
	ENDFOR
}

void Post_CollapsedDataParallel_2_7572() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_7476Post_CollapsedDataParallel_2_7572), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child0_7726_7796_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_7574() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child0_7726_7796_split[2]), &(Pre_CollapsedDataParallel_1_7574butterfly_7477));
	ENDFOR
}

void butterfly_7477() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_7574butterfly_7477), &(butterfly_7477Post_CollapsedDataParallel_2_7575));
	ENDFOR
}

void Post_CollapsedDataParallel_2_7575() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_7477Post_CollapsedDataParallel_2_7575), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child0_7726_7796_join[2]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_7577() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child0_7726_7796_split[3]), &(Pre_CollapsedDataParallel_1_7577butterfly_7478));
	ENDFOR
}

void butterfly_7478() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_7577butterfly_7478), &(butterfly_7478Post_CollapsedDataParallel_2_7578));
	ENDFOR
}

void Post_CollapsedDataParallel_2_7578() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_7478Post_CollapsedDataParallel_2_7578), &(SplitJoin12_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child0_7726_7796_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7736() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child0_7726_7796_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_Hier_7764_7795_split[0]));
			push_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child0_7726_7796_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_Hier_7764_7795_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7737() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_Hier_7764_7795_join[0], pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child0_7726_7796_join[__iter_]));
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_Hier_7764_7795_join[0], pop_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child0_7726_7796_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_7580() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin59_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child1_7729_7797_split[0]), &(Pre_CollapsedDataParallel_1_7580butterfly_7479));
	ENDFOR
}

void butterfly_7479() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_7580butterfly_7479), &(butterfly_7479Post_CollapsedDataParallel_2_7581));
	ENDFOR
}

void Post_CollapsedDataParallel_2_7581() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_7479Post_CollapsedDataParallel_2_7581), &(SplitJoin59_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child1_7729_7797_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_7583() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin59_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child1_7729_7797_split[1]), &(Pre_CollapsedDataParallel_1_7583butterfly_7480));
	ENDFOR
}

void butterfly_7480() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_7583butterfly_7480), &(butterfly_7480Post_CollapsedDataParallel_2_7584));
	ENDFOR
}

void Post_CollapsedDataParallel_2_7584() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_7480Post_CollapsedDataParallel_2_7584), &(SplitJoin59_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child1_7729_7797_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_7586() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin59_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child1_7729_7797_split[2]), &(Pre_CollapsedDataParallel_1_7586butterfly_7481));
	ENDFOR
}

void butterfly_7481() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_7586butterfly_7481), &(butterfly_7481Post_CollapsedDataParallel_2_7587));
	ENDFOR
}

void Post_CollapsedDataParallel_2_7587() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_7481Post_CollapsedDataParallel_2_7587), &(SplitJoin59_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child1_7729_7797_join[2]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_7589() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin59_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child1_7729_7797_split[3]), &(Pre_CollapsedDataParallel_1_7589butterfly_7482));
	ENDFOR
}

void butterfly_7482() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(Pre_CollapsedDataParallel_1_7589butterfly_7482), &(butterfly_7482Post_CollapsedDataParallel_2_7590));
	ENDFOR
}

void Post_CollapsedDataParallel_2_7590() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(butterfly_7482Post_CollapsedDataParallel_2_7590), &(SplitJoin59_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child1_7729_7797_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7738() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin59_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child1_7729_7797_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_Hier_7764_7795_split[1]));
			push_complex(&SplitJoin59_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child1_7729_7797_split[__iter_], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_Hier_7764_7795_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7739() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_Hier_7764_7795_join[1], pop_complex(&SplitJoin59_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child1_7729_7797_join[__iter_]));
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_Hier_7764_7795_join[1], pop_complex(&SplitJoin59_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child1_7729_7797_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_7735() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_Hier_7764_7795_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7592WEIGHTED_ROUND_ROBIN_Splitter_7735));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_Hier_7764_7795_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7592WEIGHTED_ROUND_ROBIN_Splitter_7735));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7740() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7740WEIGHTED_ROUND_ROBIN_Splitter_7741, pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_Hier_7764_7795_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7740WEIGHTED_ROUND_ROBIN_Splitter_7741, pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_Hier_7764_7795_join[1]));
		ENDFOR
	ENDFOR
}}

void butterfly_7484() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin18_SplitJoin12_SplitJoin12_split2_7394_7647_7725_7800_split[0]), &(SplitJoin18_SplitJoin12_SplitJoin12_split2_7394_7647_7725_7800_join[0]));
	ENDFOR
}

void butterfly_7485() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin18_SplitJoin12_SplitJoin12_split2_7394_7647_7725_7800_split[1]), &(SplitJoin18_SplitJoin12_SplitJoin12_split2_7394_7647_7725_7800_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7625() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_7394_7647_7725_7800_split[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child0_7732_7799_split[0]));
		push_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_7394_7647_7725_7800_split[1], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child0_7732_7799_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7626() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child0_7732_7799_join[0], pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_7394_7647_7725_7800_join[0]));
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child0_7732_7799_join[0], pop_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_7394_7647_7725_7800_join[1]));
	ENDFOR
}}

void butterfly_7486() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin42_SplitJoin29_SplitJoin29_split2_7396_7661_7727_7801_split[0]), &(SplitJoin42_SplitJoin29_SplitJoin29_split2_7396_7661_7727_7801_join[0]));
	ENDFOR
}

void butterfly_7487() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin42_SplitJoin29_SplitJoin29_split2_7396_7661_7727_7801_split[1]), &(SplitJoin42_SplitJoin29_SplitJoin29_split2_7396_7661_7727_7801_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7627() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin42_SplitJoin29_SplitJoin29_split2_7396_7661_7727_7801_split[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child0_7732_7799_split[1]));
		push_complex(&SplitJoin42_SplitJoin29_SplitJoin29_split2_7396_7661_7727_7801_split[1], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child0_7732_7799_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7628() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child0_7732_7799_join[1], pop_complex(&SplitJoin42_SplitJoin29_SplitJoin29_split2_7396_7661_7727_7801_join[0]));
		push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child0_7732_7799_join[1], pop_complex(&SplitJoin42_SplitJoin29_SplitJoin29_split2_7396_7661_7727_7801_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_7742() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child0_7732_7799_split[0], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_Hier_7765_7798_split[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child0_7732_7799_split[1], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_Hier_7765_7798_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7743() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_Hier_7765_7798_join[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child0_7732_7799_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_Hier_7765_7798_join[0], pop_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child0_7732_7799_join[1]));
		ENDFOR
	ENDFOR
}}

void butterfly_7488() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin48_SplitJoin33_SplitJoin33_split2_7398_7664_7728_7803_split[0]), &(SplitJoin48_SplitJoin33_SplitJoin33_split2_7398_7664_7728_7803_join[0]));
	ENDFOR
}

void butterfly_7489() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin48_SplitJoin33_SplitJoin33_split2_7398_7664_7728_7803_split[1]), &(SplitJoin48_SplitJoin33_SplitJoin33_split2_7398_7664_7728_7803_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7629() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin48_SplitJoin33_SplitJoin33_split2_7398_7664_7728_7803_split[0], pop_complex(&SplitJoin46_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child1_7734_7802_split[0]));
		push_complex(&SplitJoin48_SplitJoin33_SplitJoin33_split2_7398_7664_7728_7803_split[1], pop_complex(&SplitJoin46_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child1_7734_7802_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7630() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin46_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child1_7734_7802_join[0], pop_complex(&SplitJoin48_SplitJoin33_SplitJoin33_split2_7398_7664_7728_7803_join[0]));
		push_complex(&SplitJoin46_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child1_7734_7802_join[0], pop_complex(&SplitJoin48_SplitJoin33_SplitJoin33_split2_7398_7664_7728_7803_join[1]));
	ENDFOR
}}

void butterfly_7490() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin52_SplitJoin37_SplitJoin37_split2_7400_7667_7730_7804_split[0]), &(SplitJoin52_SplitJoin37_SplitJoin37_split2_7400_7667_7730_7804_join[0]));
	ENDFOR
}

void butterfly_7491() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin52_SplitJoin37_SplitJoin37_split2_7400_7667_7730_7804_split[1]), &(SplitJoin52_SplitJoin37_SplitJoin37_split2_7400_7667_7730_7804_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7631() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin52_SplitJoin37_SplitJoin37_split2_7400_7667_7730_7804_split[0], pop_complex(&SplitJoin46_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child1_7734_7802_split[1]));
		push_complex(&SplitJoin52_SplitJoin37_SplitJoin37_split2_7400_7667_7730_7804_split[1], pop_complex(&SplitJoin46_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child1_7734_7802_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7632() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		push_complex(&SplitJoin46_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child1_7734_7802_join[1], pop_complex(&SplitJoin52_SplitJoin37_SplitJoin37_split2_7400_7667_7730_7804_join[0]));
		push_complex(&SplitJoin46_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child1_7734_7802_join[1], pop_complex(&SplitJoin52_SplitJoin37_SplitJoin37_split2_7400_7667_7730_7804_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_7744() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin46_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child1_7734_7802_split[0], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_Hier_7765_7798_split[1]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin46_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child1_7734_7802_split[1], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_Hier_7765_7798_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7745() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_Hier_7765_7798_join[1], pop_complex(&SplitJoin46_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child1_7734_7802_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_Hier_7765_7798_join[1], pop_complex(&SplitJoin46_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child1_7734_7802_join[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_7741() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_Hier_7765_7798_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7740WEIGHTED_ROUND_ROBIN_Splitter_7741));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_Hier_7765_7798_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7740WEIGHTED_ROUND_ROBIN_Splitter_7741));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7746() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7746WEIGHTED_ROUND_ROBIN_Splitter_7633, pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_Hier_7765_7798_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7746WEIGHTED_ROUND_ROBIN_Splitter_7633, pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_Hier_7765_7798_join[1]));
		ENDFOR
	ENDFOR
}}

void butterfly_7493() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_7407_7650_7731_7806_split[0]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_7407_7650_7731_7806_join[0]));
	ENDFOR
}

void butterfly_7494() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_7407_7650_7731_7806_split[1]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_7407_7650_7731_7806_join[1]));
	ENDFOR
}

void butterfly_7495() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_7407_7650_7731_7806_split[2]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_7407_7650_7731_7806_join[2]));
	ENDFOR
}

void butterfly_7496() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin22_SplitJoin16_SplitJoin16_split2_7407_7650_7731_7806_split[3]), &(SplitJoin22_SplitJoin16_SplitJoin16_split2_7407_7650_7731_7806_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7635() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_7407_7650_7731_7806_split[__iter_], pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_7405_7649_7766_7805_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7636() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_7405_7649_7766_7805_join[0], pop_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_7407_7650_7731_7806_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void butterfly_7497() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin35_SplitJoin22_SplitJoin22_split2_7409_7655_7733_7807_split[0]), &(SplitJoin35_SplitJoin22_SplitJoin22_split2_7409_7655_7733_7807_join[0]));
	ENDFOR
}

void butterfly_7498() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin35_SplitJoin22_SplitJoin22_split2_7409_7655_7733_7807_split[1]), &(SplitJoin35_SplitJoin22_SplitJoin22_split2_7409_7655_7733_7807_join[1]));
	ENDFOR
}

void butterfly_7499() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin35_SplitJoin22_SplitJoin22_split2_7409_7655_7733_7807_split[2]), &(SplitJoin35_SplitJoin22_SplitJoin22_split2_7409_7655_7733_7807_join[2]));
	ENDFOR
}

void butterfly_7500() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		butterfly(&(SplitJoin35_SplitJoin22_SplitJoin22_split2_7409_7655_7733_7807_split[3]), &(SplitJoin35_SplitJoin22_SplitJoin22_split2_7409_7655_7733_7807_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7637() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin35_SplitJoin22_SplitJoin22_split2_7409_7655_7733_7807_split[__iter_], pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_7405_7649_7766_7805_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7638() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 6, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_7405_7649_7766_7805_join[1], pop_complex(&SplitJoin35_SplitJoin22_SplitJoin22_split2_7409_7655_7733_7807_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_7633() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_7405_7649_7766_7805_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7746WEIGHTED_ROUND_ROBIN_Splitter_7633));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_7405_7649_7766_7805_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7746WEIGHTED_ROUND_ROBIN_Splitter_7633));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7634() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7634WEIGHTED_ROUND_ROBIN_Splitter_7752, pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_7405_7649_7766_7805_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7634WEIGHTED_ROUND_ROBIN_Splitter_7752, pop_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_7405_7649_7766_7805_join[1]));
		ENDFOR
	ENDFOR
}}

void magnitude(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		push_float(&(*chanout), ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}


void magnitude_7754() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_7767_7808_split[0]), &(SplitJoin24_magnitude_Fiss_7767_7808_join[0]));
	ENDFOR
}

void magnitude_7755() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_7767_7808_split[1]), &(SplitJoin24_magnitude_Fiss_7767_7808_join[1]));
	ENDFOR
}

void magnitude_7756() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_7767_7808_split[2]), &(SplitJoin24_magnitude_Fiss_7767_7808_join[2]));
	ENDFOR
}

void magnitude_7757() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_7767_7808_split[3]), &(SplitJoin24_magnitude_Fiss_7767_7808_join[3]));
	ENDFOR
}

void magnitude_7758() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_7767_7808_split[4]), &(SplitJoin24_magnitude_Fiss_7767_7808_join[4]));
	ENDFOR
}

void magnitude_7759() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		magnitude(&(SplitJoin24_magnitude_Fiss_7767_7808_split[5]), &(SplitJoin24_magnitude_Fiss_7767_7808_join[5]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_7752() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 6, __iter_++)
			push_complex(&SplitJoin24_magnitude_Fiss_7767_7808_split[__iter_], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7634WEIGHTED_ROUND_ROBIN_Splitter_7752));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_7753() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 6, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_7753sink_7502, pop_float(&SplitJoin24_magnitude_Fiss_7767_7808_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void sink(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void sink_7502() {
	FOR(uint32_t, __iter_steady_, 0, <, 48, __iter_steady_++)
		sink(&(WEIGHTED_ROUND_ROBIN_Joiner_7753sink_7502));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_complex(&SplitJoin0_source_Fiss_7760_7779_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 2, __iter_init_1_++)
		init_buffer_complex(&SplitJoin66_SplitJoin49_SplitJoin49_AnonFilter_a0_7425_7678_7768_7784_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7413_7639_7761_7780_join[__iter_init_2_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_7574butterfly_7477);
	FOR(int, __iter_init_3_, 0, <, 2, __iter_init_3_++)
		init_buffer_complex(&SplitJoin82_SplitJoin65_SplitJoin65_AnonFilter_a0_7447_7689_7772_7789_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 2, __iter_init_4_++)
		init_buffer_complex(&SplitJoin80_SplitJoin63_SplitJoin63_AnonFilter_a0_7445_7688_7724_7788_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 2, __iter_init_5_++)
		init_buffer_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_7415_7640_7723_7781_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 2, __iter_init_6_++)
		init_buffer_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_7419_7642_7763_7783_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 2, __iter_init_7_++)
		init_buffer_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_Hier_7765_7798_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_complex(&SplitJoin84_SplitJoin67_SplitJoin67_AnonFilter_a0_7449_7690_7773_7790_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 2, __iter_init_9_++)
		init_buffer_complex(&SplitJoin4_SplitJoin2_SplitJoin2_AnonFilter_a0_7415_7640_7723_7781_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 4, __iter_init_10_++)
		init_buffer_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child0_7726_7796_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 2, __iter_init_11_++)
		init_buffer_complex(&SplitJoin94_SplitJoin77_SplitJoin77_AnonFilter_a0_7463_7697_7776_7793_split[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 2, __iter_init_12_++)
		init_buffer_complex(&SplitJoin42_SplitJoin29_SplitJoin29_split2_7396_7661_7727_7801_join[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_complex(&SplitJoin76_SplitJoin59_SplitJoin59_AnonFilter_a0_7439_7685_7771_7787_join[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 4, __iter_init_14_++)
		init_buffer_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_7407_7650_7731_7806_split[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 4, __iter_init_15_++)
		init_buffer_complex(&SplitJoin35_SplitJoin22_SplitJoin22_split2_7409_7655_7733_7807_split[__iter_init_15_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7592WEIGHTED_ROUND_ROBIN_Splitter_7735);
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_complex(&SplitJoin94_SplitJoin77_SplitJoin77_AnonFilter_a0_7463_7697_7776_7793_join[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_7394_7647_7725_7800_split[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 2, __iter_init_18_++)
		init_buffer_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_7405_7649_7766_7805_join[__iter_init_18_]);
	ENDFOR
	FOR(int, __iter_init_19_, 0, <, 2, __iter_init_19_++)
		init_buffer_complex(&SplitJoin46_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child1_7734_7802_split[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 2, __iter_init_20_++)
		init_buffer_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_7417_7641_7762_7782_join[__iter_init_20_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_7589butterfly_7482);
	FOR(int, __iter_init_21_, 0, <, 2, __iter_init_21_++)
		init_buffer_complex(&SplitJoin52_SplitJoin37_SplitJoin37_split2_7400_7667_7730_7804_split[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 2, __iter_init_22_++)
		init_buffer_complex(&SplitJoin72_SplitJoin55_SplitJoin55_AnonFilter_a0_7433_7682_7770_7786_split[__iter_init_22_]);
	ENDFOR
	FOR(int, __iter_init_23_, 0, <, 4, __iter_init_23_++)
		init_buffer_complex(&SplitJoin59_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child1_7729_7797_split[__iter_init_23_]);
	ENDFOR
	init_buffer_complex(&butterfly_7475Post_CollapsedDataParallel_2_7569);
	FOR(int, __iter_init_24_, 0, <, 4, __iter_init_24_++)
		init_buffer_complex(&SplitJoin59_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child1_7729_7797_join[__iter_init_24_]);
	ENDFOR
	FOR(int, __iter_init_25_, 0, <, 2, __iter_init_25_++)
		init_buffer_complex(&SplitJoin48_SplitJoin33_SplitJoin33_split2_7398_7664_7728_7803_join[__iter_init_25_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_7586butterfly_7481);
	FOR(int, __iter_init_26_, 0, <, 2, __iter_init_26_++)
		init_buffer_complex(&SplitJoin76_SplitJoin59_SplitJoin59_AnonFilter_a0_7439_7685_7771_7787_split[__iter_init_26_]);
	ENDFOR
	FOR(int, __iter_init_27_, 0, <, 2, __iter_init_27_++)
		init_buffer_complex(&SplitJoin92_SplitJoin75_SplitJoin75_AnonFilter_a0_7461_7696_7775_7792_split[__iter_init_27_]);
	ENDFOR
	FOR(int, __iter_init_28_, 0, <, 2, __iter_init_28_++)
		init_buffer_complex(&SplitJoin46_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child1_7734_7802_join[__iter_init_28_]);
	ENDFOR
	FOR(int, __iter_init_29_, 0, <, 2, __iter_init_29_++)
		init_buffer_complex(&SplitJoin8_SplitJoin6_SplitJoin6_AnonFilter_a0_7419_7642_7763_7783_join[__iter_init_29_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7746WEIGHTED_ROUND_ROBIN_Splitter_7633);
	FOR(int, __iter_init_30_, 0, <, 2, __iter_init_30_++)
		init_buffer_complex(&SplitJoin20_SplitJoin14_SplitJoin14_split1_7405_7649_7766_7805_split[__iter_init_30_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_7571butterfly_7476);
	FOR(int, __iter_init_31_, 0, <, 2, __iter_init_31_++)
		init_buffer_complex(&SplitJoin80_SplitJoin63_SplitJoin63_AnonFilter_a0_7445_7688_7724_7788_join[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 4, __iter_init_32_++)
		init_buffer_complex(&SplitJoin35_SplitJoin22_SplitJoin22_split2_7409_7655_7733_7807_join[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 2, __iter_init_33_++)
		init_buffer_complex(&SplitJoin84_SplitJoin67_SplitJoin67_AnonFilter_a0_7449_7690_7773_7790_split[__iter_init_33_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_7568butterfly_7475);
	FOR(int, __iter_init_34_, 0, <, 2, __iter_init_34_++)
		init_buffer_complex(&SplitJoin70_SplitJoin53_SplitJoin53_AnonFilter_a0_7431_7681_7769_7785_split[__iter_init_34_]);
	ENDFOR
	init_buffer_complex(&butterfly_7476Post_CollapsedDataParallel_2_7572);
	FOR(int, __iter_init_35_, 0, <, 6, __iter_init_35_++)
		init_buffer_complex(&SplitJoin24_magnitude_Fiss_7767_7808_split[__iter_init_35_]);
	ENDFOR
	init_buffer_complex(&butterfly_7477Post_CollapsedDataParallel_2_7575);
	FOR(int, __iter_init_36_, 0, <, 2, __iter_init_36_++)
		init_buffer_complex(&SplitJoin70_SplitJoin53_SplitJoin53_AnonFilter_a0_7431_7681_7769_7785_join[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 2, __iter_init_37_++)
		init_buffer_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_Hier_7764_7795_join[__iter_init_37_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7634WEIGHTED_ROUND_ROBIN_Splitter_7752);
	FOR(int, __iter_init_38_, 0, <, 2, __iter_init_38_++)
		init_buffer_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_Hier_7765_7798_split[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 2, __iter_init_39_++)
		init_buffer_complex(&SplitJoin6_SplitJoin4_SplitJoin4_AnonFilter_a0_7417_7641_7762_7782_split[__iter_init_39_]);
	ENDFOR
	init_buffer_complex(&butterfly_7478Post_CollapsedDataParallel_2_7578);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_7753sink_7502);
	FOR(int, __iter_init_40_, 0, <, 2, __iter_init_40_++)
		init_buffer_complex(&SplitJoin18_SplitJoin12_SplitJoin12_split2_7394_7647_7725_7800_join[__iter_init_40_]);
	ENDFOR
	init_buffer_complex(&butterfly_7481Post_CollapsedDataParallel_2_7587);
	init_buffer_complex(&Pre_CollapsedDataParallel_1_7580butterfly_7479);
	FOR(int, __iter_init_41_, 0, <, 2, __iter_init_41_++)
		init_buffer_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_Hier_7764_7795_split[__iter_init_41_]);
	ENDFOR
	init_buffer_complex(&butterfly_7480Post_CollapsedDataParallel_2_7584);
	FOR(int, __iter_init_42_, 0, <, 2, __iter_init_42_++)
		init_buffer_complex(&SplitJoin88_SplitJoin71_SplitJoin71_AnonFilter_a0_7455_7693_7774_7791_split[__iter_init_42_]);
	ENDFOR
	FOR(int, __iter_init_43_, 0, <, 2, __iter_init_43_++)
		init_buffer_complex(&SplitJoin98_SplitJoin81_SplitJoin81_AnonFilter_a0_7469_7700_7777_7794_split[__iter_init_43_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_7577butterfly_7478);
	FOR(int, __iter_init_44_, 0, <, 6, __iter_init_44_++)
		init_buffer_float(&SplitJoin24_magnitude_Fiss_7767_7808_join[__iter_init_44_]);
	ENDFOR
	FOR(int, __iter_init_45_, 0, <, 2, __iter_init_45_++)
		init_buffer_complex(&SplitJoin82_SplitJoin65_SplitJoin65_AnonFilter_a0_7447_7689_7772_7789_split[__iter_init_45_]);
	ENDFOR
	FOR(int, __iter_init_46_, 0, <, 2, __iter_init_46_++)
		init_buffer_complex(&SplitJoin72_SplitJoin55_SplitJoin55_AnonFilter_a0_7433_7682_7770_7786_join[__iter_init_46_]);
	ENDFOR
	FOR(int, __iter_init_47_, 0, <, 2, __iter_init_47_++)
		init_buffer_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child0_7732_7799_split[__iter_init_47_]);
	ENDFOR
	FOR(int, __iter_init_48_, 0, <, 2, __iter_init_48_++)
		init_buffer_complex(&SplitJoin48_SplitJoin33_SplitJoin33_split2_7398_7664_7728_7803_split[__iter_init_48_]);
	ENDFOR
	FOR(int, __iter_init_49_, 0, <, 2, __iter_init_49_++)
		init_buffer_complex(&SplitJoin2_SplitJoin0_SplitJoin0_AnonFilter_a0_7413_7639_7761_7780_split[__iter_init_49_]);
	ENDFOR
	FOR(int, __iter_init_50_, 0, <, 4, __iter_init_50_++)
		init_buffer_complex(&SplitJoin12_SplitJoin8_SplitJoin8_split1_7371_7644_Hier_child0_7726_7796_split[__iter_init_50_]);
	ENDFOR
	FOR(int, __iter_init_51_, 0, <, 2, __iter_init_51_++)
		init_buffer_complex(&SplitJoin92_SplitJoin75_SplitJoin75_AnonFilter_a0_7461_7696_7775_7792_join[__iter_init_51_]);
	ENDFOR
	FOR(int, __iter_init_52_, 0, <, 2, __iter_init_52_++)
		init_buffer_complex(&SplitJoin66_SplitJoin49_SplitJoin49_AnonFilter_a0_7425_7678_7768_7784_split[__iter_init_52_]);
	ENDFOR
	FOR(int, __iter_init_53_, 0, <, 2, __iter_init_53_++)
		init_buffer_complex(&SplitJoin42_SplitJoin29_SplitJoin29_split2_7396_7661_7727_7801_split[__iter_init_53_]);
	ENDFOR
	FOR(int, __iter_init_54_, 0, <, 2, __iter_init_54_++)
		init_buffer_complex(&SplitJoin98_SplitJoin81_SplitJoin81_AnonFilter_a0_7469_7700_7777_7794_join[__iter_init_54_]);
	ENDFOR
	FOR(int, __iter_init_55_, 0, <, 2, __iter_init_55_++)
		init_buffer_complex(&SplitJoin52_SplitJoin37_SplitJoin37_split2_7400_7667_7730_7804_join[__iter_init_55_]);
	ENDFOR
	FOR(int, __iter_init_56_, 0, <, 2, __iter_init_56_++)
		init_buffer_complex(&SplitJoin0_source_Fiss_7760_7779_split[__iter_init_56_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7740WEIGHTED_ROUND_ROBIN_Splitter_7741);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_7749WEIGHTED_ROUND_ROBIN_Splitter_7591);
	init_buffer_complex(&Pre_CollapsedDataParallel_1_7583butterfly_7480);
	FOR(int, __iter_init_57_, 0, <, 2, __iter_init_57_++)
		init_buffer_complex(&SplitJoin16_SplitJoin10_SplitJoin10_split1_7392_7646_Hier_child0_7732_7799_join[__iter_init_57_]);
	ENDFOR
	init_buffer_complex(&butterfly_7479Post_CollapsedDataParallel_2_7581);
	init_buffer_complex(&butterfly_7482Post_CollapsedDataParallel_2_7590);
	FOR(int, __iter_init_58_, 0, <, 4, __iter_init_58_++)
		init_buffer_complex(&SplitJoin22_SplitJoin16_SplitJoin16_split2_7407_7650_7731_7806_join[__iter_init_58_]);
	ENDFOR
	FOR(int, __iter_init_59_, 0, <, 2, __iter_init_59_++)
		init_buffer_complex(&SplitJoin88_SplitJoin71_SplitJoin71_AnonFilter_a0_7455_7693_7774_7791_join[__iter_init_59_]);
	ENDFOR
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_7748();
			source_7750();
			source_7751();
		WEIGHTED_ROUND_ROBIN_Joiner_7749();
		WEIGHTED_ROUND_ROBIN_Splitter_7591();
			WEIGHTED_ROUND_ROBIN_Splitter_7593();
				WEIGHTED_ROUND_ROBIN_Splitter_7595();
					WEIGHTED_ROUND_ROBIN_Splitter_7597();
						Identity_7421();
						Identity_7423();
					WEIGHTED_ROUND_ROBIN_Joiner_7598();
					WEIGHTED_ROUND_ROBIN_Splitter_7599();
						Identity_7427();
						Identity_7429();
					WEIGHTED_ROUND_ROBIN_Joiner_7600();
				WEIGHTED_ROUND_ROBIN_Joiner_7596();
				WEIGHTED_ROUND_ROBIN_Splitter_7601();
					WEIGHTED_ROUND_ROBIN_Splitter_7603();
						Identity_7435();
						Identity_7437();
					WEIGHTED_ROUND_ROBIN_Joiner_7604();
					WEIGHTED_ROUND_ROBIN_Splitter_7605();
						Identity_7441();
						Identity_7443();
					WEIGHTED_ROUND_ROBIN_Joiner_7606();
				WEIGHTED_ROUND_ROBIN_Joiner_7602();
			WEIGHTED_ROUND_ROBIN_Joiner_7594();
			WEIGHTED_ROUND_ROBIN_Splitter_7607();
				WEIGHTED_ROUND_ROBIN_Splitter_7609();
					WEIGHTED_ROUND_ROBIN_Splitter_7611();
						Identity_7451();
						Identity_7453();
					WEIGHTED_ROUND_ROBIN_Joiner_7612();
					WEIGHTED_ROUND_ROBIN_Splitter_7613();
						Identity_7457();
						Identity_7459();
					WEIGHTED_ROUND_ROBIN_Joiner_7614();
				WEIGHTED_ROUND_ROBIN_Joiner_7610();
				WEIGHTED_ROUND_ROBIN_Splitter_7615();
					WEIGHTED_ROUND_ROBIN_Splitter_7617();
						Identity_7465();
						Identity_7467();
					WEIGHTED_ROUND_ROBIN_Joiner_7618();
					WEIGHTED_ROUND_ROBIN_Splitter_7619();
						Identity_7471();
						Identity_7473();
					WEIGHTED_ROUND_ROBIN_Joiner_7620();
				WEIGHTED_ROUND_ROBIN_Joiner_7616();
			WEIGHTED_ROUND_ROBIN_Joiner_7608();
		WEIGHTED_ROUND_ROBIN_Joiner_7592();
		WEIGHTED_ROUND_ROBIN_Splitter_7735();
			WEIGHTED_ROUND_ROBIN_Splitter_7736();
				Pre_CollapsedDataParallel_1_7568();
				butterfly_7475();
				Post_CollapsedDataParallel_2_7569();
				Pre_CollapsedDataParallel_1_7571();
				butterfly_7476();
				Post_CollapsedDataParallel_2_7572();
				Pre_CollapsedDataParallel_1_7574();
				butterfly_7477();
				Post_CollapsedDataParallel_2_7575();
				Pre_CollapsedDataParallel_1_7577();
				butterfly_7478();
				Post_CollapsedDataParallel_2_7578();
			WEIGHTED_ROUND_ROBIN_Joiner_7737();
			WEIGHTED_ROUND_ROBIN_Splitter_7738();
				Pre_CollapsedDataParallel_1_7580();
				butterfly_7479();
				Post_CollapsedDataParallel_2_7581();
				Pre_CollapsedDataParallel_1_7583();
				butterfly_7480();
				Post_CollapsedDataParallel_2_7584();
				Pre_CollapsedDataParallel_1_7586();
				butterfly_7481();
				Post_CollapsedDataParallel_2_7587();
				Pre_CollapsedDataParallel_1_7589();
				butterfly_7482();
				Post_CollapsedDataParallel_2_7590();
			WEIGHTED_ROUND_ROBIN_Joiner_7739();
		WEIGHTED_ROUND_ROBIN_Joiner_7740();
		WEIGHTED_ROUND_ROBIN_Splitter_7741();
			WEIGHTED_ROUND_ROBIN_Splitter_7742();
				WEIGHTED_ROUND_ROBIN_Splitter_7625();
					butterfly_7484();
					butterfly_7485();
				WEIGHTED_ROUND_ROBIN_Joiner_7626();
				WEIGHTED_ROUND_ROBIN_Splitter_7627();
					butterfly_7486();
					butterfly_7487();
				WEIGHTED_ROUND_ROBIN_Joiner_7628();
			WEIGHTED_ROUND_ROBIN_Joiner_7743();
			WEIGHTED_ROUND_ROBIN_Splitter_7744();
				WEIGHTED_ROUND_ROBIN_Splitter_7629();
					butterfly_7488();
					butterfly_7489();
				WEIGHTED_ROUND_ROBIN_Joiner_7630();
				WEIGHTED_ROUND_ROBIN_Splitter_7631();
					butterfly_7490();
					butterfly_7491();
				WEIGHTED_ROUND_ROBIN_Joiner_7632();
			WEIGHTED_ROUND_ROBIN_Joiner_7745();
		WEIGHTED_ROUND_ROBIN_Joiner_7746();
		WEIGHTED_ROUND_ROBIN_Splitter_7633();
			WEIGHTED_ROUND_ROBIN_Splitter_7635();
				butterfly_7493();
				butterfly_7494();
				butterfly_7495();
				butterfly_7496();
			WEIGHTED_ROUND_ROBIN_Joiner_7636();
			WEIGHTED_ROUND_ROBIN_Splitter_7637();
				butterfly_7497();
				butterfly_7498();
				butterfly_7499();
				butterfly_7500();
			WEIGHTED_ROUND_ROBIN_Joiner_7638();
		WEIGHTED_ROUND_ROBIN_Joiner_7634();
		WEIGHTED_ROUND_ROBIN_Splitter_7752();
			magnitude_7754();
			magnitude_7755();
			magnitude_7756();
			magnitude_7757();
			magnitude_7758();
			magnitude_7759();
		WEIGHTED_ROUND_ROBIN_Joiner_7753();
		sink_7502();
	ENDFOR
	return EXIT_SUCCESS;
}
