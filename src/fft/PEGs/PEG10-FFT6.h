#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=640 on the compile command line
#else
#if BUF_SIZEMAX < 640
#error BUF_SIZEMAX too small, it must be at least 640
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	complex_t wn;
} CombineDFT_4883_t;
void FFTTestSource(buffer_complex_t *chanout);
void FFTTestSource_4835();
void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout);
void FFTReorderSimple_4836();
void WEIGHTED_ROUND_ROBIN_Splitter_4849();
void FFTReorderSimple_4851();
void FFTReorderSimple_4852();
void WEIGHTED_ROUND_ROBIN_Joiner_4850();
void WEIGHTED_ROUND_ROBIN_Splitter_4853();
void FFTReorderSimple_4855();
void FFTReorderSimple_4856();
void FFTReorderSimple_4857();
void FFTReorderSimple_4858();
void WEIGHTED_ROUND_ROBIN_Joiner_4854();
void WEIGHTED_ROUND_ROBIN_Splitter_4859();
void FFTReorderSimple_4861();
void FFTReorderSimple_4862();
void FFTReorderSimple_4863();
void FFTReorderSimple_4864();
void FFTReorderSimple_4865();
void FFTReorderSimple_4866();
void FFTReorderSimple_4867();
void FFTReorderSimple_4868();
void WEIGHTED_ROUND_ROBIN_Joiner_4860();
void WEIGHTED_ROUND_ROBIN_Splitter_4869();
void FFTReorderSimple_4871();
void FFTReorderSimple_4872();
void FFTReorderSimple_4873();
void FFTReorderSimple_4874();
void FFTReorderSimple_4875();
void FFTReorderSimple_4876();
void FFTReorderSimple_4877();
void FFTReorderSimple_4878();
void FFTReorderSimple_4879();
void FFTReorderSimple_4880();
void WEIGHTED_ROUND_ROBIN_Joiner_4870();
void WEIGHTED_ROUND_ROBIN_Splitter_4881();
void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout);
void CombineDFT_4883();
void CombineDFT_4884();
void CombineDFT_4885();
void CombineDFT_4886();
void CombineDFT_4887();
void CombineDFT_4888();
void CombineDFT_4889();
void CombineDFT_4890();
void CombineDFT_4891();
void CombineDFT_4892();
void WEIGHTED_ROUND_ROBIN_Joiner_4882();
void WEIGHTED_ROUND_ROBIN_Splitter_4893();
void CombineDFT_4895();
void CombineDFT_4896();
void CombineDFT_4897();
void CombineDFT_4898();
void CombineDFT_4899();
void CombineDFT_4900();
void CombineDFT_4901();
void CombineDFT_4902();
void CombineDFT_4903();
void CombineDFT_4904();
void WEIGHTED_ROUND_ROBIN_Joiner_4894();
void WEIGHTED_ROUND_ROBIN_Splitter_4905();
void CombineDFT_4907();
void CombineDFT_4908();
void CombineDFT_4909();
void CombineDFT_4910();
void CombineDFT_4911();
void CombineDFT_4912();
void CombineDFT_4913();
void CombineDFT_4914();
void WEIGHTED_ROUND_ROBIN_Joiner_4906();
void WEIGHTED_ROUND_ROBIN_Splitter_4915();
void CombineDFT_4917();
void CombineDFT_4918();
void CombineDFT_4919();
void CombineDFT_4920();
void WEIGHTED_ROUND_ROBIN_Joiner_4916();
void WEIGHTED_ROUND_ROBIN_Splitter_4921();
void CombineDFT_4923();
void CombineDFT_4924();
void WEIGHTED_ROUND_ROBIN_Joiner_4922();
void CombineDFT_4846();
void CPrinter(buffer_complex_t *chanin);
void CPrinter_4847();

#ifdef __cplusplus
}
#endif
#endif
