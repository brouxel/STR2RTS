#include "PEG14-FFT2.h"

buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_8747_8768_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8622WEIGHTED_ROUND_ROBIN_Splitter_8637;
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_8748_8769_split[4];
buffer_float_t SplitJoin84_FFTReorderSimple_Fiss_8756_8777_join[2];
buffer_float_t SplitJoin92_CombineDFT_Fiss_8760_8781_join[14];
buffer_float_t SplitJoin100_CombineDFT_Fiss_8764_8785_join[2];
buffer_float_t SplitJoin84_FFTReorderSimple_Fiss_8756_8777_split[2];
buffer_float_t SplitJoin88_FFTReorderSimple_Fiss_8758_8779_split[8];
buffer_float_t SplitJoin12_CombineDFT_Fiss_8751_8772_join[14];
buffer_float_t SplitJoin18_CombineDFT_Fiss_8754_8775_split[4];
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_8750_8771_split[14];
buffer_float_t SplitJoin86_FFTReorderSimple_Fiss_8757_8778_split[4];
buffer_float_t SplitJoin4_FFTReorderSimple_Fiss_8747_8768_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8736WEIGHTED_ROUND_ROBIN_Splitter_8741;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8654CombineDFT_8544;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8668WEIGHTED_ROUND_ROBIN_Splitter_8677;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8648WEIGHTED_ROUND_ROBIN_Splitter_8653;
buffer_float_t SplitJoin0_FFTTestSource_Fiss_8745_8766_split[2];
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_8749_8770_split[8];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_8523_8559_8746_8767_join[2];
buffer_float_t SplitJoin6_FFTReorderSimple_Fiss_8748_8769_join[4];
buffer_float_t SplitJoin16_CombineDFT_Fiss_8753_8774_split[8];
buffer_float_t SplitJoin18_CombineDFT_Fiss_8754_8775_join[4];
buffer_float_t SplitJoin10_FFTReorderSimple_Fiss_8750_8771_join[14];
buffer_float_t FFTReorderSimple_8534WEIGHTED_ROUND_ROBIN_Splitter_8569;
buffer_float_t SplitJoin98_CombineDFT_Fiss_8763_8784_split[4];
buffer_float_t SplitJoin96_CombineDFT_Fiss_8762_8783_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8694WEIGHTED_ROUND_ROBIN_Splitter_8709;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8710WEIGHTED_ROUND_ROBIN_Splitter_8725;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8570WEIGHTED_ROUND_ROBIN_Splitter_8573;
buffer_float_t SplitJoin98_CombineDFT_Fiss_8763_8784_join[4];
buffer_float_t SplitJoin16_CombineDFT_Fiss_8753_8774_join[8];
buffer_float_t SplitJoin100_CombineDFT_Fiss_8764_8785_split[2];
buffer_float_t SplitJoin90_FFTReorderSimple_Fiss_8759_8780_split[14];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8678WEIGHTED_ROUND_ROBIN_Splitter_8693;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8606WEIGHTED_ROUND_ROBIN_Splitter_8621;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8574WEIGHTED_ROUND_ROBIN_Splitter_8579;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8638WEIGHTED_ROUND_ROBIN_Splitter_8647;
buffer_float_t SplitJoin96_CombineDFT_Fiss_8762_8783_join[8];
buffer_float_t SplitJoin14_CombineDFT_Fiss_8752_8773_split[14];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8726WEIGHTED_ROUND_ROBIN_Splitter_8735;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8590WEIGHTED_ROUND_ROBIN_Splitter_8605;
buffer_float_t SplitJoin86_FFTReorderSimple_Fiss_8757_8778_join[4];
buffer_float_t SplitJoin94_CombineDFT_Fiss_8761_8782_split[14];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8742CombineDFT_8555;
buffer_float_t SplitJoin20_CombineDFT_Fiss_8755_8776_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8662WEIGHTED_ROUND_ROBIN_Splitter_8667;
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_8523_8559_8746_8767_split[2];
buffer_float_t SplitJoin20_CombineDFT_Fiss_8755_8776_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8658WEIGHTED_ROUND_ROBIN_Splitter_8661;
buffer_float_t SplitJoin14_CombineDFT_Fiss_8752_8773_join[14];
buffer_float_t SplitJoin92_CombineDFT_Fiss_8760_8781_split[14];
buffer_float_t SplitJoin12_CombineDFT_Fiss_8751_8772_split[14];
buffer_float_t SplitJoin90_FFTReorderSimple_Fiss_8759_8780_join[14];
buffer_float_t SplitJoin0_FFTTestSource_Fiss_8745_8766_join[2];
buffer_float_t SplitJoin94_CombineDFT_Fiss_8761_8782_join[14];
buffer_float_t FFTReorderSimple_8545WEIGHTED_ROUND_ROBIN_Splitter_8657;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8558FloatPrinter_8556;
buffer_float_t SplitJoin8_FFTReorderSimple_Fiss_8749_8770_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8566WEIGHTED_ROUND_ROBIN_Splitter_8557;
buffer_float_t SplitJoin88_FFTReorderSimple_Fiss_8758_8779_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8580WEIGHTED_ROUND_ROBIN_Splitter_8589;


CombineDFT_8607_t CombineDFT_8607_s;
CombineDFT_8607_t CombineDFT_8608_s;
CombineDFT_8607_t CombineDFT_8609_s;
CombineDFT_8607_t CombineDFT_8610_s;
CombineDFT_8607_t CombineDFT_8611_s;
CombineDFT_8607_t CombineDFT_8612_s;
CombineDFT_8607_t CombineDFT_8613_s;
CombineDFT_8607_t CombineDFT_8614_s;
CombineDFT_8607_t CombineDFT_8615_s;
CombineDFT_8607_t CombineDFT_8616_s;
CombineDFT_8607_t CombineDFT_8617_s;
CombineDFT_8607_t CombineDFT_8618_s;
CombineDFT_8607_t CombineDFT_8619_s;
CombineDFT_8607_t CombineDFT_8620_s;
CombineDFT_8623_t CombineDFT_8623_s;
CombineDFT_8623_t CombineDFT_8624_s;
CombineDFT_8623_t CombineDFT_8625_s;
CombineDFT_8623_t CombineDFT_8626_s;
CombineDFT_8623_t CombineDFT_8627_s;
CombineDFT_8623_t CombineDFT_8628_s;
CombineDFT_8623_t CombineDFT_8629_s;
CombineDFT_8623_t CombineDFT_8630_s;
CombineDFT_8623_t CombineDFT_8631_s;
CombineDFT_8623_t CombineDFT_8632_s;
CombineDFT_8623_t CombineDFT_8633_s;
CombineDFT_8623_t CombineDFT_8634_s;
CombineDFT_8623_t CombineDFT_8635_s;
CombineDFT_8623_t CombineDFT_8636_s;
CombineDFT_8639_t CombineDFT_8639_s;
CombineDFT_8639_t CombineDFT_8640_s;
CombineDFT_8639_t CombineDFT_8641_s;
CombineDFT_8639_t CombineDFT_8642_s;
CombineDFT_8639_t CombineDFT_8643_s;
CombineDFT_8639_t CombineDFT_8644_s;
CombineDFT_8639_t CombineDFT_8645_s;
CombineDFT_8639_t CombineDFT_8646_s;
CombineDFT_8649_t CombineDFT_8649_s;
CombineDFT_8649_t CombineDFT_8650_s;
CombineDFT_8649_t CombineDFT_8651_s;
CombineDFT_8649_t CombineDFT_8652_s;
CombineDFT_8655_t CombineDFT_8655_s;
CombineDFT_8655_t CombineDFT_8656_s;
CombineDFT_8544_t CombineDFT_8544_s;
CombineDFT_8607_t CombineDFT_8695_s;
CombineDFT_8607_t CombineDFT_8696_s;
CombineDFT_8607_t CombineDFT_8697_s;
CombineDFT_8607_t CombineDFT_8698_s;
CombineDFT_8607_t CombineDFT_8699_s;
CombineDFT_8607_t CombineDFT_8700_s;
CombineDFT_8607_t CombineDFT_8701_s;
CombineDFT_8607_t CombineDFT_8702_s;
CombineDFT_8607_t CombineDFT_8703_s;
CombineDFT_8607_t CombineDFT_8704_s;
CombineDFT_8607_t CombineDFT_8705_s;
CombineDFT_8607_t CombineDFT_8706_s;
CombineDFT_8607_t CombineDFT_8707_s;
CombineDFT_8607_t CombineDFT_8708_s;
CombineDFT_8623_t CombineDFT_8711_s;
CombineDFT_8623_t CombineDFT_8712_s;
CombineDFT_8623_t CombineDFT_8713_s;
CombineDFT_8623_t CombineDFT_8714_s;
CombineDFT_8623_t CombineDFT_8715_s;
CombineDFT_8623_t CombineDFT_8716_s;
CombineDFT_8623_t CombineDFT_8717_s;
CombineDFT_8623_t CombineDFT_8718_s;
CombineDFT_8623_t CombineDFT_8719_s;
CombineDFT_8623_t CombineDFT_8720_s;
CombineDFT_8623_t CombineDFT_8721_s;
CombineDFT_8623_t CombineDFT_8722_s;
CombineDFT_8623_t CombineDFT_8723_s;
CombineDFT_8623_t CombineDFT_8724_s;
CombineDFT_8639_t CombineDFT_8727_s;
CombineDFT_8639_t CombineDFT_8728_s;
CombineDFT_8639_t CombineDFT_8729_s;
CombineDFT_8639_t CombineDFT_8730_s;
CombineDFT_8639_t CombineDFT_8731_s;
CombineDFT_8639_t CombineDFT_8732_s;
CombineDFT_8639_t CombineDFT_8733_s;
CombineDFT_8639_t CombineDFT_8734_s;
CombineDFT_8649_t CombineDFT_8737_s;
CombineDFT_8649_t CombineDFT_8738_s;
CombineDFT_8649_t CombineDFT_8739_s;
CombineDFT_8649_t CombineDFT_8740_s;
CombineDFT_8655_t CombineDFT_8743_s;
CombineDFT_8655_t CombineDFT_8744_s;
CombineDFT_8544_t CombineDFT_8555_s;

void FFTTestSource(buffer_void_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), 0.0) ; 
		push_float(&(*chanout), 0.0) ; 
		push_float(&(*chanout), 1.0) ; 
		push_float(&(*chanout), 0.0) ; 
		FOR(int, i, 0,  < , 124, i++) {
			push_float(&(*chanout), 0.0) ; 
		}
		ENDFOR
	}


void FFTTestSource_8567() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_8745_8766_split[0]), &(SplitJoin0_FFTTestSource_Fiss_8745_8766_join[0]));
	ENDFOR
}

void FFTTestSource_8568() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTTestSource(&(SplitJoin0_FFTTestSource_Fiss_8745_8766_split[1]), &(SplitJoin0_FFTTestSource_Fiss_8745_8766_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8565() {
}

void WEIGHTED_ROUND_ROBIN_Joiner_8566() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8566WEIGHTED_ROUND_ROBIN_Splitter_8557, pop_float(&SplitJoin0_FFTTestSource_Fiss_8745_8766_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8566WEIGHTED_ROUND_ROBIN_Splitter_8557, pop_float(&SplitJoin0_FFTTestSource_Fiss_8745_8766_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, i, 0,  < , 128, i = (i + 4)) {
			push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 2,  < , 128, i = (i + 4)) {
			push_float(&(*chanout), peek_float(&(*chanin), i)) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (i + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_float(&(*chanin)) ; 
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void FFTReorderSimple_8534() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_8523_8559_8746_8767_split[0]), &(FFTReorderSimple_8534WEIGHTED_ROUND_ROBIN_Splitter_8569));
	ENDFOR
}

void FFTReorderSimple_8571() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_8747_8768_split[0]), &(SplitJoin4_FFTReorderSimple_Fiss_8747_8768_join[0]));
	ENDFOR
}

void FFTReorderSimple_8572() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin4_FFTReorderSimple_Fiss_8747_8768_split[1]), &(SplitJoin4_FFTReorderSimple_Fiss_8747_8768_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8569() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_8747_8768_split[0], pop_float(&FFTReorderSimple_8534WEIGHTED_ROUND_ROBIN_Splitter_8569));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin4_FFTReorderSimple_Fiss_8747_8768_split[1], pop_float(&FFTReorderSimple_8534WEIGHTED_ROUND_ROBIN_Splitter_8569));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8570() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8570WEIGHTED_ROUND_ROBIN_Splitter_8573, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_8747_8768_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8570WEIGHTED_ROUND_ROBIN_Splitter_8573, pop_float(&SplitJoin4_FFTReorderSimple_Fiss_8747_8768_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_8575() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_8748_8769_split[0]), &(SplitJoin6_FFTReorderSimple_Fiss_8748_8769_join[0]));
	ENDFOR
}

void FFTReorderSimple_8576() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_8748_8769_split[1]), &(SplitJoin6_FFTReorderSimple_Fiss_8748_8769_join[1]));
	ENDFOR
}

void FFTReorderSimple_8577() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_8748_8769_split[2]), &(SplitJoin6_FFTReorderSimple_Fiss_8748_8769_join[2]));
	ENDFOR
}

void FFTReorderSimple_8578() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin6_FFTReorderSimple_Fiss_8748_8769_split[3]), &(SplitJoin6_FFTReorderSimple_Fiss_8748_8769_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8573() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin6_FFTReorderSimple_Fiss_8748_8769_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8570WEIGHTED_ROUND_ROBIN_Splitter_8573));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8574() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8574WEIGHTED_ROUND_ROBIN_Splitter_8579, pop_float(&SplitJoin6_FFTReorderSimple_Fiss_8748_8769_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_8581() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_8749_8770_split[0]), &(SplitJoin8_FFTReorderSimple_Fiss_8749_8770_join[0]));
	ENDFOR
}

void FFTReorderSimple_8582() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_8749_8770_split[1]), &(SplitJoin8_FFTReorderSimple_Fiss_8749_8770_join[1]));
	ENDFOR
}

void FFTReorderSimple_8583() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_8749_8770_split[2]), &(SplitJoin8_FFTReorderSimple_Fiss_8749_8770_join[2]));
	ENDFOR
}

void FFTReorderSimple_8584() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_8749_8770_split[3]), &(SplitJoin8_FFTReorderSimple_Fiss_8749_8770_join[3]));
	ENDFOR
}

void FFTReorderSimple_8585() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_8749_8770_split[4]), &(SplitJoin8_FFTReorderSimple_Fiss_8749_8770_join[4]));
	ENDFOR
}

void FFTReorderSimple_8586() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_8749_8770_split[5]), &(SplitJoin8_FFTReorderSimple_Fiss_8749_8770_join[5]));
	ENDFOR
}

void FFTReorderSimple_8587() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_8749_8770_split[6]), &(SplitJoin8_FFTReorderSimple_Fiss_8749_8770_join[6]));
	ENDFOR
}

void FFTReorderSimple_8588() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin8_FFTReorderSimple_Fiss_8749_8770_split[7]), &(SplitJoin8_FFTReorderSimple_Fiss_8749_8770_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8579() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin8_FFTReorderSimple_Fiss_8749_8770_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8574WEIGHTED_ROUND_ROBIN_Splitter_8579));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8580() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8580WEIGHTED_ROUND_ROBIN_Splitter_8589, pop_float(&SplitJoin8_FFTReorderSimple_Fiss_8749_8770_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_8591() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_8750_8771_split[0]), &(SplitJoin10_FFTReorderSimple_Fiss_8750_8771_join[0]));
	ENDFOR
}

void FFTReorderSimple_8592() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_8750_8771_split[1]), &(SplitJoin10_FFTReorderSimple_Fiss_8750_8771_join[1]));
	ENDFOR
}

void FFTReorderSimple_8593() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_8750_8771_split[2]), &(SplitJoin10_FFTReorderSimple_Fiss_8750_8771_join[2]));
	ENDFOR
}

void FFTReorderSimple_8594() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_8750_8771_split[3]), &(SplitJoin10_FFTReorderSimple_Fiss_8750_8771_join[3]));
	ENDFOR
}

void FFTReorderSimple_8595() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_8750_8771_split[4]), &(SplitJoin10_FFTReorderSimple_Fiss_8750_8771_join[4]));
	ENDFOR
}

void FFTReorderSimple_8596() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_8750_8771_split[5]), &(SplitJoin10_FFTReorderSimple_Fiss_8750_8771_join[5]));
	ENDFOR
}

void FFTReorderSimple_8597() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_8750_8771_split[6]), &(SplitJoin10_FFTReorderSimple_Fiss_8750_8771_join[6]));
	ENDFOR
}

void FFTReorderSimple_8598() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_8750_8771_split[7]), &(SplitJoin10_FFTReorderSimple_Fiss_8750_8771_join[7]));
	ENDFOR
}

void FFTReorderSimple_8599() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_8750_8771_split[8]), &(SplitJoin10_FFTReorderSimple_Fiss_8750_8771_join[8]));
	ENDFOR
}

void FFTReorderSimple_8600() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_8750_8771_split[9]), &(SplitJoin10_FFTReorderSimple_Fiss_8750_8771_join[9]));
	ENDFOR
}

void FFTReorderSimple_8601() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_8750_8771_split[10]), &(SplitJoin10_FFTReorderSimple_Fiss_8750_8771_join[10]));
	ENDFOR
}

void FFTReorderSimple_8602() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_8750_8771_split[11]), &(SplitJoin10_FFTReorderSimple_Fiss_8750_8771_join[11]));
	ENDFOR
}

void FFTReorderSimple_8603() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_8750_8771_split[12]), &(SplitJoin10_FFTReorderSimple_Fiss_8750_8771_join[12]));
	ENDFOR
}

void FFTReorderSimple_8604() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin10_FFTReorderSimple_Fiss_8750_8771_split[13]), &(SplitJoin10_FFTReorderSimple_Fiss_8750_8771_join[13]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8589() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 14, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_FFTReorderSimple_Fiss_8750_8771_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8580WEIGHTED_ROUND_ROBIN_Splitter_8589));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8590() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 14, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8590WEIGHTED_ROUND_ROBIN_Splitter_8605, pop_float(&SplitJoin10_FFTReorderSimple_Fiss_8750_8771_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT(buffer_float_t *chanin, buffer_float_t *chanout) {
		float results[4];
		FOR(int, i, 0,  < , 2, i = (i + 2)) {
			int i_plus_1 = 0;
			float y0_r = 0.0;
			float y0_i = 0.0;
			float y1_r = 0.0;
			float y1_i = 0.0;
			float weight_real = 0.0;
			float weight_imag = 0.0;
			float y1w_r = 0.0;
			float y1w_i = 0.0;
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			y0_r = 0.0 ; 
			y0_i = 0.0 ; 
			y1_r = 0.0 ; 
			y1_i = 0.0 ; 
			weight_real = 0.0 ; 
			weight_imag = 0.0 ; 
			y1w_r = 0.0 ; 
			y1w_i = 0.0 ; 
			i_plus_1 = 0 ; 
			i_plus_1 = (i + 1) ; 
			y0_r = 0.0 ; 
			y0_r = peek_float(&(*chanin), i) ; 
			y0_i = 0.0 ; 
			y0_i = peek_float(&(*chanin), i_plus_1) ; 
			y1_r = 0.0 ; 
			y1_r = peek_float(&(*chanin), (2 + i)) ; 
			y1_i = 0.0 ; 
			y1_i = peek_float(&(*chanin), (2 + i_plus_1)) ; 
			weight_real = 0.0 ; 
			weight_real = CombineDFT_8607_s.w[i] ; 
			weight_imag = 0.0 ; 
			weight_imag = CombineDFT_8607_s.w[i_plus_1] ; 
			y1w_r = 0.0 ; 
			y1w_r = ((y1_r * weight_real) - (y1_i * weight_imag)) ; 
			y1w_i = 0.0 ; 
			y1w_i = ((y1_r * weight_imag) + (y1_i * weight_real)) ; 
			results[i] = (y0_r + y1w_r) ; 
			results[(i + 1)] = (y0_i + y1w_i) ; 
			results[(2 + i)] = (y0_r - y1w_r) ; 
			results[((2 + i) + 1)] = (y0_i - y1w_i) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 4, i++) {
			pop_float(&(*chanin)) ; 
			push_float(&(*chanout), results[i]) ; 
		}
		ENDFOR
	}


void CombineDFT_8607() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_8751_8772_split[0]), &(SplitJoin12_CombineDFT_Fiss_8751_8772_join[0]));
	ENDFOR
}

void CombineDFT_8608() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_8751_8772_split[1]), &(SplitJoin12_CombineDFT_Fiss_8751_8772_join[1]));
	ENDFOR
}

void CombineDFT_8609() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_8751_8772_split[2]), &(SplitJoin12_CombineDFT_Fiss_8751_8772_join[2]));
	ENDFOR
}

void CombineDFT_8610() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_8751_8772_split[3]), &(SplitJoin12_CombineDFT_Fiss_8751_8772_join[3]));
	ENDFOR
}

void CombineDFT_8611() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_8751_8772_split[4]), &(SplitJoin12_CombineDFT_Fiss_8751_8772_join[4]));
	ENDFOR
}

void CombineDFT_8612() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_8751_8772_split[5]), &(SplitJoin12_CombineDFT_Fiss_8751_8772_join[5]));
	ENDFOR
}

void CombineDFT_8613() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_8751_8772_split[6]), &(SplitJoin12_CombineDFT_Fiss_8751_8772_join[6]));
	ENDFOR
}

void CombineDFT_8614() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_8751_8772_split[7]), &(SplitJoin12_CombineDFT_Fiss_8751_8772_join[7]));
	ENDFOR
}

void CombineDFT_8615() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_8751_8772_split[8]), &(SplitJoin12_CombineDFT_Fiss_8751_8772_join[8]));
	ENDFOR
}

void CombineDFT_8616() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_8751_8772_split[9]), &(SplitJoin12_CombineDFT_Fiss_8751_8772_join[9]));
	ENDFOR
}

void CombineDFT_8617() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_8751_8772_split[10]), &(SplitJoin12_CombineDFT_Fiss_8751_8772_join[10]));
	ENDFOR
}

void CombineDFT_8618() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_8751_8772_split[11]), &(SplitJoin12_CombineDFT_Fiss_8751_8772_join[11]));
	ENDFOR
}

void CombineDFT_8619() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_8751_8772_split[12]), &(SplitJoin12_CombineDFT_Fiss_8751_8772_join[12]));
	ENDFOR
}

void CombineDFT_8620() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin12_CombineDFT_Fiss_8751_8772_split[13]), &(SplitJoin12_CombineDFT_Fiss_8751_8772_join[13]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8605() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 14, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin12_CombineDFT_Fiss_8751_8772_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8590WEIGHTED_ROUND_ROBIN_Splitter_8605));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8606() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 14, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8606WEIGHTED_ROUND_ROBIN_Splitter_8621, pop_float(&SplitJoin12_CombineDFT_Fiss_8751_8772_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_8623() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_8752_8773_split[0]), &(SplitJoin14_CombineDFT_Fiss_8752_8773_join[0]));
	ENDFOR
}

void CombineDFT_8624() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_8752_8773_split[1]), &(SplitJoin14_CombineDFT_Fiss_8752_8773_join[1]));
	ENDFOR
}

void CombineDFT_8625() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_8752_8773_split[2]), &(SplitJoin14_CombineDFT_Fiss_8752_8773_join[2]));
	ENDFOR
}

void CombineDFT_8626() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_8752_8773_split[3]), &(SplitJoin14_CombineDFT_Fiss_8752_8773_join[3]));
	ENDFOR
}

void CombineDFT_8627() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_8752_8773_split[4]), &(SplitJoin14_CombineDFT_Fiss_8752_8773_join[4]));
	ENDFOR
}

void CombineDFT_8628() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_8752_8773_split[5]), &(SplitJoin14_CombineDFT_Fiss_8752_8773_join[5]));
	ENDFOR
}

void CombineDFT_8629() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_8752_8773_split[6]), &(SplitJoin14_CombineDFT_Fiss_8752_8773_join[6]));
	ENDFOR
}

void CombineDFT_8630() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_8752_8773_split[7]), &(SplitJoin14_CombineDFT_Fiss_8752_8773_join[7]));
	ENDFOR
}

void CombineDFT_8631() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_8752_8773_split[8]), &(SplitJoin14_CombineDFT_Fiss_8752_8773_join[8]));
	ENDFOR
}

void CombineDFT_8632() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_8752_8773_split[9]), &(SplitJoin14_CombineDFT_Fiss_8752_8773_join[9]));
	ENDFOR
}

void CombineDFT_8633() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_8752_8773_split[10]), &(SplitJoin14_CombineDFT_Fiss_8752_8773_join[10]));
	ENDFOR
}

void CombineDFT_8634() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_8752_8773_split[11]), &(SplitJoin14_CombineDFT_Fiss_8752_8773_join[11]));
	ENDFOR
}

void CombineDFT_8635() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_8752_8773_split[12]), &(SplitJoin14_CombineDFT_Fiss_8752_8773_join[12]));
	ENDFOR
}

void CombineDFT_8636() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin14_CombineDFT_Fiss_8752_8773_split[13]), &(SplitJoin14_CombineDFT_Fiss_8752_8773_join[13]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8621() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 14, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin14_CombineDFT_Fiss_8752_8773_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8606WEIGHTED_ROUND_ROBIN_Splitter_8621));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8622() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 14, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8622WEIGHTED_ROUND_ROBIN_Splitter_8637, pop_float(&SplitJoin14_CombineDFT_Fiss_8752_8773_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_8639() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_8753_8774_split[0]), &(SplitJoin16_CombineDFT_Fiss_8753_8774_join[0]));
	ENDFOR
}

void CombineDFT_8640() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_8753_8774_split[1]), &(SplitJoin16_CombineDFT_Fiss_8753_8774_join[1]));
	ENDFOR
}

void CombineDFT_8641() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_8753_8774_split[2]), &(SplitJoin16_CombineDFT_Fiss_8753_8774_join[2]));
	ENDFOR
}

void CombineDFT_8642() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_8753_8774_split[3]), &(SplitJoin16_CombineDFT_Fiss_8753_8774_join[3]));
	ENDFOR
}

void CombineDFT_8643() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_8753_8774_split[4]), &(SplitJoin16_CombineDFT_Fiss_8753_8774_join[4]));
	ENDFOR
}

void CombineDFT_8644() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_8753_8774_split[5]), &(SplitJoin16_CombineDFT_Fiss_8753_8774_join[5]));
	ENDFOR
}

void CombineDFT_8645() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_8753_8774_split[6]), &(SplitJoin16_CombineDFT_Fiss_8753_8774_join[6]));
	ENDFOR
}

void CombineDFT_8646() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin16_CombineDFT_Fiss_8753_8774_split[7]), &(SplitJoin16_CombineDFT_Fiss_8753_8774_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8637() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin16_CombineDFT_Fiss_8753_8774_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8622WEIGHTED_ROUND_ROBIN_Splitter_8637));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8638() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8638WEIGHTED_ROUND_ROBIN_Splitter_8647, pop_float(&SplitJoin16_CombineDFT_Fiss_8753_8774_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_8649() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_8754_8775_split[0]), &(SplitJoin18_CombineDFT_Fiss_8754_8775_join[0]));
	ENDFOR
}

void CombineDFT_8650() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_8754_8775_split[1]), &(SplitJoin18_CombineDFT_Fiss_8754_8775_join[1]));
	ENDFOR
}

void CombineDFT_8651() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_8754_8775_split[2]), &(SplitJoin18_CombineDFT_Fiss_8754_8775_join[2]));
	ENDFOR
}

void CombineDFT_8652() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin18_CombineDFT_Fiss_8754_8775_split[3]), &(SplitJoin18_CombineDFT_Fiss_8754_8775_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8647() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin18_CombineDFT_Fiss_8754_8775_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8638WEIGHTED_ROUND_ROBIN_Splitter_8647));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8648() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8648WEIGHTED_ROUND_ROBIN_Splitter_8653, pop_float(&SplitJoin18_CombineDFT_Fiss_8754_8775_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_8655() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin20_CombineDFT_Fiss_8755_8776_split[0]), &(SplitJoin20_CombineDFT_Fiss_8755_8776_join[0]));
	ENDFOR
}

void CombineDFT_8656() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin20_CombineDFT_Fiss_8755_8776_split[1]), &(SplitJoin20_CombineDFT_Fiss_8755_8776_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8653() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_8755_8776_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8648WEIGHTED_ROUND_ROBIN_Splitter_8653));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin20_CombineDFT_Fiss_8755_8776_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8648WEIGHTED_ROUND_ROBIN_Splitter_8653));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8654() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8654CombineDFT_8544, pop_float(&SplitJoin20_CombineDFT_Fiss_8755_8776_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8654CombineDFT_8544, pop_float(&SplitJoin20_CombineDFT_Fiss_8755_8776_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_8544() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_8654CombineDFT_8544), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_8523_8559_8746_8767_join[0]));
	ENDFOR
}

void FFTReorderSimple_8545() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_8523_8559_8746_8767_split[1]), &(FFTReorderSimple_8545WEIGHTED_ROUND_ROBIN_Splitter_8657));
	ENDFOR
}

void FFTReorderSimple_8659() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin84_FFTReorderSimple_Fiss_8756_8777_split[0]), &(SplitJoin84_FFTReorderSimple_Fiss_8756_8777_join[0]));
	ENDFOR
}

void FFTReorderSimple_8660() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin84_FFTReorderSimple_Fiss_8756_8777_split[1]), &(SplitJoin84_FFTReorderSimple_Fiss_8756_8777_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8657() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin84_FFTReorderSimple_Fiss_8756_8777_split[0], pop_float(&FFTReorderSimple_8545WEIGHTED_ROUND_ROBIN_Splitter_8657));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin84_FFTReorderSimple_Fiss_8756_8777_split[1], pop_float(&FFTReorderSimple_8545WEIGHTED_ROUND_ROBIN_Splitter_8657));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8658() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8658WEIGHTED_ROUND_ROBIN_Splitter_8661, pop_float(&SplitJoin84_FFTReorderSimple_Fiss_8756_8777_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8658WEIGHTED_ROUND_ROBIN_Splitter_8661, pop_float(&SplitJoin84_FFTReorderSimple_Fiss_8756_8777_join[1]));
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_8663() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin86_FFTReorderSimple_Fiss_8757_8778_split[0]), &(SplitJoin86_FFTReorderSimple_Fiss_8757_8778_join[0]));
	ENDFOR
}

void FFTReorderSimple_8664() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin86_FFTReorderSimple_Fiss_8757_8778_split[1]), &(SplitJoin86_FFTReorderSimple_Fiss_8757_8778_join[1]));
	ENDFOR
}

void FFTReorderSimple_8665() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin86_FFTReorderSimple_Fiss_8757_8778_split[2]), &(SplitJoin86_FFTReorderSimple_Fiss_8757_8778_join[2]));
	ENDFOR
}

void FFTReorderSimple_8666() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin86_FFTReorderSimple_Fiss_8757_8778_split[3]), &(SplitJoin86_FFTReorderSimple_Fiss_8757_8778_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8661() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin86_FFTReorderSimple_Fiss_8757_8778_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8658WEIGHTED_ROUND_ROBIN_Splitter_8661));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8662() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8662WEIGHTED_ROUND_ROBIN_Splitter_8667, pop_float(&SplitJoin86_FFTReorderSimple_Fiss_8757_8778_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_8669() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin88_FFTReorderSimple_Fiss_8758_8779_split[0]), &(SplitJoin88_FFTReorderSimple_Fiss_8758_8779_join[0]));
	ENDFOR
}

void FFTReorderSimple_8670() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin88_FFTReorderSimple_Fiss_8758_8779_split[1]), &(SplitJoin88_FFTReorderSimple_Fiss_8758_8779_join[1]));
	ENDFOR
}

void FFTReorderSimple_8671() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin88_FFTReorderSimple_Fiss_8758_8779_split[2]), &(SplitJoin88_FFTReorderSimple_Fiss_8758_8779_join[2]));
	ENDFOR
}

void FFTReorderSimple_8672() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin88_FFTReorderSimple_Fiss_8758_8779_split[3]), &(SplitJoin88_FFTReorderSimple_Fiss_8758_8779_join[3]));
	ENDFOR
}

void FFTReorderSimple_8673() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin88_FFTReorderSimple_Fiss_8758_8779_split[4]), &(SplitJoin88_FFTReorderSimple_Fiss_8758_8779_join[4]));
	ENDFOR
}

void FFTReorderSimple_8674() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin88_FFTReorderSimple_Fiss_8758_8779_split[5]), &(SplitJoin88_FFTReorderSimple_Fiss_8758_8779_join[5]));
	ENDFOR
}

void FFTReorderSimple_8675() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin88_FFTReorderSimple_Fiss_8758_8779_split[6]), &(SplitJoin88_FFTReorderSimple_Fiss_8758_8779_join[6]));
	ENDFOR
}

void FFTReorderSimple_8676() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin88_FFTReorderSimple_Fiss_8758_8779_split[7]), &(SplitJoin88_FFTReorderSimple_Fiss_8758_8779_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8667() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin88_FFTReorderSimple_Fiss_8758_8779_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8662WEIGHTED_ROUND_ROBIN_Splitter_8667));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8668() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8668WEIGHTED_ROUND_ROBIN_Splitter_8677, pop_float(&SplitJoin88_FFTReorderSimple_Fiss_8758_8779_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void FFTReorderSimple_8679() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin90_FFTReorderSimple_Fiss_8759_8780_split[0]), &(SplitJoin90_FFTReorderSimple_Fiss_8759_8780_join[0]));
	ENDFOR
}

void FFTReorderSimple_8680() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin90_FFTReorderSimple_Fiss_8759_8780_split[1]), &(SplitJoin90_FFTReorderSimple_Fiss_8759_8780_join[1]));
	ENDFOR
}

void FFTReorderSimple_8681() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin90_FFTReorderSimple_Fiss_8759_8780_split[2]), &(SplitJoin90_FFTReorderSimple_Fiss_8759_8780_join[2]));
	ENDFOR
}

void FFTReorderSimple_8682() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin90_FFTReorderSimple_Fiss_8759_8780_split[3]), &(SplitJoin90_FFTReorderSimple_Fiss_8759_8780_join[3]));
	ENDFOR
}

void FFTReorderSimple_8683() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin90_FFTReorderSimple_Fiss_8759_8780_split[4]), &(SplitJoin90_FFTReorderSimple_Fiss_8759_8780_join[4]));
	ENDFOR
}

void FFTReorderSimple_8684() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin90_FFTReorderSimple_Fiss_8759_8780_split[5]), &(SplitJoin90_FFTReorderSimple_Fiss_8759_8780_join[5]));
	ENDFOR
}

void FFTReorderSimple_8685() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin90_FFTReorderSimple_Fiss_8759_8780_split[6]), &(SplitJoin90_FFTReorderSimple_Fiss_8759_8780_join[6]));
	ENDFOR
}

void FFTReorderSimple_8686() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin90_FFTReorderSimple_Fiss_8759_8780_split[7]), &(SplitJoin90_FFTReorderSimple_Fiss_8759_8780_join[7]));
	ENDFOR
}

void FFTReorderSimple_8687() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin90_FFTReorderSimple_Fiss_8759_8780_split[8]), &(SplitJoin90_FFTReorderSimple_Fiss_8759_8780_join[8]));
	ENDFOR
}

void FFTReorderSimple_8688() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin90_FFTReorderSimple_Fiss_8759_8780_split[9]), &(SplitJoin90_FFTReorderSimple_Fiss_8759_8780_join[9]));
	ENDFOR
}

void FFTReorderSimple_8689() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin90_FFTReorderSimple_Fiss_8759_8780_split[10]), &(SplitJoin90_FFTReorderSimple_Fiss_8759_8780_join[10]));
	ENDFOR
}

void FFTReorderSimple_8690() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin90_FFTReorderSimple_Fiss_8759_8780_split[11]), &(SplitJoin90_FFTReorderSimple_Fiss_8759_8780_join[11]));
	ENDFOR
}

void FFTReorderSimple_8691() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin90_FFTReorderSimple_Fiss_8759_8780_split[12]), &(SplitJoin90_FFTReorderSimple_Fiss_8759_8780_join[12]));
	ENDFOR
}

void FFTReorderSimple_8692() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FFTReorderSimple(&(SplitJoin90_FFTReorderSimple_Fiss_8759_8780_split[13]), &(SplitJoin90_FFTReorderSimple_Fiss_8759_8780_join[13]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8677() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 14, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin90_FFTReorderSimple_Fiss_8759_8780_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8668WEIGHTED_ROUND_ROBIN_Splitter_8677));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8678() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 14, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8678WEIGHTED_ROUND_ROBIN_Splitter_8693, pop_float(&SplitJoin90_FFTReorderSimple_Fiss_8759_8780_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_8695() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin92_CombineDFT_Fiss_8760_8781_split[0]), &(SplitJoin92_CombineDFT_Fiss_8760_8781_join[0]));
	ENDFOR
}

void CombineDFT_8696() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin92_CombineDFT_Fiss_8760_8781_split[1]), &(SplitJoin92_CombineDFT_Fiss_8760_8781_join[1]));
	ENDFOR
}

void CombineDFT_8697() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin92_CombineDFT_Fiss_8760_8781_split[2]), &(SplitJoin92_CombineDFT_Fiss_8760_8781_join[2]));
	ENDFOR
}

void CombineDFT_8698() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin92_CombineDFT_Fiss_8760_8781_split[3]), &(SplitJoin92_CombineDFT_Fiss_8760_8781_join[3]));
	ENDFOR
}

void CombineDFT_8699() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin92_CombineDFT_Fiss_8760_8781_split[4]), &(SplitJoin92_CombineDFT_Fiss_8760_8781_join[4]));
	ENDFOR
}

void CombineDFT_8700() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin92_CombineDFT_Fiss_8760_8781_split[5]), &(SplitJoin92_CombineDFT_Fiss_8760_8781_join[5]));
	ENDFOR
}

void CombineDFT_8701() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin92_CombineDFT_Fiss_8760_8781_split[6]), &(SplitJoin92_CombineDFT_Fiss_8760_8781_join[6]));
	ENDFOR
}

void CombineDFT_8702() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin92_CombineDFT_Fiss_8760_8781_split[7]), &(SplitJoin92_CombineDFT_Fiss_8760_8781_join[7]));
	ENDFOR
}

void CombineDFT_8703() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin92_CombineDFT_Fiss_8760_8781_split[8]), &(SplitJoin92_CombineDFT_Fiss_8760_8781_join[8]));
	ENDFOR
}

void CombineDFT_8704() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin92_CombineDFT_Fiss_8760_8781_split[9]), &(SplitJoin92_CombineDFT_Fiss_8760_8781_join[9]));
	ENDFOR
}

void CombineDFT_8705() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin92_CombineDFT_Fiss_8760_8781_split[10]), &(SplitJoin92_CombineDFT_Fiss_8760_8781_join[10]));
	ENDFOR
}

void CombineDFT_8706() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin92_CombineDFT_Fiss_8760_8781_split[11]), &(SplitJoin92_CombineDFT_Fiss_8760_8781_join[11]));
	ENDFOR
}

void CombineDFT_8707() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin92_CombineDFT_Fiss_8760_8781_split[12]), &(SplitJoin92_CombineDFT_Fiss_8760_8781_join[12]));
	ENDFOR
}

void CombineDFT_8708() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		CombineDFT(&(SplitJoin92_CombineDFT_Fiss_8760_8781_split[13]), &(SplitJoin92_CombineDFT_Fiss_8760_8781_join[13]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8693() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 14, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin92_CombineDFT_Fiss_8760_8781_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8678WEIGHTED_ROUND_ROBIN_Splitter_8693));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8694() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 14, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8694WEIGHTED_ROUND_ROBIN_Splitter_8709, pop_float(&SplitJoin92_CombineDFT_Fiss_8760_8781_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_8711() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin94_CombineDFT_Fiss_8761_8782_split[0]), &(SplitJoin94_CombineDFT_Fiss_8761_8782_join[0]));
	ENDFOR
}

void CombineDFT_8712() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin94_CombineDFT_Fiss_8761_8782_split[1]), &(SplitJoin94_CombineDFT_Fiss_8761_8782_join[1]));
	ENDFOR
}

void CombineDFT_8713() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin94_CombineDFT_Fiss_8761_8782_split[2]), &(SplitJoin94_CombineDFT_Fiss_8761_8782_join[2]));
	ENDFOR
}

void CombineDFT_8714() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin94_CombineDFT_Fiss_8761_8782_split[3]), &(SplitJoin94_CombineDFT_Fiss_8761_8782_join[3]));
	ENDFOR
}

void CombineDFT_8715() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin94_CombineDFT_Fiss_8761_8782_split[4]), &(SplitJoin94_CombineDFT_Fiss_8761_8782_join[4]));
	ENDFOR
}

void CombineDFT_8716() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin94_CombineDFT_Fiss_8761_8782_split[5]), &(SplitJoin94_CombineDFT_Fiss_8761_8782_join[5]));
	ENDFOR
}

void CombineDFT_8717() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin94_CombineDFT_Fiss_8761_8782_split[6]), &(SplitJoin94_CombineDFT_Fiss_8761_8782_join[6]));
	ENDFOR
}

void CombineDFT_8718() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin94_CombineDFT_Fiss_8761_8782_split[7]), &(SplitJoin94_CombineDFT_Fiss_8761_8782_join[7]));
	ENDFOR
}

void CombineDFT_8719() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin94_CombineDFT_Fiss_8761_8782_split[8]), &(SplitJoin94_CombineDFT_Fiss_8761_8782_join[8]));
	ENDFOR
}

void CombineDFT_8720() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin94_CombineDFT_Fiss_8761_8782_split[9]), &(SplitJoin94_CombineDFT_Fiss_8761_8782_join[9]));
	ENDFOR
}

void CombineDFT_8721() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin94_CombineDFT_Fiss_8761_8782_split[10]), &(SplitJoin94_CombineDFT_Fiss_8761_8782_join[10]));
	ENDFOR
}

void CombineDFT_8722() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin94_CombineDFT_Fiss_8761_8782_split[11]), &(SplitJoin94_CombineDFT_Fiss_8761_8782_join[11]));
	ENDFOR
}

void CombineDFT_8723() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin94_CombineDFT_Fiss_8761_8782_split[12]), &(SplitJoin94_CombineDFT_Fiss_8761_8782_join[12]));
	ENDFOR
}

void CombineDFT_8724() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		CombineDFT(&(SplitJoin94_CombineDFT_Fiss_8761_8782_split[13]), &(SplitJoin94_CombineDFT_Fiss_8761_8782_join[13]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8709() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 14, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin94_CombineDFT_Fiss_8761_8782_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8694WEIGHTED_ROUND_ROBIN_Splitter_8709));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8710() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 14, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8710WEIGHTED_ROUND_ROBIN_Splitter_8725, pop_float(&SplitJoin94_CombineDFT_Fiss_8761_8782_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_8727() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin96_CombineDFT_Fiss_8762_8783_split[0]), &(SplitJoin96_CombineDFT_Fiss_8762_8783_join[0]));
	ENDFOR
}

void CombineDFT_8728() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin96_CombineDFT_Fiss_8762_8783_split[1]), &(SplitJoin96_CombineDFT_Fiss_8762_8783_join[1]));
	ENDFOR
}

void CombineDFT_8729() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin96_CombineDFT_Fiss_8762_8783_split[2]), &(SplitJoin96_CombineDFT_Fiss_8762_8783_join[2]));
	ENDFOR
}

void CombineDFT_8730() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin96_CombineDFT_Fiss_8762_8783_split[3]), &(SplitJoin96_CombineDFT_Fiss_8762_8783_join[3]));
	ENDFOR
}

void CombineDFT_8731() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin96_CombineDFT_Fiss_8762_8783_split[4]), &(SplitJoin96_CombineDFT_Fiss_8762_8783_join[4]));
	ENDFOR
}

void CombineDFT_8732() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin96_CombineDFT_Fiss_8762_8783_split[5]), &(SplitJoin96_CombineDFT_Fiss_8762_8783_join[5]));
	ENDFOR
}

void CombineDFT_8733() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin96_CombineDFT_Fiss_8762_8783_split[6]), &(SplitJoin96_CombineDFT_Fiss_8762_8783_join[6]));
	ENDFOR
}

void CombineDFT_8734() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin96_CombineDFT_Fiss_8762_8783_split[7]), &(SplitJoin96_CombineDFT_Fiss_8762_8783_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8725() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&SplitJoin96_CombineDFT_Fiss_8762_8783_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8710WEIGHTED_ROUND_ROBIN_Splitter_8725));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8726() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 16, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8726WEIGHTED_ROUND_ROBIN_Splitter_8735, pop_float(&SplitJoin96_CombineDFT_Fiss_8762_8783_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_8737() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin98_CombineDFT_Fiss_8763_8784_split[0]), &(SplitJoin98_CombineDFT_Fiss_8763_8784_join[0]));
	ENDFOR
}

void CombineDFT_8738() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin98_CombineDFT_Fiss_8763_8784_split[1]), &(SplitJoin98_CombineDFT_Fiss_8763_8784_join[1]));
	ENDFOR
}

void CombineDFT_8739() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin98_CombineDFT_Fiss_8763_8784_split[2]), &(SplitJoin98_CombineDFT_Fiss_8763_8784_join[2]));
	ENDFOR
}

void CombineDFT_8740() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin98_CombineDFT_Fiss_8763_8784_split[3]), &(SplitJoin98_CombineDFT_Fiss_8763_8784_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8735() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&SplitJoin98_CombineDFT_Fiss_8763_8784_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8726WEIGHTED_ROUND_ROBIN_Splitter_8735));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8736() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 32, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8736WEIGHTED_ROUND_ROBIN_Splitter_8741, pop_float(&SplitJoin98_CombineDFT_Fiss_8763_8784_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void CombineDFT_8743() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin100_CombineDFT_Fiss_8764_8785_split[0]), &(SplitJoin100_CombineDFT_Fiss_8764_8785_join[0]));
	ENDFOR
}

void CombineDFT_8744() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(SplitJoin100_CombineDFT_Fiss_8764_8785_split[1]), &(SplitJoin100_CombineDFT_Fiss_8764_8785_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8741() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin100_CombineDFT_Fiss_8764_8785_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8736WEIGHTED_ROUND_ROBIN_Splitter_8741));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&SplitJoin100_CombineDFT_Fiss_8764_8785_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8736WEIGHTED_ROUND_ROBIN_Splitter_8741));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8742() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8742CombineDFT_8555, pop_float(&SplitJoin100_CombineDFT_Fiss_8764_8785_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 64, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8742CombineDFT_8555, pop_float(&SplitJoin100_CombineDFT_Fiss_8764_8785_join[1]));
		ENDFOR
	ENDFOR
}}

void CombineDFT_8555() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		CombineDFT(&(WEIGHTED_ROUND_ROBIN_Joiner_8742CombineDFT_8555), &(SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_8523_8559_8746_8767_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8557() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_8523_8559_8746_8767_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8566WEIGHTED_ROUND_ROBIN_Splitter_8557));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_8523_8559_8746_8767_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8566WEIGHTED_ROUND_ROBIN_Splitter_8557));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8558() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8558FloatPrinter_8556, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_8523_8559_8746_8767_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 128, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8558FloatPrinter_8556, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_8523_8559_8746_8767_join[1]));
		ENDFOR
	ENDFOR
}}

void FloatPrinter(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void FloatPrinter_8556() {
	FOR(uint32_t, __iter_steady_, 0, <, 1792, __iter_steady_++)
		FloatPrinter(&(WEIGHTED_ROUND_ROBIN_Joiner_8558FloatPrinter_8556));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_8747_8768_join[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8622WEIGHTED_ROUND_ROBIN_Splitter_8637);
	FOR(int, __iter_init_1_, 0, <, 4, __iter_init_1_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_8748_8769_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_float(&SplitJoin84_FFTReorderSimple_Fiss_8756_8777_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 14, __iter_init_3_++)
		init_buffer_float(&SplitJoin92_CombineDFT_Fiss_8760_8781_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 2, __iter_init_4_++)
		init_buffer_float(&SplitJoin100_CombineDFT_Fiss_8764_8785_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 2, __iter_init_5_++)
		init_buffer_float(&SplitJoin84_FFTReorderSimple_Fiss_8756_8777_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_float(&SplitJoin88_FFTReorderSimple_Fiss_8758_8779_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 14, __iter_init_7_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_8751_8772_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 4, __iter_init_8_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_8754_8775_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 14, __iter_init_9_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_8750_8771_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 4, __iter_init_10_++)
		init_buffer_float(&SplitJoin86_FFTReorderSimple_Fiss_8757_8778_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 2, __iter_init_11_++)
		init_buffer_float(&SplitJoin4_FFTReorderSimple_Fiss_8747_8768_split[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8736WEIGHTED_ROUND_ROBIN_Splitter_8741);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8654CombineDFT_8544);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8668WEIGHTED_ROUND_ROBIN_Splitter_8677);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8648WEIGHTED_ROUND_ROBIN_Splitter_8653);
	FOR(int, __iter_init_12_, 0, <, 2, __iter_init_12_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_8745_8766_split[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 8, __iter_init_13_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_8749_8770_split[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_8523_8559_8746_8767_join[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 4, __iter_init_15_++)
		init_buffer_float(&SplitJoin6_FFTReorderSimple_Fiss_8748_8769_join[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 8, __iter_init_16_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_8753_8774_split[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 4, __iter_init_17_++)
		init_buffer_float(&SplitJoin18_CombineDFT_Fiss_8754_8775_join[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 14, __iter_init_18_++)
		init_buffer_float(&SplitJoin10_FFTReorderSimple_Fiss_8750_8771_join[__iter_init_18_]);
	ENDFOR
	init_buffer_float(&FFTReorderSimple_8534WEIGHTED_ROUND_ROBIN_Splitter_8569);
	FOR(int, __iter_init_19_, 0, <, 4, __iter_init_19_++)
		init_buffer_float(&SplitJoin98_CombineDFT_Fiss_8763_8784_split[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 8, __iter_init_20_++)
		init_buffer_float(&SplitJoin96_CombineDFT_Fiss_8762_8783_split[__iter_init_20_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8694WEIGHTED_ROUND_ROBIN_Splitter_8709);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8710WEIGHTED_ROUND_ROBIN_Splitter_8725);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8570WEIGHTED_ROUND_ROBIN_Splitter_8573);
	FOR(int, __iter_init_21_, 0, <, 4, __iter_init_21_++)
		init_buffer_float(&SplitJoin98_CombineDFT_Fiss_8763_8784_join[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 8, __iter_init_22_++)
		init_buffer_float(&SplitJoin16_CombineDFT_Fiss_8753_8774_join[__iter_init_22_]);
	ENDFOR
	FOR(int, __iter_init_23_, 0, <, 2, __iter_init_23_++)
		init_buffer_float(&SplitJoin100_CombineDFT_Fiss_8764_8785_split[__iter_init_23_]);
	ENDFOR
	FOR(int, __iter_init_24_, 0, <, 14, __iter_init_24_++)
		init_buffer_float(&SplitJoin90_FFTReorderSimple_Fiss_8759_8780_split[__iter_init_24_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8678WEIGHTED_ROUND_ROBIN_Splitter_8693);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8606WEIGHTED_ROUND_ROBIN_Splitter_8621);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8574WEIGHTED_ROUND_ROBIN_Splitter_8579);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8638WEIGHTED_ROUND_ROBIN_Splitter_8647);
	FOR(int, __iter_init_25_, 0, <, 8, __iter_init_25_++)
		init_buffer_float(&SplitJoin96_CombineDFT_Fiss_8762_8783_join[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 14, __iter_init_26_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_8752_8773_split[__iter_init_26_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8726WEIGHTED_ROUND_ROBIN_Splitter_8735);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8590WEIGHTED_ROUND_ROBIN_Splitter_8605);
	FOR(int, __iter_init_27_, 0, <, 4, __iter_init_27_++)
		init_buffer_float(&SplitJoin86_FFTReorderSimple_Fiss_8757_8778_join[__iter_init_27_]);
	ENDFOR
	FOR(int, __iter_init_28_, 0, <, 14, __iter_init_28_++)
		init_buffer_float(&SplitJoin94_CombineDFT_Fiss_8761_8782_split[__iter_init_28_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8742CombineDFT_8555);
	FOR(int, __iter_init_29_, 0, <, 2, __iter_init_29_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_8755_8776_split[__iter_init_29_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8662WEIGHTED_ROUND_ROBIN_Splitter_8667);
	FOR(int, __iter_init_30_, 0, <, 2, __iter_init_30_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_FFTKernel2_8523_8559_8746_8767_split[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 2, __iter_init_31_++)
		init_buffer_float(&SplitJoin20_CombineDFT_Fiss_8755_8776_join[__iter_init_31_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8658WEIGHTED_ROUND_ROBIN_Splitter_8661);
	FOR(int, __iter_init_32_, 0, <, 14, __iter_init_32_++)
		init_buffer_float(&SplitJoin14_CombineDFT_Fiss_8752_8773_join[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 14, __iter_init_33_++)
		init_buffer_float(&SplitJoin92_CombineDFT_Fiss_8760_8781_split[__iter_init_33_]);
	ENDFOR
	FOR(int, __iter_init_34_, 0, <, 14, __iter_init_34_++)
		init_buffer_float(&SplitJoin12_CombineDFT_Fiss_8751_8772_split[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 14, __iter_init_35_++)
		init_buffer_float(&SplitJoin90_FFTReorderSimple_Fiss_8759_8780_join[__iter_init_35_]);
	ENDFOR
	FOR(int, __iter_init_36_, 0, <, 2, __iter_init_36_++)
		init_buffer_float(&SplitJoin0_FFTTestSource_Fiss_8745_8766_join[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 14, __iter_init_37_++)
		init_buffer_float(&SplitJoin94_CombineDFT_Fiss_8761_8782_join[__iter_init_37_]);
	ENDFOR
	init_buffer_float(&FFTReorderSimple_8545WEIGHTED_ROUND_ROBIN_Splitter_8657);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8558FloatPrinter_8556);
	FOR(int, __iter_init_38_, 0, <, 8, __iter_init_38_++)
		init_buffer_float(&SplitJoin8_FFTReorderSimple_Fiss_8749_8770_join[__iter_init_38_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8566WEIGHTED_ROUND_ROBIN_Splitter_8557);
	FOR(int, __iter_init_39_, 0, <, 8, __iter_init_39_++)
		init_buffer_float(&SplitJoin88_FFTReorderSimple_Fiss_8758_8779_join[__iter_init_39_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8580WEIGHTED_ROUND_ROBIN_Splitter_8589);
// --- init: CombineDFT_8607
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_8607_s.w[i] = real ; 
		CombineDFT_8607_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8608
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_8608_s.w[i] = real ; 
		CombineDFT_8608_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8609
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_8609_s.w[i] = real ; 
		CombineDFT_8609_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8610
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_8610_s.w[i] = real ; 
		CombineDFT_8610_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8611
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_8611_s.w[i] = real ; 
		CombineDFT_8611_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8612
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_8612_s.w[i] = real ; 
		CombineDFT_8612_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8613
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_8613_s.w[i] = real ; 
		CombineDFT_8613_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8614
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_8614_s.w[i] = real ; 
		CombineDFT_8614_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8615
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_8615_s.w[i] = real ; 
		CombineDFT_8615_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8616
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_8616_s.w[i] = real ; 
		CombineDFT_8616_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8617
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_8617_s.w[i] = real ; 
		CombineDFT_8617_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8618
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_8618_s.w[i] = real ; 
		CombineDFT_8618_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8619
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_8619_s.w[i] = real ; 
		CombineDFT_8619_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8620
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_8620_s.w[i] = real ; 
		CombineDFT_8620_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8623
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_8623_s.w[i] = real ; 
		CombineDFT_8623_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8624
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_8624_s.w[i] = real ; 
		CombineDFT_8624_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8625
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_8625_s.w[i] = real ; 
		CombineDFT_8625_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8626
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_8626_s.w[i] = real ; 
		CombineDFT_8626_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8627
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_8627_s.w[i] = real ; 
		CombineDFT_8627_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8628
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_8628_s.w[i] = real ; 
		CombineDFT_8628_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8629
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_8629_s.w[i] = real ; 
		CombineDFT_8629_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8630
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_8630_s.w[i] = real ; 
		CombineDFT_8630_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8631
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_8631_s.w[i] = real ; 
		CombineDFT_8631_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8632
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_8632_s.w[i] = real ; 
		CombineDFT_8632_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8633
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_8633_s.w[i] = real ; 
		CombineDFT_8633_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8634
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_8634_s.w[i] = real ; 
		CombineDFT_8634_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8635
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_8635_s.w[i] = real ; 
		CombineDFT_8635_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8636
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_8636_s.w[i] = real ; 
		CombineDFT_8636_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8639
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_8639_s.w[i] = real ; 
		CombineDFT_8639_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8640
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_8640_s.w[i] = real ; 
		CombineDFT_8640_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8641
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_8641_s.w[i] = real ; 
		CombineDFT_8641_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8642
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_8642_s.w[i] = real ; 
		CombineDFT_8642_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8643
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_8643_s.w[i] = real ; 
		CombineDFT_8643_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8644
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_8644_s.w[i] = real ; 
		CombineDFT_8644_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8645
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_8645_s.w[i] = real ; 
		CombineDFT_8645_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8646
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_8646_s.w[i] = real ; 
		CombineDFT_8646_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8649
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_8649_s.w[i] = real ; 
		CombineDFT_8649_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8650
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_8650_s.w[i] = real ; 
		CombineDFT_8650_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8651
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_8651_s.w[i] = real ; 
		CombineDFT_8651_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8652
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_8652_s.w[i] = real ; 
		CombineDFT_8652_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8655
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_8655_s.w[i] = real ; 
		CombineDFT_8655_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8656
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_8656_s.w[i] = real ; 
		CombineDFT_8656_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8544
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_8544_s.w[i] = real ; 
		CombineDFT_8544_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8695
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_8695_s.w[i] = real ; 
		CombineDFT_8695_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8696
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_8696_s.w[i] = real ; 
		CombineDFT_8696_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8697
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_8697_s.w[i] = real ; 
		CombineDFT_8697_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8698
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_8698_s.w[i] = real ; 
		CombineDFT_8698_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8699
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_8699_s.w[i] = real ; 
		CombineDFT_8699_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8700
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_8700_s.w[i] = real ; 
		CombineDFT_8700_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8701
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_8701_s.w[i] = real ; 
		CombineDFT_8701_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8702
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_8702_s.w[i] = real ; 
		CombineDFT_8702_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8703
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_8703_s.w[i] = real ; 
		CombineDFT_8703_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8704
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_8704_s.w[i] = real ; 
		CombineDFT_8704_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8705
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_8705_s.w[i] = real ; 
		CombineDFT_8705_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8706
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_8706_s.w[i] = real ; 
		CombineDFT_8706_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8707
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_8707_s.w[i] = real ; 
		CombineDFT_8707_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8708
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 2, i = (i + 2)) {
		CombineDFT_8708_s.w[i] = real ; 
		CombineDFT_8708_s.w[(i + 1)] = imag ; 
		next_real = ((real * -1.0) - (imag * 8.742278E-8)) ; 
		next_imag = ((real * 8.742278E-8) + (imag * -1.0)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8711
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_8711_s.w[i] = real ; 
		CombineDFT_8711_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8712
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_8712_s.w[i] = real ; 
		CombineDFT_8712_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8713
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_8713_s.w[i] = real ; 
		CombineDFT_8713_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8714
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_8714_s.w[i] = real ; 
		CombineDFT_8714_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8715
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_8715_s.w[i] = real ; 
		CombineDFT_8715_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8716
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_8716_s.w[i] = real ; 
		CombineDFT_8716_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8717
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_8717_s.w[i] = real ; 
		CombineDFT_8717_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8718
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_8718_s.w[i] = real ; 
		CombineDFT_8718_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8719
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_8719_s.w[i] = real ; 
		CombineDFT_8719_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8720
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_8720_s.w[i] = real ; 
		CombineDFT_8720_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8721
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_8721_s.w[i] = real ; 
		CombineDFT_8721_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8722
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_8722_s.w[i] = real ; 
		CombineDFT_8722_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8723
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_8723_s.w[i] = real ; 
		CombineDFT_8723_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8724
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 4, i = (i + 2)) {
		CombineDFT_8724_s.w[i] = real ; 
		CombineDFT_8724_s.w[(i + 1)] = imag ; 
		next_real = ((real * -4.371139E-8) - (imag * -1.0)) ; 
		next_imag = ((real * -1.0) + (imag * -4.371139E-8)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8727
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_8727_s.w[i] = real ; 
		CombineDFT_8727_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8728
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_8728_s.w[i] = real ; 
		CombineDFT_8728_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8729
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_8729_s.w[i] = real ; 
		CombineDFT_8729_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8730
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_8730_s.w[i] = real ; 
		CombineDFT_8730_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8731
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_8731_s.w[i] = real ; 
		CombineDFT_8731_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8732
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_8732_s.w[i] = real ; 
		CombineDFT_8732_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8733
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_8733_s.w[i] = real ; 
		CombineDFT_8733_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8734
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 8, i = (i + 2)) {
		CombineDFT_8734_s.w[i] = real ; 
		CombineDFT_8734_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.70710677) - (imag * -0.70710677)) ; 
		next_imag = ((real * -0.70710677) + (imag * 0.70710677)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8737
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_8737_s.w[i] = real ; 
		CombineDFT_8737_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8738
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_8738_s.w[i] = real ; 
		CombineDFT_8738_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8739
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_8739_s.w[i] = real ; 
		CombineDFT_8739_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8740
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 16, i = (i + 2)) {
		CombineDFT_8740_s.w[i] = real ; 
		CombineDFT_8740_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9238795) - (imag * -0.38268346)) ; 
		next_imag = ((real * -0.38268346) + (imag * 0.9238795)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8743
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_8743_s.w[i] = real ; 
		CombineDFT_8743_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8744
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 32, i = (i + 2)) {
		CombineDFT_8744_s.w[i] = real ; 
		CombineDFT_8744_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.98078525) - (imag * -0.19509032)) ; 
		next_imag = ((real * -0.19509032) + (imag * 0.98078525)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
// --- init: CombineDFT_8555
	 {
	float real = 0.0;
	float imag = 0.0;
	float next_real = 0.0;
	float next_imag = 0.0;
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 0.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	real = 1.0 ; 
	imag = 0.0 ; 
	next_real = 0.0 ; 
	next_imag = 0.0 ; 
	FOR(int, i, 0,  < , 64, i = (i + 2)) {
		CombineDFT_8555_s.w[i] = real ; 
		CombineDFT_8555_s.w[(i + 1)] = imag ; 
		next_real = ((real * 0.9951847) - (imag * -0.09801714)) ; 
		next_imag = ((real * -0.09801714) + (imag * 0.9951847)) ; 
		real = next_real ; 
		imag = next_imag ; 
	}
	ENDFOR
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		WEIGHTED_ROUND_ROBIN_Splitter_8565();
			FFTTestSource_8567();
			FFTTestSource_8568();
		WEIGHTED_ROUND_ROBIN_Joiner_8566();
		WEIGHTED_ROUND_ROBIN_Splitter_8557();
			FFTReorderSimple_8534();
			WEIGHTED_ROUND_ROBIN_Splitter_8569();
				FFTReorderSimple_8571();
				FFTReorderSimple_8572();
			WEIGHTED_ROUND_ROBIN_Joiner_8570();
			WEIGHTED_ROUND_ROBIN_Splitter_8573();
				FFTReorderSimple_8575();
				FFTReorderSimple_8576();
				FFTReorderSimple_8577();
				FFTReorderSimple_8578();
			WEIGHTED_ROUND_ROBIN_Joiner_8574();
			WEIGHTED_ROUND_ROBIN_Splitter_8579();
				FFTReorderSimple_8581();
				FFTReorderSimple_8582();
				FFTReorderSimple_8583();
				FFTReorderSimple_8584();
				FFTReorderSimple_8585();
				FFTReorderSimple_8586();
				FFTReorderSimple_8587();
				FFTReorderSimple_8588();
			WEIGHTED_ROUND_ROBIN_Joiner_8580();
			WEIGHTED_ROUND_ROBIN_Splitter_8589();
				FFTReorderSimple_8591();
				FFTReorderSimple_8592();
				FFTReorderSimple_8593();
				FFTReorderSimple_8594();
				FFTReorderSimple_8595();
				FFTReorderSimple_8596();
				FFTReorderSimple_8597();
				FFTReorderSimple_8598();
				FFTReorderSimple_8599();
				FFTReorderSimple_8600();
				FFTReorderSimple_8601();
				FFTReorderSimple_8602();
				FFTReorderSimple_8603();
				FFTReorderSimple_8604();
			WEIGHTED_ROUND_ROBIN_Joiner_8590();
			WEIGHTED_ROUND_ROBIN_Splitter_8605();
				CombineDFT_8607();
				CombineDFT_8608();
				CombineDFT_8609();
				CombineDFT_8610();
				CombineDFT_8611();
				CombineDFT_8612();
				CombineDFT_8613();
				CombineDFT_8614();
				CombineDFT_8615();
				CombineDFT_8616();
				CombineDFT_8617();
				CombineDFT_8618();
				CombineDFT_8619();
				CombineDFT_8620();
			WEIGHTED_ROUND_ROBIN_Joiner_8606();
			WEIGHTED_ROUND_ROBIN_Splitter_8621();
				CombineDFT_8623();
				CombineDFT_8624();
				CombineDFT_8625();
				CombineDFT_8626();
				CombineDFT_8627();
				CombineDFT_8628();
				CombineDFT_8629();
				CombineDFT_8630();
				CombineDFT_8631();
				CombineDFT_8632();
				CombineDFT_8633();
				CombineDFT_8634();
				CombineDFT_8635();
				CombineDFT_8636();
			WEIGHTED_ROUND_ROBIN_Joiner_8622();
			WEIGHTED_ROUND_ROBIN_Splitter_8637();
				CombineDFT_8639();
				CombineDFT_8640();
				CombineDFT_8641();
				CombineDFT_8642();
				CombineDFT_8643();
				CombineDFT_8644();
				CombineDFT_8645();
				CombineDFT_8646();
			WEIGHTED_ROUND_ROBIN_Joiner_8638();
			WEIGHTED_ROUND_ROBIN_Splitter_8647();
				CombineDFT_8649();
				CombineDFT_8650();
				CombineDFT_8651();
				CombineDFT_8652();
			WEIGHTED_ROUND_ROBIN_Joiner_8648();
			WEIGHTED_ROUND_ROBIN_Splitter_8653();
				CombineDFT_8655();
				CombineDFT_8656();
			WEIGHTED_ROUND_ROBIN_Joiner_8654();
			CombineDFT_8544();
			FFTReorderSimple_8545();
			WEIGHTED_ROUND_ROBIN_Splitter_8657();
				FFTReorderSimple_8659();
				FFTReorderSimple_8660();
			WEIGHTED_ROUND_ROBIN_Joiner_8658();
			WEIGHTED_ROUND_ROBIN_Splitter_8661();
				FFTReorderSimple_8663();
				FFTReorderSimple_8664();
				FFTReorderSimple_8665();
				FFTReorderSimple_8666();
			WEIGHTED_ROUND_ROBIN_Joiner_8662();
			WEIGHTED_ROUND_ROBIN_Splitter_8667();
				FFTReorderSimple_8669();
				FFTReorderSimple_8670();
				FFTReorderSimple_8671();
				FFTReorderSimple_8672();
				FFTReorderSimple_8673();
				FFTReorderSimple_8674();
				FFTReorderSimple_8675();
				FFTReorderSimple_8676();
			WEIGHTED_ROUND_ROBIN_Joiner_8668();
			WEIGHTED_ROUND_ROBIN_Splitter_8677();
				FFTReorderSimple_8679();
				FFTReorderSimple_8680();
				FFTReorderSimple_8681();
				FFTReorderSimple_8682();
				FFTReorderSimple_8683();
				FFTReorderSimple_8684();
				FFTReorderSimple_8685();
				FFTReorderSimple_8686();
				FFTReorderSimple_8687();
				FFTReorderSimple_8688();
				FFTReorderSimple_8689();
				FFTReorderSimple_8690();
				FFTReorderSimple_8691();
				FFTReorderSimple_8692();
			WEIGHTED_ROUND_ROBIN_Joiner_8678();
			WEIGHTED_ROUND_ROBIN_Splitter_8693();
				CombineDFT_8695();
				CombineDFT_8696();
				CombineDFT_8697();
				CombineDFT_8698();
				CombineDFT_8699();
				CombineDFT_8700();
				CombineDFT_8701();
				CombineDFT_8702();
				CombineDFT_8703();
				CombineDFT_8704();
				CombineDFT_8705();
				CombineDFT_8706();
				CombineDFT_8707();
				CombineDFT_8708();
			WEIGHTED_ROUND_ROBIN_Joiner_8694();
			WEIGHTED_ROUND_ROBIN_Splitter_8709();
				CombineDFT_8711();
				CombineDFT_8712();
				CombineDFT_8713();
				CombineDFT_8714();
				CombineDFT_8715();
				CombineDFT_8716();
				CombineDFT_8717();
				CombineDFT_8718();
				CombineDFT_8719();
				CombineDFT_8720();
				CombineDFT_8721();
				CombineDFT_8722();
				CombineDFT_8723();
				CombineDFT_8724();
			WEIGHTED_ROUND_ROBIN_Joiner_8710();
			WEIGHTED_ROUND_ROBIN_Splitter_8725();
				CombineDFT_8727();
				CombineDFT_8728();
				CombineDFT_8729();
				CombineDFT_8730();
				CombineDFT_8731();
				CombineDFT_8732();
				CombineDFT_8733();
				CombineDFT_8734();
			WEIGHTED_ROUND_ROBIN_Joiner_8726();
			WEIGHTED_ROUND_ROBIN_Splitter_8735();
				CombineDFT_8737();
				CombineDFT_8738();
				CombineDFT_8739();
				CombineDFT_8740();
			WEIGHTED_ROUND_ROBIN_Joiner_8736();
			WEIGHTED_ROUND_ROBIN_Splitter_8741();
				CombineDFT_8743();
				CombineDFT_8744();
			WEIGHTED_ROUND_ROBIN_Joiner_8742();
			CombineDFT_8555();
		WEIGHTED_ROUND_ROBIN_Joiner_8558();
		FloatPrinter_8556();
	ENDFOR
	return EXIT_SUCCESS;
}
