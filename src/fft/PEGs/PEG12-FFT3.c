#include "PEG12-FFT3.h"

buffer_float_t SplitJoin8_Butterfly_Fiss_4946_4968_split[4];
buffer_float_t Pre_CollapsedDataParallel_1_4732WEIGHTED_ROUND_ROBIN_Splitter_4911;
buffer_float_t SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_Hier_4947_4974_split[2];
buffer_float_t Post_CollapsedDataParallel_2_4712WEIGHTED_ROUND_ROBIN_Splitter_4755;
buffer_float_t Pre_CollapsedDataParallel_1_4738WEIGHTED_ROUND_ROBIN_Splitter_4919;
buffer_float_t SplitJoin47_Butterfly_Fiss_4954_4979_join[2];
buffer_float_t SplitJoin14_Butterfly_Fiss_4948_4976_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4920Post_CollapsedDataParallel_2_4739;
buffer_float_t SplitJoin53_Butterfly_Fiss_4955_4981_split[2];
buffer_float_t SplitJoin65_Butterfly_Fiss_4958_4984_split[2];
buffer_float_t SplitJoin65_Butterfly_Fiss_4958_4984_join[2];
buffer_float_t Pre_CollapsedDataParallel_1_4720WEIGHTED_ROUND_ROBIN_Splitter_4877;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4890Post_CollapsedDataParallel_2_4718;
buffer_float_t SplitJoin43_Butterfly_Fiss_4953_4978_split[2];
buffer_float_t SplitJoin57_Butterfly_Fiss_4956_4982_join[2];
buffer_float_t SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_child0_4833_4975_split[4];
buffer_float_t FloatSource_4410Pre_CollapsedDataParallel_1_4711;
buffer_float_t SplitJoin0_Butterfly_Fiss_4943_4964_split[12];
buffer_float_t Pre_CollapsedDataParallel_1_4729WEIGHTED_ROUND_ROBIN_Splitter_4905;
buffer_float_t SplitJoin85_Butterfly_Fiss_4960_4970_join[8];
buffer_float_t SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child0_4950_4986_join[8];
buffer_float_t SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_Hier_4947_4974_join[2];
buffer_float_t Pre_CollapsedDataParallel_1_4744WEIGHTED_ROUND_ROBIN_Splitter_4927;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4854Post_CollapsedDataParallel_2_4712;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4912Post_CollapsedDataParallel_2_4733;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4916Post_CollapsedDataParallel_2_4736;
buffer_float_t Pre_CollapsedDataParallel_1_4747WEIGHTED_ROUND_ROBIN_Splitter_4931;
buffer_float_t SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_child1_4809_4971_join[2];
buffer_float_t Pre_CollapsedDataParallel_1_4717WEIGHTED_ROUND_ROBIN_Splitter_4889;
buffer_float_t SplitJoin4_Butterfly_Fiss_4945_4966_split[8];
buffer_float_t SplitJoin95_Butterfly_Fiss_4962_4973_split[4];
buffer_float_t Pre_CollapsedDataParallel_1_4711WEIGHTED_ROUND_ROBIN_Splitter_4853;
buffer_float_t SplitJoin43_Butterfly_Fiss_4953_4978_join[2];
buffer_float_t SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_child1_4809_4971_split[2];
buffer_float_t Pre_CollapsedDataParallel_1_4750WEIGHTED_ROUND_ROBIN_Splitter_4935;
buffer_float_t SplitJoin57_Butterfly_Fiss_4956_4982_split[2];
buffer_float_t SplitJoin61_Butterfly_Fiss_4957_4983_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4839WEIGHTED_ROUND_ROBIN_Splitter_4840;
buffer_float_t Pre_CollapsedDataParallel_1_4735WEIGHTED_ROUND_ROBIN_Splitter_4915;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4940Post_CollapsedDataParallel_2_4754;
buffer_float_t SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_child1_4834_4980_join[4];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_4368_4763_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_Hier_4944_4965_join[2];
buffer_float_t Post_CollapsedDataParallel_2_4715WEIGHTED_ROUND_ROBIN_Splitter_4835;
buffer_float_t SplitJoin85_Butterfly_Fiss_4960_4970_split[8];
buffer_float_t SplitJoin39_Butterfly_Fiss_4952_4977_join[2];
buffer_float_t Pre_CollapsedDataParallel_1_4714WEIGHTED_ROUND_ROBIN_Splitter_4867;
buffer_float_t SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_child0_4804_4967_join[2];
buffer_float_t SplitJoin53_Butterfly_Fiss_4955_4981_join[2];
buffer_float_t SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child0_4950_4986_split[8];
buffer_float_t SplitJoin95_Butterfly_Fiss_4962_4973_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4868Post_CollapsedDataParallel_2_4715;
buffer_float_t SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_child0_4833_4975_join[4];
buffer_float_t SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child1_4951_4987_split[8];
buffer_float_t Pre_CollapsedDataParallel_1_4726WEIGHTED_ROUND_ROBIN_Splitter_4899;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4851BitReverse_4491;
buffer_float_t SplitJoin14_Butterfly_Fiss_4948_4976_split[2];
buffer_float_t SplitJoin47_Butterfly_Fiss_4954_4979_split[2];
buffer_float_t SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child1_4951_4987_join[8];
buffer_float_t SplitJoin89_Butterfly_Fiss_4961_4972_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4936Post_CollapsedDataParallel_2_4751;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4884Post_CollapsedDataParallel_2_4724;
buffer_float_t SplitJoin72_Butterfly_Fiss_4959_4969_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4924Post_CollapsedDataParallel_2_4742;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4928Post_CollapsedDataParallel_2_4745;
buffer_float_t SplitJoin89_Butterfly_Fiss_4961_4972_split[4];
buffer_float_t SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_Hier_4949_4985_split[2];
buffer_float_t SplitJoin8_Butterfly_Fiss_4946_4968_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4900Post_CollapsedDataParallel_2_4727;
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_4368_4763_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_Hier_4944_4965_split[2];
buffer_float_t SplitJoin0_Butterfly_Fiss_4943_4964_join[12];
buffer_float_t Pre_CollapsedDataParallel_1_4741WEIGHTED_ROUND_ROBIN_Splitter_4923;
buffer_float_t SplitJoin72_Butterfly_Fiss_4959_4969_split[4];
buffer_float_t Post_CollapsedDataParallel_2_4718WEIGHTED_ROUND_ROBIN_Splitter_4837;
buffer_float_t SplitJoin39_Butterfly_Fiss_4952_4977_split[2];
buffer_float_t SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_child1_4834_4980_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4906Post_CollapsedDataParallel_2_4730;
buffer_float_t BitReverse_4491FloatPrinter_4492;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4932Post_CollapsedDataParallel_2_4748;
buffer_float_t SplitJoin61_Butterfly_Fiss_4957_4983_split[2];
buffer_float_t SplitJoin4_Butterfly_Fiss_4945_4966_join[8];
buffer_float_t SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_Hier_4949_4985_join[2];
buffer_float_t SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_child0_4804_4967_split[2];
buffer_float_t Pre_CollapsedDataParallel_1_4753WEIGHTED_ROUND_ROBIN_Splitter_4939;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4878Post_CollapsedDataParallel_2_4721;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4845WEIGHTED_ROUND_ROBIN_Splitter_4846;
buffer_float_t Pre_CollapsedDataParallel_1_4723WEIGHTED_ROUND_ROBIN_Splitter_4883;


FloatSource_4410_t FloatSource_4410_s;

void FloatSource(buffer_float_t *chanout) {
		push_float(&(*chanout), FloatSource_4410_s.A_re[FloatSource_4410_s.idx]) ; 
		push_float(&(*chanout), FloatSource_4410_s.A_im[FloatSource_4410_s.idx]) ; 
		FloatSource_4410_s.idx++ ; 
		if((FloatSource_4410_s.idx >= 32)) {
			FloatSource_4410_s.idx = 0 ; 
		}
	}


void FloatSource_4410() {
	FOR(uint32_t, __iter_steady_, 0, <, 96, __iter_steady_++)
		FloatSource(&(FloatSource_4410Pre_CollapsedDataParallel_1_4711));
	ENDFOR
}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
		int partialSum_k = 0;
 {
		FOR(int, _k, 0,  < , 16, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k;
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 32) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_4711() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(FloatSource_4410Pre_CollapsedDataParallel_1_4711), &(Pre_CollapsedDataParallel_1_4711WEIGHTED_ROUND_ROBIN_Splitter_4853));
	ENDFOR
}

void Butterfly(buffer_float_t *chanin, buffer_float_t *chanout) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&(*chanin)) ; 
		u_im = pop_float(&(*chanin)) ; 
		t_re = pop_float(&(*chanin)) ; 
		t_im = pop_float(&(*chanin)) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&(*chanout), u_re) ; 
		push_float(&(*chanout), u_im) ; 
		push_float(&(*chanout), t_re) ; 
		push_float(&(*chanout), t_im) ; 
	}


void Butterfly_4855() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_4943_4964_split[0]), &(SplitJoin0_Butterfly_Fiss_4943_4964_join[0]));
	ENDFOR
}

void Butterfly_4856() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_4943_4964_split[1]), &(SplitJoin0_Butterfly_Fiss_4943_4964_join[1]));
	ENDFOR
}

void Butterfly_4857() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_4943_4964_split[2]), &(SplitJoin0_Butterfly_Fiss_4943_4964_join[2]));
	ENDFOR
}

void Butterfly_4858() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_4943_4964_split[3]), &(SplitJoin0_Butterfly_Fiss_4943_4964_join[3]));
	ENDFOR
}

void Butterfly_4859() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_4943_4964_split[4]), &(SplitJoin0_Butterfly_Fiss_4943_4964_join[4]));
	ENDFOR
}

void Butterfly_4860() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_4943_4964_split[5]), &(SplitJoin0_Butterfly_Fiss_4943_4964_join[5]));
	ENDFOR
}

void Butterfly_4861() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_4943_4964_split[6]), &(SplitJoin0_Butterfly_Fiss_4943_4964_join[6]));
	ENDFOR
}

void Butterfly_4862() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_4943_4964_split[7]), &(SplitJoin0_Butterfly_Fiss_4943_4964_join[7]));
	ENDFOR
}

void Butterfly_4863() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_4943_4964_split[8]), &(SplitJoin0_Butterfly_Fiss_4943_4964_join[8]));
	ENDFOR
}

void Butterfly_4864() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_4943_4964_split[9]), &(SplitJoin0_Butterfly_Fiss_4943_4964_join[9]));
	ENDFOR
}

void Butterfly_4865() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_4943_4964_split[10]), &(SplitJoin0_Butterfly_Fiss_4943_4964_join[10]));
	ENDFOR
}

void Butterfly_4866() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_4943_4964_split[11]), &(SplitJoin0_Butterfly_Fiss_4943_4964_join[11]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4853() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 12, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin0_Butterfly_Fiss_4943_4964_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_4711WEIGHTED_ROUND_ROBIN_Splitter_4853));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4854() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 12, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4854Post_CollapsedDataParallel_2_4712, pop_float(&SplitJoin0_Butterfly_Fiss_4943_4964_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
		int kTimesWeights_i = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 16, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&(*chanout), peek_float(&(*chanin), (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_4712() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4854Post_CollapsedDataParallel_2_4712), &(Post_CollapsedDataParallel_2_4712WEIGHTED_ROUND_ROBIN_Splitter_4755));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_4714() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_4368_4763_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_Hier_4944_4965_split[0]), &(Pre_CollapsedDataParallel_1_4714WEIGHTED_ROUND_ROBIN_Splitter_4867));
	ENDFOR
}

void Butterfly_4869() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_4945_4966_split[0]), &(SplitJoin4_Butterfly_Fiss_4945_4966_join[0]));
	ENDFOR
}

void Butterfly_4870() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_4945_4966_split[1]), &(SplitJoin4_Butterfly_Fiss_4945_4966_join[1]));
	ENDFOR
}

void Butterfly_4871() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_4945_4966_split[2]), &(SplitJoin4_Butterfly_Fiss_4945_4966_join[2]));
	ENDFOR
}

void Butterfly_4872() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_4945_4966_split[3]), &(SplitJoin4_Butterfly_Fiss_4945_4966_join[3]));
	ENDFOR
}

void Butterfly_4873() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_4945_4966_split[4]), &(SplitJoin4_Butterfly_Fiss_4945_4966_join[4]));
	ENDFOR
}

void Butterfly_4874() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_4945_4966_split[5]), &(SplitJoin4_Butterfly_Fiss_4945_4966_join[5]));
	ENDFOR
}

void Butterfly_4875() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_4945_4966_split[6]), &(SplitJoin4_Butterfly_Fiss_4945_4966_join[6]));
	ENDFOR
}

void Butterfly_4876() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_4945_4966_split[7]), &(SplitJoin4_Butterfly_Fiss_4945_4966_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4867() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin4_Butterfly_Fiss_4945_4966_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_4714WEIGHTED_ROUND_ROBIN_Splitter_4867));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4868() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4868Post_CollapsedDataParallel_2_4715, pop_float(&SplitJoin4_Butterfly_Fiss_4945_4966_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_4715() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4868Post_CollapsedDataParallel_2_4715), &(Post_CollapsedDataParallel_2_4715WEIGHTED_ROUND_ROBIN_Splitter_4835));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_4720() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_child0_4804_4967_split[0]), &(Pre_CollapsedDataParallel_1_4720WEIGHTED_ROUND_ROBIN_Splitter_4877));
	ENDFOR
}

void Butterfly_4879() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin8_Butterfly_Fiss_4946_4968_split[0]), &(SplitJoin8_Butterfly_Fiss_4946_4968_join[0]));
	ENDFOR
}

void Butterfly_4880() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin8_Butterfly_Fiss_4946_4968_split[1]), &(SplitJoin8_Butterfly_Fiss_4946_4968_join[1]));
	ENDFOR
}

void Butterfly_4881() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin8_Butterfly_Fiss_4946_4968_split[2]), &(SplitJoin8_Butterfly_Fiss_4946_4968_join[2]));
	ENDFOR
}

void Butterfly_4882() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin8_Butterfly_Fiss_4946_4968_split[3]), &(SplitJoin8_Butterfly_Fiss_4946_4968_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4877() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin8_Butterfly_Fiss_4946_4968_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_4720WEIGHTED_ROUND_ROBIN_Splitter_4877));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4878() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4878Post_CollapsedDataParallel_2_4721, pop_float(&SplitJoin8_Butterfly_Fiss_4946_4968_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_4721() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4878Post_CollapsedDataParallel_2_4721), &(SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_child0_4804_4967_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_4723() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_child0_4804_4967_split[1]), &(Pre_CollapsedDataParallel_1_4723WEIGHTED_ROUND_ROBIN_Splitter_4883));
	ENDFOR
}

void Butterfly_4885() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin72_Butterfly_Fiss_4959_4969_split[0]), &(SplitJoin72_Butterfly_Fiss_4959_4969_join[0]));
	ENDFOR
}

void Butterfly_4886() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin72_Butterfly_Fiss_4959_4969_split[1]), &(SplitJoin72_Butterfly_Fiss_4959_4969_join[1]));
	ENDFOR
}

void Butterfly_4887() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin72_Butterfly_Fiss_4959_4969_split[2]), &(SplitJoin72_Butterfly_Fiss_4959_4969_join[2]));
	ENDFOR
}

void Butterfly_4888() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin72_Butterfly_Fiss_4959_4969_split[3]), &(SplitJoin72_Butterfly_Fiss_4959_4969_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4883() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin72_Butterfly_Fiss_4959_4969_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_4723WEIGHTED_ROUND_ROBIN_Splitter_4883));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4884() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4884Post_CollapsedDataParallel_2_4724, pop_float(&SplitJoin72_Butterfly_Fiss_4959_4969_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_4724() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4884Post_CollapsedDataParallel_2_4724), &(SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_child0_4804_4967_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4835() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_child0_4804_4967_split[0], pop_float(&Post_CollapsedDataParallel_2_4715WEIGHTED_ROUND_ROBIN_Splitter_4835));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_child0_4804_4967_split[1], pop_float(&Post_CollapsedDataParallel_2_4715WEIGHTED_ROUND_ROBIN_Splitter_4835));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4836() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_4368_4763_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_Hier_4944_4965_join[0], pop_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_child0_4804_4967_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_4368_4763_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_Hier_4944_4965_join[0], pop_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_child0_4804_4967_join[1]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_4717() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_4368_4763_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_Hier_4944_4965_split[1]), &(Pre_CollapsedDataParallel_1_4717WEIGHTED_ROUND_ROBIN_Splitter_4889));
	ENDFOR
}

void Butterfly_4891() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_4960_4970_split[0]), &(SplitJoin85_Butterfly_Fiss_4960_4970_join[0]));
	ENDFOR
}

void Butterfly_4892() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_4960_4970_split[1]), &(SplitJoin85_Butterfly_Fiss_4960_4970_join[1]));
	ENDFOR
}

void Butterfly_4893() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_4960_4970_split[2]), &(SplitJoin85_Butterfly_Fiss_4960_4970_join[2]));
	ENDFOR
}

void Butterfly_4894() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_4960_4970_split[3]), &(SplitJoin85_Butterfly_Fiss_4960_4970_join[3]));
	ENDFOR
}

void Butterfly_4895() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_4960_4970_split[4]), &(SplitJoin85_Butterfly_Fiss_4960_4970_join[4]));
	ENDFOR
}

void Butterfly_4896() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_4960_4970_split[5]), &(SplitJoin85_Butterfly_Fiss_4960_4970_join[5]));
	ENDFOR
}

void Butterfly_4897() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_4960_4970_split[6]), &(SplitJoin85_Butterfly_Fiss_4960_4970_join[6]));
	ENDFOR
}

void Butterfly_4898() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin85_Butterfly_Fiss_4960_4970_split[7]), &(SplitJoin85_Butterfly_Fiss_4960_4970_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4889() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin85_Butterfly_Fiss_4960_4970_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_4717WEIGHTED_ROUND_ROBIN_Splitter_4889));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4890() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4890Post_CollapsedDataParallel_2_4718, pop_float(&SplitJoin85_Butterfly_Fiss_4960_4970_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_4718() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4890Post_CollapsedDataParallel_2_4718), &(Post_CollapsedDataParallel_2_4718WEIGHTED_ROUND_ROBIN_Splitter_4837));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_4726() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_child1_4809_4971_split[0]), &(Pre_CollapsedDataParallel_1_4726WEIGHTED_ROUND_ROBIN_Splitter_4899));
	ENDFOR
}

void Butterfly_4901() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin89_Butterfly_Fiss_4961_4972_split[0]), &(SplitJoin89_Butterfly_Fiss_4961_4972_join[0]));
	ENDFOR
}

void Butterfly_4902() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin89_Butterfly_Fiss_4961_4972_split[1]), &(SplitJoin89_Butterfly_Fiss_4961_4972_join[1]));
	ENDFOR
}

void Butterfly_4903() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin89_Butterfly_Fiss_4961_4972_split[2]), &(SplitJoin89_Butterfly_Fiss_4961_4972_join[2]));
	ENDFOR
}

void Butterfly_4904() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin89_Butterfly_Fiss_4961_4972_split[3]), &(SplitJoin89_Butterfly_Fiss_4961_4972_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4899() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin89_Butterfly_Fiss_4961_4972_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_4726WEIGHTED_ROUND_ROBIN_Splitter_4899));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4900() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4900Post_CollapsedDataParallel_2_4727, pop_float(&SplitJoin89_Butterfly_Fiss_4961_4972_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_4727() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4900Post_CollapsedDataParallel_2_4727), &(SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_child1_4809_4971_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_4729() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_child1_4809_4971_split[1]), &(Pre_CollapsedDataParallel_1_4729WEIGHTED_ROUND_ROBIN_Splitter_4905));
	ENDFOR
}

void Butterfly_4907() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin95_Butterfly_Fiss_4962_4973_split[0]), &(SplitJoin95_Butterfly_Fiss_4962_4973_join[0]));
	ENDFOR
}

void Butterfly_4908() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin95_Butterfly_Fiss_4962_4973_split[1]), &(SplitJoin95_Butterfly_Fiss_4962_4973_join[1]));
	ENDFOR
}

void Butterfly_4909() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin95_Butterfly_Fiss_4962_4973_split[2]), &(SplitJoin95_Butterfly_Fiss_4962_4973_join[2]));
	ENDFOR
}

void Butterfly_4910() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin95_Butterfly_Fiss_4962_4973_split[3]), &(SplitJoin95_Butterfly_Fiss_4962_4973_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4905() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin95_Butterfly_Fiss_4962_4973_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_4729WEIGHTED_ROUND_ROBIN_Splitter_4905));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4906() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4906Post_CollapsedDataParallel_2_4730, pop_float(&SplitJoin95_Butterfly_Fiss_4962_4973_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_4730() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4906Post_CollapsedDataParallel_2_4730), &(SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_child1_4809_4971_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4837() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_child1_4809_4971_split[0], pop_float(&Post_CollapsedDataParallel_2_4718WEIGHTED_ROUND_ROBIN_Splitter_4837));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_child1_4809_4971_split[1], pop_float(&Post_CollapsedDataParallel_2_4718WEIGHTED_ROUND_ROBIN_Splitter_4837));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4838() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_4368_4763_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_Hier_4944_4965_join[1], pop_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_child1_4809_4971_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_4368_4763_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_Hier_4944_4965_join[1], pop_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_child1_4809_4971_join[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_4755() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_4368_4763_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_Hier_4944_4965_split[0], pop_float(&Post_CollapsedDataParallel_2_4712WEIGHTED_ROUND_ROBIN_Splitter_4755));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_4368_4763_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_Hier_4944_4965_split[1], pop_float(&Post_CollapsedDataParallel_2_4712WEIGHTED_ROUND_ROBIN_Splitter_4755));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4839() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4839WEIGHTED_ROUND_ROBIN_Splitter_4840, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_4368_4763_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_Hier_4944_4965_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4839WEIGHTED_ROUND_ROBIN_Splitter_4840, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_4368_4763_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_Hier_4944_4965_join[1]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_4732() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_child0_4833_4975_split[0]), &(Pre_CollapsedDataParallel_1_4732WEIGHTED_ROUND_ROBIN_Splitter_4911));
	ENDFOR
}

void Butterfly_4913() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin14_Butterfly_Fiss_4948_4976_split[0]), &(SplitJoin14_Butterfly_Fiss_4948_4976_join[0]));
	ENDFOR
}

void Butterfly_4914() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin14_Butterfly_Fiss_4948_4976_split[1]), &(SplitJoin14_Butterfly_Fiss_4948_4976_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4911() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin14_Butterfly_Fiss_4948_4976_split[0], pop_float(&Pre_CollapsedDataParallel_1_4732WEIGHTED_ROUND_ROBIN_Splitter_4911));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin14_Butterfly_Fiss_4948_4976_split[1], pop_float(&Pre_CollapsedDataParallel_1_4732WEIGHTED_ROUND_ROBIN_Splitter_4911));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4912() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4912Post_CollapsedDataParallel_2_4733, pop_float(&SplitJoin14_Butterfly_Fiss_4948_4976_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4912Post_CollapsedDataParallel_2_4733, pop_float(&SplitJoin14_Butterfly_Fiss_4948_4976_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_4733() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4912Post_CollapsedDataParallel_2_4733), &(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_child0_4833_4975_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_4735() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_child0_4833_4975_split[1]), &(Pre_CollapsedDataParallel_1_4735WEIGHTED_ROUND_ROBIN_Splitter_4915));
	ENDFOR
}

void Butterfly_4917() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin39_Butterfly_Fiss_4952_4977_split[0]), &(SplitJoin39_Butterfly_Fiss_4952_4977_join[0]));
	ENDFOR
}

void Butterfly_4918() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin39_Butterfly_Fiss_4952_4977_split[1]), &(SplitJoin39_Butterfly_Fiss_4952_4977_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4915() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin39_Butterfly_Fiss_4952_4977_split[0], pop_float(&Pre_CollapsedDataParallel_1_4735WEIGHTED_ROUND_ROBIN_Splitter_4915));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin39_Butterfly_Fiss_4952_4977_split[1], pop_float(&Pre_CollapsedDataParallel_1_4735WEIGHTED_ROUND_ROBIN_Splitter_4915));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4916() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4916Post_CollapsedDataParallel_2_4736, pop_float(&SplitJoin39_Butterfly_Fiss_4952_4977_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4916Post_CollapsedDataParallel_2_4736, pop_float(&SplitJoin39_Butterfly_Fiss_4952_4977_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_4736() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4916Post_CollapsedDataParallel_2_4736), &(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_child0_4833_4975_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_4738() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_child0_4833_4975_split[2]), &(Pre_CollapsedDataParallel_1_4738WEIGHTED_ROUND_ROBIN_Splitter_4919));
	ENDFOR
}

void Butterfly_4921() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin43_Butterfly_Fiss_4953_4978_split[0]), &(SplitJoin43_Butterfly_Fiss_4953_4978_join[0]));
	ENDFOR
}

void Butterfly_4922() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin43_Butterfly_Fiss_4953_4978_split[1]), &(SplitJoin43_Butterfly_Fiss_4953_4978_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4919() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin43_Butterfly_Fiss_4953_4978_split[0], pop_float(&Pre_CollapsedDataParallel_1_4738WEIGHTED_ROUND_ROBIN_Splitter_4919));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin43_Butterfly_Fiss_4953_4978_split[1], pop_float(&Pre_CollapsedDataParallel_1_4738WEIGHTED_ROUND_ROBIN_Splitter_4919));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4920() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4920Post_CollapsedDataParallel_2_4739, pop_float(&SplitJoin43_Butterfly_Fiss_4953_4978_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4920Post_CollapsedDataParallel_2_4739, pop_float(&SplitJoin43_Butterfly_Fiss_4953_4978_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_4739() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4920Post_CollapsedDataParallel_2_4739), &(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_child0_4833_4975_join[2]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_4741() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_child0_4833_4975_split[3]), &(Pre_CollapsedDataParallel_1_4741WEIGHTED_ROUND_ROBIN_Splitter_4923));
	ENDFOR
}

void Butterfly_4925() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin47_Butterfly_Fiss_4954_4979_split[0]), &(SplitJoin47_Butterfly_Fiss_4954_4979_join[0]));
	ENDFOR
}

void Butterfly_4926() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin47_Butterfly_Fiss_4954_4979_split[1]), &(SplitJoin47_Butterfly_Fiss_4954_4979_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4923() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin47_Butterfly_Fiss_4954_4979_split[0], pop_float(&Pre_CollapsedDataParallel_1_4741WEIGHTED_ROUND_ROBIN_Splitter_4923));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin47_Butterfly_Fiss_4954_4979_split[1], pop_float(&Pre_CollapsedDataParallel_1_4741WEIGHTED_ROUND_ROBIN_Splitter_4923));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4924() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4924Post_CollapsedDataParallel_2_4742, pop_float(&SplitJoin47_Butterfly_Fiss_4954_4979_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4924Post_CollapsedDataParallel_2_4742, pop_float(&SplitJoin47_Butterfly_Fiss_4954_4979_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_4742() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4924Post_CollapsedDataParallel_2_4742), &(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_child0_4833_4975_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4841() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_child0_4833_4975_split[__iter_dec_], pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_Hier_4947_4974_split[0]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4842() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_Hier_4947_4974_join[0], pop_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_child0_4833_4975_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_4744() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_child1_4834_4980_split[0]), &(Pre_CollapsedDataParallel_1_4744WEIGHTED_ROUND_ROBIN_Splitter_4927));
	ENDFOR
}

void Butterfly_4929() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin53_Butterfly_Fiss_4955_4981_split[0]), &(SplitJoin53_Butterfly_Fiss_4955_4981_join[0]));
	ENDFOR
}

void Butterfly_4930() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin53_Butterfly_Fiss_4955_4981_split[1]), &(SplitJoin53_Butterfly_Fiss_4955_4981_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4927() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin53_Butterfly_Fiss_4955_4981_split[0], pop_float(&Pre_CollapsedDataParallel_1_4744WEIGHTED_ROUND_ROBIN_Splitter_4927));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin53_Butterfly_Fiss_4955_4981_split[1], pop_float(&Pre_CollapsedDataParallel_1_4744WEIGHTED_ROUND_ROBIN_Splitter_4927));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4928() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4928Post_CollapsedDataParallel_2_4745, pop_float(&SplitJoin53_Butterfly_Fiss_4955_4981_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4928Post_CollapsedDataParallel_2_4745, pop_float(&SplitJoin53_Butterfly_Fiss_4955_4981_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_4745() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4928Post_CollapsedDataParallel_2_4745), &(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_child1_4834_4980_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_4747() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_child1_4834_4980_split[1]), &(Pre_CollapsedDataParallel_1_4747WEIGHTED_ROUND_ROBIN_Splitter_4931));
	ENDFOR
}

void Butterfly_4933() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin57_Butterfly_Fiss_4956_4982_split[0]), &(SplitJoin57_Butterfly_Fiss_4956_4982_join[0]));
	ENDFOR
}

void Butterfly_4934() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin57_Butterfly_Fiss_4956_4982_split[1]), &(SplitJoin57_Butterfly_Fiss_4956_4982_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4931() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin57_Butterfly_Fiss_4956_4982_split[0], pop_float(&Pre_CollapsedDataParallel_1_4747WEIGHTED_ROUND_ROBIN_Splitter_4931));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin57_Butterfly_Fiss_4956_4982_split[1], pop_float(&Pre_CollapsedDataParallel_1_4747WEIGHTED_ROUND_ROBIN_Splitter_4931));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4932() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4932Post_CollapsedDataParallel_2_4748, pop_float(&SplitJoin57_Butterfly_Fiss_4956_4982_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4932Post_CollapsedDataParallel_2_4748, pop_float(&SplitJoin57_Butterfly_Fiss_4956_4982_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_4748() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4932Post_CollapsedDataParallel_2_4748), &(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_child1_4834_4980_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_4750() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_child1_4834_4980_split[2]), &(Pre_CollapsedDataParallel_1_4750WEIGHTED_ROUND_ROBIN_Splitter_4935));
	ENDFOR
}

void Butterfly_4937() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin61_Butterfly_Fiss_4957_4983_split[0]), &(SplitJoin61_Butterfly_Fiss_4957_4983_join[0]));
	ENDFOR
}

void Butterfly_4938() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin61_Butterfly_Fiss_4957_4983_split[1]), &(SplitJoin61_Butterfly_Fiss_4957_4983_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4935() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin61_Butterfly_Fiss_4957_4983_split[0], pop_float(&Pre_CollapsedDataParallel_1_4750WEIGHTED_ROUND_ROBIN_Splitter_4935));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin61_Butterfly_Fiss_4957_4983_split[1], pop_float(&Pre_CollapsedDataParallel_1_4750WEIGHTED_ROUND_ROBIN_Splitter_4935));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4936() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4936Post_CollapsedDataParallel_2_4751, pop_float(&SplitJoin61_Butterfly_Fiss_4957_4983_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4936Post_CollapsedDataParallel_2_4751, pop_float(&SplitJoin61_Butterfly_Fiss_4957_4983_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_4751() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4936Post_CollapsedDataParallel_2_4751), &(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_child1_4834_4980_join[2]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_4753() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_child1_4834_4980_split[3]), &(Pre_CollapsedDataParallel_1_4753WEIGHTED_ROUND_ROBIN_Splitter_4939));
	ENDFOR
}

void Butterfly_4941() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin65_Butterfly_Fiss_4958_4984_split[0]), &(SplitJoin65_Butterfly_Fiss_4958_4984_join[0]));
	ENDFOR
}

void Butterfly_4942() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin65_Butterfly_Fiss_4958_4984_split[1]), &(SplitJoin65_Butterfly_Fiss_4958_4984_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4939() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin65_Butterfly_Fiss_4958_4984_split[0], pop_float(&Pre_CollapsedDataParallel_1_4753WEIGHTED_ROUND_ROBIN_Splitter_4939));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin65_Butterfly_Fiss_4958_4984_split[1], pop_float(&Pre_CollapsedDataParallel_1_4753WEIGHTED_ROUND_ROBIN_Splitter_4939));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4940() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4940Post_CollapsedDataParallel_2_4754, pop_float(&SplitJoin65_Butterfly_Fiss_4958_4984_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4940Post_CollapsedDataParallel_2_4754, pop_float(&SplitJoin65_Butterfly_Fiss_4958_4984_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_4754() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_4940Post_CollapsedDataParallel_2_4754), &(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_child1_4834_4980_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4843() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_child1_4834_4980_split[__iter_dec_], pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_Hier_4947_4974_split[1]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4844() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_Hier_4947_4974_join[1], pop_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_child1_4834_4980_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_4840() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_Hier_4947_4974_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4839WEIGHTED_ROUND_ROBIN_Splitter_4840));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_Hier_4947_4974_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4839WEIGHTED_ROUND_ROBIN_Splitter_4840));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4845() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4845WEIGHTED_ROUND_ROBIN_Splitter_4846, pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_Hier_4947_4974_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4845WEIGHTED_ROUND_ROBIN_Splitter_4846, pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_Hier_4947_4974_join[1]));
		ENDFOR
	ENDFOR
}}

void Butterfly_4475() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child0_4950_4986_split[0]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child0_4950_4986_join[0]));
	ENDFOR
}

void Butterfly_4476() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child0_4950_4986_split[1]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child0_4950_4986_join[1]));
	ENDFOR
}

void Butterfly_4477() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child0_4950_4986_split[2]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child0_4950_4986_join[2]));
	ENDFOR
}

void Butterfly_4478() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child0_4950_4986_split[3]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child0_4950_4986_join[3]));
	ENDFOR
}

void Butterfly_4479() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child0_4950_4986_split[4]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child0_4950_4986_join[4]));
	ENDFOR
}

void Butterfly_4480() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child0_4950_4986_split[5]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child0_4950_4986_join[5]));
	ENDFOR
}

void Butterfly_4481() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child0_4950_4986_split[6]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child0_4950_4986_join[6]));
	ENDFOR
}

void Butterfly_4482() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child0_4950_4986_split[7]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child0_4950_4986_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4847() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child0_4950_4986_split[__iter_dec_], pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_Hier_4949_4985_split[0]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4848() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_Hier_4949_4985_join[0], pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child0_4950_4986_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Butterfly_4483() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child1_4951_4987_split[0]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child1_4951_4987_join[0]));
	ENDFOR
}

void Butterfly_4484() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child1_4951_4987_split[1]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child1_4951_4987_join[1]));
	ENDFOR
}

void Butterfly_4485() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child1_4951_4987_split[2]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child1_4951_4987_join[2]));
	ENDFOR
}

void Butterfly_4486() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child1_4951_4987_split[3]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child1_4951_4987_join[3]));
	ENDFOR
}

void Butterfly_4487() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child1_4951_4987_split[4]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child1_4951_4987_join[4]));
	ENDFOR
}

void Butterfly_4488() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child1_4951_4987_split[5]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child1_4951_4987_join[5]));
	ENDFOR
}

void Butterfly_4489() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child1_4951_4987_split[6]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child1_4951_4987_join[6]));
	ENDFOR
}

void Butterfly_4490() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child1_4951_4987_split[7]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child1_4951_4987_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4849() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child1_4951_4987_split[__iter_dec_], pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_Hier_4949_4985_split[1]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4850() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_Hier_4949_4985_join[1], pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child1_4951_4987_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_4846() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_Hier_4949_4985_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4845WEIGHTED_ROUND_ROBIN_Splitter_4846));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_Hier_4949_4985_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4845WEIGHTED_ROUND_ROBIN_Splitter_4846));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4851() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4851BitReverse_4491, pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_Hier_4949_4985_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4851BitReverse_4491, pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_Hier_4949_4985_join[1]));
		ENDFOR
	ENDFOR
}}

int BitReverse_4491_bitrev(int inp, int numbits) {
	int rev = 0;
	FOR(int, i, 0,  < , numbits, i++) {
		rev = ((rev * 2) | (inp & 1)) ; 
		inp = (inp / 2) ; 
	}
	ENDFOR
	return rev;
}
void BitReverse(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, i__conflict__0, 0,  < , 32, i__conflict__0++) {
			int br = 0;
			br = BitReverse_4491_bitrev(i__conflict__0, 5) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (2 * br))) ; 
			push_float(&(*chanout), peek_float(&(*chanin), ((2 * br) + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&(*chanin)) ; 
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void BitReverse_4491() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		BitReverse(&(WEIGHTED_ROUND_ROBIN_Joiner_4851BitReverse_4491), &(BitReverse_4491FloatPrinter_4492));
	ENDFOR
}

void FloatPrinter(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void FloatPrinter_4492() {
	FOR(uint32_t, __iter_steady_, 0, <, 96, __iter_steady_++)
		FloatPrinter(&(BitReverse_4491FloatPrinter_4492));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 4, __iter_init_0_++)
		init_buffer_float(&SplitJoin8_Butterfly_Fiss_4946_4968_split[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_4732WEIGHTED_ROUND_ROBIN_Splitter_4911);
	FOR(int, __iter_init_1_, 0, <, 2, __iter_init_1_++)
		init_buffer_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_Hier_4947_4974_split[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_4712WEIGHTED_ROUND_ROBIN_Splitter_4755);
	init_buffer_float(&Pre_CollapsedDataParallel_1_4738WEIGHTED_ROUND_ROBIN_Splitter_4919);
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_float(&SplitJoin47_Butterfly_Fiss_4954_4979_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 2, __iter_init_3_++)
		init_buffer_float(&SplitJoin14_Butterfly_Fiss_4948_4976_join[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4920Post_CollapsedDataParallel_2_4739);
	FOR(int, __iter_init_4_, 0, <, 2, __iter_init_4_++)
		init_buffer_float(&SplitJoin53_Butterfly_Fiss_4955_4981_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 2, __iter_init_5_++)
		init_buffer_float(&SplitJoin65_Butterfly_Fiss_4958_4984_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 2, __iter_init_6_++)
		init_buffer_float(&SplitJoin65_Butterfly_Fiss_4958_4984_join[__iter_init_6_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_4720WEIGHTED_ROUND_ROBIN_Splitter_4877);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4890Post_CollapsedDataParallel_2_4718);
	FOR(int, __iter_init_7_, 0, <, 2, __iter_init_7_++)
		init_buffer_float(&SplitJoin43_Butterfly_Fiss_4953_4978_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_float(&SplitJoin57_Butterfly_Fiss_4956_4982_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 4, __iter_init_9_++)
		init_buffer_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_child0_4833_4975_split[__iter_init_9_]);
	ENDFOR
	init_buffer_float(&FloatSource_4410Pre_CollapsedDataParallel_1_4711);
	FOR(int, __iter_init_10_, 0, <, 12, __iter_init_10_++)
		init_buffer_float(&SplitJoin0_Butterfly_Fiss_4943_4964_split[__iter_init_10_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_4729WEIGHTED_ROUND_ROBIN_Splitter_4905);
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_float(&SplitJoin85_Butterfly_Fiss_4960_4970_join[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 8, __iter_init_12_++)
		init_buffer_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child0_4950_4986_join[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_Hier_4947_4974_join[__iter_init_13_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_4744WEIGHTED_ROUND_ROBIN_Splitter_4927);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4854Post_CollapsedDataParallel_2_4712);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4912Post_CollapsedDataParallel_2_4733);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4916Post_CollapsedDataParallel_2_4736);
	init_buffer_float(&Pre_CollapsedDataParallel_1_4747WEIGHTED_ROUND_ROBIN_Splitter_4931);
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_child1_4809_4971_join[__iter_init_14_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_4717WEIGHTED_ROUND_ROBIN_Splitter_4889);
	FOR(int, __iter_init_15_, 0, <, 8, __iter_init_15_++)
		init_buffer_float(&SplitJoin4_Butterfly_Fiss_4945_4966_split[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 4, __iter_init_16_++)
		init_buffer_float(&SplitJoin95_Butterfly_Fiss_4962_4973_split[__iter_init_16_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_4711WEIGHTED_ROUND_ROBIN_Splitter_4853);
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_float(&SplitJoin43_Butterfly_Fiss_4953_4978_join[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 2, __iter_init_18_++)
		init_buffer_float(&SplitJoin87_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_child1_4809_4971_split[__iter_init_18_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_4750WEIGHTED_ROUND_ROBIN_Splitter_4935);
	FOR(int, __iter_init_19_, 0, <, 2, __iter_init_19_++)
		init_buffer_float(&SplitJoin57_Butterfly_Fiss_4956_4982_split[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 2, __iter_init_20_++)
		init_buffer_float(&SplitJoin61_Butterfly_Fiss_4957_4983_join[__iter_init_20_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4839WEIGHTED_ROUND_ROBIN_Splitter_4840);
	init_buffer_float(&Pre_CollapsedDataParallel_1_4735WEIGHTED_ROUND_ROBIN_Splitter_4915);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4940Post_CollapsedDataParallel_2_4754);
	FOR(int, __iter_init_21_, 0, <, 4, __iter_init_21_++)
		init_buffer_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_child1_4834_4980_join[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 2, __iter_init_22_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_4368_4763_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_Hier_4944_4965_join[__iter_init_22_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_4715WEIGHTED_ROUND_ROBIN_Splitter_4835);
	FOR(int, __iter_init_23_, 0, <, 8, __iter_init_23_++)
		init_buffer_float(&SplitJoin85_Butterfly_Fiss_4960_4970_split[__iter_init_23_]);
	ENDFOR
	FOR(int, __iter_init_24_, 0, <, 2, __iter_init_24_++)
		init_buffer_float(&SplitJoin39_Butterfly_Fiss_4952_4977_join[__iter_init_24_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_4714WEIGHTED_ROUND_ROBIN_Splitter_4867);
	FOR(int, __iter_init_25_, 0, <, 2, __iter_init_25_++)
		init_buffer_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_child0_4804_4967_join[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 2, __iter_init_26_++)
		init_buffer_float(&SplitJoin53_Butterfly_Fiss_4955_4981_join[__iter_init_26_]);
	ENDFOR
	FOR(int, __iter_init_27_, 0, <, 8, __iter_init_27_++)
		init_buffer_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child0_4950_4986_split[__iter_init_27_]);
	ENDFOR
	FOR(int, __iter_init_28_, 0, <, 4, __iter_init_28_++)
		init_buffer_float(&SplitJoin95_Butterfly_Fiss_4962_4973_join[__iter_init_28_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4868Post_CollapsedDataParallel_2_4715);
	FOR(int, __iter_init_29_, 0, <, 4, __iter_init_29_++)
		init_buffer_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_child0_4833_4975_join[__iter_init_29_]);
	ENDFOR
	FOR(int, __iter_init_30_, 0, <, 8, __iter_init_30_++)
		init_buffer_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child1_4951_4987_split[__iter_init_30_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_4726WEIGHTED_ROUND_ROBIN_Splitter_4899);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4851BitReverse_4491);
	FOR(int, __iter_init_31_, 0, <, 2, __iter_init_31_++)
		init_buffer_float(&SplitJoin14_Butterfly_Fiss_4948_4976_split[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 2, __iter_init_32_++)
		init_buffer_float(&SplitJoin47_Butterfly_Fiss_4954_4979_split[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 8, __iter_init_33_++)
		init_buffer_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_child1_4951_4987_join[__iter_init_33_]);
	ENDFOR
	FOR(int, __iter_init_34_, 0, <, 4, __iter_init_34_++)
		init_buffer_float(&SplitJoin89_Butterfly_Fiss_4961_4972_join[__iter_init_34_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4936Post_CollapsedDataParallel_2_4751);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4884Post_CollapsedDataParallel_2_4724);
	FOR(int, __iter_init_35_, 0, <, 4, __iter_init_35_++)
		init_buffer_float(&SplitJoin72_Butterfly_Fiss_4959_4969_join[__iter_init_35_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4924Post_CollapsedDataParallel_2_4742);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4928Post_CollapsedDataParallel_2_4745);
	FOR(int, __iter_init_36_, 0, <, 4, __iter_init_36_++)
		init_buffer_float(&SplitJoin89_Butterfly_Fiss_4961_4972_split[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 2, __iter_init_37_++)
		init_buffer_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_Hier_4949_4985_split[__iter_init_37_]);
	ENDFOR
	FOR(int, __iter_init_38_, 0, <, 4, __iter_init_38_++)
		init_buffer_float(&SplitJoin8_Butterfly_Fiss_4946_4968_join[__iter_init_38_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4900Post_CollapsedDataParallel_2_4727);
	FOR(int, __iter_init_39_, 0, <, 2, __iter_init_39_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_4368_4763_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_Hier_4944_4965_split[__iter_init_39_]);
	ENDFOR
	FOR(int, __iter_init_40_, 0, <, 12, __iter_init_40_++)
		init_buffer_float(&SplitJoin0_Butterfly_Fiss_4943_4964_join[__iter_init_40_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_4741WEIGHTED_ROUND_ROBIN_Splitter_4923);
	FOR(int, __iter_init_41_, 0, <, 4, __iter_init_41_++)
		init_buffer_float(&SplitJoin72_Butterfly_Fiss_4959_4969_split[__iter_init_41_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_4718WEIGHTED_ROUND_ROBIN_Splitter_4837);
	FOR(int, __iter_init_42_, 0, <, 2, __iter_init_42_++)
		init_buffer_float(&SplitJoin39_Butterfly_Fiss_4952_4977_split[__iter_init_42_]);
	ENDFOR
	FOR(int, __iter_init_43_, 0, <, 4, __iter_init_43_++)
		init_buffer_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_4390_4767_Hier_child1_4834_4980_split[__iter_init_43_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4906Post_CollapsedDataParallel_2_4730);
	init_buffer_float(&BitReverse_4491FloatPrinter_4492);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4932Post_CollapsedDataParallel_2_4748);
	FOR(int, __iter_init_44_, 0, <, 2, __iter_init_44_++)
		init_buffer_float(&SplitJoin61_Butterfly_Fiss_4957_4983_split[__iter_init_44_]);
	ENDFOR
	FOR(int, __iter_init_45_, 0, <, 8, __iter_init_45_++)
		init_buffer_float(&SplitJoin4_Butterfly_Fiss_4945_4966_join[__iter_init_45_]);
	ENDFOR
	FOR(int, __iter_init_46_, 0, <, 2, __iter_init_46_++)
		init_buffer_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_4408_4769_Hier_Hier_4949_4985_join[__iter_init_46_]);
	ENDFOR
	FOR(int, __iter_init_47_, 0, <, 2, __iter_init_47_++)
		init_buffer_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_4377_4765_child0_4804_4967_split[__iter_init_47_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_4753WEIGHTED_ROUND_ROBIN_Splitter_4939);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4878Post_CollapsedDataParallel_2_4721);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4845WEIGHTED_ROUND_ROBIN_Splitter_4846);
	init_buffer_float(&Pre_CollapsedDataParallel_1_4723WEIGHTED_ROUND_ROBIN_Splitter_4883);
// --- init: FloatSource_4410
	 {
	FOR(int, i, 0,  < , 32, i++) {
		FloatSource_4410_s.A_re[i] = 0.0 ; 
		FloatSource_4410_s.A_im[i] = 0.0 ; 
	}
	ENDFOR
	FloatSource_4410_s.A_re[1] = 1.0 ; 
	FloatSource_4410_s.idx = 0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FloatSource_4410();
		Pre_CollapsedDataParallel_1_4711();
		WEIGHTED_ROUND_ROBIN_Splitter_4853();
			Butterfly_4855();
			Butterfly_4856();
			Butterfly_4857();
			Butterfly_4858();
			Butterfly_4859();
			Butterfly_4860();
			Butterfly_4861();
			Butterfly_4862();
			Butterfly_4863();
			Butterfly_4864();
			Butterfly_4865();
			Butterfly_4866();
		WEIGHTED_ROUND_ROBIN_Joiner_4854();
		Post_CollapsedDataParallel_2_4712();
		WEIGHTED_ROUND_ROBIN_Splitter_4755();
			Pre_CollapsedDataParallel_1_4714();
			WEIGHTED_ROUND_ROBIN_Splitter_4867();
				Butterfly_4869();
				Butterfly_4870();
				Butterfly_4871();
				Butterfly_4872();
				Butterfly_4873();
				Butterfly_4874();
				Butterfly_4875();
				Butterfly_4876();
			WEIGHTED_ROUND_ROBIN_Joiner_4868();
			Post_CollapsedDataParallel_2_4715();
			WEIGHTED_ROUND_ROBIN_Splitter_4835();
				Pre_CollapsedDataParallel_1_4720();
				WEIGHTED_ROUND_ROBIN_Splitter_4877();
					Butterfly_4879();
					Butterfly_4880();
					Butterfly_4881();
					Butterfly_4882();
				WEIGHTED_ROUND_ROBIN_Joiner_4878();
				Post_CollapsedDataParallel_2_4721();
				Pre_CollapsedDataParallel_1_4723();
				WEIGHTED_ROUND_ROBIN_Splitter_4883();
					Butterfly_4885();
					Butterfly_4886();
					Butterfly_4887();
					Butterfly_4888();
				WEIGHTED_ROUND_ROBIN_Joiner_4884();
				Post_CollapsedDataParallel_2_4724();
			WEIGHTED_ROUND_ROBIN_Joiner_4836();
			Pre_CollapsedDataParallel_1_4717();
			WEIGHTED_ROUND_ROBIN_Splitter_4889();
				Butterfly_4891();
				Butterfly_4892();
				Butterfly_4893();
				Butterfly_4894();
				Butterfly_4895();
				Butterfly_4896();
				Butterfly_4897();
				Butterfly_4898();
			WEIGHTED_ROUND_ROBIN_Joiner_4890();
			Post_CollapsedDataParallel_2_4718();
			WEIGHTED_ROUND_ROBIN_Splitter_4837();
				Pre_CollapsedDataParallel_1_4726();
				WEIGHTED_ROUND_ROBIN_Splitter_4899();
					Butterfly_4901();
					Butterfly_4902();
					Butterfly_4903();
					Butterfly_4904();
				WEIGHTED_ROUND_ROBIN_Joiner_4900();
				Post_CollapsedDataParallel_2_4727();
				Pre_CollapsedDataParallel_1_4729();
				WEIGHTED_ROUND_ROBIN_Splitter_4905();
					Butterfly_4907();
					Butterfly_4908();
					Butterfly_4909();
					Butterfly_4910();
				WEIGHTED_ROUND_ROBIN_Joiner_4906();
				Post_CollapsedDataParallel_2_4730();
			WEIGHTED_ROUND_ROBIN_Joiner_4838();
		WEIGHTED_ROUND_ROBIN_Joiner_4839();
		WEIGHTED_ROUND_ROBIN_Splitter_4840();
			WEIGHTED_ROUND_ROBIN_Splitter_4841();
				Pre_CollapsedDataParallel_1_4732();
				WEIGHTED_ROUND_ROBIN_Splitter_4911();
					Butterfly_4913();
					Butterfly_4914();
				WEIGHTED_ROUND_ROBIN_Joiner_4912();
				Post_CollapsedDataParallel_2_4733();
				Pre_CollapsedDataParallel_1_4735();
				WEIGHTED_ROUND_ROBIN_Splitter_4915();
					Butterfly_4917();
					Butterfly_4918();
				WEIGHTED_ROUND_ROBIN_Joiner_4916();
				Post_CollapsedDataParallel_2_4736();
				Pre_CollapsedDataParallel_1_4738();
				WEIGHTED_ROUND_ROBIN_Splitter_4919();
					Butterfly_4921();
					Butterfly_4922();
				WEIGHTED_ROUND_ROBIN_Joiner_4920();
				Post_CollapsedDataParallel_2_4739();
				Pre_CollapsedDataParallel_1_4741();
				WEIGHTED_ROUND_ROBIN_Splitter_4923();
					Butterfly_4925();
					Butterfly_4926();
				WEIGHTED_ROUND_ROBIN_Joiner_4924();
				Post_CollapsedDataParallel_2_4742();
			WEIGHTED_ROUND_ROBIN_Joiner_4842();
			WEIGHTED_ROUND_ROBIN_Splitter_4843();
				Pre_CollapsedDataParallel_1_4744();
				WEIGHTED_ROUND_ROBIN_Splitter_4927();
					Butterfly_4929();
					Butterfly_4930();
				WEIGHTED_ROUND_ROBIN_Joiner_4928();
				Post_CollapsedDataParallel_2_4745();
				Pre_CollapsedDataParallel_1_4747();
				WEIGHTED_ROUND_ROBIN_Splitter_4931();
					Butterfly_4933();
					Butterfly_4934();
				WEIGHTED_ROUND_ROBIN_Joiner_4932();
				Post_CollapsedDataParallel_2_4748();
				Pre_CollapsedDataParallel_1_4750();
				WEIGHTED_ROUND_ROBIN_Splitter_4935();
					Butterfly_4937();
					Butterfly_4938();
				WEIGHTED_ROUND_ROBIN_Joiner_4936();
				Post_CollapsedDataParallel_2_4751();
				Pre_CollapsedDataParallel_1_4753();
				WEIGHTED_ROUND_ROBIN_Splitter_4939();
					Butterfly_4941();
					Butterfly_4942();
				WEIGHTED_ROUND_ROBIN_Joiner_4940();
				Post_CollapsedDataParallel_2_4754();
			WEIGHTED_ROUND_ROBIN_Joiner_4844();
		WEIGHTED_ROUND_ROBIN_Joiner_4845();
		WEIGHTED_ROUND_ROBIN_Splitter_4846();
			WEIGHTED_ROUND_ROBIN_Splitter_4847();
				Butterfly_4475();
				Butterfly_4476();
				Butterfly_4477();
				Butterfly_4478();
				Butterfly_4479();
				Butterfly_4480();
				Butterfly_4481();
				Butterfly_4482();
			WEIGHTED_ROUND_ROBIN_Joiner_4848();
			WEIGHTED_ROUND_ROBIN_Splitter_4849();
				Butterfly_4483();
				Butterfly_4484();
				Butterfly_4485();
				Butterfly_4486();
				Butterfly_4487();
				Butterfly_4488();
				Butterfly_4489();
				Butterfly_4490();
			WEIGHTED_ROUND_ROBIN_Joiner_4850();
		WEIGHTED_ROUND_ROBIN_Joiner_4851();
		BitReverse_4491();
		FloatPrinter_4492();
	ENDFOR
	return EXIT_SUCCESS;
}
