#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=384 on the compile command line
#else
#if BUF_SIZEMAX < 384
#error BUF_SIZEMAX too small, it must be at least 384
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float A_re[32];
	float A_im[32];
	int idx;
} FloatSource_10850_t;
void FloatSource_10850();
void Pre_CollapsedDataParallel_1_11151();
void WEIGHTED_ROUND_ROBIN_Splitter_11293();
void Butterfly_11295();
void Butterfly_11296();
void Butterfly_11297();
void WEIGHTED_ROUND_ROBIN_Joiner_11294();
void Post_CollapsedDataParallel_2_11152();
void WEIGHTED_ROUND_ROBIN_Splitter_11195();
void Pre_CollapsedDataParallel_1_11154();
void WEIGHTED_ROUND_ROBIN_Splitter_11298();
void Butterfly_11300();
void Butterfly_11301();
void Butterfly_11302();
void WEIGHTED_ROUND_ROBIN_Joiner_11299();
void Post_CollapsedDataParallel_2_11155();
void WEIGHTED_ROUND_ROBIN_Splitter_11275();
void Pre_CollapsedDataParallel_1_11160();
void WEIGHTED_ROUND_ROBIN_Splitter_11303();
void Butterfly_11305();
void Butterfly_11306();
void Butterfly_11307();
void WEIGHTED_ROUND_ROBIN_Joiner_11304();
void Post_CollapsedDataParallel_2_11161();
void Pre_CollapsedDataParallel_1_11163();
void WEIGHTED_ROUND_ROBIN_Splitter_11308();
void Butterfly_11310();
void Butterfly_11311();
void Butterfly_11312();
void WEIGHTED_ROUND_ROBIN_Joiner_11309();
void Post_CollapsedDataParallel_2_11164();
void WEIGHTED_ROUND_ROBIN_Joiner_11276();
void Pre_CollapsedDataParallel_1_11157();
void WEIGHTED_ROUND_ROBIN_Splitter_11313();
void Butterfly_11315();
void Butterfly_11316();
void Butterfly_11317();
void WEIGHTED_ROUND_ROBIN_Joiner_11314();
void Post_CollapsedDataParallel_2_11158();
void WEIGHTED_ROUND_ROBIN_Splitter_11277();
void Pre_CollapsedDataParallel_1_11166();
void WEIGHTED_ROUND_ROBIN_Splitter_11318();
void Butterfly_11320();
void Butterfly_11321();
void Butterfly_11322();
void WEIGHTED_ROUND_ROBIN_Joiner_11319();
void Post_CollapsedDataParallel_2_11167();
void Pre_CollapsedDataParallel_1_11169();
void WEIGHTED_ROUND_ROBIN_Splitter_11323();
void Butterfly_11325();
void Butterfly_11326();
void Butterfly_11327();
void WEIGHTED_ROUND_ROBIN_Joiner_11324();
void Post_CollapsedDataParallel_2_11170();
void WEIGHTED_ROUND_ROBIN_Joiner_11278();
void WEIGHTED_ROUND_ROBIN_Joiner_11279();
void WEIGHTED_ROUND_ROBIN_Splitter_11280();
void WEIGHTED_ROUND_ROBIN_Splitter_11281();
void Pre_CollapsedDataParallel_1_11172();
void WEIGHTED_ROUND_ROBIN_Splitter_11328();
void Butterfly_11330();
void Butterfly_11331();
void WEIGHTED_ROUND_ROBIN_Joiner_11329();
void Post_CollapsedDataParallel_2_11173();
void Pre_CollapsedDataParallel_1_11175();
void WEIGHTED_ROUND_ROBIN_Splitter_11332();
void Butterfly_11334();
void Butterfly_11335();
void WEIGHTED_ROUND_ROBIN_Joiner_11333();
void Post_CollapsedDataParallel_2_11176();
void Pre_CollapsedDataParallel_1_11178();
void WEIGHTED_ROUND_ROBIN_Splitter_11336();
void Butterfly_11338();
void Butterfly_11339();
void WEIGHTED_ROUND_ROBIN_Joiner_11337();
void Post_CollapsedDataParallel_2_11179();
void Pre_CollapsedDataParallel_1_11181();
void WEIGHTED_ROUND_ROBIN_Splitter_11340();
void Butterfly_11342();
void Butterfly_11343();
void WEIGHTED_ROUND_ROBIN_Joiner_11341();
void Post_CollapsedDataParallel_2_11182();
void WEIGHTED_ROUND_ROBIN_Joiner_11282();
void WEIGHTED_ROUND_ROBIN_Splitter_11283();
void Pre_CollapsedDataParallel_1_11184();
void WEIGHTED_ROUND_ROBIN_Splitter_11344();
void Butterfly_11346();
void Butterfly_11347();
void WEIGHTED_ROUND_ROBIN_Joiner_11345();
void Post_CollapsedDataParallel_2_11185();
void Pre_CollapsedDataParallel_1_11187();
void WEIGHTED_ROUND_ROBIN_Splitter_11348();
void Butterfly_11350();
void Butterfly_11351();
void WEIGHTED_ROUND_ROBIN_Joiner_11349();
void Post_CollapsedDataParallel_2_11188();
void Pre_CollapsedDataParallel_1_11190();
void WEIGHTED_ROUND_ROBIN_Splitter_11352();
void Butterfly_11354();
void Butterfly_11355();
void WEIGHTED_ROUND_ROBIN_Joiner_11353();
void Post_CollapsedDataParallel_2_11191();
void Pre_CollapsedDataParallel_1_11193();
void WEIGHTED_ROUND_ROBIN_Splitter_11356();
void Butterfly_11358();
void Butterfly_11359();
void WEIGHTED_ROUND_ROBIN_Joiner_11357();
void Post_CollapsedDataParallel_2_11194();
void WEIGHTED_ROUND_ROBIN_Joiner_11284();
void WEIGHTED_ROUND_ROBIN_Joiner_11285();
void WEIGHTED_ROUND_ROBIN_Splitter_11286();
void WEIGHTED_ROUND_ROBIN_Splitter_11287();
void Butterfly_10915();
void Butterfly_10916();
void Butterfly_10917();
void Butterfly_10918();
void Butterfly_10919();
void Butterfly_10920();
void Butterfly_10921();
void Butterfly_10922();
void WEIGHTED_ROUND_ROBIN_Joiner_11288();
void WEIGHTED_ROUND_ROBIN_Splitter_11289();
void Butterfly_10923();
void Butterfly_10924();
void Butterfly_10925();
void Butterfly_10926();
void Butterfly_10927();
void Butterfly_10928();
void Butterfly_10929();
void Butterfly_10930();
void WEIGHTED_ROUND_ROBIN_Joiner_11290();
void WEIGHTED_ROUND_ROBIN_Joiner_11291();
int BitReverse_10931_bitrev(int inp, int numbits);
void BitReverse_10931();
void FloatPrinter_10932();

#ifdef __cplusplus
}
#endif
#endif
