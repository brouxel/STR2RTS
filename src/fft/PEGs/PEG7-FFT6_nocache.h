#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=896 on the compile command line
#else
#if BUF_SIZEMAX < 896
#error BUF_SIZEMAX too small, it must be at least 896
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	complex_t wn;
} CombineDFT_5377_t;
void FFTTestSource_5333();
void FFTReorderSimple_5334();
void WEIGHTED_ROUND_ROBIN_Splitter_5347();
void FFTReorderSimple_5349();
void FFTReorderSimple_5350();
void WEIGHTED_ROUND_ROBIN_Joiner_5348();
void WEIGHTED_ROUND_ROBIN_Splitter_5351();
void FFTReorderSimple_5353();
void FFTReorderSimple_5354();
void FFTReorderSimple_5355();
void FFTReorderSimple_5356();
void WEIGHTED_ROUND_ROBIN_Joiner_5352();
void WEIGHTED_ROUND_ROBIN_Splitter_5357();
void FFTReorderSimple_5359();
void FFTReorderSimple_5360();
void FFTReorderSimple_5361();
void FFTReorderSimple_5362();
void FFTReorderSimple_5363();
void FFTReorderSimple_5364();
void FFTReorderSimple_5365();
void WEIGHTED_ROUND_ROBIN_Joiner_5358();
void WEIGHTED_ROUND_ROBIN_Splitter_5366();
void FFTReorderSimple_5368();
void FFTReorderSimple_5369();
void FFTReorderSimple_5370();
void FFTReorderSimple_5371();
void FFTReorderSimple_5372();
void FFTReorderSimple_5373();
void FFTReorderSimple_5374();
void WEIGHTED_ROUND_ROBIN_Joiner_5367();
void WEIGHTED_ROUND_ROBIN_Splitter_5375();
void CombineDFT_5377();
void CombineDFT_5378();
void CombineDFT_5379();
void CombineDFT_5380();
void CombineDFT_5381();
void CombineDFT_5382();
void CombineDFT_5383();
void WEIGHTED_ROUND_ROBIN_Joiner_5376();
void WEIGHTED_ROUND_ROBIN_Splitter_5384();
void CombineDFT_5386();
void CombineDFT_5387();
void CombineDFT_5388();
void CombineDFT_5389();
void CombineDFT_5390();
void CombineDFT_5391();
void CombineDFT_5392();
void WEIGHTED_ROUND_ROBIN_Joiner_5385();
void WEIGHTED_ROUND_ROBIN_Splitter_5393();
void CombineDFT_5395();
void CombineDFT_5396();
void CombineDFT_5397();
void CombineDFT_5398();
void CombineDFT_5399();
void CombineDFT_5400();
void CombineDFT_5401();
void WEIGHTED_ROUND_ROBIN_Joiner_5394();
void WEIGHTED_ROUND_ROBIN_Splitter_5402();
void CombineDFT_5404();
void CombineDFT_5405();
void CombineDFT_5406();
void CombineDFT_5407();
void WEIGHTED_ROUND_ROBIN_Joiner_5403();
void WEIGHTED_ROUND_ROBIN_Splitter_5408();
void CombineDFT_5410();
void CombineDFT_5411();
void WEIGHTED_ROUND_ROBIN_Joiner_5409();
void CombineDFT_5344();
void CPrinter_5345();

#ifdef __cplusplus
}
#endif
#endif
