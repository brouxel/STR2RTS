#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=384 on the compile command line
#else
#if BUF_SIZEMAX < 384
#error BUF_SIZEMAX too small, it must be at least 384
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	complex_t wn;
} CombineDFT_5908_t;
void FFTTestSource(buffer_complex_t *chanout);
void FFTTestSource_5873();
void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout);
void FFTReorderSimple_5874();
void WEIGHTED_ROUND_ROBIN_Splitter_5887();
void FFTReorderSimple_5889();
void FFTReorderSimple_5890();
void WEIGHTED_ROUND_ROBIN_Joiner_5888();
void WEIGHTED_ROUND_ROBIN_Splitter_5891();
void FFTReorderSimple_5893();
void FFTReorderSimple_5894();
void FFTReorderSimple_5895();
void WEIGHTED_ROUND_ROBIN_Joiner_5892();
void WEIGHTED_ROUND_ROBIN_Splitter_5896();
void FFTReorderSimple_5898();
void FFTReorderSimple_5899();
void FFTReorderSimple_5900();
void WEIGHTED_ROUND_ROBIN_Joiner_5897();
void WEIGHTED_ROUND_ROBIN_Splitter_5901();
void FFTReorderSimple_5903();
void FFTReorderSimple_5904();
void FFTReorderSimple_5905();
void WEIGHTED_ROUND_ROBIN_Joiner_5902();
void WEIGHTED_ROUND_ROBIN_Splitter_5906();
void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout);
void CombineDFT_5908();
void CombineDFT_5909();
void CombineDFT_5910();
void WEIGHTED_ROUND_ROBIN_Joiner_5907();
void WEIGHTED_ROUND_ROBIN_Splitter_5911();
void CombineDFT_5913();
void CombineDFT_5914();
void CombineDFT_5915();
void WEIGHTED_ROUND_ROBIN_Joiner_5912();
void WEIGHTED_ROUND_ROBIN_Splitter_5916();
void CombineDFT_5918();
void CombineDFT_5919();
void CombineDFT_5920();
void WEIGHTED_ROUND_ROBIN_Joiner_5917();
void WEIGHTED_ROUND_ROBIN_Splitter_5921();
void CombineDFT_5923();
void CombineDFT_5924();
void CombineDFT_5925();
void WEIGHTED_ROUND_ROBIN_Joiner_5922();
void WEIGHTED_ROUND_ROBIN_Splitter_5926();
void CombineDFT_5928();
void CombineDFT_5929();
void WEIGHTED_ROUND_ROBIN_Joiner_5927();
void CombineDFT_5884();
void CPrinter(buffer_complex_t *chanin);
void CPrinter_5885();

#ifdef __cplusplus
}
#endif
#endif
