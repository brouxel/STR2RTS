#include "PEG7-FFT3_nocache.h"

buffer_float_t SplitJoin43_Butterfly_Fiss_8566_8591_join[2];
buffer_float_t Pre_CollapsedDataParallel_1_8346WEIGHTED_ROUND_ROBIN_Splitter_8512;
buffer_float_t SplitJoin8_Butterfly_Fiss_8559_8581_split[4];
buffer_float_t SplitJoin8_Butterfly_Fiss_8559_8581_join[4];
buffer_float_t Pre_CollapsedDataParallel_1_8343WEIGHTED_ROUND_ROBIN_Splitter_8497;
buffer_float_t SplitJoin94_Butterfly_Fiss_8575_8586_split[4];
buffer_float_t SplitJoin53_Butterfly_Fiss_8568_8594_split[2];
buffer_float_t SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child0_8453_8588_split[4];
buffer_float_t SplitJoin72_Butterfly_Fiss_8572_8582_join[4];
buffer_float_t Pre_CollapsedDataParallel_1_8361WEIGHTED_ROUND_ROBIN_Splitter_8536;
buffer_float_t SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[8];
buffer_float_t SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_Hier_8560_8587_join[2];
buffer_float_t Post_CollapsedDataParallel_2_8335WEIGHTED_ROUND_ROBIN_Splitter_8455;
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_7988_8383_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_Hier_8557_8578_split[2];
buffer_float_t SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_Hier_8562_8598_split[2];
buffer_float_t FloatSource_8030Pre_CollapsedDataParallel_1_8331;
buffer_float_t BitReverse_8111FloatPrinter_8112;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8513Post_CollapsedDataParallel_2_8347;
buffer_float_t Pre_CollapsedDataParallel_1_8355WEIGHTED_ROUND_ROBIN_Splitter_8528;
buffer_float_t Pre_CollapsedDataParallel_1_8334WEIGHTED_ROUND_ROBIN_Splitter_8482;
buffer_float_t SplitJoin84_Butterfly_Fiss_8573_8583_split[7];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8549Post_CollapsedDataParallel_2_8371;
buffer_float_t SplitJoin43_Butterfly_Fiss_8566_8591_split[2];
buffer_float_t SplitJoin47_Butterfly_Fiss_8567_8592_split[2];
buffer_float_t SplitJoin84_Butterfly_Fiss_8573_8583_join[7];
buffer_float_t SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child1_8454_8593_join[4];
buffer_float_t Pre_CollapsedDataParallel_1_8370WEIGHTED_ROUND_ROBIN_Splitter_8548;
buffer_float_t SplitJoin61_Butterfly_Fiss_8570_8596_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8519Post_CollapsedDataParallel_2_8350;
buffer_float_t SplitJoin57_Butterfly_Fiss_8569_8595_join[2];
buffer_float_t SplitJoin4_Butterfly_Fiss_8558_8579_split[7];
buffer_float_t SplitJoin86_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_child1_8429_8584_split[2];
buffer_float_t SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[8];
buffer_float_t SplitJoin88_Butterfly_Fiss_8574_8585_join[4];
buffer_float_t Pre_CollapsedDataParallel_1_8367WEIGHTED_ROUND_ROBIN_Splitter_8544;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8504Post_CollapsedDataParallel_2_8338;
buffer_float_t SplitJoin88_Butterfly_Fiss_8574_8585_split[4];
buffer_float_t SplitJoin39_Butterfly_Fiss_8565_8590_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8545Post_CollapsedDataParallel_2_8368;
buffer_float_t SplitJoin65_Butterfly_Fiss_8571_8597_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8541Post_CollapsedDataParallel_2_8365;
buffer_float_t SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_Hier_8560_8587_split[2];
buffer_float_t SplitJoin57_Butterfly_Fiss_8569_8595_split[2];
buffer_float_t SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[8];
buffer_float_t SplitJoin61_Butterfly_Fiss_8570_8596_join[2];
buffer_float_t SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[8];
buffer_float_t Pre_CollapsedDataParallel_1_8340WEIGHTED_ROUND_ROBIN_Splitter_8491;
buffer_float_t SplitJoin0_Butterfly_Fiss_8556_8577_join[7];
buffer_float_t Post_CollapsedDataParallel_2_8332WEIGHTED_ROUND_ROBIN_Splitter_8375;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8474Post_CollapsedDataParallel_2_8332;
buffer_float_t Pre_CollapsedDataParallel_1_8364WEIGHTED_ROUND_ROBIN_Splitter_8540;
buffer_float_t Pre_CollapsedDataParallel_1_8358WEIGHTED_ROUND_ROBIN_Splitter_8532;
buffer_float_t Pre_CollapsedDataParallel_1_8337WEIGHTED_ROUND_ROBIN_Splitter_8503;
buffer_float_t SplitJoin86_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_child1_8429_8584_join[2];
buffer_float_t SplitJoin94_Butterfly_Fiss_8575_8586_join[4];
buffer_float_t Pre_CollapsedDataParallel_1_8331WEIGHTED_ROUND_ROBIN_Splitter_8473;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8483Post_CollapsedDataParallel_2_8335;
buffer_float_t Post_CollapsedDataParallel_2_8338WEIGHTED_ROUND_ROBIN_Splitter_8457;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8498Post_CollapsedDataParallel_2_8344;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8459WEIGHTED_ROUND_ROBIN_Splitter_8460;
buffer_float_t SplitJoin4_Butterfly_Fiss_8558_8579_join[7];
buffer_float_t SplitJoin53_Butterfly_Fiss_8568_8594_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8492Post_CollapsedDataParallel_2_8341;
buffer_float_t SplitJoin39_Butterfly_Fiss_8565_8590_split[2];
buffer_float_t Pre_CollapsedDataParallel_1_8349WEIGHTED_ROUND_ROBIN_Splitter_8518;
buffer_float_t Pre_CollapsedDataParallel_1_8352WEIGHTED_ROUND_ROBIN_Splitter_8524;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8525Post_CollapsedDataParallel_2_8353;
buffer_float_t SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_Hier_8562_8598_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8529Post_CollapsedDataParallel_2_8356;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8537Post_CollapsedDataParallel_2_8362;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8465WEIGHTED_ROUND_ROBIN_Splitter_8466;
buffer_float_t Pre_CollapsedDataParallel_1_8373WEIGHTED_ROUND_ROBIN_Splitter_8552;
buffer_float_t SplitJoin0_Butterfly_Fiss_8556_8577_split[7];
buffer_float_t SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_child0_8424_8580_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8553Post_CollapsedDataParallel_2_8374;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8471BitReverse_8111;
buffer_float_t SplitJoin47_Butterfly_Fiss_8567_8592_join[2];
buffer_float_t SplitJoin65_Butterfly_Fiss_8571_8597_join[2];
buffer_float_t SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_child0_8424_8580_split[2];
buffer_float_t SplitJoin14_Butterfly_Fiss_8561_8589_split[2];
buffer_float_t SplitJoin14_Butterfly_Fiss_8561_8589_join[2];
buffer_float_t SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child1_8454_8593_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8533Post_CollapsedDataParallel_2_8359;
buffer_float_t SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child0_8453_8588_join[4];
buffer_float_t SplitJoin72_Butterfly_Fiss_8572_8582_split[4];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_7988_8383_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_Hier_8557_8578_join[2];


FloatSource_8030_t FloatSource_8030_s;

void FloatSource_8030(){
	FOR(uint32_t, __iter_steady_, 0, <, 224, __iter_steady_++) {
		push_float(&FloatSource_8030Pre_CollapsedDataParallel_1_8331, FloatSource_8030_s.A_re[FloatSource_8030_s.idx]) ; 
		push_float(&FloatSource_8030Pre_CollapsedDataParallel_1_8331, FloatSource_8030_s.A_im[FloatSource_8030_s.idx]) ; 
		FloatSource_8030_s.idx++ ; 
		if((FloatSource_8030_s.idx >= 32)) {
			FloatSource_8030_s.idx = 0 ; 
		}
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_8331(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
		int partialSum_k = 0;
 {
		FOR(int, _k, 0,  < , 16, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k;
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Pre_CollapsedDataParallel_1_8331WEIGHTED_ROUND_ROBIN_Splitter_8473, peek_float(&FloatSource_8030Pre_CollapsedDataParallel_1_8331, (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 32) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&FloatSource_8030Pre_CollapsedDataParallel_1_8331) ; 
	}
	ENDFOR
}

void Butterfly_8475(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin0_Butterfly_Fiss_8556_8577_split[0]) ; 
		u_im = pop_float(&SplitJoin0_Butterfly_Fiss_8556_8577_split[0]) ; 
		t_re = pop_float(&SplitJoin0_Butterfly_Fiss_8556_8577_split[0]) ; 
		t_im = pop_float(&SplitJoin0_Butterfly_Fiss_8556_8577_split[0]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_8556_8577_join[0], u_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_8556_8577_join[0], u_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_8556_8577_join[0], t_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_8556_8577_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8476(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin0_Butterfly_Fiss_8556_8577_split[1]) ; 
		u_im = pop_float(&SplitJoin0_Butterfly_Fiss_8556_8577_split[1]) ; 
		t_re = pop_float(&SplitJoin0_Butterfly_Fiss_8556_8577_split[1]) ; 
		t_im = pop_float(&SplitJoin0_Butterfly_Fiss_8556_8577_split[1]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_8556_8577_join[1], u_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_8556_8577_join[1], u_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_8556_8577_join[1], t_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_8556_8577_join[1], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8477(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin0_Butterfly_Fiss_8556_8577_split[2]) ; 
		u_im = pop_float(&SplitJoin0_Butterfly_Fiss_8556_8577_split[2]) ; 
		t_re = pop_float(&SplitJoin0_Butterfly_Fiss_8556_8577_split[2]) ; 
		t_im = pop_float(&SplitJoin0_Butterfly_Fiss_8556_8577_split[2]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_8556_8577_join[2], u_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_8556_8577_join[2], u_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_8556_8577_join[2], t_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_8556_8577_join[2], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8478(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin0_Butterfly_Fiss_8556_8577_split[3]) ; 
		u_im = pop_float(&SplitJoin0_Butterfly_Fiss_8556_8577_split[3]) ; 
		t_re = pop_float(&SplitJoin0_Butterfly_Fiss_8556_8577_split[3]) ; 
		t_im = pop_float(&SplitJoin0_Butterfly_Fiss_8556_8577_split[3]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_8556_8577_join[3], u_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_8556_8577_join[3], u_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_8556_8577_join[3], t_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_8556_8577_join[3], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8479(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin0_Butterfly_Fiss_8556_8577_split[4]) ; 
		u_im = pop_float(&SplitJoin0_Butterfly_Fiss_8556_8577_split[4]) ; 
		t_re = pop_float(&SplitJoin0_Butterfly_Fiss_8556_8577_split[4]) ; 
		t_im = pop_float(&SplitJoin0_Butterfly_Fiss_8556_8577_split[4]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_8556_8577_join[4], u_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_8556_8577_join[4], u_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_8556_8577_join[4], t_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_8556_8577_join[4], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8480(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin0_Butterfly_Fiss_8556_8577_split[5]) ; 
		u_im = pop_float(&SplitJoin0_Butterfly_Fiss_8556_8577_split[5]) ; 
		t_re = pop_float(&SplitJoin0_Butterfly_Fiss_8556_8577_split[5]) ; 
		t_im = pop_float(&SplitJoin0_Butterfly_Fiss_8556_8577_split[5]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_8556_8577_join[5], u_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_8556_8577_join[5], u_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_8556_8577_join[5], t_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_8556_8577_join[5], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8481(){
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin0_Butterfly_Fiss_8556_8577_split[6]) ; 
		u_im = pop_float(&SplitJoin0_Butterfly_Fiss_8556_8577_split[6]) ; 
		t_re = pop_float(&SplitJoin0_Butterfly_Fiss_8556_8577_split[6]) ; 
		t_im = pop_float(&SplitJoin0_Butterfly_Fiss_8556_8577_split[6]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_8556_8577_join[6], u_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_8556_8577_join[6], u_im) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_8556_8577_join[6], t_re) ; 
		push_float(&SplitJoin0_Butterfly_Fiss_8556_8577_join[6], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8473() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin0_Butterfly_Fiss_8556_8577_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_8331WEIGHTED_ROUND_ROBIN_Splitter_8473));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8474() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8474Post_CollapsedDataParallel_2_8332, pop_float(&SplitJoin0_Butterfly_Fiss_8556_8577_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_8332(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
		int kTimesWeights_i = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 16, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Post_CollapsedDataParallel_2_8332WEIGHTED_ROUND_ROBIN_Splitter_8375, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_8474Post_CollapsedDataParallel_2_8332, (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8474Post_CollapsedDataParallel_2_8332) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_8334(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
		int partialSum_k = 0;
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Pre_CollapsedDataParallel_1_8334WEIGHTED_ROUND_ROBIN_Splitter_8482, peek_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_7988_8383_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_Hier_8557_8578_split[0], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 16) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_7988_8383_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_Hier_8557_8578_split[0]) ; 
	}
	ENDFOR
}

void Butterfly_8484(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin4_Butterfly_Fiss_8558_8579_split[0]) ; 
		u_im = pop_float(&SplitJoin4_Butterfly_Fiss_8558_8579_split[0]) ; 
		t_re = pop_float(&SplitJoin4_Butterfly_Fiss_8558_8579_split[0]) ; 
		t_im = pop_float(&SplitJoin4_Butterfly_Fiss_8558_8579_split[0]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_8558_8579_join[0], u_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_8558_8579_join[0], u_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_8558_8579_join[0], t_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_8558_8579_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8485(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin4_Butterfly_Fiss_8558_8579_split[1]) ; 
		u_im = pop_float(&SplitJoin4_Butterfly_Fiss_8558_8579_split[1]) ; 
		t_re = pop_float(&SplitJoin4_Butterfly_Fiss_8558_8579_split[1]) ; 
		t_im = pop_float(&SplitJoin4_Butterfly_Fiss_8558_8579_split[1]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_8558_8579_join[1], u_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_8558_8579_join[1], u_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_8558_8579_join[1], t_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_8558_8579_join[1], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8486(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin4_Butterfly_Fiss_8558_8579_split[2]) ; 
		u_im = pop_float(&SplitJoin4_Butterfly_Fiss_8558_8579_split[2]) ; 
		t_re = pop_float(&SplitJoin4_Butterfly_Fiss_8558_8579_split[2]) ; 
		t_im = pop_float(&SplitJoin4_Butterfly_Fiss_8558_8579_split[2]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_8558_8579_join[2], u_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_8558_8579_join[2], u_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_8558_8579_join[2], t_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_8558_8579_join[2], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8487(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin4_Butterfly_Fiss_8558_8579_split[3]) ; 
		u_im = pop_float(&SplitJoin4_Butterfly_Fiss_8558_8579_split[3]) ; 
		t_re = pop_float(&SplitJoin4_Butterfly_Fiss_8558_8579_split[3]) ; 
		t_im = pop_float(&SplitJoin4_Butterfly_Fiss_8558_8579_split[3]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_8558_8579_join[3], u_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_8558_8579_join[3], u_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_8558_8579_join[3], t_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_8558_8579_join[3], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8488(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin4_Butterfly_Fiss_8558_8579_split[4]) ; 
		u_im = pop_float(&SplitJoin4_Butterfly_Fiss_8558_8579_split[4]) ; 
		t_re = pop_float(&SplitJoin4_Butterfly_Fiss_8558_8579_split[4]) ; 
		t_im = pop_float(&SplitJoin4_Butterfly_Fiss_8558_8579_split[4]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_8558_8579_join[4], u_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_8558_8579_join[4], u_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_8558_8579_join[4], t_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_8558_8579_join[4], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8489(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin4_Butterfly_Fiss_8558_8579_split[5]) ; 
		u_im = pop_float(&SplitJoin4_Butterfly_Fiss_8558_8579_split[5]) ; 
		t_re = pop_float(&SplitJoin4_Butterfly_Fiss_8558_8579_split[5]) ; 
		t_im = pop_float(&SplitJoin4_Butterfly_Fiss_8558_8579_split[5]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_8558_8579_join[5], u_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_8558_8579_join[5], u_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_8558_8579_join[5], t_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_8558_8579_join[5], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8490(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin4_Butterfly_Fiss_8558_8579_split[6]) ; 
		u_im = pop_float(&SplitJoin4_Butterfly_Fiss_8558_8579_split[6]) ; 
		t_re = pop_float(&SplitJoin4_Butterfly_Fiss_8558_8579_split[6]) ; 
		t_im = pop_float(&SplitJoin4_Butterfly_Fiss_8558_8579_split[6]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_8558_8579_join[6], u_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_8558_8579_join[6], u_im) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_8558_8579_join[6], t_re) ; 
		push_float(&SplitJoin4_Butterfly_Fiss_8558_8579_join[6], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8482() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin4_Butterfly_Fiss_8558_8579_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_8334WEIGHTED_ROUND_ROBIN_Splitter_8482));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8483() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8483Post_CollapsedDataParallel_2_8335, pop_float(&SplitJoin4_Butterfly_Fiss_8558_8579_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_8335(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
		int kTimesWeights_i = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Post_CollapsedDataParallel_2_8335WEIGHTED_ROUND_ROBIN_Splitter_8455, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_8483Post_CollapsedDataParallel_2_8335, (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8483Post_CollapsedDataParallel_2_8335) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_8340(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
		int partialSum_k = 0;
		partialSum_k = 0 ; 
 {
		FOR(int, _k, 0,  < , 4, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Pre_CollapsedDataParallel_1_8340WEIGHTED_ROUND_ROBIN_Splitter_8491, peek_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_child0_8424_8580_split[0], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_child0_8424_8580_split[0]) ; 
	}
	ENDFOR
}

void Butterfly_8493(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin8_Butterfly_Fiss_8559_8581_split[0]) ; 
		u_im = pop_float(&SplitJoin8_Butterfly_Fiss_8559_8581_split[0]) ; 
		t_re = pop_float(&SplitJoin8_Butterfly_Fiss_8559_8581_split[0]) ; 
		t_im = pop_float(&SplitJoin8_Butterfly_Fiss_8559_8581_split[0]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_8559_8581_join[0], u_re) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_8559_8581_join[0], u_im) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_8559_8581_join[0], t_re) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_8559_8581_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8494(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin8_Butterfly_Fiss_8559_8581_split[1]) ; 
		u_im = pop_float(&SplitJoin8_Butterfly_Fiss_8559_8581_split[1]) ; 
		t_re = pop_float(&SplitJoin8_Butterfly_Fiss_8559_8581_split[1]) ; 
		t_im = pop_float(&SplitJoin8_Butterfly_Fiss_8559_8581_split[1]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_8559_8581_join[1], u_re) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_8559_8581_join[1], u_im) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_8559_8581_join[1], t_re) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_8559_8581_join[1], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8495(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin8_Butterfly_Fiss_8559_8581_split[2]) ; 
		u_im = pop_float(&SplitJoin8_Butterfly_Fiss_8559_8581_split[2]) ; 
		t_re = pop_float(&SplitJoin8_Butterfly_Fiss_8559_8581_split[2]) ; 
		t_im = pop_float(&SplitJoin8_Butterfly_Fiss_8559_8581_split[2]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_8559_8581_join[2], u_re) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_8559_8581_join[2], u_im) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_8559_8581_join[2], t_re) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_8559_8581_join[2], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8496(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin8_Butterfly_Fiss_8559_8581_split[3]) ; 
		u_im = pop_float(&SplitJoin8_Butterfly_Fiss_8559_8581_split[3]) ; 
		t_re = pop_float(&SplitJoin8_Butterfly_Fiss_8559_8581_split[3]) ; 
		t_im = pop_float(&SplitJoin8_Butterfly_Fiss_8559_8581_split[3]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_8559_8581_join[3], u_re) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_8559_8581_join[3], u_im) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_8559_8581_join[3], t_re) ; 
		push_float(&SplitJoin8_Butterfly_Fiss_8559_8581_join[3], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8491() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin8_Butterfly_Fiss_8559_8581_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_8340WEIGHTED_ROUND_ROBIN_Splitter_8491));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8492() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8492Post_CollapsedDataParallel_2_8341, pop_float(&SplitJoin8_Butterfly_Fiss_8559_8581_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_8341(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
		int kTimesWeights_i = 0;
		kTimesWeights_i = 0 ; 
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 4, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_child0_8424_8580_join[0], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_8492Post_CollapsedDataParallel_2_8341, (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8492Post_CollapsedDataParallel_2_8341) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_8343(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
		int partialSum_k = 0;
		partialSum_k = 0 ; 
 {
		FOR(int, _k, 0,  < , 4, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Pre_CollapsedDataParallel_1_8343WEIGHTED_ROUND_ROBIN_Splitter_8497, peek_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_child0_8424_8580_split[1], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_child0_8424_8580_split[1]) ; 
	}
	ENDFOR
}

void Butterfly_8499(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin72_Butterfly_Fiss_8572_8582_split[0]) ; 
		u_im = pop_float(&SplitJoin72_Butterfly_Fiss_8572_8582_split[0]) ; 
		t_re = pop_float(&SplitJoin72_Butterfly_Fiss_8572_8582_split[0]) ; 
		t_im = pop_float(&SplitJoin72_Butterfly_Fiss_8572_8582_split[0]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_8572_8582_join[0], u_re) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_8572_8582_join[0], u_im) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_8572_8582_join[0], t_re) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_8572_8582_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8500(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin72_Butterfly_Fiss_8572_8582_split[1]) ; 
		u_im = pop_float(&SplitJoin72_Butterfly_Fiss_8572_8582_split[1]) ; 
		t_re = pop_float(&SplitJoin72_Butterfly_Fiss_8572_8582_split[1]) ; 
		t_im = pop_float(&SplitJoin72_Butterfly_Fiss_8572_8582_split[1]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_8572_8582_join[1], u_re) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_8572_8582_join[1], u_im) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_8572_8582_join[1], t_re) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_8572_8582_join[1], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8501(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin72_Butterfly_Fiss_8572_8582_split[2]) ; 
		u_im = pop_float(&SplitJoin72_Butterfly_Fiss_8572_8582_split[2]) ; 
		t_re = pop_float(&SplitJoin72_Butterfly_Fiss_8572_8582_split[2]) ; 
		t_im = pop_float(&SplitJoin72_Butterfly_Fiss_8572_8582_split[2]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_8572_8582_join[2], u_re) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_8572_8582_join[2], u_im) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_8572_8582_join[2], t_re) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_8572_8582_join[2], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8502(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin72_Butterfly_Fiss_8572_8582_split[3]) ; 
		u_im = pop_float(&SplitJoin72_Butterfly_Fiss_8572_8582_split[3]) ; 
		t_re = pop_float(&SplitJoin72_Butterfly_Fiss_8572_8582_split[3]) ; 
		t_im = pop_float(&SplitJoin72_Butterfly_Fiss_8572_8582_split[3]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_8572_8582_join[3], u_re) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_8572_8582_join[3], u_im) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_8572_8582_join[3], t_re) ; 
		push_float(&SplitJoin72_Butterfly_Fiss_8572_8582_join[3], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8497() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin72_Butterfly_Fiss_8572_8582_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_8343WEIGHTED_ROUND_ROBIN_Splitter_8497));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8498() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8498Post_CollapsedDataParallel_2_8344, pop_float(&SplitJoin72_Butterfly_Fiss_8572_8582_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_8344(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
		int kTimesWeights_i = 0;
		kTimesWeights_i = 0 ; 
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 4, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_child0_8424_8580_join[1], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_8498Post_CollapsedDataParallel_2_8344, (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8498Post_CollapsedDataParallel_2_8344) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8455() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_child0_8424_8580_split[0], pop_float(&Post_CollapsedDataParallel_2_8335WEIGHTED_ROUND_ROBIN_Splitter_8455));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_child0_8424_8580_split[1], pop_float(&Post_CollapsedDataParallel_2_8335WEIGHTED_ROUND_ROBIN_Splitter_8455));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8456() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_7988_8383_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_Hier_8557_8578_join[0], pop_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_child0_8424_8580_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_7988_8383_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_Hier_8557_8578_join[0], pop_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_child0_8424_8580_join[1]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_8337(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
		int partialSum_k = 0;
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Pre_CollapsedDataParallel_1_8337WEIGHTED_ROUND_ROBIN_Splitter_8503, peek_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_7988_8383_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_Hier_8557_8578_split[1], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 16) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_7988_8383_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_Hier_8557_8578_split[1]) ; 
	}
	ENDFOR
}

void Butterfly_8505(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin84_Butterfly_Fiss_8573_8583_split[0]) ; 
		u_im = pop_float(&SplitJoin84_Butterfly_Fiss_8573_8583_split[0]) ; 
		t_re = pop_float(&SplitJoin84_Butterfly_Fiss_8573_8583_split[0]) ; 
		t_im = pop_float(&SplitJoin84_Butterfly_Fiss_8573_8583_split[0]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin84_Butterfly_Fiss_8573_8583_join[0], u_re) ; 
		push_float(&SplitJoin84_Butterfly_Fiss_8573_8583_join[0], u_im) ; 
		push_float(&SplitJoin84_Butterfly_Fiss_8573_8583_join[0], t_re) ; 
		push_float(&SplitJoin84_Butterfly_Fiss_8573_8583_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8506(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin84_Butterfly_Fiss_8573_8583_split[1]) ; 
		u_im = pop_float(&SplitJoin84_Butterfly_Fiss_8573_8583_split[1]) ; 
		t_re = pop_float(&SplitJoin84_Butterfly_Fiss_8573_8583_split[1]) ; 
		t_im = pop_float(&SplitJoin84_Butterfly_Fiss_8573_8583_split[1]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin84_Butterfly_Fiss_8573_8583_join[1], u_re) ; 
		push_float(&SplitJoin84_Butterfly_Fiss_8573_8583_join[1], u_im) ; 
		push_float(&SplitJoin84_Butterfly_Fiss_8573_8583_join[1], t_re) ; 
		push_float(&SplitJoin84_Butterfly_Fiss_8573_8583_join[1], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8507(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin84_Butterfly_Fiss_8573_8583_split[2]) ; 
		u_im = pop_float(&SplitJoin84_Butterfly_Fiss_8573_8583_split[2]) ; 
		t_re = pop_float(&SplitJoin84_Butterfly_Fiss_8573_8583_split[2]) ; 
		t_im = pop_float(&SplitJoin84_Butterfly_Fiss_8573_8583_split[2]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin84_Butterfly_Fiss_8573_8583_join[2], u_re) ; 
		push_float(&SplitJoin84_Butterfly_Fiss_8573_8583_join[2], u_im) ; 
		push_float(&SplitJoin84_Butterfly_Fiss_8573_8583_join[2], t_re) ; 
		push_float(&SplitJoin84_Butterfly_Fiss_8573_8583_join[2], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8508(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin84_Butterfly_Fiss_8573_8583_split[3]) ; 
		u_im = pop_float(&SplitJoin84_Butterfly_Fiss_8573_8583_split[3]) ; 
		t_re = pop_float(&SplitJoin84_Butterfly_Fiss_8573_8583_split[3]) ; 
		t_im = pop_float(&SplitJoin84_Butterfly_Fiss_8573_8583_split[3]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin84_Butterfly_Fiss_8573_8583_join[3], u_re) ; 
		push_float(&SplitJoin84_Butterfly_Fiss_8573_8583_join[3], u_im) ; 
		push_float(&SplitJoin84_Butterfly_Fiss_8573_8583_join[3], t_re) ; 
		push_float(&SplitJoin84_Butterfly_Fiss_8573_8583_join[3], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8509(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin84_Butterfly_Fiss_8573_8583_split[4]) ; 
		u_im = pop_float(&SplitJoin84_Butterfly_Fiss_8573_8583_split[4]) ; 
		t_re = pop_float(&SplitJoin84_Butterfly_Fiss_8573_8583_split[4]) ; 
		t_im = pop_float(&SplitJoin84_Butterfly_Fiss_8573_8583_split[4]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin84_Butterfly_Fiss_8573_8583_join[4], u_re) ; 
		push_float(&SplitJoin84_Butterfly_Fiss_8573_8583_join[4], u_im) ; 
		push_float(&SplitJoin84_Butterfly_Fiss_8573_8583_join[4], t_re) ; 
		push_float(&SplitJoin84_Butterfly_Fiss_8573_8583_join[4], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8510(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin84_Butterfly_Fiss_8573_8583_split[5]) ; 
		u_im = pop_float(&SplitJoin84_Butterfly_Fiss_8573_8583_split[5]) ; 
		t_re = pop_float(&SplitJoin84_Butterfly_Fiss_8573_8583_split[5]) ; 
		t_im = pop_float(&SplitJoin84_Butterfly_Fiss_8573_8583_split[5]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin84_Butterfly_Fiss_8573_8583_join[5], u_re) ; 
		push_float(&SplitJoin84_Butterfly_Fiss_8573_8583_join[5], u_im) ; 
		push_float(&SplitJoin84_Butterfly_Fiss_8573_8583_join[5], t_re) ; 
		push_float(&SplitJoin84_Butterfly_Fiss_8573_8583_join[5], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8511(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin84_Butterfly_Fiss_8573_8583_split[6]) ; 
		u_im = pop_float(&SplitJoin84_Butterfly_Fiss_8573_8583_split[6]) ; 
		t_re = pop_float(&SplitJoin84_Butterfly_Fiss_8573_8583_split[6]) ; 
		t_im = pop_float(&SplitJoin84_Butterfly_Fiss_8573_8583_split[6]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin84_Butterfly_Fiss_8573_8583_join[6], u_re) ; 
		push_float(&SplitJoin84_Butterfly_Fiss_8573_8583_join[6], u_im) ; 
		push_float(&SplitJoin84_Butterfly_Fiss_8573_8583_join[6], t_re) ; 
		push_float(&SplitJoin84_Butterfly_Fiss_8573_8583_join[6], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8503() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin84_Butterfly_Fiss_8573_8583_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_8337WEIGHTED_ROUND_ROBIN_Splitter_8503));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8504() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8504Post_CollapsedDataParallel_2_8338, pop_float(&SplitJoin84_Butterfly_Fiss_8573_8583_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_8338(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
		int kTimesWeights_i = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Post_CollapsedDataParallel_2_8338WEIGHTED_ROUND_ROBIN_Splitter_8457, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_8504Post_CollapsedDataParallel_2_8338, (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8504Post_CollapsedDataParallel_2_8338) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_8346(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
		int partialSum_k = 0;
		partialSum_k = 0 ; 
 {
		FOR(int, _k, 0,  < , 4, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Pre_CollapsedDataParallel_1_8346WEIGHTED_ROUND_ROBIN_Splitter_8512, peek_float(&SplitJoin86_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_child1_8429_8584_split[0], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&SplitJoin86_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_child1_8429_8584_split[0]) ; 
	}
	ENDFOR
}

void Butterfly_8514(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin88_Butterfly_Fiss_8574_8585_split[0]) ; 
		u_im = pop_float(&SplitJoin88_Butterfly_Fiss_8574_8585_split[0]) ; 
		t_re = pop_float(&SplitJoin88_Butterfly_Fiss_8574_8585_split[0]) ; 
		t_im = pop_float(&SplitJoin88_Butterfly_Fiss_8574_8585_split[0]) ; 
		wt_re = ((0.70710677 * t_re) - (0.70710677 * t_im)) ; 
		wt_im = ((0.70710677 * t_im) + (0.70710677 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin88_Butterfly_Fiss_8574_8585_join[0], u_re) ; 
		push_float(&SplitJoin88_Butterfly_Fiss_8574_8585_join[0], u_im) ; 
		push_float(&SplitJoin88_Butterfly_Fiss_8574_8585_join[0], t_re) ; 
		push_float(&SplitJoin88_Butterfly_Fiss_8574_8585_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8515(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin88_Butterfly_Fiss_8574_8585_split[1]) ; 
		u_im = pop_float(&SplitJoin88_Butterfly_Fiss_8574_8585_split[1]) ; 
		t_re = pop_float(&SplitJoin88_Butterfly_Fiss_8574_8585_split[1]) ; 
		t_im = pop_float(&SplitJoin88_Butterfly_Fiss_8574_8585_split[1]) ; 
		wt_re = ((0.70710677 * t_re) - (0.70710677 * t_im)) ; 
		wt_im = ((0.70710677 * t_im) + (0.70710677 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin88_Butterfly_Fiss_8574_8585_join[1], u_re) ; 
		push_float(&SplitJoin88_Butterfly_Fiss_8574_8585_join[1], u_im) ; 
		push_float(&SplitJoin88_Butterfly_Fiss_8574_8585_join[1], t_re) ; 
		push_float(&SplitJoin88_Butterfly_Fiss_8574_8585_join[1], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8516(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin88_Butterfly_Fiss_8574_8585_split[2]) ; 
		u_im = pop_float(&SplitJoin88_Butterfly_Fiss_8574_8585_split[2]) ; 
		t_re = pop_float(&SplitJoin88_Butterfly_Fiss_8574_8585_split[2]) ; 
		t_im = pop_float(&SplitJoin88_Butterfly_Fiss_8574_8585_split[2]) ; 
		wt_re = ((0.70710677 * t_re) - (0.70710677 * t_im)) ; 
		wt_im = ((0.70710677 * t_im) + (0.70710677 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin88_Butterfly_Fiss_8574_8585_join[2], u_re) ; 
		push_float(&SplitJoin88_Butterfly_Fiss_8574_8585_join[2], u_im) ; 
		push_float(&SplitJoin88_Butterfly_Fiss_8574_8585_join[2], t_re) ; 
		push_float(&SplitJoin88_Butterfly_Fiss_8574_8585_join[2], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8517(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin88_Butterfly_Fiss_8574_8585_split[3]) ; 
		u_im = pop_float(&SplitJoin88_Butterfly_Fiss_8574_8585_split[3]) ; 
		t_re = pop_float(&SplitJoin88_Butterfly_Fiss_8574_8585_split[3]) ; 
		t_im = pop_float(&SplitJoin88_Butterfly_Fiss_8574_8585_split[3]) ; 
		wt_re = ((0.70710677 * t_re) - (0.70710677 * t_im)) ; 
		wt_im = ((0.70710677 * t_im) + (0.70710677 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin88_Butterfly_Fiss_8574_8585_join[3], u_re) ; 
		push_float(&SplitJoin88_Butterfly_Fiss_8574_8585_join[3], u_im) ; 
		push_float(&SplitJoin88_Butterfly_Fiss_8574_8585_join[3], t_re) ; 
		push_float(&SplitJoin88_Butterfly_Fiss_8574_8585_join[3], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8512() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin88_Butterfly_Fiss_8574_8585_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_8346WEIGHTED_ROUND_ROBIN_Splitter_8512));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8513() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8513Post_CollapsedDataParallel_2_8347, pop_float(&SplitJoin88_Butterfly_Fiss_8574_8585_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_8347(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
		int kTimesWeights_i = 0;
		kTimesWeights_i = 0 ; 
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 4, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&SplitJoin86_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_child1_8429_8584_join[0], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_8513Post_CollapsedDataParallel_2_8347, (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8513Post_CollapsedDataParallel_2_8347) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_8349(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
		int partialSum_k = 0;
		partialSum_k = 0 ; 
 {
		FOR(int, _k, 0,  < , 4, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Pre_CollapsedDataParallel_1_8349WEIGHTED_ROUND_ROBIN_Splitter_8518, peek_float(&SplitJoin86_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_child1_8429_8584_split[1], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&SplitJoin86_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_child1_8429_8584_split[1]) ; 
	}
	ENDFOR
}

void Butterfly_8520(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin94_Butterfly_Fiss_8575_8586_split[0]) ; 
		u_im = pop_float(&SplitJoin94_Butterfly_Fiss_8575_8586_split[0]) ; 
		t_re = pop_float(&SplitJoin94_Butterfly_Fiss_8575_8586_split[0]) ; 
		t_im = pop_float(&SplitJoin94_Butterfly_Fiss_8575_8586_split[0]) ; 
		wt_re = ((-0.70710677 * t_re) - (0.70710677 * t_im)) ; 
		wt_im = ((-0.70710677 * t_im) + (0.70710677 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin94_Butterfly_Fiss_8575_8586_join[0], u_re) ; 
		push_float(&SplitJoin94_Butterfly_Fiss_8575_8586_join[0], u_im) ; 
		push_float(&SplitJoin94_Butterfly_Fiss_8575_8586_join[0], t_re) ; 
		push_float(&SplitJoin94_Butterfly_Fiss_8575_8586_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8521(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin94_Butterfly_Fiss_8575_8586_split[1]) ; 
		u_im = pop_float(&SplitJoin94_Butterfly_Fiss_8575_8586_split[1]) ; 
		t_re = pop_float(&SplitJoin94_Butterfly_Fiss_8575_8586_split[1]) ; 
		t_im = pop_float(&SplitJoin94_Butterfly_Fiss_8575_8586_split[1]) ; 
		wt_re = ((-0.70710677 * t_re) - (0.70710677 * t_im)) ; 
		wt_im = ((-0.70710677 * t_im) + (0.70710677 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin94_Butterfly_Fiss_8575_8586_join[1], u_re) ; 
		push_float(&SplitJoin94_Butterfly_Fiss_8575_8586_join[1], u_im) ; 
		push_float(&SplitJoin94_Butterfly_Fiss_8575_8586_join[1], t_re) ; 
		push_float(&SplitJoin94_Butterfly_Fiss_8575_8586_join[1], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8522(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin94_Butterfly_Fiss_8575_8586_split[2]) ; 
		u_im = pop_float(&SplitJoin94_Butterfly_Fiss_8575_8586_split[2]) ; 
		t_re = pop_float(&SplitJoin94_Butterfly_Fiss_8575_8586_split[2]) ; 
		t_im = pop_float(&SplitJoin94_Butterfly_Fiss_8575_8586_split[2]) ; 
		wt_re = ((-0.70710677 * t_re) - (0.70710677 * t_im)) ; 
		wt_im = ((-0.70710677 * t_im) + (0.70710677 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin94_Butterfly_Fiss_8575_8586_join[2], u_re) ; 
		push_float(&SplitJoin94_Butterfly_Fiss_8575_8586_join[2], u_im) ; 
		push_float(&SplitJoin94_Butterfly_Fiss_8575_8586_join[2], t_re) ; 
		push_float(&SplitJoin94_Butterfly_Fiss_8575_8586_join[2], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8523(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = 0.0 ; 
		u_im = 0.0 ; 
		t_re = 0.0 ; 
		t_im = 0.0 ; 
		wt_re = 0.0 ; 
		wt_im = 0.0 ; 
		u_re = pop_float(&SplitJoin94_Butterfly_Fiss_8575_8586_split[3]) ; 
		u_im = pop_float(&SplitJoin94_Butterfly_Fiss_8575_8586_split[3]) ; 
		t_re = pop_float(&SplitJoin94_Butterfly_Fiss_8575_8586_split[3]) ; 
		t_im = pop_float(&SplitJoin94_Butterfly_Fiss_8575_8586_split[3]) ; 
		wt_re = ((-0.70710677 * t_re) - (0.70710677 * t_im)) ; 
		wt_im = ((-0.70710677 * t_im) + (0.70710677 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin94_Butterfly_Fiss_8575_8586_join[3], u_re) ; 
		push_float(&SplitJoin94_Butterfly_Fiss_8575_8586_join[3], u_im) ; 
		push_float(&SplitJoin94_Butterfly_Fiss_8575_8586_join[3], t_re) ; 
		push_float(&SplitJoin94_Butterfly_Fiss_8575_8586_join[3], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8518() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin94_Butterfly_Fiss_8575_8586_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_8349WEIGHTED_ROUND_ROBIN_Splitter_8518));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8519() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8519Post_CollapsedDataParallel_2_8350, pop_float(&SplitJoin94_Butterfly_Fiss_8575_8586_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_8350(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
		int kTimesWeights_i = 0;
		kTimesWeights_i = 0 ; 
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 4, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&SplitJoin86_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_child1_8429_8584_join[1], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_8519Post_CollapsedDataParallel_2_8350, (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8519Post_CollapsedDataParallel_2_8350) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8457() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin86_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_child1_8429_8584_split[0], pop_float(&Post_CollapsedDataParallel_2_8338WEIGHTED_ROUND_ROBIN_Splitter_8457));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin86_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_child1_8429_8584_split[1], pop_float(&Post_CollapsedDataParallel_2_8338WEIGHTED_ROUND_ROBIN_Splitter_8457));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8458() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_7988_8383_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_Hier_8557_8578_join[1], pop_float(&SplitJoin86_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_child1_8429_8584_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_7988_8383_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_Hier_8557_8578_join[1], pop_float(&SplitJoin86_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_child1_8429_8584_join[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_8375() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_7988_8383_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_Hier_8557_8578_split[0], pop_float(&Post_CollapsedDataParallel_2_8332WEIGHTED_ROUND_ROBIN_Splitter_8375));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_7988_8383_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_Hier_8557_8578_split[1], pop_float(&Post_CollapsedDataParallel_2_8332WEIGHTED_ROUND_ROBIN_Splitter_8375));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8459() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8459WEIGHTED_ROUND_ROBIN_Splitter_8460, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_7988_8383_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_Hier_8557_8578_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8459WEIGHTED_ROUND_ROBIN_Splitter_8460, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_7988_8383_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_Hier_8557_8578_join[1]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_8352(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
		int partialSum_k = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Pre_CollapsedDataParallel_1_8352WEIGHTED_ROUND_ROBIN_Splitter_8524, peek_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child0_8453_8588_split[0], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 4) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child0_8453_8588_split[0]) ; 
	}
	ENDFOR
}

void Butterfly_8526(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin14_Butterfly_Fiss_8561_8589_split[0]) ; 
		u_im = pop_float(&SplitJoin14_Butterfly_Fiss_8561_8589_split[0]) ; 
		t_re = pop_float(&SplitJoin14_Butterfly_Fiss_8561_8589_split[0]) ; 
		t_im = pop_float(&SplitJoin14_Butterfly_Fiss_8561_8589_split[0]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin14_Butterfly_Fiss_8561_8589_join[0], u_re) ; 
		push_float(&SplitJoin14_Butterfly_Fiss_8561_8589_join[0], u_im) ; 
		push_float(&SplitJoin14_Butterfly_Fiss_8561_8589_join[0], t_re) ; 
		push_float(&SplitJoin14_Butterfly_Fiss_8561_8589_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8527(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin14_Butterfly_Fiss_8561_8589_split[1]) ; 
		u_im = pop_float(&SplitJoin14_Butterfly_Fiss_8561_8589_split[1]) ; 
		t_re = pop_float(&SplitJoin14_Butterfly_Fiss_8561_8589_split[1]) ; 
		t_im = pop_float(&SplitJoin14_Butterfly_Fiss_8561_8589_split[1]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin14_Butterfly_Fiss_8561_8589_join[1], u_re) ; 
		push_float(&SplitJoin14_Butterfly_Fiss_8561_8589_join[1], u_im) ; 
		push_float(&SplitJoin14_Butterfly_Fiss_8561_8589_join[1], t_re) ; 
		push_float(&SplitJoin14_Butterfly_Fiss_8561_8589_join[1], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8524() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin14_Butterfly_Fiss_8561_8589_split[0], pop_float(&Pre_CollapsedDataParallel_1_8352WEIGHTED_ROUND_ROBIN_Splitter_8524));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin14_Butterfly_Fiss_8561_8589_split[1], pop_float(&Pre_CollapsedDataParallel_1_8352WEIGHTED_ROUND_ROBIN_Splitter_8524));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8525() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8525Post_CollapsedDataParallel_2_8353, pop_float(&SplitJoin14_Butterfly_Fiss_8561_8589_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8525Post_CollapsedDataParallel_2_8353, pop_float(&SplitJoin14_Butterfly_Fiss_8561_8589_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_8353(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
		int kTimesWeights_i = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child0_8453_8588_join[0], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_8525Post_CollapsedDataParallel_2_8353, (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8525Post_CollapsedDataParallel_2_8353) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_8355(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
		int partialSum_k = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Pre_CollapsedDataParallel_1_8355WEIGHTED_ROUND_ROBIN_Splitter_8528, peek_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child0_8453_8588_split[1], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 4) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child0_8453_8588_split[1]) ; 
	}
	ENDFOR
}

void Butterfly_8530(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin39_Butterfly_Fiss_8565_8590_split[0]) ; 
		u_im = pop_float(&SplitJoin39_Butterfly_Fiss_8565_8590_split[0]) ; 
		t_re = pop_float(&SplitJoin39_Butterfly_Fiss_8565_8590_split[0]) ; 
		t_im = pop_float(&SplitJoin39_Butterfly_Fiss_8565_8590_split[0]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin39_Butterfly_Fiss_8565_8590_join[0], u_re) ; 
		push_float(&SplitJoin39_Butterfly_Fiss_8565_8590_join[0], u_im) ; 
		push_float(&SplitJoin39_Butterfly_Fiss_8565_8590_join[0], t_re) ; 
		push_float(&SplitJoin39_Butterfly_Fiss_8565_8590_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8531(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin39_Butterfly_Fiss_8565_8590_split[1]) ; 
		u_im = pop_float(&SplitJoin39_Butterfly_Fiss_8565_8590_split[1]) ; 
		t_re = pop_float(&SplitJoin39_Butterfly_Fiss_8565_8590_split[1]) ; 
		t_im = pop_float(&SplitJoin39_Butterfly_Fiss_8565_8590_split[1]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin39_Butterfly_Fiss_8565_8590_join[1], u_re) ; 
		push_float(&SplitJoin39_Butterfly_Fiss_8565_8590_join[1], u_im) ; 
		push_float(&SplitJoin39_Butterfly_Fiss_8565_8590_join[1], t_re) ; 
		push_float(&SplitJoin39_Butterfly_Fiss_8565_8590_join[1], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8528() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin39_Butterfly_Fiss_8565_8590_split[0], pop_float(&Pre_CollapsedDataParallel_1_8355WEIGHTED_ROUND_ROBIN_Splitter_8528));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin39_Butterfly_Fiss_8565_8590_split[1], pop_float(&Pre_CollapsedDataParallel_1_8355WEIGHTED_ROUND_ROBIN_Splitter_8528));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8529() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8529Post_CollapsedDataParallel_2_8356, pop_float(&SplitJoin39_Butterfly_Fiss_8565_8590_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8529Post_CollapsedDataParallel_2_8356, pop_float(&SplitJoin39_Butterfly_Fiss_8565_8590_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_8356(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
		int kTimesWeights_i = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child0_8453_8588_join[1], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_8529Post_CollapsedDataParallel_2_8356, (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8529Post_CollapsedDataParallel_2_8356) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_8358(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
		int partialSum_k = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Pre_CollapsedDataParallel_1_8358WEIGHTED_ROUND_ROBIN_Splitter_8532, peek_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child0_8453_8588_split[2], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 4) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child0_8453_8588_split[2]) ; 
	}
	ENDFOR
}

void Butterfly_8534(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin43_Butterfly_Fiss_8566_8591_split[0]) ; 
		u_im = pop_float(&SplitJoin43_Butterfly_Fiss_8566_8591_split[0]) ; 
		t_re = pop_float(&SplitJoin43_Butterfly_Fiss_8566_8591_split[0]) ; 
		t_im = pop_float(&SplitJoin43_Butterfly_Fiss_8566_8591_split[0]) ; 
		wt_re = ((0.70710677 * t_re) - (0.70710677 * t_im)) ; 
		wt_im = ((0.70710677 * t_im) + (0.70710677 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin43_Butterfly_Fiss_8566_8591_join[0], u_re) ; 
		push_float(&SplitJoin43_Butterfly_Fiss_8566_8591_join[0], u_im) ; 
		push_float(&SplitJoin43_Butterfly_Fiss_8566_8591_join[0], t_re) ; 
		push_float(&SplitJoin43_Butterfly_Fiss_8566_8591_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8535(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin43_Butterfly_Fiss_8566_8591_split[1]) ; 
		u_im = pop_float(&SplitJoin43_Butterfly_Fiss_8566_8591_split[1]) ; 
		t_re = pop_float(&SplitJoin43_Butterfly_Fiss_8566_8591_split[1]) ; 
		t_im = pop_float(&SplitJoin43_Butterfly_Fiss_8566_8591_split[1]) ; 
		wt_re = ((0.70710677 * t_re) - (0.70710677 * t_im)) ; 
		wt_im = ((0.70710677 * t_im) + (0.70710677 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin43_Butterfly_Fiss_8566_8591_join[1], u_re) ; 
		push_float(&SplitJoin43_Butterfly_Fiss_8566_8591_join[1], u_im) ; 
		push_float(&SplitJoin43_Butterfly_Fiss_8566_8591_join[1], t_re) ; 
		push_float(&SplitJoin43_Butterfly_Fiss_8566_8591_join[1], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8532() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin43_Butterfly_Fiss_8566_8591_split[0], pop_float(&Pre_CollapsedDataParallel_1_8358WEIGHTED_ROUND_ROBIN_Splitter_8532));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin43_Butterfly_Fiss_8566_8591_split[1], pop_float(&Pre_CollapsedDataParallel_1_8358WEIGHTED_ROUND_ROBIN_Splitter_8532));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8533() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8533Post_CollapsedDataParallel_2_8359, pop_float(&SplitJoin43_Butterfly_Fiss_8566_8591_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8533Post_CollapsedDataParallel_2_8359, pop_float(&SplitJoin43_Butterfly_Fiss_8566_8591_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_8359(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
		int kTimesWeights_i = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child0_8453_8588_join[2], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_8533Post_CollapsedDataParallel_2_8359, (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8533Post_CollapsedDataParallel_2_8359) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_8361(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
		int partialSum_k = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Pre_CollapsedDataParallel_1_8361WEIGHTED_ROUND_ROBIN_Splitter_8536, peek_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child0_8453_8588_split[3], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 4) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child0_8453_8588_split[3]) ; 
	}
	ENDFOR
}

void Butterfly_8538(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin47_Butterfly_Fiss_8567_8592_split[0]) ; 
		u_im = pop_float(&SplitJoin47_Butterfly_Fiss_8567_8592_split[0]) ; 
		t_re = pop_float(&SplitJoin47_Butterfly_Fiss_8567_8592_split[0]) ; 
		t_im = pop_float(&SplitJoin47_Butterfly_Fiss_8567_8592_split[0]) ; 
		wt_re = ((-0.70710677 * t_re) - (0.70710677 * t_im)) ; 
		wt_im = ((-0.70710677 * t_im) + (0.70710677 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin47_Butterfly_Fiss_8567_8592_join[0], u_re) ; 
		push_float(&SplitJoin47_Butterfly_Fiss_8567_8592_join[0], u_im) ; 
		push_float(&SplitJoin47_Butterfly_Fiss_8567_8592_join[0], t_re) ; 
		push_float(&SplitJoin47_Butterfly_Fiss_8567_8592_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8539(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin47_Butterfly_Fiss_8567_8592_split[1]) ; 
		u_im = pop_float(&SplitJoin47_Butterfly_Fiss_8567_8592_split[1]) ; 
		t_re = pop_float(&SplitJoin47_Butterfly_Fiss_8567_8592_split[1]) ; 
		t_im = pop_float(&SplitJoin47_Butterfly_Fiss_8567_8592_split[1]) ; 
		wt_re = ((-0.70710677 * t_re) - (0.70710677 * t_im)) ; 
		wt_im = ((-0.70710677 * t_im) + (0.70710677 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin47_Butterfly_Fiss_8567_8592_join[1], u_re) ; 
		push_float(&SplitJoin47_Butterfly_Fiss_8567_8592_join[1], u_im) ; 
		push_float(&SplitJoin47_Butterfly_Fiss_8567_8592_join[1], t_re) ; 
		push_float(&SplitJoin47_Butterfly_Fiss_8567_8592_join[1], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8536() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin47_Butterfly_Fiss_8567_8592_split[0], pop_float(&Pre_CollapsedDataParallel_1_8361WEIGHTED_ROUND_ROBIN_Splitter_8536));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin47_Butterfly_Fiss_8567_8592_split[1], pop_float(&Pre_CollapsedDataParallel_1_8361WEIGHTED_ROUND_ROBIN_Splitter_8536));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8537() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8537Post_CollapsedDataParallel_2_8362, pop_float(&SplitJoin47_Butterfly_Fiss_8567_8592_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8537Post_CollapsedDataParallel_2_8362, pop_float(&SplitJoin47_Butterfly_Fiss_8567_8592_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_8362(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
		int kTimesWeights_i = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child0_8453_8588_join[3], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_8537Post_CollapsedDataParallel_2_8362, (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8537Post_CollapsedDataParallel_2_8362) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8461() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child0_8453_8588_split[__iter_dec_], pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_Hier_8560_8587_split[0]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8462() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_Hier_8560_8587_join[0], pop_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child0_8453_8588_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_8364(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
		int partialSum_k = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Pre_CollapsedDataParallel_1_8364WEIGHTED_ROUND_ROBIN_Splitter_8540, peek_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child1_8454_8593_split[0], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 4) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child1_8454_8593_split[0]) ; 
	}
	ENDFOR
}

void Butterfly_8542(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin53_Butterfly_Fiss_8568_8594_split[0]) ; 
		u_im = pop_float(&SplitJoin53_Butterfly_Fiss_8568_8594_split[0]) ; 
		t_re = pop_float(&SplitJoin53_Butterfly_Fiss_8568_8594_split[0]) ; 
		t_im = pop_float(&SplitJoin53_Butterfly_Fiss_8568_8594_split[0]) ; 
		wt_re = ((0.9238795 * t_re) - (0.38268346 * t_im)) ; 
		wt_im = ((0.9238795 * t_im) + (0.38268346 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin53_Butterfly_Fiss_8568_8594_join[0], u_re) ; 
		push_float(&SplitJoin53_Butterfly_Fiss_8568_8594_join[0], u_im) ; 
		push_float(&SplitJoin53_Butterfly_Fiss_8568_8594_join[0], t_re) ; 
		push_float(&SplitJoin53_Butterfly_Fiss_8568_8594_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8543(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin53_Butterfly_Fiss_8568_8594_split[1]) ; 
		u_im = pop_float(&SplitJoin53_Butterfly_Fiss_8568_8594_split[1]) ; 
		t_re = pop_float(&SplitJoin53_Butterfly_Fiss_8568_8594_split[1]) ; 
		t_im = pop_float(&SplitJoin53_Butterfly_Fiss_8568_8594_split[1]) ; 
		wt_re = ((0.9238795 * t_re) - (0.38268346 * t_im)) ; 
		wt_im = ((0.9238795 * t_im) + (0.38268346 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin53_Butterfly_Fiss_8568_8594_join[1], u_re) ; 
		push_float(&SplitJoin53_Butterfly_Fiss_8568_8594_join[1], u_im) ; 
		push_float(&SplitJoin53_Butterfly_Fiss_8568_8594_join[1], t_re) ; 
		push_float(&SplitJoin53_Butterfly_Fiss_8568_8594_join[1], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8540() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin53_Butterfly_Fiss_8568_8594_split[0], pop_float(&Pre_CollapsedDataParallel_1_8364WEIGHTED_ROUND_ROBIN_Splitter_8540));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin53_Butterfly_Fiss_8568_8594_split[1], pop_float(&Pre_CollapsedDataParallel_1_8364WEIGHTED_ROUND_ROBIN_Splitter_8540));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8541() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8541Post_CollapsedDataParallel_2_8365, pop_float(&SplitJoin53_Butterfly_Fiss_8568_8594_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8541Post_CollapsedDataParallel_2_8365, pop_float(&SplitJoin53_Butterfly_Fiss_8568_8594_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_8365(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
		int kTimesWeights_i = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child1_8454_8593_join[0], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_8541Post_CollapsedDataParallel_2_8365, (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8541Post_CollapsedDataParallel_2_8365) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_8367(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
		int partialSum_k = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Pre_CollapsedDataParallel_1_8367WEIGHTED_ROUND_ROBIN_Splitter_8544, peek_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child1_8454_8593_split[1], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 4) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child1_8454_8593_split[1]) ; 
	}
	ENDFOR
}

void Butterfly_8546(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin57_Butterfly_Fiss_8569_8595_split[0]) ; 
		u_im = pop_float(&SplitJoin57_Butterfly_Fiss_8569_8595_split[0]) ; 
		t_re = pop_float(&SplitJoin57_Butterfly_Fiss_8569_8595_split[0]) ; 
		t_im = pop_float(&SplitJoin57_Butterfly_Fiss_8569_8595_split[0]) ; 
		wt_re = ((-0.38268352 * t_re) - (0.9238795 * t_im)) ; 
		wt_im = ((-0.38268352 * t_im) + (0.9238795 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin57_Butterfly_Fiss_8569_8595_join[0], u_re) ; 
		push_float(&SplitJoin57_Butterfly_Fiss_8569_8595_join[0], u_im) ; 
		push_float(&SplitJoin57_Butterfly_Fiss_8569_8595_join[0], t_re) ; 
		push_float(&SplitJoin57_Butterfly_Fiss_8569_8595_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8547(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin57_Butterfly_Fiss_8569_8595_split[1]) ; 
		u_im = pop_float(&SplitJoin57_Butterfly_Fiss_8569_8595_split[1]) ; 
		t_re = pop_float(&SplitJoin57_Butterfly_Fiss_8569_8595_split[1]) ; 
		t_im = pop_float(&SplitJoin57_Butterfly_Fiss_8569_8595_split[1]) ; 
		wt_re = ((-0.38268352 * t_re) - (0.9238795 * t_im)) ; 
		wt_im = ((-0.38268352 * t_im) + (0.9238795 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin57_Butterfly_Fiss_8569_8595_join[1], u_re) ; 
		push_float(&SplitJoin57_Butterfly_Fiss_8569_8595_join[1], u_im) ; 
		push_float(&SplitJoin57_Butterfly_Fiss_8569_8595_join[1], t_re) ; 
		push_float(&SplitJoin57_Butterfly_Fiss_8569_8595_join[1], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8544() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin57_Butterfly_Fiss_8569_8595_split[0], pop_float(&Pre_CollapsedDataParallel_1_8367WEIGHTED_ROUND_ROBIN_Splitter_8544));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin57_Butterfly_Fiss_8569_8595_split[1], pop_float(&Pre_CollapsedDataParallel_1_8367WEIGHTED_ROUND_ROBIN_Splitter_8544));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8545() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8545Post_CollapsedDataParallel_2_8368, pop_float(&SplitJoin57_Butterfly_Fiss_8569_8595_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8545Post_CollapsedDataParallel_2_8368, pop_float(&SplitJoin57_Butterfly_Fiss_8569_8595_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_8368(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
		int kTimesWeights_i = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child1_8454_8593_join[1], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_8545Post_CollapsedDataParallel_2_8368, (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8545Post_CollapsedDataParallel_2_8368) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_8370(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
		int partialSum_k = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Pre_CollapsedDataParallel_1_8370WEIGHTED_ROUND_ROBIN_Splitter_8548, peek_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child1_8454_8593_split[2], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 4) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child1_8454_8593_split[2]) ; 
	}
	ENDFOR
}

void Butterfly_8550(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin61_Butterfly_Fiss_8570_8596_split[0]) ; 
		u_im = pop_float(&SplitJoin61_Butterfly_Fiss_8570_8596_split[0]) ; 
		t_re = pop_float(&SplitJoin61_Butterfly_Fiss_8570_8596_split[0]) ; 
		t_im = pop_float(&SplitJoin61_Butterfly_Fiss_8570_8596_split[0]) ; 
		wt_re = ((0.38268343 * t_re) - (0.9238795 * t_im)) ; 
		wt_im = ((0.38268343 * t_im) + (0.9238795 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin61_Butterfly_Fiss_8570_8596_join[0], u_re) ; 
		push_float(&SplitJoin61_Butterfly_Fiss_8570_8596_join[0], u_im) ; 
		push_float(&SplitJoin61_Butterfly_Fiss_8570_8596_join[0], t_re) ; 
		push_float(&SplitJoin61_Butterfly_Fiss_8570_8596_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8551(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin61_Butterfly_Fiss_8570_8596_split[1]) ; 
		u_im = pop_float(&SplitJoin61_Butterfly_Fiss_8570_8596_split[1]) ; 
		t_re = pop_float(&SplitJoin61_Butterfly_Fiss_8570_8596_split[1]) ; 
		t_im = pop_float(&SplitJoin61_Butterfly_Fiss_8570_8596_split[1]) ; 
		wt_re = ((0.38268343 * t_re) - (0.9238795 * t_im)) ; 
		wt_im = ((0.38268343 * t_im) + (0.9238795 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin61_Butterfly_Fiss_8570_8596_join[1], u_re) ; 
		push_float(&SplitJoin61_Butterfly_Fiss_8570_8596_join[1], u_im) ; 
		push_float(&SplitJoin61_Butterfly_Fiss_8570_8596_join[1], t_re) ; 
		push_float(&SplitJoin61_Butterfly_Fiss_8570_8596_join[1], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8548() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin61_Butterfly_Fiss_8570_8596_split[0], pop_float(&Pre_CollapsedDataParallel_1_8370WEIGHTED_ROUND_ROBIN_Splitter_8548));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin61_Butterfly_Fiss_8570_8596_split[1], pop_float(&Pre_CollapsedDataParallel_1_8370WEIGHTED_ROUND_ROBIN_Splitter_8548));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8549() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8549Post_CollapsedDataParallel_2_8371, pop_float(&SplitJoin61_Butterfly_Fiss_8570_8596_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8549Post_CollapsedDataParallel_2_8371, pop_float(&SplitJoin61_Butterfly_Fiss_8570_8596_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_8371(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
		int kTimesWeights_i = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child1_8454_8593_join[2], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_8549Post_CollapsedDataParallel_2_8371, (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8549Post_CollapsedDataParallel_2_8371) ; 
	}
	ENDFOR
}

void Pre_CollapsedDataParallel_1_8373(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
		int partialSum_k = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k ; 
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&Pre_CollapsedDataParallel_1_8373WEIGHTED_ROUND_ROBIN_Splitter_8552, peek_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child1_8454_8593_split[3], (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 4) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child1_8454_8593_split[3]) ; 
	}
	ENDFOR
}

void Butterfly_8554(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin65_Butterfly_Fiss_8571_8597_split[0]) ; 
		u_im = pop_float(&SplitJoin65_Butterfly_Fiss_8571_8597_split[0]) ; 
		t_re = pop_float(&SplitJoin65_Butterfly_Fiss_8571_8597_split[0]) ; 
		t_im = pop_float(&SplitJoin65_Butterfly_Fiss_8571_8597_split[0]) ; 
		wt_re = ((-0.9238796 * t_re) - (0.38268328 * t_im)) ; 
		wt_im = ((-0.9238796 * t_im) + (0.38268328 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin65_Butterfly_Fiss_8571_8597_join[0], u_re) ; 
		push_float(&SplitJoin65_Butterfly_Fiss_8571_8597_join[0], u_im) ; 
		push_float(&SplitJoin65_Butterfly_Fiss_8571_8597_join[0], t_re) ; 
		push_float(&SplitJoin65_Butterfly_Fiss_8571_8597_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8555(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin65_Butterfly_Fiss_8571_8597_split[1]) ; 
		u_im = pop_float(&SplitJoin65_Butterfly_Fiss_8571_8597_split[1]) ; 
		t_re = pop_float(&SplitJoin65_Butterfly_Fiss_8571_8597_split[1]) ; 
		t_im = pop_float(&SplitJoin65_Butterfly_Fiss_8571_8597_split[1]) ; 
		wt_re = ((-0.9238796 * t_re) - (0.38268328 * t_im)) ; 
		wt_im = ((-0.9238796 * t_im) + (0.38268328 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin65_Butterfly_Fiss_8571_8597_join[1], u_re) ; 
		push_float(&SplitJoin65_Butterfly_Fiss_8571_8597_join[1], u_im) ; 
		push_float(&SplitJoin65_Butterfly_Fiss_8571_8597_join[1], t_re) ; 
		push_float(&SplitJoin65_Butterfly_Fiss_8571_8597_join[1], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8552() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin65_Butterfly_Fiss_8571_8597_split[0], pop_float(&Pre_CollapsedDataParallel_1_8373WEIGHTED_ROUND_ROBIN_Splitter_8552));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin65_Butterfly_Fiss_8571_8597_split[1], pop_float(&Pre_CollapsedDataParallel_1_8373WEIGHTED_ROUND_ROBIN_Splitter_8552));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8553() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8553Post_CollapsedDataParallel_2_8374, pop_float(&SplitJoin65_Butterfly_Fiss_8571_8597_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8553Post_CollapsedDataParallel_2_8374, pop_float(&SplitJoin65_Butterfly_Fiss_8571_8597_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_8374(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
 {
		int kTimesWeights_i = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child1_8454_8593_join[3], peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_8553Post_CollapsedDataParallel_2_8374, (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8553Post_CollapsedDataParallel_2_8374) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8463() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child1_8454_8593_split[__iter_dec_], pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_Hier_8560_8587_split[1]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8464() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_Hier_8560_8587_join[1], pop_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child1_8454_8593_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_8460() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_Hier_8560_8587_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8459WEIGHTED_ROUND_ROBIN_Splitter_8460));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_Hier_8560_8587_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8459WEIGHTED_ROUND_ROBIN_Splitter_8460));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8465() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8465WEIGHTED_ROUND_ROBIN_Splitter_8466, pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_Hier_8560_8587_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8465WEIGHTED_ROUND_ROBIN_Splitter_8466, pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_Hier_8560_8587_join[1]));
		ENDFOR
	ENDFOR
}}

void Butterfly_8095(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[0]) ; 
		u_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[0]) ; 
		t_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[0]) ; 
		t_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[0]) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[0], u_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[0], u_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[0], t_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8096(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[1]) ; 
		u_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[1]) ; 
		t_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[1]) ; 
		t_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[1]) ; 
		wt_re = ((-4.371139E-8 * t_re) - (1.0 * t_im)) ; 
		wt_im = ((-4.371139E-8 * t_im) + (1.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[1], u_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[1], u_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[1], t_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[1], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8097(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[2]) ; 
		u_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[2]) ; 
		t_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[2]) ; 
		t_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[2]) ; 
		wt_re = ((0.70710677 * t_re) - (0.70710677 * t_im)) ; 
		wt_im = ((0.70710677 * t_im) + (0.70710677 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[2], u_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[2], u_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[2], t_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[2], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8098(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[3]) ; 
		u_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[3]) ; 
		t_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[3]) ; 
		t_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[3]) ; 
		wt_re = ((-0.70710677 * t_re) - (0.70710677 * t_im)) ; 
		wt_im = ((-0.70710677 * t_im) + (0.70710677 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[3], u_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[3], u_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[3], t_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[3], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8099(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[4]) ; 
		u_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[4]) ; 
		t_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[4]) ; 
		t_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[4]) ; 
		wt_re = ((0.9238795 * t_re) - (0.38268346 * t_im)) ; 
		wt_im = ((0.9238795 * t_im) + (0.38268346 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[4], u_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[4], u_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[4], t_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[4], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8100(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[5]) ; 
		u_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[5]) ; 
		t_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[5]) ; 
		t_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[5]) ; 
		wt_re = ((-0.38268352 * t_re) - (0.9238795 * t_im)) ; 
		wt_im = ((-0.38268352 * t_im) + (0.9238795 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[5], u_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[5], u_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[5], t_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[5], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8101(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[6]) ; 
		u_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[6]) ; 
		t_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[6]) ; 
		t_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[6]) ; 
		wt_re = ((0.38268343 * t_re) - (0.9238795 * t_im)) ; 
		wt_im = ((0.38268343 * t_im) + (0.9238795 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[6], u_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[6], u_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[6], t_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[6], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8102(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[7]) ; 
		u_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[7]) ; 
		t_re = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[7]) ; 
		t_im = pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[7]) ; 
		wt_re = ((-0.9238796 * t_re) - (0.38268328 * t_im)) ; 
		wt_im = ((-0.9238796 * t_im) + (0.38268328 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[7], u_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[7], u_im) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[7], t_re) ; 
		push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[7], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8467() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[__iter_dec_], pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_Hier_8562_8598_split[0]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8468() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_Hier_8562_8598_join[0], pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Butterfly_8103(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[0]) ; 
		u_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[0]) ; 
		t_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[0]) ; 
		t_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[0]) ; 
		wt_re = ((0.98078525 * t_re) - (0.19509032 * t_im)) ; 
		wt_im = ((0.98078525 * t_im) + (0.19509032 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[0], u_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[0], u_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[0], t_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[0], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8104(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[1]) ; 
		u_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[1]) ; 
		t_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[1]) ; 
		t_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[1]) ; 
		wt_re = ((-0.19509032 * t_re) - (0.98078525 * t_im)) ; 
		wt_im = ((-0.19509032 * t_im) + (0.98078525 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[1], u_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[1], u_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[1], t_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[1], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8105(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[2]) ; 
		u_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[2]) ; 
		t_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[2]) ; 
		t_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[2]) ; 
		wt_re = ((0.5555702 * t_re) - (0.83146966 * t_im)) ; 
		wt_im = ((0.5555702 * t_im) + (0.83146966 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[2], u_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[2], u_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[2], t_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[2], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8106(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[3]) ; 
		u_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[3]) ; 
		t_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[3]) ; 
		t_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[3]) ; 
		wt_re = ((-0.83146966 * t_re) - (0.5555702 * t_im)) ; 
		wt_im = ((-0.83146966 * t_im) + (0.5555702 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[3], u_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[3], u_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[3], t_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[3], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8107(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[4]) ; 
		u_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[4]) ; 
		t_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[4]) ; 
		t_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[4]) ; 
		wt_re = ((0.8314696 * t_re) - (0.55557024 * t_im)) ; 
		wt_im = ((0.8314696 * t_im) + (0.55557024 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[4], u_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[4], u_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[4], t_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[4], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8108(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[5]) ; 
		u_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[5]) ; 
		t_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[5]) ; 
		t_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[5]) ; 
		wt_re = ((-0.55557036 * t_re) - (0.83146954 * t_im)) ; 
		wt_im = ((-0.55557036 * t_im) + (0.83146954 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[5], u_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[5], u_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[5], t_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[5], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8109(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[6]) ; 
		u_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[6]) ; 
		t_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[6]) ; 
		t_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[6]) ; 
		wt_re = ((0.19509023 * t_re) - (0.9807853 * t_im)) ; 
		wt_im = ((0.19509023 * t_im) + (0.9807853 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[6], u_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[6], u_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[6], t_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[6], t_im) ; 
	}
	ENDFOR
}

void Butterfly_8110(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[7]) ; 
		u_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[7]) ; 
		t_re = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[7]) ; 
		t_im = pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[7]) ; 
		wt_re = ((-0.9807853 * t_re) - (0.19509031 * t_im)) ; 
		wt_im = ((-0.9807853 * t_im) + (0.19509031 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[7], u_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[7], u_im) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[7], t_re) ; 
		push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[7], t_im) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8469() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[__iter_dec_], pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_Hier_8562_8598_split[1]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8470() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_Hier_8562_8598_join[1], pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_8466() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_Hier_8562_8598_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8465WEIGHTED_ROUND_ROBIN_Splitter_8466));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_Hier_8562_8598_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8465WEIGHTED_ROUND_ROBIN_Splitter_8466));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8471() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8471BitReverse_8111, pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_Hier_8562_8598_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8471BitReverse_8111, pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_Hier_8562_8598_join[1]));
		ENDFOR
	ENDFOR
}}

int BitReverse_8111_bitrev(int inp, int numbits) {
	int rev = 0;
	FOR(int, i, 0,  < , numbits, i++) {
		rev = ((rev * 2) | (inp & 1)) ; 
		inp = (inp / 2) ; 
	}
	ENDFOR
	return rev;
}
void BitReverse_8111(){
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++) {
		FOR(int, i__conflict__0, 0,  < , 32, i__conflict__0++) {
			int br = 0;
			br = BitReverse_8111_bitrev(i__conflict__0, 5) ; 
			push_float(&BitReverse_8111FloatPrinter_8112, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_8471BitReverse_8111, (2 * br))) ; 
			push_float(&BitReverse_8111FloatPrinter_8112, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_8471BitReverse_8111, ((2 * br) + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8471BitReverse_8111) ; 
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8471BitReverse_8111) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void FloatPrinter_8112(){
	FOR(uint32_t, __iter_steady_, 0, <, 224, __iter_steady_++) {
		printf("%.10f", pop_float(&BitReverse_8111FloatPrinter_8112));
		printf("\n");
		printf("%.10f", pop_float(&BitReverse_8111FloatPrinter_8112));
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_float(&SplitJoin43_Butterfly_Fiss_8566_8591_join[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_8346WEIGHTED_ROUND_ROBIN_Splitter_8512);
	FOR(int, __iter_init_1_, 0, <, 4, __iter_init_1_++)
		init_buffer_float(&SplitJoin8_Butterfly_Fiss_8559_8581_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 4, __iter_init_2_++)
		init_buffer_float(&SplitJoin8_Butterfly_Fiss_8559_8581_join[__iter_init_2_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_8343WEIGHTED_ROUND_ROBIN_Splitter_8497);
	FOR(int, __iter_init_3_, 0, <, 4, __iter_init_3_++)
		init_buffer_float(&SplitJoin94_Butterfly_Fiss_8575_8586_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 2, __iter_init_4_++)
		init_buffer_float(&SplitJoin53_Butterfly_Fiss_8568_8594_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 4, __iter_init_5_++)
		init_buffer_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child0_8453_8588_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 4, __iter_init_6_++)
		init_buffer_float(&SplitJoin72_Butterfly_Fiss_8572_8582_join[__iter_init_6_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_8361WEIGHTED_ROUND_ROBIN_Splitter_8536);
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_Hier_8560_8587_join[__iter_init_8_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_8335WEIGHTED_ROUND_ROBIN_Splitter_8455);
	FOR(int, __iter_init_9_, 0, <, 2, __iter_init_9_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_7988_8383_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_Hier_8557_8578_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_Hier_8562_8598_split[__iter_init_10_]);
	ENDFOR
	init_buffer_float(&FloatSource_8030Pre_CollapsedDataParallel_1_8331);
	init_buffer_float(&BitReverse_8111FloatPrinter_8112);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8513Post_CollapsedDataParallel_2_8347);
	init_buffer_float(&Pre_CollapsedDataParallel_1_8355WEIGHTED_ROUND_ROBIN_Splitter_8528);
	init_buffer_float(&Pre_CollapsedDataParallel_1_8334WEIGHTED_ROUND_ROBIN_Splitter_8482);
	FOR(int, __iter_init_11_, 0, <, 7, __iter_init_11_++)
		init_buffer_float(&SplitJoin84_Butterfly_Fiss_8573_8583_split[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8549Post_CollapsedDataParallel_2_8371);
	FOR(int, __iter_init_12_, 0, <, 2, __iter_init_12_++)
		init_buffer_float(&SplitJoin43_Butterfly_Fiss_8566_8591_split[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_float(&SplitJoin47_Butterfly_Fiss_8567_8592_split[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 7, __iter_init_14_++)
		init_buffer_float(&SplitJoin84_Butterfly_Fiss_8573_8583_join[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 4, __iter_init_15_++)
		init_buffer_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child1_8454_8593_join[__iter_init_15_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_8370WEIGHTED_ROUND_ROBIN_Splitter_8548);
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_float(&SplitJoin61_Butterfly_Fiss_8570_8596_split[__iter_init_16_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8519Post_CollapsedDataParallel_2_8350);
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_float(&SplitJoin57_Butterfly_Fiss_8569_8595_join[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 7, __iter_init_18_++)
		init_buffer_float(&SplitJoin4_Butterfly_Fiss_8558_8579_split[__iter_init_18_]);
	ENDFOR
	FOR(int, __iter_init_19_, 0, <, 2, __iter_init_19_++)
		init_buffer_float(&SplitJoin86_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_child1_8429_8584_split[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 8, __iter_init_20_++)
		init_buffer_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child1_8564_8600_join[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 4, __iter_init_21_++)
		init_buffer_float(&SplitJoin88_Butterfly_Fiss_8574_8585_join[__iter_init_21_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_8367WEIGHTED_ROUND_ROBIN_Splitter_8544);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8504Post_CollapsedDataParallel_2_8338);
	FOR(int, __iter_init_22_, 0, <, 4, __iter_init_22_++)
		init_buffer_float(&SplitJoin88_Butterfly_Fiss_8574_8585_split[__iter_init_22_]);
	ENDFOR
	FOR(int, __iter_init_23_, 0, <, 2, __iter_init_23_++)
		init_buffer_float(&SplitJoin39_Butterfly_Fiss_8565_8590_join[__iter_init_23_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8545Post_CollapsedDataParallel_2_8368);
	FOR(int, __iter_init_24_, 0, <, 2, __iter_init_24_++)
		init_buffer_float(&SplitJoin65_Butterfly_Fiss_8571_8597_split[__iter_init_24_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8541Post_CollapsedDataParallel_2_8365);
	FOR(int, __iter_init_25_, 0, <, 2, __iter_init_25_++)
		init_buffer_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_Hier_8560_8587_split[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 2, __iter_init_26_++)
		init_buffer_float(&SplitJoin57_Butterfly_Fiss_8569_8595_split[__iter_init_26_]);
	ENDFOR
	FOR(int, __iter_init_27_, 0, <, 8, __iter_init_27_++)
		init_buffer_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_join[__iter_init_27_]);
	ENDFOR
	FOR(int, __iter_init_28_, 0, <, 2, __iter_init_28_++)
		init_buffer_float(&SplitJoin61_Butterfly_Fiss_8570_8596_join[__iter_init_28_]);
	ENDFOR
	FOR(int, __iter_init_29_, 0, <, 8, __iter_init_29_++)
		init_buffer_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_child0_8563_8599_split[__iter_init_29_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_8340WEIGHTED_ROUND_ROBIN_Splitter_8491);
	FOR(int, __iter_init_30_, 0, <, 7, __iter_init_30_++)
		init_buffer_float(&SplitJoin0_Butterfly_Fiss_8556_8577_join[__iter_init_30_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_8332WEIGHTED_ROUND_ROBIN_Splitter_8375);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8474Post_CollapsedDataParallel_2_8332);
	init_buffer_float(&Pre_CollapsedDataParallel_1_8364WEIGHTED_ROUND_ROBIN_Splitter_8540);
	init_buffer_float(&Pre_CollapsedDataParallel_1_8358WEIGHTED_ROUND_ROBIN_Splitter_8532);
	init_buffer_float(&Pre_CollapsedDataParallel_1_8337WEIGHTED_ROUND_ROBIN_Splitter_8503);
	FOR(int, __iter_init_31_, 0, <, 2, __iter_init_31_++)
		init_buffer_float(&SplitJoin86_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_child1_8429_8584_join[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 4, __iter_init_32_++)
		init_buffer_float(&SplitJoin94_Butterfly_Fiss_8575_8586_join[__iter_init_32_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_8331WEIGHTED_ROUND_ROBIN_Splitter_8473);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8483Post_CollapsedDataParallel_2_8335);
	init_buffer_float(&Post_CollapsedDataParallel_2_8338WEIGHTED_ROUND_ROBIN_Splitter_8457);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8498Post_CollapsedDataParallel_2_8344);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8459WEIGHTED_ROUND_ROBIN_Splitter_8460);
	FOR(int, __iter_init_33_, 0, <, 7, __iter_init_33_++)
		init_buffer_float(&SplitJoin4_Butterfly_Fiss_8558_8579_join[__iter_init_33_]);
	ENDFOR
	FOR(int, __iter_init_34_, 0, <, 2, __iter_init_34_++)
		init_buffer_float(&SplitJoin53_Butterfly_Fiss_8568_8594_join[__iter_init_34_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8492Post_CollapsedDataParallel_2_8341);
	FOR(int, __iter_init_35_, 0, <, 2, __iter_init_35_++)
		init_buffer_float(&SplitJoin39_Butterfly_Fiss_8565_8590_split[__iter_init_35_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_8349WEIGHTED_ROUND_ROBIN_Splitter_8518);
	init_buffer_float(&Pre_CollapsedDataParallel_1_8352WEIGHTED_ROUND_ROBIN_Splitter_8524);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8525Post_CollapsedDataParallel_2_8353);
	FOR(int, __iter_init_36_, 0, <, 2, __iter_init_36_++)
		init_buffer_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_8028_8389_Hier_Hier_8562_8598_join[__iter_init_36_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8529Post_CollapsedDataParallel_2_8356);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8537Post_CollapsedDataParallel_2_8362);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8465WEIGHTED_ROUND_ROBIN_Splitter_8466);
	init_buffer_float(&Pre_CollapsedDataParallel_1_8373WEIGHTED_ROUND_ROBIN_Splitter_8552);
	FOR(int, __iter_init_37_, 0, <, 7, __iter_init_37_++)
		init_buffer_float(&SplitJoin0_Butterfly_Fiss_8556_8577_split[__iter_init_37_]);
	ENDFOR
	FOR(int, __iter_init_38_, 0, <, 2, __iter_init_38_++)
		init_buffer_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_child0_8424_8580_join[__iter_init_38_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8553Post_CollapsedDataParallel_2_8374);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8471BitReverse_8111);
	FOR(int, __iter_init_39_, 0, <, 2, __iter_init_39_++)
		init_buffer_float(&SplitJoin47_Butterfly_Fiss_8567_8592_join[__iter_init_39_]);
	ENDFOR
	FOR(int, __iter_init_40_, 0, <, 2, __iter_init_40_++)
		init_buffer_float(&SplitJoin65_Butterfly_Fiss_8571_8597_join[__iter_init_40_]);
	ENDFOR
	FOR(int, __iter_init_41_, 0, <, 2, __iter_init_41_++)
		init_buffer_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_child0_8424_8580_split[__iter_init_41_]);
	ENDFOR
	FOR(int, __iter_init_42_, 0, <, 2, __iter_init_42_++)
		init_buffer_float(&SplitJoin14_Butterfly_Fiss_8561_8589_split[__iter_init_42_]);
	ENDFOR
	FOR(int, __iter_init_43_, 0, <, 2, __iter_init_43_++)
		init_buffer_float(&SplitJoin14_Butterfly_Fiss_8561_8589_join[__iter_init_43_]);
	ENDFOR
	FOR(int, __iter_init_44_, 0, <, 4, __iter_init_44_++)
		init_buffer_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child1_8454_8593_split[__iter_init_44_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8533Post_CollapsedDataParallel_2_8359);
	FOR(int, __iter_init_45_, 0, <, 4, __iter_init_45_++)
		init_buffer_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_8010_8387_Hier_child0_8453_8588_join[__iter_init_45_]);
	ENDFOR
	FOR(int, __iter_init_46_, 0, <, 4, __iter_init_46_++)
		init_buffer_float(&SplitJoin72_Butterfly_Fiss_8572_8582_split[__iter_init_46_]);
	ENDFOR
	FOR(int, __iter_init_47_, 0, <, 2, __iter_init_47_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_7988_8383_SplitJoin2_SplitJoin2_ComputeStage_7997_8385_Hier_8557_8578_join[__iter_init_47_]);
	ENDFOR
// --- init: FloatSource_8030
	 {
	FOR(int, i, 0,  < , 32, i++) {
		FloatSource_8030_s.A_re[i] = 0.0 ; 
		FloatSource_8030_s.A_im[i] = 0.0 ; 
	}
	ENDFOR
	FloatSource_8030_s.A_re[1] = 1.0 ; 
	FloatSource_8030_s.idx = 0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FloatSource_8030();
		Pre_CollapsedDataParallel_1_8331();
		WEIGHTED_ROUND_ROBIN_Splitter_8473();
			Butterfly_8475();
			Butterfly_8476();
			Butterfly_8477();
			Butterfly_8478();
			Butterfly_8479();
			Butterfly_8480();
			Butterfly_8481();
		WEIGHTED_ROUND_ROBIN_Joiner_8474();
		Post_CollapsedDataParallel_2_8332();
		WEIGHTED_ROUND_ROBIN_Splitter_8375();
			Pre_CollapsedDataParallel_1_8334();
			WEIGHTED_ROUND_ROBIN_Splitter_8482();
				Butterfly_8484();
				Butterfly_8485();
				Butterfly_8486();
				Butterfly_8487();
				Butterfly_8488();
				Butterfly_8489();
				Butterfly_8490();
			WEIGHTED_ROUND_ROBIN_Joiner_8483();
			Post_CollapsedDataParallel_2_8335();
			WEIGHTED_ROUND_ROBIN_Splitter_8455();
				Pre_CollapsedDataParallel_1_8340();
				WEIGHTED_ROUND_ROBIN_Splitter_8491();
					Butterfly_8493();
					Butterfly_8494();
					Butterfly_8495();
					Butterfly_8496();
				WEIGHTED_ROUND_ROBIN_Joiner_8492();
				Post_CollapsedDataParallel_2_8341();
				Pre_CollapsedDataParallel_1_8343();
				WEIGHTED_ROUND_ROBIN_Splitter_8497();
					Butterfly_8499();
					Butterfly_8500();
					Butterfly_8501();
					Butterfly_8502();
				WEIGHTED_ROUND_ROBIN_Joiner_8498();
				Post_CollapsedDataParallel_2_8344();
			WEIGHTED_ROUND_ROBIN_Joiner_8456();
			Pre_CollapsedDataParallel_1_8337();
			WEIGHTED_ROUND_ROBIN_Splitter_8503();
				Butterfly_8505();
				Butterfly_8506();
				Butterfly_8507();
				Butterfly_8508();
				Butterfly_8509();
				Butterfly_8510();
				Butterfly_8511();
			WEIGHTED_ROUND_ROBIN_Joiner_8504();
			Post_CollapsedDataParallel_2_8338();
			WEIGHTED_ROUND_ROBIN_Splitter_8457();
				Pre_CollapsedDataParallel_1_8346();
				WEIGHTED_ROUND_ROBIN_Splitter_8512();
					Butterfly_8514();
					Butterfly_8515();
					Butterfly_8516();
					Butterfly_8517();
				WEIGHTED_ROUND_ROBIN_Joiner_8513();
				Post_CollapsedDataParallel_2_8347();
				Pre_CollapsedDataParallel_1_8349();
				WEIGHTED_ROUND_ROBIN_Splitter_8518();
					Butterfly_8520();
					Butterfly_8521();
					Butterfly_8522();
					Butterfly_8523();
				WEIGHTED_ROUND_ROBIN_Joiner_8519();
				Post_CollapsedDataParallel_2_8350();
			WEIGHTED_ROUND_ROBIN_Joiner_8458();
		WEIGHTED_ROUND_ROBIN_Joiner_8459();
		WEIGHTED_ROUND_ROBIN_Splitter_8460();
			WEIGHTED_ROUND_ROBIN_Splitter_8461();
				Pre_CollapsedDataParallel_1_8352();
				WEIGHTED_ROUND_ROBIN_Splitter_8524();
					Butterfly_8526();
					Butterfly_8527();
				WEIGHTED_ROUND_ROBIN_Joiner_8525();
				Post_CollapsedDataParallel_2_8353();
				Pre_CollapsedDataParallel_1_8355();
				WEIGHTED_ROUND_ROBIN_Splitter_8528();
					Butterfly_8530();
					Butterfly_8531();
				WEIGHTED_ROUND_ROBIN_Joiner_8529();
				Post_CollapsedDataParallel_2_8356();
				Pre_CollapsedDataParallel_1_8358();
				WEIGHTED_ROUND_ROBIN_Splitter_8532();
					Butterfly_8534();
					Butterfly_8535();
				WEIGHTED_ROUND_ROBIN_Joiner_8533();
				Post_CollapsedDataParallel_2_8359();
				Pre_CollapsedDataParallel_1_8361();
				WEIGHTED_ROUND_ROBIN_Splitter_8536();
					Butterfly_8538();
					Butterfly_8539();
				WEIGHTED_ROUND_ROBIN_Joiner_8537();
				Post_CollapsedDataParallel_2_8362();
			WEIGHTED_ROUND_ROBIN_Joiner_8462();
			WEIGHTED_ROUND_ROBIN_Splitter_8463();
				Pre_CollapsedDataParallel_1_8364();
				WEIGHTED_ROUND_ROBIN_Splitter_8540();
					Butterfly_8542();
					Butterfly_8543();
				WEIGHTED_ROUND_ROBIN_Joiner_8541();
				Post_CollapsedDataParallel_2_8365();
				Pre_CollapsedDataParallel_1_8367();
				WEIGHTED_ROUND_ROBIN_Splitter_8544();
					Butterfly_8546();
					Butterfly_8547();
				WEIGHTED_ROUND_ROBIN_Joiner_8545();
				Post_CollapsedDataParallel_2_8368();
				Pre_CollapsedDataParallel_1_8370();
				WEIGHTED_ROUND_ROBIN_Splitter_8548();
					Butterfly_8550();
					Butterfly_8551();
				WEIGHTED_ROUND_ROBIN_Joiner_8549();
				Post_CollapsedDataParallel_2_8371();
				Pre_CollapsedDataParallel_1_8373();
				WEIGHTED_ROUND_ROBIN_Splitter_8552();
					Butterfly_8554();
					Butterfly_8555();
				WEIGHTED_ROUND_ROBIN_Joiner_8553();
				Post_CollapsedDataParallel_2_8374();
			WEIGHTED_ROUND_ROBIN_Joiner_8464();
		WEIGHTED_ROUND_ROBIN_Joiner_8465();
		WEIGHTED_ROUND_ROBIN_Splitter_8466();
			WEIGHTED_ROUND_ROBIN_Splitter_8467();
				Butterfly_8095();
				Butterfly_8096();
				Butterfly_8097();
				Butterfly_8098();
				Butterfly_8099();
				Butterfly_8100();
				Butterfly_8101();
				Butterfly_8102();
			WEIGHTED_ROUND_ROBIN_Joiner_8468();
			WEIGHTED_ROUND_ROBIN_Splitter_8469();
				Butterfly_8103();
				Butterfly_8104();
				Butterfly_8105();
				Butterfly_8106();
				Butterfly_8107();
				Butterfly_8108();
				Butterfly_8109();
				Butterfly_8110();
			WEIGHTED_ROUND_ROBIN_Joiner_8470();
		WEIGHTED_ROUND_ROBIN_Joiner_8471();
		BitReverse_8111();
		FloatPrinter_8112();
	ENDFOR
	return EXIT_SUCCESS;
}
