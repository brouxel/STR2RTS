#include "PEG3-FFT3.h"

buffer_float_t SplitJoin82_Butterfly_Fiss_11378_11389_join[3];
buffer_float_t SplitJoin78_Butterfly_Fiss_11377_11387_join[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11291BitReverse_10931;
buffer_float_t SplitJoin43_Butterfly_Fiss_11370_11395_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11357Post_CollapsedDataParallel_2_11194;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11349Post_CollapsedDataParallel_2_11188;
buffer_float_t Pre_CollapsedDataParallel_1_11166WEIGHTED_ROUND_ROBIN_Splitter_11318;
buffer_float_t SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child0_11367_11403_split[8];
buffer_float_t Pre_CollapsedDataParallel_1_11154WEIGHTED_ROUND_ROBIN_Splitter_11298;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11299Post_CollapsedDataParallel_2_11155;
buffer_float_t Post_CollapsedDataParallel_2_11158WEIGHTED_ROUND_ROBIN_Splitter_11277;
buffer_float_t Pre_CollapsedDataParallel_1_11163WEIGHTED_ROUND_ROBIN_Splitter_11308;
buffer_float_t SplitJoin61_Butterfly_Fiss_11374_11400_join[2];
buffer_float_t SplitJoin4_Butterfly_Fiss_11362_11383_split[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11285WEIGHTED_ROUND_ROBIN_Splitter_11286;
buffer_float_t SplitJoin53_Butterfly_Fiss_11372_11398_join[2];
buffer_float_t SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_Hier_11364_11391_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11345Post_CollapsedDataParallel_2_11185;
buffer_float_t SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_Hier_11366_11402_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11337Post_CollapsedDataParallel_2_11179;
buffer_float_t SplitJoin65_Butterfly_Fiss_11375_11401_split[2];
buffer_float_t Pre_CollapsedDataParallel_1_11172WEIGHTED_ROUND_ROBIN_Splitter_11328;
buffer_float_t SplitJoin39_Butterfly_Fiss_11369_11394_join[2];
buffer_float_t BitReverse_10931FloatPrinter_10932;
buffer_float_t SplitJoin87_Butterfly_Fiss_11379_11390_join[3];
buffer_float_t SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_child1_11274_11397_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11294Post_CollapsedDataParallel_2_11152;
buffer_float_t SplitJoin71_Butterfly_Fiss_11376_11386_split[3];
buffer_float_t SplitJoin0_Butterfly_Fiss_11360_11381_split[3];
buffer_float_t SplitJoin57_Butterfly_Fiss_11373_11399_split[2];
buffer_float_t SplitJoin14_Butterfly_Fiss_11365_11393_split[2];
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_10808_11203_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_Hier_11361_11382_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11319Post_CollapsedDataParallel_2_11167;
buffer_float_t SplitJoin87_Butterfly_Fiss_11379_11390_split[3];
buffer_float_t Pre_CollapsedDataParallel_1_11169WEIGHTED_ROUND_ROBIN_Splitter_11323;
buffer_float_t SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_child1_11274_11397_split[4];
buffer_float_t Pre_CollapsedDataParallel_1_11151WEIGHTED_ROUND_ROBIN_Splitter_11293;
buffer_float_t SplitJoin80_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_child1_11249_11388_join[2];
buffer_float_t Pre_CollapsedDataParallel_1_11184WEIGHTED_ROUND_ROBIN_Splitter_11344;
buffer_float_t Post_CollapsedDataParallel_2_11155WEIGHTED_ROUND_ROBIN_Splitter_11275;
buffer_float_t SplitJoin4_Butterfly_Fiss_11362_11383_join[3];
buffer_float_t Pre_CollapsedDataParallel_1_11157WEIGHTED_ROUND_ROBIN_Splitter_11313;
buffer_float_t SplitJoin8_Butterfly_Fiss_11363_11385_join[3];
buffer_float_t SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child1_11368_11404_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11314Post_CollapsedDataParallel_2_11158;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11341Post_CollapsedDataParallel_2_11182;
buffer_float_t SplitJoin71_Butterfly_Fiss_11376_11386_join[3];
buffer_float_t SplitJoin8_Butterfly_Fiss_11363_11385_split[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11309Post_CollapsedDataParallel_2_11164;
buffer_float_t SplitJoin39_Butterfly_Fiss_11369_11394_split[2];
buffer_float_t SplitJoin53_Butterfly_Fiss_11372_11398_split[2];
buffer_float_t SplitJoin0_Butterfly_Fiss_11360_11381_join[3];
buffer_float_t SplitJoin57_Butterfly_Fiss_11373_11399_join[2];
buffer_float_t Pre_CollapsedDataParallel_1_11178WEIGHTED_ROUND_ROBIN_Splitter_11336;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11304Post_CollapsedDataParallel_2_11161;
buffer_float_t SplitJoin14_Butterfly_Fiss_11365_11393_join[2];
buffer_float_t Post_CollapsedDataParallel_2_11152WEIGHTED_ROUND_ROBIN_Splitter_11195;
buffer_float_t SplitJoin78_Butterfly_Fiss_11377_11387_split[3];
buffer_float_t SplitJoin82_Butterfly_Fiss_11378_11389_split[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11353Post_CollapsedDataParallel_2_11191;
buffer_float_t SplitJoin65_Butterfly_Fiss_11375_11401_join[2];
buffer_float_t SplitJoin47_Butterfly_Fiss_11371_11396_join[2];
buffer_float_t SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child0_11367_11403_join[8];
buffer_float_t SplitJoin47_Butterfly_Fiss_11371_11396_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11279WEIGHTED_ROUND_ROBIN_Splitter_11280;
buffer_float_t SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_child0_11273_11392_join[4];
buffer_float_t SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_Hier_11366_11402_split[2];
buffer_float_t FloatSource_10850Pre_CollapsedDataParallel_1_11151;
buffer_float_t Pre_CollapsedDataParallel_1_11181WEIGHTED_ROUND_ROBIN_Splitter_11340;
buffer_float_t Pre_CollapsedDataParallel_1_11187WEIGHTED_ROUND_ROBIN_Splitter_11348;
buffer_float_t Pre_CollapsedDataParallel_1_11175WEIGHTED_ROUND_ROBIN_Splitter_11332;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11324Post_CollapsedDataParallel_2_11170;
buffer_float_t SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_child0_11244_11384_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11329Post_CollapsedDataParallel_2_11173;
buffer_float_t Pre_CollapsedDataParallel_1_11193WEIGHTED_ROUND_ROBIN_Splitter_11356;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_11333Post_CollapsedDataParallel_2_11176;
buffer_float_t SplitJoin43_Butterfly_Fiss_11370_11395_split[2];
buffer_float_t SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_child0_11244_11384_split[2];
buffer_float_t Pre_CollapsedDataParallel_1_11190WEIGHTED_ROUND_ROBIN_Splitter_11352;
buffer_float_t SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_10808_11203_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_Hier_11361_11382_join[2];
buffer_float_t Pre_CollapsedDataParallel_1_11160WEIGHTED_ROUND_ROBIN_Splitter_11303;
buffer_float_t SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_child0_11273_11392_split[4];
buffer_float_t SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_Hier_11364_11391_split[2];
buffer_float_t SplitJoin61_Butterfly_Fiss_11374_11400_split[2];
buffer_float_t SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child1_11368_11404_split[8];
buffer_float_t SplitJoin80_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_child1_11249_11388_split[2];


FloatSource_10850_t FloatSource_10850_s;

void FloatSource(buffer_float_t *chanout) {
		push_float(&(*chanout), FloatSource_10850_s.A_re[FloatSource_10850_s.idx]) ; 
		push_float(&(*chanout), FloatSource_10850_s.A_im[FloatSource_10850_s.idx]) ; 
		FloatSource_10850_s.idx++ ; 
		if((FloatSource_10850_s.idx >= 32)) {
			FloatSource_10850_s.idx = 0 ; 
		}
	}


void FloatSource_10850() {
	FOR(uint32_t, __iter_steady_, 0, <, 96, __iter_steady_++)
		FloatSource(&(FloatSource_10850Pre_CollapsedDataParallel_1_11151));
	ENDFOR
}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
		int partialSum_k = 0;
 {
		FOR(int, _k, 0,  < , 16, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = partialSum_k;
 {
			FOR(int, _i, 0,  < , 2, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + _j))) ; 
				}
				ENDFOR
			}
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 32) ; 
			}
			ENDFOR
		}
			partialSum_k = (partialSum_k + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_11151() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(FloatSource_10850Pre_CollapsedDataParallel_1_11151), &(Pre_CollapsedDataParallel_1_11151WEIGHTED_ROUND_ROBIN_Splitter_11293));
	ENDFOR
}

void Butterfly(buffer_float_t *chanin, buffer_float_t *chanout) {
		float u_re = 0.0;
		float u_im = 0.0;
		float t_re = 0.0;
		float t_im = 0.0;
		float wt_re = 0.0;
		float wt_im = 0.0;
		u_re = pop_float(&(*chanin)) ; 
		u_im = pop_float(&(*chanin)) ; 
		t_re = pop_float(&(*chanin)) ; 
		t_im = pop_float(&(*chanin)) ; 
		wt_re = ((1.0 * t_re) - (0.0 * t_im)) ; 
		wt_im = ((1.0 * t_im) + (0.0 * t_re)) ; 
		t_re = (u_re - wt_re) ; 
		t_im = (u_im - wt_im) ; 
		u_re = (u_re + wt_re) ; 
		u_im = (u_im + wt_im) ; 
		push_float(&(*chanout), u_re) ; 
		push_float(&(*chanout), u_im) ; 
		push_float(&(*chanout), t_re) ; 
		push_float(&(*chanout), t_im) ; 
	}


void Butterfly_11295() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_11360_11381_split[0]), &(SplitJoin0_Butterfly_Fiss_11360_11381_join[0]));
	ENDFOR
}

void Butterfly_11296() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_11360_11381_split[1]), &(SplitJoin0_Butterfly_Fiss_11360_11381_join[1]));
	ENDFOR
}

void Butterfly_11297() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		Butterfly(&(SplitJoin0_Butterfly_Fiss_11360_11381_split[2]), &(SplitJoin0_Butterfly_Fiss_11360_11381_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11293() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin0_Butterfly_Fiss_11360_11381_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_11151WEIGHTED_ROUND_ROBIN_Splitter_11293));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11294() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11294Post_CollapsedDataParallel_2_11152, pop_float(&SplitJoin0_Butterfly_Fiss_11360_11381_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
		int kTimesWeights_i = 0;
 {
		FOR(int, _k, 0,  < , 2, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 16, _i++) {
 {
				FOR(int, _j, 0,  < , 2, _j++) {
					push_float(&(*chanout), peek_float(&(*chanin), (kTimesWeights_i + (partialSum_i + _j)))) ; 
				}
				ENDFOR
			}
				partialSum_i = (partialSum_i + 4) ; 
			}
			ENDFOR
		}
			kTimesWeights_i = (kTimesWeights_i + 2) ; 
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_11152() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_11294Post_CollapsedDataParallel_2_11152), &(Post_CollapsedDataParallel_2_11152WEIGHTED_ROUND_ROBIN_Splitter_11195));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_11154() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_10808_11203_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_Hier_11361_11382_split[0]), &(Pre_CollapsedDataParallel_1_11154WEIGHTED_ROUND_ROBIN_Splitter_11298));
	ENDFOR
}

void Butterfly_11300() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_11362_11383_split[0]), &(SplitJoin4_Butterfly_Fiss_11362_11383_join[0]));
	ENDFOR
}

void Butterfly_11301() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_11362_11383_split[1]), &(SplitJoin4_Butterfly_Fiss_11362_11383_join[1]));
	ENDFOR
}

void Butterfly_11302() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Butterfly(&(SplitJoin4_Butterfly_Fiss_11362_11383_split[2]), &(SplitJoin4_Butterfly_Fiss_11362_11383_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11298() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin4_Butterfly_Fiss_11362_11383_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_11154WEIGHTED_ROUND_ROBIN_Splitter_11298));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11299() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11299Post_CollapsedDataParallel_2_11155, pop_float(&SplitJoin4_Butterfly_Fiss_11362_11383_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_11155() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_11299Post_CollapsedDataParallel_2_11155), &(Post_CollapsedDataParallel_2_11155WEIGHTED_ROUND_ROBIN_Splitter_11275));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_11160() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_child0_11244_11384_split[0]), &(Pre_CollapsedDataParallel_1_11160WEIGHTED_ROUND_ROBIN_Splitter_11303));
	ENDFOR
}

void Butterfly_11305() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin8_Butterfly_Fiss_11363_11385_split[0]), &(SplitJoin8_Butterfly_Fiss_11363_11385_join[0]));
	ENDFOR
}

void Butterfly_11306() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin8_Butterfly_Fiss_11363_11385_split[1]), &(SplitJoin8_Butterfly_Fiss_11363_11385_join[1]));
	ENDFOR
}

void Butterfly_11307() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin8_Butterfly_Fiss_11363_11385_split[2]), &(SplitJoin8_Butterfly_Fiss_11363_11385_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11303() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin8_Butterfly_Fiss_11363_11385_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_11160WEIGHTED_ROUND_ROBIN_Splitter_11303));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11304() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11304Post_CollapsedDataParallel_2_11161, pop_float(&SplitJoin8_Butterfly_Fiss_11363_11385_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_11161() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_11304Post_CollapsedDataParallel_2_11161), &(SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_child0_11244_11384_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_11163() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_child0_11244_11384_split[1]), &(Pre_CollapsedDataParallel_1_11163WEIGHTED_ROUND_ROBIN_Splitter_11308));
	ENDFOR
}

void Butterfly_11310() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin71_Butterfly_Fiss_11376_11386_split[0]), &(SplitJoin71_Butterfly_Fiss_11376_11386_join[0]));
	ENDFOR
}

void Butterfly_11311() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin71_Butterfly_Fiss_11376_11386_split[1]), &(SplitJoin71_Butterfly_Fiss_11376_11386_join[1]));
	ENDFOR
}

void Butterfly_11312() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin71_Butterfly_Fiss_11376_11386_split[2]), &(SplitJoin71_Butterfly_Fiss_11376_11386_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11308() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin71_Butterfly_Fiss_11376_11386_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_11163WEIGHTED_ROUND_ROBIN_Splitter_11308));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11309() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11309Post_CollapsedDataParallel_2_11164, pop_float(&SplitJoin71_Butterfly_Fiss_11376_11386_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_11164() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_11309Post_CollapsedDataParallel_2_11164), &(SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_child0_11244_11384_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11275() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_child0_11244_11384_split[0], pop_float(&Post_CollapsedDataParallel_2_11155WEIGHTED_ROUND_ROBIN_Splitter_11275));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_child0_11244_11384_split[1], pop_float(&Post_CollapsedDataParallel_2_11155WEIGHTED_ROUND_ROBIN_Splitter_11275));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11276() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_10808_11203_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_Hier_11361_11382_join[0], pop_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_child0_11244_11384_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_10808_11203_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_Hier_11361_11382_join[0], pop_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_child0_11244_11384_join[1]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_11157() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_10808_11203_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_Hier_11361_11382_split[1]), &(Pre_CollapsedDataParallel_1_11157WEIGHTED_ROUND_ROBIN_Splitter_11313));
	ENDFOR
}

void Butterfly_11315() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Butterfly(&(SplitJoin78_Butterfly_Fiss_11377_11387_split[0]), &(SplitJoin78_Butterfly_Fiss_11377_11387_join[0]));
	ENDFOR
}

void Butterfly_11316() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Butterfly(&(SplitJoin78_Butterfly_Fiss_11377_11387_split[1]), &(SplitJoin78_Butterfly_Fiss_11377_11387_join[1]));
	ENDFOR
}

void Butterfly_11317() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Butterfly(&(SplitJoin78_Butterfly_Fiss_11377_11387_split[2]), &(SplitJoin78_Butterfly_Fiss_11377_11387_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11313() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin78_Butterfly_Fiss_11377_11387_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_11157WEIGHTED_ROUND_ROBIN_Splitter_11313));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11314() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11314Post_CollapsedDataParallel_2_11158, pop_float(&SplitJoin78_Butterfly_Fiss_11377_11387_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_11158() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_11314Post_CollapsedDataParallel_2_11158), &(Post_CollapsedDataParallel_2_11158WEIGHTED_ROUND_ROBIN_Splitter_11277));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_11166() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin80_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_child1_11249_11388_split[0]), &(Pre_CollapsedDataParallel_1_11166WEIGHTED_ROUND_ROBIN_Splitter_11318));
	ENDFOR
}

void Butterfly_11320() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin82_Butterfly_Fiss_11378_11389_split[0]), &(SplitJoin82_Butterfly_Fiss_11378_11389_join[0]));
	ENDFOR
}

void Butterfly_11321() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin82_Butterfly_Fiss_11378_11389_split[1]), &(SplitJoin82_Butterfly_Fiss_11378_11389_join[1]));
	ENDFOR
}

void Butterfly_11322() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin82_Butterfly_Fiss_11378_11389_split[2]), &(SplitJoin82_Butterfly_Fiss_11378_11389_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11318() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin82_Butterfly_Fiss_11378_11389_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_11166WEIGHTED_ROUND_ROBIN_Splitter_11318));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11319() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11319Post_CollapsedDataParallel_2_11167, pop_float(&SplitJoin82_Butterfly_Fiss_11378_11389_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_11167() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_11319Post_CollapsedDataParallel_2_11167), &(SplitJoin80_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_child1_11249_11388_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_11169() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin80_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_child1_11249_11388_split[1]), &(Pre_CollapsedDataParallel_1_11169WEIGHTED_ROUND_ROBIN_Splitter_11323));
	ENDFOR
}

void Butterfly_11325() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin87_Butterfly_Fiss_11379_11390_split[0]), &(SplitJoin87_Butterfly_Fiss_11379_11390_join[0]));
	ENDFOR
}

void Butterfly_11326() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin87_Butterfly_Fiss_11379_11390_split[1]), &(SplitJoin87_Butterfly_Fiss_11379_11390_join[1]));
	ENDFOR
}

void Butterfly_11327() {
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		Butterfly(&(SplitJoin87_Butterfly_Fiss_11379_11390_split[2]), &(SplitJoin87_Butterfly_Fiss_11379_11390_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11323() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin87_Butterfly_Fiss_11379_11390_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_11169WEIGHTED_ROUND_ROBIN_Splitter_11323));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11324() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11324Post_CollapsedDataParallel_2_11170, pop_float(&SplitJoin87_Butterfly_Fiss_11379_11390_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_11170() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_11324Post_CollapsedDataParallel_2_11170), &(SplitJoin80_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_child1_11249_11388_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11277() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin80_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_child1_11249_11388_split[0], pop_float(&Post_CollapsedDataParallel_2_11158WEIGHTED_ROUND_ROBIN_Splitter_11277));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin80_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_child1_11249_11388_split[1], pop_float(&Post_CollapsedDataParallel_2_11158WEIGHTED_ROUND_ROBIN_Splitter_11277));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11278() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_10808_11203_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_Hier_11361_11382_join[1], pop_float(&SplitJoin80_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_child1_11249_11388_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 16, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_10808_11203_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_Hier_11361_11382_join[1], pop_float(&SplitJoin80_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_child1_11249_11388_join[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_11195() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_10808_11203_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_Hier_11361_11382_split[0], pop_float(&Post_CollapsedDataParallel_2_11152WEIGHTED_ROUND_ROBIN_Splitter_11195));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_10808_11203_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_Hier_11361_11382_split[1], pop_float(&Post_CollapsedDataParallel_2_11152WEIGHTED_ROUND_ROBIN_Splitter_11195));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11279() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11279WEIGHTED_ROUND_ROBIN_Splitter_11280, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_10808_11203_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_Hier_11361_11382_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11279WEIGHTED_ROUND_ROBIN_Splitter_11280, pop_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_10808_11203_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_Hier_11361_11382_join[1]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_11172() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_child0_11273_11392_split[0]), &(Pre_CollapsedDataParallel_1_11172WEIGHTED_ROUND_ROBIN_Splitter_11328));
	ENDFOR
}

void Butterfly_11330() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin14_Butterfly_Fiss_11365_11393_split[0]), &(SplitJoin14_Butterfly_Fiss_11365_11393_join[0]));
	ENDFOR
}

void Butterfly_11331() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin14_Butterfly_Fiss_11365_11393_split[1]), &(SplitJoin14_Butterfly_Fiss_11365_11393_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11328() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin14_Butterfly_Fiss_11365_11393_split[0], pop_float(&Pre_CollapsedDataParallel_1_11172WEIGHTED_ROUND_ROBIN_Splitter_11328));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin14_Butterfly_Fiss_11365_11393_split[1], pop_float(&Pre_CollapsedDataParallel_1_11172WEIGHTED_ROUND_ROBIN_Splitter_11328));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11329() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11329Post_CollapsedDataParallel_2_11173, pop_float(&SplitJoin14_Butterfly_Fiss_11365_11393_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11329Post_CollapsedDataParallel_2_11173, pop_float(&SplitJoin14_Butterfly_Fiss_11365_11393_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_11173() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_11329Post_CollapsedDataParallel_2_11173), &(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_child0_11273_11392_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_11175() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_child0_11273_11392_split[1]), &(Pre_CollapsedDataParallel_1_11175WEIGHTED_ROUND_ROBIN_Splitter_11332));
	ENDFOR
}

void Butterfly_11334() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin39_Butterfly_Fiss_11369_11394_split[0]), &(SplitJoin39_Butterfly_Fiss_11369_11394_join[0]));
	ENDFOR
}

void Butterfly_11335() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin39_Butterfly_Fiss_11369_11394_split[1]), &(SplitJoin39_Butterfly_Fiss_11369_11394_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11332() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin39_Butterfly_Fiss_11369_11394_split[0], pop_float(&Pre_CollapsedDataParallel_1_11175WEIGHTED_ROUND_ROBIN_Splitter_11332));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin39_Butterfly_Fiss_11369_11394_split[1], pop_float(&Pre_CollapsedDataParallel_1_11175WEIGHTED_ROUND_ROBIN_Splitter_11332));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11333() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11333Post_CollapsedDataParallel_2_11176, pop_float(&SplitJoin39_Butterfly_Fiss_11369_11394_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11333Post_CollapsedDataParallel_2_11176, pop_float(&SplitJoin39_Butterfly_Fiss_11369_11394_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_11176() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_11333Post_CollapsedDataParallel_2_11176), &(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_child0_11273_11392_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_11178() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_child0_11273_11392_split[2]), &(Pre_CollapsedDataParallel_1_11178WEIGHTED_ROUND_ROBIN_Splitter_11336));
	ENDFOR
}

void Butterfly_11338() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin43_Butterfly_Fiss_11370_11395_split[0]), &(SplitJoin43_Butterfly_Fiss_11370_11395_join[0]));
	ENDFOR
}

void Butterfly_11339() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin43_Butterfly_Fiss_11370_11395_split[1]), &(SplitJoin43_Butterfly_Fiss_11370_11395_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11336() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin43_Butterfly_Fiss_11370_11395_split[0], pop_float(&Pre_CollapsedDataParallel_1_11178WEIGHTED_ROUND_ROBIN_Splitter_11336));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin43_Butterfly_Fiss_11370_11395_split[1], pop_float(&Pre_CollapsedDataParallel_1_11178WEIGHTED_ROUND_ROBIN_Splitter_11336));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11337() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11337Post_CollapsedDataParallel_2_11179, pop_float(&SplitJoin43_Butterfly_Fiss_11370_11395_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11337Post_CollapsedDataParallel_2_11179, pop_float(&SplitJoin43_Butterfly_Fiss_11370_11395_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_11179() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_11337Post_CollapsedDataParallel_2_11179), &(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_child0_11273_11392_join[2]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_11181() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_child0_11273_11392_split[3]), &(Pre_CollapsedDataParallel_1_11181WEIGHTED_ROUND_ROBIN_Splitter_11340));
	ENDFOR
}

void Butterfly_11342() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin47_Butterfly_Fiss_11371_11396_split[0]), &(SplitJoin47_Butterfly_Fiss_11371_11396_join[0]));
	ENDFOR
}

void Butterfly_11343() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin47_Butterfly_Fiss_11371_11396_split[1]), &(SplitJoin47_Butterfly_Fiss_11371_11396_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11340() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin47_Butterfly_Fiss_11371_11396_split[0], pop_float(&Pre_CollapsedDataParallel_1_11181WEIGHTED_ROUND_ROBIN_Splitter_11340));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin47_Butterfly_Fiss_11371_11396_split[1], pop_float(&Pre_CollapsedDataParallel_1_11181WEIGHTED_ROUND_ROBIN_Splitter_11340));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11341() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11341Post_CollapsedDataParallel_2_11182, pop_float(&SplitJoin47_Butterfly_Fiss_11371_11396_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11341Post_CollapsedDataParallel_2_11182, pop_float(&SplitJoin47_Butterfly_Fiss_11371_11396_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_11182() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_11341Post_CollapsedDataParallel_2_11182), &(SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_child0_11273_11392_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11281() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_child0_11273_11392_split[__iter_dec_], pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_Hier_11364_11391_split[0]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11282() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_Hier_11364_11391_join[0], pop_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_child0_11273_11392_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_11184() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_child1_11274_11397_split[0]), &(Pre_CollapsedDataParallel_1_11184WEIGHTED_ROUND_ROBIN_Splitter_11344));
	ENDFOR
}

void Butterfly_11346() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin53_Butterfly_Fiss_11372_11398_split[0]), &(SplitJoin53_Butterfly_Fiss_11372_11398_join[0]));
	ENDFOR
}

void Butterfly_11347() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin53_Butterfly_Fiss_11372_11398_split[1]), &(SplitJoin53_Butterfly_Fiss_11372_11398_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11344() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin53_Butterfly_Fiss_11372_11398_split[0], pop_float(&Pre_CollapsedDataParallel_1_11184WEIGHTED_ROUND_ROBIN_Splitter_11344));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin53_Butterfly_Fiss_11372_11398_split[1], pop_float(&Pre_CollapsedDataParallel_1_11184WEIGHTED_ROUND_ROBIN_Splitter_11344));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11345() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11345Post_CollapsedDataParallel_2_11185, pop_float(&SplitJoin53_Butterfly_Fiss_11372_11398_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11345Post_CollapsedDataParallel_2_11185, pop_float(&SplitJoin53_Butterfly_Fiss_11372_11398_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_11185() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_11345Post_CollapsedDataParallel_2_11185), &(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_child1_11274_11397_join[0]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_11187() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_child1_11274_11397_split[1]), &(Pre_CollapsedDataParallel_1_11187WEIGHTED_ROUND_ROBIN_Splitter_11348));
	ENDFOR
}

void Butterfly_11350() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin57_Butterfly_Fiss_11373_11399_split[0]), &(SplitJoin57_Butterfly_Fiss_11373_11399_join[0]));
	ENDFOR
}

void Butterfly_11351() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin57_Butterfly_Fiss_11373_11399_split[1]), &(SplitJoin57_Butterfly_Fiss_11373_11399_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11348() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin57_Butterfly_Fiss_11373_11399_split[0], pop_float(&Pre_CollapsedDataParallel_1_11187WEIGHTED_ROUND_ROBIN_Splitter_11348));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin57_Butterfly_Fiss_11373_11399_split[1], pop_float(&Pre_CollapsedDataParallel_1_11187WEIGHTED_ROUND_ROBIN_Splitter_11348));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11349() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11349Post_CollapsedDataParallel_2_11188, pop_float(&SplitJoin57_Butterfly_Fiss_11373_11399_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11349Post_CollapsedDataParallel_2_11188, pop_float(&SplitJoin57_Butterfly_Fiss_11373_11399_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_11188() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_11349Post_CollapsedDataParallel_2_11188), &(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_child1_11274_11397_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_11190() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_child1_11274_11397_split[2]), &(Pre_CollapsedDataParallel_1_11190WEIGHTED_ROUND_ROBIN_Splitter_11352));
	ENDFOR
}

void Butterfly_11354() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin61_Butterfly_Fiss_11374_11400_split[0]), &(SplitJoin61_Butterfly_Fiss_11374_11400_join[0]));
	ENDFOR
}

void Butterfly_11355() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin61_Butterfly_Fiss_11374_11400_split[1]), &(SplitJoin61_Butterfly_Fiss_11374_11400_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11352() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin61_Butterfly_Fiss_11374_11400_split[0], pop_float(&Pre_CollapsedDataParallel_1_11190WEIGHTED_ROUND_ROBIN_Splitter_11352));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin61_Butterfly_Fiss_11374_11400_split[1], pop_float(&Pre_CollapsedDataParallel_1_11190WEIGHTED_ROUND_ROBIN_Splitter_11352));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11353() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11353Post_CollapsedDataParallel_2_11191, pop_float(&SplitJoin61_Butterfly_Fiss_11374_11400_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11353Post_CollapsedDataParallel_2_11191, pop_float(&SplitJoin61_Butterfly_Fiss_11374_11400_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_11191() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_11353Post_CollapsedDataParallel_2_11191), &(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_child1_11274_11397_join[2]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_11193() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_child1_11274_11397_split[3]), &(Pre_CollapsedDataParallel_1_11193WEIGHTED_ROUND_ROBIN_Splitter_11356));
	ENDFOR
}

void Butterfly_11358() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin65_Butterfly_Fiss_11375_11401_split[0]), &(SplitJoin65_Butterfly_Fiss_11375_11401_join[0]));
	ENDFOR
}

void Butterfly_11359() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin65_Butterfly_Fiss_11375_11401_split[1]), &(SplitJoin65_Butterfly_Fiss_11375_11401_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11356() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin65_Butterfly_Fiss_11375_11401_split[0], pop_float(&Pre_CollapsedDataParallel_1_11193WEIGHTED_ROUND_ROBIN_Splitter_11356));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin65_Butterfly_Fiss_11375_11401_split[1], pop_float(&Pre_CollapsedDataParallel_1_11193WEIGHTED_ROUND_ROBIN_Splitter_11356));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11357() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11357Post_CollapsedDataParallel_2_11194, pop_float(&SplitJoin65_Butterfly_Fiss_11375_11401_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11357Post_CollapsedDataParallel_2_11194, pop_float(&SplitJoin65_Butterfly_Fiss_11375_11401_join[1]));
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_11194() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_11357Post_CollapsedDataParallel_2_11194), &(SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_child1_11274_11397_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11283() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_child1_11274_11397_split[__iter_dec_], pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_Hier_11364_11391_split[1]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11284() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_Hier_11364_11391_join[1], pop_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_child1_11274_11397_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_11280() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_Hier_11364_11391_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11279WEIGHTED_ROUND_ROBIN_Splitter_11280));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_Hier_11364_11391_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11279WEIGHTED_ROUND_ROBIN_Splitter_11280));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11285() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11285WEIGHTED_ROUND_ROBIN_Splitter_11286, pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_Hier_11364_11391_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11285WEIGHTED_ROUND_ROBIN_Splitter_11286, pop_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_Hier_11364_11391_join[1]));
		ENDFOR
	ENDFOR
}}

void Butterfly_10915() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child0_11367_11403_split[0]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child0_11367_11403_join[0]));
	ENDFOR
}

void Butterfly_10916() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child0_11367_11403_split[1]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child0_11367_11403_join[1]));
	ENDFOR
}

void Butterfly_10917() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child0_11367_11403_split[2]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child0_11367_11403_join[2]));
	ENDFOR
}

void Butterfly_10918() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child0_11367_11403_split[3]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child0_11367_11403_join[3]));
	ENDFOR
}

void Butterfly_10919() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child0_11367_11403_split[4]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child0_11367_11403_join[4]));
	ENDFOR
}

void Butterfly_10920() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child0_11367_11403_split[5]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child0_11367_11403_join[5]));
	ENDFOR
}

void Butterfly_10921() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child0_11367_11403_split[6]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child0_11367_11403_join[6]));
	ENDFOR
}

void Butterfly_10922() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child0_11367_11403_split[7]), &(SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child0_11367_11403_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11287() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child0_11367_11403_split[__iter_dec_], pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_Hier_11366_11402_split[0]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11288() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_Hier_11366_11402_join[0], pop_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child0_11367_11403_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Butterfly_10923() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child1_11368_11404_split[0]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child1_11368_11404_join[0]));
	ENDFOR
}

void Butterfly_10924() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child1_11368_11404_split[1]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child1_11368_11404_join[1]));
	ENDFOR
}

void Butterfly_10925() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child1_11368_11404_split[2]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child1_11368_11404_join[2]));
	ENDFOR
}

void Butterfly_10926() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child1_11368_11404_split[3]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child1_11368_11404_join[3]));
	ENDFOR
}

void Butterfly_10927() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child1_11368_11404_split[4]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child1_11368_11404_join[4]));
	ENDFOR
}

void Butterfly_10928() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child1_11368_11404_split[5]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child1_11368_11404_join[5]));
	ENDFOR
}

void Butterfly_10929() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child1_11368_11404_split[6]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child1_11368_11404_join[6]));
	ENDFOR
}

void Butterfly_10930() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		Butterfly(&(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child1_11368_11404_split[7]), &(SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child1_11368_11404_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_11289() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child1_11368_11404_split[__iter_dec_], pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_Hier_11366_11402_split[1]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11290() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 4, __iter_tok_++)
				push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_Hier_11366_11402_join[1], pop_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child1_11368_11404_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_11286() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_Hier_11366_11402_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11285WEIGHTED_ROUND_ROBIN_Splitter_11286));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_Hier_11366_11402_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_11285WEIGHTED_ROUND_ROBIN_Splitter_11286));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_11291() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11291BitReverse_10931, pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_Hier_11366_11402_join[0]));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 32, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_11291BitReverse_10931, pop_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_Hier_11366_11402_join[1]));
		ENDFOR
	ENDFOR
}}

int BitReverse_10931_bitrev(int inp, int numbits) {
	int rev = 0;
	FOR(int, i, 0,  < , numbits, i++) {
		rev = ((rev * 2) | (inp & 1)) ; 
		inp = (inp / 2) ; 
	}
	ENDFOR
	return rev;
}
void BitReverse(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, i__conflict__0, 0,  < , 32, i__conflict__0++) {
			int br = 0;
			br = BitReverse_10931_bitrev(i__conflict__0, 5) ; 
			push_float(&(*chanout), peek_float(&(*chanin), (2 * br))) ; 
			push_float(&(*chanout), peek_float(&(*chanin), ((2 * br) + 1))) ; 
		}
		ENDFOR
		FOR(int, i, 0,  < , 32, i++) {
			pop_float(&(*chanin)) ; 
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void BitReverse_10931() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		BitReverse(&(WEIGHTED_ROUND_ROBIN_Joiner_11291BitReverse_10931), &(BitReverse_10931FloatPrinter_10932));
	ENDFOR
}

void FloatPrinter(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void FloatPrinter_10932() {
	FOR(uint32_t, __iter_steady_, 0, <, 96, __iter_steady_++)
		FloatPrinter(&(BitReverse_10931FloatPrinter_10932));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 3, __iter_init_0_++)
		init_buffer_float(&SplitJoin82_Butterfly_Fiss_11378_11389_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 3, __iter_init_1_++)
		init_buffer_float(&SplitJoin78_Butterfly_Fiss_11377_11387_join[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11291BitReverse_10931);
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_float(&SplitJoin43_Butterfly_Fiss_11370_11395_join[__iter_init_2_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11357Post_CollapsedDataParallel_2_11194);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11349Post_CollapsedDataParallel_2_11188);
	init_buffer_float(&Pre_CollapsedDataParallel_1_11166WEIGHTED_ROUND_ROBIN_Splitter_11318);
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child0_11367_11403_split[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_11154WEIGHTED_ROUND_ROBIN_Splitter_11298);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11299Post_CollapsedDataParallel_2_11155);
	init_buffer_float(&Post_CollapsedDataParallel_2_11158WEIGHTED_ROUND_ROBIN_Splitter_11277);
	init_buffer_float(&Pre_CollapsedDataParallel_1_11163WEIGHTED_ROUND_ROBIN_Splitter_11308);
	FOR(int, __iter_init_4_, 0, <, 2, __iter_init_4_++)
		init_buffer_float(&SplitJoin61_Butterfly_Fiss_11374_11400_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 3, __iter_init_5_++)
		init_buffer_float(&SplitJoin4_Butterfly_Fiss_11362_11383_split[__iter_init_5_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11285WEIGHTED_ROUND_ROBIN_Splitter_11286);
	FOR(int, __iter_init_6_, 0, <, 2, __iter_init_6_++)
		init_buffer_float(&SplitJoin53_Butterfly_Fiss_11372_11398_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 2, __iter_init_7_++)
		init_buffer_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_Hier_11364_11391_join[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11345Post_CollapsedDataParallel_2_11185);
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_Hier_11366_11402_join[__iter_init_8_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11337Post_CollapsedDataParallel_2_11179);
	FOR(int, __iter_init_9_, 0, <, 2, __iter_init_9_++)
		init_buffer_float(&SplitJoin65_Butterfly_Fiss_11375_11401_split[__iter_init_9_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_11172WEIGHTED_ROUND_ROBIN_Splitter_11328);
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_float(&SplitJoin39_Butterfly_Fiss_11369_11394_join[__iter_init_10_]);
	ENDFOR
	init_buffer_float(&BitReverse_10931FloatPrinter_10932);
	FOR(int, __iter_init_11_, 0, <, 3, __iter_init_11_++)
		init_buffer_float(&SplitJoin87_Butterfly_Fiss_11379_11390_join[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 4, __iter_init_12_++)
		init_buffer_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_child1_11274_11397_join[__iter_init_12_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11294Post_CollapsedDataParallel_2_11152);
	FOR(int, __iter_init_13_, 0, <, 3, __iter_init_13_++)
		init_buffer_float(&SplitJoin71_Butterfly_Fiss_11376_11386_split[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 3, __iter_init_14_++)
		init_buffer_float(&SplitJoin0_Butterfly_Fiss_11360_11381_split[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 2, __iter_init_15_++)
		init_buffer_float(&SplitJoin57_Butterfly_Fiss_11373_11399_split[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_float(&SplitJoin14_Butterfly_Fiss_11365_11393_split[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_10808_11203_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_Hier_11361_11382_split[__iter_init_17_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11319Post_CollapsedDataParallel_2_11167);
	FOR(int, __iter_init_18_, 0, <, 3, __iter_init_18_++)
		init_buffer_float(&SplitJoin87_Butterfly_Fiss_11379_11390_split[__iter_init_18_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_11169WEIGHTED_ROUND_ROBIN_Splitter_11323);
	FOR(int, __iter_init_19_, 0, <, 4, __iter_init_19_++)
		init_buffer_float(&SplitJoin51_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_child1_11274_11397_split[__iter_init_19_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_11151WEIGHTED_ROUND_ROBIN_Splitter_11293);
	FOR(int, __iter_init_20_, 0, <, 2, __iter_init_20_++)
		init_buffer_float(&SplitJoin80_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_child1_11249_11388_join[__iter_init_20_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_11184WEIGHTED_ROUND_ROBIN_Splitter_11344);
	init_buffer_float(&Post_CollapsedDataParallel_2_11155WEIGHTED_ROUND_ROBIN_Splitter_11275);
	FOR(int, __iter_init_21_, 0, <, 3, __iter_init_21_++)
		init_buffer_float(&SplitJoin4_Butterfly_Fiss_11362_11383_join[__iter_init_21_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_11157WEIGHTED_ROUND_ROBIN_Splitter_11313);
	FOR(int, __iter_init_22_, 0, <, 3, __iter_init_22_++)
		init_buffer_float(&SplitJoin8_Butterfly_Fiss_11363_11385_join[__iter_init_22_]);
	ENDFOR
	FOR(int, __iter_init_23_, 0, <, 8, __iter_init_23_++)
		init_buffer_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child1_11368_11404_join[__iter_init_23_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11314Post_CollapsedDataParallel_2_11158);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11341Post_CollapsedDataParallel_2_11182);
	FOR(int, __iter_init_24_, 0, <, 3, __iter_init_24_++)
		init_buffer_float(&SplitJoin71_Butterfly_Fiss_11376_11386_join[__iter_init_24_]);
	ENDFOR
	FOR(int, __iter_init_25_, 0, <, 3, __iter_init_25_++)
		init_buffer_float(&SplitJoin8_Butterfly_Fiss_11363_11385_split[__iter_init_25_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11309Post_CollapsedDataParallel_2_11164);
	FOR(int, __iter_init_26_, 0, <, 2, __iter_init_26_++)
		init_buffer_float(&SplitJoin39_Butterfly_Fiss_11369_11394_split[__iter_init_26_]);
	ENDFOR
	FOR(int, __iter_init_27_, 0, <, 2, __iter_init_27_++)
		init_buffer_float(&SplitJoin53_Butterfly_Fiss_11372_11398_split[__iter_init_27_]);
	ENDFOR
	FOR(int, __iter_init_28_, 0, <, 3, __iter_init_28_++)
		init_buffer_float(&SplitJoin0_Butterfly_Fiss_11360_11381_join[__iter_init_28_]);
	ENDFOR
	FOR(int, __iter_init_29_, 0, <, 2, __iter_init_29_++)
		init_buffer_float(&SplitJoin57_Butterfly_Fiss_11373_11399_join[__iter_init_29_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_11178WEIGHTED_ROUND_ROBIN_Splitter_11336);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11304Post_CollapsedDataParallel_2_11161);
	FOR(int, __iter_init_30_, 0, <, 2, __iter_init_30_++)
		init_buffer_float(&SplitJoin14_Butterfly_Fiss_11365_11393_join[__iter_init_30_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_11152WEIGHTED_ROUND_ROBIN_Splitter_11195);
	FOR(int, __iter_init_31_, 0, <, 3, __iter_init_31_++)
		init_buffer_float(&SplitJoin78_Butterfly_Fiss_11377_11387_split[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 3, __iter_init_32_++)
		init_buffer_float(&SplitJoin82_Butterfly_Fiss_11378_11389_split[__iter_init_32_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11353Post_CollapsedDataParallel_2_11191);
	FOR(int, __iter_init_33_, 0, <, 2, __iter_init_33_++)
		init_buffer_float(&SplitJoin65_Butterfly_Fiss_11375_11401_join[__iter_init_33_]);
	ENDFOR
	FOR(int, __iter_init_34_, 0, <, 2, __iter_init_34_++)
		init_buffer_float(&SplitJoin47_Butterfly_Fiss_11371_11396_join[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 8, __iter_init_35_++)
		init_buffer_float(&SplitJoin18_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child0_11367_11403_join[__iter_init_35_]);
	ENDFOR
	FOR(int, __iter_init_36_, 0, <, 2, __iter_init_36_++)
		init_buffer_float(&SplitJoin47_Butterfly_Fiss_11371_11396_split[__iter_init_36_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11279WEIGHTED_ROUND_ROBIN_Splitter_11280);
	FOR(int, __iter_init_37_, 0, <, 4, __iter_init_37_++)
		init_buffer_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_child0_11273_11392_join[__iter_init_37_]);
	ENDFOR
	FOR(int, __iter_init_38_, 0, <, 2, __iter_init_38_++)
		init_buffer_float(&SplitJoin16_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_Hier_11366_11402_split[__iter_init_38_]);
	ENDFOR
	init_buffer_float(&FloatSource_10850Pre_CollapsedDataParallel_1_11151);
	init_buffer_float(&Pre_CollapsedDataParallel_1_11181WEIGHTED_ROUND_ROBIN_Splitter_11340);
	init_buffer_float(&Pre_CollapsedDataParallel_1_11187WEIGHTED_ROUND_ROBIN_Splitter_11348);
	init_buffer_float(&Pre_CollapsedDataParallel_1_11175WEIGHTED_ROUND_ROBIN_Splitter_11332);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11324Post_CollapsedDataParallel_2_11170);
	FOR(int, __iter_init_39_, 0, <, 2, __iter_init_39_++)
		init_buffer_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_child0_11244_11384_join[__iter_init_39_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11329Post_CollapsedDataParallel_2_11173);
	init_buffer_float(&Pre_CollapsedDataParallel_1_11193WEIGHTED_ROUND_ROBIN_Splitter_11356);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_11333Post_CollapsedDataParallel_2_11176);
	FOR(int, __iter_init_40_, 0, <, 2, __iter_init_40_++)
		init_buffer_float(&SplitJoin43_Butterfly_Fiss_11370_11395_split[__iter_init_40_]);
	ENDFOR
	FOR(int, __iter_init_41_, 0, <, 2, __iter_init_41_++)
		init_buffer_float(&SplitJoin6_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_child0_11244_11384_split[__iter_init_41_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_11190WEIGHTED_ROUND_ROBIN_Splitter_11352);
	FOR(int, __iter_init_42_, 0, <, 2, __iter_init_42_++)
		init_buffer_float(&SplitJoin2_SplitJoin0_SplitJoin0_ComputeStage_10808_11203_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_Hier_11361_11382_join[__iter_init_42_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_11160WEIGHTED_ROUND_ROBIN_Splitter_11303);
	FOR(int, __iter_init_43_, 0, <, 4, __iter_init_43_++)
		init_buffer_float(&SplitJoin12_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_child0_11273_11392_split[__iter_init_43_]);
	ENDFOR
	FOR(int, __iter_init_44_, 0, <, 2, __iter_init_44_++)
		init_buffer_float(&SplitJoin10_SplitJoin4_SplitJoin4_ComputeStage_10830_11207_Hier_Hier_11364_11391_split[__iter_init_44_]);
	ENDFOR
	FOR(int, __iter_init_45_, 0, <, 2, __iter_init_45_++)
		init_buffer_float(&SplitJoin61_Butterfly_Fiss_11374_11400_split[__iter_init_45_]);
	ENDFOR
	FOR(int, __iter_init_46_, 0, <, 8, __iter_init_46_++)
		init_buffer_float(&SplitJoin28_SplitJoin6_SplitJoin6_LastComputeStage_10848_11209_Hier_child1_11368_11404_split[__iter_init_46_]);
	ENDFOR
	FOR(int, __iter_init_47_, 0, <, 2, __iter_init_47_++)
		init_buffer_float(&SplitJoin80_SplitJoin2_SplitJoin2_ComputeStage_10817_11205_child1_11249_11388_split[__iter_init_47_]);
	ENDFOR
// --- init: FloatSource_10850
	 {
	FOR(int, i, 0,  < , 32, i++) {
		FloatSource_10850_s.A_re[i] = 0.0 ; 
		FloatSource_10850_s.A_im[i] = 0.0 ; 
	}
	ENDFOR
	FloatSource_10850_s.A_re[1] = 1.0 ; 
	FloatSource_10850_s.idx = 0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FloatSource_10850();
		Pre_CollapsedDataParallel_1_11151();
		WEIGHTED_ROUND_ROBIN_Splitter_11293();
			Butterfly_11295();
			Butterfly_11296();
			Butterfly_11297();
		WEIGHTED_ROUND_ROBIN_Joiner_11294();
		Post_CollapsedDataParallel_2_11152();
		WEIGHTED_ROUND_ROBIN_Splitter_11195();
			Pre_CollapsedDataParallel_1_11154();
			WEIGHTED_ROUND_ROBIN_Splitter_11298();
				Butterfly_11300();
				Butterfly_11301();
				Butterfly_11302();
			WEIGHTED_ROUND_ROBIN_Joiner_11299();
			Post_CollapsedDataParallel_2_11155();
			WEIGHTED_ROUND_ROBIN_Splitter_11275();
				Pre_CollapsedDataParallel_1_11160();
				WEIGHTED_ROUND_ROBIN_Splitter_11303();
					Butterfly_11305();
					Butterfly_11306();
					Butterfly_11307();
				WEIGHTED_ROUND_ROBIN_Joiner_11304();
				Post_CollapsedDataParallel_2_11161();
				Pre_CollapsedDataParallel_1_11163();
				WEIGHTED_ROUND_ROBIN_Splitter_11308();
					Butterfly_11310();
					Butterfly_11311();
					Butterfly_11312();
				WEIGHTED_ROUND_ROBIN_Joiner_11309();
				Post_CollapsedDataParallel_2_11164();
			WEIGHTED_ROUND_ROBIN_Joiner_11276();
			Pre_CollapsedDataParallel_1_11157();
			WEIGHTED_ROUND_ROBIN_Splitter_11313();
				Butterfly_11315();
				Butterfly_11316();
				Butterfly_11317();
			WEIGHTED_ROUND_ROBIN_Joiner_11314();
			Post_CollapsedDataParallel_2_11158();
			WEIGHTED_ROUND_ROBIN_Splitter_11277();
				Pre_CollapsedDataParallel_1_11166();
				WEIGHTED_ROUND_ROBIN_Splitter_11318();
					Butterfly_11320();
					Butterfly_11321();
					Butterfly_11322();
				WEIGHTED_ROUND_ROBIN_Joiner_11319();
				Post_CollapsedDataParallel_2_11167();
				Pre_CollapsedDataParallel_1_11169();
				WEIGHTED_ROUND_ROBIN_Splitter_11323();
					Butterfly_11325();
					Butterfly_11326();
					Butterfly_11327();
				WEIGHTED_ROUND_ROBIN_Joiner_11324();
				Post_CollapsedDataParallel_2_11170();
			WEIGHTED_ROUND_ROBIN_Joiner_11278();
		WEIGHTED_ROUND_ROBIN_Joiner_11279();
		WEIGHTED_ROUND_ROBIN_Splitter_11280();
			WEIGHTED_ROUND_ROBIN_Splitter_11281();
				Pre_CollapsedDataParallel_1_11172();
				WEIGHTED_ROUND_ROBIN_Splitter_11328();
					Butterfly_11330();
					Butterfly_11331();
				WEIGHTED_ROUND_ROBIN_Joiner_11329();
				Post_CollapsedDataParallel_2_11173();
				Pre_CollapsedDataParallel_1_11175();
				WEIGHTED_ROUND_ROBIN_Splitter_11332();
					Butterfly_11334();
					Butterfly_11335();
				WEIGHTED_ROUND_ROBIN_Joiner_11333();
				Post_CollapsedDataParallel_2_11176();
				Pre_CollapsedDataParallel_1_11178();
				WEIGHTED_ROUND_ROBIN_Splitter_11336();
					Butterfly_11338();
					Butterfly_11339();
				WEIGHTED_ROUND_ROBIN_Joiner_11337();
				Post_CollapsedDataParallel_2_11179();
				Pre_CollapsedDataParallel_1_11181();
				WEIGHTED_ROUND_ROBIN_Splitter_11340();
					Butterfly_11342();
					Butterfly_11343();
				WEIGHTED_ROUND_ROBIN_Joiner_11341();
				Post_CollapsedDataParallel_2_11182();
			WEIGHTED_ROUND_ROBIN_Joiner_11282();
			WEIGHTED_ROUND_ROBIN_Splitter_11283();
				Pre_CollapsedDataParallel_1_11184();
				WEIGHTED_ROUND_ROBIN_Splitter_11344();
					Butterfly_11346();
					Butterfly_11347();
				WEIGHTED_ROUND_ROBIN_Joiner_11345();
				Post_CollapsedDataParallel_2_11185();
				Pre_CollapsedDataParallel_1_11187();
				WEIGHTED_ROUND_ROBIN_Splitter_11348();
					Butterfly_11350();
					Butterfly_11351();
				WEIGHTED_ROUND_ROBIN_Joiner_11349();
				Post_CollapsedDataParallel_2_11188();
				Pre_CollapsedDataParallel_1_11190();
				WEIGHTED_ROUND_ROBIN_Splitter_11352();
					Butterfly_11354();
					Butterfly_11355();
				WEIGHTED_ROUND_ROBIN_Joiner_11353();
				Post_CollapsedDataParallel_2_11191();
				Pre_CollapsedDataParallel_1_11193();
				WEIGHTED_ROUND_ROBIN_Splitter_11356();
					Butterfly_11358();
					Butterfly_11359();
				WEIGHTED_ROUND_ROBIN_Joiner_11357();
				Post_CollapsedDataParallel_2_11194();
			WEIGHTED_ROUND_ROBIN_Joiner_11284();
		WEIGHTED_ROUND_ROBIN_Joiner_11285();
		WEIGHTED_ROUND_ROBIN_Splitter_11286();
			WEIGHTED_ROUND_ROBIN_Splitter_11287();
				Butterfly_10915();
				Butterfly_10916();
				Butterfly_10917();
				Butterfly_10918();
				Butterfly_10919();
				Butterfly_10920();
				Butterfly_10921();
				Butterfly_10922();
			WEIGHTED_ROUND_ROBIN_Joiner_11288();
			WEIGHTED_ROUND_ROBIN_Splitter_11289();
				Butterfly_10923();
				Butterfly_10924();
				Butterfly_10925();
				Butterfly_10926();
				Butterfly_10927();
				Butterfly_10928();
				Butterfly_10929();
				Butterfly_10930();
			WEIGHTED_ROUND_ROBIN_Joiner_11290();
		WEIGHTED_ROUND_ROBIN_Joiner_11291();
		BitReverse_10931();
		FloatPrinter_10932();
	ENDFOR
	return EXIT_SUCCESS;
}
