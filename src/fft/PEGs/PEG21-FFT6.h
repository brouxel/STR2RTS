#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=2688 on the compile command line
#else
#if BUF_SIZEMAX < 2688
#error BUF_SIZEMAX too small, it must be at least 2688
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	complex_t wn;
} CombineDFT_2661_t;
void FFTTestSource(buffer_complex_t *chanout);
void FFTTestSource_2607();
void FFTReorderSimple(buffer_complex_t *chanin, buffer_complex_t *chanout);
void FFTReorderSimple_2608();
void WEIGHTED_ROUND_ROBIN_Splitter_2621();
void FFTReorderSimple_2623();
void FFTReorderSimple_2624();
void WEIGHTED_ROUND_ROBIN_Joiner_2622();
void WEIGHTED_ROUND_ROBIN_Splitter_2625();
void FFTReorderSimple_2627();
void FFTReorderSimple_2628();
void FFTReorderSimple_2629();
void FFTReorderSimple_2630();
void WEIGHTED_ROUND_ROBIN_Joiner_2626();
void WEIGHTED_ROUND_ROBIN_Splitter_2631();
void FFTReorderSimple_2633();
void FFTReorderSimple_2634();
void FFTReorderSimple_2635();
void FFTReorderSimple_2636();
void FFTReorderSimple_2637();
void FFTReorderSimple_2638();
void FFTReorderSimple_2639();
void FFTReorderSimple_2640();
void WEIGHTED_ROUND_ROBIN_Joiner_2632();
void WEIGHTED_ROUND_ROBIN_Splitter_2641();
void FFTReorderSimple_2643();
void FFTReorderSimple_2644();
void FFTReorderSimple_2645();
void FFTReorderSimple_2646();
void FFTReorderSimple_2647();
void FFTReorderSimple_2648();
void FFTReorderSimple_2649();
void FFTReorderSimple_2650();
void FFTReorderSimple_2651();
void FFTReorderSimple_2652();
void FFTReorderSimple_2653();
void FFTReorderSimple_2654();
void FFTReorderSimple_2655();
void FFTReorderSimple_2656();
void FFTReorderSimple_2657();
void FFTReorderSimple_2658();
void WEIGHTED_ROUND_ROBIN_Joiner_2642();
void WEIGHTED_ROUND_ROBIN_Splitter_2659();
void CombineDFT(buffer_complex_t *chanin, buffer_complex_t *chanout);
void CombineDFT_2661();
void CombineDFT_2662();
void CombineDFT_2663();
void CombineDFT_2664();
void CombineDFT_2665();
void CombineDFT_2666();
void CombineDFT_2667();
void CombineDFT_2668();
void CombineDFT_2669();
void CombineDFT_2670();
void CombineDFT_2671();
void CombineDFT_2672();
void CombineDFT_2673();
void CombineDFT_2674();
void CombineDFT_2675();
void CombineDFT_2676();
void CombineDFT_2677();
void CombineDFT_2678();
void CombineDFT_2679();
void CombineDFT_2680();
void CombineDFT_2681();
void WEIGHTED_ROUND_ROBIN_Joiner_2660();
void WEIGHTED_ROUND_ROBIN_Splitter_2682();
void CombineDFT_2684();
void CombineDFT_2685();
void CombineDFT_2686();
void CombineDFT_2687();
void CombineDFT_2688();
void CombineDFT_2689();
void CombineDFT_2690();
void CombineDFT_2691();
void CombineDFT_2692();
void CombineDFT_2693();
void CombineDFT_2694();
void CombineDFT_2695();
void CombineDFT_2696();
void CombineDFT_2697();
void CombineDFT_2698();
void CombineDFT_2699();
void WEIGHTED_ROUND_ROBIN_Joiner_2683();
void WEIGHTED_ROUND_ROBIN_Splitter_2700();
void CombineDFT_2702();
void CombineDFT_2703();
void CombineDFT_2704();
void CombineDFT_2705();
void CombineDFT_2706();
void CombineDFT_2707();
void CombineDFT_2708();
void CombineDFT_2709();
void WEIGHTED_ROUND_ROBIN_Joiner_2701();
void WEIGHTED_ROUND_ROBIN_Splitter_2710();
void CombineDFT_2712();
void CombineDFT_2713();
void CombineDFT_2714();
void CombineDFT_2715();
void WEIGHTED_ROUND_ROBIN_Joiner_2711();
void WEIGHTED_ROUND_ROBIN_Splitter_2716();
void CombineDFT_2718();
void CombineDFT_2719();
void WEIGHTED_ROUND_ROBIN_Joiner_2717();
void CombineDFT_2618();
void CPrinter(buffer_complex_t *chanin);
void CPrinter_2619();

#ifdef __cplusplus
}
#endif
#endif
