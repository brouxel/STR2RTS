#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=128 on the compile command line
#else
#if BUF_SIZEMAX < 128
#error BUF_SIZEMAX too small, it must be at least 128
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	complex_t wn;
} CombineDFT_113_t;
void FFTTestSource_59();
void FFTReorderSimple_60();
void WEIGHTED_ROUND_ROBIN_Splitter_73();
void FFTReorderSimple_75();
void FFTReorderSimple_76();
void WEIGHTED_ROUND_ROBIN_Joiner_74();
void WEIGHTED_ROUND_ROBIN_Splitter_77();
void FFTReorderSimple_79();
void FFTReorderSimple_80();
void FFTReorderSimple_81();
void FFTReorderSimple_82();
void WEIGHTED_ROUND_ROBIN_Joiner_78();
void WEIGHTED_ROUND_ROBIN_Splitter_83();
void FFTReorderSimple_85();
void FFTReorderSimple_86();
void FFTReorderSimple_87();
void FFTReorderSimple_88();
void FFTReorderSimple_89();
void FFTReorderSimple_90();
void FFTReorderSimple_91();
void FFTReorderSimple_92();
void WEIGHTED_ROUND_ROBIN_Joiner_84();
void WEIGHTED_ROUND_ROBIN_Splitter_93();
void FFTReorderSimple_95();
void FFTReorderSimple_96();
void FFTReorderSimple_97();
void FFTReorderSimple_98();
void FFTReorderSimple_99();
void FFTReorderSimple_100();
void FFTReorderSimple_101();
void FFTReorderSimple_102();
void FFTReorderSimple_103();
void FFTReorderSimple_104();
void FFTReorderSimple_105();
void FFTReorderSimple_106();
void FFTReorderSimple_107();
void FFTReorderSimple_108();
void FFTReorderSimple_109();
void FFTReorderSimple_110();
void WEIGHTED_ROUND_ROBIN_Joiner_94();
void WEIGHTED_ROUND_ROBIN_Splitter_111();
void CombineDFT_113();
void CombineDFT_114();
void CombineDFT_115();
void CombineDFT_116();
void CombineDFT_117();
void CombineDFT_118();
void CombineDFT_119();
void CombineDFT_120();
void CombineDFT_121();
void CombineDFT_122();
void CombineDFT_123();
void CombineDFT_124();
void CombineDFT_125();
void CombineDFT_126();
void CombineDFT_127();
void CombineDFT_128();
void CombineDFT_129();
void CombineDFT_130();
void CombineDFT_131();
void CombineDFT_132();
void CombineDFT_133();
void CombineDFT_134();
void CombineDFT_135();
void CombineDFT_136();
void CombineDFT_137();
void CombineDFT_138();
void CombineDFT_139();
void CombineDFT_140();
void CombineDFT_141();
void CombineDFT_142();
void CombineDFT_143();
void CombineDFT_144();
void WEIGHTED_ROUND_ROBIN_Joiner_112();
void WEIGHTED_ROUND_ROBIN_Splitter_145();
void CombineDFT_147();
void CombineDFT_148();
void CombineDFT_149();
void CombineDFT_150();
void CombineDFT_151();
void CombineDFT_152();
void CombineDFT_153();
void CombineDFT_154();
void CombineDFT_155();
void CombineDFT_156();
void CombineDFT_157();
void CombineDFT_158();
void CombineDFT_159();
void CombineDFT_160();
void CombineDFT_161();
void CombineDFT_162();
void WEIGHTED_ROUND_ROBIN_Joiner_146();
void WEIGHTED_ROUND_ROBIN_Splitter_163();
void CombineDFT_165();
void CombineDFT_166();
void CombineDFT_167();
void CombineDFT_168();
void CombineDFT_169();
void CombineDFT_170();
void CombineDFT_171();
void CombineDFT_172();
void WEIGHTED_ROUND_ROBIN_Joiner_164();
void WEIGHTED_ROUND_ROBIN_Splitter_173();
void CombineDFT_175();
void CombineDFT_176();
void CombineDFT_177();
void CombineDFT_178();
void WEIGHTED_ROUND_ROBIN_Joiner_174();
void WEIGHTED_ROUND_ROBIN_Splitter_179();
void CombineDFT_181();
void CombineDFT_182();
void WEIGHTED_ROUND_ROBIN_Joiner_180();
void CombineDFT_70();
void CPrinter_71();

#ifdef __cplusplus
}
#endif
#endif
