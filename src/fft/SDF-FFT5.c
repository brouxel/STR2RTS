#include "SDF-FFT5.h"

buffer_complex_t SplitJoin75_SplitJoin67_SplitJoin67_AnonFilter_a0_243_484_552_568_join[2];
buffer_complex_t SplitJoin39_SplitJoin33_SplitJoin33_split2_192_458_522_581_split[2];
buffer_complex_t SplitJoin83_SplitJoin75_SplitJoin75_AnonFilter_a0_255_490_554_570_join[2];
buffer_complex_t SplitJoin26_SplitJoin22_SplitJoin22_split2_203_449_527_585_join[4];
buffer_complex_t SplitJoin63_SplitJoin55_SplitJoin55_AnonFilter_a0_227_476_549_564_join[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_428magnitude_295;
buffer_complex_t SplitJoin20_SplitJoin16_SplitJoin16_split2_201_444_525_584_join[4];
buffer_complex_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a0_207_433_541_558_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_368butterfly_271;
buffer_complex_t SplitJoin57_SplitJoin49_SplitJoin49_AnonFilter_a0_219_472_547_562_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_386WEIGHTED_ROUND_ROBIN_Splitter_529;
buffer_complex_t SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a0_209_434_517_559_join[2];
buffer_complex_t butterfly_269Post_CollapsedDataParallel_2_363;
buffer_complex_t SplitJoin18_SplitJoin14_SplitJoin14_split1_199_443_546_583_split[2];
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_536WEIGHTED_ROUND_ROBIN_Splitter_427;
buffer_complex_t SplitJoin12_SplitJoin10_SplitJoin10_split1_186_440_Hier_Hier_545_576_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_383butterfly_276;
buffer_complex_t SplitJoin6_SplitJoin6_SplitJoin6_AnonFilter_a0_213_436_543_561_join[2];
buffer_complex_t SplitJoin67_SplitJoin59_SplitJoin59_AnonFilter_a0_233_479_550_565_join[2];
buffer_complex_t SplitJoin71_SplitJoin63_SplitJoin63_AnonFilter_a0_239_482_518_566_split[2];
buffer_complex_t butterfly_276Post_CollapsedDataParallel_2_384;
buffer_complex_t SplitJoin39_SplitJoin33_SplitJoin33_split2_192_458_522_581_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_362butterfly_269;
buffer_complex_t SplitJoin43_SplitJoin37_SplitJoin37_split2_194_461_524_582_join[2];
buffer_complex_t SplitJoin50_SplitJoin8_SplitJoin8_split1_165_438_Hier_child1_523_575_split[4];
buffer_complex_t SplitJoin6_SplitJoin6_SplitJoin6_AnonFilter_a0_213_436_543_561_split[2];
buffer_complex_t SplitJoin8_SplitJoin8_SplitJoin8_split1_165_438_Hier_Hier_544_573_split[2];
buffer_complex_t SplitJoin67_SplitJoin59_SplitJoin59_AnonFilter_a0_233_479_550_565_split[2];
buffer_complex_t SplitJoin75_SplitJoin67_SplitJoin67_AnonFilter_a0_243_484_552_568_split[2];
buffer_complex_t SplitJoin79_SplitJoin71_SplitJoin71_AnonFilter_a0_249_487_553_569_split[2];
buffer_complex_t SplitJoin43_SplitJoin37_SplitJoin37_split2_194_461_524_582_split[2];
buffer_complex_t butterfly_272Post_CollapsedDataParallel_2_372;
buffer_complex_t SplitJoin14_SplitJoin10_SplitJoin10_split1_186_440_Hier_child0_526_577_split[2];
buffer_complex_t SplitJoin57_SplitJoin49_SplitJoin49_AnonFilter_a0_219_472_547_562_join[2];
buffer_complex_t SplitJoin37_SplitJoin10_SplitJoin10_split1_186_440_Hier_child1_528_580_split[2];
buffer_complex_t source_205WEIGHTED_ROUND_ROBIN_Splitter_385;
buffer_complex_t SplitJoin61_SplitJoin53_SplitJoin53_AnonFilter_a0_225_475_548_563_join[2];
buffer_complex_t SplitJoin61_SplitJoin53_SplitJoin53_AnonFilter_a0_225_475_548_563_split[2];
buffer_complex_t butterfly_273Post_CollapsedDataParallel_2_375;
buffer_complex_t SplitJoin85_SplitJoin77_SplitJoin77_AnonFilter_a0_257_491_555_571_split[2];
buffer_complex_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a0_207_433_541_558_split[2];
buffer_complex_t SplitJoin85_SplitJoin77_SplitJoin77_AnonFilter_a0_257_491_555_571_join[2];
buffer_float_t magnitude_295sink_296;
buffer_complex_t SplitJoin63_SplitJoin55_SplitJoin55_AnonFilter_a0_227_476_549_564_split[2];
buffer_complex_t SplitJoin73_SplitJoin65_SplitJoin65_AnonFilter_a0_241_483_551_567_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_365butterfly_270;
buffer_complex_t SplitJoin33_SplitJoin29_SplitJoin29_split2_190_455_521_579_split[2];
buffer_complex_t SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a0_209_434_517_559_split[2];
buffer_complex_t SplitJoin20_SplitJoin16_SplitJoin16_split2_201_444_525_584_split[4];
buffer_complex_t SplitJoin18_SplitJoin14_SplitJoin14_split1_199_443_546_583_join[2];
buffer_complex_t SplitJoin71_SplitJoin63_SplitJoin63_AnonFilter_a0_239_482_518_566_join[2];
buffer_complex_t butterfly_271Post_CollapsedDataParallel_2_369;
buffer_complex_t SplitJoin16_SplitJoin12_SplitJoin12_split2_188_441_519_578_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_380butterfly_275;
buffer_complex_t SplitJoin4_SplitJoin4_SplitJoin4_AnonFilter_a0_211_435_542_560_split[2];
buffer_complex_t SplitJoin16_SplitJoin12_SplitJoin12_split2_188_441_519_578_split[2];
buffer_complex_t SplitJoin73_SplitJoin65_SplitJoin65_AnonFilter_a0_241_483_551_567_split[2];
buffer_complex_t SplitJoin83_SplitJoin75_SplitJoin75_AnonFilter_a0_255_490_554_570_split[2];
buffer_complex_t SplitJoin50_SplitJoin8_SplitJoin8_split1_165_438_Hier_child1_523_575_join[4];
buffer_complex_t butterfly_270Post_CollapsedDataParallel_2_366;
buffer_complex_t SplitJoin37_SplitJoin10_SplitJoin10_split1_186_440_Hier_child1_528_580_join[2];
buffer_complex_t SplitJoin79_SplitJoin71_SplitJoin71_AnonFilter_a0_249_487_553_569_join[2];
buffer_complex_t SplitJoin12_SplitJoin10_SplitJoin10_split1_186_440_Hier_Hier_545_576_split[2];
buffer_complex_t SplitJoin4_SplitJoin4_SplitJoin4_AnonFilter_a0_211_435_542_560_join[2];
buffer_complex_t butterfly_275Post_CollapsedDataParallel_2_381;
buffer_complex_t SplitJoin26_SplitJoin22_SplitJoin22_split2_203_449_527_585_split[4];
buffer_complex_t SplitJoin10_SplitJoin8_SplitJoin8_split1_165_438_Hier_child0_520_574_split[4];
buffer_complex_t SplitJoin8_SplitJoin8_SplitJoin8_split1_165_438_Hier_Hier_544_573_join[2];
buffer_complex_t SplitJoin14_SplitJoin10_SplitJoin10_split1_186_440_Hier_child0_526_577_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_377butterfly_274;
buffer_complex_t SplitJoin89_SplitJoin81_SplitJoin81_AnonFilter_a0_263_494_556_572_split[2];
buffer_complex_t SplitJoin89_SplitJoin81_SplitJoin81_AnonFilter_a0_263_494_556_572_join[2];
buffer_complex_t Pre_CollapsedDataParallel_1_371butterfly_272;
buffer_complex_t WEIGHTED_ROUND_ROBIN_Joiner_530WEIGHTED_ROUND_ROBIN_Splitter_535;
buffer_complex_t Pre_CollapsedDataParallel_1_374butterfly_273;
buffer_complex_t SplitJoin33_SplitJoin29_SplitJoin29_split2_190_455_521_579_join[2];
buffer_complex_t butterfly_274Post_CollapsedDataParallel_2_378;
buffer_complex_t SplitJoin10_SplitJoin8_SplitJoin8_split1_165_438_Hier_child0_520_574_join[4];



void source(buffer_complex_t *chanout) {
		complex_t t;
		t.imag = 0.0 ; 
		t.real = 0.9501 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.2311 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.6068 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.486 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.8913 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.7621 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.4565 ; 
		push_complex(&(*chanout), t) ; 
		t.real = 0.0185 ; 
		push_complex(&(*chanout), t) ; 
	}


void source_205() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		source(&(source_205WEIGHTED_ROUND_ROBIN_Splitter_385));
	ENDFOR
}

void Identity(buffer_complex_t *chanin, buffer_complex_t *chanout) {
	complex_t __tmp30 = pop_complex(&(*chanin));
	push_complex(&(*chanout), __tmp30) ; 
}


void Identity_215() {
	Identity(&(SplitJoin6_SplitJoin6_SplitJoin6_AnonFilter_a0_213_436_543_561_split[0]), &(SplitJoin6_SplitJoin6_SplitJoin6_AnonFilter_a0_213_436_543_561_join[0]));
}

void Identity_217() {
	Identity(&(SplitJoin6_SplitJoin6_SplitJoin6_AnonFilter_a0_213_436_543_561_split[1]), &(SplitJoin6_SplitJoin6_SplitJoin6_AnonFilter_a0_213_436_543_561_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_391() {
	push_complex(&SplitJoin6_SplitJoin6_SplitJoin6_AnonFilter_a0_213_436_543_561_split[0], pop_complex(&SplitJoin4_SplitJoin4_SplitJoin4_AnonFilter_a0_211_435_542_560_split[0]));
	push_complex(&SplitJoin6_SplitJoin6_SplitJoin6_AnonFilter_a0_213_436_543_561_split[1], pop_complex(&SplitJoin4_SplitJoin4_SplitJoin4_AnonFilter_a0_211_435_542_560_split[0]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_392() {
	push_complex(&SplitJoin4_SplitJoin4_SplitJoin4_AnonFilter_a0_211_435_542_560_join[0], pop_complex(&SplitJoin6_SplitJoin6_SplitJoin6_AnonFilter_a0_213_436_543_561_join[0]));
	push_complex(&SplitJoin4_SplitJoin4_SplitJoin4_AnonFilter_a0_211_435_542_560_join[0], pop_complex(&SplitJoin6_SplitJoin6_SplitJoin6_AnonFilter_a0_213_436_543_561_join[1]));
}

void Identity_221() {
	Identity(&(SplitJoin57_SplitJoin49_SplitJoin49_AnonFilter_a0_219_472_547_562_split[0]), &(SplitJoin57_SplitJoin49_SplitJoin49_AnonFilter_a0_219_472_547_562_join[0]));
}

void Identity_223() {
	Identity(&(SplitJoin57_SplitJoin49_SplitJoin49_AnonFilter_a0_219_472_547_562_split[1]), &(SplitJoin57_SplitJoin49_SplitJoin49_AnonFilter_a0_219_472_547_562_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_393() {
	push_complex(&SplitJoin57_SplitJoin49_SplitJoin49_AnonFilter_a0_219_472_547_562_split[0], pop_complex(&SplitJoin4_SplitJoin4_SplitJoin4_AnonFilter_a0_211_435_542_560_split[1]));
	push_complex(&SplitJoin57_SplitJoin49_SplitJoin49_AnonFilter_a0_219_472_547_562_split[1], pop_complex(&SplitJoin4_SplitJoin4_SplitJoin4_AnonFilter_a0_211_435_542_560_split[1]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_394() {
	push_complex(&SplitJoin4_SplitJoin4_SplitJoin4_AnonFilter_a0_211_435_542_560_join[1], pop_complex(&SplitJoin57_SplitJoin49_SplitJoin49_AnonFilter_a0_219_472_547_562_join[0]));
	push_complex(&SplitJoin4_SplitJoin4_SplitJoin4_AnonFilter_a0_211_435_542_560_join[1], pop_complex(&SplitJoin57_SplitJoin49_SplitJoin49_AnonFilter_a0_219_472_547_562_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_389() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin4_SplitJoin4_SplitJoin4_AnonFilter_a0_211_435_542_560_split[0], pop_complex(&SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a0_209_434_517_559_split[0]));
		push_complex(&SplitJoin4_SplitJoin4_SplitJoin4_AnonFilter_a0_211_435_542_560_split[1], pop_complex(&SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a0_209_434_517_559_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_390() {
	push_complex(&SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a0_209_434_517_559_join[0], pop_complex(&SplitJoin4_SplitJoin4_SplitJoin4_AnonFilter_a0_211_435_542_560_join[0]));
	push_complex(&SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a0_209_434_517_559_join[0], pop_complex(&SplitJoin4_SplitJoin4_SplitJoin4_AnonFilter_a0_211_435_542_560_join[0]));
	push_complex(&SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a0_209_434_517_559_join[0], pop_complex(&SplitJoin4_SplitJoin4_SplitJoin4_AnonFilter_a0_211_435_542_560_join[1]));
	push_complex(&SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a0_209_434_517_559_join[0], pop_complex(&SplitJoin4_SplitJoin4_SplitJoin4_AnonFilter_a0_211_435_542_560_join[1]));
}

void Identity_229() {
	Identity(&(SplitJoin63_SplitJoin55_SplitJoin55_AnonFilter_a0_227_476_549_564_split[0]), &(SplitJoin63_SplitJoin55_SplitJoin55_AnonFilter_a0_227_476_549_564_join[0]));
}

void Identity_231() {
	Identity(&(SplitJoin63_SplitJoin55_SplitJoin55_AnonFilter_a0_227_476_549_564_split[1]), &(SplitJoin63_SplitJoin55_SplitJoin55_AnonFilter_a0_227_476_549_564_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_397() {
	push_complex(&SplitJoin63_SplitJoin55_SplitJoin55_AnonFilter_a0_227_476_549_564_split[0], pop_complex(&SplitJoin61_SplitJoin53_SplitJoin53_AnonFilter_a0_225_475_548_563_split[0]));
	push_complex(&SplitJoin63_SplitJoin55_SplitJoin55_AnonFilter_a0_227_476_549_564_split[1], pop_complex(&SplitJoin61_SplitJoin53_SplitJoin53_AnonFilter_a0_225_475_548_563_split[0]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_398() {
	push_complex(&SplitJoin61_SplitJoin53_SplitJoin53_AnonFilter_a0_225_475_548_563_join[0], pop_complex(&SplitJoin63_SplitJoin55_SplitJoin55_AnonFilter_a0_227_476_549_564_join[0]));
	push_complex(&SplitJoin61_SplitJoin53_SplitJoin53_AnonFilter_a0_225_475_548_563_join[0], pop_complex(&SplitJoin63_SplitJoin55_SplitJoin55_AnonFilter_a0_227_476_549_564_join[1]));
}

void Identity_235() {
	Identity(&(SplitJoin67_SplitJoin59_SplitJoin59_AnonFilter_a0_233_479_550_565_split[0]), &(SplitJoin67_SplitJoin59_SplitJoin59_AnonFilter_a0_233_479_550_565_join[0]));
}

void Identity_237() {
	Identity(&(SplitJoin67_SplitJoin59_SplitJoin59_AnonFilter_a0_233_479_550_565_split[1]), &(SplitJoin67_SplitJoin59_SplitJoin59_AnonFilter_a0_233_479_550_565_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_399() {
	push_complex(&SplitJoin67_SplitJoin59_SplitJoin59_AnonFilter_a0_233_479_550_565_split[0], pop_complex(&SplitJoin61_SplitJoin53_SplitJoin53_AnonFilter_a0_225_475_548_563_split[1]));
	push_complex(&SplitJoin67_SplitJoin59_SplitJoin59_AnonFilter_a0_233_479_550_565_split[1], pop_complex(&SplitJoin61_SplitJoin53_SplitJoin53_AnonFilter_a0_225_475_548_563_split[1]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_400() {
	push_complex(&SplitJoin61_SplitJoin53_SplitJoin53_AnonFilter_a0_225_475_548_563_join[1], pop_complex(&SplitJoin67_SplitJoin59_SplitJoin59_AnonFilter_a0_233_479_550_565_join[0]));
	push_complex(&SplitJoin61_SplitJoin53_SplitJoin53_AnonFilter_a0_225_475_548_563_join[1], pop_complex(&SplitJoin67_SplitJoin59_SplitJoin59_AnonFilter_a0_233_479_550_565_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_395() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin61_SplitJoin53_SplitJoin53_AnonFilter_a0_225_475_548_563_split[0], pop_complex(&SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a0_209_434_517_559_split[1]));
		push_complex(&SplitJoin61_SplitJoin53_SplitJoin53_AnonFilter_a0_225_475_548_563_split[1], pop_complex(&SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a0_209_434_517_559_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_396() {
	push_complex(&SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a0_209_434_517_559_join[1], pop_complex(&SplitJoin61_SplitJoin53_SplitJoin53_AnonFilter_a0_225_475_548_563_join[0]));
	push_complex(&SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a0_209_434_517_559_join[1], pop_complex(&SplitJoin61_SplitJoin53_SplitJoin53_AnonFilter_a0_225_475_548_563_join[0]));
	push_complex(&SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a0_209_434_517_559_join[1], pop_complex(&SplitJoin61_SplitJoin53_SplitJoin53_AnonFilter_a0_225_475_548_563_join[1]));
	push_complex(&SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a0_209_434_517_559_join[1], pop_complex(&SplitJoin61_SplitJoin53_SplitJoin53_AnonFilter_a0_225_475_548_563_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_387() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_complex(&SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a0_209_434_517_559_split[0], pop_complex(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a0_207_433_541_558_split[0]));
		push_complex(&SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a0_209_434_517_559_split[1], pop_complex(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a0_207_433_541_558_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_388() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a0_207_433_541_558_join[0], pop_complex(&SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a0_209_434_517_559_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a0_207_433_541_558_join[0], pop_complex(&SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a0_209_434_517_559_join[1]));
	ENDFOR
}

void Identity_245() {
	Identity(&(SplitJoin75_SplitJoin67_SplitJoin67_AnonFilter_a0_243_484_552_568_split[0]), &(SplitJoin75_SplitJoin67_SplitJoin67_AnonFilter_a0_243_484_552_568_join[0]));
}

void Identity_247() {
	Identity(&(SplitJoin75_SplitJoin67_SplitJoin67_AnonFilter_a0_243_484_552_568_split[1]), &(SplitJoin75_SplitJoin67_SplitJoin67_AnonFilter_a0_243_484_552_568_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_405() {
	push_complex(&SplitJoin75_SplitJoin67_SplitJoin67_AnonFilter_a0_243_484_552_568_split[0], pop_complex(&SplitJoin73_SplitJoin65_SplitJoin65_AnonFilter_a0_241_483_551_567_split[0]));
	push_complex(&SplitJoin75_SplitJoin67_SplitJoin67_AnonFilter_a0_243_484_552_568_split[1], pop_complex(&SplitJoin73_SplitJoin65_SplitJoin65_AnonFilter_a0_241_483_551_567_split[0]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_406() {
	push_complex(&SplitJoin73_SplitJoin65_SplitJoin65_AnonFilter_a0_241_483_551_567_join[0], pop_complex(&SplitJoin75_SplitJoin67_SplitJoin67_AnonFilter_a0_243_484_552_568_join[0]));
	push_complex(&SplitJoin73_SplitJoin65_SplitJoin65_AnonFilter_a0_241_483_551_567_join[0], pop_complex(&SplitJoin75_SplitJoin67_SplitJoin67_AnonFilter_a0_243_484_552_568_join[1]));
}

void Identity_251() {
	Identity(&(SplitJoin79_SplitJoin71_SplitJoin71_AnonFilter_a0_249_487_553_569_split[0]), &(SplitJoin79_SplitJoin71_SplitJoin71_AnonFilter_a0_249_487_553_569_join[0]));
}

void Identity_253() {
	Identity(&(SplitJoin79_SplitJoin71_SplitJoin71_AnonFilter_a0_249_487_553_569_split[1]), &(SplitJoin79_SplitJoin71_SplitJoin71_AnonFilter_a0_249_487_553_569_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_407() {
	push_complex(&SplitJoin79_SplitJoin71_SplitJoin71_AnonFilter_a0_249_487_553_569_split[0], pop_complex(&SplitJoin73_SplitJoin65_SplitJoin65_AnonFilter_a0_241_483_551_567_split[1]));
	push_complex(&SplitJoin79_SplitJoin71_SplitJoin71_AnonFilter_a0_249_487_553_569_split[1], pop_complex(&SplitJoin73_SplitJoin65_SplitJoin65_AnonFilter_a0_241_483_551_567_split[1]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_408() {
	push_complex(&SplitJoin73_SplitJoin65_SplitJoin65_AnonFilter_a0_241_483_551_567_join[1], pop_complex(&SplitJoin79_SplitJoin71_SplitJoin71_AnonFilter_a0_249_487_553_569_join[0]));
	push_complex(&SplitJoin73_SplitJoin65_SplitJoin65_AnonFilter_a0_241_483_551_567_join[1], pop_complex(&SplitJoin79_SplitJoin71_SplitJoin71_AnonFilter_a0_249_487_553_569_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_403() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin73_SplitJoin65_SplitJoin65_AnonFilter_a0_241_483_551_567_split[0], pop_complex(&SplitJoin71_SplitJoin63_SplitJoin63_AnonFilter_a0_239_482_518_566_split[0]));
		push_complex(&SplitJoin73_SplitJoin65_SplitJoin65_AnonFilter_a0_241_483_551_567_split[1], pop_complex(&SplitJoin71_SplitJoin63_SplitJoin63_AnonFilter_a0_239_482_518_566_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_404() {
	push_complex(&SplitJoin71_SplitJoin63_SplitJoin63_AnonFilter_a0_239_482_518_566_join[0], pop_complex(&SplitJoin73_SplitJoin65_SplitJoin65_AnonFilter_a0_241_483_551_567_join[0]));
	push_complex(&SplitJoin71_SplitJoin63_SplitJoin63_AnonFilter_a0_239_482_518_566_join[0], pop_complex(&SplitJoin73_SplitJoin65_SplitJoin65_AnonFilter_a0_241_483_551_567_join[0]));
	push_complex(&SplitJoin71_SplitJoin63_SplitJoin63_AnonFilter_a0_239_482_518_566_join[0], pop_complex(&SplitJoin73_SplitJoin65_SplitJoin65_AnonFilter_a0_241_483_551_567_join[1]));
	push_complex(&SplitJoin71_SplitJoin63_SplitJoin63_AnonFilter_a0_239_482_518_566_join[0], pop_complex(&SplitJoin73_SplitJoin65_SplitJoin65_AnonFilter_a0_241_483_551_567_join[1]));
}

void Identity_259() {
	Identity(&(SplitJoin85_SplitJoin77_SplitJoin77_AnonFilter_a0_257_491_555_571_split[0]), &(SplitJoin85_SplitJoin77_SplitJoin77_AnonFilter_a0_257_491_555_571_join[0]));
}

void Identity_261() {
	Identity(&(SplitJoin85_SplitJoin77_SplitJoin77_AnonFilter_a0_257_491_555_571_split[1]), &(SplitJoin85_SplitJoin77_SplitJoin77_AnonFilter_a0_257_491_555_571_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_411() {
	push_complex(&SplitJoin85_SplitJoin77_SplitJoin77_AnonFilter_a0_257_491_555_571_split[0], pop_complex(&SplitJoin83_SplitJoin75_SplitJoin75_AnonFilter_a0_255_490_554_570_split[0]));
	push_complex(&SplitJoin85_SplitJoin77_SplitJoin77_AnonFilter_a0_257_491_555_571_split[1], pop_complex(&SplitJoin83_SplitJoin75_SplitJoin75_AnonFilter_a0_255_490_554_570_split[0]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_412() {
	push_complex(&SplitJoin83_SplitJoin75_SplitJoin75_AnonFilter_a0_255_490_554_570_join[0], pop_complex(&SplitJoin85_SplitJoin77_SplitJoin77_AnonFilter_a0_257_491_555_571_join[0]));
	push_complex(&SplitJoin83_SplitJoin75_SplitJoin75_AnonFilter_a0_255_490_554_570_join[0], pop_complex(&SplitJoin85_SplitJoin77_SplitJoin77_AnonFilter_a0_257_491_555_571_join[1]));
}

void Identity_265() {
	Identity(&(SplitJoin89_SplitJoin81_SplitJoin81_AnonFilter_a0_263_494_556_572_split[0]), &(SplitJoin89_SplitJoin81_SplitJoin81_AnonFilter_a0_263_494_556_572_join[0]));
}

void Identity_267() {
	Identity(&(SplitJoin89_SplitJoin81_SplitJoin81_AnonFilter_a0_263_494_556_572_split[1]), &(SplitJoin89_SplitJoin81_SplitJoin81_AnonFilter_a0_263_494_556_572_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_413() {
	push_complex(&SplitJoin89_SplitJoin81_SplitJoin81_AnonFilter_a0_263_494_556_572_split[0], pop_complex(&SplitJoin83_SplitJoin75_SplitJoin75_AnonFilter_a0_255_490_554_570_split[1]));
	push_complex(&SplitJoin89_SplitJoin81_SplitJoin81_AnonFilter_a0_263_494_556_572_split[1], pop_complex(&SplitJoin83_SplitJoin75_SplitJoin75_AnonFilter_a0_255_490_554_570_split[1]));
}

void WEIGHTED_ROUND_ROBIN_Joiner_414() {
	push_complex(&SplitJoin83_SplitJoin75_SplitJoin75_AnonFilter_a0_255_490_554_570_join[1], pop_complex(&SplitJoin89_SplitJoin81_SplitJoin81_AnonFilter_a0_263_494_556_572_join[0]));
	push_complex(&SplitJoin83_SplitJoin75_SplitJoin75_AnonFilter_a0_255_490_554_570_join[1], pop_complex(&SplitJoin89_SplitJoin81_SplitJoin81_AnonFilter_a0_263_494_556_572_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_409() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin83_SplitJoin75_SplitJoin75_AnonFilter_a0_255_490_554_570_split[0], pop_complex(&SplitJoin71_SplitJoin63_SplitJoin63_AnonFilter_a0_239_482_518_566_split[1]));
		push_complex(&SplitJoin83_SplitJoin75_SplitJoin75_AnonFilter_a0_255_490_554_570_split[1], pop_complex(&SplitJoin71_SplitJoin63_SplitJoin63_AnonFilter_a0_239_482_518_566_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_410() {
	push_complex(&SplitJoin71_SplitJoin63_SplitJoin63_AnonFilter_a0_239_482_518_566_join[1], pop_complex(&SplitJoin83_SplitJoin75_SplitJoin75_AnonFilter_a0_255_490_554_570_join[0]));
	push_complex(&SplitJoin71_SplitJoin63_SplitJoin63_AnonFilter_a0_239_482_518_566_join[1], pop_complex(&SplitJoin83_SplitJoin75_SplitJoin75_AnonFilter_a0_255_490_554_570_join[0]));
	push_complex(&SplitJoin71_SplitJoin63_SplitJoin63_AnonFilter_a0_239_482_518_566_join[1], pop_complex(&SplitJoin83_SplitJoin75_SplitJoin75_AnonFilter_a0_255_490_554_570_join[1]));
	push_complex(&SplitJoin71_SplitJoin63_SplitJoin63_AnonFilter_a0_239_482_518_566_join[1], pop_complex(&SplitJoin83_SplitJoin75_SplitJoin75_AnonFilter_a0_255_490_554_570_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_401() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_complex(&SplitJoin71_SplitJoin63_SplitJoin63_AnonFilter_a0_239_482_518_566_split[0], pop_complex(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a0_207_433_541_558_split[1]));
		push_complex(&SplitJoin71_SplitJoin63_SplitJoin63_AnonFilter_a0_239_482_518_566_split[1], pop_complex(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a0_207_433_541_558_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_402() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a0_207_433_541_558_join[1], pop_complex(&SplitJoin71_SplitJoin63_SplitJoin63_AnonFilter_a0_239_482_518_566_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a0_207_433_541_558_join[1], pop_complex(&SplitJoin71_SplitJoin63_SplitJoin63_AnonFilter_a0_239_482_518_566_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_385() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		push_complex(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a0_207_433_541_558_split[0], pop_complex(&source_205WEIGHTED_ROUND_ROBIN_Splitter_385));
		push_complex(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a0_207_433_541_558_split[1], pop_complex(&source_205WEIGHTED_ROUND_ROBIN_Splitter_385));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_386() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_386WEIGHTED_ROUND_ROBIN_Splitter_529, pop_complex(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a0_207_433_541_558_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_386WEIGHTED_ROUND_ROBIN_Splitter_529, pop_complex(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a0_207_433_541_558_join[1]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1(buffer_complex_t *chanin, buffer_complex_t *chanout) {
 {
 {
	int iTimesSumOfWeights_Plus_PartialSum_k = 0;
 {
	FOR(int, _i, 0,  < , 2, _i++) {
		push_complex(&(*chanout), peek_complex(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
		iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 1) ; 
	}
	ENDFOR
}
}
}
	pop_complex(&(*chanin)) ; 
}


void Pre_CollapsedDataParallel_1_362() {
	Pre_CollapsedDataParallel_1(&(SplitJoin10_SplitJoin8_SplitJoin8_split1_165_438_Hier_child0_520_574_split[0]), &(Pre_CollapsedDataParallel_1_362butterfly_269));
}

void butterfly(buffer_complex_t *chanin, buffer_complex_t *chanout) {
	complex_t WN1;
	complex_t WN2;
	complex_t one = ((complex_t) pop_complex(&(*chanin)));
	complex_t two = ((complex_t) pop_complex(&(*chanin)));
	complex_t __sa1;
	complex_t __sa2;
	WN1.real = 1.0 ; 
	WN1.imag = -0.0 ; 
	WN2.real = -1.0 ; 
	WN2.imag = 8.742278E-8 ; 
	__sa1.real = (one.real + ((two.real * WN1.real) - (two.imag * WN1.imag))) ; 
	__sa1.imag = (one.imag + ((two.real * WN1.imag) + (two.imag * WN1.real))) ; 
	push_complex(&(*chanout), __sa1) ; 
	__sa2.real = (one.real + ((two.real * WN2.real) - (two.imag * WN2.imag))) ; 
	__sa2.imag = (one.imag + ((two.real * WN2.imag) + (two.imag * WN2.real))) ; 
	push_complex(&(*chanout), __sa2) ; 
}


void butterfly_269() {
	butterfly(&(Pre_CollapsedDataParallel_1_362butterfly_269), &(butterfly_269Post_CollapsedDataParallel_2_363));
}

void Post_CollapsedDataParallel_2(buffer_complex_t *chanin, buffer_complex_t *chanout) {
 {
 {
	FOR(int, _k, 0,  < , 2, _k++) {
 {
		push_complex(&(*chanout), peek_complex(&(*chanin), (_k + 0))) ; 
	}
	}
	ENDFOR
}
}
	pop_complex(&(*chanin)) ; 
}


void Post_CollapsedDataParallel_2_363() {
	Post_CollapsedDataParallel_2(&(butterfly_269Post_CollapsedDataParallel_2_363), &(SplitJoin10_SplitJoin8_SplitJoin8_split1_165_438_Hier_child0_520_574_join[0]));
}

void Pre_CollapsedDataParallel_1_365() {
	Pre_CollapsedDataParallel_1(&(SplitJoin10_SplitJoin8_SplitJoin8_split1_165_438_Hier_child0_520_574_split[1]), &(Pre_CollapsedDataParallel_1_365butterfly_270));
}

void butterfly_270() {
	butterfly(&(Pre_CollapsedDataParallel_1_365butterfly_270), &(butterfly_270Post_CollapsedDataParallel_2_366));
}

void Post_CollapsedDataParallel_2_366() {
	Post_CollapsedDataParallel_2(&(butterfly_270Post_CollapsedDataParallel_2_366), &(SplitJoin10_SplitJoin8_SplitJoin8_split1_165_438_Hier_child0_520_574_join[1]));
}

void Pre_CollapsedDataParallel_1_368() {
	Pre_CollapsedDataParallel_1(&(SplitJoin10_SplitJoin8_SplitJoin8_split1_165_438_Hier_child0_520_574_split[2]), &(Pre_CollapsedDataParallel_1_368butterfly_271));
}

void butterfly_271() {
	butterfly(&(Pre_CollapsedDataParallel_1_368butterfly_271), &(butterfly_271Post_CollapsedDataParallel_2_369));
}

void Post_CollapsedDataParallel_2_369() {
	Post_CollapsedDataParallel_2(&(butterfly_271Post_CollapsedDataParallel_2_369), &(SplitJoin10_SplitJoin8_SplitJoin8_split1_165_438_Hier_child0_520_574_join[2]));
}

void Pre_CollapsedDataParallel_1_371() {
	Pre_CollapsedDataParallel_1(&(SplitJoin10_SplitJoin8_SplitJoin8_split1_165_438_Hier_child0_520_574_split[3]), &(Pre_CollapsedDataParallel_1_371butterfly_272));
}

void butterfly_272() {
	butterfly(&(Pre_CollapsedDataParallel_1_371butterfly_272), &(butterfly_272Post_CollapsedDataParallel_2_372));
}

void Post_CollapsedDataParallel_2_372() {
	Post_CollapsedDataParallel_2(&(butterfly_272Post_CollapsedDataParallel_2_372), &(SplitJoin10_SplitJoin8_SplitJoin8_split1_165_438_Hier_child0_520_574_join[3]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_531() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_165_438_Hier_child0_520_574_split[__iter_], pop_complex(&SplitJoin8_SplitJoin8_SplitJoin8_split1_165_438_Hier_Hier_544_573_split[0]));
		push_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_165_438_Hier_child0_520_574_split[__iter_], pop_complex(&SplitJoin8_SplitJoin8_SplitJoin8_split1_165_438_Hier_Hier_544_573_split[0]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_532() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin8_SplitJoin8_SplitJoin8_split1_165_438_Hier_Hier_544_573_join[0], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_165_438_Hier_child0_520_574_join[__iter_]));
		push_complex(&SplitJoin8_SplitJoin8_SplitJoin8_split1_165_438_Hier_Hier_544_573_join[0], pop_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_165_438_Hier_child0_520_574_join[__iter_]));
	ENDFOR
}

void Pre_CollapsedDataParallel_1_374() {
	Pre_CollapsedDataParallel_1(&(SplitJoin50_SplitJoin8_SplitJoin8_split1_165_438_Hier_child1_523_575_split[0]), &(Pre_CollapsedDataParallel_1_374butterfly_273));
}

void butterfly_273() {
	butterfly(&(Pre_CollapsedDataParallel_1_374butterfly_273), &(butterfly_273Post_CollapsedDataParallel_2_375));
}

void Post_CollapsedDataParallel_2_375() {
	Post_CollapsedDataParallel_2(&(butterfly_273Post_CollapsedDataParallel_2_375), &(SplitJoin50_SplitJoin8_SplitJoin8_split1_165_438_Hier_child1_523_575_join[0]));
}

void Pre_CollapsedDataParallel_1_377() {
	Pre_CollapsedDataParallel_1(&(SplitJoin50_SplitJoin8_SplitJoin8_split1_165_438_Hier_child1_523_575_split[1]), &(Pre_CollapsedDataParallel_1_377butterfly_274));
}

void butterfly_274() {
	butterfly(&(Pre_CollapsedDataParallel_1_377butterfly_274), &(butterfly_274Post_CollapsedDataParallel_2_378));
}

void Post_CollapsedDataParallel_2_378() {
	Post_CollapsedDataParallel_2(&(butterfly_274Post_CollapsedDataParallel_2_378), &(SplitJoin50_SplitJoin8_SplitJoin8_split1_165_438_Hier_child1_523_575_join[1]));
}

void Pre_CollapsedDataParallel_1_380() {
	Pre_CollapsedDataParallel_1(&(SplitJoin50_SplitJoin8_SplitJoin8_split1_165_438_Hier_child1_523_575_split[2]), &(Pre_CollapsedDataParallel_1_380butterfly_275));
}

void butterfly_275() {
	butterfly(&(Pre_CollapsedDataParallel_1_380butterfly_275), &(butterfly_275Post_CollapsedDataParallel_2_381));
}

void Post_CollapsedDataParallel_2_381() {
	Post_CollapsedDataParallel_2(&(butterfly_275Post_CollapsedDataParallel_2_381), &(SplitJoin50_SplitJoin8_SplitJoin8_split1_165_438_Hier_child1_523_575_join[2]));
}

void Pre_CollapsedDataParallel_1_383() {
	Pre_CollapsedDataParallel_1(&(SplitJoin50_SplitJoin8_SplitJoin8_split1_165_438_Hier_child1_523_575_split[3]), &(Pre_CollapsedDataParallel_1_383butterfly_276));
}

void butterfly_276() {
	butterfly(&(Pre_CollapsedDataParallel_1_383butterfly_276), &(butterfly_276Post_CollapsedDataParallel_2_384));
}

void Post_CollapsedDataParallel_2_384() {
	Post_CollapsedDataParallel_2(&(butterfly_276Post_CollapsedDataParallel_2_384), &(SplitJoin50_SplitJoin8_SplitJoin8_split1_165_438_Hier_child1_523_575_join[3]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_533() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin50_SplitJoin8_SplitJoin8_split1_165_438_Hier_child1_523_575_split[__iter_], pop_complex(&SplitJoin8_SplitJoin8_SplitJoin8_split1_165_438_Hier_Hier_544_573_split[1]));
		push_complex(&SplitJoin50_SplitJoin8_SplitJoin8_split1_165_438_Hier_child1_523_575_split[__iter_], pop_complex(&SplitJoin8_SplitJoin8_SplitJoin8_split1_165_438_Hier_Hier_544_573_split[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_534() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin8_SplitJoin8_SplitJoin8_split1_165_438_Hier_Hier_544_573_join[1], pop_complex(&SplitJoin50_SplitJoin8_SplitJoin8_split1_165_438_Hier_child1_523_575_join[__iter_]));
		push_complex(&SplitJoin8_SplitJoin8_SplitJoin8_split1_165_438_Hier_Hier_544_573_join[1], pop_complex(&SplitJoin50_SplitJoin8_SplitJoin8_split1_165_438_Hier_child1_523_575_join[__iter_]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_529() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&SplitJoin8_SplitJoin8_SplitJoin8_split1_165_438_Hier_Hier_544_573_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_386WEIGHTED_ROUND_ROBIN_Splitter_529));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&SplitJoin8_SplitJoin8_SplitJoin8_split1_165_438_Hier_Hier_544_573_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_386WEIGHTED_ROUND_ROBIN_Splitter_529));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_530() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_530WEIGHTED_ROUND_ROBIN_Splitter_535, pop_complex(&SplitJoin8_SplitJoin8_SplitJoin8_split1_165_438_Hier_Hier_544_573_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_530WEIGHTED_ROUND_ROBIN_Splitter_535, pop_complex(&SplitJoin8_SplitJoin8_SplitJoin8_split1_165_438_Hier_Hier_544_573_join[1]));
	ENDFOR
}

void butterfly_278() {
	butterfly(&(SplitJoin16_SplitJoin12_SplitJoin12_split2_188_441_519_578_split[0]), &(SplitJoin16_SplitJoin12_SplitJoin12_split2_188_441_519_578_join[0]));
}

void butterfly_279() {
	butterfly(&(SplitJoin16_SplitJoin12_SplitJoin12_split2_188_441_519_578_split[1]), &(SplitJoin16_SplitJoin12_SplitJoin12_split2_188_441_519_578_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_419() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin16_SplitJoin12_SplitJoin12_split2_188_441_519_578_split[0], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_186_440_Hier_child0_526_577_split[0]));
		push_complex(&SplitJoin16_SplitJoin12_SplitJoin12_split2_188_441_519_578_split[1], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_186_440_Hier_child0_526_577_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_420() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_186_440_Hier_child0_526_577_join[0], pop_complex(&SplitJoin16_SplitJoin12_SplitJoin12_split2_188_441_519_578_join[0]));
		push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_186_440_Hier_child0_526_577_join[0], pop_complex(&SplitJoin16_SplitJoin12_SplitJoin12_split2_188_441_519_578_join[1]));
	ENDFOR
}}

void butterfly_280() {
	butterfly(&(SplitJoin33_SplitJoin29_SplitJoin29_split2_190_455_521_579_split[0]), &(SplitJoin33_SplitJoin29_SplitJoin29_split2_190_455_521_579_join[0]));
}

void butterfly_281() {
	butterfly(&(SplitJoin33_SplitJoin29_SplitJoin29_split2_190_455_521_579_split[1]), &(SplitJoin33_SplitJoin29_SplitJoin29_split2_190_455_521_579_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_421() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin33_SplitJoin29_SplitJoin29_split2_190_455_521_579_split[0], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_186_440_Hier_child0_526_577_split[1]));
		push_complex(&SplitJoin33_SplitJoin29_SplitJoin29_split2_190_455_521_579_split[1], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_186_440_Hier_child0_526_577_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_422() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_186_440_Hier_child0_526_577_join[1], pop_complex(&SplitJoin33_SplitJoin29_SplitJoin29_split2_190_455_521_579_join[0]));
		push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_186_440_Hier_child0_526_577_join[1], pop_complex(&SplitJoin33_SplitJoin29_SplitJoin29_split2_190_455_521_579_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_537() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_186_440_Hier_child0_526_577_split[0], pop_complex(&SplitJoin12_SplitJoin10_SplitJoin10_split1_186_440_Hier_Hier_545_576_split[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_186_440_Hier_child0_526_577_split[1], pop_complex(&SplitJoin12_SplitJoin10_SplitJoin10_split1_186_440_Hier_Hier_545_576_split[0]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_538() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin12_SplitJoin10_SplitJoin10_split1_186_440_Hier_Hier_545_576_join[0], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_186_440_Hier_child0_526_577_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin12_SplitJoin10_SplitJoin10_split1_186_440_Hier_Hier_545_576_join[0], pop_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_186_440_Hier_child0_526_577_join[1]));
	ENDFOR
}

void butterfly_282() {
	butterfly(&(SplitJoin39_SplitJoin33_SplitJoin33_split2_192_458_522_581_split[0]), &(SplitJoin39_SplitJoin33_SplitJoin33_split2_192_458_522_581_join[0]));
}

void butterfly_283() {
	butterfly(&(SplitJoin39_SplitJoin33_SplitJoin33_split2_192_458_522_581_split[1]), &(SplitJoin39_SplitJoin33_SplitJoin33_split2_192_458_522_581_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_423() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin39_SplitJoin33_SplitJoin33_split2_192_458_522_581_split[0], pop_complex(&SplitJoin37_SplitJoin10_SplitJoin10_split1_186_440_Hier_child1_528_580_split[0]));
		push_complex(&SplitJoin39_SplitJoin33_SplitJoin33_split2_192_458_522_581_split[1], pop_complex(&SplitJoin37_SplitJoin10_SplitJoin10_split1_186_440_Hier_child1_528_580_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_424() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin37_SplitJoin10_SplitJoin10_split1_186_440_Hier_child1_528_580_join[0], pop_complex(&SplitJoin39_SplitJoin33_SplitJoin33_split2_192_458_522_581_join[0]));
		push_complex(&SplitJoin37_SplitJoin10_SplitJoin10_split1_186_440_Hier_child1_528_580_join[0], pop_complex(&SplitJoin39_SplitJoin33_SplitJoin33_split2_192_458_522_581_join[1]));
	ENDFOR
}}

void butterfly_284() {
	butterfly(&(SplitJoin43_SplitJoin37_SplitJoin37_split2_194_461_524_582_split[0]), &(SplitJoin43_SplitJoin37_SplitJoin37_split2_194_461_524_582_join[0]));
}

void butterfly_285() {
	butterfly(&(SplitJoin43_SplitJoin37_SplitJoin37_split2_194_461_524_582_split[1]), &(SplitJoin43_SplitJoin37_SplitJoin37_split2_194_461_524_582_join[1]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_425() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin43_SplitJoin37_SplitJoin37_split2_194_461_524_582_split[0], pop_complex(&SplitJoin37_SplitJoin10_SplitJoin10_split1_186_440_Hier_child1_528_580_split[1]));
		push_complex(&SplitJoin43_SplitJoin37_SplitJoin37_split2_194_461_524_582_split[1], pop_complex(&SplitJoin37_SplitJoin10_SplitJoin10_split1_186_440_Hier_child1_528_580_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_426() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		push_complex(&SplitJoin37_SplitJoin10_SplitJoin10_split1_186_440_Hier_child1_528_580_join[1], pop_complex(&SplitJoin43_SplitJoin37_SplitJoin37_split2_194_461_524_582_join[0]));
		push_complex(&SplitJoin37_SplitJoin10_SplitJoin10_split1_186_440_Hier_child1_528_580_join[1], pop_complex(&SplitJoin43_SplitJoin37_SplitJoin37_split2_194_461_524_582_join[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_539() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin37_SplitJoin10_SplitJoin10_split1_186_440_Hier_child1_528_580_split[0], pop_complex(&SplitJoin12_SplitJoin10_SplitJoin10_split1_186_440_Hier_Hier_545_576_split[1]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin37_SplitJoin10_SplitJoin10_split1_186_440_Hier_child1_528_580_split[1], pop_complex(&SplitJoin12_SplitJoin10_SplitJoin10_split1_186_440_Hier_Hier_545_576_split[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_540() {
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin12_SplitJoin10_SplitJoin10_split1_186_440_Hier_Hier_545_576_join[1], pop_complex(&SplitJoin37_SplitJoin10_SplitJoin10_split1_186_440_Hier_child1_528_580_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_complex(&SplitJoin12_SplitJoin10_SplitJoin10_split1_186_440_Hier_Hier_545_576_join[1], pop_complex(&SplitJoin37_SplitJoin10_SplitJoin10_split1_186_440_Hier_child1_528_580_join[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_535() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&SplitJoin12_SplitJoin10_SplitJoin10_split1_186_440_Hier_Hier_545_576_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_530WEIGHTED_ROUND_ROBIN_Splitter_535));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&SplitJoin12_SplitJoin10_SplitJoin10_split1_186_440_Hier_Hier_545_576_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_530WEIGHTED_ROUND_ROBIN_Splitter_535));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_536() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_536WEIGHTED_ROUND_ROBIN_Splitter_427, pop_complex(&SplitJoin12_SplitJoin10_SplitJoin10_split1_186_440_Hier_Hier_545_576_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_536WEIGHTED_ROUND_ROBIN_Splitter_427, pop_complex(&SplitJoin12_SplitJoin10_SplitJoin10_split1_186_440_Hier_Hier_545_576_join[1]));
	ENDFOR
}

void butterfly_287() {
	butterfly(&(SplitJoin20_SplitJoin16_SplitJoin16_split2_201_444_525_584_split[0]), &(SplitJoin20_SplitJoin16_SplitJoin16_split2_201_444_525_584_join[0]));
}

void butterfly_288() {
	butterfly(&(SplitJoin20_SplitJoin16_SplitJoin16_split2_201_444_525_584_split[1]), &(SplitJoin20_SplitJoin16_SplitJoin16_split2_201_444_525_584_join[1]));
}

void butterfly_289() {
	butterfly(&(SplitJoin20_SplitJoin16_SplitJoin16_split2_201_444_525_584_split[2]), &(SplitJoin20_SplitJoin16_SplitJoin16_split2_201_444_525_584_join[2]));
}

void butterfly_290() {
	butterfly(&(SplitJoin20_SplitJoin16_SplitJoin16_split2_201_444_525_584_split[3]), &(SplitJoin20_SplitJoin16_SplitJoin16_split2_201_444_525_584_join[3]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_429() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin20_SplitJoin16_SplitJoin16_split2_201_444_525_584_split[__iter_], pop_complex(&SplitJoin18_SplitJoin14_SplitJoin14_split1_199_443_546_583_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_430() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin18_SplitJoin14_SplitJoin14_split1_199_443_546_583_join[0], pop_complex(&SplitJoin20_SplitJoin16_SplitJoin16_split2_201_444_525_584_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void butterfly_291() {
	butterfly(&(SplitJoin26_SplitJoin22_SplitJoin22_split2_203_449_527_585_split[0]), &(SplitJoin26_SplitJoin22_SplitJoin22_split2_203_449_527_585_join[0]));
}

void butterfly_292() {
	butterfly(&(SplitJoin26_SplitJoin22_SplitJoin22_split2_203_449_527_585_split[1]), &(SplitJoin26_SplitJoin22_SplitJoin22_split2_203_449_527_585_join[1]));
}

void butterfly_293() {
	butterfly(&(SplitJoin26_SplitJoin22_SplitJoin22_split2_203_449_527_585_split[2]), &(SplitJoin26_SplitJoin22_SplitJoin22_split2_203_449_527_585_join[2]));
}

void butterfly_294() {
	butterfly(&(SplitJoin26_SplitJoin22_SplitJoin22_split2_203_449_527_585_split[3]), &(SplitJoin26_SplitJoin22_SplitJoin22_split2_203_449_527_585_join[3]));
}

void WEIGHTED_ROUND_ROBIN_Splitter_431() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin26_SplitJoin22_SplitJoin22_split2_203_449_527_585_split[__iter_], pop_complex(&SplitJoin18_SplitJoin14_SplitJoin14_split1_199_443_546_583_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_432() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_complex(&SplitJoin18_SplitJoin14_SplitJoin14_split1_199_443_546_583_join[1], pop_complex(&SplitJoin26_SplitJoin22_SplitJoin22_split2_203_449_527_585_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Splitter_427() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&SplitJoin18_SplitJoin14_SplitJoin14_split1_199_443_546_583_split[0], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_536WEIGHTED_ROUND_ROBIN_Splitter_427));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&SplitJoin18_SplitJoin14_SplitJoin14_split1_199_443_546_583_split[1], pop_complex(&WEIGHTED_ROUND_ROBIN_Joiner_536WEIGHTED_ROUND_ROBIN_Splitter_427));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_428() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_428magnitude_295, pop_complex(&SplitJoin18_SplitJoin14_SplitJoin14_split1_199_443_546_583_join[0]));
	ENDFOR
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_complex(&WEIGHTED_ROUND_ROBIN_Joiner_428magnitude_295, pop_complex(&SplitJoin18_SplitJoin14_SplitJoin14_split1_199_443_546_583_join[1]));
	ENDFOR
}

void magnitude(buffer_complex_t *chanin, buffer_float_t *chanout) {
		complex_t c = ((complex_t) pop_complex(&(*chanin)));
		push_float(&(*chanout), ((float) sqrt(((c.real * c.real) + (c.imag * c.imag))))) ; 
	}


void magnitude_295() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		magnitude(&(WEIGHTED_ROUND_ROBIN_Joiner_428magnitude_295), &(magnitude_295sink_296));
	ENDFOR
}

void sink(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void sink_296() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		sink(&(magnitude_295sink_296));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_complex(&SplitJoin75_SplitJoin67_SplitJoin67_AnonFilter_a0_243_484_552_568_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 2, __iter_init_1_++)
		init_buffer_complex(&SplitJoin39_SplitJoin33_SplitJoin33_split2_192_458_522_581_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_complex(&SplitJoin83_SplitJoin75_SplitJoin75_AnonFilter_a0_255_490_554_570_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 4, __iter_init_3_++)
		init_buffer_complex(&SplitJoin26_SplitJoin22_SplitJoin22_split2_203_449_527_585_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 2, __iter_init_4_++)
		init_buffer_complex(&SplitJoin63_SplitJoin55_SplitJoin55_AnonFilter_a0_227_476_549_564_join[__iter_init_4_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_428magnitude_295);
	FOR(int, __iter_init_5_, 0, <, 4, __iter_init_5_++)
		init_buffer_complex(&SplitJoin20_SplitJoin16_SplitJoin16_split2_201_444_525_584_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 2, __iter_init_6_++)
		init_buffer_complex(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a0_207_433_541_558_join[__iter_init_6_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_368butterfly_271);
	FOR(int, __iter_init_7_, 0, <, 2, __iter_init_7_++)
		init_buffer_complex(&SplitJoin57_SplitJoin49_SplitJoin49_AnonFilter_a0_219_472_547_562_split[__iter_init_7_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_386WEIGHTED_ROUND_ROBIN_Splitter_529);
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_complex(&SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a0_209_434_517_559_join[__iter_init_8_]);
	ENDFOR
	init_buffer_complex(&butterfly_269Post_CollapsedDataParallel_2_363);
	FOR(int, __iter_init_9_, 0, <, 2, __iter_init_9_++)
		init_buffer_complex(&SplitJoin18_SplitJoin14_SplitJoin14_split1_199_443_546_583_split[__iter_init_9_]);
	ENDFOR
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_536WEIGHTED_ROUND_ROBIN_Splitter_427);
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_complex(&SplitJoin12_SplitJoin10_SplitJoin10_split1_186_440_Hier_Hier_545_576_join[__iter_init_10_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_383butterfly_276);
	FOR(int, __iter_init_11_, 0, <, 2, __iter_init_11_++)
		init_buffer_complex(&SplitJoin6_SplitJoin6_SplitJoin6_AnonFilter_a0_213_436_543_561_join[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 2, __iter_init_12_++)
		init_buffer_complex(&SplitJoin67_SplitJoin59_SplitJoin59_AnonFilter_a0_233_479_550_565_join[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_complex(&SplitJoin71_SplitJoin63_SplitJoin63_AnonFilter_a0_239_482_518_566_split[__iter_init_13_]);
	ENDFOR
	init_buffer_complex(&butterfly_276Post_CollapsedDataParallel_2_384);
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_complex(&SplitJoin39_SplitJoin33_SplitJoin33_split2_192_458_522_581_join[__iter_init_14_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_362butterfly_269);
	FOR(int, __iter_init_15_, 0, <, 2, __iter_init_15_++)
		init_buffer_complex(&SplitJoin43_SplitJoin37_SplitJoin37_split2_194_461_524_582_join[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 4, __iter_init_16_++)
		init_buffer_complex(&SplitJoin50_SplitJoin8_SplitJoin8_split1_165_438_Hier_child1_523_575_split[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_complex(&SplitJoin6_SplitJoin6_SplitJoin6_AnonFilter_a0_213_436_543_561_split[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 2, __iter_init_18_++)
		init_buffer_complex(&SplitJoin8_SplitJoin8_SplitJoin8_split1_165_438_Hier_Hier_544_573_split[__iter_init_18_]);
	ENDFOR
	FOR(int, __iter_init_19_, 0, <, 2, __iter_init_19_++)
		init_buffer_complex(&SplitJoin67_SplitJoin59_SplitJoin59_AnonFilter_a0_233_479_550_565_split[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 2, __iter_init_20_++)
		init_buffer_complex(&SplitJoin75_SplitJoin67_SplitJoin67_AnonFilter_a0_243_484_552_568_split[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 2, __iter_init_21_++)
		init_buffer_complex(&SplitJoin79_SplitJoin71_SplitJoin71_AnonFilter_a0_249_487_553_569_split[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 2, __iter_init_22_++)
		init_buffer_complex(&SplitJoin43_SplitJoin37_SplitJoin37_split2_194_461_524_582_split[__iter_init_22_]);
	ENDFOR
	init_buffer_complex(&butterfly_272Post_CollapsedDataParallel_2_372);
	FOR(int, __iter_init_23_, 0, <, 2, __iter_init_23_++)
		init_buffer_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_186_440_Hier_child0_526_577_split[__iter_init_23_]);
	ENDFOR
	FOR(int, __iter_init_24_, 0, <, 2, __iter_init_24_++)
		init_buffer_complex(&SplitJoin57_SplitJoin49_SplitJoin49_AnonFilter_a0_219_472_547_562_join[__iter_init_24_]);
	ENDFOR
	FOR(int, __iter_init_25_, 0, <, 2, __iter_init_25_++)
		init_buffer_complex(&SplitJoin37_SplitJoin10_SplitJoin10_split1_186_440_Hier_child1_528_580_split[__iter_init_25_]);
	ENDFOR
	init_buffer_complex(&source_205WEIGHTED_ROUND_ROBIN_Splitter_385);
	FOR(int, __iter_init_26_, 0, <, 2, __iter_init_26_++)
		init_buffer_complex(&SplitJoin61_SplitJoin53_SplitJoin53_AnonFilter_a0_225_475_548_563_join[__iter_init_26_]);
	ENDFOR
	FOR(int, __iter_init_27_, 0, <, 2, __iter_init_27_++)
		init_buffer_complex(&SplitJoin61_SplitJoin53_SplitJoin53_AnonFilter_a0_225_475_548_563_split[__iter_init_27_]);
	ENDFOR
	init_buffer_complex(&butterfly_273Post_CollapsedDataParallel_2_375);
	FOR(int, __iter_init_28_, 0, <, 2, __iter_init_28_++)
		init_buffer_complex(&SplitJoin85_SplitJoin77_SplitJoin77_AnonFilter_a0_257_491_555_571_split[__iter_init_28_]);
	ENDFOR
	FOR(int, __iter_init_29_, 0, <, 2, __iter_init_29_++)
		init_buffer_complex(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a0_207_433_541_558_split[__iter_init_29_]);
	ENDFOR
	FOR(int, __iter_init_30_, 0, <, 2, __iter_init_30_++)
		init_buffer_complex(&SplitJoin85_SplitJoin77_SplitJoin77_AnonFilter_a0_257_491_555_571_join[__iter_init_30_]);
	ENDFOR
	init_buffer_float(&magnitude_295sink_296);
	FOR(int, __iter_init_31_, 0, <, 2, __iter_init_31_++)
		init_buffer_complex(&SplitJoin63_SplitJoin55_SplitJoin55_AnonFilter_a0_227_476_549_564_split[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 2, __iter_init_32_++)
		init_buffer_complex(&SplitJoin73_SplitJoin65_SplitJoin65_AnonFilter_a0_241_483_551_567_join[__iter_init_32_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_365butterfly_270);
	FOR(int, __iter_init_33_, 0, <, 2, __iter_init_33_++)
		init_buffer_complex(&SplitJoin33_SplitJoin29_SplitJoin29_split2_190_455_521_579_split[__iter_init_33_]);
	ENDFOR
	FOR(int, __iter_init_34_, 0, <, 2, __iter_init_34_++)
		init_buffer_complex(&SplitJoin2_SplitJoin2_SplitJoin2_AnonFilter_a0_209_434_517_559_split[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 4, __iter_init_35_++)
		init_buffer_complex(&SplitJoin20_SplitJoin16_SplitJoin16_split2_201_444_525_584_split[__iter_init_35_]);
	ENDFOR
	FOR(int, __iter_init_36_, 0, <, 2, __iter_init_36_++)
		init_buffer_complex(&SplitJoin18_SplitJoin14_SplitJoin14_split1_199_443_546_583_join[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 2, __iter_init_37_++)
		init_buffer_complex(&SplitJoin71_SplitJoin63_SplitJoin63_AnonFilter_a0_239_482_518_566_join[__iter_init_37_]);
	ENDFOR
	init_buffer_complex(&butterfly_271Post_CollapsedDataParallel_2_369);
	FOR(int, __iter_init_38_, 0, <, 2, __iter_init_38_++)
		init_buffer_complex(&SplitJoin16_SplitJoin12_SplitJoin12_split2_188_441_519_578_join[__iter_init_38_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_380butterfly_275);
	FOR(int, __iter_init_39_, 0, <, 2, __iter_init_39_++)
		init_buffer_complex(&SplitJoin4_SplitJoin4_SplitJoin4_AnonFilter_a0_211_435_542_560_split[__iter_init_39_]);
	ENDFOR
	FOR(int, __iter_init_40_, 0, <, 2, __iter_init_40_++)
		init_buffer_complex(&SplitJoin16_SplitJoin12_SplitJoin12_split2_188_441_519_578_split[__iter_init_40_]);
	ENDFOR
	FOR(int, __iter_init_41_, 0, <, 2, __iter_init_41_++)
		init_buffer_complex(&SplitJoin73_SplitJoin65_SplitJoin65_AnonFilter_a0_241_483_551_567_split[__iter_init_41_]);
	ENDFOR
	FOR(int, __iter_init_42_, 0, <, 2, __iter_init_42_++)
		init_buffer_complex(&SplitJoin83_SplitJoin75_SplitJoin75_AnonFilter_a0_255_490_554_570_split[__iter_init_42_]);
	ENDFOR
	FOR(int, __iter_init_43_, 0, <, 4, __iter_init_43_++)
		init_buffer_complex(&SplitJoin50_SplitJoin8_SplitJoin8_split1_165_438_Hier_child1_523_575_join[__iter_init_43_]);
	ENDFOR
	init_buffer_complex(&butterfly_270Post_CollapsedDataParallel_2_366);
	FOR(int, __iter_init_44_, 0, <, 2, __iter_init_44_++)
		init_buffer_complex(&SplitJoin37_SplitJoin10_SplitJoin10_split1_186_440_Hier_child1_528_580_join[__iter_init_44_]);
	ENDFOR
	FOR(int, __iter_init_45_, 0, <, 2, __iter_init_45_++)
		init_buffer_complex(&SplitJoin79_SplitJoin71_SplitJoin71_AnonFilter_a0_249_487_553_569_join[__iter_init_45_]);
	ENDFOR
	FOR(int, __iter_init_46_, 0, <, 2, __iter_init_46_++)
		init_buffer_complex(&SplitJoin12_SplitJoin10_SplitJoin10_split1_186_440_Hier_Hier_545_576_split[__iter_init_46_]);
	ENDFOR
	FOR(int, __iter_init_47_, 0, <, 2, __iter_init_47_++)
		init_buffer_complex(&SplitJoin4_SplitJoin4_SplitJoin4_AnonFilter_a0_211_435_542_560_join[__iter_init_47_]);
	ENDFOR
	init_buffer_complex(&butterfly_275Post_CollapsedDataParallel_2_381);
	FOR(int, __iter_init_48_, 0, <, 4, __iter_init_48_++)
		init_buffer_complex(&SplitJoin26_SplitJoin22_SplitJoin22_split2_203_449_527_585_split[__iter_init_48_]);
	ENDFOR
	FOR(int, __iter_init_49_, 0, <, 4, __iter_init_49_++)
		init_buffer_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_165_438_Hier_child0_520_574_split[__iter_init_49_]);
	ENDFOR
	FOR(int, __iter_init_50_, 0, <, 2, __iter_init_50_++)
		init_buffer_complex(&SplitJoin8_SplitJoin8_SplitJoin8_split1_165_438_Hier_Hier_544_573_join[__iter_init_50_]);
	ENDFOR
	FOR(int, __iter_init_51_, 0, <, 2, __iter_init_51_++)
		init_buffer_complex(&SplitJoin14_SplitJoin10_SplitJoin10_split1_186_440_Hier_child0_526_577_join[__iter_init_51_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_377butterfly_274);
	FOR(int, __iter_init_52_, 0, <, 2, __iter_init_52_++)
		init_buffer_complex(&SplitJoin89_SplitJoin81_SplitJoin81_AnonFilter_a0_263_494_556_572_split[__iter_init_52_]);
	ENDFOR
	FOR(int, __iter_init_53_, 0, <, 2, __iter_init_53_++)
		init_buffer_complex(&SplitJoin89_SplitJoin81_SplitJoin81_AnonFilter_a0_263_494_556_572_join[__iter_init_53_]);
	ENDFOR
	init_buffer_complex(&Pre_CollapsedDataParallel_1_371butterfly_272);
	init_buffer_complex(&WEIGHTED_ROUND_ROBIN_Joiner_530WEIGHTED_ROUND_ROBIN_Splitter_535);
	init_buffer_complex(&Pre_CollapsedDataParallel_1_374butterfly_273);
	FOR(int, __iter_init_54_, 0, <, 2, __iter_init_54_++)
		init_buffer_complex(&SplitJoin33_SplitJoin29_SplitJoin29_split2_190_455_521_579_join[__iter_init_54_]);
	ENDFOR
	init_buffer_complex(&butterfly_274Post_CollapsedDataParallel_2_378);
	FOR(int, __iter_init_55_, 0, <, 4, __iter_init_55_++)
		init_buffer_complex(&SplitJoin10_SplitJoin8_SplitJoin8_split1_165_438_Hier_child0_520_574_join[__iter_init_55_]);
	ENDFOR
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		source_205();
		WEIGHTED_ROUND_ROBIN_Splitter_385();
			WEIGHTED_ROUND_ROBIN_Splitter_387();
				WEIGHTED_ROUND_ROBIN_Splitter_389();
					WEIGHTED_ROUND_ROBIN_Splitter_391();
						Identity_215();
						Identity_217();
					WEIGHTED_ROUND_ROBIN_Joiner_392();
					WEIGHTED_ROUND_ROBIN_Splitter_393();
						Identity_221();
						Identity_223();
					WEIGHTED_ROUND_ROBIN_Joiner_394();
				WEIGHTED_ROUND_ROBIN_Joiner_390();
				WEIGHTED_ROUND_ROBIN_Splitter_395();
					WEIGHTED_ROUND_ROBIN_Splitter_397();
						Identity_229();
						Identity_231();
					WEIGHTED_ROUND_ROBIN_Joiner_398();
					WEIGHTED_ROUND_ROBIN_Splitter_399();
						Identity_235();
						Identity_237();
					WEIGHTED_ROUND_ROBIN_Joiner_400();
				WEIGHTED_ROUND_ROBIN_Joiner_396();
			WEIGHTED_ROUND_ROBIN_Joiner_388();
			WEIGHTED_ROUND_ROBIN_Splitter_401();
				WEIGHTED_ROUND_ROBIN_Splitter_403();
					WEIGHTED_ROUND_ROBIN_Splitter_405();
						Identity_245();
						Identity_247();
					WEIGHTED_ROUND_ROBIN_Joiner_406();
					WEIGHTED_ROUND_ROBIN_Splitter_407();
						Identity_251();
						Identity_253();
					WEIGHTED_ROUND_ROBIN_Joiner_408();
				WEIGHTED_ROUND_ROBIN_Joiner_404();
				WEIGHTED_ROUND_ROBIN_Splitter_409();
					WEIGHTED_ROUND_ROBIN_Splitter_411();
						Identity_259();
						Identity_261();
					WEIGHTED_ROUND_ROBIN_Joiner_412();
					WEIGHTED_ROUND_ROBIN_Splitter_413();
						Identity_265();
						Identity_267();
					WEIGHTED_ROUND_ROBIN_Joiner_414();
				WEIGHTED_ROUND_ROBIN_Joiner_410();
			WEIGHTED_ROUND_ROBIN_Joiner_402();
		WEIGHTED_ROUND_ROBIN_Joiner_386();
		WEIGHTED_ROUND_ROBIN_Splitter_529();
			WEIGHTED_ROUND_ROBIN_Splitter_531();
				Pre_CollapsedDataParallel_1_362();
				butterfly_269();
				Post_CollapsedDataParallel_2_363();
				Pre_CollapsedDataParallel_1_365();
				butterfly_270();
				Post_CollapsedDataParallel_2_366();
				Pre_CollapsedDataParallel_1_368();
				butterfly_271();
				Post_CollapsedDataParallel_2_369();
				Pre_CollapsedDataParallel_1_371();
				butterfly_272();
				Post_CollapsedDataParallel_2_372();
			WEIGHTED_ROUND_ROBIN_Joiner_532();
			WEIGHTED_ROUND_ROBIN_Splitter_533();
				Pre_CollapsedDataParallel_1_374();
				butterfly_273();
				Post_CollapsedDataParallel_2_375();
				Pre_CollapsedDataParallel_1_377();
				butterfly_274();
				Post_CollapsedDataParallel_2_378();
				Pre_CollapsedDataParallel_1_380();
				butterfly_275();
				Post_CollapsedDataParallel_2_381();
				Pre_CollapsedDataParallel_1_383();
				butterfly_276();
				Post_CollapsedDataParallel_2_384();
			WEIGHTED_ROUND_ROBIN_Joiner_534();
		WEIGHTED_ROUND_ROBIN_Joiner_530();
		WEIGHTED_ROUND_ROBIN_Splitter_535();
			WEIGHTED_ROUND_ROBIN_Splitter_537();
				WEIGHTED_ROUND_ROBIN_Splitter_419();
					butterfly_278();
					butterfly_279();
				WEIGHTED_ROUND_ROBIN_Joiner_420();
				WEIGHTED_ROUND_ROBIN_Splitter_421();
					butterfly_280();
					butterfly_281();
				WEIGHTED_ROUND_ROBIN_Joiner_422();
			WEIGHTED_ROUND_ROBIN_Joiner_538();
			WEIGHTED_ROUND_ROBIN_Splitter_539();
				WEIGHTED_ROUND_ROBIN_Splitter_423();
					butterfly_282();
					butterfly_283();
				WEIGHTED_ROUND_ROBIN_Joiner_424();
				WEIGHTED_ROUND_ROBIN_Splitter_425();
					butterfly_284();
					butterfly_285();
				WEIGHTED_ROUND_ROBIN_Joiner_426();
			WEIGHTED_ROUND_ROBIN_Joiner_540();
		WEIGHTED_ROUND_ROBIN_Joiner_536();
		WEIGHTED_ROUND_ROBIN_Splitter_427();
			WEIGHTED_ROUND_ROBIN_Splitter_429();
				butterfly_287();
				butterfly_288();
				butterfly_289();
				butterfly_290();
			WEIGHTED_ROUND_ROBIN_Joiner_430();
			WEIGHTED_ROUND_ROBIN_Splitter_431();
				butterfly_291();
				butterfly_292();
				butterfly_293();
				butterfly_294();
			WEIGHTED_ROUND_ROBIN_Joiner_432();
		WEIGHTED_ROUND_ROBIN_Joiner_428();
		magnitude_295();
		sink_296();
	ENDFOR
	return EXIT_SUCCESS;
}
