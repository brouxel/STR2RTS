#include "globals.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Make sure N is a power_of_2 */      
#define TheGlobal_N 4
/* true for UP sort and false for DOWN sort */ 
#define TheGlobal_SortDir 1
    
typedef struct {  // BitonicSort.str:28
} CompareExchange_t;

typedef struct {  // BitonicSort.str:207
    int A [TheGlobal_N];	// BitonicSort.str:209
    buffer_int_t buffer_out;
} KeySource_t;

typedef struct {  // BitonicSort.str:230
    buffer_int_t buffer_in;
} KeyPrinter_t;

void CompareExchange();
void KeySource();
void KeyPrinter();
void BitonicSort_init();


#ifdef __cplusplus
}
#endif