#include "BitonicSort.h"

#include "comm_shared_buffer.c"

CompareExchange_t CompareExchangeBuf;
KeySource_t KeySourceBuf;
KeyPrinter_t KeyPrinterBuf;

void CompareExchange(boolean sortdir) { 
    //work pop 2 push 2
    /* the input keys and min,max keys */ 
    int k1, k2, mink, maxk; 

    k1 = pop_int(&); 
    k2 = pop_int(&); 
    if (k1 <= k2)
    {  
        mink = k1; 
        maxk = k2; 
    } 
    else /* k1 > k2 */ 
    { 
        mink = k2; 
        maxk = k1; 
    } 

    if (sortdir) { 
        /* UP sort */
        push_int(&, mink);
        push_int(&, maxk);
    } 
    else {/* sortdir == false */ 
        /* DOWN sort */ 
        push_int(&, maxk);
        push_int(&, mink);
    }
} 

void BitonicSort_init() { // BitonicSort.str:211
    for (int i = 0; i < TheGlobal_N; (i)++) { // BitonicSort.str:217
        KeySourceBuf.A[i] = (TheGlobal_N - i); // BitonicSort.str:217
    }
}
/**
 * Creates N keys and sends it out  
 */
void KeySource() {
    //work push N 
    for (int i=0 ; i < TheGlobal_N; i++) {
        push_int(&, KeySourceBuf.A[i]);
    }
} 

/**
 * Prints out the sorted keys and verifies if they are sorted.
 */
void KeyPrinter() {
    //work pop N
    for (int i=0 ; i < TheGlobal_N ; i++) {
        printf("", pop_int(&));
    } 
}

void main(int argv, char** argc) {
    BitonicSort_init();
    
    for(int iter=0 ; iter < MAX_ITERATION ; iter++) {
    	KeySource();
    	//BitonicSortKernel();
        for (int i = 2; i <= TheGlobal_N / 2; i *= 2) {
    		//MergeStage();
                for(int j = 1; j < i; j *= 2) { // BitonicSort.str:152
                    int L; int numseqp; int dircnt; // BitonicSort.str:148
                    L = i / j; // BitonicSort.str:155
                    numseqp = (N / i) * j; // BitonicSort.str:157
                    dircnt = j; // BitonicSort.str:158
                    //StepOfMerge();
                    boolean curdir; // BitonicSort.str:91
                    Split2ROUND_ROBIN(L);
                        for (int k = 0; k < numseqp; k++) { // BitonicSort.str:94
                            curdir = (((k / dircnt) % 2) == 0); // BitonicSort.str:104
                            if (L > 2) {
                                //PartitionBitonicSequence();
                                _80211_r6_split1_round_robin();
                                    for (int l = 0; l < L / 2; l++) {
                                        CompareExchange(curdir);
                                    }
                                Join1ROUND_ROBIN();
                            }
                            else {
                                CompareExchange(TheGlobal_SortDir);
                            }
                        }
                    Join2ROUND_ROBIN(L);
                }
        }
        //LastMergeStage();
        int L; int numseqp; // BitonicSort.str:174
        for (int i = 1; i < TheGlobal_N ; i *= 2) { // BitonicSort.str:178
            L = TheGlobal_N / i; // BitonicSort.str:181
            numseqp = i; // BitonicSort.str:183
            //StepOfLastMerge();
            Split3ROUND_ROBIN(L);
                for (int j = 0; j < numseqp; j++) { // BitonicSort.str:126
                    if ((L > 2)) {
                        //PartitionBitonicSequence();
                        _80211_r6_split1_round_robin();
                            for (int k = 0; k < L / 2; k++) {
                                CompareExchange(TheGlobal_SortDir);
                            }
                        Join1ROUND_ROBIN();
                    }
                    else {
                        CompareExchange(TheGlobal_SortDir);
                    }
                }
            _80211_r6_join3_round_robin(L);
        }
    	KeyPrinter();
    }
} 
