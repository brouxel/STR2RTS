#include "BitonicSort_4n.h"

#include "comm_shared_buffer.c"

CompareExchange_t CompareExchangeBuf;
KeySource_t KeySourceBuf;
KeyPrinter_t KeyPrinterBuf;

void CompareExchange_UP() { 
    //work pop 2 push 2
    /* the input keys and min,max keys */ 
    int k1, k2, mink, maxk; 

    k1 = pop_int(&); 
    k2 = pop_int(&); 
    if (k1 <= k2)
    {  
        mink = k1; 
        maxk = k2; 
    } 
    else /* k1 > k2 */ 
    { 
        mink = k2; 
        maxk = k1; 
    } 

    /* UP sort */
    push_int(&, mink);
    push_int(&, maxk);
}

void CompareExchange_DOWN() { 
    //work pop 2 push 2
    /* the input keys and min,max keys */ 
    int k1, k2, mink, maxk; 

    k1 = pop_int(&); 
    k2 = pop_int(&); 
    if (k1 <= k2)
    {  
        mink = k1; 
        maxk = k2; 
    } 
    else /* k1 > k2 */ 
    { 
        mink = k2; 
        maxk = k1; 
    } 

    /* DOWN sort */ 
    push_int(&, maxk);
    push_int(&, mink);
} 

void BitonicSort_init() { // BitonicSort.str:211
    FOR(uint32_t, i, 0, <, TheGlobal_N, i++)  // BitonicSort.str:217
        KeySourceBuf.A[i] = (TheGlobal_N - i); // BitonicSort.str:217
    ENDFOR
            
    init_buffer_int(&KeySourceBuf.buffer_out);
    init_buffer_int(&KeyPrinterBuf.buffer_in);
}
/**
 * Creates N keys and sends it out  
 */
void KeySource() {
    FOR(uint32_t, i, 0, <, TheGlobal_N, i++) 
        push_int(&KeySourceBuf.buffer_out, KeySourceBuf.A[i]);
    ENDFOR
} 

/**
 * Prints out the sorted keys and verifies if they are sorted.
 */
void KeyPrinter() {
    FOR(uint32_t, i, 0, <, TheGlobal_N, i++) 
        printf("%i", pop_int(&KeyPrinterBuf.buffer_in));
    ENDFOR
}

void Split2ROUND_ROBIN() {
    FOR(uint32_t, i, 0, <, TheGlobal_N/2, i++)
        push_int(&StepOfMerge[i].buffer_in, pop_int(&KeySourceBuf.buffer_out));
        push_int(&StepOfMerge[i].buffer_in, pop_int(&KeySourceBuf.buffer_out));
    ENDFOR
}

void Join2ROUND_ROBIN() {
    FOR(uint32_t, i, 0, <, TheGlobal_N/2, i++)
        push_int(&, pop_int(&StepOfMerge[i].buffer_out));
        push_int(&, pop_int(&StepOfMerge[i].buffer_out));
    ENDFOR
}

void main(int argv, char** argc) {
    BitonicSort_init();
    
    FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++) 
    	KeySource();
    	//BitonicSortKernel();
            //MergeStage();
                //StepOfMerge();
                Split2ROUND_ROBIN();
                    FOR(uint32_t, i, 0, <, TheGlobal_N/2, i++)
                        CompareExchange(&StepOfMerge[i].buffer_in, &StepOfMerge[i].buffer_out);
                    ENDFOR
                Join2ROUND_ROBIN();
                
        //LastMergeStage();
            //StepOfLastMerge();
            Split3ROUND_ROBIN(L);
                //PartitionBitonicSequence();
                _80211_r6_split1_round_robin();
                    CompareExchange(TheGlobal_SortDir);
                    CompareExchange(TheGlobal_SortDir);
                Join1ROUND_ROBIN();
            _80211_r6_join3_round_robin(L);
            //StepOfLastMerge();
            Split3ROUND_ROBIN(L);
                CompareExchange(TheGlobal_SortDir);
                CompareExchange(TheGlobal_SortDir);
            _80211_r6_join3_round_robin(L);
    	KeyPrinter();
    ENDFOR
} 
