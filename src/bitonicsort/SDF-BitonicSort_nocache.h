#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=32 on the compile command line
#else
#if BUF_SIZEMAX < 32
#error BUF_SIZEMAX too small, it must be at least 32
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	int A[16];
} KeySource_202_t;
void KeySource_202();
void WEIGHTED_ROUND_ROBIN_Splitter_670();
void WEIGHTED_ROUND_ROBIN_Splitter_672();
void CompareExchange_203();
void CompareExchange_204();
void WEIGHTED_ROUND_ROBIN_Joiner_673();
void WEIGHTED_ROUND_ROBIN_Splitter_674();
void CompareExchange_205();
void CompareExchange_206();
void WEIGHTED_ROUND_ROBIN_Joiner_675();
void WEIGHTED_ROUND_ROBIN_Splitter_676();
void CompareExchange_207();
void CompareExchange_208();
void WEIGHTED_ROUND_ROBIN_Joiner_677();
void WEIGHTED_ROUND_ROBIN_Splitter_678();
void CompareExchange_209();
void CompareExchange_210();
void WEIGHTED_ROUND_ROBIN_Joiner_679();
void WEIGHTED_ROUND_ROBIN_Joiner_671();
void WEIGHTED_ROUND_ROBIN_Splitter_556();
void Pre_CollapsedDataParallel_1_505();
void CompareExchange_211();
void Post_CollapsedDataParallel_2_506();
void Pre_CollapsedDataParallel_1_508();
void CompareExchange_213();
void Post_CollapsedDataParallel_2_509();
void Pre_CollapsedDataParallel_1_511();
void CompareExchange_215();
void Post_CollapsedDataParallel_2_512();
void Pre_CollapsedDataParallel_1_514();
void CompareExchange_217();
void Post_CollapsedDataParallel_2_515();
void WEIGHTED_ROUND_ROBIN_Joiner_557();
void WEIGHTED_ROUND_ROBIN_Splitter_680();
void WEIGHTED_ROUND_ROBIN_Splitter_682();
void WEIGHTED_ROUND_ROBIN_Splitter_684();
void CompareExchange_219();
void CompareExchange_220();
void WEIGHTED_ROUND_ROBIN_Joiner_685();
void WEIGHTED_ROUND_ROBIN_Splitter_686();
void CompareExchange_221();
void CompareExchange_222();
void WEIGHTED_ROUND_ROBIN_Joiner_687();
void WEIGHTED_ROUND_ROBIN_Joiner_683();
void WEIGHTED_ROUND_ROBIN_Splitter_688();
void WEIGHTED_ROUND_ROBIN_Splitter_690();
void CompareExchange_223();
void CompareExchange_224();
void WEIGHTED_ROUND_ROBIN_Joiner_691();
void WEIGHTED_ROUND_ROBIN_Splitter_692();
void CompareExchange_225();
void CompareExchange_226();
void WEIGHTED_ROUND_ROBIN_Joiner_693();
void WEIGHTED_ROUND_ROBIN_Joiner_689();
void WEIGHTED_ROUND_ROBIN_Joiner_681();
void WEIGHTED_ROUND_ROBIN_Splitter_560();
void Pre_CollapsedDataParallel_1_517();
void CompareExchange_227();
void Post_CollapsedDataParallel_2_518();
void WEIGHTED_ROUND_ROBIN_Splitter_695();
void Pre_CollapsedDataParallel_1_523();
void CompareExchange_235();
void Post_CollapsedDataParallel_2_524();
void Pre_CollapsedDataParallel_1_526();
void CompareExchange_237();
void Post_CollapsedDataParallel_2_527();
void WEIGHTED_ROUND_ROBIN_Joiner_696();
void Pre_CollapsedDataParallel_1_520();
void CompareExchange_231();
void Post_CollapsedDataParallel_2_521();
void WEIGHTED_ROUND_ROBIN_Splitter_697();
void Pre_CollapsedDataParallel_1_529();
void CompareExchange_239();
void Post_CollapsedDataParallel_2_530();
void Pre_CollapsedDataParallel_1_532();
void CompareExchange_241();
void Post_CollapsedDataParallel_2_533();
void WEIGHTED_ROUND_ROBIN_Joiner_698();
void WEIGHTED_ROUND_ROBIN_Joiner_694();
void WEIGHTED_ROUND_ROBIN_Splitter_699();
void WEIGHTED_ROUND_ROBIN_Splitter_701();
void CompareExchange_243();
void CompareExchange_244();
void CompareExchange_245();
void CompareExchange_246();
void WEIGHTED_ROUND_ROBIN_Joiner_702();
void WEIGHTED_ROUND_ROBIN_Splitter_703();
void CompareExchange_247();
void CompareExchange_248();
void CompareExchange_249();
void CompareExchange_250();
void WEIGHTED_ROUND_ROBIN_Joiner_704();
void WEIGHTED_ROUND_ROBIN_Joiner_700();
void Pre_CollapsedDataParallel_1_534();
void CompareExchange_251();
void Post_CollapsedDataParallel_2_535();
void WEIGHTED_ROUND_ROBIN_Splitter_566();
void Pre_CollapsedDataParallel_1_537();
void CompareExchange_259();
void Post_CollapsedDataParallel_2_538();
void WEIGHTED_ROUND_ROBIN_Splitter_706();
void Pre_CollapsedDataParallel_1_543();
void CompareExchange_267();
void Post_CollapsedDataParallel_2_544();
void Pre_CollapsedDataParallel_1_546();
void CompareExchange_269();
void Post_CollapsedDataParallel_2_547();
void WEIGHTED_ROUND_ROBIN_Joiner_707();
void Pre_CollapsedDataParallel_1_540();
void CompareExchange_263();
void Post_CollapsedDataParallel_2_541();
void WEIGHTED_ROUND_ROBIN_Splitter_708();
void Pre_CollapsedDataParallel_1_549();
void CompareExchange_271();
void Post_CollapsedDataParallel_2_550();
void Pre_CollapsedDataParallel_1_552();
void CompareExchange_273();
void Post_CollapsedDataParallel_2_553();
void WEIGHTED_ROUND_ROBIN_Joiner_709();
void WEIGHTED_ROUND_ROBIN_Joiner_705();
void CompareExchange_275();
void KeyPrinter_283();

#ifdef __cplusplus
}
#endif
#endif
