#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=32 on the compile command line
#else
#if BUF_SIZEMAX < 32
#error BUF_SIZEMAX too small, it must be at least 32
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	int A[16];
} KeySource_1409_t;
void KeySource_1409();
void WEIGHTED_ROUND_ROBIN_Splitter_1877();
void WEIGHTED_ROUND_ROBIN_Splitter_1878();
void CompareExchange_1410();
void CompareExchange_1411();
void WEIGHTED_ROUND_ROBIN_Joiner_1879();
void WEIGHTED_ROUND_ROBIN_Splitter_1880();
void CompareExchange_1412();
void CompareExchange_1413();
void WEIGHTED_ROUND_ROBIN_Joiner_1881();
void WEIGHTED_ROUND_ROBIN_Splitter_1882();
void CompareExchange_1414();
void CompareExchange_1415();
void WEIGHTED_ROUND_ROBIN_Joiner_1883();
void WEIGHTED_ROUND_ROBIN_Splitter_1884();
void CompareExchange_1416();
void CompareExchange_1417();
void WEIGHTED_ROUND_ROBIN_Joiner_1885();
void WEIGHTED_ROUND_ROBIN_Joiner_1886();
void WEIGHTED_ROUND_ROBIN_Splitter_1763();
void Pre_CollapsedDataParallel_1_1712();
void WEIGHTED_ROUND_ROBIN_Splitter_1920();
void CompareExchange_1922();
void CompareExchange_1923();
void WEIGHTED_ROUND_ROBIN_Joiner_1921();
void Post_CollapsedDataParallel_2_1713();
void Pre_CollapsedDataParallel_1_1715();
void WEIGHTED_ROUND_ROBIN_Splitter_1924();
void CompareExchange_1926();
void CompareExchange_1927();
void WEIGHTED_ROUND_ROBIN_Joiner_1925();
void Post_CollapsedDataParallel_2_1716();
void Pre_CollapsedDataParallel_1_1718();
void WEIGHTED_ROUND_ROBIN_Splitter_1928();
void CompareExchange_1930();
void CompareExchange_1931();
void WEIGHTED_ROUND_ROBIN_Joiner_1929();
void Post_CollapsedDataParallel_2_1719();
void Pre_CollapsedDataParallel_1_1721();
void WEIGHTED_ROUND_ROBIN_Splitter_1932();
void CompareExchange_1934();
void CompareExchange_1935();
void WEIGHTED_ROUND_ROBIN_Joiner_1933();
void Post_CollapsedDataParallel_2_1722();
void WEIGHTED_ROUND_ROBIN_Joiner_1764();
void WEIGHTED_ROUND_ROBIN_Splitter_1887();
void WEIGHTED_ROUND_ROBIN_Splitter_1888();
void WEIGHTED_ROUND_ROBIN_Splitter_1889();
void CompareExchange_1426();
void CompareExchange_1427();
void WEIGHTED_ROUND_ROBIN_Joiner_1890();
void WEIGHTED_ROUND_ROBIN_Splitter_1891();
void CompareExchange_1428();
void CompareExchange_1429();
void WEIGHTED_ROUND_ROBIN_Joiner_1892();
void WEIGHTED_ROUND_ROBIN_Joiner_1893();
void WEIGHTED_ROUND_ROBIN_Splitter_1894();
void WEIGHTED_ROUND_ROBIN_Splitter_1895();
void CompareExchange_1430();
void CompareExchange_1431();
void WEIGHTED_ROUND_ROBIN_Joiner_1896();
void WEIGHTED_ROUND_ROBIN_Splitter_1897();
void CompareExchange_1432();
void CompareExchange_1433();
void WEIGHTED_ROUND_ROBIN_Joiner_1898();
void WEIGHTED_ROUND_ROBIN_Joiner_1899();
void WEIGHTED_ROUND_ROBIN_Joiner_1900();
void WEIGHTED_ROUND_ROBIN_Splitter_1767();
void Pre_CollapsedDataParallel_1_1724();
void WEIGHTED_ROUND_ROBIN_Splitter_1936();
void CompareExchange_1938();
void CompareExchange_1939();
void CompareExchange_1940();
void CompareExchange_1941();
void WEIGHTED_ROUND_ROBIN_Joiner_1937();
void Post_CollapsedDataParallel_2_1725();
void WEIGHTED_ROUND_ROBIN_Splitter_1901();
void Pre_CollapsedDataParallel_1_1730();
void WEIGHTED_ROUND_ROBIN_Splitter_1942();
void CompareExchange_1944();
void CompareExchange_1945();
void WEIGHTED_ROUND_ROBIN_Joiner_1943();
void Post_CollapsedDataParallel_2_1731();
void Pre_CollapsedDataParallel_1_1733();
void WEIGHTED_ROUND_ROBIN_Splitter_1946();
void CompareExchange_1948();
void CompareExchange_1949();
void WEIGHTED_ROUND_ROBIN_Joiner_1947();
void Post_CollapsedDataParallel_2_1734();
void WEIGHTED_ROUND_ROBIN_Joiner_1902();
void Pre_CollapsedDataParallel_1_1727();
void WEIGHTED_ROUND_ROBIN_Splitter_1950();
void CompareExchange_1952();
void CompareExchange_1953();
void CompareExchange_1954();
void CompareExchange_1955();
void WEIGHTED_ROUND_ROBIN_Joiner_1951();
void Post_CollapsedDataParallel_2_1728();
void WEIGHTED_ROUND_ROBIN_Splitter_1903();
void Pre_CollapsedDataParallel_1_1736();
void WEIGHTED_ROUND_ROBIN_Splitter_1956();
void CompareExchange_1958();
void CompareExchange_1959();
void WEIGHTED_ROUND_ROBIN_Joiner_1957();
void Post_CollapsedDataParallel_2_1737();
void Pre_CollapsedDataParallel_1_1739();
void WEIGHTED_ROUND_ROBIN_Splitter_1960();
void CompareExchange_1962();
void CompareExchange_1963();
void WEIGHTED_ROUND_ROBIN_Joiner_1961();
void Post_CollapsedDataParallel_2_1740();
void WEIGHTED_ROUND_ROBIN_Joiner_1904();
void WEIGHTED_ROUND_ROBIN_Joiner_1905();
void WEIGHTED_ROUND_ROBIN_Splitter_1906();
void WEIGHTED_ROUND_ROBIN_Splitter_1907();
void CompareExchange_1450();
void CompareExchange_1451();
void CompareExchange_1452();
void CompareExchange_1453();
void WEIGHTED_ROUND_ROBIN_Joiner_1908();
void WEIGHTED_ROUND_ROBIN_Splitter_1909();
void CompareExchange_1454();
void CompareExchange_1455();
void CompareExchange_1456();
void CompareExchange_1457();
void WEIGHTED_ROUND_ROBIN_Joiner_1910();
void WEIGHTED_ROUND_ROBIN_Joiner_1911();
void Pre_CollapsedDataParallel_1_1741();
void WEIGHTED_ROUND_ROBIN_Splitter_1964();
void CompareExchange_1966();
void CompareExchange_1967();
void CompareExchange_1968();
void CompareExchange_1969();
void CompareExchange_1970();
void CompareExchange_1971();
void CompareExchange_1972();
void CompareExchange_1973();
void WEIGHTED_ROUND_ROBIN_Joiner_1965();
void Post_CollapsedDataParallel_2_1742();
void WEIGHTED_ROUND_ROBIN_Splitter_1773();
void Pre_CollapsedDataParallel_1_1744();
void WEIGHTED_ROUND_ROBIN_Splitter_1974();
void CompareExchange_1976();
void CompareExchange_1977();
void CompareExchange_1978();
void CompareExchange_1979();
void WEIGHTED_ROUND_ROBIN_Joiner_1975();
void Post_CollapsedDataParallel_2_1745();
void WEIGHTED_ROUND_ROBIN_Splitter_1912();
void Pre_CollapsedDataParallel_1_1750();
void WEIGHTED_ROUND_ROBIN_Splitter_1980();
void CompareExchange_1982();
void CompareExchange_1983();
void WEIGHTED_ROUND_ROBIN_Joiner_1981();
void Post_CollapsedDataParallel_2_1751();
void Pre_CollapsedDataParallel_1_1753();
void WEIGHTED_ROUND_ROBIN_Splitter_1984();
void CompareExchange_1986();
void CompareExchange_1987();
void WEIGHTED_ROUND_ROBIN_Joiner_1985();
void Post_CollapsedDataParallel_2_1754();
void WEIGHTED_ROUND_ROBIN_Joiner_1913();
void Pre_CollapsedDataParallel_1_1747();
void WEIGHTED_ROUND_ROBIN_Splitter_1988();
void CompareExchange_1990();
void CompareExchange_1991();
void CompareExchange_1992();
void CompareExchange_1993();
void WEIGHTED_ROUND_ROBIN_Joiner_1989();
void Post_CollapsedDataParallel_2_1748();
void WEIGHTED_ROUND_ROBIN_Splitter_1914();
void Pre_CollapsedDataParallel_1_1756();
void WEIGHTED_ROUND_ROBIN_Splitter_1994();
void CompareExchange_1996();
void CompareExchange_1997();
void WEIGHTED_ROUND_ROBIN_Joiner_1995();
void Post_CollapsedDataParallel_2_1757();
void Pre_CollapsedDataParallel_1_1759();
void WEIGHTED_ROUND_ROBIN_Splitter_1998();
void CompareExchange_2000();
void CompareExchange_2001();
void WEIGHTED_ROUND_ROBIN_Joiner_1999();
void Post_CollapsedDataParallel_2_1760();
void WEIGHTED_ROUND_ROBIN_Joiner_1915();
void WEIGHTED_ROUND_ROBIN_Joiner_1916();
void WEIGHTED_ROUND_ROBIN_Splitter_2002();
void CompareExchange_2004();
void CompareExchange_2005();
void CompareExchange_2006();
void CompareExchange_2007();
void CompareExchange_2008();
void CompareExchange_2009();
void CompareExchange_2010();
void CompareExchange_2011();
void WEIGHTED_ROUND_ROBIN_Joiner_2003();
void KeyPrinter_1490();

#ifdef __cplusplus
}
#endif
#endif
