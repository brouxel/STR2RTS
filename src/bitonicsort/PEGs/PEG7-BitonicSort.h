#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=224 on the compile command line
#else
#if BUF_SIZEMAX < 224
#error BUF_SIZEMAX too small, it must be at least 224
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	int A[16];
} KeySource_2801_t;
void KeySource(buffer_int_t *chanout);
void KeySource_2801();
void WEIGHTED_ROUND_ROBIN_Splitter_3269();
void WEIGHTED_ROUND_ROBIN_Splitter_3270();
void CompareExchange(buffer_int_t *chanin, buffer_int_t *chanout);
void CompareExchange_2802();
void CompareExchange_2803();
void WEIGHTED_ROUND_ROBIN_Joiner_3271();
void WEIGHTED_ROUND_ROBIN_Splitter_3272();
void CompareExchange_2804();
void CompareExchange_2805();
void WEIGHTED_ROUND_ROBIN_Joiner_3273();
void WEIGHTED_ROUND_ROBIN_Splitter_3274();
void CompareExchange_2806();
void CompareExchange_2807();
void WEIGHTED_ROUND_ROBIN_Joiner_3275();
void WEIGHTED_ROUND_ROBIN_Splitter_3276();
void CompareExchange_2808();
void CompareExchange_2809();
void WEIGHTED_ROUND_ROBIN_Joiner_3277();
void WEIGHTED_ROUND_ROBIN_Joiner_3278();
void WEIGHTED_ROUND_ROBIN_Splitter_3155();
void Pre_CollapsedDataParallel_1(buffer_int_t *chanin, buffer_int_t *chanout);
void Pre_CollapsedDataParallel_1_3104();
void WEIGHTED_ROUND_ROBIN_Splitter_3312();
void CompareExchange_3314();
void CompareExchange_3315();
void WEIGHTED_ROUND_ROBIN_Joiner_3313();
void Post_CollapsedDataParallel_2(buffer_int_t *chanin, buffer_int_t *chanout);
void Post_CollapsedDataParallel_2_3105();
void Pre_CollapsedDataParallel_1_3107();
void WEIGHTED_ROUND_ROBIN_Splitter_3316();
void CompareExchange_3318();
void CompareExchange_3319();
void WEIGHTED_ROUND_ROBIN_Joiner_3317();
void Post_CollapsedDataParallel_2_3108();
void Pre_CollapsedDataParallel_1_3110();
void WEIGHTED_ROUND_ROBIN_Splitter_3320();
void CompareExchange_3322();
void CompareExchange_3323();
void WEIGHTED_ROUND_ROBIN_Joiner_3321();
void Post_CollapsedDataParallel_2_3111();
void Pre_CollapsedDataParallel_1_3113();
void WEIGHTED_ROUND_ROBIN_Splitter_3324();
void CompareExchange_3326();
void CompareExchange_3327();
void WEIGHTED_ROUND_ROBIN_Joiner_3325();
void Post_CollapsedDataParallel_2_3114();
void WEIGHTED_ROUND_ROBIN_Joiner_3156();
void WEIGHTED_ROUND_ROBIN_Splitter_3279();
void WEIGHTED_ROUND_ROBIN_Splitter_3280();
void WEIGHTED_ROUND_ROBIN_Splitter_3281();
void CompareExchange_2818();
void CompareExchange_2819();
void WEIGHTED_ROUND_ROBIN_Joiner_3282();
void WEIGHTED_ROUND_ROBIN_Splitter_3283();
void CompareExchange_2820();
void CompareExchange_2821();
void WEIGHTED_ROUND_ROBIN_Joiner_3284();
void WEIGHTED_ROUND_ROBIN_Joiner_3285();
void WEIGHTED_ROUND_ROBIN_Splitter_3286();
void WEIGHTED_ROUND_ROBIN_Splitter_3287();
void CompareExchange_2822();
void CompareExchange_2823();
void WEIGHTED_ROUND_ROBIN_Joiner_3288();
void WEIGHTED_ROUND_ROBIN_Splitter_3289();
void CompareExchange_2824();
void CompareExchange_2825();
void WEIGHTED_ROUND_ROBIN_Joiner_3290();
void WEIGHTED_ROUND_ROBIN_Joiner_3291();
void WEIGHTED_ROUND_ROBIN_Joiner_3292();
void WEIGHTED_ROUND_ROBIN_Splitter_3159();
void Pre_CollapsedDataParallel_1_3116();
void WEIGHTED_ROUND_ROBIN_Splitter_3328();
void CompareExchange_3330();
void CompareExchange_3331();
void CompareExchange_3332();
void CompareExchange_3333();
void WEIGHTED_ROUND_ROBIN_Joiner_3329();
void Post_CollapsedDataParallel_2_3117();
void WEIGHTED_ROUND_ROBIN_Splitter_3293();
void Pre_CollapsedDataParallel_1_3122();
void WEIGHTED_ROUND_ROBIN_Splitter_3334();
void CompareExchange_3336();
void CompareExchange_3337();
void WEIGHTED_ROUND_ROBIN_Joiner_3335();
void Post_CollapsedDataParallel_2_3123();
void Pre_CollapsedDataParallel_1_3125();
void WEIGHTED_ROUND_ROBIN_Splitter_3338();
void CompareExchange_3340();
void CompareExchange_3341();
void WEIGHTED_ROUND_ROBIN_Joiner_3339();
void Post_CollapsedDataParallel_2_3126();
void WEIGHTED_ROUND_ROBIN_Joiner_3294();
void Pre_CollapsedDataParallel_1_3119();
void WEIGHTED_ROUND_ROBIN_Splitter_3342();
void CompareExchange_3344();
void CompareExchange_3345();
void CompareExchange_3346();
void CompareExchange_3347();
void WEIGHTED_ROUND_ROBIN_Joiner_3343();
void Post_CollapsedDataParallel_2_3120();
void WEIGHTED_ROUND_ROBIN_Splitter_3295();
void Pre_CollapsedDataParallel_1_3128();
void WEIGHTED_ROUND_ROBIN_Splitter_3348();
void CompareExchange_3350();
void CompareExchange_3351();
void WEIGHTED_ROUND_ROBIN_Joiner_3349();
void Post_CollapsedDataParallel_2_3129();
void Pre_CollapsedDataParallel_1_3131();
void WEIGHTED_ROUND_ROBIN_Splitter_3352();
void CompareExchange_3354();
void CompareExchange_3355();
void WEIGHTED_ROUND_ROBIN_Joiner_3353();
void Post_CollapsedDataParallel_2_3132();
void WEIGHTED_ROUND_ROBIN_Joiner_3296();
void WEIGHTED_ROUND_ROBIN_Joiner_3297();
void WEIGHTED_ROUND_ROBIN_Splitter_3298();
void WEIGHTED_ROUND_ROBIN_Splitter_3299();
void CompareExchange_2842();
void CompareExchange_2843();
void CompareExchange_2844();
void CompareExchange_2845();
void WEIGHTED_ROUND_ROBIN_Joiner_3300();
void WEIGHTED_ROUND_ROBIN_Splitter_3301();
void CompareExchange_2846();
void CompareExchange_2847();
void CompareExchange_2848();
void CompareExchange_2849();
void WEIGHTED_ROUND_ROBIN_Joiner_3302();
void WEIGHTED_ROUND_ROBIN_Joiner_3303();
void Pre_CollapsedDataParallel_1_3133();
void WEIGHTED_ROUND_ROBIN_Splitter_3356();
void CompareExchange_3358();
void CompareExchange_3359();
void CompareExchange_3360();
void CompareExchange_3361();
void CompareExchange_3362();
void CompareExchange_3363();
void CompareExchange_3364();
void WEIGHTED_ROUND_ROBIN_Joiner_3357();
void Post_CollapsedDataParallel_2_3134();
void WEIGHTED_ROUND_ROBIN_Splitter_3165();
void Pre_CollapsedDataParallel_1_3136();
void WEIGHTED_ROUND_ROBIN_Splitter_3365();
void CompareExchange_3367();
void CompareExchange_3368();
void CompareExchange_3369();
void CompareExchange_3370();
void WEIGHTED_ROUND_ROBIN_Joiner_3366();
void Post_CollapsedDataParallel_2_3137();
void WEIGHTED_ROUND_ROBIN_Splitter_3304();
void Pre_CollapsedDataParallel_1_3142();
void WEIGHTED_ROUND_ROBIN_Splitter_3371();
void CompareExchange_3373();
void CompareExchange_3374();
void WEIGHTED_ROUND_ROBIN_Joiner_3372();
void Post_CollapsedDataParallel_2_3143();
void Pre_CollapsedDataParallel_1_3145();
void WEIGHTED_ROUND_ROBIN_Splitter_3375();
void CompareExchange_3377();
void CompareExchange_3378();
void WEIGHTED_ROUND_ROBIN_Joiner_3376();
void Post_CollapsedDataParallel_2_3146();
void WEIGHTED_ROUND_ROBIN_Joiner_3305();
void Pre_CollapsedDataParallel_1_3139();
void WEIGHTED_ROUND_ROBIN_Splitter_3379();
void CompareExchange_3381();
void CompareExchange_3382();
void CompareExchange_3383();
void CompareExchange_3384();
void WEIGHTED_ROUND_ROBIN_Joiner_3380();
void Post_CollapsedDataParallel_2_3140();
void WEIGHTED_ROUND_ROBIN_Splitter_3306();
void Pre_CollapsedDataParallel_1_3148();
void WEIGHTED_ROUND_ROBIN_Splitter_3385();
void CompareExchange_3387();
void CompareExchange_3388();
void WEIGHTED_ROUND_ROBIN_Joiner_3386();
void Post_CollapsedDataParallel_2_3149();
void Pre_CollapsedDataParallel_1_3151();
void WEIGHTED_ROUND_ROBIN_Splitter_3389();
void CompareExchange_3391();
void CompareExchange_3392();
void WEIGHTED_ROUND_ROBIN_Joiner_3390();
void Post_CollapsedDataParallel_2_3152();
void WEIGHTED_ROUND_ROBIN_Joiner_3307();
void WEIGHTED_ROUND_ROBIN_Joiner_3308();
void WEIGHTED_ROUND_ROBIN_Splitter_3393();
void CompareExchange_3395();
void CompareExchange_3396();
void CompareExchange_3397();
void CompareExchange_3398();
void CompareExchange_3399();
void CompareExchange_3400();
void CompareExchange_3401();
void WEIGHTED_ROUND_ROBIN_Joiner_3394();
void KeyPrinter(buffer_int_t *chanin);
void KeyPrinter_2882();

#ifdef __cplusplus
}
#endif
#endif
