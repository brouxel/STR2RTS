#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=32 on the compile command line
#else
#if BUF_SIZEMAX < 32
#error BUF_SIZEMAX too small, it must be at least 32
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	int A[16];
} KeySource_5492_t;
void KeySource(buffer_int_t *chanout);
void KeySource_5492();
void WEIGHTED_ROUND_ROBIN_Splitter_5960();
void WEIGHTED_ROUND_ROBIN_Splitter_5961();
void CompareExchange(buffer_int_t *chanin, buffer_int_t *chanout);
void CompareExchange_5493();
void CompareExchange_5494();
void WEIGHTED_ROUND_ROBIN_Joiner_5962();
void WEIGHTED_ROUND_ROBIN_Splitter_5963();
void CompareExchange_5495();
void CompareExchange_5496();
void WEIGHTED_ROUND_ROBIN_Joiner_5964();
void WEIGHTED_ROUND_ROBIN_Splitter_5965();
void CompareExchange_5497();
void CompareExchange_5498();
void WEIGHTED_ROUND_ROBIN_Joiner_5966();
void WEIGHTED_ROUND_ROBIN_Splitter_5967();
void CompareExchange_5499();
void CompareExchange_5500();
void WEIGHTED_ROUND_ROBIN_Joiner_5968();
void WEIGHTED_ROUND_ROBIN_Joiner_5969();
void WEIGHTED_ROUND_ROBIN_Splitter_5846();
void Pre_CollapsedDataParallel_1(buffer_int_t *chanin, buffer_int_t *chanout);
void Pre_CollapsedDataParallel_1_5795();
void WEIGHTED_ROUND_ROBIN_Splitter_6003();
void CompareExchange_6005();
void CompareExchange_6006();
void WEIGHTED_ROUND_ROBIN_Joiner_6004();
void Post_CollapsedDataParallel_2(buffer_int_t *chanin, buffer_int_t *chanout);
void Post_CollapsedDataParallel_2_5796();
void Pre_CollapsedDataParallel_1_5798();
void WEIGHTED_ROUND_ROBIN_Splitter_6007();
void CompareExchange_6009();
void CompareExchange_6010();
void WEIGHTED_ROUND_ROBIN_Joiner_6008();
void Post_CollapsedDataParallel_2_5799();
void Pre_CollapsedDataParallel_1_5801();
void WEIGHTED_ROUND_ROBIN_Splitter_6011();
void CompareExchange_6013();
void CompareExchange_6014();
void WEIGHTED_ROUND_ROBIN_Joiner_6012();
void Post_CollapsedDataParallel_2_5802();
void Pre_CollapsedDataParallel_1_5804();
void WEIGHTED_ROUND_ROBIN_Splitter_6015();
void CompareExchange_6017();
void CompareExchange_6018();
void WEIGHTED_ROUND_ROBIN_Joiner_6016();
void Post_CollapsedDataParallel_2_5805();
void WEIGHTED_ROUND_ROBIN_Joiner_5847();
void WEIGHTED_ROUND_ROBIN_Splitter_5970();
void WEIGHTED_ROUND_ROBIN_Splitter_5971();
void WEIGHTED_ROUND_ROBIN_Splitter_5972();
void CompareExchange_5509();
void CompareExchange_5510();
void WEIGHTED_ROUND_ROBIN_Joiner_5973();
void WEIGHTED_ROUND_ROBIN_Splitter_5974();
void CompareExchange_5511();
void CompareExchange_5512();
void WEIGHTED_ROUND_ROBIN_Joiner_5975();
void WEIGHTED_ROUND_ROBIN_Joiner_5976();
void WEIGHTED_ROUND_ROBIN_Splitter_5977();
void WEIGHTED_ROUND_ROBIN_Splitter_5978();
void CompareExchange_5513();
void CompareExchange_5514();
void WEIGHTED_ROUND_ROBIN_Joiner_5979();
void WEIGHTED_ROUND_ROBIN_Splitter_5980();
void CompareExchange_5515();
void CompareExchange_5516();
void WEIGHTED_ROUND_ROBIN_Joiner_5981();
void WEIGHTED_ROUND_ROBIN_Joiner_5982();
void WEIGHTED_ROUND_ROBIN_Joiner_5983();
void WEIGHTED_ROUND_ROBIN_Splitter_5850();
void Pre_CollapsedDataParallel_1_5807();
void WEIGHTED_ROUND_ROBIN_Splitter_6019();
void CompareExchange_6021();
void CompareExchange_6022();
void CompareExchange_6023();
void CompareExchange_6024();
void WEIGHTED_ROUND_ROBIN_Joiner_6020();
void Post_CollapsedDataParallel_2_5808();
void WEIGHTED_ROUND_ROBIN_Splitter_5984();
void Pre_CollapsedDataParallel_1_5813();
void WEIGHTED_ROUND_ROBIN_Splitter_6025();
void CompareExchange_6027();
void CompareExchange_6028();
void WEIGHTED_ROUND_ROBIN_Joiner_6026();
void Post_CollapsedDataParallel_2_5814();
void Pre_CollapsedDataParallel_1_5816();
void WEIGHTED_ROUND_ROBIN_Splitter_6029();
void CompareExchange_6031();
void CompareExchange_6032();
void WEIGHTED_ROUND_ROBIN_Joiner_6030();
void Post_CollapsedDataParallel_2_5817();
void WEIGHTED_ROUND_ROBIN_Joiner_5985();
void Pre_CollapsedDataParallel_1_5810();
void WEIGHTED_ROUND_ROBIN_Splitter_6033();
void CompareExchange_6035();
void CompareExchange_6036();
void CompareExchange_6037();
void CompareExchange_6038();
void WEIGHTED_ROUND_ROBIN_Joiner_6034();
void Post_CollapsedDataParallel_2_5811();
void WEIGHTED_ROUND_ROBIN_Splitter_5986();
void Pre_CollapsedDataParallel_1_5819();
void WEIGHTED_ROUND_ROBIN_Splitter_6039();
void CompareExchange_6041();
void CompareExchange_6042();
void WEIGHTED_ROUND_ROBIN_Joiner_6040();
void Post_CollapsedDataParallel_2_5820();
void Pre_CollapsedDataParallel_1_5822();
void WEIGHTED_ROUND_ROBIN_Splitter_6043();
void CompareExchange_6045();
void CompareExchange_6046();
void WEIGHTED_ROUND_ROBIN_Joiner_6044();
void Post_CollapsedDataParallel_2_5823();
void WEIGHTED_ROUND_ROBIN_Joiner_5987();
void WEIGHTED_ROUND_ROBIN_Joiner_5988();
void WEIGHTED_ROUND_ROBIN_Splitter_5989();
void WEIGHTED_ROUND_ROBIN_Splitter_5990();
void CompareExchange_5533();
void CompareExchange_5534();
void CompareExchange_5535();
void CompareExchange_5536();
void WEIGHTED_ROUND_ROBIN_Joiner_5991();
void WEIGHTED_ROUND_ROBIN_Splitter_5992();
void CompareExchange_5537();
void CompareExchange_5538();
void CompareExchange_5539();
void CompareExchange_5540();
void WEIGHTED_ROUND_ROBIN_Joiner_5993();
void WEIGHTED_ROUND_ROBIN_Joiner_5994();
void Pre_CollapsedDataParallel_1_5824();
void WEIGHTED_ROUND_ROBIN_Splitter_6047();
void CompareExchange_6049();
void CompareExchange_6050();
void CompareExchange_6051();
void CompareExchange_6052();
void WEIGHTED_ROUND_ROBIN_Joiner_6048();
void Post_CollapsedDataParallel_2_5825();
void WEIGHTED_ROUND_ROBIN_Splitter_5856();
void Pre_CollapsedDataParallel_1_5827();
void WEIGHTED_ROUND_ROBIN_Splitter_6053();
void CompareExchange_6055();
void CompareExchange_6056();
void CompareExchange_6057();
void CompareExchange_6058();
void WEIGHTED_ROUND_ROBIN_Joiner_6054();
void Post_CollapsedDataParallel_2_5828();
void WEIGHTED_ROUND_ROBIN_Splitter_5995();
void Pre_CollapsedDataParallel_1_5833();
void WEIGHTED_ROUND_ROBIN_Splitter_6059();
void CompareExchange_6061();
void CompareExchange_6062();
void WEIGHTED_ROUND_ROBIN_Joiner_6060();
void Post_CollapsedDataParallel_2_5834();
void Pre_CollapsedDataParallel_1_5836();
void WEIGHTED_ROUND_ROBIN_Splitter_6063();
void CompareExchange_6065();
void CompareExchange_6066();
void WEIGHTED_ROUND_ROBIN_Joiner_6064();
void Post_CollapsedDataParallel_2_5837();
void WEIGHTED_ROUND_ROBIN_Joiner_5996();
void Pre_CollapsedDataParallel_1_5830();
void WEIGHTED_ROUND_ROBIN_Splitter_6067();
void CompareExchange_6069();
void CompareExchange_6070();
void CompareExchange_6071();
void CompareExchange_6072();
void WEIGHTED_ROUND_ROBIN_Joiner_6068();
void Post_CollapsedDataParallel_2_5831();
void WEIGHTED_ROUND_ROBIN_Splitter_5997();
void Pre_CollapsedDataParallel_1_5839();
void WEIGHTED_ROUND_ROBIN_Splitter_6073();
void CompareExchange_6075();
void CompareExchange_6076();
void WEIGHTED_ROUND_ROBIN_Joiner_6074();
void Post_CollapsedDataParallel_2_5840();
void Pre_CollapsedDataParallel_1_5842();
void WEIGHTED_ROUND_ROBIN_Splitter_6077();
void CompareExchange_6079();
void CompareExchange_6080();
void WEIGHTED_ROUND_ROBIN_Joiner_6078();
void Post_CollapsedDataParallel_2_5843();
void WEIGHTED_ROUND_ROBIN_Joiner_5998();
void WEIGHTED_ROUND_ROBIN_Joiner_5999();
void WEIGHTED_ROUND_ROBIN_Splitter_6081();
void CompareExchange_6083();
void CompareExchange_6084();
void CompareExchange_6085();
void CompareExchange_6086();
void WEIGHTED_ROUND_ROBIN_Joiner_6082();
void KeyPrinter(buffer_int_t *chanin);
void KeyPrinter_5573();

#ifdef __cplusplus
}
#endif
#endif
