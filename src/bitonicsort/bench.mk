BENCH:=sdf_bitonicsort_nocache
BENCHSRCFILE:=$(SRC_DIR)/bitonicsort/SDF-BitonicSort_nocache.c
BENCHHFILE:=$(SRC_DIR)/bitonicsort/SDF-BitonicSort_nocache.h
BENCHEXTRAFLAGS:=-DBUF_SIZEMAX=48

$(eval $(call BENCH_TEMPLATE, $(BENCH), $(BENCHSRCFILE), $(BENCHHFILE), $(BENCHEXTRAFLAGS)))

BENCH:=hsdf_bitonicsort_nocache
BENCHSRCFILE:=$(SRC_DIR)/bitonicsort/HSDF-BitonicSort_nocache.c
BENCHHFILE:=$(SRC_DIR)/bitonicsort/HSDF-BitonicSort_nocache.h
BENCHEXTRAFLAGS:=-DBUF_SIZEMAX=32

$(eval $(call BENCH_TEMPLATE, $(BENCH), $(BENCHSRCFILE), $(BENCHHFILE), $(BENCHEXTRAFLAGS)))

#$(foreach b, $(shell ls $(SRC_DIR)/bitonicsort/PEGs/PEG*.h), \
   $(eval $(call BENCH_TEMPLATE, \
		 $(shell echo $$(basename -s'.h' $(b)) | tr A-Z a-z | tr - _), \
		 $(shell dirname $(b))/$(shell basename -s'.h' $(b)).c, \
		 $(b), \
		 $(shell grep "\-DBUF_SIZEMAX=" $(b) | sed -e 's/.*\(-DBUF_SIZEMAX=[0-9]*\).*/\1/')))\
)