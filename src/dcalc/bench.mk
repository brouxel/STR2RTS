BENCH:=dcalc
BENCHSRCFILE:=$(SRC_DIR)/dcalc/DCalc.c
BENCHHFILE:=$(SRC_DIR)/dcalc/DCalc.h
BENCHEXTRAFLAGS:=-DBUF_SIZEMAX=256

#$(eval $(call BENCH_TEMPLATE, $(BENCH), $(BENCHSRCFILE), $(BENCHHFILE), $(BENCHEXTRAFLAGS)))

BENCH:=sdf_dcalc_nocache
BENCHSRCFILE:=$(SRC_DIR)/dcalc/SDF-dcalc_nocache.c
BENCHHFILE:=$(SRC_DIR)/dcalc/SDF-dcalc_nocache.h
BENCHEXTRAFLAGS:=-DBUF_SIZEMAX=100

$(eval $(call BENCH_TEMPLATE, $(BENCH), $(BENCHSRCFILE), $(BENCHHFILE), $(BENCHEXTRAFLAGS)))

BENCH:=hsdf_dcalc_nocache
BENCHSRCFILE:=$(SRC_DIR)/dcalc/HSDF-dcalc_nocache.c
BENCHHFILE:=$(SRC_DIR)/dcalc/HSDF-dcalc_nocache.h
BENCHEXTRAFLAGS:=-DBUF_SIZEMAX=100

$(eval $(call BENCH_TEMPLATE, $(BENCH), $(BENCHSRCFILE), $(BENCHHFILE), $(BENCHEXTRAFLAGS)))

#$(foreach b, $(shell ls $(SRC_DIR)/dcalc/PEGs/PEG*.h), \
   $(eval $(call BENCH_TEMPLATE, \
		 $(shell echo $$(basename -s'.h' $(b)) | tr A-Z a-z | tr - _), \
		 $(shell dirname $(b))/$(shell basename -s'.h' $(b)).c, \
		 $(b), \
		 $(shell grep "\-DBUF_SIZEMAX=" $(b) | sed -e 's/.*\(-DBUF_SIZEMAX=[0-9]*\).*/\1/')))\
)