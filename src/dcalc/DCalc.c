#include "DCalc.h"

TheGlobal_t TheGlobal;
channel_t SourcehBuf;
channel_t choldBuf;
channel_t ZeroGenBegBuf[TheGlobal_K];
channel_t ZeroGenEndBuf[TheGlobal_K];
channel_t backsBuf;
channel_t FirFilterBuf[TheGlobal_K];
channel_t forwBuf;
channel_t multvectBuf;
channel_t SelfProdBuf;
channel_t SourcerBuf;
channel_t error_estBuf;
channel_t choldsigmaBuf;

channel_t IdLrLBuf;
channel_t IdAhrcholdBuf;
channel_t IdAhrdAhABuf;
channel_t LestBuf;
channel_t AhrdAhABuf;
channel_t LrLBuf;
channel_t vectdoubleBuf[2];
channel_t AhrL1Buf;
channel_t MVectDoubTBuf[2];
channel_t MVectDoubBuf;
channel_t IdSourcerSplitBuf;
channel_t SourceSplitBuf;
channel_t Dsolve;
channel_t SAhrdBuf;
channel_t split_ahrdBuf;
channel_t AhrL1VectDoubBuf;
channel_t IdErrSplitBuf;
channel_t AhrcholdBuf;
channel_t RowcolBuf;
channel_t IdAandLBuf;
channel_t SplitMatTBuf[TheGlobal_K];
channel_t SplitMatBuf;
channel_t ConvMatBuf[TheGlobal_K];
channel_t AandL;
channel_t SAhrdforwBuf;
channel_t IdSAhrdLrLBuf;
channel_t SAhrdbacksBuf;
channel_t IdRowCol[TheGlobal_K*TheGlobal_N];
channel_t SAhrdVectDoubTBuf[2];
channel_t IdCholdAha;
channel_t IdDelMatTBuf[TheGlobal_K];
channel_t DelayBuf[TheGlobal_K][TheGlobal_N];
channel_t IdCMAddZeroBegBuf[TheGlobal_K];
channel_t IdCMAddZeroEndBuf[TheGlobal_K];
channel_t IdAhrL1VectDoubTBuf[2];

void Sourceh() { // dcalc.str:57
    FOR(uint32_t, i, 0, <, TheGlobal_K, i++)  // dcalc.str:60
        FOR(uint32_t, j, 0, <, TheGlobal_W, j++)  // dcalc.str:61
            push_float(&SourcehBuf.buffer_out, TheGlobal.h[j][i]); // dcalc.str:61
        ENDFOR
    ENDFOR
}
void chold() { // dcalc.str:70
    float A [TheGlobal_K*TheGlobal_N][TheGlobal_K*TheGlobal_N]; // dcalc.str:72
    float p [TheGlobal_K*TheGlobal_N]; // dcalc.str:73
    float sum; // dcalc.str:74
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++)  // dcalc.str:77
        FOR(uint32_t, j, 0, <=, i, j++)  // dcalc.str:79
            A[i][j] = pop_float(&choldBuf.buffer_in); // dcalc.str:79
        ENDFOR
    ENDFOR
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++)  // dcalc.str:83
        FOR(uint32_t, j, i, <, TheGlobal_K*TheGlobal_N, j++)  // dcalc.str:84
            sum = A[j][i]; // dcalc.str:85
            FOR2(int, k, (i-1), >=, 0, TheGlobal_K*TheGlobal_N-1, k--)  // dcalc.str:86
                sum -= (A[k][i] * A[k][j]); // dcalc.str:86
            ENDFOR
            if(i == j) { // dcalc.str:88
                p[i] = ((float)(sqrt(sum))); // dcalc.str:89
                push_float(&choldBuf.buffer_out, p[i]); // dcalc.str:90
            } else { // dcalc.str:93
                A[i][j] = (sum / p[i]); // dcalc.str:94
                push_float(&choldBuf.buffer_out, A[i][j]); // dcalc.str:95
            }
        ENDFOR
    ENDFOR
}
void ZeroGenBeg(uint32_t k) { // dcalc.str:164
    FOR(uint32_t, i, 0, <, TheGlobal_Q-1, i++)
        push_float(&ZeroGenBegBuf[k].buffer_out, 0.0f); // dcalc.str:165
    ENDFOR
}
void ZeroGenEnd(uint32_t k) { // dcalc.str:164
    FOR(uint32_t, i, 0, <, TheGlobal_M-(TheGlobal_W+TheGlobal_Q-1), i++)
        push_float(&ZeroGenEndBuf[k].buffer_out, 0.0f); // dcalc.str:165
    ENDFOR
}

void backs(buffer_float_t *in, buffer_float_t *out) { // dcalc.str:190
    float LT [TheGlobal_K*TheGlobal_N][TheGlobal_K*TheGlobal_N]; // dcalc.str:191
    float y [TheGlobal_K*TheGlobal_N]; // dcalc.str:192
    float sum; // dcalc.str:194
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++)  // dcalc.str:197
        y[i] = pop_float(in); // dcalc.str:197
    ENDFOR
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++)  // dcalc.str:200
        FOR(uint32_t, j, i, <, TheGlobal_K*TheGlobal_N, j++)  // dcalc.str:201
            LT[i][j] = pop_float(in); // dcalc.str:201
        ENDFOR
    ENDFOR
    FOR3(int, i, TheGlobal_K*TheGlobal_N-1, >=, 0, i--)  // dcalc.str:205
        sum = y[i]; // dcalc.str:206
        FOR(uint32_t, j, i + 1, <=, TheGlobal_K*TheGlobal_N-1, j++)  // dcalc.str:208
            sum -= (LT[i][j] * y[j]); // dcalc.str:208
        ENDFOR
        y[i] = (sum / LT[i][i]); // dcalc.str:209
        push_float(out, y[i]); // dcalc.str:210
    ENDFOR
}
void backsLrL() {
    backs(&backsBuf.buffer_in, &backsBuf.buffer_out);
}
void backsSahrd() {
    backs(&SAhrdbacksBuf.buffer_in, &SAhrdbacksBuf.buffer_out);
}

void Delay(uint32_t k, uint32_t l) { // dcalc.str:224
    push_float(&DelayBuf[k][l].buffer_out, pop_float(&DelayBuf[k][l].buffer_in)); // dcalc.str:225
}
/*
 * In the original Streamit file, I think there is a bug in the coefficient retrieval
 * FirFilter only uses "C[0][1]=0; and C[1][1]=2;" for every call
 * I corrected it here
 * Note: nbreps is required for the init phase, default value is the steady phase
 */
void FirFilter(uint32_t k, uint32_t nbreps) { // dcalc.str:233
    FOR2(uint32_t, reps, 0, <, nbreps, TheGlobal_Q-1+TheGlobal_W, reps++)
        float sum = 0; // dcalc.str:234
        FOR(uint32_t, i, 0, <, TheGlobal_Q, i++)  // dcalc.str:236
            sum += (peek_float(&FirFilterBuf[k].buffer_in, i) * TheGlobal.COEFF[k][((TheGlobal_Q - 1) - i)]); // dcalc.str:236
        ENDFOR
        printf("====> %f -- %f\n", pop_float(&FirFilterBuf[k].buffer_in), sum); // dcalc.str:237
        push_float(&FirFilterBuf[k].buffer_out, sum); // dcalc.str:238
    ENDFOR
}
void forw(buffer_float_t *in, buffer_float_t *out) { // dcalc.str:245
    float L [TheGlobal_K*TheGlobal_N][TheGlobal_K*TheGlobal_N]; // dcalc.str:246
    float y [TheGlobal_K*TheGlobal_N]; // dcalc.str:247
    float sum; // dcalc.str:249
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++)  // dcalc.str:251
        y[i] = pop_float(in); // dcalc.str:252
    ENDFOR
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++)  // dcalc.str:256
        FOR(uint32_t, j, i, <, TheGlobal_K*TheGlobal_N, j++)  // dcalc.str:256
            L[j][i] = pop_float(in); // dcalc.str:257
        ENDFOR
    ENDFOR
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++)  // dcalc.str:261
        sum = y[i]; // dcalc.str:263
        FOR(uint32_t, j, 0, <, i, j++)  // dcalc.str:265
            sum -= (L[i][j] * y[j]); // dcalc.str:265
        ENDFOR
        y[i] = (sum / L[i][i]); // dcalc.str:266
        push_float(out, y[i]); // dcalc.str:267
    ENDFOR
}
void forwLrL() { // dcalc.str:245
    forw(&forwBuf.buffer_in, &forwBuf.buffer_out);
}
void forwSahrd() { // dcalc.str:245
    forw(&SAhrdforwBuf.buffer_in, &SAhrdforwBuf.buffer_out);
}

void multvect() { // dcalc.str:277
    float r [TheGlobal_M]; // dcalc.str:278
    float AH [TheGlobal_K*TheGlobal_N][TheGlobal_M]; // dcalc.str:279
    float sum; // dcalc.str:280
    FOR(uint32_t, i, 0, <, TheGlobal_M, i++)  // dcalc.str:283
        r[i] = pop_float(&multvectBuf.buffer_in); // dcalc.str:283
    ENDFOR
    FOR(uint32_t, i, 0, <, TheGlobal_M, i++)  // dcalc.str:285
        FOR(uint32_t, j, 0, <, TheGlobal_K*TheGlobal_N, j++)  // dcalc.str:286
            AH[j][i] = pop_float(&multvectBuf.buffer_in); // dcalc.str:286
        ENDFOR
    ENDFOR
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++)  // dcalc.str:288
        sum = 0.0f; // dcalc.str:289
        FOR(uint32_t, j, 0, <, TheGlobal_M, j++)  // dcalc.str:291
            sum += (AH[i][j] * r[j]); // dcalc.str:291
        ENDFOR
        push_float(&multvectBuf.buffer_out, sum); // dcalc.str:292
    ENDFOR
}
void SelfProd() { // dcalc.str:311
    float A [TheGlobal_M][TheGlobal_K*TheGlobal_N]; // dcalc.str:312
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++)  // dcalc.str:315
        FOR(uint32_t, j, 0, <, TheGlobal_M, j++)  // dcalc.str:317
            A[j][i] = pop_float(&SelfProdBuf.buffer_in); // dcalc.str:317
        ENDFOR
        FOR2(uint32_t, k, 0, <=, i, TheGlobal_K*TheGlobal_N, k++)  // dcalc.str:320
            float prod = 0; // dcalc.str:321
            FOR(uint32_t, j, 0, <, TheGlobal_M, j++)  // dcalc.str:323
                prod = (prod + (A[j][i] * A[j][k])); // dcalc.str:324
            ENDFOR
            push_float(&SelfProdBuf.buffer_out, prod); // dcalc.str:326
        ENDFOR
    ENDFOR
}
void Sourcer() { // dcalc.str:429
    FOR(uint32_t, i, 0, <, TheGlobal_M, i++)  // dcalc.str:431
        push_float(&SourcerBuf.buffer_out, TheGlobal.r[i]); // dcalc.str:431
    ENDFOR
}
void SinkD() { // dcalc.str:438
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++)  // dcalc.str:441
        printf("%f\n", pop_float(&backsBuf.buffer_out)); // dcalc.str:441
    ENDFOR
}
void error_est() { // dcalc.str:449
    float Ahr [TheGlobal_K*TheGlobal_N];
    float d [TheGlobal_K*TheGlobal_N]; // dcalc.str:450
    float sigma = 0; // dcalc.str:452
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++)  // dcalc.str:453
        Ahr[i] = pop_float(&error_estBuf.buffer_in); // dcalc.str:454
    ENDFOR
    FOR3(int, i, (TheGlobal_K*TheGlobal_N-1), >=, 0, i--)  // dcalc.str:457
        d[i] = pop_float(&error_estBuf.buffer_in); // dcalc.str:458
    ENDFOR
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++)  // dcalc.str:462
        sigma += ((d[i] - Ahr[i]) * (d[i] - Ahr[i])); // dcalc.str:462
    ENDFOR
    push_float(&error_estBuf.buffer_out, sigma); // dcalc.str:463
}
void choldsigma() { // dcalc.str:472
    float A [TheGlobal_K*TheGlobal_N][TheGlobal_K*TheGlobal_N]; // dcalc.str:473
    float p [TheGlobal_K*TheGlobal_N]; // dcalc.str:474
    float sigma; // dcalc.str:475
    float sum; // dcalc.str:476
    sigma = pop_float(&choldsigmaBuf.buffer_in); // dcalc.str:477
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++)  // dcalc.str:479
        FOR2(uint32_t, j, 0, <=, i, TheGlobal_K*TheGlobal_N, j++)  // dcalc.str:481
            A[i][j] = pop_float(&choldsigmaBuf.buffer_in); // dcalc.str:481
        ENDFOR
    ENDFOR
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++)  // dcalc.str:485
        FOR(uint32_t, j, i, <, TheGlobal_K*TheGlobal_N, j++)  // dcalc.str:486
            sum = A[j][i]; // dcalc.str:487
            FOR2(int, k, i - 1, >=, 0, TheGlobal_K*TheGlobal_N-1, k--)  // dcalc.str:488
                sum -= (A[k][i] * A[k][j]); // dcalc.str:488
            ENDFOR
            if(i == j) { // dcalc.str:490
                p[i] = ((float)(sqrt((sum + (sigma / TheGlobal_K*TheGlobal_N))))); // dcalc.str:491
                push_float(&choldsigmaBuf.buffer_out, p[i]); // dcalc.str:492
            } 
            else { // dcalc.str:495
                A[i][j] = (sum / p[i]); // dcalc.str:496
                push_float(&choldsigmaBuf.buffer_out, A[i][j]); // dcalc.str:497
            }
        ENDFOR
    ENDFOR
}

void IdentityLrL() {
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N * (TheGlobal_K*TheGlobal_N + 1) / 2, i++)
        push_float(&IdLrLBuf.buffer_out, pop_float(&IdLrLBuf.buffer_in));
    ENDFOR
}
void IdentityLrLSahrd() {
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N * (TheGlobal_K*TheGlobal_N + 1) / 2, i++)
        push_float(&IdSAhrdLrLBuf.buffer_out, pop_float(&IdSAhrdLrLBuf.buffer_in));
    ENDFOR
}
void IdentityAhrchold() {
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++)
        push_float(&IdAhrcholdBuf.buffer_out, pop_float(&IdAhrcholdBuf.buffer_in));
    ENDFOR
}
void IdentityErrorSplit() {
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N * (TheGlobal_K*TheGlobal_N + 1) / 2, i++)
        push_float(&IdErrSplitBuf.buffer_out, pop_float(&IdErrSplitBuf.buffer_in));
    ENDFOR
}
void IdentityVectDouble(uint32_t k) {
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N * (TheGlobal_K*TheGlobal_N + 1) / 2, i++)
        push_float(&vectdoubleBuf[k].buffer_out, pop_float(&vectdoubleBuf[k].buffer_in));
    ENDFOR
}
void IdentityAhrdAhA() {
    FOR(uint32_t, i, 0, <, TheGlobal_N * (TheGlobal_K*TheGlobal_N + 1) / 2, i++)
        push_float(&IdAhrdAhABuf.buffer_out, pop_float(&IdAhrdAhABuf.buffer_in));
    ENDFOR
}
void IdentitySourcer() {
    FOR(uint32_t, i, 0, <, TheGlobal_M * (TheGlobal_K*TheGlobal_N + 1) - TheGlobal_M, i++)
        push_float(&IdSourcerSplitBuf.buffer_out, pop_float(&IdSourcerSplitBuf.buffer_in));
    ENDFOR
}
void IdentityMVectDoub(uint32_t k) {
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++)
        push_float(&MVectDoubTBuf[k].buffer_out, pop_float(&MVectDoubTBuf[k].buffer_in));
    ENDFOR
}
void IdentityAhrL1VectDoub(uint32_t k) {
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N * (TheGlobal_K*TheGlobal_N + 1) / 2, i++)
        push_float(&IdAhrL1VectDoubTBuf[k].buffer_out, pop_float(&IdAhrL1VectDoubTBuf[k].buffer_in));
    ENDFOR
}
void IdentitySahrdVD(uint32_t k) {
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++)
        push_float(&SAhrdVectDoubTBuf[k].buffer_out, pop_float(&SAhrdVectDoubTBuf[k].buffer_in));
    ENDFOR
}
void IdentityAandL() {
    FOR(uint32_t, i, 0, <, TheGlobal_M * TheGlobal_K*TheGlobal_N, i++)
        push_float(&IdAandLBuf.buffer_out, pop_float(&IdAandLBuf.buffer_in));
    ENDFOR
}
void IdentityRowCol(uint32_t k) {
    FOR(uint32_t, i, 0, <, TheGlobal_M, i++)
        push_float(&IdRowCol[k].buffer_out, pop_float(&IdRowCol[k].buffer_in));
    ENDFOR
}
void IdentityDelMat(uint32_t k) {
    FOR(uint32_t, i, 0, <, TheGlobal_M, i++)
        push_float(&IdDelMatTBuf[k].buffer_out, pop_float(&IdDelMatTBuf[k].buffer_in));
    ENDFOR
}
void IdentityAddZB(uint32_t k) {
    FOR(uint32_t, i, 0, <, TheGlobal_W, i++)
        push_float(&IdCMAddZeroBegBuf[k].buffer_out, pop_float(&IdCMAddZeroBegBuf[k].buffer_in));
    ENDFOR
}
void IdentityAddZE(uint32_t k) {
    FOR(uint32_t, i, 0, <, TheGlobal_M, i++)
        push_float(&IdCMAddZeroEndBuf[k].buffer_out, pop_float(&IdCMAddZeroEndBuf[k].buffer_in));
    ENDFOR
}
void IdentityCholdAha() {
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N*(TheGlobal_K*TheGlobal_N + 1) / 2, i++)
        push_float(&IdCholdAha.buffer_out, pop_float(&IdCholdAha.buffer_in));
    ENDFOR
}
void dcalc_init() { // dcalc.str:536
    TheGlobal.h[0][0] = 1; // dcalc.str:554
    TheGlobal.h[0][1] = 3; // dcalc.str:555
    TheGlobal.h[1][0] = 2; // dcalc.str:556
    TheGlobal.h[1][1] = 5; // dcalc.str:557
    TheGlobal.COEFF[0][0] = 1; // dcalc.str:558
    TheGlobal.COEFF[0][1] = 1; // dcalc.str:559 //access index swapped
    TheGlobal.COEFF[1][0] = 0; // dcalc.str:560
    TheGlobal.COEFF[1][1] = 2; // dcalc.str:561
    TheGlobal.r[0] = 1; // dcalc.str:562
    TheGlobal.r[1] = 2; // dcalc.str:563
    TheGlobal.r[2] = 3; // dcalc.str:564
    TheGlobal.r[3] = 4; // dcalc.str:565
    TheGlobal.r[4] = 5; // dcalc.str:566
    TheGlobal.r[5] = 6; // dcalc.str:567
    
    init_buffer_float(&SourcehBuf.buffer_out);
    init_buffer_float(&choldBuf.buffer_in);
    init_buffer_float(&choldBuf.buffer_out);
    FOR(uint32_t, i, 0, <, TheGlobal_K, i++)
        init_buffer_float(&ZeroGenBegBuf[i].buffer_in);
        init_buffer_float(&ZeroGenBegBuf[i].buffer_out);
        init_buffer_float(&ZeroGenEndBuf[i].buffer_in);
        init_buffer_float(&ZeroGenEndBuf[i].buffer_out);
        init_buffer_float(&FirFilterBuf[i].buffer_in);
        init_buffer_float(&FirFilterBuf[i].buffer_out);
        init_buffer_float(&ConvMatBuf[i].buffer_in);
        init_buffer_float(&ConvMatBuf[i].buffer_out);
        init_buffer_float(&IdCMAddZeroBegBuf[i].buffer_in);
        init_buffer_float(&IdCMAddZeroBegBuf[i].buffer_out);
        init_buffer_float(&IdCMAddZeroEndBuf[i].buffer_in);
        init_buffer_float(&IdCMAddZeroEndBuf[i].buffer_out);
        init_buffer_float(&SplitMatTBuf[i].buffer_in);
        init_buffer_float(&SplitMatTBuf[i].buffer_out);
        init_buffer_float(&IdDelMatTBuf[i].buffer_in);
        init_buffer_float(&IdDelMatTBuf[i].buffer_out);
        FOR(uint32_t, j, 0, <, TheGlobal_N, j++)
            init_buffer_float(&DelayBuf[i][j].buffer_in);
            init_buffer_float(&DelayBuf[i][j].buffer_out);
        ENDFOR
    ENDFOR
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++)
        init_buffer_float(&IdRowCol[i].buffer_in);
        init_buffer_float(&IdRowCol[i].buffer_out);
    ENDFOR
    init_buffer_float(&backsBuf.buffer_in);
    init_buffer_float(&backsBuf.buffer_out);
    init_buffer_float(&forwBuf.buffer_in);
    init_buffer_float(&forwBuf.buffer_out);
    init_buffer_float(&multvectBuf.buffer_in);
    init_buffer_float(&multvectBuf.buffer_out);
    init_buffer_float(&SelfProdBuf.buffer_in);
    init_buffer_float(&SelfProdBuf.buffer_out);
    init_buffer_float(&SourcerBuf.buffer_out);
    init_buffer_float(&error_estBuf.buffer_in);
    init_buffer_float(&error_estBuf.buffer_out);
    init_buffer_float(&choldsigmaBuf.buffer_in);
    init_buffer_float(&choldsigmaBuf.buffer_out);
    
    init_buffer_float(&IdLrLBuf.buffer_in);
    init_buffer_float(&IdLrLBuf.buffer_out);    
    init_buffer_float(&IdAhrcholdBuf.buffer_in);
    init_buffer_float(&IdAhrcholdBuf.buffer_out);
    init_buffer_float(&IdAhrdAhABuf.buffer_in);
    init_buffer_float(&IdAhrdAhABuf.buffer_out);
    init_buffer_float(&LestBuf.buffer_in);
    init_buffer_float(&LestBuf.buffer_out);
    init_buffer_float(&LrLBuf.buffer_in);
    init_buffer_float(&AhrdAhABuf.buffer_in);
    FOR(uint32_t, i, 0, <, 2, i++)
        init_buffer_float(&vectdoubleBuf[i].buffer_in);
        init_buffer_float(&vectdoubleBuf[i].buffer_out);
        init_buffer_float(&IdAhrL1VectDoubTBuf[i].buffer_in);
        init_buffer_float(&IdAhrL1VectDoubTBuf[i].buffer_out);
    ENDFOR
    init_buffer_float(&AhrL1Buf.buffer_in);
    init_buffer_float(&AhrL1Buf.buffer_out);
    init_buffer_float(&SplitMatBuf.buffer_in);
    init_buffer_float(&SplitMatBuf.buffer_out);
    init_buffer_float(&AandL.buffer_in);
    init_buffer_float(&AandL.buffer_out);
    init_buffer_float(&IdAandLBuf.buffer_in);
    init_buffer_float(&IdAandLBuf.buffer_out);
    init_buffer_float(&RowcolBuf.buffer_in);
    init_buffer_float(&RowcolBuf.buffer_out);
    init_buffer_float(&choldBuf.buffer_in);
    init_buffer_float(&choldBuf.buffer_out);
    init_buffer_float(&IdCholdAha.buffer_in);
    init_buffer_float(&IdCholdAha.buffer_out);
    init_buffer_float(&SourceSplitBuf.buffer_in);
    init_buffer_float(&SourceSplitBuf.buffer_out);
    init_buffer_float(&AhrL1VectDoubBuf.buffer_in);
    init_buffer_float(&AhrL1VectDoubBuf.buffer_out);
    init_buffer_float(&IdSourcerSplitBuf.buffer_in);
    init_buffer_float(&IdSourcerSplitBuf.buffer_out);
    
    
    // Delay_prework dcalc.str:218
    FOR(uint32_t, k, 0, <, TheGlobal_K, k++) // K times Delmat
        FOR(uint32_t, i, 1, <=, TheGlobal_N-1, i++)  // dcalc.str:105 N times Delay
            FOR2(uint32_t, j, 0, <, i*TheGlobal_Q, (TheGlobal_N-1)*TheGlobal_Q, j++) // dcalc.str:219
                push_float(&DelayBuf[k][i-1].buffer_out, 0); // dcalc.str:220
            ENDFOR
        ENDFOR
    ENDFOR

}

void Split5ROUND_ROBIN() {
    FOR(uint32_t, i, 0, <, TheGlobal_K, i++)
        FOR(uint32_t, j, 0, <, TheGlobal_W, j++)
            push_float(&ConvMatBuf[i].buffer_in, pop_float(&SourcehBuf.buffer_out));
        ENDFOR
    ENDFOR
}

void Join5ROUND_ROBIN() {
    FOR(uint32_t, i, 0, <, TheGlobal_K, i++)
        FOR(uint32_t, j, 0, <, TheGlobal_M, j++)
            push_float(&SplitMatBuf.buffer_in, pop_float(&ConvMatBuf[i].buffer_out));
        ENDFOR
    ENDFOR
}

void Split6ROUND_ROBIN() {
    FOR(uint32_t, i, 0, <, TheGlobal_K, i++)
        FOR(uint32_t, j, 0, <, TheGlobal_M, j++)
            push_float(&SplitMatTBuf[i].buffer_in, pop_float(&SplitMatBuf.buffer_in));
        ENDFOR
    ENDFOR
}

void Join6ROUND_ROBIN() {
    FOR(uint32_t, i, 0, <, TheGlobal_K, i++)
        FOR(uint32_t, j, 0, <, TheGlobal_N, j++)
            push_float(&AandL.buffer_in, pop_float(&SplitMatTBuf[i].buffer_out));
        ENDFOR
    ENDFOR
}

void Split2DUPLICATE() {
    FOR(uint32_t, i, 0, <, TheGlobal_M * TheGlobal_K*TheGlobal_N, i++)
        float val = pop_float(&AandL.buffer_in);
        push_float(&IdAandLBuf.buffer_in, val);
        push_float(&RowcolBuf.buffer_in, val);
    ENDFOR
}

void _80211_r6_join2_weighted_round_robin() {
    FOR(uint32_t, i, 0, <, TheGlobal_M * TheGlobal_K*TheGlobal_N, i++)
        push_float(&AhrdAhABuf.buffer_in, pop_float(&IdAandLBuf.buffer_out));
    ENDFOR
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N * (TheGlobal_K*TheGlobal_N + 1), i++)
        push_float(&AhrdAhABuf.buffer_in, pop_float(&RowcolBuf.buffer_out));
    ENDFOR
}

void Split14WEIGHTED_ROUND_ROBIN() {
    FOR(uint32_t, i, 0, <, TheGlobal_M * (TheGlobal_K*TheGlobal_N + 1) + TheGlobal_K*TheGlobal_N * (TheGlobal_K*TheGlobal_N + 1) / 2 - TheGlobal_M, i++)
        push_float(&AhrL1Buf.buffer_in, pop_float(&AhrdAhABuf.buffer_in));
    ENDFOR
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N * (TheGlobal_K*TheGlobal_N + 1) / 2, i++)
        push_float(&IdAhrdAhABuf.buffer_in, pop_float(&AhrdAhABuf.buffer_in));
    ENDFOR
}

void _80211_r6_join14_weighted_round_robin() {
    FOR(uint32_t, i, 0, <, 3 * TheGlobal_K*TheGlobal_N, i++)
        push_float(&AhrcholdBuf.buffer_in, pop_float(&AhrL1Buf.buffer_out));
    ENDFOR
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N * (TheGlobal_K*TheGlobal_N + 1) / 2, i++)
        push_float(&AhrcholdBuf.buffer_in, pop_float(&IdAhrdAhABuf.buffer_out));
    ENDFOR
}

void Split17WEIGHTED_ROUND_ROBIN() {
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++)
        push_float(&IdAhrcholdBuf.buffer_in, pop_float(&AhrcholdBuf.buffer_in));
    ENDFOR
    FOR(uint32_t, i, 0, <, 2 * TheGlobal_K*TheGlobal_N + TheGlobal_K*TheGlobal_N * (TheGlobal_K*TheGlobal_N + 1) / 2, i++)
        push_float(&LestBuf.buffer_in, pop_float(&AhrcholdBuf.buffer_in));
    ENDFOR
}

void Join17WEIGHTED_ROUND_ROBIN() {
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++)
        push_float(&LrLBuf.buffer_in, pop_float(&IdAhrcholdBuf.buffer_out));
    ENDFOR
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N * (TheGlobal_K*TheGlobal_N + 1), i++)
        push_float(&LrLBuf.buffer_in, pop_float(&LestBuf.buffer_out)); 
    ENDFOR
}

void Split18WEIGHTED_ROUND_ROBIN() {
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N + TheGlobal_K*TheGlobal_N * (TheGlobal_K*TheGlobal_N + 1) / 2, i++)
        push_float(&forwBuf.buffer_in, pop_float(&LrLBuf.buffer_in));
    ENDFOR
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N * (TheGlobal_K*TheGlobal_N + 1) / 2, i++)
        push_float(&IdLrLBuf.buffer_in, pop_float(&LrLBuf.buffer_in));
    ENDFOR
}

void Join18WEIGHTED_ROUND_ROBIN() {
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++)
        push_float(&backsBuf.buffer_in, pop_float(&forwBuf.buffer_out));
    ENDFOR
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N * (TheGlobal_K*TheGlobal_N + 1) / 2, i++)
        push_float(&backsBuf.buffer_in, pop_float(&IdLrLBuf.buffer_out));
    ENDFOR
}

void _80211_r6_split16_weighted_round_robin() {
    FOR(uint32_t, i, 0, <, 2 * TheGlobal_K*TheGlobal_N, i++)
        push_float(&error_estBuf.buffer_in, pop_float(&LestBuf.buffer_in));
    ENDFOR
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N * (TheGlobal_K*TheGlobal_N + 1) / 2, i++)
        push_float(&IdErrSplitBuf.buffer_in, pop_float(&LestBuf.buffer_in));
    ENDFOR
}

void _80211_r6_join16_weighted_round_robin() {
    push_float(&choldsigmaBuf.buffer_in, pop_float(&error_estBuf.buffer_out));
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N * (TheGlobal_K*TheGlobal_N + 1) / 2, i++)
        push_float(&choldsigmaBuf.buffer_in, pop_float(&IdErrSplitBuf.buffer_out));
    ENDFOR
}

void Split10DUPLICATE() {
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N * (TheGlobal_K*TheGlobal_N + 1) / 2, i++)
        float val = pop_float(&choldsigmaBuf.buffer_out);
        push_float(&vectdoubleBuf[0].buffer_in, val);
        push_float(&vectdoubleBuf[1].buffer_in, val);
    ENDFOR
}

void Join10ROUND_ROBIN() {
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N * (TheGlobal_K*TheGlobal_N + 1) / 2, i++)
        push_float(&LestBuf.buffer_out, pop_float(&vectdoubleBuf[0].buffer_out));
    ENDFOR
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N * (TheGlobal_K*TheGlobal_N + 1) / 2, i++)
        push_float(&LestBuf.buffer_out, pop_float(&vectdoubleBuf[1].buffer_out));
    ENDFOR
}

void Split12WEIGHTED_ROUND_ROBIN() {
    FOR(uint32_t, i, 0, <, TheGlobal_M * (TheGlobal_K*TheGlobal_N + 1) - TheGlobal_M, i++)
        push_float(&SourceSplitBuf.buffer_in, pop_float(&AhrL1Buf.buffer_in));
    ENDFOR
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N * (TheGlobal_K*TheGlobal_N + 1) / 2, i++)
        push_float(&AhrL1VectDoubBuf.buffer_in, pop_float(&AhrL1Buf.buffer_in));
    ENDFOR
}

void Join12WEIGHTED_ROUND_ROBIN() {
    FOR(uint32_t, i, 0, <, 2 * TheGlobal_K*TheGlobal_N, i++) 
        push_float(&split_ahrdBuf.buffer_in, pop_float(&MVectDoubBuf.buffer_out));
    ENDFOR
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N*(TheGlobal_K*TheGlobal_N+1), i++)
        push_float(&split_ahrdBuf.buffer_in, pop_float(&AhrL1VectDoubBuf.buffer_out));
    ENDFOR
}

void Split13WEIGHTED_ROUND_ROBIN() {
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++) 
        push_float(&SAhrdBuf.buffer_in, pop_float(&split_ahrdBuf.buffer_in));
    ENDFOR
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N*(TheGlobal_K*TheGlobal_N+1) + TheGlobal_K*TheGlobal_N, i++)
        push_float(&Dsolve.buffer_in, pop_float(&split_ahrdBuf.buffer_in));
    ENDFOR
}

void Join13WEIGHTED_ROUND_ROBIN() {
    FOR(uint32_t, i, 0, <, 2*TheGlobal_K*TheGlobal_N, i++)
        push_float(&AhrL1Buf.buffer_out, pop_float(&SAhrdBuf.buffer_out));
    ENDFOR
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++)
        push_float(&AhrL1Buf.buffer_out, pop_float(&Dsolve.buffer_out));
    ENDFOR
}

void _80211_r6_split11_weighted_round_robin() {
    FOR(uint32_t, i, 0, <, TheGlobal_M * (TheGlobal_K*TheGlobal_N + 1) - TheGlobal_M, i++)
        push_float(&IdSourcerSplitBuf.buffer_in, pop_float(&SourceSplitBuf.buffer_in));
    ENDFOR
}
void _80211_r6_join11_weighted_round_robin() {
    FOR(uint32_t, i, 0, <, TheGlobal_M, i++)
        push_float(&multvectBuf.buffer_in, pop_float(&SourcerBuf.buffer_out));
    ENDFOR   
    FOR(uint32_t, i, 0, <, TheGlobal_M*(TheGlobal_K*TheGlobal_N+1) - TheGlobal_M, i++)
        push_float(&multvectBuf.buffer_in, pop_float(&IdSourcerSplitBuf.buffer_out));
    ENDFOR
}

void Split19DUPLICATE() {
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++)
        float val = pop_float(&multvectBuf.buffer_out);
        push_float(&MVectDoubTBuf[0].buffer_in, val);
        push_float(&MVectDoubTBuf[1].buffer_in, val);
    ENDFOR
}
void Join19ROUND_ROBIN() {
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++)
        push_float(&MVectDoubBuf.buffer_out, pop_float(&MVectDoubTBuf[0].buffer_out));
    ENDFOR
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++)
        push_float(&MVectDoubBuf.buffer_out, pop_float(&MVectDoubTBuf[1].buffer_out));
    ENDFOR
}

void Split20DUPLICATE() {
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N * (TheGlobal_K*TheGlobal_N + 1) / 2, i++)
        float val = pop_float(&AhrL1VectDoubBuf.buffer_in);
        push_float(&IdAhrL1VectDoubTBuf[0].buffer_in, val);
        push_float(&IdAhrL1VectDoubTBuf[1].buffer_in, val);
    ENDFOR
}
void Join20ROUND_ROBIN() {
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N * (TheGlobal_K*TheGlobal_N + 1) / 2, i++)
        push_float(&AhrL1VectDoubBuf.buffer_out, pop_float(&IdAhrL1VectDoubTBuf[0].buffer_out));
    ENDFOR
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N * (TheGlobal_K*TheGlobal_N + 1) / 2, i++)
        push_float(&AhrL1VectDoubBuf.buffer_out, pop_float(&IdAhrL1VectDoubTBuf[1].buffer_out));
    ENDFOR
}

void Split21DUPLICATE() {
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++)
        float val = pop_float(&SAhrdBuf.buffer_in);
        push_float(&SAhrdVectDoubTBuf[0].buffer_in, val);
        push_float(&SAhrdVectDoubTBuf[1].buffer_in, val);
    ENDFOR
}
void Join21ROUND_ROBIN() {
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++)
        push_float(&SAhrdBuf.buffer_out, pop_float(&SAhrdVectDoubTBuf[0].buffer_out));
    ENDFOR
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++)
        push_float(&SAhrdBuf.buffer_out, pop_float(&SAhrdVectDoubTBuf[1].buffer_out));
    ENDFOR
}

void Split1WEIGHTED_ROUND_ROBIN() {
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N + TheGlobal_K*TheGlobal_N * (TheGlobal_K*TheGlobal_N + 1) / 2, i++)
        push_float(&SAhrdforwBuf.buffer_in, pop_float(&Dsolve.buffer_in));
    ENDFOR
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N * (TheGlobal_K*TheGlobal_N + 1) / 2, i++)
        push_float(&IdSAhrdLrLBuf.buffer_in, pop_float(&Dsolve.buffer_in));
    ENDFOR
}
void _80211_r6_join1_weighted_round_robin() {
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++)
        push_float(&SAhrdbacksBuf.buffer_in, pop_float(&SAhrdforwBuf.buffer_out));
    ENDFOR
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N * (TheGlobal_K*TheGlobal_N + 1) / 2, i++)
        push_float(&SAhrdbacksBuf.buffer_in, pop_float(&IdSAhrdLrLBuf.buffer_out));
    ENDFOR
}

void Split9ROUND_ROBIN() {
    FOR(uint32_t, j, 0, <, TheGlobal_M*TheGlobal_K*TheGlobal_N, j++) 
        FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++)
            push_float(&IdRowCol[i].buffer_in, pop_float(&RowcolBuf.buffer_in));
        ENDFOR
    ENDFOR
}
void Join9ROUND_ROBIN() {
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++)
        FOR(uint32_t, j, 0, <, TheGlobal_M, j++) 
            push_float(&SelfProdBuf.buffer_in, pop_float(&IdRowCol[i].buffer_out));
        ENDFOR
    ENDFOR
}

void Split3DUPLICATE() {
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N*(TheGlobal_K*TheGlobal_N + 1) / 2, i++)
        float val = pop_float(&SelfProdBuf.buffer_out);
        push_float(&choldBuf.buffer_in, val);
        push_float(&IdCholdAha.buffer_in, val);
    ENDFOR
}
void Join3WEIGHTED_ROUND_ROBIN() {
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N*(TheGlobal_K*TheGlobal_N + 1) / 2, i++)
        push_float(&RowcolBuf.buffer_out, pop_float(&choldBuf.buffer_out));
    ENDFOR
    FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N*(TheGlobal_K*TheGlobal_N + 1) / 2, i++)
        push_float(&RowcolBuf.buffer_out, pop_float(&IdCholdAha.buffer_out));
    ENDFOR
}

void Split4DUPLICATE(uint32_t k) {
    FOR(uint32_t, i, 0, <, TheGlobal_M, i++)
        float val = pop_float(&SplitMatTBuf[k].buffer_in);
        push_float(&IdDelMatTBuf[k].buffer_in, val);
        FOR(uint32_t, j, 1, <, TheGlobal_N, j++) 
            push_float(&DelayBuf[k][j].buffer_in, val);
        ENDFOR
    ENDFOR
}
void Join4ROUND_ROBIN(uint32_t k) {
    FOR(uint32_t, i, 0, <, TheGlobal_M, i++)
        push_float(&SplitMatTBuf[k].buffer_out, pop_float(&IdDelMatTBuf[k].buffer_out));
        FOR(uint32_t, j, 1, <, TheGlobal_N, j++) 
            push_float(&SplitMatTBuf[k].buffer_out, pop_float(&DelayBuf[k][j].buffer_out));
        ENDFOR
    ENDFOR
}
void Split8WEIGHTED_ROUND_ROBIN(uint32_t k) {
    FOR(uint32_t, i, 0, <, TheGlobal_W, i++)
        push_float(&IdCMAddZeroBegBuf[k].buffer_in, pop_float(&ConvMatBuf[k].buffer_in));
    ENDFOR
}
void Join8WEIGHTED_ROUND_ROBIN(uint32_t k) {
    FOR(uint32_t, i, 0, <, TheGlobal_Q-1, i++)
        push_float(&FirFilterBuf[k].buffer_in, pop_float(&ZeroGenBegBuf[k].buffer_out));
    ENDFOR
    FOR(uint32_t, i, 0, <, TheGlobal_W, i++)
        push_float(&FirFilterBuf[k].buffer_in, pop_float(&IdCMAddZeroBegBuf[k].buffer_out));
    ENDFOR
}

void Split7WEIGHTED_ROUND_ROBIN(uint32_t k) {
    FOR(uint32_t, i, 0, <, TheGlobal_W, i++)
        push_float(&IdCMAddZeroEndBuf[k].buffer_in, pop_float(&FirFilterBuf[k].buffer_out));
    ENDFOR
}
void Join7WEIGHTED_ROUND_ROBIN(uint32_t k) {
    FOR(uint32_t, i, 0, <, TheGlobal_W+TheGlobal_Q-1, i++)
        push_float(&ConvMatBuf[k].buffer_out, pop_float(&IdCMAddZeroEndBuf[k].buffer_out));
    ENDFOR
    FOR(uint32_t, i, 0, <, TheGlobal_M, i++)
        push_float(&ConvMatBuf[k].buffer_out, pop_float(&ZeroGenEndBuf[k].buffer_out));
    ENDFOR
}
int main(int argv, char** argc) {
    dcalc_init();
    
    Sourceh(2);
    Split5ROUND_ROBIN(2);
    FOR(uint32_t, i, 0, <, TheGlobal_K, i++) 
        Split8WEIGHTED_ROUND_ROBIN(2, i);
            ZeroGenBeg(2, i);
            IdentityAddZB(2, i);
        Join8WEIGHTED_ROUND_ROBIN(2, i);
        FirFilter(i, 5);
        Split7WEIGHTED_ROUND_ROBIN(i);
            IdentityAddZE(3, i);
            ZeroGenEnd(2, i);
        Join7WEIGHTED_ROUND_ROBIN(i);
    ENDFOR
    Join5ROUND_ROBIN();
    for(int x=0 ; x < TheGlobal_K*TheGlobal_M; x++)
        printf("---> %f\n", peek_float(&SplitMatBuf.buffer_in, x));     
    
    FOR(uint32_t, iter,0, <, MAX_ITERATION, iter++) 
        //dcalc();
        Sourceh(24);
        //ConvMat();
        Split5ROUND_ROBIN(24); // dcalc.str:119
            FOR(uint32_t, i, 0, <, TheGlobal_K, i++) 
                //AddZeroBeg();
                Split8WEIGHTED_ROUND_ROBIN(i); // dcalc.str:154
                    ZeroGenBeg(i);
                    IdentityAddZB(i);
                Join8WEIGHTED_ROUND_ROBIN(i); // dcalc.str:157
                FirFilter(i, TheGlobal_Q-1+TheGlobal_W);
                //AddZeroEnd();
                Split7WEIGHTED_ROUND_ROBIN(i); // dcalc.str:144
                    IdentityAddZE(i);
                    ZeroGenEnd(i);
                Join7WEIGHTED_ROUND_ROBIN(i); // dcalc.str:147
            ENDFOR
        Join5ROUND_ROBIN(); // dcalc.str:126
            for(int x=0 ; x < TheGlobal_K*TheGlobal_M; x++)
        printf("---> %f\n", peek_float(&SplitMatBuf.buffer_in, x));     
        
        //SplitMat();
/*        Split6ROUND_ROBIN(); // dcalc.str:133
            FOR(uint32_t, i, 0, <, TheGlobal_K, i++) 
                //DelMat();
                Split4DUPLICATE(i);
                    IdentityDelMat(i);
                    FOR(uint32_t, j, 1, <=, TheGlobal_N-1, j++) 
                        Delay(i,j-1);
                    ENDFOR
                Join4ROUND_ROBIN(i); // dcalc.str:111
            ENDFOR
        Join6ROUND_ROBIN(); // dcalc.str:137
            
        //AandL();
        Split2DUPLICATE();
            IdentityAandL();
            //RowCol();
            Split9ROUND_ROBIN(); // dcalc.str:300
            FOR(uint32_t, i, 0, <, TheGlobal_K*TheGlobal_N, i++) 
                IdentityRowCol(i);
            ENDFOR
            Join9ROUND_ROBIN(); // dcalc.str:303
            SelfProd();
            //choldAha();
            Split3DUPLICATE();
                chold();
                IdentityCholdAha();
            Join3WEIGHTED_ROUND_ROBIN(); // dcalc.str:51
        Join2WEIGHTED_ROUND_ROBIN(); // dcalc.str:32
        
    	//AhrdAhA();
        Split14WEIGHTED_ROUND_ROBIN(); // dcalc.str:399
            //AhrL1();
            Split12WEIGHTED_ROUND_ROBIN(); // dcalc.str:364
                //multvectdoub();
                    //sourcerSplit();
                    Split11WEIGHTED_ROUND_ROBIN(); // dcalc.str:345
                        Sourcer();
                        IdentitySourcer();
                    Join11WEIGHTED_ROUND_ROBIN(); // dcalc.str:348
                    //vectdouble();
                    Split19DUPLICATE();
                        IdentityMVectDoub(0);
                        IdentityMVectDoub(1);
                    Join19ROUND_ROBIN(); // dcalc.str:339
                //vectdouble();
                Split20DUPLICATE();
                    IdentityAhrL1VectDoub(0);
                    IdentityAhrL1VectDoub(1);
                Join20ROUND_ROBIN();
            Join12WEIGHTED_ROUND_ROBIN(); // dcalc.str:367
            //split_ahrd();
            Split13WEIGHTED_ROUND_ROBIN(); // dcalc.str:381
                //vectdouble();
                Split21DUPLICATE();
                    IdentitySahrdVD(0);
                    IdentitySahrdVD(1);
                Join21ROUND_ROBIN();
                //LrL();
                Split1WEIGHTED_ROUND_ROBIN(); // dcalc.str:4
                        forwSahrd();
                        IdentityLrLSahrd();
                Join1WEIGHTED_ROUND_ROBIN(); // dcalc.str:7
                backsSahrd();
            Join13WEIGHTED_ROUND_ROBIN(); // dcalc.str:384
            IdentityAhrdAhA();
        Join14WEIGHTED_ROUND_ROBIN(); // dcalc.str:402
        
    	//Ahrchold();
        Split17WEIGHTED_ROUND_ROBIN(); // dcalc.str:527
            IdentityAhrchold();
            //error_split();
            Split16WEIGHTED_ROUND_ROBIN(); // dcalc.str:507
                error_est();
                IdentityErrorSplit();
            Join16WEIGHTED_ROUND_ROBIN(); // dcalc.str:510
            choldsigma();
            //vectdouble();
            Split10DUPLICATE();
                IdentityVectDouble(0);
                IdentityVectDouble(1);
            Join10ROUND_ROBIN();
        Join17WEIGHTED_ROUND_ROBIN(); // dcalc.str:530
        
    	//LrL();
        Split18WEIGHTED_ROUND_ROBIN();
            forwLrL();
            IdentityLrL();
        Join18WEIGHTED_ROUND_ROBIN();
    	backsLrL();
 */    	SinkD();

    ENDFOR
    return EXIT_SUCCESS;
} 
