#ifndef DCALC_H
#define DCALC_H

#ifdef __cplusplus
extern "C" {
#endif

#include "globals.h"
    
#define TheGlobal_K 2 // dcalc.str:549
#define TheGlobal_N 2 // dcalc.str:550
#define TheGlobal_Q 2 // dcalc.str:551
#define TheGlobal_W 2 // dcalc.str:552
#define TheGlobal_M (TheGlobal_Q * TheGlobal_N + TheGlobal_W - 1)
#define TheGlobal_r_size 6

typedef struct {
    float r [TheGlobal_r_size];	// dcalc.str:418
    float d [TheGlobal_W][TheGlobal_K];	// dcalc.str:55
    float COEFF[TheGlobal_K][TheGlobal_Q];	// dcalc.str:230
    float h [2][8]; // dcalc.str:546
} TheGlobal_t;
    

typedef struct {  // dcalc.str:470
    buffer_float_t buffer_in;
    buffer_float_t buffer_out;
} channel_t;

void Sourceh();
void chold();
void ZeroGen();
void backs();
void Delay_prework();
void Delay();
void FirFilter();
void forw();
void multvect();
void SelfProd();
void Sourcer();
void SinkD();
void error_est();
void choldsigma();


#ifdef __cplusplus
}
#endif

#endif /* DCALC_H */

