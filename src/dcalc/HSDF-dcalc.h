#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=100 on the compile command line
#else
#if BUF_SIZEMAX < 100
#error BUF_SIZEMAX too small, it must be at least 100
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float d[2][2];
} Sourceh_1006_t;

typedef struct {
	float COEFF[2];
} FirFilter_1239_t;

typedef struct {
	float r[6];
} Sourcer_1044_t;
void Sourceh(buffer_float_t *chanout);
void Sourceh_1006();
void WEIGHTED_ROUND_ROBIN_Splitter_1135();
void WEIGHTED_ROUND_ROBIN_Splitter_1137();
void ZeroGen(buffer_float_t *chanin, buffer_float_t *chanout);
void ZeroGen_1010();
void Identity(buffer_float_t *chanin, buffer_float_t *chanout);
void Identity_1011();
void WEIGHTED_ROUND_ROBIN_Joiner_1138();
void DUPLICATE_Splitter_1237();
void FirFilter(buffer_float_t *chanin, buffer_float_t *chanout);
void FirFilter_1239();
void FirFilter_1240();
void FirFilter_1241();
void WEIGHTED_ROUND_ROBIN_Joiner_1238();
void WEIGHTED_ROUND_ROBIN_Splitter_1139();
void Identity_1014();
void WEIGHTED_ROUND_ROBIN_Splitter_1242();
void ZeroGen_1244();
void ZeroGen_1245();
void WEIGHTED_ROUND_ROBIN_Joiner_1243();
void WEIGHTED_ROUND_ROBIN_Joiner_1140();
void WEIGHTED_ROUND_ROBIN_Splitter_1141();
void ZeroGen_1018();
void Identity_1019();
void WEIGHTED_ROUND_ROBIN_Joiner_1142();
void DUPLICATE_Splitter_1246();
void FirFilter_1248();
void FirFilter_1249();
void FirFilter_1250();
void WEIGHTED_ROUND_ROBIN_Joiner_1247();
void WEIGHTED_ROUND_ROBIN_Splitter_1143();
void Identity_1022();
void WEIGHTED_ROUND_ROBIN_Splitter_1251();
void ZeroGen_1253();
void ZeroGen_1254();
void WEIGHTED_ROUND_ROBIN_Joiner_1252();
void WEIGHTED_ROUND_ROBIN_Joiner_1144();
void WEIGHTED_ROUND_ROBIN_Joiner_1136();
void WEIGHTED_ROUND_ROBIN_Splitter_1145();
void DUPLICATE_Splitter_1147();
void Identity_1024();
void WEIGHTED_ROUND_ROBIN_Splitter_1255();
void Delay(buffer_float_t *chanin, buffer_float_t *chanout);
void Delay_1257();
void Delay_1258();
void Delay_1259();
void Delay_1260();
void Delay_1261();
void WEIGHTED_ROUND_ROBIN_Joiner_1256();
void WEIGHTED_ROUND_ROBIN_Joiner_1148();
void DUPLICATE_Splitter_1149();
void Identity_1026();
void WEIGHTED_ROUND_ROBIN_Splitter_1262();
void Delay_1264();
void Delay_1265();
void Delay_1266();
void Delay_1267();
void Delay_1268();
void WEIGHTED_ROUND_ROBIN_Joiner_1263();
void WEIGHTED_ROUND_ROBIN_Joiner_1150();
void WEIGHTED_ROUND_ROBIN_Joiner_1146();
void DUPLICATE_Splitter_1151();
void Identity_1029();
void Identity_1031();
void Pre_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_2_1128();
void SelfProd(buffer_float_t *chanin, buffer_float_t *chanout);
void SelfProd_1035();
void DUPLICATE_Splitter_1153();
void chold(buffer_float_t *chanin, buffer_float_t *chanout);
void chold_1037();
void Identity_1038();
void WEIGHTED_ROUND_ROBIN_Joiner_1154();
void WEIGHTED_ROUND_ROBIN_Joiner_1152();
void WEIGHTED_ROUND_ROBIN_Splitter_1155();
void WEIGHTED_ROUND_ROBIN_Splitter_1157();
void WEIGHTED_ROUND_ROBIN_Splitter_1159();
void Sourcer(buffer_float_t *chanin, buffer_float_t *chanout);
void Sourcer_1044();
void Identity_1045();
void WEIGHTED_ROUND_ROBIN_Joiner_1160();
void multvect(buffer_float_t *chanin, buffer_float_t *chanout);
void multvect_1046();
void WEIGHTED_ROUND_ROBIN_Splitter_1269();
void AutoGeneratedExpander(buffer_float_t *chanin, buffer_float_t *chanout);
void AutoGeneratedExpander_1271();
void AutoGeneratedExpander_1272();
void AutoGeneratedExpander_1273();
void AutoGeneratedExpander_1274();
void WEIGHTED_ROUND_ROBIN_Joiner_1270();
void Identity_1047();
void Pre_CollapsedDataParallel_2_1129();
void WEIGHTED_ROUND_ROBIN_Splitter_1275();
void AutoGeneratedExpander_1277();
void AutoGeneratedExpander_1278();
void AutoGeneratedExpander_1279();
void AutoGeneratedExpander_1280();
void AutoGeneratedExpander_1281();
void AutoGeneratedExpander_1282();
void AutoGeneratedExpander_1283();
void AutoGeneratedExpander_1284();
void AutoGeneratedExpander_1285();
void AutoGeneratedExpander_1286();
void WEIGHTED_ROUND_ROBIN_Joiner_1276();
void Identity_1049();
void Pre_CollapsedDataParallel_2_1131();
void WEIGHTED_ROUND_ROBIN_Joiner_1158();
void WEIGHTED_ROUND_ROBIN_Splitter_1161();
void WEIGHTED_ROUND_ROBIN_Splitter_1287();
void AutoGeneratedExpander_1289();
void AutoGeneratedExpander_1290();
void AutoGeneratedExpander_1291();
void AutoGeneratedExpander_1292();
void WEIGHTED_ROUND_ROBIN_Joiner_1288();
void Identity_1052();
void Pre_CollapsedDataParallel_2_1133();
void WEIGHTED_ROUND_ROBIN_Splitter_1163();
void forw(buffer_float_t *chanin, buffer_float_t *chanout);
void forw_1056();
void Identity_1057();
void WEIGHTED_ROUND_ROBIN_Joiner_1164();
void backs(buffer_float_t *chanin, buffer_float_t *chanout);
void backs_1058();
void WEIGHTED_ROUND_ROBIN_Joiner_1162();
void Identity_1059();
void WEIGHTED_ROUND_ROBIN_Joiner_1156();
void WEIGHTED_ROUND_ROBIN_Splitter_1165();
void Identity_1061();
void WEIGHTED_ROUND_ROBIN_Splitter_1167();
void error_est(buffer_float_t *chanin, buffer_float_t *chanout);
void error_est_1064();
void Identity_1065();
void WEIGHTED_ROUND_ROBIN_Joiner_1168();
void choldsigma(buffer_float_t *chanin, buffer_float_t *chanout);
void choldsigma_1066();
void WEIGHTED_ROUND_ROBIN_Splitter_1293();
void AutoGeneratedExpander_1295();
void AutoGeneratedExpander_1296();
void AutoGeneratedExpander_1297();
void AutoGeneratedExpander_1298();
void AutoGeneratedExpander_1299();
void AutoGeneratedExpander_1300();
void AutoGeneratedExpander_1301();
void AutoGeneratedExpander_1302();
void AutoGeneratedExpander_1303();
void AutoGeneratedExpander_1304();
void WEIGHTED_ROUND_ROBIN_Joiner_1294();
void Identity_1067();
void Pre_CollapsedDataParallel_2_1134();
void WEIGHTED_ROUND_ROBIN_Joiner_1166();
void WEIGHTED_ROUND_ROBIN_Splitter_1169();
void forw_1070();
void Identity_1071();
void WEIGHTED_ROUND_ROBIN_Joiner_1170();
void backs_1072();
void SinkD(buffer_float_t *chanin);
void SinkD_1073();

#ifdef __cplusplus
}
#endif
#endif
