#include "SDF-FIRcoarse_nocache.h"

buffer_float_t FIR_9FloatPrinter_10;
buffer_float_t FloatSource_8FIR_9;


FloatSource_8_t FloatSource_8_s;
FIR_9_t FIR_9_s;

void FloatSource_8() {
	push_float(&FloatSource_8FIR_9, FloatSource_8_s.num) ; 
	FloatSource_8_s.num = (FloatSource_8_s.num + 1.0) ; 
	if(FloatSource_8_s.num == 10000.0) {
		FloatSource_8_s.num = 0.0 ; 
	}
}


void FIR_9() {
	float sum = 0.0;
	FOR(int, i, 0,  < , 64, i++) {
		sum = (sum + (FIR_9_s.h[i] * peek_float(&FloatSource_8FIR_9, i))) ; 
	}
	ENDFOR
	push_float(&FIR_9FloatPrinter_10, sum) ; 
	pop_float(&FloatSource_8FIR_9) ; 
}


void FloatPrinter_10() {
	printf("%.10f", pop_float(&FIR_9FloatPrinter_10));
	printf("\n");
}


void __stream_init__() {
	init_buffer_float(&FIR_9FloatPrinter_10);
	init_buffer_float(&FloatSource_8FIR_9);
// --- init: FloatSource_8
	 {
	FloatSource_8_s.num = 0.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 63, __iter_init_++) {
		push_float(&FloatSource_8FIR_9, FloatSource_8_s.num) ; 
		FloatSource_8_s.num = (FloatSource_8_s.num + 1.0) ; 
		if(FloatSource_8_s.num == 10000.0) {
			FloatSource_8_s.num = 0.0 ; 
		}
	}
	ENDFOR
//--------------------------------
// --- init: FIR_9
	 {
	FOR(int, i, 0,  < , 64, i++) {
		FIR_9_s.h[i] = (((2 * ((64 - i) - 1)) * ((64 - i) - 1)) / (((64 - i) - 1) + 1.0)) ; 
	}
	ENDFOR
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		FloatSource_8();
		FIR_9();
		FloatPrinter_10();
	ENDFOR
	return EXIT_SUCCESS;
}
