#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=65 on the compile command line
#else
#if BUF_SIZEMAX < 65
#error BUF_SIZEMAX too small, it must be at least 65
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float num;
} FloatSource_8_t;

typedef struct {
	float h[64];
} FIR_9_t;
void FloatSource_8();
void FIR_9();
void FloatPrinter_10();

#ifdef __cplusplus
}
#endif
#endif
