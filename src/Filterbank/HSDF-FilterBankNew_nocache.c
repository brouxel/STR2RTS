#include "HSDF-FilterBankNew_nocache.h"

buffer_float_t UpSamp_272WEIGHTED_ROUND_ROBIN_Splitter_531;
buffer_float_t SplitJoin235_FirFilter_Fiss_668_702_split[8];
buffer_float_t SplitJoin2_Delay_N_Fiss_642_677_split[8];
buffer_float_t DownSamp_257UpSamp_258;
buffer_float_t SplitJoin200_Delay_N_Fiss_665_699_join[8];
buffer_float_t SplitJoin198_FirFilter_Fiss_664_698_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_372DUPLICATE_Splitter_381;
buffer_float_t SplitJoin159_Delay_N_Fiss_659_693_split[8];
buffer_float_t SplitJoin54_FirFilter_Fiss_650_684_split[8];
buffer_float_t SplitJoin235_FirFilter_Fiss_668_702_join[8];
buffer_float_t SplitJoin276_FirFilter_Fiss_674_708_join[8];
buffer_float_t UpSamp_265WEIGHTED_ROUND_ROBIN_Splitter_491;
buffer_float_t SplitJoin196_Delay_N_Fiss_663_697_split[8];
buffer_float_t SplitJoin274_Delay_N_Fiss_673_707_join[8];
buffer_float_t SplitJoin126_Delay_N_Fiss_657_691_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_402DownSamp_250;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_632sink_290;
buffer_float_t SplitJoin161_FirFilter_Fiss_660_694_join[8];
buffer_float_t SplitJoin52_Delay_N_Fiss_649_683_split[8];
buffer_float_t DownSamp_243UpSamp_244;
buffer_float_t SplitJoin8_FirFilter_Fiss_645_680_join[8];
buffer_float_t SplitJoin87_FirFilter_Fiss_652_686_join[8];
buffer_float_t SplitJoin8_FirFilter_Fiss_645_680_split[8];
buffer_float_t SplitJoin2_Delay_N_Fiss_642_677_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_592DUPLICATE_Splitter_601;
buffer_float_t SplitJoin128_FirFilter_Fiss_658_692_join[8];
buffer_float_t SplitJoin239_FirFilter_Fiss_670_704_split[8];
buffer_float_t SplitJoin48_Delay_N_Fiss_647_681_join[8];
buffer_float_t SplitJoin128_FirFilter_Fiss_658_692_split[8];
buffer_float_t UpSamp_258WEIGHTED_ROUND_ROBIN_Splitter_451;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_492DUPLICATE_Splitter_501;
buffer_float_t SplitJoin6_Delay_N_Fiss_644_679_split[8];
buffer_float_t SplitJoin0_SplitJoin0_SplitJoin0_Branches_229_293_641_676_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_602DownSamp_285;
buffer_float_t SplitJoin10_Combine_Fiss_646_709_join[8];
buffer_float_t SplitJoin274_Delay_N_Fiss_673_707_split[8];
buffer_float_t SplitJoin50_FirFilter_Fiss_648_682_split[8];
buffer_float_t SplitJoin48_Delay_N_Fiss_647_681_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_322DownSamp_236;
buffer_float_t SplitJoin54_FirFilter_Fiss_650_684_join[8];
buffer_float_t SplitJoin270_Delay_N_Fiss_671_705_join[8];
buffer_float_t SplitJoin276_FirFilter_Fiss_674_708_split[8];
buffer_float_t SplitJoin89_Delay_N_Fiss_653_687_join[8];
buffer_float_t SplitJoin124_FirFilter_Fiss_656_690_split[8];
buffer_float_t DownSamp_236UpSamp_237;
buffer_float_t SplitJoin233_Delay_N_Fiss_667_701_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_292WEIGHTED_ROUND_ROBIN_Splitter_631;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_362DownSamp_243;
buffer_float_t SplitJoin6_Delay_N_Fiss_644_679_join[8];
buffer_float_t DownSamp_278UpSamp_279;
buffer_float_t UpSamp_279WEIGHTED_ROUND_ROBIN_Splitter_571;
buffer_float_t SplitJoin200_Delay_N_Fiss_665_699_split[8];
buffer_float_t SplitJoin4_FirFilter_Fiss_643_678_join[8];
buffer_float_t SplitJoin124_FirFilter_Fiss_656_690_join[8];
buffer_float_t SplitJoin159_Delay_N_Fiss_659_693_join[8];
buffer_float_t SplitJoin165_FirFilter_Fiss_662_696_join[8];
buffer_float_t SplitJoin163_Delay_N_Fiss_661_695_split[8];
buffer_float_t SplitJoin126_Delay_N_Fiss_657_691_split[8];
buffer_float_t SplitJoin270_Delay_N_Fiss_671_705_split[8];
buffer_float_t SplitJoin161_FirFilter_Fiss_660_694_split[8];
buffer_float_t SplitJoin198_FirFilter_Fiss_664_698_join[8];
buffer_float_t SplitJoin89_Delay_N_Fiss_653_687_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_482DownSamp_264;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_392DUPLICATE_Splitter_401;
buffer_float_t SplitJoin233_Delay_N_Fiss_667_701_split[8];
buffer_float_t source_231DUPLICATE_Splitter_291;
buffer_float_t UpSamp_286WEIGHTED_ROUND_ROBIN_Splitter_611;
buffer_float_t SplitJoin85_Delay_N_Fiss_651_685_join[8];
buffer_float_t SplitJoin87_FirFilter_Fiss_652_686_split[8];
buffer_float_t SplitJoin272_FirFilter_Fiss_672_706_split[8];
buffer_float_t SplitJoin4_FirFilter_Fiss_643_678_split[8];
buffer_float_t SplitJoin237_Delay_N_Fiss_669_703_join[8];
buffer_float_t UpSamp_244WEIGHTED_ROUND_ROBIN_Splitter_371;
buffer_float_t SplitJoin91_FirFilter_Fiss_654_688_split[8];
buffer_float_t SplitJoin163_Delay_N_Fiss_661_695_join[8];
buffer_float_t SplitJoin202_FirFilter_Fiss_666_700_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_412DUPLICATE_Splitter_421;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_572DUPLICATE_Splitter_581;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_442DownSamp_257;
buffer_float_t DownSamp_264UpSamp_265;
buffer_float_t SplitJoin10_Combine_Fiss_646_709_split[8];
buffer_float_t SplitJoin85_Delay_N_Fiss_651_685_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_452DUPLICATE_Splitter_461;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_432DUPLICATE_Splitter_441;
buffer_float_t SplitJoin202_FirFilter_Fiss_666_700_join[8];
buffer_float_t DownSamp_285UpSamp_286;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_312DUPLICATE_Splitter_321;
buffer_float_t SplitJoin52_Delay_N_Fiss_649_683_join[8];
buffer_float_t UpSamp_251WEIGHTED_ROUND_ROBIN_Splitter_411;
buffer_float_t SplitJoin50_FirFilter_Fiss_648_682_join[8];
buffer_float_t SplitJoin165_FirFilter_Fiss_662_696_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_522DownSamp_271;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_352DUPLICATE_Splitter_361;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_332DUPLICATE_Splitter_341;
buffer_float_t SplitJoin91_FirFilter_Fiss_654_688_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_562DownSamp_278;
buffer_float_t SplitJoin122_Delay_N_Fiss_655_689_join[8];
buffer_float_t SplitJoin239_FirFilter_Fiss_670_704_join[8];
buffer_float_t UpSamp_237WEIGHTED_ROUND_ROBIN_Splitter_331;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_612DUPLICATE_Splitter_621;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_472DUPLICATE_Splitter_481;
buffer_float_t SplitJoin237_Delay_N_Fiss_669_703_split[8];
buffer_float_t DownSamp_271UpSamp_272;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_532DUPLICATE_Splitter_541;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_512DUPLICATE_Splitter_521;
buffer_float_t SplitJoin0_SplitJoin0_SplitJoin0_Branches_229_293_641_676_split[8];
buffer_float_t SplitJoin272_FirFilter_Fiss_672_706_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_552DUPLICATE_Splitter_561;
buffer_float_t DownSamp_250UpSamp_251;
buffer_float_t SplitJoin122_Delay_N_Fiss_655_689_split[8];
buffer_float_t SplitJoin196_Delay_N_Fiss_663_697_join[8];


source_231_t source_231_s;
FirFilter_323_t FirFilter_323_s;
FirFilter_323_t FirFilter_324_s;
FirFilter_323_t FirFilter_325_s;
FirFilter_323_t FirFilter_326_s;
FirFilter_323_t FirFilter_327_s;
FirFilter_323_t FirFilter_328_s;
FirFilter_323_t FirFilter_329_s;
FirFilter_323_t FirFilter_330_s;
FirFilter_323_t FirFilter_343_s;
FirFilter_323_t FirFilter_344_s;
FirFilter_323_t FirFilter_345_s;
FirFilter_323_t FirFilter_346_s;
FirFilter_323_t FirFilter_347_s;
FirFilter_323_t FirFilter_348_s;
FirFilter_323_t FirFilter_349_s;
FirFilter_323_t FirFilter_350_s;
FirFilter_323_t FirFilter_363_s;
FirFilter_323_t FirFilter_364_s;
FirFilter_323_t FirFilter_365_s;
FirFilter_323_t FirFilter_366_s;
FirFilter_323_t FirFilter_367_s;
FirFilter_323_t FirFilter_368_s;
FirFilter_323_t FirFilter_369_s;
FirFilter_323_t FirFilter_370_s;
FirFilter_323_t FirFilter_383_s;
FirFilter_323_t FirFilter_384_s;
FirFilter_323_t FirFilter_385_s;
FirFilter_323_t FirFilter_386_s;
FirFilter_323_t FirFilter_387_s;
FirFilter_323_t FirFilter_388_s;
FirFilter_323_t FirFilter_389_s;
FirFilter_323_t FirFilter_390_s;
FirFilter_323_t FirFilter_403_s;
FirFilter_323_t FirFilter_404_s;
FirFilter_323_t FirFilter_405_s;
FirFilter_323_t FirFilter_406_s;
FirFilter_323_t FirFilter_407_s;
FirFilter_323_t FirFilter_408_s;
FirFilter_323_t FirFilter_409_s;
FirFilter_323_t FirFilter_410_s;
FirFilter_323_t FirFilter_423_s;
FirFilter_323_t FirFilter_424_s;
FirFilter_323_t FirFilter_425_s;
FirFilter_323_t FirFilter_426_s;
FirFilter_323_t FirFilter_427_s;
FirFilter_323_t FirFilter_428_s;
FirFilter_323_t FirFilter_429_s;
FirFilter_323_t FirFilter_430_s;
FirFilter_323_t FirFilter_443_s;
FirFilter_323_t FirFilter_444_s;
FirFilter_323_t FirFilter_445_s;
FirFilter_323_t FirFilter_446_s;
FirFilter_323_t FirFilter_447_s;
FirFilter_323_t FirFilter_448_s;
FirFilter_323_t FirFilter_449_s;
FirFilter_323_t FirFilter_450_s;
FirFilter_323_t FirFilter_463_s;
FirFilter_323_t FirFilter_464_s;
FirFilter_323_t FirFilter_465_s;
FirFilter_323_t FirFilter_466_s;
FirFilter_323_t FirFilter_467_s;
FirFilter_323_t FirFilter_468_s;
FirFilter_323_t FirFilter_469_s;
FirFilter_323_t FirFilter_470_s;
FirFilter_323_t FirFilter_483_s;
FirFilter_323_t FirFilter_484_s;
FirFilter_323_t FirFilter_485_s;
FirFilter_323_t FirFilter_486_s;
FirFilter_323_t FirFilter_487_s;
FirFilter_323_t FirFilter_488_s;
FirFilter_323_t FirFilter_489_s;
FirFilter_323_t FirFilter_490_s;
FirFilter_323_t FirFilter_503_s;
FirFilter_323_t FirFilter_504_s;
FirFilter_323_t FirFilter_505_s;
FirFilter_323_t FirFilter_506_s;
FirFilter_323_t FirFilter_507_s;
FirFilter_323_t FirFilter_508_s;
FirFilter_323_t FirFilter_509_s;
FirFilter_323_t FirFilter_510_s;
FirFilter_323_t FirFilter_523_s;
FirFilter_323_t FirFilter_524_s;
FirFilter_323_t FirFilter_525_s;
FirFilter_323_t FirFilter_526_s;
FirFilter_323_t FirFilter_527_s;
FirFilter_323_t FirFilter_528_s;
FirFilter_323_t FirFilter_529_s;
FirFilter_323_t FirFilter_530_s;
FirFilter_323_t FirFilter_543_s;
FirFilter_323_t FirFilter_544_s;
FirFilter_323_t FirFilter_545_s;
FirFilter_323_t FirFilter_546_s;
FirFilter_323_t FirFilter_547_s;
FirFilter_323_t FirFilter_548_s;
FirFilter_323_t FirFilter_549_s;
FirFilter_323_t FirFilter_550_s;
FirFilter_323_t FirFilter_563_s;
FirFilter_323_t FirFilter_564_s;
FirFilter_323_t FirFilter_565_s;
FirFilter_323_t FirFilter_566_s;
FirFilter_323_t FirFilter_567_s;
FirFilter_323_t FirFilter_568_s;
FirFilter_323_t FirFilter_569_s;
FirFilter_323_t FirFilter_570_s;
FirFilter_323_t FirFilter_583_s;
FirFilter_323_t FirFilter_584_s;
FirFilter_323_t FirFilter_585_s;
FirFilter_323_t FirFilter_586_s;
FirFilter_323_t FirFilter_587_s;
FirFilter_323_t FirFilter_588_s;
FirFilter_323_t FirFilter_589_s;
FirFilter_323_t FirFilter_590_s;
FirFilter_323_t FirFilter_603_s;
FirFilter_323_t FirFilter_604_s;
FirFilter_323_t FirFilter_605_s;
FirFilter_323_t FirFilter_606_s;
FirFilter_323_t FirFilter_607_s;
FirFilter_323_t FirFilter_608_s;
FirFilter_323_t FirFilter_609_s;
FirFilter_323_t FirFilter_610_s;
FirFilter_323_t FirFilter_623_s;
FirFilter_323_t FirFilter_624_s;
FirFilter_323_t FirFilter_625_s;
FirFilter_323_t FirFilter_626_s;
FirFilter_323_t FirFilter_627_s;
FirFilter_323_t FirFilter_628_s;
FirFilter_323_t FirFilter_629_s;
FirFilter_323_t FirFilter_630_s;

void source_231(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&source_231DUPLICATE_Splitter_291, source_231_s.current) ; 
		if((source_231_s.current > 1000.0)) {
			source_231_s.current = 0.0 ; 
		}
		else {
			source_231_s.current = (source_231_s.current + 1.0) ; 
		}
	}
	ENDFOR
}

void Delay_N_313() {
	push_float(&SplitJoin2_Delay_N_Fiss_642_677_join[0], pop_float(&SplitJoin2_Delay_N_Fiss_642_677_split[0])) ; 
}


void Delay_N_314() {
	push_float(&SplitJoin2_Delay_N_Fiss_642_677_join[1], pop_float(&SplitJoin2_Delay_N_Fiss_642_677_split[1])) ; 
}


void Delay_N_315() {
	push_float(&SplitJoin2_Delay_N_Fiss_642_677_join[2], pop_float(&SplitJoin2_Delay_N_Fiss_642_677_split[2])) ; 
}


void Delay_N_316() {
	push_float(&SplitJoin2_Delay_N_Fiss_642_677_join[3], pop_float(&SplitJoin2_Delay_N_Fiss_642_677_split[3])) ; 
}


void Delay_N_317() {
	push_float(&SplitJoin2_Delay_N_Fiss_642_677_join[4], pop_float(&SplitJoin2_Delay_N_Fiss_642_677_split[4])) ; 
}


void Delay_N_318() {
	push_float(&SplitJoin2_Delay_N_Fiss_642_677_join[5], pop_float(&SplitJoin2_Delay_N_Fiss_642_677_split[5])) ; 
}


void Delay_N_319() {
	push_float(&SplitJoin2_Delay_N_Fiss_642_677_join[6], pop_float(&SplitJoin2_Delay_N_Fiss_642_677_split[6])) ; 
}


void Delay_N_320() {
	push_float(&SplitJoin2_Delay_N_Fiss_642_677_join[7], pop_float(&SplitJoin2_Delay_N_Fiss_642_677_split[7])) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_311() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&SplitJoin2_Delay_N_Fiss_642_677_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_229_293_641_676_split[0]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_312() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_312DUPLICATE_Splitter_321, pop_float(&SplitJoin2_Delay_N_Fiss_642_677_join[__iter_]));
	ENDFOR
}

void FirFilter_323() {
	float sum = 0.0;
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin4_FirFilter_Fiss_643_678_split[0], i) * FirFilter_323_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin4_FirFilter_Fiss_643_678_split[0]) ; 
	push_float(&SplitJoin4_FirFilter_Fiss_643_678_join[0], sum) ; 
 {
	FOR(int, streamItVar61, 0,  < , 7, streamItVar61++) {
		pop_void(&SplitJoin4_FirFilter_Fiss_643_678_split[0]) ; 
	}
	ENDFOR
}
}


void FirFilter_324() {
	float sum = 0.0;
 {
	pop_void(&SplitJoin4_FirFilter_Fiss_643_678_split[1]) ; 
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin4_FirFilter_Fiss_643_678_split[1], i) * FirFilter_324_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin4_FirFilter_Fiss_643_678_split[1]) ; 
	push_float(&SplitJoin4_FirFilter_Fiss_643_678_join[1], sum) ; 
 {
	FOR(int, streamItVar62, 0,  < , 6, streamItVar62++) {
		pop_void(&SplitJoin4_FirFilter_Fiss_643_678_split[1]) ; 
	}
	ENDFOR
}
}


void FirFilter_325() {
	float sum = 0.0;
 {
	FOR(int, streamItVar63, 0,  < , 2, streamItVar63++) {
		pop_void(&SplitJoin4_FirFilter_Fiss_643_678_split[2]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin4_FirFilter_Fiss_643_678_split[2], i) * FirFilter_325_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin4_FirFilter_Fiss_643_678_split[2]) ; 
	push_float(&SplitJoin4_FirFilter_Fiss_643_678_join[2], sum) ; 
 {
	FOR(int, streamItVar64, 0,  < , 5, streamItVar64++) {
		pop_void(&SplitJoin4_FirFilter_Fiss_643_678_split[2]) ; 
	}
	ENDFOR
}
}


void FirFilter_326() {
	float sum = 0.0;
 {
	FOR(int, streamItVar65, 0,  < , 3, streamItVar65++) {
		pop_void(&SplitJoin4_FirFilter_Fiss_643_678_split[3]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin4_FirFilter_Fiss_643_678_split[3], i) * FirFilter_326_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin4_FirFilter_Fiss_643_678_split[3]) ; 
	push_float(&SplitJoin4_FirFilter_Fiss_643_678_join[3], sum) ; 
 {
	FOR(int, streamItVar66, 0,  < , 4, streamItVar66++) {
		pop_void(&SplitJoin4_FirFilter_Fiss_643_678_split[3]) ; 
	}
	ENDFOR
}
}


void FirFilter_327() {
	float sum = 0.0;
 {
	FOR(int, streamItVar67, 0,  < , 4, streamItVar67++) {
		pop_void(&SplitJoin4_FirFilter_Fiss_643_678_split[4]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin4_FirFilter_Fiss_643_678_split[4], i) * FirFilter_327_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin4_FirFilter_Fiss_643_678_split[4]) ; 
	push_float(&SplitJoin4_FirFilter_Fiss_643_678_join[4], sum) ; 
 {
	FOR(int, streamItVar68, 0,  < , 3, streamItVar68++) {
		pop_void(&SplitJoin4_FirFilter_Fiss_643_678_split[4]) ; 
	}
	ENDFOR
}
}


void FirFilter_328() {
	float sum = 0.0;
 {
	FOR(int, streamItVar69, 0,  < , 5, streamItVar69++) {
		pop_void(&SplitJoin4_FirFilter_Fiss_643_678_split[5]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin4_FirFilter_Fiss_643_678_split[5], i) * FirFilter_328_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin4_FirFilter_Fiss_643_678_split[5]) ; 
	push_float(&SplitJoin4_FirFilter_Fiss_643_678_join[5], sum) ; 
 {
	FOR(int, streamItVar70, 0,  < , 2, streamItVar70++) {
		pop_void(&SplitJoin4_FirFilter_Fiss_643_678_split[5]) ; 
	}
	ENDFOR
}
}


void FirFilter_329() {
	float sum = 0.0;
 {
	FOR(int, streamItVar71, 0,  < , 6, streamItVar71++) {
		pop_void(&SplitJoin4_FirFilter_Fiss_643_678_split[6]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin4_FirFilter_Fiss_643_678_split[6], i) * FirFilter_329_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin4_FirFilter_Fiss_643_678_split[6]) ; 
	push_float(&SplitJoin4_FirFilter_Fiss_643_678_join[6], sum) ; 
 {
	pop_void(&SplitJoin4_FirFilter_Fiss_643_678_split[6]) ; 
}
}


void FirFilter_330() {
	float sum = 0.0;
 {
	FOR(int, streamItVar72, 0,  < , 7, streamItVar72++) {
		pop_void(&SplitJoin4_FirFilter_Fiss_643_678_split[7]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin4_FirFilter_Fiss_643_678_split[7], i) * FirFilter_330_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin4_FirFilter_Fiss_643_678_split[7]) ; 
	push_float(&SplitJoin4_FirFilter_Fiss_643_678_join[7], sum) ; 
}


void DUPLICATE_Splitter_321() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_312DUPLICATE_Splitter_321);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin4_FirFilter_Fiss_643_678_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_322() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_322DownSamp_236, pop_float(&SplitJoin4_FirFilter_Fiss_643_678_join[__iter_]));
	ENDFOR
}

void DownSamp_236() {
	push_float(&DownSamp_236UpSamp_237, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_322DownSamp_236)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_322DownSamp_236) ; 
	}
	ENDFOR
}


void UpSamp_237() {
	push_float(&UpSamp_237WEIGHTED_ROUND_ROBIN_Splitter_331, pop_float(&DownSamp_236UpSamp_237)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		push_float(&UpSamp_237WEIGHTED_ROUND_ROBIN_Splitter_331, 0.0) ; 
	}
	ENDFOR
}


void Delay_N_333() {
	push_float(&SplitJoin6_Delay_N_Fiss_644_679_join[0], pop_float(&SplitJoin6_Delay_N_Fiss_644_679_split[0])) ; 
}


void Delay_N_334() {
	push_float(&SplitJoin6_Delay_N_Fiss_644_679_join[1], pop_float(&SplitJoin6_Delay_N_Fiss_644_679_split[1])) ; 
}


void Delay_N_335() {
	push_float(&SplitJoin6_Delay_N_Fiss_644_679_join[2], pop_float(&SplitJoin6_Delay_N_Fiss_644_679_split[2])) ; 
}


void Delay_N_336() {
	push_float(&SplitJoin6_Delay_N_Fiss_644_679_join[3], pop_float(&SplitJoin6_Delay_N_Fiss_644_679_split[3])) ; 
}


void Delay_N_337() {
	push_float(&SplitJoin6_Delay_N_Fiss_644_679_join[4], pop_float(&SplitJoin6_Delay_N_Fiss_644_679_split[4])) ; 
}


void Delay_N_338() {
	push_float(&SplitJoin6_Delay_N_Fiss_644_679_join[5], pop_float(&SplitJoin6_Delay_N_Fiss_644_679_split[5])) ; 
}


void Delay_N_339() {
	push_float(&SplitJoin6_Delay_N_Fiss_644_679_join[6], pop_float(&SplitJoin6_Delay_N_Fiss_644_679_split[6])) ; 
}


void Delay_N_340() {
	push_float(&SplitJoin6_Delay_N_Fiss_644_679_join[7], pop_float(&SplitJoin6_Delay_N_Fiss_644_679_split[7])) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_331() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&SplitJoin6_Delay_N_Fiss_644_679_split[__iter_], pop_float(&UpSamp_237WEIGHTED_ROUND_ROBIN_Splitter_331));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_332() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_332DUPLICATE_Splitter_341, pop_float(&SplitJoin6_Delay_N_Fiss_644_679_join[__iter_]));
	ENDFOR
}

void FirFilter_343() {
	float sum = 0.0;
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin8_FirFilter_Fiss_645_680_split[0], i) * FirFilter_343_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin8_FirFilter_Fiss_645_680_split[0]) ; 
	push_float(&SplitJoin8_FirFilter_Fiss_645_680_join[0], sum) ; 
 {
	FOR(int, streamItVar49, 0,  < , 7, streamItVar49++) {
		pop_void(&SplitJoin8_FirFilter_Fiss_645_680_split[0]) ; 
	}
	ENDFOR
}
}


void FirFilter_344() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	pop_void(&SplitJoin8_FirFilter_Fiss_645_680_split[1]) ; 
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin8_FirFilter_Fiss_645_680_split[1], i) * FirFilter_344_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin8_FirFilter_Fiss_645_680_split[1]) ; 
	push_float(&SplitJoin8_FirFilter_Fiss_645_680_join[1], sum) ; 
 {
	FOR(int, streamItVar50, 0,  < , 6, streamItVar50++) {
		pop_void(&SplitJoin8_FirFilter_Fiss_645_680_split[1]) ; 
	}
	ENDFOR
}
}


void FirFilter_345() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar51, 0,  < , 2, streamItVar51++) {
		pop_void(&SplitJoin8_FirFilter_Fiss_645_680_split[2]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin8_FirFilter_Fiss_645_680_split[2], i) * FirFilter_345_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin8_FirFilter_Fiss_645_680_split[2]) ; 
	push_float(&SplitJoin8_FirFilter_Fiss_645_680_join[2], sum) ; 
 {
	FOR(int, streamItVar52, 0,  < , 5, streamItVar52++) {
		pop_void(&SplitJoin8_FirFilter_Fiss_645_680_split[2]) ; 
	}
	ENDFOR
}
}


void FirFilter_346() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar53, 0,  < , 3, streamItVar53++) {
		pop_void(&SplitJoin8_FirFilter_Fiss_645_680_split[3]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin8_FirFilter_Fiss_645_680_split[3], i) * FirFilter_346_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin8_FirFilter_Fiss_645_680_split[3]) ; 
	push_float(&SplitJoin8_FirFilter_Fiss_645_680_join[3], sum) ; 
 {
	FOR(int, streamItVar54, 0,  < , 4, streamItVar54++) {
		pop_void(&SplitJoin8_FirFilter_Fiss_645_680_split[3]) ; 
	}
	ENDFOR
}
}


void FirFilter_347() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar55, 0,  < , 4, streamItVar55++) {
		pop_void(&SplitJoin8_FirFilter_Fiss_645_680_split[4]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin8_FirFilter_Fiss_645_680_split[4], i) * FirFilter_347_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin8_FirFilter_Fiss_645_680_split[4]) ; 
	push_float(&SplitJoin8_FirFilter_Fiss_645_680_join[4], sum) ; 
 {
	FOR(int, streamItVar56, 0,  < , 3, streamItVar56++) {
		pop_void(&SplitJoin8_FirFilter_Fiss_645_680_split[4]) ; 
	}
	ENDFOR
}
}


void FirFilter_348() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar57, 0,  < , 5, streamItVar57++) {
		pop_void(&SplitJoin8_FirFilter_Fiss_645_680_split[5]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin8_FirFilter_Fiss_645_680_split[5], i) * FirFilter_348_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin8_FirFilter_Fiss_645_680_split[5]) ; 
	push_float(&SplitJoin8_FirFilter_Fiss_645_680_join[5], sum) ; 
 {
	FOR(int, streamItVar58, 0,  < , 2, streamItVar58++) {
		pop_void(&SplitJoin8_FirFilter_Fiss_645_680_split[5]) ; 
	}
	ENDFOR
}
}


void FirFilter_349() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar59, 0,  < , 6, streamItVar59++) {
		pop_void(&SplitJoin8_FirFilter_Fiss_645_680_split[6]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin8_FirFilter_Fiss_645_680_split[6], i) * FirFilter_349_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin8_FirFilter_Fiss_645_680_split[6]) ; 
	push_float(&SplitJoin8_FirFilter_Fiss_645_680_join[6], sum) ; 
 {
	pop_void(&SplitJoin8_FirFilter_Fiss_645_680_split[6]) ; 
}
}


void FirFilter_350() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar60, 0,  < , 7, streamItVar60++) {
		pop_void(&SplitJoin8_FirFilter_Fiss_645_680_split[7]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin8_FirFilter_Fiss_645_680_split[7], i) * FirFilter_350_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin8_FirFilter_Fiss_645_680_split[7]) ; 
	push_float(&SplitJoin8_FirFilter_Fiss_645_680_join[7], sum) ; 
}


void DUPLICATE_Splitter_341() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_332DUPLICATE_Splitter_341);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin8_FirFilter_Fiss_645_680_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_342() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_229_293_641_676_join[0], pop_float(&SplitJoin8_FirFilter_Fiss_645_680_join[__iter_]));
	ENDFOR
}

void Delay_N_353() {
	push_float(&SplitJoin48_Delay_N_Fiss_647_681_join[0], pop_float(&SplitJoin48_Delay_N_Fiss_647_681_split[0])) ; 
}


void Delay_N_354() {
	push_float(&SplitJoin48_Delay_N_Fiss_647_681_join[1], pop_float(&SplitJoin48_Delay_N_Fiss_647_681_split[1])) ; 
}


void Delay_N_355() {
	push_float(&SplitJoin48_Delay_N_Fiss_647_681_join[2], pop_float(&SplitJoin48_Delay_N_Fiss_647_681_split[2])) ; 
}


void Delay_N_356() {
	push_float(&SplitJoin48_Delay_N_Fiss_647_681_join[3], pop_float(&SplitJoin48_Delay_N_Fiss_647_681_split[3])) ; 
}


void Delay_N_357() {
	push_float(&SplitJoin48_Delay_N_Fiss_647_681_join[4], pop_float(&SplitJoin48_Delay_N_Fiss_647_681_split[4])) ; 
}


void Delay_N_358() {
	push_float(&SplitJoin48_Delay_N_Fiss_647_681_join[5], pop_float(&SplitJoin48_Delay_N_Fiss_647_681_split[5])) ; 
}


void Delay_N_359() {
	push_float(&SplitJoin48_Delay_N_Fiss_647_681_join[6], pop_float(&SplitJoin48_Delay_N_Fiss_647_681_split[6])) ; 
}


void Delay_N_360() {
	push_float(&SplitJoin48_Delay_N_Fiss_647_681_join[7], pop_float(&SplitJoin48_Delay_N_Fiss_647_681_split[7])) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_351() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&SplitJoin48_Delay_N_Fiss_647_681_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_229_293_641_676_split[1]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_352() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_352DUPLICATE_Splitter_361, pop_float(&SplitJoin48_Delay_N_Fiss_647_681_join[__iter_]));
	ENDFOR
}

void FirFilter_363() {
	float sum = 0.0;
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin50_FirFilter_Fiss_648_682_split[0], i) * FirFilter_363_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin50_FirFilter_Fiss_648_682_split[0]) ; 
	push_float(&SplitJoin50_FirFilter_Fiss_648_682_join[0], sum) ; 
 {
	FOR(int, streamItVar37, 0,  < , 7, streamItVar37++) {
		pop_void(&SplitJoin50_FirFilter_Fiss_648_682_split[0]) ; 
	}
	ENDFOR
}
}


void FirFilter_364() {
	float sum = 0.0;
 {
	pop_void(&SplitJoin50_FirFilter_Fiss_648_682_split[1]) ; 
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin50_FirFilter_Fiss_648_682_split[1], i) * FirFilter_364_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin50_FirFilter_Fiss_648_682_split[1]) ; 
	push_float(&SplitJoin50_FirFilter_Fiss_648_682_join[1], sum) ; 
 {
	FOR(int, streamItVar38, 0,  < , 6, streamItVar38++) {
		pop_void(&SplitJoin50_FirFilter_Fiss_648_682_split[1]) ; 
	}
	ENDFOR
}
}


void FirFilter_365() {
	float sum = 0.0;
 {
	FOR(int, streamItVar39, 0,  < , 2, streamItVar39++) {
		pop_void(&SplitJoin50_FirFilter_Fiss_648_682_split[2]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin50_FirFilter_Fiss_648_682_split[2], i) * FirFilter_365_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin50_FirFilter_Fiss_648_682_split[2]) ; 
	push_float(&SplitJoin50_FirFilter_Fiss_648_682_join[2], sum) ; 
 {
	FOR(int, streamItVar40, 0,  < , 5, streamItVar40++) {
		pop_void(&SplitJoin50_FirFilter_Fiss_648_682_split[2]) ; 
	}
	ENDFOR
}
}


void FirFilter_366() {
	float sum = 0.0;
 {
	FOR(int, streamItVar41, 0,  < , 3, streamItVar41++) {
		pop_void(&SplitJoin50_FirFilter_Fiss_648_682_split[3]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin50_FirFilter_Fiss_648_682_split[3], i) * FirFilter_366_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin50_FirFilter_Fiss_648_682_split[3]) ; 
	push_float(&SplitJoin50_FirFilter_Fiss_648_682_join[3], sum) ; 
 {
	FOR(int, streamItVar42, 0,  < , 4, streamItVar42++) {
		pop_void(&SplitJoin50_FirFilter_Fiss_648_682_split[3]) ; 
	}
	ENDFOR
}
}


void FirFilter_367() {
	float sum = 0.0;
 {
	FOR(int, streamItVar43, 0,  < , 4, streamItVar43++) {
		pop_void(&SplitJoin50_FirFilter_Fiss_648_682_split[4]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin50_FirFilter_Fiss_648_682_split[4], i) * FirFilter_367_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin50_FirFilter_Fiss_648_682_split[4]) ; 
	push_float(&SplitJoin50_FirFilter_Fiss_648_682_join[4], sum) ; 
 {
	FOR(int, streamItVar44, 0,  < , 3, streamItVar44++) {
		pop_void(&SplitJoin50_FirFilter_Fiss_648_682_split[4]) ; 
	}
	ENDFOR
}
}


void FirFilter_368() {
	float sum = 0.0;
 {
	FOR(int, streamItVar45, 0,  < , 5, streamItVar45++) {
		pop_void(&SplitJoin50_FirFilter_Fiss_648_682_split[5]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin50_FirFilter_Fiss_648_682_split[5], i) * FirFilter_368_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin50_FirFilter_Fiss_648_682_split[5]) ; 
	push_float(&SplitJoin50_FirFilter_Fiss_648_682_join[5], sum) ; 
 {
	FOR(int, streamItVar46, 0,  < , 2, streamItVar46++) {
		pop_void(&SplitJoin50_FirFilter_Fiss_648_682_split[5]) ; 
	}
	ENDFOR
}
}


void FirFilter_369() {
	float sum = 0.0;
 {
	FOR(int, streamItVar47, 0,  < , 6, streamItVar47++) {
		pop_void(&SplitJoin50_FirFilter_Fiss_648_682_split[6]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin50_FirFilter_Fiss_648_682_split[6], i) * FirFilter_369_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin50_FirFilter_Fiss_648_682_split[6]) ; 
	push_float(&SplitJoin50_FirFilter_Fiss_648_682_join[6], sum) ; 
 {
	pop_void(&SplitJoin50_FirFilter_Fiss_648_682_split[6]) ; 
}
}


void FirFilter_370() {
	float sum = 0.0;
 {
	FOR(int, streamItVar48, 0,  < , 7, streamItVar48++) {
		pop_void(&SplitJoin50_FirFilter_Fiss_648_682_split[7]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin50_FirFilter_Fiss_648_682_split[7], i) * FirFilter_370_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin50_FirFilter_Fiss_648_682_split[7]) ; 
	push_float(&SplitJoin50_FirFilter_Fiss_648_682_join[7], sum) ; 
}


void DUPLICATE_Splitter_361() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_352DUPLICATE_Splitter_361);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin50_FirFilter_Fiss_648_682_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_362() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_362DownSamp_243, pop_float(&SplitJoin50_FirFilter_Fiss_648_682_join[__iter_]));
	ENDFOR
}

void DownSamp_243() {
	push_float(&DownSamp_243UpSamp_244, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_362DownSamp_243)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_362DownSamp_243) ; 
	}
	ENDFOR
}


void UpSamp_244() {
	push_float(&UpSamp_244WEIGHTED_ROUND_ROBIN_Splitter_371, pop_float(&DownSamp_243UpSamp_244)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		push_float(&UpSamp_244WEIGHTED_ROUND_ROBIN_Splitter_371, 0.0) ; 
	}
	ENDFOR
}


void Delay_N_373() {
	push_float(&SplitJoin52_Delay_N_Fiss_649_683_join[0], pop_float(&SplitJoin52_Delay_N_Fiss_649_683_split[0])) ; 
}


void Delay_N_374() {
	push_float(&SplitJoin52_Delay_N_Fiss_649_683_join[1], pop_float(&SplitJoin52_Delay_N_Fiss_649_683_split[1])) ; 
}


void Delay_N_375() {
	push_float(&SplitJoin52_Delay_N_Fiss_649_683_join[2], pop_float(&SplitJoin52_Delay_N_Fiss_649_683_split[2])) ; 
}


void Delay_N_376() {
	push_float(&SplitJoin52_Delay_N_Fiss_649_683_join[3], pop_float(&SplitJoin52_Delay_N_Fiss_649_683_split[3])) ; 
}


void Delay_N_377() {
	push_float(&SplitJoin52_Delay_N_Fiss_649_683_join[4], pop_float(&SplitJoin52_Delay_N_Fiss_649_683_split[4])) ; 
}


void Delay_N_378() {
	push_float(&SplitJoin52_Delay_N_Fiss_649_683_join[5], pop_float(&SplitJoin52_Delay_N_Fiss_649_683_split[5])) ; 
}


void Delay_N_379() {
	push_float(&SplitJoin52_Delay_N_Fiss_649_683_join[6], pop_float(&SplitJoin52_Delay_N_Fiss_649_683_split[6])) ; 
}


void Delay_N_380() {
	push_float(&SplitJoin52_Delay_N_Fiss_649_683_join[7], pop_float(&SplitJoin52_Delay_N_Fiss_649_683_split[7])) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_371() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&SplitJoin52_Delay_N_Fiss_649_683_split[__iter_], pop_float(&UpSamp_244WEIGHTED_ROUND_ROBIN_Splitter_371));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_372() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_372DUPLICATE_Splitter_381, pop_float(&SplitJoin52_Delay_N_Fiss_649_683_join[__iter_]));
	ENDFOR
}

void FirFilter_383() {
	float sum = 0.0;
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin54_FirFilter_Fiss_650_684_split[0], i) * FirFilter_383_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin54_FirFilter_Fiss_650_684_split[0]) ; 
	push_float(&SplitJoin54_FirFilter_Fiss_650_684_join[0], sum) ; 
 {
	FOR(int, streamItVar25, 0,  < , 7, streamItVar25++) {
		pop_void(&SplitJoin54_FirFilter_Fiss_650_684_split[0]) ; 
	}
	ENDFOR
}
}


void FirFilter_384() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	pop_void(&SplitJoin54_FirFilter_Fiss_650_684_split[1]) ; 
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin54_FirFilter_Fiss_650_684_split[1], i) * FirFilter_384_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin54_FirFilter_Fiss_650_684_split[1]) ; 
	push_float(&SplitJoin54_FirFilter_Fiss_650_684_join[1], sum) ; 
 {
	FOR(int, streamItVar26, 0,  < , 6, streamItVar26++) {
		pop_void(&SplitJoin54_FirFilter_Fiss_650_684_split[1]) ; 
	}
	ENDFOR
}
}


void FirFilter_385() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar27, 0,  < , 2, streamItVar27++) {
		pop_void(&SplitJoin54_FirFilter_Fiss_650_684_split[2]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin54_FirFilter_Fiss_650_684_split[2], i) * FirFilter_385_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin54_FirFilter_Fiss_650_684_split[2]) ; 
	push_float(&SplitJoin54_FirFilter_Fiss_650_684_join[2], sum) ; 
 {
	FOR(int, streamItVar28, 0,  < , 5, streamItVar28++) {
		pop_void(&SplitJoin54_FirFilter_Fiss_650_684_split[2]) ; 
	}
	ENDFOR
}
}


void FirFilter_386() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar29, 0,  < , 3, streamItVar29++) {
		pop_void(&SplitJoin54_FirFilter_Fiss_650_684_split[3]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin54_FirFilter_Fiss_650_684_split[3], i) * FirFilter_386_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin54_FirFilter_Fiss_650_684_split[3]) ; 
	push_float(&SplitJoin54_FirFilter_Fiss_650_684_join[3], sum) ; 
 {
	FOR(int, streamItVar30, 0,  < , 4, streamItVar30++) {
		pop_void(&SplitJoin54_FirFilter_Fiss_650_684_split[3]) ; 
	}
	ENDFOR
}
}


void FirFilter_387() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar31, 0,  < , 4, streamItVar31++) {
		pop_void(&SplitJoin54_FirFilter_Fiss_650_684_split[4]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin54_FirFilter_Fiss_650_684_split[4], i) * FirFilter_387_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin54_FirFilter_Fiss_650_684_split[4]) ; 
	push_float(&SplitJoin54_FirFilter_Fiss_650_684_join[4], sum) ; 
 {
	FOR(int, streamItVar32, 0,  < , 3, streamItVar32++) {
		pop_void(&SplitJoin54_FirFilter_Fiss_650_684_split[4]) ; 
	}
	ENDFOR
}
}


void FirFilter_388() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar33, 0,  < , 5, streamItVar33++) {
		pop_void(&SplitJoin54_FirFilter_Fiss_650_684_split[5]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin54_FirFilter_Fiss_650_684_split[5], i) * FirFilter_388_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin54_FirFilter_Fiss_650_684_split[5]) ; 
	push_float(&SplitJoin54_FirFilter_Fiss_650_684_join[5], sum) ; 
 {
	FOR(int, streamItVar34, 0,  < , 2, streamItVar34++) {
		pop_void(&SplitJoin54_FirFilter_Fiss_650_684_split[5]) ; 
	}
	ENDFOR
}
}


void FirFilter_389() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar35, 0,  < , 6, streamItVar35++) {
		pop_void(&SplitJoin54_FirFilter_Fiss_650_684_split[6]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin54_FirFilter_Fiss_650_684_split[6], i) * FirFilter_389_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin54_FirFilter_Fiss_650_684_split[6]) ; 
	push_float(&SplitJoin54_FirFilter_Fiss_650_684_join[6], sum) ; 
 {
	pop_void(&SplitJoin54_FirFilter_Fiss_650_684_split[6]) ; 
}
}


void FirFilter_390() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar36, 0,  < , 7, streamItVar36++) {
		pop_void(&SplitJoin54_FirFilter_Fiss_650_684_split[7]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin54_FirFilter_Fiss_650_684_split[7], i) * FirFilter_390_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin54_FirFilter_Fiss_650_684_split[7]) ; 
	push_float(&SplitJoin54_FirFilter_Fiss_650_684_join[7], sum) ; 
}


void DUPLICATE_Splitter_381() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_372DUPLICATE_Splitter_381);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin54_FirFilter_Fiss_650_684_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_382() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_229_293_641_676_join[1], pop_float(&SplitJoin54_FirFilter_Fiss_650_684_join[__iter_]));
	ENDFOR
}

void Delay_N_393() {
	push_float(&SplitJoin85_Delay_N_Fiss_651_685_join[0], pop_float(&SplitJoin85_Delay_N_Fiss_651_685_split[0])) ; 
}


void Delay_N_394() {
	push_float(&SplitJoin85_Delay_N_Fiss_651_685_join[1], pop_float(&SplitJoin85_Delay_N_Fiss_651_685_split[1])) ; 
}


void Delay_N_395() {
	push_float(&SplitJoin85_Delay_N_Fiss_651_685_join[2], pop_float(&SplitJoin85_Delay_N_Fiss_651_685_split[2])) ; 
}


void Delay_N_396() {
	push_float(&SplitJoin85_Delay_N_Fiss_651_685_join[3], pop_float(&SplitJoin85_Delay_N_Fiss_651_685_split[3])) ; 
}


void Delay_N_397() {
	push_float(&SplitJoin85_Delay_N_Fiss_651_685_join[4], pop_float(&SplitJoin85_Delay_N_Fiss_651_685_split[4])) ; 
}


void Delay_N_398() {
	push_float(&SplitJoin85_Delay_N_Fiss_651_685_join[5], pop_float(&SplitJoin85_Delay_N_Fiss_651_685_split[5])) ; 
}


void Delay_N_399() {
	push_float(&SplitJoin85_Delay_N_Fiss_651_685_join[6], pop_float(&SplitJoin85_Delay_N_Fiss_651_685_split[6])) ; 
}


void Delay_N_400() {
	push_float(&SplitJoin85_Delay_N_Fiss_651_685_join[7], pop_float(&SplitJoin85_Delay_N_Fiss_651_685_split[7])) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_391() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&SplitJoin85_Delay_N_Fiss_651_685_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_229_293_641_676_split[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_392() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_392DUPLICATE_Splitter_401, pop_float(&SplitJoin85_Delay_N_Fiss_651_685_join[__iter_]));
	ENDFOR
}

void FirFilter_403() {
	float sum = 0.0;
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin87_FirFilter_Fiss_652_686_split[0], i) * FirFilter_403_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin87_FirFilter_Fiss_652_686_split[0]) ; 
	push_float(&SplitJoin87_FirFilter_Fiss_652_686_join[0], sum) ; 
 {
	FOR(int, streamItVar13, 0,  < , 7, streamItVar13++) {
		pop_void(&SplitJoin87_FirFilter_Fiss_652_686_split[0]) ; 
	}
	ENDFOR
}
}


void FirFilter_404() {
	float sum = 0.0;
 {
	pop_void(&SplitJoin87_FirFilter_Fiss_652_686_split[1]) ; 
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin87_FirFilter_Fiss_652_686_split[1], i) * FirFilter_404_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin87_FirFilter_Fiss_652_686_split[1]) ; 
	push_float(&SplitJoin87_FirFilter_Fiss_652_686_join[1], sum) ; 
 {
	FOR(int, streamItVar14, 0,  < , 6, streamItVar14++) {
		pop_void(&SplitJoin87_FirFilter_Fiss_652_686_split[1]) ; 
	}
	ENDFOR
}
}


void FirFilter_405() {
	float sum = 0.0;
 {
	FOR(int, streamItVar15, 0,  < , 2, streamItVar15++) {
		pop_void(&SplitJoin87_FirFilter_Fiss_652_686_split[2]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin87_FirFilter_Fiss_652_686_split[2], i) * FirFilter_405_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin87_FirFilter_Fiss_652_686_split[2]) ; 
	push_float(&SplitJoin87_FirFilter_Fiss_652_686_join[2], sum) ; 
 {
	FOR(int, streamItVar16, 0,  < , 5, streamItVar16++) {
		pop_void(&SplitJoin87_FirFilter_Fiss_652_686_split[2]) ; 
	}
	ENDFOR
}
}


void FirFilter_406() {
	float sum = 0.0;
 {
	FOR(int, streamItVar17, 0,  < , 3, streamItVar17++) {
		pop_void(&SplitJoin87_FirFilter_Fiss_652_686_split[3]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin87_FirFilter_Fiss_652_686_split[3], i) * FirFilter_406_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin87_FirFilter_Fiss_652_686_split[3]) ; 
	push_float(&SplitJoin87_FirFilter_Fiss_652_686_join[3], sum) ; 
 {
	FOR(int, streamItVar18, 0,  < , 4, streamItVar18++) {
		pop_void(&SplitJoin87_FirFilter_Fiss_652_686_split[3]) ; 
	}
	ENDFOR
}
}


void FirFilter_407() {
	float sum = 0.0;
 {
	FOR(int, streamItVar19, 0,  < , 4, streamItVar19++) {
		pop_void(&SplitJoin87_FirFilter_Fiss_652_686_split[4]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin87_FirFilter_Fiss_652_686_split[4], i) * FirFilter_407_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin87_FirFilter_Fiss_652_686_split[4]) ; 
	push_float(&SplitJoin87_FirFilter_Fiss_652_686_join[4], sum) ; 
 {
	FOR(int, streamItVar20, 0,  < , 3, streamItVar20++) {
		pop_void(&SplitJoin87_FirFilter_Fiss_652_686_split[4]) ; 
	}
	ENDFOR
}
}


void FirFilter_408() {
	float sum = 0.0;
 {
	FOR(int, streamItVar21, 0,  < , 5, streamItVar21++) {
		pop_void(&SplitJoin87_FirFilter_Fiss_652_686_split[5]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin87_FirFilter_Fiss_652_686_split[5], i) * FirFilter_408_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin87_FirFilter_Fiss_652_686_split[5]) ; 
	push_float(&SplitJoin87_FirFilter_Fiss_652_686_join[5], sum) ; 
 {
	FOR(int, streamItVar22, 0,  < , 2, streamItVar22++) {
		pop_void(&SplitJoin87_FirFilter_Fiss_652_686_split[5]) ; 
	}
	ENDFOR
}
}


void FirFilter_409() {
	float sum = 0.0;
 {
	FOR(int, streamItVar23, 0,  < , 6, streamItVar23++) {
		pop_void(&SplitJoin87_FirFilter_Fiss_652_686_split[6]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin87_FirFilter_Fiss_652_686_split[6], i) * FirFilter_409_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin87_FirFilter_Fiss_652_686_split[6]) ; 
	push_float(&SplitJoin87_FirFilter_Fiss_652_686_join[6], sum) ; 
 {
	pop_void(&SplitJoin87_FirFilter_Fiss_652_686_split[6]) ; 
}
}


void FirFilter_410() {
	float sum = 0.0;
 {
	FOR(int, streamItVar24, 0,  < , 7, streamItVar24++) {
		pop_void(&SplitJoin87_FirFilter_Fiss_652_686_split[7]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin87_FirFilter_Fiss_652_686_split[7], i) * FirFilter_410_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin87_FirFilter_Fiss_652_686_split[7]) ; 
	push_float(&SplitJoin87_FirFilter_Fiss_652_686_join[7], sum) ; 
}


void DUPLICATE_Splitter_401() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_392DUPLICATE_Splitter_401);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin87_FirFilter_Fiss_652_686_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_402() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_402DownSamp_250, pop_float(&SplitJoin87_FirFilter_Fiss_652_686_join[__iter_]));
	ENDFOR
}

void DownSamp_250() {
	push_float(&DownSamp_250UpSamp_251, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_402DownSamp_250)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_402DownSamp_250) ; 
	}
	ENDFOR
}


void UpSamp_251() {
	push_float(&UpSamp_251WEIGHTED_ROUND_ROBIN_Splitter_411, pop_float(&DownSamp_250UpSamp_251)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		push_float(&UpSamp_251WEIGHTED_ROUND_ROBIN_Splitter_411, 0.0) ; 
	}
	ENDFOR
}


void Delay_N_413() {
	push_float(&SplitJoin89_Delay_N_Fiss_653_687_join[0], pop_float(&SplitJoin89_Delay_N_Fiss_653_687_split[0])) ; 
}


void Delay_N_414() {
	push_float(&SplitJoin89_Delay_N_Fiss_653_687_join[1], pop_float(&SplitJoin89_Delay_N_Fiss_653_687_split[1])) ; 
}


void Delay_N_415() {
	push_float(&SplitJoin89_Delay_N_Fiss_653_687_join[2], pop_float(&SplitJoin89_Delay_N_Fiss_653_687_split[2])) ; 
}


void Delay_N_416() {
	push_float(&SplitJoin89_Delay_N_Fiss_653_687_join[3], pop_float(&SplitJoin89_Delay_N_Fiss_653_687_split[3])) ; 
}


void Delay_N_417() {
	push_float(&SplitJoin89_Delay_N_Fiss_653_687_join[4], pop_float(&SplitJoin89_Delay_N_Fiss_653_687_split[4])) ; 
}


void Delay_N_418() {
	push_float(&SplitJoin89_Delay_N_Fiss_653_687_join[5], pop_float(&SplitJoin89_Delay_N_Fiss_653_687_split[5])) ; 
}


void Delay_N_419() {
	push_float(&SplitJoin89_Delay_N_Fiss_653_687_join[6], pop_float(&SplitJoin89_Delay_N_Fiss_653_687_split[6])) ; 
}


void Delay_N_420() {
	push_float(&SplitJoin89_Delay_N_Fiss_653_687_join[7], pop_float(&SplitJoin89_Delay_N_Fiss_653_687_split[7])) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_411() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&SplitJoin89_Delay_N_Fiss_653_687_split[__iter_], pop_float(&UpSamp_251WEIGHTED_ROUND_ROBIN_Splitter_411));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_412() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_412DUPLICATE_Splitter_421, pop_float(&SplitJoin89_Delay_N_Fiss_653_687_join[__iter_]));
	ENDFOR
}

void FirFilter_423() {
	float sum = 0.0;
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin91_FirFilter_Fiss_654_688_split[0], i) * FirFilter_423_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin91_FirFilter_Fiss_654_688_split[0]) ; 
	push_float(&SplitJoin91_FirFilter_Fiss_654_688_join[0], sum) ; 
 {
	FOR(int, streamItVar1, 0,  < , 7, streamItVar1++) {
		pop_void(&SplitJoin91_FirFilter_Fiss_654_688_split[0]) ; 
	}
	ENDFOR
}
}


void FirFilter_424() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	pop_void(&SplitJoin91_FirFilter_Fiss_654_688_split[1]) ; 
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin91_FirFilter_Fiss_654_688_split[1], i) * FirFilter_424_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin91_FirFilter_Fiss_654_688_split[1]) ; 
	push_float(&SplitJoin91_FirFilter_Fiss_654_688_join[1], sum) ; 
 {
	FOR(int, streamItVar2, 0,  < , 6, streamItVar2++) {
		pop_void(&SplitJoin91_FirFilter_Fiss_654_688_split[1]) ; 
	}
	ENDFOR
}
}


void FirFilter_425() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar3, 0,  < , 2, streamItVar3++) {
		pop_void(&SplitJoin91_FirFilter_Fiss_654_688_split[2]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin91_FirFilter_Fiss_654_688_split[2], i) * FirFilter_425_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin91_FirFilter_Fiss_654_688_split[2]) ; 
	push_float(&SplitJoin91_FirFilter_Fiss_654_688_join[2], sum) ; 
 {
	FOR(int, streamItVar4, 0,  < , 5, streamItVar4++) {
		pop_void(&SplitJoin91_FirFilter_Fiss_654_688_split[2]) ; 
	}
	ENDFOR
}
}


void FirFilter_426() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar5, 0,  < , 3, streamItVar5++) {
		pop_void(&SplitJoin91_FirFilter_Fiss_654_688_split[3]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin91_FirFilter_Fiss_654_688_split[3], i) * FirFilter_426_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin91_FirFilter_Fiss_654_688_split[3]) ; 
	push_float(&SplitJoin91_FirFilter_Fiss_654_688_join[3], sum) ; 
 {
	FOR(int, streamItVar6, 0,  < , 4, streamItVar6++) {
		pop_void(&SplitJoin91_FirFilter_Fiss_654_688_split[3]) ; 
	}
	ENDFOR
}
}


void FirFilter_427() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar7, 0,  < , 4, streamItVar7++) {
		pop_void(&SplitJoin91_FirFilter_Fiss_654_688_split[4]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin91_FirFilter_Fiss_654_688_split[4], i) * FirFilter_427_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin91_FirFilter_Fiss_654_688_split[4]) ; 
	push_float(&SplitJoin91_FirFilter_Fiss_654_688_join[4], sum) ; 
 {
	FOR(int, streamItVar8, 0,  < , 3, streamItVar8++) {
		pop_void(&SplitJoin91_FirFilter_Fiss_654_688_split[4]) ; 
	}
	ENDFOR
}
}


void FirFilter_428() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar9, 0,  < , 5, streamItVar9++) {
		pop_void(&SplitJoin91_FirFilter_Fiss_654_688_split[5]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin91_FirFilter_Fiss_654_688_split[5], i) * FirFilter_428_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin91_FirFilter_Fiss_654_688_split[5]) ; 
	push_float(&SplitJoin91_FirFilter_Fiss_654_688_join[5], sum) ; 
 {
	FOR(int, streamItVar10, 0,  < , 2, streamItVar10++) {
		pop_void(&SplitJoin91_FirFilter_Fiss_654_688_split[5]) ; 
	}
	ENDFOR
}
}


void FirFilter_429() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar11, 0,  < , 6, streamItVar11++) {
		pop_void(&SplitJoin91_FirFilter_Fiss_654_688_split[6]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin91_FirFilter_Fiss_654_688_split[6], i) * FirFilter_429_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin91_FirFilter_Fiss_654_688_split[6]) ; 
	push_float(&SplitJoin91_FirFilter_Fiss_654_688_join[6], sum) ; 
 {
	pop_void(&SplitJoin91_FirFilter_Fiss_654_688_split[6]) ; 
}
}


void FirFilter_430() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar12, 0,  < , 7, streamItVar12++) {
		pop_void(&SplitJoin91_FirFilter_Fiss_654_688_split[7]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin91_FirFilter_Fiss_654_688_split[7], i) * FirFilter_430_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin91_FirFilter_Fiss_654_688_split[7]) ; 
	push_float(&SplitJoin91_FirFilter_Fiss_654_688_join[7], sum) ; 
}


void DUPLICATE_Splitter_421() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_412DUPLICATE_Splitter_421);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin91_FirFilter_Fiss_654_688_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_422() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_229_293_641_676_join[2], pop_float(&SplitJoin91_FirFilter_Fiss_654_688_join[__iter_]));
	ENDFOR
}

void Delay_N_433() {
	push_float(&SplitJoin122_Delay_N_Fiss_655_689_join[0], pop_float(&SplitJoin122_Delay_N_Fiss_655_689_split[0])) ; 
}


void Delay_N_434() {
	push_float(&SplitJoin122_Delay_N_Fiss_655_689_join[1], pop_float(&SplitJoin122_Delay_N_Fiss_655_689_split[1])) ; 
}


void Delay_N_435() {
	push_float(&SplitJoin122_Delay_N_Fiss_655_689_join[2], pop_float(&SplitJoin122_Delay_N_Fiss_655_689_split[2])) ; 
}


void Delay_N_436() {
	push_float(&SplitJoin122_Delay_N_Fiss_655_689_join[3], pop_float(&SplitJoin122_Delay_N_Fiss_655_689_split[3])) ; 
}


void Delay_N_437() {
	push_float(&SplitJoin122_Delay_N_Fiss_655_689_join[4], pop_float(&SplitJoin122_Delay_N_Fiss_655_689_split[4])) ; 
}


void Delay_N_438() {
	push_float(&SplitJoin122_Delay_N_Fiss_655_689_join[5], pop_float(&SplitJoin122_Delay_N_Fiss_655_689_split[5])) ; 
}


void Delay_N_439() {
	push_float(&SplitJoin122_Delay_N_Fiss_655_689_join[6], pop_float(&SplitJoin122_Delay_N_Fiss_655_689_split[6])) ; 
}


void Delay_N_440() {
	push_float(&SplitJoin122_Delay_N_Fiss_655_689_join[7], pop_float(&SplitJoin122_Delay_N_Fiss_655_689_split[7])) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_431() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&SplitJoin122_Delay_N_Fiss_655_689_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_229_293_641_676_split[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_432() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_432DUPLICATE_Splitter_441, pop_float(&SplitJoin122_Delay_N_Fiss_655_689_join[__iter_]));
	ENDFOR
}

void FirFilter_443() {
	float sum = 0.0;
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin124_FirFilter_Fiss_656_690_split[0], i) * FirFilter_443_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin124_FirFilter_Fiss_656_690_split[0]) ; 
	push_float(&SplitJoin124_FirFilter_Fiss_656_690_join[0], sum) ; 
 {
	FOR(int, streamItVar181, 0,  < , 7, streamItVar181++) {
		pop_void(&SplitJoin124_FirFilter_Fiss_656_690_split[0]) ; 
	}
	ENDFOR
}
}


void FirFilter_444() {
	float sum = 0.0;
 {
	pop_void(&SplitJoin124_FirFilter_Fiss_656_690_split[1]) ; 
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin124_FirFilter_Fiss_656_690_split[1], i) * FirFilter_444_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin124_FirFilter_Fiss_656_690_split[1]) ; 
	push_float(&SplitJoin124_FirFilter_Fiss_656_690_join[1], sum) ; 
 {
	FOR(int, streamItVar182, 0,  < , 6, streamItVar182++) {
		pop_void(&SplitJoin124_FirFilter_Fiss_656_690_split[1]) ; 
	}
	ENDFOR
}
}


void FirFilter_445() {
	float sum = 0.0;
 {
	FOR(int, streamItVar183, 0,  < , 2, streamItVar183++) {
		pop_void(&SplitJoin124_FirFilter_Fiss_656_690_split[2]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin124_FirFilter_Fiss_656_690_split[2], i) * FirFilter_445_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin124_FirFilter_Fiss_656_690_split[2]) ; 
	push_float(&SplitJoin124_FirFilter_Fiss_656_690_join[2], sum) ; 
 {
	FOR(int, streamItVar184, 0,  < , 5, streamItVar184++) {
		pop_void(&SplitJoin124_FirFilter_Fiss_656_690_split[2]) ; 
	}
	ENDFOR
}
}


void FirFilter_446() {
	float sum = 0.0;
 {
	FOR(int, streamItVar185, 0,  < , 3, streamItVar185++) {
		pop_void(&SplitJoin124_FirFilter_Fiss_656_690_split[3]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin124_FirFilter_Fiss_656_690_split[3], i) * FirFilter_446_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin124_FirFilter_Fiss_656_690_split[3]) ; 
	push_float(&SplitJoin124_FirFilter_Fiss_656_690_join[3], sum) ; 
 {
	FOR(int, streamItVar186, 0,  < , 4, streamItVar186++) {
		pop_void(&SplitJoin124_FirFilter_Fiss_656_690_split[3]) ; 
	}
	ENDFOR
}
}


void FirFilter_447() {
	float sum = 0.0;
 {
	FOR(int, streamItVar187, 0,  < , 4, streamItVar187++) {
		pop_void(&SplitJoin124_FirFilter_Fiss_656_690_split[4]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin124_FirFilter_Fiss_656_690_split[4], i) * FirFilter_447_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin124_FirFilter_Fiss_656_690_split[4]) ; 
	push_float(&SplitJoin124_FirFilter_Fiss_656_690_join[4], sum) ; 
 {
	FOR(int, streamItVar188, 0,  < , 3, streamItVar188++) {
		pop_void(&SplitJoin124_FirFilter_Fiss_656_690_split[4]) ; 
	}
	ENDFOR
}
}


void FirFilter_448() {
	float sum = 0.0;
 {
	FOR(int, streamItVar189, 0,  < , 5, streamItVar189++) {
		pop_void(&SplitJoin124_FirFilter_Fiss_656_690_split[5]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin124_FirFilter_Fiss_656_690_split[5], i) * FirFilter_448_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin124_FirFilter_Fiss_656_690_split[5]) ; 
	push_float(&SplitJoin124_FirFilter_Fiss_656_690_join[5], sum) ; 
 {
	FOR(int, streamItVar190, 0,  < , 2, streamItVar190++) {
		pop_void(&SplitJoin124_FirFilter_Fiss_656_690_split[5]) ; 
	}
	ENDFOR
}
}


void FirFilter_449() {
	float sum = 0.0;
 {
	FOR(int, streamItVar191, 0,  < , 6, streamItVar191++) {
		pop_void(&SplitJoin124_FirFilter_Fiss_656_690_split[6]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin124_FirFilter_Fiss_656_690_split[6], i) * FirFilter_449_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin124_FirFilter_Fiss_656_690_split[6]) ; 
	push_float(&SplitJoin124_FirFilter_Fiss_656_690_join[6], sum) ; 
 {
	pop_void(&SplitJoin124_FirFilter_Fiss_656_690_split[6]) ; 
}
}


void FirFilter_450() {
	float sum = 0.0;
 {
	FOR(int, streamItVar192, 0,  < , 7, streamItVar192++) {
		pop_void(&SplitJoin124_FirFilter_Fiss_656_690_split[7]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin124_FirFilter_Fiss_656_690_split[7], i) * FirFilter_450_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin124_FirFilter_Fiss_656_690_split[7]) ; 
	push_float(&SplitJoin124_FirFilter_Fiss_656_690_join[7], sum) ; 
}


void DUPLICATE_Splitter_441() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_432DUPLICATE_Splitter_441);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin124_FirFilter_Fiss_656_690_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_442() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_442DownSamp_257, pop_float(&SplitJoin124_FirFilter_Fiss_656_690_join[__iter_]));
	ENDFOR
}

void DownSamp_257() {
	push_float(&DownSamp_257UpSamp_258, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_442DownSamp_257)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_442DownSamp_257) ; 
	}
	ENDFOR
}


void UpSamp_258() {
	push_float(&UpSamp_258WEIGHTED_ROUND_ROBIN_Splitter_451, pop_float(&DownSamp_257UpSamp_258)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		push_float(&UpSamp_258WEIGHTED_ROUND_ROBIN_Splitter_451, 0.0) ; 
	}
	ENDFOR
}


void Delay_N_453() {
	push_float(&SplitJoin126_Delay_N_Fiss_657_691_join[0], pop_float(&SplitJoin126_Delay_N_Fiss_657_691_split[0])) ; 
}


void Delay_N_454() {
	push_float(&SplitJoin126_Delay_N_Fiss_657_691_join[1], pop_float(&SplitJoin126_Delay_N_Fiss_657_691_split[1])) ; 
}


void Delay_N_455() {
	push_float(&SplitJoin126_Delay_N_Fiss_657_691_join[2], pop_float(&SplitJoin126_Delay_N_Fiss_657_691_split[2])) ; 
}


void Delay_N_456() {
	push_float(&SplitJoin126_Delay_N_Fiss_657_691_join[3], pop_float(&SplitJoin126_Delay_N_Fiss_657_691_split[3])) ; 
}


void Delay_N_457() {
	push_float(&SplitJoin126_Delay_N_Fiss_657_691_join[4], pop_float(&SplitJoin126_Delay_N_Fiss_657_691_split[4])) ; 
}


void Delay_N_458() {
	push_float(&SplitJoin126_Delay_N_Fiss_657_691_join[5], pop_float(&SplitJoin126_Delay_N_Fiss_657_691_split[5])) ; 
}


void Delay_N_459() {
	push_float(&SplitJoin126_Delay_N_Fiss_657_691_join[6], pop_float(&SplitJoin126_Delay_N_Fiss_657_691_split[6])) ; 
}


void Delay_N_460() {
	push_float(&SplitJoin126_Delay_N_Fiss_657_691_join[7], pop_float(&SplitJoin126_Delay_N_Fiss_657_691_split[7])) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_451() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&SplitJoin126_Delay_N_Fiss_657_691_split[__iter_], pop_float(&UpSamp_258WEIGHTED_ROUND_ROBIN_Splitter_451));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_452() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_452DUPLICATE_Splitter_461, pop_float(&SplitJoin126_Delay_N_Fiss_657_691_join[__iter_]));
	ENDFOR
}

void FirFilter_463() {
	float sum = 0.0;
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin128_FirFilter_Fiss_658_692_split[0], i) * FirFilter_463_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin128_FirFilter_Fiss_658_692_split[0]) ; 
	push_float(&SplitJoin128_FirFilter_Fiss_658_692_join[0], sum) ; 
 {
	FOR(int, streamItVar169, 0,  < , 7, streamItVar169++) {
		pop_void(&SplitJoin128_FirFilter_Fiss_658_692_split[0]) ; 
	}
	ENDFOR
}
}


void FirFilter_464() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	pop_void(&SplitJoin128_FirFilter_Fiss_658_692_split[1]) ; 
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin128_FirFilter_Fiss_658_692_split[1], i) * FirFilter_464_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin128_FirFilter_Fiss_658_692_split[1]) ; 
	push_float(&SplitJoin128_FirFilter_Fiss_658_692_join[1], sum) ; 
 {
	FOR(int, streamItVar170, 0,  < , 6, streamItVar170++) {
		pop_void(&SplitJoin128_FirFilter_Fiss_658_692_split[1]) ; 
	}
	ENDFOR
}
}


void FirFilter_465() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar171, 0,  < , 2, streamItVar171++) {
		pop_void(&SplitJoin128_FirFilter_Fiss_658_692_split[2]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin128_FirFilter_Fiss_658_692_split[2], i) * FirFilter_465_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin128_FirFilter_Fiss_658_692_split[2]) ; 
	push_float(&SplitJoin128_FirFilter_Fiss_658_692_join[2], sum) ; 
 {
	FOR(int, streamItVar172, 0,  < , 5, streamItVar172++) {
		pop_void(&SplitJoin128_FirFilter_Fiss_658_692_split[2]) ; 
	}
	ENDFOR
}
}


void FirFilter_466() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar173, 0,  < , 3, streamItVar173++) {
		pop_void(&SplitJoin128_FirFilter_Fiss_658_692_split[3]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin128_FirFilter_Fiss_658_692_split[3], i) * FirFilter_466_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin128_FirFilter_Fiss_658_692_split[3]) ; 
	push_float(&SplitJoin128_FirFilter_Fiss_658_692_join[3], sum) ; 
 {
	FOR(int, streamItVar174, 0,  < , 4, streamItVar174++) {
		pop_void(&SplitJoin128_FirFilter_Fiss_658_692_split[3]) ; 
	}
	ENDFOR
}
}


void FirFilter_467() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar175, 0,  < , 4, streamItVar175++) {
		pop_void(&SplitJoin128_FirFilter_Fiss_658_692_split[4]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin128_FirFilter_Fiss_658_692_split[4], i) * FirFilter_467_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin128_FirFilter_Fiss_658_692_split[4]) ; 
	push_float(&SplitJoin128_FirFilter_Fiss_658_692_join[4], sum) ; 
 {
	FOR(int, streamItVar176, 0,  < , 3, streamItVar176++) {
		pop_void(&SplitJoin128_FirFilter_Fiss_658_692_split[4]) ; 
	}
	ENDFOR
}
}


void FirFilter_468() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar177, 0,  < , 5, streamItVar177++) {
		pop_void(&SplitJoin128_FirFilter_Fiss_658_692_split[5]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin128_FirFilter_Fiss_658_692_split[5], i) * FirFilter_468_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin128_FirFilter_Fiss_658_692_split[5]) ; 
	push_float(&SplitJoin128_FirFilter_Fiss_658_692_join[5], sum) ; 
 {
	FOR(int, streamItVar178, 0,  < , 2, streamItVar178++) {
		pop_void(&SplitJoin128_FirFilter_Fiss_658_692_split[5]) ; 
	}
	ENDFOR
}
}


void FirFilter_469() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar179, 0,  < , 6, streamItVar179++) {
		pop_void(&SplitJoin128_FirFilter_Fiss_658_692_split[6]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin128_FirFilter_Fiss_658_692_split[6], i) * FirFilter_469_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin128_FirFilter_Fiss_658_692_split[6]) ; 
	push_float(&SplitJoin128_FirFilter_Fiss_658_692_join[6], sum) ; 
 {
	pop_void(&SplitJoin128_FirFilter_Fiss_658_692_split[6]) ; 
}
}


void FirFilter_470() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar180, 0,  < , 7, streamItVar180++) {
		pop_void(&SplitJoin128_FirFilter_Fiss_658_692_split[7]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin128_FirFilter_Fiss_658_692_split[7], i) * FirFilter_470_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin128_FirFilter_Fiss_658_692_split[7]) ; 
	push_float(&SplitJoin128_FirFilter_Fiss_658_692_join[7], sum) ; 
}


void DUPLICATE_Splitter_461() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_452DUPLICATE_Splitter_461);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin128_FirFilter_Fiss_658_692_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_462() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_229_293_641_676_join[3], pop_float(&SplitJoin128_FirFilter_Fiss_658_692_join[__iter_]));
	ENDFOR
}

void Delay_N_473() {
	push_float(&SplitJoin159_Delay_N_Fiss_659_693_join[0], pop_float(&SplitJoin159_Delay_N_Fiss_659_693_split[0])) ; 
}


void Delay_N_474() {
	push_float(&SplitJoin159_Delay_N_Fiss_659_693_join[1], pop_float(&SplitJoin159_Delay_N_Fiss_659_693_split[1])) ; 
}


void Delay_N_475() {
	push_float(&SplitJoin159_Delay_N_Fiss_659_693_join[2], pop_float(&SplitJoin159_Delay_N_Fiss_659_693_split[2])) ; 
}


void Delay_N_476() {
	push_float(&SplitJoin159_Delay_N_Fiss_659_693_join[3], pop_float(&SplitJoin159_Delay_N_Fiss_659_693_split[3])) ; 
}


void Delay_N_477() {
	push_float(&SplitJoin159_Delay_N_Fiss_659_693_join[4], pop_float(&SplitJoin159_Delay_N_Fiss_659_693_split[4])) ; 
}


void Delay_N_478() {
	push_float(&SplitJoin159_Delay_N_Fiss_659_693_join[5], pop_float(&SplitJoin159_Delay_N_Fiss_659_693_split[5])) ; 
}


void Delay_N_479() {
	push_float(&SplitJoin159_Delay_N_Fiss_659_693_join[6], pop_float(&SplitJoin159_Delay_N_Fiss_659_693_split[6])) ; 
}


void Delay_N_480() {
	push_float(&SplitJoin159_Delay_N_Fiss_659_693_join[7], pop_float(&SplitJoin159_Delay_N_Fiss_659_693_split[7])) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_471() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&SplitJoin159_Delay_N_Fiss_659_693_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_229_293_641_676_split[4]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_472() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_472DUPLICATE_Splitter_481, pop_float(&SplitJoin159_Delay_N_Fiss_659_693_join[__iter_]));
	ENDFOR
}

void FirFilter_483() {
	float sum = 0.0;
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin161_FirFilter_Fiss_660_694_split[0], i) * FirFilter_483_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin161_FirFilter_Fiss_660_694_split[0]) ; 
	push_float(&SplitJoin161_FirFilter_Fiss_660_694_join[0], sum) ; 
 {
	FOR(int, streamItVar157, 0,  < , 7, streamItVar157++) {
		pop_void(&SplitJoin161_FirFilter_Fiss_660_694_split[0]) ; 
	}
	ENDFOR
}
}


void FirFilter_484() {
	float sum = 0.0;
 {
	pop_void(&SplitJoin161_FirFilter_Fiss_660_694_split[1]) ; 
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin161_FirFilter_Fiss_660_694_split[1], i) * FirFilter_484_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin161_FirFilter_Fiss_660_694_split[1]) ; 
	push_float(&SplitJoin161_FirFilter_Fiss_660_694_join[1], sum) ; 
 {
	FOR(int, streamItVar158, 0,  < , 6, streamItVar158++) {
		pop_void(&SplitJoin161_FirFilter_Fiss_660_694_split[1]) ; 
	}
	ENDFOR
}
}


void FirFilter_485() {
	float sum = 0.0;
 {
	FOR(int, streamItVar159, 0,  < , 2, streamItVar159++) {
		pop_void(&SplitJoin161_FirFilter_Fiss_660_694_split[2]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin161_FirFilter_Fiss_660_694_split[2], i) * FirFilter_485_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin161_FirFilter_Fiss_660_694_split[2]) ; 
	push_float(&SplitJoin161_FirFilter_Fiss_660_694_join[2], sum) ; 
 {
	FOR(int, streamItVar160, 0,  < , 5, streamItVar160++) {
		pop_void(&SplitJoin161_FirFilter_Fiss_660_694_split[2]) ; 
	}
	ENDFOR
}
}


void FirFilter_486() {
	float sum = 0.0;
 {
	FOR(int, streamItVar161, 0,  < , 3, streamItVar161++) {
		pop_void(&SplitJoin161_FirFilter_Fiss_660_694_split[3]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin161_FirFilter_Fiss_660_694_split[3], i) * FirFilter_486_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin161_FirFilter_Fiss_660_694_split[3]) ; 
	push_float(&SplitJoin161_FirFilter_Fiss_660_694_join[3], sum) ; 
 {
	FOR(int, streamItVar162, 0,  < , 4, streamItVar162++) {
		pop_void(&SplitJoin161_FirFilter_Fiss_660_694_split[3]) ; 
	}
	ENDFOR
}
}


void FirFilter_487() {
	float sum = 0.0;
 {
	FOR(int, streamItVar163, 0,  < , 4, streamItVar163++) {
		pop_void(&SplitJoin161_FirFilter_Fiss_660_694_split[4]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin161_FirFilter_Fiss_660_694_split[4], i) * FirFilter_487_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin161_FirFilter_Fiss_660_694_split[4]) ; 
	push_float(&SplitJoin161_FirFilter_Fiss_660_694_join[4], sum) ; 
 {
	FOR(int, streamItVar164, 0,  < , 3, streamItVar164++) {
		pop_void(&SplitJoin161_FirFilter_Fiss_660_694_split[4]) ; 
	}
	ENDFOR
}
}


void FirFilter_488() {
	float sum = 0.0;
 {
	FOR(int, streamItVar165, 0,  < , 5, streamItVar165++) {
		pop_void(&SplitJoin161_FirFilter_Fiss_660_694_split[5]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin161_FirFilter_Fiss_660_694_split[5], i) * FirFilter_488_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin161_FirFilter_Fiss_660_694_split[5]) ; 
	push_float(&SplitJoin161_FirFilter_Fiss_660_694_join[5], sum) ; 
 {
	FOR(int, streamItVar166, 0,  < , 2, streamItVar166++) {
		pop_void(&SplitJoin161_FirFilter_Fiss_660_694_split[5]) ; 
	}
	ENDFOR
}
}


void FirFilter_489() {
	float sum = 0.0;
 {
	FOR(int, streamItVar167, 0,  < , 6, streamItVar167++) {
		pop_void(&SplitJoin161_FirFilter_Fiss_660_694_split[6]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin161_FirFilter_Fiss_660_694_split[6], i) * FirFilter_489_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin161_FirFilter_Fiss_660_694_split[6]) ; 
	push_float(&SplitJoin161_FirFilter_Fiss_660_694_join[6], sum) ; 
 {
	pop_void(&SplitJoin161_FirFilter_Fiss_660_694_split[6]) ; 
}
}


void FirFilter_490() {
	float sum = 0.0;
 {
	FOR(int, streamItVar168, 0,  < , 7, streamItVar168++) {
		pop_void(&SplitJoin161_FirFilter_Fiss_660_694_split[7]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin161_FirFilter_Fiss_660_694_split[7], i) * FirFilter_490_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin161_FirFilter_Fiss_660_694_split[7]) ; 
	push_float(&SplitJoin161_FirFilter_Fiss_660_694_join[7], sum) ; 
}


void DUPLICATE_Splitter_481() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_472DUPLICATE_Splitter_481);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin161_FirFilter_Fiss_660_694_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_482() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_482DownSamp_264, pop_float(&SplitJoin161_FirFilter_Fiss_660_694_join[__iter_]));
	ENDFOR
}

void DownSamp_264() {
	push_float(&DownSamp_264UpSamp_265, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_482DownSamp_264)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_482DownSamp_264) ; 
	}
	ENDFOR
}


void UpSamp_265() {
	push_float(&UpSamp_265WEIGHTED_ROUND_ROBIN_Splitter_491, pop_float(&DownSamp_264UpSamp_265)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		push_float(&UpSamp_265WEIGHTED_ROUND_ROBIN_Splitter_491, 0.0) ; 
	}
	ENDFOR
}


void Delay_N_493() {
	push_float(&SplitJoin163_Delay_N_Fiss_661_695_join[0], pop_float(&SplitJoin163_Delay_N_Fiss_661_695_split[0])) ; 
}


void Delay_N_494() {
	push_float(&SplitJoin163_Delay_N_Fiss_661_695_join[1], pop_float(&SplitJoin163_Delay_N_Fiss_661_695_split[1])) ; 
}


void Delay_N_495() {
	push_float(&SplitJoin163_Delay_N_Fiss_661_695_join[2], pop_float(&SplitJoin163_Delay_N_Fiss_661_695_split[2])) ; 
}


void Delay_N_496() {
	push_float(&SplitJoin163_Delay_N_Fiss_661_695_join[3], pop_float(&SplitJoin163_Delay_N_Fiss_661_695_split[3])) ; 
}


void Delay_N_497() {
	push_float(&SplitJoin163_Delay_N_Fiss_661_695_join[4], pop_float(&SplitJoin163_Delay_N_Fiss_661_695_split[4])) ; 
}


void Delay_N_498() {
	push_float(&SplitJoin163_Delay_N_Fiss_661_695_join[5], pop_float(&SplitJoin163_Delay_N_Fiss_661_695_split[5])) ; 
}


void Delay_N_499() {
	push_float(&SplitJoin163_Delay_N_Fiss_661_695_join[6], pop_float(&SplitJoin163_Delay_N_Fiss_661_695_split[6])) ; 
}


void Delay_N_500() {
	push_float(&SplitJoin163_Delay_N_Fiss_661_695_join[7], pop_float(&SplitJoin163_Delay_N_Fiss_661_695_split[7])) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_491() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&SplitJoin163_Delay_N_Fiss_661_695_split[__iter_], pop_float(&UpSamp_265WEIGHTED_ROUND_ROBIN_Splitter_491));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_492() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_492DUPLICATE_Splitter_501, pop_float(&SplitJoin163_Delay_N_Fiss_661_695_join[__iter_]));
	ENDFOR
}

void FirFilter_503() {
	float sum = 0.0;
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin165_FirFilter_Fiss_662_696_split[0], i) * FirFilter_503_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin165_FirFilter_Fiss_662_696_split[0]) ; 
	push_float(&SplitJoin165_FirFilter_Fiss_662_696_join[0], sum) ; 
 {
	FOR(int, streamItVar145, 0,  < , 7, streamItVar145++) {
		pop_void(&SplitJoin165_FirFilter_Fiss_662_696_split[0]) ; 
	}
	ENDFOR
}
}


void FirFilter_504() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	pop_void(&SplitJoin165_FirFilter_Fiss_662_696_split[1]) ; 
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin165_FirFilter_Fiss_662_696_split[1], i) * FirFilter_504_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin165_FirFilter_Fiss_662_696_split[1]) ; 
	push_float(&SplitJoin165_FirFilter_Fiss_662_696_join[1], sum) ; 
 {
	FOR(int, streamItVar146, 0,  < , 6, streamItVar146++) {
		pop_void(&SplitJoin165_FirFilter_Fiss_662_696_split[1]) ; 
	}
	ENDFOR
}
}


void FirFilter_505() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar147, 0,  < , 2, streamItVar147++) {
		pop_void(&SplitJoin165_FirFilter_Fiss_662_696_split[2]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin165_FirFilter_Fiss_662_696_split[2], i) * FirFilter_505_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin165_FirFilter_Fiss_662_696_split[2]) ; 
	push_float(&SplitJoin165_FirFilter_Fiss_662_696_join[2], sum) ; 
 {
	FOR(int, streamItVar148, 0,  < , 5, streamItVar148++) {
		pop_void(&SplitJoin165_FirFilter_Fiss_662_696_split[2]) ; 
	}
	ENDFOR
}
}


void FirFilter_506() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar149, 0,  < , 3, streamItVar149++) {
		pop_void(&SplitJoin165_FirFilter_Fiss_662_696_split[3]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin165_FirFilter_Fiss_662_696_split[3], i) * FirFilter_506_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin165_FirFilter_Fiss_662_696_split[3]) ; 
	push_float(&SplitJoin165_FirFilter_Fiss_662_696_join[3], sum) ; 
 {
	FOR(int, streamItVar150, 0,  < , 4, streamItVar150++) {
		pop_void(&SplitJoin165_FirFilter_Fiss_662_696_split[3]) ; 
	}
	ENDFOR
}
}


void FirFilter_507() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar151, 0,  < , 4, streamItVar151++) {
		pop_void(&SplitJoin165_FirFilter_Fiss_662_696_split[4]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin165_FirFilter_Fiss_662_696_split[4], i) * FirFilter_507_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin165_FirFilter_Fiss_662_696_split[4]) ; 
	push_float(&SplitJoin165_FirFilter_Fiss_662_696_join[4], sum) ; 
 {
	FOR(int, streamItVar152, 0,  < , 3, streamItVar152++) {
		pop_void(&SplitJoin165_FirFilter_Fiss_662_696_split[4]) ; 
	}
	ENDFOR
}
}


void FirFilter_508() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar153, 0,  < , 5, streamItVar153++) {
		pop_void(&SplitJoin165_FirFilter_Fiss_662_696_split[5]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin165_FirFilter_Fiss_662_696_split[5], i) * FirFilter_508_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin165_FirFilter_Fiss_662_696_split[5]) ; 
	push_float(&SplitJoin165_FirFilter_Fiss_662_696_join[5], sum) ; 
 {
	FOR(int, streamItVar154, 0,  < , 2, streamItVar154++) {
		pop_void(&SplitJoin165_FirFilter_Fiss_662_696_split[5]) ; 
	}
	ENDFOR
}
}


void FirFilter_509() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar155, 0,  < , 6, streamItVar155++) {
		pop_void(&SplitJoin165_FirFilter_Fiss_662_696_split[6]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin165_FirFilter_Fiss_662_696_split[6], i) * FirFilter_509_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin165_FirFilter_Fiss_662_696_split[6]) ; 
	push_float(&SplitJoin165_FirFilter_Fiss_662_696_join[6], sum) ; 
 {
	pop_void(&SplitJoin165_FirFilter_Fiss_662_696_split[6]) ; 
}
}


void FirFilter_510() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar156, 0,  < , 7, streamItVar156++) {
		pop_void(&SplitJoin165_FirFilter_Fiss_662_696_split[7]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin165_FirFilter_Fiss_662_696_split[7], i) * FirFilter_510_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin165_FirFilter_Fiss_662_696_split[7]) ; 
	push_float(&SplitJoin165_FirFilter_Fiss_662_696_join[7], sum) ; 
}


void DUPLICATE_Splitter_501() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_492DUPLICATE_Splitter_501);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin165_FirFilter_Fiss_662_696_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_502() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_229_293_641_676_join[4], pop_float(&SplitJoin165_FirFilter_Fiss_662_696_join[__iter_]));
	ENDFOR
}

void Delay_N_513() {
	push_float(&SplitJoin196_Delay_N_Fiss_663_697_join[0], pop_float(&SplitJoin196_Delay_N_Fiss_663_697_split[0])) ; 
}


void Delay_N_514() {
	push_float(&SplitJoin196_Delay_N_Fiss_663_697_join[1], pop_float(&SplitJoin196_Delay_N_Fiss_663_697_split[1])) ; 
}


void Delay_N_515() {
	push_float(&SplitJoin196_Delay_N_Fiss_663_697_join[2], pop_float(&SplitJoin196_Delay_N_Fiss_663_697_split[2])) ; 
}


void Delay_N_516() {
	push_float(&SplitJoin196_Delay_N_Fiss_663_697_join[3], pop_float(&SplitJoin196_Delay_N_Fiss_663_697_split[3])) ; 
}


void Delay_N_517() {
	push_float(&SplitJoin196_Delay_N_Fiss_663_697_join[4], pop_float(&SplitJoin196_Delay_N_Fiss_663_697_split[4])) ; 
}


void Delay_N_518() {
	push_float(&SplitJoin196_Delay_N_Fiss_663_697_join[5], pop_float(&SplitJoin196_Delay_N_Fiss_663_697_split[5])) ; 
}


void Delay_N_519() {
	push_float(&SplitJoin196_Delay_N_Fiss_663_697_join[6], pop_float(&SplitJoin196_Delay_N_Fiss_663_697_split[6])) ; 
}


void Delay_N_520() {
	push_float(&SplitJoin196_Delay_N_Fiss_663_697_join[7], pop_float(&SplitJoin196_Delay_N_Fiss_663_697_split[7])) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_511() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&SplitJoin196_Delay_N_Fiss_663_697_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_229_293_641_676_split[5]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_512() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_512DUPLICATE_Splitter_521, pop_float(&SplitJoin196_Delay_N_Fiss_663_697_join[__iter_]));
	ENDFOR
}

void FirFilter_523() {
	float sum = 0.0;
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin198_FirFilter_Fiss_664_698_split[0], i) * FirFilter_523_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin198_FirFilter_Fiss_664_698_split[0]) ; 
	push_float(&SplitJoin198_FirFilter_Fiss_664_698_join[0], sum) ; 
 {
	FOR(int, streamItVar133, 0,  < , 7, streamItVar133++) {
		pop_void(&SplitJoin198_FirFilter_Fiss_664_698_split[0]) ; 
	}
	ENDFOR
}
}


void FirFilter_524() {
	float sum = 0.0;
 {
	pop_void(&SplitJoin198_FirFilter_Fiss_664_698_split[1]) ; 
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin198_FirFilter_Fiss_664_698_split[1], i) * FirFilter_524_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin198_FirFilter_Fiss_664_698_split[1]) ; 
	push_float(&SplitJoin198_FirFilter_Fiss_664_698_join[1], sum) ; 
 {
	FOR(int, streamItVar134, 0,  < , 6, streamItVar134++) {
		pop_void(&SplitJoin198_FirFilter_Fiss_664_698_split[1]) ; 
	}
	ENDFOR
}
}


void FirFilter_525() {
	float sum = 0.0;
 {
	FOR(int, streamItVar135, 0,  < , 2, streamItVar135++) {
		pop_void(&SplitJoin198_FirFilter_Fiss_664_698_split[2]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin198_FirFilter_Fiss_664_698_split[2], i) * FirFilter_525_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin198_FirFilter_Fiss_664_698_split[2]) ; 
	push_float(&SplitJoin198_FirFilter_Fiss_664_698_join[2], sum) ; 
 {
	FOR(int, streamItVar136, 0,  < , 5, streamItVar136++) {
		pop_void(&SplitJoin198_FirFilter_Fiss_664_698_split[2]) ; 
	}
	ENDFOR
}
}


void FirFilter_526() {
	float sum = 0.0;
 {
	FOR(int, streamItVar137, 0,  < , 3, streamItVar137++) {
		pop_void(&SplitJoin198_FirFilter_Fiss_664_698_split[3]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin198_FirFilter_Fiss_664_698_split[3], i) * FirFilter_526_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin198_FirFilter_Fiss_664_698_split[3]) ; 
	push_float(&SplitJoin198_FirFilter_Fiss_664_698_join[3], sum) ; 
 {
	FOR(int, streamItVar138, 0,  < , 4, streamItVar138++) {
		pop_void(&SplitJoin198_FirFilter_Fiss_664_698_split[3]) ; 
	}
	ENDFOR
}
}


void FirFilter_527() {
	float sum = 0.0;
 {
	FOR(int, streamItVar139, 0,  < , 4, streamItVar139++) {
		pop_void(&SplitJoin198_FirFilter_Fiss_664_698_split[4]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin198_FirFilter_Fiss_664_698_split[4], i) * FirFilter_527_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin198_FirFilter_Fiss_664_698_split[4]) ; 
	push_float(&SplitJoin198_FirFilter_Fiss_664_698_join[4], sum) ; 
 {
	FOR(int, streamItVar140, 0,  < , 3, streamItVar140++) {
		pop_void(&SplitJoin198_FirFilter_Fiss_664_698_split[4]) ; 
	}
	ENDFOR
}
}


void FirFilter_528() {
	float sum = 0.0;
 {
	FOR(int, streamItVar141, 0,  < , 5, streamItVar141++) {
		pop_void(&SplitJoin198_FirFilter_Fiss_664_698_split[5]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin198_FirFilter_Fiss_664_698_split[5], i) * FirFilter_528_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin198_FirFilter_Fiss_664_698_split[5]) ; 
	push_float(&SplitJoin198_FirFilter_Fiss_664_698_join[5], sum) ; 
 {
	FOR(int, streamItVar142, 0,  < , 2, streamItVar142++) {
		pop_void(&SplitJoin198_FirFilter_Fiss_664_698_split[5]) ; 
	}
	ENDFOR
}
}


void FirFilter_529() {
	float sum = 0.0;
 {
	FOR(int, streamItVar143, 0,  < , 6, streamItVar143++) {
		pop_void(&SplitJoin198_FirFilter_Fiss_664_698_split[6]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin198_FirFilter_Fiss_664_698_split[6], i) * FirFilter_529_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin198_FirFilter_Fiss_664_698_split[6]) ; 
	push_float(&SplitJoin198_FirFilter_Fiss_664_698_join[6], sum) ; 
 {
	pop_void(&SplitJoin198_FirFilter_Fiss_664_698_split[6]) ; 
}
}


void FirFilter_530() {
	float sum = 0.0;
 {
	FOR(int, streamItVar144, 0,  < , 7, streamItVar144++) {
		pop_void(&SplitJoin198_FirFilter_Fiss_664_698_split[7]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin198_FirFilter_Fiss_664_698_split[7], i) * FirFilter_530_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin198_FirFilter_Fiss_664_698_split[7]) ; 
	push_float(&SplitJoin198_FirFilter_Fiss_664_698_join[7], sum) ; 
}


void DUPLICATE_Splitter_521() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_512DUPLICATE_Splitter_521);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin198_FirFilter_Fiss_664_698_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_522() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_522DownSamp_271, pop_float(&SplitJoin198_FirFilter_Fiss_664_698_join[__iter_]));
	ENDFOR
}

void DownSamp_271() {
	push_float(&DownSamp_271UpSamp_272, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_522DownSamp_271)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_522DownSamp_271) ; 
	}
	ENDFOR
}


void UpSamp_272() {
	push_float(&UpSamp_272WEIGHTED_ROUND_ROBIN_Splitter_531, pop_float(&DownSamp_271UpSamp_272)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		push_float(&UpSamp_272WEIGHTED_ROUND_ROBIN_Splitter_531, 0.0) ; 
	}
	ENDFOR
}


void Delay_N_533() {
	push_float(&SplitJoin200_Delay_N_Fiss_665_699_join[0], pop_float(&SplitJoin200_Delay_N_Fiss_665_699_split[0])) ; 
}


void Delay_N_534() {
	push_float(&SplitJoin200_Delay_N_Fiss_665_699_join[1], pop_float(&SplitJoin200_Delay_N_Fiss_665_699_split[1])) ; 
}


void Delay_N_535() {
	push_float(&SplitJoin200_Delay_N_Fiss_665_699_join[2], pop_float(&SplitJoin200_Delay_N_Fiss_665_699_split[2])) ; 
}


void Delay_N_536() {
	push_float(&SplitJoin200_Delay_N_Fiss_665_699_join[3], pop_float(&SplitJoin200_Delay_N_Fiss_665_699_split[3])) ; 
}


void Delay_N_537() {
	push_float(&SplitJoin200_Delay_N_Fiss_665_699_join[4], pop_float(&SplitJoin200_Delay_N_Fiss_665_699_split[4])) ; 
}


void Delay_N_538() {
	push_float(&SplitJoin200_Delay_N_Fiss_665_699_join[5], pop_float(&SplitJoin200_Delay_N_Fiss_665_699_split[5])) ; 
}


void Delay_N_539() {
	push_float(&SplitJoin200_Delay_N_Fiss_665_699_join[6], pop_float(&SplitJoin200_Delay_N_Fiss_665_699_split[6])) ; 
}


void Delay_N_540() {
	push_float(&SplitJoin200_Delay_N_Fiss_665_699_join[7], pop_float(&SplitJoin200_Delay_N_Fiss_665_699_split[7])) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_531() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&SplitJoin200_Delay_N_Fiss_665_699_split[__iter_], pop_float(&UpSamp_272WEIGHTED_ROUND_ROBIN_Splitter_531));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_532() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_532DUPLICATE_Splitter_541, pop_float(&SplitJoin200_Delay_N_Fiss_665_699_join[__iter_]));
	ENDFOR
}

void FirFilter_543() {
	float sum = 0.0;
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin202_FirFilter_Fiss_666_700_split[0], i) * FirFilter_543_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin202_FirFilter_Fiss_666_700_split[0]) ; 
	push_float(&SplitJoin202_FirFilter_Fiss_666_700_join[0], sum) ; 
 {
	FOR(int, streamItVar121, 0,  < , 7, streamItVar121++) {
		pop_void(&SplitJoin202_FirFilter_Fiss_666_700_split[0]) ; 
	}
	ENDFOR
}
}


void FirFilter_544() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	pop_void(&SplitJoin202_FirFilter_Fiss_666_700_split[1]) ; 
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin202_FirFilter_Fiss_666_700_split[1], i) * FirFilter_544_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin202_FirFilter_Fiss_666_700_split[1]) ; 
	push_float(&SplitJoin202_FirFilter_Fiss_666_700_join[1], sum) ; 
 {
	FOR(int, streamItVar122, 0,  < , 6, streamItVar122++) {
		pop_void(&SplitJoin202_FirFilter_Fiss_666_700_split[1]) ; 
	}
	ENDFOR
}
}


void FirFilter_545() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar123, 0,  < , 2, streamItVar123++) {
		pop_void(&SplitJoin202_FirFilter_Fiss_666_700_split[2]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin202_FirFilter_Fiss_666_700_split[2], i) * FirFilter_545_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin202_FirFilter_Fiss_666_700_split[2]) ; 
	push_float(&SplitJoin202_FirFilter_Fiss_666_700_join[2], sum) ; 
 {
	FOR(int, streamItVar124, 0,  < , 5, streamItVar124++) {
		pop_void(&SplitJoin202_FirFilter_Fiss_666_700_split[2]) ; 
	}
	ENDFOR
}
}


void FirFilter_546() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar125, 0,  < , 3, streamItVar125++) {
		pop_void(&SplitJoin202_FirFilter_Fiss_666_700_split[3]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin202_FirFilter_Fiss_666_700_split[3], i) * FirFilter_546_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin202_FirFilter_Fiss_666_700_split[3]) ; 
	push_float(&SplitJoin202_FirFilter_Fiss_666_700_join[3], sum) ; 
 {
	FOR(int, streamItVar126, 0,  < , 4, streamItVar126++) {
		pop_void(&SplitJoin202_FirFilter_Fiss_666_700_split[3]) ; 
	}
	ENDFOR
}
}


void FirFilter_547() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar127, 0,  < , 4, streamItVar127++) {
		pop_void(&SplitJoin202_FirFilter_Fiss_666_700_split[4]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin202_FirFilter_Fiss_666_700_split[4], i) * FirFilter_547_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin202_FirFilter_Fiss_666_700_split[4]) ; 
	push_float(&SplitJoin202_FirFilter_Fiss_666_700_join[4], sum) ; 
 {
	FOR(int, streamItVar128, 0,  < , 3, streamItVar128++) {
		pop_void(&SplitJoin202_FirFilter_Fiss_666_700_split[4]) ; 
	}
	ENDFOR
}
}


void FirFilter_548() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar129, 0,  < , 5, streamItVar129++) {
		pop_void(&SplitJoin202_FirFilter_Fiss_666_700_split[5]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin202_FirFilter_Fiss_666_700_split[5], i) * FirFilter_548_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin202_FirFilter_Fiss_666_700_split[5]) ; 
	push_float(&SplitJoin202_FirFilter_Fiss_666_700_join[5], sum) ; 
 {
	FOR(int, streamItVar130, 0,  < , 2, streamItVar130++) {
		pop_void(&SplitJoin202_FirFilter_Fiss_666_700_split[5]) ; 
	}
	ENDFOR
}
}


void FirFilter_549() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar131, 0,  < , 6, streamItVar131++) {
		pop_void(&SplitJoin202_FirFilter_Fiss_666_700_split[6]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin202_FirFilter_Fiss_666_700_split[6], i) * FirFilter_549_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin202_FirFilter_Fiss_666_700_split[6]) ; 
	push_float(&SplitJoin202_FirFilter_Fiss_666_700_join[6], sum) ; 
 {
	pop_void(&SplitJoin202_FirFilter_Fiss_666_700_split[6]) ; 
}
}


void FirFilter_550() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar132, 0,  < , 7, streamItVar132++) {
		pop_void(&SplitJoin202_FirFilter_Fiss_666_700_split[7]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin202_FirFilter_Fiss_666_700_split[7], i) * FirFilter_550_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin202_FirFilter_Fiss_666_700_split[7]) ; 
	push_float(&SplitJoin202_FirFilter_Fiss_666_700_join[7], sum) ; 
}


void DUPLICATE_Splitter_541() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_532DUPLICATE_Splitter_541);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin202_FirFilter_Fiss_666_700_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_542() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_229_293_641_676_join[5], pop_float(&SplitJoin202_FirFilter_Fiss_666_700_join[__iter_]));
	ENDFOR
}

void Delay_N_553() {
	push_float(&SplitJoin233_Delay_N_Fiss_667_701_join[0], pop_float(&SplitJoin233_Delay_N_Fiss_667_701_split[0])) ; 
}


void Delay_N_554() {
	push_float(&SplitJoin233_Delay_N_Fiss_667_701_join[1], pop_float(&SplitJoin233_Delay_N_Fiss_667_701_split[1])) ; 
}


void Delay_N_555() {
	push_float(&SplitJoin233_Delay_N_Fiss_667_701_join[2], pop_float(&SplitJoin233_Delay_N_Fiss_667_701_split[2])) ; 
}


void Delay_N_556() {
	push_float(&SplitJoin233_Delay_N_Fiss_667_701_join[3], pop_float(&SplitJoin233_Delay_N_Fiss_667_701_split[3])) ; 
}


void Delay_N_557() {
	push_float(&SplitJoin233_Delay_N_Fiss_667_701_join[4], pop_float(&SplitJoin233_Delay_N_Fiss_667_701_split[4])) ; 
}


void Delay_N_558() {
	push_float(&SplitJoin233_Delay_N_Fiss_667_701_join[5], pop_float(&SplitJoin233_Delay_N_Fiss_667_701_split[5])) ; 
}


void Delay_N_559() {
	push_float(&SplitJoin233_Delay_N_Fiss_667_701_join[6], pop_float(&SplitJoin233_Delay_N_Fiss_667_701_split[6])) ; 
}


void Delay_N_560() {
	push_float(&SplitJoin233_Delay_N_Fiss_667_701_join[7], pop_float(&SplitJoin233_Delay_N_Fiss_667_701_split[7])) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_551() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&SplitJoin233_Delay_N_Fiss_667_701_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_229_293_641_676_split[6]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_552() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_552DUPLICATE_Splitter_561, pop_float(&SplitJoin233_Delay_N_Fiss_667_701_join[__iter_]));
	ENDFOR
}

void FirFilter_563() {
	float sum = 0.0;
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin235_FirFilter_Fiss_668_702_split[0], i) * FirFilter_563_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin235_FirFilter_Fiss_668_702_split[0]) ; 
	push_float(&SplitJoin235_FirFilter_Fiss_668_702_join[0], sum) ; 
 {
	FOR(int, streamItVar109, 0,  < , 7, streamItVar109++) {
		pop_void(&SplitJoin235_FirFilter_Fiss_668_702_split[0]) ; 
	}
	ENDFOR
}
}


void FirFilter_564() {
	float sum = 0.0;
 {
	pop_void(&SplitJoin235_FirFilter_Fiss_668_702_split[1]) ; 
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin235_FirFilter_Fiss_668_702_split[1], i) * FirFilter_564_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin235_FirFilter_Fiss_668_702_split[1]) ; 
	push_float(&SplitJoin235_FirFilter_Fiss_668_702_join[1], sum) ; 
 {
	FOR(int, streamItVar110, 0,  < , 6, streamItVar110++) {
		pop_void(&SplitJoin235_FirFilter_Fiss_668_702_split[1]) ; 
	}
	ENDFOR
}
}


void FirFilter_565() {
	float sum = 0.0;
 {
	FOR(int, streamItVar111, 0,  < , 2, streamItVar111++) {
		pop_void(&SplitJoin235_FirFilter_Fiss_668_702_split[2]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin235_FirFilter_Fiss_668_702_split[2], i) * FirFilter_565_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin235_FirFilter_Fiss_668_702_split[2]) ; 
	push_float(&SplitJoin235_FirFilter_Fiss_668_702_join[2], sum) ; 
 {
	FOR(int, streamItVar112, 0,  < , 5, streamItVar112++) {
		pop_void(&SplitJoin235_FirFilter_Fiss_668_702_split[2]) ; 
	}
	ENDFOR
}
}


void FirFilter_566() {
	float sum = 0.0;
 {
	FOR(int, streamItVar113, 0,  < , 3, streamItVar113++) {
		pop_void(&SplitJoin235_FirFilter_Fiss_668_702_split[3]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin235_FirFilter_Fiss_668_702_split[3], i) * FirFilter_566_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin235_FirFilter_Fiss_668_702_split[3]) ; 
	push_float(&SplitJoin235_FirFilter_Fiss_668_702_join[3], sum) ; 
 {
	FOR(int, streamItVar114, 0,  < , 4, streamItVar114++) {
		pop_void(&SplitJoin235_FirFilter_Fiss_668_702_split[3]) ; 
	}
	ENDFOR
}
}


void FirFilter_567() {
	float sum = 0.0;
 {
	FOR(int, streamItVar115, 0,  < , 4, streamItVar115++) {
		pop_void(&SplitJoin235_FirFilter_Fiss_668_702_split[4]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin235_FirFilter_Fiss_668_702_split[4], i) * FirFilter_567_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin235_FirFilter_Fiss_668_702_split[4]) ; 
	push_float(&SplitJoin235_FirFilter_Fiss_668_702_join[4], sum) ; 
 {
	FOR(int, streamItVar116, 0,  < , 3, streamItVar116++) {
		pop_void(&SplitJoin235_FirFilter_Fiss_668_702_split[4]) ; 
	}
	ENDFOR
}
}


void FirFilter_568() {
	float sum = 0.0;
 {
	FOR(int, streamItVar117, 0,  < , 5, streamItVar117++) {
		pop_void(&SplitJoin235_FirFilter_Fiss_668_702_split[5]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin235_FirFilter_Fiss_668_702_split[5], i) * FirFilter_568_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin235_FirFilter_Fiss_668_702_split[5]) ; 
	push_float(&SplitJoin235_FirFilter_Fiss_668_702_join[5], sum) ; 
 {
	FOR(int, streamItVar118, 0,  < , 2, streamItVar118++) {
		pop_void(&SplitJoin235_FirFilter_Fiss_668_702_split[5]) ; 
	}
	ENDFOR
}
}


void FirFilter_569() {
	float sum = 0.0;
 {
	FOR(int, streamItVar119, 0,  < , 6, streamItVar119++) {
		pop_void(&SplitJoin235_FirFilter_Fiss_668_702_split[6]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin235_FirFilter_Fiss_668_702_split[6], i) * FirFilter_569_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin235_FirFilter_Fiss_668_702_split[6]) ; 
	push_float(&SplitJoin235_FirFilter_Fiss_668_702_join[6], sum) ; 
 {
	pop_void(&SplitJoin235_FirFilter_Fiss_668_702_split[6]) ; 
}
}


void FirFilter_570() {
	float sum = 0.0;
 {
	FOR(int, streamItVar120, 0,  < , 7, streamItVar120++) {
		pop_void(&SplitJoin235_FirFilter_Fiss_668_702_split[7]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin235_FirFilter_Fiss_668_702_split[7], i) * FirFilter_570_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin235_FirFilter_Fiss_668_702_split[7]) ; 
	push_float(&SplitJoin235_FirFilter_Fiss_668_702_join[7], sum) ; 
}


void DUPLICATE_Splitter_561() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_552DUPLICATE_Splitter_561);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin235_FirFilter_Fiss_668_702_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_562() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_562DownSamp_278, pop_float(&SplitJoin235_FirFilter_Fiss_668_702_join[__iter_]));
	ENDFOR
}

void DownSamp_278() {
	push_float(&DownSamp_278UpSamp_279, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_562DownSamp_278)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_562DownSamp_278) ; 
	}
	ENDFOR
}


void UpSamp_279() {
	push_float(&UpSamp_279WEIGHTED_ROUND_ROBIN_Splitter_571, pop_float(&DownSamp_278UpSamp_279)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		push_float(&UpSamp_279WEIGHTED_ROUND_ROBIN_Splitter_571, 0.0) ; 
	}
	ENDFOR
}


void Delay_N_573() {
	push_float(&SplitJoin237_Delay_N_Fiss_669_703_join[0], pop_float(&SplitJoin237_Delay_N_Fiss_669_703_split[0])) ; 
}


void Delay_N_574() {
	push_float(&SplitJoin237_Delay_N_Fiss_669_703_join[1], pop_float(&SplitJoin237_Delay_N_Fiss_669_703_split[1])) ; 
}


void Delay_N_575() {
	push_float(&SplitJoin237_Delay_N_Fiss_669_703_join[2], pop_float(&SplitJoin237_Delay_N_Fiss_669_703_split[2])) ; 
}


void Delay_N_576() {
	push_float(&SplitJoin237_Delay_N_Fiss_669_703_join[3], pop_float(&SplitJoin237_Delay_N_Fiss_669_703_split[3])) ; 
}


void Delay_N_577() {
	push_float(&SplitJoin237_Delay_N_Fiss_669_703_join[4], pop_float(&SplitJoin237_Delay_N_Fiss_669_703_split[4])) ; 
}


void Delay_N_578() {
	push_float(&SplitJoin237_Delay_N_Fiss_669_703_join[5], pop_float(&SplitJoin237_Delay_N_Fiss_669_703_split[5])) ; 
}


void Delay_N_579() {
	push_float(&SplitJoin237_Delay_N_Fiss_669_703_join[6], pop_float(&SplitJoin237_Delay_N_Fiss_669_703_split[6])) ; 
}


void Delay_N_580() {
	push_float(&SplitJoin237_Delay_N_Fiss_669_703_join[7], pop_float(&SplitJoin237_Delay_N_Fiss_669_703_split[7])) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_571() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&SplitJoin237_Delay_N_Fiss_669_703_split[__iter_], pop_float(&UpSamp_279WEIGHTED_ROUND_ROBIN_Splitter_571));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_572() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_572DUPLICATE_Splitter_581, pop_float(&SplitJoin237_Delay_N_Fiss_669_703_join[__iter_]));
	ENDFOR
}

void FirFilter_583() {
	float sum = 0.0;
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin239_FirFilter_Fiss_670_704_split[0], i) * FirFilter_583_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin239_FirFilter_Fiss_670_704_split[0]) ; 
	push_float(&SplitJoin239_FirFilter_Fiss_670_704_join[0], sum) ; 
 {
	FOR(int, streamItVar97, 0,  < , 7, streamItVar97++) {
		pop_void(&SplitJoin239_FirFilter_Fiss_670_704_split[0]) ; 
	}
	ENDFOR
}
}


void FirFilter_584() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	pop_void(&SplitJoin239_FirFilter_Fiss_670_704_split[1]) ; 
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin239_FirFilter_Fiss_670_704_split[1], i) * FirFilter_584_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin239_FirFilter_Fiss_670_704_split[1]) ; 
	push_float(&SplitJoin239_FirFilter_Fiss_670_704_join[1], sum) ; 
 {
	FOR(int, streamItVar98, 0,  < , 6, streamItVar98++) {
		pop_void(&SplitJoin239_FirFilter_Fiss_670_704_split[1]) ; 
	}
	ENDFOR
}
}


void FirFilter_585() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar99, 0,  < , 2, streamItVar99++) {
		pop_void(&SplitJoin239_FirFilter_Fiss_670_704_split[2]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin239_FirFilter_Fiss_670_704_split[2], i) * FirFilter_585_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin239_FirFilter_Fiss_670_704_split[2]) ; 
	push_float(&SplitJoin239_FirFilter_Fiss_670_704_join[2], sum) ; 
 {
	FOR(int, streamItVar100, 0,  < , 5, streamItVar100++) {
		pop_void(&SplitJoin239_FirFilter_Fiss_670_704_split[2]) ; 
	}
	ENDFOR
}
}


void FirFilter_586() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar101, 0,  < , 3, streamItVar101++) {
		pop_void(&SplitJoin239_FirFilter_Fiss_670_704_split[3]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin239_FirFilter_Fiss_670_704_split[3], i) * FirFilter_586_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin239_FirFilter_Fiss_670_704_split[3]) ; 
	push_float(&SplitJoin239_FirFilter_Fiss_670_704_join[3], sum) ; 
 {
	FOR(int, streamItVar102, 0,  < , 4, streamItVar102++) {
		pop_void(&SplitJoin239_FirFilter_Fiss_670_704_split[3]) ; 
	}
	ENDFOR
}
}


void FirFilter_587() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar103, 0,  < , 4, streamItVar103++) {
		pop_void(&SplitJoin239_FirFilter_Fiss_670_704_split[4]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin239_FirFilter_Fiss_670_704_split[4], i) * FirFilter_587_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin239_FirFilter_Fiss_670_704_split[4]) ; 
	push_float(&SplitJoin239_FirFilter_Fiss_670_704_join[4], sum) ; 
 {
	FOR(int, streamItVar104, 0,  < , 3, streamItVar104++) {
		pop_void(&SplitJoin239_FirFilter_Fiss_670_704_split[4]) ; 
	}
	ENDFOR
}
}


void FirFilter_588() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar105, 0,  < , 5, streamItVar105++) {
		pop_void(&SplitJoin239_FirFilter_Fiss_670_704_split[5]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin239_FirFilter_Fiss_670_704_split[5], i) * FirFilter_588_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin239_FirFilter_Fiss_670_704_split[5]) ; 
	push_float(&SplitJoin239_FirFilter_Fiss_670_704_join[5], sum) ; 
 {
	FOR(int, streamItVar106, 0,  < , 2, streamItVar106++) {
		pop_void(&SplitJoin239_FirFilter_Fiss_670_704_split[5]) ; 
	}
	ENDFOR
}
}


void FirFilter_589() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar107, 0,  < , 6, streamItVar107++) {
		pop_void(&SplitJoin239_FirFilter_Fiss_670_704_split[6]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin239_FirFilter_Fiss_670_704_split[6], i) * FirFilter_589_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin239_FirFilter_Fiss_670_704_split[6]) ; 
	push_float(&SplitJoin239_FirFilter_Fiss_670_704_join[6], sum) ; 
 {
	pop_void(&SplitJoin239_FirFilter_Fiss_670_704_split[6]) ; 
}
}


void FirFilter_590() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar108, 0,  < , 7, streamItVar108++) {
		pop_void(&SplitJoin239_FirFilter_Fiss_670_704_split[7]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin239_FirFilter_Fiss_670_704_split[7], i) * FirFilter_590_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin239_FirFilter_Fiss_670_704_split[7]) ; 
	push_float(&SplitJoin239_FirFilter_Fiss_670_704_join[7], sum) ; 
}


void DUPLICATE_Splitter_581() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_572DUPLICATE_Splitter_581);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin239_FirFilter_Fiss_670_704_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_582() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_229_293_641_676_join[6], pop_float(&SplitJoin239_FirFilter_Fiss_670_704_join[__iter_]));
	ENDFOR
}

void Delay_N_593() {
	push_float(&SplitJoin270_Delay_N_Fiss_671_705_join[0], pop_float(&SplitJoin270_Delay_N_Fiss_671_705_split[0])) ; 
}


void Delay_N_594() {
	push_float(&SplitJoin270_Delay_N_Fiss_671_705_join[1], pop_float(&SplitJoin270_Delay_N_Fiss_671_705_split[1])) ; 
}


void Delay_N_595() {
	push_float(&SplitJoin270_Delay_N_Fiss_671_705_join[2], pop_float(&SplitJoin270_Delay_N_Fiss_671_705_split[2])) ; 
}


void Delay_N_596() {
	push_float(&SplitJoin270_Delay_N_Fiss_671_705_join[3], pop_float(&SplitJoin270_Delay_N_Fiss_671_705_split[3])) ; 
}


void Delay_N_597() {
	push_float(&SplitJoin270_Delay_N_Fiss_671_705_join[4], pop_float(&SplitJoin270_Delay_N_Fiss_671_705_split[4])) ; 
}


void Delay_N_598() {
	push_float(&SplitJoin270_Delay_N_Fiss_671_705_join[5], pop_float(&SplitJoin270_Delay_N_Fiss_671_705_split[5])) ; 
}


void Delay_N_599() {
	push_float(&SplitJoin270_Delay_N_Fiss_671_705_join[6], pop_float(&SplitJoin270_Delay_N_Fiss_671_705_split[6])) ; 
}


void Delay_N_600() {
	push_float(&SplitJoin270_Delay_N_Fiss_671_705_join[7], pop_float(&SplitJoin270_Delay_N_Fiss_671_705_split[7])) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_591() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&SplitJoin270_Delay_N_Fiss_671_705_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_229_293_641_676_split[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_592() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_592DUPLICATE_Splitter_601, pop_float(&SplitJoin270_Delay_N_Fiss_671_705_join[__iter_]));
	ENDFOR
}

void FirFilter_603() {
	float sum = 0.0;
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin272_FirFilter_Fiss_672_706_split[0], i) * FirFilter_603_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin272_FirFilter_Fiss_672_706_split[0]) ; 
	push_float(&SplitJoin272_FirFilter_Fiss_672_706_join[0], sum) ; 
 {
	FOR(int, streamItVar85, 0,  < , 7, streamItVar85++) {
		pop_void(&SplitJoin272_FirFilter_Fiss_672_706_split[0]) ; 
	}
	ENDFOR
}
}


void FirFilter_604() {
	float sum = 0.0;
 {
	pop_void(&SplitJoin272_FirFilter_Fiss_672_706_split[1]) ; 
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin272_FirFilter_Fiss_672_706_split[1], i) * FirFilter_604_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin272_FirFilter_Fiss_672_706_split[1]) ; 
	push_float(&SplitJoin272_FirFilter_Fiss_672_706_join[1], sum) ; 
 {
	FOR(int, streamItVar86, 0,  < , 6, streamItVar86++) {
		pop_void(&SplitJoin272_FirFilter_Fiss_672_706_split[1]) ; 
	}
	ENDFOR
}
}


void FirFilter_605() {
	float sum = 0.0;
 {
	FOR(int, streamItVar87, 0,  < , 2, streamItVar87++) {
		pop_void(&SplitJoin272_FirFilter_Fiss_672_706_split[2]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin272_FirFilter_Fiss_672_706_split[2], i) * FirFilter_605_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin272_FirFilter_Fiss_672_706_split[2]) ; 
	push_float(&SplitJoin272_FirFilter_Fiss_672_706_join[2], sum) ; 
 {
	FOR(int, streamItVar88, 0,  < , 5, streamItVar88++) {
		pop_void(&SplitJoin272_FirFilter_Fiss_672_706_split[2]) ; 
	}
	ENDFOR
}
}


void FirFilter_606() {
	float sum = 0.0;
 {
	FOR(int, streamItVar89, 0,  < , 3, streamItVar89++) {
		pop_void(&SplitJoin272_FirFilter_Fiss_672_706_split[3]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin272_FirFilter_Fiss_672_706_split[3], i) * FirFilter_606_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin272_FirFilter_Fiss_672_706_split[3]) ; 
	push_float(&SplitJoin272_FirFilter_Fiss_672_706_join[3], sum) ; 
 {
	FOR(int, streamItVar90, 0,  < , 4, streamItVar90++) {
		pop_void(&SplitJoin272_FirFilter_Fiss_672_706_split[3]) ; 
	}
	ENDFOR
}
}


void FirFilter_607() {
	float sum = 0.0;
 {
	FOR(int, streamItVar91, 0,  < , 4, streamItVar91++) {
		pop_void(&SplitJoin272_FirFilter_Fiss_672_706_split[4]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin272_FirFilter_Fiss_672_706_split[4], i) * FirFilter_607_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin272_FirFilter_Fiss_672_706_split[4]) ; 
	push_float(&SplitJoin272_FirFilter_Fiss_672_706_join[4], sum) ; 
 {
	FOR(int, streamItVar92, 0,  < , 3, streamItVar92++) {
		pop_void(&SplitJoin272_FirFilter_Fiss_672_706_split[4]) ; 
	}
	ENDFOR
}
}


void FirFilter_608() {
	float sum = 0.0;
 {
	FOR(int, streamItVar93, 0,  < , 5, streamItVar93++) {
		pop_void(&SplitJoin272_FirFilter_Fiss_672_706_split[5]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin272_FirFilter_Fiss_672_706_split[5], i) * FirFilter_608_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin272_FirFilter_Fiss_672_706_split[5]) ; 
	push_float(&SplitJoin272_FirFilter_Fiss_672_706_join[5], sum) ; 
 {
	FOR(int, streamItVar94, 0,  < , 2, streamItVar94++) {
		pop_void(&SplitJoin272_FirFilter_Fiss_672_706_split[5]) ; 
	}
	ENDFOR
}
}


void FirFilter_609() {
	float sum = 0.0;
 {
	FOR(int, streamItVar95, 0,  < , 6, streamItVar95++) {
		pop_void(&SplitJoin272_FirFilter_Fiss_672_706_split[6]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin272_FirFilter_Fiss_672_706_split[6], i) * FirFilter_609_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin272_FirFilter_Fiss_672_706_split[6]) ; 
	push_float(&SplitJoin272_FirFilter_Fiss_672_706_join[6], sum) ; 
 {
	pop_void(&SplitJoin272_FirFilter_Fiss_672_706_split[6]) ; 
}
}


void FirFilter_610() {
	float sum = 0.0;
 {
	FOR(int, streamItVar96, 0,  < , 7, streamItVar96++) {
		pop_void(&SplitJoin272_FirFilter_Fiss_672_706_split[7]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin272_FirFilter_Fiss_672_706_split[7], i) * FirFilter_610_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin272_FirFilter_Fiss_672_706_split[7]) ; 
	push_float(&SplitJoin272_FirFilter_Fiss_672_706_join[7], sum) ; 
}


void DUPLICATE_Splitter_601() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_592DUPLICATE_Splitter_601);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin272_FirFilter_Fiss_672_706_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_602() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_602DownSamp_285, pop_float(&SplitJoin272_FirFilter_Fiss_672_706_join[__iter_]));
	ENDFOR
}

void DownSamp_285() {
	push_float(&DownSamp_285UpSamp_286, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_602DownSamp_285)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_602DownSamp_285) ; 
	}
	ENDFOR
}


void UpSamp_286() {
	push_float(&UpSamp_286WEIGHTED_ROUND_ROBIN_Splitter_611, pop_float(&DownSamp_285UpSamp_286)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		push_float(&UpSamp_286WEIGHTED_ROUND_ROBIN_Splitter_611, 0.0) ; 
	}
	ENDFOR
}


void Delay_N_613() {
	push_float(&SplitJoin274_Delay_N_Fiss_673_707_join[0], pop_float(&SplitJoin274_Delay_N_Fiss_673_707_split[0])) ; 
}


void Delay_N_614() {
	push_float(&SplitJoin274_Delay_N_Fiss_673_707_join[1], pop_float(&SplitJoin274_Delay_N_Fiss_673_707_split[1])) ; 
}


void Delay_N_615() {
	push_float(&SplitJoin274_Delay_N_Fiss_673_707_join[2], pop_float(&SplitJoin274_Delay_N_Fiss_673_707_split[2])) ; 
}


void Delay_N_616() {
	push_float(&SplitJoin274_Delay_N_Fiss_673_707_join[3], pop_float(&SplitJoin274_Delay_N_Fiss_673_707_split[3])) ; 
}


void Delay_N_617() {
	push_float(&SplitJoin274_Delay_N_Fiss_673_707_join[4], pop_float(&SplitJoin274_Delay_N_Fiss_673_707_split[4])) ; 
}


void Delay_N_618() {
	push_float(&SplitJoin274_Delay_N_Fiss_673_707_join[5], pop_float(&SplitJoin274_Delay_N_Fiss_673_707_split[5])) ; 
}


void Delay_N_619() {
	push_float(&SplitJoin274_Delay_N_Fiss_673_707_join[6], pop_float(&SplitJoin274_Delay_N_Fiss_673_707_split[6])) ; 
}


void Delay_N_620() {
	push_float(&SplitJoin274_Delay_N_Fiss_673_707_join[7], pop_float(&SplitJoin274_Delay_N_Fiss_673_707_split[7])) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_611() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&SplitJoin274_Delay_N_Fiss_673_707_split[__iter_], pop_float(&UpSamp_286WEIGHTED_ROUND_ROBIN_Splitter_611));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_612() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_612DUPLICATE_Splitter_621, pop_float(&SplitJoin274_Delay_N_Fiss_673_707_join[__iter_]));
	ENDFOR
}

void FirFilter_623() {
	float sum = 0.0;
	sum = 0.0 ; 
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin276_FirFilter_Fiss_674_708_split[0], i) * FirFilter_623_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin276_FirFilter_Fiss_674_708_split[0]) ; 
	push_float(&SplitJoin276_FirFilter_Fiss_674_708_join[0], sum) ; 
 {
	FOR(int, streamItVar73, 0,  < , 7, streamItVar73++) {
		pop_void(&SplitJoin276_FirFilter_Fiss_674_708_split[0]) ; 
	}
	ENDFOR
}
}


void FirFilter_624() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	pop_void(&SplitJoin276_FirFilter_Fiss_674_708_split[1]) ; 
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin276_FirFilter_Fiss_674_708_split[1], i) * FirFilter_624_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin276_FirFilter_Fiss_674_708_split[1]) ; 
	push_float(&SplitJoin276_FirFilter_Fiss_674_708_join[1], sum) ; 
 {
	FOR(int, streamItVar74, 0,  < , 6, streamItVar74++) {
		pop_void(&SplitJoin276_FirFilter_Fiss_674_708_split[1]) ; 
	}
	ENDFOR
}
}


void FirFilter_625() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar75, 0,  < , 2, streamItVar75++) {
		pop_void(&SplitJoin276_FirFilter_Fiss_674_708_split[2]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin276_FirFilter_Fiss_674_708_split[2], i) * FirFilter_625_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin276_FirFilter_Fiss_674_708_split[2]) ; 
	push_float(&SplitJoin276_FirFilter_Fiss_674_708_join[2], sum) ; 
 {
	FOR(int, streamItVar76, 0,  < , 5, streamItVar76++) {
		pop_void(&SplitJoin276_FirFilter_Fiss_674_708_split[2]) ; 
	}
	ENDFOR
}
}


void FirFilter_626() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar77, 0,  < , 3, streamItVar77++) {
		pop_void(&SplitJoin276_FirFilter_Fiss_674_708_split[3]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin276_FirFilter_Fiss_674_708_split[3], i) * FirFilter_626_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin276_FirFilter_Fiss_674_708_split[3]) ; 
	push_float(&SplitJoin276_FirFilter_Fiss_674_708_join[3], sum) ; 
 {
	FOR(int, streamItVar78, 0,  < , 4, streamItVar78++) {
		pop_void(&SplitJoin276_FirFilter_Fiss_674_708_split[3]) ; 
	}
	ENDFOR
}
}


void FirFilter_627() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar79, 0,  < , 4, streamItVar79++) {
		pop_void(&SplitJoin276_FirFilter_Fiss_674_708_split[4]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin276_FirFilter_Fiss_674_708_split[4], i) * FirFilter_627_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin276_FirFilter_Fiss_674_708_split[4]) ; 
	push_float(&SplitJoin276_FirFilter_Fiss_674_708_join[4], sum) ; 
 {
	FOR(int, streamItVar80, 0,  < , 3, streamItVar80++) {
		pop_void(&SplitJoin276_FirFilter_Fiss_674_708_split[4]) ; 
	}
	ENDFOR
}
}


void FirFilter_628() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar81, 0,  < , 5, streamItVar81++) {
		pop_void(&SplitJoin276_FirFilter_Fiss_674_708_split[5]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin276_FirFilter_Fiss_674_708_split[5], i) * FirFilter_628_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin276_FirFilter_Fiss_674_708_split[5]) ; 
	push_float(&SplitJoin276_FirFilter_Fiss_674_708_join[5], sum) ; 
 {
	FOR(int, streamItVar82, 0,  < , 2, streamItVar82++) {
		pop_void(&SplitJoin276_FirFilter_Fiss_674_708_split[5]) ; 
	}
	ENDFOR
}
}


void FirFilter_629() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar83, 0,  < , 6, streamItVar83++) {
		pop_void(&SplitJoin276_FirFilter_Fiss_674_708_split[6]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin276_FirFilter_Fiss_674_708_split[6], i) * FirFilter_629_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin276_FirFilter_Fiss_674_708_split[6]) ; 
	push_float(&SplitJoin276_FirFilter_Fiss_674_708_join[6], sum) ; 
 {
	pop_void(&SplitJoin276_FirFilter_Fiss_674_708_split[6]) ; 
}
}


void FirFilter_630() {
	float sum = 0.0;
	sum = 0.0 ; 
 {
	FOR(int, streamItVar84, 0,  < , 7, streamItVar84++) {
		pop_void(&SplitJoin276_FirFilter_Fiss_674_708_split[7]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin276_FirFilter_Fiss_674_708_split[7], i) * FirFilter_630_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin276_FirFilter_Fiss_674_708_split[7]) ; 
	push_float(&SplitJoin276_FirFilter_Fiss_674_708_join[7], sum) ; 
}


void DUPLICATE_Splitter_621() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_612DUPLICATE_Splitter_621);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin276_FirFilter_Fiss_674_708_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_622() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_229_293_641_676_join[7], pop_float(&SplitJoin276_FirFilter_Fiss_674_708_join[__iter_]));
	ENDFOR
}

void DUPLICATE_Splitter_291() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&source_231DUPLICATE_Splitter_291);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_229_293_641_676_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_292() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_292WEIGHTED_ROUND_ROBIN_Splitter_631, pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_229_293_641_676_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Combine_633() {
	float sum = 0.0;
	FOR(int, i, 0,  < , 8, i++) {
		sum = (sum + pop_float(&SplitJoin10_Combine_Fiss_646_709_split[0])) ; 
	}
	ENDFOR
	push_float(&SplitJoin10_Combine_Fiss_646_709_join[0], sum) ; 
}


void Combine_634() {
	float sum = 0.0;
	FOR(int, i, 0,  < , 8, i++) {
		sum = (sum + pop_float(&SplitJoin10_Combine_Fiss_646_709_split[1])) ; 
	}
	ENDFOR
	push_float(&SplitJoin10_Combine_Fiss_646_709_join[1], sum) ; 
}


void Combine_635() {
	float sum = 0.0;
	FOR(int, i, 0,  < , 8, i++) {
		sum = (sum + pop_float(&SplitJoin10_Combine_Fiss_646_709_split[2])) ; 
	}
	ENDFOR
	push_float(&SplitJoin10_Combine_Fiss_646_709_join[2], sum) ; 
}


void Combine_636() {
	float sum = 0.0;
	FOR(int, i, 0,  < , 8, i++) {
		sum = (sum + pop_float(&SplitJoin10_Combine_Fiss_646_709_split[3])) ; 
	}
	ENDFOR
	push_float(&SplitJoin10_Combine_Fiss_646_709_join[3], sum) ; 
}


void Combine_637() {
	float sum = 0.0;
	FOR(int, i, 0,  < , 8, i++) {
		sum = (sum + pop_float(&SplitJoin10_Combine_Fiss_646_709_split[4])) ; 
	}
	ENDFOR
	push_float(&SplitJoin10_Combine_Fiss_646_709_join[4], sum) ; 
}


void Combine_638() {
	float sum = 0.0;
	FOR(int, i, 0,  < , 8, i++) {
		sum = (sum + pop_float(&SplitJoin10_Combine_Fiss_646_709_split[5])) ; 
	}
	ENDFOR
	push_float(&SplitJoin10_Combine_Fiss_646_709_join[5], sum) ; 
}


void Combine_639() {
	float sum = 0.0;
	FOR(int, i, 0,  < , 8, i++) {
		sum = (sum + pop_float(&SplitJoin10_Combine_Fiss_646_709_split[6])) ; 
	}
	ENDFOR
	push_float(&SplitJoin10_Combine_Fiss_646_709_join[6], sum) ; 
}


void Combine_640() {
	float sum = 0.0;
	FOR(int, i, 0,  < , 8, i++) {
		sum = (sum + pop_float(&SplitJoin10_Combine_Fiss_646_709_split[7])) ; 
	}
	ENDFOR
	push_float(&SplitJoin10_Combine_Fiss_646_709_join[7], sum) ; 
}


void WEIGHTED_ROUND_ROBIN_Splitter_631() {
	FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_float(&SplitJoin10_Combine_Fiss_646_709_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_292WEIGHTED_ROUND_ROBIN_Splitter_631));
		ENDFOR
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Joiner_632() {
	FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_632sink_290, pop_float(&SplitJoin10_Combine_Fiss_646_709_join[__iter_]));
	ENDFOR
}

void sink_290(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		printf("%.10f", pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_632sink_290));
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&UpSamp_272WEIGHTED_ROUND_ROBIN_Splitter_531);
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_float(&SplitJoin235_FirFilter_Fiss_668_702_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_float(&SplitJoin2_Delay_N_Fiss_642_677_split[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&DownSamp_257UpSamp_258);
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_float(&SplitJoin200_Delay_N_Fiss_665_699_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_float(&SplitJoin198_FirFilter_Fiss_664_698_split[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_372DUPLICATE_Splitter_381);
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_float(&SplitJoin159_Delay_N_Fiss_659_693_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin54_FirFilter_Fiss_650_684_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_float(&SplitJoin235_FirFilter_Fiss_668_702_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_float(&SplitJoin276_FirFilter_Fiss_674_708_join[__iter_init_7_]);
	ENDFOR
	init_buffer_float(&UpSamp_265WEIGHTED_ROUND_ROBIN_Splitter_491);
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_float(&SplitJoin196_Delay_N_Fiss_663_697_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_float(&SplitJoin274_Delay_N_Fiss_673_707_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_float(&SplitJoin126_Delay_N_Fiss_657_691_join[__iter_init_10_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_402DownSamp_250);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_632sink_290);
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_float(&SplitJoin161_FirFilter_Fiss_660_694_join[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 8, __iter_init_12_++)
		init_buffer_float(&SplitJoin52_Delay_N_Fiss_649_683_split[__iter_init_12_]);
	ENDFOR
	init_buffer_float(&DownSamp_243UpSamp_244);
	FOR(int, __iter_init_13_, 0, <, 8, __iter_init_13_++)
		init_buffer_float(&SplitJoin8_FirFilter_Fiss_645_680_join[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 8, __iter_init_14_++)
		init_buffer_float(&SplitJoin87_FirFilter_Fiss_652_686_join[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 8, __iter_init_15_++)
		init_buffer_float(&SplitJoin8_FirFilter_Fiss_645_680_split[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 8, __iter_init_16_++)
		init_buffer_float(&SplitJoin2_Delay_N_Fiss_642_677_join[__iter_init_16_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_592DUPLICATE_Splitter_601);
	FOR(int, __iter_init_17_, 0, <, 8, __iter_init_17_++)
		init_buffer_float(&SplitJoin128_FirFilter_Fiss_658_692_join[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 8, __iter_init_18_++)
		init_buffer_float(&SplitJoin239_FirFilter_Fiss_670_704_split[__iter_init_18_]);
	ENDFOR
	FOR(int, __iter_init_19_, 0, <, 8, __iter_init_19_++)
		init_buffer_float(&SplitJoin48_Delay_N_Fiss_647_681_join[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 8, __iter_init_20_++)
		init_buffer_float(&SplitJoin128_FirFilter_Fiss_658_692_split[__iter_init_20_]);
	ENDFOR
	init_buffer_float(&UpSamp_258WEIGHTED_ROUND_ROBIN_Splitter_451);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_492DUPLICATE_Splitter_501);
	FOR(int, __iter_init_21_, 0, <, 8, __iter_init_21_++)
		init_buffer_float(&SplitJoin6_Delay_N_Fiss_644_679_split[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 8, __iter_init_22_++)
		init_buffer_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_229_293_641_676_join[__iter_init_22_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_602DownSamp_285);
	FOR(int, __iter_init_23_, 0, <, 8, __iter_init_23_++)
		init_buffer_float(&SplitJoin10_Combine_Fiss_646_709_join[__iter_init_23_]);
	ENDFOR
	FOR(int, __iter_init_24_, 0, <, 8, __iter_init_24_++)
		init_buffer_float(&SplitJoin274_Delay_N_Fiss_673_707_split[__iter_init_24_]);
	ENDFOR
	FOR(int, __iter_init_25_, 0, <, 8, __iter_init_25_++)
		init_buffer_float(&SplitJoin50_FirFilter_Fiss_648_682_split[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 8, __iter_init_26_++)
		init_buffer_float(&SplitJoin48_Delay_N_Fiss_647_681_split[__iter_init_26_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_322DownSamp_236);
	FOR(int, __iter_init_27_, 0, <, 8, __iter_init_27_++)
		init_buffer_float(&SplitJoin54_FirFilter_Fiss_650_684_join[__iter_init_27_]);
	ENDFOR
	FOR(int, __iter_init_28_, 0, <, 8, __iter_init_28_++)
		init_buffer_float(&SplitJoin270_Delay_N_Fiss_671_705_join[__iter_init_28_]);
	ENDFOR
	FOR(int, __iter_init_29_, 0, <, 8, __iter_init_29_++)
		init_buffer_float(&SplitJoin276_FirFilter_Fiss_674_708_split[__iter_init_29_]);
	ENDFOR
	FOR(int, __iter_init_30_, 0, <, 8, __iter_init_30_++)
		init_buffer_float(&SplitJoin89_Delay_N_Fiss_653_687_join[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 8, __iter_init_31_++)
		init_buffer_float(&SplitJoin124_FirFilter_Fiss_656_690_split[__iter_init_31_]);
	ENDFOR
	init_buffer_float(&DownSamp_236UpSamp_237);
	FOR(int, __iter_init_32_, 0, <, 8, __iter_init_32_++)
		init_buffer_float(&SplitJoin233_Delay_N_Fiss_667_701_join[__iter_init_32_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_292WEIGHTED_ROUND_ROBIN_Splitter_631);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_362DownSamp_243);
	FOR(int, __iter_init_33_, 0, <, 8, __iter_init_33_++)
		init_buffer_float(&SplitJoin6_Delay_N_Fiss_644_679_join[__iter_init_33_]);
	ENDFOR
	init_buffer_float(&DownSamp_278UpSamp_279);
	init_buffer_float(&UpSamp_279WEIGHTED_ROUND_ROBIN_Splitter_571);
	FOR(int, __iter_init_34_, 0, <, 8, __iter_init_34_++)
		init_buffer_float(&SplitJoin200_Delay_N_Fiss_665_699_split[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 8, __iter_init_35_++)
		init_buffer_float(&SplitJoin4_FirFilter_Fiss_643_678_join[__iter_init_35_]);
	ENDFOR
	FOR(int, __iter_init_36_, 0, <, 8, __iter_init_36_++)
		init_buffer_float(&SplitJoin124_FirFilter_Fiss_656_690_join[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 8, __iter_init_37_++)
		init_buffer_float(&SplitJoin159_Delay_N_Fiss_659_693_join[__iter_init_37_]);
	ENDFOR
	FOR(int, __iter_init_38_, 0, <, 8, __iter_init_38_++)
		init_buffer_float(&SplitJoin165_FirFilter_Fiss_662_696_join[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 8, __iter_init_39_++)
		init_buffer_float(&SplitJoin163_Delay_N_Fiss_661_695_split[__iter_init_39_]);
	ENDFOR
	FOR(int, __iter_init_40_, 0, <, 8, __iter_init_40_++)
		init_buffer_float(&SplitJoin126_Delay_N_Fiss_657_691_split[__iter_init_40_]);
	ENDFOR
	FOR(int, __iter_init_41_, 0, <, 8, __iter_init_41_++)
		init_buffer_float(&SplitJoin270_Delay_N_Fiss_671_705_split[__iter_init_41_]);
	ENDFOR
	FOR(int, __iter_init_42_, 0, <, 8, __iter_init_42_++)
		init_buffer_float(&SplitJoin161_FirFilter_Fiss_660_694_split[__iter_init_42_]);
	ENDFOR
	FOR(int, __iter_init_43_, 0, <, 8, __iter_init_43_++)
		init_buffer_float(&SplitJoin198_FirFilter_Fiss_664_698_join[__iter_init_43_]);
	ENDFOR
	FOR(int, __iter_init_44_, 0, <, 8, __iter_init_44_++)
		init_buffer_float(&SplitJoin89_Delay_N_Fiss_653_687_split[__iter_init_44_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_482DownSamp_264);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_392DUPLICATE_Splitter_401);
	FOR(int, __iter_init_45_, 0, <, 8, __iter_init_45_++)
		init_buffer_float(&SplitJoin233_Delay_N_Fiss_667_701_split[__iter_init_45_]);
	ENDFOR
	init_buffer_float(&source_231DUPLICATE_Splitter_291);
	init_buffer_float(&UpSamp_286WEIGHTED_ROUND_ROBIN_Splitter_611);
	FOR(int, __iter_init_46_, 0, <, 8, __iter_init_46_++)
		init_buffer_float(&SplitJoin85_Delay_N_Fiss_651_685_join[__iter_init_46_]);
	ENDFOR
	FOR(int, __iter_init_47_, 0, <, 8, __iter_init_47_++)
		init_buffer_float(&SplitJoin87_FirFilter_Fiss_652_686_split[__iter_init_47_]);
	ENDFOR
	FOR(int, __iter_init_48_, 0, <, 8, __iter_init_48_++)
		init_buffer_float(&SplitJoin272_FirFilter_Fiss_672_706_split[__iter_init_48_]);
	ENDFOR
	FOR(int, __iter_init_49_, 0, <, 8, __iter_init_49_++)
		init_buffer_float(&SplitJoin4_FirFilter_Fiss_643_678_split[__iter_init_49_]);
	ENDFOR
	FOR(int, __iter_init_50_, 0, <, 8, __iter_init_50_++)
		init_buffer_float(&SplitJoin237_Delay_N_Fiss_669_703_join[__iter_init_50_]);
	ENDFOR
	init_buffer_float(&UpSamp_244WEIGHTED_ROUND_ROBIN_Splitter_371);
	FOR(int, __iter_init_51_, 0, <, 8, __iter_init_51_++)
		init_buffer_float(&SplitJoin91_FirFilter_Fiss_654_688_split[__iter_init_51_]);
	ENDFOR
	FOR(int, __iter_init_52_, 0, <, 8, __iter_init_52_++)
		init_buffer_float(&SplitJoin163_Delay_N_Fiss_661_695_join[__iter_init_52_]);
	ENDFOR
	FOR(int, __iter_init_53_, 0, <, 8, __iter_init_53_++)
		init_buffer_float(&SplitJoin202_FirFilter_Fiss_666_700_split[__iter_init_53_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_412DUPLICATE_Splitter_421);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_572DUPLICATE_Splitter_581);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_442DownSamp_257);
	init_buffer_float(&DownSamp_264UpSamp_265);
	FOR(int, __iter_init_54_, 0, <, 8, __iter_init_54_++)
		init_buffer_float(&SplitJoin10_Combine_Fiss_646_709_split[__iter_init_54_]);
	ENDFOR
	FOR(int, __iter_init_55_, 0, <, 8, __iter_init_55_++)
		init_buffer_float(&SplitJoin85_Delay_N_Fiss_651_685_split[__iter_init_55_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_452DUPLICATE_Splitter_461);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_432DUPLICATE_Splitter_441);
	FOR(int, __iter_init_56_, 0, <, 8, __iter_init_56_++)
		init_buffer_float(&SplitJoin202_FirFilter_Fiss_666_700_join[__iter_init_56_]);
	ENDFOR
	init_buffer_float(&DownSamp_285UpSamp_286);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_312DUPLICATE_Splitter_321);
	FOR(int, __iter_init_57_, 0, <, 8, __iter_init_57_++)
		init_buffer_float(&SplitJoin52_Delay_N_Fiss_649_683_join[__iter_init_57_]);
	ENDFOR
	init_buffer_float(&UpSamp_251WEIGHTED_ROUND_ROBIN_Splitter_411);
	FOR(int, __iter_init_58_, 0, <, 8, __iter_init_58_++)
		init_buffer_float(&SplitJoin50_FirFilter_Fiss_648_682_join[__iter_init_58_]);
	ENDFOR
	FOR(int, __iter_init_59_, 0, <, 8, __iter_init_59_++)
		init_buffer_float(&SplitJoin165_FirFilter_Fiss_662_696_split[__iter_init_59_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_522DownSamp_271);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_352DUPLICATE_Splitter_361);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_332DUPLICATE_Splitter_341);
	FOR(int, __iter_init_60_, 0, <, 8, __iter_init_60_++)
		init_buffer_float(&SplitJoin91_FirFilter_Fiss_654_688_join[__iter_init_60_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_562DownSamp_278);
	FOR(int, __iter_init_61_, 0, <, 8, __iter_init_61_++)
		init_buffer_float(&SplitJoin122_Delay_N_Fiss_655_689_join[__iter_init_61_]);
	ENDFOR
	FOR(int, __iter_init_62_, 0, <, 8, __iter_init_62_++)
		init_buffer_float(&SplitJoin239_FirFilter_Fiss_670_704_join[__iter_init_62_]);
	ENDFOR
	init_buffer_float(&UpSamp_237WEIGHTED_ROUND_ROBIN_Splitter_331);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_612DUPLICATE_Splitter_621);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_472DUPLICATE_Splitter_481);
	FOR(int, __iter_init_63_, 0, <, 8, __iter_init_63_++)
		init_buffer_float(&SplitJoin237_Delay_N_Fiss_669_703_split[__iter_init_63_]);
	ENDFOR
	init_buffer_float(&DownSamp_271UpSamp_272);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_532DUPLICATE_Splitter_541);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_512DUPLICATE_Splitter_521);
	FOR(int, __iter_init_64_, 0, <, 8, __iter_init_64_++)
		init_buffer_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_229_293_641_676_split[__iter_init_64_]);
	ENDFOR
	FOR(int, __iter_init_65_, 0, <, 8, __iter_init_65_++)
		init_buffer_float(&SplitJoin272_FirFilter_Fiss_672_706_join[__iter_init_65_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_552DUPLICATE_Splitter_561);
	init_buffer_float(&DownSamp_250UpSamp_251);
	FOR(int, __iter_init_66_, 0, <, 8, __iter_init_66_++)
		init_buffer_float(&SplitJoin122_Delay_N_Fiss_655_689_split[__iter_init_66_]);
	ENDFOR
	FOR(int, __iter_init_67_, 0, <, 8, __iter_init_67_++)
		init_buffer_float(&SplitJoin196_Delay_N_Fiss_663_697_join[__iter_init_67_]);
	ENDFOR
// --- init: source_231
	 {
	source_231_s.current = 0.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 528, __iter_init_++) {
		push_float(&source_231DUPLICATE_Splitter_291, source_231_s.current) ; 
		if((source_231_s.current > 1000.0)) {
			source_231_s.current = 0.0 ; 
		}
		else {
			source_231_s.current = (source_231_s.current + 1.0) ; 
		}
	}
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_291
	FOR(uint32_t, __iter_init_, 0, <, 528, __iter_init_++)
		float __token_ = pop_float(&source_231DUPLICATE_Splitter_291);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_229_293_641_676_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_311
	FOR(uint32_t, __iter_init_, 0, <, 66, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&SplitJoin2_Delay_N_Fiss_642_677_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_229_293_641_676_split[0]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_313
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin2_Delay_N_Fiss_642_677_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_314
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin2_Delay_N_Fiss_642_677_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_315
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin2_Delay_N_Fiss_642_677_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_316
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin2_Delay_N_Fiss_642_677_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_317
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin2_Delay_N_Fiss_642_677_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_318
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin2_Delay_N_Fiss_642_677_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_319
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin2_Delay_N_Fiss_642_677_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_320
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin2_Delay_N_Fiss_642_677_join[7], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_312
	FOR(uint32_t, __iter_init_, 0, <, 37, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_312DUPLICATE_Splitter_321, pop_float(&SplitJoin2_Delay_N_Fiss_642_677_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_321
	FOR(uint32_t, __iter_init_, 0, <, 295, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_312DUPLICATE_Splitter_321);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin4_FirFilter_Fiss_643_678_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_323
	 {
	FirFilter_323_s.COEFF[0] = 1.0 ; 
	FirFilter_323_s.COEFF[1] = 34.0 ; 
	FirFilter_323_s.COEFF[2] = 67.0 ; 
	FirFilter_323_s.COEFF[3] = 100.0 ; 
	FirFilter_323_s.COEFF[4] = 133.0 ; 
	FirFilter_323_s.COEFF[5] = 166.0 ; 
	FirFilter_323_s.COEFF[6] = 199.0 ; 
	FirFilter_323_s.COEFF[7] = 232.0 ; 
	FirFilter_323_s.COEFF[8] = 265.0 ; 
	FirFilter_323_s.COEFF[9] = 298.0 ; 
	FirFilter_323_s.COEFF[10] = 331.0 ; 
	FirFilter_323_s.COEFF[11] = 364.0 ; 
	FirFilter_323_s.COEFF[12] = 397.0 ; 
	FirFilter_323_s.COEFF[13] = 430.0 ; 
	FirFilter_323_s.COEFF[14] = 463.0 ; 
	FirFilter_323_s.COEFF[15] = 496.0 ; 
	FirFilter_323_s.COEFF[16] = 529.0 ; 
	FirFilter_323_s.COEFF[17] = 562.0 ; 
	FirFilter_323_s.COEFF[18] = 595.0 ; 
	FirFilter_323_s.COEFF[19] = 628.0 ; 
	FirFilter_323_s.COEFF[20] = 661.0 ; 
	FirFilter_323_s.COEFF[21] = 694.0 ; 
	FirFilter_323_s.COEFF[22] = 727.0 ; 
	FirFilter_323_s.COEFF[23] = 760.0 ; 
	FirFilter_323_s.COEFF[24] = 793.0 ; 
	FirFilter_323_s.COEFF[25] = 826.0 ; 
	FirFilter_323_s.COEFF[26] = 859.0 ; 
	FirFilter_323_s.COEFF[27] = 892.0 ; 
	FirFilter_323_s.COEFF[28] = 925.0 ; 
	FirFilter_323_s.COEFF[29] = 958.0 ; 
	FirFilter_323_s.COEFF[30] = 991.0 ; 
	FirFilter_323_s.COEFF[31] = 1024.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin4_FirFilter_Fiss_643_678_split[0], i) * FirFilter_323_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin4_FirFilter_Fiss_643_678_split[0]) ; 
		push_float(&SplitJoin4_FirFilter_Fiss_643_678_join[0], sum) ; 
 {
		FOR(int, streamItVar61, 0,  < , 7, streamItVar61++) {
			pop_void(&SplitJoin4_FirFilter_Fiss_643_678_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_324
	 {
	FirFilter_324_s.COEFF[0] = 1.0 ; 
	FirFilter_324_s.COEFF[1] = 34.0 ; 
	FirFilter_324_s.COEFF[2] = 67.0 ; 
	FirFilter_324_s.COEFF[3] = 100.0 ; 
	FirFilter_324_s.COEFF[4] = 133.0 ; 
	FirFilter_324_s.COEFF[5] = 166.0 ; 
	FirFilter_324_s.COEFF[6] = 199.0 ; 
	FirFilter_324_s.COEFF[7] = 232.0 ; 
	FirFilter_324_s.COEFF[8] = 265.0 ; 
	FirFilter_324_s.COEFF[9] = 298.0 ; 
	FirFilter_324_s.COEFF[10] = 331.0 ; 
	FirFilter_324_s.COEFF[11] = 364.0 ; 
	FirFilter_324_s.COEFF[12] = 397.0 ; 
	FirFilter_324_s.COEFF[13] = 430.0 ; 
	FirFilter_324_s.COEFF[14] = 463.0 ; 
	FirFilter_324_s.COEFF[15] = 496.0 ; 
	FirFilter_324_s.COEFF[16] = 529.0 ; 
	FirFilter_324_s.COEFF[17] = 562.0 ; 
	FirFilter_324_s.COEFF[18] = 595.0 ; 
	FirFilter_324_s.COEFF[19] = 628.0 ; 
	FirFilter_324_s.COEFF[20] = 661.0 ; 
	FirFilter_324_s.COEFF[21] = 694.0 ; 
	FirFilter_324_s.COEFF[22] = 727.0 ; 
	FirFilter_324_s.COEFF[23] = 760.0 ; 
	FirFilter_324_s.COEFF[24] = 793.0 ; 
	FirFilter_324_s.COEFF[25] = 826.0 ; 
	FirFilter_324_s.COEFF[26] = 859.0 ; 
	FirFilter_324_s.COEFF[27] = 892.0 ; 
	FirFilter_324_s.COEFF[28] = 925.0 ; 
	FirFilter_324_s.COEFF[29] = 958.0 ; 
	FirFilter_324_s.COEFF[30] = 991.0 ; 
	FirFilter_324_s.COEFF[31] = 1024.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin4_FirFilter_Fiss_643_678_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin4_FirFilter_Fiss_643_678_split[1], i) * FirFilter_324_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin4_FirFilter_Fiss_643_678_split[1]) ; 
		push_float(&SplitJoin4_FirFilter_Fiss_643_678_join[1], sum) ; 
 {
		FOR(int, streamItVar62, 0,  < , 6, streamItVar62++) {
			pop_void(&SplitJoin4_FirFilter_Fiss_643_678_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_325
	 {
	FirFilter_325_s.COEFF[0] = 1.0 ; 
	FirFilter_325_s.COEFF[1] = 34.0 ; 
	FirFilter_325_s.COEFF[2] = 67.0 ; 
	FirFilter_325_s.COEFF[3] = 100.0 ; 
	FirFilter_325_s.COEFF[4] = 133.0 ; 
	FirFilter_325_s.COEFF[5] = 166.0 ; 
	FirFilter_325_s.COEFF[6] = 199.0 ; 
	FirFilter_325_s.COEFF[7] = 232.0 ; 
	FirFilter_325_s.COEFF[8] = 265.0 ; 
	FirFilter_325_s.COEFF[9] = 298.0 ; 
	FirFilter_325_s.COEFF[10] = 331.0 ; 
	FirFilter_325_s.COEFF[11] = 364.0 ; 
	FirFilter_325_s.COEFF[12] = 397.0 ; 
	FirFilter_325_s.COEFF[13] = 430.0 ; 
	FirFilter_325_s.COEFF[14] = 463.0 ; 
	FirFilter_325_s.COEFF[15] = 496.0 ; 
	FirFilter_325_s.COEFF[16] = 529.0 ; 
	FirFilter_325_s.COEFF[17] = 562.0 ; 
	FirFilter_325_s.COEFF[18] = 595.0 ; 
	FirFilter_325_s.COEFF[19] = 628.0 ; 
	FirFilter_325_s.COEFF[20] = 661.0 ; 
	FirFilter_325_s.COEFF[21] = 694.0 ; 
	FirFilter_325_s.COEFF[22] = 727.0 ; 
	FirFilter_325_s.COEFF[23] = 760.0 ; 
	FirFilter_325_s.COEFF[24] = 793.0 ; 
	FirFilter_325_s.COEFF[25] = 826.0 ; 
	FirFilter_325_s.COEFF[26] = 859.0 ; 
	FirFilter_325_s.COEFF[27] = 892.0 ; 
	FirFilter_325_s.COEFF[28] = 925.0 ; 
	FirFilter_325_s.COEFF[29] = 958.0 ; 
	FirFilter_325_s.COEFF[30] = 991.0 ; 
	FirFilter_325_s.COEFF[31] = 1024.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar63, 0,  < , 2, streamItVar63++) {
			pop_void(&SplitJoin4_FirFilter_Fiss_643_678_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin4_FirFilter_Fiss_643_678_split[2], i) * FirFilter_325_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin4_FirFilter_Fiss_643_678_split[2]) ; 
		push_float(&SplitJoin4_FirFilter_Fiss_643_678_join[2], sum) ; 
 {
		FOR(int, streamItVar64, 0,  < , 5, streamItVar64++) {
			pop_void(&SplitJoin4_FirFilter_Fiss_643_678_split[2]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_326
	 {
	FirFilter_326_s.COEFF[0] = 1.0 ; 
	FirFilter_326_s.COEFF[1] = 34.0 ; 
	FirFilter_326_s.COEFF[2] = 67.0 ; 
	FirFilter_326_s.COEFF[3] = 100.0 ; 
	FirFilter_326_s.COEFF[4] = 133.0 ; 
	FirFilter_326_s.COEFF[5] = 166.0 ; 
	FirFilter_326_s.COEFF[6] = 199.0 ; 
	FirFilter_326_s.COEFF[7] = 232.0 ; 
	FirFilter_326_s.COEFF[8] = 265.0 ; 
	FirFilter_326_s.COEFF[9] = 298.0 ; 
	FirFilter_326_s.COEFF[10] = 331.0 ; 
	FirFilter_326_s.COEFF[11] = 364.0 ; 
	FirFilter_326_s.COEFF[12] = 397.0 ; 
	FirFilter_326_s.COEFF[13] = 430.0 ; 
	FirFilter_326_s.COEFF[14] = 463.0 ; 
	FirFilter_326_s.COEFF[15] = 496.0 ; 
	FirFilter_326_s.COEFF[16] = 529.0 ; 
	FirFilter_326_s.COEFF[17] = 562.0 ; 
	FirFilter_326_s.COEFF[18] = 595.0 ; 
	FirFilter_326_s.COEFF[19] = 628.0 ; 
	FirFilter_326_s.COEFF[20] = 661.0 ; 
	FirFilter_326_s.COEFF[21] = 694.0 ; 
	FirFilter_326_s.COEFF[22] = 727.0 ; 
	FirFilter_326_s.COEFF[23] = 760.0 ; 
	FirFilter_326_s.COEFF[24] = 793.0 ; 
	FirFilter_326_s.COEFF[25] = 826.0 ; 
	FirFilter_326_s.COEFF[26] = 859.0 ; 
	FirFilter_326_s.COEFF[27] = 892.0 ; 
	FirFilter_326_s.COEFF[28] = 925.0 ; 
	FirFilter_326_s.COEFF[29] = 958.0 ; 
	FirFilter_326_s.COEFF[30] = 991.0 ; 
	FirFilter_326_s.COEFF[31] = 1024.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar65, 0,  < , 3, streamItVar65++) {
			pop_void(&SplitJoin4_FirFilter_Fiss_643_678_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin4_FirFilter_Fiss_643_678_split[3], i) * FirFilter_326_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin4_FirFilter_Fiss_643_678_split[3]) ; 
		push_float(&SplitJoin4_FirFilter_Fiss_643_678_join[3], sum) ; 
 {
		FOR(int, streamItVar66, 0,  < , 4, streamItVar66++) {
			pop_void(&SplitJoin4_FirFilter_Fiss_643_678_split[3]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_327
	 {
	FirFilter_327_s.COEFF[0] = 1.0 ; 
	FirFilter_327_s.COEFF[1] = 34.0 ; 
	FirFilter_327_s.COEFF[2] = 67.0 ; 
	FirFilter_327_s.COEFF[3] = 100.0 ; 
	FirFilter_327_s.COEFF[4] = 133.0 ; 
	FirFilter_327_s.COEFF[5] = 166.0 ; 
	FirFilter_327_s.COEFF[6] = 199.0 ; 
	FirFilter_327_s.COEFF[7] = 232.0 ; 
	FirFilter_327_s.COEFF[8] = 265.0 ; 
	FirFilter_327_s.COEFF[9] = 298.0 ; 
	FirFilter_327_s.COEFF[10] = 331.0 ; 
	FirFilter_327_s.COEFF[11] = 364.0 ; 
	FirFilter_327_s.COEFF[12] = 397.0 ; 
	FirFilter_327_s.COEFF[13] = 430.0 ; 
	FirFilter_327_s.COEFF[14] = 463.0 ; 
	FirFilter_327_s.COEFF[15] = 496.0 ; 
	FirFilter_327_s.COEFF[16] = 529.0 ; 
	FirFilter_327_s.COEFF[17] = 562.0 ; 
	FirFilter_327_s.COEFF[18] = 595.0 ; 
	FirFilter_327_s.COEFF[19] = 628.0 ; 
	FirFilter_327_s.COEFF[20] = 661.0 ; 
	FirFilter_327_s.COEFF[21] = 694.0 ; 
	FirFilter_327_s.COEFF[22] = 727.0 ; 
	FirFilter_327_s.COEFF[23] = 760.0 ; 
	FirFilter_327_s.COEFF[24] = 793.0 ; 
	FirFilter_327_s.COEFF[25] = 826.0 ; 
	FirFilter_327_s.COEFF[26] = 859.0 ; 
	FirFilter_327_s.COEFF[27] = 892.0 ; 
	FirFilter_327_s.COEFF[28] = 925.0 ; 
	FirFilter_327_s.COEFF[29] = 958.0 ; 
	FirFilter_327_s.COEFF[30] = 991.0 ; 
	FirFilter_327_s.COEFF[31] = 1024.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar67, 0,  < , 4, streamItVar67++) {
			pop_void(&SplitJoin4_FirFilter_Fiss_643_678_split[4]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin4_FirFilter_Fiss_643_678_split[4], i) * FirFilter_327_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin4_FirFilter_Fiss_643_678_split[4]) ; 
		push_float(&SplitJoin4_FirFilter_Fiss_643_678_join[4], sum) ; 
 {
		FOR(int, streamItVar68, 0,  < , 3, streamItVar68++) {
			pop_void(&SplitJoin4_FirFilter_Fiss_643_678_split[4]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_328
	 {
	FirFilter_328_s.COEFF[0] = 1.0 ; 
	FirFilter_328_s.COEFF[1] = 34.0 ; 
	FirFilter_328_s.COEFF[2] = 67.0 ; 
	FirFilter_328_s.COEFF[3] = 100.0 ; 
	FirFilter_328_s.COEFF[4] = 133.0 ; 
	FirFilter_328_s.COEFF[5] = 166.0 ; 
	FirFilter_328_s.COEFF[6] = 199.0 ; 
	FirFilter_328_s.COEFF[7] = 232.0 ; 
	FirFilter_328_s.COEFF[8] = 265.0 ; 
	FirFilter_328_s.COEFF[9] = 298.0 ; 
	FirFilter_328_s.COEFF[10] = 331.0 ; 
	FirFilter_328_s.COEFF[11] = 364.0 ; 
	FirFilter_328_s.COEFF[12] = 397.0 ; 
	FirFilter_328_s.COEFF[13] = 430.0 ; 
	FirFilter_328_s.COEFF[14] = 463.0 ; 
	FirFilter_328_s.COEFF[15] = 496.0 ; 
	FirFilter_328_s.COEFF[16] = 529.0 ; 
	FirFilter_328_s.COEFF[17] = 562.0 ; 
	FirFilter_328_s.COEFF[18] = 595.0 ; 
	FirFilter_328_s.COEFF[19] = 628.0 ; 
	FirFilter_328_s.COEFF[20] = 661.0 ; 
	FirFilter_328_s.COEFF[21] = 694.0 ; 
	FirFilter_328_s.COEFF[22] = 727.0 ; 
	FirFilter_328_s.COEFF[23] = 760.0 ; 
	FirFilter_328_s.COEFF[24] = 793.0 ; 
	FirFilter_328_s.COEFF[25] = 826.0 ; 
	FirFilter_328_s.COEFF[26] = 859.0 ; 
	FirFilter_328_s.COEFF[27] = 892.0 ; 
	FirFilter_328_s.COEFF[28] = 925.0 ; 
	FirFilter_328_s.COEFF[29] = 958.0 ; 
	FirFilter_328_s.COEFF[30] = 991.0 ; 
	FirFilter_328_s.COEFF[31] = 1024.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar69, 0,  < , 5, streamItVar69++) {
			pop_void(&SplitJoin4_FirFilter_Fiss_643_678_split[5]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin4_FirFilter_Fiss_643_678_split[5], i) * FirFilter_328_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin4_FirFilter_Fiss_643_678_split[5]) ; 
		push_float(&SplitJoin4_FirFilter_Fiss_643_678_join[5], sum) ; 
 {
		FOR(int, streamItVar70, 0,  < , 2, streamItVar70++) {
			pop_void(&SplitJoin4_FirFilter_Fiss_643_678_split[5]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_329
	 {
	FirFilter_329_s.COEFF[0] = 1.0 ; 
	FirFilter_329_s.COEFF[1] = 34.0 ; 
	FirFilter_329_s.COEFF[2] = 67.0 ; 
	FirFilter_329_s.COEFF[3] = 100.0 ; 
	FirFilter_329_s.COEFF[4] = 133.0 ; 
	FirFilter_329_s.COEFF[5] = 166.0 ; 
	FirFilter_329_s.COEFF[6] = 199.0 ; 
	FirFilter_329_s.COEFF[7] = 232.0 ; 
	FirFilter_329_s.COEFF[8] = 265.0 ; 
	FirFilter_329_s.COEFF[9] = 298.0 ; 
	FirFilter_329_s.COEFF[10] = 331.0 ; 
	FirFilter_329_s.COEFF[11] = 364.0 ; 
	FirFilter_329_s.COEFF[12] = 397.0 ; 
	FirFilter_329_s.COEFF[13] = 430.0 ; 
	FirFilter_329_s.COEFF[14] = 463.0 ; 
	FirFilter_329_s.COEFF[15] = 496.0 ; 
	FirFilter_329_s.COEFF[16] = 529.0 ; 
	FirFilter_329_s.COEFF[17] = 562.0 ; 
	FirFilter_329_s.COEFF[18] = 595.0 ; 
	FirFilter_329_s.COEFF[19] = 628.0 ; 
	FirFilter_329_s.COEFF[20] = 661.0 ; 
	FirFilter_329_s.COEFF[21] = 694.0 ; 
	FirFilter_329_s.COEFF[22] = 727.0 ; 
	FirFilter_329_s.COEFF[23] = 760.0 ; 
	FirFilter_329_s.COEFF[24] = 793.0 ; 
	FirFilter_329_s.COEFF[25] = 826.0 ; 
	FirFilter_329_s.COEFF[26] = 859.0 ; 
	FirFilter_329_s.COEFF[27] = 892.0 ; 
	FirFilter_329_s.COEFF[28] = 925.0 ; 
	FirFilter_329_s.COEFF[29] = 958.0 ; 
	FirFilter_329_s.COEFF[30] = 991.0 ; 
	FirFilter_329_s.COEFF[31] = 1024.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar71, 0,  < , 6, streamItVar71++) {
			pop_void(&SplitJoin4_FirFilter_Fiss_643_678_split[6]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin4_FirFilter_Fiss_643_678_split[6], i) * FirFilter_329_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin4_FirFilter_Fiss_643_678_split[6]) ; 
		push_float(&SplitJoin4_FirFilter_Fiss_643_678_join[6], sum) ; 
 {
		pop_void(&SplitJoin4_FirFilter_Fiss_643_678_split[6]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_330
	 {
	FirFilter_330_s.COEFF[0] = 1.0 ; 
	FirFilter_330_s.COEFF[1] = 34.0 ; 
	FirFilter_330_s.COEFF[2] = 67.0 ; 
	FirFilter_330_s.COEFF[3] = 100.0 ; 
	FirFilter_330_s.COEFF[4] = 133.0 ; 
	FirFilter_330_s.COEFF[5] = 166.0 ; 
	FirFilter_330_s.COEFF[6] = 199.0 ; 
	FirFilter_330_s.COEFF[7] = 232.0 ; 
	FirFilter_330_s.COEFF[8] = 265.0 ; 
	FirFilter_330_s.COEFF[9] = 298.0 ; 
	FirFilter_330_s.COEFF[10] = 331.0 ; 
	FirFilter_330_s.COEFF[11] = 364.0 ; 
	FirFilter_330_s.COEFF[12] = 397.0 ; 
	FirFilter_330_s.COEFF[13] = 430.0 ; 
	FirFilter_330_s.COEFF[14] = 463.0 ; 
	FirFilter_330_s.COEFF[15] = 496.0 ; 
	FirFilter_330_s.COEFF[16] = 529.0 ; 
	FirFilter_330_s.COEFF[17] = 562.0 ; 
	FirFilter_330_s.COEFF[18] = 595.0 ; 
	FirFilter_330_s.COEFF[19] = 628.0 ; 
	FirFilter_330_s.COEFF[20] = 661.0 ; 
	FirFilter_330_s.COEFF[21] = 694.0 ; 
	FirFilter_330_s.COEFF[22] = 727.0 ; 
	FirFilter_330_s.COEFF[23] = 760.0 ; 
	FirFilter_330_s.COEFF[24] = 793.0 ; 
	FirFilter_330_s.COEFF[25] = 826.0 ; 
	FirFilter_330_s.COEFF[26] = 859.0 ; 
	FirFilter_330_s.COEFF[27] = 892.0 ; 
	FirFilter_330_s.COEFF[28] = 925.0 ; 
	FirFilter_330_s.COEFF[29] = 958.0 ; 
	FirFilter_330_s.COEFF[30] = 991.0 ; 
	FirFilter_330_s.COEFF[31] = 1024.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar72, 0,  < , 7, streamItVar72++) {
			pop_void(&SplitJoin4_FirFilter_Fiss_643_678_split[7]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin4_FirFilter_Fiss_643_678_split[7], i) * FirFilter_330_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin4_FirFilter_Fiss_643_678_split[7]) ; 
		push_float(&SplitJoin4_FirFilter_Fiss_643_678_join[7], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_322
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_322DownSamp_236, pop_float(&SplitJoin4_FirFilter_Fiss_643_678_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DownSamp_236
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		push_float(&DownSamp_236UpSamp_237, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_322DownSamp_236)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_322DownSamp_236) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: UpSamp_237
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		push_float(&UpSamp_237WEIGHTED_ROUND_ROBIN_Splitter_331, pop_float(&DownSamp_236UpSamp_237)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			push_float(&UpSamp_237WEIGHTED_ROUND_ROBIN_Splitter_331, 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_331
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&SplitJoin6_Delay_N_Fiss_644_679_split[__iter_], pop_float(&UpSamp_237WEIGHTED_ROUND_ROBIN_Splitter_331));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_333
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin6_Delay_N_Fiss_644_679_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_334
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin6_Delay_N_Fiss_644_679_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_335
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin6_Delay_N_Fiss_644_679_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_336
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin6_Delay_N_Fiss_644_679_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_337
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin6_Delay_N_Fiss_644_679_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_338
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin6_Delay_N_Fiss_644_679_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_339
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin6_Delay_N_Fiss_644_679_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_340
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin6_Delay_N_Fiss_644_679_join[7], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_332
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_332DUPLICATE_Splitter_341, pop_float(&SplitJoin6_Delay_N_Fiss_644_679_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_341
	FOR(uint32_t, __iter_init_, 0, <, 31, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_332DUPLICATE_Splitter_341);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin8_FirFilter_Fiss_645_680_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_343
	 {
	FirFilter_343_s.COEFF[0] = 0.0 ; 
	FirFilter_343_s.COEFF[1] = 1.0 ; 
	FirFilter_343_s.COEFF[2] = 2.0 ; 
	FirFilter_343_s.COEFF[3] = 3.0 ; 
	FirFilter_343_s.COEFF[4] = 4.0 ; 
	FirFilter_343_s.COEFF[5] = 5.0 ; 
	FirFilter_343_s.COEFF[6] = 6.0 ; 
	FirFilter_343_s.COEFF[7] = 7.0 ; 
	FirFilter_343_s.COEFF[8] = 8.0 ; 
	FirFilter_343_s.COEFF[9] = 9.0 ; 
	FirFilter_343_s.COEFF[10] = 10.0 ; 
	FirFilter_343_s.COEFF[11] = 11.0 ; 
	FirFilter_343_s.COEFF[12] = 12.0 ; 
	FirFilter_343_s.COEFF[13] = 13.0 ; 
	FirFilter_343_s.COEFF[14] = 14.0 ; 
	FirFilter_343_s.COEFF[15] = 15.0 ; 
	FirFilter_343_s.COEFF[16] = 16.0 ; 
	FirFilter_343_s.COEFF[17] = 17.0 ; 
	FirFilter_343_s.COEFF[18] = 18.0 ; 
	FirFilter_343_s.COEFF[19] = 19.0 ; 
	FirFilter_343_s.COEFF[20] = 20.0 ; 
	FirFilter_343_s.COEFF[21] = 21.0 ; 
	FirFilter_343_s.COEFF[22] = 22.0 ; 
	FirFilter_343_s.COEFF[23] = 23.0 ; 
	FirFilter_343_s.COEFF[24] = 24.0 ; 
	FirFilter_343_s.COEFF[25] = 25.0 ; 
	FirFilter_343_s.COEFF[26] = 26.0 ; 
	FirFilter_343_s.COEFF[27] = 27.0 ; 
	FirFilter_343_s.COEFF[28] = 28.0 ; 
	FirFilter_343_s.COEFF[29] = 29.0 ; 
	FirFilter_343_s.COEFF[30] = 30.0 ; 
	FirFilter_343_s.COEFF[31] = 31.0 ; 
}
//--------------------------------
// --- init: FirFilter_344
	 {
	FirFilter_344_s.COEFF[0] = 0.0 ; 
	FirFilter_344_s.COEFF[1] = 1.0 ; 
	FirFilter_344_s.COEFF[2] = 2.0 ; 
	FirFilter_344_s.COEFF[3] = 3.0 ; 
	FirFilter_344_s.COEFF[4] = 4.0 ; 
	FirFilter_344_s.COEFF[5] = 5.0 ; 
	FirFilter_344_s.COEFF[6] = 6.0 ; 
	FirFilter_344_s.COEFF[7] = 7.0 ; 
	FirFilter_344_s.COEFF[8] = 8.0 ; 
	FirFilter_344_s.COEFF[9] = 9.0 ; 
	FirFilter_344_s.COEFF[10] = 10.0 ; 
	FirFilter_344_s.COEFF[11] = 11.0 ; 
	FirFilter_344_s.COEFF[12] = 12.0 ; 
	FirFilter_344_s.COEFF[13] = 13.0 ; 
	FirFilter_344_s.COEFF[14] = 14.0 ; 
	FirFilter_344_s.COEFF[15] = 15.0 ; 
	FirFilter_344_s.COEFF[16] = 16.0 ; 
	FirFilter_344_s.COEFF[17] = 17.0 ; 
	FirFilter_344_s.COEFF[18] = 18.0 ; 
	FirFilter_344_s.COEFF[19] = 19.0 ; 
	FirFilter_344_s.COEFF[20] = 20.0 ; 
	FirFilter_344_s.COEFF[21] = 21.0 ; 
	FirFilter_344_s.COEFF[22] = 22.0 ; 
	FirFilter_344_s.COEFF[23] = 23.0 ; 
	FirFilter_344_s.COEFF[24] = 24.0 ; 
	FirFilter_344_s.COEFF[25] = 25.0 ; 
	FirFilter_344_s.COEFF[26] = 26.0 ; 
	FirFilter_344_s.COEFF[27] = 27.0 ; 
	FirFilter_344_s.COEFF[28] = 28.0 ; 
	FirFilter_344_s.COEFF[29] = 29.0 ; 
	FirFilter_344_s.COEFF[30] = 30.0 ; 
	FirFilter_344_s.COEFF[31] = 31.0 ; 
}
//--------------------------------
// --- init: FirFilter_345
	 {
	FirFilter_345_s.COEFF[0] = 0.0 ; 
	FirFilter_345_s.COEFF[1] = 1.0 ; 
	FirFilter_345_s.COEFF[2] = 2.0 ; 
	FirFilter_345_s.COEFF[3] = 3.0 ; 
	FirFilter_345_s.COEFF[4] = 4.0 ; 
	FirFilter_345_s.COEFF[5] = 5.0 ; 
	FirFilter_345_s.COEFF[6] = 6.0 ; 
	FirFilter_345_s.COEFF[7] = 7.0 ; 
	FirFilter_345_s.COEFF[8] = 8.0 ; 
	FirFilter_345_s.COEFF[9] = 9.0 ; 
	FirFilter_345_s.COEFF[10] = 10.0 ; 
	FirFilter_345_s.COEFF[11] = 11.0 ; 
	FirFilter_345_s.COEFF[12] = 12.0 ; 
	FirFilter_345_s.COEFF[13] = 13.0 ; 
	FirFilter_345_s.COEFF[14] = 14.0 ; 
	FirFilter_345_s.COEFF[15] = 15.0 ; 
	FirFilter_345_s.COEFF[16] = 16.0 ; 
	FirFilter_345_s.COEFF[17] = 17.0 ; 
	FirFilter_345_s.COEFF[18] = 18.0 ; 
	FirFilter_345_s.COEFF[19] = 19.0 ; 
	FirFilter_345_s.COEFF[20] = 20.0 ; 
	FirFilter_345_s.COEFF[21] = 21.0 ; 
	FirFilter_345_s.COEFF[22] = 22.0 ; 
	FirFilter_345_s.COEFF[23] = 23.0 ; 
	FirFilter_345_s.COEFF[24] = 24.0 ; 
	FirFilter_345_s.COEFF[25] = 25.0 ; 
	FirFilter_345_s.COEFF[26] = 26.0 ; 
	FirFilter_345_s.COEFF[27] = 27.0 ; 
	FirFilter_345_s.COEFF[28] = 28.0 ; 
	FirFilter_345_s.COEFF[29] = 29.0 ; 
	FirFilter_345_s.COEFF[30] = 30.0 ; 
	FirFilter_345_s.COEFF[31] = 31.0 ; 
}
//--------------------------------
// --- init: FirFilter_346
	 {
	FirFilter_346_s.COEFF[0] = 0.0 ; 
	FirFilter_346_s.COEFF[1] = 1.0 ; 
	FirFilter_346_s.COEFF[2] = 2.0 ; 
	FirFilter_346_s.COEFF[3] = 3.0 ; 
	FirFilter_346_s.COEFF[4] = 4.0 ; 
	FirFilter_346_s.COEFF[5] = 5.0 ; 
	FirFilter_346_s.COEFF[6] = 6.0 ; 
	FirFilter_346_s.COEFF[7] = 7.0 ; 
	FirFilter_346_s.COEFF[8] = 8.0 ; 
	FirFilter_346_s.COEFF[9] = 9.0 ; 
	FirFilter_346_s.COEFF[10] = 10.0 ; 
	FirFilter_346_s.COEFF[11] = 11.0 ; 
	FirFilter_346_s.COEFF[12] = 12.0 ; 
	FirFilter_346_s.COEFF[13] = 13.0 ; 
	FirFilter_346_s.COEFF[14] = 14.0 ; 
	FirFilter_346_s.COEFF[15] = 15.0 ; 
	FirFilter_346_s.COEFF[16] = 16.0 ; 
	FirFilter_346_s.COEFF[17] = 17.0 ; 
	FirFilter_346_s.COEFF[18] = 18.0 ; 
	FirFilter_346_s.COEFF[19] = 19.0 ; 
	FirFilter_346_s.COEFF[20] = 20.0 ; 
	FirFilter_346_s.COEFF[21] = 21.0 ; 
	FirFilter_346_s.COEFF[22] = 22.0 ; 
	FirFilter_346_s.COEFF[23] = 23.0 ; 
	FirFilter_346_s.COEFF[24] = 24.0 ; 
	FirFilter_346_s.COEFF[25] = 25.0 ; 
	FirFilter_346_s.COEFF[26] = 26.0 ; 
	FirFilter_346_s.COEFF[27] = 27.0 ; 
	FirFilter_346_s.COEFF[28] = 28.0 ; 
	FirFilter_346_s.COEFF[29] = 29.0 ; 
	FirFilter_346_s.COEFF[30] = 30.0 ; 
	FirFilter_346_s.COEFF[31] = 31.0 ; 
}
//--------------------------------
// --- init: FirFilter_347
	 {
	FirFilter_347_s.COEFF[0] = 0.0 ; 
	FirFilter_347_s.COEFF[1] = 1.0 ; 
	FirFilter_347_s.COEFF[2] = 2.0 ; 
	FirFilter_347_s.COEFF[3] = 3.0 ; 
	FirFilter_347_s.COEFF[4] = 4.0 ; 
	FirFilter_347_s.COEFF[5] = 5.0 ; 
	FirFilter_347_s.COEFF[6] = 6.0 ; 
	FirFilter_347_s.COEFF[7] = 7.0 ; 
	FirFilter_347_s.COEFF[8] = 8.0 ; 
	FirFilter_347_s.COEFF[9] = 9.0 ; 
	FirFilter_347_s.COEFF[10] = 10.0 ; 
	FirFilter_347_s.COEFF[11] = 11.0 ; 
	FirFilter_347_s.COEFF[12] = 12.0 ; 
	FirFilter_347_s.COEFF[13] = 13.0 ; 
	FirFilter_347_s.COEFF[14] = 14.0 ; 
	FirFilter_347_s.COEFF[15] = 15.0 ; 
	FirFilter_347_s.COEFF[16] = 16.0 ; 
	FirFilter_347_s.COEFF[17] = 17.0 ; 
	FirFilter_347_s.COEFF[18] = 18.0 ; 
	FirFilter_347_s.COEFF[19] = 19.0 ; 
	FirFilter_347_s.COEFF[20] = 20.0 ; 
	FirFilter_347_s.COEFF[21] = 21.0 ; 
	FirFilter_347_s.COEFF[22] = 22.0 ; 
	FirFilter_347_s.COEFF[23] = 23.0 ; 
	FirFilter_347_s.COEFF[24] = 24.0 ; 
	FirFilter_347_s.COEFF[25] = 25.0 ; 
	FirFilter_347_s.COEFF[26] = 26.0 ; 
	FirFilter_347_s.COEFF[27] = 27.0 ; 
	FirFilter_347_s.COEFF[28] = 28.0 ; 
	FirFilter_347_s.COEFF[29] = 29.0 ; 
	FirFilter_347_s.COEFF[30] = 30.0 ; 
	FirFilter_347_s.COEFF[31] = 31.0 ; 
}
//--------------------------------
// --- init: FirFilter_348
	 {
	FirFilter_348_s.COEFF[0] = 0.0 ; 
	FirFilter_348_s.COEFF[1] = 1.0 ; 
	FirFilter_348_s.COEFF[2] = 2.0 ; 
	FirFilter_348_s.COEFF[3] = 3.0 ; 
	FirFilter_348_s.COEFF[4] = 4.0 ; 
	FirFilter_348_s.COEFF[5] = 5.0 ; 
	FirFilter_348_s.COEFF[6] = 6.0 ; 
	FirFilter_348_s.COEFF[7] = 7.0 ; 
	FirFilter_348_s.COEFF[8] = 8.0 ; 
	FirFilter_348_s.COEFF[9] = 9.0 ; 
	FirFilter_348_s.COEFF[10] = 10.0 ; 
	FirFilter_348_s.COEFF[11] = 11.0 ; 
	FirFilter_348_s.COEFF[12] = 12.0 ; 
	FirFilter_348_s.COEFF[13] = 13.0 ; 
	FirFilter_348_s.COEFF[14] = 14.0 ; 
	FirFilter_348_s.COEFF[15] = 15.0 ; 
	FirFilter_348_s.COEFF[16] = 16.0 ; 
	FirFilter_348_s.COEFF[17] = 17.0 ; 
	FirFilter_348_s.COEFF[18] = 18.0 ; 
	FirFilter_348_s.COEFF[19] = 19.0 ; 
	FirFilter_348_s.COEFF[20] = 20.0 ; 
	FirFilter_348_s.COEFF[21] = 21.0 ; 
	FirFilter_348_s.COEFF[22] = 22.0 ; 
	FirFilter_348_s.COEFF[23] = 23.0 ; 
	FirFilter_348_s.COEFF[24] = 24.0 ; 
	FirFilter_348_s.COEFF[25] = 25.0 ; 
	FirFilter_348_s.COEFF[26] = 26.0 ; 
	FirFilter_348_s.COEFF[27] = 27.0 ; 
	FirFilter_348_s.COEFF[28] = 28.0 ; 
	FirFilter_348_s.COEFF[29] = 29.0 ; 
	FirFilter_348_s.COEFF[30] = 30.0 ; 
	FirFilter_348_s.COEFF[31] = 31.0 ; 
}
//--------------------------------
// --- init: FirFilter_349
	 {
	FirFilter_349_s.COEFF[0] = 0.0 ; 
	FirFilter_349_s.COEFF[1] = 1.0 ; 
	FirFilter_349_s.COEFF[2] = 2.0 ; 
	FirFilter_349_s.COEFF[3] = 3.0 ; 
	FirFilter_349_s.COEFF[4] = 4.0 ; 
	FirFilter_349_s.COEFF[5] = 5.0 ; 
	FirFilter_349_s.COEFF[6] = 6.0 ; 
	FirFilter_349_s.COEFF[7] = 7.0 ; 
	FirFilter_349_s.COEFF[8] = 8.0 ; 
	FirFilter_349_s.COEFF[9] = 9.0 ; 
	FirFilter_349_s.COEFF[10] = 10.0 ; 
	FirFilter_349_s.COEFF[11] = 11.0 ; 
	FirFilter_349_s.COEFF[12] = 12.0 ; 
	FirFilter_349_s.COEFF[13] = 13.0 ; 
	FirFilter_349_s.COEFF[14] = 14.0 ; 
	FirFilter_349_s.COEFF[15] = 15.0 ; 
	FirFilter_349_s.COEFF[16] = 16.0 ; 
	FirFilter_349_s.COEFF[17] = 17.0 ; 
	FirFilter_349_s.COEFF[18] = 18.0 ; 
	FirFilter_349_s.COEFF[19] = 19.0 ; 
	FirFilter_349_s.COEFF[20] = 20.0 ; 
	FirFilter_349_s.COEFF[21] = 21.0 ; 
	FirFilter_349_s.COEFF[22] = 22.0 ; 
	FirFilter_349_s.COEFF[23] = 23.0 ; 
	FirFilter_349_s.COEFF[24] = 24.0 ; 
	FirFilter_349_s.COEFF[25] = 25.0 ; 
	FirFilter_349_s.COEFF[26] = 26.0 ; 
	FirFilter_349_s.COEFF[27] = 27.0 ; 
	FirFilter_349_s.COEFF[28] = 28.0 ; 
	FirFilter_349_s.COEFF[29] = 29.0 ; 
	FirFilter_349_s.COEFF[30] = 30.0 ; 
	FirFilter_349_s.COEFF[31] = 31.0 ; 
}
//--------------------------------
// --- init: FirFilter_350
	 {
	FirFilter_350_s.COEFF[0] = 0.0 ; 
	FirFilter_350_s.COEFF[1] = 1.0 ; 
	FirFilter_350_s.COEFF[2] = 2.0 ; 
	FirFilter_350_s.COEFF[3] = 3.0 ; 
	FirFilter_350_s.COEFF[4] = 4.0 ; 
	FirFilter_350_s.COEFF[5] = 5.0 ; 
	FirFilter_350_s.COEFF[6] = 6.0 ; 
	FirFilter_350_s.COEFF[7] = 7.0 ; 
	FirFilter_350_s.COEFF[8] = 8.0 ; 
	FirFilter_350_s.COEFF[9] = 9.0 ; 
	FirFilter_350_s.COEFF[10] = 10.0 ; 
	FirFilter_350_s.COEFF[11] = 11.0 ; 
	FirFilter_350_s.COEFF[12] = 12.0 ; 
	FirFilter_350_s.COEFF[13] = 13.0 ; 
	FirFilter_350_s.COEFF[14] = 14.0 ; 
	FirFilter_350_s.COEFF[15] = 15.0 ; 
	FirFilter_350_s.COEFF[16] = 16.0 ; 
	FirFilter_350_s.COEFF[17] = 17.0 ; 
	FirFilter_350_s.COEFF[18] = 18.0 ; 
	FirFilter_350_s.COEFF[19] = 19.0 ; 
	FirFilter_350_s.COEFF[20] = 20.0 ; 
	FirFilter_350_s.COEFF[21] = 21.0 ; 
	FirFilter_350_s.COEFF[22] = 22.0 ; 
	FirFilter_350_s.COEFF[23] = 23.0 ; 
	FirFilter_350_s.COEFF[24] = 24.0 ; 
	FirFilter_350_s.COEFF[25] = 25.0 ; 
	FirFilter_350_s.COEFF[26] = 26.0 ; 
	FirFilter_350_s.COEFF[27] = 27.0 ; 
	FirFilter_350_s.COEFF[28] = 28.0 ; 
	FirFilter_350_s.COEFF[29] = 29.0 ; 
	FirFilter_350_s.COEFF[30] = 30.0 ; 
	FirFilter_350_s.COEFF[31] = 31.0 ; 
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_351
	FOR(uint32_t, __iter_init_, 0, <, 66, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&SplitJoin48_Delay_N_Fiss_647_681_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_229_293_641_676_split[1]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_353
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin48_Delay_N_Fiss_647_681_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_354
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin48_Delay_N_Fiss_647_681_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_355
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin48_Delay_N_Fiss_647_681_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_356
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin48_Delay_N_Fiss_647_681_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_357
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin48_Delay_N_Fiss_647_681_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_358
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin48_Delay_N_Fiss_647_681_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_359
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin48_Delay_N_Fiss_647_681_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_360
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin48_Delay_N_Fiss_647_681_join[7], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_352
	FOR(uint32_t, __iter_init_, 0, <, 37, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_352DUPLICATE_Splitter_361, pop_float(&SplitJoin48_Delay_N_Fiss_647_681_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_361
	FOR(uint32_t, __iter_init_, 0, <, 295, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_352DUPLICATE_Splitter_361);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin50_FirFilter_Fiss_648_682_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_363
	 {
	FirFilter_363_s.COEFF[0] = 11.0 ; 
	FirFilter_363_s.COEFF[1] = 44.0 ; 
	FirFilter_363_s.COEFF[2] = 77.0 ; 
	FirFilter_363_s.COEFF[3] = 110.0 ; 
	FirFilter_363_s.COEFF[4] = 143.0 ; 
	FirFilter_363_s.COEFF[5] = 176.0 ; 
	FirFilter_363_s.COEFF[6] = 209.0 ; 
	FirFilter_363_s.COEFF[7] = 242.0 ; 
	FirFilter_363_s.COEFF[8] = 275.0 ; 
	FirFilter_363_s.COEFF[9] = 308.0 ; 
	FirFilter_363_s.COEFF[10] = 341.0 ; 
	FirFilter_363_s.COEFF[11] = 374.0 ; 
	FirFilter_363_s.COEFF[12] = 407.0 ; 
	FirFilter_363_s.COEFF[13] = 440.0 ; 
	FirFilter_363_s.COEFF[14] = 473.0 ; 
	FirFilter_363_s.COEFF[15] = 506.0 ; 
	FirFilter_363_s.COEFF[16] = 539.0 ; 
	FirFilter_363_s.COEFF[17] = 572.0 ; 
	FirFilter_363_s.COEFF[18] = 605.0 ; 
	FirFilter_363_s.COEFF[19] = 638.0 ; 
	FirFilter_363_s.COEFF[20] = 671.0 ; 
	FirFilter_363_s.COEFF[21] = 704.0 ; 
	FirFilter_363_s.COEFF[22] = 737.0 ; 
	FirFilter_363_s.COEFF[23] = 770.0 ; 
	FirFilter_363_s.COEFF[24] = 803.0 ; 
	FirFilter_363_s.COEFF[25] = 836.0 ; 
	FirFilter_363_s.COEFF[26] = 869.0 ; 
	FirFilter_363_s.COEFF[27] = 902.0 ; 
	FirFilter_363_s.COEFF[28] = 935.0 ; 
	FirFilter_363_s.COEFF[29] = 968.0 ; 
	FirFilter_363_s.COEFF[30] = 1001.0 ; 
	FirFilter_363_s.COEFF[31] = 1034.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin50_FirFilter_Fiss_648_682_split[0], i) * FirFilter_363_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin50_FirFilter_Fiss_648_682_split[0]) ; 
		push_float(&SplitJoin50_FirFilter_Fiss_648_682_join[0], sum) ; 
 {
		FOR(int, streamItVar37, 0,  < , 7, streamItVar37++) {
			pop_void(&SplitJoin50_FirFilter_Fiss_648_682_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_364
	 {
	FirFilter_364_s.COEFF[0] = 11.0 ; 
	FirFilter_364_s.COEFF[1] = 44.0 ; 
	FirFilter_364_s.COEFF[2] = 77.0 ; 
	FirFilter_364_s.COEFF[3] = 110.0 ; 
	FirFilter_364_s.COEFF[4] = 143.0 ; 
	FirFilter_364_s.COEFF[5] = 176.0 ; 
	FirFilter_364_s.COEFF[6] = 209.0 ; 
	FirFilter_364_s.COEFF[7] = 242.0 ; 
	FirFilter_364_s.COEFF[8] = 275.0 ; 
	FirFilter_364_s.COEFF[9] = 308.0 ; 
	FirFilter_364_s.COEFF[10] = 341.0 ; 
	FirFilter_364_s.COEFF[11] = 374.0 ; 
	FirFilter_364_s.COEFF[12] = 407.0 ; 
	FirFilter_364_s.COEFF[13] = 440.0 ; 
	FirFilter_364_s.COEFF[14] = 473.0 ; 
	FirFilter_364_s.COEFF[15] = 506.0 ; 
	FirFilter_364_s.COEFF[16] = 539.0 ; 
	FirFilter_364_s.COEFF[17] = 572.0 ; 
	FirFilter_364_s.COEFF[18] = 605.0 ; 
	FirFilter_364_s.COEFF[19] = 638.0 ; 
	FirFilter_364_s.COEFF[20] = 671.0 ; 
	FirFilter_364_s.COEFF[21] = 704.0 ; 
	FirFilter_364_s.COEFF[22] = 737.0 ; 
	FirFilter_364_s.COEFF[23] = 770.0 ; 
	FirFilter_364_s.COEFF[24] = 803.0 ; 
	FirFilter_364_s.COEFF[25] = 836.0 ; 
	FirFilter_364_s.COEFF[26] = 869.0 ; 
	FirFilter_364_s.COEFF[27] = 902.0 ; 
	FirFilter_364_s.COEFF[28] = 935.0 ; 
	FirFilter_364_s.COEFF[29] = 968.0 ; 
	FirFilter_364_s.COEFF[30] = 1001.0 ; 
	FirFilter_364_s.COEFF[31] = 1034.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin50_FirFilter_Fiss_648_682_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin50_FirFilter_Fiss_648_682_split[1], i) * FirFilter_364_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin50_FirFilter_Fiss_648_682_split[1]) ; 
		push_float(&SplitJoin50_FirFilter_Fiss_648_682_join[1], sum) ; 
 {
		FOR(int, streamItVar38, 0,  < , 6, streamItVar38++) {
			pop_void(&SplitJoin50_FirFilter_Fiss_648_682_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_365
	 {
	FirFilter_365_s.COEFF[0] = 11.0 ; 
	FirFilter_365_s.COEFF[1] = 44.0 ; 
	FirFilter_365_s.COEFF[2] = 77.0 ; 
	FirFilter_365_s.COEFF[3] = 110.0 ; 
	FirFilter_365_s.COEFF[4] = 143.0 ; 
	FirFilter_365_s.COEFF[5] = 176.0 ; 
	FirFilter_365_s.COEFF[6] = 209.0 ; 
	FirFilter_365_s.COEFF[7] = 242.0 ; 
	FirFilter_365_s.COEFF[8] = 275.0 ; 
	FirFilter_365_s.COEFF[9] = 308.0 ; 
	FirFilter_365_s.COEFF[10] = 341.0 ; 
	FirFilter_365_s.COEFF[11] = 374.0 ; 
	FirFilter_365_s.COEFF[12] = 407.0 ; 
	FirFilter_365_s.COEFF[13] = 440.0 ; 
	FirFilter_365_s.COEFF[14] = 473.0 ; 
	FirFilter_365_s.COEFF[15] = 506.0 ; 
	FirFilter_365_s.COEFF[16] = 539.0 ; 
	FirFilter_365_s.COEFF[17] = 572.0 ; 
	FirFilter_365_s.COEFF[18] = 605.0 ; 
	FirFilter_365_s.COEFF[19] = 638.0 ; 
	FirFilter_365_s.COEFF[20] = 671.0 ; 
	FirFilter_365_s.COEFF[21] = 704.0 ; 
	FirFilter_365_s.COEFF[22] = 737.0 ; 
	FirFilter_365_s.COEFF[23] = 770.0 ; 
	FirFilter_365_s.COEFF[24] = 803.0 ; 
	FirFilter_365_s.COEFF[25] = 836.0 ; 
	FirFilter_365_s.COEFF[26] = 869.0 ; 
	FirFilter_365_s.COEFF[27] = 902.0 ; 
	FirFilter_365_s.COEFF[28] = 935.0 ; 
	FirFilter_365_s.COEFF[29] = 968.0 ; 
	FirFilter_365_s.COEFF[30] = 1001.0 ; 
	FirFilter_365_s.COEFF[31] = 1034.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar39, 0,  < , 2, streamItVar39++) {
			pop_void(&SplitJoin50_FirFilter_Fiss_648_682_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin50_FirFilter_Fiss_648_682_split[2], i) * FirFilter_365_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin50_FirFilter_Fiss_648_682_split[2]) ; 
		push_float(&SplitJoin50_FirFilter_Fiss_648_682_join[2], sum) ; 
 {
		FOR(int, streamItVar40, 0,  < , 5, streamItVar40++) {
			pop_void(&SplitJoin50_FirFilter_Fiss_648_682_split[2]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_366
	 {
	FirFilter_366_s.COEFF[0] = 11.0 ; 
	FirFilter_366_s.COEFF[1] = 44.0 ; 
	FirFilter_366_s.COEFF[2] = 77.0 ; 
	FirFilter_366_s.COEFF[3] = 110.0 ; 
	FirFilter_366_s.COEFF[4] = 143.0 ; 
	FirFilter_366_s.COEFF[5] = 176.0 ; 
	FirFilter_366_s.COEFF[6] = 209.0 ; 
	FirFilter_366_s.COEFF[7] = 242.0 ; 
	FirFilter_366_s.COEFF[8] = 275.0 ; 
	FirFilter_366_s.COEFF[9] = 308.0 ; 
	FirFilter_366_s.COEFF[10] = 341.0 ; 
	FirFilter_366_s.COEFF[11] = 374.0 ; 
	FirFilter_366_s.COEFF[12] = 407.0 ; 
	FirFilter_366_s.COEFF[13] = 440.0 ; 
	FirFilter_366_s.COEFF[14] = 473.0 ; 
	FirFilter_366_s.COEFF[15] = 506.0 ; 
	FirFilter_366_s.COEFF[16] = 539.0 ; 
	FirFilter_366_s.COEFF[17] = 572.0 ; 
	FirFilter_366_s.COEFF[18] = 605.0 ; 
	FirFilter_366_s.COEFF[19] = 638.0 ; 
	FirFilter_366_s.COEFF[20] = 671.0 ; 
	FirFilter_366_s.COEFF[21] = 704.0 ; 
	FirFilter_366_s.COEFF[22] = 737.0 ; 
	FirFilter_366_s.COEFF[23] = 770.0 ; 
	FirFilter_366_s.COEFF[24] = 803.0 ; 
	FirFilter_366_s.COEFF[25] = 836.0 ; 
	FirFilter_366_s.COEFF[26] = 869.0 ; 
	FirFilter_366_s.COEFF[27] = 902.0 ; 
	FirFilter_366_s.COEFF[28] = 935.0 ; 
	FirFilter_366_s.COEFF[29] = 968.0 ; 
	FirFilter_366_s.COEFF[30] = 1001.0 ; 
	FirFilter_366_s.COEFF[31] = 1034.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar41, 0,  < , 3, streamItVar41++) {
			pop_void(&SplitJoin50_FirFilter_Fiss_648_682_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin50_FirFilter_Fiss_648_682_split[3], i) * FirFilter_366_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin50_FirFilter_Fiss_648_682_split[3]) ; 
		push_float(&SplitJoin50_FirFilter_Fiss_648_682_join[3], sum) ; 
 {
		FOR(int, streamItVar42, 0,  < , 4, streamItVar42++) {
			pop_void(&SplitJoin50_FirFilter_Fiss_648_682_split[3]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_367
	 {
	FirFilter_367_s.COEFF[0] = 11.0 ; 
	FirFilter_367_s.COEFF[1] = 44.0 ; 
	FirFilter_367_s.COEFF[2] = 77.0 ; 
	FirFilter_367_s.COEFF[3] = 110.0 ; 
	FirFilter_367_s.COEFF[4] = 143.0 ; 
	FirFilter_367_s.COEFF[5] = 176.0 ; 
	FirFilter_367_s.COEFF[6] = 209.0 ; 
	FirFilter_367_s.COEFF[7] = 242.0 ; 
	FirFilter_367_s.COEFF[8] = 275.0 ; 
	FirFilter_367_s.COEFF[9] = 308.0 ; 
	FirFilter_367_s.COEFF[10] = 341.0 ; 
	FirFilter_367_s.COEFF[11] = 374.0 ; 
	FirFilter_367_s.COEFF[12] = 407.0 ; 
	FirFilter_367_s.COEFF[13] = 440.0 ; 
	FirFilter_367_s.COEFF[14] = 473.0 ; 
	FirFilter_367_s.COEFF[15] = 506.0 ; 
	FirFilter_367_s.COEFF[16] = 539.0 ; 
	FirFilter_367_s.COEFF[17] = 572.0 ; 
	FirFilter_367_s.COEFF[18] = 605.0 ; 
	FirFilter_367_s.COEFF[19] = 638.0 ; 
	FirFilter_367_s.COEFF[20] = 671.0 ; 
	FirFilter_367_s.COEFF[21] = 704.0 ; 
	FirFilter_367_s.COEFF[22] = 737.0 ; 
	FirFilter_367_s.COEFF[23] = 770.0 ; 
	FirFilter_367_s.COEFF[24] = 803.0 ; 
	FirFilter_367_s.COEFF[25] = 836.0 ; 
	FirFilter_367_s.COEFF[26] = 869.0 ; 
	FirFilter_367_s.COEFF[27] = 902.0 ; 
	FirFilter_367_s.COEFF[28] = 935.0 ; 
	FirFilter_367_s.COEFF[29] = 968.0 ; 
	FirFilter_367_s.COEFF[30] = 1001.0 ; 
	FirFilter_367_s.COEFF[31] = 1034.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar43, 0,  < , 4, streamItVar43++) {
			pop_void(&SplitJoin50_FirFilter_Fiss_648_682_split[4]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin50_FirFilter_Fiss_648_682_split[4], i) * FirFilter_367_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin50_FirFilter_Fiss_648_682_split[4]) ; 
		push_float(&SplitJoin50_FirFilter_Fiss_648_682_join[4], sum) ; 
 {
		FOR(int, streamItVar44, 0,  < , 3, streamItVar44++) {
			pop_void(&SplitJoin50_FirFilter_Fiss_648_682_split[4]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_368
	 {
	FirFilter_368_s.COEFF[0] = 11.0 ; 
	FirFilter_368_s.COEFF[1] = 44.0 ; 
	FirFilter_368_s.COEFF[2] = 77.0 ; 
	FirFilter_368_s.COEFF[3] = 110.0 ; 
	FirFilter_368_s.COEFF[4] = 143.0 ; 
	FirFilter_368_s.COEFF[5] = 176.0 ; 
	FirFilter_368_s.COEFF[6] = 209.0 ; 
	FirFilter_368_s.COEFF[7] = 242.0 ; 
	FirFilter_368_s.COEFF[8] = 275.0 ; 
	FirFilter_368_s.COEFF[9] = 308.0 ; 
	FirFilter_368_s.COEFF[10] = 341.0 ; 
	FirFilter_368_s.COEFF[11] = 374.0 ; 
	FirFilter_368_s.COEFF[12] = 407.0 ; 
	FirFilter_368_s.COEFF[13] = 440.0 ; 
	FirFilter_368_s.COEFF[14] = 473.0 ; 
	FirFilter_368_s.COEFF[15] = 506.0 ; 
	FirFilter_368_s.COEFF[16] = 539.0 ; 
	FirFilter_368_s.COEFF[17] = 572.0 ; 
	FirFilter_368_s.COEFF[18] = 605.0 ; 
	FirFilter_368_s.COEFF[19] = 638.0 ; 
	FirFilter_368_s.COEFF[20] = 671.0 ; 
	FirFilter_368_s.COEFF[21] = 704.0 ; 
	FirFilter_368_s.COEFF[22] = 737.0 ; 
	FirFilter_368_s.COEFF[23] = 770.0 ; 
	FirFilter_368_s.COEFF[24] = 803.0 ; 
	FirFilter_368_s.COEFF[25] = 836.0 ; 
	FirFilter_368_s.COEFF[26] = 869.0 ; 
	FirFilter_368_s.COEFF[27] = 902.0 ; 
	FirFilter_368_s.COEFF[28] = 935.0 ; 
	FirFilter_368_s.COEFF[29] = 968.0 ; 
	FirFilter_368_s.COEFF[30] = 1001.0 ; 
	FirFilter_368_s.COEFF[31] = 1034.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar45, 0,  < , 5, streamItVar45++) {
			pop_void(&SplitJoin50_FirFilter_Fiss_648_682_split[5]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin50_FirFilter_Fiss_648_682_split[5], i) * FirFilter_368_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin50_FirFilter_Fiss_648_682_split[5]) ; 
		push_float(&SplitJoin50_FirFilter_Fiss_648_682_join[5], sum) ; 
 {
		FOR(int, streamItVar46, 0,  < , 2, streamItVar46++) {
			pop_void(&SplitJoin50_FirFilter_Fiss_648_682_split[5]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_369
	 {
	FirFilter_369_s.COEFF[0] = 11.0 ; 
	FirFilter_369_s.COEFF[1] = 44.0 ; 
	FirFilter_369_s.COEFF[2] = 77.0 ; 
	FirFilter_369_s.COEFF[3] = 110.0 ; 
	FirFilter_369_s.COEFF[4] = 143.0 ; 
	FirFilter_369_s.COEFF[5] = 176.0 ; 
	FirFilter_369_s.COEFF[6] = 209.0 ; 
	FirFilter_369_s.COEFF[7] = 242.0 ; 
	FirFilter_369_s.COEFF[8] = 275.0 ; 
	FirFilter_369_s.COEFF[9] = 308.0 ; 
	FirFilter_369_s.COEFF[10] = 341.0 ; 
	FirFilter_369_s.COEFF[11] = 374.0 ; 
	FirFilter_369_s.COEFF[12] = 407.0 ; 
	FirFilter_369_s.COEFF[13] = 440.0 ; 
	FirFilter_369_s.COEFF[14] = 473.0 ; 
	FirFilter_369_s.COEFF[15] = 506.0 ; 
	FirFilter_369_s.COEFF[16] = 539.0 ; 
	FirFilter_369_s.COEFF[17] = 572.0 ; 
	FirFilter_369_s.COEFF[18] = 605.0 ; 
	FirFilter_369_s.COEFF[19] = 638.0 ; 
	FirFilter_369_s.COEFF[20] = 671.0 ; 
	FirFilter_369_s.COEFF[21] = 704.0 ; 
	FirFilter_369_s.COEFF[22] = 737.0 ; 
	FirFilter_369_s.COEFF[23] = 770.0 ; 
	FirFilter_369_s.COEFF[24] = 803.0 ; 
	FirFilter_369_s.COEFF[25] = 836.0 ; 
	FirFilter_369_s.COEFF[26] = 869.0 ; 
	FirFilter_369_s.COEFF[27] = 902.0 ; 
	FirFilter_369_s.COEFF[28] = 935.0 ; 
	FirFilter_369_s.COEFF[29] = 968.0 ; 
	FirFilter_369_s.COEFF[30] = 1001.0 ; 
	FirFilter_369_s.COEFF[31] = 1034.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar47, 0,  < , 6, streamItVar47++) {
			pop_void(&SplitJoin50_FirFilter_Fiss_648_682_split[6]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin50_FirFilter_Fiss_648_682_split[6], i) * FirFilter_369_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin50_FirFilter_Fiss_648_682_split[6]) ; 
		push_float(&SplitJoin50_FirFilter_Fiss_648_682_join[6], sum) ; 
 {
		pop_void(&SplitJoin50_FirFilter_Fiss_648_682_split[6]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_370
	 {
	FirFilter_370_s.COEFF[0] = 11.0 ; 
	FirFilter_370_s.COEFF[1] = 44.0 ; 
	FirFilter_370_s.COEFF[2] = 77.0 ; 
	FirFilter_370_s.COEFF[3] = 110.0 ; 
	FirFilter_370_s.COEFF[4] = 143.0 ; 
	FirFilter_370_s.COEFF[5] = 176.0 ; 
	FirFilter_370_s.COEFF[6] = 209.0 ; 
	FirFilter_370_s.COEFF[7] = 242.0 ; 
	FirFilter_370_s.COEFF[8] = 275.0 ; 
	FirFilter_370_s.COEFF[9] = 308.0 ; 
	FirFilter_370_s.COEFF[10] = 341.0 ; 
	FirFilter_370_s.COEFF[11] = 374.0 ; 
	FirFilter_370_s.COEFF[12] = 407.0 ; 
	FirFilter_370_s.COEFF[13] = 440.0 ; 
	FirFilter_370_s.COEFF[14] = 473.0 ; 
	FirFilter_370_s.COEFF[15] = 506.0 ; 
	FirFilter_370_s.COEFF[16] = 539.0 ; 
	FirFilter_370_s.COEFF[17] = 572.0 ; 
	FirFilter_370_s.COEFF[18] = 605.0 ; 
	FirFilter_370_s.COEFF[19] = 638.0 ; 
	FirFilter_370_s.COEFF[20] = 671.0 ; 
	FirFilter_370_s.COEFF[21] = 704.0 ; 
	FirFilter_370_s.COEFF[22] = 737.0 ; 
	FirFilter_370_s.COEFF[23] = 770.0 ; 
	FirFilter_370_s.COEFF[24] = 803.0 ; 
	FirFilter_370_s.COEFF[25] = 836.0 ; 
	FirFilter_370_s.COEFF[26] = 869.0 ; 
	FirFilter_370_s.COEFF[27] = 902.0 ; 
	FirFilter_370_s.COEFF[28] = 935.0 ; 
	FirFilter_370_s.COEFF[29] = 968.0 ; 
	FirFilter_370_s.COEFF[30] = 1001.0 ; 
	FirFilter_370_s.COEFF[31] = 1034.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar48, 0,  < , 7, streamItVar48++) {
			pop_void(&SplitJoin50_FirFilter_Fiss_648_682_split[7]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin50_FirFilter_Fiss_648_682_split[7], i) * FirFilter_370_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin50_FirFilter_Fiss_648_682_split[7]) ; 
		push_float(&SplitJoin50_FirFilter_Fiss_648_682_join[7], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_362
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_362DownSamp_243, pop_float(&SplitJoin50_FirFilter_Fiss_648_682_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DownSamp_243
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		push_float(&DownSamp_243UpSamp_244, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_362DownSamp_243)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_362DownSamp_243) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: UpSamp_244
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		push_float(&UpSamp_244WEIGHTED_ROUND_ROBIN_Splitter_371, pop_float(&DownSamp_243UpSamp_244)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			push_float(&UpSamp_244WEIGHTED_ROUND_ROBIN_Splitter_371, 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_371
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&SplitJoin52_Delay_N_Fiss_649_683_split[__iter_], pop_float(&UpSamp_244WEIGHTED_ROUND_ROBIN_Splitter_371));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_373
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin52_Delay_N_Fiss_649_683_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_374
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin52_Delay_N_Fiss_649_683_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_375
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin52_Delay_N_Fiss_649_683_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_376
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin52_Delay_N_Fiss_649_683_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_377
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin52_Delay_N_Fiss_649_683_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_378
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin52_Delay_N_Fiss_649_683_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_379
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin52_Delay_N_Fiss_649_683_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_380
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin52_Delay_N_Fiss_649_683_join[7], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_372
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_372DUPLICATE_Splitter_381, pop_float(&SplitJoin52_Delay_N_Fiss_649_683_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_381
	FOR(uint32_t, __iter_init_, 0, <, 31, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_372DUPLICATE_Splitter_381);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin54_FirFilter_Fiss_650_684_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_383
	 {
	FirFilter_383_s.COEFF[0] = 2.0 ; 
	FirFilter_383_s.COEFF[1] = 4.0 ; 
	FirFilter_383_s.COEFF[2] = 6.0 ; 
	FirFilter_383_s.COEFF[3] = 8.0 ; 
	FirFilter_383_s.COEFF[4] = 10.0 ; 
	FirFilter_383_s.COEFF[5] = 12.0 ; 
	FirFilter_383_s.COEFF[6] = 14.0 ; 
	FirFilter_383_s.COEFF[7] = 16.0 ; 
	FirFilter_383_s.COEFF[8] = 18.0 ; 
	FirFilter_383_s.COEFF[9] = 20.0 ; 
	FirFilter_383_s.COEFF[10] = 22.0 ; 
	FirFilter_383_s.COEFF[11] = 24.0 ; 
	FirFilter_383_s.COEFF[12] = 26.0 ; 
	FirFilter_383_s.COEFF[13] = 28.0 ; 
	FirFilter_383_s.COEFF[14] = 30.0 ; 
	FirFilter_383_s.COEFF[15] = 32.0 ; 
	FirFilter_383_s.COEFF[16] = 34.0 ; 
	FirFilter_383_s.COEFF[17] = 36.0 ; 
	FirFilter_383_s.COEFF[18] = 38.0 ; 
	FirFilter_383_s.COEFF[19] = 40.0 ; 
	FirFilter_383_s.COEFF[20] = 42.0 ; 
	FirFilter_383_s.COEFF[21] = 44.0 ; 
	FirFilter_383_s.COEFF[22] = 46.0 ; 
	FirFilter_383_s.COEFF[23] = 48.0 ; 
	FirFilter_383_s.COEFF[24] = 50.0 ; 
	FirFilter_383_s.COEFF[25] = 52.0 ; 
	FirFilter_383_s.COEFF[26] = 54.0 ; 
	FirFilter_383_s.COEFF[27] = 56.0 ; 
	FirFilter_383_s.COEFF[28] = 58.0 ; 
	FirFilter_383_s.COEFF[29] = 60.0 ; 
	FirFilter_383_s.COEFF[30] = 62.0 ; 
	FirFilter_383_s.COEFF[31] = 64.0 ; 
}
//--------------------------------
// --- init: FirFilter_384
	 {
	FirFilter_384_s.COEFF[0] = 2.0 ; 
	FirFilter_384_s.COEFF[1] = 4.0 ; 
	FirFilter_384_s.COEFF[2] = 6.0 ; 
	FirFilter_384_s.COEFF[3] = 8.0 ; 
	FirFilter_384_s.COEFF[4] = 10.0 ; 
	FirFilter_384_s.COEFF[5] = 12.0 ; 
	FirFilter_384_s.COEFF[6] = 14.0 ; 
	FirFilter_384_s.COEFF[7] = 16.0 ; 
	FirFilter_384_s.COEFF[8] = 18.0 ; 
	FirFilter_384_s.COEFF[9] = 20.0 ; 
	FirFilter_384_s.COEFF[10] = 22.0 ; 
	FirFilter_384_s.COEFF[11] = 24.0 ; 
	FirFilter_384_s.COEFF[12] = 26.0 ; 
	FirFilter_384_s.COEFF[13] = 28.0 ; 
	FirFilter_384_s.COEFF[14] = 30.0 ; 
	FirFilter_384_s.COEFF[15] = 32.0 ; 
	FirFilter_384_s.COEFF[16] = 34.0 ; 
	FirFilter_384_s.COEFF[17] = 36.0 ; 
	FirFilter_384_s.COEFF[18] = 38.0 ; 
	FirFilter_384_s.COEFF[19] = 40.0 ; 
	FirFilter_384_s.COEFF[20] = 42.0 ; 
	FirFilter_384_s.COEFF[21] = 44.0 ; 
	FirFilter_384_s.COEFF[22] = 46.0 ; 
	FirFilter_384_s.COEFF[23] = 48.0 ; 
	FirFilter_384_s.COEFF[24] = 50.0 ; 
	FirFilter_384_s.COEFF[25] = 52.0 ; 
	FirFilter_384_s.COEFF[26] = 54.0 ; 
	FirFilter_384_s.COEFF[27] = 56.0 ; 
	FirFilter_384_s.COEFF[28] = 58.0 ; 
	FirFilter_384_s.COEFF[29] = 60.0 ; 
	FirFilter_384_s.COEFF[30] = 62.0 ; 
	FirFilter_384_s.COEFF[31] = 64.0 ; 
}
//--------------------------------
// --- init: FirFilter_385
	 {
	FirFilter_385_s.COEFF[0] = 2.0 ; 
	FirFilter_385_s.COEFF[1] = 4.0 ; 
	FirFilter_385_s.COEFF[2] = 6.0 ; 
	FirFilter_385_s.COEFF[3] = 8.0 ; 
	FirFilter_385_s.COEFF[4] = 10.0 ; 
	FirFilter_385_s.COEFF[5] = 12.0 ; 
	FirFilter_385_s.COEFF[6] = 14.0 ; 
	FirFilter_385_s.COEFF[7] = 16.0 ; 
	FirFilter_385_s.COEFF[8] = 18.0 ; 
	FirFilter_385_s.COEFF[9] = 20.0 ; 
	FirFilter_385_s.COEFF[10] = 22.0 ; 
	FirFilter_385_s.COEFF[11] = 24.0 ; 
	FirFilter_385_s.COEFF[12] = 26.0 ; 
	FirFilter_385_s.COEFF[13] = 28.0 ; 
	FirFilter_385_s.COEFF[14] = 30.0 ; 
	FirFilter_385_s.COEFF[15] = 32.0 ; 
	FirFilter_385_s.COEFF[16] = 34.0 ; 
	FirFilter_385_s.COEFF[17] = 36.0 ; 
	FirFilter_385_s.COEFF[18] = 38.0 ; 
	FirFilter_385_s.COEFF[19] = 40.0 ; 
	FirFilter_385_s.COEFF[20] = 42.0 ; 
	FirFilter_385_s.COEFF[21] = 44.0 ; 
	FirFilter_385_s.COEFF[22] = 46.0 ; 
	FirFilter_385_s.COEFF[23] = 48.0 ; 
	FirFilter_385_s.COEFF[24] = 50.0 ; 
	FirFilter_385_s.COEFF[25] = 52.0 ; 
	FirFilter_385_s.COEFF[26] = 54.0 ; 
	FirFilter_385_s.COEFF[27] = 56.0 ; 
	FirFilter_385_s.COEFF[28] = 58.0 ; 
	FirFilter_385_s.COEFF[29] = 60.0 ; 
	FirFilter_385_s.COEFF[30] = 62.0 ; 
	FirFilter_385_s.COEFF[31] = 64.0 ; 
}
//--------------------------------
// --- init: FirFilter_386
	 {
	FirFilter_386_s.COEFF[0] = 2.0 ; 
	FirFilter_386_s.COEFF[1] = 4.0 ; 
	FirFilter_386_s.COEFF[2] = 6.0 ; 
	FirFilter_386_s.COEFF[3] = 8.0 ; 
	FirFilter_386_s.COEFF[4] = 10.0 ; 
	FirFilter_386_s.COEFF[5] = 12.0 ; 
	FirFilter_386_s.COEFF[6] = 14.0 ; 
	FirFilter_386_s.COEFF[7] = 16.0 ; 
	FirFilter_386_s.COEFF[8] = 18.0 ; 
	FirFilter_386_s.COEFF[9] = 20.0 ; 
	FirFilter_386_s.COEFF[10] = 22.0 ; 
	FirFilter_386_s.COEFF[11] = 24.0 ; 
	FirFilter_386_s.COEFF[12] = 26.0 ; 
	FirFilter_386_s.COEFF[13] = 28.0 ; 
	FirFilter_386_s.COEFF[14] = 30.0 ; 
	FirFilter_386_s.COEFF[15] = 32.0 ; 
	FirFilter_386_s.COEFF[16] = 34.0 ; 
	FirFilter_386_s.COEFF[17] = 36.0 ; 
	FirFilter_386_s.COEFF[18] = 38.0 ; 
	FirFilter_386_s.COEFF[19] = 40.0 ; 
	FirFilter_386_s.COEFF[20] = 42.0 ; 
	FirFilter_386_s.COEFF[21] = 44.0 ; 
	FirFilter_386_s.COEFF[22] = 46.0 ; 
	FirFilter_386_s.COEFF[23] = 48.0 ; 
	FirFilter_386_s.COEFF[24] = 50.0 ; 
	FirFilter_386_s.COEFF[25] = 52.0 ; 
	FirFilter_386_s.COEFF[26] = 54.0 ; 
	FirFilter_386_s.COEFF[27] = 56.0 ; 
	FirFilter_386_s.COEFF[28] = 58.0 ; 
	FirFilter_386_s.COEFF[29] = 60.0 ; 
	FirFilter_386_s.COEFF[30] = 62.0 ; 
	FirFilter_386_s.COEFF[31] = 64.0 ; 
}
//--------------------------------
// --- init: FirFilter_387
	 {
	FirFilter_387_s.COEFF[0] = 2.0 ; 
	FirFilter_387_s.COEFF[1] = 4.0 ; 
	FirFilter_387_s.COEFF[2] = 6.0 ; 
	FirFilter_387_s.COEFF[3] = 8.0 ; 
	FirFilter_387_s.COEFF[4] = 10.0 ; 
	FirFilter_387_s.COEFF[5] = 12.0 ; 
	FirFilter_387_s.COEFF[6] = 14.0 ; 
	FirFilter_387_s.COEFF[7] = 16.0 ; 
	FirFilter_387_s.COEFF[8] = 18.0 ; 
	FirFilter_387_s.COEFF[9] = 20.0 ; 
	FirFilter_387_s.COEFF[10] = 22.0 ; 
	FirFilter_387_s.COEFF[11] = 24.0 ; 
	FirFilter_387_s.COEFF[12] = 26.0 ; 
	FirFilter_387_s.COEFF[13] = 28.0 ; 
	FirFilter_387_s.COEFF[14] = 30.0 ; 
	FirFilter_387_s.COEFF[15] = 32.0 ; 
	FirFilter_387_s.COEFF[16] = 34.0 ; 
	FirFilter_387_s.COEFF[17] = 36.0 ; 
	FirFilter_387_s.COEFF[18] = 38.0 ; 
	FirFilter_387_s.COEFF[19] = 40.0 ; 
	FirFilter_387_s.COEFF[20] = 42.0 ; 
	FirFilter_387_s.COEFF[21] = 44.0 ; 
	FirFilter_387_s.COEFF[22] = 46.0 ; 
	FirFilter_387_s.COEFF[23] = 48.0 ; 
	FirFilter_387_s.COEFF[24] = 50.0 ; 
	FirFilter_387_s.COEFF[25] = 52.0 ; 
	FirFilter_387_s.COEFF[26] = 54.0 ; 
	FirFilter_387_s.COEFF[27] = 56.0 ; 
	FirFilter_387_s.COEFF[28] = 58.0 ; 
	FirFilter_387_s.COEFF[29] = 60.0 ; 
	FirFilter_387_s.COEFF[30] = 62.0 ; 
	FirFilter_387_s.COEFF[31] = 64.0 ; 
}
//--------------------------------
// --- init: FirFilter_388
	 {
	FirFilter_388_s.COEFF[0] = 2.0 ; 
	FirFilter_388_s.COEFF[1] = 4.0 ; 
	FirFilter_388_s.COEFF[2] = 6.0 ; 
	FirFilter_388_s.COEFF[3] = 8.0 ; 
	FirFilter_388_s.COEFF[4] = 10.0 ; 
	FirFilter_388_s.COEFF[5] = 12.0 ; 
	FirFilter_388_s.COEFF[6] = 14.0 ; 
	FirFilter_388_s.COEFF[7] = 16.0 ; 
	FirFilter_388_s.COEFF[8] = 18.0 ; 
	FirFilter_388_s.COEFF[9] = 20.0 ; 
	FirFilter_388_s.COEFF[10] = 22.0 ; 
	FirFilter_388_s.COEFF[11] = 24.0 ; 
	FirFilter_388_s.COEFF[12] = 26.0 ; 
	FirFilter_388_s.COEFF[13] = 28.0 ; 
	FirFilter_388_s.COEFF[14] = 30.0 ; 
	FirFilter_388_s.COEFF[15] = 32.0 ; 
	FirFilter_388_s.COEFF[16] = 34.0 ; 
	FirFilter_388_s.COEFF[17] = 36.0 ; 
	FirFilter_388_s.COEFF[18] = 38.0 ; 
	FirFilter_388_s.COEFF[19] = 40.0 ; 
	FirFilter_388_s.COEFF[20] = 42.0 ; 
	FirFilter_388_s.COEFF[21] = 44.0 ; 
	FirFilter_388_s.COEFF[22] = 46.0 ; 
	FirFilter_388_s.COEFF[23] = 48.0 ; 
	FirFilter_388_s.COEFF[24] = 50.0 ; 
	FirFilter_388_s.COEFF[25] = 52.0 ; 
	FirFilter_388_s.COEFF[26] = 54.0 ; 
	FirFilter_388_s.COEFF[27] = 56.0 ; 
	FirFilter_388_s.COEFF[28] = 58.0 ; 
	FirFilter_388_s.COEFF[29] = 60.0 ; 
	FirFilter_388_s.COEFF[30] = 62.0 ; 
	FirFilter_388_s.COEFF[31] = 64.0 ; 
}
//--------------------------------
// --- init: FirFilter_389
	 {
	FirFilter_389_s.COEFF[0] = 2.0 ; 
	FirFilter_389_s.COEFF[1] = 4.0 ; 
	FirFilter_389_s.COEFF[2] = 6.0 ; 
	FirFilter_389_s.COEFF[3] = 8.0 ; 
	FirFilter_389_s.COEFF[4] = 10.0 ; 
	FirFilter_389_s.COEFF[5] = 12.0 ; 
	FirFilter_389_s.COEFF[6] = 14.0 ; 
	FirFilter_389_s.COEFF[7] = 16.0 ; 
	FirFilter_389_s.COEFF[8] = 18.0 ; 
	FirFilter_389_s.COEFF[9] = 20.0 ; 
	FirFilter_389_s.COEFF[10] = 22.0 ; 
	FirFilter_389_s.COEFF[11] = 24.0 ; 
	FirFilter_389_s.COEFF[12] = 26.0 ; 
	FirFilter_389_s.COEFF[13] = 28.0 ; 
	FirFilter_389_s.COEFF[14] = 30.0 ; 
	FirFilter_389_s.COEFF[15] = 32.0 ; 
	FirFilter_389_s.COEFF[16] = 34.0 ; 
	FirFilter_389_s.COEFF[17] = 36.0 ; 
	FirFilter_389_s.COEFF[18] = 38.0 ; 
	FirFilter_389_s.COEFF[19] = 40.0 ; 
	FirFilter_389_s.COEFF[20] = 42.0 ; 
	FirFilter_389_s.COEFF[21] = 44.0 ; 
	FirFilter_389_s.COEFF[22] = 46.0 ; 
	FirFilter_389_s.COEFF[23] = 48.0 ; 
	FirFilter_389_s.COEFF[24] = 50.0 ; 
	FirFilter_389_s.COEFF[25] = 52.0 ; 
	FirFilter_389_s.COEFF[26] = 54.0 ; 
	FirFilter_389_s.COEFF[27] = 56.0 ; 
	FirFilter_389_s.COEFF[28] = 58.0 ; 
	FirFilter_389_s.COEFF[29] = 60.0 ; 
	FirFilter_389_s.COEFF[30] = 62.0 ; 
	FirFilter_389_s.COEFF[31] = 64.0 ; 
}
//--------------------------------
// --- init: FirFilter_390
	 {
	FirFilter_390_s.COEFF[0] = 2.0 ; 
	FirFilter_390_s.COEFF[1] = 4.0 ; 
	FirFilter_390_s.COEFF[2] = 6.0 ; 
	FirFilter_390_s.COEFF[3] = 8.0 ; 
	FirFilter_390_s.COEFF[4] = 10.0 ; 
	FirFilter_390_s.COEFF[5] = 12.0 ; 
	FirFilter_390_s.COEFF[6] = 14.0 ; 
	FirFilter_390_s.COEFF[7] = 16.0 ; 
	FirFilter_390_s.COEFF[8] = 18.0 ; 
	FirFilter_390_s.COEFF[9] = 20.0 ; 
	FirFilter_390_s.COEFF[10] = 22.0 ; 
	FirFilter_390_s.COEFF[11] = 24.0 ; 
	FirFilter_390_s.COEFF[12] = 26.0 ; 
	FirFilter_390_s.COEFF[13] = 28.0 ; 
	FirFilter_390_s.COEFF[14] = 30.0 ; 
	FirFilter_390_s.COEFF[15] = 32.0 ; 
	FirFilter_390_s.COEFF[16] = 34.0 ; 
	FirFilter_390_s.COEFF[17] = 36.0 ; 
	FirFilter_390_s.COEFF[18] = 38.0 ; 
	FirFilter_390_s.COEFF[19] = 40.0 ; 
	FirFilter_390_s.COEFF[20] = 42.0 ; 
	FirFilter_390_s.COEFF[21] = 44.0 ; 
	FirFilter_390_s.COEFF[22] = 46.0 ; 
	FirFilter_390_s.COEFF[23] = 48.0 ; 
	FirFilter_390_s.COEFF[24] = 50.0 ; 
	FirFilter_390_s.COEFF[25] = 52.0 ; 
	FirFilter_390_s.COEFF[26] = 54.0 ; 
	FirFilter_390_s.COEFF[27] = 56.0 ; 
	FirFilter_390_s.COEFF[28] = 58.0 ; 
	FirFilter_390_s.COEFF[29] = 60.0 ; 
	FirFilter_390_s.COEFF[30] = 62.0 ; 
	FirFilter_390_s.COEFF[31] = 64.0 ; 
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_391
	FOR(uint32_t, __iter_init_, 0, <, 66, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&SplitJoin85_Delay_N_Fiss_651_685_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_229_293_641_676_split[2]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_393
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin85_Delay_N_Fiss_651_685_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_394
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin85_Delay_N_Fiss_651_685_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_395
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin85_Delay_N_Fiss_651_685_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_396
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin85_Delay_N_Fiss_651_685_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_397
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin85_Delay_N_Fiss_651_685_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_398
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin85_Delay_N_Fiss_651_685_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_399
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin85_Delay_N_Fiss_651_685_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_400
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin85_Delay_N_Fiss_651_685_join[7], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_392
	FOR(uint32_t, __iter_init_, 0, <, 37, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_392DUPLICATE_Splitter_401, pop_float(&SplitJoin85_Delay_N_Fiss_651_685_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_401
	FOR(uint32_t, __iter_init_, 0, <, 295, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_392DUPLICATE_Splitter_401);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin87_FirFilter_Fiss_652_686_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_403
	 {
	FirFilter_403_s.COEFF[0] = 21.0 ; 
	FirFilter_403_s.COEFF[1] = 54.0 ; 
	FirFilter_403_s.COEFF[2] = 87.0 ; 
	FirFilter_403_s.COEFF[3] = 120.0 ; 
	FirFilter_403_s.COEFF[4] = 153.0 ; 
	FirFilter_403_s.COEFF[5] = 186.0 ; 
	FirFilter_403_s.COEFF[6] = 219.0 ; 
	FirFilter_403_s.COEFF[7] = 252.0 ; 
	FirFilter_403_s.COEFF[8] = 285.0 ; 
	FirFilter_403_s.COEFF[9] = 318.0 ; 
	FirFilter_403_s.COEFF[10] = 351.0 ; 
	FirFilter_403_s.COEFF[11] = 384.0 ; 
	FirFilter_403_s.COEFF[12] = 417.0 ; 
	FirFilter_403_s.COEFF[13] = 450.0 ; 
	FirFilter_403_s.COEFF[14] = 483.0 ; 
	FirFilter_403_s.COEFF[15] = 516.0 ; 
	FirFilter_403_s.COEFF[16] = 549.0 ; 
	FirFilter_403_s.COEFF[17] = 582.0 ; 
	FirFilter_403_s.COEFF[18] = 615.0 ; 
	FirFilter_403_s.COEFF[19] = 648.0 ; 
	FirFilter_403_s.COEFF[20] = 681.0 ; 
	FirFilter_403_s.COEFF[21] = 714.0 ; 
	FirFilter_403_s.COEFF[22] = 747.0 ; 
	FirFilter_403_s.COEFF[23] = 780.0 ; 
	FirFilter_403_s.COEFF[24] = 813.0 ; 
	FirFilter_403_s.COEFF[25] = 846.0 ; 
	FirFilter_403_s.COEFF[26] = 879.0 ; 
	FirFilter_403_s.COEFF[27] = 912.0 ; 
	FirFilter_403_s.COEFF[28] = 945.0 ; 
	FirFilter_403_s.COEFF[29] = 978.0 ; 
	FirFilter_403_s.COEFF[30] = 1011.0 ; 
	FirFilter_403_s.COEFF[31] = 1044.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin87_FirFilter_Fiss_652_686_split[0], i) * FirFilter_403_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin87_FirFilter_Fiss_652_686_split[0]) ; 
		push_float(&SplitJoin87_FirFilter_Fiss_652_686_join[0], sum) ; 
 {
		FOR(int, streamItVar13, 0,  < , 7, streamItVar13++) {
			pop_void(&SplitJoin87_FirFilter_Fiss_652_686_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_404
	 {
	FirFilter_404_s.COEFF[0] = 21.0 ; 
	FirFilter_404_s.COEFF[1] = 54.0 ; 
	FirFilter_404_s.COEFF[2] = 87.0 ; 
	FirFilter_404_s.COEFF[3] = 120.0 ; 
	FirFilter_404_s.COEFF[4] = 153.0 ; 
	FirFilter_404_s.COEFF[5] = 186.0 ; 
	FirFilter_404_s.COEFF[6] = 219.0 ; 
	FirFilter_404_s.COEFF[7] = 252.0 ; 
	FirFilter_404_s.COEFF[8] = 285.0 ; 
	FirFilter_404_s.COEFF[9] = 318.0 ; 
	FirFilter_404_s.COEFF[10] = 351.0 ; 
	FirFilter_404_s.COEFF[11] = 384.0 ; 
	FirFilter_404_s.COEFF[12] = 417.0 ; 
	FirFilter_404_s.COEFF[13] = 450.0 ; 
	FirFilter_404_s.COEFF[14] = 483.0 ; 
	FirFilter_404_s.COEFF[15] = 516.0 ; 
	FirFilter_404_s.COEFF[16] = 549.0 ; 
	FirFilter_404_s.COEFF[17] = 582.0 ; 
	FirFilter_404_s.COEFF[18] = 615.0 ; 
	FirFilter_404_s.COEFF[19] = 648.0 ; 
	FirFilter_404_s.COEFF[20] = 681.0 ; 
	FirFilter_404_s.COEFF[21] = 714.0 ; 
	FirFilter_404_s.COEFF[22] = 747.0 ; 
	FirFilter_404_s.COEFF[23] = 780.0 ; 
	FirFilter_404_s.COEFF[24] = 813.0 ; 
	FirFilter_404_s.COEFF[25] = 846.0 ; 
	FirFilter_404_s.COEFF[26] = 879.0 ; 
	FirFilter_404_s.COEFF[27] = 912.0 ; 
	FirFilter_404_s.COEFF[28] = 945.0 ; 
	FirFilter_404_s.COEFF[29] = 978.0 ; 
	FirFilter_404_s.COEFF[30] = 1011.0 ; 
	FirFilter_404_s.COEFF[31] = 1044.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin87_FirFilter_Fiss_652_686_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin87_FirFilter_Fiss_652_686_split[1], i) * FirFilter_404_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin87_FirFilter_Fiss_652_686_split[1]) ; 
		push_float(&SplitJoin87_FirFilter_Fiss_652_686_join[1], sum) ; 
 {
		FOR(int, streamItVar14, 0,  < , 6, streamItVar14++) {
			pop_void(&SplitJoin87_FirFilter_Fiss_652_686_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_405
	 {
	FirFilter_405_s.COEFF[0] = 21.0 ; 
	FirFilter_405_s.COEFF[1] = 54.0 ; 
	FirFilter_405_s.COEFF[2] = 87.0 ; 
	FirFilter_405_s.COEFF[3] = 120.0 ; 
	FirFilter_405_s.COEFF[4] = 153.0 ; 
	FirFilter_405_s.COEFF[5] = 186.0 ; 
	FirFilter_405_s.COEFF[6] = 219.0 ; 
	FirFilter_405_s.COEFF[7] = 252.0 ; 
	FirFilter_405_s.COEFF[8] = 285.0 ; 
	FirFilter_405_s.COEFF[9] = 318.0 ; 
	FirFilter_405_s.COEFF[10] = 351.0 ; 
	FirFilter_405_s.COEFF[11] = 384.0 ; 
	FirFilter_405_s.COEFF[12] = 417.0 ; 
	FirFilter_405_s.COEFF[13] = 450.0 ; 
	FirFilter_405_s.COEFF[14] = 483.0 ; 
	FirFilter_405_s.COEFF[15] = 516.0 ; 
	FirFilter_405_s.COEFF[16] = 549.0 ; 
	FirFilter_405_s.COEFF[17] = 582.0 ; 
	FirFilter_405_s.COEFF[18] = 615.0 ; 
	FirFilter_405_s.COEFF[19] = 648.0 ; 
	FirFilter_405_s.COEFF[20] = 681.0 ; 
	FirFilter_405_s.COEFF[21] = 714.0 ; 
	FirFilter_405_s.COEFF[22] = 747.0 ; 
	FirFilter_405_s.COEFF[23] = 780.0 ; 
	FirFilter_405_s.COEFF[24] = 813.0 ; 
	FirFilter_405_s.COEFF[25] = 846.0 ; 
	FirFilter_405_s.COEFF[26] = 879.0 ; 
	FirFilter_405_s.COEFF[27] = 912.0 ; 
	FirFilter_405_s.COEFF[28] = 945.0 ; 
	FirFilter_405_s.COEFF[29] = 978.0 ; 
	FirFilter_405_s.COEFF[30] = 1011.0 ; 
	FirFilter_405_s.COEFF[31] = 1044.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar15, 0,  < , 2, streamItVar15++) {
			pop_void(&SplitJoin87_FirFilter_Fiss_652_686_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin87_FirFilter_Fiss_652_686_split[2], i) * FirFilter_405_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin87_FirFilter_Fiss_652_686_split[2]) ; 
		push_float(&SplitJoin87_FirFilter_Fiss_652_686_join[2], sum) ; 
 {
		FOR(int, streamItVar16, 0,  < , 5, streamItVar16++) {
			pop_void(&SplitJoin87_FirFilter_Fiss_652_686_split[2]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_406
	 {
	FirFilter_406_s.COEFF[0] = 21.0 ; 
	FirFilter_406_s.COEFF[1] = 54.0 ; 
	FirFilter_406_s.COEFF[2] = 87.0 ; 
	FirFilter_406_s.COEFF[3] = 120.0 ; 
	FirFilter_406_s.COEFF[4] = 153.0 ; 
	FirFilter_406_s.COEFF[5] = 186.0 ; 
	FirFilter_406_s.COEFF[6] = 219.0 ; 
	FirFilter_406_s.COEFF[7] = 252.0 ; 
	FirFilter_406_s.COEFF[8] = 285.0 ; 
	FirFilter_406_s.COEFF[9] = 318.0 ; 
	FirFilter_406_s.COEFF[10] = 351.0 ; 
	FirFilter_406_s.COEFF[11] = 384.0 ; 
	FirFilter_406_s.COEFF[12] = 417.0 ; 
	FirFilter_406_s.COEFF[13] = 450.0 ; 
	FirFilter_406_s.COEFF[14] = 483.0 ; 
	FirFilter_406_s.COEFF[15] = 516.0 ; 
	FirFilter_406_s.COEFF[16] = 549.0 ; 
	FirFilter_406_s.COEFF[17] = 582.0 ; 
	FirFilter_406_s.COEFF[18] = 615.0 ; 
	FirFilter_406_s.COEFF[19] = 648.0 ; 
	FirFilter_406_s.COEFF[20] = 681.0 ; 
	FirFilter_406_s.COEFF[21] = 714.0 ; 
	FirFilter_406_s.COEFF[22] = 747.0 ; 
	FirFilter_406_s.COEFF[23] = 780.0 ; 
	FirFilter_406_s.COEFF[24] = 813.0 ; 
	FirFilter_406_s.COEFF[25] = 846.0 ; 
	FirFilter_406_s.COEFF[26] = 879.0 ; 
	FirFilter_406_s.COEFF[27] = 912.0 ; 
	FirFilter_406_s.COEFF[28] = 945.0 ; 
	FirFilter_406_s.COEFF[29] = 978.0 ; 
	FirFilter_406_s.COEFF[30] = 1011.0 ; 
	FirFilter_406_s.COEFF[31] = 1044.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar17, 0,  < , 3, streamItVar17++) {
			pop_void(&SplitJoin87_FirFilter_Fiss_652_686_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin87_FirFilter_Fiss_652_686_split[3], i) * FirFilter_406_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin87_FirFilter_Fiss_652_686_split[3]) ; 
		push_float(&SplitJoin87_FirFilter_Fiss_652_686_join[3], sum) ; 
 {
		FOR(int, streamItVar18, 0,  < , 4, streamItVar18++) {
			pop_void(&SplitJoin87_FirFilter_Fiss_652_686_split[3]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_407
	 {
	FirFilter_407_s.COEFF[0] = 21.0 ; 
	FirFilter_407_s.COEFF[1] = 54.0 ; 
	FirFilter_407_s.COEFF[2] = 87.0 ; 
	FirFilter_407_s.COEFF[3] = 120.0 ; 
	FirFilter_407_s.COEFF[4] = 153.0 ; 
	FirFilter_407_s.COEFF[5] = 186.0 ; 
	FirFilter_407_s.COEFF[6] = 219.0 ; 
	FirFilter_407_s.COEFF[7] = 252.0 ; 
	FirFilter_407_s.COEFF[8] = 285.0 ; 
	FirFilter_407_s.COEFF[9] = 318.0 ; 
	FirFilter_407_s.COEFF[10] = 351.0 ; 
	FirFilter_407_s.COEFF[11] = 384.0 ; 
	FirFilter_407_s.COEFF[12] = 417.0 ; 
	FirFilter_407_s.COEFF[13] = 450.0 ; 
	FirFilter_407_s.COEFF[14] = 483.0 ; 
	FirFilter_407_s.COEFF[15] = 516.0 ; 
	FirFilter_407_s.COEFF[16] = 549.0 ; 
	FirFilter_407_s.COEFF[17] = 582.0 ; 
	FirFilter_407_s.COEFF[18] = 615.0 ; 
	FirFilter_407_s.COEFF[19] = 648.0 ; 
	FirFilter_407_s.COEFF[20] = 681.0 ; 
	FirFilter_407_s.COEFF[21] = 714.0 ; 
	FirFilter_407_s.COEFF[22] = 747.0 ; 
	FirFilter_407_s.COEFF[23] = 780.0 ; 
	FirFilter_407_s.COEFF[24] = 813.0 ; 
	FirFilter_407_s.COEFF[25] = 846.0 ; 
	FirFilter_407_s.COEFF[26] = 879.0 ; 
	FirFilter_407_s.COEFF[27] = 912.0 ; 
	FirFilter_407_s.COEFF[28] = 945.0 ; 
	FirFilter_407_s.COEFF[29] = 978.0 ; 
	FirFilter_407_s.COEFF[30] = 1011.0 ; 
	FirFilter_407_s.COEFF[31] = 1044.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar19, 0,  < , 4, streamItVar19++) {
			pop_void(&SplitJoin87_FirFilter_Fiss_652_686_split[4]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin87_FirFilter_Fiss_652_686_split[4], i) * FirFilter_407_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin87_FirFilter_Fiss_652_686_split[4]) ; 
		push_float(&SplitJoin87_FirFilter_Fiss_652_686_join[4], sum) ; 
 {
		FOR(int, streamItVar20, 0,  < , 3, streamItVar20++) {
			pop_void(&SplitJoin87_FirFilter_Fiss_652_686_split[4]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_408
	 {
	FirFilter_408_s.COEFF[0] = 21.0 ; 
	FirFilter_408_s.COEFF[1] = 54.0 ; 
	FirFilter_408_s.COEFF[2] = 87.0 ; 
	FirFilter_408_s.COEFF[3] = 120.0 ; 
	FirFilter_408_s.COEFF[4] = 153.0 ; 
	FirFilter_408_s.COEFF[5] = 186.0 ; 
	FirFilter_408_s.COEFF[6] = 219.0 ; 
	FirFilter_408_s.COEFF[7] = 252.0 ; 
	FirFilter_408_s.COEFF[8] = 285.0 ; 
	FirFilter_408_s.COEFF[9] = 318.0 ; 
	FirFilter_408_s.COEFF[10] = 351.0 ; 
	FirFilter_408_s.COEFF[11] = 384.0 ; 
	FirFilter_408_s.COEFF[12] = 417.0 ; 
	FirFilter_408_s.COEFF[13] = 450.0 ; 
	FirFilter_408_s.COEFF[14] = 483.0 ; 
	FirFilter_408_s.COEFF[15] = 516.0 ; 
	FirFilter_408_s.COEFF[16] = 549.0 ; 
	FirFilter_408_s.COEFF[17] = 582.0 ; 
	FirFilter_408_s.COEFF[18] = 615.0 ; 
	FirFilter_408_s.COEFF[19] = 648.0 ; 
	FirFilter_408_s.COEFF[20] = 681.0 ; 
	FirFilter_408_s.COEFF[21] = 714.0 ; 
	FirFilter_408_s.COEFF[22] = 747.0 ; 
	FirFilter_408_s.COEFF[23] = 780.0 ; 
	FirFilter_408_s.COEFF[24] = 813.0 ; 
	FirFilter_408_s.COEFF[25] = 846.0 ; 
	FirFilter_408_s.COEFF[26] = 879.0 ; 
	FirFilter_408_s.COEFF[27] = 912.0 ; 
	FirFilter_408_s.COEFF[28] = 945.0 ; 
	FirFilter_408_s.COEFF[29] = 978.0 ; 
	FirFilter_408_s.COEFF[30] = 1011.0 ; 
	FirFilter_408_s.COEFF[31] = 1044.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar21, 0,  < , 5, streamItVar21++) {
			pop_void(&SplitJoin87_FirFilter_Fiss_652_686_split[5]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin87_FirFilter_Fiss_652_686_split[5], i) * FirFilter_408_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin87_FirFilter_Fiss_652_686_split[5]) ; 
		push_float(&SplitJoin87_FirFilter_Fiss_652_686_join[5], sum) ; 
 {
		FOR(int, streamItVar22, 0,  < , 2, streamItVar22++) {
			pop_void(&SplitJoin87_FirFilter_Fiss_652_686_split[5]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_409
	 {
	FirFilter_409_s.COEFF[0] = 21.0 ; 
	FirFilter_409_s.COEFF[1] = 54.0 ; 
	FirFilter_409_s.COEFF[2] = 87.0 ; 
	FirFilter_409_s.COEFF[3] = 120.0 ; 
	FirFilter_409_s.COEFF[4] = 153.0 ; 
	FirFilter_409_s.COEFF[5] = 186.0 ; 
	FirFilter_409_s.COEFF[6] = 219.0 ; 
	FirFilter_409_s.COEFF[7] = 252.0 ; 
	FirFilter_409_s.COEFF[8] = 285.0 ; 
	FirFilter_409_s.COEFF[9] = 318.0 ; 
	FirFilter_409_s.COEFF[10] = 351.0 ; 
	FirFilter_409_s.COEFF[11] = 384.0 ; 
	FirFilter_409_s.COEFF[12] = 417.0 ; 
	FirFilter_409_s.COEFF[13] = 450.0 ; 
	FirFilter_409_s.COEFF[14] = 483.0 ; 
	FirFilter_409_s.COEFF[15] = 516.0 ; 
	FirFilter_409_s.COEFF[16] = 549.0 ; 
	FirFilter_409_s.COEFF[17] = 582.0 ; 
	FirFilter_409_s.COEFF[18] = 615.0 ; 
	FirFilter_409_s.COEFF[19] = 648.0 ; 
	FirFilter_409_s.COEFF[20] = 681.0 ; 
	FirFilter_409_s.COEFF[21] = 714.0 ; 
	FirFilter_409_s.COEFF[22] = 747.0 ; 
	FirFilter_409_s.COEFF[23] = 780.0 ; 
	FirFilter_409_s.COEFF[24] = 813.0 ; 
	FirFilter_409_s.COEFF[25] = 846.0 ; 
	FirFilter_409_s.COEFF[26] = 879.0 ; 
	FirFilter_409_s.COEFF[27] = 912.0 ; 
	FirFilter_409_s.COEFF[28] = 945.0 ; 
	FirFilter_409_s.COEFF[29] = 978.0 ; 
	FirFilter_409_s.COEFF[30] = 1011.0 ; 
	FirFilter_409_s.COEFF[31] = 1044.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar23, 0,  < , 6, streamItVar23++) {
			pop_void(&SplitJoin87_FirFilter_Fiss_652_686_split[6]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin87_FirFilter_Fiss_652_686_split[6], i) * FirFilter_409_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin87_FirFilter_Fiss_652_686_split[6]) ; 
		push_float(&SplitJoin87_FirFilter_Fiss_652_686_join[6], sum) ; 
 {
		pop_void(&SplitJoin87_FirFilter_Fiss_652_686_split[6]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_410
	 {
	FirFilter_410_s.COEFF[0] = 21.0 ; 
	FirFilter_410_s.COEFF[1] = 54.0 ; 
	FirFilter_410_s.COEFF[2] = 87.0 ; 
	FirFilter_410_s.COEFF[3] = 120.0 ; 
	FirFilter_410_s.COEFF[4] = 153.0 ; 
	FirFilter_410_s.COEFF[5] = 186.0 ; 
	FirFilter_410_s.COEFF[6] = 219.0 ; 
	FirFilter_410_s.COEFF[7] = 252.0 ; 
	FirFilter_410_s.COEFF[8] = 285.0 ; 
	FirFilter_410_s.COEFF[9] = 318.0 ; 
	FirFilter_410_s.COEFF[10] = 351.0 ; 
	FirFilter_410_s.COEFF[11] = 384.0 ; 
	FirFilter_410_s.COEFF[12] = 417.0 ; 
	FirFilter_410_s.COEFF[13] = 450.0 ; 
	FirFilter_410_s.COEFF[14] = 483.0 ; 
	FirFilter_410_s.COEFF[15] = 516.0 ; 
	FirFilter_410_s.COEFF[16] = 549.0 ; 
	FirFilter_410_s.COEFF[17] = 582.0 ; 
	FirFilter_410_s.COEFF[18] = 615.0 ; 
	FirFilter_410_s.COEFF[19] = 648.0 ; 
	FirFilter_410_s.COEFF[20] = 681.0 ; 
	FirFilter_410_s.COEFF[21] = 714.0 ; 
	FirFilter_410_s.COEFF[22] = 747.0 ; 
	FirFilter_410_s.COEFF[23] = 780.0 ; 
	FirFilter_410_s.COEFF[24] = 813.0 ; 
	FirFilter_410_s.COEFF[25] = 846.0 ; 
	FirFilter_410_s.COEFF[26] = 879.0 ; 
	FirFilter_410_s.COEFF[27] = 912.0 ; 
	FirFilter_410_s.COEFF[28] = 945.0 ; 
	FirFilter_410_s.COEFF[29] = 978.0 ; 
	FirFilter_410_s.COEFF[30] = 1011.0 ; 
	FirFilter_410_s.COEFF[31] = 1044.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar24, 0,  < , 7, streamItVar24++) {
			pop_void(&SplitJoin87_FirFilter_Fiss_652_686_split[7]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin87_FirFilter_Fiss_652_686_split[7], i) * FirFilter_410_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin87_FirFilter_Fiss_652_686_split[7]) ; 
		push_float(&SplitJoin87_FirFilter_Fiss_652_686_join[7], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_402
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_402DownSamp_250, pop_float(&SplitJoin87_FirFilter_Fiss_652_686_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DownSamp_250
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		push_float(&DownSamp_250UpSamp_251, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_402DownSamp_250)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_402DownSamp_250) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: UpSamp_251
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		push_float(&UpSamp_251WEIGHTED_ROUND_ROBIN_Splitter_411, pop_float(&DownSamp_250UpSamp_251)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			push_float(&UpSamp_251WEIGHTED_ROUND_ROBIN_Splitter_411, 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_411
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&SplitJoin89_Delay_N_Fiss_653_687_split[__iter_], pop_float(&UpSamp_251WEIGHTED_ROUND_ROBIN_Splitter_411));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_413
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin89_Delay_N_Fiss_653_687_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_414
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin89_Delay_N_Fiss_653_687_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_415
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin89_Delay_N_Fiss_653_687_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_416
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin89_Delay_N_Fiss_653_687_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_417
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin89_Delay_N_Fiss_653_687_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_418
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin89_Delay_N_Fiss_653_687_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_419
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin89_Delay_N_Fiss_653_687_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_420
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin89_Delay_N_Fiss_653_687_join[7], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_412
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_412DUPLICATE_Splitter_421, pop_float(&SplitJoin89_Delay_N_Fiss_653_687_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_421
	FOR(uint32_t, __iter_init_, 0, <, 31, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_412DUPLICATE_Splitter_421);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin91_FirFilter_Fiss_654_688_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_423
	 {
	FirFilter_423_s.COEFF[0] = 6.0 ; 
	FirFilter_423_s.COEFF[1] = 9.0 ; 
	FirFilter_423_s.COEFF[2] = 12.0 ; 
	FirFilter_423_s.COEFF[3] = 15.0 ; 
	FirFilter_423_s.COEFF[4] = 18.0 ; 
	FirFilter_423_s.COEFF[5] = 21.0 ; 
	FirFilter_423_s.COEFF[6] = 24.0 ; 
	FirFilter_423_s.COEFF[7] = 27.0 ; 
	FirFilter_423_s.COEFF[8] = 30.0 ; 
	FirFilter_423_s.COEFF[9] = 33.0 ; 
	FirFilter_423_s.COEFF[10] = 36.0 ; 
	FirFilter_423_s.COEFF[11] = 39.0 ; 
	FirFilter_423_s.COEFF[12] = 42.0 ; 
	FirFilter_423_s.COEFF[13] = 45.0 ; 
	FirFilter_423_s.COEFF[14] = 48.0 ; 
	FirFilter_423_s.COEFF[15] = 51.0 ; 
	FirFilter_423_s.COEFF[16] = 54.0 ; 
	FirFilter_423_s.COEFF[17] = 57.0 ; 
	FirFilter_423_s.COEFF[18] = 60.0 ; 
	FirFilter_423_s.COEFF[19] = 63.0 ; 
	FirFilter_423_s.COEFF[20] = 66.0 ; 
	FirFilter_423_s.COEFF[21] = 69.0 ; 
	FirFilter_423_s.COEFF[22] = 72.0 ; 
	FirFilter_423_s.COEFF[23] = 75.0 ; 
	FirFilter_423_s.COEFF[24] = 78.0 ; 
	FirFilter_423_s.COEFF[25] = 81.0 ; 
	FirFilter_423_s.COEFF[26] = 84.0 ; 
	FirFilter_423_s.COEFF[27] = 87.0 ; 
	FirFilter_423_s.COEFF[28] = 90.0 ; 
	FirFilter_423_s.COEFF[29] = 93.0 ; 
	FirFilter_423_s.COEFF[30] = 96.0 ; 
	FirFilter_423_s.COEFF[31] = 99.0 ; 
}
//--------------------------------
// --- init: FirFilter_424
	 {
	FirFilter_424_s.COEFF[0] = 6.0 ; 
	FirFilter_424_s.COEFF[1] = 9.0 ; 
	FirFilter_424_s.COEFF[2] = 12.0 ; 
	FirFilter_424_s.COEFF[3] = 15.0 ; 
	FirFilter_424_s.COEFF[4] = 18.0 ; 
	FirFilter_424_s.COEFF[5] = 21.0 ; 
	FirFilter_424_s.COEFF[6] = 24.0 ; 
	FirFilter_424_s.COEFF[7] = 27.0 ; 
	FirFilter_424_s.COEFF[8] = 30.0 ; 
	FirFilter_424_s.COEFF[9] = 33.0 ; 
	FirFilter_424_s.COEFF[10] = 36.0 ; 
	FirFilter_424_s.COEFF[11] = 39.0 ; 
	FirFilter_424_s.COEFF[12] = 42.0 ; 
	FirFilter_424_s.COEFF[13] = 45.0 ; 
	FirFilter_424_s.COEFF[14] = 48.0 ; 
	FirFilter_424_s.COEFF[15] = 51.0 ; 
	FirFilter_424_s.COEFF[16] = 54.0 ; 
	FirFilter_424_s.COEFF[17] = 57.0 ; 
	FirFilter_424_s.COEFF[18] = 60.0 ; 
	FirFilter_424_s.COEFF[19] = 63.0 ; 
	FirFilter_424_s.COEFF[20] = 66.0 ; 
	FirFilter_424_s.COEFF[21] = 69.0 ; 
	FirFilter_424_s.COEFF[22] = 72.0 ; 
	FirFilter_424_s.COEFF[23] = 75.0 ; 
	FirFilter_424_s.COEFF[24] = 78.0 ; 
	FirFilter_424_s.COEFF[25] = 81.0 ; 
	FirFilter_424_s.COEFF[26] = 84.0 ; 
	FirFilter_424_s.COEFF[27] = 87.0 ; 
	FirFilter_424_s.COEFF[28] = 90.0 ; 
	FirFilter_424_s.COEFF[29] = 93.0 ; 
	FirFilter_424_s.COEFF[30] = 96.0 ; 
	FirFilter_424_s.COEFF[31] = 99.0 ; 
}
//--------------------------------
// --- init: FirFilter_425
	 {
	FirFilter_425_s.COEFF[0] = 6.0 ; 
	FirFilter_425_s.COEFF[1] = 9.0 ; 
	FirFilter_425_s.COEFF[2] = 12.0 ; 
	FirFilter_425_s.COEFF[3] = 15.0 ; 
	FirFilter_425_s.COEFF[4] = 18.0 ; 
	FirFilter_425_s.COEFF[5] = 21.0 ; 
	FirFilter_425_s.COEFF[6] = 24.0 ; 
	FirFilter_425_s.COEFF[7] = 27.0 ; 
	FirFilter_425_s.COEFF[8] = 30.0 ; 
	FirFilter_425_s.COEFF[9] = 33.0 ; 
	FirFilter_425_s.COEFF[10] = 36.0 ; 
	FirFilter_425_s.COEFF[11] = 39.0 ; 
	FirFilter_425_s.COEFF[12] = 42.0 ; 
	FirFilter_425_s.COEFF[13] = 45.0 ; 
	FirFilter_425_s.COEFF[14] = 48.0 ; 
	FirFilter_425_s.COEFF[15] = 51.0 ; 
	FirFilter_425_s.COEFF[16] = 54.0 ; 
	FirFilter_425_s.COEFF[17] = 57.0 ; 
	FirFilter_425_s.COEFF[18] = 60.0 ; 
	FirFilter_425_s.COEFF[19] = 63.0 ; 
	FirFilter_425_s.COEFF[20] = 66.0 ; 
	FirFilter_425_s.COEFF[21] = 69.0 ; 
	FirFilter_425_s.COEFF[22] = 72.0 ; 
	FirFilter_425_s.COEFF[23] = 75.0 ; 
	FirFilter_425_s.COEFF[24] = 78.0 ; 
	FirFilter_425_s.COEFF[25] = 81.0 ; 
	FirFilter_425_s.COEFF[26] = 84.0 ; 
	FirFilter_425_s.COEFF[27] = 87.0 ; 
	FirFilter_425_s.COEFF[28] = 90.0 ; 
	FirFilter_425_s.COEFF[29] = 93.0 ; 
	FirFilter_425_s.COEFF[30] = 96.0 ; 
	FirFilter_425_s.COEFF[31] = 99.0 ; 
}
//--------------------------------
// --- init: FirFilter_426
	 {
	FirFilter_426_s.COEFF[0] = 6.0 ; 
	FirFilter_426_s.COEFF[1] = 9.0 ; 
	FirFilter_426_s.COEFF[2] = 12.0 ; 
	FirFilter_426_s.COEFF[3] = 15.0 ; 
	FirFilter_426_s.COEFF[4] = 18.0 ; 
	FirFilter_426_s.COEFF[5] = 21.0 ; 
	FirFilter_426_s.COEFF[6] = 24.0 ; 
	FirFilter_426_s.COEFF[7] = 27.0 ; 
	FirFilter_426_s.COEFF[8] = 30.0 ; 
	FirFilter_426_s.COEFF[9] = 33.0 ; 
	FirFilter_426_s.COEFF[10] = 36.0 ; 
	FirFilter_426_s.COEFF[11] = 39.0 ; 
	FirFilter_426_s.COEFF[12] = 42.0 ; 
	FirFilter_426_s.COEFF[13] = 45.0 ; 
	FirFilter_426_s.COEFF[14] = 48.0 ; 
	FirFilter_426_s.COEFF[15] = 51.0 ; 
	FirFilter_426_s.COEFF[16] = 54.0 ; 
	FirFilter_426_s.COEFF[17] = 57.0 ; 
	FirFilter_426_s.COEFF[18] = 60.0 ; 
	FirFilter_426_s.COEFF[19] = 63.0 ; 
	FirFilter_426_s.COEFF[20] = 66.0 ; 
	FirFilter_426_s.COEFF[21] = 69.0 ; 
	FirFilter_426_s.COEFF[22] = 72.0 ; 
	FirFilter_426_s.COEFF[23] = 75.0 ; 
	FirFilter_426_s.COEFF[24] = 78.0 ; 
	FirFilter_426_s.COEFF[25] = 81.0 ; 
	FirFilter_426_s.COEFF[26] = 84.0 ; 
	FirFilter_426_s.COEFF[27] = 87.0 ; 
	FirFilter_426_s.COEFF[28] = 90.0 ; 
	FirFilter_426_s.COEFF[29] = 93.0 ; 
	FirFilter_426_s.COEFF[30] = 96.0 ; 
	FirFilter_426_s.COEFF[31] = 99.0 ; 
}
//--------------------------------
// --- init: FirFilter_427
	 {
	FirFilter_427_s.COEFF[0] = 6.0 ; 
	FirFilter_427_s.COEFF[1] = 9.0 ; 
	FirFilter_427_s.COEFF[2] = 12.0 ; 
	FirFilter_427_s.COEFF[3] = 15.0 ; 
	FirFilter_427_s.COEFF[4] = 18.0 ; 
	FirFilter_427_s.COEFF[5] = 21.0 ; 
	FirFilter_427_s.COEFF[6] = 24.0 ; 
	FirFilter_427_s.COEFF[7] = 27.0 ; 
	FirFilter_427_s.COEFF[8] = 30.0 ; 
	FirFilter_427_s.COEFF[9] = 33.0 ; 
	FirFilter_427_s.COEFF[10] = 36.0 ; 
	FirFilter_427_s.COEFF[11] = 39.0 ; 
	FirFilter_427_s.COEFF[12] = 42.0 ; 
	FirFilter_427_s.COEFF[13] = 45.0 ; 
	FirFilter_427_s.COEFF[14] = 48.0 ; 
	FirFilter_427_s.COEFF[15] = 51.0 ; 
	FirFilter_427_s.COEFF[16] = 54.0 ; 
	FirFilter_427_s.COEFF[17] = 57.0 ; 
	FirFilter_427_s.COEFF[18] = 60.0 ; 
	FirFilter_427_s.COEFF[19] = 63.0 ; 
	FirFilter_427_s.COEFF[20] = 66.0 ; 
	FirFilter_427_s.COEFF[21] = 69.0 ; 
	FirFilter_427_s.COEFF[22] = 72.0 ; 
	FirFilter_427_s.COEFF[23] = 75.0 ; 
	FirFilter_427_s.COEFF[24] = 78.0 ; 
	FirFilter_427_s.COEFF[25] = 81.0 ; 
	FirFilter_427_s.COEFF[26] = 84.0 ; 
	FirFilter_427_s.COEFF[27] = 87.0 ; 
	FirFilter_427_s.COEFF[28] = 90.0 ; 
	FirFilter_427_s.COEFF[29] = 93.0 ; 
	FirFilter_427_s.COEFF[30] = 96.0 ; 
	FirFilter_427_s.COEFF[31] = 99.0 ; 
}
//--------------------------------
// --- init: FirFilter_428
	 {
	FirFilter_428_s.COEFF[0] = 6.0 ; 
	FirFilter_428_s.COEFF[1] = 9.0 ; 
	FirFilter_428_s.COEFF[2] = 12.0 ; 
	FirFilter_428_s.COEFF[3] = 15.0 ; 
	FirFilter_428_s.COEFF[4] = 18.0 ; 
	FirFilter_428_s.COEFF[5] = 21.0 ; 
	FirFilter_428_s.COEFF[6] = 24.0 ; 
	FirFilter_428_s.COEFF[7] = 27.0 ; 
	FirFilter_428_s.COEFF[8] = 30.0 ; 
	FirFilter_428_s.COEFF[9] = 33.0 ; 
	FirFilter_428_s.COEFF[10] = 36.0 ; 
	FirFilter_428_s.COEFF[11] = 39.0 ; 
	FirFilter_428_s.COEFF[12] = 42.0 ; 
	FirFilter_428_s.COEFF[13] = 45.0 ; 
	FirFilter_428_s.COEFF[14] = 48.0 ; 
	FirFilter_428_s.COEFF[15] = 51.0 ; 
	FirFilter_428_s.COEFF[16] = 54.0 ; 
	FirFilter_428_s.COEFF[17] = 57.0 ; 
	FirFilter_428_s.COEFF[18] = 60.0 ; 
	FirFilter_428_s.COEFF[19] = 63.0 ; 
	FirFilter_428_s.COEFF[20] = 66.0 ; 
	FirFilter_428_s.COEFF[21] = 69.0 ; 
	FirFilter_428_s.COEFF[22] = 72.0 ; 
	FirFilter_428_s.COEFF[23] = 75.0 ; 
	FirFilter_428_s.COEFF[24] = 78.0 ; 
	FirFilter_428_s.COEFF[25] = 81.0 ; 
	FirFilter_428_s.COEFF[26] = 84.0 ; 
	FirFilter_428_s.COEFF[27] = 87.0 ; 
	FirFilter_428_s.COEFF[28] = 90.0 ; 
	FirFilter_428_s.COEFF[29] = 93.0 ; 
	FirFilter_428_s.COEFF[30] = 96.0 ; 
	FirFilter_428_s.COEFF[31] = 99.0 ; 
}
//--------------------------------
// --- init: FirFilter_429
	 {
	FirFilter_429_s.COEFF[0] = 6.0 ; 
	FirFilter_429_s.COEFF[1] = 9.0 ; 
	FirFilter_429_s.COEFF[2] = 12.0 ; 
	FirFilter_429_s.COEFF[3] = 15.0 ; 
	FirFilter_429_s.COEFF[4] = 18.0 ; 
	FirFilter_429_s.COEFF[5] = 21.0 ; 
	FirFilter_429_s.COEFF[6] = 24.0 ; 
	FirFilter_429_s.COEFF[7] = 27.0 ; 
	FirFilter_429_s.COEFF[8] = 30.0 ; 
	FirFilter_429_s.COEFF[9] = 33.0 ; 
	FirFilter_429_s.COEFF[10] = 36.0 ; 
	FirFilter_429_s.COEFF[11] = 39.0 ; 
	FirFilter_429_s.COEFF[12] = 42.0 ; 
	FirFilter_429_s.COEFF[13] = 45.0 ; 
	FirFilter_429_s.COEFF[14] = 48.0 ; 
	FirFilter_429_s.COEFF[15] = 51.0 ; 
	FirFilter_429_s.COEFF[16] = 54.0 ; 
	FirFilter_429_s.COEFF[17] = 57.0 ; 
	FirFilter_429_s.COEFF[18] = 60.0 ; 
	FirFilter_429_s.COEFF[19] = 63.0 ; 
	FirFilter_429_s.COEFF[20] = 66.0 ; 
	FirFilter_429_s.COEFF[21] = 69.0 ; 
	FirFilter_429_s.COEFF[22] = 72.0 ; 
	FirFilter_429_s.COEFF[23] = 75.0 ; 
	FirFilter_429_s.COEFF[24] = 78.0 ; 
	FirFilter_429_s.COEFF[25] = 81.0 ; 
	FirFilter_429_s.COEFF[26] = 84.0 ; 
	FirFilter_429_s.COEFF[27] = 87.0 ; 
	FirFilter_429_s.COEFF[28] = 90.0 ; 
	FirFilter_429_s.COEFF[29] = 93.0 ; 
	FirFilter_429_s.COEFF[30] = 96.0 ; 
	FirFilter_429_s.COEFF[31] = 99.0 ; 
}
//--------------------------------
// --- init: FirFilter_430
	 {
	FirFilter_430_s.COEFF[0] = 6.0 ; 
	FirFilter_430_s.COEFF[1] = 9.0 ; 
	FirFilter_430_s.COEFF[2] = 12.0 ; 
	FirFilter_430_s.COEFF[3] = 15.0 ; 
	FirFilter_430_s.COEFF[4] = 18.0 ; 
	FirFilter_430_s.COEFF[5] = 21.0 ; 
	FirFilter_430_s.COEFF[6] = 24.0 ; 
	FirFilter_430_s.COEFF[7] = 27.0 ; 
	FirFilter_430_s.COEFF[8] = 30.0 ; 
	FirFilter_430_s.COEFF[9] = 33.0 ; 
	FirFilter_430_s.COEFF[10] = 36.0 ; 
	FirFilter_430_s.COEFF[11] = 39.0 ; 
	FirFilter_430_s.COEFF[12] = 42.0 ; 
	FirFilter_430_s.COEFF[13] = 45.0 ; 
	FirFilter_430_s.COEFF[14] = 48.0 ; 
	FirFilter_430_s.COEFF[15] = 51.0 ; 
	FirFilter_430_s.COEFF[16] = 54.0 ; 
	FirFilter_430_s.COEFF[17] = 57.0 ; 
	FirFilter_430_s.COEFF[18] = 60.0 ; 
	FirFilter_430_s.COEFF[19] = 63.0 ; 
	FirFilter_430_s.COEFF[20] = 66.0 ; 
	FirFilter_430_s.COEFF[21] = 69.0 ; 
	FirFilter_430_s.COEFF[22] = 72.0 ; 
	FirFilter_430_s.COEFF[23] = 75.0 ; 
	FirFilter_430_s.COEFF[24] = 78.0 ; 
	FirFilter_430_s.COEFF[25] = 81.0 ; 
	FirFilter_430_s.COEFF[26] = 84.0 ; 
	FirFilter_430_s.COEFF[27] = 87.0 ; 
	FirFilter_430_s.COEFF[28] = 90.0 ; 
	FirFilter_430_s.COEFF[29] = 93.0 ; 
	FirFilter_430_s.COEFF[30] = 96.0 ; 
	FirFilter_430_s.COEFF[31] = 99.0 ; 
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_431
	FOR(uint32_t, __iter_init_, 0, <, 66, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&SplitJoin122_Delay_N_Fiss_655_689_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_229_293_641_676_split[3]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_433
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin122_Delay_N_Fiss_655_689_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_434
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin122_Delay_N_Fiss_655_689_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_435
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin122_Delay_N_Fiss_655_689_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_436
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin122_Delay_N_Fiss_655_689_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_437
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin122_Delay_N_Fiss_655_689_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_438
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin122_Delay_N_Fiss_655_689_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_439
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin122_Delay_N_Fiss_655_689_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_440
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin122_Delay_N_Fiss_655_689_join[7], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_432
	FOR(uint32_t, __iter_init_, 0, <, 37, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_432DUPLICATE_Splitter_441, pop_float(&SplitJoin122_Delay_N_Fiss_655_689_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_441
	FOR(uint32_t, __iter_init_, 0, <, 295, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_432DUPLICATE_Splitter_441);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin124_FirFilter_Fiss_656_690_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_443
	 {
	FirFilter_443_s.COEFF[0] = 31.0 ; 
	FirFilter_443_s.COEFF[1] = 64.0 ; 
	FirFilter_443_s.COEFF[2] = 97.0 ; 
	FirFilter_443_s.COEFF[3] = 130.0 ; 
	FirFilter_443_s.COEFF[4] = 163.0 ; 
	FirFilter_443_s.COEFF[5] = 196.0 ; 
	FirFilter_443_s.COEFF[6] = 229.0 ; 
	FirFilter_443_s.COEFF[7] = 262.0 ; 
	FirFilter_443_s.COEFF[8] = 295.0 ; 
	FirFilter_443_s.COEFF[9] = 328.0 ; 
	FirFilter_443_s.COEFF[10] = 361.0 ; 
	FirFilter_443_s.COEFF[11] = 394.0 ; 
	FirFilter_443_s.COEFF[12] = 427.0 ; 
	FirFilter_443_s.COEFF[13] = 460.0 ; 
	FirFilter_443_s.COEFF[14] = 493.0 ; 
	FirFilter_443_s.COEFF[15] = 526.0 ; 
	FirFilter_443_s.COEFF[16] = 559.0 ; 
	FirFilter_443_s.COEFF[17] = 592.0 ; 
	FirFilter_443_s.COEFF[18] = 625.0 ; 
	FirFilter_443_s.COEFF[19] = 658.0 ; 
	FirFilter_443_s.COEFF[20] = 691.0 ; 
	FirFilter_443_s.COEFF[21] = 724.0 ; 
	FirFilter_443_s.COEFF[22] = 757.0 ; 
	FirFilter_443_s.COEFF[23] = 790.0 ; 
	FirFilter_443_s.COEFF[24] = 823.0 ; 
	FirFilter_443_s.COEFF[25] = 856.0 ; 
	FirFilter_443_s.COEFF[26] = 889.0 ; 
	FirFilter_443_s.COEFF[27] = 922.0 ; 
	FirFilter_443_s.COEFF[28] = 955.0 ; 
	FirFilter_443_s.COEFF[29] = 988.0 ; 
	FirFilter_443_s.COEFF[30] = 1021.0 ; 
	FirFilter_443_s.COEFF[31] = 1054.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin124_FirFilter_Fiss_656_690_split[0], i) * FirFilter_443_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin124_FirFilter_Fiss_656_690_split[0]) ; 
		push_float(&SplitJoin124_FirFilter_Fiss_656_690_join[0], sum) ; 
 {
		FOR(int, streamItVar181, 0,  < , 7, streamItVar181++) {
			pop_void(&SplitJoin124_FirFilter_Fiss_656_690_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_444
	 {
	FirFilter_444_s.COEFF[0] = 31.0 ; 
	FirFilter_444_s.COEFF[1] = 64.0 ; 
	FirFilter_444_s.COEFF[2] = 97.0 ; 
	FirFilter_444_s.COEFF[3] = 130.0 ; 
	FirFilter_444_s.COEFF[4] = 163.0 ; 
	FirFilter_444_s.COEFF[5] = 196.0 ; 
	FirFilter_444_s.COEFF[6] = 229.0 ; 
	FirFilter_444_s.COEFF[7] = 262.0 ; 
	FirFilter_444_s.COEFF[8] = 295.0 ; 
	FirFilter_444_s.COEFF[9] = 328.0 ; 
	FirFilter_444_s.COEFF[10] = 361.0 ; 
	FirFilter_444_s.COEFF[11] = 394.0 ; 
	FirFilter_444_s.COEFF[12] = 427.0 ; 
	FirFilter_444_s.COEFF[13] = 460.0 ; 
	FirFilter_444_s.COEFF[14] = 493.0 ; 
	FirFilter_444_s.COEFF[15] = 526.0 ; 
	FirFilter_444_s.COEFF[16] = 559.0 ; 
	FirFilter_444_s.COEFF[17] = 592.0 ; 
	FirFilter_444_s.COEFF[18] = 625.0 ; 
	FirFilter_444_s.COEFF[19] = 658.0 ; 
	FirFilter_444_s.COEFF[20] = 691.0 ; 
	FirFilter_444_s.COEFF[21] = 724.0 ; 
	FirFilter_444_s.COEFF[22] = 757.0 ; 
	FirFilter_444_s.COEFF[23] = 790.0 ; 
	FirFilter_444_s.COEFF[24] = 823.0 ; 
	FirFilter_444_s.COEFF[25] = 856.0 ; 
	FirFilter_444_s.COEFF[26] = 889.0 ; 
	FirFilter_444_s.COEFF[27] = 922.0 ; 
	FirFilter_444_s.COEFF[28] = 955.0 ; 
	FirFilter_444_s.COEFF[29] = 988.0 ; 
	FirFilter_444_s.COEFF[30] = 1021.0 ; 
	FirFilter_444_s.COEFF[31] = 1054.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin124_FirFilter_Fiss_656_690_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin124_FirFilter_Fiss_656_690_split[1], i) * FirFilter_444_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin124_FirFilter_Fiss_656_690_split[1]) ; 
		push_float(&SplitJoin124_FirFilter_Fiss_656_690_join[1], sum) ; 
 {
		FOR(int, streamItVar182, 0,  < , 6, streamItVar182++) {
			pop_void(&SplitJoin124_FirFilter_Fiss_656_690_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_445
	 {
	FirFilter_445_s.COEFF[0] = 31.0 ; 
	FirFilter_445_s.COEFF[1] = 64.0 ; 
	FirFilter_445_s.COEFF[2] = 97.0 ; 
	FirFilter_445_s.COEFF[3] = 130.0 ; 
	FirFilter_445_s.COEFF[4] = 163.0 ; 
	FirFilter_445_s.COEFF[5] = 196.0 ; 
	FirFilter_445_s.COEFF[6] = 229.0 ; 
	FirFilter_445_s.COEFF[7] = 262.0 ; 
	FirFilter_445_s.COEFF[8] = 295.0 ; 
	FirFilter_445_s.COEFF[9] = 328.0 ; 
	FirFilter_445_s.COEFF[10] = 361.0 ; 
	FirFilter_445_s.COEFF[11] = 394.0 ; 
	FirFilter_445_s.COEFF[12] = 427.0 ; 
	FirFilter_445_s.COEFF[13] = 460.0 ; 
	FirFilter_445_s.COEFF[14] = 493.0 ; 
	FirFilter_445_s.COEFF[15] = 526.0 ; 
	FirFilter_445_s.COEFF[16] = 559.0 ; 
	FirFilter_445_s.COEFF[17] = 592.0 ; 
	FirFilter_445_s.COEFF[18] = 625.0 ; 
	FirFilter_445_s.COEFF[19] = 658.0 ; 
	FirFilter_445_s.COEFF[20] = 691.0 ; 
	FirFilter_445_s.COEFF[21] = 724.0 ; 
	FirFilter_445_s.COEFF[22] = 757.0 ; 
	FirFilter_445_s.COEFF[23] = 790.0 ; 
	FirFilter_445_s.COEFF[24] = 823.0 ; 
	FirFilter_445_s.COEFF[25] = 856.0 ; 
	FirFilter_445_s.COEFF[26] = 889.0 ; 
	FirFilter_445_s.COEFF[27] = 922.0 ; 
	FirFilter_445_s.COEFF[28] = 955.0 ; 
	FirFilter_445_s.COEFF[29] = 988.0 ; 
	FirFilter_445_s.COEFF[30] = 1021.0 ; 
	FirFilter_445_s.COEFF[31] = 1054.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar183, 0,  < , 2, streamItVar183++) {
			pop_void(&SplitJoin124_FirFilter_Fiss_656_690_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin124_FirFilter_Fiss_656_690_split[2], i) * FirFilter_445_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin124_FirFilter_Fiss_656_690_split[2]) ; 
		push_float(&SplitJoin124_FirFilter_Fiss_656_690_join[2], sum) ; 
 {
		FOR(int, streamItVar184, 0,  < , 5, streamItVar184++) {
			pop_void(&SplitJoin124_FirFilter_Fiss_656_690_split[2]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_446
	 {
	FirFilter_446_s.COEFF[0] = 31.0 ; 
	FirFilter_446_s.COEFF[1] = 64.0 ; 
	FirFilter_446_s.COEFF[2] = 97.0 ; 
	FirFilter_446_s.COEFF[3] = 130.0 ; 
	FirFilter_446_s.COEFF[4] = 163.0 ; 
	FirFilter_446_s.COEFF[5] = 196.0 ; 
	FirFilter_446_s.COEFF[6] = 229.0 ; 
	FirFilter_446_s.COEFF[7] = 262.0 ; 
	FirFilter_446_s.COEFF[8] = 295.0 ; 
	FirFilter_446_s.COEFF[9] = 328.0 ; 
	FirFilter_446_s.COEFF[10] = 361.0 ; 
	FirFilter_446_s.COEFF[11] = 394.0 ; 
	FirFilter_446_s.COEFF[12] = 427.0 ; 
	FirFilter_446_s.COEFF[13] = 460.0 ; 
	FirFilter_446_s.COEFF[14] = 493.0 ; 
	FirFilter_446_s.COEFF[15] = 526.0 ; 
	FirFilter_446_s.COEFF[16] = 559.0 ; 
	FirFilter_446_s.COEFF[17] = 592.0 ; 
	FirFilter_446_s.COEFF[18] = 625.0 ; 
	FirFilter_446_s.COEFF[19] = 658.0 ; 
	FirFilter_446_s.COEFF[20] = 691.0 ; 
	FirFilter_446_s.COEFF[21] = 724.0 ; 
	FirFilter_446_s.COEFF[22] = 757.0 ; 
	FirFilter_446_s.COEFF[23] = 790.0 ; 
	FirFilter_446_s.COEFF[24] = 823.0 ; 
	FirFilter_446_s.COEFF[25] = 856.0 ; 
	FirFilter_446_s.COEFF[26] = 889.0 ; 
	FirFilter_446_s.COEFF[27] = 922.0 ; 
	FirFilter_446_s.COEFF[28] = 955.0 ; 
	FirFilter_446_s.COEFF[29] = 988.0 ; 
	FirFilter_446_s.COEFF[30] = 1021.0 ; 
	FirFilter_446_s.COEFF[31] = 1054.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar185, 0,  < , 3, streamItVar185++) {
			pop_void(&SplitJoin124_FirFilter_Fiss_656_690_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin124_FirFilter_Fiss_656_690_split[3], i) * FirFilter_446_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin124_FirFilter_Fiss_656_690_split[3]) ; 
		push_float(&SplitJoin124_FirFilter_Fiss_656_690_join[3], sum) ; 
 {
		FOR(int, streamItVar186, 0,  < , 4, streamItVar186++) {
			pop_void(&SplitJoin124_FirFilter_Fiss_656_690_split[3]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_447
	 {
	FirFilter_447_s.COEFF[0] = 31.0 ; 
	FirFilter_447_s.COEFF[1] = 64.0 ; 
	FirFilter_447_s.COEFF[2] = 97.0 ; 
	FirFilter_447_s.COEFF[3] = 130.0 ; 
	FirFilter_447_s.COEFF[4] = 163.0 ; 
	FirFilter_447_s.COEFF[5] = 196.0 ; 
	FirFilter_447_s.COEFF[6] = 229.0 ; 
	FirFilter_447_s.COEFF[7] = 262.0 ; 
	FirFilter_447_s.COEFF[8] = 295.0 ; 
	FirFilter_447_s.COEFF[9] = 328.0 ; 
	FirFilter_447_s.COEFF[10] = 361.0 ; 
	FirFilter_447_s.COEFF[11] = 394.0 ; 
	FirFilter_447_s.COEFF[12] = 427.0 ; 
	FirFilter_447_s.COEFF[13] = 460.0 ; 
	FirFilter_447_s.COEFF[14] = 493.0 ; 
	FirFilter_447_s.COEFF[15] = 526.0 ; 
	FirFilter_447_s.COEFF[16] = 559.0 ; 
	FirFilter_447_s.COEFF[17] = 592.0 ; 
	FirFilter_447_s.COEFF[18] = 625.0 ; 
	FirFilter_447_s.COEFF[19] = 658.0 ; 
	FirFilter_447_s.COEFF[20] = 691.0 ; 
	FirFilter_447_s.COEFF[21] = 724.0 ; 
	FirFilter_447_s.COEFF[22] = 757.0 ; 
	FirFilter_447_s.COEFF[23] = 790.0 ; 
	FirFilter_447_s.COEFF[24] = 823.0 ; 
	FirFilter_447_s.COEFF[25] = 856.0 ; 
	FirFilter_447_s.COEFF[26] = 889.0 ; 
	FirFilter_447_s.COEFF[27] = 922.0 ; 
	FirFilter_447_s.COEFF[28] = 955.0 ; 
	FirFilter_447_s.COEFF[29] = 988.0 ; 
	FirFilter_447_s.COEFF[30] = 1021.0 ; 
	FirFilter_447_s.COEFF[31] = 1054.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar187, 0,  < , 4, streamItVar187++) {
			pop_void(&SplitJoin124_FirFilter_Fiss_656_690_split[4]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin124_FirFilter_Fiss_656_690_split[4], i) * FirFilter_447_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin124_FirFilter_Fiss_656_690_split[4]) ; 
		push_float(&SplitJoin124_FirFilter_Fiss_656_690_join[4], sum) ; 
 {
		FOR(int, streamItVar188, 0,  < , 3, streamItVar188++) {
			pop_void(&SplitJoin124_FirFilter_Fiss_656_690_split[4]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_448
	 {
	FirFilter_448_s.COEFF[0] = 31.0 ; 
	FirFilter_448_s.COEFF[1] = 64.0 ; 
	FirFilter_448_s.COEFF[2] = 97.0 ; 
	FirFilter_448_s.COEFF[3] = 130.0 ; 
	FirFilter_448_s.COEFF[4] = 163.0 ; 
	FirFilter_448_s.COEFF[5] = 196.0 ; 
	FirFilter_448_s.COEFF[6] = 229.0 ; 
	FirFilter_448_s.COEFF[7] = 262.0 ; 
	FirFilter_448_s.COEFF[8] = 295.0 ; 
	FirFilter_448_s.COEFF[9] = 328.0 ; 
	FirFilter_448_s.COEFF[10] = 361.0 ; 
	FirFilter_448_s.COEFF[11] = 394.0 ; 
	FirFilter_448_s.COEFF[12] = 427.0 ; 
	FirFilter_448_s.COEFF[13] = 460.0 ; 
	FirFilter_448_s.COEFF[14] = 493.0 ; 
	FirFilter_448_s.COEFF[15] = 526.0 ; 
	FirFilter_448_s.COEFF[16] = 559.0 ; 
	FirFilter_448_s.COEFF[17] = 592.0 ; 
	FirFilter_448_s.COEFF[18] = 625.0 ; 
	FirFilter_448_s.COEFF[19] = 658.0 ; 
	FirFilter_448_s.COEFF[20] = 691.0 ; 
	FirFilter_448_s.COEFF[21] = 724.0 ; 
	FirFilter_448_s.COEFF[22] = 757.0 ; 
	FirFilter_448_s.COEFF[23] = 790.0 ; 
	FirFilter_448_s.COEFF[24] = 823.0 ; 
	FirFilter_448_s.COEFF[25] = 856.0 ; 
	FirFilter_448_s.COEFF[26] = 889.0 ; 
	FirFilter_448_s.COEFF[27] = 922.0 ; 
	FirFilter_448_s.COEFF[28] = 955.0 ; 
	FirFilter_448_s.COEFF[29] = 988.0 ; 
	FirFilter_448_s.COEFF[30] = 1021.0 ; 
	FirFilter_448_s.COEFF[31] = 1054.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar189, 0,  < , 5, streamItVar189++) {
			pop_void(&SplitJoin124_FirFilter_Fiss_656_690_split[5]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin124_FirFilter_Fiss_656_690_split[5], i) * FirFilter_448_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin124_FirFilter_Fiss_656_690_split[5]) ; 
		push_float(&SplitJoin124_FirFilter_Fiss_656_690_join[5], sum) ; 
 {
		FOR(int, streamItVar190, 0,  < , 2, streamItVar190++) {
			pop_void(&SplitJoin124_FirFilter_Fiss_656_690_split[5]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_449
	 {
	FirFilter_449_s.COEFF[0] = 31.0 ; 
	FirFilter_449_s.COEFF[1] = 64.0 ; 
	FirFilter_449_s.COEFF[2] = 97.0 ; 
	FirFilter_449_s.COEFF[3] = 130.0 ; 
	FirFilter_449_s.COEFF[4] = 163.0 ; 
	FirFilter_449_s.COEFF[5] = 196.0 ; 
	FirFilter_449_s.COEFF[6] = 229.0 ; 
	FirFilter_449_s.COEFF[7] = 262.0 ; 
	FirFilter_449_s.COEFF[8] = 295.0 ; 
	FirFilter_449_s.COEFF[9] = 328.0 ; 
	FirFilter_449_s.COEFF[10] = 361.0 ; 
	FirFilter_449_s.COEFF[11] = 394.0 ; 
	FirFilter_449_s.COEFF[12] = 427.0 ; 
	FirFilter_449_s.COEFF[13] = 460.0 ; 
	FirFilter_449_s.COEFF[14] = 493.0 ; 
	FirFilter_449_s.COEFF[15] = 526.0 ; 
	FirFilter_449_s.COEFF[16] = 559.0 ; 
	FirFilter_449_s.COEFF[17] = 592.0 ; 
	FirFilter_449_s.COEFF[18] = 625.0 ; 
	FirFilter_449_s.COEFF[19] = 658.0 ; 
	FirFilter_449_s.COEFF[20] = 691.0 ; 
	FirFilter_449_s.COEFF[21] = 724.0 ; 
	FirFilter_449_s.COEFF[22] = 757.0 ; 
	FirFilter_449_s.COEFF[23] = 790.0 ; 
	FirFilter_449_s.COEFF[24] = 823.0 ; 
	FirFilter_449_s.COEFF[25] = 856.0 ; 
	FirFilter_449_s.COEFF[26] = 889.0 ; 
	FirFilter_449_s.COEFF[27] = 922.0 ; 
	FirFilter_449_s.COEFF[28] = 955.0 ; 
	FirFilter_449_s.COEFF[29] = 988.0 ; 
	FirFilter_449_s.COEFF[30] = 1021.0 ; 
	FirFilter_449_s.COEFF[31] = 1054.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar191, 0,  < , 6, streamItVar191++) {
			pop_void(&SplitJoin124_FirFilter_Fiss_656_690_split[6]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin124_FirFilter_Fiss_656_690_split[6], i) * FirFilter_449_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin124_FirFilter_Fiss_656_690_split[6]) ; 
		push_float(&SplitJoin124_FirFilter_Fiss_656_690_join[6], sum) ; 
 {
		pop_void(&SplitJoin124_FirFilter_Fiss_656_690_split[6]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_450
	 {
	FirFilter_450_s.COEFF[0] = 31.0 ; 
	FirFilter_450_s.COEFF[1] = 64.0 ; 
	FirFilter_450_s.COEFF[2] = 97.0 ; 
	FirFilter_450_s.COEFF[3] = 130.0 ; 
	FirFilter_450_s.COEFF[4] = 163.0 ; 
	FirFilter_450_s.COEFF[5] = 196.0 ; 
	FirFilter_450_s.COEFF[6] = 229.0 ; 
	FirFilter_450_s.COEFF[7] = 262.0 ; 
	FirFilter_450_s.COEFF[8] = 295.0 ; 
	FirFilter_450_s.COEFF[9] = 328.0 ; 
	FirFilter_450_s.COEFF[10] = 361.0 ; 
	FirFilter_450_s.COEFF[11] = 394.0 ; 
	FirFilter_450_s.COEFF[12] = 427.0 ; 
	FirFilter_450_s.COEFF[13] = 460.0 ; 
	FirFilter_450_s.COEFF[14] = 493.0 ; 
	FirFilter_450_s.COEFF[15] = 526.0 ; 
	FirFilter_450_s.COEFF[16] = 559.0 ; 
	FirFilter_450_s.COEFF[17] = 592.0 ; 
	FirFilter_450_s.COEFF[18] = 625.0 ; 
	FirFilter_450_s.COEFF[19] = 658.0 ; 
	FirFilter_450_s.COEFF[20] = 691.0 ; 
	FirFilter_450_s.COEFF[21] = 724.0 ; 
	FirFilter_450_s.COEFF[22] = 757.0 ; 
	FirFilter_450_s.COEFF[23] = 790.0 ; 
	FirFilter_450_s.COEFF[24] = 823.0 ; 
	FirFilter_450_s.COEFF[25] = 856.0 ; 
	FirFilter_450_s.COEFF[26] = 889.0 ; 
	FirFilter_450_s.COEFF[27] = 922.0 ; 
	FirFilter_450_s.COEFF[28] = 955.0 ; 
	FirFilter_450_s.COEFF[29] = 988.0 ; 
	FirFilter_450_s.COEFF[30] = 1021.0 ; 
	FirFilter_450_s.COEFF[31] = 1054.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar192, 0,  < , 7, streamItVar192++) {
			pop_void(&SplitJoin124_FirFilter_Fiss_656_690_split[7]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin124_FirFilter_Fiss_656_690_split[7], i) * FirFilter_450_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin124_FirFilter_Fiss_656_690_split[7]) ; 
		push_float(&SplitJoin124_FirFilter_Fiss_656_690_join[7], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_442
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_442DownSamp_257, pop_float(&SplitJoin124_FirFilter_Fiss_656_690_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DownSamp_257
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		push_float(&DownSamp_257UpSamp_258, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_442DownSamp_257)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_442DownSamp_257) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: UpSamp_258
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		push_float(&UpSamp_258WEIGHTED_ROUND_ROBIN_Splitter_451, pop_float(&DownSamp_257UpSamp_258)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			push_float(&UpSamp_258WEIGHTED_ROUND_ROBIN_Splitter_451, 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_451
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&SplitJoin126_Delay_N_Fiss_657_691_split[__iter_], pop_float(&UpSamp_258WEIGHTED_ROUND_ROBIN_Splitter_451));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_453
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin126_Delay_N_Fiss_657_691_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_454
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin126_Delay_N_Fiss_657_691_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_455
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin126_Delay_N_Fiss_657_691_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_456
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin126_Delay_N_Fiss_657_691_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_457
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin126_Delay_N_Fiss_657_691_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_458
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin126_Delay_N_Fiss_657_691_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_459
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin126_Delay_N_Fiss_657_691_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_460
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin126_Delay_N_Fiss_657_691_join[7], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_452
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_452DUPLICATE_Splitter_461, pop_float(&SplitJoin126_Delay_N_Fiss_657_691_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_461
	FOR(uint32_t, __iter_init_, 0, <, 31, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_452DUPLICATE_Splitter_461);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin128_FirFilter_Fiss_658_692_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_463
	 {
	FirFilter_463_s.COEFF[0] = 12.0 ; 
	FirFilter_463_s.COEFF[1] = 16.0 ; 
	FirFilter_463_s.COEFF[2] = 20.0 ; 
	FirFilter_463_s.COEFF[3] = 24.0 ; 
	FirFilter_463_s.COEFF[4] = 28.0 ; 
	FirFilter_463_s.COEFF[5] = 32.0 ; 
	FirFilter_463_s.COEFF[6] = 36.0 ; 
	FirFilter_463_s.COEFF[7] = 40.0 ; 
	FirFilter_463_s.COEFF[8] = 44.0 ; 
	FirFilter_463_s.COEFF[9] = 48.0 ; 
	FirFilter_463_s.COEFF[10] = 52.0 ; 
	FirFilter_463_s.COEFF[11] = 56.0 ; 
	FirFilter_463_s.COEFF[12] = 60.0 ; 
	FirFilter_463_s.COEFF[13] = 64.0 ; 
	FirFilter_463_s.COEFF[14] = 68.0 ; 
	FirFilter_463_s.COEFF[15] = 72.0 ; 
	FirFilter_463_s.COEFF[16] = 76.0 ; 
	FirFilter_463_s.COEFF[17] = 80.0 ; 
	FirFilter_463_s.COEFF[18] = 84.0 ; 
	FirFilter_463_s.COEFF[19] = 88.0 ; 
	FirFilter_463_s.COEFF[20] = 92.0 ; 
	FirFilter_463_s.COEFF[21] = 96.0 ; 
	FirFilter_463_s.COEFF[22] = 100.0 ; 
	FirFilter_463_s.COEFF[23] = 104.0 ; 
	FirFilter_463_s.COEFF[24] = 108.0 ; 
	FirFilter_463_s.COEFF[25] = 112.0 ; 
	FirFilter_463_s.COEFF[26] = 116.0 ; 
	FirFilter_463_s.COEFF[27] = 120.0 ; 
	FirFilter_463_s.COEFF[28] = 124.0 ; 
	FirFilter_463_s.COEFF[29] = 128.0 ; 
	FirFilter_463_s.COEFF[30] = 132.0 ; 
	FirFilter_463_s.COEFF[31] = 136.0 ; 
}
//--------------------------------
// --- init: FirFilter_464
	 {
	FirFilter_464_s.COEFF[0] = 12.0 ; 
	FirFilter_464_s.COEFF[1] = 16.0 ; 
	FirFilter_464_s.COEFF[2] = 20.0 ; 
	FirFilter_464_s.COEFF[3] = 24.0 ; 
	FirFilter_464_s.COEFF[4] = 28.0 ; 
	FirFilter_464_s.COEFF[5] = 32.0 ; 
	FirFilter_464_s.COEFF[6] = 36.0 ; 
	FirFilter_464_s.COEFF[7] = 40.0 ; 
	FirFilter_464_s.COEFF[8] = 44.0 ; 
	FirFilter_464_s.COEFF[9] = 48.0 ; 
	FirFilter_464_s.COEFF[10] = 52.0 ; 
	FirFilter_464_s.COEFF[11] = 56.0 ; 
	FirFilter_464_s.COEFF[12] = 60.0 ; 
	FirFilter_464_s.COEFF[13] = 64.0 ; 
	FirFilter_464_s.COEFF[14] = 68.0 ; 
	FirFilter_464_s.COEFF[15] = 72.0 ; 
	FirFilter_464_s.COEFF[16] = 76.0 ; 
	FirFilter_464_s.COEFF[17] = 80.0 ; 
	FirFilter_464_s.COEFF[18] = 84.0 ; 
	FirFilter_464_s.COEFF[19] = 88.0 ; 
	FirFilter_464_s.COEFF[20] = 92.0 ; 
	FirFilter_464_s.COEFF[21] = 96.0 ; 
	FirFilter_464_s.COEFF[22] = 100.0 ; 
	FirFilter_464_s.COEFF[23] = 104.0 ; 
	FirFilter_464_s.COEFF[24] = 108.0 ; 
	FirFilter_464_s.COEFF[25] = 112.0 ; 
	FirFilter_464_s.COEFF[26] = 116.0 ; 
	FirFilter_464_s.COEFF[27] = 120.0 ; 
	FirFilter_464_s.COEFF[28] = 124.0 ; 
	FirFilter_464_s.COEFF[29] = 128.0 ; 
	FirFilter_464_s.COEFF[30] = 132.0 ; 
	FirFilter_464_s.COEFF[31] = 136.0 ; 
}
//--------------------------------
// --- init: FirFilter_465
	 {
	FirFilter_465_s.COEFF[0] = 12.0 ; 
	FirFilter_465_s.COEFF[1] = 16.0 ; 
	FirFilter_465_s.COEFF[2] = 20.0 ; 
	FirFilter_465_s.COEFF[3] = 24.0 ; 
	FirFilter_465_s.COEFF[4] = 28.0 ; 
	FirFilter_465_s.COEFF[5] = 32.0 ; 
	FirFilter_465_s.COEFF[6] = 36.0 ; 
	FirFilter_465_s.COEFF[7] = 40.0 ; 
	FirFilter_465_s.COEFF[8] = 44.0 ; 
	FirFilter_465_s.COEFF[9] = 48.0 ; 
	FirFilter_465_s.COEFF[10] = 52.0 ; 
	FirFilter_465_s.COEFF[11] = 56.0 ; 
	FirFilter_465_s.COEFF[12] = 60.0 ; 
	FirFilter_465_s.COEFF[13] = 64.0 ; 
	FirFilter_465_s.COEFF[14] = 68.0 ; 
	FirFilter_465_s.COEFF[15] = 72.0 ; 
	FirFilter_465_s.COEFF[16] = 76.0 ; 
	FirFilter_465_s.COEFF[17] = 80.0 ; 
	FirFilter_465_s.COEFF[18] = 84.0 ; 
	FirFilter_465_s.COEFF[19] = 88.0 ; 
	FirFilter_465_s.COEFF[20] = 92.0 ; 
	FirFilter_465_s.COEFF[21] = 96.0 ; 
	FirFilter_465_s.COEFF[22] = 100.0 ; 
	FirFilter_465_s.COEFF[23] = 104.0 ; 
	FirFilter_465_s.COEFF[24] = 108.0 ; 
	FirFilter_465_s.COEFF[25] = 112.0 ; 
	FirFilter_465_s.COEFF[26] = 116.0 ; 
	FirFilter_465_s.COEFF[27] = 120.0 ; 
	FirFilter_465_s.COEFF[28] = 124.0 ; 
	FirFilter_465_s.COEFF[29] = 128.0 ; 
	FirFilter_465_s.COEFF[30] = 132.0 ; 
	FirFilter_465_s.COEFF[31] = 136.0 ; 
}
//--------------------------------
// --- init: FirFilter_466
	 {
	FirFilter_466_s.COEFF[0] = 12.0 ; 
	FirFilter_466_s.COEFF[1] = 16.0 ; 
	FirFilter_466_s.COEFF[2] = 20.0 ; 
	FirFilter_466_s.COEFF[3] = 24.0 ; 
	FirFilter_466_s.COEFF[4] = 28.0 ; 
	FirFilter_466_s.COEFF[5] = 32.0 ; 
	FirFilter_466_s.COEFF[6] = 36.0 ; 
	FirFilter_466_s.COEFF[7] = 40.0 ; 
	FirFilter_466_s.COEFF[8] = 44.0 ; 
	FirFilter_466_s.COEFF[9] = 48.0 ; 
	FirFilter_466_s.COEFF[10] = 52.0 ; 
	FirFilter_466_s.COEFF[11] = 56.0 ; 
	FirFilter_466_s.COEFF[12] = 60.0 ; 
	FirFilter_466_s.COEFF[13] = 64.0 ; 
	FirFilter_466_s.COEFF[14] = 68.0 ; 
	FirFilter_466_s.COEFF[15] = 72.0 ; 
	FirFilter_466_s.COEFF[16] = 76.0 ; 
	FirFilter_466_s.COEFF[17] = 80.0 ; 
	FirFilter_466_s.COEFF[18] = 84.0 ; 
	FirFilter_466_s.COEFF[19] = 88.0 ; 
	FirFilter_466_s.COEFF[20] = 92.0 ; 
	FirFilter_466_s.COEFF[21] = 96.0 ; 
	FirFilter_466_s.COEFF[22] = 100.0 ; 
	FirFilter_466_s.COEFF[23] = 104.0 ; 
	FirFilter_466_s.COEFF[24] = 108.0 ; 
	FirFilter_466_s.COEFF[25] = 112.0 ; 
	FirFilter_466_s.COEFF[26] = 116.0 ; 
	FirFilter_466_s.COEFF[27] = 120.0 ; 
	FirFilter_466_s.COEFF[28] = 124.0 ; 
	FirFilter_466_s.COEFF[29] = 128.0 ; 
	FirFilter_466_s.COEFF[30] = 132.0 ; 
	FirFilter_466_s.COEFF[31] = 136.0 ; 
}
//--------------------------------
// --- init: FirFilter_467
	 {
	FirFilter_467_s.COEFF[0] = 12.0 ; 
	FirFilter_467_s.COEFF[1] = 16.0 ; 
	FirFilter_467_s.COEFF[2] = 20.0 ; 
	FirFilter_467_s.COEFF[3] = 24.0 ; 
	FirFilter_467_s.COEFF[4] = 28.0 ; 
	FirFilter_467_s.COEFF[5] = 32.0 ; 
	FirFilter_467_s.COEFF[6] = 36.0 ; 
	FirFilter_467_s.COEFF[7] = 40.0 ; 
	FirFilter_467_s.COEFF[8] = 44.0 ; 
	FirFilter_467_s.COEFF[9] = 48.0 ; 
	FirFilter_467_s.COEFF[10] = 52.0 ; 
	FirFilter_467_s.COEFF[11] = 56.0 ; 
	FirFilter_467_s.COEFF[12] = 60.0 ; 
	FirFilter_467_s.COEFF[13] = 64.0 ; 
	FirFilter_467_s.COEFF[14] = 68.0 ; 
	FirFilter_467_s.COEFF[15] = 72.0 ; 
	FirFilter_467_s.COEFF[16] = 76.0 ; 
	FirFilter_467_s.COEFF[17] = 80.0 ; 
	FirFilter_467_s.COEFF[18] = 84.0 ; 
	FirFilter_467_s.COEFF[19] = 88.0 ; 
	FirFilter_467_s.COEFF[20] = 92.0 ; 
	FirFilter_467_s.COEFF[21] = 96.0 ; 
	FirFilter_467_s.COEFF[22] = 100.0 ; 
	FirFilter_467_s.COEFF[23] = 104.0 ; 
	FirFilter_467_s.COEFF[24] = 108.0 ; 
	FirFilter_467_s.COEFF[25] = 112.0 ; 
	FirFilter_467_s.COEFF[26] = 116.0 ; 
	FirFilter_467_s.COEFF[27] = 120.0 ; 
	FirFilter_467_s.COEFF[28] = 124.0 ; 
	FirFilter_467_s.COEFF[29] = 128.0 ; 
	FirFilter_467_s.COEFF[30] = 132.0 ; 
	FirFilter_467_s.COEFF[31] = 136.0 ; 
}
//--------------------------------
// --- init: FirFilter_468
	 {
	FirFilter_468_s.COEFF[0] = 12.0 ; 
	FirFilter_468_s.COEFF[1] = 16.0 ; 
	FirFilter_468_s.COEFF[2] = 20.0 ; 
	FirFilter_468_s.COEFF[3] = 24.0 ; 
	FirFilter_468_s.COEFF[4] = 28.0 ; 
	FirFilter_468_s.COEFF[5] = 32.0 ; 
	FirFilter_468_s.COEFF[6] = 36.0 ; 
	FirFilter_468_s.COEFF[7] = 40.0 ; 
	FirFilter_468_s.COEFF[8] = 44.0 ; 
	FirFilter_468_s.COEFF[9] = 48.0 ; 
	FirFilter_468_s.COEFF[10] = 52.0 ; 
	FirFilter_468_s.COEFF[11] = 56.0 ; 
	FirFilter_468_s.COEFF[12] = 60.0 ; 
	FirFilter_468_s.COEFF[13] = 64.0 ; 
	FirFilter_468_s.COEFF[14] = 68.0 ; 
	FirFilter_468_s.COEFF[15] = 72.0 ; 
	FirFilter_468_s.COEFF[16] = 76.0 ; 
	FirFilter_468_s.COEFF[17] = 80.0 ; 
	FirFilter_468_s.COEFF[18] = 84.0 ; 
	FirFilter_468_s.COEFF[19] = 88.0 ; 
	FirFilter_468_s.COEFF[20] = 92.0 ; 
	FirFilter_468_s.COEFF[21] = 96.0 ; 
	FirFilter_468_s.COEFF[22] = 100.0 ; 
	FirFilter_468_s.COEFF[23] = 104.0 ; 
	FirFilter_468_s.COEFF[24] = 108.0 ; 
	FirFilter_468_s.COEFF[25] = 112.0 ; 
	FirFilter_468_s.COEFF[26] = 116.0 ; 
	FirFilter_468_s.COEFF[27] = 120.0 ; 
	FirFilter_468_s.COEFF[28] = 124.0 ; 
	FirFilter_468_s.COEFF[29] = 128.0 ; 
	FirFilter_468_s.COEFF[30] = 132.0 ; 
	FirFilter_468_s.COEFF[31] = 136.0 ; 
}
//--------------------------------
// --- init: FirFilter_469
	 {
	FirFilter_469_s.COEFF[0] = 12.0 ; 
	FirFilter_469_s.COEFF[1] = 16.0 ; 
	FirFilter_469_s.COEFF[2] = 20.0 ; 
	FirFilter_469_s.COEFF[3] = 24.0 ; 
	FirFilter_469_s.COEFF[4] = 28.0 ; 
	FirFilter_469_s.COEFF[5] = 32.0 ; 
	FirFilter_469_s.COEFF[6] = 36.0 ; 
	FirFilter_469_s.COEFF[7] = 40.0 ; 
	FirFilter_469_s.COEFF[8] = 44.0 ; 
	FirFilter_469_s.COEFF[9] = 48.0 ; 
	FirFilter_469_s.COEFF[10] = 52.0 ; 
	FirFilter_469_s.COEFF[11] = 56.0 ; 
	FirFilter_469_s.COEFF[12] = 60.0 ; 
	FirFilter_469_s.COEFF[13] = 64.0 ; 
	FirFilter_469_s.COEFF[14] = 68.0 ; 
	FirFilter_469_s.COEFF[15] = 72.0 ; 
	FirFilter_469_s.COEFF[16] = 76.0 ; 
	FirFilter_469_s.COEFF[17] = 80.0 ; 
	FirFilter_469_s.COEFF[18] = 84.0 ; 
	FirFilter_469_s.COEFF[19] = 88.0 ; 
	FirFilter_469_s.COEFF[20] = 92.0 ; 
	FirFilter_469_s.COEFF[21] = 96.0 ; 
	FirFilter_469_s.COEFF[22] = 100.0 ; 
	FirFilter_469_s.COEFF[23] = 104.0 ; 
	FirFilter_469_s.COEFF[24] = 108.0 ; 
	FirFilter_469_s.COEFF[25] = 112.0 ; 
	FirFilter_469_s.COEFF[26] = 116.0 ; 
	FirFilter_469_s.COEFF[27] = 120.0 ; 
	FirFilter_469_s.COEFF[28] = 124.0 ; 
	FirFilter_469_s.COEFF[29] = 128.0 ; 
	FirFilter_469_s.COEFF[30] = 132.0 ; 
	FirFilter_469_s.COEFF[31] = 136.0 ; 
}
//--------------------------------
// --- init: FirFilter_470
	 {
	FirFilter_470_s.COEFF[0] = 12.0 ; 
	FirFilter_470_s.COEFF[1] = 16.0 ; 
	FirFilter_470_s.COEFF[2] = 20.0 ; 
	FirFilter_470_s.COEFF[3] = 24.0 ; 
	FirFilter_470_s.COEFF[4] = 28.0 ; 
	FirFilter_470_s.COEFF[5] = 32.0 ; 
	FirFilter_470_s.COEFF[6] = 36.0 ; 
	FirFilter_470_s.COEFF[7] = 40.0 ; 
	FirFilter_470_s.COEFF[8] = 44.0 ; 
	FirFilter_470_s.COEFF[9] = 48.0 ; 
	FirFilter_470_s.COEFF[10] = 52.0 ; 
	FirFilter_470_s.COEFF[11] = 56.0 ; 
	FirFilter_470_s.COEFF[12] = 60.0 ; 
	FirFilter_470_s.COEFF[13] = 64.0 ; 
	FirFilter_470_s.COEFF[14] = 68.0 ; 
	FirFilter_470_s.COEFF[15] = 72.0 ; 
	FirFilter_470_s.COEFF[16] = 76.0 ; 
	FirFilter_470_s.COEFF[17] = 80.0 ; 
	FirFilter_470_s.COEFF[18] = 84.0 ; 
	FirFilter_470_s.COEFF[19] = 88.0 ; 
	FirFilter_470_s.COEFF[20] = 92.0 ; 
	FirFilter_470_s.COEFF[21] = 96.0 ; 
	FirFilter_470_s.COEFF[22] = 100.0 ; 
	FirFilter_470_s.COEFF[23] = 104.0 ; 
	FirFilter_470_s.COEFF[24] = 108.0 ; 
	FirFilter_470_s.COEFF[25] = 112.0 ; 
	FirFilter_470_s.COEFF[26] = 116.0 ; 
	FirFilter_470_s.COEFF[27] = 120.0 ; 
	FirFilter_470_s.COEFF[28] = 124.0 ; 
	FirFilter_470_s.COEFF[29] = 128.0 ; 
	FirFilter_470_s.COEFF[30] = 132.0 ; 
	FirFilter_470_s.COEFF[31] = 136.0 ; 
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_471
	FOR(uint32_t, __iter_init_, 0, <, 66, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&SplitJoin159_Delay_N_Fiss_659_693_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_229_293_641_676_split[4]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_473
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin159_Delay_N_Fiss_659_693_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_474
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin159_Delay_N_Fiss_659_693_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_475
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin159_Delay_N_Fiss_659_693_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_476
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin159_Delay_N_Fiss_659_693_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_477
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin159_Delay_N_Fiss_659_693_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_478
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin159_Delay_N_Fiss_659_693_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_479
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin159_Delay_N_Fiss_659_693_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_480
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin159_Delay_N_Fiss_659_693_join[7], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_472
	FOR(uint32_t, __iter_init_, 0, <, 37, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_472DUPLICATE_Splitter_481, pop_float(&SplitJoin159_Delay_N_Fiss_659_693_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_481
	FOR(uint32_t, __iter_init_, 0, <, 295, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_472DUPLICATE_Splitter_481);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin161_FirFilter_Fiss_660_694_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_483
	 {
	FirFilter_483_s.COEFF[0] = 41.0 ; 
	FirFilter_483_s.COEFF[1] = 74.0 ; 
	FirFilter_483_s.COEFF[2] = 107.0 ; 
	FirFilter_483_s.COEFF[3] = 140.0 ; 
	FirFilter_483_s.COEFF[4] = 173.0 ; 
	FirFilter_483_s.COEFF[5] = 206.0 ; 
	FirFilter_483_s.COEFF[6] = 239.0 ; 
	FirFilter_483_s.COEFF[7] = 272.0 ; 
	FirFilter_483_s.COEFF[8] = 305.0 ; 
	FirFilter_483_s.COEFF[9] = 338.0 ; 
	FirFilter_483_s.COEFF[10] = 371.0 ; 
	FirFilter_483_s.COEFF[11] = 404.0 ; 
	FirFilter_483_s.COEFF[12] = 437.0 ; 
	FirFilter_483_s.COEFF[13] = 470.0 ; 
	FirFilter_483_s.COEFF[14] = 503.0 ; 
	FirFilter_483_s.COEFF[15] = 536.0 ; 
	FirFilter_483_s.COEFF[16] = 569.0 ; 
	FirFilter_483_s.COEFF[17] = 602.0 ; 
	FirFilter_483_s.COEFF[18] = 635.0 ; 
	FirFilter_483_s.COEFF[19] = 668.0 ; 
	FirFilter_483_s.COEFF[20] = 701.0 ; 
	FirFilter_483_s.COEFF[21] = 734.0 ; 
	FirFilter_483_s.COEFF[22] = 767.0 ; 
	FirFilter_483_s.COEFF[23] = 800.0 ; 
	FirFilter_483_s.COEFF[24] = 833.0 ; 
	FirFilter_483_s.COEFF[25] = 866.0 ; 
	FirFilter_483_s.COEFF[26] = 899.0 ; 
	FirFilter_483_s.COEFF[27] = 932.0 ; 
	FirFilter_483_s.COEFF[28] = 965.0 ; 
	FirFilter_483_s.COEFF[29] = 998.0 ; 
	FirFilter_483_s.COEFF[30] = 1031.0 ; 
	FirFilter_483_s.COEFF[31] = 1064.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin161_FirFilter_Fiss_660_694_split[0], i) * FirFilter_483_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin161_FirFilter_Fiss_660_694_split[0]) ; 
		push_float(&SplitJoin161_FirFilter_Fiss_660_694_join[0], sum) ; 
 {
		FOR(int, streamItVar157, 0,  < , 7, streamItVar157++) {
			pop_void(&SplitJoin161_FirFilter_Fiss_660_694_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_484
	 {
	FirFilter_484_s.COEFF[0] = 41.0 ; 
	FirFilter_484_s.COEFF[1] = 74.0 ; 
	FirFilter_484_s.COEFF[2] = 107.0 ; 
	FirFilter_484_s.COEFF[3] = 140.0 ; 
	FirFilter_484_s.COEFF[4] = 173.0 ; 
	FirFilter_484_s.COEFF[5] = 206.0 ; 
	FirFilter_484_s.COEFF[6] = 239.0 ; 
	FirFilter_484_s.COEFF[7] = 272.0 ; 
	FirFilter_484_s.COEFF[8] = 305.0 ; 
	FirFilter_484_s.COEFF[9] = 338.0 ; 
	FirFilter_484_s.COEFF[10] = 371.0 ; 
	FirFilter_484_s.COEFF[11] = 404.0 ; 
	FirFilter_484_s.COEFF[12] = 437.0 ; 
	FirFilter_484_s.COEFF[13] = 470.0 ; 
	FirFilter_484_s.COEFF[14] = 503.0 ; 
	FirFilter_484_s.COEFF[15] = 536.0 ; 
	FirFilter_484_s.COEFF[16] = 569.0 ; 
	FirFilter_484_s.COEFF[17] = 602.0 ; 
	FirFilter_484_s.COEFF[18] = 635.0 ; 
	FirFilter_484_s.COEFF[19] = 668.0 ; 
	FirFilter_484_s.COEFF[20] = 701.0 ; 
	FirFilter_484_s.COEFF[21] = 734.0 ; 
	FirFilter_484_s.COEFF[22] = 767.0 ; 
	FirFilter_484_s.COEFF[23] = 800.0 ; 
	FirFilter_484_s.COEFF[24] = 833.0 ; 
	FirFilter_484_s.COEFF[25] = 866.0 ; 
	FirFilter_484_s.COEFF[26] = 899.0 ; 
	FirFilter_484_s.COEFF[27] = 932.0 ; 
	FirFilter_484_s.COEFF[28] = 965.0 ; 
	FirFilter_484_s.COEFF[29] = 998.0 ; 
	FirFilter_484_s.COEFF[30] = 1031.0 ; 
	FirFilter_484_s.COEFF[31] = 1064.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin161_FirFilter_Fiss_660_694_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin161_FirFilter_Fiss_660_694_split[1], i) * FirFilter_484_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin161_FirFilter_Fiss_660_694_split[1]) ; 
		push_float(&SplitJoin161_FirFilter_Fiss_660_694_join[1], sum) ; 
 {
		FOR(int, streamItVar158, 0,  < , 6, streamItVar158++) {
			pop_void(&SplitJoin161_FirFilter_Fiss_660_694_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_485
	 {
	FirFilter_485_s.COEFF[0] = 41.0 ; 
	FirFilter_485_s.COEFF[1] = 74.0 ; 
	FirFilter_485_s.COEFF[2] = 107.0 ; 
	FirFilter_485_s.COEFF[3] = 140.0 ; 
	FirFilter_485_s.COEFF[4] = 173.0 ; 
	FirFilter_485_s.COEFF[5] = 206.0 ; 
	FirFilter_485_s.COEFF[6] = 239.0 ; 
	FirFilter_485_s.COEFF[7] = 272.0 ; 
	FirFilter_485_s.COEFF[8] = 305.0 ; 
	FirFilter_485_s.COEFF[9] = 338.0 ; 
	FirFilter_485_s.COEFF[10] = 371.0 ; 
	FirFilter_485_s.COEFF[11] = 404.0 ; 
	FirFilter_485_s.COEFF[12] = 437.0 ; 
	FirFilter_485_s.COEFF[13] = 470.0 ; 
	FirFilter_485_s.COEFF[14] = 503.0 ; 
	FirFilter_485_s.COEFF[15] = 536.0 ; 
	FirFilter_485_s.COEFF[16] = 569.0 ; 
	FirFilter_485_s.COEFF[17] = 602.0 ; 
	FirFilter_485_s.COEFF[18] = 635.0 ; 
	FirFilter_485_s.COEFF[19] = 668.0 ; 
	FirFilter_485_s.COEFF[20] = 701.0 ; 
	FirFilter_485_s.COEFF[21] = 734.0 ; 
	FirFilter_485_s.COEFF[22] = 767.0 ; 
	FirFilter_485_s.COEFF[23] = 800.0 ; 
	FirFilter_485_s.COEFF[24] = 833.0 ; 
	FirFilter_485_s.COEFF[25] = 866.0 ; 
	FirFilter_485_s.COEFF[26] = 899.0 ; 
	FirFilter_485_s.COEFF[27] = 932.0 ; 
	FirFilter_485_s.COEFF[28] = 965.0 ; 
	FirFilter_485_s.COEFF[29] = 998.0 ; 
	FirFilter_485_s.COEFF[30] = 1031.0 ; 
	FirFilter_485_s.COEFF[31] = 1064.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar159, 0,  < , 2, streamItVar159++) {
			pop_void(&SplitJoin161_FirFilter_Fiss_660_694_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin161_FirFilter_Fiss_660_694_split[2], i) * FirFilter_485_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin161_FirFilter_Fiss_660_694_split[2]) ; 
		push_float(&SplitJoin161_FirFilter_Fiss_660_694_join[2], sum) ; 
 {
		FOR(int, streamItVar160, 0,  < , 5, streamItVar160++) {
			pop_void(&SplitJoin161_FirFilter_Fiss_660_694_split[2]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_486
	 {
	FirFilter_486_s.COEFF[0] = 41.0 ; 
	FirFilter_486_s.COEFF[1] = 74.0 ; 
	FirFilter_486_s.COEFF[2] = 107.0 ; 
	FirFilter_486_s.COEFF[3] = 140.0 ; 
	FirFilter_486_s.COEFF[4] = 173.0 ; 
	FirFilter_486_s.COEFF[5] = 206.0 ; 
	FirFilter_486_s.COEFF[6] = 239.0 ; 
	FirFilter_486_s.COEFF[7] = 272.0 ; 
	FirFilter_486_s.COEFF[8] = 305.0 ; 
	FirFilter_486_s.COEFF[9] = 338.0 ; 
	FirFilter_486_s.COEFF[10] = 371.0 ; 
	FirFilter_486_s.COEFF[11] = 404.0 ; 
	FirFilter_486_s.COEFF[12] = 437.0 ; 
	FirFilter_486_s.COEFF[13] = 470.0 ; 
	FirFilter_486_s.COEFF[14] = 503.0 ; 
	FirFilter_486_s.COEFF[15] = 536.0 ; 
	FirFilter_486_s.COEFF[16] = 569.0 ; 
	FirFilter_486_s.COEFF[17] = 602.0 ; 
	FirFilter_486_s.COEFF[18] = 635.0 ; 
	FirFilter_486_s.COEFF[19] = 668.0 ; 
	FirFilter_486_s.COEFF[20] = 701.0 ; 
	FirFilter_486_s.COEFF[21] = 734.0 ; 
	FirFilter_486_s.COEFF[22] = 767.0 ; 
	FirFilter_486_s.COEFF[23] = 800.0 ; 
	FirFilter_486_s.COEFF[24] = 833.0 ; 
	FirFilter_486_s.COEFF[25] = 866.0 ; 
	FirFilter_486_s.COEFF[26] = 899.0 ; 
	FirFilter_486_s.COEFF[27] = 932.0 ; 
	FirFilter_486_s.COEFF[28] = 965.0 ; 
	FirFilter_486_s.COEFF[29] = 998.0 ; 
	FirFilter_486_s.COEFF[30] = 1031.0 ; 
	FirFilter_486_s.COEFF[31] = 1064.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar161, 0,  < , 3, streamItVar161++) {
			pop_void(&SplitJoin161_FirFilter_Fiss_660_694_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin161_FirFilter_Fiss_660_694_split[3], i) * FirFilter_486_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin161_FirFilter_Fiss_660_694_split[3]) ; 
		push_float(&SplitJoin161_FirFilter_Fiss_660_694_join[3], sum) ; 
 {
		FOR(int, streamItVar162, 0,  < , 4, streamItVar162++) {
			pop_void(&SplitJoin161_FirFilter_Fiss_660_694_split[3]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_487
	 {
	FirFilter_487_s.COEFF[0] = 41.0 ; 
	FirFilter_487_s.COEFF[1] = 74.0 ; 
	FirFilter_487_s.COEFF[2] = 107.0 ; 
	FirFilter_487_s.COEFF[3] = 140.0 ; 
	FirFilter_487_s.COEFF[4] = 173.0 ; 
	FirFilter_487_s.COEFF[5] = 206.0 ; 
	FirFilter_487_s.COEFF[6] = 239.0 ; 
	FirFilter_487_s.COEFF[7] = 272.0 ; 
	FirFilter_487_s.COEFF[8] = 305.0 ; 
	FirFilter_487_s.COEFF[9] = 338.0 ; 
	FirFilter_487_s.COEFF[10] = 371.0 ; 
	FirFilter_487_s.COEFF[11] = 404.0 ; 
	FirFilter_487_s.COEFF[12] = 437.0 ; 
	FirFilter_487_s.COEFF[13] = 470.0 ; 
	FirFilter_487_s.COEFF[14] = 503.0 ; 
	FirFilter_487_s.COEFF[15] = 536.0 ; 
	FirFilter_487_s.COEFF[16] = 569.0 ; 
	FirFilter_487_s.COEFF[17] = 602.0 ; 
	FirFilter_487_s.COEFF[18] = 635.0 ; 
	FirFilter_487_s.COEFF[19] = 668.0 ; 
	FirFilter_487_s.COEFF[20] = 701.0 ; 
	FirFilter_487_s.COEFF[21] = 734.0 ; 
	FirFilter_487_s.COEFF[22] = 767.0 ; 
	FirFilter_487_s.COEFF[23] = 800.0 ; 
	FirFilter_487_s.COEFF[24] = 833.0 ; 
	FirFilter_487_s.COEFF[25] = 866.0 ; 
	FirFilter_487_s.COEFF[26] = 899.0 ; 
	FirFilter_487_s.COEFF[27] = 932.0 ; 
	FirFilter_487_s.COEFF[28] = 965.0 ; 
	FirFilter_487_s.COEFF[29] = 998.0 ; 
	FirFilter_487_s.COEFF[30] = 1031.0 ; 
	FirFilter_487_s.COEFF[31] = 1064.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar163, 0,  < , 4, streamItVar163++) {
			pop_void(&SplitJoin161_FirFilter_Fiss_660_694_split[4]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin161_FirFilter_Fiss_660_694_split[4], i) * FirFilter_487_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin161_FirFilter_Fiss_660_694_split[4]) ; 
		push_float(&SplitJoin161_FirFilter_Fiss_660_694_join[4], sum) ; 
 {
		FOR(int, streamItVar164, 0,  < , 3, streamItVar164++) {
			pop_void(&SplitJoin161_FirFilter_Fiss_660_694_split[4]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_488
	 {
	FirFilter_488_s.COEFF[0] = 41.0 ; 
	FirFilter_488_s.COEFF[1] = 74.0 ; 
	FirFilter_488_s.COEFF[2] = 107.0 ; 
	FirFilter_488_s.COEFF[3] = 140.0 ; 
	FirFilter_488_s.COEFF[4] = 173.0 ; 
	FirFilter_488_s.COEFF[5] = 206.0 ; 
	FirFilter_488_s.COEFF[6] = 239.0 ; 
	FirFilter_488_s.COEFF[7] = 272.0 ; 
	FirFilter_488_s.COEFF[8] = 305.0 ; 
	FirFilter_488_s.COEFF[9] = 338.0 ; 
	FirFilter_488_s.COEFF[10] = 371.0 ; 
	FirFilter_488_s.COEFF[11] = 404.0 ; 
	FirFilter_488_s.COEFF[12] = 437.0 ; 
	FirFilter_488_s.COEFF[13] = 470.0 ; 
	FirFilter_488_s.COEFF[14] = 503.0 ; 
	FirFilter_488_s.COEFF[15] = 536.0 ; 
	FirFilter_488_s.COEFF[16] = 569.0 ; 
	FirFilter_488_s.COEFF[17] = 602.0 ; 
	FirFilter_488_s.COEFF[18] = 635.0 ; 
	FirFilter_488_s.COEFF[19] = 668.0 ; 
	FirFilter_488_s.COEFF[20] = 701.0 ; 
	FirFilter_488_s.COEFF[21] = 734.0 ; 
	FirFilter_488_s.COEFF[22] = 767.0 ; 
	FirFilter_488_s.COEFF[23] = 800.0 ; 
	FirFilter_488_s.COEFF[24] = 833.0 ; 
	FirFilter_488_s.COEFF[25] = 866.0 ; 
	FirFilter_488_s.COEFF[26] = 899.0 ; 
	FirFilter_488_s.COEFF[27] = 932.0 ; 
	FirFilter_488_s.COEFF[28] = 965.0 ; 
	FirFilter_488_s.COEFF[29] = 998.0 ; 
	FirFilter_488_s.COEFF[30] = 1031.0 ; 
	FirFilter_488_s.COEFF[31] = 1064.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar165, 0,  < , 5, streamItVar165++) {
			pop_void(&SplitJoin161_FirFilter_Fiss_660_694_split[5]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin161_FirFilter_Fiss_660_694_split[5], i) * FirFilter_488_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin161_FirFilter_Fiss_660_694_split[5]) ; 
		push_float(&SplitJoin161_FirFilter_Fiss_660_694_join[5], sum) ; 
 {
		FOR(int, streamItVar166, 0,  < , 2, streamItVar166++) {
			pop_void(&SplitJoin161_FirFilter_Fiss_660_694_split[5]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_489
	 {
	FirFilter_489_s.COEFF[0] = 41.0 ; 
	FirFilter_489_s.COEFF[1] = 74.0 ; 
	FirFilter_489_s.COEFF[2] = 107.0 ; 
	FirFilter_489_s.COEFF[3] = 140.0 ; 
	FirFilter_489_s.COEFF[4] = 173.0 ; 
	FirFilter_489_s.COEFF[5] = 206.0 ; 
	FirFilter_489_s.COEFF[6] = 239.0 ; 
	FirFilter_489_s.COEFF[7] = 272.0 ; 
	FirFilter_489_s.COEFF[8] = 305.0 ; 
	FirFilter_489_s.COEFF[9] = 338.0 ; 
	FirFilter_489_s.COEFF[10] = 371.0 ; 
	FirFilter_489_s.COEFF[11] = 404.0 ; 
	FirFilter_489_s.COEFF[12] = 437.0 ; 
	FirFilter_489_s.COEFF[13] = 470.0 ; 
	FirFilter_489_s.COEFF[14] = 503.0 ; 
	FirFilter_489_s.COEFF[15] = 536.0 ; 
	FirFilter_489_s.COEFF[16] = 569.0 ; 
	FirFilter_489_s.COEFF[17] = 602.0 ; 
	FirFilter_489_s.COEFF[18] = 635.0 ; 
	FirFilter_489_s.COEFF[19] = 668.0 ; 
	FirFilter_489_s.COEFF[20] = 701.0 ; 
	FirFilter_489_s.COEFF[21] = 734.0 ; 
	FirFilter_489_s.COEFF[22] = 767.0 ; 
	FirFilter_489_s.COEFF[23] = 800.0 ; 
	FirFilter_489_s.COEFF[24] = 833.0 ; 
	FirFilter_489_s.COEFF[25] = 866.0 ; 
	FirFilter_489_s.COEFF[26] = 899.0 ; 
	FirFilter_489_s.COEFF[27] = 932.0 ; 
	FirFilter_489_s.COEFF[28] = 965.0 ; 
	FirFilter_489_s.COEFF[29] = 998.0 ; 
	FirFilter_489_s.COEFF[30] = 1031.0 ; 
	FirFilter_489_s.COEFF[31] = 1064.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar167, 0,  < , 6, streamItVar167++) {
			pop_void(&SplitJoin161_FirFilter_Fiss_660_694_split[6]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin161_FirFilter_Fiss_660_694_split[6], i) * FirFilter_489_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin161_FirFilter_Fiss_660_694_split[6]) ; 
		push_float(&SplitJoin161_FirFilter_Fiss_660_694_join[6], sum) ; 
 {
		pop_void(&SplitJoin161_FirFilter_Fiss_660_694_split[6]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_490
	 {
	FirFilter_490_s.COEFF[0] = 41.0 ; 
	FirFilter_490_s.COEFF[1] = 74.0 ; 
	FirFilter_490_s.COEFF[2] = 107.0 ; 
	FirFilter_490_s.COEFF[3] = 140.0 ; 
	FirFilter_490_s.COEFF[4] = 173.0 ; 
	FirFilter_490_s.COEFF[5] = 206.0 ; 
	FirFilter_490_s.COEFF[6] = 239.0 ; 
	FirFilter_490_s.COEFF[7] = 272.0 ; 
	FirFilter_490_s.COEFF[8] = 305.0 ; 
	FirFilter_490_s.COEFF[9] = 338.0 ; 
	FirFilter_490_s.COEFF[10] = 371.0 ; 
	FirFilter_490_s.COEFF[11] = 404.0 ; 
	FirFilter_490_s.COEFF[12] = 437.0 ; 
	FirFilter_490_s.COEFF[13] = 470.0 ; 
	FirFilter_490_s.COEFF[14] = 503.0 ; 
	FirFilter_490_s.COEFF[15] = 536.0 ; 
	FirFilter_490_s.COEFF[16] = 569.0 ; 
	FirFilter_490_s.COEFF[17] = 602.0 ; 
	FirFilter_490_s.COEFF[18] = 635.0 ; 
	FirFilter_490_s.COEFF[19] = 668.0 ; 
	FirFilter_490_s.COEFF[20] = 701.0 ; 
	FirFilter_490_s.COEFF[21] = 734.0 ; 
	FirFilter_490_s.COEFF[22] = 767.0 ; 
	FirFilter_490_s.COEFF[23] = 800.0 ; 
	FirFilter_490_s.COEFF[24] = 833.0 ; 
	FirFilter_490_s.COEFF[25] = 866.0 ; 
	FirFilter_490_s.COEFF[26] = 899.0 ; 
	FirFilter_490_s.COEFF[27] = 932.0 ; 
	FirFilter_490_s.COEFF[28] = 965.0 ; 
	FirFilter_490_s.COEFF[29] = 998.0 ; 
	FirFilter_490_s.COEFF[30] = 1031.0 ; 
	FirFilter_490_s.COEFF[31] = 1064.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar168, 0,  < , 7, streamItVar168++) {
			pop_void(&SplitJoin161_FirFilter_Fiss_660_694_split[7]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin161_FirFilter_Fiss_660_694_split[7], i) * FirFilter_490_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin161_FirFilter_Fiss_660_694_split[7]) ; 
		push_float(&SplitJoin161_FirFilter_Fiss_660_694_join[7], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_482
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_482DownSamp_264, pop_float(&SplitJoin161_FirFilter_Fiss_660_694_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DownSamp_264
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		push_float(&DownSamp_264UpSamp_265, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_482DownSamp_264)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_482DownSamp_264) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: UpSamp_265
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		push_float(&UpSamp_265WEIGHTED_ROUND_ROBIN_Splitter_491, pop_float(&DownSamp_264UpSamp_265)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			push_float(&UpSamp_265WEIGHTED_ROUND_ROBIN_Splitter_491, 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_491
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&SplitJoin163_Delay_N_Fiss_661_695_split[__iter_], pop_float(&UpSamp_265WEIGHTED_ROUND_ROBIN_Splitter_491));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_493
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin163_Delay_N_Fiss_661_695_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_494
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin163_Delay_N_Fiss_661_695_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_495
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin163_Delay_N_Fiss_661_695_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_496
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin163_Delay_N_Fiss_661_695_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_497
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin163_Delay_N_Fiss_661_695_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_498
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin163_Delay_N_Fiss_661_695_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_499
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin163_Delay_N_Fiss_661_695_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_500
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin163_Delay_N_Fiss_661_695_join[7], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_492
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_492DUPLICATE_Splitter_501, pop_float(&SplitJoin163_Delay_N_Fiss_661_695_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_501
	FOR(uint32_t, __iter_init_, 0, <, 31, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_492DUPLICATE_Splitter_501);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin165_FirFilter_Fiss_662_696_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_503
	 {
	FirFilter_503_s.COEFF[0] = 20.0 ; 
	FirFilter_503_s.COEFF[1] = 25.0 ; 
	FirFilter_503_s.COEFF[2] = 30.0 ; 
	FirFilter_503_s.COEFF[3] = 35.0 ; 
	FirFilter_503_s.COEFF[4] = 40.0 ; 
	FirFilter_503_s.COEFF[5] = 45.0 ; 
	FirFilter_503_s.COEFF[6] = 50.0 ; 
	FirFilter_503_s.COEFF[7] = 55.0 ; 
	FirFilter_503_s.COEFF[8] = 60.0 ; 
	FirFilter_503_s.COEFF[9] = 65.0 ; 
	FirFilter_503_s.COEFF[10] = 70.0 ; 
	FirFilter_503_s.COEFF[11] = 75.0 ; 
	FirFilter_503_s.COEFF[12] = 80.0 ; 
	FirFilter_503_s.COEFF[13] = 85.0 ; 
	FirFilter_503_s.COEFF[14] = 90.0 ; 
	FirFilter_503_s.COEFF[15] = 95.0 ; 
	FirFilter_503_s.COEFF[16] = 100.0 ; 
	FirFilter_503_s.COEFF[17] = 105.0 ; 
	FirFilter_503_s.COEFF[18] = 110.0 ; 
	FirFilter_503_s.COEFF[19] = 115.0 ; 
	FirFilter_503_s.COEFF[20] = 120.0 ; 
	FirFilter_503_s.COEFF[21] = 125.0 ; 
	FirFilter_503_s.COEFF[22] = 130.0 ; 
	FirFilter_503_s.COEFF[23] = 135.0 ; 
	FirFilter_503_s.COEFF[24] = 140.0 ; 
	FirFilter_503_s.COEFF[25] = 145.0 ; 
	FirFilter_503_s.COEFF[26] = 150.0 ; 
	FirFilter_503_s.COEFF[27] = 155.0 ; 
	FirFilter_503_s.COEFF[28] = 160.0 ; 
	FirFilter_503_s.COEFF[29] = 165.0 ; 
	FirFilter_503_s.COEFF[30] = 170.0 ; 
	FirFilter_503_s.COEFF[31] = 175.0 ; 
}
//--------------------------------
// --- init: FirFilter_504
	 {
	FirFilter_504_s.COEFF[0] = 20.0 ; 
	FirFilter_504_s.COEFF[1] = 25.0 ; 
	FirFilter_504_s.COEFF[2] = 30.0 ; 
	FirFilter_504_s.COEFF[3] = 35.0 ; 
	FirFilter_504_s.COEFF[4] = 40.0 ; 
	FirFilter_504_s.COEFF[5] = 45.0 ; 
	FirFilter_504_s.COEFF[6] = 50.0 ; 
	FirFilter_504_s.COEFF[7] = 55.0 ; 
	FirFilter_504_s.COEFF[8] = 60.0 ; 
	FirFilter_504_s.COEFF[9] = 65.0 ; 
	FirFilter_504_s.COEFF[10] = 70.0 ; 
	FirFilter_504_s.COEFF[11] = 75.0 ; 
	FirFilter_504_s.COEFF[12] = 80.0 ; 
	FirFilter_504_s.COEFF[13] = 85.0 ; 
	FirFilter_504_s.COEFF[14] = 90.0 ; 
	FirFilter_504_s.COEFF[15] = 95.0 ; 
	FirFilter_504_s.COEFF[16] = 100.0 ; 
	FirFilter_504_s.COEFF[17] = 105.0 ; 
	FirFilter_504_s.COEFF[18] = 110.0 ; 
	FirFilter_504_s.COEFF[19] = 115.0 ; 
	FirFilter_504_s.COEFF[20] = 120.0 ; 
	FirFilter_504_s.COEFF[21] = 125.0 ; 
	FirFilter_504_s.COEFF[22] = 130.0 ; 
	FirFilter_504_s.COEFF[23] = 135.0 ; 
	FirFilter_504_s.COEFF[24] = 140.0 ; 
	FirFilter_504_s.COEFF[25] = 145.0 ; 
	FirFilter_504_s.COEFF[26] = 150.0 ; 
	FirFilter_504_s.COEFF[27] = 155.0 ; 
	FirFilter_504_s.COEFF[28] = 160.0 ; 
	FirFilter_504_s.COEFF[29] = 165.0 ; 
	FirFilter_504_s.COEFF[30] = 170.0 ; 
	FirFilter_504_s.COEFF[31] = 175.0 ; 
}
//--------------------------------
// --- init: FirFilter_505
	 {
	FirFilter_505_s.COEFF[0] = 20.0 ; 
	FirFilter_505_s.COEFF[1] = 25.0 ; 
	FirFilter_505_s.COEFF[2] = 30.0 ; 
	FirFilter_505_s.COEFF[3] = 35.0 ; 
	FirFilter_505_s.COEFF[4] = 40.0 ; 
	FirFilter_505_s.COEFF[5] = 45.0 ; 
	FirFilter_505_s.COEFF[6] = 50.0 ; 
	FirFilter_505_s.COEFF[7] = 55.0 ; 
	FirFilter_505_s.COEFF[8] = 60.0 ; 
	FirFilter_505_s.COEFF[9] = 65.0 ; 
	FirFilter_505_s.COEFF[10] = 70.0 ; 
	FirFilter_505_s.COEFF[11] = 75.0 ; 
	FirFilter_505_s.COEFF[12] = 80.0 ; 
	FirFilter_505_s.COEFF[13] = 85.0 ; 
	FirFilter_505_s.COEFF[14] = 90.0 ; 
	FirFilter_505_s.COEFF[15] = 95.0 ; 
	FirFilter_505_s.COEFF[16] = 100.0 ; 
	FirFilter_505_s.COEFF[17] = 105.0 ; 
	FirFilter_505_s.COEFF[18] = 110.0 ; 
	FirFilter_505_s.COEFF[19] = 115.0 ; 
	FirFilter_505_s.COEFF[20] = 120.0 ; 
	FirFilter_505_s.COEFF[21] = 125.0 ; 
	FirFilter_505_s.COEFF[22] = 130.0 ; 
	FirFilter_505_s.COEFF[23] = 135.0 ; 
	FirFilter_505_s.COEFF[24] = 140.0 ; 
	FirFilter_505_s.COEFF[25] = 145.0 ; 
	FirFilter_505_s.COEFF[26] = 150.0 ; 
	FirFilter_505_s.COEFF[27] = 155.0 ; 
	FirFilter_505_s.COEFF[28] = 160.0 ; 
	FirFilter_505_s.COEFF[29] = 165.0 ; 
	FirFilter_505_s.COEFF[30] = 170.0 ; 
	FirFilter_505_s.COEFF[31] = 175.0 ; 
}
//--------------------------------
// --- init: FirFilter_506
	 {
	FirFilter_506_s.COEFF[0] = 20.0 ; 
	FirFilter_506_s.COEFF[1] = 25.0 ; 
	FirFilter_506_s.COEFF[2] = 30.0 ; 
	FirFilter_506_s.COEFF[3] = 35.0 ; 
	FirFilter_506_s.COEFF[4] = 40.0 ; 
	FirFilter_506_s.COEFF[5] = 45.0 ; 
	FirFilter_506_s.COEFF[6] = 50.0 ; 
	FirFilter_506_s.COEFF[7] = 55.0 ; 
	FirFilter_506_s.COEFF[8] = 60.0 ; 
	FirFilter_506_s.COEFF[9] = 65.0 ; 
	FirFilter_506_s.COEFF[10] = 70.0 ; 
	FirFilter_506_s.COEFF[11] = 75.0 ; 
	FirFilter_506_s.COEFF[12] = 80.0 ; 
	FirFilter_506_s.COEFF[13] = 85.0 ; 
	FirFilter_506_s.COEFF[14] = 90.0 ; 
	FirFilter_506_s.COEFF[15] = 95.0 ; 
	FirFilter_506_s.COEFF[16] = 100.0 ; 
	FirFilter_506_s.COEFF[17] = 105.0 ; 
	FirFilter_506_s.COEFF[18] = 110.0 ; 
	FirFilter_506_s.COEFF[19] = 115.0 ; 
	FirFilter_506_s.COEFF[20] = 120.0 ; 
	FirFilter_506_s.COEFF[21] = 125.0 ; 
	FirFilter_506_s.COEFF[22] = 130.0 ; 
	FirFilter_506_s.COEFF[23] = 135.0 ; 
	FirFilter_506_s.COEFF[24] = 140.0 ; 
	FirFilter_506_s.COEFF[25] = 145.0 ; 
	FirFilter_506_s.COEFF[26] = 150.0 ; 
	FirFilter_506_s.COEFF[27] = 155.0 ; 
	FirFilter_506_s.COEFF[28] = 160.0 ; 
	FirFilter_506_s.COEFF[29] = 165.0 ; 
	FirFilter_506_s.COEFF[30] = 170.0 ; 
	FirFilter_506_s.COEFF[31] = 175.0 ; 
}
//--------------------------------
// --- init: FirFilter_507
	 {
	FirFilter_507_s.COEFF[0] = 20.0 ; 
	FirFilter_507_s.COEFF[1] = 25.0 ; 
	FirFilter_507_s.COEFF[2] = 30.0 ; 
	FirFilter_507_s.COEFF[3] = 35.0 ; 
	FirFilter_507_s.COEFF[4] = 40.0 ; 
	FirFilter_507_s.COEFF[5] = 45.0 ; 
	FirFilter_507_s.COEFF[6] = 50.0 ; 
	FirFilter_507_s.COEFF[7] = 55.0 ; 
	FirFilter_507_s.COEFF[8] = 60.0 ; 
	FirFilter_507_s.COEFF[9] = 65.0 ; 
	FirFilter_507_s.COEFF[10] = 70.0 ; 
	FirFilter_507_s.COEFF[11] = 75.0 ; 
	FirFilter_507_s.COEFF[12] = 80.0 ; 
	FirFilter_507_s.COEFF[13] = 85.0 ; 
	FirFilter_507_s.COEFF[14] = 90.0 ; 
	FirFilter_507_s.COEFF[15] = 95.0 ; 
	FirFilter_507_s.COEFF[16] = 100.0 ; 
	FirFilter_507_s.COEFF[17] = 105.0 ; 
	FirFilter_507_s.COEFF[18] = 110.0 ; 
	FirFilter_507_s.COEFF[19] = 115.0 ; 
	FirFilter_507_s.COEFF[20] = 120.0 ; 
	FirFilter_507_s.COEFF[21] = 125.0 ; 
	FirFilter_507_s.COEFF[22] = 130.0 ; 
	FirFilter_507_s.COEFF[23] = 135.0 ; 
	FirFilter_507_s.COEFF[24] = 140.0 ; 
	FirFilter_507_s.COEFF[25] = 145.0 ; 
	FirFilter_507_s.COEFF[26] = 150.0 ; 
	FirFilter_507_s.COEFF[27] = 155.0 ; 
	FirFilter_507_s.COEFF[28] = 160.0 ; 
	FirFilter_507_s.COEFF[29] = 165.0 ; 
	FirFilter_507_s.COEFF[30] = 170.0 ; 
	FirFilter_507_s.COEFF[31] = 175.0 ; 
}
//--------------------------------
// --- init: FirFilter_508
	 {
	FirFilter_508_s.COEFF[0] = 20.0 ; 
	FirFilter_508_s.COEFF[1] = 25.0 ; 
	FirFilter_508_s.COEFF[2] = 30.0 ; 
	FirFilter_508_s.COEFF[3] = 35.0 ; 
	FirFilter_508_s.COEFF[4] = 40.0 ; 
	FirFilter_508_s.COEFF[5] = 45.0 ; 
	FirFilter_508_s.COEFF[6] = 50.0 ; 
	FirFilter_508_s.COEFF[7] = 55.0 ; 
	FirFilter_508_s.COEFF[8] = 60.0 ; 
	FirFilter_508_s.COEFF[9] = 65.0 ; 
	FirFilter_508_s.COEFF[10] = 70.0 ; 
	FirFilter_508_s.COEFF[11] = 75.0 ; 
	FirFilter_508_s.COEFF[12] = 80.0 ; 
	FirFilter_508_s.COEFF[13] = 85.0 ; 
	FirFilter_508_s.COEFF[14] = 90.0 ; 
	FirFilter_508_s.COEFF[15] = 95.0 ; 
	FirFilter_508_s.COEFF[16] = 100.0 ; 
	FirFilter_508_s.COEFF[17] = 105.0 ; 
	FirFilter_508_s.COEFF[18] = 110.0 ; 
	FirFilter_508_s.COEFF[19] = 115.0 ; 
	FirFilter_508_s.COEFF[20] = 120.0 ; 
	FirFilter_508_s.COEFF[21] = 125.0 ; 
	FirFilter_508_s.COEFF[22] = 130.0 ; 
	FirFilter_508_s.COEFF[23] = 135.0 ; 
	FirFilter_508_s.COEFF[24] = 140.0 ; 
	FirFilter_508_s.COEFF[25] = 145.0 ; 
	FirFilter_508_s.COEFF[26] = 150.0 ; 
	FirFilter_508_s.COEFF[27] = 155.0 ; 
	FirFilter_508_s.COEFF[28] = 160.0 ; 
	FirFilter_508_s.COEFF[29] = 165.0 ; 
	FirFilter_508_s.COEFF[30] = 170.0 ; 
	FirFilter_508_s.COEFF[31] = 175.0 ; 
}
//--------------------------------
// --- init: FirFilter_509
	 {
	FirFilter_509_s.COEFF[0] = 20.0 ; 
	FirFilter_509_s.COEFF[1] = 25.0 ; 
	FirFilter_509_s.COEFF[2] = 30.0 ; 
	FirFilter_509_s.COEFF[3] = 35.0 ; 
	FirFilter_509_s.COEFF[4] = 40.0 ; 
	FirFilter_509_s.COEFF[5] = 45.0 ; 
	FirFilter_509_s.COEFF[6] = 50.0 ; 
	FirFilter_509_s.COEFF[7] = 55.0 ; 
	FirFilter_509_s.COEFF[8] = 60.0 ; 
	FirFilter_509_s.COEFF[9] = 65.0 ; 
	FirFilter_509_s.COEFF[10] = 70.0 ; 
	FirFilter_509_s.COEFF[11] = 75.0 ; 
	FirFilter_509_s.COEFF[12] = 80.0 ; 
	FirFilter_509_s.COEFF[13] = 85.0 ; 
	FirFilter_509_s.COEFF[14] = 90.0 ; 
	FirFilter_509_s.COEFF[15] = 95.0 ; 
	FirFilter_509_s.COEFF[16] = 100.0 ; 
	FirFilter_509_s.COEFF[17] = 105.0 ; 
	FirFilter_509_s.COEFF[18] = 110.0 ; 
	FirFilter_509_s.COEFF[19] = 115.0 ; 
	FirFilter_509_s.COEFF[20] = 120.0 ; 
	FirFilter_509_s.COEFF[21] = 125.0 ; 
	FirFilter_509_s.COEFF[22] = 130.0 ; 
	FirFilter_509_s.COEFF[23] = 135.0 ; 
	FirFilter_509_s.COEFF[24] = 140.0 ; 
	FirFilter_509_s.COEFF[25] = 145.0 ; 
	FirFilter_509_s.COEFF[26] = 150.0 ; 
	FirFilter_509_s.COEFF[27] = 155.0 ; 
	FirFilter_509_s.COEFF[28] = 160.0 ; 
	FirFilter_509_s.COEFF[29] = 165.0 ; 
	FirFilter_509_s.COEFF[30] = 170.0 ; 
	FirFilter_509_s.COEFF[31] = 175.0 ; 
}
//--------------------------------
// --- init: FirFilter_510
	 {
	FirFilter_510_s.COEFF[0] = 20.0 ; 
	FirFilter_510_s.COEFF[1] = 25.0 ; 
	FirFilter_510_s.COEFF[2] = 30.0 ; 
	FirFilter_510_s.COEFF[3] = 35.0 ; 
	FirFilter_510_s.COEFF[4] = 40.0 ; 
	FirFilter_510_s.COEFF[5] = 45.0 ; 
	FirFilter_510_s.COEFF[6] = 50.0 ; 
	FirFilter_510_s.COEFF[7] = 55.0 ; 
	FirFilter_510_s.COEFF[8] = 60.0 ; 
	FirFilter_510_s.COEFF[9] = 65.0 ; 
	FirFilter_510_s.COEFF[10] = 70.0 ; 
	FirFilter_510_s.COEFF[11] = 75.0 ; 
	FirFilter_510_s.COEFF[12] = 80.0 ; 
	FirFilter_510_s.COEFF[13] = 85.0 ; 
	FirFilter_510_s.COEFF[14] = 90.0 ; 
	FirFilter_510_s.COEFF[15] = 95.0 ; 
	FirFilter_510_s.COEFF[16] = 100.0 ; 
	FirFilter_510_s.COEFF[17] = 105.0 ; 
	FirFilter_510_s.COEFF[18] = 110.0 ; 
	FirFilter_510_s.COEFF[19] = 115.0 ; 
	FirFilter_510_s.COEFF[20] = 120.0 ; 
	FirFilter_510_s.COEFF[21] = 125.0 ; 
	FirFilter_510_s.COEFF[22] = 130.0 ; 
	FirFilter_510_s.COEFF[23] = 135.0 ; 
	FirFilter_510_s.COEFF[24] = 140.0 ; 
	FirFilter_510_s.COEFF[25] = 145.0 ; 
	FirFilter_510_s.COEFF[26] = 150.0 ; 
	FirFilter_510_s.COEFF[27] = 155.0 ; 
	FirFilter_510_s.COEFF[28] = 160.0 ; 
	FirFilter_510_s.COEFF[29] = 165.0 ; 
	FirFilter_510_s.COEFF[30] = 170.0 ; 
	FirFilter_510_s.COEFF[31] = 175.0 ; 
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_511
	FOR(uint32_t, __iter_init_, 0, <, 66, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&SplitJoin196_Delay_N_Fiss_663_697_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_229_293_641_676_split[5]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_513
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin196_Delay_N_Fiss_663_697_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_514
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin196_Delay_N_Fiss_663_697_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_515
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin196_Delay_N_Fiss_663_697_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_516
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin196_Delay_N_Fiss_663_697_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_517
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin196_Delay_N_Fiss_663_697_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_518
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin196_Delay_N_Fiss_663_697_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_519
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin196_Delay_N_Fiss_663_697_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_520
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin196_Delay_N_Fiss_663_697_join[7], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_512
	FOR(uint32_t, __iter_init_, 0, <, 37, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_512DUPLICATE_Splitter_521, pop_float(&SplitJoin196_Delay_N_Fiss_663_697_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_521
	FOR(uint32_t, __iter_init_, 0, <, 295, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_512DUPLICATE_Splitter_521);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin198_FirFilter_Fiss_664_698_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_523
	 {
	FirFilter_523_s.COEFF[0] = 51.0 ; 
	FirFilter_523_s.COEFF[1] = 84.0 ; 
	FirFilter_523_s.COEFF[2] = 117.0 ; 
	FirFilter_523_s.COEFF[3] = 150.0 ; 
	FirFilter_523_s.COEFF[4] = 183.0 ; 
	FirFilter_523_s.COEFF[5] = 216.0 ; 
	FirFilter_523_s.COEFF[6] = 249.0 ; 
	FirFilter_523_s.COEFF[7] = 282.0 ; 
	FirFilter_523_s.COEFF[8] = 315.0 ; 
	FirFilter_523_s.COEFF[9] = 348.0 ; 
	FirFilter_523_s.COEFF[10] = 381.0 ; 
	FirFilter_523_s.COEFF[11] = 414.0 ; 
	FirFilter_523_s.COEFF[12] = 447.0 ; 
	FirFilter_523_s.COEFF[13] = 480.0 ; 
	FirFilter_523_s.COEFF[14] = 513.0 ; 
	FirFilter_523_s.COEFF[15] = 546.0 ; 
	FirFilter_523_s.COEFF[16] = 579.0 ; 
	FirFilter_523_s.COEFF[17] = 612.0 ; 
	FirFilter_523_s.COEFF[18] = 645.0 ; 
	FirFilter_523_s.COEFF[19] = 678.0 ; 
	FirFilter_523_s.COEFF[20] = 711.0 ; 
	FirFilter_523_s.COEFF[21] = 744.0 ; 
	FirFilter_523_s.COEFF[22] = 777.0 ; 
	FirFilter_523_s.COEFF[23] = 810.0 ; 
	FirFilter_523_s.COEFF[24] = 843.0 ; 
	FirFilter_523_s.COEFF[25] = 876.0 ; 
	FirFilter_523_s.COEFF[26] = 909.0 ; 
	FirFilter_523_s.COEFF[27] = 942.0 ; 
	FirFilter_523_s.COEFF[28] = 975.0 ; 
	FirFilter_523_s.COEFF[29] = 1008.0 ; 
	FirFilter_523_s.COEFF[30] = 1041.0 ; 
	FirFilter_523_s.COEFF[31] = 1074.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin198_FirFilter_Fiss_664_698_split[0], i) * FirFilter_523_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin198_FirFilter_Fiss_664_698_split[0]) ; 
		push_float(&SplitJoin198_FirFilter_Fiss_664_698_join[0], sum) ; 
 {
		FOR(int, streamItVar133, 0,  < , 7, streamItVar133++) {
			pop_void(&SplitJoin198_FirFilter_Fiss_664_698_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_524
	 {
	FirFilter_524_s.COEFF[0] = 51.0 ; 
	FirFilter_524_s.COEFF[1] = 84.0 ; 
	FirFilter_524_s.COEFF[2] = 117.0 ; 
	FirFilter_524_s.COEFF[3] = 150.0 ; 
	FirFilter_524_s.COEFF[4] = 183.0 ; 
	FirFilter_524_s.COEFF[5] = 216.0 ; 
	FirFilter_524_s.COEFF[6] = 249.0 ; 
	FirFilter_524_s.COEFF[7] = 282.0 ; 
	FirFilter_524_s.COEFF[8] = 315.0 ; 
	FirFilter_524_s.COEFF[9] = 348.0 ; 
	FirFilter_524_s.COEFF[10] = 381.0 ; 
	FirFilter_524_s.COEFF[11] = 414.0 ; 
	FirFilter_524_s.COEFF[12] = 447.0 ; 
	FirFilter_524_s.COEFF[13] = 480.0 ; 
	FirFilter_524_s.COEFF[14] = 513.0 ; 
	FirFilter_524_s.COEFF[15] = 546.0 ; 
	FirFilter_524_s.COEFF[16] = 579.0 ; 
	FirFilter_524_s.COEFF[17] = 612.0 ; 
	FirFilter_524_s.COEFF[18] = 645.0 ; 
	FirFilter_524_s.COEFF[19] = 678.0 ; 
	FirFilter_524_s.COEFF[20] = 711.0 ; 
	FirFilter_524_s.COEFF[21] = 744.0 ; 
	FirFilter_524_s.COEFF[22] = 777.0 ; 
	FirFilter_524_s.COEFF[23] = 810.0 ; 
	FirFilter_524_s.COEFF[24] = 843.0 ; 
	FirFilter_524_s.COEFF[25] = 876.0 ; 
	FirFilter_524_s.COEFF[26] = 909.0 ; 
	FirFilter_524_s.COEFF[27] = 942.0 ; 
	FirFilter_524_s.COEFF[28] = 975.0 ; 
	FirFilter_524_s.COEFF[29] = 1008.0 ; 
	FirFilter_524_s.COEFF[30] = 1041.0 ; 
	FirFilter_524_s.COEFF[31] = 1074.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin198_FirFilter_Fiss_664_698_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin198_FirFilter_Fiss_664_698_split[1], i) * FirFilter_524_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin198_FirFilter_Fiss_664_698_split[1]) ; 
		push_float(&SplitJoin198_FirFilter_Fiss_664_698_join[1], sum) ; 
 {
		FOR(int, streamItVar134, 0,  < , 6, streamItVar134++) {
			pop_void(&SplitJoin198_FirFilter_Fiss_664_698_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_525
	 {
	FirFilter_525_s.COEFF[0] = 51.0 ; 
	FirFilter_525_s.COEFF[1] = 84.0 ; 
	FirFilter_525_s.COEFF[2] = 117.0 ; 
	FirFilter_525_s.COEFF[3] = 150.0 ; 
	FirFilter_525_s.COEFF[4] = 183.0 ; 
	FirFilter_525_s.COEFF[5] = 216.0 ; 
	FirFilter_525_s.COEFF[6] = 249.0 ; 
	FirFilter_525_s.COEFF[7] = 282.0 ; 
	FirFilter_525_s.COEFF[8] = 315.0 ; 
	FirFilter_525_s.COEFF[9] = 348.0 ; 
	FirFilter_525_s.COEFF[10] = 381.0 ; 
	FirFilter_525_s.COEFF[11] = 414.0 ; 
	FirFilter_525_s.COEFF[12] = 447.0 ; 
	FirFilter_525_s.COEFF[13] = 480.0 ; 
	FirFilter_525_s.COEFF[14] = 513.0 ; 
	FirFilter_525_s.COEFF[15] = 546.0 ; 
	FirFilter_525_s.COEFF[16] = 579.0 ; 
	FirFilter_525_s.COEFF[17] = 612.0 ; 
	FirFilter_525_s.COEFF[18] = 645.0 ; 
	FirFilter_525_s.COEFF[19] = 678.0 ; 
	FirFilter_525_s.COEFF[20] = 711.0 ; 
	FirFilter_525_s.COEFF[21] = 744.0 ; 
	FirFilter_525_s.COEFF[22] = 777.0 ; 
	FirFilter_525_s.COEFF[23] = 810.0 ; 
	FirFilter_525_s.COEFF[24] = 843.0 ; 
	FirFilter_525_s.COEFF[25] = 876.0 ; 
	FirFilter_525_s.COEFF[26] = 909.0 ; 
	FirFilter_525_s.COEFF[27] = 942.0 ; 
	FirFilter_525_s.COEFF[28] = 975.0 ; 
	FirFilter_525_s.COEFF[29] = 1008.0 ; 
	FirFilter_525_s.COEFF[30] = 1041.0 ; 
	FirFilter_525_s.COEFF[31] = 1074.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar135, 0,  < , 2, streamItVar135++) {
			pop_void(&SplitJoin198_FirFilter_Fiss_664_698_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin198_FirFilter_Fiss_664_698_split[2], i) * FirFilter_525_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin198_FirFilter_Fiss_664_698_split[2]) ; 
		push_float(&SplitJoin198_FirFilter_Fiss_664_698_join[2], sum) ; 
 {
		FOR(int, streamItVar136, 0,  < , 5, streamItVar136++) {
			pop_void(&SplitJoin198_FirFilter_Fiss_664_698_split[2]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_526
	 {
	FirFilter_526_s.COEFF[0] = 51.0 ; 
	FirFilter_526_s.COEFF[1] = 84.0 ; 
	FirFilter_526_s.COEFF[2] = 117.0 ; 
	FirFilter_526_s.COEFF[3] = 150.0 ; 
	FirFilter_526_s.COEFF[4] = 183.0 ; 
	FirFilter_526_s.COEFF[5] = 216.0 ; 
	FirFilter_526_s.COEFF[6] = 249.0 ; 
	FirFilter_526_s.COEFF[7] = 282.0 ; 
	FirFilter_526_s.COEFF[8] = 315.0 ; 
	FirFilter_526_s.COEFF[9] = 348.0 ; 
	FirFilter_526_s.COEFF[10] = 381.0 ; 
	FirFilter_526_s.COEFF[11] = 414.0 ; 
	FirFilter_526_s.COEFF[12] = 447.0 ; 
	FirFilter_526_s.COEFF[13] = 480.0 ; 
	FirFilter_526_s.COEFF[14] = 513.0 ; 
	FirFilter_526_s.COEFF[15] = 546.0 ; 
	FirFilter_526_s.COEFF[16] = 579.0 ; 
	FirFilter_526_s.COEFF[17] = 612.0 ; 
	FirFilter_526_s.COEFF[18] = 645.0 ; 
	FirFilter_526_s.COEFF[19] = 678.0 ; 
	FirFilter_526_s.COEFF[20] = 711.0 ; 
	FirFilter_526_s.COEFF[21] = 744.0 ; 
	FirFilter_526_s.COEFF[22] = 777.0 ; 
	FirFilter_526_s.COEFF[23] = 810.0 ; 
	FirFilter_526_s.COEFF[24] = 843.0 ; 
	FirFilter_526_s.COEFF[25] = 876.0 ; 
	FirFilter_526_s.COEFF[26] = 909.0 ; 
	FirFilter_526_s.COEFF[27] = 942.0 ; 
	FirFilter_526_s.COEFF[28] = 975.0 ; 
	FirFilter_526_s.COEFF[29] = 1008.0 ; 
	FirFilter_526_s.COEFF[30] = 1041.0 ; 
	FirFilter_526_s.COEFF[31] = 1074.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar137, 0,  < , 3, streamItVar137++) {
			pop_void(&SplitJoin198_FirFilter_Fiss_664_698_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin198_FirFilter_Fiss_664_698_split[3], i) * FirFilter_526_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin198_FirFilter_Fiss_664_698_split[3]) ; 
		push_float(&SplitJoin198_FirFilter_Fiss_664_698_join[3], sum) ; 
 {
		FOR(int, streamItVar138, 0,  < , 4, streamItVar138++) {
			pop_void(&SplitJoin198_FirFilter_Fiss_664_698_split[3]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_527
	 {
	FirFilter_527_s.COEFF[0] = 51.0 ; 
	FirFilter_527_s.COEFF[1] = 84.0 ; 
	FirFilter_527_s.COEFF[2] = 117.0 ; 
	FirFilter_527_s.COEFF[3] = 150.0 ; 
	FirFilter_527_s.COEFF[4] = 183.0 ; 
	FirFilter_527_s.COEFF[5] = 216.0 ; 
	FirFilter_527_s.COEFF[6] = 249.0 ; 
	FirFilter_527_s.COEFF[7] = 282.0 ; 
	FirFilter_527_s.COEFF[8] = 315.0 ; 
	FirFilter_527_s.COEFF[9] = 348.0 ; 
	FirFilter_527_s.COEFF[10] = 381.0 ; 
	FirFilter_527_s.COEFF[11] = 414.0 ; 
	FirFilter_527_s.COEFF[12] = 447.0 ; 
	FirFilter_527_s.COEFF[13] = 480.0 ; 
	FirFilter_527_s.COEFF[14] = 513.0 ; 
	FirFilter_527_s.COEFF[15] = 546.0 ; 
	FirFilter_527_s.COEFF[16] = 579.0 ; 
	FirFilter_527_s.COEFF[17] = 612.0 ; 
	FirFilter_527_s.COEFF[18] = 645.0 ; 
	FirFilter_527_s.COEFF[19] = 678.0 ; 
	FirFilter_527_s.COEFF[20] = 711.0 ; 
	FirFilter_527_s.COEFF[21] = 744.0 ; 
	FirFilter_527_s.COEFF[22] = 777.0 ; 
	FirFilter_527_s.COEFF[23] = 810.0 ; 
	FirFilter_527_s.COEFF[24] = 843.0 ; 
	FirFilter_527_s.COEFF[25] = 876.0 ; 
	FirFilter_527_s.COEFF[26] = 909.0 ; 
	FirFilter_527_s.COEFF[27] = 942.0 ; 
	FirFilter_527_s.COEFF[28] = 975.0 ; 
	FirFilter_527_s.COEFF[29] = 1008.0 ; 
	FirFilter_527_s.COEFF[30] = 1041.0 ; 
	FirFilter_527_s.COEFF[31] = 1074.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar139, 0,  < , 4, streamItVar139++) {
			pop_void(&SplitJoin198_FirFilter_Fiss_664_698_split[4]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin198_FirFilter_Fiss_664_698_split[4], i) * FirFilter_527_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin198_FirFilter_Fiss_664_698_split[4]) ; 
		push_float(&SplitJoin198_FirFilter_Fiss_664_698_join[4], sum) ; 
 {
		FOR(int, streamItVar140, 0,  < , 3, streamItVar140++) {
			pop_void(&SplitJoin198_FirFilter_Fiss_664_698_split[4]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_528
	 {
	FirFilter_528_s.COEFF[0] = 51.0 ; 
	FirFilter_528_s.COEFF[1] = 84.0 ; 
	FirFilter_528_s.COEFF[2] = 117.0 ; 
	FirFilter_528_s.COEFF[3] = 150.0 ; 
	FirFilter_528_s.COEFF[4] = 183.0 ; 
	FirFilter_528_s.COEFF[5] = 216.0 ; 
	FirFilter_528_s.COEFF[6] = 249.0 ; 
	FirFilter_528_s.COEFF[7] = 282.0 ; 
	FirFilter_528_s.COEFF[8] = 315.0 ; 
	FirFilter_528_s.COEFF[9] = 348.0 ; 
	FirFilter_528_s.COEFF[10] = 381.0 ; 
	FirFilter_528_s.COEFF[11] = 414.0 ; 
	FirFilter_528_s.COEFF[12] = 447.0 ; 
	FirFilter_528_s.COEFF[13] = 480.0 ; 
	FirFilter_528_s.COEFF[14] = 513.0 ; 
	FirFilter_528_s.COEFF[15] = 546.0 ; 
	FirFilter_528_s.COEFF[16] = 579.0 ; 
	FirFilter_528_s.COEFF[17] = 612.0 ; 
	FirFilter_528_s.COEFF[18] = 645.0 ; 
	FirFilter_528_s.COEFF[19] = 678.0 ; 
	FirFilter_528_s.COEFF[20] = 711.0 ; 
	FirFilter_528_s.COEFF[21] = 744.0 ; 
	FirFilter_528_s.COEFF[22] = 777.0 ; 
	FirFilter_528_s.COEFF[23] = 810.0 ; 
	FirFilter_528_s.COEFF[24] = 843.0 ; 
	FirFilter_528_s.COEFF[25] = 876.0 ; 
	FirFilter_528_s.COEFF[26] = 909.0 ; 
	FirFilter_528_s.COEFF[27] = 942.0 ; 
	FirFilter_528_s.COEFF[28] = 975.0 ; 
	FirFilter_528_s.COEFF[29] = 1008.0 ; 
	FirFilter_528_s.COEFF[30] = 1041.0 ; 
	FirFilter_528_s.COEFF[31] = 1074.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar141, 0,  < , 5, streamItVar141++) {
			pop_void(&SplitJoin198_FirFilter_Fiss_664_698_split[5]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin198_FirFilter_Fiss_664_698_split[5], i) * FirFilter_528_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin198_FirFilter_Fiss_664_698_split[5]) ; 
		push_float(&SplitJoin198_FirFilter_Fiss_664_698_join[5], sum) ; 
 {
		FOR(int, streamItVar142, 0,  < , 2, streamItVar142++) {
			pop_void(&SplitJoin198_FirFilter_Fiss_664_698_split[5]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_529
	 {
	FirFilter_529_s.COEFF[0] = 51.0 ; 
	FirFilter_529_s.COEFF[1] = 84.0 ; 
	FirFilter_529_s.COEFF[2] = 117.0 ; 
	FirFilter_529_s.COEFF[3] = 150.0 ; 
	FirFilter_529_s.COEFF[4] = 183.0 ; 
	FirFilter_529_s.COEFF[5] = 216.0 ; 
	FirFilter_529_s.COEFF[6] = 249.0 ; 
	FirFilter_529_s.COEFF[7] = 282.0 ; 
	FirFilter_529_s.COEFF[8] = 315.0 ; 
	FirFilter_529_s.COEFF[9] = 348.0 ; 
	FirFilter_529_s.COEFF[10] = 381.0 ; 
	FirFilter_529_s.COEFF[11] = 414.0 ; 
	FirFilter_529_s.COEFF[12] = 447.0 ; 
	FirFilter_529_s.COEFF[13] = 480.0 ; 
	FirFilter_529_s.COEFF[14] = 513.0 ; 
	FirFilter_529_s.COEFF[15] = 546.0 ; 
	FirFilter_529_s.COEFF[16] = 579.0 ; 
	FirFilter_529_s.COEFF[17] = 612.0 ; 
	FirFilter_529_s.COEFF[18] = 645.0 ; 
	FirFilter_529_s.COEFF[19] = 678.0 ; 
	FirFilter_529_s.COEFF[20] = 711.0 ; 
	FirFilter_529_s.COEFF[21] = 744.0 ; 
	FirFilter_529_s.COEFF[22] = 777.0 ; 
	FirFilter_529_s.COEFF[23] = 810.0 ; 
	FirFilter_529_s.COEFF[24] = 843.0 ; 
	FirFilter_529_s.COEFF[25] = 876.0 ; 
	FirFilter_529_s.COEFF[26] = 909.0 ; 
	FirFilter_529_s.COEFF[27] = 942.0 ; 
	FirFilter_529_s.COEFF[28] = 975.0 ; 
	FirFilter_529_s.COEFF[29] = 1008.0 ; 
	FirFilter_529_s.COEFF[30] = 1041.0 ; 
	FirFilter_529_s.COEFF[31] = 1074.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar143, 0,  < , 6, streamItVar143++) {
			pop_void(&SplitJoin198_FirFilter_Fiss_664_698_split[6]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin198_FirFilter_Fiss_664_698_split[6], i) * FirFilter_529_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin198_FirFilter_Fiss_664_698_split[6]) ; 
		push_float(&SplitJoin198_FirFilter_Fiss_664_698_join[6], sum) ; 
 {
		pop_void(&SplitJoin198_FirFilter_Fiss_664_698_split[6]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_530
	 {
	FirFilter_530_s.COEFF[0] = 51.0 ; 
	FirFilter_530_s.COEFF[1] = 84.0 ; 
	FirFilter_530_s.COEFF[2] = 117.0 ; 
	FirFilter_530_s.COEFF[3] = 150.0 ; 
	FirFilter_530_s.COEFF[4] = 183.0 ; 
	FirFilter_530_s.COEFF[5] = 216.0 ; 
	FirFilter_530_s.COEFF[6] = 249.0 ; 
	FirFilter_530_s.COEFF[7] = 282.0 ; 
	FirFilter_530_s.COEFF[8] = 315.0 ; 
	FirFilter_530_s.COEFF[9] = 348.0 ; 
	FirFilter_530_s.COEFF[10] = 381.0 ; 
	FirFilter_530_s.COEFF[11] = 414.0 ; 
	FirFilter_530_s.COEFF[12] = 447.0 ; 
	FirFilter_530_s.COEFF[13] = 480.0 ; 
	FirFilter_530_s.COEFF[14] = 513.0 ; 
	FirFilter_530_s.COEFF[15] = 546.0 ; 
	FirFilter_530_s.COEFF[16] = 579.0 ; 
	FirFilter_530_s.COEFF[17] = 612.0 ; 
	FirFilter_530_s.COEFF[18] = 645.0 ; 
	FirFilter_530_s.COEFF[19] = 678.0 ; 
	FirFilter_530_s.COEFF[20] = 711.0 ; 
	FirFilter_530_s.COEFF[21] = 744.0 ; 
	FirFilter_530_s.COEFF[22] = 777.0 ; 
	FirFilter_530_s.COEFF[23] = 810.0 ; 
	FirFilter_530_s.COEFF[24] = 843.0 ; 
	FirFilter_530_s.COEFF[25] = 876.0 ; 
	FirFilter_530_s.COEFF[26] = 909.0 ; 
	FirFilter_530_s.COEFF[27] = 942.0 ; 
	FirFilter_530_s.COEFF[28] = 975.0 ; 
	FirFilter_530_s.COEFF[29] = 1008.0 ; 
	FirFilter_530_s.COEFF[30] = 1041.0 ; 
	FirFilter_530_s.COEFF[31] = 1074.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar144, 0,  < , 7, streamItVar144++) {
			pop_void(&SplitJoin198_FirFilter_Fiss_664_698_split[7]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin198_FirFilter_Fiss_664_698_split[7], i) * FirFilter_530_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin198_FirFilter_Fiss_664_698_split[7]) ; 
		push_float(&SplitJoin198_FirFilter_Fiss_664_698_join[7], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_522
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_522DownSamp_271, pop_float(&SplitJoin198_FirFilter_Fiss_664_698_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DownSamp_271
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		push_float(&DownSamp_271UpSamp_272, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_522DownSamp_271)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_522DownSamp_271) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: UpSamp_272
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		push_float(&UpSamp_272WEIGHTED_ROUND_ROBIN_Splitter_531, pop_float(&DownSamp_271UpSamp_272)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			push_float(&UpSamp_272WEIGHTED_ROUND_ROBIN_Splitter_531, 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_531
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&SplitJoin200_Delay_N_Fiss_665_699_split[__iter_], pop_float(&UpSamp_272WEIGHTED_ROUND_ROBIN_Splitter_531));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_533
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin200_Delay_N_Fiss_665_699_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_534
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin200_Delay_N_Fiss_665_699_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_535
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin200_Delay_N_Fiss_665_699_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_536
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin200_Delay_N_Fiss_665_699_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_537
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin200_Delay_N_Fiss_665_699_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_538
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin200_Delay_N_Fiss_665_699_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_539
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin200_Delay_N_Fiss_665_699_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_540
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin200_Delay_N_Fiss_665_699_join[7], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_532
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_532DUPLICATE_Splitter_541, pop_float(&SplitJoin200_Delay_N_Fiss_665_699_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_541
	FOR(uint32_t, __iter_init_, 0, <, 31, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_532DUPLICATE_Splitter_541);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin202_FirFilter_Fiss_666_700_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_543
	 {
	FirFilter_543_s.COEFF[0] = 30.0 ; 
	FirFilter_543_s.COEFF[1] = 36.0 ; 
	FirFilter_543_s.COEFF[2] = 42.0 ; 
	FirFilter_543_s.COEFF[3] = 48.0 ; 
	FirFilter_543_s.COEFF[4] = 54.0 ; 
	FirFilter_543_s.COEFF[5] = 60.0 ; 
	FirFilter_543_s.COEFF[6] = 66.0 ; 
	FirFilter_543_s.COEFF[7] = 72.0 ; 
	FirFilter_543_s.COEFF[8] = 78.0 ; 
	FirFilter_543_s.COEFF[9] = 84.0 ; 
	FirFilter_543_s.COEFF[10] = 90.0 ; 
	FirFilter_543_s.COEFF[11] = 96.0 ; 
	FirFilter_543_s.COEFF[12] = 102.0 ; 
	FirFilter_543_s.COEFF[13] = 108.0 ; 
	FirFilter_543_s.COEFF[14] = 114.0 ; 
	FirFilter_543_s.COEFF[15] = 120.0 ; 
	FirFilter_543_s.COEFF[16] = 126.0 ; 
	FirFilter_543_s.COEFF[17] = 132.0 ; 
	FirFilter_543_s.COEFF[18] = 138.0 ; 
	FirFilter_543_s.COEFF[19] = 144.0 ; 
	FirFilter_543_s.COEFF[20] = 150.0 ; 
	FirFilter_543_s.COEFF[21] = 156.0 ; 
	FirFilter_543_s.COEFF[22] = 162.0 ; 
	FirFilter_543_s.COEFF[23] = 168.0 ; 
	FirFilter_543_s.COEFF[24] = 174.0 ; 
	FirFilter_543_s.COEFF[25] = 180.0 ; 
	FirFilter_543_s.COEFF[26] = 186.0 ; 
	FirFilter_543_s.COEFF[27] = 192.0 ; 
	FirFilter_543_s.COEFF[28] = 198.0 ; 
	FirFilter_543_s.COEFF[29] = 204.0 ; 
	FirFilter_543_s.COEFF[30] = 210.0 ; 
	FirFilter_543_s.COEFF[31] = 216.0 ; 
}
//--------------------------------
// --- init: FirFilter_544
	 {
	FirFilter_544_s.COEFF[0] = 30.0 ; 
	FirFilter_544_s.COEFF[1] = 36.0 ; 
	FirFilter_544_s.COEFF[2] = 42.0 ; 
	FirFilter_544_s.COEFF[3] = 48.0 ; 
	FirFilter_544_s.COEFF[4] = 54.0 ; 
	FirFilter_544_s.COEFF[5] = 60.0 ; 
	FirFilter_544_s.COEFF[6] = 66.0 ; 
	FirFilter_544_s.COEFF[7] = 72.0 ; 
	FirFilter_544_s.COEFF[8] = 78.0 ; 
	FirFilter_544_s.COEFF[9] = 84.0 ; 
	FirFilter_544_s.COEFF[10] = 90.0 ; 
	FirFilter_544_s.COEFF[11] = 96.0 ; 
	FirFilter_544_s.COEFF[12] = 102.0 ; 
	FirFilter_544_s.COEFF[13] = 108.0 ; 
	FirFilter_544_s.COEFF[14] = 114.0 ; 
	FirFilter_544_s.COEFF[15] = 120.0 ; 
	FirFilter_544_s.COEFF[16] = 126.0 ; 
	FirFilter_544_s.COEFF[17] = 132.0 ; 
	FirFilter_544_s.COEFF[18] = 138.0 ; 
	FirFilter_544_s.COEFF[19] = 144.0 ; 
	FirFilter_544_s.COEFF[20] = 150.0 ; 
	FirFilter_544_s.COEFF[21] = 156.0 ; 
	FirFilter_544_s.COEFF[22] = 162.0 ; 
	FirFilter_544_s.COEFF[23] = 168.0 ; 
	FirFilter_544_s.COEFF[24] = 174.0 ; 
	FirFilter_544_s.COEFF[25] = 180.0 ; 
	FirFilter_544_s.COEFF[26] = 186.0 ; 
	FirFilter_544_s.COEFF[27] = 192.0 ; 
	FirFilter_544_s.COEFF[28] = 198.0 ; 
	FirFilter_544_s.COEFF[29] = 204.0 ; 
	FirFilter_544_s.COEFF[30] = 210.0 ; 
	FirFilter_544_s.COEFF[31] = 216.0 ; 
}
//--------------------------------
// --- init: FirFilter_545
	 {
	FirFilter_545_s.COEFF[0] = 30.0 ; 
	FirFilter_545_s.COEFF[1] = 36.0 ; 
	FirFilter_545_s.COEFF[2] = 42.0 ; 
	FirFilter_545_s.COEFF[3] = 48.0 ; 
	FirFilter_545_s.COEFF[4] = 54.0 ; 
	FirFilter_545_s.COEFF[5] = 60.0 ; 
	FirFilter_545_s.COEFF[6] = 66.0 ; 
	FirFilter_545_s.COEFF[7] = 72.0 ; 
	FirFilter_545_s.COEFF[8] = 78.0 ; 
	FirFilter_545_s.COEFF[9] = 84.0 ; 
	FirFilter_545_s.COEFF[10] = 90.0 ; 
	FirFilter_545_s.COEFF[11] = 96.0 ; 
	FirFilter_545_s.COEFF[12] = 102.0 ; 
	FirFilter_545_s.COEFF[13] = 108.0 ; 
	FirFilter_545_s.COEFF[14] = 114.0 ; 
	FirFilter_545_s.COEFF[15] = 120.0 ; 
	FirFilter_545_s.COEFF[16] = 126.0 ; 
	FirFilter_545_s.COEFF[17] = 132.0 ; 
	FirFilter_545_s.COEFF[18] = 138.0 ; 
	FirFilter_545_s.COEFF[19] = 144.0 ; 
	FirFilter_545_s.COEFF[20] = 150.0 ; 
	FirFilter_545_s.COEFF[21] = 156.0 ; 
	FirFilter_545_s.COEFF[22] = 162.0 ; 
	FirFilter_545_s.COEFF[23] = 168.0 ; 
	FirFilter_545_s.COEFF[24] = 174.0 ; 
	FirFilter_545_s.COEFF[25] = 180.0 ; 
	FirFilter_545_s.COEFF[26] = 186.0 ; 
	FirFilter_545_s.COEFF[27] = 192.0 ; 
	FirFilter_545_s.COEFF[28] = 198.0 ; 
	FirFilter_545_s.COEFF[29] = 204.0 ; 
	FirFilter_545_s.COEFF[30] = 210.0 ; 
	FirFilter_545_s.COEFF[31] = 216.0 ; 
}
//--------------------------------
// --- init: FirFilter_546
	 {
	FirFilter_546_s.COEFF[0] = 30.0 ; 
	FirFilter_546_s.COEFF[1] = 36.0 ; 
	FirFilter_546_s.COEFF[2] = 42.0 ; 
	FirFilter_546_s.COEFF[3] = 48.0 ; 
	FirFilter_546_s.COEFF[4] = 54.0 ; 
	FirFilter_546_s.COEFF[5] = 60.0 ; 
	FirFilter_546_s.COEFF[6] = 66.0 ; 
	FirFilter_546_s.COEFF[7] = 72.0 ; 
	FirFilter_546_s.COEFF[8] = 78.0 ; 
	FirFilter_546_s.COEFF[9] = 84.0 ; 
	FirFilter_546_s.COEFF[10] = 90.0 ; 
	FirFilter_546_s.COEFF[11] = 96.0 ; 
	FirFilter_546_s.COEFF[12] = 102.0 ; 
	FirFilter_546_s.COEFF[13] = 108.0 ; 
	FirFilter_546_s.COEFF[14] = 114.0 ; 
	FirFilter_546_s.COEFF[15] = 120.0 ; 
	FirFilter_546_s.COEFF[16] = 126.0 ; 
	FirFilter_546_s.COEFF[17] = 132.0 ; 
	FirFilter_546_s.COEFF[18] = 138.0 ; 
	FirFilter_546_s.COEFF[19] = 144.0 ; 
	FirFilter_546_s.COEFF[20] = 150.0 ; 
	FirFilter_546_s.COEFF[21] = 156.0 ; 
	FirFilter_546_s.COEFF[22] = 162.0 ; 
	FirFilter_546_s.COEFF[23] = 168.0 ; 
	FirFilter_546_s.COEFF[24] = 174.0 ; 
	FirFilter_546_s.COEFF[25] = 180.0 ; 
	FirFilter_546_s.COEFF[26] = 186.0 ; 
	FirFilter_546_s.COEFF[27] = 192.0 ; 
	FirFilter_546_s.COEFF[28] = 198.0 ; 
	FirFilter_546_s.COEFF[29] = 204.0 ; 
	FirFilter_546_s.COEFF[30] = 210.0 ; 
	FirFilter_546_s.COEFF[31] = 216.0 ; 
}
//--------------------------------
// --- init: FirFilter_547
	 {
	FirFilter_547_s.COEFF[0] = 30.0 ; 
	FirFilter_547_s.COEFF[1] = 36.0 ; 
	FirFilter_547_s.COEFF[2] = 42.0 ; 
	FirFilter_547_s.COEFF[3] = 48.0 ; 
	FirFilter_547_s.COEFF[4] = 54.0 ; 
	FirFilter_547_s.COEFF[5] = 60.0 ; 
	FirFilter_547_s.COEFF[6] = 66.0 ; 
	FirFilter_547_s.COEFF[7] = 72.0 ; 
	FirFilter_547_s.COEFF[8] = 78.0 ; 
	FirFilter_547_s.COEFF[9] = 84.0 ; 
	FirFilter_547_s.COEFF[10] = 90.0 ; 
	FirFilter_547_s.COEFF[11] = 96.0 ; 
	FirFilter_547_s.COEFF[12] = 102.0 ; 
	FirFilter_547_s.COEFF[13] = 108.0 ; 
	FirFilter_547_s.COEFF[14] = 114.0 ; 
	FirFilter_547_s.COEFF[15] = 120.0 ; 
	FirFilter_547_s.COEFF[16] = 126.0 ; 
	FirFilter_547_s.COEFF[17] = 132.0 ; 
	FirFilter_547_s.COEFF[18] = 138.0 ; 
	FirFilter_547_s.COEFF[19] = 144.0 ; 
	FirFilter_547_s.COEFF[20] = 150.0 ; 
	FirFilter_547_s.COEFF[21] = 156.0 ; 
	FirFilter_547_s.COEFF[22] = 162.0 ; 
	FirFilter_547_s.COEFF[23] = 168.0 ; 
	FirFilter_547_s.COEFF[24] = 174.0 ; 
	FirFilter_547_s.COEFF[25] = 180.0 ; 
	FirFilter_547_s.COEFF[26] = 186.0 ; 
	FirFilter_547_s.COEFF[27] = 192.0 ; 
	FirFilter_547_s.COEFF[28] = 198.0 ; 
	FirFilter_547_s.COEFF[29] = 204.0 ; 
	FirFilter_547_s.COEFF[30] = 210.0 ; 
	FirFilter_547_s.COEFF[31] = 216.0 ; 
}
//--------------------------------
// --- init: FirFilter_548
	 {
	FirFilter_548_s.COEFF[0] = 30.0 ; 
	FirFilter_548_s.COEFF[1] = 36.0 ; 
	FirFilter_548_s.COEFF[2] = 42.0 ; 
	FirFilter_548_s.COEFF[3] = 48.0 ; 
	FirFilter_548_s.COEFF[4] = 54.0 ; 
	FirFilter_548_s.COEFF[5] = 60.0 ; 
	FirFilter_548_s.COEFF[6] = 66.0 ; 
	FirFilter_548_s.COEFF[7] = 72.0 ; 
	FirFilter_548_s.COEFF[8] = 78.0 ; 
	FirFilter_548_s.COEFF[9] = 84.0 ; 
	FirFilter_548_s.COEFF[10] = 90.0 ; 
	FirFilter_548_s.COEFF[11] = 96.0 ; 
	FirFilter_548_s.COEFF[12] = 102.0 ; 
	FirFilter_548_s.COEFF[13] = 108.0 ; 
	FirFilter_548_s.COEFF[14] = 114.0 ; 
	FirFilter_548_s.COEFF[15] = 120.0 ; 
	FirFilter_548_s.COEFF[16] = 126.0 ; 
	FirFilter_548_s.COEFF[17] = 132.0 ; 
	FirFilter_548_s.COEFF[18] = 138.0 ; 
	FirFilter_548_s.COEFF[19] = 144.0 ; 
	FirFilter_548_s.COEFF[20] = 150.0 ; 
	FirFilter_548_s.COEFF[21] = 156.0 ; 
	FirFilter_548_s.COEFF[22] = 162.0 ; 
	FirFilter_548_s.COEFF[23] = 168.0 ; 
	FirFilter_548_s.COEFF[24] = 174.0 ; 
	FirFilter_548_s.COEFF[25] = 180.0 ; 
	FirFilter_548_s.COEFF[26] = 186.0 ; 
	FirFilter_548_s.COEFF[27] = 192.0 ; 
	FirFilter_548_s.COEFF[28] = 198.0 ; 
	FirFilter_548_s.COEFF[29] = 204.0 ; 
	FirFilter_548_s.COEFF[30] = 210.0 ; 
	FirFilter_548_s.COEFF[31] = 216.0 ; 
}
//--------------------------------
// --- init: FirFilter_549
	 {
	FirFilter_549_s.COEFF[0] = 30.0 ; 
	FirFilter_549_s.COEFF[1] = 36.0 ; 
	FirFilter_549_s.COEFF[2] = 42.0 ; 
	FirFilter_549_s.COEFF[3] = 48.0 ; 
	FirFilter_549_s.COEFF[4] = 54.0 ; 
	FirFilter_549_s.COEFF[5] = 60.0 ; 
	FirFilter_549_s.COEFF[6] = 66.0 ; 
	FirFilter_549_s.COEFF[7] = 72.0 ; 
	FirFilter_549_s.COEFF[8] = 78.0 ; 
	FirFilter_549_s.COEFF[9] = 84.0 ; 
	FirFilter_549_s.COEFF[10] = 90.0 ; 
	FirFilter_549_s.COEFF[11] = 96.0 ; 
	FirFilter_549_s.COEFF[12] = 102.0 ; 
	FirFilter_549_s.COEFF[13] = 108.0 ; 
	FirFilter_549_s.COEFF[14] = 114.0 ; 
	FirFilter_549_s.COEFF[15] = 120.0 ; 
	FirFilter_549_s.COEFF[16] = 126.0 ; 
	FirFilter_549_s.COEFF[17] = 132.0 ; 
	FirFilter_549_s.COEFF[18] = 138.0 ; 
	FirFilter_549_s.COEFF[19] = 144.0 ; 
	FirFilter_549_s.COEFF[20] = 150.0 ; 
	FirFilter_549_s.COEFF[21] = 156.0 ; 
	FirFilter_549_s.COEFF[22] = 162.0 ; 
	FirFilter_549_s.COEFF[23] = 168.0 ; 
	FirFilter_549_s.COEFF[24] = 174.0 ; 
	FirFilter_549_s.COEFF[25] = 180.0 ; 
	FirFilter_549_s.COEFF[26] = 186.0 ; 
	FirFilter_549_s.COEFF[27] = 192.0 ; 
	FirFilter_549_s.COEFF[28] = 198.0 ; 
	FirFilter_549_s.COEFF[29] = 204.0 ; 
	FirFilter_549_s.COEFF[30] = 210.0 ; 
	FirFilter_549_s.COEFF[31] = 216.0 ; 
}
//--------------------------------
// --- init: FirFilter_550
	 {
	FirFilter_550_s.COEFF[0] = 30.0 ; 
	FirFilter_550_s.COEFF[1] = 36.0 ; 
	FirFilter_550_s.COEFF[2] = 42.0 ; 
	FirFilter_550_s.COEFF[3] = 48.0 ; 
	FirFilter_550_s.COEFF[4] = 54.0 ; 
	FirFilter_550_s.COEFF[5] = 60.0 ; 
	FirFilter_550_s.COEFF[6] = 66.0 ; 
	FirFilter_550_s.COEFF[7] = 72.0 ; 
	FirFilter_550_s.COEFF[8] = 78.0 ; 
	FirFilter_550_s.COEFF[9] = 84.0 ; 
	FirFilter_550_s.COEFF[10] = 90.0 ; 
	FirFilter_550_s.COEFF[11] = 96.0 ; 
	FirFilter_550_s.COEFF[12] = 102.0 ; 
	FirFilter_550_s.COEFF[13] = 108.0 ; 
	FirFilter_550_s.COEFF[14] = 114.0 ; 
	FirFilter_550_s.COEFF[15] = 120.0 ; 
	FirFilter_550_s.COEFF[16] = 126.0 ; 
	FirFilter_550_s.COEFF[17] = 132.0 ; 
	FirFilter_550_s.COEFF[18] = 138.0 ; 
	FirFilter_550_s.COEFF[19] = 144.0 ; 
	FirFilter_550_s.COEFF[20] = 150.0 ; 
	FirFilter_550_s.COEFF[21] = 156.0 ; 
	FirFilter_550_s.COEFF[22] = 162.0 ; 
	FirFilter_550_s.COEFF[23] = 168.0 ; 
	FirFilter_550_s.COEFF[24] = 174.0 ; 
	FirFilter_550_s.COEFF[25] = 180.0 ; 
	FirFilter_550_s.COEFF[26] = 186.0 ; 
	FirFilter_550_s.COEFF[27] = 192.0 ; 
	FirFilter_550_s.COEFF[28] = 198.0 ; 
	FirFilter_550_s.COEFF[29] = 204.0 ; 
	FirFilter_550_s.COEFF[30] = 210.0 ; 
	FirFilter_550_s.COEFF[31] = 216.0 ; 
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_551
	FOR(uint32_t, __iter_init_, 0, <, 66, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&SplitJoin233_Delay_N_Fiss_667_701_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_229_293_641_676_split[6]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_553
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin233_Delay_N_Fiss_667_701_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_554
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin233_Delay_N_Fiss_667_701_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_555
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin233_Delay_N_Fiss_667_701_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_556
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin233_Delay_N_Fiss_667_701_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_557
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin233_Delay_N_Fiss_667_701_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_558
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin233_Delay_N_Fiss_667_701_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_559
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin233_Delay_N_Fiss_667_701_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_560
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin233_Delay_N_Fiss_667_701_join[7], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_552
	FOR(uint32_t, __iter_init_, 0, <, 37, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_552DUPLICATE_Splitter_561, pop_float(&SplitJoin233_Delay_N_Fiss_667_701_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_561
	FOR(uint32_t, __iter_init_, 0, <, 295, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_552DUPLICATE_Splitter_561);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin235_FirFilter_Fiss_668_702_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_563
	 {
	FirFilter_563_s.COEFF[0] = 61.0 ; 
	FirFilter_563_s.COEFF[1] = 94.0 ; 
	FirFilter_563_s.COEFF[2] = 127.0 ; 
	FirFilter_563_s.COEFF[3] = 160.0 ; 
	FirFilter_563_s.COEFF[4] = 193.0 ; 
	FirFilter_563_s.COEFF[5] = 226.0 ; 
	FirFilter_563_s.COEFF[6] = 259.0 ; 
	FirFilter_563_s.COEFF[7] = 292.0 ; 
	FirFilter_563_s.COEFF[8] = 325.0 ; 
	FirFilter_563_s.COEFF[9] = 358.0 ; 
	FirFilter_563_s.COEFF[10] = 391.0 ; 
	FirFilter_563_s.COEFF[11] = 424.0 ; 
	FirFilter_563_s.COEFF[12] = 457.0 ; 
	FirFilter_563_s.COEFF[13] = 490.0 ; 
	FirFilter_563_s.COEFF[14] = 523.0 ; 
	FirFilter_563_s.COEFF[15] = 556.0 ; 
	FirFilter_563_s.COEFF[16] = 589.0 ; 
	FirFilter_563_s.COEFF[17] = 622.0 ; 
	FirFilter_563_s.COEFF[18] = 655.0 ; 
	FirFilter_563_s.COEFF[19] = 688.0 ; 
	FirFilter_563_s.COEFF[20] = 721.0 ; 
	FirFilter_563_s.COEFF[21] = 754.0 ; 
	FirFilter_563_s.COEFF[22] = 787.0 ; 
	FirFilter_563_s.COEFF[23] = 820.0 ; 
	FirFilter_563_s.COEFF[24] = 853.0 ; 
	FirFilter_563_s.COEFF[25] = 886.0 ; 
	FirFilter_563_s.COEFF[26] = 919.0 ; 
	FirFilter_563_s.COEFF[27] = 952.0 ; 
	FirFilter_563_s.COEFF[28] = 985.0 ; 
	FirFilter_563_s.COEFF[29] = 1018.0 ; 
	FirFilter_563_s.COEFF[30] = 1051.0 ; 
	FirFilter_563_s.COEFF[31] = 1084.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin235_FirFilter_Fiss_668_702_split[0], i) * FirFilter_563_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin235_FirFilter_Fiss_668_702_split[0]) ; 
		push_float(&SplitJoin235_FirFilter_Fiss_668_702_join[0], sum) ; 
 {
		FOR(int, streamItVar109, 0,  < , 7, streamItVar109++) {
			pop_void(&SplitJoin235_FirFilter_Fiss_668_702_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_564
	 {
	FirFilter_564_s.COEFF[0] = 61.0 ; 
	FirFilter_564_s.COEFF[1] = 94.0 ; 
	FirFilter_564_s.COEFF[2] = 127.0 ; 
	FirFilter_564_s.COEFF[3] = 160.0 ; 
	FirFilter_564_s.COEFF[4] = 193.0 ; 
	FirFilter_564_s.COEFF[5] = 226.0 ; 
	FirFilter_564_s.COEFF[6] = 259.0 ; 
	FirFilter_564_s.COEFF[7] = 292.0 ; 
	FirFilter_564_s.COEFF[8] = 325.0 ; 
	FirFilter_564_s.COEFF[9] = 358.0 ; 
	FirFilter_564_s.COEFF[10] = 391.0 ; 
	FirFilter_564_s.COEFF[11] = 424.0 ; 
	FirFilter_564_s.COEFF[12] = 457.0 ; 
	FirFilter_564_s.COEFF[13] = 490.0 ; 
	FirFilter_564_s.COEFF[14] = 523.0 ; 
	FirFilter_564_s.COEFF[15] = 556.0 ; 
	FirFilter_564_s.COEFF[16] = 589.0 ; 
	FirFilter_564_s.COEFF[17] = 622.0 ; 
	FirFilter_564_s.COEFF[18] = 655.0 ; 
	FirFilter_564_s.COEFF[19] = 688.0 ; 
	FirFilter_564_s.COEFF[20] = 721.0 ; 
	FirFilter_564_s.COEFF[21] = 754.0 ; 
	FirFilter_564_s.COEFF[22] = 787.0 ; 
	FirFilter_564_s.COEFF[23] = 820.0 ; 
	FirFilter_564_s.COEFF[24] = 853.0 ; 
	FirFilter_564_s.COEFF[25] = 886.0 ; 
	FirFilter_564_s.COEFF[26] = 919.0 ; 
	FirFilter_564_s.COEFF[27] = 952.0 ; 
	FirFilter_564_s.COEFF[28] = 985.0 ; 
	FirFilter_564_s.COEFF[29] = 1018.0 ; 
	FirFilter_564_s.COEFF[30] = 1051.0 ; 
	FirFilter_564_s.COEFF[31] = 1084.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin235_FirFilter_Fiss_668_702_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin235_FirFilter_Fiss_668_702_split[1], i) * FirFilter_564_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin235_FirFilter_Fiss_668_702_split[1]) ; 
		push_float(&SplitJoin235_FirFilter_Fiss_668_702_join[1], sum) ; 
 {
		FOR(int, streamItVar110, 0,  < , 6, streamItVar110++) {
			pop_void(&SplitJoin235_FirFilter_Fiss_668_702_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_565
	 {
	FirFilter_565_s.COEFF[0] = 61.0 ; 
	FirFilter_565_s.COEFF[1] = 94.0 ; 
	FirFilter_565_s.COEFF[2] = 127.0 ; 
	FirFilter_565_s.COEFF[3] = 160.0 ; 
	FirFilter_565_s.COEFF[4] = 193.0 ; 
	FirFilter_565_s.COEFF[5] = 226.0 ; 
	FirFilter_565_s.COEFF[6] = 259.0 ; 
	FirFilter_565_s.COEFF[7] = 292.0 ; 
	FirFilter_565_s.COEFF[8] = 325.0 ; 
	FirFilter_565_s.COEFF[9] = 358.0 ; 
	FirFilter_565_s.COEFF[10] = 391.0 ; 
	FirFilter_565_s.COEFF[11] = 424.0 ; 
	FirFilter_565_s.COEFF[12] = 457.0 ; 
	FirFilter_565_s.COEFF[13] = 490.0 ; 
	FirFilter_565_s.COEFF[14] = 523.0 ; 
	FirFilter_565_s.COEFF[15] = 556.0 ; 
	FirFilter_565_s.COEFF[16] = 589.0 ; 
	FirFilter_565_s.COEFF[17] = 622.0 ; 
	FirFilter_565_s.COEFF[18] = 655.0 ; 
	FirFilter_565_s.COEFF[19] = 688.0 ; 
	FirFilter_565_s.COEFF[20] = 721.0 ; 
	FirFilter_565_s.COEFF[21] = 754.0 ; 
	FirFilter_565_s.COEFF[22] = 787.0 ; 
	FirFilter_565_s.COEFF[23] = 820.0 ; 
	FirFilter_565_s.COEFF[24] = 853.0 ; 
	FirFilter_565_s.COEFF[25] = 886.0 ; 
	FirFilter_565_s.COEFF[26] = 919.0 ; 
	FirFilter_565_s.COEFF[27] = 952.0 ; 
	FirFilter_565_s.COEFF[28] = 985.0 ; 
	FirFilter_565_s.COEFF[29] = 1018.0 ; 
	FirFilter_565_s.COEFF[30] = 1051.0 ; 
	FirFilter_565_s.COEFF[31] = 1084.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar111, 0,  < , 2, streamItVar111++) {
			pop_void(&SplitJoin235_FirFilter_Fiss_668_702_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin235_FirFilter_Fiss_668_702_split[2], i) * FirFilter_565_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin235_FirFilter_Fiss_668_702_split[2]) ; 
		push_float(&SplitJoin235_FirFilter_Fiss_668_702_join[2], sum) ; 
 {
		FOR(int, streamItVar112, 0,  < , 5, streamItVar112++) {
			pop_void(&SplitJoin235_FirFilter_Fiss_668_702_split[2]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_566
	 {
	FirFilter_566_s.COEFF[0] = 61.0 ; 
	FirFilter_566_s.COEFF[1] = 94.0 ; 
	FirFilter_566_s.COEFF[2] = 127.0 ; 
	FirFilter_566_s.COEFF[3] = 160.0 ; 
	FirFilter_566_s.COEFF[4] = 193.0 ; 
	FirFilter_566_s.COEFF[5] = 226.0 ; 
	FirFilter_566_s.COEFF[6] = 259.0 ; 
	FirFilter_566_s.COEFF[7] = 292.0 ; 
	FirFilter_566_s.COEFF[8] = 325.0 ; 
	FirFilter_566_s.COEFF[9] = 358.0 ; 
	FirFilter_566_s.COEFF[10] = 391.0 ; 
	FirFilter_566_s.COEFF[11] = 424.0 ; 
	FirFilter_566_s.COEFF[12] = 457.0 ; 
	FirFilter_566_s.COEFF[13] = 490.0 ; 
	FirFilter_566_s.COEFF[14] = 523.0 ; 
	FirFilter_566_s.COEFF[15] = 556.0 ; 
	FirFilter_566_s.COEFF[16] = 589.0 ; 
	FirFilter_566_s.COEFF[17] = 622.0 ; 
	FirFilter_566_s.COEFF[18] = 655.0 ; 
	FirFilter_566_s.COEFF[19] = 688.0 ; 
	FirFilter_566_s.COEFF[20] = 721.0 ; 
	FirFilter_566_s.COEFF[21] = 754.0 ; 
	FirFilter_566_s.COEFF[22] = 787.0 ; 
	FirFilter_566_s.COEFF[23] = 820.0 ; 
	FirFilter_566_s.COEFF[24] = 853.0 ; 
	FirFilter_566_s.COEFF[25] = 886.0 ; 
	FirFilter_566_s.COEFF[26] = 919.0 ; 
	FirFilter_566_s.COEFF[27] = 952.0 ; 
	FirFilter_566_s.COEFF[28] = 985.0 ; 
	FirFilter_566_s.COEFF[29] = 1018.0 ; 
	FirFilter_566_s.COEFF[30] = 1051.0 ; 
	FirFilter_566_s.COEFF[31] = 1084.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar113, 0,  < , 3, streamItVar113++) {
			pop_void(&SplitJoin235_FirFilter_Fiss_668_702_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin235_FirFilter_Fiss_668_702_split[3], i) * FirFilter_566_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin235_FirFilter_Fiss_668_702_split[3]) ; 
		push_float(&SplitJoin235_FirFilter_Fiss_668_702_join[3], sum) ; 
 {
		FOR(int, streamItVar114, 0,  < , 4, streamItVar114++) {
			pop_void(&SplitJoin235_FirFilter_Fiss_668_702_split[3]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_567
	 {
	FirFilter_567_s.COEFF[0] = 61.0 ; 
	FirFilter_567_s.COEFF[1] = 94.0 ; 
	FirFilter_567_s.COEFF[2] = 127.0 ; 
	FirFilter_567_s.COEFF[3] = 160.0 ; 
	FirFilter_567_s.COEFF[4] = 193.0 ; 
	FirFilter_567_s.COEFF[5] = 226.0 ; 
	FirFilter_567_s.COEFF[6] = 259.0 ; 
	FirFilter_567_s.COEFF[7] = 292.0 ; 
	FirFilter_567_s.COEFF[8] = 325.0 ; 
	FirFilter_567_s.COEFF[9] = 358.0 ; 
	FirFilter_567_s.COEFF[10] = 391.0 ; 
	FirFilter_567_s.COEFF[11] = 424.0 ; 
	FirFilter_567_s.COEFF[12] = 457.0 ; 
	FirFilter_567_s.COEFF[13] = 490.0 ; 
	FirFilter_567_s.COEFF[14] = 523.0 ; 
	FirFilter_567_s.COEFF[15] = 556.0 ; 
	FirFilter_567_s.COEFF[16] = 589.0 ; 
	FirFilter_567_s.COEFF[17] = 622.0 ; 
	FirFilter_567_s.COEFF[18] = 655.0 ; 
	FirFilter_567_s.COEFF[19] = 688.0 ; 
	FirFilter_567_s.COEFF[20] = 721.0 ; 
	FirFilter_567_s.COEFF[21] = 754.0 ; 
	FirFilter_567_s.COEFF[22] = 787.0 ; 
	FirFilter_567_s.COEFF[23] = 820.0 ; 
	FirFilter_567_s.COEFF[24] = 853.0 ; 
	FirFilter_567_s.COEFF[25] = 886.0 ; 
	FirFilter_567_s.COEFF[26] = 919.0 ; 
	FirFilter_567_s.COEFF[27] = 952.0 ; 
	FirFilter_567_s.COEFF[28] = 985.0 ; 
	FirFilter_567_s.COEFF[29] = 1018.0 ; 
	FirFilter_567_s.COEFF[30] = 1051.0 ; 
	FirFilter_567_s.COEFF[31] = 1084.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar115, 0,  < , 4, streamItVar115++) {
			pop_void(&SplitJoin235_FirFilter_Fiss_668_702_split[4]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin235_FirFilter_Fiss_668_702_split[4], i) * FirFilter_567_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin235_FirFilter_Fiss_668_702_split[4]) ; 
		push_float(&SplitJoin235_FirFilter_Fiss_668_702_join[4], sum) ; 
 {
		FOR(int, streamItVar116, 0,  < , 3, streamItVar116++) {
			pop_void(&SplitJoin235_FirFilter_Fiss_668_702_split[4]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_568
	 {
	FirFilter_568_s.COEFF[0] = 61.0 ; 
	FirFilter_568_s.COEFF[1] = 94.0 ; 
	FirFilter_568_s.COEFF[2] = 127.0 ; 
	FirFilter_568_s.COEFF[3] = 160.0 ; 
	FirFilter_568_s.COEFF[4] = 193.0 ; 
	FirFilter_568_s.COEFF[5] = 226.0 ; 
	FirFilter_568_s.COEFF[6] = 259.0 ; 
	FirFilter_568_s.COEFF[7] = 292.0 ; 
	FirFilter_568_s.COEFF[8] = 325.0 ; 
	FirFilter_568_s.COEFF[9] = 358.0 ; 
	FirFilter_568_s.COEFF[10] = 391.0 ; 
	FirFilter_568_s.COEFF[11] = 424.0 ; 
	FirFilter_568_s.COEFF[12] = 457.0 ; 
	FirFilter_568_s.COEFF[13] = 490.0 ; 
	FirFilter_568_s.COEFF[14] = 523.0 ; 
	FirFilter_568_s.COEFF[15] = 556.0 ; 
	FirFilter_568_s.COEFF[16] = 589.0 ; 
	FirFilter_568_s.COEFF[17] = 622.0 ; 
	FirFilter_568_s.COEFF[18] = 655.0 ; 
	FirFilter_568_s.COEFF[19] = 688.0 ; 
	FirFilter_568_s.COEFF[20] = 721.0 ; 
	FirFilter_568_s.COEFF[21] = 754.0 ; 
	FirFilter_568_s.COEFF[22] = 787.0 ; 
	FirFilter_568_s.COEFF[23] = 820.0 ; 
	FirFilter_568_s.COEFF[24] = 853.0 ; 
	FirFilter_568_s.COEFF[25] = 886.0 ; 
	FirFilter_568_s.COEFF[26] = 919.0 ; 
	FirFilter_568_s.COEFF[27] = 952.0 ; 
	FirFilter_568_s.COEFF[28] = 985.0 ; 
	FirFilter_568_s.COEFF[29] = 1018.0 ; 
	FirFilter_568_s.COEFF[30] = 1051.0 ; 
	FirFilter_568_s.COEFF[31] = 1084.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar117, 0,  < , 5, streamItVar117++) {
			pop_void(&SplitJoin235_FirFilter_Fiss_668_702_split[5]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin235_FirFilter_Fiss_668_702_split[5], i) * FirFilter_568_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin235_FirFilter_Fiss_668_702_split[5]) ; 
		push_float(&SplitJoin235_FirFilter_Fiss_668_702_join[5], sum) ; 
 {
		FOR(int, streamItVar118, 0,  < , 2, streamItVar118++) {
			pop_void(&SplitJoin235_FirFilter_Fiss_668_702_split[5]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_569
	 {
	FirFilter_569_s.COEFF[0] = 61.0 ; 
	FirFilter_569_s.COEFF[1] = 94.0 ; 
	FirFilter_569_s.COEFF[2] = 127.0 ; 
	FirFilter_569_s.COEFF[3] = 160.0 ; 
	FirFilter_569_s.COEFF[4] = 193.0 ; 
	FirFilter_569_s.COEFF[5] = 226.0 ; 
	FirFilter_569_s.COEFF[6] = 259.0 ; 
	FirFilter_569_s.COEFF[7] = 292.0 ; 
	FirFilter_569_s.COEFF[8] = 325.0 ; 
	FirFilter_569_s.COEFF[9] = 358.0 ; 
	FirFilter_569_s.COEFF[10] = 391.0 ; 
	FirFilter_569_s.COEFF[11] = 424.0 ; 
	FirFilter_569_s.COEFF[12] = 457.0 ; 
	FirFilter_569_s.COEFF[13] = 490.0 ; 
	FirFilter_569_s.COEFF[14] = 523.0 ; 
	FirFilter_569_s.COEFF[15] = 556.0 ; 
	FirFilter_569_s.COEFF[16] = 589.0 ; 
	FirFilter_569_s.COEFF[17] = 622.0 ; 
	FirFilter_569_s.COEFF[18] = 655.0 ; 
	FirFilter_569_s.COEFF[19] = 688.0 ; 
	FirFilter_569_s.COEFF[20] = 721.0 ; 
	FirFilter_569_s.COEFF[21] = 754.0 ; 
	FirFilter_569_s.COEFF[22] = 787.0 ; 
	FirFilter_569_s.COEFF[23] = 820.0 ; 
	FirFilter_569_s.COEFF[24] = 853.0 ; 
	FirFilter_569_s.COEFF[25] = 886.0 ; 
	FirFilter_569_s.COEFF[26] = 919.0 ; 
	FirFilter_569_s.COEFF[27] = 952.0 ; 
	FirFilter_569_s.COEFF[28] = 985.0 ; 
	FirFilter_569_s.COEFF[29] = 1018.0 ; 
	FirFilter_569_s.COEFF[30] = 1051.0 ; 
	FirFilter_569_s.COEFF[31] = 1084.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar119, 0,  < , 6, streamItVar119++) {
			pop_void(&SplitJoin235_FirFilter_Fiss_668_702_split[6]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin235_FirFilter_Fiss_668_702_split[6], i) * FirFilter_569_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin235_FirFilter_Fiss_668_702_split[6]) ; 
		push_float(&SplitJoin235_FirFilter_Fiss_668_702_join[6], sum) ; 
 {
		pop_void(&SplitJoin235_FirFilter_Fiss_668_702_split[6]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_570
	 {
	FirFilter_570_s.COEFF[0] = 61.0 ; 
	FirFilter_570_s.COEFF[1] = 94.0 ; 
	FirFilter_570_s.COEFF[2] = 127.0 ; 
	FirFilter_570_s.COEFF[3] = 160.0 ; 
	FirFilter_570_s.COEFF[4] = 193.0 ; 
	FirFilter_570_s.COEFF[5] = 226.0 ; 
	FirFilter_570_s.COEFF[6] = 259.0 ; 
	FirFilter_570_s.COEFF[7] = 292.0 ; 
	FirFilter_570_s.COEFF[8] = 325.0 ; 
	FirFilter_570_s.COEFF[9] = 358.0 ; 
	FirFilter_570_s.COEFF[10] = 391.0 ; 
	FirFilter_570_s.COEFF[11] = 424.0 ; 
	FirFilter_570_s.COEFF[12] = 457.0 ; 
	FirFilter_570_s.COEFF[13] = 490.0 ; 
	FirFilter_570_s.COEFF[14] = 523.0 ; 
	FirFilter_570_s.COEFF[15] = 556.0 ; 
	FirFilter_570_s.COEFF[16] = 589.0 ; 
	FirFilter_570_s.COEFF[17] = 622.0 ; 
	FirFilter_570_s.COEFF[18] = 655.0 ; 
	FirFilter_570_s.COEFF[19] = 688.0 ; 
	FirFilter_570_s.COEFF[20] = 721.0 ; 
	FirFilter_570_s.COEFF[21] = 754.0 ; 
	FirFilter_570_s.COEFF[22] = 787.0 ; 
	FirFilter_570_s.COEFF[23] = 820.0 ; 
	FirFilter_570_s.COEFF[24] = 853.0 ; 
	FirFilter_570_s.COEFF[25] = 886.0 ; 
	FirFilter_570_s.COEFF[26] = 919.0 ; 
	FirFilter_570_s.COEFF[27] = 952.0 ; 
	FirFilter_570_s.COEFF[28] = 985.0 ; 
	FirFilter_570_s.COEFF[29] = 1018.0 ; 
	FirFilter_570_s.COEFF[30] = 1051.0 ; 
	FirFilter_570_s.COEFF[31] = 1084.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar120, 0,  < , 7, streamItVar120++) {
			pop_void(&SplitJoin235_FirFilter_Fiss_668_702_split[7]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin235_FirFilter_Fiss_668_702_split[7], i) * FirFilter_570_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin235_FirFilter_Fiss_668_702_split[7]) ; 
		push_float(&SplitJoin235_FirFilter_Fiss_668_702_join[7], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_562
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_562DownSamp_278, pop_float(&SplitJoin235_FirFilter_Fiss_668_702_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DownSamp_278
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		push_float(&DownSamp_278UpSamp_279, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_562DownSamp_278)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_562DownSamp_278) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: UpSamp_279
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		push_float(&UpSamp_279WEIGHTED_ROUND_ROBIN_Splitter_571, pop_float(&DownSamp_278UpSamp_279)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			push_float(&UpSamp_279WEIGHTED_ROUND_ROBIN_Splitter_571, 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_571
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&SplitJoin237_Delay_N_Fiss_669_703_split[__iter_], pop_float(&UpSamp_279WEIGHTED_ROUND_ROBIN_Splitter_571));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_573
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin237_Delay_N_Fiss_669_703_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_574
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin237_Delay_N_Fiss_669_703_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_575
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin237_Delay_N_Fiss_669_703_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_576
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin237_Delay_N_Fiss_669_703_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_577
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin237_Delay_N_Fiss_669_703_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_578
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin237_Delay_N_Fiss_669_703_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_579
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin237_Delay_N_Fiss_669_703_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_580
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin237_Delay_N_Fiss_669_703_join[7], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_572
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_572DUPLICATE_Splitter_581, pop_float(&SplitJoin237_Delay_N_Fiss_669_703_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_581
	FOR(uint32_t, __iter_init_, 0, <, 31, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_572DUPLICATE_Splitter_581);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin239_FirFilter_Fiss_670_704_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_583
	 {
	FirFilter_583_s.COEFF[0] = 42.0 ; 
	FirFilter_583_s.COEFF[1] = 49.0 ; 
	FirFilter_583_s.COEFF[2] = 56.0 ; 
	FirFilter_583_s.COEFF[3] = 63.0 ; 
	FirFilter_583_s.COEFF[4] = 70.0 ; 
	FirFilter_583_s.COEFF[5] = 77.0 ; 
	FirFilter_583_s.COEFF[6] = 84.0 ; 
	FirFilter_583_s.COEFF[7] = 91.0 ; 
	FirFilter_583_s.COEFF[8] = 98.0 ; 
	FirFilter_583_s.COEFF[9] = 105.0 ; 
	FirFilter_583_s.COEFF[10] = 112.0 ; 
	FirFilter_583_s.COEFF[11] = 119.0 ; 
	FirFilter_583_s.COEFF[12] = 126.0 ; 
	FirFilter_583_s.COEFF[13] = 133.0 ; 
	FirFilter_583_s.COEFF[14] = 140.0 ; 
	FirFilter_583_s.COEFF[15] = 147.0 ; 
	FirFilter_583_s.COEFF[16] = 154.0 ; 
	FirFilter_583_s.COEFF[17] = 161.0 ; 
	FirFilter_583_s.COEFF[18] = 168.0 ; 
	FirFilter_583_s.COEFF[19] = 175.0 ; 
	FirFilter_583_s.COEFF[20] = 182.0 ; 
	FirFilter_583_s.COEFF[21] = 189.0 ; 
	FirFilter_583_s.COEFF[22] = 196.0 ; 
	FirFilter_583_s.COEFF[23] = 203.0 ; 
	FirFilter_583_s.COEFF[24] = 210.0 ; 
	FirFilter_583_s.COEFF[25] = 217.0 ; 
	FirFilter_583_s.COEFF[26] = 224.0 ; 
	FirFilter_583_s.COEFF[27] = 231.0 ; 
	FirFilter_583_s.COEFF[28] = 238.0 ; 
	FirFilter_583_s.COEFF[29] = 245.0 ; 
	FirFilter_583_s.COEFF[30] = 252.0 ; 
	FirFilter_583_s.COEFF[31] = 259.0 ; 
}
//--------------------------------
// --- init: FirFilter_584
	 {
	FirFilter_584_s.COEFF[0] = 42.0 ; 
	FirFilter_584_s.COEFF[1] = 49.0 ; 
	FirFilter_584_s.COEFF[2] = 56.0 ; 
	FirFilter_584_s.COEFF[3] = 63.0 ; 
	FirFilter_584_s.COEFF[4] = 70.0 ; 
	FirFilter_584_s.COEFF[5] = 77.0 ; 
	FirFilter_584_s.COEFF[6] = 84.0 ; 
	FirFilter_584_s.COEFF[7] = 91.0 ; 
	FirFilter_584_s.COEFF[8] = 98.0 ; 
	FirFilter_584_s.COEFF[9] = 105.0 ; 
	FirFilter_584_s.COEFF[10] = 112.0 ; 
	FirFilter_584_s.COEFF[11] = 119.0 ; 
	FirFilter_584_s.COEFF[12] = 126.0 ; 
	FirFilter_584_s.COEFF[13] = 133.0 ; 
	FirFilter_584_s.COEFF[14] = 140.0 ; 
	FirFilter_584_s.COEFF[15] = 147.0 ; 
	FirFilter_584_s.COEFF[16] = 154.0 ; 
	FirFilter_584_s.COEFF[17] = 161.0 ; 
	FirFilter_584_s.COEFF[18] = 168.0 ; 
	FirFilter_584_s.COEFF[19] = 175.0 ; 
	FirFilter_584_s.COEFF[20] = 182.0 ; 
	FirFilter_584_s.COEFF[21] = 189.0 ; 
	FirFilter_584_s.COEFF[22] = 196.0 ; 
	FirFilter_584_s.COEFF[23] = 203.0 ; 
	FirFilter_584_s.COEFF[24] = 210.0 ; 
	FirFilter_584_s.COEFF[25] = 217.0 ; 
	FirFilter_584_s.COEFF[26] = 224.0 ; 
	FirFilter_584_s.COEFF[27] = 231.0 ; 
	FirFilter_584_s.COEFF[28] = 238.0 ; 
	FirFilter_584_s.COEFF[29] = 245.0 ; 
	FirFilter_584_s.COEFF[30] = 252.0 ; 
	FirFilter_584_s.COEFF[31] = 259.0 ; 
}
//--------------------------------
// --- init: FirFilter_585
	 {
	FirFilter_585_s.COEFF[0] = 42.0 ; 
	FirFilter_585_s.COEFF[1] = 49.0 ; 
	FirFilter_585_s.COEFF[2] = 56.0 ; 
	FirFilter_585_s.COEFF[3] = 63.0 ; 
	FirFilter_585_s.COEFF[4] = 70.0 ; 
	FirFilter_585_s.COEFF[5] = 77.0 ; 
	FirFilter_585_s.COEFF[6] = 84.0 ; 
	FirFilter_585_s.COEFF[7] = 91.0 ; 
	FirFilter_585_s.COEFF[8] = 98.0 ; 
	FirFilter_585_s.COEFF[9] = 105.0 ; 
	FirFilter_585_s.COEFF[10] = 112.0 ; 
	FirFilter_585_s.COEFF[11] = 119.0 ; 
	FirFilter_585_s.COEFF[12] = 126.0 ; 
	FirFilter_585_s.COEFF[13] = 133.0 ; 
	FirFilter_585_s.COEFF[14] = 140.0 ; 
	FirFilter_585_s.COEFF[15] = 147.0 ; 
	FirFilter_585_s.COEFF[16] = 154.0 ; 
	FirFilter_585_s.COEFF[17] = 161.0 ; 
	FirFilter_585_s.COEFF[18] = 168.0 ; 
	FirFilter_585_s.COEFF[19] = 175.0 ; 
	FirFilter_585_s.COEFF[20] = 182.0 ; 
	FirFilter_585_s.COEFF[21] = 189.0 ; 
	FirFilter_585_s.COEFF[22] = 196.0 ; 
	FirFilter_585_s.COEFF[23] = 203.0 ; 
	FirFilter_585_s.COEFF[24] = 210.0 ; 
	FirFilter_585_s.COEFF[25] = 217.0 ; 
	FirFilter_585_s.COEFF[26] = 224.0 ; 
	FirFilter_585_s.COEFF[27] = 231.0 ; 
	FirFilter_585_s.COEFF[28] = 238.0 ; 
	FirFilter_585_s.COEFF[29] = 245.0 ; 
	FirFilter_585_s.COEFF[30] = 252.0 ; 
	FirFilter_585_s.COEFF[31] = 259.0 ; 
}
//--------------------------------
// --- init: FirFilter_586
	 {
	FirFilter_586_s.COEFF[0] = 42.0 ; 
	FirFilter_586_s.COEFF[1] = 49.0 ; 
	FirFilter_586_s.COEFF[2] = 56.0 ; 
	FirFilter_586_s.COEFF[3] = 63.0 ; 
	FirFilter_586_s.COEFF[4] = 70.0 ; 
	FirFilter_586_s.COEFF[5] = 77.0 ; 
	FirFilter_586_s.COEFF[6] = 84.0 ; 
	FirFilter_586_s.COEFF[7] = 91.0 ; 
	FirFilter_586_s.COEFF[8] = 98.0 ; 
	FirFilter_586_s.COEFF[9] = 105.0 ; 
	FirFilter_586_s.COEFF[10] = 112.0 ; 
	FirFilter_586_s.COEFF[11] = 119.0 ; 
	FirFilter_586_s.COEFF[12] = 126.0 ; 
	FirFilter_586_s.COEFF[13] = 133.0 ; 
	FirFilter_586_s.COEFF[14] = 140.0 ; 
	FirFilter_586_s.COEFF[15] = 147.0 ; 
	FirFilter_586_s.COEFF[16] = 154.0 ; 
	FirFilter_586_s.COEFF[17] = 161.0 ; 
	FirFilter_586_s.COEFF[18] = 168.0 ; 
	FirFilter_586_s.COEFF[19] = 175.0 ; 
	FirFilter_586_s.COEFF[20] = 182.0 ; 
	FirFilter_586_s.COEFF[21] = 189.0 ; 
	FirFilter_586_s.COEFF[22] = 196.0 ; 
	FirFilter_586_s.COEFF[23] = 203.0 ; 
	FirFilter_586_s.COEFF[24] = 210.0 ; 
	FirFilter_586_s.COEFF[25] = 217.0 ; 
	FirFilter_586_s.COEFF[26] = 224.0 ; 
	FirFilter_586_s.COEFF[27] = 231.0 ; 
	FirFilter_586_s.COEFF[28] = 238.0 ; 
	FirFilter_586_s.COEFF[29] = 245.0 ; 
	FirFilter_586_s.COEFF[30] = 252.0 ; 
	FirFilter_586_s.COEFF[31] = 259.0 ; 
}
//--------------------------------
// --- init: FirFilter_587
	 {
	FirFilter_587_s.COEFF[0] = 42.0 ; 
	FirFilter_587_s.COEFF[1] = 49.0 ; 
	FirFilter_587_s.COEFF[2] = 56.0 ; 
	FirFilter_587_s.COEFF[3] = 63.0 ; 
	FirFilter_587_s.COEFF[4] = 70.0 ; 
	FirFilter_587_s.COEFF[5] = 77.0 ; 
	FirFilter_587_s.COEFF[6] = 84.0 ; 
	FirFilter_587_s.COEFF[7] = 91.0 ; 
	FirFilter_587_s.COEFF[8] = 98.0 ; 
	FirFilter_587_s.COEFF[9] = 105.0 ; 
	FirFilter_587_s.COEFF[10] = 112.0 ; 
	FirFilter_587_s.COEFF[11] = 119.0 ; 
	FirFilter_587_s.COEFF[12] = 126.0 ; 
	FirFilter_587_s.COEFF[13] = 133.0 ; 
	FirFilter_587_s.COEFF[14] = 140.0 ; 
	FirFilter_587_s.COEFF[15] = 147.0 ; 
	FirFilter_587_s.COEFF[16] = 154.0 ; 
	FirFilter_587_s.COEFF[17] = 161.0 ; 
	FirFilter_587_s.COEFF[18] = 168.0 ; 
	FirFilter_587_s.COEFF[19] = 175.0 ; 
	FirFilter_587_s.COEFF[20] = 182.0 ; 
	FirFilter_587_s.COEFF[21] = 189.0 ; 
	FirFilter_587_s.COEFF[22] = 196.0 ; 
	FirFilter_587_s.COEFF[23] = 203.0 ; 
	FirFilter_587_s.COEFF[24] = 210.0 ; 
	FirFilter_587_s.COEFF[25] = 217.0 ; 
	FirFilter_587_s.COEFF[26] = 224.0 ; 
	FirFilter_587_s.COEFF[27] = 231.0 ; 
	FirFilter_587_s.COEFF[28] = 238.0 ; 
	FirFilter_587_s.COEFF[29] = 245.0 ; 
	FirFilter_587_s.COEFF[30] = 252.0 ; 
	FirFilter_587_s.COEFF[31] = 259.0 ; 
}
//--------------------------------
// --- init: FirFilter_588
	 {
	FirFilter_588_s.COEFF[0] = 42.0 ; 
	FirFilter_588_s.COEFF[1] = 49.0 ; 
	FirFilter_588_s.COEFF[2] = 56.0 ; 
	FirFilter_588_s.COEFF[3] = 63.0 ; 
	FirFilter_588_s.COEFF[4] = 70.0 ; 
	FirFilter_588_s.COEFF[5] = 77.0 ; 
	FirFilter_588_s.COEFF[6] = 84.0 ; 
	FirFilter_588_s.COEFF[7] = 91.0 ; 
	FirFilter_588_s.COEFF[8] = 98.0 ; 
	FirFilter_588_s.COEFF[9] = 105.0 ; 
	FirFilter_588_s.COEFF[10] = 112.0 ; 
	FirFilter_588_s.COEFF[11] = 119.0 ; 
	FirFilter_588_s.COEFF[12] = 126.0 ; 
	FirFilter_588_s.COEFF[13] = 133.0 ; 
	FirFilter_588_s.COEFF[14] = 140.0 ; 
	FirFilter_588_s.COEFF[15] = 147.0 ; 
	FirFilter_588_s.COEFF[16] = 154.0 ; 
	FirFilter_588_s.COEFF[17] = 161.0 ; 
	FirFilter_588_s.COEFF[18] = 168.0 ; 
	FirFilter_588_s.COEFF[19] = 175.0 ; 
	FirFilter_588_s.COEFF[20] = 182.0 ; 
	FirFilter_588_s.COEFF[21] = 189.0 ; 
	FirFilter_588_s.COEFF[22] = 196.0 ; 
	FirFilter_588_s.COEFF[23] = 203.0 ; 
	FirFilter_588_s.COEFF[24] = 210.0 ; 
	FirFilter_588_s.COEFF[25] = 217.0 ; 
	FirFilter_588_s.COEFF[26] = 224.0 ; 
	FirFilter_588_s.COEFF[27] = 231.0 ; 
	FirFilter_588_s.COEFF[28] = 238.0 ; 
	FirFilter_588_s.COEFF[29] = 245.0 ; 
	FirFilter_588_s.COEFF[30] = 252.0 ; 
	FirFilter_588_s.COEFF[31] = 259.0 ; 
}
//--------------------------------
// --- init: FirFilter_589
	 {
	FirFilter_589_s.COEFF[0] = 42.0 ; 
	FirFilter_589_s.COEFF[1] = 49.0 ; 
	FirFilter_589_s.COEFF[2] = 56.0 ; 
	FirFilter_589_s.COEFF[3] = 63.0 ; 
	FirFilter_589_s.COEFF[4] = 70.0 ; 
	FirFilter_589_s.COEFF[5] = 77.0 ; 
	FirFilter_589_s.COEFF[6] = 84.0 ; 
	FirFilter_589_s.COEFF[7] = 91.0 ; 
	FirFilter_589_s.COEFF[8] = 98.0 ; 
	FirFilter_589_s.COEFF[9] = 105.0 ; 
	FirFilter_589_s.COEFF[10] = 112.0 ; 
	FirFilter_589_s.COEFF[11] = 119.0 ; 
	FirFilter_589_s.COEFF[12] = 126.0 ; 
	FirFilter_589_s.COEFF[13] = 133.0 ; 
	FirFilter_589_s.COEFF[14] = 140.0 ; 
	FirFilter_589_s.COEFF[15] = 147.0 ; 
	FirFilter_589_s.COEFF[16] = 154.0 ; 
	FirFilter_589_s.COEFF[17] = 161.0 ; 
	FirFilter_589_s.COEFF[18] = 168.0 ; 
	FirFilter_589_s.COEFF[19] = 175.0 ; 
	FirFilter_589_s.COEFF[20] = 182.0 ; 
	FirFilter_589_s.COEFF[21] = 189.0 ; 
	FirFilter_589_s.COEFF[22] = 196.0 ; 
	FirFilter_589_s.COEFF[23] = 203.0 ; 
	FirFilter_589_s.COEFF[24] = 210.0 ; 
	FirFilter_589_s.COEFF[25] = 217.0 ; 
	FirFilter_589_s.COEFF[26] = 224.0 ; 
	FirFilter_589_s.COEFF[27] = 231.0 ; 
	FirFilter_589_s.COEFF[28] = 238.0 ; 
	FirFilter_589_s.COEFF[29] = 245.0 ; 
	FirFilter_589_s.COEFF[30] = 252.0 ; 
	FirFilter_589_s.COEFF[31] = 259.0 ; 
}
//--------------------------------
// --- init: FirFilter_590
	 {
	FirFilter_590_s.COEFF[0] = 42.0 ; 
	FirFilter_590_s.COEFF[1] = 49.0 ; 
	FirFilter_590_s.COEFF[2] = 56.0 ; 
	FirFilter_590_s.COEFF[3] = 63.0 ; 
	FirFilter_590_s.COEFF[4] = 70.0 ; 
	FirFilter_590_s.COEFF[5] = 77.0 ; 
	FirFilter_590_s.COEFF[6] = 84.0 ; 
	FirFilter_590_s.COEFF[7] = 91.0 ; 
	FirFilter_590_s.COEFF[8] = 98.0 ; 
	FirFilter_590_s.COEFF[9] = 105.0 ; 
	FirFilter_590_s.COEFF[10] = 112.0 ; 
	FirFilter_590_s.COEFF[11] = 119.0 ; 
	FirFilter_590_s.COEFF[12] = 126.0 ; 
	FirFilter_590_s.COEFF[13] = 133.0 ; 
	FirFilter_590_s.COEFF[14] = 140.0 ; 
	FirFilter_590_s.COEFF[15] = 147.0 ; 
	FirFilter_590_s.COEFF[16] = 154.0 ; 
	FirFilter_590_s.COEFF[17] = 161.0 ; 
	FirFilter_590_s.COEFF[18] = 168.0 ; 
	FirFilter_590_s.COEFF[19] = 175.0 ; 
	FirFilter_590_s.COEFF[20] = 182.0 ; 
	FirFilter_590_s.COEFF[21] = 189.0 ; 
	FirFilter_590_s.COEFF[22] = 196.0 ; 
	FirFilter_590_s.COEFF[23] = 203.0 ; 
	FirFilter_590_s.COEFF[24] = 210.0 ; 
	FirFilter_590_s.COEFF[25] = 217.0 ; 
	FirFilter_590_s.COEFF[26] = 224.0 ; 
	FirFilter_590_s.COEFF[27] = 231.0 ; 
	FirFilter_590_s.COEFF[28] = 238.0 ; 
	FirFilter_590_s.COEFF[29] = 245.0 ; 
	FirFilter_590_s.COEFF[30] = 252.0 ; 
	FirFilter_590_s.COEFF[31] = 259.0 ; 
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_591
	FOR(uint32_t, __iter_init_, 0, <, 66, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&SplitJoin270_Delay_N_Fiss_671_705_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_229_293_641_676_split[7]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_593
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin270_Delay_N_Fiss_671_705_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_594
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin270_Delay_N_Fiss_671_705_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_595
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin270_Delay_N_Fiss_671_705_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_596
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin270_Delay_N_Fiss_671_705_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_597
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin270_Delay_N_Fiss_671_705_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_598
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin270_Delay_N_Fiss_671_705_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_599
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin270_Delay_N_Fiss_671_705_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_600
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin270_Delay_N_Fiss_671_705_join[7], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_592
	FOR(uint32_t, __iter_init_, 0, <, 37, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_592DUPLICATE_Splitter_601, pop_float(&SplitJoin270_Delay_N_Fiss_671_705_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_601
	FOR(uint32_t, __iter_init_, 0, <, 295, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_592DUPLICATE_Splitter_601);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin272_FirFilter_Fiss_672_706_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_603
	 {
	FirFilter_603_s.COEFF[0] = 71.0 ; 
	FirFilter_603_s.COEFF[1] = 104.0 ; 
	FirFilter_603_s.COEFF[2] = 137.0 ; 
	FirFilter_603_s.COEFF[3] = 170.0 ; 
	FirFilter_603_s.COEFF[4] = 203.0 ; 
	FirFilter_603_s.COEFF[5] = 236.0 ; 
	FirFilter_603_s.COEFF[6] = 269.0 ; 
	FirFilter_603_s.COEFF[7] = 302.0 ; 
	FirFilter_603_s.COEFF[8] = 335.0 ; 
	FirFilter_603_s.COEFF[9] = 368.0 ; 
	FirFilter_603_s.COEFF[10] = 401.0 ; 
	FirFilter_603_s.COEFF[11] = 434.0 ; 
	FirFilter_603_s.COEFF[12] = 467.0 ; 
	FirFilter_603_s.COEFF[13] = 500.0 ; 
	FirFilter_603_s.COEFF[14] = 533.0 ; 
	FirFilter_603_s.COEFF[15] = 566.0 ; 
	FirFilter_603_s.COEFF[16] = 599.0 ; 
	FirFilter_603_s.COEFF[17] = 632.0 ; 
	FirFilter_603_s.COEFF[18] = 665.0 ; 
	FirFilter_603_s.COEFF[19] = 698.0 ; 
	FirFilter_603_s.COEFF[20] = 731.0 ; 
	FirFilter_603_s.COEFF[21] = 764.0 ; 
	FirFilter_603_s.COEFF[22] = 797.0 ; 
	FirFilter_603_s.COEFF[23] = 830.0 ; 
	FirFilter_603_s.COEFF[24] = 863.0 ; 
	FirFilter_603_s.COEFF[25] = 896.0 ; 
	FirFilter_603_s.COEFF[26] = 929.0 ; 
	FirFilter_603_s.COEFF[27] = 962.0 ; 
	FirFilter_603_s.COEFF[28] = 995.0 ; 
	FirFilter_603_s.COEFF[29] = 1028.0 ; 
	FirFilter_603_s.COEFF[30] = 1061.0 ; 
	FirFilter_603_s.COEFF[31] = 1094.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin272_FirFilter_Fiss_672_706_split[0], i) * FirFilter_603_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin272_FirFilter_Fiss_672_706_split[0]) ; 
		push_float(&SplitJoin272_FirFilter_Fiss_672_706_join[0], sum) ; 
 {
		FOR(int, streamItVar85, 0,  < , 7, streamItVar85++) {
			pop_void(&SplitJoin272_FirFilter_Fiss_672_706_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_604
	 {
	FirFilter_604_s.COEFF[0] = 71.0 ; 
	FirFilter_604_s.COEFF[1] = 104.0 ; 
	FirFilter_604_s.COEFF[2] = 137.0 ; 
	FirFilter_604_s.COEFF[3] = 170.0 ; 
	FirFilter_604_s.COEFF[4] = 203.0 ; 
	FirFilter_604_s.COEFF[5] = 236.0 ; 
	FirFilter_604_s.COEFF[6] = 269.0 ; 
	FirFilter_604_s.COEFF[7] = 302.0 ; 
	FirFilter_604_s.COEFF[8] = 335.0 ; 
	FirFilter_604_s.COEFF[9] = 368.0 ; 
	FirFilter_604_s.COEFF[10] = 401.0 ; 
	FirFilter_604_s.COEFF[11] = 434.0 ; 
	FirFilter_604_s.COEFF[12] = 467.0 ; 
	FirFilter_604_s.COEFF[13] = 500.0 ; 
	FirFilter_604_s.COEFF[14] = 533.0 ; 
	FirFilter_604_s.COEFF[15] = 566.0 ; 
	FirFilter_604_s.COEFF[16] = 599.0 ; 
	FirFilter_604_s.COEFF[17] = 632.0 ; 
	FirFilter_604_s.COEFF[18] = 665.0 ; 
	FirFilter_604_s.COEFF[19] = 698.0 ; 
	FirFilter_604_s.COEFF[20] = 731.0 ; 
	FirFilter_604_s.COEFF[21] = 764.0 ; 
	FirFilter_604_s.COEFF[22] = 797.0 ; 
	FirFilter_604_s.COEFF[23] = 830.0 ; 
	FirFilter_604_s.COEFF[24] = 863.0 ; 
	FirFilter_604_s.COEFF[25] = 896.0 ; 
	FirFilter_604_s.COEFF[26] = 929.0 ; 
	FirFilter_604_s.COEFF[27] = 962.0 ; 
	FirFilter_604_s.COEFF[28] = 995.0 ; 
	FirFilter_604_s.COEFF[29] = 1028.0 ; 
	FirFilter_604_s.COEFF[30] = 1061.0 ; 
	FirFilter_604_s.COEFF[31] = 1094.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin272_FirFilter_Fiss_672_706_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin272_FirFilter_Fiss_672_706_split[1], i) * FirFilter_604_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin272_FirFilter_Fiss_672_706_split[1]) ; 
		push_float(&SplitJoin272_FirFilter_Fiss_672_706_join[1], sum) ; 
 {
		FOR(int, streamItVar86, 0,  < , 6, streamItVar86++) {
			pop_void(&SplitJoin272_FirFilter_Fiss_672_706_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_605
	 {
	FirFilter_605_s.COEFF[0] = 71.0 ; 
	FirFilter_605_s.COEFF[1] = 104.0 ; 
	FirFilter_605_s.COEFF[2] = 137.0 ; 
	FirFilter_605_s.COEFF[3] = 170.0 ; 
	FirFilter_605_s.COEFF[4] = 203.0 ; 
	FirFilter_605_s.COEFF[5] = 236.0 ; 
	FirFilter_605_s.COEFF[6] = 269.0 ; 
	FirFilter_605_s.COEFF[7] = 302.0 ; 
	FirFilter_605_s.COEFF[8] = 335.0 ; 
	FirFilter_605_s.COEFF[9] = 368.0 ; 
	FirFilter_605_s.COEFF[10] = 401.0 ; 
	FirFilter_605_s.COEFF[11] = 434.0 ; 
	FirFilter_605_s.COEFF[12] = 467.0 ; 
	FirFilter_605_s.COEFF[13] = 500.0 ; 
	FirFilter_605_s.COEFF[14] = 533.0 ; 
	FirFilter_605_s.COEFF[15] = 566.0 ; 
	FirFilter_605_s.COEFF[16] = 599.0 ; 
	FirFilter_605_s.COEFF[17] = 632.0 ; 
	FirFilter_605_s.COEFF[18] = 665.0 ; 
	FirFilter_605_s.COEFF[19] = 698.0 ; 
	FirFilter_605_s.COEFF[20] = 731.0 ; 
	FirFilter_605_s.COEFF[21] = 764.0 ; 
	FirFilter_605_s.COEFF[22] = 797.0 ; 
	FirFilter_605_s.COEFF[23] = 830.0 ; 
	FirFilter_605_s.COEFF[24] = 863.0 ; 
	FirFilter_605_s.COEFF[25] = 896.0 ; 
	FirFilter_605_s.COEFF[26] = 929.0 ; 
	FirFilter_605_s.COEFF[27] = 962.0 ; 
	FirFilter_605_s.COEFF[28] = 995.0 ; 
	FirFilter_605_s.COEFF[29] = 1028.0 ; 
	FirFilter_605_s.COEFF[30] = 1061.0 ; 
	FirFilter_605_s.COEFF[31] = 1094.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar87, 0,  < , 2, streamItVar87++) {
			pop_void(&SplitJoin272_FirFilter_Fiss_672_706_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin272_FirFilter_Fiss_672_706_split[2], i) * FirFilter_605_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin272_FirFilter_Fiss_672_706_split[2]) ; 
		push_float(&SplitJoin272_FirFilter_Fiss_672_706_join[2], sum) ; 
 {
		FOR(int, streamItVar88, 0,  < , 5, streamItVar88++) {
			pop_void(&SplitJoin272_FirFilter_Fiss_672_706_split[2]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_606
	 {
	FirFilter_606_s.COEFF[0] = 71.0 ; 
	FirFilter_606_s.COEFF[1] = 104.0 ; 
	FirFilter_606_s.COEFF[2] = 137.0 ; 
	FirFilter_606_s.COEFF[3] = 170.0 ; 
	FirFilter_606_s.COEFF[4] = 203.0 ; 
	FirFilter_606_s.COEFF[5] = 236.0 ; 
	FirFilter_606_s.COEFF[6] = 269.0 ; 
	FirFilter_606_s.COEFF[7] = 302.0 ; 
	FirFilter_606_s.COEFF[8] = 335.0 ; 
	FirFilter_606_s.COEFF[9] = 368.0 ; 
	FirFilter_606_s.COEFF[10] = 401.0 ; 
	FirFilter_606_s.COEFF[11] = 434.0 ; 
	FirFilter_606_s.COEFF[12] = 467.0 ; 
	FirFilter_606_s.COEFF[13] = 500.0 ; 
	FirFilter_606_s.COEFF[14] = 533.0 ; 
	FirFilter_606_s.COEFF[15] = 566.0 ; 
	FirFilter_606_s.COEFF[16] = 599.0 ; 
	FirFilter_606_s.COEFF[17] = 632.0 ; 
	FirFilter_606_s.COEFF[18] = 665.0 ; 
	FirFilter_606_s.COEFF[19] = 698.0 ; 
	FirFilter_606_s.COEFF[20] = 731.0 ; 
	FirFilter_606_s.COEFF[21] = 764.0 ; 
	FirFilter_606_s.COEFF[22] = 797.0 ; 
	FirFilter_606_s.COEFF[23] = 830.0 ; 
	FirFilter_606_s.COEFF[24] = 863.0 ; 
	FirFilter_606_s.COEFF[25] = 896.0 ; 
	FirFilter_606_s.COEFF[26] = 929.0 ; 
	FirFilter_606_s.COEFF[27] = 962.0 ; 
	FirFilter_606_s.COEFF[28] = 995.0 ; 
	FirFilter_606_s.COEFF[29] = 1028.0 ; 
	FirFilter_606_s.COEFF[30] = 1061.0 ; 
	FirFilter_606_s.COEFF[31] = 1094.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar89, 0,  < , 3, streamItVar89++) {
			pop_void(&SplitJoin272_FirFilter_Fiss_672_706_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin272_FirFilter_Fiss_672_706_split[3], i) * FirFilter_606_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin272_FirFilter_Fiss_672_706_split[3]) ; 
		push_float(&SplitJoin272_FirFilter_Fiss_672_706_join[3], sum) ; 
 {
		FOR(int, streamItVar90, 0,  < , 4, streamItVar90++) {
			pop_void(&SplitJoin272_FirFilter_Fiss_672_706_split[3]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_607
	 {
	FirFilter_607_s.COEFF[0] = 71.0 ; 
	FirFilter_607_s.COEFF[1] = 104.0 ; 
	FirFilter_607_s.COEFF[2] = 137.0 ; 
	FirFilter_607_s.COEFF[3] = 170.0 ; 
	FirFilter_607_s.COEFF[4] = 203.0 ; 
	FirFilter_607_s.COEFF[5] = 236.0 ; 
	FirFilter_607_s.COEFF[6] = 269.0 ; 
	FirFilter_607_s.COEFF[7] = 302.0 ; 
	FirFilter_607_s.COEFF[8] = 335.0 ; 
	FirFilter_607_s.COEFF[9] = 368.0 ; 
	FirFilter_607_s.COEFF[10] = 401.0 ; 
	FirFilter_607_s.COEFF[11] = 434.0 ; 
	FirFilter_607_s.COEFF[12] = 467.0 ; 
	FirFilter_607_s.COEFF[13] = 500.0 ; 
	FirFilter_607_s.COEFF[14] = 533.0 ; 
	FirFilter_607_s.COEFF[15] = 566.0 ; 
	FirFilter_607_s.COEFF[16] = 599.0 ; 
	FirFilter_607_s.COEFF[17] = 632.0 ; 
	FirFilter_607_s.COEFF[18] = 665.0 ; 
	FirFilter_607_s.COEFF[19] = 698.0 ; 
	FirFilter_607_s.COEFF[20] = 731.0 ; 
	FirFilter_607_s.COEFF[21] = 764.0 ; 
	FirFilter_607_s.COEFF[22] = 797.0 ; 
	FirFilter_607_s.COEFF[23] = 830.0 ; 
	FirFilter_607_s.COEFF[24] = 863.0 ; 
	FirFilter_607_s.COEFF[25] = 896.0 ; 
	FirFilter_607_s.COEFF[26] = 929.0 ; 
	FirFilter_607_s.COEFF[27] = 962.0 ; 
	FirFilter_607_s.COEFF[28] = 995.0 ; 
	FirFilter_607_s.COEFF[29] = 1028.0 ; 
	FirFilter_607_s.COEFF[30] = 1061.0 ; 
	FirFilter_607_s.COEFF[31] = 1094.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar91, 0,  < , 4, streamItVar91++) {
			pop_void(&SplitJoin272_FirFilter_Fiss_672_706_split[4]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin272_FirFilter_Fiss_672_706_split[4], i) * FirFilter_607_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin272_FirFilter_Fiss_672_706_split[4]) ; 
		push_float(&SplitJoin272_FirFilter_Fiss_672_706_join[4], sum) ; 
 {
		FOR(int, streamItVar92, 0,  < , 3, streamItVar92++) {
			pop_void(&SplitJoin272_FirFilter_Fiss_672_706_split[4]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_608
	 {
	FirFilter_608_s.COEFF[0] = 71.0 ; 
	FirFilter_608_s.COEFF[1] = 104.0 ; 
	FirFilter_608_s.COEFF[2] = 137.0 ; 
	FirFilter_608_s.COEFF[3] = 170.0 ; 
	FirFilter_608_s.COEFF[4] = 203.0 ; 
	FirFilter_608_s.COEFF[5] = 236.0 ; 
	FirFilter_608_s.COEFF[6] = 269.0 ; 
	FirFilter_608_s.COEFF[7] = 302.0 ; 
	FirFilter_608_s.COEFF[8] = 335.0 ; 
	FirFilter_608_s.COEFF[9] = 368.0 ; 
	FirFilter_608_s.COEFF[10] = 401.0 ; 
	FirFilter_608_s.COEFF[11] = 434.0 ; 
	FirFilter_608_s.COEFF[12] = 467.0 ; 
	FirFilter_608_s.COEFF[13] = 500.0 ; 
	FirFilter_608_s.COEFF[14] = 533.0 ; 
	FirFilter_608_s.COEFF[15] = 566.0 ; 
	FirFilter_608_s.COEFF[16] = 599.0 ; 
	FirFilter_608_s.COEFF[17] = 632.0 ; 
	FirFilter_608_s.COEFF[18] = 665.0 ; 
	FirFilter_608_s.COEFF[19] = 698.0 ; 
	FirFilter_608_s.COEFF[20] = 731.0 ; 
	FirFilter_608_s.COEFF[21] = 764.0 ; 
	FirFilter_608_s.COEFF[22] = 797.0 ; 
	FirFilter_608_s.COEFF[23] = 830.0 ; 
	FirFilter_608_s.COEFF[24] = 863.0 ; 
	FirFilter_608_s.COEFF[25] = 896.0 ; 
	FirFilter_608_s.COEFF[26] = 929.0 ; 
	FirFilter_608_s.COEFF[27] = 962.0 ; 
	FirFilter_608_s.COEFF[28] = 995.0 ; 
	FirFilter_608_s.COEFF[29] = 1028.0 ; 
	FirFilter_608_s.COEFF[30] = 1061.0 ; 
	FirFilter_608_s.COEFF[31] = 1094.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar93, 0,  < , 5, streamItVar93++) {
			pop_void(&SplitJoin272_FirFilter_Fiss_672_706_split[5]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin272_FirFilter_Fiss_672_706_split[5], i) * FirFilter_608_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin272_FirFilter_Fiss_672_706_split[5]) ; 
		push_float(&SplitJoin272_FirFilter_Fiss_672_706_join[5], sum) ; 
 {
		FOR(int, streamItVar94, 0,  < , 2, streamItVar94++) {
			pop_void(&SplitJoin272_FirFilter_Fiss_672_706_split[5]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_609
	 {
	FirFilter_609_s.COEFF[0] = 71.0 ; 
	FirFilter_609_s.COEFF[1] = 104.0 ; 
	FirFilter_609_s.COEFF[2] = 137.0 ; 
	FirFilter_609_s.COEFF[3] = 170.0 ; 
	FirFilter_609_s.COEFF[4] = 203.0 ; 
	FirFilter_609_s.COEFF[5] = 236.0 ; 
	FirFilter_609_s.COEFF[6] = 269.0 ; 
	FirFilter_609_s.COEFF[7] = 302.0 ; 
	FirFilter_609_s.COEFF[8] = 335.0 ; 
	FirFilter_609_s.COEFF[9] = 368.0 ; 
	FirFilter_609_s.COEFF[10] = 401.0 ; 
	FirFilter_609_s.COEFF[11] = 434.0 ; 
	FirFilter_609_s.COEFF[12] = 467.0 ; 
	FirFilter_609_s.COEFF[13] = 500.0 ; 
	FirFilter_609_s.COEFF[14] = 533.0 ; 
	FirFilter_609_s.COEFF[15] = 566.0 ; 
	FirFilter_609_s.COEFF[16] = 599.0 ; 
	FirFilter_609_s.COEFF[17] = 632.0 ; 
	FirFilter_609_s.COEFF[18] = 665.0 ; 
	FirFilter_609_s.COEFF[19] = 698.0 ; 
	FirFilter_609_s.COEFF[20] = 731.0 ; 
	FirFilter_609_s.COEFF[21] = 764.0 ; 
	FirFilter_609_s.COEFF[22] = 797.0 ; 
	FirFilter_609_s.COEFF[23] = 830.0 ; 
	FirFilter_609_s.COEFF[24] = 863.0 ; 
	FirFilter_609_s.COEFF[25] = 896.0 ; 
	FirFilter_609_s.COEFF[26] = 929.0 ; 
	FirFilter_609_s.COEFF[27] = 962.0 ; 
	FirFilter_609_s.COEFF[28] = 995.0 ; 
	FirFilter_609_s.COEFF[29] = 1028.0 ; 
	FirFilter_609_s.COEFF[30] = 1061.0 ; 
	FirFilter_609_s.COEFF[31] = 1094.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar95, 0,  < , 6, streamItVar95++) {
			pop_void(&SplitJoin272_FirFilter_Fiss_672_706_split[6]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin272_FirFilter_Fiss_672_706_split[6], i) * FirFilter_609_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin272_FirFilter_Fiss_672_706_split[6]) ; 
		push_float(&SplitJoin272_FirFilter_Fiss_672_706_join[6], sum) ; 
 {
		pop_void(&SplitJoin272_FirFilter_Fiss_672_706_split[6]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_610
	 {
	FirFilter_610_s.COEFF[0] = 71.0 ; 
	FirFilter_610_s.COEFF[1] = 104.0 ; 
	FirFilter_610_s.COEFF[2] = 137.0 ; 
	FirFilter_610_s.COEFF[3] = 170.0 ; 
	FirFilter_610_s.COEFF[4] = 203.0 ; 
	FirFilter_610_s.COEFF[5] = 236.0 ; 
	FirFilter_610_s.COEFF[6] = 269.0 ; 
	FirFilter_610_s.COEFF[7] = 302.0 ; 
	FirFilter_610_s.COEFF[8] = 335.0 ; 
	FirFilter_610_s.COEFF[9] = 368.0 ; 
	FirFilter_610_s.COEFF[10] = 401.0 ; 
	FirFilter_610_s.COEFF[11] = 434.0 ; 
	FirFilter_610_s.COEFF[12] = 467.0 ; 
	FirFilter_610_s.COEFF[13] = 500.0 ; 
	FirFilter_610_s.COEFF[14] = 533.0 ; 
	FirFilter_610_s.COEFF[15] = 566.0 ; 
	FirFilter_610_s.COEFF[16] = 599.0 ; 
	FirFilter_610_s.COEFF[17] = 632.0 ; 
	FirFilter_610_s.COEFF[18] = 665.0 ; 
	FirFilter_610_s.COEFF[19] = 698.0 ; 
	FirFilter_610_s.COEFF[20] = 731.0 ; 
	FirFilter_610_s.COEFF[21] = 764.0 ; 
	FirFilter_610_s.COEFF[22] = 797.0 ; 
	FirFilter_610_s.COEFF[23] = 830.0 ; 
	FirFilter_610_s.COEFF[24] = 863.0 ; 
	FirFilter_610_s.COEFF[25] = 896.0 ; 
	FirFilter_610_s.COEFF[26] = 929.0 ; 
	FirFilter_610_s.COEFF[27] = 962.0 ; 
	FirFilter_610_s.COEFF[28] = 995.0 ; 
	FirFilter_610_s.COEFF[29] = 1028.0 ; 
	FirFilter_610_s.COEFF[30] = 1061.0 ; 
	FirFilter_610_s.COEFF[31] = 1094.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar96, 0,  < , 7, streamItVar96++) {
			pop_void(&SplitJoin272_FirFilter_Fiss_672_706_split[7]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin272_FirFilter_Fiss_672_706_split[7], i) * FirFilter_610_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin272_FirFilter_Fiss_672_706_split[7]) ; 
		push_float(&SplitJoin272_FirFilter_Fiss_672_706_join[7], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_602
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_602DownSamp_285, pop_float(&SplitJoin272_FirFilter_Fiss_672_706_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DownSamp_285
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		push_float(&DownSamp_285UpSamp_286, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_602DownSamp_285)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_602DownSamp_285) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: UpSamp_286
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++) {
		push_float(&UpSamp_286WEIGHTED_ROUND_ROBIN_Splitter_611, pop_float(&DownSamp_285UpSamp_286)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			push_float(&UpSamp_286WEIGHTED_ROUND_ROBIN_Splitter_611, 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_611
	FOR(uint32_t, __iter_init_, 0, <, 33, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&SplitJoin274_Delay_N_Fiss_673_707_split[__iter_], pop_float(&UpSamp_286WEIGHTED_ROUND_ROBIN_Splitter_611));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_613
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin274_Delay_N_Fiss_673_707_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_614
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin274_Delay_N_Fiss_673_707_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_615
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin274_Delay_N_Fiss_673_707_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_616
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin274_Delay_N_Fiss_673_707_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_617
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin274_Delay_N_Fiss_673_707_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_618
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin274_Delay_N_Fiss_673_707_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_619
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin274_Delay_N_Fiss_673_707_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_620
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin274_Delay_N_Fiss_673_707_join[7], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_612
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_612DUPLICATE_Splitter_621, pop_float(&SplitJoin274_Delay_N_Fiss_673_707_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_621
	FOR(uint32_t, __iter_init_, 0, <, 31, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_612DUPLICATE_Splitter_621);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin276_FirFilter_Fiss_674_708_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_623
	 {
	FirFilter_623_s.COEFF[0] = 56.0 ; 
	FirFilter_623_s.COEFF[1] = 64.0 ; 
	FirFilter_623_s.COEFF[2] = 72.0 ; 
	FirFilter_623_s.COEFF[3] = 80.0 ; 
	FirFilter_623_s.COEFF[4] = 88.0 ; 
	FirFilter_623_s.COEFF[5] = 96.0 ; 
	FirFilter_623_s.COEFF[6] = 104.0 ; 
	FirFilter_623_s.COEFF[7] = 112.0 ; 
	FirFilter_623_s.COEFF[8] = 120.0 ; 
	FirFilter_623_s.COEFF[9] = 128.0 ; 
	FirFilter_623_s.COEFF[10] = 136.0 ; 
	FirFilter_623_s.COEFF[11] = 144.0 ; 
	FirFilter_623_s.COEFF[12] = 152.0 ; 
	FirFilter_623_s.COEFF[13] = 160.0 ; 
	FirFilter_623_s.COEFF[14] = 168.0 ; 
	FirFilter_623_s.COEFF[15] = 176.0 ; 
	FirFilter_623_s.COEFF[16] = 184.0 ; 
	FirFilter_623_s.COEFF[17] = 192.0 ; 
	FirFilter_623_s.COEFF[18] = 200.0 ; 
	FirFilter_623_s.COEFF[19] = 208.0 ; 
	FirFilter_623_s.COEFF[20] = 216.0 ; 
	FirFilter_623_s.COEFF[21] = 224.0 ; 
	FirFilter_623_s.COEFF[22] = 232.0 ; 
	FirFilter_623_s.COEFF[23] = 240.0 ; 
	FirFilter_623_s.COEFF[24] = 248.0 ; 
	FirFilter_623_s.COEFF[25] = 256.0 ; 
	FirFilter_623_s.COEFF[26] = 264.0 ; 
	FirFilter_623_s.COEFF[27] = 272.0 ; 
	FirFilter_623_s.COEFF[28] = 280.0 ; 
	FirFilter_623_s.COEFF[29] = 288.0 ; 
	FirFilter_623_s.COEFF[30] = 296.0 ; 
	FirFilter_623_s.COEFF[31] = 304.0 ; 
}
//--------------------------------
// --- init: FirFilter_624
	 {
	FirFilter_624_s.COEFF[0] = 56.0 ; 
	FirFilter_624_s.COEFF[1] = 64.0 ; 
	FirFilter_624_s.COEFF[2] = 72.0 ; 
	FirFilter_624_s.COEFF[3] = 80.0 ; 
	FirFilter_624_s.COEFF[4] = 88.0 ; 
	FirFilter_624_s.COEFF[5] = 96.0 ; 
	FirFilter_624_s.COEFF[6] = 104.0 ; 
	FirFilter_624_s.COEFF[7] = 112.0 ; 
	FirFilter_624_s.COEFF[8] = 120.0 ; 
	FirFilter_624_s.COEFF[9] = 128.0 ; 
	FirFilter_624_s.COEFF[10] = 136.0 ; 
	FirFilter_624_s.COEFF[11] = 144.0 ; 
	FirFilter_624_s.COEFF[12] = 152.0 ; 
	FirFilter_624_s.COEFF[13] = 160.0 ; 
	FirFilter_624_s.COEFF[14] = 168.0 ; 
	FirFilter_624_s.COEFF[15] = 176.0 ; 
	FirFilter_624_s.COEFF[16] = 184.0 ; 
	FirFilter_624_s.COEFF[17] = 192.0 ; 
	FirFilter_624_s.COEFF[18] = 200.0 ; 
	FirFilter_624_s.COEFF[19] = 208.0 ; 
	FirFilter_624_s.COEFF[20] = 216.0 ; 
	FirFilter_624_s.COEFF[21] = 224.0 ; 
	FirFilter_624_s.COEFF[22] = 232.0 ; 
	FirFilter_624_s.COEFF[23] = 240.0 ; 
	FirFilter_624_s.COEFF[24] = 248.0 ; 
	FirFilter_624_s.COEFF[25] = 256.0 ; 
	FirFilter_624_s.COEFF[26] = 264.0 ; 
	FirFilter_624_s.COEFF[27] = 272.0 ; 
	FirFilter_624_s.COEFF[28] = 280.0 ; 
	FirFilter_624_s.COEFF[29] = 288.0 ; 
	FirFilter_624_s.COEFF[30] = 296.0 ; 
	FirFilter_624_s.COEFF[31] = 304.0 ; 
}
//--------------------------------
// --- init: FirFilter_625
	 {
	FirFilter_625_s.COEFF[0] = 56.0 ; 
	FirFilter_625_s.COEFF[1] = 64.0 ; 
	FirFilter_625_s.COEFF[2] = 72.0 ; 
	FirFilter_625_s.COEFF[3] = 80.0 ; 
	FirFilter_625_s.COEFF[4] = 88.0 ; 
	FirFilter_625_s.COEFF[5] = 96.0 ; 
	FirFilter_625_s.COEFF[6] = 104.0 ; 
	FirFilter_625_s.COEFF[7] = 112.0 ; 
	FirFilter_625_s.COEFF[8] = 120.0 ; 
	FirFilter_625_s.COEFF[9] = 128.0 ; 
	FirFilter_625_s.COEFF[10] = 136.0 ; 
	FirFilter_625_s.COEFF[11] = 144.0 ; 
	FirFilter_625_s.COEFF[12] = 152.0 ; 
	FirFilter_625_s.COEFF[13] = 160.0 ; 
	FirFilter_625_s.COEFF[14] = 168.0 ; 
	FirFilter_625_s.COEFF[15] = 176.0 ; 
	FirFilter_625_s.COEFF[16] = 184.0 ; 
	FirFilter_625_s.COEFF[17] = 192.0 ; 
	FirFilter_625_s.COEFF[18] = 200.0 ; 
	FirFilter_625_s.COEFF[19] = 208.0 ; 
	FirFilter_625_s.COEFF[20] = 216.0 ; 
	FirFilter_625_s.COEFF[21] = 224.0 ; 
	FirFilter_625_s.COEFF[22] = 232.0 ; 
	FirFilter_625_s.COEFF[23] = 240.0 ; 
	FirFilter_625_s.COEFF[24] = 248.0 ; 
	FirFilter_625_s.COEFF[25] = 256.0 ; 
	FirFilter_625_s.COEFF[26] = 264.0 ; 
	FirFilter_625_s.COEFF[27] = 272.0 ; 
	FirFilter_625_s.COEFF[28] = 280.0 ; 
	FirFilter_625_s.COEFF[29] = 288.0 ; 
	FirFilter_625_s.COEFF[30] = 296.0 ; 
	FirFilter_625_s.COEFF[31] = 304.0 ; 
}
//--------------------------------
// --- init: FirFilter_626
	 {
	FirFilter_626_s.COEFF[0] = 56.0 ; 
	FirFilter_626_s.COEFF[1] = 64.0 ; 
	FirFilter_626_s.COEFF[2] = 72.0 ; 
	FirFilter_626_s.COEFF[3] = 80.0 ; 
	FirFilter_626_s.COEFF[4] = 88.0 ; 
	FirFilter_626_s.COEFF[5] = 96.0 ; 
	FirFilter_626_s.COEFF[6] = 104.0 ; 
	FirFilter_626_s.COEFF[7] = 112.0 ; 
	FirFilter_626_s.COEFF[8] = 120.0 ; 
	FirFilter_626_s.COEFF[9] = 128.0 ; 
	FirFilter_626_s.COEFF[10] = 136.0 ; 
	FirFilter_626_s.COEFF[11] = 144.0 ; 
	FirFilter_626_s.COEFF[12] = 152.0 ; 
	FirFilter_626_s.COEFF[13] = 160.0 ; 
	FirFilter_626_s.COEFF[14] = 168.0 ; 
	FirFilter_626_s.COEFF[15] = 176.0 ; 
	FirFilter_626_s.COEFF[16] = 184.0 ; 
	FirFilter_626_s.COEFF[17] = 192.0 ; 
	FirFilter_626_s.COEFF[18] = 200.0 ; 
	FirFilter_626_s.COEFF[19] = 208.0 ; 
	FirFilter_626_s.COEFF[20] = 216.0 ; 
	FirFilter_626_s.COEFF[21] = 224.0 ; 
	FirFilter_626_s.COEFF[22] = 232.0 ; 
	FirFilter_626_s.COEFF[23] = 240.0 ; 
	FirFilter_626_s.COEFF[24] = 248.0 ; 
	FirFilter_626_s.COEFF[25] = 256.0 ; 
	FirFilter_626_s.COEFF[26] = 264.0 ; 
	FirFilter_626_s.COEFF[27] = 272.0 ; 
	FirFilter_626_s.COEFF[28] = 280.0 ; 
	FirFilter_626_s.COEFF[29] = 288.0 ; 
	FirFilter_626_s.COEFF[30] = 296.0 ; 
	FirFilter_626_s.COEFF[31] = 304.0 ; 
}
//--------------------------------
// --- init: FirFilter_627
	 {
	FirFilter_627_s.COEFF[0] = 56.0 ; 
	FirFilter_627_s.COEFF[1] = 64.0 ; 
	FirFilter_627_s.COEFF[2] = 72.0 ; 
	FirFilter_627_s.COEFF[3] = 80.0 ; 
	FirFilter_627_s.COEFF[4] = 88.0 ; 
	FirFilter_627_s.COEFF[5] = 96.0 ; 
	FirFilter_627_s.COEFF[6] = 104.0 ; 
	FirFilter_627_s.COEFF[7] = 112.0 ; 
	FirFilter_627_s.COEFF[8] = 120.0 ; 
	FirFilter_627_s.COEFF[9] = 128.0 ; 
	FirFilter_627_s.COEFF[10] = 136.0 ; 
	FirFilter_627_s.COEFF[11] = 144.0 ; 
	FirFilter_627_s.COEFF[12] = 152.0 ; 
	FirFilter_627_s.COEFF[13] = 160.0 ; 
	FirFilter_627_s.COEFF[14] = 168.0 ; 
	FirFilter_627_s.COEFF[15] = 176.0 ; 
	FirFilter_627_s.COEFF[16] = 184.0 ; 
	FirFilter_627_s.COEFF[17] = 192.0 ; 
	FirFilter_627_s.COEFF[18] = 200.0 ; 
	FirFilter_627_s.COEFF[19] = 208.0 ; 
	FirFilter_627_s.COEFF[20] = 216.0 ; 
	FirFilter_627_s.COEFF[21] = 224.0 ; 
	FirFilter_627_s.COEFF[22] = 232.0 ; 
	FirFilter_627_s.COEFF[23] = 240.0 ; 
	FirFilter_627_s.COEFF[24] = 248.0 ; 
	FirFilter_627_s.COEFF[25] = 256.0 ; 
	FirFilter_627_s.COEFF[26] = 264.0 ; 
	FirFilter_627_s.COEFF[27] = 272.0 ; 
	FirFilter_627_s.COEFF[28] = 280.0 ; 
	FirFilter_627_s.COEFF[29] = 288.0 ; 
	FirFilter_627_s.COEFF[30] = 296.0 ; 
	FirFilter_627_s.COEFF[31] = 304.0 ; 
}
//--------------------------------
// --- init: FirFilter_628
	 {
	FirFilter_628_s.COEFF[0] = 56.0 ; 
	FirFilter_628_s.COEFF[1] = 64.0 ; 
	FirFilter_628_s.COEFF[2] = 72.0 ; 
	FirFilter_628_s.COEFF[3] = 80.0 ; 
	FirFilter_628_s.COEFF[4] = 88.0 ; 
	FirFilter_628_s.COEFF[5] = 96.0 ; 
	FirFilter_628_s.COEFF[6] = 104.0 ; 
	FirFilter_628_s.COEFF[7] = 112.0 ; 
	FirFilter_628_s.COEFF[8] = 120.0 ; 
	FirFilter_628_s.COEFF[9] = 128.0 ; 
	FirFilter_628_s.COEFF[10] = 136.0 ; 
	FirFilter_628_s.COEFF[11] = 144.0 ; 
	FirFilter_628_s.COEFF[12] = 152.0 ; 
	FirFilter_628_s.COEFF[13] = 160.0 ; 
	FirFilter_628_s.COEFF[14] = 168.0 ; 
	FirFilter_628_s.COEFF[15] = 176.0 ; 
	FirFilter_628_s.COEFF[16] = 184.0 ; 
	FirFilter_628_s.COEFF[17] = 192.0 ; 
	FirFilter_628_s.COEFF[18] = 200.0 ; 
	FirFilter_628_s.COEFF[19] = 208.0 ; 
	FirFilter_628_s.COEFF[20] = 216.0 ; 
	FirFilter_628_s.COEFF[21] = 224.0 ; 
	FirFilter_628_s.COEFF[22] = 232.0 ; 
	FirFilter_628_s.COEFF[23] = 240.0 ; 
	FirFilter_628_s.COEFF[24] = 248.0 ; 
	FirFilter_628_s.COEFF[25] = 256.0 ; 
	FirFilter_628_s.COEFF[26] = 264.0 ; 
	FirFilter_628_s.COEFF[27] = 272.0 ; 
	FirFilter_628_s.COEFF[28] = 280.0 ; 
	FirFilter_628_s.COEFF[29] = 288.0 ; 
	FirFilter_628_s.COEFF[30] = 296.0 ; 
	FirFilter_628_s.COEFF[31] = 304.0 ; 
}
//--------------------------------
// --- init: FirFilter_629
	 {
	FirFilter_629_s.COEFF[0] = 56.0 ; 
	FirFilter_629_s.COEFF[1] = 64.0 ; 
	FirFilter_629_s.COEFF[2] = 72.0 ; 
	FirFilter_629_s.COEFF[3] = 80.0 ; 
	FirFilter_629_s.COEFF[4] = 88.0 ; 
	FirFilter_629_s.COEFF[5] = 96.0 ; 
	FirFilter_629_s.COEFF[6] = 104.0 ; 
	FirFilter_629_s.COEFF[7] = 112.0 ; 
	FirFilter_629_s.COEFF[8] = 120.0 ; 
	FirFilter_629_s.COEFF[9] = 128.0 ; 
	FirFilter_629_s.COEFF[10] = 136.0 ; 
	FirFilter_629_s.COEFF[11] = 144.0 ; 
	FirFilter_629_s.COEFF[12] = 152.0 ; 
	FirFilter_629_s.COEFF[13] = 160.0 ; 
	FirFilter_629_s.COEFF[14] = 168.0 ; 
	FirFilter_629_s.COEFF[15] = 176.0 ; 
	FirFilter_629_s.COEFF[16] = 184.0 ; 
	FirFilter_629_s.COEFF[17] = 192.0 ; 
	FirFilter_629_s.COEFF[18] = 200.0 ; 
	FirFilter_629_s.COEFF[19] = 208.0 ; 
	FirFilter_629_s.COEFF[20] = 216.0 ; 
	FirFilter_629_s.COEFF[21] = 224.0 ; 
	FirFilter_629_s.COEFF[22] = 232.0 ; 
	FirFilter_629_s.COEFF[23] = 240.0 ; 
	FirFilter_629_s.COEFF[24] = 248.0 ; 
	FirFilter_629_s.COEFF[25] = 256.0 ; 
	FirFilter_629_s.COEFF[26] = 264.0 ; 
	FirFilter_629_s.COEFF[27] = 272.0 ; 
	FirFilter_629_s.COEFF[28] = 280.0 ; 
	FirFilter_629_s.COEFF[29] = 288.0 ; 
	FirFilter_629_s.COEFF[30] = 296.0 ; 
	FirFilter_629_s.COEFF[31] = 304.0 ; 
}
//--------------------------------
// --- init: FirFilter_630
	 {
	FirFilter_630_s.COEFF[0] = 56.0 ; 
	FirFilter_630_s.COEFF[1] = 64.0 ; 
	FirFilter_630_s.COEFF[2] = 72.0 ; 
	FirFilter_630_s.COEFF[3] = 80.0 ; 
	FirFilter_630_s.COEFF[4] = 88.0 ; 
	FirFilter_630_s.COEFF[5] = 96.0 ; 
	FirFilter_630_s.COEFF[6] = 104.0 ; 
	FirFilter_630_s.COEFF[7] = 112.0 ; 
	FirFilter_630_s.COEFF[8] = 120.0 ; 
	FirFilter_630_s.COEFF[9] = 128.0 ; 
	FirFilter_630_s.COEFF[10] = 136.0 ; 
	FirFilter_630_s.COEFF[11] = 144.0 ; 
	FirFilter_630_s.COEFF[12] = 152.0 ; 
	FirFilter_630_s.COEFF[13] = 160.0 ; 
	FirFilter_630_s.COEFF[14] = 168.0 ; 
	FirFilter_630_s.COEFF[15] = 176.0 ; 
	FirFilter_630_s.COEFF[16] = 184.0 ; 
	FirFilter_630_s.COEFF[17] = 192.0 ; 
	FirFilter_630_s.COEFF[18] = 200.0 ; 
	FirFilter_630_s.COEFF[19] = 208.0 ; 
	FirFilter_630_s.COEFF[20] = 216.0 ; 
	FirFilter_630_s.COEFF[21] = 224.0 ; 
	FirFilter_630_s.COEFF[22] = 232.0 ; 
	FirFilter_630_s.COEFF[23] = 240.0 ; 
	FirFilter_630_s.COEFF[24] = 248.0 ; 
	FirFilter_630_s.COEFF[25] = 256.0 ; 
	FirFilter_630_s.COEFF[26] = 264.0 ; 
	FirFilter_630_s.COEFF[27] = 272.0 ; 
	FirFilter_630_s.COEFF[28] = 280.0 ; 
	FirFilter_630_s.COEFF[29] = 288.0 ; 
	FirFilter_630_s.COEFF[30] = 296.0 ; 
	FirFilter_630_s.COEFF[31] = 304.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		source_231();
		DUPLICATE_Splitter_291();
			WEIGHTED_ROUND_ROBIN_Splitter_311();
				Delay_N_313();
				Delay_N_314();
				Delay_N_315();
				Delay_N_316();
				Delay_N_317();
				Delay_N_318();
				Delay_N_319();
				Delay_N_320();
			WEIGHTED_ROUND_ROBIN_Joiner_312();
			DUPLICATE_Splitter_321();
				FirFilter_323();
				FirFilter_324();
				FirFilter_325();
				FirFilter_326();
				FirFilter_327();
				FirFilter_328();
				FirFilter_329();
				FirFilter_330();
			WEIGHTED_ROUND_ROBIN_Joiner_322();
			DownSamp_236();
			UpSamp_237();
			WEIGHTED_ROUND_ROBIN_Splitter_331();
				Delay_N_333();
				Delay_N_334();
				Delay_N_335();
				Delay_N_336();
				Delay_N_337();
				Delay_N_338();
				Delay_N_339();
				Delay_N_340();
			WEIGHTED_ROUND_ROBIN_Joiner_332();
			DUPLICATE_Splitter_341();
				FirFilter_343();
				FirFilter_344();
				FirFilter_345();
				FirFilter_346();
				FirFilter_347();
				FirFilter_348();
				FirFilter_349();
				FirFilter_350();
			WEIGHTED_ROUND_ROBIN_Joiner_342();
			WEIGHTED_ROUND_ROBIN_Splitter_351();
				Delay_N_353();
				Delay_N_354();
				Delay_N_355();
				Delay_N_356();
				Delay_N_357();
				Delay_N_358();
				Delay_N_359();
				Delay_N_360();
			WEIGHTED_ROUND_ROBIN_Joiner_352();
			DUPLICATE_Splitter_361();
				FirFilter_363();
				FirFilter_364();
				FirFilter_365();
				FirFilter_366();
				FirFilter_367();
				FirFilter_368();
				FirFilter_369();
				FirFilter_370();
			WEIGHTED_ROUND_ROBIN_Joiner_362();
			DownSamp_243();
			UpSamp_244();
			WEIGHTED_ROUND_ROBIN_Splitter_371();
				Delay_N_373();
				Delay_N_374();
				Delay_N_375();
				Delay_N_376();
				Delay_N_377();
				Delay_N_378();
				Delay_N_379();
				Delay_N_380();
			WEIGHTED_ROUND_ROBIN_Joiner_372();
			DUPLICATE_Splitter_381();
				FirFilter_383();
				FirFilter_384();
				FirFilter_385();
				FirFilter_386();
				FirFilter_387();
				FirFilter_388();
				FirFilter_389();
				FirFilter_390();
			WEIGHTED_ROUND_ROBIN_Joiner_382();
			WEIGHTED_ROUND_ROBIN_Splitter_391();
				Delay_N_393();
				Delay_N_394();
				Delay_N_395();
				Delay_N_396();
				Delay_N_397();
				Delay_N_398();
				Delay_N_399();
				Delay_N_400();
			WEIGHTED_ROUND_ROBIN_Joiner_392();
			DUPLICATE_Splitter_401();
				FirFilter_403();
				FirFilter_404();
				FirFilter_405();
				FirFilter_406();
				FirFilter_407();
				FirFilter_408();
				FirFilter_409();
				FirFilter_410();
			WEIGHTED_ROUND_ROBIN_Joiner_402();
			DownSamp_250();
			UpSamp_251();
			WEIGHTED_ROUND_ROBIN_Splitter_411();
				Delay_N_413();
				Delay_N_414();
				Delay_N_415();
				Delay_N_416();
				Delay_N_417();
				Delay_N_418();
				Delay_N_419();
				Delay_N_420();
			WEIGHTED_ROUND_ROBIN_Joiner_412();
			DUPLICATE_Splitter_421();
				FirFilter_423();
				FirFilter_424();
				FirFilter_425();
				FirFilter_426();
				FirFilter_427();
				FirFilter_428();
				FirFilter_429();
				FirFilter_430();
			WEIGHTED_ROUND_ROBIN_Joiner_422();
			WEIGHTED_ROUND_ROBIN_Splitter_431();
				Delay_N_433();
				Delay_N_434();
				Delay_N_435();
				Delay_N_436();
				Delay_N_437();
				Delay_N_438();
				Delay_N_439();
				Delay_N_440();
			WEIGHTED_ROUND_ROBIN_Joiner_432();
			DUPLICATE_Splitter_441();
				FirFilter_443();
				FirFilter_444();
				FirFilter_445();
				FirFilter_446();
				FirFilter_447();
				FirFilter_448();
				FirFilter_449();
				FirFilter_450();
			WEIGHTED_ROUND_ROBIN_Joiner_442();
			DownSamp_257();
			UpSamp_258();
			WEIGHTED_ROUND_ROBIN_Splitter_451();
				Delay_N_453();
				Delay_N_454();
				Delay_N_455();
				Delay_N_456();
				Delay_N_457();
				Delay_N_458();
				Delay_N_459();
				Delay_N_460();
			WEIGHTED_ROUND_ROBIN_Joiner_452();
			DUPLICATE_Splitter_461();
				FirFilter_463();
				FirFilter_464();
				FirFilter_465();
				FirFilter_466();
				FirFilter_467();
				FirFilter_468();
				FirFilter_469();
				FirFilter_470();
			WEIGHTED_ROUND_ROBIN_Joiner_462();
			WEIGHTED_ROUND_ROBIN_Splitter_471();
				Delay_N_473();
				Delay_N_474();
				Delay_N_475();
				Delay_N_476();
				Delay_N_477();
				Delay_N_478();
				Delay_N_479();
				Delay_N_480();
			WEIGHTED_ROUND_ROBIN_Joiner_472();
			DUPLICATE_Splitter_481();
				FirFilter_483();
				FirFilter_484();
				FirFilter_485();
				FirFilter_486();
				FirFilter_487();
				FirFilter_488();
				FirFilter_489();
				FirFilter_490();
			WEIGHTED_ROUND_ROBIN_Joiner_482();
			DownSamp_264();
			UpSamp_265();
			WEIGHTED_ROUND_ROBIN_Splitter_491();
				Delay_N_493();
				Delay_N_494();
				Delay_N_495();
				Delay_N_496();
				Delay_N_497();
				Delay_N_498();
				Delay_N_499();
				Delay_N_500();
			WEIGHTED_ROUND_ROBIN_Joiner_492();
			DUPLICATE_Splitter_501();
				FirFilter_503();
				FirFilter_504();
				FirFilter_505();
				FirFilter_506();
				FirFilter_507();
				FirFilter_508();
				FirFilter_509();
				FirFilter_510();
			WEIGHTED_ROUND_ROBIN_Joiner_502();
			WEIGHTED_ROUND_ROBIN_Splitter_511();
				Delay_N_513();
				Delay_N_514();
				Delay_N_515();
				Delay_N_516();
				Delay_N_517();
				Delay_N_518();
				Delay_N_519();
				Delay_N_520();
			WEIGHTED_ROUND_ROBIN_Joiner_512();
			DUPLICATE_Splitter_521();
				FirFilter_523();
				FirFilter_524();
				FirFilter_525();
				FirFilter_526();
				FirFilter_527();
				FirFilter_528();
				FirFilter_529();
				FirFilter_530();
			WEIGHTED_ROUND_ROBIN_Joiner_522();
			DownSamp_271();
			UpSamp_272();
			WEIGHTED_ROUND_ROBIN_Splitter_531();
				Delay_N_533();
				Delay_N_534();
				Delay_N_535();
				Delay_N_536();
				Delay_N_537();
				Delay_N_538();
				Delay_N_539();
				Delay_N_540();
			WEIGHTED_ROUND_ROBIN_Joiner_532();
			DUPLICATE_Splitter_541();
				FirFilter_543();
				FirFilter_544();
				FirFilter_545();
				FirFilter_546();
				FirFilter_547();
				FirFilter_548();
				FirFilter_549();
				FirFilter_550();
			WEIGHTED_ROUND_ROBIN_Joiner_542();
			WEIGHTED_ROUND_ROBIN_Splitter_551();
				Delay_N_553();
				Delay_N_554();
				Delay_N_555();
				Delay_N_556();
				Delay_N_557();
				Delay_N_558();
				Delay_N_559();
				Delay_N_560();
			WEIGHTED_ROUND_ROBIN_Joiner_552();
			DUPLICATE_Splitter_561();
				FirFilter_563();
				FirFilter_564();
				FirFilter_565();
				FirFilter_566();
				FirFilter_567();
				FirFilter_568();
				FirFilter_569();
				FirFilter_570();
			WEIGHTED_ROUND_ROBIN_Joiner_562();
			DownSamp_278();
			UpSamp_279();
			WEIGHTED_ROUND_ROBIN_Splitter_571();
				Delay_N_573();
				Delay_N_574();
				Delay_N_575();
				Delay_N_576();
				Delay_N_577();
				Delay_N_578();
				Delay_N_579();
				Delay_N_580();
			WEIGHTED_ROUND_ROBIN_Joiner_572();
			DUPLICATE_Splitter_581();
				FirFilter_583();
				FirFilter_584();
				FirFilter_585();
				FirFilter_586();
				FirFilter_587();
				FirFilter_588();
				FirFilter_589();
				FirFilter_590();
			WEIGHTED_ROUND_ROBIN_Joiner_582();
			WEIGHTED_ROUND_ROBIN_Splitter_591();
				Delay_N_593();
				Delay_N_594();
				Delay_N_595();
				Delay_N_596();
				Delay_N_597();
				Delay_N_598();
				Delay_N_599();
				Delay_N_600();
			WEIGHTED_ROUND_ROBIN_Joiner_592();
			DUPLICATE_Splitter_601();
				FirFilter_603();
				FirFilter_604();
				FirFilter_605();
				FirFilter_606();
				FirFilter_607();
				FirFilter_608();
				FirFilter_609();
				FirFilter_610();
			WEIGHTED_ROUND_ROBIN_Joiner_602();
			DownSamp_285();
			UpSamp_286();
			WEIGHTED_ROUND_ROBIN_Splitter_611();
				Delay_N_613();
				Delay_N_614();
				Delay_N_615();
				Delay_N_616();
				Delay_N_617();
				Delay_N_618();
				Delay_N_619();
				Delay_N_620();
			WEIGHTED_ROUND_ROBIN_Joiner_612();
			DUPLICATE_Splitter_621();
				FirFilter_623();
				FirFilter_624();
				FirFilter_625();
				FirFilter_626();
				FirFilter_627();
				FirFilter_628();
				FirFilter_629();
				FirFilter_630();
			WEIGHTED_ROUND_ROBIN_Joiner_622();
		WEIGHTED_ROUND_ROBIN_Joiner_292();
		WEIGHTED_ROUND_ROBIN_Splitter_631();
			Combine_633();
			Combine_634();
			Combine_635();
			Combine_636();
			Combine_637();
			Combine_638();
			Combine_639();
			Combine_640();
		WEIGHTED_ROUND_ROBIN_Joiner_632();
		sink_290();
	ENDFOR
	return EXIT_SUCCESS;
}
