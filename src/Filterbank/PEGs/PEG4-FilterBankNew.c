#include "PEG4-FilterBankNew.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3207DUPLICATE_Splitter_3212;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3063DUPLICATE_Splitter_3068;
buffer_float_t SplitJoin139_FirFilter_Fiss_3253_3287_split[4];
buffer_float_t DownSamp_2951UpSamp_2952;
buffer_float_t SplitJoin4_FirFilter_Fiss_3226_3261_join[4];
buffer_float_t SplitJoin55_FirFilter_Fiss_3237_3271_split[4];
buffer_float_t SplitJoin6_Delay_N_Fiss_3227_3262_split[4];
buffer_float_t SplitJoin51_FirFilter_Fiss_3235_3269_join[4];
buffer_float_t SplitJoin74_Delay_N_Fiss_3240_3274_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3129DownSamp_2979;
buffer_float_t UpSamp_2994WEIGHTED_ROUND_ROBIN_Splitter_3182;
buffer_float_t SplitJoin154_Delay_N_Fiss_3254_3288_join[4];
buffer_float_t SplitJoin72_FirFilter_Fiss_3239_3273_join[4];
buffer_float_t SplitJoin76_FirFilter_Fiss_3241_3275_join[4];
buffer_float_t SplitJoin28_Delay_N_Fiss_3230_3264_split[4];
buffer_float_t DownSamp_3000UpSamp_3001;
buffer_float_t SplitJoin93_FirFilter_Fiss_3243_3277_join[4];
buffer_float_t SplitJoin160_FirFilter_Fiss_3257_3291_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3081DownSamp_2965;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3183DUPLICATE_Splitter_3188;
buffer_float_t SplitJoin8_FirFilter_Fiss_3228_3263_join[4];
buffer_float_t source_2946DUPLICATE_Splitter_3006;
buffer_float_t SplitJoin10_Combine_Fiss_3229_3292_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3027DUPLICATE_Splitter_3032;
buffer_float_t DownSamp_2993UpSamp_2994;
buffer_float_t SplitJoin74_Delay_N_Fiss_3240_3274_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3105DownSamp_2972;
buffer_float_t UpSamp_2973WEIGHTED_ROUND_ROBIN_Splitter_3110;
buffer_float_t SplitJoin70_Delay_N_Fiss_3238_3272_split[4];
buffer_float_t SplitJoin95_Delay_N_Fiss_3244_3278_join[4];
buffer_float_t SplitJoin156_FirFilter_Fiss_3255_3289_join[4];
buffer_float_t SplitJoin112_Delay_N_Fiss_3246_3280_split[4];
buffer_float_t SplitJoin53_Delay_N_Fiss_3236_3270_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3075DUPLICATE_Splitter_3080;
buffer_float_t UpSamp_2987WEIGHTED_ROUND_ROBIN_Splitter_3158;
buffer_float_t SplitJoin49_Delay_N_Fiss_3234_3268_join[4];
buffer_float_t SplitJoin49_Delay_N_Fiss_3234_3268_split[4];
buffer_float_t SplitJoin158_Delay_N_Fiss_3256_3290_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3195DUPLICATE_Splitter_3200;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3219sink_3005;
buffer_float_t SplitJoin2_Delay_N_Fiss_3225_3260_join[4];
buffer_float_t DownSamp_2986UpSamp_2987;
buffer_float_t SplitJoin30_FirFilter_Fiss_3231_3265_join[4];
buffer_float_t SplitJoin32_Delay_N_Fiss_3232_3266_join[4];
buffer_float_t SplitJoin53_Delay_N_Fiss_3236_3270_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3171DUPLICATE_Splitter_3176;
buffer_float_t SplitJoin91_Delay_N_Fiss_3242_3276_join[4];
buffer_float_t UpSamp_3001WEIGHTED_ROUND_ROBIN_Splitter_3206;
buffer_float_t SplitJoin10_Combine_Fiss_3229_3292_split[4];
buffer_float_t UpSamp_2952WEIGHTED_ROUND_ROBIN_Splitter_3038;
buffer_float_t SplitJoin6_Delay_N_Fiss_3227_3262_join[4];
buffer_float_t SplitJoin135_FirFilter_Fiss_3251_3285_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3201DownSamp_3000;
buffer_float_t SplitJoin8_FirFilter_Fiss_3228_3263_split[4];
buffer_float_t SplitJoin30_FirFilter_Fiss_3231_3265_split[4];
buffer_float_t SplitJoin2_Delay_N_Fiss_3225_3260_split[4];
buffer_float_t DownSamp_2979UpSamp_2980;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3039DUPLICATE_Splitter_3044;
buffer_float_t SplitJoin154_Delay_N_Fiss_3254_3288_split[4];
buffer_float_t SplitJoin112_Delay_N_Fiss_3246_3280_join[4];
buffer_float_t SplitJoin158_Delay_N_Fiss_3256_3290_join[4];
buffer_float_t SplitJoin97_FirFilter_Fiss_3245_3279_split[4];
buffer_float_t SplitJoin114_FirFilter_Fiss_3247_3281_join[4];
buffer_float_t SplitJoin118_FirFilter_Fiss_3249_3283_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3087DUPLICATE_Splitter_3092;
buffer_float_t SplitJoin4_FirFilter_Fiss_3226_3261_split[4];
buffer_float_t SplitJoin137_Delay_N_Fiss_3252_3286_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3159DUPLICATE_Splitter_3164;
buffer_float_t SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[8];
buffer_float_t SplitJoin70_Delay_N_Fiss_3238_3272_join[4];
buffer_float_t SplitJoin118_FirFilter_Fiss_3249_3283_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3099DUPLICATE_Splitter_3104;
buffer_float_t SplitJoin160_FirFilter_Fiss_3257_3291_join[4];
buffer_float_t SplitJoin32_Delay_N_Fiss_3232_3266_split[4];
buffer_float_t SplitJoin139_FirFilter_Fiss_3253_3287_join[4];
buffer_float_t UpSamp_2980WEIGHTED_ROUND_ROBIN_Splitter_3134;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3177DownSamp_2993;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3051DUPLICATE_Splitter_3056;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3007WEIGHTED_ROUND_ROBIN_Splitter_3218;
buffer_float_t SplitJoin95_Delay_N_Fiss_3244_3278_split[4];
buffer_float_t SplitJoin156_FirFilter_Fiss_3255_3289_split[4];
buffer_float_t SplitJoin34_FirFilter_Fiss_3233_3267_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3111DUPLICATE_Splitter_3116;
buffer_float_t SplitJoin133_Delay_N_Fiss_3250_3284_split[4];
buffer_float_t SplitJoin133_Delay_N_Fiss_3250_3284_join[4];
buffer_float_t DownSamp_2958UpSamp_2959;
buffer_float_t SplitJoin91_Delay_N_Fiss_3242_3276_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3153DownSamp_2986;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3033DownSamp_2951;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3147DUPLICATE_Splitter_3152;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3123DUPLICATE_Splitter_3128;
buffer_float_t SplitJoin51_FirFilter_Fiss_3235_3269_split[4];
buffer_float_t SplitJoin116_Delay_N_Fiss_3248_3282_join[4];
buffer_float_t SplitJoin135_FirFilter_Fiss_3251_3285_split[4];
buffer_float_t SplitJoin76_FirFilter_Fiss_3241_3275_split[4];
buffer_float_t SplitJoin114_FirFilter_Fiss_3247_3281_split[4];
buffer_float_t SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[8];
buffer_float_t SplitJoin97_FirFilter_Fiss_3245_3279_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3057DownSamp_2958;
buffer_float_t DownSamp_2972UpSamp_2973;
buffer_float_t DownSamp_2965UpSamp_2966;
buffer_float_t SplitJoin72_FirFilter_Fiss_3239_3273_split[4];
buffer_float_t SplitJoin55_FirFilter_Fiss_3237_3271_join[4];
buffer_float_t SplitJoin137_Delay_N_Fiss_3252_3286_join[4];
buffer_float_t UpSamp_2966WEIGHTED_ROUND_ROBIN_Splitter_3086;
buffer_float_t UpSamp_2959WEIGHTED_ROUND_ROBIN_Splitter_3062;
buffer_float_t SplitJoin93_FirFilter_Fiss_3243_3277_split[4];
buffer_float_t SplitJoin34_FirFilter_Fiss_3233_3267_join[4];
buffer_float_t SplitJoin28_Delay_N_Fiss_3230_3264_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3135DUPLICATE_Splitter_3140;
buffer_float_t SplitJoin116_Delay_N_Fiss_3248_3282_split[4];


source_2946_t source_2946_s;
FirFilter_3034_t FirFilter_3034_s;
FirFilter_3034_t FirFilter_3035_s;
FirFilter_3034_t FirFilter_3036_s;
FirFilter_3034_t FirFilter_3037_s;
FirFilter_3034_t FirFilter_3046_s;
FirFilter_3034_t FirFilter_3047_s;
FirFilter_3034_t FirFilter_3048_s;
FirFilter_3034_t FirFilter_3049_s;
FirFilter_3034_t FirFilter_3058_s;
FirFilter_3034_t FirFilter_3059_s;
FirFilter_3034_t FirFilter_3060_s;
FirFilter_3034_t FirFilter_3061_s;
FirFilter_3034_t FirFilter_3070_s;
FirFilter_3034_t FirFilter_3071_s;
FirFilter_3034_t FirFilter_3072_s;
FirFilter_3034_t FirFilter_3073_s;
FirFilter_3034_t FirFilter_3082_s;
FirFilter_3034_t FirFilter_3083_s;
FirFilter_3034_t FirFilter_3084_s;
FirFilter_3034_t FirFilter_3085_s;
FirFilter_3034_t FirFilter_3094_s;
FirFilter_3034_t FirFilter_3095_s;
FirFilter_3034_t FirFilter_3096_s;
FirFilter_3034_t FirFilter_3097_s;
FirFilter_3034_t FirFilter_3106_s;
FirFilter_3034_t FirFilter_3107_s;
FirFilter_3034_t FirFilter_3108_s;
FirFilter_3034_t FirFilter_3109_s;
FirFilter_3034_t FirFilter_3118_s;
FirFilter_3034_t FirFilter_3119_s;
FirFilter_3034_t FirFilter_3120_s;
FirFilter_3034_t FirFilter_3121_s;
FirFilter_3034_t FirFilter_3130_s;
FirFilter_3034_t FirFilter_3131_s;
FirFilter_3034_t FirFilter_3132_s;
FirFilter_3034_t FirFilter_3133_s;
FirFilter_3034_t FirFilter_3142_s;
FirFilter_3034_t FirFilter_3143_s;
FirFilter_3034_t FirFilter_3144_s;
FirFilter_3034_t FirFilter_3145_s;
FirFilter_3034_t FirFilter_3154_s;
FirFilter_3034_t FirFilter_3155_s;
FirFilter_3034_t FirFilter_3156_s;
FirFilter_3034_t FirFilter_3157_s;
FirFilter_3034_t FirFilter_3166_s;
FirFilter_3034_t FirFilter_3167_s;
FirFilter_3034_t FirFilter_3168_s;
FirFilter_3034_t FirFilter_3169_s;
FirFilter_3034_t FirFilter_3178_s;
FirFilter_3034_t FirFilter_3179_s;
FirFilter_3034_t FirFilter_3180_s;
FirFilter_3034_t FirFilter_3181_s;
FirFilter_3034_t FirFilter_3190_s;
FirFilter_3034_t FirFilter_3191_s;
FirFilter_3034_t FirFilter_3192_s;
FirFilter_3034_t FirFilter_3193_s;
FirFilter_3034_t FirFilter_3202_s;
FirFilter_3034_t FirFilter_3203_s;
FirFilter_3034_t FirFilter_3204_s;
FirFilter_3034_t FirFilter_3205_s;
FirFilter_3034_t FirFilter_3214_s;
FirFilter_3034_t FirFilter_3215_s;
FirFilter_3034_t FirFilter_3216_s;
FirFilter_3034_t FirFilter_3217_s;

void source(buffer_float_t *chanout) {
		push_float(&(*chanout), source_2946_s.current) ; 
		if((source_2946_s.current > 1000.0)) {
			source_2946_s.current = 0.0 ; 
		}
		else {
			source_2946_s.current = (source_2946_s.current + 1.0) ; 
		}
	}


void source_2946() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		source(&(source_2946DUPLICATE_Splitter_3006));
	ENDFOR
}

void Delay_N(buffer_float_t *chanin, buffer_float_t *chanout) {
	}


void Delay_N_3028() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin2_Delay_N_Fiss_3225_3260_split[0]), &(SplitJoin2_Delay_N_Fiss_3225_3260_join[0]));
	ENDFOR
}

void Delay_N_3029() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin2_Delay_N_Fiss_3225_3260_split[1]), &(SplitJoin2_Delay_N_Fiss_3225_3260_join[1]));
	ENDFOR
}

void Delay_N_3030() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin2_Delay_N_Fiss_3225_3260_split[2]), &(SplitJoin2_Delay_N_Fiss_3225_3260_join[2]));
	ENDFOR
}

void Delay_N_3031() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin2_Delay_N_Fiss_3225_3260_split[3]), &(SplitJoin2_Delay_N_Fiss_3225_3260_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3026() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin2_Delay_N_Fiss_3225_3260_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3027() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3027DUPLICATE_Splitter_3032, pop_float(&SplitJoin2_Delay_N_Fiss_3225_3260_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&(*chanin), i) * FirFilter_3034_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&(*chanin)) ; 
		push_float(&(*chanout), sum) ; 
 {
		FOR(int, streamItVar637, 0,  < , 3, streamItVar637++) {
			pop_void(&(*chanin)) ; 
		}
		ENDFOR
	}
	}


void FirFilter_3034() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin4_FirFilter_Fiss_3226_3261_split[0]), &(SplitJoin4_FirFilter_Fiss_3226_3261_join[0]));
	ENDFOR
}

void FirFilter_3035() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin4_FirFilter_Fiss_3226_3261_split[1]), &(SplitJoin4_FirFilter_Fiss_3226_3261_join[1]));
	ENDFOR
}

void FirFilter_3036() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin4_FirFilter_Fiss_3226_3261_split[2]), &(SplitJoin4_FirFilter_Fiss_3226_3261_join[2]));
	ENDFOR
}

void FirFilter_3037() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin4_FirFilter_Fiss_3226_3261_split[3]), &(SplitJoin4_FirFilter_Fiss_3226_3261_join[3]));
	ENDFOR
}

void DUPLICATE_Splitter_3032() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3027DUPLICATE_Splitter_3032);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin4_FirFilter_Fiss_3226_3261_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3033() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3033DownSamp_2951, pop_float(&SplitJoin4_FirFilter_Fiss_3226_3261_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp(buffer_float_t *chanin, buffer_float_t *chanout) {
	push_float(&(*chanout), pop_float(&(*chanin))) ; 
	FOR(int, i, 0,  < , 7, i++) {
		pop_float(&(*chanin)) ; 
	}
	ENDFOR
}


void DownSamp_2951() {
	DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3033DownSamp_2951), &(DownSamp_2951UpSamp_2952));
}

void UpSamp(buffer_float_t *chanin, buffer_float_t *chanout) {
	push_float(&(*chanout), pop_float(&(*chanin))) ; 
	FOR(int, i, 0,  < , 7, i++) {
		push_float(&(*chanout), 0.0) ; 
	}
	ENDFOR
}


void UpSamp_2952() {
	UpSamp(&(DownSamp_2951UpSamp_2952), &(UpSamp_2952WEIGHTED_ROUND_ROBIN_Splitter_3038));
}

void Delay_N_3040() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin6_Delay_N_Fiss_3227_3262_split[0]), &(SplitJoin6_Delay_N_Fiss_3227_3262_join[0]));
	ENDFOR
}

void Delay_N_3041() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin6_Delay_N_Fiss_3227_3262_split[1]), &(SplitJoin6_Delay_N_Fiss_3227_3262_join[1]));
	ENDFOR
}

void Delay_N_3042() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin6_Delay_N_Fiss_3227_3262_split[2]), &(SplitJoin6_Delay_N_Fiss_3227_3262_join[2]));
	ENDFOR
}

void Delay_N_3043() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin6_Delay_N_Fiss_3227_3262_split[3]), &(SplitJoin6_Delay_N_Fiss_3227_3262_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3038() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin6_Delay_N_Fiss_3227_3262_split[__iter_], pop_float(&UpSamp_2952WEIGHTED_ROUND_ROBIN_Splitter_3038));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3039() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3039DUPLICATE_Splitter_3044, pop_float(&SplitJoin6_Delay_N_Fiss_3227_3262_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3046() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin8_FirFilter_Fiss_3228_3263_split[0]), &(SplitJoin8_FirFilter_Fiss_3228_3263_join[0]));
	ENDFOR
}

void FirFilter_3047() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin8_FirFilter_Fiss_3228_3263_split[1]), &(SplitJoin8_FirFilter_Fiss_3228_3263_join[1]));
	ENDFOR
}

void FirFilter_3048() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin8_FirFilter_Fiss_3228_3263_split[2]), &(SplitJoin8_FirFilter_Fiss_3228_3263_join[2]));
	ENDFOR
}

void FirFilter_3049() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin8_FirFilter_Fiss_3228_3263_split[3]), &(SplitJoin8_FirFilter_Fiss_3228_3263_join[3]));
	ENDFOR
}

void DUPLICATE_Splitter_3044() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3039DUPLICATE_Splitter_3044);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin8_FirFilter_Fiss_3228_3263_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3045() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[0], pop_float(&SplitJoin8_FirFilter_Fiss_3228_3263_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Delay_N_3052() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin28_Delay_N_Fiss_3230_3264_split[0]), &(SplitJoin28_Delay_N_Fiss_3230_3264_join[0]));
	ENDFOR
}

void Delay_N_3053() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin28_Delay_N_Fiss_3230_3264_split[1]), &(SplitJoin28_Delay_N_Fiss_3230_3264_join[1]));
	ENDFOR
}

void Delay_N_3054() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin28_Delay_N_Fiss_3230_3264_split[2]), &(SplitJoin28_Delay_N_Fiss_3230_3264_join[2]));
	ENDFOR
}

void Delay_N_3055() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin28_Delay_N_Fiss_3230_3264_split[3]), &(SplitJoin28_Delay_N_Fiss_3230_3264_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3050() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin28_Delay_N_Fiss_3230_3264_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3051() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3051DUPLICATE_Splitter_3056, pop_float(&SplitJoin28_Delay_N_Fiss_3230_3264_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3058() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin30_FirFilter_Fiss_3231_3265_split[0]), &(SplitJoin30_FirFilter_Fiss_3231_3265_join[0]));
	ENDFOR
}

void FirFilter_3059() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin30_FirFilter_Fiss_3231_3265_split[1]), &(SplitJoin30_FirFilter_Fiss_3231_3265_join[1]));
	ENDFOR
}

void FirFilter_3060() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin30_FirFilter_Fiss_3231_3265_split[2]), &(SplitJoin30_FirFilter_Fiss_3231_3265_join[2]));
	ENDFOR
}

void FirFilter_3061() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin30_FirFilter_Fiss_3231_3265_split[3]), &(SplitJoin30_FirFilter_Fiss_3231_3265_join[3]));
	ENDFOR
}

void DUPLICATE_Splitter_3056() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3051DUPLICATE_Splitter_3056);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin30_FirFilter_Fiss_3231_3265_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3057() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3057DownSamp_2958, pop_float(&SplitJoin30_FirFilter_Fiss_3231_3265_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp_2958() {
	DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3057DownSamp_2958), &(DownSamp_2958UpSamp_2959));
}

void UpSamp_2959() {
	UpSamp(&(DownSamp_2958UpSamp_2959), &(UpSamp_2959WEIGHTED_ROUND_ROBIN_Splitter_3062));
}

void Delay_N_3064() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin32_Delay_N_Fiss_3232_3266_split[0]), &(SplitJoin32_Delay_N_Fiss_3232_3266_join[0]));
	ENDFOR
}

void Delay_N_3065() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin32_Delay_N_Fiss_3232_3266_split[1]), &(SplitJoin32_Delay_N_Fiss_3232_3266_join[1]));
	ENDFOR
}

void Delay_N_3066() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin32_Delay_N_Fiss_3232_3266_split[2]), &(SplitJoin32_Delay_N_Fiss_3232_3266_join[2]));
	ENDFOR
}

void Delay_N_3067() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin32_Delay_N_Fiss_3232_3266_split[3]), &(SplitJoin32_Delay_N_Fiss_3232_3266_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3062() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin32_Delay_N_Fiss_3232_3266_split[__iter_], pop_float(&UpSamp_2959WEIGHTED_ROUND_ROBIN_Splitter_3062));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3063() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3063DUPLICATE_Splitter_3068, pop_float(&SplitJoin32_Delay_N_Fiss_3232_3266_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3070() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin34_FirFilter_Fiss_3233_3267_split[0]), &(SplitJoin34_FirFilter_Fiss_3233_3267_join[0]));
	ENDFOR
}

void FirFilter_3071() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin34_FirFilter_Fiss_3233_3267_split[1]), &(SplitJoin34_FirFilter_Fiss_3233_3267_join[1]));
	ENDFOR
}

void FirFilter_3072() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin34_FirFilter_Fiss_3233_3267_split[2]), &(SplitJoin34_FirFilter_Fiss_3233_3267_join[2]));
	ENDFOR
}

void FirFilter_3073() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin34_FirFilter_Fiss_3233_3267_split[3]), &(SplitJoin34_FirFilter_Fiss_3233_3267_join[3]));
	ENDFOR
}

void DUPLICATE_Splitter_3068() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3063DUPLICATE_Splitter_3068);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin34_FirFilter_Fiss_3233_3267_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3069() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[1], pop_float(&SplitJoin34_FirFilter_Fiss_3233_3267_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Delay_N_3076() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin49_Delay_N_Fiss_3234_3268_split[0]), &(SplitJoin49_Delay_N_Fiss_3234_3268_join[0]));
	ENDFOR
}

void Delay_N_3077() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin49_Delay_N_Fiss_3234_3268_split[1]), &(SplitJoin49_Delay_N_Fiss_3234_3268_join[1]));
	ENDFOR
}

void Delay_N_3078() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin49_Delay_N_Fiss_3234_3268_split[2]), &(SplitJoin49_Delay_N_Fiss_3234_3268_join[2]));
	ENDFOR
}

void Delay_N_3079() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin49_Delay_N_Fiss_3234_3268_split[3]), &(SplitJoin49_Delay_N_Fiss_3234_3268_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3074() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin49_Delay_N_Fiss_3234_3268_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[2]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3075() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3075DUPLICATE_Splitter_3080, pop_float(&SplitJoin49_Delay_N_Fiss_3234_3268_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3082() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin51_FirFilter_Fiss_3235_3269_split[0]), &(SplitJoin51_FirFilter_Fiss_3235_3269_join[0]));
	ENDFOR
}

void FirFilter_3083() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin51_FirFilter_Fiss_3235_3269_split[1]), &(SplitJoin51_FirFilter_Fiss_3235_3269_join[1]));
	ENDFOR
}

void FirFilter_3084() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin51_FirFilter_Fiss_3235_3269_split[2]), &(SplitJoin51_FirFilter_Fiss_3235_3269_join[2]));
	ENDFOR
}

void FirFilter_3085() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin51_FirFilter_Fiss_3235_3269_split[3]), &(SplitJoin51_FirFilter_Fiss_3235_3269_join[3]));
	ENDFOR
}

void DUPLICATE_Splitter_3080() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3075DUPLICATE_Splitter_3080);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin51_FirFilter_Fiss_3235_3269_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3081() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3081DownSamp_2965, pop_float(&SplitJoin51_FirFilter_Fiss_3235_3269_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp_2965() {
	DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3081DownSamp_2965), &(DownSamp_2965UpSamp_2966));
}

void UpSamp_2966() {
	UpSamp(&(DownSamp_2965UpSamp_2966), &(UpSamp_2966WEIGHTED_ROUND_ROBIN_Splitter_3086));
}

void Delay_N_3088() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin53_Delay_N_Fiss_3236_3270_split[0]), &(SplitJoin53_Delay_N_Fiss_3236_3270_join[0]));
	ENDFOR
}

void Delay_N_3089() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin53_Delay_N_Fiss_3236_3270_split[1]), &(SplitJoin53_Delay_N_Fiss_3236_3270_join[1]));
	ENDFOR
}

void Delay_N_3090() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin53_Delay_N_Fiss_3236_3270_split[2]), &(SplitJoin53_Delay_N_Fiss_3236_3270_join[2]));
	ENDFOR
}

void Delay_N_3091() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin53_Delay_N_Fiss_3236_3270_split[3]), &(SplitJoin53_Delay_N_Fiss_3236_3270_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3086() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin53_Delay_N_Fiss_3236_3270_split[__iter_], pop_float(&UpSamp_2966WEIGHTED_ROUND_ROBIN_Splitter_3086));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3087() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3087DUPLICATE_Splitter_3092, pop_float(&SplitJoin53_Delay_N_Fiss_3236_3270_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3094() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin55_FirFilter_Fiss_3237_3271_split[0]), &(SplitJoin55_FirFilter_Fiss_3237_3271_join[0]));
	ENDFOR
}

void FirFilter_3095() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin55_FirFilter_Fiss_3237_3271_split[1]), &(SplitJoin55_FirFilter_Fiss_3237_3271_join[1]));
	ENDFOR
}

void FirFilter_3096() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin55_FirFilter_Fiss_3237_3271_split[2]), &(SplitJoin55_FirFilter_Fiss_3237_3271_join[2]));
	ENDFOR
}

void FirFilter_3097() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin55_FirFilter_Fiss_3237_3271_split[3]), &(SplitJoin55_FirFilter_Fiss_3237_3271_join[3]));
	ENDFOR
}

void DUPLICATE_Splitter_3092() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3087DUPLICATE_Splitter_3092);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin55_FirFilter_Fiss_3237_3271_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3093() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[2], pop_float(&SplitJoin55_FirFilter_Fiss_3237_3271_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Delay_N_3100() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin70_Delay_N_Fiss_3238_3272_split[0]), &(SplitJoin70_Delay_N_Fiss_3238_3272_join[0]));
	ENDFOR
}

void Delay_N_3101() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin70_Delay_N_Fiss_3238_3272_split[1]), &(SplitJoin70_Delay_N_Fiss_3238_3272_join[1]));
	ENDFOR
}

void Delay_N_3102() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin70_Delay_N_Fiss_3238_3272_split[2]), &(SplitJoin70_Delay_N_Fiss_3238_3272_join[2]));
	ENDFOR
}

void Delay_N_3103() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin70_Delay_N_Fiss_3238_3272_split[3]), &(SplitJoin70_Delay_N_Fiss_3238_3272_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3098() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin70_Delay_N_Fiss_3238_3272_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[3]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3099() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3099DUPLICATE_Splitter_3104, pop_float(&SplitJoin70_Delay_N_Fiss_3238_3272_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3106() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin72_FirFilter_Fiss_3239_3273_split[0]), &(SplitJoin72_FirFilter_Fiss_3239_3273_join[0]));
	ENDFOR
}

void FirFilter_3107() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin72_FirFilter_Fiss_3239_3273_split[1]), &(SplitJoin72_FirFilter_Fiss_3239_3273_join[1]));
	ENDFOR
}

void FirFilter_3108() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin72_FirFilter_Fiss_3239_3273_split[2]), &(SplitJoin72_FirFilter_Fiss_3239_3273_join[2]));
	ENDFOR
}

void FirFilter_3109() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin72_FirFilter_Fiss_3239_3273_split[3]), &(SplitJoin72_FirFilter_Fiss_3239_3273_join[3]));
	ENDFOR
}

void DUPLICATE_Splitter_3104() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3099DUPLICATE_Splitter_3104);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin72_FirFilter_Fiss_3239_3273_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3105() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3105DownSamp_2972, pop_float(&SplitJoin72_FirFilter_Fiss_3239_3273_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp_2972() {
	DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3105DownSamp_2972), &(DownSamp_2972UpSamp_2973));
}

void UpSamp_2973() {
	UpSamp(&(DownSamp_2972UpSamp_2973), &(UpSamp_2973WEIGHTED_ROUND_ROBIN_Splitter_3110));
}

void Delay_N_3112() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin74_Delay_N_Fiss_3240_3274_split[0]), &(SplitJoin74_Delay_N_Fiss_3240_3274_join[0]));
	ENDFOR
}

void Delay_N_3113() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin74_Delay_N_Fiss_3240_3274_split[1]), &(SplitJoin74_Delay_N_Fiss_3240_3274_join[1]));
	ENDFOR
}

void Delay_N_3114() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin74_Delay_N_Fiss_3240_3274_split[2]), &(SplitJoin74_Delay_N_Fiss_3240_3274_join[2]));
	ENDFOR
}

void Delay_N_3115() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin74_Delay_N_Fiss_3240_3274_split[3]), &(SplitJoin74_Delay_N_Fiss_3240_3274_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3110() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin74_Delay_N_Fiss_3240_3274_split[__iter_], pop_float(&UpSamp_2973WEIGHTED_ROUND_ROBIN_Splitter_3110));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3111() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3111DUPLICATE_Splitter_3116, pop_float(&SplitJoin74_Delay_N_Fiss_3240_3274_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3118() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin76_FirFilter_Fiss_3241_3275_split[0]), &(SplitJoin76_FirFilter_Fiss_3241_3275_join[0]));
	ENDFOR
}

void FirFilter_3119() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin76_FirFilter_Fiss_3241_3275_split[1]), &(SplitJoin76_FirFilter_Fiss_3241_3275_join[1]));
	ENDFOR
}

void FirFilter_3120() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin76_FirFilter_Fiss_3241_3275_split[2]), &(SplitJoin76_FirFilter_Fiss_3241_3275_join[2]));
	ENDFOR
}

void FirFilter_3121() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin76_FirFilter_Fiss_3241_3275_split[3]), &(SplitJoin76_FirFilter_Fiss_3241_3275_join[3]));
	ENDFOR
}

void DUPLICATE_Splitter_3116() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3111DUPLICATE_Splitter_3116);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin76_FirFilter_Fiss_3241_3275_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3117() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[3], pop_float(&SplitJoin76_FirFilter_Fiss_3241_3275_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Delay_N_3124() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin91_Delay_N_Fiss_3242_3276_split[0]), &(SplitJoin91_Delay_N_Fiss_3242_3276_join[0]));
	ENDFOR
}

void Delay_N_3125() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin91_Delay_N_Fiss_3242_3276_split[1]), &(SplitJoin91_Delay_N_Fiss_3242_3276_join[1]));
	ENDFOR
}

void Delay_N_3126() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin91_Delay_N_Fiss_3242_3276_split[2]), &(SplitJoin91_Delay_N_Fiss_3242_3276_join[2]));
	ENDFOR
}

void Delay_N_3127() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin91_Delay_N_Fiss_3242_3276_split[3]), &(SplitJoin91_Delay_N_Fiss_3242_3276_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3122() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin91_Delay_N_Fiss_3242_3276_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[4]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3123() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3123DUPLICATE_Splitter_3128, pop_float(&SplitJoin91_Delay_N_Fiss_3242_3276_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3130() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin93_FirFilter_Fiss_3243_3277_split[0]), &(SplitJoin93_FirFilter_Fiss_3243_3277_join[0]));
	ENDFOR
}

void FirFilter_3131() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin93_FirFilter_Fiss_3243_3277_split[1]), &(SplitJoin93_FirFilter_Fiss_3243_3277_join[1]));
	ENDFOR
}

void FirFilter_3132() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin93_FirFilter_Fiss_3243_3277_split[2]), &(SplitJoin93_FirFilter_Fiss_3243_3277_join[2]));
	ENDFOR
}

void FirFilter_3133() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin93_FirFilter_Fiss_3243_3277_split[3]), &(SplitJoin93_FirFilter_Fiss_3243_3277_join[3]));
	ENDFOR
}

void DUPLICATE_Splitter_3128() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3123DUPLICATE_Splitter_3128);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin93_FirFilter_Fiss_3243_3277_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3129() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3129DownSamp_2979, pop_float(&SplitJoin93_FirFilter_Fiss_3243_3277_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp_2979() {
	DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3129DownSamp_2979), &(DownSamp_2979UpSamp_2980));
}

void UpSamp_2980() {
	UpSamp(&(DownSamp_2979UpSamp_2980), &(UpSamp_2980WEIGHTED_ROUND_ROBIN_Splitter_3134));
}

void Delay_N_3136() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin95_Delay_N_Fiss_3244_3278_split[0]), &(SplitJoin95_Delay_N_Fiss_3244_3278_join[0]));
	ENDFOR
}

void Delay_N_3137() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin95_Delay_N_Fiss_3244_3278_split[1]), &(SplitJoin95_Delay_N_Fiss_3244_3278_join[1]));
	ENDFOR
}

void Delay_N_3138() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin95_Delay_N_Fiss_3244_3278_split[2]), &(SplitJoin95_Delay_N_Fiss_3244_3278_join[2]));
	ENDFOR
}

void Delay_N_3139() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin95_Delay_N_Fiss_3244_3278_split[3]), &(SplitJoin95_Delay_N_Fiss_3244_3278_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3134() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin95_Delay_N_Fiss_3244_3278_split[__iter_], pop_float(&UpSamp_2980WEIGHTED_ROUND_ROBIN_Splitter_3134));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3135() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3135DUPLICATE_Splitter_3140, pop_float(&SplitJoin95_Delay_N_Fiss_3244_3278_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3142() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin97_FirFilter_Fiss_3245_3279_split[0]), &(SplitJoin97_FirFilter_Fiss_3245_3279_join[0]));
	ENDFOR
}

void FirFilter_3143() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin97_FirFilter_Fiss_3245_3279_split[1]), &(SplitJoin97_FirFilter_Fiss_3245_3279_join[1]));
	ENDFOR
}

void FirFilter_3144() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin97_FirFilter_Fiss_3245_3279_split[2]), &(SplitJoin97_FirFilter_Fiss_3245_3279_join[2]));
	ENDFOR
}

void FirFilter_3145() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin97_FirFilter_Fiss_3245_3279_split[3]), &(SplitJoin97_FirFilter_Fiss_3245_3279_join[3]));
	ENDFOR
}

void DUPLICATE_Splitter_3140() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3135DUPLICATE_Splitter_3140);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin97_FirFilter_Fiss_3245_3279_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3141() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[4], pop_float(&SplitJoin97_FirFilter_Fiss_3245_3279_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Delay_N_3148() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin112_Delay_N_Fiss_3246_3280_split[0]), &(SplitJoin112_Delay_N_Fiss_3246_3280_join[0]));
	ENDFOR
}

void Delay_N_3149() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin112_Delay_N_Fiss_3246_3280_split[1]), &(SplitJoin112_Delay_N_Fiss_3246_3280_join[1]));
	ENDFOR
}

void Delay_N_3150() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin112_Delay_N_Fiss_3246_3280_split[2]), &(SplitJoin112_Delay_N_Fiss_3246_3280_join[2]));
	ENDFOR
}

void Delay_N_3151() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin112_Delay_N_Fiss_3246_3280_split[3]), &(SplitJoin112_Delay_N_Fiss_3246_3280_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3146() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin112_Delay_N_Fiss_3246_3280_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[5]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3147() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3147DUPLICATE_Splitter_3152, pop_float(&SplitJoin112_Delay_N_Fiss_3246_3280_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3154() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin114_FirFilter_Fiss_3247_3281_split[0]), &(SplitJoin114_FirFilter_Fiss_3247_3281_join[0]));
	ENDFOR
}

void FirFilter_3155() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin114_FirFilter_Fiss_3247_3281_split[1]), &(SplitJoin114_FirFilter_Fiss_3247_3281_join[1]));
	ENDFOR
}

void FirFilter_3156() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin114_FirFilter_Fiss_3247_3281_split[2]), &(SplitJoin114_FirFilter_Fiss_3247_3281_join[2]));
	ENDFOR
}

void FirFilter_3157() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin114_FirFilter_Fiss_3247_3281_split[3]), &(SplitJoin114_FirFilter_Fiss_3247_3281_join[3]));
	ENDFOR
}

void DUPLICATE_Splitter_3152() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3147DUPLICATE_Splitter_3152);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin114_FirFilter_Fiss_3247_3281_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3153() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3153DownSamp_2986, pop_float(&SplitJoin114_FirFilter_Fiss_3247_3281_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp_2986() {
	DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3153DownSamp_2986), &(DownSamp_2986UpSamp_2987));
}

void UpSamp_2987() {
	UpSamp(&(DownSamp_2986UpSamp_2987), &(UpSamp_2987WEIGHTED_ROUND_ROBIN_Splitter_3158));
}

void Delay_N_3160() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin116_Delay_N_Fiss_3248_3282_split[0]), &(SplitJoin116_Delay_N_Fiss_3248_3282_join[0]));
	ENDFOR
}

void Delay_N_3161() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin116_Delay_N_Fiss_3248_3282_split[1]), &(SplitJoin116_Delay_N_Fiss_3248_3282_join[1]));
	ENDFOR
}

void Delay_N_3162() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin116_Delay_N_Fiss_3248_3282_split[2]), &(SplitJoin116_Delay_N_Fiss_3248_3282_join[2]));
	ENDFOR
}

void Delay_N_3163() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin116_Delay_N_Fiss_3248_3282_split[3]), &(SplitJoin116_Delay_N_Fiss_3248_3282_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3158() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin116_Delay_N_Fiss_3248_3282_split[__iter_], pop_float(&UpSamp_2987WEIGHTED_ROUND_ROBIN_Splitter_3158));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3159() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3159DUPLICATE_Splitter_3164, pop_float(&SplitJoin116_Delay_N_Fiss_3248_3282_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3166() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin118_FirFilter_Fiss_3249_3283_split[0]), &(SplitJoin118_FirFilter_Fiss_3249_3283_join[0]));
	ENDFOR
}

void FirFilter_3167() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin118_FirFilter_Fiss_3249_3283_split[1]), &(SplitJoin118_FirFilter_Fiss_3249_3283_join[1]));
	ENDFOR
}

void FirFilter_3168() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin118_FirFilter_Fiss_3249_3283_split[2]), &(SplitJoin118_FirFilter_Fiss_3249_3283_join[2]));
	ENDFOR
}

void FirFilter_3169() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin118_FirFilter_Fiss_3249_3283_split[3]), &(SplitJoin118_FirFilter_Fiss_3249_3283_join[3]));
	ENDFOR
}

void DUPLICATE_Splitter_3164() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3159DUPLICATE_Splitter_3164);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin118_FirFilter_Fiss_3249_3283_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3165() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[5], pop_float(&SplitJoin118_FirFilter_Fiss_3249_3283_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Delay_N_3172() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin133_Delay_N_Fiss_3250_3284_split[0]), &(SplitJoin133_Delay_N_Fiss_3250_3284_join[0]));
	ENDFOR
}

void Delay_N_3173() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin133_Delay_N_Fiss_3250_3284_split[1]), &(SplitJoin133_Delay_N_Fiss_3250_3284_join[1]));
	ENDFOR
}

void Delay_N_3174() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin133_Delay_N_Fiss_3250_3284_split[2]), &(SplitJoin133_Delay_N_Fiss_3250_3284_join[2]));
	ENDFOR
}

void Delay_N_3175() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin133_Delay_N_Fiss_3250_3284_split[3]), &(SplitJoin133_Delay_N_Fiss_3250_3284_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3170() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin133_Delay_N_Fiss_3250_3284_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[6]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3171() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3171DUPLICATE_Splitter_3176, pop_float(&SplitJoin133_Delay_N_Fiss_3250_3284_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3178() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin135_FirFilter_Fiss_3251_3285_split[0]), &(SplitJoin135_FirFilter_Fiss_3251_3285_join[0]));
	ENDFOR
}

void FirFilter_3179() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin135_FirFilter_Fiss_3251_3285_split[1]), &(SplitJoin135_FirFilter_Fiss_3251_3285_join[1]));
	ENDFOR
}

void FirFilter_3180() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin135_FirFilter_Fiss_3251_3285_split[2]), &(SplitJoin135_FirFilter_Fiss_3251_3285_join[2]));
	ENDFOR
}

void FirFilter_3181() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin135_FirFilter_Fiss_3251_3285_split[3]), &(SplitJoin135_FirFilter_Fiss_3251_3285_join[3]));
	ENDFOR
}

void DUPLICATE_Splitter_3176() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3171DUPLICATE_Splitter_3176);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin135_FirFilter_Fiss_3251_3285_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3177() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3177DownSamp_2993, pop_float(&SplitJoin135_FirFilter_Fiss_3251_3285_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp_2993() {
	DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3177DownSamp_2993), &(DownSamp_2993UpSamp_2994));
}

void UpSamp_2994() {
	UpSamp(&(DownSamp_2993UpSamp_2994), &(UpSamp_2994WEIGHTED_ROUND_ROBIN_Splitter_3182));
}

void Delay_N_3184() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin137_Delay_N_Fiss_3252_3286_split[0]), &(SplitJoin137_Delay_N_Fiss_3252_3286_join[0]));
	ENDFOR
}

void Delay_N_3185() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin137_Delay_N_Fiss_3252_3286_split[1]), &(SplitJoin137_Delay_N_Fiss_3252_3286_join[1]));
	ENDFOR
}

void Delay_N_3186() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin137_Delay_N_Fiss_3252_3286_split[2]), &(SplitJoin137_Delay_N_Fiss_3252_3286_join[2]));
	ENDFOR
}

void Delay_N_3187() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin137_Delay_N_Fiss_3252_3286_split[3]), &(SplitJoin137_Delay_N_Fiss_3252_3286_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3182() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin137_Delay_N_Fiss_3252_3286_split[__iter_], pop_float(&UpSamp_2994WEIGHTED_ROUND_ROBIN_Splitter_3182));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3183() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3183DUPLICATE_Splitter_3188, pop_float(&SplitJoin137_Delay_N_Fiss_3252_3286_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3190() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin139_FirFilter_Fiss_3253_3287_split[0]), &(SplitJoin139_FirFilter_Fiss_3253_3287_join[0]));
	ENDFOR
}

void FirFilter_3191() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin139_FirFilter_Fiss_3253_3287_split[1]), &(SplitJoin139_FirFilter_Fiss_3253_3287_join[1]));
	ENDFOR
}

void FirFilter_3192() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin139_FirFilter_Fiss_3253_3287_split[2]), &(SplitJoin139_FirFilter_Fiss_3253_3287_join[2]));
	ENDFOR
}

void FirFilter_3193() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin139_FirFilter_Fiss_3253_3287_split[3]), &(SplitJoin139_FirFilter_Fiss_3253_3287_join[3]));
	ENDFOR
}

void DUPLICATE_Splitter_3188() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3183DUPLICATE_Splitter_3188);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin139_FirFilter_Fiss_3253_3287_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3189() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[6], pop_float(&SplitJoin139_FirFilter_Fiss_3253_3287_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Delay_N_3196() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin154_Delay_N_Fiss_3254_3288_split[0]), &(SplitJoin154_Delay_N_Fiss_3254_3288_join[0]));
	ENDFOR
}

void Delay_N_3197() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin154_Delay_N_Fiss_3254_3288_split[1]), &(SplitJoin154_Delay_N_Fiss_3254_3288_join[1]));
	ENDFOR
}

void Delay_N_3198() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin154_Delay_N_Fiss_3254_3288_split[2]), &(SplitJoin154_Delay_N_Fiss_3254_3288_join[2]));
	ENDFOR
}

void Delay_N_3199() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin154_Delay_N_Fiss_3254_3288_split[3]), &(SplitJoin154_Delay_N_Fiss_3254_3288_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3194() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin154_Delay_N_Fiss_3254_3288_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[7]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3195() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3195DUPLICATE_Splitter_3200, pop_float(&SplitJoin154_Delay_N_Fiss_3254_3288_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3202() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin156_FirFilter_Fiss_3255_3289_split[0]), &(SplitJoin156_FirFilter_Fiss_3255_3289_join[0]));
	ENDFOR
}

void FirFilter_3203() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin156_FirFilter_Fiss_3255_3289_split[1]), &(SplitJoin156_FirFilter_Fiss_3255_3289_join[1]));
	ENDFOR
}

void FirFilter_3204() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin156_FirFilter_Fiss_3255_3289_split[2]), &(SplitJoin156_FirFilter_Fiss_3255_3289_join[2]));
	ENDFOR
}

void FirFilter_3205() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin156_FirFilter_Fiss_3255_3289_split[3]), &(SplitJoin156_FirFilter_Fiss_3255_3289_join[3]));
	ENDFOR
}

void DUPLICATE_Splitter_3200() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3195DUPLICATE_Splitter_3200);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin156_FirFilter_Fiss_3255_3289_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3201() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3201DownSamp_3000, pop_float(&SplitJoin156_FirFilter_Fiss_3255_3289_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp_3000() {
	DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3201DownSamp_3000), &(DownSamp_3000UpSamp_3001));
}

void UpSamp_3001() {
	UpSamp(&(DownSamp_3000UpSamp_3001), &(UpSamp_3001WEIGHTED_ROUND_ROBIN_Splitter_3206));
}

void Delay_N_3208() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin158_Delay_N_Fiss_3256_3290_split[0]), &(SplitJoin158_Delay_N_Fiss_3256_3290_join[0]));
	ENDFOR
}

void Delay_N_3209() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin158_Delay_N_Fiss_3256_3290_split[1]), &(SplitJoin158_Delay_N_Fiss_3256_3290_join[1]));
	ENDFOR
}

void Delay_N_3210() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin158_Delay_N_Fiss_3256_3290_split[2]), &(SplitJoin158_Delay_N_Fiss_3256_3290_join[2]));
	ENDFOR
}

void Delay_N_3211() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Delay_N(&(SplitJoin158_Delay_N_Fiss_3256_3290_split[3]), &(SplitJoin158_Delay_N_Fiss_3256_3290_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3206() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin158_Delay_N_Fiss_3256_3290_split[__iter_], pop_float(&UpSamp_3001WEIGHTED_ROUND_ROBIN_Splitter_3206));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3207() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3207DUPLICATE_Splitter_3212, pop_float(&SplitJoin158_Delay_N_Fiss_3256_3290_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3214() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin160_FirFilter_Fiss_3257_3291_split[0]), &(SplitJoin160_FirFilter_Fiss_3257_3291_join[0]));
	ENDFOR
}

void FirFilter_3215() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin160_FirFilter_Fiss_3257_3291_split[1]), &(SplitJoin160_FirFilter_Fiss_3257_3291_join[1]));
	ENDFOR
}

void FirFilter_3216() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin160_FirFilter_Fiss_3257_3291_split[2]), &(SplitJoin160_FirFilter_Fiss_3257_3291_join[2]));
	ENDFOR
}

void FirFilter_3217() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FirFilter(&(SplitJoin160_FirFilter_Fiss_3257_3291_split[3]), &(SplitJoin160_FirFilter_Fiss_3257_3291_join[3]));
	ENDFOR
}

void DUPLICATE_Splitter_3212() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3207DUPLICATE_Splitter_3212);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin160_FirFilter_Fiss_3257_3291_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3213() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[7], pop_float(&SplitJoin160_FirFilter_Fiss_3257_3291_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DUPLICATE_Splitter_3006() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&source_2946DUPLICATE_Splitter_3006);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3007() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3007WEIGHTED_ROUND_ROBIN_Splitter_3218, pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Combine(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR(int, i, 0,  < , 8, i++) {
			sum = (sum + pop_float(&(*chanin))) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
	}


void Combine_3220() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Combine(&(SplitJoin10_Combine_Fiss_3229_3292_split[0]), &(SplitJoin10_Combine_Fiss_3229_3292_join[0]));
	ENDFOR
}

void Combine_3221() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Combine(&(SplitJoin10_Combine_Fiss_3229_3292_split[1]), &(SplitJoin10_Combine_Fiss_3229_3292_join[1]));
	ENDFOR
}

void Combine_3222() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Combine(&(SplitJoin10_Combine_Fiss_3229_3292_split[2]), &(SplitJoin10_Combine_Fiss_3229_3292_join[2]));
	ENDFOR
}

void Combine_3223() {
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		Combine(&(SplitJoin10_Combine_Fiss_3229_3292_split[3]), &(SplitJoin10_Combine_Fiss_3229_3292_join[3]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3218() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_Combine_Fiss_3229_3292_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3007WEIGHTED_ROUND_ROBIN_Splitter_3218));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3219() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3219sink_3005, pop_float(&SplitJoin10_Combine_Fiss_3229_3292_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void sink(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void sink_3005() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		sink(&(WEIGHTED_ROUND_ROBIN_Joiner_3219sink_3005));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3207DUPLICATE_Splitter_3212);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3063DUPLICATE_Splitter_3068);
	FOR(int, __iter_init_0_, 0, <, 4, __iter_init_0_++)
		init_buffer_float(&SplitJoin139_FirFilter_Fiss_3253_3287_split[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&DownSamp_2951UpSamp_2952);
	FOR(int, __iter_init_1_, 0, <, 4, __iter_init_1_++)
		init_buffer_float(&SplitJoin4_FirFilter_Fiss_3226_3261_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 4, __iter_init_2_++)
		init_buffer_float(&SplitJoin55_FirFilter_Fiss_3237_3271_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 4, __iter_init_3_++)
		init_buffer_float(&SplitJoin6_Delay_N_Fiss_3227_3262_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 4, __iter_init_4_++)
		init_buffer_float(&SplitJoin51_FirFilter_Fiss_3235_3269_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 4, __iter_init_5_++)
		init_buffer_float(&SplitJoin74_Delay_N_Fiss_3240_3274_join[__iter_init_5_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3129DownSamp_2979);
	init_buffer_float(&UpSamp_2994WEIGHTED_ROUND_ROBIN_Splitter_3182);
	FOR(int, __iter_init_6_, 0, <, 4, __iter_init_6_++)
		init_buffer_float(&SplitJoin154_Delay_N_Fiss_3254_3288_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 4, __iter_init_7_++)
		init_buffer_float(&SplitJoin72_FirFilter_Fiss_3239_3273_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 4, __iter_init_8_++)
		init_buffer_float(&SplitJoin76_FirFilter_Fiss_3241_3275_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 4, __iter_init_9_++)
		init_buffer_float(&SplitJoin28_Delay_N_Fiss_3230_3264_split[__iter_init_9_]);
	ENDFOR
	init_buffer_float(&DownSamp_3000UpSamp_3001);
	FOR(int, __iter_init_10_, 0, <, 4, __iter_init_10_++)
		init_buffer_float(&SplitJoin93_FirFilter_Fiss_3243_3277_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 4, __iter_init_11_++)
		init_buffer_float(&SplitJoin160_FirFilter_Fiss_3257_3291_split[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3081DownSamp_2965);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3183DUPLICATE_Splitter_3188);
	FOR(int, __iter_init_12_, 0, <, 4, __iter_init_12_++)
		init_buffer_float(&SplitJoin8_FirFilter_Fiss_3228_3263_join[__iter_init_12_]);
	ENDFOR
	init_buffer_float(&source_2946DUPLICATE_Splitter_3006);
	FOR(int, __iter_init_13_, 0, <, 4, __iter_init_13_++)
		init_buffer_float(&SplitJoin10_Combine_Fiss_3229_3292_join[__iter_init_13_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3027DUPLICATE_Splitter_3032);
	init_buffer_float(&DownSamp_2993UpSamp_2994);
	FOR(int, __iter_init_14_, 0, <, 4, __iter_init_14_++)
		init_buffer_float(&SplitJoin74_Delay_N_Fiss_3240_3274_split[__iter_init_14_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3105DownSamp_2972);
	init_buffer_float(&UpSamp_2973WEIGHTED_ROUND_ROBIN_Splitter_3110);
	FOR(int, __iter_init_15_, 0, <, 4, __iter_init_15_++)
		init_buffer_float(&SplitJoin70_Delay_N_Fiss_3238_3272_split[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 4, __iter_init_16_++)
		init_buffer_float(&SplitJoin95_Delay_N_Fiss_3244_3278_join[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 4, __iter_init_17_++)
		init_buffer_float(&SplitJoin156_FirFilter_Fiss_3255_3289_join[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 4, __iter_init_18_++)
		init_buffer_float(&SplitJoin112_Delay_N_Fiss_3246_3280_split[__iter_init_18_]);
	ENDFOR
	FOR(int, __iter_init_19_, 0, <, 4, __iter_init_19_++)
		init_buffer_float(&SplitJoin53_Delay_N_Fiss_3236_3270_join[__iter_init_19_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3075DUPLICATE_Splitter_3080);
	init_buffer_float(&UpSamp_2987WEIGHTED_ROUND_ROBIN_Splitter_3158);
	FOR(int, __iter_init_20_, 0, <, 4, __iter_init_20_++)
		init_buffer_float(&SplitJoin49_Delay_N_Fiss_3234_3268_join[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 4, __iter_init_21_++)
		init_buffer_float(&SplitJoin49_Delay_N_Fiss_3234_3268_split[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 4, __iter_init_22_++)
		init_buffer_float(&SplitJoin158_Delay_N_Fiss_3256_3290_split[__iter_init_22_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3195DUPLICATE_Splitter_3200);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3219sink_3005);
	FOR(int, __iter_init_23_, 0, <, 4, __iter_init_23_++)
		init_buffer_float(&SplitJoin2_Delay_N_Fiss_3225_3260_join[__iter_init_23_]);
	ENDFOR
	init_buffer_float(&DownSamp_2986UpSamp_2987);
	FOR(int, __iter_init_24_, 0, <, 4, __iter_init_24_++)
		init_buffer_float(&SplitJoin30_FirFilter_Fiss_3231_3265_join[__iter_init_24_]);
	ENDFOR
	FOR(int, __iter_init_25_, 0, <, 4, __iter_init_25_++)
		init_buffer_float(&SplitJoin32_Delay_N_Fiss_3232_3266_join[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 4, __iter_init_26_++)
		init_buffer_float(&SplitJoin53_Delay_N_Fiss_3236_3270_split[__iter_init_26_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3171DUPLICATE_Splitter_3176);
	FOR(int, __iter_init_27_, 0, <, 4, __iter_init_27_++)
		init_buffer_float(&SplitJoin91_Delay_N_Fiss_3242_3276_join[__iter_init_27_]);
	ENDFOR
	init_buffer_float(&UpSamp_3001WEIGHTED_ROUND_ROBIN_Splitter_3206);
	FOR(int, __iter_init_28_, 0, <, 4, __iter_init_28_++)
		init_buffer_float(&SplitJoin10_Combine_Fiss_3229_3292_split[__iter_init_28_]);
	ENDFOR
	init_buffer_float(&UpSamp_2952WEIGHTED_ROUND_ROBIN_Splitter_3038);
	FOR(int, __iter_init_29_, 0, <, 4, __iter_init_29_++)
		init_buffer_float(&SplitJoin6_Delay_N_Fiss_3227_3262_join[__iter_init_29_]);
	ENDFOR
	FOR(int, __iter_init_30_, 0, <, 4, __iter_init_30_++)
		init_buffer_float(&SplitJoin135_FirFilter_Fiss_3251_3285_join[__iter_init_30_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3201DownSamp_3000);
	FOR(int, __iter_init_31_, 0, <, 4, __iter_init_31_++)
		init_buffer_float(&SplitJoin8_FirFilter_Fiss_3228_3263_split[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 4, __iter_init_32_++)
		init_buffer_float(&SplitJoin30_FirFilter_Fiss_3231_3265_split[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 4, __iter_init_33_++)
		init_buffer_float(&SplitJoin2_Delay_N_Fiss_3225_3260_split[__iter_init_33_]);
	ENDFOR
	init_buffer_float(&DownSamp_2979UpSamp_2980);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3039DUPLICATE_Splitter_3044);
	FOR(int, __iter_init_34_, 0, <, 4, __iter_init_34_++)
		init_buffer_float(&SplitJoin154_Delay_N_Fiss_3254_3288_split[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 4, __iter_init_35_++)
		init_buffer_float(&SplitJoin112_Delay_N_Fiss_3246_3280_join[__iter_init_35_]);
	ENDFOR
	FOR(int, __iter_init_36_, 0, <, 4, __iter_init_36_++)
		init_buffer_float(&SplitJoin158_Delay_N_Fiss_3256_3290_join[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 4, __iter_init_37_++)
		init_buffer_float(&SplitJoin97_FirFilter_Fiss_3245_3279_split[__iter_init_37_]);
	ENDFOR
	FOR(int, __iter_init_38_, 0, <, 4, __iter_init_38_++)
		init_buffer_float(&SplitJoin114_FirFilter_Fiss_3247_3281_join[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 4, __iter_init_39_++)
		init_buffer_float(&SplitJoin118_FirFilter_Fiss_3249_3283_join[__iter_init_39_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3087DUPLICATE_Splitter_3092);
	FOR(int, __iter_init_40_, 0, <, 4, __iter_init_40_++)
		init_buffer_float(&SplitJoin4_FirFilter_Fiss_3226_3261_split[__iter_init_40_]);
	ENDFOR
	FOR(int, __iter_init_41_, 0, <, 4, __iter_init_41_++)
		init_buffer_float(&SplitJoin137_Delay_N_Fiss_3252_3286_split[__iter_init_41_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3159DUPLICATE_Splitter_3164);
	FOR(int, __iter_init_42_, 0, <, 8, __iter_init_42_++)
		init_buffer_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[__iter_init_42_]);
	ENDFOR
	FOR(int, __iter_init_43_, 0, <, 4, __iter_init_43_++)
		init_buffer_float(&SplitJoin70_Delay_N_Fiss_3238_3272_join[__iter_init_43_]);
	ENDFOR
	FOR(int, __iter_init_44_, 0, <, 4, __iter_init_44_++)
		init_buffer_float(&SplitJoin118_FirFilter_Fiss_3249_3283_split[__iter_init_44_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3099DUPLICATE_Splitter_3104);
	FOR(int, __iter_init_45_, 0, <, 4, __iter_init_45_++)
		init_buffer_float(&SplitJoin160_FirFilter_Fiss_3257_3291_join[__iter_init_45_]);
	ENDFOR
	FOR(int, __iter_init_46_, 0, <, 4, __iter_init_46_++)
		init_buffer_float(&SplitJoin32_Delay_N_Fiss_3232_3266_split[__iter_init_46_]);
	ENDFOR
	FOR(int, __iter_init_47_, 0, <, 4, __iter_init_47_++)
		init_buffer_float(&SplitJoin139_FirFilter_Fiss_3253_3287_join[__iter_init_47_]);
	ENDFOR
	init_buffer_float(&UpSamp_2980WEIGHTED_ROUND_ROBIN_Splitter_3134);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3177DownSamp_2993);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3051DUPLICATE_Splitter_3056);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3007WEIGHTED_ROUND_ROBIN_Splitter_3218);
	FOR(int, __iter_init_48_, 0, <, 4, __iter_init_48_++)
		init_buffer_float(&SplitJoin95_Delay_N_Fiss_3244_3278_split[__iter_init_48_]);
	ENDFOR
	FOR(int, __iter_init_49_, 0, <, 4, __iter_init_49_++)
		init_buffer_float(&SplitJoin156_FirFilter_Fiss_3255_3289_split[__iter_init_49_]);
	ENDFOR
	FOR(int, __iter_init_50_, 0, <, 4, __iter_init_50_++)
		init_buffer_float(&SplitJoin34_FirFilter_Fiss_3233_3267_split[__iter_init_50_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3111DUPLICATE_Splitter_3116);
	FOR(int, __iter_init_51_, 0, <, 4, __iter_init_51_++)
		init_buffer_float(&SplitJoin133_Delay_N_Fiss_3250_3284_split[__iter_init_51_]);
	ENDFOR
	FOR(int, __iter_init_52_, 0, <, 4, __iter_init_52_++)
		init_buffer_float(&SplitJoin133_Delay_N_Fiss_3250_3284_join[__iter_init_52_]);
	ENDFOR
	init_buffer_float(&DownSamp_2958UpSamp_2959);
	FOR(int, __iter_init_53_, 0, <, 4, __iter_init_53_++)
		init_buffer_float(&SplitJoin91_Delay_N_Fiss_3242_3276_split[__iter_init_53_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3153DownSamp_2986);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3033DownSamp_2951);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3147DUPLICATE_Splitter_3152);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3123DUPLICATE_Splitter_3128);
	FOR(int, __iter_init_54_, 0, <, 4, __iter_init_54_++)
		init_buffer_float(&SplitJoin51_FirFilter_Fiss_3235_3269_split[__iter_init_54_]);
	ENDFOR
	FOR(int, __iter_init_55_, 0, <, 4, __iter_init_55_++)
		init_buffer_float(&SplitJoin116_Delay_N_Fiss_3248_3282_join[__iter_init_55_]);
	ENDFOR
	FOR(int, __iter_init_56_, 0, <, 4, __iter_init_56_++)
		init_buffer_float(&SplitJoin135_FirFilter_Fiss_3251_3285_split[__iter_init_56_]);
	ENDFOR
	FOR(int, __iter_init_57_, 0, <, 4, __iter_init_57_++)
		init_buffer_float(&SplitJoin76_FirFilter_Fiss_3241_3275_split[__iter_init_57_]);
	ENDFOR
	FOR(int, __iter_init_58_, 0, <, 4, __iter_init_58_++)
		init_buffer_float(&SplitJoin114_FirFilter_Fiss_3247_3281_split[__iter_init_58_]);
	ENDFOR
	FOR(int, __iter_init_59_, 0, <, 8, __iter_init_59_++)
		init_buffer_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[__iter_init_59_]);
	ENDFOR
	FOR(int, __iter_init_60_, 0, <, 4, __iter_init_60_++)
		init_buffer_float(&SplitJoin97_FirFilter_Fiss_3245_3279_join[__iter_init_60_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3057DownSamp_2958);
	init_buffer_float(&DownSamp_2972UpSamp_2973);
	init_buffer_float(&DownSamp_2965UpSamp_2966);
	FOR(int, __iter_init_61_, 0, <, 4, __iter_init_61_++)
		init_buffer_float(&SplitJoin72_FirFilter_Fiss_3239_3273_split[__iter_init_61_]);
	ENDFOR
	FOR(int, __iter_init_62_, 0, <, 4, __iter_init_62_++)
		init_buffer_float(&SplitJoin55_FirFilter_Fiss_3237_3271_join[__iter_init_62_]);
	ENDFOR
	FOR(int, __iter_init_63_, 0, <, 4, __iter_init_63_++)
		init_buffer_float(&SplitJoin137_Delay_N_Fiss_3252_3286_join[__iter_init_63_]);
	ENDFOR
	init_buffer_float(&UpSamp_2966WEIGHTED_ROUND_ROBIN_Splitter_3086);
	init_buffer_float(&UpSamp_2959WEIGHTED_ROUND_ROBIN_Splitter_3062);
	FOR(int, __iter_init_64_, 0, <, 4, __iter_init_64_++)
		init_buffer_float(&SplitJoin93_FirFilter_Fiss_3243_3277_split[__iter_init_64_]);
	ENDFOR
	FOR(int, __iter_init_65_, 0, <, 4, __iter_init_65_++)
		init_buffer_float(&SplitJoin34_FirFilter_Fiss_3233_3267_join[__iter_init_65_]);
	ENDFOR
	FOR(int, __iter_init_66_, 0, <, 4, __iter_init_66_++)
		init_buffer_float(&SplitJoin28_Delay_N_Fiss_3230_3264_join[__iter_init_66_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3135DUPLICATE_Splitter_3140);
	FOR(int, __iter_init_67_, 0, <, 4, __iter_init_67_++)
		init_buffer_float(&SplitJoin116_Delay_N_Fiss_3248_3282_split[__iter_init_67_]);
	ENDFOR
// --- init: source_2946
	 {
	source_2946_s.current = 0.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 300, __iter_init_++)
		source(&(source_2946DUPLICATE_Splitter_3006));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3006
	FOR(uint32_t, __iter_init_, 0, <, 300, __iter_init_++)
		DUPLICATE_Splitter(&(source_2946DUPLICATE_Splitter_3006), &(SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3026
	FOR(uint32_t, __iter_init_, 0, <, 75, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[0]), &(SplitJoin2_Delay_N_Fiss_3225_3260_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3028
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin2_Delay_N_Fiss_3225_3260_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3029
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin2_Delay_N_Fiss_3225_3260_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3030
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin2_Delay_N_Fiss_3225_3260_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3031
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin2_Delay_N_Fiss_3225_3260_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3027
	FOR(uint32_t, __iter_init_, 0, <, 46, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin2_Delay_N_Fiss_3225_3260_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3027DUPLICATE_Splitter_3032));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3032
	FOR(uint32_t, __iter_init_, 0, <, 183, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3027DUPLICATE_Splitter_3032), &(SplitJoin4_FirFilter_Fiss_3226_3261_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3034
	 {
	FirFilter_3034_s.COEFF[0] = 1.0 ; 
	FirFilter_3034_s.COEFF[1] = 34.0 ; 
	FirFilter_3034_s.COEFF[2] = 67.0 ; 
	FirFilter_3034_s.COEFF[3] = 100.0 ; 
	FirFilter_3034_s.COEFF[4] = 133.0 ; 
	FirFilter_3034_s.COEFF[5] = 166.0 ; 
	FirFilter_3034_s.COEFF[6] = 199.0 ; 
	FirFilter_3034_s.COEFF[7] = 232.0 ; 
	FirFilter_3034_s.COEFF[8] = 265.0 ; 
	FirFilter_3034_s.COEFF[9] = 298.0 ; 
	FirFilter_3034_s.COEFF[10] = 331.0 ; 
	FirFilter_3034_s.COEFF[11] = 364.0 ; 
	FirFilter_3034_s.COEFF[12] = 397.0 ; 
	FirFilter_3034_s.COEFF[13] = 430.0 ; 
	FirFilter_3034_s.COEFF[14] = 463.0 ; 
	FirFilter_3034_s.COEFF[15] = 496.0 ; 
	FirFilter_3034_s.COEFF[16] = 529.0 ; 
	FirFilter_3034_s.COEFF[17] = 562.0 ; 
	FirFilter_3034_s.COEFF[18] = 595.0 ; 
	FirFilter_3034_s.COEFF[19] = 628.0 ; 
	FirFilter_3034_s.COEFF[20] = 661.0 ; 
	FirFilter_3034_s.COEFF[21] = 694.0 ; 
	FirFilter_3034_s.COEFF[22] = 727.0 ; 
	FirFilter_3034_s.COEFF[23] = 760.0 ; 
	FirFilter_3034_s.COEFF[24] = 793.0 ; 
	FirFilter_3034_s.COEFF[25] = 826.0 ; 
	FirFilter_3034_s.COEFF[26] = 859.0 ; 
	FirFilter_3034_s.COEFF[27] = 892.0 ; 
	FirFilter_3034_s.COEFF[28] = 925.0 ; 
	FirFilter_3034_s.COEFF[29] = 958.0 ; 
	FirFilter_3034_s.COEFF[30] = 991.0 ; 
	FirFilter_3034_s.COEFF[31] = 1024.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin4_FirFilter_Fiss_3226_3261_split[0]), &(SplitJoin4_FirFilter_Fiss_3226_3261_join[0]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3035
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin4_FirFilter_Fiss_3226_3261_split[1]), &(SplitJoin4_FirFilter_Fiss_3226_3261_join[1]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3036
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin4_FirFilter_Fiss_3226_3261_split[2]), &(SplitJoin4_FirFilter_Fiss_3226_3261_join[2]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3037
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin4_FirFilter_Fiss_3226_3261_split[3]), &(SplitJoin4_FirFilter_Fiss_3226_3261_join[3]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3033
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin4_FirFilter_Fiss_3226_3261_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3033DownSamp_2951));
	ENDFOR
//--------------------------------
// --- init: DownSamp_2951
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3033DownSamp_2951), &(DownSamp_2951UpSamp_2952));
	ENDFOR
//--------------------------------
// --- init: UpSamp_2952
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++)
		UpSamp(&(DownSamp_2951UpSamp_2952), &(UpSamp_2952WEIGHTED_ROUND_ROBIN_Splitter_3038));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3038
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(UpSamp_2952WEIGHTED_ROUND_ROBIN_Splitter_3038), &(SplitJoin6_Delay_N_Fiss_3227_3262_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3040
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin6_Delay_N_Fiss_3227_3262_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3041
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin6_Delay_N_Fiss_3227_3262_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3042
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin6_Delay_N_Fiss_3227_3262_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3043
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin6_Delay_N_Fiss_3227_3262_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3039
	FOR(uint32_t, __iter_init_, 0, <, 9, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin6_Delay_N_Fiss_3227_3262_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3039DUPLICATE_Splitter_3044));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3044
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3039DUPLICATE_Splitter_3044), &(SplitJoin8_FirFilter_Fiss_3228_3263_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3046
	FirFilter(&(SplitJoin8_FirFilter_Fiss_3228_3263_split[0]), &(SplitJoin8_FirFilter_Fiss_3228_3263_join[0]));
//--------------------------------
// --- init: FirFilter_3047
	FirFilter(&(SplitJoin8_FirFilter_Fiss_3228_3263_split[1]), &(SplitJoin8_FirFilter_Fiss_3228_3263_join[1]));
//--------------------------------
// --- init: FirFilter_3048
	FirFilter(&(SplitJoin8_FirFilter_Fiss_3228_3263_split[2]), &(SplitJoin8_FirFilter_Fiss_3228_3263_join[2]));
//--------------------------------
// --- init: FirFilter_3049
	FirFilter(&(SplitJoin8_FirFilter_Fiss_3228_3263_split[3]), &(SplitJoin8_FirFilter_Fiss_3228_3263_join[3]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3045
	WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin8_FirFilter_Fiss_3228_3263_join[Need Human for iter var]), &(SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[0]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3050
	FOR(uint32_t, __iter_init_, 0, <, 75, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[1]), &(SplitJoin28_Delay_N_Fiss_3230_3264_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3052
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin28_Delay_N_Fiss_3230_3264_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3053
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin28_Delay_N_Fiss_3230_3264_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3054
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin28_Delay_N_Fiss_3230_3264_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3055
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin28_Delay_N_Fiss_3230_3264_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3051
	FOR(uint32_t, __iter_init_, 0, <, 46, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin28_Delay_N_Fiss_3230_3264_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3051DUPLICATE_Splitter_3056));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3056
	FOR(uint32_t, __iter_init_, 0, <, 183, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3051DUPLICATE_Splitter_3056), &(SplitJoin30_FirFilter_Fiss_3231_3265_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3058
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin30_FirFilter_Fiss_3231_3265_split[0]), &(SplitJoin30_FirFilter_Fiss_3231_3265_join[0]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3059
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin30_FirFilter_Fiss_3231_3265_split[1]), &(SplitJoin30_FirFilter_Fiss_3231_3265_join[1]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3060
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin30_FirFilter_Fiss_3231_3265_split[2]), &(SplitJoin30_FirFilter_Fiss_3231_3265_join[2]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3061
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin30_FirFilter_Fiss_3231_3265_split[3]), &(SplitJoin30_FirFilter_Fiss_3231_3265_join[3]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3057
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin30_FirFilter_Fiss_3231_3265_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3057DownSamp_2958));
	ENDFOR
//--------------------------------
// --- init: DownSamp_2958
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3057DownSamp_2958), &(DownSamp_2958UpSamp_2959));
	ENDFOR
//--------------------------------
// --- init: UpSamp_2959
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++)
		UpSamp(&(DownSamp_2958UpSamp_2959), &(UpSamp_2959WEIGHTED_ROUND_ROBIN_Splitter_3062));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3062
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(UpSamp_2959WEIGHTED_ROUND_ROBIN_Splitter_3062), &(SplitJoin32_Delay_N_Fiss_3232_3266_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3064
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin32_Delay_N_Fiss_3232_3266_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3065
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin32_Delay_N_Fiss_3232_3266_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3066
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin32_Delay_N_Fiss_3232_3266_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3067
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin32_Delay_N_Fiss_3232_3266_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3063
	FOR(uint32_t, __iter_init_, 0, <, 9, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin32_Delay_N_Fiss_3232_3266_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3063DUPLICATE_Splitter_3068));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3068
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3063DUPLICATE_Splitter_3068), &(SplitJoin34_FirFilter_Fiss_3233_3267_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3070
	FirFilter(&(SplitJoin34_FirFilter_Fiss_3233_3267_split[0]), &(SplitJoin34_FirFilter_Fiss_3233_3267_join[0]));
//--------------------------------
// --- init: FirFilter_3071
	FirFilter(&(SplitJoin34_FirFilter_Fiss_3233_3267_split[1]), &(SplitJoin34_FirFilter_Fiss_3233_3267_join[1]));
//--------------------------------
// --- init: FirFilter_3072
	FirFilter(&(SplitJoin34_FirFilter_Fiss_3233_3267_split[2]), &(SplitJoin34_FirFilter_Fiss_3233_3267_join[2]));
//--------------------------------
// --- init: FirFilter_3073
	FirFilter(&(SplitJoin34_FirFilter_Fiss_3233_3267_split[3]), &(SplitJoin34_FirFilter_Fiss_3233_3267_join[3]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3069
	WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin34_FirFilter_Fiss_3233_3267_join[Need Human for iter var]), &(SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[1]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3074
	FOR(uint32_t, __iter_init_, 0, <, 75, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[2]), &(SplitJoin49_Delay_N_Fiss_3234_3268_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3076
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin49_Delay_N_Fiss_3234_3268_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3077
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin49_Delay_N_Fiss_3234_3268_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3078
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin49_Delay_N_Fiss_3234_3268_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3079
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin49_Delay_N_Fiss_3234_3268_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3075
	FOR(uint32_t, __iter_init_, 0, <, 46, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin49_Delay_N_Fiss_3234_3268_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3075DUPLICATE_Splitter_3080));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3080
	FOR(uint32_t, __iter_init_, 0, <, 183, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3075DUPLICATE_Splitter_3080), &(SplitJoin51_FirFilter_Fiss_3235_3269_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3082
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin51_FirFilter_Fiss_3235_3269_split[0]), &(SplitJoin51_FirFilter_Fiss_3235_3269_join[0]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3083
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin51_FirFilter_Fiss_3235_3269_split[1]), &(SplitJoin51_FirFilter_Fiss_3235_3269_join[1]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3084
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin51_FirFilter_Fiss_3235_3269_split[2]), &(SplitJoin51_FirFilter_Fiss_3235_3269_join[2]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3085
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin51_FirFilter_Fiss_3235_3269_split[3]), &(SplitJoin51_FirFilter_Fiss_3235_3269_join[3]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3081
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin51_FirFilter_Fiss_3235_3269_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3081DownSamp_2965));
	ENDFOR
//--------------------------------
// --- init: DownSamp_2965
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3081DownSamp_2965), &(DownSamp_2965UpSamp_2966));
	ENDFOR
//--------------------------------
// --- init: UpSamp_2966
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++)
		UpSamp(&(DownSamp_2965UpSamp_2966), &(UpSamp_2966WEIGHTED_ROUND_ROBIN_Splitter_3086));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3086
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(UpSamp_2966WEIGHTED_ROUND_ROBIN_Splitter_3086), &(SplitJoin53_Delay_N_Fiss_3236_3270_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3088
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin53_Delay_N_Fiss_3236_3270_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3089
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin53_Delay_N_Fiss_3236_3270_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3090
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin53_Delay_N_Fiss_3236_3270_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3091
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin53_Delay_N_Fiss_3236_3270_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3087
	FOR(uint32_t, __iter_init_, 0, <, 9, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin53_Delay_N_Fiss_3236_3270_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3087DUPLICATE_Splitter_3092));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3092
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3087DUPLICATE_Splitter_3092), &(SplitJoin55_FirFilter_Fiss_3237_3271_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3094
	FirFilter(&(SplitJoin55_FirFilter_Fiss_3237_3271_split[0]), &(SplitJoin55_FirFilter_Fiss_3237_3271_join[0]));
//--------------------------------
// --- init: FirFilter_3095
	FirFilter(&(SplitJoin55_FirFilter_Fiss_3237_3271_split[1]), &(SplitJoin55_FirFilter_Fiss_3237_3271_join[1]));
//--------------------------------
// --- init: FirFilter_3096
	FirFilter(&(SplitJoin55_FirFilter_Fiss_3237_3271_split[2]), &(SplitJoin55_FirFilter_Fiss_3237_3271_join[2]));
//--------------------------------
// --- init: FirFilter_3097
	FirFilter(&(SplitJoin55_FirFilter_Fiss_3237_3271_split[3]), &(SplitJoin55_FirFilter_Fiss_3237_3271_join[3]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3093
	WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin55_FirFilter_Fiss_3237_3271_join[Need Human for iter var]), &(SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[2]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3098
	FOR(uint32_t, __iter_init_, 0, <, 75, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[3]), &(SplitJoin70_Delay_N_Fiss_3238_3272_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3100
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin70_Delay_N_Fiss_3238_3272_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3101
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin70_Delay_N_Fiss_3238_3272_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3102
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin70_Delay_N_Fiss_3238_3272_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3103
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin70_Delay_N_Fiss_3238_3272_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3099
	FOR(uint32_t, __iter_init_, 0, <, 46, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin70_Delay_N_Fiss_3238_3272_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3099DUPLICATE_Splitter_3104));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3104
	FOR(uint32_t, __iter_init_, 0, <, 183, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3099DUPLICATE_Splitter_3104), &(SplitJoin72_FirFilter_Fiss_3239_3273_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3106
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin72_FirFilter_Fiss_3239_3273_split[0]), &(SplitJoin72_FirFilter_Fiss_3239_3273_join[0]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3107
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin72_FirFilter_Fiss_3239_3273_split[1]), &(SplitJoin72_FirFilter_Fiss_3239_3273_join[1]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3108
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin72_FirFilter_Fiss_3239_3273_split[2]), &(SplitJoin72_FirFilter_Fiss_3239_3273_join[2]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3109
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin72_FirFilter_Fiss_3239_3273_split[3]), &(SplitJoin72_FirFilter_Fiss_3239_3273_join[3]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3105
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin72_FirFilter_Fiss_3239_3273_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3105DownSamp_2972));
	ENDFOR
//--------------------------------
// --- init: DownSamp_2972
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3105DownSamp_2972), &(DownSamp_2972UpSamp_2973));
	ENDFOR
//--------------------------------
// --- init: UpSamp_2973
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++)
		UpSamp(&(DownSamp_2972UpSamp_2973), &(UpSamp_2973WEIGHTED_ROUND_ROBIN_Splitter_3110));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3110
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(UpSamp_2973WEIGHTED_ROUND_ROBIN_Splitter_3110), &(SplitJoin74_Delay_N_Fiss_3240_3274_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3112
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin74_Delay_N_Fiss_3240_3274_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3113
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin74_Delay_N_Fiss_3240_3274_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3114
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin74_Delay_N_Fiss_3240_3274_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3115
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin74_Delay_N_Fiss_3240_3274_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3111
	FOR(uint32_t, __iter_init_, 0, <, 9, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin74_Delay_N_Fiss_3240_3274_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3111DUPLICATE_Splitter_3116));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3116
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3111DUPLICATE_Splitter_3116), &(SplitJoin76_FirFilter_Fiss_3241_3275_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3118
	FirFilter(&(SplitJoin76_FirFilter_Fiss_3241_3275_split[0]), &(SplitJoin76_FirFilter_Fiss_3241_3275_join[0]));
//--------------------------------
// --- init: FirFilter_3119
	FirFilter(&(SplitJoin76_FirFilter_Fiss_3241_3275_split[1]), &(SplitJoin76_FirFilter_Fiss_3241_3275_join[1]));
//--------------------------------
// --- init: FirFilter_3120
	FirFilter(&(SplitJoin76_FirFilter_Fiss_3241_3275_split[2]), &(SplitJoin76_FirFilter_Fiss_3241_3275_join[2]));
//--------------------------------
// --- init: FirFilter_3121
	FirFilter(&(SplitJoin76_FirFilter_Fiss_3241_3275_split[3]), &(SplitJoin76_FirFilter_Fiss_3241_3275_join[3]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3117
	WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin76_FirFilter_Fiss_3241_3275_join[Need Human for iter var]), &(SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[3]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3122
	FOR(uint32_t, __iter_init_, 0, <, 75, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[4]), &(SplitJoin91_Delay_N_Fiss_3242_3276_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3124
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin91_Delay_N_Fiss_3242_3276_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3125
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin91_Delay_N_Fiss_3242_3276_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3126
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin91_Delay_N_Fiss_3242_3276_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3127
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin91_Delay_N_Fiss_3242_3276_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3123
	FOR(uint32_t, __iter_init_, 0, <, 46, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin91_Delay_N_Fiss_3242_3276_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3123DUPLICATE_Splitter_3128));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3128
	FOR(uint32_t, __iter_init_, 0, <, 183, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3123DUPLICATE_Splitter_3128), &(SplitJoin93_FirFilter_Fiss_3243_3277_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3130
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin93_FirFilter_Fiss_3243_3277_split[0]), &(SplitJoin93_FirFilter_Fiss_3243_3277_join[0]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3131
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin93_FirFilter_Fiss_3243_3277_split[1]), &(SplitJoin93_FirFilter_Fiss_3243_3277_join[1]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3132
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin93_FirFilter_Fiss_3243_3277_split[2]), &(SplitJoin93_FirFilter_Fiss_3243_3277_join[2]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3133
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin93_FirFilter_Fiss_3243_3277_split[3]), &(SplitJoin93_FirFilter_Fiss_3243_3277_join[3]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3129
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin93_FirFilter_Fiss_3243_3277_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3129DownSamp_2979));
	ENDFOR
//--------------------------------
// --- init: DownSamp_2979
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3129DownSamp_2979), &(DownSamp_2979UpSamp_2980));
	ENDFOR
//--------------------------------
// --- init: UpSamp_2980
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++)
		UpSamp(&(DownSamp_2979UpSamp_2980), &(UpSamp_2980WEIGHTED_ROUND_ROBIN_Splitter_3134));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3134
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(UpSamp_2980WEIGHTED_ROUND_ROBIN_Splitter_3134), &(SplitJoin95_Delay_N_Fiss_3244_3278_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3136
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin95_Delay_N_Fiss_3244_3278_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3137
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin95_Delay_N_Fiss_3244_3278_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3138
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin95_Delay_N_Fiss_3244_3278_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3139
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin95_Delay_N_Fiss_3244_3278_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3135
	FOR(uint32_t, __iter_init_, 0, <, 9, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin95_Delay_N_Fiss_3244_3278_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3135DUPLICATE_Splitter_3140));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3140
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3135DUPLICATE_Splitter_3140), &(SplitJoin97_FirFilter_Fiss_3245_3279_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3142
	FirFilter(&(SplitJoin97_FirFilter_Fiss_3245_3279_split[0]), &(SplitJoin97_FirFilter_Fiss_3245_3279_join[0]));
//--------------------------------
// --- init: FirFilter_3143
	FirFilter(&(SplitJoin97_FirFilter_Fiss_3245_3279_split[1]), &(SplitJoin97_FirFilter_Fiss_3245_3279_join[1]));
//--------------------------------
// --- init: FirFilter_3144
	FirFilter(&(SplitJoin97_FirFilter_Fiss_3245_3279_split[2]), &(SplitJoin97_FirFilter_Fiss_3245_3279_join[2]));
//--------------------------------
// --- init: FirFilter_3145
	FirFilter(&(SplitJoin97_FirFilter_Fiss_3245_3279_split[3]), &(SplitJoin97_FirFilter_Fiss_3245_3279_join[3]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3141
	WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin97_FirFilter_Fiss_3245_3279_join[Need Human for iter var]), &(SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[4]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3146
	FOR(uint32_t, __iter_init_, 0, <, 75, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[5]), &(SplitJoin112_Delay_N_Fiss_3246_3280_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3148
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin112_Delay_N_Fiss_3246_3280_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3149
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin112_Delay_N_Fiss_3246_3280_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3150
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin112_Delay_N_Fiss_3246_3280_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3151
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin112_Delay_N_Fiss_3246_3280_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3147
	FOR(uint32_t, __iter_init_, 0, <, 46, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin112_Delay_N_Fiss_3246_3280_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3147DUPLICATE_Splitter_3152));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3152
	FOR(uint32_t, __iter_init_, 0, <, 183, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3147DUPLICATE_Splitter_3152), &(SplitJoin114_FirFilter_Fiss_3247_3281_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3154
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin114_FirFilter_Fiss_3247_3281_split[0]), &(SplitJoin114_FirFilter_Fiss_3247_3281_join[0]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3155
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin114_FirFilter_Fiss_3247_3281_split[1]), &(SplitJoin114_FirFilter_Fiss_3247_3281_join[1]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3156
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin114_FirFilter_Fiss_3247_3281_split[2]), &(SplitJoin114_FirFilter_Fiss_3247_3281_join[2]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3157
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin114_FirFilter_Fiss_3247_3281_split[3]), &(SplitJoin114_FirFilter_Fiss_3247_3281_join[3]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3153
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin114_FirFilter_Fiss_3247_3281_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3153DownSamp_2986));
	ENDFOR
//--------------------------------
// --- init: DownSamp_2986
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3153DownSamp_2986), &(DownSamp_2986UpSamp_2987));
	ENDFOR
//--------------------------------
// --- init: UpSamp_2987
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++)
		UpSamp(&(DownSamp_2986UpSamp_2987), &(UpSamp_2987WEIGHTED_ROUND_ROBIN_Splitter_3158));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3158
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(UpSamp_2987WEIGHTED_ROUND_ROBIN_Splitter_3158), &(SplitJoin116_Delay_N_Fiss_3248_3282_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3160
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin116_Delay_N_Fiss_3248_3282_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3161
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin116_Delay_N_Fiss_3248_3282_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3162
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin116_Delay_N_Fiss_3248_3282_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3163
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin116_Delay_N_Fiss_3248_3282_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3159
	FOR(uint32_t, __iter_init_, 0, <, 9, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin116_Delay_N_Fiss_3248_3282_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3159DUPLICATE_Splitter_3164));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3164
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3159DUPLICATE_Splitter_3164), &(SplitJoin118_FirFilter_Fiss_3249_3283_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3166
	FirFilter(&(SplitJoin118_FirFilter_Fiss_3249_3283_split[0]), &(SplitJoin118_FirFilter_Fiss_3249_3283_join[0]));
//--------------------------------
// --- init: FirFilter_3167
	FirFilter(&(SplitJoin118_FirFilter_Fiss_3249_3283_split[1]), &(SplitJoin118_FirFilter_Fiss_3249_3283_join[1]));
//--------------------------------
// --- init: FirFilter_3168
	FirFilter(&(SplitJoin118_FirFilter_Fiss_3249_3283_split[2]), &(SplitJoin118_FirFilter_Fiss_3249_3283_join[2]));
//--------------------------------
// --- init: FirFilter_3169
	FirFilter(&(SplitJoin118_FirFilter_Fiss_3249_3283_split[3]), &(SplitJoin118_FirFilter_Fiss_3249_3283_join[3]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3165
	WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin118_FirFilter_Fiss_3249_3283_join[Need Human for iter var]), &(SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[5]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3170
	FOR(uint32_t, __iter_init_, 0, <, 75, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[6]), &(SplitJoin133_Delay_N_Fiss_3250_3284_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3172
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin133_Delay_N_Fiss_3250_3284_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3173
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin133_Delay_N_Fiss_3250_3284_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3174
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin133_Delay_N_Fiss_3250_3284_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3175
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin133_Delay_N_Fiss_3250_3284_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3171
	FOR(uint32_t, __iter_init_, 0, <, 46, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin133_Delay_N_Fiss_3250_3284_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3171DUPLICATE_Splitter_3176));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3176
	FOR(uint32_t, __iter_init_, 0, <, 183, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3171DUPLICATE_Splitter_3176), &(SplitJoin135_FirFilter_Fiss_3251_3285_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3178
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin135_FirFilter_Fiss_3251_3285_split[0]), &(SplitJoin135_FirFilter_Fiss_3251_3285_join[0]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3179
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin135_FirFilter_Fiss_3251_3285_split[1]), &(SplitJoin135_FirFilter_Fiss_3251_3285_join[1]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3180
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin135_FirFilter_Fiss_3251_3285_split[2]), &(SplitJoin135_FirFilter_Fiss_3251_3285_join[2]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3181
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin135_FirFilter_Fiss_3251_3285_split[3]), &(SplitJoin135_FirFilter_Fiss_3251_3285_join[3]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3177
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin135_FirFilter_Fiss_3251_3285_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3177DownSamp_2993));
	ENDFOR
//--------------------------------
// --- init: DownSamp_2993
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3177DownSamp_2993), &(DownSamp_2993UpSamp_2994));
	ENDFOR
//--------------------------------
// --- init: UpSamp_2994
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++)
		UpSamp(&(DownSamp_2993UpSamp_2994), &(UpSamp_2994WEIGHTED_ROUND_ROBIN_Splitter_3182));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3182
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(UpSamp_2994WEIGHTED_ROUND_ROBIN_Splitter_3182), &(SplitJoin137_Delay_N_Fiss_3252_3286_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3184
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin137_Delay_N_Fiss_3252_3286_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3185
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin137_Delay_N_Fiss_3252_3286_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3186
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin137_Delay_N_Fiss_3252_3286_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3187
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin137_Delay_N_Fiss_3252_3286_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3183
	FOR(uint32_t, __iter_init_, 0, <, 9, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin137_Delay_N_Fiss_3252_3286_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3183DUPLICATE_Splitter_3188));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3188
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3183DUPLICATE_Splitter_3188), &(SplitJoin139_FirFilter_Fiss_3253_3287_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3190
	FirFilter(&(SplitJoin139_FirFilter_Fiss_3253_3287_split[0]), &(SplitJoin139_FirFilter_Fiss_3253_3287_join[0]));
//--------------------------------
// --- init: FirFilter_3191
	FirFilter(&(SplitJoin139_FirFilter_Fiss_3253_3287_split[1]), &(SplitJoin139_FirFilter_Fiss_3253_3287_join[1]));
//--------------------------------
// --- init: FirFilter_3192
	FirFilter(&(SplitJoin139_FirFilter_Fiss_3253_3287_split[2]), &(SplitJoin139_FirFilter_Fiss_3253_3287_join[2]));
//--------------------------------
// --- init: FirFilter_3193
	FirFilter(&(SplitJoin139_FirFilter_Fiss_3253_3287_split[3]), &(SplitJoin139_FirFilter_Fiss_3253_3287_join[3]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3189
	WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin139_FirFilter_Fiss_3253_3287_join[Need Human for iter var]), &(SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[6]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3194
	FOR(uint32_t, __iter_init_, 0, <, 75, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[7]), &(SplitJoin154_Delay_N_Fiss_3254_3288_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3196
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin154_Delay_N_Fiss_3254_3288_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3197
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin154_Delay_N_Fiss_3254_3288_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3198
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin154_Delay_N_Fiss_3254_3288_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3199
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin154_Delay_N_Fiss_3254_3288_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3195
	FOR(uint32_t, __iter_init_, 0, <, 46, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin154_Delay_N_Fiss_3254_3288_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3195DUPLICATE_Splitter_3200));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3200
	FOR(uint32_t, __iter_init_, 0, <, 183, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3195DUPLICATE_Splitter_3200), &(SplitJoin156_FirFilter_Fiss_3255_3289_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3202
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin156_FirFilter_Fiss_3255_3289_split[0]), &(SplitJoin156_FirFilter_Fiss_3255_3289_join[0]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3203
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin156_FirFilter_Fiss_3255_3289_split[1]), &(SplitJoin156_FirFilter_Fiss_3255_3289_join[1]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3204
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin156_FirFilter_Fiss_3255_3289_split[2]), &(SplitJoin156_FirFilter_Fiss_3255_3289_join[2]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3205
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FirFilter(&(SplitJoin156_FirFilter_Fiss_3255_3289_split[3]), &(SplitJoin156_FirFilter_Fiss_3255_3289_join[3]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3201
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin156_FirFilter_Fiss_3255_3289_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3201DownSamp_3000));
	ENDFOR
//--------------------------------
// --- init: DownSamp_3000
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3201DownSamp_3000), &(DownSamp_3000UpSamp_3001));
	ENDFOR
//--------------------------------
// --- init: UpSamp_3001
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++)
		UpSamp(&(DownSamp_3000UpSamp_3001), &(UpSamp_3001WEIGHTED_ROUND_ROBIN_Splitter_3206));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3206
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(UpSamp_3001WEIGHTED_ROUND_ROBIN_Splitter_3206), &(SplitJoin158_Delay_N_Fiss_3256_3290_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3208
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin158_Delay_N_Fiss_3256_3290_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3209
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin158_Delay_N_Fiss_3256_3290_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3210
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin158_Delay_N_Fiss_3256_3290_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3211
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin158_Delay_N_Fiss_3256_3290_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3207
	FOR(uint32_t, __iter_init_, 0, <, 9, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin158_Delay_N_Fiss_3256_3290_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3207DUPLICATE_Splitter_3212));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3212
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3207DUPLICATE_Splitter_3212), &(SplitJoin160_FirFilter_Fiss_3257_3291_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3214
	FirFilter(&(SplitJoin160_FirFilter_Fiss_3257_3291_split[0]), &(SplitJoin160_FirFilter_Fiss_3257_3291_join[0]));
//--------------------------------
// --- init: FirFilter_3215
	FirFilter(&(SplitJoin160_FirFilter_Fiss_3257_3291_split[1]), &(SplitJoin160_FirFilter_Fiss_3257_3291_join[1]));
//--------------------------------
// --- init: FirFilter_3216
	FirFilter(&(SplitJoin160_FirFilter_Fiss_3257_3291_split[2]), &(SplitJoin160_FirFilter_Fiss_3257_3291_join[2]));
//--------------------------------
// --- init: FirFilter_3217
	 {
	FirFilter_3217_s.COEFF[0] = 56.0 ; 
	FirFilter_3217_s.COEFF[1] = 64.0 ; 
	FirFilter_3217_s.COEFF[2] = 72.0 ; 
	FirFilter_3217_s.COEFF[3] = 80.0 ; 
	FirFilter_3217_s.COEFF[4] = 88.0 ; 
	FirFilter_3217_s.COEFF[5] = 96.0 ; 
	FirFilter_3217_s.COEFF[6] = 104.0 ; 
	FirFilter_3217_s.COEFF[7] = 112.0 ; 
	FirFilter_3217_s.COEFF[8] = 120.0 ; 
	FirFilter_3217_s.COEFF[9] = 128.0 ; 
	FirFilter_3217_s.COEFF[10] = 136.0 ; 
	FirFilter_3217_s.COEFF[11] = 144.0 ; 
	FirFilter_3217_s.COEFF[12] = 152.0 ; 
	FirFilter_3217_s.COEFF[13] = 160.0 ; 
	FirFilter_3217_s.COEFF[14] = 168.0 ; 
	FirFilter_3217_s.COEFF[15] = 176.0 ; 
	FirFilter_3217_s.COEFF[16] = 184.0 ; 
	FirFilter_3217_s.COEFF[17] = 192.0 ; 
	FirFilter_3217_s.COEFF[18] = 200.0 ; 
	FirFilter_3217_s.COEFF[19] = 208.0 ; 
	FirFilter_3217_s.COEFF[20] = 216.0 ; 
	FirFilter_3217_s.COEFF[21] = 224.0 ; 
	FirFilter_3217_s.COEFF[22] = 232.0 ; 
	FirFilter_3217_s.COEFF[23] = 240.0 ; 
	FirFilter_3217_s.COEFF[24] = 248.0 ; 
	FirFilter_3217_s.COEFF[25] = 256.0 ; 
	FirFilter_3217_s.COEFF[26] = 264.0 ; 
	FirFilter_3217_s.COEFF[27] = 272.0 ; 
	FirFilter_3217_s.COEFF[28] = 280.0 ; 
	FirFilter_3217_s.COEFF[29] = 288.0 ; 
	FirFilter_3217_s.COEFF[30] = 296.0 ; 
	FirFilter_3217_s.COEFF[31] = 304.0 ; 
}
	FirFilter(&(SplitJoin160_FirFilter_Fiss_3257_3291_split[3]), &(SplitJoin160_FirFilter_Fiss_3257_3291_join[3]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3213
	WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin160_FirFilter_Fiss_3257_3291_join[Need Human for iter var]), &(SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[7]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3007
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3007WEIGHTED_ROUND_ROBIN_Splitter_3218));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3218
	WEIGHTED_ROUND_ROBIN_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3007WEIGHTED_ROUND_ROBIN_Splitter_3218), &(SplitJoin10_Combine_Fiss_3229_3292_split[Need Human for iter var]));
//--------------------------------
// --- init: Combine_3220
	Combine(&(SplitJoin10_Combine_Fiss_3229_3292_split[0]), &(SplitJoin10_Combine_Fiss_3229_3292_join[0]));
//--------------------------------
// --- init: Combine_3221
	Combine(&(SplitJoin10_Combine_Fiss_3229_3292_split[1]), &(SplitJoin10_Combine_Fiss_3229_3292_join[1]));
//--------------------------------
// --- init: Combine_3222
	Combine(&(SplitJoin10_Combine_Fiss_3229_3292_split[2]), &(SplitJoin10_Combine_Fiss_3229_3292_join[2]));
//--------------------------------
// --- init: Combine_3223
	Combine(&(SplitJoin10_Combine_Fiss_3229_3292_split[3]), &(SplitJoin10_Combine_Fiss_3229_3292_join[3]));
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3219
	WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin10_Combine_Fiss_3229_3292_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3219sink_3005));
//--------------------------------
// --- init: sink_3005
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		sink(&(WEIGHTED_ROUND_ROBIN_Joiner_3219sink_3005));
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		source_2946();
		DUPLICATE_Splitter_3006();
			WEIGHTED_ROUND_ROBIN_Splitter_3026();
				Delay_N_3028();
				Delay_N_3029();
				Delay_N_3030();
				Delay_N_3031();
			WEIGHTED_ROUND_ROBIN_Joiner_3027();
			DUPLICATE_Splitter_3032();
				FirFilter_3034();
				FirFilter_3035();
				FirFilter_3036();
				FirFilter_3037();
			WEIGHTED_ROUND_ROBIN_Joiner_3033();
			DownSamp_2951();
			UpSamp_2952();
			WEIGHTED_ROUND_ROBIN_Splitter_3038();
				Delay_N_3040();
				Delay_N_3041();
				Delay_N_3042();
				Delay_N_3043();
			WEIGHTED_ROUND_ROBIN_Joiner_3039();
			DUPLICATE_Splitter_3044();
				FirFilter_3046();
				FirFilter_3047();
				FirFilter_3048();
				FirFilter_3049();
			WEIGHTED_ROUND_ROBIN_Joiner_3045();
			WEIGHTED_ROUND_ROBIN_Splitter_3050();
				Delay_N_3052();
				Delay_N_3053();
				Delay_N_3054();
				Delay_N_3055();
			WEIGHTED_ROUND_ROBIN_Joiner_3051();
			DUPLICATE_Splitter_3056();
				FirFilter_3058();
				FirFilter_3059();
				FirFilter_3060();
				FirFilter_3061();
			WEIGHTED_ROUND_ROBIN_Joiner_3057();
			DownSamp_2958();
			UpSamp_2959();
			WEIGHTED_ROUND_ROBIN_Splitter_3062();
				Delay_N_3064();
				Delay_N_3065();
				Delay_N_3066();
				Delay_N_3067();
			WEIGHTED_ROUND_ROBIN_Joiner_3063();
			DUPLICATE_Splitter_3068();
				FirFilter_3070();
				FirFilter_3071();
				FirFilter_3072();
				FirFilter_3073();
			WEIGHTED_ROUND_ROBIN_Joiner_3069();
			WEIGHTED_ROUND_ROBIN_Splitter_3074();
				Delay_N_3076();
				Delay_N_3077();
				Delay_N_3078();
				Delay_N_3079();
			WEIGHTED_ROUND_ROBIN_Joiner_3075();
			DUPLICATE_Splitter_3080();
				FirFilter_3082();
				FirFilter_3083();
				FirFilter_3084();
				FirFilter_3085();
			WEIGHTED_ROUND_ROBIN_Joiner_3081();
			DownSamp_2965();
			UpSamp_2966();
			WEIGHTED_ROUND_ROBIN_Splitter_3086();
				Delay_N_3088();
				Delay_N_3089();
				Delay_N_3090();
				Delay_N_3091();
			WEIGHTED_ROUND_ROBIN_Joiner_3087();
			DUPLICATE_Splitter_3092();
				FirFilter_3094();
				FirFilter_3095();
				FirFilter_3096();
				FirFilter_3097();
			WEIGHTED_ROUND_ROBIN_Joiner_3093();
			WEIGHTED_ROUND_ROBIN_Splitter_3098();
				Delay_N_3100();
				Delay_N_3101();
				Delay_N_3102();
				Delay_N_3103();
			WEIGHTED_ROUND_ROBIN_Joiner_3099();
			DUPLICATE_Splitter_3104();
				FirFilter_3106();
				FirFilter_3107();
				FirFilter_3108();
				FirFilter_3109();
			WEIGHTED_ROUND_ROBIN_Joiner_3105();
			DownSamp_2972();
			UpSamp_2973();
			WEIGHTED_ROUND_ROBIN_Splitter_3110();
				Delay_N_3112();
				Delay_N_3113();
				Delay_N_3114();
				Delay_N_3115();
			WEIGHTED_ROUND_ROBIN_Joiner_3111();
			DUPLICATE_Splitter_3116();
				FirFilter_3118();
				FirFilter_3119();
				FirFilter_3120();
				FirFilter_3121();
			WEIGHTED_ROUND_ROBIN_Joiner_3117();
			WEIGHTED_ROUND_ROBIN_Splitter_3122();
				Delay_N_3124();
				Delay_N_3125();
				Delay_N_3126();
				Delay_N_3127();
			WEIGHTED_ROUND_ROBIN_Joiner_3123();
			DUPLICATE_Splitter_3128();
				FirFilter_3130();
				FirFilter_3131();
				FirFilter_3132();
				FirFilter_3133();
			WEIGHTED_ROUND_ROBIN_Joiner_3129();
			DownSamp_2979();
			UpSamp_2980();
			WEIGHTED_ROUND_ROBIN_Splitter_3134();
				Delay_N_3136();
				Delay_N_3137();
				Delay_N_3138();
				Delay_N_3139();
			WEIGHTED_ROUND_ROBIN_Joiner_3135();
			DUPLICATE_Splitter_3140();
				FirFilter_3142();
				FirFilter_3143();
				FirFilter_3144();
				FirFilter_3145();
			WEIGHTED_ROUND_ROBIN_Joiner_3141();
			WEIGHTED_ROUND_ROBIN_Splitter_3146();
				Delay_N_3148();
				Delay_N_3149();
				Delay_N_3150();
				Delay_N_3151();
			WEIGHTED_ROUND_ROBIN_Joiner_3147();
			DUPLICATE_Splitter_3152();
				FirFilter_3154();
				FirFilter_3155();
				FirFilter_3156();
				FirFilter_3157();
			WEIGHTED_ROUND_ROBIN_Joiner_3153();
			DownSamp_2986();
			UpSamp_2987();
			WEIGHTED_ROUND_ROBIN_Splitter_3158();
				Delay_N_3160();
				Delay_N_3161();
				Delay_N_3162();
				Delay_N_3163();
			WEIGHTED_ROUND_ROBIN_Joiner_3159();
			DUPLICATE_Splitter_3164();
				FirFilter_3166();
				FirFilter_3167();
				FirFilter_3168();
				FirFilter_3169();
			WEIGHTED_ROUND_ROBIN_Joiner_3165();
			WEIGHTED_ROUND_ROBIN_Splitter_3170();
				Delay_N_3172();
				Delay_N_3173();
				Delay_N_3174();
				Delay_N_3175();
			WEIGHTED_ROUND_ROBIN_Joiner_3171();
			DUPLICATE_Splitter_3176();
				FirFilter_3178();
				FirFilter_3179();
				FirFilter_3180();
				FirFilter_3181();
			WEIGHTED_ROUND_ROBIN_Joiner_3177();
			DownSamp_2993();
			UpSamp_2994();
			WEIGHTED_ROUND_ROBIN_Splitter_3182();
				Delay_N_3184();
				Delay_N_3185();
				Delay_N_3186();
				Delay_N_3187();
			WEIGHTED_ROUND_ROBIN_Joiner_3183();
			DUPLICATE_Splitter_3188();
				FirFilter_3190();
				FirFilter_3191();
				FirFilter_3192();
				FirFilter_3193();
			WEIGHTED_ROUND_ROBIN_Joiner_3189();
			WEIGHTED_ROUND_ROBIN_Splitter_3194();
				Delay_N_3196();
				Delay_N_3197();
				Delay_N_3198();
				Delay_N_3199();
			WEIGHTED_ROUND_ROBIN_Joiner_3195();
			DUPLICATE_Splitter_3200();
				FirFilter_3202();
				FirFilter_3203();
				FirFilter_3204();
				FirFilter_3205();
			WEIGHTED_ROUND_ROBIN_Joiner_3201();
			DownSamp_3000();
			UpSamp_3001();
			WEIGHTED_ROUND_ROBIN_Splitter_3206();
				Delay_N_3208();
				Delay_N_3209();
				Delay_N_3210();
				Delay_N_3211();
			WEIGHTED_ROUND_ROBIN_Joiner_3207();
			DUPLICATE_Splitter_3212();
				FirFilter_3214();
				FirFilter_3215();
				FirFilter_3216();
				FirFilter_3217();
			WEIGHTED_ROUND_ROBIN_Joiner_3213();
		WEIGHTED_ROUND_ROBIN_Joiner_3007();
		WEIGHTED_ROUND_ROBIN_Splitter_3218();
			Combine_3220();
			Combine_3221();
			Combine_3222();
			Combine_3223();
		WEIGHTED_ROUND_ROBIN_Joiner_3219();
		sink_3005();
	ENDFOR
	return EXIT_SUCCESS;
}
