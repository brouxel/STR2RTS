#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=1776 on the compile command line
#else
#if BUF_SIZEMAX < 1776
#error BUF_SIZEMAX too small, it must be at least 1776
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float current;
} source_3442_t;

typedef struct {
	float COEFF[32];
} FirFilter_3529_t;
void source(buffer_float_t *chanout);
void source_3442();
void DUPLICATE_Splitter_3502();
void WEIGHTED_ROUND_ROBIN_Splitter_3522();
void Delay_N(buffer_float_t *chanin, buffer_float_t *chanout);
void Delay_N_3524();
void Delay_N_3525();
void Delay_N_3526();
void WEIGHTED_ROUND_ROBIN_Joiner_3523();
void DUPLICATE_Splitter_3527();
void FirFilter(buffer_float_t *chanin, buffer_float_t *chanout);
void FirFilter_3529();
void FirFilter_3530();
void FirFilter_3531();
void WEIGHTED_ROUND_ROBIN_Joiner_3528();
void DownSamp(buffer_float_t *chanin, buffer_float_t *chanout);
void DownSamp_3447();
void UpSamp(buffer_float_t *chanin, buffer_float_t *chanout);
void UpSamp_3448();
void WEIGHTED_ROUND_ROBIN_Splitter_3532();
void Delay_N_3534();
void Delay_N_3535();
void Delay_N_3536();
void WEIGHTED_ROUND_ROBIN_Joiner_3533();
void DUPLICATE_Splitter_3537();
void FirFilter_3539();
void FirFilter_3540();
void FirFilter_3541();
void WEIGHTED_ROUND_ROBIN_Joiner_3538();
void WEIGHTED_ROUND_ROBIN_Splitter_3542();
void Delay_N_3544();
void Delay_N_3545();
void Delay_N_3546();
void WEIGHTED_ROUND_ROBIN_Joiner_3543();
void DUPLICATE_Splitter_3547();
void FirFilter_3549();
void FirFilter_3550();
void FirFilter_3551();
void WEIGHTED_ROUND_ROBIN_Joiner_3548();
void DownSamp_3454();
void UpSamp_3455();
void WEIGHTED_ROUND_ROBIN_Splitter_3552();
void Delay_N_3554();
void Delay_N_3555();
void Delay_N_3556();
void WEIGHTED_ROUND_ROBIN_Joiner_3553();
void DUPLICATE_Splitter_3557();
void FirFilter_3559();
void FirFilter_3560();
void FirFilter_3561();
void WEIGHTED_ROUND_ROBIN_Joiner_3558();
void WEIGHTED_ROUND_ROBIN_Splitter_3562();
void Delay_N_3564();
void Delay_N_3565();
void Delay_N_3566();
void WEIGHTED_ROUND_ROBIN_Joiner_3563();
void DUPLICATE_Splitter_3567();
void FirFilter_3569();
void FirFilter_3570();
void FirFilter_3571();
void WEIGHTED_ROUND_ROBIN_Joiner_3568();
void DownSamp_3461();
void UpSamp_3462();
void WEIGHTED_ROUND_ROBIN_Splitter_3572();
void Delay_N_3574();
void Delay_N_3575();
void Delay_N_3576();
void WEIGHTED_ROUND_ROBIN_Joiner_3573();
void DUPLICATE_Splitter_3577();
void FirFilter_3579();
void FirFilter_3580();
void FirFilter_3581();
void WEIGHTED_ROUND_ROBIN_Joiner_3578();
void WEIGHTED_ROUND_ROBIN_Splitter_3582();
void Delay_N_3584();
void Delay_N_3585();
void Delay_N_3586();
void WEIGHTED_ROUND_ROBIN_Joiner_3583();
void DUPLICATE_Splitter_3587();
void FirFilter_3589();
void FirFilter_3590();
void FirFilter_3591();
void WEIGHTED_ROUND_ROBIN_Joiner_3588();
void DownSamp_3468();
void UpSamp_3469();
void WEIGHTED_ROUND_ROBIN_Splitter_3592();
void Delay_N_3594();
void Delay_N_3595();
void Delay_N_3596();
void WEIGHTED_ROUND_ROBIN_Joiner_3593();
void DUPLICATE_Splitter_3597();
void FirFilter_3599();
void FirFilter_3600();
void FirFilter_3601();
void WEIGHTED_ROUND_ROBIN_Joiner_3598();
void WEIGHTED_ROUND_ROBIN_Splitter_3602();
void Delay_N_3604();
void Delay_N_3605();
void Delay_N_3606();
void WEIGHTED_ROUND_ROBIN_Joiner_3603();
void DUPLICATE_Splitter_3607();
void FirFilter_3609();
void FirFilter_3610();
void FirFilter_3611();
void WEIGHTED_ROUND_ROBIN_Joiner_3608();
void DownSamp_3475();
void UpSamp_3476();
void WEIGHTED_ROUND_ROBIN_Splitter_3612();
void Delay_N_3614();
void Delay_N_3615();
void Delay_N_3616();
void WEIGHTED_ROUND_ROBIN_Joiner_3613();
void DUPLICATE_Splitter_3617();
void FirFilter_3619();
void FirFilter_3620();
void FirFilter_3621();
void WEIGHTED_ROUND_ROBIN_Joiner_3618();
void WEIGHTED_ROUND_ROBIN_Splitter_3622();
void Delay_N_3624();
void Delay_N_3625();
void Delay_N_3626();
void WEIGHTED_ROUND_ROBIN_Joiner_3623();
void DUPLICATE_Splitter_3627();
void FirFilter_3629();
void FirFilter_3630();
void FirFilter_3631();
void WEIGHTED_ROUND_ROBIN_Joiner_3628();
void DownSamp_3482();
void UpSamp_3483();
void WEIGHTED_ROUND_ROBIN_Splitter_3632();
void Delay_N_3634();
void Delay_N_3635();
void Delay_N_3636();
void WEIGHTED_ROUND_ROBIN_Joiner_3633();
void DUPLICATE_Splitter_3637();
void FirFilter_3639();
void FirFilter_3640();
void FirFilter_3641();
void WEIGHTED_ROUND_ROBIN_Joiner_3638();
void WEIGHTED_ROUND_ROBIN_Splitter_3642();
void Delay_N_3644();
void Delay_N_3645();
void Delay_N_3646();
void WEIGHTED_ROUND_ROBIN_Joiner_3643();
void DUPLICATE_Splitter_3647();
void FirFilter_3649();
void FirFilter_3650();
void FirFilter_3651();
void WEIGHTED_ROUND_ROBIN_Joiner_3648();
void DownSamp_3489();
void UpSamp_3490();
void WEIGHTED_ROUND_ROBIN_Splitter_3652();
void Delay_N_3654();
void Delay_N_3655();
void Delay_N_3656();
void WEIGHTED_ROUND_ROBIN_Joiner_3653();
void DUPLICATE_Splitter_3657();
void FirFilter_3659();
void FirFilter_3660();
void FirFilter_3661();
void WEIGHTED_ROUND_ROBIN_Joiner_3658();
void WEIGHTED_ROUND_ROBIN_Splitter_3662();
void Delay_N_3664();
void Delay_N_3665();
void Delay_N_3666();
void WEIGHTED_ROUND_ROBIN_Joiner_3663();
void DUPLICATE_Splitter_3667();
void FirFilter_3669();
void FirFilter_3670();
void FirFilter_3671();
void WEIGHTED_ROUND_ROBIN_Joiner_3668();
void DownSamp_3496();
void UpSamp_3497();
void WEIGHTED_ROUND_ROBIN_Splitter_3672();
void Delay_N_3674();
void Delay_N_3675();
void Delay_N_3676();
void WEIGHTED_ROUND_ROBIN_Joiner_3673();
void DUPLICATE_Splitter_3677();
void FirFilter_3679();
void FirFilter_3680();
void FirFilter_3681();
void WEIGHTED_ROUND_ROBIN_Joiner_3678();
void WEIGHTED_ROUND_ROBIN_Joiner_3503();
void WEIGHTED_ROUND_ROBIN_Splitter_3682();
void Combine(buffer_float_t *chanin, buffer_float_t *chanout);
void Combine_3684();
void Combine_3685();
void Combine_3686();
void WEIGHTED_ROUND_ROBIN_Joiner_3683();
void sink(buffer_float_t *chanin);
void sink_3501();

#ifdef __cplusplus
}
#endif
#endif
