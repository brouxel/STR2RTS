#include "PEG3-FilterBankNew.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3583DUPLICATE_Splitter_3587;
buffer_float_t SplitJoin23_Delay_N_Fiss_3693_3727_split[3];
buffer_float_t SplitJoin0_SplitJoin0_SplitJoin0_Branches_3440_3504_3687_3722_split[8];
buffer_float_t SplitJoin23_Delay_N_Fiss_3693_3727_join[3];
buffer_float_t SplitJoin95_Delay_N_Fiss_3711_3745_split[3];
buffer_float_t SplitJoin131_FirFilter_Fiss_3720_3754_split[3];
buffer_float_t SplitJoin108_Delay_N_Fiss_3713_3747_split[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3593DUPLICATE_Splitter_3597;
buffer_float_t SplitJoin91_Delay_N_Fiss_3709_3743_split[3];
buffer_float_t DownSamp_3489UpSamp_3490;
buffer_float_t SplitJoin46_FirFilter_Fiss_3700_3734_join[3];
buffer_float_t SplitJoin131_FirFilter_Fiss_3720_3754_join[3];
buffer_float_t SplitJoin93_FirFilter_Fiss_3710_3744_split[3];
buffer_float_t SplitJoin108_Delay_N_Fiss_3713_3747_join[3];
buffer_float_t UpSamp_3455WEIGHTED_ROUND_ROBIN_Splitter_3552;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3663DUPLICATE_Splitter_3667;
buffer_float_t SplitJoin8_FirFilter_Fiss_3691_3726_join[3];
buffer_float_t DownSamp_3496UpSamp_3497;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3573DUPLICATE_Splitter_3577;
buffer_float_t UpSamp_3462WEIGHTED_ROUND_ROBIN_Splitter_3572;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3543DUPLICATE_Splitter_3547;
buffer_float_t SplitJoin27_Delay_N_Fiss_3695_3729_join[3];
buffer_float_t SplitJoin61_Delay_N_Fiss_3703_3737_split[3];
buffer_float_t SplitJoin112_Delay_N_Fiss_3715_3749_split[3];
buffer_float_t SplitJoin63_FirFilter_Fiss_3704_3738_split[3];
buffer_float_t SplitJoin44_Delay_N_Fiss_3699_3733_split[3];
buffer_float_t SplitJoin114_FirFilter_Fiss_3716_3750_split[3];
buffer_float_t SplitJoin25_FirFilter_Fiss_3694_3728_join[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3553DUPLICATE_Splitter_3557;
buffer_float_t UpSamp_3490WEIGHTED_ROUND_ROBIN_Splitter_3652;
buffer_float_t DownSamp_3482UpSamp_3483;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3653DUPLICATE_Splitter_3657;
buffer_float_t SplitJoin78_Delay_N_Fiss_3707_3741_join[3];
buffer_float_t SplitJoin97_FirFilter_Fiss_3712_3746_split[3];
buffer_float_t SplitJoin44_Delay_N_Fiss_3699_3733_join[3];
buffer_float_t SplitJoin61_Delay_N_Fiss_3703_3737_join[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3668DownSamp_3496;
buffer_float_t DownSamp_3475UpSamp_3476;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3623DUPLICATE_Splitter_3627;
buffer_float_t SplitJoin80_FirFilter_Fiss_3708_3742_join[3];
buffer_float_t UpSamp_3483WEIGHTED_ROUND_ROBIN_Splitter_3632;
buffer_float_t SplitJoin110_FirFilter_Fiss_3714_3748_join[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3683sink_3501;
buffer_float_t SplitJoin76_FirFilter_Fiss_3706_3740_join[3];
buffer_float_t UpSamp_3476WEIGHTED_ROUND_ROBIN_Splitter_3612;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3608DownSamp_3475;
buffer_float_t SplitJoin127_FirFilter_Fiss_3718_3752_join[3];
buffer_float_t SplitJoin2_Delay_N_Fiss_3688_3723_split[3];
buffer_float_t DownSamp_3468UpSamp_3469;
buffer_float_t SplitJoin110_FirFilter_Fiss_3714_3748_split[3];
buffer_float_t SplitJoin59_FirFilter_Fiss_3702_3736_split[3];
buffer_float_t SplitJoin6_Delay_N_Fiss_3690_3725_split[3];
buffer_float_t SplitJoin125_Delay_N_Fiss_3717_3751_split[3];
buffer_float_t UpSamp_3448WEIGHTED_ROUND_ROBIN_Splitter_3532;
buffer_float_t SplitJoin42_FirFilter_Fiss_3698_3732_split[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3603DUPLICATE_Splitter_3607;
buffer_float_t SplitJoin74_Delay_N_Fiss_3705_3739_split[3];
buffer_float_t SplitJoin93_FirFilter_Fiss_3710_3744_join[3];
buffer_float_t SplitJoin6_Delay_N_Fiss_3690_3725_join[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3628DownSamp_3482;
buffer_float_t SplitJoin63_FirFilter_Fiss_3704_3738_join[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3568DownSamp_3461;
buffer_float_t SplitJoin127_FirFilter_Fiss_3718_3752_split[3];
buffer_float_t SplitJoin40_Delay_N_Fiss_3697_3731_join[3];
buffer_float_t SplitJoin76_FirFilter_Fiss_3706_3740_split[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3613DUPLICATE_Splitter_3617;
buffer_float_t SplitJoin97_FirFilter_Fiss_3712_3746_join[3];
buffer_float_t SplitJoin125_Delay_N_Fiss_3717_3751_join[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3523DUPLICATE_Splitter_3527;
buffer_float_t source_3442DUPLICATE_Splitter_3502;
buffer_float_t SplitJoin91_Delay_N_Fiss_3709_3743_join[3];
buffer_float_t SplitJoin129_Delay_N_Fiss_3719_3753_split[3];
buffer_float_t SplitJoin112_Delay_N_Fiss_3715_3749_join[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3548DownSamp_3454;
buffer_float_t SplitJoin74_Delay_N_Fiss_3705_3739_join[3];
buffer_float_t SplitJoin0_SplitJoin0_SplitJoin0_Branches_3440_3504_3687_3722_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3633DUPLICATE_Splitter_3637;
buffer_float_t SplitJoin57_Delay_N_Fiss_3701_3735_split[3];
buffer_float_t DownSamp_3454UpSamp_3455;
buffer_float_t SplitJoin57_Delay_N_Fiss_3701_3735_join[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3648DownSamp_3489;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3643DUPLICATE_Splitter_3647;
buffer_float_t SplitJoin42_FirFilter_Fiss_3698_3732_join[3];
buffer_float_t SplitJoin29_FirFilter_Fiss_3696_3730_join[3];
buffer_float_t SplitJoin78_Delay_N_Fiss_3707_3741_split[3];
buffer_float_t DownSamp_3447UpSamp_3448;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3588DownSamp_3468;
buffer_float_t SplitJoin59_FirFilter_Fiss_3702_3736_join[3];
buffer_float_t SplitJoin2_Delay_N_Fiss_3688_3723_join[3];
buffer_float_t UpSamp_3469WEIGHTED_ROUND_ROBIN_Splitter_3592;
buffer_float_t SplitJoin4_FirFilter_Fiss_3689_3724_split[3];
buffer_float_t SplitJoin29_FirFilter_Fiss_3696_3730_split[3];
buffer_float_t SplitJoin4_FirFilter_Fiss_3689_3724_join[3];
buffer_float_t SplitJoin10_Combine_Fiss_3692_3755_join[3];
buffer_float_t SplitJoin8_FirFilter_Fiss_3691_3726_split[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3563DUPLICATE_Splitter_3567;
buffer_float_t SplitJoin10_Combine_Fiss_3692_3755_split[3];
buffer_float_t SplitJoin46_FirFilter_Fiss_3700_3734_split[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3528DownSamp_3447;
buffer_float_t SplitJoin25_FirFilter_Fiss_3694_3728_split[3];
buffer_float_t SplitJoin40_Delay_N_Fiss_3697_3731_split[3];
buffer_float_t DownSamp_3461UpSamp_3462;
buffer_float_t SplitJoin129_Delay_N_Fiss_3719_3753_join[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3533DUPLICATE_Splitter_3537;
buffer_float_t UpSamp_3497WEIGHTED_ROUND_ROBIN_Splitter_3672;
buffer_float_t SplitJoin80_FirFilter_Fiss_3708_3742_split[3];
buffer_float_t SplitJoin27_Delay_N_Fiss_3695_3729_split[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3673DUPLICATE_Splitter_3677;
buffer_float_t SplitJoin95_Delay_N_Fiss_3711_3745_join[3];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3503WEIGHTED_ROUND_ROBIN_Splitter_3682;
buffer_float_t SplitJoin114_FirFilter_Fiss_3716_3750_join[3];


source_3442_t source_3442_s;
FirFilter_3529_t FirFilter_3529_s;
FirFilter_3529_t FirFilter_3530_s;
FirFilter_3529_t FirFilter_3531_s;
FirFilter_3529_t FirFilter_3539_s;
FirFilter_3529_t FirFilter_3540_s;
FirFilter_3529_t FirFilter_3541_s;
FirFilter_3529_t FirFilter_3549_s;
FirFilter_3529_t FirFilter_3550_s;
FirFilter_3529_t FirFilter_3551_s;
FirFilter_3529_t FirFilter_3559_s;
FirFilter_3529_t FirFilter_3560_s;
FirFilter_3529_t FirFilter_3561_s;
FirFilter_3529_t FirFilter_3569_s;
FirFilter_3529_t FirFilter_3570_s;
FirFilter_3529_t FirFilter_3571_s;
FirFilter_3529_t FirFilter_3579_s;
FirFilter_3529_t FirFilter_3580_s;
FirFilter_3529_t FirFilter_3581_s;
FirFilter_3529_t FirFilter_3589_s;
FirFilter_3529_t FirFilter_3590_s;
FirFilter_3529_t FirFilter_3591_s;
FirFilter_3529_t FirFilter_3599_s;
FirFilter_3529_t FirFilter_3600_s;
FirFilter_3529_t FirFilter_3601_s;
FirFilter_3529_t FirFilter_3609_s;
FirFilter_3529_t FirFilter_3610_s;
FirFilter_3529_t FirFilter_3611_s;
FirFilter_3529_t FirFilter_3619_s;
FirFilter_3529_t FirFilter_3620_s;
FirFilter_3529_t FirFilter_3621_s;
FirFilter_3529_t FirFilter_3629_s;
FirFilter_3529_t FirFilter_3630_s;
FirFilter_3529_t FirFilter_3631_s;
FirFilter_3529_t FirFilter_3639_s;
FirFilter_3529_t FirFilter_3640_s;
FirFilter_3529_t FirFilter_3641_s;
FirFilter_3529_t FirFilter_3649_s;
FirFilter_3529_t FirFilter_3650_s;
FirFilter_3529_t FirFilter_3651_s;
FirFilter_3529_t FirFilter_3659_s;
FirFilter_3529_t FirFilter_3660_s;
FirFilter_3529_t FirFilter_3661_s;
FirFilter_3529_t FirFilter_3669_s;
FirFilter_3529_t FirFilter_3670_s;
FirFilter_3529_t FirFilter_3671_s;
FirFilter_3529_t FirFilter_3679_s;
FirFilter_3529_t FirFilter_3680_s;
FirFilter_3529_t FirFilter_3681_s;

void source(buffer_float_t *chanout) {
		push_float(&(*chanout), source_3442_s.current) ; 
		if((source_3442_s.current > 1000.0)) {
			source_3442_s.current = 0.0 ; 
		}
		else {
			source_3442_s.current = (source_3442_s.current + 1.0) ; 
		}
	}


void source_3442() {
	FOR(uint32_t, __iter_steady_, 0, <, 24, __iter_steady_++)
		source(&(source_3442DUPLICATE_Splitter_3502));
	ENDFOR
}

void Delay_N(buffer_float_t *chanin, buffer_float_t *chanout) {
	}


void Delay_N_3524() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin2_Delay_N_Fiss_3688_3723_split[0]), &(SplitJoin2_Delay_N_Fiss_3688_3723_join[0]));
	ENDFOR
}

void Delay_N_3525() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin2_Delay_N_Fiss_3688_3723_split[1]), &(SplitJoin2_Delay_N_Fiss_3688_3723_join[1]));
	ENDFOR
}

void Delay_N_3526() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin2_Delay_N_Fiss_3688_3723_split[2]), &(SplitJoin2_Delay_N_Fiss_3688_3723_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3522() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&SplitJoin2_Delay_N_Fiss_3688_3723_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3440_3504_3687_3722_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3523() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3523DUPLICATE_Splitter_3527, pop_float(&SplitJoin2_Delay_N_Fiss_3688_3723_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&(*chanin), i) * FirFilter_3529_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&(*chanin)) ; 
		push_float(&(*chanout), sum) ; 
 {
		FOR(int, streamItVar645, 0,  < , 2, streamItVar645++) {
			pop_void(&(*chanin)) ; 
		}
		ENDFOR
	}
	}


void FirFilter_3529() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin4_FirFilter_Fiss_3689_3724_split[0]), &(SplitJoin4_FirFilter_Fiss_3689_3724_join[0]));
	ENDFOR
}

void FirFilter_3530() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin4_FirFilter_Fiss_3689_3724_split[1]), &(SplitJoin4_FirFilter_Fiss_3689_3724_join[1]));
	ENDFOR
}

void FirFilter_3531() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin4_FirFilter_Fiss_3689_3724_split[2]), &(SplitJoin4_FirFilter_Fiss_3689_3724_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_3527() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 24, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3523DUPLICATE_Splitter_3527);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_float(&SplitJoin4_FirFilter_Fiss_3689_3724_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3528() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3528DownSamp_3447, pop_float(&SplitJoin4_FirFilter_Fiss_3689_3724_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp(buffer_float_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_float(&(*chanin))) ; 
		FOR(int, i, 0,  < , 7, i++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void DownSamp_3447() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3528DownSamp_3447), &(DownSamp_3447UpSamp_3448));
	ENDFOR
}

void UpSamp(buffer_float_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_float(&(*chanin))) ; 
		FOR(int, i, 0,  < , 7, i++) {
			push_float(&(*chanout), 0.0) ; 
		}
		ENDFOR
	}


void UpSamp_3448() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		UpSamp(&(DownSamp_3447UpSamp_3448), &(UpSamp_3448WEIGHTED_ROUND_ROBIN_Splitter_3532));
	ENDFOR
}

void Delay_N_3534() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin6_Delay_N_Fiss_3690_3725_split[0]), &(SplitJoin6_Delay_N_Fiss_3690_3725_join[0]));
	ENDFOR
}

void Delay_N_3535() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin6_Delay_N_Fiss_3690_3725_split[1]), &(SplitJoin6_Delay_N_Fiss_3690_3725_join[1]));
	ENDFOR
}

void Delay_N_3536() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin6_Delay_N_Fiss_3690_3725_split[2]), &(SplitJoin6_Delay_N_Fiss_3690_3725_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3532() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&SplitJoin6_Delay_N_Fiss_3690_3725_split[__iter_], pop_float(&UpSamp_3448WEIGHTED_ROUND_ROBIN_Splitter_3532));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3533() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3533DUPLICATE_Splitter_3537, pop_float(&SplitJoin6_Delay_N_Fiss_3690_3725_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3539() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin8_FirFilter_Fiss_3691_3726_split[0]), &(SplitJoin8_FirFilter_Fiss_3691_3726_join[0]));
	ENDFOR
}

void FirFilter_3540() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin8_FirFilter_Fiss_3691_3726_split[1]), &(SplitJoin8_FirFilter_Fiss_3691_3726_join[1]));
	ENDFOR
}

void FirFilter_3541() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin8_FirFilter_Fiss_3691_3726_split[2]), &(SplitJoin8_FirFilter_Fiss_3691_3726_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_3537() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 24, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3533DUPLICATE_Splitter_3537);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_float(&SplitJoin8_FirFilter_Fiss_3691_3726_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3538() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3440_3504_3687_3722_join[0], pop_float(&SplitJoin8_FirFilter_Fiss_3691_3726_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Delay_N_3544() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin23_Delay_N_Fiss_3693_3727_split[0]), &(SplitJoin23_Delay_N_Fiss_3693_3727_join[0]));
	ENDFOR
}

void Delay_N_3545() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin23_Delay_N_Fiss_3693_3727_split[1]), &(SplitJoin23_Delay_N_Fiss_3693_3727_join[1]));
	ENDFOR
}

void Delay_N_3546() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin23_Delay_N_Fiss_3693_3727_split[2]), &(SplitJoin23_Delay_N_Fiss_3693_3727_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3542() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&SplitJoin23_Delay_N_Fiss_3693_3727_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3440_3504_3687_3722_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3543() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3543DUPLICATE_Splitter_3547, pop_float(&SplitJoin23_Delay_N_Fiss_3693_3727_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3549() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin25_FirFilter_Fiss_3694_3728_split[0]), &(SplitJoin25_FirFilter_Fiss_3694_3728_join[0]));
	ENDFOR
}

void FirFilter_3550() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin25_FirFilter_Fiss_3694_3728_split[1]), &(SplitJoin25_FirFilter_Fiss_3694_3728_join[1]));
	ENDFOR
}

void FirFilter_3551() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin25_FirFilter_Fiss_3694_3728_split[2]), &(SplitJoin25_FirFilter_Fiss_3694_3728_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_3547() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 24, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3543DUPLICATE_Splitter_3547);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_float(&SplitJoin25_FirFilter_Fiss_3694_3728_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3548() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3548DownSamp_3454, pop_float(&SplitJoin25_FirFilter_Fiss_3694_3728_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp_3454() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3548DownSamp_3454), &(DownSamp_3454UpSamp_3455));
	ENDFOR
}

void UpSamp_3455() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		UpSamp(&(DownSamp_3454UpSamp_3455), &(UpSamp_3455WEIGHTED_ROUND_ROBIN_Splitter_3552));
	ENDFOR
}

void Delay_N_3554() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin27_Delay_N_Fiss_3695_3729_split[0]), &(SplitJoin27_Delay_N_Fiss_3695_3729_join[0]));
	ENDFOR
}

void Delay_N_3555() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin27_Delay_N_Fiss_3695_3729_split[1]), &(SplitJoin27_Delay_N_Fiss_3695_3729_join[1]));
	ENDFOR
}

void Delay_N_3556() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin27_Delay_N_Fiss_3695_3729_split[2]), &(SplitJoin27_Delay_N_Fiss_3695_3729_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3552() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&SplitJoin27_Delay_N_Fiss_3695_3729_split[__iter_], pop_float(&UpSamp_3455WEIGHTED_ROUND_ROBIN_Splitter_3552));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3553() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3553DUPLICATE_Splitter_3557, pop_float(&SplitJoin27_Delay_N_Fiss_3695_3729_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3559() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin29_FirFilter_Fiss_3696_3730_split[0]), &(SplitJoin29_FirFilter_Fiss_3696_3730_join[0]));
	ENDFOR
}

void FirFilter_3560() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin29_FirFilter_Fiss_3696_3730_split[1]), &(SplitJoin29_FirFilter_Fiss_3696_3730_join[1]));
	ENDFOR
}

void FirFilter_3561() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin29_FirFilter_Fiss_3696_3730_split[2]), &(SplitJoin29_FirFilter_Fiss_3696_3730_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_3557() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 24, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3553DUPLICATE_Splitter_3557);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_float(&SplitJoin29_FirFilter_Fiss_3696_3730_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3558() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3440_3504_3687_3722_join[1], pop_float(&SplitJoin29_FirFilter_Fiss_3696_3730_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Delay_N_3564() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin40_Delay_N_Fiss_3697_3731_split[0]), &(SplitJoin40_Delay_N_Fiss_3697_3731_join[0]));
	ENDFOR
}

void Delay_N_3565() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin40_Delay_N_Fiss_3697_3731_split[1]), &(SplitJoin40_Delay_N_Fiss_3697_3731_join[1]));
	ENDFOR
}

void Delay_N_3566() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin40_Delay_N_Fiss_3697_3731_split[2]), &(SplitJoin40_Delay_N_Fiss_3697_3731_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3562() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&SplitJoin40_Delay_N_Fiss_3697_3731_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3440_3504_3687_3722_split[2]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3563() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3563DUPLICATE_Splitter_3567, pop_float(&SplitJoin40_Delay_N_Fiss_3697_3731_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3569() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin42_FirFilter_Fiss_3698_3732_split[0]), &(SplitJoin42_FirFilter_Fiss_3698_3732_join[0]));
	ENDFOR
}

void FirFilter_3570() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin42_FirFilter_Fiss_3698_3732_split[1]), &(SplitJoin42_FirFilter_Fiss_3698_3732_join[1]));
	ENDFOR
}

void FirFilter_3571() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin42_FirFilter_Fiss_3698_3732_split[2]), &(SplitJoin42_FirFilter_Fiss_3698_3732_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_3567() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 24, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3563DUPLICATE_Splitter_3567);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_float(&SplitJoin42_FirFilter_Fiss_3698_3732_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3568() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3568DownSamp_3461, pop_float(&SplitJoin42_FirFilter_Fiss_3698_3732_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp_3461() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3568DownSamp_3461), &(DownSamp_3461UpSamp_3462));
	ENDFOR
}

void UpSamp_3462() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		UpSamp(&(DownSamp_3461UpSamp_3462), &(UpSamp_3462WEIGHTED_ROUND_ROBIN_Splitter_3572));
	ENDFOR
}

void Delay_N_3574() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin44_Delay_N_Fiss_3699_3733_split[0]), &(SplitJoin44_Delay_N_Fiss_3699_3733_join[0]));
	ENDFOR
}

void Delay_N_3575() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin44_Delay_N_Fiss_3699_3733_split[1]), &(SplitJoin44_Delay_N_Fiss_3699_3733_join[1]));
	ENDFOR
}

void Delay_N_3576() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin44_Delay_N_Fiss_3699_3733_split[2]), &(SplitJoin44_Delay_N_Fiss_3699_3733_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3572() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&SplitJoin44_Delay_N_Fiss_3699_3733_split[__iter_], pop_float(&UpSamp_3462WEIGHTED_ROUND_ROBIN_Splitter_3572));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3573() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3573DUPLICATE_Splitter_3577, pop_float(&SplitJoin44_Delay_N_Fiss_3699_3733_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3579() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin46_FirFilter_Fiss_3700_3734_split[0]), &(SplitJoin46_FirFilter_Fiss_3700_3734_join[0]));
	ENDFOR
}

void FirFilter_3580() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin46_FirFilter_Fiss_3700_3734_split[1]), &(SplitJoin46_FirFilter_Fiss_3700_3734_join[1]));
	ENDFOR
}

void FirFilter_3581() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin46_FirFilter_Fiss_3700_3734_split[2]), &(SplitJoin46_FirFilter_Fiss_3700_3734_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_3577() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 24, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3573DUPLICATE_Splitter_3577);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_float(&SplitJoin46_FirFilter_Fiss_3700_3734_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3578() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3440_3504_3687_3722_join[2], pop_float(&SplitJoin46_FirFilter_Fiss_3700_3734_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Delay_N_3584() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin57_Delay_N_Fiss_3701_3735_split[0]), &(SplitJoin57_Delay_N_Fiss_3701_3735_join[0]));
	ENDFOR
}

void Delay_N_3585() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin57_Delay_N_Fiss_3701_3735_split[1]), &(SplitJoin57_Delay_N_Fiss_3701_3735_join[1]));
	ENDFOR
}

void Delay_N_3586() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin57_Delay_N_Fiss_3701_3735_split[2]), &(SplitJoin57_Delay_N_Fiss_3701_3735_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3582() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&SplitJoin57_Delay_N_Fiss_3701_3735_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3440_3504_3687_3722_split[3]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3583() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3583DUPLICATE_Splitter_3587, pop_float(&SplitJoin57_Delay_N_Fiss_3701_3735_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3589() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin59_FirFilter_Fiss_3702_3736_split[0]), &(SplitJoin59_FirFilter_Fiss_3702_3736_join[0]));
	ENDFOR
}

void FirFilter_3590() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin59_FirFilter_Fiss_3702_3736_split[1]), &(SplitJoin59_FirFilter_Fiss_3702_3736_join[1]));
	ENDFOR
}

void FirFilter_3591() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin59_FirFilter_Fiss_3702_3736_split[2]), &(SplitJoin59_FirFilter_Fiss_3702_3736_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_3587() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 24, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3583DUPLICATE_Splitter_3587);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_float(&SplitJoin59_FirFilter_Fiss_3702_3736_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3588() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3588DownSamp_3468, pop_float(&SplitJoin59_FirFilter_Fiss_3702_3736_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp_3468() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3588DownSamp_3468), &(DownSamp_3468UpSamp_3469));
	ENDFOR
}

void UpSamp_3469() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		UpSamp(&(DownSamp_3468UpSamp_3469), &(UpSamp_3469WEIGHTED_ROUND_ROBIN_Splitter_3592));
	ENDFOR
}

void Delay_N_3594() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin61_Delay_N_Fiss_3703_3737_split[0]), &(SplitJoin61_Delay_N_Fiss_3703_3737_join[0]));
	ENDFOR
}

void Delay_N_3595() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin61_Delay_N_Fiss_3703_3737_split[1]), &(SplitJoin61_Delay_N_Fiss_3703_3737_join[1]));
	ENDFOR
}

void Delay_N_3596() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin61_Delay_N_Fiss_3703_3737_split[2]), &(SplitJoin61_Delay_N_Fiss_3703_3737_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3592() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&SplitJoin61_Delay_N_Fiss_3703_3737_split[__iter_], pop_float(&UpSamp_3469WEIGHTED_ROUND_ROBIN_Splitter_3592));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3593() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3593DUPLICATE_Splitter_3597, pop_float(&SplitJoin61_Delay_N_Fiss_3703_3737_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3599() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin63_FirFilter_Fiss_3704_3738_split[0]), &(SplitJoin63_FirFilter_Fiss_3704_3738_join[0]));
	ENDFOR
}

void FirFilter_3600() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin63_FirFilter_Fiss_3704_3738_split[1]), &(SplitJoin63_FirFilter_Fiss_3704_3738_join[1]));
	ENDFOR
}

void FirFilter_3601() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin63_FirFilter_Fiss_3704_3738_split[2]), &(SplitJoin63_FirFilter_Fiss_3704_3738_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_3597() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 24, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3593DUPLICATE_Splitter_3597);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_float(&SplitJoin63_FirFilter_Fiss_3704_3738_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3598() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3440_3504_3687_3722_join[3], pop_float(&SplitJoin63_FirFilter_Fiss_3704_3738_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Delay_N_3604() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin74_Delay_N_Fiss_3705_3739_split[0]), &(SplitJoin74_Delay_N_Fiss_3705_3739_join[0]));
	ENDFOR
}

void Delay_N_3605() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin74_Delay_N_Fiss_3705_3739_split[1]), &(SplitJoin74_Delay_N_Fiss_3705_3739_join[1]));
	ENDFOR
}

void Delay_N_3606() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin74_Delay_N_Fiss_3705_3739_split[2]), &(SplitJoin74_Delay_N_Fiss_3705_3739_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3602() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&SplitJoin74_Delay_N_Fiss_3705_3739_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3440_3504_3687_3722_split[4]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3603() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3603DUPLICATE_Splitter_3607, pop_float(&SplitJoin74_Delay_N_Fiss_3705_3739_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3609() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin76_FirFilter_Fiss_3706_3740_split[0]), &(SplitJoin76_FirFilter_Fiss_3706_3740_join[0]));
	ENDFOR
}

void FirFilter_3610() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin76_FirFilter_Fiss_3706_3740_split[1]), &(SplitJoin76_FirFilter_Fiss_3706_3740_join[1]));
	ENDFOR
}

void FirFilter_3611() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin76_FirFilter_Fiss_3706_3740_split[2]), &(SplitJoin76_FirFilter_Fiss_3706_3740_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_3607() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 24, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3603DUPLICATE_Splitter_3607);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_float(&SplitJoin76_FirFilter_Fiss_3706_3740_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3608() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3608DownSamp_3475, pop_float(&SplitJoin76_FirFilter_Fiss_3706_3740_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp_3475() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3608DownSamp_3475), &(DownSamp_3475UpSamp_3476));
	ENDFOR
}

void UpSamp_3476() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		UpSamp(&(DownSamp_3475UpSamp_3476), &(UpSamp_3476WEIGHTED_ROUND_ROBIN_Splitter_3612));
	ENDFOR
}

void Delay_N_3614() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin78_Delay_N_Fiss_3707_3741_split[0]), &(SplitJoin78_Delay_N_Fiss_3707_3741_join[0]));
	ENDFOR
}

void Delay_N_3615() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin78_Delay_N_Fiss_3707_3741_split[1]), &(SplitJoin78_Delay_N_Fiss_3707_3741_join[1]));
	ENDFOR
}

void Delay_N_3616() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin78_Delay_N_Fiss_3707_3741_split[2]), &(SplitJoin78_Delay_N_Fiss_3707_3741_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3612() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&SplitJoin78_Delay_N_Fiss_3707_3741_split[__iter_], pop_float(&UpSamp_3476WEIGHTED_ROUND_ROBIN_Splitter_3612));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3613() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3613DUPLICATE_Splitter_3617, pop_float(&SplitJoin78_Delay_N_Fiss_3707_3741_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3619() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin80_FirFilter_Fiss_3708_3742_split[0]), &(SplitJoin80_FirFilter_Fiss_3708_3742_join[0]));
	ENDFOR
}

void FirFilter_3620() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin80_FirFilter_Fiss_3708_3742_split[1]), &(SplitJoin80_FirFilter_Fiss_3708_3742_join[1]));
	ENDFOR
}

void FirFilter_3621() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin80_FirFilter_Fiss_3708_3742_split[2]), &(SplitJoin80_FirFilter_Fiss_3708_3742_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_3617() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 24, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3613DUPLICATE_Splitter_3617);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_float(&SplitJoin80_FirFilter_Fiss_3708_3742_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3618() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3440_3504_3687_3722_join[4], pop_float(&SplitJoin80_FirFilter_Fiss_3708_3742_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Delay_N_3624() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin91_Delay_N_Fiss_3709_3743_split[0]), &(SplitJoin91_Delay_N_Fiss_3709_3743_join[0]));
	ENDFOR
}

void Delay_N_3625() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin91_Delay_N_Fiss_3709_3743_split[1]), &(SplitJoin91_Delay_N_Fiss_3709_3743_join[1]));
	ENDFOR
}

void Delay_N_3626() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin91_Delay_N_Fiss_3709_3743_split[2]), &(SplitJoin91_Delay_N_Fiss_3709_3743_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3622() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&SplitJoin91_Delay_N_Fiss_3709_3743_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3440_3504_3687_3722_split[5]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3623() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3623DUPLICATE_Splitter_3627, pop_float(&SplitJoin91_Delay_N_Fiss_3709_3743_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3629() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin93_FirFilter_Fiss_3710_3744_split[0]), &(SplitJoin93_FirFilter_Fiss_3710_3744_join[0]));
	ENDFOR
}

void FirFilter_3630() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin93_FirFilter_Fiss_3710_3744_split[1]), &(SplitJoin93_FirFilter_Fiss_3710_3744_join[1]));
	ENDFOR
}

void FirFilter_3631() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin93_FirFilter_Fiss_3710_3744_split[2]), &(SplitJoin93_FirFilter_Fiss_3710_3744_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_3627() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 24, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3623DUPLICATE_Splitter_3627);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_float(&SplitJoin93_FirFilter_Fiss_3710_3744_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3628() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3628DownSamp_3482, pop_float(&SplitJoin93_FirFilter_Fiss_3710_3744_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp_3482() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3628DownSamp_3482), &(DownSamp_3482UpSamp_3483));
	ENDFOR
}

void UpSamp_3483() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		UpSamp(&(DownSamp_3482UpSamp_3483), &(UpSamp_3483WEIGHTED_ROUND_ROBIN_Splitter_3632));
	ENDFOR
}

void Delay_N_3634() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin95_Delay_N_Fiss_3711_3745_split[0]), &(SplitJoin95_Delay_N_Fiss_3711_3745_join[0]));
	ENDFOR
}

void Delay_N_3635() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin95_Delay_N_Fiss_3711_3745_split[1]), &(SplitJoin95_Delay_N_Fiss_3711_3745_join[1]));
	ENDFOR
}

void Delay_N_3636() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin95_Delay_N_Fiss_3711_3745_split[2]), &(SplitJoin95_Delay_N_Fiss_3711_3745_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3632() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&SplitJoin95_Delay_N_Fiss_3711_3745_split[__iter_], pop_float(&UpSamp_3483WEIGHTED_ROUND_ROBIN_Splitter_3632));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3633() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3633DUPLICATE_Splitter_3637, pop_float(&SplitJoin95_Delay_N_Fiss_3711_3745_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3639() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin97_FirFilter_Fiss_3712_3746_split[0]), &(SplitJoin97_FirFilter_Fiss_3712_3746_join[0]));
	ENDFOR
}

void FirFilter_3640() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin97_FirFilter_Fiss_3712_3746_split[1]), &(SplitJoin97_FirFilter_Fiss_3712_3746_join[1]));
	ENDFOR
}

void FirFilter_3641() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin97_FirFilter_Fiss_3712_3746_split[2]), &(SplitJoin97_FirFilter_Fiss_3712_3746_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_3637() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 24, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3633DUPLICATE_Splitter_3637);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_float(&SplitJoin97_FirFilter_Fiss_3712_3746_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3638() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3440_3504_3687_3722_join[5], pop_float(&SplitJoin97_FirFilter_Fiss_3712_3746_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Delay_N_3644() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin108_Delay_N_Fiss_3713_3747_split[0]), &(SplitJoin108_Delay_N_Fiss_3713_3747_join[0]));
	ENDFOR
}

void Delay_N_3645() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin108_Delay_N_Fiss_3713_3747_split[1]), &(SplitJoin108_Delay_N_Fiss_3713_3747_join[1]));
	ENDFOR
}

void Delay_N_3646() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin108_Delay_N_Fiss_3713_3747_split[2]), &(SplitJoin108_Delay_N_Fiss_3713_3747_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3642() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&SplitJoin108_Delay_N_Fiss_3713_3747_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3440_3504_3687_3722_split[6]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3643() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3643DUPLICATE_Splitter_3647, pop_float(&SplitJoin108_Delay_N_Fiss_3713_3747_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3649() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin110_FirFilter_Fiss_3714_3748_split[0]), &(SplitJoin110_FirFilter_Fiss_3714_3748_join[0]));
	ENDFOR
}

void FirFilter_3650() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin110_FirFilter_Fiss_3714_3748_split[1]), &(SplitJoin110_FirFilter_Fiss_3714_3748_join[1]));
	ENDFOR
}

void FirFilter_3651() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin110_FirFilter_Fiss_3714_3748_split[2]), &(SplitJoin110_FirFilter_Fiss_3714_3748_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_3647() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 24, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3643DUPLICATE_Splitter_3647);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_float(&SplitJoin110_FirFilter_Fiss_3714_3748_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3648() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3648DownSamp_3489, pop_float(&SplitJoin110_FirFilter_Fiss_3714_3748_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp_3489() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3648DownSamp_3489), &(DownSamp_3489UpSamp_3490));
	ENDFOR
}

void UpSamp_3490() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		UpSamp(&(DownSamp_3489UpSamp_3490), &(UpSamp_3490WEIGHTED_ROUND_ROBIN_Splitter_3652));
	ENDFOR
}

void Delay_N_3654() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin112_Delay_N_Fiss_3715_3749_split[0]), &(SplitJoin112_Delay_N_Fiss_3715_3749_join[0]));
	ENDFOR
}

void Delay_N_3655() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin112_Delay_N_Fiss_3715_3749_split[1]), &(SplitJoin112_Delay_N_Fiss_3715_3749_join[1]));
	ENDFOR
}

void Delay_N_3656() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin112_Delay_N_Fiss_3715_3749_split[2]), &(SplitJoin112_Delay_N_Fiss_3715_3749_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3652() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&SplitJoin112_Delay_N_Fiss_3715_3749_split[__iter_], pop_float(&UpSamp_3490WEIGHTED_ROUND_ROBIN_Splitter_3652));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3653() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3653DUPLICATE_Splitter_3657, pop_float(&SplitJoin112_Delay_N_Fiss_3715_3749_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3659() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin114_FirFilter_Fiss_3716_3750_split[0]), &(SplitJoin114_FirFilter_Fiss_3716_3750_join[0]));
	ENDFOR
}

void FirFilter_3660() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin114_FirFilter_Fiss_3716_3750_split[1]), &(SplitJoin114_FirFilter_Fiss_3716_3750_join[1]));
	ENDFOR
}

void FirFilter_3661() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin114_FirFilter_Fiss_3716_3750_split[2]), &(SplitJoin114_FirFilter_Fiss_3716_3750_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_3657() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 24, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3653DUPLICATE_Splitter_3657);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_float(&SplitJoin114_FirFilter_Fiss_3716_3750_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3658() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3440_3504_3687_3722_join[6], pop_float(&SplitJoin114_FirFilter_Fiss_3716_3750_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Delay_N_3664() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin125_Delay_N_Fiss_3717_3751_split[0]), &(SplitJoin125_Delay_N_Fiss_3717_3751_join[0]));
	ENDFOR
}

void Delay_N_3665() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin125_Delay_N_Fiss_3717_3751_split[1]), &(SplitJoin125_Delay_N_Fiss_3717_3751_join[1]));
	ENDFOR
}

void Delay_N_3666() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin125_Delay_N_Fiss_3717_3751_split[2]), &(SplitJoin125_Delay_N_Fiss_3717_3751_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3662() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&SplitJoin125_Delay_N_Fiss_3717_3751_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3440_3504_3687_3722_split[7]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3663() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3663DUPLICATE_Splitter_3667, pop_float(&SplitJoin125_Delay_N_Fiss_3717_3751_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3669() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin127_FirFilter_Fiss_3718_3752_split[0]), &(SplitJoin127_FirFilter_Fiss_3718_3752_join[0]));
	ENDFOR
}

void FirFilter_3670() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin127_FirFilter_Fiss_3718_3752_split[1]), &(SplitJoin127_FirFilter_Fiss_3718_3752_join[1]));
	ENDFOR
}

void FirFilter_3671() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin127_FirFilter_Fiss_3718_3752_split[2]), &(SplitJoin127_FirFilter_Fiss_3718_3752_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_3667() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 24, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3663DUPLICATE_Splitter_3667);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_float(&SplitJoin127_FirFilter_Fiss_3718_3752_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3668() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3668DownSamp_3496, pop_float(&SplitJoin127_FirFilter_Fiss_3718_3752_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp_3496() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3668DownSamp_3496), &(DownSamp_3496UpSamp_3497));
	ENDFOR
}

void UpSamp_3497() {
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		UpSamp(&(DownSamp_3496UpSamp_3497), &(UpSamp_3497WEIGHTED_ROUND_ROBIN_Splitter_3672));
	ENDFOR
}

void Delay_N_3674() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin129_Delay_N_Fiss_3719_3753_split[0]), &(SplitJoin129_Delay_N_Fiss_3719_3753_join[0]));
	ENDFOR
}

void Delay_N_3675() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin129_Delay_N_Fiss_3719_3753_split[1]), &(SplitJoin129_Delay_N_Fiss_3719_3753_join[1]));
	ENDFOR
}

void Delay_N_3676() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin129_Delay_N_Fiss_3719_3753_split[2]), &(SplitJoin129_Delay_N_Fiss_3719_3753_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3672() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&SplitJoin129_Delay_N_Fiss_3719_3753_split[__iter_], pop_float(&UpSamp_3497WEIGHTED_ROUND_ROBIN_Splitter_3672));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3673() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3673DUPLICATE_Splitter_3677, pop_float(&SplitJoin129_Delay_N_Fiss_3719_3753_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3679() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin131_FirFilter_Fiss_3720_3754_split[0]), &(SplitJoin131_FirFilter_Fiss_3720_3754_join[0]));
	ENDFOR
}

void FirFilter_3680() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin131_FirFilter_Fiss_3720_3754_split[1]), &(SplitJoin131_FirFilter_Fiss_3720_3754_join[1]));
	ENDFOR
}

void FirFilter_3681() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin131_FirFilter_Fiss_3720_3754_split[2]), &(SplitJoin131_FirFilter_Fiss_3720_3754_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_3677() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 24, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3673DUPLICATE_Splitter_3677);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_float(&SplitJoin131_FirFilter_Fiss_3720_3754_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3678() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3440_3504_3687_3722_join[7], pop_float(&SplitJoin131_FirFilter_Fiss_3720_3754_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DUPLICATE_Splitter_3502() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 24, __iter_steady_++)
		float __token_ = pop_float(&source_3442DUPLICATE_Splitter_3502);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3440_3504_3687_3722_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3503() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 24, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3503WEIGHTED_ROUND_ROBIN_Splitter_3682, pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3440_3504_3687_3722_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Combine(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR(int, i, 0,  < , 8, i++) {
			sum = (sum + pop_float(&(*chanin))) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
	}


void Combine_3684() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Combine(&(SplitJoin10_Combine_Fiss_3692_3755_split[0]), &(SplitJoin10_Combine_Fiss_3692_3755_join[0]));
	ENDFOR
}

void Combine_3685() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Combine(&(SplitJoin10_Combine_Fiss_3692_3755_split[1]), &(SplitJoin10_Combine_Fiss_3692_3755_join[1]));
	ENDFOR
}

void Combine_3686() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Combine(&(SplitJoin10_Combine_Fiss_3692_3755_split[2]), &(SplitJoin10_Combine_Fiss_3692_3755_join[2]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3682() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 3, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_Combine_Fiss_3692_3755_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3503WEIGHTED_ROUND_ROBIN_Splitter_3682));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3683() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3683sink_3501, pop_float(&SplitJoin10_Combine_Fiss_3692_3755_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void sink(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void sink_3501() {
	FOR(uint32_t, __iter_steady_, 0, <, 24, __iter_steady_++)
		sink(&(WEIGHTED_ROUND_ROBIN_Joiner_3683sink_3501));
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3583DUPLICATE_Splitter_3587);
	FOR(int, __iter_init_0_, 0, <, 3, __iter_init_0_++)
		init_buffer_float(&SplitJoin23_Delay_N_Fiss_3693_3727_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3440_3504_3687_3722_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 3, __iter_init_2_++)
		init_buffer_float(&SplitJoin23_Delay_N_Fiss_3693_3727_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 3, __iter_init_3_++)
		init_buffer_float(&SplitJoin95_Delay_N_Fiss_3711_3745_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 3, __iter_init_4_++)
		init_buffer_float(&SplitJoin131_FirFilter_Fiss_3720_3754_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 3, __iter_init_5_++)
		init_buffer_float(&SplitJoin108_Delay_N_Fiss_3713_3747_split[__iter_init_5_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3593DUPLICATE_Splitter_3597);
	FOR(int, __iter_init_6_, 0, <, 3, __iter_init_6_++)
		init_buffer_float(&SplitJoin91_Delay_N_Fiss_3709_3743_split[__iter_init_6_]);
	ENDFOR
	init_buffer_float(&DownSamp_3489UpSamp_3490);
	FOR(int, __iter_init_7_, 0, <, 3, __iter_init_7_++)
		init_buffer_float(&SplitJoin46_FirFilter_Fiss_3700_3734_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 3, __iter_init_8_++)
		init_buffer_float(&SplitJoin131_FirFilter_Fiss_3720_3754_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 3, __iter_init_9_++)
		init_buffer_float(&SplitJoin93_FirFilter_Fiss_3710_3744_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 3, __iter_init_10_++)
		init_buffer_float(&SplitJoin108_Delay_N_Fiss_3713_3747_join[__iter_init_10_]);
	ENDFOR
	init_buffer_float(&UpSamp_3455WEIGHTED_ROUND_ROBIN_Splitter_3552);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3663DUPLICATE_Splitter_3667);
	FOR(int, __iter_init_11_, 0, <, 3, __iter_init_11_++)
		init_buffer_float(&SplitJoin8_FirFilter_Fiss_3691_3726_join[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&DownSamp_3496UpSamp_3497);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3573DUPLICATE_Splitter_3577);
	init_buffer_float(&UpSamp_3462WEIGHTED_ROUND_ROBIN_Splitter_3572);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3543DUPLICATE_Splitter_3547);
	FOR(int, __iter_init_12_, 0, <, 3, __iter_init_12_++)
		init_buffer_float(&SplitJoin27_Delay_N_Fiss_3695_3729_join[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 3, __iter_init_13_++)
		init_buffer_float(&SplitJoin61_Delay_N_Fiss_3703_3737_split[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 3, __iter_init_14_++)
		init_buffer_float(&SplitJoin112_Delay_N_Fiss_3715_3749_split[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 3, __iter_init_15_++)
		init_buffer_float(&SplitJoin63_FirFilter_Fiss_3704_3738_split[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 3, __iter_init_16_++)
		init_buffer_float(&SplitJoin44_Delay_N_Fiss_3699_3733_split[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 3, __iter_init_17_++)
		init_buffer_float(&SplitJoin114_FirFilter_Fiss_3716_3750_split[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 3, __iter_init_18_++)
		init_buffer_float(&SplitJoin25_FirFilter_Fiss_3694_3728_join[__iter_init_18_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3553DUPLICATE_Splitter_3557);
	init_buffer_float(&UpSamp_3490WEIGHTED_ROUND_ROBIN_Splitter_3652);
	init_buffer_float(&DownSamp_3482UpSamp_3483);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3653DUPLICATE_Splitter_3657);
	FOR(int, __iter_init_19_, 0, <, 3, __iter_init_19_++)
		init_buffer_float(&SplitJoin78_Delay_N_Fiss_3707_3741_join[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 3, __iter_init_20_++)
		init_buffer_float(&SplitJoin97_FirFilter_Fiss_3712_3746_split[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 3, __iter_init_21_++)
		init_buffer_float(&SplitJoin44_Delay_N_Fiss_3699_3733_join[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 3, __iter_init_22_++)
		init_buffer_float(&SplitJoin61_Delay_N_Fiss_3703_3737_join[__iter_init_22_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3668DownSamp_3496);
	init_buffer_float(&DownSamp_3475UpSamp_3476);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3623DUPLICATE_Splitter_3627);
	FOR(int, __iter_init_23_, 0, <, 3, __iter_init_23_++)
		init_buffer_float(&SplitJoin80_FirFilter_Fiss_3708_3742_join[__iter_init_23_]);
	ENDFOR
	init_buffer_float(&UpSamp_3483WEIGHTED_ROUND_ROBIN_Splitter_3632);
	FOR(int, __iter_init_24_, 0, <, 3, __iter_init_24_++)
		init_buffer_float(&SplitJoin110_FirFilter_Fiss_3714_3748_join[__iter_init_24_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3683sink_3501);
	FOR(int, __iter_init_25_, 0, <, 3, __iter_init_25_++)
		init_buffer_float(&SplitJoin76_FirFilter_Fiss_3706_3740_join[__iter_init_25_]);
	ENDFOR
	init_buffer_float(&UpSamp_3476WEIGHTED_ROUND_ROBIN_Splitter_3612);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3608DownSamp_3475);
	FOR(int, __iter_init_26_, 0, <, 3, __iter_init_26_++)
		init_buffer_float(&SplitJoin127_FirFilter_Fiss_3718_3752_join[__iter_init_26_]);
	ENDFOR
	FOR(int, __iter_init_27_, 0, <, 3, __iter_init_27_++)
		init_buffer_float(&SplitJoin2_Delay_N_Fiss_3688_3723_split[__iter_init_27_]);
	ENDFOR
	init_buffer_float(&DownSamp_3468UpSamp_3469);
	FOR(int, __iter_init_28_, 0, <, 3, __iter_init_28_++)
		init_buffer_float(&SplitJoin110_FirFilter_Fiss_3714_3748_split[__iter_init_28_]);
	ENDFOR
	FOR(int, __iter_init_29_, 0, <, 3, __iter_init_29_++)
		init_buffer_float(&SplitJoin59_FirFilter_Fiss_3702_3736_split[__iter_init_29_]);
	ENDFOR
	FOR(int, __iter_init_30_, 0, <, 3, __iter_init_30_++)
		init_buffer_float(&SplitJoin6_Delay_N_Fiss_3690_3725_split[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 3, __iter_init_31_++)
		init_buffer_float(&SplitJoin125_Delay_N_Fiss_3717_3751_split[__iter_init_31_]);
	ENDFOR
	init_buffer_float(&UpSamp_3448WEIGHTED_ROUND_ROBIN_Splitter_3532);
	FOR(int, __iter_init_32_, 0, <, 3, __iter_init_32_++)
		init_buffer_float(&SplitJoin42_FirFilter_Fiss_3698_3732_split[__iter_init_32_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3603DUPLICATE_Splitter_3607);
	FOR(int, __iter_init_33_, 0, <, 3, __iter_init_33_++)
		init_buffer_float(&SplitJoin74_Delay_N_Fiss_3705_3739_split[__iter_init_33_]);
	ENDFOR
	FOR(int, __iter_init_34_, 0, <, 3, __iter_init_34_++)
		init_buffer_float(&SplitJoin93_FirFilter_Fiss_3710_3744_join[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 3, __iter_init_35_++)
		init_buffer_float(&SplitJoin6_Delay_N_Fiss_3690_3725_join[__iter_init_35_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3628DownSamp_3482);
	FOR(int, __iter_init_36_, 0, <, 3, __iter_init_36_++)
		init_buffer_float(&SplitJoin63_FirFilter_Fiss_3704_3738_join[__iter_init_36_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3568DownSamp_3461);
	FOR(int, __iter_init_37_, 0, <, 3, __iter_init_37_++)
		init_buffer_float(&SplitJoin127_FirFilter_Fiss_3718_3752_split[__iter_init_37_]);
	ENDFOR
	FOR(int, __iter_init_38_, 0, <, 3, __iter_init_38_++)
		init_buffer_float(&SplitJoin40_Delay_N_Fiss_3697_3731_join[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 3, __iter_init_39_++)
		init_buffer_float(&SplitJoin76_FirFilter_Fiss_3706_3740_split[__iter_init_39_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3613DUPLICATE_Splitter_3617);
	FOR(int, __iter_init_40_, 0, <, 3, __iter_init_40_++)
		init_buffer_float(&SplitJoin97_FirFilter_Fiss_3712_3746_join[__iter_init_40_]);
	ENDFOR
	FOR(int, __iter_init_41_, 0, <, 3, __iter_init_41_++)
		init_buffer_float(&SplitJoin125_Delay_N_Fiss_3717_3751_join[__iter_init_41_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3523DUPLICATE_Splitter_3527);
	init_buffer_float(&source_3442DUPLICATE_Splitter_3502);
	FOR(int, __iter_init_42_, 0, <, 3, __iter_init_42_++)
		init_buffer_float(&SplitJoin91_Delay_N_Fiss_3709_3743_join[__iter_init_42_]);
	ENDFOR
	FOR(int, __iter_init_43_, 0, <, 3, __iter_init_43_++)
		init_buffer_float(&SplitJoin129_Delay_N_Fiss_3719_3753_split[__iter_init_43_]);
	ENDFOR
	FOR(int, __iter_init_44_, 0, <, 3, __iter_init_44_++)
		init_buffer_float(&SplitJoin112_Delay_N_Fiss_3715_3749_join[__iter_init_44_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3548DownSamp_3454);
	FOR(int, __iter_init_45_, 0, <, 3, __iter_init_45_++)
		init_buffer_float(&SplitJoin74_Delay_N_Fiss_3705_3739_join[__iter_init_45_]);
	ENDFOR
	FOR(int, __iter_init_46_, 0, <, 8, __iter_init_46_++)
		init_buffer_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3440_3504_3687_3722_join[__iter_init_46_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3633DUPLICATE_Splitter_3637);
	FOR(int, __iter_init_47_, 0, <, 3, __iter_init_47_++)
		init_buffer_float(&SplitJoin57_Delay_N_Fiss_3701_3735_split[__iter_init_47_]);
	ENDFOR
	init_buffer_float(&DownSamp_3454UpSamp_3455);
	FOR(int, __iter_init_48_, 0, <, 3, __iter_init_48_++)
		init_buffer_float(&SplitJoin57_Delay_N_Fiss_3701_3735_join[__iter_init_48_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3648DownSamp_3489);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3643DUPLICATE_Splitter_3647);
	FOR(int, __iter_init_49_, 0, <, 3, __iter_init_49_++)
		init_buffer_float(&SplitJoin42_FirFilter_Fiss_3698_3732_join[__iter_init_49_]);
	ENDFOR
	FOR(int, __iter_init_50_, 0, <, 3, __iter_init_50_++)
		init_buffer_float(&SplitJoin29_FirFilter_Fiss_3696_3730_join[__iter_init_50_]);
	ENDFOR
	FOR(int, __iter_init_51_, 0, <, 3, __iter_init_51_++)
		init_buffer_float(&SplitJoin78_Delay_N_Fiss_3707_3741_split[__iter_init_51_]);
	ENDFOR
	init_buffer_float(&DownSamp_3447UpSamp_3448);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3588DownSamp_3468);
	FOR(int, __iter_init_52_, 0, <, 3, __iter_init_52_++)
		init_buffer_float(&SplitJoin59_FirFilter_Fiss_3702_3736_join[__iter_init_52_]);
	ENDFOR
	FOR(int, __iter_init_53_, 0, <, 3, __iter_init_53_++)
		init_buffer_float(&SplitJoin2_Delay_N_Fiss_3688_3723_join[__iter_init_53_]);
	ENDFOR
	init_buffer_float(&UpSamp_3469WEIGHTED_ROUND_ROBIN_Splitter_3592);
	FOR(int, __iter_init_54_, 0, <, 3, __iter_init_54_++)
		init_buffer_float(&SplitJoin4_FirFilter_Fiss_3689_3724_split[__iter_init_54_]);
	ENDFOR
	FOR(int, __iter_init_55_, 0, <, 3, __iter_init_55_++)
		init_buffer_float(&SplitJoin29_FirFilter_Fiss_3696_3730_split[__iter_init_55_]);
	ENDFOR
	FOR(int, __iter_init_56_, 0, <, 3, __iter_init_56_++)
		init_buffer_float(&SplitJoin4_FirFilter_Fiss_3689_3724_join[__iter_init_56_]);
	ENDFOR
	FOR(int, __iter_init_57_, 0, <, 3, __iter_init_57_++)
		init_buffer_float(&SplitJoin10_Combine_Fiss_3692_3755_join[__iter_init_57_]);
	ENDFOR
	FOR(int, __iter_init_58_, 0, <, 3, __iter_init_58_++)
		init_buffer_float(&SplitJoin8_FirFilter_Fiss_3691_3726_split[__iter_init_58_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3563DUPLICATE_Splitter_3567);
	FOR(int, __iter_init_59_, 0, <, 3, __iter_init_59_++)
		init_buffer_float(&SplitJoin10_Combine_Fiss_3692_3755_split[__iter_init_59_]);
	ENDFOR
	FOR(int, __iter_init_60_, 0, <, 3, __iter_init_60_++)
		init_buffer_float(&SplitJoin46_FirFilter_Fiss_3700_3734_split[__iter_init_60_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3528DownSamp_3447);
	FOR(int, __iter_init_61_, 0, <, 3, __iter_init_61_++)
		init_buffer_float(&SplitJoin25_FirFilter_Fiss_3694_3728_split[__iter_init_61_]);
	ENDFOR
	FOR(int, __iter_init_62_, 0, <, 3, __iter_init_62_++)
		init_buffer_float(&SplitJoin40_Delay_N_Fiss_3697_3731_split[__iter_init_62_]);
	ENDFOR
	init_buffer_float(&DownSamp_3461UpSamp_3462);
	FOR(int, __iter_init_63_, 0, <, 3, __iter_init_63_++)
		init_buffer_float(&SplitJoin129_Delay_N_Fiss_3719_3753_join[__iter_init_63_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3533DUPLICATE_Splitter_3537);
	init_buffer_float(&UpSamp_3497WEIGHTED_ROUND_ROBIN_Splitter_3672);
	FOR(int, __iter_init_64_, 0, <, 3, __iter_init_64_++)
		init_buffer_float(&SplitJoin80_FirFilter_Fiss_3708_3742_split[__iter_init_64_]);
	ENDFOR
	FOR(int, __iter_init_65_, 0, <, 3, __iter_init_65_++)
		init_buffer_float(&SplitJoin27_Delay_N_Fiss_3695_3729_split[__iter_init_65_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3673DUPLICATE_Splitter_3677);
	FOR(int, __iter_init_66_, 0, <, 3, __iter_init_66_++)
		init_buffer_float(&SplitJoin95_Delay_N_Fiss_3711_3745_join[__iter_init_66_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3503WEIGHTED_ROUND_ROBIN_Splitter_3682);
	FOR(int, __iter_init_67_, 0, <, 3, __iter_init_67_++)
		init_buffer_float(&SplitJoin114_FirFilter_Fiss_3716_3750_join[__iter_init_67_]);
	ENDFOR
// --- init: source_3442
	 {
	source_3442_s.current = 0.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 240, __iter_init_++)
		source(&(source_3442DUPLICATE_Splitter_3502));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3502
	FOR(uint32_t, __iter_init_, 0, <, 240, __iter_init_++)
		DUPLICATE_Splitter(&(source_3442DUPLICATE_Splitter_3502), &(SplitJoin0_SplitJoin0_SplitJoin0_Branches_3440_3504_3687_3722_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3522
	FOR(uint32_t, __iter_init_, 0, <, 80, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(SplitJoin0_SplitJoin0_SplitJoin0_Branches_3440_3504_3687_3722_split[0]), &(SplitJoin2_Delay_N_Fiss_3688_3723_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3524
	FOR(uint32_t, __iter_init_, 0, <, 52, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin2_Delay_N_Fiss_3688_3723_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3525
	FOR(uint32_t, __iter_init_, 0, <, 52, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin2_Delay_N_Fiss_3688_3723_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3526
	FOR(uint32_t, __iter_init_, 0, <, 52, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin2_Delay_N_Fiss_3688_3723_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3523
	FOR(uint32_t, __iter_init_, 0, <, 51, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin2_Delay_N_Fiss_3688_3723_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3523DUPLICATE_Splitter_3527));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3527
	FOR(uint32_t, __iter_init_, 0, <, 151, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3523DUPLICATE_Splitter_3527), &(SplitJoin4_FirFilter_Fiss_3689_3724_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3529
	 {
	FirFilter_3529_s.COEFF[0] = 1.0 ; 
	FirFilter_3529_s.COEFF[1] = 34.0 ; 
	FirFilter_3529_s.COEFF[2] = 67.0 ; 
	FirFilter_3529_s.COEFF[3] = 100.0 ; 
	FirFilter_3529_s.COEFF[4] = 133.0 ; 
	FirFilter_3529_s.COEFF[5] = 166.0 ; 
	FirFilter_3529_s.COEFF[6] = 199.0 ; 
	FirFilter_3529_s.COEFF[7] = 232.0 ; 
	FirFilter_3529_s.COEFF[8] = 265.0 ; 
	FirFilter_3529_s.COEFF[9] = 298.0 ; 
	FirFilter_3529_s.COEFF[10] = 331.0 ; 
	FirFilter_3529_s.COEFF[11] = 364.0 ; 
	FirFilter_3529_s.COEFF[12] = 397.0 ; 
	FirFilter_3529_s.COEFF[13] = 430.0 ; 
	FirFilter_3529_s.COEFF[14] = 463.0 ; 
	FirFilter_3529_s.COEFF[15] = 496.0 ; 
	FirFilter_3529_s.COEFF[16] = 529.0 ; 
	FirFilter_3529_s.COEFF[17] = 562.0 ; 
	FirFilter_3529_s.COEFF[18] = 595.0 ; 
	FirFilter_3529_s.COEFF[19] = 628.0 ; 
	FirFilter_3529_s.COEFF[20] = 661.0 ; 
	FirFilter_3529_s.COEFF[21] = 694.0 ; 
	FirFilter_3529_s.COEFF[22] = 727.0 ; 
	FirFilter_3529_s.COEFF[23] = 760.0 ; 
	FirFilter_3529_s.COEFF[24] = 793.0 ; 
	FirFilter_3529_s.COEFF[25] = 826.0 ; 
	FirFilter_3529_s.COEFF[26] = 859.0 ; 
	FirFilter_3529_s.COEFF[27] = 892.0 ; 
	FirFilter_3529_s.COEFF[28] = 925.0 ; 
	FirFilter_3529_s.COEFF[29] = 958.0 ; 
	FirFilter_3529_s.COEFF[30] = 991.0 ; 
	FirFilter_3529_s.COEFF[31] = 1024.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		FirFilter(&(SplitJoin4_FirFilter_Fiss_3689_3724_split[0]), &(SplitJoin4_FirFilter_Fiss_3689_3724_join[0]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3530
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		FirFilter(&(SplitJoin4_FirFilter_Fiss_3689_3724_split[1]), &(SplitJoin4_FirFilter_Fiss_3689_3724_join[1]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3531
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		FirFilter(&(SplitJoin4_FirFilter_Fiss_3689_3724_split[2]), &(SplitJoin4_FirFilter_Fiss_3689_3724_join[2]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3528
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin4_FirFilter_Fiss_3689_3724_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3528DownSamp_3447));
	ENDFOR
//--------------------------------
// --- init: DownSamp_3447
	FOR(uint32_t, __iter_init_, 0, <, 15, __iter_init_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3528DownSamp_3447), &(DownSamp_3447UpSamp_3448));
	ENDFOR
//--------------------------------
// --- init: UpSamp_3448
	FOR(uint32_t, __iter_init_, 0, <, 15, __iter_init_++)
		UpSamp(&(DownSamp_3447UpSamp_3448), &(UpSamp_3448WEIGHTED_ROUND_ROBIN_Splitter_3532));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3532
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(UpSamp_3448WEIGHTED_ROUND_ROBIN_Splitter_3532), &(SplitJoin6_Delay_N_Fiss_3690_3725_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3534
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin6_Delay_N_Fiss_3690_3725_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3535
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin6_Delay_N_Fiss_3690_3725_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3536
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin6_Delay_N_Fiss_3690_3725_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3533
	FOR(uint32_t, __iter_init_, 0, <, 11, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin6_Delay_N_Fiss_3690_3725_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3533DUPLICATE_Splitter_3537));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3537
	FOR(uint32_t, __iter_init_, 0, <, 31, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3533DUPLICATE_Splitter_3537), &(SplitJoin8_FirFilter_Fiss_3691_3726_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3542
	FOR(uint32_t, __iter_init_, 0, <, 80, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(SplitJoin0_SplitJoin0_SplitJoin0_Branches_3440_3504_3687_3722_split[1]), &(SplitJoin23_Delay_N_Fiss_3693_3727_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3544
	FOR(uint32_t, __iter_init_, 0, <, 52, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin23_Delay_N_Fiss_3693_3727_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3545
	FOR(uint32_t, __iter_init_, 0, <, 52, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin23_Delay_N_Fiss_3693_3727_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3546
	FOR(uint32_t, __iter_init_, 0, <, 52, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin23_Delay_N_Fiss_3693_3727_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3543
	FOR(uint32_t, __iter_init_, 0, <, 51, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin23_Delay_N_Fiss_3693_3727_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3543DUPLICATE_Splitter_3547));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3547
	FOR(uint32_t, __iter_init_, 0, <, 151, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3543DUPLICATE_Splitter_3547), &(SplitJoin25_FirFilter_Fiss_3694_3728_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3549
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		FirFilter(&(SplitJoin25_FirFilter_Fiss_3694_3728_split[0]), &(SplitJoin25_FirFilter_Fiss_3694_3728_join[0]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3550
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		FirFilter(&(SplitJoin25_FirFilter_Fiss_3694_3728_split[1]), &(SplitJoin25_FirFilter_Fiss_3694_3728_join[1]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3551
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		FirFilter(&(SplitJoin25_FirFilter_Fiss_3694_3728_split[2]), &(SplitJoin25_FirFilter_Fiss_3694_3728_join[2]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3548
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin25_FirFilter_Fiss_3694_3728_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3548DownSamp_3454));
	ENDFOR
//--------------------------------
// --- init: DownSamp_3454
	FOR(uint32_t, __iter_init_, 0, <, 15, __iter_init_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3548DownSamp_3454), &(DownSamp_3454UpSamp_3455));
	ENDFOR
//--------------------------------
// --- init: UpSamp_3455
	FOR(uint32_t, __iter_init_, 0, <, 15, __iter_init_++)
		UpSamp(&(DownSamp_3454UpSamp_3455), &(UpSamp_3455WEIGHTED_ROUND_ROBIN_Splitter_3552));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3552
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(UpSamp_3455WEIGHTED_ROUND_ROBIN_Splitter_3552), &(SplitJoin27_Delay_N_Fiss_3695_3729_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3554
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin27_Delay_N_Fiss_3695_3729_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3555
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin27_Delay_N_Fiss_3695_3729_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3556
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin27_Delay_N_Fiss_3695_3729_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3553
	FOR(uint32_t, __iter_init_, 0, <, 11, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin27_Delay_N_Fiss_3695_3729_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3553DUPLICATE_Splitter_3557));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3557
	FOR(uint32_t, __iter_init_, 0, <, 31, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3553DUPLICATE_Splitter_3557), &(SplitJoin29_FirFilter_Fiss_3696_3730_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3562
	FOR(uint32_t, __iter_init_, 0, <, 80, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(SplitJoin0_SplitJoin0_SplitJoin0_Branches_3440_3504_3687_3722_split[2]), &(SplitJoin40_Delay_N_Fiss_3697_3731_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3564
	FOR(uint32_t, __iter_init_, 0, <, 52, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin40_Delay_N_Fiss_3697_3731_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3565
	FOR(uint32_t, __iter_init_, 0, <, 52, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin40_Delay_N_Fiss_3697_3731_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3566
	FOR(uint32_t, __iter_init_, 0, <, 52, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin40_Delay_N_Fiss_3697_3731_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3563
	FOR(uint32_t, __iter_init_, 0, <, 51, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin40_Delay_N_Fiss_3697_3731_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3563DUPLICATE_Splitter_3567));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3567
	FOR(uint32_t, __iter_init_, 0, <, 151, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3563DUPLICATE_Splitter_3567), &(SplitJoin42_FirFilter_Fiss_3698_3732_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3569
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		FirFilter(&(SplitJoin42_FirFilter_Fiss_3698_3732_split[0]), &(SplitJoin42_FirFilter_Fiss_3698_3732_join[0]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3570
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		FirFilter(&(SplitJoin42_FirFilter_Fiss_3698_3732_split[1]), &(SplitJoin42_FirFilter_Fiss_3698_3732_join[1]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3571
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		FirFilter(&(SplitJoin42_FirFilter_Fiss_3698_3732_split[2]), &(SplitJoin42_FirFilter_Fiss_3698_3732_join[2]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3568
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin42_FirFilter_Fiss_3698_3732_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3568DownSamp_3461));
	ENDFOR
//--------------------------------
// --- init: DownSamp_3461
	FOR(uint32_t, __iter_init_, 0, <, 15, __iter_init_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3568DownSamp_3461), &(DownSamp_3461UpSamp_3462));
	ENDFOR
//--------------------------------
// --- init: UpSamp_3462
	FOR(uint32_t, __iter_init_, 0, <, 15, __iter_init_++)
		UpSamp(&(DownSamp_3461UpSamp_3462), &(UpSamp_3462WEIGHTED_ROUND_ROBIN_Splitter_3572));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3572
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(UpSamp_3462WEIGHTED_ROUND_ROBIN_Splitter_3572), &(SplitJoin44_Delay_N_Fiss_3699_3733_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3574
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin44_Delay_N_Fiss_3699_3733_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3575
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin44_Delay_N_Fiss_3699_3733_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3576
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin44_Delay_N_Fiss_3699_3733_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3573
	FOR(uint32_t, __iter_init_, 0, <, 11, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin44_Delay_N_Fiss_3699_3733_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3573DUPLICATE_Splitter_3577));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3577
	FOR(uint32_t, __iter_init_, 0, <, 31, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3573DUPLICATE_Splitter_3577), &(SplitJoin46_FirFilter_Fiss_3700_3734_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3582
	FOR(uint32_t, __iter_init_, 0, <, 80, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(SplitJoin0_SplitJoin0_SplitJoin0_Branches_3440_3504_3687_3722_split[3]), &(SplitJoin57_Delay_N_Fiss_3701_3735_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3584
	FOR(uint32_t, __iter_init_, 0, <, 52, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin57_Delay_N_Fiss_3701_3735_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3585
	FOR(uint32_t, __iter_init_, 0, <, 52, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin57_Delay_N_Fiss_3701_3735_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3586
	FOR(uint32_t, __iter_init_, 0, <, 52, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin57_Delay_N_Fiss_3701_3735_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3583
	FOR(uint32_t, __iter_init_, 0, <, 51, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin57_Delay_N_Fiss_3701_3735_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3583DUPLICATE_Splitter_3587));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3587
	FOR(uint32_t, __iter_init_, 0, <, 151, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3583DUPLICATE_Splitter_3587), &(SplitJoin59_FirFilter_Fiss_3702_3736_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3589
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		FirFilter(&(SplitJoin59_FirFilter_Fiss_3702_3736_split[0]), &(SplitJoin59_FirFilter_Fiss_3702_3736_join[0]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3590
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		FirFilter(&(SplitJoin59_FirFilter_Fiss_3702_3736_split[1]), &(SplitJoin59_FirFilter_Fiss_3702_3736_join[1]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3591
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		FirFilter(&(SplitJoin59_FirFilter_Fiss_3702_3736_split[2]), &(SplitJoin59_FirFilter_Fiss_3702_3736_join[2]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3588
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin59_FirFilter_Fiss_3702_3736_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3588DownSamp_3468));
	ENDFOR
//--------------------------------
// --- init: DownSamp_3468
	FOR(uint32_t, __iter_init_, 0, <, 15, __iter_init_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3588DownSamp_3468), &(DownSamp_3468UpSamp_3469));
	ENDFOR
//--------------------------------
// --- init: UpSamp_3469
	FOR(uint32_t, __iter_init_, 0, <, 15, __iter_init_++)
		UpSamp(&(DownSamp_3468UpSamp_3469), &(UpSamp_3469WEIGHTED_ROUND_ROBIN_Splitter_3592));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3592
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(UpSamp_3469WEIGHTED_ROUND_ROBIN_Splitter_3592), &(SplitJoin61_Delay_N_Fiss_3703_3737_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3594
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin61_Delay_N_Fiss_3703_3737_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3595
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin61_Delay_N_Fiss_3703_3737_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3596
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin61_Delay_N_Fiss_3703_3737_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3593
	FOR(uint32_t, __iter_init_, 0, <, 11, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin61_Delay_N_Fiss_3703_3737_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3593DUPLICATE_Splitter_3597));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3597
	FOR(uint32_t, __iter_init_, 0, <, 31, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3593DUPLICATE_Splitter_3597), &(SplitJoin63_FirFilter_Fiss_3704_3738_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3602
	FOR(uint32_t, __iter_init_, 0, <, 80, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(SplitJoin0_SplitJoin0_SplitJoin0_Branches_3440_3504_3687_3722_split[4]), &(SplitJoin74_Delay_N_Fiss_3705_3739_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3604
	FOR(uint32_t, __iter_init_, 0, <, 52, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin74_Delay_N_Fiss_3705_3739_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3605
	FOR(uint32_t, __iter_init_, 0, <, 52, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin74_Delay_N_Fiss_3705_3739_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3606
	FOR(uint32_t, __iter_init_, 0, <, 52, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin74_Delay_N_Fiss_3705_3739_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3603
	FOR(uint32_t, __iter_init_, 0, <, 51, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin74_Delay_N_Fiss_3705_3739_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3603DUPLICATE_Splitter_3607));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3607
	FOR(uint32_t, __iter_init_, 0, <, 151, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3603DUPLICATE_Splitter_3607), &(SplitJoin76_FirFilter_Fiss_3706_3740_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3609
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		FirFilter(&(SplitJoin76_FirFilter_Fiss_3706_3740_split[0]), &(SplitJoin76_FirFilter_Fiss_3706_3740_join[0]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3610
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		FirFilter(&(SplitJoin76_FirFilter_Fiss_3706_3740_split[1]), &(SplitJoin76_FirFilter_Fiss_3706_3740_join[1]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3611
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		FirFilter(&(SplitJoin76_FirFilter_Fiss_3706_3740_split[2]), &(SplitJoin76_FirFilter_Fiss_3706_3740_join[2]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3608
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin76_FirFilter_Fiss_3706_3740_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3608DownSamp_3475));
	ENDFOR
//--------------------------------
// --- init: DownSamp_3475
	FOR(uint32_t, __iter_init_, 0, <, 15, __iter_init_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3608DownSamp_3475), &(DownSamp_3475UpSamp_3476));
	ENDFOR
//--------------------------------
// --- init: UpSamp_3476
	FOR(uint32_t, __iter_init_, 0, <, 15, __iter_init_++)
		UpSamp(&(DownSamp_3475UpSamp_3476), &(UpSamp_3476WEIGHTED_ROUND_ROBIN_Splitter_3612));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3612
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(UpSamp_3476WEIGHTED_ROUND_ROBIN_Splitter_3612), &(SplitJoin78_Delay_N_Fiss_3707_3741_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3614
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin78_Delay_N_Fiss_3707_3741_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3615
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin78_Delay_N_Fiss_3707_3741_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3616
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin78_Delay_N_Fiss_3707_3741_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3613
	FOR(uint32_t, __iter_init_, 0, <, 11, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin78_Delay_N_Fiss_3707_3741_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3613DUPLICATE_Splitter_3617));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3617
	FOR(uint32_t, __iter_init_, 0, <, 31, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3613DUPLICATE_Splitter_3617), &(SplitJoin80_FirFilter_Fiss_3708_3742_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3622
	FOR(uint32_t, __iter_init_, 0, <, 80, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(SplitJoin0_SplitJoin0_SplitJoin0_Branches_3440_3504_3687_3722_split[5]), &(SplitJoin91_Delay_N_Fiss_3709_3743_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3624
	FOR(uint32_t, __iter_init_, 0, <, 52, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin91_Delay_N_Fiss_3709_3743_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3625
	FOR(uint32_t, __iter_init_, 0, <, 52, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin91_Delay_N_Fiss_3709_3743_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3626
	FOR(uint32_t, __iter_init_, 0, <, 52, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin91_Delay_N_Fiss_3709_3743_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3623
	FOR(uint32_t, __iter_init_, 0, <, 51, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin91_Delay_N_Fiss_3709_3743_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3623DUPLICATE_Splitter_3627));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3627
	FOR(uint32_t, __iter_init_, 0, <, 151, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3623DUPLICATE_Splitter_3627), &(SplitJoin93_FirFilter_Fiss_3710_3744_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3629
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		FirFilter(&(SplitJoin93_FirFilter_Fiss_3710_3744_split[0]), &(SplitJoin93_FirFilter_Fiss_3710_3744_join[0]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3630
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		FirFilter(&(SplitJoin93_FirFilter_Fiss_3710_3744_split[1]), &(SplitJoin93_FirFilter_Fiss_3710_3744_join[1]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3631
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		FirFilter(&(SplitJoin93_FirFilter_Fiss_3710_3744_split[2]), &(SplitJoin93_FirFilter_Fiss_3710_3744_join[2]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3628
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin93_FirFilter_Fiss_3710_3744_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3628DownSamp_3482));
	ENDFOR
//--------------------------------
// --- init: DownSamp_3482
	FOR(uint32_t, __iter_init_, 0, <, 15, __iter_init_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3628DownSamp_3482), &(DownSamp_3482UpSamp_3483));
	ENDFOR
//--------------------------------
// --- init: UpSamp_3483
	FOR(uint32_t, __iter_init_, 0, <, 15, __iter_init_++)
		UpSamp(&(DownSamp_3482UpSamp_3483), &(UpSamp_3483WEIGHTED_ROUND_ROBIN_Splitter_3632));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3632
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(UpSamp_3483WEIGHTED_ROUND_ROBIN_Splitter_3632), &(SplitJoin95_Delay_N_Fiss_3711_3745_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3634
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin95_Delay_N_Fiss_3711_3745_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3635
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin95_Delay_N_Fiss_3711_3745_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3636
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin95_Delay_N_Fiss_3711_3745_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3633
	FOR(uint32_t, __iter_init_, 0, <, 11, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin95_Delay_N_Fiss_3711_3745_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3633DUPLICATE_Splitter_3637));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3637
	FOR(uint32_t, __iter_init_, 0, <, 31, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3633DUPLICATE_Splitter_3637), &(SplitJoin97_FirFilter_Fiss_3712_3746_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3642
	FOR(uint32_t, __iter_init_, 0, <, 80, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(SplitJoin0_SplitJoin0_SplitJoin0_Branches_3440_3504_3687_3722_split[6]), &(SplitJoin108_Delay_N_Fiss_3713_3747_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3644
	FOR(uint32_t, __iter_init_, 0, <, 52, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin108_Delay_N_Fiss_3713_3747_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3645
	FOR(uint32_t, __iter_init_, 0, <, 52, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin108_Delay_N_Fiss_3713_3747_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3646
	FOR(uint32_t, __iter_init_, 0, <, 52, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin108_Delay_N_Fiss_3713_3747_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3643
	FOR(uint32_t, __iter_init_, 0, <, 51, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin108_Delay_N_Fiss_3713_3747_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3643DUPLICATE_Splitter_3647));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3647
	FOR(uint32_t, __iter_init_, 0, <, 151, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3643DUPLICATE_Splitter_3647), &(SplitJoin110_FirFilter_Fiss_3714_3748_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3649
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		FirFilter(&(SplitJoin110_FirFilter_Fiss_3714_3748_split[0]), &(SplitJoin110_FirFilter_Fiss_3714_3748_join[0]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3650
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		FirFilter(&(SplitJoin110_FirFilter_Fiss_3714_3748_split[1]), &(SplitJoin110_FirFilter_Fiss_3714_3748_join[1]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3651
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		FirFilter(&(SplitJoin110_FirFilter_Fiss_3714_3748_split[2]), &(SplitJoin110_FirFilter_Fiss_3714_3748_join[2]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3648
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin110_FirFilter_Fiss_3714_3748_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3648DownSamp_3489));
	ENDFOR
//--------------------------------
// --- init: DownSamp_3489
	FOR(uint32_t, __iter_init_, 0, <, 15, __iter_init_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3648DownSamp_3489), &(DownSamp_3489UpSamp_3490));
	ENDFOR
//--------------------------------
// --- init: UpSamp_3490
	FOR(uint32_t, __iter_init_, 0, <, 15, __iter_init_++)
		UpSamp(&(DownSamp_3489UpSamp_3490), &(UpSamp_3490WEIGHTED_ROUND_ROBIN_Splitter_3652));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3652
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(UpSamp_3490WEIGHTED_ROUND_ROBIN_Splitter_3652), &(SplitJoin112_Delay_N_Fiss_3715_3749_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3654
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin112_Delay_N_Fiss_3715_3749_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3655
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin112_Delay_N_Fiss_3715_3749_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3656
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin112_Delay_N_Fiss_3715_3749_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3653
	FOR(uint32_t, __iter_init_, 0, <, 11, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin112_Delay_N_Fiss_3715_3749_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3653DUPLICATE_Splitter_3657));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3657
	FOR(uint32_t, __iter_init_, 0, <, 31, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3653DUPLICATE_Splitter_3657), &(SplitJoin114_FirFilter_Fiss_3716_3750_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3662
	FOR(uint32_t, __iter_init_, 0, <, 80, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(SplitJoin0_SplitJoin0_SplitJoin0_Branches_3440_3504_3687_3722_split[7]), &(SplitJoin125_Delay_N_Fiss_3717_3751_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3664
	FOR(uint32_t, __iter_init_, 0, <, 52, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin125_Delay_N_Fiss_3717_3751_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3665
	FOR(uint32_t, __iter_init_, 0, <, 52, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin125_Delay_N_Fiss_3717_3751_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3666
	FOR(uint32_t, __iter_init_, 0, <, 52, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin125_Delay_N_Fiss_3717_3751_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3663
	FOR(uint32_t, __iter_init_, 0, <, 51, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin125_Delay_N_Fiss_3717_3751_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3663DUPLICATE_Splitter_3667));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3667
	FOR(uint32_t, __iter_init_, 0, <, 151, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3663DUPLICATE_Splitter_3667), &(SplitJoin127_FirFilter_Fiss_3718_3752_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3669
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		FirFilter(&(SplitJoin127_FirFilter_Fiss_3718_3752_split[0]), &(SplitJoin127_FirFilter_Fiss_3718_3752_join[0]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3670
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		FirFilter(&(SplitJoin127_FirFilter_Fiss_3718_3752_split[1]), &(SplitJoin127_FirFilter_Fiss_3718_3752_join[1]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3671
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		FirFilter(&(SplitJoin127_FirFilter_Fiss_3718_3752_split[2]), &(SplitJoin127_FirFilter_Fiss_3718_3752_join[2]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3668
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin127_FirFilter_Fiss_3718_3752_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3668DownSamp_3496));
	ENDFOR
//--------------------------------
// --- init: DownSamp_3496
	FOR(uint32_t, __iter_init_, 0, <, 15, __iter_init_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_3668DownSamp_3496), &(DownSamp_3496UpSamp_3497));
	ENDFOR
//--------------------------------
// --- init: UpSamp_3497
	FOR(uint32_t, __iter_init_, 0, <, 15, __iter_init_++)
		UpSamp(&(DownSamp_3496UpSamp_3497), &(UpSamp_3497WEIGHTED_ROUND_ROBIN_Splitter_3672));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3672
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(UpSamp_3497WEIGHTED_ROUND_ROBIN_Splitter_3672), &(SplitJoin129_Delay_N_Fiss_3719_3753_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3674
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin129_Delay_N_Fiss_3719_3753_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3675
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin129_Delay_N_Fiss_3719_3753_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3676
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin129_Delay_N_Fiss_3719_3753_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3673
	FOR(uint32_t, __iter_init_, 0, <, 11, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin129_Delay_N_Fiss_3719_3753_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_3673DUPLICATE_Splitter_3677));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3677
	FOR(uint32_t, __iter_init_, 0, <, 31, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_3673DUPLICATE_Splitter_3677), &(SplitJoin131_FirFilter_Fiss_3720_3754_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_3681
	 {
	FirFilter_3681_s.COEFF[0] = 56.0 ; 
	FirFilter_3681_s.COEFF[1] = 64.0 ; 
	FirFilter_3681_s.COEFF[2] = 72.0 ; 
	FirFilter_3681_s.COEFF[3] = 80.0 ; 
	FirFilter_3681_s.COEFF[4] = 88.0 ; 
	FirFilter_3681_s.COEFF[5] = 96.0 ; 
	FirFilter_3681_s.COEFF[6] = 104.0 ; 
	FirFilter_3681_s.COEFF[7] = 112.0 ; 
	FirFilter_3681_s.COEFF[8] = 120.0 ; 
	FirFilter_3681_s.COEFF[9] = 128.0 ; 
	FirFilter_3681_s.COEFF[10] = 136.0 ; 
	FirFilter_3681_s.COEFF[11] = 144.0 ; 
	FirFilter_3681_s.COEFF[12] = 152.0 ; 
	FirFilter_3681_s.COEFF[13] = 160.0 ; 
	FirFilter_3681_s.COEFF[14] = 168.0 ; 
	FirFilter_3681_s.COEFF[15] = 176.0 ; 
	FirFilter_3681_s.COEFF[16] = 184.0 ; 
	FirFilter_3681_s.COEFF[17] = 192.0 ; 
	FirFilter_3681_s.COEFF[18] = 200.0 ; 
	FirFilter_3681_s.COEFF[19] = 208.0 ; 
	FirFilter_3681_s.COEFF[20] = 216.0 ; 
	FirFilter_3681_s.COEFF[21] = 224.0 ; 
	FirFilter_3681_s.COEFF[22] = 232.0 ; 
	FirFilter_3681_s.COEFF[23] = 240.0 ; 
	FirFilter_3681_s.COEFF[24] = 248.0 ; 
	FirFilter_3681_s.COEFF[25] = 256.0 ; 
	FirFilter_3681_s.COEFF[26] = 264.0 ; 
	FirFilter_3681_s.COEFF[27] = 272.0 ; 
	FirFilter_3681_s.COEFF[28] = 280.0 ; 
	FirFilter_3681_s.COEFF[29] = 288.0 ; 
	FirFilter_3681_s.COEFF[30] = 296.0 ; 
	FirFilter_3681_s.COEFF[31] = 304.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		source_3442();
		DUPLICATE_Splitter_3502();
			WEIGHTED_ROUND_ROBIN_Splitter_3522();
				Delay_N_3524();
				Delay_N_3525();
				Delay_N_3526();
			WEIGHTED_ROUND_ROBIN_Joiner_3523();
			DUPLICATE_Splitter_3527();
				FirFilter_3529();
				FirFilter_3530();
				FirFilter_3531();
			WEIGHTED_ROUND_ROBIN_Joiner_3528();
			DownSamp_3447();
			UpSamp_3448();
			WEIGHTED_ROUND_ROBIN_Splitter_3532();
				Delay_N_3534();
				Delay_N_3535();
				Delay_N_3536();
			WEIGHTED_ROUND_ROBIN_Joiner_3533();
			DUPLICATE_Splitter_3537();
				FirFilter_3539();
				FirFilter_3540();
				FirFilter_3541();
			WEIGHTED_ROUND_ROBIN_Joiner_3538();
			WEIGHTED_ROUND_ROBIN_Splitter_3542();
				Delay_N_3544();
				Delay_N_3545();
				Delay_N_3546();
			WEIGHTED_ROUND_ROBIN_Joiner_3543();
			DUPLICATE_Splitter_3547();
				FirFilter_3549();
				FirFilter_3550();
				FirFilter_3551();
			WEIGHTED_ROUND_ROBIN_Joiner_3548();
			DownSamp_3454();
			UpSamp_3455();
			WEIGHTED_ROUND_ROBIN_Splitter_3552();
				Delay_N_3554();
				Delay_N_3555();
				Delay_N_3556();
			WEIGHTED_ROUND_ROBIN_Joiner_3553();
			DUPLICATE_Splitter_3557();
				FirFilter_3559();
				FirFilter_3560();
				FirFilter_3561();
			WEIGHTED_ROUND_ROBIN_Joiner_3558();
			WEIGHTED_ROUND_ROBIN_Splitter_3562();
				Delay_N_3564();
				Delay_N_3565();
				Delay_N_3566();
			WEIGHTED_ROUND_ROBIN_Joiner_3563();
			DUPLICATE_Splitter_3567();
				FirFilter_3569();
				FirFilter_3570();
				FirFilter_3571();
			WEIGHTED_ROUND_ROBIN_Joiner_3568();
			DownSamp_3461();
			UpSamp_3462();
			WEIGHTED_ROUND_ROBIN_Splitter_3572();
				Delay_N_3574();
				Delay_N_3575();
				Delay_N_3576();
			WEIGHTED_ROUND_ROBIN_Joiner_3573();
			DUPLICATE_Splitter_3577();
				FirFilter_3579();
				FirFilter_3580();
				FirFilter_3581();
			WEIGHTED_ROUND_ROBIN_Joiner_3578();
			WEIGHTED_ROUND_ROBIN_Splitter_3582();
				Delay_N_3584();
				Delay_N_3585();
				Delay_N_3586();
			WEIGHTED_ROUND_ROBIN_Joiner_3583();
			DUPLICATE_Splitter_3587();
				FirFilter_3589();
				FirFilter_3590();
				FirFilter_3591();
			WEIGHTED_ROUND_ROBIN_Joiner_3588();
			DownSamp_3468();
			UpSamp_3469();
			WEIGHTED_ROUND_ROBIN_Splitter_3592();
				Delay_N_3594();
				Delay_N_3595();
				Delay_N_3596();
			WEIGHTED_ROUND_ROBIN_Joiner_3593();
			DUPLICATE_Splitter_3597();
				FirFilter_3599();
				FirFilter_3600();
				FirFilter_3601();
			WEIGHTED_ROUND_ROBIN_Joiner_3598();
			WEIGHTED_ROUND_ROBIN_Splitter_3602();
				Delay_N_3604();
				Delay_N_3605();
				Delay_N_3606();
			WEIGHTED_ROUND_ROBIN_Joiner_3603();
			DUPLICATE_Splitter_3607();
				FirFilter_3609();
				FirFilter_3610();
				FirFilter_3611();
			WEIGHTED_ROUND_ROBIN_Joiner_3608();
			DownSamp_3475();
			UpSamp_3476();
			WEIGHTED_ROUND_ROBIN_Splitter_3612();
				Delay_N_3614();
				Delay_N_3615();
				Delay_N_3616();
			WEIGHTED_ROUND_ROBIN_Joiner_3613();
			DUPLICATE_Splitter_3617();
				FirFilter_3619();
				FirFilter_3620();
				FirFilter_3621();
			WEIGHTED_ROUND_ROBIN_Joiner_3618();
			WEIGHTED_ROUND_ROBIN_Splitter_3622();
				Delay_N_3624();
				Delay_N_3625();
				Delay_N_3626();
			WEIGHTED_ROUND_ROBIN_Joiner_3623();
			DUPLICATE_Splitter_3627();
				FirFilter_3629();
				FirFilter_3630();
				FirFilter_3631();
			WEIGHTED_ROUND_ROBIN_Joiner_3628();
			DownSamp_3482();
			UpSamp_3483();
			WEIGHTED_ROUND_ROBIN_Splitter_3632();
				Delay_N_3634();
				Delay_N_3635();
				Delay_N_3636();
			WEIGHTED_ROUND_ROBIN_Joiner_3633();
			DUPLICATE_Splitter_3637();
				FirFilter_3639();
				FirFilter_3640();
				FirFilter_3641();
			WEIGHTED_ROUND_ROBIN_Joiner_3638();
			WEIGHTED_ROUND_ROBIN_Splitter_3642();
				Delay_N_3644();
				Delay_N_3645();
				Delay_N_3646();
			WEIGHTED_ROUND_ROBIN_Joiner_3643();
			DUPLICATE_Splitter_3647();
				FirFilter_3649();
				FirFilter_3650();
				FirFilter_3651();
			WEIGHTED_ROUND_ROBIN_Joiner_3648();
			DownSamp_3489();
			UpSamp_3490();
			WEIGHTED_ROUND_ROBIN_Splitter_3652();
				Delay_N_3654();
				Delay_N_3655();
				Delay_N_3656();
			WEIGHTED_ROUND_ROBIN_Joiner_3653();
			DUPLICATE_Splitter_3657();
				FirFilter_3659();
				FirFilter_3660();
				FirFilter_3661();
			WEIGHTED_ROUND_ROBIN_Joiner_3658();
			WEIGHTED_ROUND_ROBIN_Splitter_3662();
				Delay_N_3664();
				Delay_N_3665();
				Delay_N_3666();
			WEIGHTED_ROUND_ROBIN_Joiner_3663();
			DUPLICATE_Splitter_3667();
				FirFilter_3669();
				FirFilter_3670();
				FirFilter_3671();
			WEIGHTED_ROUND_ROBIN_Joiner_3668();
			DownSamp_3496();
			UpSamp_3497();
			WEIGHTED_ROUND_ROBIN_Splitter_3672();
				Delay_N_3674();
				Delay_N_3675();
				Delay_N_3676();
			WEIGHTED_ROUND_ROBIN_Joiner_3673();
			DUPLICATE_Splitter_3677();
				FirFilter_3679();
				FirFilter_3680();
				FirFilter_3681();
			WEIGHTED_ROUND_ROBIN_Joiner_3678();
		WEIGHTED_ROUND_ROBIN_Joiner_3503();
		WEIGHTED_ROUND_ROBIN_Splitter_3682();
			Combine_3684();
			Combine_3685();
			Combine_3686();
		WEIGHTED_ROUND_ROBIN_Joiner_3683();
		sink_3501();
	ENDFOR
	return EXIT_SUCCESS;
}
