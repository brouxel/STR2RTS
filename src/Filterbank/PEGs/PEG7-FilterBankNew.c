#include "PEG7-FilterBankNew.h"

buffer_float_t SplitJoin109_Delay_N_Fiss_1453_1487_split[7];
buffer_float_t SplitJoin115_FirFilter_Fiss_1456_1490_split[7];
buffer_float_t SplitJoin245_Delay_N_Fiss_1471_1505_split[7];
buffer_float_t SplitJoin78_FirFilter_Fiss_1450_1484_join[7];
buffer_float_t DownSamp_1081UpSamp_1082;
buffer_float_t SplitJoin179_Delay_N_Fiss_1463_1497_join[7];
buffer_float_t SplitJoin210_FirFilter_Fiss_1466_1500_join[7];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1377DUPLICATE_Splitter_1385;
buffer_float_t SplitJoin80_Delay_N_Fiss_1451_1485_join[7];
buffer_float_t SplitJoin0_SplitJoin0_SplitJoin0_Branches_1060_1124_1439_1474_join[8];
buffer_float_t SplitJoin80_Delay_N_Fiss_1451_1485_split[7];
buffer_float_t SplitJoin241_Delay_N_Fiss_1469_1503_split[7];
buffer_float_t SplitJoin247_FirFilter_Fiss_1472_1506_split[7];
buffer_float_t SplitJoin2_Delay_N_Fiss_1440_1475_split[7];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1413DUPLICATE_Splitter_1421;
buffer_float_t DownSamp_1109UpSamp_1110;
buffer_float_t SplitJoin148_FirFilter_Fiss_1460_1494_split[7];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1341DUPLICATE_Splitter_1349;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1431sink_1121;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1287DUPLICATE_Splitter_1295;
buffer_float_t SplitJoin212_Delay_N_Fiss_1467_1501_join[7];
buffer_float_t SplitJoin115_FirFilter_Fiss_1456_1490_join[7];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1359DUPLICATE_Splitter_1367;
buffer_float_t UpSamp_1103WEIGHTED_ROUND_ROBIN_Splitter_1340;
buffer_float_t UpSamp_1075WEIGHTED_ROUND_ROBIN_Splitter_1196;
buffer_float_t SplitJoin4_FirFilter_Fiss_1441_1476_join[7];
buffer_float_t DownSamp_1095UpSamp_1096;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1152DownSamp_1067;
buffer_float_t SplitJoin6_Delay_N_Fiss_1442_1477_split[7];
buffer_float_t SplitJoin214_FirFilter_Fiss_1468_1502_join[7];
buffer_float_t SplitJoin78_FirFilter_Fiss_1450_1484_split[7];
buffer_float_t SplitJoin111_FirFilter_Fiss_1454_1488_join[7];
buffer_float_t SplitJoin208_Delay_N_Fiss_1465_1499_join[7];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1332DownSamp_1102;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1269DUPLICATE_Splitter_1277;
buffer_float_t SplitJoin243_FirFilter_Fiss_1470_1504_split[7];
buffer_float_t SplitJoin212_Delay_N_Fiss_1467_1501_split[7];
buffer_float_t SplitJoin247_FirFilter_Fiss_1472_1506_join[7];
buffer_float_t SplitJoin111_FirFilter_Fiss_1454_1488_split[7];
buffer_float_t DownSamp_1102UpSamp_1103;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1305DUPLICATE_Splitter_1313;
buffer_float_t SplitJoin214_FirFilter_Fiss_1468_1502_split[7];
buffer_float_t SplitJoin142_Delay_N_Fiss_1457_1491_split[7];
buffer_float_t SplitJoin43_Delay_N_Fiss_1445_1479_join[7];
buffer_float_t SplitJoin208_Delay_N_Fiss_1465_1499_split[7];
buffer_float_t UpSamp_1068WEIGHTED_ROUND_ROBIN_Splitter_1160;
buffer_float_t SplitJoin148_FirFilter_Fiss_1460_1494_join[7];
buffer_float_t SplitJoin181_FirFilter_Fiss_1464_1498_join[7];
buffer_float_t SplitJoin82_FirFilter_Fiss_1452_1486_join[7];
buffer_float_t DownSamp_1074UpSamp_1075;
buffer_float_t SplitJoin179_Delay_N_Fiss_1463_1497_split[7];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1260DownSamp_1088;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1161DUPLICATE_Splitter_1169;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1323DUPLICATE_Splitter_1331;
buffer_float_t UpSamp_1110WEIGHTED_ROUND_ROBIN_Splitter_1376;
buffer_float_t SplitJoin245_Delay_N_Fiss_1471_1505_join[7];
buffer_float_t DownSamp_1067UpSamp_1068;
buffer_float_t SplitJoin76_Delay_N_Fiss_1449_1483_split[7];
buffer_float_t SplitJoin113_Delay_N_Fiss_1455_1489_split[7];
buffer_float_t source_1062DUPLICATE_Splitter_1122;
buffer_float_t SplitJoin4_FirFilter_Fiss_1441_1476_split[7];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1368DownSamp_1109;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1123WEIGHTED_ROUND_ROBIN_Splitter_1430;
buffer_float_t UpSamp_1082WEIGHTED_ROUND_ROBIN_Splitter_1232;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1404DownSamp_1116;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1143DUPLICATE_Splitter_1151;
buffer_float_t SplitJoin47_Delay_N_Fiss_1447_1481_join[7];
buffer_float_t SplitJoin177_FirFilter_Fiss_1462_1496_join[7];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1197DUPLICATE_Splitter_1205;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1215DUPLICATE_Splitter_1223;
buffer_float_t SplitJoin10_Combine_Fiss_1444_1507_join[7];
buffer_float_t SplitJoin144_FirFilter_Fiss_1458_1492_join[7];
buffer_float_t SplitJoin210_FirFilter_Fiss_1466_1500_split[7];
buffer_float_t SplitJoin6_Delay_N_Fiss_1442_1477_join[7];
buffer_float_t SplitJoin76_Delay_N_Fiss_1449_1483_join[7];
buffer_float_t SplitJoin146_Delay_N_Fiss_1459_1493_join[7];
buffer_float_t UpSamp_1117WEIGHTED_ROUND_ROBIN_Splitter_1412;
buffer_float_t SplitJoin241_Delay_N_Fiss_1469_1503_join[7];
buffer_float_t UpSamp_1089WEIGHTED_ROUND_ROBIN_Splitter_1268;
buffer_float_t SplitJoin45_FirFilter_Fiss_1446_1480_split[7];
buffer_float_t SplitJoin45_FirFilter_Fiss_1446_1480_join[7];
buffer_float_t SplitJoin8_FirFilter_Fiss_1443_1478_split[7];
buffer_float_t SplitJoin109_Delay_N_Fiss_1453_1487_join[7];
buffer_float_t SplitJoin2_Delay_N_Fiss_1440_1475_join[7];
buffer_float_t SplitJoin177_FirFilter_Fiss_1462_1496_split[7];
buffer_float_t DownSamp_1088UpSamp_1089;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1188DownSamp_1074;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1224DownSamp_1081;
buffer_float_t SplitJoin175_Delay_N_Fiss_1461_1495_join[7];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1233DUPLICATE_Splitter_1241;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1296DownSamp_1095;
buffer_float_t SplitJoin82_FirFilter_Fiss_1452_1486_split[7];
buffer_float_t SplitJoin243_FirFilter_Fiss_1470_1504_join[7];
buffer_float_t SplitJoin113_Delay_N_Fiss_1455_1489_join[7];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1179DUPLICATE_Splitter_1187;
buffer_float_t SplitJoin49_FirFilter_Fiss_1448_1482_join[7];
buffer_float_t SplitJoin47_Delay_N_Fiss_1447_1481_split[7];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1251DUPLICATE_Splitter_1259;
buffer_float_t SplitJoin144_FirFilter_Fiss_1458_1492_split[7];
buffer_float_t SplitJoin142_Delay_N_Fiss_1457_1491_join[7];
buffer_float_t SplitJoin181_FirFilter_Fiss_1464_1498_split[7];
buffer_float_t SplitJoin8_FirFilter_Fiss_1443_1478_join[7];
buffer_float_t SplitJoin49_FirFilter_Fiss_1448_1482_split[7];
buffer_float_t UpSamp_1096WEIGHTED_ROUND_ROBIN_Splitter_1304;
buffer_float_t SplitJoin10_Combine_Fiss_1444_1507_split[7];
buffer_float_t SplitJoin0_SplitJoin0_SplitJoin0_Branches_1060_1124_1439_1474_split[8];
buffer_float_t SplitJoin175_Delay_N_Fiss_1461_1495_split[7];
buffer_float_t SplitJoin43_Delay_N_Fiss_1445_1479_split[7];
buffer_float_t SplitJoin146_Delay_N_Fiss_1459_1493_split[7];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1395DUPLICATE_Splitter_1403;
buffer_float_t DownSamp_1116UpSamp_1117;


source_1062_t source_1062_s;
FirFilter_1153_t FirFilter_1153_s;
FirFilter_1153_t FirFilter_1154_s;
FirFilter_1153_t FirFilter_1155_s;
FirFilter_1153_t FirFilter_1156_s;
FirFilter_1153_t FirFilter_1157_s;
FirFilter_1153_t FirFilter_1158_s;
FirFilter_1153_t FirFilter_1159_s;
FirFilter_1153_t FirFilter_1171_s;
FirFilter_1153_t FirFilter_1172_s;
FirFilter_1153_t FirFilter_1173_s;
FirFilter_1153_t FirFilter_1174_s;
FirFilter_1153_t FirFilter_1175_s;
FirFilter_1153_t FirFilter_1176_s;
FirFilter_1153_t FirFilter_1177_s;
FirFilter_1153_t FirFilter_1189_s;
FirFilter_1153_t FirFilter_1190_s;
FirFilter_1153_t FirFilter_1191_s;
FirFilter_1153_t FirFilter_1192_s;
FirFilter_1153_t FirFilter_1193_s;
FirFilter_1153_t FirFilter_1194_s;
FirFilter_1153_t FirFilter_1195_s;
FirFilter_1153_t FirFilter_1207_s;
FirFilter_1153_t FirFilter_1208_s;
FirFilter_1153_t FirFilter_1209_s;
FirFilter_1153_t FirFilter_1210_s;
FirFilter_1153_t FirFilter_1211_s;
FirFilter_1153_t FirFilter_1212_s;
FirFilter_1153_t FirFilter_1213_s;
FirFilter_1153_t FirFilter_1225_s;
FirFilter_1153_t FirFilter_1226_s;
FirFilter_1153_t FirFilter_1227_s;
FirFilter_1153_t FirFilter_1228_s;
FirFilter_1153_t FirFilter_1229_s;
FirFilter_1153_t FirFilter_1230_s;
FirFilter_1153_t FirFilter_1231_s;
FirFilter_1153_t FirFilter_1243_s;
FirFilter_1153_t FirFilter_1244_s;
FirFilter_1153_t FirFilter_1245_s;
FirFilter_1153_t FirFilter_1246_s;
FirFilter_1153_t FirFilter_1247_s;
FirFilter_1153_t FirFilter_1248_s;
FirFilter_1153_t FirFilter_1249_s;
FirFilter_1153_t FirFilter_1261_s;
FirFilter_1153_t FirFilter_1262_s;
FirFilter_1153_t FirFilter_1263_s;
FirFilter_1153_t FirFilter_1264_s;
FirFilter_1153_t FirFilter_1265_s;
FirFilter_1153_t FirFilter_1266_s;
FirFilter_1153_t FirFilter_1267_s;
FirFilter_1153_t FirFilter_1279_s;
FirFilter_1153_t FirFilter_1280_s;
FirFilter_1153_t FirFilter_1281_s;
FirFilter_1153_t FirFilter_1282_s;
FirFilter_1153_t FirFilter_1283_s;
FirFilter_1153_t FirFilter_1284_s;
FirFilter_1153_t FirFilter_1285_s;
FirFilter_1153_t FirFilter_1297_s;
FirFilter_1153_t FirFilter_1298_s;
FirFilter_1153_t FirFilter_1299_s;
FirFilter_1153_t FirFilter_1300_s;
FirFilter_1153_t FirFilter_1301_s;
FirFilter_1153_t FirFilter_1302_s;
FirFilter_1153_t FirFilter_1303_s;
FirFilter_1153_t FirFilter_1315_s;
FirFilter_1153_t FirFilter_1316_s;
FirFilter_1153_t FirFilter_1317_s;
FirFilter_1153_t FirFilter_1318_s;
FirFilter_1153_t FirFilter_1319_s;
FirFilter_1153_t FirFilter_1320_s;
FirFilter_1153_t FirFilter_1321_s;
FirFilter_1153_t FirFilter_1333_s;
FirFilter_1153_t FirFilter_1334_s;
FirFilter_1153_t FirFilter_1335_s;
FirFilter_1153_t FirFilter_1336_s;
FirFilter_1153_t FirFilter_1337_s;
FirFilter_1153_t FirFilter_1338_s;
FirFilter_1153_t FirFilter_1339_s;
FirFilter_1153_t FirFilter_1351_s;
FirFilter_1153_t FirFilter_1352_s;
FirFilter_1153_t FirFilter_1353_s;
FirFilter_1153_t FirFilter_1354_s;
FirFilter_1153_t FirFilter_1355_s;
FirFilter_1153_t FirFilter_1356_s;
FirFilter_1153_t FirFilter_1357_s;
FirFilter_1153_t FirFilter_1369_s;
FirFilter_1153_t FirFilter_1370_s;
FirFilter_1153_t FirFilter_1371_s;
FirFilter_1153_t FirFilter_1372_s;
FirFilter_1153_t FirFilter_1373_s;
FirFilter_1153_t FirFilter_1374_s;
FirFilter_1153_t FirFilter_1375_s;
FirFilter_1153_t FirFilter_1387_s;
FirFilter_1153_t FirFilter_1388_s;
FirFilter_1153_t FirFilter_1389_s;
FirFilter_1153_t FirFilter_1390_s;
FirFilter_1153_t FirFilter_1391_s;
FirFilter_1153_t FirFilter_1392_s;
FirFilter_1153_t FirFilter_1393_s;
FirFilter_1153_t FirFilter_1405_s;
FirFilter_1153_t FirFilter_1406_s;
FirFilter_1153_t FirFilter_1407_s;
FirFilter_1153_t FirFilter_1408_s;
FirFilter_1153_t FirFilter_1409_s;
FirFilter_1153_t FirFilter_1410_s;
FirFilter_1153_t FirFilter_1411_s;
FirFilter_1153_t FirFilter_1423_s;
FirFilter_1153_t FirFilter_1424_s;
FirFilter_1153_t FirFilter_1425_s;
FirFilter_1153_t FirFilter_1426_s;
FirFilter_1153_t FirFilter_1427_s;
FirFilter_1153_t FirFilter_1428_s;
FirFilter_1153_t FirFilter_1429_s;

void source(buffer_float_t *chanout) {
		push_float(&(*chanout), source_1062_s.current) ; 
		if((source_1062_s.current > 1000.0)) {
			source_1062_s.current = 0.0 ; 
		}
		else {
			source_1062_s.current = (source_1062_s.current + 1.0) ; 
		}
	}


void source_1062() {
	FOR(uint32_t, __iter_steady_, 0, <, 56, __iter_steady_++)
		source(&(source_1062DUPLICATE_Splitter_1122));
	ENDFOR
}

void Delay_N(buffer_float_t *chanin, buffer_float_t *chanout) {
	}


void Delay_N_1144() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin2_Delay_N_Fiss_1440_1475_split[0]), &(SplitJoin2_Delay_N_Fiss_1440_1475_join[0]));
	ENDFOR
}

void Delay_N_1145() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin2_Delay_N_Fiss_1440_1475_split[1]), &(SplitJoin2_Delay_N_Fiss_1440_1475_join[1]));
	ENDFOR
}

void Delay_N_1146() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin2_Delay_N_Fiss_1440_1475_split[2]), &(SplitJoin2_Delay_N_Fiss_1440_1475_join[2]));
	ENDFOR
}

void Delay_N_1147() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin2_Delay_N_Fiss_1440_1475_split[3]), &(SplitJoin2_Delay_N_Fiss_1440_1475_join[3]));
	ENDFOR
}

void Delay_N_1148() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin2_Delay_N_Fiss_1440_1475_split[4]), &(SplitJoin2_Delay_N_Fiss_1440_1475_join[4]));
	ENDFOR
}

void Delay_N_1149() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin2_Delay_N_Fiss_1440_1475_split[5]), &(SplitJoin2_Delay_N_Fiss_1440_1475_join[5]));
	ENDFOR
}

void Delay_N_1150() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin2_Delay_N_Fiss_1440_1475_split[6]), &(SplitJoin2_Delay_N_Fiss_1440_1475_join[6]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1142() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&SplitJoin2_Delay_N_Fiss_1440_1475_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_1060_1124_1439_1474_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1143() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1143DUPLICATE_Splitter_1151, pop_float(&SplitJoin2_Delay_N_Fiss_1440_1475_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&(*chanin), i) * FirFilter_1153_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&(*chanin)) ; 
		push_float(&(*chanout), sum) ; 
 {
		FOR(int, streamItVar343, 0,  < , 6, streamItVar343++) {
			pop_void(&(*chanin)) ; 
		}
		ENDFOR
	}
	}


void FirFilter_1153() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin4_FirFilter_Fiss_1441_1476_split[0]), &(SplitJoin4_FirFilter_Fiss_1441_1476_join[0]));
	ENDFOR
}

void FirFilter_1154() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin4_FirFilter_Fiss_1441_1476_split[1]), &(SplitJoin4_FirFilter_Fiss_1441_1476_join[1]));
	ENDFOR
}

void FirFilter_1155() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin4_FirFilter_Fiss_1441_1476_split[2]), &(SplitJoin4_FirFilter_Fiss_1441_1476_join[2]));
	ENDFOR
}

void FirFilter_1156() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin4_FirFilter_Fiss_1441_1476_split[3]), &(SplitJoin4_FirFilter_Fiss_1441_1476_join[3]));
	ENDFOR
}

void FirFilter_1157() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin4_FirFilter_Fiss_1441_1476_split[4]), &(SplitJoin4_FirFilter_Fiss_1441_1476_join[4]));
	ENDFOR
}

void FirFilter_1158() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin4_FirFilter_Fiss_1441_1476_split[5]), &(SplitJoin4_FirFilter_Fiss_1441_1476_join[5]));
	ENDFOR
}

void FirFilter_1159() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin4_FirFilter_Fiss_1441_1476_split[6]), &(SplitJoin4_FirFilter_Fiss_1441_1476_join[6]));
	ENDFOR
}

void DUPLICATE_Splitter_1151() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 56, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1143DUPLICATE_Splitter_1151);
		FOR(uint32_t, __iter_dup_, 0, <, 7, __iter_dup_++)
			push_float(&SplitJoin4_FirFilter_Fiss_1441_1476_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1152() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1152DownSamp_1067, pop_float(&SplitJoin4_FirFilter_Fiss_1441_1476_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp(buffer_float_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_float(&(*chanin))) ; 
		FOR(int, i, 0,  < , 7, i++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void DownSamp_1067() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_1152DownSamp_1067), &(DownSamp_1067UpSamp_1068));
	ENDFOR
}

void UpSamp(buffer_float_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_float(&(*chanin))) ; 
		FOR(int, i, 0,  < , 7, i++) {
			push_float(&(*chanout), 0.0) ; 
		}
		ENDFOR
	}


void UpSamp_1068() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		UpSamp(&(DownSamp_1067UpSamp_1068), &(UpSamp_1068WEIGHTED_ROUND_ROBIN_Splitter_1160));
	ENDFOR
}

void Delay_N_1162() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin6_Delay_N_Fiss_1442_1477_split[0]), &(SplitJoin6_Delay_N_Fiss_1442_1477_join[0]));
	ENDFOR
}

void Delay_N_1163() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin6_Delay_N_Fiss_1442_1477_split[1]), &(SplitJoin6_Delay_N_Fiss_1442_1477_join[1]));
	ENDFOR
}

void Delay_N_1164() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin6_Delay_N_Fiss_1442_1477_split[2]), &(SplitJoin6_Delay_N_Fiss_1442_1477_join[2]));
	ENDFOR
}

void Delay_N_1165() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin6_Delay_N_Fiss_1442_1477_split[3]), &(SplitJoin6_Delay_N_Fiss_1442_1477_join[3]));
	ENDFOR
}

void Delay_N_1166() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin6_Delay_N_Fiss_1442_1477_split[4]), &(SplitJoin6_Delay_N_Fiss_1442_1477_join[4]));
	ENDFOR
}

void Delay_N_1167() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin6_Delay_N_Fiss_1442_1477_split[5]), &(SplitJoin6_Delay_N_Fiss_1442_1477_join[5]));
	ENDFOR
}

void Delay_N_1168() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin6_Delay_N_Fiss_1442_1477_split[6]), &(SplitJoin6_Delay_N_Fiss_1442_1477_join[6]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1160() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&SplitJoin6_Delay_N_Fiss_1442_1477_split[__iter_], pop_float(&UpSamp_1068WEIGHTED_ROUND_ROBIN_Splitter_1160));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1161() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1161DUPLICATE_Splitter_1169, pop_float(&SplitJoin6_Delay_N_Fiss_1442_1477_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_1171() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin8_FirFilter_Fiss_1443_1478_split[0]), &(SplitJoin8_FirFilter_Fiss_1443_1478_join[0]));
	ENDFOR
}

void FirFilter_1172() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin8_FirFilter_Fiss_1443_1478_split[1]), &(SplitJoin8_FirFilter_Fiss_1443_1478_join[1]));
	ENDFOR
}

void FirFilter_1173() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin8_FirFilter_Fiss_1443_1478_split[2]), &(SplitJoin8_FirFilter_Fiss_1443_1478_join[2]));
	ENDFOR
}

void FirFilter_1174() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin8_FirFilter_Fiss_1443_1478_split[3]), &(SplitJoin8_FirFilter_Fiss_1443_1478_join[3]));
	ENDFOR
}

void FirFilter_1175() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin8_FirFilter_Fiss_1443_1478_split[4]), &(SplitJoin8_FirFilter_Fiss_1443_1478_join[4]));
	ENDFOR
}

void FirFilter_1176() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin8_FirFilter_Fiss_1443_1478_split[5]), &(SplitJoin8_FirFilter_Fiss_1443_1478_join[5]));
	ENDFOR
}

void FirFilter_1177() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin8_FirFilter_Fiss_1443_1478_split[6]), &(SplitJoin8_FirFilter_Fiss_1443_1478_join[6]));
	ENDFOR
}

void DUPLICATE_Splitter_1169() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 56, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1161DUPLICATE_Splitter_1169);
		FOR(uint32_t, __iter_dup_, 0, <, 7, __iter_dup_++)
			push_float(&SplitJoin8_FirFilter_Fiss_1443_1478_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1170() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_1060_1124_1439_1474_join[0], pop_float(&SplitJoin8_FirFilter_Fiss_1443_1478_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Delay_N_1180() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin43_Delay_N_Fiss_1445_1479_split[0]), &(SplitJoin43_Delay_N_Fiss_1445_1479_join[0]));
	ENDFOR
}

void Delay_N_1181() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin43_Delay_N_Fiss_1445_1479_split[1]), &(SplitJoin43_Delay_N_Fiss_1445_1479_join[1]));
	ENDFOR
}

void Delay_N_1182() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin43_Delay_N_Fiss_1445_1479_split[2]), &(SplitJoin43_Delay_N_Fiss_1445_1479_join[2]));
	ENDFOR
}

void Delay_N_1183() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin43_Delay_N_Fiss_1445_1479_split[3]), &(SplitJoin43_Delay_N_Fiss_1445_1479_join[3]));
	ENDFOR
}

void Delay_N_1184() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin43_Delay_N_Fiss_1445_1479_split[4]), &(SplitJoin43_Delay_N_Fiss_1445_1479_join[4]));
	ENDFOR
}

void Delay_N_1185() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin43_Delay_N_Fiss_1445_1479_split[5]), &(SplitJoin43_Delay_N_Fiss_1445_1479_join[5]));
	ENDFOR
}

void Delay_N_1186() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin43_Delay_N_Fiss_1445_1479_split[6]), &(SplitJoin43_Delay_N_Fiss_1445_1479_join[6]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1178() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&SplitJoin43_Delay_N_Fiss_1445_1479_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_1060_1124_1439_1474_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1179() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1179DUPLICATE_Splitter_1187, pop_float(&SplitJoin43_Delay_N_Fiss_1445_1479_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_1189() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin45_FirFilter_Fiss_1446_1480_split[0]), &(SplitJoin45_FirFilter_Fiss_1446_1480_join[0]));
	ENDFOR
}

void FirFilter_1190() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin45_FirFilter_Fiss_1446_1480_split[1]), &(SplitJoin45_FirFilter_Fiss_1446_1480_join[1]));
	ENDFOR
}

void FirFilter_1191() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin45_FirFilter_Fiss_1446_1480_split[2]), &(SplitJoin45_FirFilter_Fiss_1446_1480_join[2]));
	ENDFOR
}

void FirFilter_1192() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin45_FirFilter_Fiss_1446_1480_split[3]), &(SplitJoin45_FirFilter_Fiss_1446_1480_join[3]));
	ENDFOR
}

void FirFilter_1193() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin45_FirFilter_Fiss_1446_1480_split[4]), &(SplitJoin45_FirFilter_Fiss_1446_1480_join[4]));
	ENDFOR
}

void FirFilter_1194() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin45_FirFilter_Fiss_1446_1480_split[5]), &(SplitJoin45_FirFilter_Fiss_1446_1480_join[5]));
	ENDFOR
}

void FirFilter_1195() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin45_FirFilter_Fiss_1446_1480_split[6]), &(SplitJoin45_FirFilter_Fiss_1446_1480_join[6]));
	ENDFOR
}

void DUPLICATE_Splitter_1187() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 56, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1179DUPLICATE_Splitter_1187);
		FOR(uint32_t, __iter_dup_, 0, <, 7, __iter_dup_++)
			push_float(&SplitJoin45_FirFilter_Fiss_1446_1480_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1188() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1188DownSamp_1074, pop_float(&SplitJoin45_FirFilter_Fiss_1446_1480_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp_1074() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_1188DownSamp_1074), &(DownSamp_1074UpSamp_1075));
	ENDFOR
}

void UpSamp_1075() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		UpSamp(&(DownSamp_1074UpSamp_1075), &(UpSamp_1075WEIGHTED_ROUND_ROBIN_Splitter_1196));
	ENDFOR
}

void Delay_N_1198() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin47_Delay_N_Fiss_1447_1481_split[0]), &(SplitJoin47_Delay_N_Fiss_1447_1481_join[0]));
	ENDFOR
}

void Delay_N_1199() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin47_Delay_N_Fiss_1447_1481_split[1]), &(SplitJoin47_Delay_N_Fiss_1447_1481_join[1]));
	ENDFOR
}

void Delay_N_1200() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin47_Delay_N_Fiss_1447_1481_split[2]), &(SplitJoin47_Delay_N_Fiss_1447_1481_join[2]));
	ENDFOR
}

void Delay_N_1201() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin47_Delay_N_Fiss_1447_1481_split[3]), &(SplitJoin47_Delay_N_Fiss_1447_1481_join[3]));
	ENDFOR
}

void Delay_N_1202() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin47_Delay_N_Fiss_1447_1481_split[4]), &(SplitJoin47_Delay_N_Fiss_1447_1481_join[4]));
	ENDFOR
}

void Delay_N_1203() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin47_Delay_N_Fiss_1447_1481_split[5]), &(SplitJoin47_Delay_N_Fiss_1447_1481_join[5]));
	ENDFOR
}

void Delay_N_1204() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin47_Delay_N_Fiss_1447_1481_split[6]), &(SplitJoin47_Delay_N_Fiss_1447_1481_join[6]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1196() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&SplitJoin47_Delay_N_Fiss_1447_1481_split[__iter_], pop_float(&UpSamp_1075WEIGHTED_ROUND_ROBIN_Splitter_1196));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1197() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1197DUPLICATE_Splitter_1205, pop_float(&SplitJoin47_Delay_N_Fiss_1447_1481_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_1207() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin49_FirFilter_Fiss_1448_1482_split[0]), &(SplitJoin49_FirFilter_Fiss_1448_1482_join[0]));
	ENDFOR
}

void FirFilter_1208() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin49_FirFilter_Fiss_1448_1482_split[1]), &(SplitJoin49_FirFilter_Fiss_1448_1482_join[1]));
	ENDFOR
}

void FirFilter_1209() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin49_FirFilter_Fiss_1448_1482_split[2]), &(SplitJoin49_FirFilter_Fiss_1448_1482_join[2]));
	ENDFOR
}

void FirFilter_1210() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin49_FirFilter_Fiss_1448_1482_split[3]), &(SplitJoin49_FirFilter_Fiss_1448_1482_join[3]));
	ENDFOR
}

void FirFilter_1211() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin49_FirFilter_Fiss_1448_1482_split[4]), &(SplitJoin49_FirFilter_Fiss_1448_1482_join[4]));
	ENDFOR
}

void FirFilter_1212() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin49_FirFilter_Fiss_1448_1482_split[5]), &(SplitJoin49_FirFilter_Fiss_1448_1482_join[5]));
	ENDFOR
}

void FirFilter_1213() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin49_FirFilter_Fiss_1448_1482_split[6]), &(SplitJoin49_FirFilter_Fiss_1448_1482_join[6]));
	ENDFOR
}

void DUPLICATE_Splitter_1205() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 56, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1197DUPLICATE_Splitter_1205);
		FOR(uint32_t, __iter_dup_, 0, <, 7, __iter_dup_++)
			push_float(&SplitJoin49_FirFilter_Fiss_1448_1482_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1206() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_1060_1124_1439_1474_join[1], pop_float(&SplitJoin49_FirFilter_Fiss_1448_1482_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Delay_N_1216() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin76_Delay_N_Fiss_1449_1483_split[0]), &(SplitJoin76_Delay_N_Fiss_1449_1483_join[0]));
	ENDFOR
}

void Delay_N_1217() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin76_Delay_N_Fiss_1449_1483_split[1]), &(SplitJoin76_Delay_N_Fiss_1449_1483_join[1]));
	ENDFOR
}

void Delay_N_1218() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin76_Delay_N_Fiss_1449_1483_split[2]), &(SplitJoin76_Delay_N_Fiss_1449_1483_join[2]));
	ENDFOR
}

void Delay_N_1219() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin76_Delay_N_Fiss_1449_1483_split[3]), &(SplitJoin76_Delay_N_Fiss_1449_1483_join[3]));
	ENDFOR
}

void Delay_N_1220() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin76_Delay_N_Fiss_1449_1483_split[4]), &(SplitJoin76_Delay_N_Fiss_1449_1483_join[4]));
	ENDFOR
}

void Delay_N_1221() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin76_Delay_N_Fiss_1449_1483_split[5]), &(SplitJoin76_Delay_N_Fiss_1449_1483_join[5]));
	ENDFOR
}

void Delay_N_1222() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin76_Delay_N_Fiss_1449_1483_split[6]), &(SplitJoin76_Delay_N_Fiss_1449_1483_join[6]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1214() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&SplitJoin76_Delay_N_Fiss_1449_1483_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_1060_1124_1439_1474_split[2]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1215() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1215DUPLICATE_Splitter_1223, pop_float(&SplitJoin76_Delay_N_Fiss_1449_1483_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_1225() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin78_FirFilter_Fiss_1450_1484_split[0]), &(SplitJoin78_FirFilter_Fiss_1450_1484_join[0]));
	ENDFOR
}

void FirFilter_1226() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin78_FirFilter_Fiss_1450_1484_split[1]), &(SplitJoin78_FirFilter_Fiss_1450_1484_join[1]));
	ENDFOR
}

void FirFilter_1227() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin78_FirFilter_Fiss_1450_1484_split[2]), &(SplitJoin78_FirFilter_Fiss_1450_1484_join[2]));
	ENDFOR
}

void FirFilter_1228() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin78_FirFilter_Fiss_1450_1484_split[3]), &(SplitJoin78_FirFilter_Fiss_1450_1484_join[3]));
	ENDFOR
}

void FirFilter_1229() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin78_FirFilter_Fiss_1450_1484_split[4]), &(SplitJoin78_FirFilter_Fiss_1450_1484_join[4]));
	ENDFOR
}

void FirFilter_1230() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin78_FirFilter_Fiss_1450_1484_split[5]), &(SplitJoin78_FirFilter_Fiss_1450_1484_join[5]));
	ENDFOR
}

void FirFilter_1231() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin78_FirFilter_Fiss_1450_1484_split[6]), &(SplitJoin78_FirFilter_Fiss_1450_1484_join[6]));
	ENDFOR
}

void DUPLICATE_Splitter_1223() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 56, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1215DUPLICATE_Splitter_1223);
		FOR(uint32_t, __iter_dup_, 0, <, 7, __iter_dup_++)
			push_float(&SplitJoin78_FirFilter_Fiss_1450_1484_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1224() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1224DownSamp_1081, pop_float(&SplitJoin78_FirFilter_Fiss_1450_1484_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp_1081() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_1224DownSamp_1081), &(DownSamp_1081UpSamp_1082));
	ENDFOR
}

void UpSamp_1082() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		UpSamp(&(DownSamp_1081UpSamp_1082), &(UpSamp_1082WEIGHTED_ROUND_ROBIN_Splitter_1232));
	ENDFOR
}

void Delay_N_1234() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin80_Delay_N_Fiss_1451_1485_split[0]), &(SplitJoin80_Delay_N_Fiss_1451_1485_join[0]));
	ENDFOR
}

void Delay_N_1235() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin80_Delay_N_Fiss_1451_1485_split[1]), &(SplitJoin80_Delay_N_Fiss_1451_1485_join[1]));
	ENDFOR
}

void Delay_N_1236() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin80_Delay_N_Fiss_1451_1485_split[2]), &(SplitJoin80_Delay_N_Fiss_1451_1485_join[2]));
	ENDFOR
}

void Delay_N_1237() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin80_Delay_N_Fiss_1451_1485_split[3]), &(SplitJoin80_Delay_N_Fiss_1451_1485_join[3]));
	ENDFOR
}

void Delay_N_1238() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin80_Delay_N_Fiss_1451_1485_split[4]), &(SplitJoin80_Delay_N_Fiss_1451_1485_join[4]));
	ENDFOR
}

void Delay_N_1239() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin80_Delay_N_Fiss_1451_1485_split[5]), &(SplitJoin80_Delay_N_Fiss_1451_1485_join[5]));
	ENDFOR
}

void Delay_N_1240() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin80_Delay_N_Fiss_1451_1485_split[6]), &(SplitJoin80_Delay_N_Fiss_1451_1485_join[6]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1232() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&SplitJoin80_Delay_N_Fiss_1451_1485_split[__iter_], pop_float(&UpSamp_1082WEIGHTED_ROUND_ROBIN_Splitter_1232));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1233() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1233DUPLICATE_Splitter_1241, pop_float(&SplitJoin80_Delay_N_Fiss_1451_1485_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_1243() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin82_FirFilter_Fiss_1452_1486_split[0]), &(SplitJoin82_FirFilter_Fiss_1452_1486_join[0]));
	ENDFOR
}

void FirFilter_1244() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin82_FirFilter_Fiss_1452_1486_split[1]), &(SplitJoin82_FirFilter_Fiss_1452_1486_join[1]));
	ENDFOR
}

void FirFilter_1245() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin82_FirFilter_Fiss_1452_1486_split[2]), &(SplitJoin82_FirFilter_Fiss_1452_1486_join[2]));
	ENDFOR
}

void FirFilter_1246() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin82_FirFilter_Fiss_1452_1486_split[3]), &(SplitJoin82_FirFilter_Fiss_1452_1486_join[3]));
	ENDFOR
}

void FirFilter_1247() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin82_FirFilter_Fiss_1452_1486_split[4]), &(SplitJoin82_FirFilter_Fiss_1452_1486_join[4]));
	ENDFOR
}

void FirFilter_1248() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin82_FirFilter_Fiss_1452_1486_split[5]), &(SplitJoin82_FirFilter_Fiss_1452_1486_join[5]));
	ENDFOR
}

void FirFilter_1249() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin82_FirFilter_Fiss_1452_1486_split[6]), &(SplitJoin82_FirFilter_Fiss_1452_1486_join[6]));
	ENDFOR
}

void DUPLICATE_Splitter_1241() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 56, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1233DUPLICATE_Splitter_1241);
		FOR(uint32_t, __iter_dup_, 0, <, 7, __iter_dup_++)
			push_float(&SplitJoin82_FirFilter_Fiss_1452_1486_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1242() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_1060_1124_1439_1474_join[2], pop_float(&SplitJoin82_FirFilter_Fiss_1452_1486_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Delay_N_1252() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin109_Delay_N_Fiss_1453_1487_split[0]), &(SplitJoin109_Delay_N_Fiss_1453_1487_join[0]));
	ENDFOR
}

void Delay_N_1253() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin109_Delay_N_Fiss_1453_1487_split[1]), &(SplitJoin109_Delay_N_Fiss_1453_1487_join[1]));
	ENDFOR
}

void Delay_N_1254() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin109_Delay_N_Fiss_1453_1487_split[2]), &(SplitJoin109_Delay_N_Fiss_1453_1487_join[2]));
	ENDFOR
}

void Delay_N_1255() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin109_Delay_N_Fiss_1453_1487_split[3]), &(SplitJoin109_Delay_N_Fiss_1453_1487_join[3]));
	ENDFOR
}

void Delay_N_1256() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin109_Delay_N_Fiss_1453_1487_split[4]), &(SplitJoin109_Delay_N_Fiss_1453_1487_join[4]));
	ENDFOR
}

void Delay_N_1257() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin109_Delay_N_Fiss_1453_1487_split[5]), &(SplitJoin109_Delay_N_Fiss_1453_1487_join[5]));
	ENDFOR
}

void Delay_N_1258() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin109_Delay_N_Fiss_1453_1487_split[6]), &(SplitJoin109_Delay_N_Fiss_1453_1487_join[6]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1250() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&SplitJoin109_Delay_N_Fiss_1453_1487_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_1060_1124_1439_1474_split[3]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1251() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1251DUPLICATE_Splitter_1259, pop_float(&SplitJoin109_Delay_N_Fiss_1453_1487_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_1261() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin111_FirFilter_Fiss_1454_1488_split[0]), &(SplitJoin111_FirFilter_Fiss_1454_1488_join[0]));
	ENDFOR
}

void FirFilter_1262() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin111_FirFilter_Fiss_1454_1488_split[1]), &(SplitJoin111_FirFilter_Fiss_1454_1488_join[1]));
	ENDFOR
}

void FirFilter_1263() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin111_FirFilter_Fiss_1454_1488_split[2]), &(SplitJoin111_FirFilter_Fiss_1454_1488_join[2]));
	ENDFOR
}

void FirFilter_1264() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin111_FirFilter_Fiss_1454_1488_split[3]), &(SplitJoin111_FirFilter_Fiss_1454_1488_join[3]));
	ENDFOR
}

void FirFilter_1265() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin111_FirFilter_Fiss_1454_1488_split[4]), &(SplitJoin111_FirFilter_Fiss_1454_1488_join[4]));
	ENDFOR
}

void FirFilter_1266() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin111_FirFilter_Fiss_1454_1488_split[5]), &(SplitJoin111_FirFilter_Fiss_1454_1488_join[5]));
	ENDFOR
}

void FirFilter_1267() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin111_FirFilter_Fiss_1454_1488_split[6]), &(SplitJoin111_FirFilter_Fiss_1454_1488_join[6]));
	ENDFOR
}

void DUPLICATE_Splitter_1259() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 56, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1251DUPLICATE_Splitter_1259);
		FOR(uint32_t, __iter_dup_, 0, <, 7, __iter_dup_++)
			push_float(&SplitJoin111_FirFilter_Fiss_1454_1488_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1260() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1260DownSamp_1088, pop_float(&SplitJoin111_FirFilter_Fiss_1454_1488_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp_1088() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_1260DownSamp_1088), &(DownSamp_1088UpSamp_1089));
	ENDFOR
}

void UpSamp_1089() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		UpSamp(&(DownSamp_1088UpSamp_1089), &(UpSamp_1089WEIGHTED_ROUND_ROBIN_Splitter_1268));
	ENDFOR
}

void Delay_N_1270() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin113_Delay_N_Fiss_1455_1489_split[0]), &(SplitJoin113_Delay_N_Fiss_1455_1489_join[0]));
	ENDFOR
}

void Delay_N_1271() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin113_Delay_N_Fiss_1455_1489_split[1]), &(SplitJoin113_Delay_N_Fiss_1455_1489_join[1]));
	ENDFOR
}

void Delay_N_1272() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin113_Delay_N_Fiss_1455_1489_split[2]), &(SplitJoin113_Delay_N_Fiss_1455_1489_join[2]));
	ENDFOR
}

void Delay_N_1273() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin113_Delay_N_Fiss_1455_1489_split[3]), &(SplitJoin113_Delay_N_Fiss_1455_1489_join[3]));
	ENDFOR
}

void Delay_N_1274() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin113_Delay_N_Fiss_1455_1489_split[4]), &(SplitJoin113_Delay_N_Fiss_1455_1489_join[4]));
	ENDFOR
}

void Delay_N_1275() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin113_Delay_N_Fiss_1455_1489_split[5]), &(SplitJoin113_Delay_N_Fiss_1455_1489_join[5]));
	ENDFOR
}

void Delay_N_1276() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin113_Delay_N_Fiss_1455_1489_split[6]), &(SplitJoin113_Delay_N_Fiss_1455_1489_join[6]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1268() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&SplitJoin113_Delay_N_Fiss_1455_1489_split[__iter_], pop_float(&UpSamp_1089WEIGHTED_ROUND_ROBIN_Splitter_1268));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1269() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1269DUPLICATE_Splitter_1277, pop_float(&SplitJoin113_Delay_N_Fiss_1455_1489_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_1279() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin115_FirFilter_Fiss_1456_1490_split[0]), &(SplitJoin115_FirFilter_Fiss_1456_1490_join[0]));
	ENDFOR
}

void FirFilter_1280() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin115_FirFilter_Fiss_1456_1490_split[1]), &(SplitJoin115_FirFilter_Fiss_1456_1490_join[1]));
	ENDFOR
}

void FirFilter_1281() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin115_FirFilter_Fiss_1456_1490_split[2]), &(SplitJoin115_FirFilter_Fiss_1456_1490_join[2]));
	ENDFOR
}

void FirFilter_1282() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin115_FirFilter_Fiss_1456_1490_split[3]), &(SplitJoin115_FirFilter_Fiss_1456_1490_join[3]));
	ENDFOR
}

void FirFilter_1283() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin115_FirFilter_Fiss_1456_1490_split[4]), &(SplitJoin115_FirFilter_Fiss_1456_1490_join[4]));
	ENDFOR
}

void FirFilter_1284() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin115_FirFilter_Fiss_1456_1490_split[5]), &(SplitJoin115_FirFilter_Fiss_1456_1490_join[5]));
	ENDFOR
}

void FirFilter_1285() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin115_FirFilter_Fiss_1456_1490_split[6]), &(SplitJoin115_FirFilter_Fiss_1456_1490_join[6]));
	ENDFOR
}

void DUPLICATE_Splitter_1277() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 56, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1269DUPLICATE_Splitter_1277);
		FOR(uint32_t, __iter_dup_, 0, <, 7, __iter_dup_++)
			push_float(&SplitJoin115_FirFilter_Fiss_1456_1490_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1278() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_1060_1124_1439_1474_join[3], pop_float(&SplitJoin115_FirFilter_Fiss_1456_1490_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Delay_N_1288() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin142_Delay_N_Fiss_1457_1491_split[0]), &(SplitJoin142_Delay_N_Fiss_1457_1491_join[0]));
	ENDFOR
}

void Delay_N_1289() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin142_Delay_N_Fiss_1457_1491_split[1]), &(SplitJoin142_Delay_N_Fiss_1457_1491_join[1]));
	ENDFOR
}

void Delay_N_1290() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin142_Delay_N_Fiss_1457_1491_split[2]), &(SplitJoin142_Delay_N_Fiss_1457_1491_join[2]));
	ENDFOR
}

void Delay_N_1291() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin142_Delay_N_Fiss_1457_1491_split[3]), &(SplitJoin142_Delay_N_Fiss_1457_1491_join[3]));
	ENDFOR
}

void Delay_N_1292() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin142_Delay_N_Fiss_1457_1491_split[4]), &(SplitJoin142_Delay_N_Fiss_1457_1491_join[4]));
	ENDFOR
}

void Delay_N_1293() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin142_Delay_N_Fiss_1457_1491_split[5]), &(SplitJoin142_Delay_N_Fiss_1457_1491_join[5]));
	ENDFOR
}

void Delay_N_1294() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin142_Delay_N_Fiss_1457_1491_split[6]), &(SplitJoin142_Delay_N_Fiss_1457_1491_join[6]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1286() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&SplitJoin142_Delay_N_Fiss_1457_1491_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_1060_1124_1439_1474_split[4]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1287() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1287DUPLICATE_Splitter_1295, pop_float(&SplitJoin142_Delay_N_Fiss_1457_1491_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_1297() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin144_FirFilter_Fiss_1458_1492_split[0]), &(SplitJoin144_FirFilter_Fiss_1458_1492_join[0]));
	ENDFOR
}

void FirFilter_1298() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin144_FirFilter_Fiss_1458_1492_split[1]), &(SplitJoin144_FirFilter_Fiss_1458_1492_join[1]));
	ENDFOR
}

void FirFilter_1299() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin144_FirFilter_Fiss_1458_1492_split[2]), &(SplitJoin144_FirFilter_Fiss_1458_1492_join[2]));
	ENDFOR
}

void FirFilter_1300() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin144_FirFilter_Fiss_1458_1492_split[3]), &(SplitJoin144_FirFilter_Fiss_1458_1492_join[3]));
	ENDFOR
}

void FirFilter_1301() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin144_FirFilter_Fiss_1458_1492_split[4]), &(SplitJoin144_FirFilter_Fiss_1458_1492_join[4]));
	ENDFOR
}

void FirFilter_1302() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin144_FirFilter_Fiss_1458_1492_split[5]), &(SplitJoin144_FirFilter_Fiss_1458_1492_join[5]));
	ENDFOR
}

void FirFilter_1303() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin144_FirFilter_Fiss_1458_1492_split[6]), &(SplitJoin144_FirFilter_Fiss_1458_1492_join[6]));
	ENDFOR
}

void DUPLICATE_Splitter_1295() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 56, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1287DUPLICATE_Splitter_1295);
		FOR(uint32_t, __iter_dup_, 0, <, 7, __iter_dup_++)
			push_float(&SplitJoin144_FirFilter_Fiss_1458_1492_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1296() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1296DownSamp_1095, pop_float(&SplitJoin144_FirFilter_Fiss_1458_1492_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp_1095() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_1296DownSamp_1095), &(DownSamp_1095UpSamp_1096));
	ENDFOR
}

void UpSamp_1096() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		UpSamp(&(DownSamp_1095UpSamp_1096), &(UpSamp_1096WEIGHTED_ROUND_ROBIN_Splitter_1304));
	ENDFOR
}

void Delay_N_1306() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin146_Delay_N_Fiss_1459_1493_split[0]), &(SplitJoin146_Delay_N_Fiss_1459_1493_join[0]));
	ENDFOR
}

void Delay_N_1307() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin146_Delay_N_Fiss_1459_1493_split[1]), &(SplitJoin146_Delay_N_Fiss_1459_1493_join[1]));
	ENDFOR
}

void Delay_N_1308() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin146_Delay_N_Fiss_1459_1493_split[2]), &(SplitJoin146_Delay_N_Fiss_1459_1493_join[2]));
	ENDFOR
}

void Delay_N_1309() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin146_Delay_N_Fiss_1459_1493_split[3]), &(SplitJoin146_Delay_N_Fiss_1459_1493_join[3]));
	ENDFOR
}

void Delay_N_1310() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin146_Delay_N_Fiss_1459_1493_split[4]), &(SplitJoin146_Delay_N_Fiss_1459_1493_join[4]));
	ENDFOR
}

void Delay_N_1311() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin146_Delay_N_Fiss_1459_1493_split[5]), &(SplitJoin146_Delay_N_Fiss_1459_1493_join[5]));
	ENDFOR
}

void Delay_N_1312() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin146_Delay_N_Fiss_1459_1493_split[6]), &(SplitJoin146_Delay_N_Fiss_1459_1493_join[6]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1304() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&SplitJoin146_Delay_N_Fiss_1459_1493_split[__iter_], pop_float(&UpSamp_1096WEIGHTED_ROUND_ROBIN_Splitter_1304));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1305() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1305DUPLICATE_Splitter_1313, pop_float(&SplitJoin146_Delay_N_Fiss_1459_1493_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_1315() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin148_FirFilter_Fiss_1460_1494_split[0]), &(SplitJoin148_FirFilter_Fiss_1460_1494_join[0]));
	ENDFOR
}

void FirFilter_1316() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin148_FirFilter_Fiss_1460_1494_split[1]), &(SplitJoin148_FirFilter_Fiss_1460_1494_join[1]));
	ENDFOR
}

void FirFilter_1317() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin148_FirFilter_Fiss_1460_1494_split[2]), &(SplitJoin148_FirFilter_Fiss_1460_1494_join[2]));
	ENDFOR
}

void FirFilter_1318() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin148_FirFilter_Fiss_1460_1494_split[3]), &(SplitJoin148_FirFilter_Fiss_1460_1494_join[3]));
	ENDFOR
}

void FirFilter_1319() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin148_FirFilter_Fiss_1460_1494_split[4]), &(SplitJoin148_FirFilter_Fiss_1460_1494_join[4]));
	ENDFOR
}

void FirFilter_1320() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin148_FirFilter_Fiss_1460_1494_split[5]), &(SplitJoin148_FirFilter_Fiss_1460_1494_join[5]));
	ENDFOR
}

void FirFilter_1321() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin148_FirFilter_Fiss_1460_1494_split[6]), &(SplitJoin148_FirFilter_Fiss_1460_1494_join[6]));
	ENDFOR
}

void DUPLICATE_Splitter_1313() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 56, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1305DUPLICATE_Splitter_1313);
		FOR(uint32_t, __iter_dup_, 0, <, 7, __iter_dup_++)
			push_float(&SplitJoin148_FirFilter_Fiss_1460_1494_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1314() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_1060_1124_1439_1474_join[4], pop_float(&SplitJoin148_FirFilter_Fiss_1460_1494_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Delay_N_1324() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin175_Delay_N_Fiss_1461_1495_split[0]), &(SplitJoin175_Delay_N_Fiss_1461_1495_join[0]));
	ENDFOR
}

void Delay_N_1325() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin175_Delay_N_Fiss_1461_1495_split[1]), &(SplitJoin175_Delay_N_Fiss_1461_1495_join[1]));
	ENDFOR
}

void Delay_N_1326() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin175_Delay_N_Fiss_1461_1495_split[2]), &(SplitJoin175_Delay_N_Fiss_1461_1495_join[2]));
	ENDFOR
}

void Delay_N_1327() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin175_Delay_N_Fiss_1461_1495_split[3]), &(SplitJoin175_Delay_N_Fiss_1461_1495_join[3]));
	ENDFOR
}

void Delay_N_1328() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin175_Delay_N_Fiss_1461_1495_split[4]), &(SplitJoin175_Delay_N_Fiss_1461_1495_join[4]));
	ENDFOR
}

void Delay_N_1329() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin175_Delay_N_Fiss_1461_1495_split[5]), &(SplitJoin175_Delay_N_Fiss_1461_1495_join[5]));
	ENDFOR
}

void Delay_N_1330() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin175_Delay_N_Fiss_1461_1495_split[6]), &(SplitJoin175_Delay_N_Fiss_1461_1495_join[6]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1322() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&SplitJoin175_Delay_N_Fiss_1461_1495_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_1060_1124_1439_1474_split[5]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1323() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1323DUPLICATE_Splitter_1331, pop_float(&SplitJoin175_Delay_N_Fiss_1461_1495_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_1333() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin177_FirFilter_Fiss_1462_1496_split[0]), &(SplitJoin177_FirFilter_Fiss_1462_1496_join[0]));
	ENDFOR
}

void FirFilter_1334() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin177_FirFilter_Fiss_1462_1496_split[1]), &(SplitJoin177_FirFilter_Fiss_1462_1496_join[1]));
	ENDFOR
}

void FirFilter_1335() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin177_FirFilter_Fiss_1462_1496_split[2]), &(SplitJoin177_FirFilter_Fiss_1462_1496_join[2]));
	ENDFOR
}

void FirFilter_1336() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin177_FirFilter_Fiss_1462_1496_split[3]), &(SplitJoin177_FirFilter_Fiss_1462_1496_join[3]));
	ENDFOR
}

void FirFilter_1337() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin177_FirFilter_Fiss_1462_1496_split[4]), &(SplitJoin177_FirFilter_Fiss_1462_1496_join[4]));
	ENDFOR
}

void FirFilter_1338() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin177_FirFilter_Fiss_1462_1496_split[5]), &(SplitJoin177_FirFilter_Fiss_1462_1496_join[5]));
	ENDFOR
}

void FirFilter_1339() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin177_FirFilter_Fiss_1462_1496_split[6]), &(SplitJoin177_FirFilter_Fiss_1462_1496_join[6]));
	ENDFOR
}

void DUPLICATE_Splitter_1331() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 56, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1323DUPLICATE_Splitter_1331);
		FOR(uint32_t, __iter_dup_, 0, <, 7, __iter_dup_++)
			push_float(&SplitJoin177_FirFilter_Fiss_1462_1496_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1332() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1332DownSamp_1102, pop_float(&SplitJoin177_FirFilter_Fiss_1462_1496_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp_1102() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_1332DownSamp_1102), &(DownSamp_1102UpSamp_1103));
	ENDFOR
}

void UpSamp_1103() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		UpSamp(&(DownSamp_1102UpSamp_1103), &(UpSamp_1103WEIGHTED_ROUND_ROBIN_Splitter_1340));
	ENDFOR
}

void Delay_N_1342() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin179_Delay_N_Fiss_1463_1497_split[0]), &(SplitJoin179_Delay_N_Fiss_1463_1497_join[0]));
	ENDFOR
}

void Delay_N_1343() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin179_Delay_N_Fiss_1463_1497_split[1]), &(SplitJoin179_Delay_N_Fiss_1463_1497_join[1]));
	ENDFOR
}

void Delay_N_1344() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin179_Delay_N_Fiss_1463_1497_split[2]), &(SplitJoin179_Delay_N_Fiss_1463_1497_join[2]));
	ENDFOR
}

void Delay_N_1345() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin179_Delay_N_Fiss_1463_1497_split[3]), &(SplitJoin179_Delay_N_Fiss_1463_1497_join[3]));
	ENDFOR
}

void Delay_N_1346() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin179_Delay_N_Fiss_1463_1497_split[4]), &(SplitJoin179_Delay_N_Fiss_1463_1497_join[4]));
	ENDFOR
}

void Delay_N_1347() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin179_Delay_N_Fiss_1463_1497_split[5]), &(SplitJoin179_Delay_N_Fiss_1463_1497_join[5]));
	ENDFOR
}

void Delay_N_1348() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin179_Delay_N_Fiss_1463_1497_split[6]), &(SplitJoin179_Delay_N_Fiss_1463_1497_join[6]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1340() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&SplitJoin179_Delay_N_Fiss_1463_1497_split[__iter_], pop_float(&UpSamp_1103WEIGHTED_ROUND_ROBIN_Splitter_1340));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1341() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1341DUPLICATE_Splitter_1349, pop_float(&SplitJoin179_Delay_N_Fiss_1463_1497_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_1351() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin181_FirFilter_Fiss_1464_1498_split[0]), &(SplitJoin181_FirFilter_Fiss_1464_1498_join[0]));
	ENDFOR
}

void FirFilter_1352() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin181_FirFilter_Fiss_1464_1498_split[1]), &(SplitJoin181_FirFilter_Fiss_1464_1498_join[1]));
	ENDFOR
}

void FirFilter_1353() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin181_FirFilter_Fiss_1464_1498_split[2]), &(SplitJoin181_FirFilter_Fiss_1464_1498_join[2]));
	ENDFOR
}

void FirFilter_1354() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin181_FirFilter_Fiss_1464_1498_split[3]), &(SplitJoin181_FirFilter_Fiss_1464_1498_join[3]));
	ENDFOR
}

void FirFilter_1355() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin181_FirFilter_Fiss_1464_1498_split[4]), &(SplitJoin181_FirFilter_Fiss_1464_1498_join[4]));
	ENDFOR
}

void FirFilter_1356() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin181_FirFilter_Fiss_1464_1498_split[5]), &(SplitJoin181_FirFilter_Fiss_1464_1498_join[5]));
	ENDFOR
}

void FirFilter_1357() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin181_FirFilter_Fiss_1464_1498_split[6]), &(SplitJoin181_FirFilter_Fiss_1464_1498_join[6]));
	ENDFOR
}

void DUPLICATE_Splitter_1349() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 56, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1341DUPLICATE_Splitter_1349);
		FOR(uint32_t, __iter_dup_, 0, <, 7, __iter_dup_++)
			push_float(&SplitJoin181_FirFilter_Fiss_1464_1498_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1350() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_1060_1124_1439_1474_join[5], pop_float(&SplitJoin181_FirFilter_Fiss_1464_1498_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Delay_N_1360() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin208_Delay_N_Fiss_1465_1499_split[0]), &(SplitJoin208_Delay_N_Fiss_1465_1499_join[0]));
	ENDFOR
}

void Delay_N_1361() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin208_Delay_N_Fiss_1465_1499_split[1]), &(SplitJoin208_Delay_N_Fiss_1465_1499_join[1]));
	ENDFOR
}

void Delay_N_1362() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin208_Delay_N_Fiss_1465_1499_split[2]), &(SplitJoin208_Delay_N_Fiss_1465_1499_join[2]));
	ENDFOR
}

void Delay_N_1363() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin208_Delay_N_Fiss_1465_1499_split[3]), &(SplitJoin208_Delay_N_Fiss_1465_1499_join[3]));
	ENDFOR
}

void Delay_N_1364() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin208_Delay_N_Fiss_1465_1499_split[4]), &(SplitJoin208_Delay_N_Fiss_1465_1499_join[4]));
	ENDFOR
}

void Delay_N_1365() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin208_Delay_N_Fiss_1465_1499_split[5]), &(SplitJoin208_Delay_N_Fiss_1465_1499_join[5]));
	ENDFOR
}

void Delay_N_1366() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin208_Delay_N_Fiss_1465_1499_split[6]), &(SplitJoin208_Delay_N_Fiss_1465_1499_join[6]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1358() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&SplitJoin208_Delay_N_Fiss_1465_1499_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_1060_1124_1439_1474_split[6]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1359() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1359DUPLICATE_Splitter_1367, pop_float(&SplitJoin208_Delay_N_Fiss_1465_1499_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_1369() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin210_FirFilter_Fiss_1466_1500_split[0]), &(SplitJoin210_FirFilter_Fiss_1466_1500_join[0]));
	ENDFOR
}

void FirFilter_1370() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin210_FirFilter_Fiss_1466_1500_split[1]), &(SplitJoin210_FirFilter_Fiss_1466_1500_join[1]));
	ENDFOR
}

void FirFilter_1371() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin210_FirFilter_Fiss_1466_1500_split[2]), &(SplitJoin210_FirFilter_Fiss_1466_1500_join[2]));
	ENDFOR
}

void FirFilter_1372() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin210_FirFilter_Fiss_1466_1500_split[3]), &(SplitJoin210_FirFilter_Fiss_1466_1500_join[3]));
	ENDFOR
}

void FirFilter_1373() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin210_FirFilter_Fiss_1466_1500_split[4]), &(SplitJoin210_FirFilter_Fiss_1466_1500_join[4]));
	ENDFOR
}

void FirFilter_1374() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin210_FirFilter_Fiss_1466_1500_split[5]), &(SplitJoin210_FirFilter_Fiss_1466_1500_join[5]));
	ENDFOR
}

void FirFilter_1375() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin210_FirFilter_Fiss_1466_1500_split[6]), &(SplitJoin210_FirFilter_Fiss_1466_1500_join[6]));
	ENDFOR
}

void DUPLICATE_Splitter_1367() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 56, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1359DUPLICATE_Splitter_1367);
		FOR(uint32_t, __iter_dup_, 0, <, 7, __iter_dup_++)
			push_float(&SplitJoin210_FirFilter_Fiss_1466_1500_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1368() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1368DownSamp_1109, pop_float(&SplitJoin210_FirFilter_Fiss_1466_1500_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp_1109() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_1368DownSamp_1109), &(DownSamp_1109UpSamp_1110));
	ENDFOR
}

void UpSamp_1110() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		UpSamp(&(DownSamp_1109UpSamp_1110), &(UpSamp_1110WEIGHTED_ROUND_ROBIN_Splitter_1376));
	ENDFOR
}

void Delay_N_1378() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin212_Delay_N_Fiss_1467_1501_split[0]), &(SplitJoin212_Delay_N_Fiss_1467_1501_join[0]));
	ENDFOR
}

void Delay_N_1379() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin212_Delay_N_Fiss_1467_1501_split[1]), &(SplitJoin212_Delay_N_Fiss_1467_1501_join[1]));
	ENDFOR
}

void Delay_N_1380() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin212_Delay_N_Fiss_1467_1501_split[2]), &(SplitJoin212_Delay_N_Fiss_1467_1501_join[2]));
	ENDFOR
}

void Delay_N_1381() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin212_Delay_N_Fiss_1467_1501_split[3]), &(SplitJoin212_Delay_N_Fiss_1467_1501_join[3]));
	ENDFOR
}

void Delay_N_1382() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin212_Delay_N_Fiss_1467_1501_split[4]), &(SplitJoin212_Delay_N_Fiss_1467_1501_join[4]));
	ENDFOR
}

void Delay_N_1383() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin212_Delay_N_Fiss_1467_1501_split[5]), &(SplitJoin212_Delay_N_Fiss_1467_1501_join[5]));
	ENDFOR
}

void Delay_N_1384() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin212_Delay_N_Fiss_1467_1501_split[6]), &(SplitJoin212_Delay_N_Fiss_1467_1501_join[6]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1376() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&SplitJoin212_Delay_N_Fiss_1467_1501_split[__iter_], pop_float(&UpSamp_1110WEIGHTED_ROUND_ROBIN_Splitter_1376));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1377() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1377DUPLICATE_Splitter_1385, pop_float(&SplitJoin212_Delay_N_Fiss_1467_1501_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_1387() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin214_FirFilter_Fiss_1468_1502_split[0]), &(SplitJoin214_FirFilter_Fiss_1468_1502_join[0]));
	ENDFOR
}

void FirFilter_1388() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin214_FirFilter_Fiss_1468_1502_split[1]), &(SplitJoin214_FirFilter_Fiss_1468_1502_join[1]));
	ENDFOR
}

void FirFilter_1389() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin214_FirFilter_Fiss_1468_1502_split[2]), &(SplitJoin214_FirFilter_Fiss_1468_1502_join[2]));
	ENDFOR
}

void FirFilter_1390() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin214_FirFilter_Fiss_1468_1502_split[3]), &(SplitJoin214_FirFilter_Fiss_1468_1502_join[3]));
	ENDFOR
}

void FirFilter_1391() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin214_FirFilter_Fiss_1468_1502_split[4]), &(SplitJoin214_FirFilter_Fiss_1468_1502_join[4]));
	ENDFOR
}

void FirFilter_1392() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin214_FirFilter_Fiss_1468_1502_split[5]), &(SplitJoin214_FirFilter_Fiss_1468_1502_join[5]));
	ENDFOR
}

void FirFilter_1393() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin214_FirFilter_Fiss_1468_1502_split[6]), &(SplitJoin214_FirFilter_Fiss_1468_1502_join[6]));
	ENDFOR
}

void DUPLICATE_Splitter_1385() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 56, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1377DUPLICATE_Splitter_1385);
		FOR(uint32_t, __iter_dup_, 0, <, 7, __iter_dup_++)
			push_float(&SplitJoin214_FirFilter_Fiss_1468_1502_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1386() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_1060_1124_1439_1474_join[6], pop_float(&SplitJoin214_FirFilter_Fiss_1468_1502_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Delay_N_1396() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin241_Delay_N_Fiss_1469_1503_split[0]), &(SplitJoin241_Delay_N_Fiss_1469_1503_join[0]));
	ENDFOR
}

void Delay_N_1397() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin241_Delay_N_Fiss_1469_1503_split[1]), &(SplitJoin241_Delay_N_Fiss_1469_1503_join[1]));
	ENDFOR
}

void Delay_N_1398() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin241_Delay_N_Fiss_1469_1503_split[2]), &(SplitJoin241_Delay_N_Fiss_1469_1503_join[2]));
	ENDFOR
}

void Delay_N_1399() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin241_Delay_N_Fiss_1469_1503_split[3]), &(SplitJoin241_Delay_N_Fiss_1469_1503_join[3]));
	ENDFOR
}

void Delay_N_1400() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin241_Delay_N_Fiss_1469_1503_split[4]), &(SplitJoin241_Delay_N_Fiss_1469_1503_join[4]));
	ENDFOR
}

void Delay_N_1401() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin241_Delay_N_Fiss_1469_1503_split[5]), &(SplitJoin241_Delay_N_Fiss_1469_1503_join[5]));
	ENDFOR
}

void Delay_N_1402() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin241_Delay_N_Fiss_1469_1503_split[6]), &(SplitJoin241_Delay_N_Fiss_1469_1503_join[6]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1394() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&SplitJoin241_Delay_N_Fiss_1469_1503_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_1060_1124_1439_1474_split[7]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1395() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1395DUPLICATE_Splitter_1403, pop_float(&SplitJoin241_Delay_N_Fiss_1469_1503_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_1405() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin243_FirFilter_Fiss_1470_1504_split[0]), &(SplitJoin243_FirFilter_Fiss_1470_1504_join[0]));
	ENDFOR
}

void FirFilter_1406() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin243_FirFilter_Fiss_1470_1504_split[1]), &(SplitJoin243_FirFilter_Fiss_1470_1504_join[1]));
	ENDFOR
}

void FirFilter_1407() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin243_FirFilter_Fiss_1470_1504_split[2]), &(SplitJoin243_FirFilter_Fiss_1470_1504_join[2]));
	ENDFOR
}

void FirFilter_1408() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin243_FirFilter_Fiss_1470_1504_split[3]), &(SplitJoin243_FirFilter_Fiss_1470_1504_join[3]));
	ENDFOR
}

void FirFilter_1409() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin243_FirFilter_Fiss_1470_1504_split[4]), &(SplitJoin243_FirFilter_Fiss_1470_1504_join[4]));
	ENDFOR
}

void FirFilter_1410() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin243_FirFilter_Fiss_1470_1504_split[5]), &(SplitJoin243_FirFilter_Fiss_1470_1504_join[5]));
	ENDFOR
}

void FirFilter_1411() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin243_FirFilter_Fiss_1470_1504_split[6]), &(SplitJoin243_FirFilter_Fiss_1470_1504_join[6]));
	ENDFOR
}

void DUPLICATE_Splitter_1403() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 56, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1395DUPLICATE_Splitter_1403);
		FOR(uint32_t, __iter_dup_, 0, <, 7, __iter_dup_++)
			push_float(&SplitJoin243_FirFilter_Fiss_1470_1504_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1404() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1404DownSamp_1116, pop_float(&SplitJoin243_FirFilter_Fiss_1470_1504_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp_1116() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_1404DownSamp_1116), &(DownSamp_1116UpSamp_1117));
	ENDFOR
}

void UpSamp_1117() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		UpSamp(&(DownSamp_1116UpSamp_1117), &(UpSamp_1117WEIGHTED_ROUND_ROBIN_Splitter_1412));
	ENDFOR
}

void Delay_N_1414() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin245_Delay_N_Fiss_1471_1505_split[0]), &(SplitJoin245_Delay_N_Fiss_1471_1505_join[0]));
	ENDFOR
}

void Delay_N_1415() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin245_Delay_N_Fiss_1471_1505_split[1]), &(SplitJoin245_Delay_N_Fiss_1471_1505_join[1]));
	ENDFOR
}

void Delay_N_1416() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin245_Delay_N_Fiss_1471_1505_split[2]), &(SplitJoin245_Delay_N_Fiss_1471_1505_join[2]));
	ENDFOR
}

void Delay_N_1417() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin245_Delay_N_Fiss_1471_1505_split[3]), &(SplitJoin245_Delay_N_Fiss_1471_1505_join[3]));
	ENDFOR
}

void Delay_N_1418() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin245_Delay_N_Fiss_1471_1505_split[4]), &(SplitJoin245_Delay_N_Fiss_1471_1505_join[4]));
	ENDFOR
}

void Delay_N_1419() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin245_Delay_N_Fiss_1471_1505_split[5]), &(SplitJoin245_Delay_N_Fiss_1471_1505_join[5]));
	ENDFOR
}

void Delay_N_1420() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Delay_N(&(SplitJoin245_Delay_N_Fiss_1471_1505_split[6]), &(SplitJoin245_Delay_N_Fiss_1471_1505_join[6]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1412() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&SplitJoin245_Delay_N_Fiss_1471_1505_split[__iter_], pop_float(&UpSamp_1117WEIGHTED_ROUND_ROBIN_Splitter_1412));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1413() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1413DUPLICATE_Splitter_1421, pop_float(&SplitJoin245_Delay_N_Fiss_1471_1505_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_1423() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin247_FirFilter_Fiss_1472_1506_split[0]), &(SplitJoin247_FirFilter_Fiss_1472_1506_join[0]));
	ENDFOR
}

void FirFilter_1424() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin247_FirFilter_Fiss_1472_1506_split[1]), &(SplitJoin247_FirFilter_Fiss_1472_1506_join[1]));
	ENDFOR
}

void FirFilter_1425() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin247_FirFilter_Fiss_1472_1506_split[2]), &(SplitJoin247_FirFilter_Fiss_1472_1506_join[2]));
	ENDFOR
}

void FirFilter_1426() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin247_FirFilter_Fiss_1472_1506_split[3]), &(SplitJoin247_FirFilter_Fiss_1472_1506_join[3]));
	ENDFOR
}

void FirFilter_1427() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin247_FirFilter_Fiss_1472_1506_split[4]), &(SplitJoin247_FirFilter_Fiss_1472_1506_join[4]));
	ENDFOR
}

void FirFilter_1428() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin247_FirFilter_Fiss_1472_1506_split[5]), &(SplitJoin247_FirFilter_Fiss_1472_1506_join[5]));
	ENDFOR
}

void FirFilter_1429() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FirFilter(&(SplitJoin247_FirFilter_Fiss_1472_1506_split[6]), &(SplitJoin247_FirFilter_Fiss_1472_1506_join[6]));
	ENDFOR
}

void DUPLICATE_Splitter_1421() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 56, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1413DUPLICATE_Splitter_1421);
		FOR(uint32_t, __iter_dup_, 0, <, 7, __iter_dup_++)
			push_float(&SplitJoin247_FirFilter_Fiss_1472_1506_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1422() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_1060_1124_1439_1474_join[7], pop_float(&SplitJoin247_FirFilter_Fiss_1472_1506_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DUPLICATE_Splitter_1122() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 56, __iter_steady_++)
		float __token_ = pop_float(&source_1062DUPLICATE_Splitter_1122);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_1060_1124_1439_1474_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1123() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 56, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1123WEIGHTED_ROUND_ROBIN_Splitter_1430, pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_1060_1124_1439_1474_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Combine(buffer_float_t *chanin, buffer_float_t *chanout) {
		float sum = 0.0;
		FOR(int, i, 0,  < , 8, i++) {
			sum = (sum + pop_float(&(*chanin))) ; 
		}
		ENDFOR
		push_float(&(*chanout), sum) ; 
	}


void Combine_1432() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Combine(&(SplitJoin10_Combine_Fiss_1444_1507_split[0]), &(SplitJoin10_Combine_Fiss_1444_1507_join[0]));
	ENDFOR
}

void Combine_1433() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Combine(&(SplitJoin10_Combine_Fiss_1444_1507_split[1]), &(SplitJoin10_Combine_Fiss_1444_1507_join[1]));
	ENDFOR
}

void Combine_1434() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Combine(&(SplitJoin10_Combine_Fiss_1444_1507_split[2]), &(SplitJoin10_Combine_Fiss_1444_1507_join[2]));
	ENDFOR
}

void Combine_1435() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Combine(&(SplitJoin10_Combine_Fiss_1444_1507_split[3]), &(SplitJoin10_Combine_Fiss_1444_1507_join[3]));
	ENDFOR
}

void Combine_1436() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Combine(&(SplitJoin10_Combine_Fiss_1444_1507_split[4]), &(SplitJoin10_Combine_Fiss_1444_1507_join[4]));
	ENDFOR
}

void Combine_1437() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Combine(&(SplitJoin10_Combine_Fiss_1444_1507_split[5]), &(SplitJoin10_Combine_Fiss_1444_1507_join[5]));
	ENDFOR
}

void Combine_1438() {
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		Combine(&(SplitJoin10_Combine_Fiss_1444_1507_split[6]), &(SplitJoin10_Combine_Fiss_1444_1507_join[6]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1430() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 7, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_Combine_Fiss_1444_1507_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1123WEIGHTED_ROUND_ROBIN_Splitter_1430));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1431() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 7, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1431sink_1121, pop_float(&SplitJoin10_Combine_Fiss_1444_1507_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void sink(buffer_float_t *chanin) {
		printf("%.10f", pop_float(&(*chanin)));
		printf("\n");
	}


void sink_1121() {
	FOR(uint32_t, __iter_steady_, 0, <, 56, __iter_steady_++)
		sink(&(WEIGHTED_ROUND_ROBIN_Joiner_1431sink_1121));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 7, __iter_init_0_++)
		init_buffer_float(&SplitJoin109_Delay_N_Fiss_1453_1487_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 7, __iter_init_1_++)
		init_buffer_float(&SplitJoin115_FirFilter_Fiss_1456_1490_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 7, __iter_init_2_++)
		init_buffer_float(&SplitJoin245_Delay_N_Fiss_1471_1505_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 7, __iter_init_3_++)
		init_buffer_float(&SplitJoin78_FirFilter_Fiss_1450_1484_join[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&DownSamp_1081UpSamp_1082);
	FOR(int, __iter_init_4_, 0, <, 7, __iter_init_4_++)
		init_buffer_float(&SplitJoin179_Delay_N_Fiss_1463_1497_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 7, __iter_init_5_++)
		init_buffer_float(&SplitJoin210_FirFilter_Fiss_1466_1500_join[__iter_init_5_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1377DUPLICATE_Splitter_1385);
	FOR(int, __iter_init_6_, 0, <, 7, __iter_init_6_++)
		init_buffer_float(&SplitJoin80_Delay_N_Fiss_1451_1485_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_1060_1124_1439_1474_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 7, __iter_init_8_++)
		init_buffer_float(&SplitJoin80_Delay_N_Fiss_1451_1485_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 7, __iter_init_9_++)
		init_buffer_float(&SplitJoin241_Delay_N_Fiss_1469_1503_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 7, __iter_init_10_++)
		init_buffer_float(&SplitJoin247_FirFilter_Fiss_1472_1506_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 7, __iter_init_11_++)
		init_buffer_float(&SplitJoin2_Delay_N_Fiss_1440_1475_split[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1413DUPLICATE_Splitter_1421);
	init_buffer_float(&DownSamp_1109UpSamp_1110);
	FOR(int, __iter_init_12_, 0, <, 7, __iter_init_12_++)
		init_buffer_float(&SplitJoin148_FirFilter_Fiss_1460_1494_split[__iter_init_12_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1341DUPLICATE_Splitter_1349);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1431sink_1121);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1287DUPLICATE_Splitter_1295);
	FOR(int, __iter_init_13_, 0, <, 7, __iter_init_13_++)
		init_buffer_float(&SplitJoin212_Delay_N_Fiss_1467_1501_join[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 7, __iter_init_14_++)
		init_buffer_float(&SplitJoin115_FirFilter_Fiss_1456_1490_join[__iter_init_14_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1359DUPLICATE_Splitter_1367);
	init_buffer_float(&UpSamp_1103WEIGHTED_ROUND_ROBIN_Splitter_1340);
	init_buffer_float(&UpSamp_1075WEIGHTED_ROUND_ROBIN_Splitter_1196);
	FOR(int, __iter_init_15_, 0, <, 7, __iter_init_15_++)
		init_buffer_float(&SplitJoin4_FirFilter_Fiss_1441_1476_join[__iter_init_15_]);
	ENDFOR
	init_buffer_float(&DownSamp_1095UpSamp_1096);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1152DownSamp_1067);
	FOR(int, __iter_init_16_, 0, <, 7, __iter_init_16_++)
		init_buffer_float(&SplitJoin6_Delay_N_Fiss_1442_1477_split[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 7, __iter_init_17_++)
		init_buffer_float(&SplitJoin214_FirFilter_Fiss_1468_1502_join[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 7, __iter_init_18_++)
		init_buffer_float(&SplitJoin78_FirFilter_Fiss_1450_1484_split[__iter_init_18_]);
	ENDFOR
	FOR(int, __iter_init_19_, 0, <, 7, __iter_init_19_++)
		init_buffer_float(&SplitJoin111_FirFilter_Fiss_1454_1488_join[__iter_init_19_]);
	ENDFOR
	FOR(int, __iter_init_20_, 0, <, 7, __iter_init_20_++)
		init_buffer_float(&SplitJoin208_Delay_N_Fiss_1465_1499_join[__iter_init_20_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1332DownSamp_1102);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1269DUPLICATE_Splitter_1277);
	FOR(int, __iter_init_21_, 0, <, 7, __iter_init_21_++)
		init_buffer_float(&SplitJoin243_FirFilter_Fiss_1470_1504_split[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 7, __iter_init_22_++)
		init_buffer_float(&SplitJoin212_Delay_N_Fiss_1467_1501_split[__iter_init_22_]);
	ENDFOR
	FOR(int, __iter_init_23_, 0, <, 7, __iter_init_23_++)
		init_buffer_float(&SplitJoin247_FirFilter_Fiss_1472_1506_join[__iter_init_23_]);
	ENDFOR
	FOR(int, __iter_init_24_, 0, <, 7, __iter_init_24_++)
		init_buffer_float(&SplitJoin111_FirFilter_Fiss_1454_1488_split[__iter_init_24_]);
	ENDFOR
	init_buffer_float(&DownSamp_1102UpSamp_1103);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1305DUPLICATE_Splitter_1313);
	FOR(int, __iter_init_25_, 0, <, 7, __iter_init_25_++)
		init_buffer_float(&SplitJoin214_FirFilter_Fiss_1468_1502_split[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 7, __iter_init_26_++)
		init_buffer_float(&SplitJoin142_Delay_N_Fiss_1457_1491_split[__iter_init_26_]);
	ENDFOR
	FOR(int, __iter_init_27_, 0, <, 7, __iter_init_27_++)
		init_buffer_float(&SplitJoin43_Delay_N_Fiss_1445_1479_join[__iter_init_27_]);
	ENDFOR
	FOR(int, __iter_init_28_, 0, <, 7, __iter_init_28_++)
		init_buffer_float(&SplitJoin208_Delay_N_Fiss_1465_1499_split[__iter_init_28_]);
	ENDFOR
	init_buffer_float(&UpSamp_1068WEIGHTED_ROUND_ROBIN_Splitter_1160);
	FOR(int, __iter_init_29_, 0, <, 7, __iter_init_29_++)
		init_buffer_float(&SplitJoin148_FirFilter_Fiss_1460_1494_join[__iter_init_29_]);
	ENDFOR
	FOR(int, __iter_init_30_, 0, <, 7, __iter_init_30_++)
		init_buffer_float(&SplitJoin181_FirFilter_Fiss_1464_1498_join[__iter_init_30_]);
	ENDFOR
	FOR(int, __iter_init_31_, 0, <, 7, __iter_init_31_++)
		init_buffer_float(&SplitJoin82_FirFilter_Fiss_1452_1486_join[__iter_init_31_]);
	ENDFOR
	init_buffer_float(&DownSamp_1074UpSamp_1075);
	FOR(int, __iter_init_32_, 0, <, 7, __iter_init_32_++)
		init_buffer_float(&SplitJoin179_Delay_N_Fiss_1463_1497_split[__iter_init_32_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1260DownSamp_1088);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1161DUPLICATE_Splitter_1169);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1323DUPLICATE_Splitter_1331);
	init_buffer_float(&UpSamp_1110WEIGHTED_ROUND_ROBIN_Splitter_1376);
	FOR(int, __iter_init_33_, 0, <, 7, __iter_init_33_++)
		init_buffer_float(&SplitJoin245_Delay_N_Fiss_1471_1505_join[__iter_init_33_]);
	ENDFOR
	init_buffer_float(&DownSamp_1067UpSamp_1068);
	FOR(int, __iter_init_34_, 0, <, 7, __iter_init_34_++)
		init_buffer_float(&SplitJoin76_Delay_N_Fiss_1449_1483_split[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 7, __iter_init_35_++)
		init_buffer_float(&SplitJoin113_Delay_N_Fiss_1455_1489_split[__iter_init_35_]);
	ENDFOR
	init_buffer_float(&source_1062DUPLICATE_Splitter_1122);
	FOR(int, __iter_init_36_, 0, <, 7, __iter_init_36_++)
		init_buffer_float(&SplitJoin4_FirFilter_Fiss_1441_1476_split[__iter_init_36_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1368DownSamp_1109);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1123WEIGHTED_ROUND_ROBIN_Splitter_1430);
	init_buffer_float(&UpSamp_1082WEIGHTED_ROUND_ROBIN_Splitter_1232);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1404DownSamp_1116);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1143DUPLICATE_Splitter_1151);
	FOR(int, __iter_init_37_, 0, <, 7, __iter_init_37_++)
		init_buffer_float(&SplitJoin47_Delay_N_Fiss_1447_1481_join[__iter_init_37_]);
	ENDFOR
	FOR(int, __iter_init_38_, 0, <, 7, __iter_init_38_++)
		init_buffer_float(&SplitJoin177_FirFilter_Fiss_1462_1496_join[__iter_init_38_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1197DUPLICATE_Splitter_1205);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1215DUPLICATE_Splitter_1223);
	FOR(int, __iter_init_39_, 0, <, 7, __iter_init_39_++)
		init_buffer_float(&SplitJoin10_Combine_Fiss_1444_1507_join[__iter_init_39_]);
	ENDFOR
	FOR(int, __iter_init_40_, 0, <, 7, __iter_init_40_++)
		init_buffer_float(&SplitJoin144_FirFilter_Fiss_1458_1492_join[__iter_init_40_]);
	ENDFOR
	FOR(int, __iter_init_41_, 0, <, 7, __iter_init_41_++)
		init_buffer_float(&SplitJoin210_FirFilter_Fiss_1466_1500_split[__iter_init_41_]);
	ENDFOR
	FOR(int, __iter_init_42_, 0, <, 7, __iter_init_42_++)
		init_buffer_float(&SplitJoin6_Delay_N_Fiss_1442_1477_join[__iter_init_42_]);
	ENDFOR
	FOR(int, __iter_init_43_, 0, <, 7, __iter_init_43_++)
		init_buffer_float(&SplitJoin76_Delay_N_Fiss_1449_1483_join[__iter_init_43_]);
	ENDFOR
	FOR(int, __iter_init_44_, 0, <, 7, __iter_init_44_++)
		init_buffer_float(&SplitJoin146_Delay_N_Fiss_1459_1493_join[__iter_init_44_]);
	ENDFOR
	init_buffer_float(&UpSamp_1117WEIGHTED_ROUND_ROBIN_Splitter_1412);
	FOR(int, __iter_init_45_, 0, <, 7, __iter_init_45_++)
		init_buffer_float(&SplitJoin241_Delay_N_Fiss_1469_1503_join[__iter_init_45_]);
	ENDFOR
	init_buffer_float(&UpSamp_1089WEIGHTED_ROUND_ROBIN_Splitter_1268);
	FOR(int, __iter_init_46_, 0, <, 7, __iter_init_46_++)
		init_buffer_float(&SplitJoin45_FirFilter_Fiss_1446_1480_split[__iter_init_46_]);
	ENDFOR
	FOR(int, __iter_init_47_, 0, <, 7, __iter_init_47_++)
		init_buffer_float(&SplitJoin45_FirFilter_Fiss_1446_1480_join[__iter_init_47_]);
	ENDFOR
	FOR(int, __iter_init_48_, 0, <, 7, __iter_init_48_++)
		init_buffer_float(&SplitJoin8_FirFilter_Fiss_1443_1478_split[__iter_init_48_]);
	ENDFOR
	FOR(int, __iter_init_49_, 0, <, 7, __iter_init_49_++)
		init_buffer_float(&SplitJoin109_Delay_N_Fiss_1453_1487_join[__iter_init_49_]);
	ENDFOR
	FOR(int, __iter_init_50_, 0, <, 7, __iter_init_50_++)
		init_buffer_float(&SplitJoin2_Delay_N_Fiss_1440_1475_join[__iter_init_50_]);
	ENDFOR
	FOR(int, __iter_init_51_, 0, <, 7, __iter_init_51_++)
		init_buffer_float(&SplitJoin177_FirFilter_Fiss_1462_1496_split[__iter_init_51_]);
	ENDFOR
	init_buffer_float(&DownSamp_1088UpSamp_1089);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1188DownSamp_1074);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1224DownSamp_1081);
	FOR(int, __iter_init_52_, 0, <, 7, __iter_init_52_++)
		init_buffer_float(&SplitJoin175_Delay_N_Fiss_1461_1495_join[__iter_init_52_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1233DUPLICATE_Splitter_1241);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1296DownSamp_1095);
	FOR(int, __iter_init_53_, 0, <, 7, __iter_init_53_++)
		init_buffer_float(&SplitJoin82_FirFilter_Fiss_1452_1486_split[__iter_init_53_]);
	ENDFOR
	FOR(int, __iter_init_54_, 0, <, 7, __iter_init_54_++)
		init_buffer_float(&SplitJoin243_FirFilter_Fiss_1470_1504_join[__iter_init_54_]);
	ENDFOR
	FOR(int, __iter_init_55_, 0, <, 7, __iter_init_55_++)
		init_buffer_float(&SplitJoin113_Delay_N_Fiss_1455_1489_join[__iter_init_55_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1179DUPLICATE_Splitter_1187);
	FOR(int, __iter_init_56_, 0, <, 7, __iter_init_56_++)
		init_buffer_float(&SplitJoin49_FirFilter_Fiss_1448_1482_join[__iter_init_56_]);
	ENDFOR
	FOR(int, __iter_init_57_, 0, <, 7, __iter_init_57_++)
		init_buffer_float(&SplitJoin47_Delay_N_Fiss_1447_1481_split[__iter_init_57_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1251DUPLICATE_Splitter_1259);
	FOR(int, __iter_init_58_, 0, <, 7, __iter_init_58_++)
		init_buffer_float(&SplitJoin144_FirFilter_Fiss_1458_1492_split[__iter_init_58_]);
	ENDFOR
	FOR(int, __iter_init_59_, 0, <, 7, __iter_init_59_++)
		init_buffer_float(&SplitJoin142_Delay_N_Fiss_1457_1491_join[__iter_init_59_]);
	ENDFOR
	FOR(int, __iter_init_60_, 0, <, 7, __iter_init_60_++)
		init_buffer_float(&SplitJoin181_FirFilter_Fiss_1464_1498_split[__iter_init_60_]);
	ENDFOR
	FOR(int, __iter_init_61_, 0, <, 7, __iter_init_61_++)
		init_buffer_float(&SplitJoin8_FirFilter_Fiss_1443_1478_join[__iter_init_61_]);
	ENDFOR
	FOR(int, __iter_init_62_, 0, <, 7, __iter_init_62_++)
		init_buffer_float(&SplitJoin49_FirFilter_Fiss_1448_1482_split[__iter_init_62_]);
	ENDFOR
	init_buffer_float(&UpSamp_1096WEIGHTED_ROUND_ROBIN_Splitter_1304);
	FOR(int, __iter_init_63_, 0, <, 7, __iter_init_63_++)
		init_buffer_float(&SplitJoin10_Combine_Fiss_1444_1507_split[__iter_init_63_]);
	ENDFOR
	FOR(int, __iter_init_64_, 0, <, 8, __iter_init_64_++)
		init_buffer_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_1060_1124_1439_1474_split[__iter_init_64_]);
	ENDFOR
	FOR(int, __iter_init_65_, 0, <, 7, __iter_init_65_++)
		init_buffer_float(&SplitJoin175_Delay_N_Fiss_1461_1495_split[__iter_init_65_]);
	ENDFOR
	FOR(int, __iter_init_66_, 0, <, 7, __iter_init_66_++)
		init_buffer_float(&SplitJoin43_Delay_N_Fiss_1445_1479_split[__iter_init_66_]);
	ENDFOR
	FOR(int, __iter_init_67_, 0, <, 7, __iter_init_67_++)
		init_buffer_float(&SplitJoin146_Delay_N_Fiss_1459_1493_split[__iter_init_67_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1395DUPLICATE_Splitter_1403);
	init_buffer_float(&DownSamp_1116UpSamp_1117);
// --- init: source_1062
	 {
	source_1062_s.current = 0.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 483, __iter_init_++)
		source(&(source_1062DUPLICATE_Splitter_1122));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_1122
	FOR(uint32_t, __iter_init_, 0, <, 483, __iter_init_++)
		DUPLICATE_Splitter(&(source_1062DUPLICATE_Splitter_1122), &(SplitJoin0_SplitJoin0_SplitJoin0_Branches_1060_1124_1439_1474_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_1142
	FOR(uint32_t, __iter_init_, 0, <, 69, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(SplitJoin0_SplitJoin0_SplitJoin0_Branches_1060_1124_1439_1474_split[0]), &(SplitJoin2_Delay_N_Fiss_1440_1475_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1144
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin2_Delay_N_Fiss_1440_1475_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1145
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin2_Delay_N_Fiss_1440_1475_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1146
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin2_Delay_N_Fiss_1440_1475_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1147
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin2_Delay_N_Fiss_1440_1475_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1148
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin2_Delay_N_Fiss_1440_1475_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1149
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin2_Delay_N_Fiss_1440_1475_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1150
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin2_Delay_N_Fiss_1440_1475_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1143
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin2_Delay_N_Fiss_1440_1475_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_1143DUPLICATE_Splitter_1151));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_1151
	FOR(uint32_t, __iter_init_, 0, <, 276, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_1143DUPLICATE_Splitter_1151), &(SplitJoin4_FirFilter_Fiss_1441_1476_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1153
	 {
	FirFilter_1153_s.COEFF[0] = 1.0 ; 
	FirFilter_1153_s.COEFF[1] = 34.0 ; 
	FirFilter_1153_s.COEFF[2] = 67.0 ; 
	FirFilter_1153_s.COEFF[3] = 100.0 ; 
	FirFilter_1153_s.COEFF[4] = 133.0 ; 
	FirFilter_1153_s.COEFF[5] = 166.0 ; 
	FirFilter_1153_s.COEFF[6] = 199.0 ; 
	FirFilter_1153_s.COEFF[7] = 232.0 ; 
	FirFilter_1153_s.COEFF[8] = 265.0 ; 
	FirFilter_1153_s.COEFF[9] = 298.0 ; 
	FirFilter_1153_s.COEFF[10] = 331.0 ; 
	FirFilter_1153_s.COEFF[11] = 364.0 ; 
	FirFilter_1153_s.COEFF[12] = 397.0 ; 
	FirFilter_1153_s.COEFF[13] = 430.0 ; 
	FirFilter_1153_s.COEFF[14] = 463.0 ; 
	FirFilter_1153_s.COEFF[15] = 496.0 ; 
	FirFilter_1153_s.COEFF[16] = 529.0 ; 
	FirFilter_1153_s.COEFF[17] = 562.0 ; 
	FirFilter_1153_s.COEFF[18] = 595.0 ; 
	FirFilter_1153_s.COEFF[19] = 628.0 ; 
	FirFilter_1153_s.COEFF[20] = 661.0 ; 
	FirFilter_1153_s.COEFF[21] = 694.0 ; 
	FirFilter_1153_s.COEFF[22] = 727.0 ; 
	FirFilter_1153_s.COEFF[23] = 760.0 ; 
	FirFilter_1153_s.COEFF[24] = 793.0 ; 
	FirFilter_1153_s.COEFF[25] = 826.0 ; 
	FirFilter_1153_s.COEFF[26] = 859.0 ; 
	FirFilter_1153_s.COEFF[27] = 892.0 ; 
	FirFilter_1153_s.COEFF[28] = 925.0 ; 
	FirFilter_1153_s.COEFF[29] = 958.0 ; 
	FirFilter_1153_s.COEFF[30] = 991.0 ; 
	FirFilter_1153_s.COEFF[31] = 1024.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin4_FirFilter_Fiss_1441_1476_split[0]), &(SplitJoin4_FirFilter_Fiss_1441_1476_join[0]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1154
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin4_FirFilter_Fiss_1441_1476_split[1]), &(SplitJoin4_FirFilter_Fiss_1441_1476_join[1]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1155
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin4_FirFilter_Fiss_1441_1476_split[2]), &(SplitJoin4_FirFilter_Fiss_1441_1476_join[2]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1156
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin4_FirFilter_Fiss_1441_1476_split[3]), &(SplitJoin4_FirFilter_Fiss_1441_1476_join[3]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1157
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin4_FirFilter_Fiss_1441_1476_split[4]), &(SplitJoin4_FirFilter_Fiss_1441_1476_join[4]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1158
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin4_FirFilter_Fiss_1441_1476_split[5]), &(SplitJoin4_FirFilter_Fiss_1441_1476_join[5]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1159
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin4_FirFilter_Fiss_1441_1476_split[6]), &(SplitJoin4_FirFilter_Fiss_1441_1476_join[6]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1152
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin4_FirFilter_Fiss_1441_1476_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_1152DownSamp_1067));
	ENDFOR
//--------------------------------
// --- init: DownSamp_1067
	FOR(uint32_t, __iter_init_, 0, <, 30, __iter_init_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_1152DownSamp_1067), &(DownSamp_1067UpSamp_1068));
	ENDFOR
//--------------------------------
// --- init: UpSamp_1068
	FOR(uint32_t, __iter_init_, 0, <, 30, __iter_init_++)
		UpSamp(&(DownSamp_1067UpSamp_1068), &(UpSamp_1068WEIGHTED_ROUND_ROBIN_Splitter_1160));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_1160
	FOR(uint32_t, __iter_init_, 0, <, 34, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(UpSamp_1068WEIGHTED_ROUND_ROBIN_Splitter_1160), &(SplitJoin6_Delay_N_Fiss_1442_1477_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1162
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin6_Delay_N_Fiss_1442_1477_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1163
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin6_Delay_N_Fiss_1442_1477_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1164
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin6_Delay_N_Fiss_1442_1477_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1165
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin6_Delay_N_Fiss_1442_1477_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1166
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin6_Delay_N_Fiss_1442_1477_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1167
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin6_Delay_N_Fiss_1442_1477_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1168
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin6_Delay_N_Fiss_1442_1477_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1161
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin6_Delay_N_Fiss_1442_1477_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_1161DUPLICATE_Splitter_1169));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_1169
	FOR(uint32_t, __iter_init_, 0, <, 31, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_1161DUPLICATE_Splitter_1169), &(SplitJoin8_FirFilter_Fiss_1443_1478_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_1178
	FOR(uint32_t, __iter_init_, 0, <, 69, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(SplitJoin0_SplitJoin0_SplitJoin0_Branches_1060_1124_1439_1474_split[1]), &(SplitJoin43_Delay_N_Fiss_1445_1479_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1180
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin43_Delay_N_Fiss_1445_1479_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1181
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin43_Delay_N_Fiss_1445_1479_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1182
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin43_Delay_N_Fiss_1445_1479_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1183
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin43_Delay_N_Fiss_1445_1479_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1184
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin43_Delay_N_Fiss_1445_1479_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1185
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin43_Delay_N_Fiss_1445_1479_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1186
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin43_Delay_N_Fiss_1445_1479_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1179
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin43_Delay_N_Fiss_1445_1479_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_1179DUPLICATE_Splitter_1187));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_1187
	FOR(uint32_t, __iter_init_, 0, <, 276, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_1179DUPLICATE_Splitter_1187), &(SplitJoin45_FirFilter_Fiss_1446_1480_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1189
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin45_FirFilter_Fiss_1446_1480_split[0]), &(SplitJoin45_FirFilter_Fiss_1446_1480_join[0]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1190
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin45_FirFilter_Fiss_1446_1480_split[1]), &(SplitJoin45_FirFilter_Fiss_1446_1480_join[1]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1191
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin45_FirFilter_Fiss_1446_1480_split[2]), &(SplitJoin45_FirFilter_Fiss_1446_1480_join[2]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1192
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin45_FirFilter_Fiss_1446_1480_split[3]), &(SplitJoin45_FirFilter_Fiss_1446_1480_join[3]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1193
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin45_FirFilter_Fiss_1446_1480_split[4]), &(SplitJoin45_FirFilter_Fiss_1446_1480_join[4]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1194
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin45_FirFilter_Fiss_1446_1480_split[5]), &(SplitJoin45_FirFilter_Fiss_1446_1480_join[5]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1195
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin45_FirFilter_Fiss_1446_1480_split[6]), &(SplitJoin45_FirFilter_Fiss_1446_1480_join[6]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1188
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin45_FirFilter_Fiss_1446_1480_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_1188DownSamp_1074));
	ENDFOR
//--------------------------------
// --- init: DownSamp_1074
	FOR(uint32_t, __iter_init_, 0, <, 30, __iter_init_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_1188DownSamp_1074), &(DownSamp_1074UpSamp_1075));
	ENDFOR
//--------------------------------
// --- init: UpSamp_1075
	FOR(uint32_t, __iter_init_, 0, <, 30, __iter_init_++)
		UpSamp(&(DownSamp_1074UpSamp_1075), &(UpSamp_1075WEIGHTED_ROUND_ROBIN_Splitter_1196));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_1196
	FOR(uint32_t, __iter_init_, 0, <, 34, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(UpSamp_1075WEIGHTED_ROUND_ROBIN_Splitter_1196), &(SplitJoin47_Delay_N_Fiss_1447_1481_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1198
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin47_Delay_N_Fiss_1447_1481_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1199
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin47_Delay_N_Fiss_1447_1481_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1200
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin47_Delay_N_Fiss_1447_1481_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1201
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin47_Delay_N_Fiss_1447_1481_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1202
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin47_Delay_N_Fiss_1447_1481_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1203
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin47_Delay_N_Fiss_1447_1481_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1204
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin47_Delay_N_Fiss_1447_1481_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1197
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin47_Delay_N_Fiss_1447_1481_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_1197DUPLICATE_Splitter_1205));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_1205
	FOR(uint32_t, __iter_init_, 0, <, 31, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_1197DUPLICATE_Splitter_1205), &(SplitJoin49_FirFilter_Fiss_1448_1482_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_1214
	FOR(uint32_t, __iter_init_, 0, <, 69, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(SplitJoin0_SplitJoin0_SplitJoin0_Branches_1060_1124_1439_1474_split[2]), &(SplitJoin76_Delay_N_Fiss_1449_1483_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1216
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin76_Delay_N_Fiss_1449_1483_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1217
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin76_Delay_N_Fiss_1449_1483_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1218
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin76_Delay_N_Fiss_1449_1483_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1219
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin76_Delay_N_Fiss_1449_1483_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1220
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin76_Delay_N_Fiss_1449_1483_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1221
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin76_Delay_N_Fiss_1449_1483_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1222
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin76_Delay_N_Fiss_1449_1483_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1215
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin76_Delay_N_Fiss_1449_1483_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_1215DUPLICATE_Splitter_1223));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_1223
	FOR(uint32_t, __iter_init_, 0, <, 276, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_1215DUPLICATE_Splitter_1223), &(SplitJoin78_FirFilter_Fiss_1450_1484_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1225
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin78_FirFilter_Fiss_1450_1484_split[0]), &(SplitJoin78_FirFilter_Fiss_1450_1484_join[0]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1226
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin78_FirFilter_Fiss_1450_1484_split[1]), &(SplitJoin78_FirFilter_Fiss_1450_1484_join[1]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1227
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin78_FirFilter_Fiss_1450_1484_split[2]), &(SplitJoin78_FirFilter_Fiss_1450_1484_join[2]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1228
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin78_FirFilter_Fiss_1450_1484_split[3]), &(SplitJoin78_FirFilter_Fiss_1450_1484_join[3]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1229
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin78_FirFilter_Fiss_1450_1484_split[4]), &(SplitJoin78_FirFilter_Fiss_1450_1484_join[4]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1230
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin78_FirFilter_Fiss_1450_1484_split[5]), &(SplitJoin78_FirFilter_Fiss_1450_1484_join[5]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1231
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin78_FirFilter_Fiss_1450_1484_split[6]), &(SplitJoin78_FirFilter_Fiss_1450_1484_join[6]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1224
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin78_FirFilter_Fiss_1450_1484_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_1224DownSamp_1081));
	ENDFOR
//--------------------------------
// --- init: DownSamp_1081
	FOR(uint32_t, __iter_init_, 0, <, 30, __iter_init_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_1224DownSamp_1081), &(DownSamp_1081UpSamp_1082));
	ENDFOR
//--------------------------------
// --- init: UpSamp_1082
	FOR(uint32_t, __iter_init_, 0, <, 30, __iter_init_++)
		UpSamp(&(DownSamp_1081UpSamp_1082), &(UpSamp_1082WEIGHTED_ROUND_ROBIN_Splitter_1232));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_1232
	FOR(uint32_t, __iter_init_, 0, <, 34, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(UpSamp_1082WEIGHTED_ROUND_ROBIN_Splitter_1232), &(SplitJoin80_Delay_N_Fiss_1451_1485_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1234
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin80_Delay_N_Fiss_1451_1485_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1235
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin80_Delay_N_Fiss_1451_1485_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1236
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin80_Delay_N_Fiss_1451_1485_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1237
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin80_Delay_N_Fiss_1451_1485_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1238
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin80_Delay_N_Fiss_1451_1485_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1239
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin80_Delay_N_Fiss_1451_1485_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1240
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin80_Delay_N_Fiss_1451_1485_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1233
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin80_Delay_N_Fiss_1451_1485_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_1233DUPLICATE_Splitter_1241));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_1241
	FOR(uint32_t, __iter_init_, 0, <, 31, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_1233DUPLICATE_Splitter_1241), &(SplitJoin82_FirFilter_Fiss_1452_1486_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_1250
	FOR(uint32_t, __iter_init_, 0, <, 69, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(SplitJoin0_SplitJoin0_SplitJoin0_Branches_1060_1124_1439_1474_split[3]), &(SplitJoin109_Delay_N_Fiss_1453_1487_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1252
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin109_Delay_N_Fiss_1453_1487_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1253
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin109_Delay_N_Fiss_1453_1487_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1254
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin109_Delay_N_Fiss_1453_1487_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1255
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin109_Delay_N_Fiss_1453_1487_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1256
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin109_Delay_N_Fiss_1453_1487_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1257
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin109_Delay_N_Fiss_1453_1487_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1258
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin109_Delay_N_Fiss_1453_1487_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1251
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin109_Delay_N_Fiss_1453_1487_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_1251DUPLICATE_Splitter_1259));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_1259
	FOR(uint32_t, __iter_init_, 0, <, 276, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_1251DUPLICATE_Splitter_1259), &(SplitJoin111_FirFilter_Fiss_1454_1488_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1261
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin111_FirFilter_Fiss_1454_1488_split[0]), &(SplitJoin111_FirFilter_Fiss_1454_1488_join[0]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1262
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin111_FirFilter_Fiss_1454_1488_split[1]), &(SplitJoin111_FirFilter_Fiss_1454_1488_join[1]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1263
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin111_FirFilter_Fiss_1454_1488_split[2]), &(SplitJoin111_FirFilter_Fiss_1454_1488_join[2]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1264
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin111_FirFilter_Fiss_1454_1488_split[3]), &(SplitJoin111_FirFilter_Fiss_1454_1488_join[3]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1265
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin111_FirFilter_Fiss_1454_1488_split[4]), &(SplitJoin111_FirFilter_Fiss_1454_1488_join[4]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1266
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin111_FirFilter_Fiss_1454_1488_split[5]), &(SplitJoin111_FirFilter_Fiss_1454_1488_join[5]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1267
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin111_FirFilter_Fiss_1454_1488_split[6]), &(SplitJoin111_FirFilter_Fiss_1454_1488_join[6]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1260
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin111_FirFilter_Fiss_1454_1488_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_1260DownSamp_1088));
	ENDFOR
//--------------------------------
// --- init: DownSamp_1088
	FOR(uint32_t, __iter_init_, 0, <, 30, __iter_init_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_1260DownSamp_1088), &(DownSamp_1088UpSamp_1089));
	ENDFOR
//--------------------------------
// --- init: UpSamp_1089
	FOR(uint32_t, __iter_init_, 0, <, 30, __iter_init_++)
		UpSamp(&(DownSamp_1088UpSamp_1089), &(UpSamp_1089WEIGHTED_ROUND_ROBIN_Splitter_1268));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_1268
	FOR(uint32_t, __iter_init_, 0, <, 34, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(UpSamp_1089WEIGHTED_ROUND_ROBIN_Splitter_1268), &(SplitJoin113_Delay_N_Fiss_1455_1489_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1270
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin113_Delay_N_Fiss_1455_1489_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1271
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin113_Delay_N_Fiss_1455_1489_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1272
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin113_Delay_N_Fiss_1455_1489_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1273
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin113_Delay_N_Fiss_1455_1489_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1274
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin113_Delay_N_Fiss_1455_1489_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1275
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin113_Delay_N_Fiss_1455_1489_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1276
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin113_Delay_N_Fiss_1455_1489_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1269
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin113_Delay_N_Fiss_1455_1489_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_1269DUPLICATE_Splitter_1277));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_1277
	FOR(uint32_t, __iter_init_, 0, <, 31, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_1269DUPLICATE_Splitter_1277), &(SplitJoin115_FirFilter_Fiss_1456_1490_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_1286
	FOR(uint32_t, __iter_init_, 0, <, 69, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(SplitJoin0_SplitJoin0_SplitJoin0_Branches_1060_1124_1439_1474_split[4]), &(SplitJoin142_Delay_N_Fiss_1457_1491_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1288
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin142_Delay_N_Fiss_1457_1491_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1289
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin142_Delay_N_Fiss_1457_1491_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1290
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin142_Delay_N_Fiss_1457_1491_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1291
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin142_Delay_N_Fiss_1457_1491_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1292
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin142_Delay_N_Fiss_1457_1491_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1293
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin142_Delay_N_Fiss_1457_1491_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1294
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin142_Delay_N_Fiss_1457_1491_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1287
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin142_Delay_N_Fiss_1457_1491_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_1287DUPLICATE_Splitter_1295));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_1295
	FOR(uint32_t, __iter_init_, 0, <, 276, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_1287DUPLICATE_Splitter_1295), &(SplitJoin144_FirFilter_Fiss_1458_1492_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1297
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin144_FirFilter_Fiss_1458_1492_split[0]), &(SplitJoin144_FirFilter_Fiss_1458_1492_join[0]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1298
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin144_FirFilter_Fiss_1458_1492_split[1]), &(SplitJoin144_FirFilter_Fiss_1458_1492_join[1]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1299
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin144_FirFilter_Fiss_1458_1492_split[2]), &(SplitJoin144_FirFilter_Fiss_1458_1492_join[2]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1300
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin144_FirFilter_Fiss_1458_1492_split[3]), &(SplitJoin144_FirFilter_Fiss_1458_1492_join[3]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1301
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin144_FirFilter_Fiss_1458_1492_split[4]), &(SplitJoin144_FirFilter_Fiss_1458_1492_join[4]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1302
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin144_FirFilter_Fiss_1458_1492_split[5]), &(SplitJoin144_FirFilter_Fiss_1458_1492_join[5]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1303
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin144_FirFilter_Fiss_1458_1492_split[6]), &(SplitJoin144_FirFilter_Fiss_1458_1492_join[6]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1296
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin144_FirFilter_Fiss_1458_1492_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_1296DownSamp_1095));
	ENDFOR
//--------------------------------
// --- init: DownSamp_1095
	FOR(uint32_t, __iter_init_, 0, <, 30, __iter_init_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_1296DownSamp_1095), &(DownSamp_1095UpSamp_1096));
	ENDFOR
//--------------------------------
// --- init: UpSamp_1096
	FOR(uint32_t, __iter_init_, 0, <, 30, __iter_init_++)
		UpSamp(&(DownSamp_1095UpSamp_1096), &(UpSamp_1096WEIGHTED_ROUND_ROBIN_Splitter_1304));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_1304
	FOR(uint32_t, __iter_init_, 0, <, 34, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(UpSamp_1096WEIGHTED_ROUND_ROBIN_Splitter_1304), &(SplitJoin146_Delay_N_Fiss_1459_1493_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1306
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin146_Delay_N_Fiss_1459_1493_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1307
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin146_Delay_N_Fiss_1459_1493_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1308
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin146_Delay_N_Fiss_1459_1493_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1309
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin146_Delay_N_Fiss_1459_1493_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1310
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin146_Delay_N_Fiss_1459_1493_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1311
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin146_Delay_N_Fiss_1459_1493_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1312
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin146_Delay_N_Fiss_1459_1493_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1305
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin146_Delay_N_Fiss_1459_1493_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_1305DUPLICATE_Splitter_1313));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_1313
	FOR(uint32_t, __iter_init_, 0, <, 31, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_1305DUPLICATE_Splitter_1313), &(SplitJoin148_FirFilter_Fiss_1460_1494_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_1322
	FOR(uint32_t, __iter_init_, 0, <, 69, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(SplitJoin0_SplitJoin0_SplitJoin0_Branches_1060_1124_1439_1474_split[5]), &(SplitJoin175_Delay_N_Fiss_1461_1495_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1324
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin175_Delay_N_Fiss_1461_1495_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1325
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin175_Delay_N_Fiss_1461_1495_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1326
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin175_Delay_N_Fiss_1461_1495_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1327
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin175_Delay_N_Fiss_1461_1495_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1328
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin175_Delay_N_Fiss_1461_1495_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1329
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin175_Delay_N_Fiss_1461_1495_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1330
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin175_Delay_N_Fiss_1461_1495_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1323
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin175_Delay_N_Fiss_1461_1495_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_1323DUPLICATE_Splitter_1331));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_1331
	FOR(uint32_t, __iter_init_, 0, <, 276, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_1323DUPLICATE_Splitter_1331), &(SplitJoin177_FirFilter_Fiss_1462_1496_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1333
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin177_FirFilter_Fiss_1462_1496_split[0]), &(SplitJoin177_FirFilter_Fiss_1462_1496_join[0]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1334
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin177_FirFilter_Fiss_1462_1496_split[1]), &(SplitJoin177_FirFilter_Fiss_1462_1496_join[1]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1335
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin177_FirFilter_Fiss_1462_1496_split[2]), &(SplitJoin177_FirFilter_Fiss_1462_1496_join[2]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1336
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin177_FirFilter_Fiss_1462_1496_split[3]), &(SplitJoin177_FirFilter_Fiss_1462_1496_join[3]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1337
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin177_FirFilter_Fiss_1462_1496_split[4]), &(SplitJoin177_FirFilter_Fiss_1462_1496_join[4]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1338
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin177_FirFilter_Fiss_1462_1496_split[5]), &(SplitJoin177_FirFilter_Fiss_1462_1496_join[5]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1339
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin177_FirFilter_Fiss_1462_1496_split[6]), &(SplitJoin177_FirFilter_Fiss_1462_1496_join[6]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1332
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin177_FirFilter_Fiss_1462_1496_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_1332DownSamp_1102));
	ENDFOR
//--------------------------------
// --- init: DownSamp_1102
	FOR(uint32_t, __iter_init_, 0, <, 30, __iter_init_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_1332DownSamp_1102), &(DownSamp_1102UpSamp_1103));
	ENDFOR
//--------------------------------
// --- init: UpSamp_1103
	FOR(uint32_t, __iter_init_, 0, <, 30, __iter_init_++)
		UpSamp(&(DownSamp_1102UpSamp_1103), &(UpSamp_1103WEIGHTED_ROUND_ROBIN_Splitter_1340));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_1340
	FOR(uint32_t, __iter_init_, 0, <, 34, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(UpSamp_1103WEIGHTED_ROUND_ROBIN_Splitter_1340), &(SplitJoin179_Delay_N_Fiss_1463_1497_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1342
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin179_Delay_N_Fiss_1463_1497_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1343
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin179_Delay_N_Fiss_1463_1497_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1344
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin179_Delay_N_Fiss_1463_1497_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1345
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin179_Delay_N_Fiss_1463_1497_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1346
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin179_Delay_N_Fiss_1463_1497_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1347
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin179_Delay_N_Fiss_1463_1497_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1348
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin179_Delay_N_Fiss_1463_1497_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1341
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin179_Delay_N_Fiss_1463_1497_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_1341DUPLICATE_Splitter_1349));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_1349
	FOR(uint32_t, __iter_init_, 0, <, 31, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_1341DUPLICATE_Splitter_1349), &(SplitJoin181_FirFilter_Fiss_1464_1498_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_1358
	FOR(uint32_t, __iter_init_, 0, <, 69, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(SplitJoin0_SplitJoin0_SplitJoin0_Branches_1060_1124_1439_1474_split[6]), &(SplitJoin208_Delay_N_Fiss_1465_1499_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1360
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin208_Delay_N_Fiss_1465_1499_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1361
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin208_Delay_N_Fiss_1465_1499_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1362
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin208_Delay_N_Fiss_1465_1499_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1363
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin208_Delay_N_Fiss_1465_1499_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1364
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin208_Delay_N_Fiss_1465_1499_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1365
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin208_Delay_N_Fiss_1465_1499_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1366
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin208_Delay_N_Fiss_1465_1499_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1359
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin208_Delay_N_Fiss_1465_1499_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_1359DUPLICATE_Splitter_1367));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_1367
	FOR(uint32_t, __iter_init_, 0, <, 276, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_1359DUPLICATE_Splitter_1367), &(SplitJoin210_FirFilter_Fiss_1466_1500_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1369
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin210_FirFilter_Fiss_1466_1500_split[0]), &(SplitJoin210_FirFilter_Fiss_1466_1500_join[0]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1370
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin210_FirFilter_Fiss_1466_1500_split[1]), &(SplitJoin210_FirFilter_Fiss_1466_1500_join[1]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1371
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin210_FirFilter_Fiss_1466_1500_split[2]), &(SplitJoin210_FirFilter_Fiss_1466_1500_join[2]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1372
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin210_FirFilter_Fiss_1466_1500_split[3]), &(SplitJoin210_FirFilter_Fiss_1466_1500_join[3]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1373
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin210_FirFilter_Fiss_1466_1500_split[4]), &(SplitJoin210_FirFilter_Fiss_1466_1500_join[4]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1374
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin210_FirFilter_Fiss_1466_1500_split[5]), &(SplitJoin210_FirFilter_Fiss_1466_1500_join[5]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1375
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin210_FirFilter_Fiss_1466_1500_split[6]), &(SplitJoin210_FirFilter_Fiss_1466_1500_join[6]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1368
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin210_FirFilter_Fiss_1466_1500_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_1368DownSamp_1109));
	ENDFOR
//--------------------------------
// --- init: DownSamp_1109
	FOR(uint32_t, __iter_init_, 0, <, 30, __iter_init_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_1368DownSamp_1109), &(DownSamp_1109UpSamp_1110));
	ENDFOR
//--------------------------------
// --- init: UpSamp_1110
	FOR(uint32_t, __iter_init_, 0, <, 30, __iter_init_++)
		UpSamp(&(DownSamp_1109UpSamp_1110), &(UpSamp_1110WEIGHTED_ROUND_ROBIN_Splitter_1376));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_1376
	FOR(uint32_t, __iter_init_, 0, <, 34, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(UpSamp_1110WEIGHTED_ROUND_ROBIN_Splitter_1376), &(SplitJoin212_Delay_N_Fiss_1467_1501_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1378
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin212_Delay_N_Fiss_1467_1501_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1379
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin212_Delay_N_Fiss_1467_1501_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1380
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin212_Delay_N_Fiss_1467_1501_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1381
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin212_Delay_N_Fiss_1467_1501_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1382
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin212_Delay_N_Fiss_1467_1501_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1383
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin212_Delay_N_Fiss_1467_1501_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1384
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin212_Delay_N_Fiss_1467_1501_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1377
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin212_Delay_N_Fiss_1467_1501_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_1377DUPLICATE_Splitter_1385));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_1385
	FOR(uint32_t, __iter_init_, 0, <, 31, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_1377DUPLICATE_Splitter_1385), &(SplitJoin214_FirFilter_Fiss_1468_1502_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_1394
	FOR(uint32_t, __iter_init_, 0, <, 69, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(SplitJoin0_SplitJoin0_SplitJoin0_Branches_1060_1124_1439_1474_split[7]), &(SplitJoin241_Delay_N_Fiss_1469_1503_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1396
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin241_Delay_N_Fiss_1469_1503_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1397
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin241_Delay_N_Fiss_1469_1503_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1398
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin241_Delay_N_Fiss_1469_1503_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1399
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin241_Delay_N_Fiss_1469_1503_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1400
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin241_Delay_N_Fiss_1469_1503_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1401
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin241_Delay_N_Fiss_1469_1503_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1402
	FOR(uint32_t, __iter_init_, 0, <, 41, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin241_Delay_N_Fiss_1469_1503_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1395
	FOR(uint32_t, __iter_init_, 0, <, 40, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin241_Delay_N_Fiss_1469_1503_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_1395DUPLICATE_Splitter_1403));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_1403
	FOR(uint32_t, __iter_init_, 0, <, 276, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_1395DUPLICATE_Splitter_1403), &(SplitJoin243_FirFilter_Fiss_1470_1504_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1405
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin243_FirFilter_Fiss_1470_1504_split[0]), &(SplitJoin243_FirFilter_Fiss_1470_1504_join[0]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1406
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin243_FirFilter_Fiss_1470_1504_split[1]), &(SplitJoin243_FirFilter_Fiss_1470_1504_join[1]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1407
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin243_FirFilter_Fiss_1470_1504_split[2]), &(SplitJoin243_FirFilter_Fiss_1470_1504_join[2]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1408
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin243_FirFilter_Fiss_1470_1504_split[3]), &(SplitJoin243_FirFilter_Fiss_1470_1504_join[3]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1409
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin243_FirFilter_Fiss_1470_1504_split[4]), &(SplitJoin243_FirFilter_Fiss_1470_1504_join[4]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1410
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin243_FirFilter_Fiss_1470_1504_split[5]), &(SplitJoin243_FirFilter_Fiss_1470_1504_join[5]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1411
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		FirFilter(&(SplitJoin243_FirFilter_Fiss_1470_1504_split[6]), &(SplitJoin243_FirFilter_Fiss_1470_1504_join[6]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1404
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin243_FirFilter_Fiss_1470_1504_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_1404DownSamp_1116));
	ENDFOR
//--------------------------------
// --- init: DownSamp_1116
	FOR(uint32_t, __iter_init_, 0, <, 30, __iter_init_++)
		DownSamp(&(WEIGHTED_ROUND_ROBIN_Joiner_1404DownSamp_1116), &(DownSamp_1116UpSamp_1117));
	ENDFOR
//--------------------------------
// --- init: UpSamp_1117
	FOR(uint32_t, __iter_init_, 0, <, 30, __iter_init_++)
		UpSamp(&(DownSamp_1116UpSamp_1117), &(UpSamp_1117WEIGHTED_ROUND_ROBIN_Splitter_1412));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_1412
	FOR(uint32_t, __iter_init_, 0, <, 34, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Splitter(&(UpSamp_1117WEIGHTED_ROUND_ROBIN_Splitter_1412), &(SplitJoin245_Delay_N_Fiss_1471_1505_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1414
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin245_Delay_N_Fiss_1471_1505_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1415
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin245_Delay_N_Fiss_1471_1505_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1416
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin245_Delay_N_Fiss_1471_1505_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1417
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin245_Delay_N_Fiss_1471_1505_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1418
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin245_Delay_N_Fiss_1471_1505_join[4], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1419
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin245_Delay_N_Fiss_1471_1505_join[5], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_1420
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin245_Delay_N_Fiss_1471_1505_join[6], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_1413
	FOR(uint32_t, __iter_init_, 0, <, 5, __iter_init_++)
		WEIGHTED_ROUND_ROBIN_Joiner(&(SplitJoin245_Delay_N_Fiss_1471_1505_join[Need Human for iter var]), &(WEIGHTED_ROUND_ROBIN_Joiner_1413DUPLICATE_Splitter_1421));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_1421
	FOR(uint32_t, __iter_init_, 0, <, 31, __iter_init_++)
		DUPLICATE_Splitter(&(WEIGHTED_ROUND_ROBIN_Joiner_1413DUPLICATE_Splitter_1421), &(SplitJoin247_FirFilter_Fiss_1472_1506_split[Need Human for iter var]));
	ENDFOR
//--------------------------------
// --- init: FirFilter_1429
	 {
	FirFilter_1429_s.COEFF[0] = 56.0 ; 
	FirFilter_1429_s.COEFF[1] = 64.0 ; 
	FirFilter_1429_s.COEFF[2] = 72.0 ; 
	FirFilter_1429_s.COEFF[3] = 80.0 ; 
	FirFilter_1429_s.COEFF[4] = 88.0 ; 
	FirFilter_1429_s.COEFF[5] = 96.0 ; 
	FirFilter_1429_s.COEFF[6] = 104.0 ; 
	FirFilter_1429_s.COEFF[7] = 112.0 ; 
	FirFilter_1429_s.COEFF[8] = 120.0 ; 
	FirFilter_1429_s.COEFF[9] = 128.0 ; 
	FirFilter_1429_s.COEFF[10] = 136.0 ; 
	FirFilter_1429_s.COEFF[11] = 144.0 ; 
	FirFilter_1429_s.COEFF[12] = 152.0 ; 
	FirFilter_1429_s.COEFF[13] = 160.0 ; 
	FirFilter_1429_s.COEFF[14] = 168.0 ; 
	FirFilter_1429_s.COEFF[15] = 176.0 ; 
	FirFilter_1429_s.COEFF[16] = 184.0 ; 
	FirFilter_1429_s.COEFF[17] = 192.0 ; 
	FirFilter_1429_s.COEFF[18] = 200.0 ; 
	FirFilter_1429_s.COEFF[19] = 208.0 ; 
	FirFilter_1429_s.COEFF[20] = 216.0 ; 
	FirFilter_1429_s.COEFF[21] = 224.0 ; 
	FirFilter_1429_s.COEFF[22] = 232.0 ; 
	FirFilter_1429_s.COEFF[23] = 240.0 ; 
	FirFilter_1429_s.COEFF[24] = 248.0 ; 
	FirFilter_1429_s.COEFF[25] = 256.0 ; 
	FirFilter_1429_s.COEFF[26] = 264.0 ; 
	FirFilter_1429_s.COEFF[27] = 272.0 ; 
	FirFilter_1429_s.COEFF[28] = 280.0 ; 
	FirFilter_1429_s.COEFF[29] = 288.0 ; 
	FirFilter_1429_s.COEFF[30] = 296.0 ; 
	FirFilter_1429_s.COEFF[31] = 304.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		source_1062();
		DUPLICATE_Splitter_1122();
			WEIGHTED_ROUND_ROBIN_Splitter_1142();
				Delay_N_1144();
				Delay_N_1145();
				Delay_N_1146();
				Delay_N_1147();
				Delay_N_1148();
				Delay_N_1149();
				Delay_N_1150();
			WEIGHTED_ROUND_ROBIN_Joiner_1143();
			DUPLICATE_Splitter_1151();
				FirFilter_1153();
				FirFilter_1154();
				FirFilter_1155();
				FirFilter_1156();
				FirFilter_1157();
				FirFilter_1158();
				FirFilter_1159();
			WEIGHTED_ROUND_ROBIN_Joiner_1152();
			DownSamp_1067();
			UpSamp_1068();
			WEIGHTED_ROUND_ROBIN_Splitter_1160();
				Delay_N_1162();
				Delay_N_1163();
				Delay_N_1164();
				Delay_N_1165();
				Delay_N_1166();
				Delay_N_1167();
				Delay_N_1168();
			WEIGHTED_ROUND_ROBIN_Joiner_1161();
			DUPLICATE_Splitter_1169();
				FirFilter_1171();
				FirFilter_1172();
				FirFilter_1173();
				FirFilter_1174();
				FirFilter_1175();
				FirFilter_1176();
				FirFilter_1177();
			WEIGHTED_ROUND_ROBIN_Joiner_1170();
			WEIGHTED_ROUND_ROBIN_Splitter_1178();
				Delay_N_1180();
				Delay_N_1181();
				Delay_N_1182();
				Delay_N_1183();
				Delay_N_1184();
				Delay_N_1185();
				Delay_N_1186();
			WEIGHTED_ROUND_ROBIN_Joiner_1179();
			DUPLICATE_Splitter_1187();
				FirFilter_1189();
				FirFilter_1190();
				FirFilter_1191();
				FirFilter_1192();
				FirFilter_1193();
				FirFilter_1194();
				FirFilter_1195();
			WEIGHTED_ROUND_ROBIN_Joiner_1188();
			DownSamp_1074();
			UpSamp_1075();
			WEIGHTED_ROUND_ROBIN_Splitter_1196();
				Delay_N_1198();
				Delay_N_1199();
				Delay_N_1200();
				Delay_N_1201();
				Delay_N_1202();
				Delay_N_1203();
				Delay_N_1204();
			WEIGHTED_ROUND_ROBIN_Joiner_1197();
			DUPLICATE_Splitter_1205();
				FirFilter_1207();
				FirFilter_1208();
				FirFilter_1209();
				FirFilter_1210();
				FirFilter_1211();
				FirFilter_1212();
				FirFilter_1213();
			WEIGHTED_ROUND_ROBIN_Joiner_1206();
			WEIGHTED_ROUND_ROBIN_Splitter_1214();
				Delay_N_1216();
				Delay_N_1217();
				Delay_N_1218();
				Delay_N_1219();
				Delay_N_1220();
				Delay_N_1221();
				Delay_N_1222();
			WEIGHTED_ROUND_ROBIN_Joiner_1215();
			DUPLICATE_Splitter_1223();
				FirFilter_1225();
				FirFilter_1226();
				FirFilter_1227();
				FirFilter_1228();
				FirFilter_1229();
				FirFilter_1230();
				FirFilter_1231();
			WEIGHTED_ROUND_ROBIN_Joiner_1224();
			DownSamp_1081();
			UpSamp_1082();
			WEIGHTED_ROUND_ROBIN_Splitter_1232();
				Delay_N_1234();
				Delay_N_1235();
				Delay_N_1236();
				Delay_N_1237();
				Delay_N_1238();
				Delay_N_1239();
				Delay_N_1240();
			WEIGHTED_ROUND_ROBIN_Joiner_1233();
			DUPLICATE_Splitter_1241();
				FirFilter_1243();
				FirFilter_1244();
				FirFilter_1245();
				FirFilter_1246();
				FirFilter_1247();
				FirFilter_1248();
				FirFilter_1249();
			WEIGHTED_ROUND_ROBIN_Joiner_1242();
			WEIGHTED_ROUND_ROBIN_Splitter_1250();
				Delay_N_1252();
				Delay_N_1253();
				Delay_N_1254();
				Delay_N_1255();
				Delay_N_1256();
				Delay_N_1257();
				Delay_N_1258();
			WEIGHTED_ROUND_ROBIN_Joiner_1251();
			DUPLICATE_Splitter_1259();
				FirFilter_1261();
				FirFilter_1262();
				FirFilter_1263();
				FirFilter_1264();
				FirFilter_1265();
				FirFilter_1266();
				FirFilter_1267();
			WEIGHTED_ROUND_ROBIN_Joiner_1260();
			DownSamp_1088();
			UpSamp_1089();
			WEIGHTED_ROUND_ROBIN_Splitter_1268();
				Delay_N_1270();
				Delay_N_1271();
				Delay_N_1272();
				Delay_N_1273();
				Delay_N_1274();
				Delay_N_1275();
				Delay_N_1276();
			WEIGHTED_ROUND_ROBIN_Joiner_1269();
			DUPLICATE_Splitter_1277();
				FirFilter_1279();
				FirFilter_1280();
				FirFilter_1281();
				FirFilter_1282();
				FirFilter_1283();
				FirFilter_1284();
				FirFilter_1285();
			WEIGHTED_ROUND_ROBIN_Joiner_1278();
			WEIGHTED_ROUND_ROBIN_Splitter_1286();
				Delay_N_1288();
				Delay_N_1289();
				Delay_N_1290();
				Delay_N_1291();
				Delay_N_1292();
				Delay_N_1293();
				Delay_N_1294();
			WEIGHTED_ROUND_ROBIN_Joiner_1287();
			DUPLICATE_Splitter_1295();
				FirFilter_1297();
				FirFilter_1298();
				FirFilter_1299();
				FirFilter_1300();
				FirFilter_1301();
				FirFilter_1302();
				FirFilter_1303();
			WEIGHTED_ROUND_ROBIN_Joiner_1296();
			DownSamp_1095();
			UpSamp_1096();
			WEIGHTED_ROUND_ROBIN_Splitter_1304();
				Delay_N_1306();
				Delay_N_1307();
				Delay_N_1308();
				Delay_N_1309();
				Delay_N_1310();
				Delay_N_1311();
				Delay_N_1312();
			WEIGHTED_ROUND_ROBIN_Joiner_1305();
			DUPLICATE_Splitter_1313();
				FirFilter_1315();
				FirFilter_1316();
				FirFilter_1317();
				FirFilter_1318();
				FirFilter_1319();
				FirFilter_1320();
				FirFilter_1321();
			WEIGHTED_ROUND_ROBIN_Joiner_1314();
			WEIGHTED_ROUND_ROBIN_Splitter_1322();
				Delay_N_1324();
				Delay_N_1325();
				Delay_N_1326();
				Delay_N_1327();
				Delay_N_1328();
				Delay_N_1329();
				Delay_N_1330();
			WEIGHTED_ROUND_ROBIN_Joiner_1323();
			DUPLICATE_Splitter_1331();
				FirFilter_1333();
				FirFilter_1334();
				FirFilter_1335();
				FirFilter_1336();
				FirFilter_1337();
				FirFilter_1338();
				FirFilter_1339();
			WEIGHTED_ROUND_ROBIN_Joiner_1332();
			DownSamp_1102();
			UpSamp_1103();
			WEIGHTED_ROUND_ROBIN_Splitter_1340();
				Delay_N_1342();
				Delay_N_1343();
				Delay_N_1344();
				Delay_N_1345();
				Delay_N_1346();
				Delay_N_1347();
				Delay_N_1348();
			WEIGHTED_ROUND_ROBIN_Joiner_1341();
			DUPLICATE_Splitter_1349();
				FirFilter_1351();
				FirFilter_1352();
				FirFilter_1353();
				FirFilter_1354();
				FirFilter_1355();
				FirFilter_1356();
				FirFilter_1357();
			WEIGHTED_ROUND_ROBIN_Joiner_1350();
			WEIGHTED_ROUND_ROBIN_Splitter_1358();
				Delay_N_1360();
				Delay_N_1361();
				Delay_N_1362();
				Delay_N_1363();
				Delay_N_1364();
				Delay_N_1365();
				Delay_N_1366();
			WEIGHTED_ROUND_ROBIN_Joiner_1359();
			DUPLICATE_Splitter_1367();
				FirFilter_1369();
				FirFilter_1370();
				FirFilter_1371();
				FirFilter_1372();
				FirFilter_1373();
				FirFilter_1374();
				FirFilter_1375();
			WEIGHTED_ROUND_ROBIN_Joiner_1368();
			DownSamp_1109();
			UpSamp_1110();
			WEIGHTED_ROUND_ROBIN_Splitter_1376();
				Delay_N_1378();
				Delay_N_1379();
				Delay_N_1380();
				Delay_N_1381();
				Delay_N_1382();
				Delay_N_1383();
				Delay_N_1384();
			WEIGHTED_ROUND_ROBIN_Joiner_1377();
			DUPLICATE_Splitter_1385();
				FirFilter_1387();
				FirFilter_1388();
				FirFilter_1389();
				FirFilter_1390();
				FirFilter_1391();
				FirFilter_1392();
				FirFilter_1393();
			WEIGHTED_ROUND_ROBIN_Joiner_1386();
			WEIGHTED_ROUND_ROBIN_Splitter_1394();
				Delay_N_1396();
				Delay_N_1397();
				Delay_N_1398();
				Delay_N_1399();
				Delay_N_1400();
				Delay_N_1401();
				Delay_N_1402();
			WEIGHTED_ROUND_ROBIN_Joiner_1395();
			DUPLICATE_Splitter_1403();
				FirFilter_1405();
				FirFilter_1406();
				FirFilter_1407();
				FirFilter_1408();
				FirFilter_1409();
				FirFilter_1410();
				FirFilter_1411();
			WEIGHTED_ROUND_ROBIN_Joiner_1404();
			DownSamp_1116();
			UpSamp_1117();
			WEIGHTED_ROUND_ROBIN_Splitter_1412();
				Delay_N_1414();
				Delay_N_1415();
				Delay_N_1416();
				Delay_N_1417();
				Delay_N_1418();
				Delay_N_1419();
				Delay_N_1420();
			WEIGHTED_ROUND_ROBIN_Joiner_1413();
			DUPLICATE_Splitter_1421();
				FirFilter_1423();
				FirFilter_1424();
				FirFilter_1425();
				FirFilter_1426();
				FirFilter_1427();
				FirFilter_1428();
				FirFilter_1429();
			WEIGHTED_ROUND_ROBIN_Joiner_1422();
		WEIGHTED_ROUND_ROBIN_Joiner_1123();
		WEIGHTED_ROUND_ROBIN_Splitter_1430();
			Combine_1432();
			Combine_1433();
			Combine_1434();
			Combine_1435();
			Combine_1436();
			Combine_1437();
			Combine_1438();
		WEIGHTED_ROUND_ROBIN_Joiner_1431();
		sink_1121();
	ENDFOR
	return EXIT_SUCCESS;
}
