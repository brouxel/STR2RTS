#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=1820 on the compile command line
#else
#if BUF_SIZEMAX < 1820
#error BUF_SIZEMAX too small, it must be at least 1820
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float current;
} source_3872_t;

typedef struct {
	float COEFF[32];
} FirFilter_3958_t;
void source_3872();
void DUPLICATE_Splitter_3932();
void WEIGHTED_ROUND_ROBIN_Splitter_3952();
void Delay_N_3954();
void Delay_N_3955();
void WEIGHTED_ROUND_ROBIN_Joiner_3953();
void DUPLICATE_Splitter_3956();
void FirFilter_3958();
void FirFilter_3959();
void WEIGHTED_ROUND_ROBIN_Joiner_3957();
void DownSamp_3877();
void UpSamp_3878();
void WEIGHTED_ROUND_ROBIN_Splitter_3960();
void Delay_N_3962();
void Delay_N_3963();
void WEIGHTED_ROUND_ROBIN_Joiner_3961();
void DUPLICATE_Splitter_3964();
void FirFilter_3966();
void FirFilter_3967();
void WEIGHTED_ROUND_ROBIN_Joiner_3965();
void WEIGHTED_ROUND_ROBIN_Splitter_3968();
void Delay_N_3970();
void Delay_N_3971();
void WEIGHTED_ROUND_ROBIN_Joiner_3969();
void DUPLICATE_Splitter_3972();
void FirFilter_3974();
void FirFilter_3975();
void WEIGHTED_ROUND_ROBIN_Joiner_3973();
void DownSamp_3884();
void UpSamp_3885();
void WEIGHTED_ROUND_ROBIN_Splitter_3976();
void Delay_N_3978();
void Delay_N_3979();
void WEIGHTED_ROUND_ROBIN_Joiner_3977();
void DUPLICATE_Splitter_3980();
void FirFilter_3982();
void FirFilter_3983();
void WEIGHTED_ROUND_ROBIN_Joiner_3981();
void WEIGHTED_ROUND_ROBIN_Splitter_3984();
void Delay_N_3986();
void Delay_N_3987();
void WEIGHTED_ROUND_ROBIN_Joiner_3985();
void DUPLICATE_Splitter_3988();
void FirFilter_3990();
void FirFilter_3991();
void WEIGHTED_ROUND_ROBIN_Joiner_3989();
void DownSamp_3891();
void UpSamp_3892();
void WEIGHTED_ROUND_ROBIN_Splitter_3992();
void Delay_N_3994();
void Delay_N_3995();
void WEIGHTED_ROUND_ROBIN_Joiner_3993();
void DUPLICATE_Splitter_3996();
void FirFilter_3998();
void FirFilter_3999();
void WEIGHTED_ROUND_ROBIN_Joiner_3997();
void WEIGHTED_ROUND_ROBIN_Splitter_4000();
void Delay_N_4002();
void Delay_N_4003();
void WEIGHTED_ROUND_ROBIN_Joiner_4001();
void DUPLICATE_Splitter_4004();
void FirFilter_4006();
void FirFilter_4007();
void WEIGHTED_ROUND_ROBIN_Joiner_4005();
void DownSamp_3898();
void UpSamp_3899();
void WEIGHTED_ROUND_ROBIN_Splitter_4008();
void Delay_N_4010();
void Delay_N_4011();
void WEIGHTED_ROUND_ROBIN_Joiner_4009();
void DUPLICATE_Splitter_4012();
void FirFilter_4014();
void FirFilter_4015();
void WEIGHTED_ROUND_ROBIN_Joiner_4013();
void WEIGHTED_ROUND_ROBIN_Splitter_4016();
void Delay_N_4018();
void Delay_N_4019();
void WEIGHTED_ROUND_ROBIN_Joiner_4017();
void DUPLICATE_Splitter_4020();
void FirFilter_4022();
void FirFilter_4023();
void WEIGHTED_ROUND_ROBIN_Joiner_4021();
void DownSamp_3905();
void UpSamp_3906();
void WEIGHTED_ROUND_ROBIN_Splitter_4024();
void Delay_N_4026();
void Delay_N_4027();
void WEIGHTED_ROUND_ROBIN_Joiner_4025();
void DUPLICATE_Splitter_4028();
void FirFilter_4030();
void FirFilter_4031();
void WEIGHTED_ROUND_ROBIN_Joiner_4029();
void WEIGHTED_ROUND_ROBIN_Splitter_4032();
void Delay_N_4034();
void Delay_N_4035();
void WEIGHTED_ROUND_ROBIN_Joiner_4033();
void DUPLICATE_Splitter_4036();
void FirFilter_4038();
void FirFilter_4039();
void WEIGHTED_ROUND_ROBIN_Joiner_4037();
void DownSamp_3912();
void UpSamp_3913();
void WEIGHTED_ROUND_ROBIN_Splitter_4040();
void Delay_N_4042();
void Delay_N_4043();
void WEIGHTED_ROUND_ROBIN_Joiner_4041();
void DUPLICATE_Splitter_4044();
void FirFilter_4046();
void FirFilter_4047();
void WEIGHTED_ROUND_ROBIN_Joiner_4045();
void WEIGHTED_ROUND_ROBIN_Splitter_4048();
void Delay_N_4050();
void Delay_N_4051();
void WEIGHTED_ROUND_ROBIN_Joiner_4049();
void DUPLICATE_Splitter_4052();
void FirFilter_4054();
void FirFilter_4055();
void WEIGHTED_ROUND_ROBIN_Joiner_4053();
void DownSamp_3919();
void UpSamp_3920();
void WEIGHTED_ROUND_ROBIN_Splitter_4056();
void Delay_N_4058();
void Delay_N_4059();
void WEIGHTED_ROUND_ROBIN_Joiner_4057();
void DUPLICATE_Splitter_4060();
void FirFilter_4062();
void FirFilter_4063();
void WEIGHTED_ROUND_ROBIN_Joiner_4061();
void WEIGHTED_ROUND_ROBIN_Splitter_4064();
void Delay_N_4066();
void Delay_N_4067();
void WEIGHTED_ROUND_ROBIN_Joiner_4065();
void DUPLICATE_Splitter_4068();
void FirFilter_4070();
void FirFilter_4071();
void WEIGHTED_ROUND_ROBIN_Joiner_4069();
void DownSamp_3926();
void UpSamp_3927();
void WEIGHTED_ROUND_ROBIN_Splitter_4072();
void Delay_N_4074();
void Delay_N_4075();
void WEIGHTED_ROUND_ROBIN_Joiner_4073();
void DUPLICATE_Splitter_4076();
void FirFilter_4078();
void FirFilter_4079();
void WEIGHTED_ROUND_ROBIN_Joiner_4077();
void WEIGHTED_ROUND_ROBIN_Joiner_3933();
void WEIGHTED_ROUND_ROBIN_Splitter_4080();
void Combine_4082();
void Combine_4083();
void WEIGHTED_ROUND_ROBIN_Joiner_4081();
void sink_3931();

#ifdef __cplusplus
}
#endif
#endif
