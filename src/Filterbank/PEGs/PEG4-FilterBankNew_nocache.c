#include "PEG4-FilterBankNew_nocache.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3207DUPLICATE_Splitter_3212;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3063DUPLICATE_Splitter_3068;
buffer_float_t SplitJoin139_FirFilter_Fiss_3253_3287_split[4];
buffer_float_t DownSamp_2951UpSamp_2952;
buffer_float_t SplitJoin4_FirFilter_Fiss_3226_3261_join[4];
buffer_float_t SplitJoin55_FirFilter_Fiss_3237_3271_split[4];
buffer_float_t SplitJoin6_Delay_N_Fiss_3227_3262_split[4];
buffer_float_t SplitJoin51_FirFilter_Fiss_3235_3269_join[4];
buffer_float_t SplitJoin74_Delay_N_Fiss_3240_3274_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3129DownSamp_2979;
buffer_float_t UpSamp_2994WEIGHTED_ROUND_ROBIN_Splitter_3182;
buffer_float_t SplitJoin154_Delay_N_Fiss_3254_3288_join[4];
buffer_float_t SplitJoin72_FirFilter_Fiss_3239_3273_join[4];
buffer_float_t SplitJoin76_FirFilter_Fiss_3241_3275_join[4];
buffer_float_t SplitJoin28_Delay_N_Fiss_3230_3264_split[4];
buffer_float_t DownSamp_3000UpSamp_3001;
buffer_float_t SplitJoin93_FirFilter_Fiss_3243_3277_join[4];
buffer_float_t SplitJoin160_FirFilter_Fiss_3257_3291_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3081DownSamp_2965;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3183DUPLICATE_Splitter_3188;
buffer_float_t SplitJoin8_FirFilter_Fiss_3228_3263_join[4];
buffer_float_t source_2946DUPLICATE_Splitter_3006;
buffer_float_t SplitJoin10_Combine_Fiss_3229_3292_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3027DUPLICATE_Splitter_3032;
buffer_float_t DownSamp_2993UpSamp_2994;
buffer_float_t SplitJoin74_Delay_N_Fiss_3240_3274_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3105DownSamp_2972;
buffer_float_t UpSamp_2973WEIGHTED_ROUND_ROBIN_Splitter_3110;
buffer_float_t SplitJoin70_Delay_N_Fiss_3238_3272_split[4];
buffer_float_t SplitJoin95_Delay_N_Fiss_3244_3278_join[4];
buffer_float_t SplitJoin156_FirFilter_Fiss_3255_3289_join[4];
buffer_float_t SplitJoin112_Delay_N_Fiss_3246_3280_split[4];
buffer_float_t SplitJoin53_Delay_N_Fiss_3236_3270_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3075DUPLICATE_Splitter_3080;
buffer_float_t UpSamp_2987WEIGHTED_ROUND_ROBIN_Splitter_3158;
buffer_float_t SplitJoin49_Delay_N_Fiss_3234_3268_join[4];
buffer_float_t SplitJoin49_Delay_N_Fiss_3234_3268_split[4];
buffer_float_t SplitJoin158_Delay_N_Fiss_3256_3290_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3195DUPLICATE_Splitter_3200;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3219sink_3005;
buffer_float_t SplitJoin2_Delay_N_Fiss_3225_3260_join[4];
buffer_float_t DownSamp_2986UpSamp_2987;
buffer_float_t SplitJoin30_FirFilter_Fiss_3231_3265_join[4];
buffer_float_t SplitJoin32_Delay_N_Fiss_3232_3266_join[4];
buffer_float_t SplitJoin53_Delay_N_Fiss_3236_3270_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3171DUPLICATE_Splitter_3176;
buffer_float_t SplitJoin91_Delay_N_Fiss_3242_3276_join[4];
buffer_float_t UpSamp_3001WEIGHTED_ROUND_ROBIN_Splitter_3206;
buffer_float_t SplitJoin10_Combine_Fiss_3229_3292_split[4];
buffer_float_t UpSamp_2952WEIGHTED_ROUND_ROBIN_Splitter_3038;
buffer_float_t SplitJoin6_Delay_N_Fiss_3227_3262_join[4];
buffer_float_t SplitJoin135_FirFilter_Fiss_3251_3285_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3201DownSamp_3000;
buffer_float_t SplitJoin8_FirFilter_Fiss_3228_3263_split[4];
buffer_float_t SplitJoin30_FirFilter_Fiss_3231_3265_split[4];
buffer_float_t SplitJoin2_Delay_N_Fiss_3225_3260_split[4];
buffer_float_t DownSamp_2979UpSamp_2980;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3039DUPLICATE_Splitter_3044;
buffer_float_t SplitJoin154_Delay_N_Fiss_3254_3288_split[4];
buffer_float_t SplitJoin112_Delay_N_Fiss_3246_3280_join[4];
buffer_float_t SplitJoin158_Delay_N_Fiss_3256_3290_join[4];
buffer_float_t SplitJoin97_FirFilter_Fiss_3245_3279_split[4];
buffer_float_t SplitJoin114_FirFilter_Fiss_3247_3281_join[4];
buffer_float_t SplitJoin118_FirFilter_Fiss_3249_3283_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3087DUPLICATE_Splitter_3092;
buffer_float_t SplitJoin4_FirFilter_Fiss_3226_3261_split[4];
buffer_float_t SplitJoin137_Delay_N_Fiss_3252_3286_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3159DUPLICATE_Splitter_3164;
buffer_float_t SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[8];
buffer_float_t SplitJoin70_Delay_N_Fiss_3238_3272_join[4];
buffer_float_t SplitJoin118_FirFilter_Fiss_3249_3283_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3099DUPLICATE_Splitter_3104;
buffer_float_t SplitJoin160_FirFilter_Fiss_3257_3291_join[4];
buffer_float_t SplitJoin32_Delay_N_Fiss_3232_3266_split[4];
buffer_float_t SplitJoin139_FirFilter_Fiss_3253_3287_join[4];
buffer_float_t UpSamp_2980WEIGHTED_ROUND_ROBIN_Splitter_3134;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3177DownSamp_2993;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3051DUPLICATE_Splitter_3056;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3007WEIGHTED_ROUND_ROBIN_Splitter_3218;
buffer_float_t SplitJoin95_Delay_N_Fiss_3244_3278_split[4];
buffer_float_t SplitJoin156_FirFilter_Fiss_3255_3289_split[4];
buffer_float_t SplitJoin34_FirFilter_Fiss_3233_3267_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3111DUPLICATE_Splitter_3116;
buffer_float_t SplitJoin133_Delay_N_Fiss_3250_3284_split[4];
buffer_float_t SplitJoin133_Delay_N_Fiss_3250_3284_join[4];
buffer_float_t DownSamp_2958UpSamp_2959;
buffer_float_t SplitJoin91_Delay_N_Fiss_3242_3276_split[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3153DownSamp_2986;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3033DownSamp_2951;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3147DUPLICATE_Splitter_3152;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3123DUPLICATE_Splitter_3128;
buffer_float_t SplitJoin51_FirFilter_Fiss_3235_3269_split[4];
buffer_float_t SplitJoin116_Delay_N_Fiss_3248_3282_join[4];
buffer_float_t SplitJoin135_FirFilter_Fiss_3251_3285_split[4];
buffer_float_t SplitJoin76_FirFilter_Fiss_3241_3275_split[4];
buffer_float_t SplitJoin114_FirFilter_Fiss_3247_3281_split[4];
buffer_float_t SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[8];
buffer_float_t SplitJoin97_FirFilter_Fiss_3245_3279_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3057DownSamp_2958;
buffer_float_t DownSamp_2972UpSamp_2973;
buffer_float_t DownSamp_2965UpSamp_2966;
buffer_float_t SplitJoin72_FirFilter_Fiss_3239_3273_split[4];
buffer_float_t SplitJoin55_FirFilter_Fiss_3237_3271_join[4];
buffer_float_t SplitJoin137_Delay_N_Fiss_3252_3286_join[4];
buffer_float_t UpSamp_2966WEIGHTED_ROUND_ROBIN_Splitter_3086;
buffer_float_t UpSamp_2959WEIGHTED_ROUND_ROBIN_Splitter_3062;
buffer_float_t SplitJoin93_FirFilter_Fiss_3243_3277_split[4];
buffer_float_t SplitJoin34_FirFilter_Fiss_3233_3267_join[4];
buffer_float_t SplitJoin28_Delay_N_Fiss_3230_3264_join[4];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3135DUPLICATE_Splitter_3140;
buffer_float_t SplitJoin116_Delay_N_Fiss_3248_3282_split[4];


source_2946_t source_2946_s;
FirFilter_3034_t FirFilter_3034_s;
FirFilter_3034_t FirFilter_3035_s;
FirFilter_3034_t FirFilter_3036_s;
FirFilter_3034_t FirFilter_3037_s;
FirFilter_3034_t FirFilter_3046_s;
FirFilter_3034_t FirFilter_3047_s;
FirFilter_3034_t FirFilter_3048_s;
FirFilter_3034_t FirFilter_3049_s;
FirFilter_3034_t FirFilter_3058_s;
FirFilter_3034_t FirFilter_3059_s;
FirFilter_3034_t FirFilter_3060_s;
FirFilter_3034_t FirFilter_3061_s;
FirFilter_3034_t FirFilter_3070_s;
FirFilter_3034_t FirFilter_3071_s;
FirFilter_3034_t FirFilter_3072_s;
FirFilter_3034_t FirFilter_3073_s;
FirFilter_3034_t FirFilter_3082_s;
FirFilter_3034_t FirFilter_3083_s;
FirFilter_3034_t FirFilter_3084_s;
FirFilter_3034_t FirFilter_3085_s;
FirFilter_3034_t FirFilter_3094_s;
FirFilter_3034_t FirFilter_3095_s;
FirFilter_3034_t FirFilter_3096_s;
FirFilter_3034_t FirFilter_3097_s;
FirFilter_3034_t FirFilter_3106_s;
FirFilter_3034_t FirFilter_3107_s;
FirFilter_3034_t FirFilter_3108_s;
FirFilter_3034_t FirFilter_3109_s;
FirFilter_3034_t FirFilter_3118_s;
FirFilter_3034_t FirFilter_3119_s;
FirFilter_3034_t FirFilter_3120_s;
FirFilter_3034_t FirFilter_3121_s;
FirFilter_3034_t FirFilter_3130_s;
FirFilter_3034_t FirFilter_3131_s;
FirFilter_3034_t FirFilter_3132_s;
FirFilter_3034_t FirFilter_3133_s;
FirFilter_3034_t FirFilter_3142_s;
FirFilter_3034_t FirFilter_3143_s;
FirFilter_3034_t FirFilter_3144_s;
FirFilter_3034_t FirFilter_3145_s;
FirFilter_3034_t FirFilter_3154_s;
FirFilter_3034_t FirFilter_3155_s;
FirFilter_3034_t FirFilter_3156_s;
FirFilter_3034_t FirFilter_3157_s;
FirFilter_3034_t FirFilter_3166_s;
FirFilter_3034_t FirFilter_3167_s;
FirFilter_3034_t FirFilter_3168_s;
FirFilter_3034_t FirFilter_3169_s;
FirFilter_3034_t FirFilter_3178_s;
FirFilter_3034_t FirFilter_3179_s;
FirFilter_3034_t FirFilter_3180_s;
FirFilter_3034_t FirFilter_3181_s;
FirFilter_3034_t FirFilter_3190_s;
FirFilter_3034_t FirFilter_3191_s;
FirFilter_3034_t FirFilter_3192_s;
FirFilter_3034_t FirFilter_3193_s;
FirFilter_3034_t FirFilter_3202_s;
FirFilter_3034_t FirFilter_3203_s;
FirFilter_3034_t FirFilter_3204_s;
FirFilter_3034_t FirFilter_3205_s;
FirFilter_3034_t FirFilter_3214_s;
FirFilter_3034_t FirFilter_3215_s;
FirFilter_3034_t FirFilter_3216_s;
FirFilter_3034_t FirFilter_3217_s;

void source_2946(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&source_2946DUPLICATE_Splitter_3006, source_2946_s.current) ; 
		if((source_2946_s.current > 1000.0)) {
			source_2946_s.current = 0.0 ; 
		}
		else {
			source_2946_s.current = (source_2946_s.current + 1.0) ; 
		}
	}
	ENDFOR
}

void Delay_N_3028(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin2_Delay_N_Fiss_3225_3260_join[0], pop_float(&SplitJoin2_Delay_N_Fiss_3225_3260_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_3029(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin2_Delay_N_Fiss_3225_3260_join[1], pop_float(&SplitJoin2_Delay_N_Fiss_3225_3260_split[1])) ; 
	}
	ENDFOR
}

void Delay_N_3030(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin2_Delay_N_Fiss_3225_3260_join[2], pop_float(&SplitJoin2_Delay_N_Fiss_3225_3260_split[2])) ; 
	}
	ENDFOR
}

void Delay_N_3031(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin2_Delay_N_Fiss_3225_3260_join[3], pop_float(&SplitJoin2_Delay_N_Fiss_3225_3260_split[3])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3026() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin2_Delay_N_Fiss_3225_3260_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[0]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3027() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3027DUPLICATE_Splitter_3032, pop_float(&SplitJoin2_Delay_N_Fiss_3225_3260_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3034(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin4_FirFilter_Fiss_3226_3261_split[0], i) * FirFilter_3034_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin4_FirFilter_Fiss_3226_3261_split[0]) ; 
		push_float(&SplitJoin4_FirFilter_Fiss_3226_3261_join[0], sum) ; 
 {
		FOR(int, streamItVar637, 0,  < , 3, streamItVar637++) {
			pop_void(&SplitJoin4_FirFilter_Fiss_3226_3261_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3035(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin4_FirFilter_Fiss_3226_3261_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin4_FirFilter_Fiss_3226_3261_split[1], i) * FirFilter_3035_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin4_FirFilter_Fiss_3226_3261_split[1]) ; 
		push_float(&SplitJoin4_FirFilter_Fiss_3226_3261_join[1], sum) ; 
 {
		FOR(int, streamItVar638, 0,  < , 2, streamItVar638++) {
			pop_void(&SplitJoin4_FirFilter_Fiss_3226_3261_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3036(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar639, 0,  < , 2, streamItVar639++) {
			pop_void(&SplitJoin4_FirFilter_Fiss_3226_3261_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin4_FirFilter_Fiss_3226_3261_split[2], i) * FirFilter_3036_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin4_FirFilter_Fiss_3226_3261_split[2]) ; 
		push_float(&SplitJoin4_FirFilter_Fiss_3226_3261_join[2], sum) ; 
 {
		pop_void(&SplitJoin4_FirFilter_Fiss_3226_3261_split[2]) ; 
	}
	}
	ENDFOR
}

void FirFilter_3037(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar640, 0,  < , 3, streamItVar640++) {
			pop_void(&SplitJoin4_FirFilter_Fiss_3226_3261_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin4_FirFilter_Fiss_3226_3261_split[3], i) * FirFilter_3037_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin4_FirFilter_Fiss_3226_3261_split[3]) ; 
		push_float(&SplitJoin4_FirFilter_Fiss_3226_3261_join[3], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_3032() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3027DUPLICATE_Splitter_3032);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin4_FirFilter_Fiss_3226_3261_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3033() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3033DownSamp_2951, pop_float(&SplitJoin4_FirFilter_Fiss_3226_3261_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp_2951() {
	push_float(&DownSamp_2951UpSamp_2952, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3033DownSamp_2951)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3033DownSamp_2951) ; 
	}
	ENDFOR
}


void UpSamp_2952() {
	push_float(&UpSamp_2952WEIGHTED_ROUND_ROBIN_Splitter_3038, pop_float(&DownSamp_2951UpSamp_2952)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		push_float(&UpSamp_2952WEIGHTED_ROUND_ROBIN_Splitter_3038, 0.0) ; 
	}
	ENDFOR
}


void Delay_N_3040(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin6_Delay_N_Fiss_3227_3262_join[0], pop_float(&SplitJoin6_Delay_N_Fiss_3227_3262_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_3041(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin6_Delay_N_Fiss_3227_3262_join[1], pop_float(&SplitJoin6_Delay_N_Fiss_3227_3262_split[1])) ; 
	}
	ENDFOR
}

void Delay_N_3042(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin6_Delay_N_Fiss_3227_3262_join[2], pop_float(&SplitJoin6_Delay_N_Fiss_3227_3262_split[2])) ; 
	}
	ENDFOR
}

void Delay_N_3043(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin6_Delay_N_Fiss_3227_3262_join[3], pop_float(&SplitJoin6_Delay_N_Fiss_3227_3262_split[3])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3038() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin6_Delay_N_Fiss_3227_3262_split[__iter_], pop_float(&UpSamp_2952WEIGHTED_ROUND_ROBIN_Splitter_3038));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3039() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3039DUPLICATE_Splitter_3044, pop_float(&SplitJoin6_Delay_N_Fiss_3227_3262_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3046(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin8_FirFilter_Fiss_3228_3263_split[0], i) * FirFilter_3046_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin8_FirFilter_Fiss_3228_3263_split[0]) ; 
		push_float(&SplitJoin8_FirFilter_Fiss_3228_3263_join[0], sum) ; 
 {
		FOR(int, streamItVar633, 0,  < , 3, streamItVar633++) {
			pop_void(&SplitJoin8_FirFilter_Fiss_3228_3263_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3047(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		pop_void(&SplitJoin8_FirFilter_Fiss_3228_3263_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin8_FirFilter_Fiss_3228_3263_split[1], i) * FirFilter_3047_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin8_FirFilter_Fiss_3228_3263_split[1]) ; 
		push_float(&SplitJoin8_FirFilter_Fiss_3228_3263_join[1], sum) ; 
 {
		FOR(int, streamItVar634, 0,  < , 2, streamItVar634++) {
			pop_void(&SplitJoin8_FirFilter_Fiss_3228_3263_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3048(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		FOR(int, streamItVar635, 0,  < , 2, streamItVar635++) {
			pop_void(&SplitJoin8_FirFilter_Fiss_3228_3263_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin8_FirFilter_Fiss_3228_3263_split[2], i) * FirFilter_3048_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin8_FirFilter_Fiss_3228_3263_split[2]) ; 
		push_float(&SplitJoin8_FirFilter_Fiss_3228_3263_join[2], sum) ; 
 {
		pop_void(&SplitJoin8_FirFilter_Fiss_3228_3263_split[2]) ; 
	}
	}
	ENDFOR
}

void FirFilter_3049(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		FOR(int, streamItVar636, 0,  < , 3, streamItVar636++) {
			pop_void(&SplitJoin8_FirFilter_Fiss_3228_3263_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin8_FirFilter_Fiss_3228_3263_split[3], i) * FirFilter_3049_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin8_FirFilter_Fiss_3228_3263_split[3]) ; 
		push_float(&SplitJoin8_FirFilter_Fiss_3228_3263_join[3], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_3044() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3039DUPLICATE_Splitter_3044);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin8_FirFilter_Fiss_3228_3263_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3045() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[0], pop_float(&SplitJoin8_FirFilter_Fiss_3228_3263_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Delay_N_3052(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin28_Delay_N_Fiss_3230_3264_join[0], pop_float(&SplitJoin28_Delay_N_Fiss_3230_3264_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_3053(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin28_Delay_N_Fiss_3230_3264_join[1], pop_float(&SplitJoin28_Delay_N_Fiss_3230_3264_split[1])) ; 
	}
	ENDFOR
}

void Delay_N_3054(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin28_Delay_N_Fiss_3230_3264_join[2], pop_float(&SplitJoin28_Delay_N_Fiss_3230_3264_split[2])) ; 
	}
	ENDFOR
}

void Delay_N_3055(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin28_Delay_N_Fiss_3230_3264_join[3], pop_float(&SplitJoin28_Delay_N_Fiss_3230_3264_split[3])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3050() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin28_Delay_N_Fiss_3230_3264_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3051() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3051DUPLICATE_Splitter_3056, pop_float(&SplitJoin28_Delay_N_Fiss_3230_3264_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3058(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin30_FirFilter_Fiss_3231_3265_split[0], i) * FirFilter_3058_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin30_FirFilter_Fiss_3231_3265_split[0]) ; 
		push_float(&SplitJoin30_FirFilter_Fiss_3231_3265_join[0], sum) ; 
 {
		FOR(int, streamItVar629, 0,  < , 3, streamItVar629++) {
			pop_void(&SplitJoin30_FirFilter_Fiss_3231_3265_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3059(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin30_FirFilter_Fiss_3231_3265_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin30_FirFilter_Fiss_3231_3265_split[1], i) * FirFilter_3059_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin30_FirFilter_Fiss_3231_3265_split[1]) ; 
		push_float(&SplitJoin30_FirFilter_Fiss_3231_3265_join[1], sum) ; 
 {
		FOR(int, streamItVar630, 0,  < , 2, streamItVar630++) {
			pop_void(&SplitJoin30_FirFilter_Fiss_3231_3265_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3060(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar631, 0,  < , 2, streamItVar631++) {
			pop_void(&SplitJoin30_FirFilter_Fiss_3231_3265_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin30_FirFilter_Fiss_3231_3265_split[2], i) * FirFilter_3060_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin30_FirFilter_Fiss_3231_3265_split[2]) ; 
		push_float(&SplitJoin30_FirFilter_Fiss_3231_3265_join[2], sum) ; 
 {
		pop_void(&SplitJoin30_FirFilter_Fiss_3231_3265_split[2]) ; 
	}
	}
	ENDFOR
}

void FirFilter_3061(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar632, 0,  < , 3, streamItVar632++) {
			pop_void(&SplitJoin30_FirFilter_Fiss_3231_3265_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin30_FirFilter_Fiss_3231_3265_split[3], i) * FirFilter_3061_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin30_FirFilter_Fiss_3231_3265_split[3]) ; 
		push_float(&SplitJoin30_FirFilter_Fiss_3231_3265_join[3], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_3056() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3051DUPLICATE_Splitter_3056);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin30_FirFilter_Fiss_3231_3265_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3057() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3057DownSamp_2958, pop_float(&SplitJoin30_FirFilter_Fiss_3231_3265_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp_2958() {
	push_float(&DownSamp_2958UpSamp_2959, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3057DownSamp_2958)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3057DownSamp_2958) ; 
	}
	ENDFOR
}


void UpSamp_2959() {
	push_float(&UpSamp_2959WEIGHTED_ROUND_ROBIN_Splitter_3062, pop_float(&DownSamp_2958UpSamp_2959)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		push_float(&UpSamp_2959WEIGHTED_ROUND_ROBIN_Splitter_3062, 0.0) ; 
	}
	ENDFOR
}


void Delay_N_3064(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin32_Delay_N_Fiss_3232_3266_join[0], pop_float(&SplitJoin32_Delay_N_Fiss_3232_3266_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_3065(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin32_Delay_N_Fiss_3232_3266_join[1], pop_float(&SplitJoin32_Delay_N_Fiss_3232_3266_split[1])) ; 
	}
	ENDFOR
}

void Delay_N_3066(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin32_Delay_N_Fiss_3232_3266_join[2], pop_float(&SplitJoin32_Delay_N_Fiss_3232_3266_split[2])) ; 
	}
	ENDFOR
}

void Delay_N_3067(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin32_Delay_N_Fiss_3232_3266_join[3], pop_float(&SplitJoin32_Delay_N_Fiss_3232_3266_split[3])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3062() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin32_Delay_N_Fiss_3232_3266_split[__iter_], pop_float(&UpSamp_2959WEIGHTED_ROUND_ROBIN_Splitter_3062));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3063() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3063DUPLICATE_Splitter_3068, pop_float(&SplitJoin32_Delay_N_Fiss_3232_3266_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3070(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin34_FirFilter_Fiss_3233_3267_split[0], i) * FirFilter_3070_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin34_FirFilter_Fiss_3233_3267_split[0]) ; 
		push_float(&SplitJoin34_FirFilter_Fiss_3233_3267_join[0], sum) ; 
 {
		FOR(int, streamItVar625, 0,  < , 3, streamItVar625++) {
			pop_void(&SplitJoin34_FirFilter_Fiss_3233_3267_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3071(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		pop_void(&SplitJoin34_FirFilter_Fiss_3233_3267_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin34_FirFilter_Fiss_3233_3267_split[1], i) * FirFilter_3071_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin34_FirFilter_Fiss_3233_3267_split[1]) ; 
		push_float(&SplitJoin34_FirFilter_Fiss_3233_3267_join[1], sum) ; 
 {
		FOR(int, streamItVar626, 0,  < , 2, streamItVar626++) {
			pop_void(&SplitJoin34_FirFilter_Fiss_3233_3267_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3072(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		FOR(int, streamItVar627, 0,  < , 2, streamItVar627++) {
			pop_void(&SplitJoin34_FirFilter_Fiss_3233_3267_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin34_FirFilter_Fiss_3233_3267_split[2], i) * FirFilter_3072_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin34_FirFilter_Fiss_3233_3267_split[2]) ; 
		push_float(&SplitJoin34_FirFilter_Fiss_3233_3267_join[2], sum) ; 
 {
		pop_void(&SplitJoin34_FirFilter_Fiss_3233_3267_split[2]) ; 
	}
	}
	ENDFOR
}

void FirFilter_3073(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		FOR(int, streamItVar628, 0,  < , 3, streamItVar628++) {
			pop_void(&SplitJoin34_FirFilter_Fiss_3233_3267_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin34_FirFilter_Fiss_3233_3267_split[3], i) * FirFilter_3073_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin34_FirFilter_Fiss_3233_3267_split[3]) ; 
		push_float(&SplitJoin34_FirFilter_Fiss_3233_3267_join[3], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_3068() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3063DUPLICATE_Splitter_3068);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin34_FirFilter_Fiss_3233_3267_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3069() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[1], pop_float(&SplitJoin34_FirFilter_Fiss_3233_3267_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Delay_N_3076(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin49_Delay_N_Fiss_3234_3268_join[0], pop_float(&SplitJoin49_Delay_N_Fiss_3234_3268_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_3077(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin49_Delay_N_Fiss_3234_3268_join[1], pop_float(&SplitJoin49_Delay_N_Fiss_3234_3268_split[1])) ; 
	}
	ENDFOR
}

void Delay_N_3078(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin49_Delay_N_Fiss_3234_3268_join[2], pop_float(&SplitJoin49_Delay_N_Fiss_3234_3268_split[2])) ; 
	}
	ENDFOR
}

void Delay_N_3079(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin49_Delay_N_Fiss_3234_3268_join[3], pop_float(&SplitJoin49_Delay_N_Fiss_3234_3268_split[3])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3074() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin49_Delay_N_Fiss_3234_3268_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[2]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3075() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3075DUPLICATE_Splitter_3080, pop_float(&SplitJoin49_Delay_N_Fiss_3234_3268_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3082(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin51_FirFilter_Fiss_3235_3269_split[0], i) * FirFilter_3082_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin51_FirFilter_Fiss_3235_3269_split[0]) ; 
		push_float(&SplitJoin51_FirFilter_Fiss_3235_3269_join[0], sum) ; 
 {
		FOR(int, streamItVar621, 0,  < , 3, streamItVar621++) {
			pop_void(&SplitJoin51_FirFilter_Fiss_3235_3269_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3083(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin51_FirFilter_Fiss_3235_3269_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin51_FirFilter_Fiss_3235_3269_split[1], i) * FirFilter_3083_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin51_FirFilter_Fiss_3235_3269_split[1]) ; 
		push_float(&SplitJoin51_FirFilter_Fiss_3235_3269_join[1], sum) ; 
 {
		FOR(int, streamItVar622, 0,  < , 2, streamItVar622++) {
			pop_void(&SplitJoin51_FirFilter_Fiss_3235_3269_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3084(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar623, 0,  < , 2, streamItVar623++) {
			pop_void(&SplitJoin51_FirFilter_Fiss_3235_3269_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin51_FirFilter_Fiss_3235_3269_split[2], i) * FirFilter_3084_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin51_FirFilter_Fiss_3235_3269_split[2]) ; 
		push_float(&SplitJoin51_FirFilter_Fiss_3235_3269_join[2], sum) ; 
 {
		pop_void(&SplitJoin51_FirFilter_Fiss_3235_3269_split[2]) ; 
	}
	}
	ENDFOR
}

void FirFilter_3085(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar624, 0,  < , 3, streamItVar624++) {
			pop_void(&SplitJoin51_FirFilter_Fiss_3235_3269_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin51_FirFilter_Fiss_3235_3269_split[3], i) * FirFilter_3085_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin51_FirFilter_Fiss_3235_3269_split[3]) ; 
		push_float(&SplitJoin51_FirFilter_Fiss_3235_3269_join[3], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_3080() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3075DUPLICATE_Splitter_3080);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin51_FirFilter_Fiss_3235_3269_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3081() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3081DownSamp_2965, pop_float(&SplitJoin51_FirFilter_Fiss_3235_3269_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp_2965() {
	push_float(&DownSamp_2965UpSamp_2966, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3081DownSamp_2965)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3081DownSamp_2965) ; 
	}
	ENDFOR
}


void UpSamp_2966() {
	push_float(&UpSamp_2966WEIGHTED_ROUND_ROBIN_Splitter_3086, pop_float(&DownSamp_2965UpSamp_2966)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		push_float(&UpSamp_2966WEIGHTED_ROUND_ROBIN_Splitter_3086, 0.0) ; 
	}
	ENDFOR
}


void Delay_N_3088(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin53_Delay_N_Fiss_3236_3270_join[0], pop_float(&SplitJoin53_Delay_N_Fiss_3236_3270_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_3089(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin53_Delay_N_Fiss_3236_3270_join[1], pop_float(&SplitJoin53_Delay_N_Fiss_3236_3270_split[1])) ; 
	}
	ENDFOR
}

void Delay_N_3090(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin53_Delay_N_Fiss_3236_3270_join[2], pop_float(&SplitJoin53_Delay_N_Fiss_3236_3270_split[2])) ; 
	}
	ENDFOR
}

void Delay_N_3091(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin53_Delay_N_Fiss_3236_3270_join[3], pop_float(&SplitJoin53_Delay_N_Fiss_3236_3270_split[3])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3086() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin53_Delay_N_Fiss_3236_3270_split[__iter_], pop_float(&UpSamp_2966WEIGHTED_ROUND_ROBIN_Splitter_3086));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3087() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3087DUPLICATE_Splitter_3092, pop_float(&SplitJoin53_Delay_N_Fiss_3236_3270_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3094(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin55_FirFilter_Fiss_3237_3271_split[0], i) * FirFilter_3094_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin55_FirFilter_Fiss_3237_3271_split[0]) ; 
		push_float(&SplitJoin55_FirFilter_Fiss_3237_3271_join[0], sum) ; 
 {
		FOR(int, streamItVar617, 0,  < , 3, streamItVar617++) {
			pop_void(&SplitJoin55_FirFilter_Fiss_3237_3271_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3095(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		pop_void(&SplitJoin55_FirFilter_Fiss_3237_3271_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin55_FirFilter_Fiss_3237_3271_split[1], i) * FirFilter_3095_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin55_FirFilter_Fiss_3237_3271_split[1]) ; 
		push_float(&SplitJoin55_FirFilter_Fiss_3237_3271_join[1], sum) ; 
 {
		FOR(int, streamItVar618, 0,  < , 2, streamItVar618++) {
			pop_void(&SplitJoin55_FirFilter_Fiss_3237_3271_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3096(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		FOR(int, streamItVar619, 0,  < , 2, streamItVar619++) {
			pop_void(&SplitJoin55_FirFilter_Fiss_3237_3271_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin55_FirFilter_Fiss_3237_3271_split[2], i) * FirFilter_3096_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin55_FirFilter_Fiss_3237_3271_split[2]) ; 
		push_float(&SplitJoin55_FirFilter_Fiss_3237_3271_join[2], sum) ; 
 {
		pop_void(&SplitJoin55_FirFilter_Fiss_3237_3271_split[2]) ; 
	}
	}
	ENDFOR
}

void FirFilter_3097(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		FOR(int, streamItVar620, 0,  < , 3, streamItVar620++) {
			pop_void(&SplitJoin55_FirFilter_Fiss_3237_3271_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin55_FirFilter_Fiss_3237_3271_split[3], i) * FirFilter_3097_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin55_FirFilter_Fiss_3237_3271_split[3]) ; 
		push_float(&SplitJoin55_FirFilter_Fiss_3237_3271_join[3], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_3092() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3087DUPLICATE_Splitter_3092);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin55_FirFilter_Fiss_3237_3271_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3093() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[2], pop_float(&SplitJoin55_FirFilter_Fiss_3237_3271_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Delay_N_3100(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin70_Delay_N_Fiss_3238_3272_join[0], pop_float(&SplitJoin70_Delay_N_Fiss_3238_3272_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_3101(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin70_Delay_N_Fiss_3238_3272_join[1], pop_float(&SplitJoin70_Delay_N_Fiss_3238_3272_split[1])) ; 
	}
	ENDFOR
}

void Delay_N_3102(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin70_Delay_N_Fiss_3238_3272_join[2], pop_float(&SplitJoin70_Delay_N_Fiss_3238_3272_split[2])) ; 
	}
	ENDFOR
}

void Delay_N_3103(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin70_Delay_N_Fiss_3238_3272_join[3], pop_float(&SplitJoin70_Delay_N_Fiss_3238_3272_split[3])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3098() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin70_Delay_N_Fiss_3238_3272_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[3]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3099() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3099DUPLICATE_Splitter_3104, pop_float(&SplitJoin70_Delay_N_Fiss_3238_3272_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3106(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin72_FirFilter_Fiss_3239_3273_split[0], i) * FirFilter_3106_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin72_FirFilter_Fiss_3239_3273_split[0]) ; 
		push_float(&SplitJoin72_FirFilter_Fiss_3239_3273_join[0], sum) ; 
 {
		FOR(int, streamItVar613, 0,  < , 3, streamItVar613++) {
			pop_void(&SplitJoin72_FirFilter_Fiss_3239_3273_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3107(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin72_FirFilter_Fiss_3239_3273_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin72_FirFilter_Fiss_3239_3273_split[1], i) * FirFilter_3107_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin72_FirFilter_Fiss_3239_3273_split[1]) ; 
		push_float(&SplitJoin72_FirFilter_Fiss_3239_3273_join[1], sum) ; 
 {
		FOR(int, streamItVar614, 0,  < , 2, streamItVar614++) {
			pop_void(&SplitJoin72_FirFilter_Fiss_3239_3273_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3108(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar615, 0,  < , 2, streamItVar615++) {
			pop_void(&SplitJoin72_FirFilter_Fiss_3239_3273_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin72_FirFilter_Fiss_3239_3273_split[2], i) * FirFilter_3108_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin72_FirFilter_Fiss_3239_3273_split[2]) ; 
		push_float(&SplitJoin72_FirFilter_Fiss_3239_3273_join[2], sum) ; 
 {
		pop_void(&SplitJoin72_FirFilter_Fiss_3239_3273_split[2]) ; 
	}
	}
	ENDFOR
}

void FirFilter_3109(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar616, 0,  < , 3, streamItVar616++) {
			pop_void(&SplitJoin72_FirFilter_Fiss_3239_3273_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin72_FirFilter_Fiss_3239_3273_split[3], i) * FirFilter_3109_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin72_FirFilter_Fiss_3239_3273_split[3]) ; 
		push_float(&SplitJoin72_FirFilter_Fiss_3239_3273_join[3], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_3104() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3099DUPLICATE_Splitter_3104);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin72_FirFilter_Fiss_3239_3273_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3105() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3105DownSamp_2972, pop_float(&SplitJoin72_FirFilter_Fiss_3239_3273_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp_2972() {
	push_float(&DownSamp_2972UpSamp_2973, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3105DownSamp_2972)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3105DownSamp_2972) ; 
	}
	ENDFOR
}


void UpSamp_2973() {
	push_float(&UpSamp_2973WEIGHTED_ROUND_ROBIN_Splitter_3110, pop_float(&DownSamp_2972UpSamp_2973)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		push_float(&UpSamp_2973WEIGHTED_ROUND_ROBIN_Splitter_3110, 0.0) ; 
	}
	ENDFOR
}


void Delay_N_3112(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin74_Delay_N_Fiss_3240_3274_join[0], pop_float(&SplitJoin74_Delay_N_Fiss_3240_3274_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_3113(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin74_Delay_N_Fiss_3240_3274_join[1], pop_float(&SplitJoin74_Delay_N_Fiss_3240_3274_split[1])) ; 
	}
	ENDFOR
}

void Delay_N_3114(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin74_Delay_N_Fiss_3240_3274_join[2], pop_float(&SplitJoin74_Delay_N_Fiss_3240_3274_split[2])) ; 
	}
	ENDFOR
}

void Delay_N_3115(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin74_Delay_N_Fiss_3240_3274_join[3], pop_float(&SplitJoin74_Delay_N_Fiss_3240_3274_split[3])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3110() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin74_Delay_N_Fiss_3240_3274_split[__iter_], pop_float(&UpSamp_2973WEIGHTED_ROUND_ROBIN_Splitter_3110));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3111() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3111DUPLICATE_Splitter_3116, pop_float(&SplitJoin74_Delay_N_Fiss_3240_3274_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3118(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin76_FirFilter_Fiss_3241_3275_split[0], i) * FirFilter_3118_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin76_FirFilter_Fiss_3241_3275_split[0]) ; 
		push_float(&SplitJoin76_FirFilter_Fiss_3241_3275_join[0], sum) ; 
 {
		FOR(int, streamItVar609, 0,  < , 3, streamItVar609++) {
			pop_void(&SplitJoin76_FirFilter_Fiss_3241_3275_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3119(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		pop_void(&SplitJoin76_FirFilter_Fiss_3241_3275_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin76_FirFilter_Fiss_3241_3275_split[1], i) * FirFilter_3119_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin76_FirFilter_Fiss_3241_3275_split[1]) ; 
		push_float(&SplitJoin76_FirFilter_Fiss_3241_3275_join[1], sum) ; 
 {
		FOR(int, streamItVar610, 0,  < , 2, streamItVar610++) {
			pop_void(&SplitJoin76_FirFilter_Fiss_3241_3275_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3120(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		FOR(int, streamItVar611, 0,  < , 2, streamItVar611++) {
			pop_void(&SplitJoin76_FirFilter_Fiss_3241_3275_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin76_FirFilter_Fiss_3241_3275_split[2], i) * FirFilter_3120_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin76_FirFilter_Fiss_3241_3275_split[2]) ; 
		push_float(&SplitJoin76_FirFilter_Fiss_3241_3275_join[2], sum) ; 
 {
		pop_void(&SplitJoin76_FirFilter_Fiss_3241_3275_split[2]) ; 
	}
	}
	ENDFOR
}

void FirFilter_3121(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		FOR(int, streamItVar612, 0,  < , 3, streamItVar612++) {
			pop_void(&SplitJoin76_FirFilter_Fiss_3241_3275_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin76_FirFilter_Fiss_3241_3275_split[3], i) * FirFilter_3121_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin76_FirFilter_Fiss_3241_3275_split[3]) ; 
		push_float(&SplitJoin76_FirFilter_Fiss_3241_3275_join[3], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_3116() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3111DUPLICATE_Splitter_3116);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin76_FirFilter_Fiss_3241_3275_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3117() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[3], pop_float(&SplitJoin76_FirFilter_Fiss_3241_3275_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Delay_N_3124(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin91_Delay_N_Fiss_3242_3276_join[0], pop_float(&SplitJoin91_Delay_N_Fiss_3242_3276_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_3125(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin91_Delay_N_Fiss_3242_3276_join[1], pop_float(&SplitJoin91_Delay_N_Fiss_3242_3276_split[1])) ; 
	}
	ENDFOR
}

void Delay_N_3126(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin91_Delay_N_Fiss_3242_3276_join[2], pop_float(&SplitJoin91_Delay_N_Fiss_3242_3276_split[2])) ; 
	}
	ENDFOR
}

void Delay_N_3127(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin91_Delay_N_Fiss_3242_3276_join[3], pop_float(&SplitJoin91_Delay_N_Fiss_3242_3276_split[3])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3122() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin91_Delay_N_Fiss_3242_3276_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[4]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3123() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3123DUPLICATE_Splitter_3128, pop_float(&SplitJoin91_Delay_N_Fiss_3242_3276_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3130(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin93_FirFilter_Fiss_3243_3277_split[0], i) * FirFilter_3130_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin93_FirFilter_Fiss_3243_3277_split[0]) ; 
		push_float(&SplitJoin93_FirFilter_Fiss_3243_3277_join[0], sum) ; 
 {
		FOR(int, streamItVar605, 0,  < , 3, streamItVar605++) {
			pop_void(&SplitJoin93_FirFilter_Fiss_3243_3277_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3131(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin93_FirFilter_Fiss_3243_3277_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin93_FirFilter_Fiss_3243_3277_split[1], i) * FirFilter_3131_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin93_FirFilter_Fiss_3243_3277_split[1]) ; 
		push_float(&SplitJoin93_FirFilter_Fiss_3243_3277_join[1], sum) ; 
 {
		FOR(int, streamItVar606, 0,  < , 2, streamItVar606++) {
			pop_void(&SplitJoin93_FirFilter_Fiss_3243_3277_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3132(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar607, 0,  < , 2, streamItVar607++) {
			pop_void(&SplitJoin93_FirFilter_Fiss_3243_3277_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin93_FirFilter_Fiss_3243_3277_split[2], i) * FirFilter_3132_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin93_FirFilter_Fiss_3243_3277_split[2]) ; 
		push_float(&SplitJoin93_FirFilter_Fiss_3243_3277_join[2], sum) ; 
 {
		pop_void(&SplitJoin93_FirFilter_Fiss_3243_3277_split[2]) ; 
	}
	}
	ENDFOR
}

void FirFilter_3133(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar608, 0,  < , 3, streamItVar608++) {
			pop_void(&SplitJoin93_FirFilter_Fiss_3243_3277_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin93_FirFilter_Fiss_3243_3277_split[3], i) * FirFilter_3133_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin93_FirFilter_Fiss_3243_3277_split[3]) ; 
		push_float(&SplitJoin93_FirFilter_Fiss_3243_3277_join[3], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_3128() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3123DUPLICATE_Splitter_3128);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin93_FirFilter_Fiss_3243_3277_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3129() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3129DownSamp_2979, pop_float(&SplitJoin93_FirFilter_Fiss_3243_3277_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp_2979() {
	push_float(&DownSamp_2979UpSamp_2980, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3129DownSamp_2979)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3129DownSamp_2979) ; 
	}
	ENDFOR
}


void UpSamp_2980() {
	push_float(&UpSamp_2980WEIGHTED_ROUND_ROBIN_Splitter_3134, pop_float(&DownSamp_2979UpSamp_2980)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		push_float(&UpSamp_2980WEIGHTED_ROUND_ROBIN_Splitter_3134, 0.0) ; 
	}
	ENDFOR
}


void Delay_N_3136(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin95_Delay_N_Fiss_3244_3278_join[0], pop_float(&SplitJoin95_Delay_N_Fiss_3244_3278_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_3137(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin95_Delay_N_Fiss_3244_3278_join[1], pop_float(&SplitJoin95_Delay_N_Fiss_3244_3278_split[1])) ; 
	}
	ENDFOR
}

void Delay_N_3138(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin95_Delay_N_Fiss_3244_3278_join[2], pop_float(&SplitJoin95_Delay_N_Fiss_3244_3278_split[2])) ; 
	}
	ENDFOR
}

void Delay_N_3139(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin95_Delay_N_Fiss_3244_3278_join[3], pop_float(&SplitJoin95_Delay_N_Fiss_3244_3278_split[3])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3134() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin95_Delay_N_Fiss_3244_3278_split[__iter_], pop_float(&UpSamp_2980WEIGHTED_ROUND_ROBIN_Splitter_3134));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3135() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3135DUPLICATE_Splitter_3140, pop_float(&SplitJoin95_Delay_N_Fiss_3244_3278_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3142(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin97_FirFilter_Fiss_3245_3279_split[0], i) * FirFilter_3142_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin97_FirFilter_Fiss_3245_3279_split[0]) ; 
		push_float(&SplitJoin97_FirFilter_Fiss_3245_3279_join[0], sum) ; 
 {
		FOR(int, streamItVar601, 0,  < , 3, streamItVar601++) {
			pop_void(&SplitJoin97_FirFilter_Fiss_3245_3279_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3143(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		pop_void(&SplitJoin97_FirFilter_Fiss_3245_3279_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin97_FirFilter_Fiss_3245_3279_split[1], i) * FirFilter_3143_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin97_FirFilter_Fiss_3245_3279_split[1]) ; 
		push_float(&SplitJoin97_FirFilter_Fiss_3245_3279_join[1], sum) ; 
 {
		FOR(int, streamItVar602, 0,  < , 2, streamItVar602++) {
			pop_void(&SplitJoin97_FirFilter_Fiss_3245_3279_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3144(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		FOR(int, streamItVar603, 0,  < , 2, streamItVar603++) {
			pop_void(&SplitJoin97_FirFilter_Fiss_3245_3279_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin97_FirFilter_Fiss_3245_3279_split[2], i) * FirFilter_3144_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin97_FirFilter_Fiss_3245_3279_split[2]) ; 
		push_float(&SplitJoin97_FirFilter_Fiss_3245_3279_join[2], sum) ; 
 {
		pop_void(&SplitJoin97_FirFilter_Fiss_3245_3279_split[2]) ; 
	}
	}
	ENDFOR
}

void FirFilter_3145(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		FOR(int, streamItVar604, 0,  < , 3, streamItVar604++) {
			pop_void(&SplitJoin97_FirFilter_Fiss_3245_3279_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin97_FirFilter_Fiss_3245_3279_split[3], i) * FirFilter_3145_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin97_FirFilter_Fiss_3245_3279_split[3]) ; 
		push_float(&SplitJoin97_FirFilter_Fiss_3245_3279_join[3], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_3140() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3135DUPLICATE_Splitter_3140);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin97_FirFilter_Fiss_3245_3279_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3141() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[4], pop_float(&SplitJoin97_FirFilter_Fiss_3245_3279_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Delay_N_3148(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin112_Delay_N_Fiss_3246_3280_join[0], pop_float(&SplitJoin112_Delay_N_Fiss_3246_3280_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_3149(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin112_Delay_N_Fiss_3246_3280_join[1], pop_float(&SplitJoin112_Delay_N_Fiss_3246_3280_split[1])) ; 
	}
	ENDFOR
}

void Delay_N_3150(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin112_Delay_N_Fiss_3246_3280_join[2], pop_float(&SplitJoin112_Delay_N_Fiss_3246_3280_split[2])) ; 
	}
	ENDFOR
}

void Delay_N_3151(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin112_Delay_N_Fiss_3246_3280_join[3], pop_float(&SplitJoin112_Delay_N_Fiss_3246_3280_split[3])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3146() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin112_Delay_N_Fiss_3246_3280_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[5]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3147() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3147DUPLICATE_Splitter_3152, pop_float(&SplitJoin112_Delay_N_Fiss_3246_3280_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3154(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin114_FirFilter_Fiss_3247_3281_split[0], i) * FirFilter_3154_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin114_FirFilter_Fiss_3247_3281_split[0]) ; 
		push_float(&SplitJoin114_FirFilter_Fiss_3247_3281_join[0], sum) ; 
 {
		FOR(int, streamItVar597, 0,  < , 3, streamItVar597++) {
			pop_void(&SplitJoin114_FirFilter_Fiss_3247_3281_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3155(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin114_FirFilter_Fiss_3247_3281_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin114_FirFilter_Fiss_3247_3281_split[1], i) * FirFilter_3155_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin114_FirFilter_Fiss_3247_3281_split[1]) ; 
		push_float(&SplitJoin114_FirFilter_Fiss_3247_3281_join[1], sum) ; 
 {
		FOR(int, streamItVar598, 0,  < , 2, streamItVar598++) {
			pop_void(&SplitJoin114_FirFilter_Fiss_3247_3281_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3156(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar599, 0,  < , 2, streamItVar599++) {
			pop_void(&SplitJoin114_FirFilter_Fiss_3247_3281_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin114_FirFilter_Fiss_3247_3281_split[2], i) * FirFilter_3156_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin114_FirFilter_Fiss_3247_3281_split[2]) ; 
		push_float(&SplitJoin114_FirFilter_Fiss_3247_3281_join[2], sum) ; 
 {
		pop_void(&SplitJoin114_FirFilter_Fiss_3247_3281_split[2]) ; 
	}
	}
	ENDFOR
}

void FirFilter_3157(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar600, 0,  < , 3, streamItVar600++) {
			pop_void(&SplitJoin114_FirFilter_Fiss_3247_3281_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin114_FirFilter_Fiss_3247_3281_split[3], i) * FirFilter_3157_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin114_FirFilter_Fiss_3247_3281_split[3]) ; 
		push_float(&SplitJoin114_FirFilter_Fiss_3247_3281_join[3], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_3152() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3147DUPLICATE_Splitter_3152);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin114_FirFilter_Fiss_3247_3281_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3153() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3153DownSamp_2986, pop_float(&SplitJoin114_FirFilter_Fiss_3247_3281_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp_2986() {
	push_float(&DownSamp_2986UpSamp_2987, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3153DownSamp_2986)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3153DownSamp_2986) ; 
	}
	ENDFOR
}


void UpSamp_2987() {
	push_float(&UpSamp_2987WEIGHTED_ROUND_ROBIN_Splitter_3158, pop_float(&DownSamp_2986UpSamp_2987)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		push_float(&UpSamp_2987WEIGHTED_ROUND_ROBIN_Splitter_3158, 0.0) ; 
	}
	ENDFOR
}


void Delay_N_3160(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin116_Delay_N_Fiss_3248_3282_join[0], pop_float(&SplitJoin116_Delay_N_Fiss_3248_3282_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_3161(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin116_Delay_N_Fiss_3248_3282_join[1], pop_float(&SplitJoin116_Delay_N_Fiss_3248_3282_split[1])) ; 
	}
	ENDFOR
}

void Delay_N_3162(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin116_Delay_N_Fiss_3248_3282_join[2], pop_float(&SplitJoin116_Delay_N_Fiss_3248_3282_split[2])) ; 
	}
	ENDFOR
}

void Delay_N_3163(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin116_Delay_N_Fiss_3248_3282_join[3], pop_float(&SplitJoin116_Delay_N_Fiss_3248_3282_split[3])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3158() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin116_Delay_N_Fiss_3248_3282_split[__iter_], pop_float(&UpSamp_2987WEIGHTED_ROUND_ROBIN_Splitter_3158));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3159() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3159DUPLICATE_Splitter_3164, pop_float(&SplitJoin116_Delay_N_Fiss_3248_3282_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3166(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin118_FirFilter_Fiss_3249_3283_split[0], i) * FirFilter_3166_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin118_FirFilter_Fiss_3249_3283_split[0]) ; 
		push_float(&SplitJoin118_FirFilter_Fiss_3249_3283_join[0], sum) ; 
 {
		FOR(int, streamItVar593, 0,  < , 3, streamItVar593++) {
			pop_void(&SplitJoin118_FirFilter_Fiss_3249_3283_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3167(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		pop_void(&SplitJoin118_FirFilter_Fiss_3249_3283_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin118_FirFilter_Fiss_3249_3283_split[1], i) * FirFilter_3167_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin118_FirFilter_Fiss_3249_3283_split[1]) ; 
		push_float(&SplitJoin118_FirFilter_Fiss_3249_3283_join[1], sum) ; 
 {
		FOR(int, streamItVar594, 0,  < , 2, streamItVar594++) {
			pop_void(&SplitJoin118_FirFilter_Fiss_3249_3283_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3168(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		FOR(int, streamItVar595, 0,  < , 2, streamItVar595++) {
			pop_void(&SplitJoin118_FirFilter_Fiss_3249_3283_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin118_FirFilter_Fiss_3249_3283_split[2], i) * FirFilter_3168_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin118_FirFilter_Fiss_3249_3283_split[2]) ; 
		push_float(&SplitJoin118_FirFilter_Fiss_3249_3283_join[2], sum) ; 
 {
		pop_void(&SplitJoin118_FirFilter_Fiss_3249_3283_split[2]) ; 
	}
	}
	ENDFOR
}

void FirFilter_3169(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		FOR(int, streamItVar596, 0,  < , 3, streamItVar596++) {
			pop_void(&SplitJoin118_FirFilter_Fiss_3249_3283_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin118_FirFilter_Fiss_3249_3283_split[3], i) * FirFilter_3169_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin118_FirFilter_Fiss_3249_3283_split[3]) ; 
		push_float(&SplitJoin118_FirFilter_Fiss_3249_3283_join[3], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_3164() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3159DUPLICATE_Splitter_3164);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin118_FirFilter_Fiss_3249_3283_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3165() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[5], pop_float(&SplitJoin118_FirFilter_Fiss_3249_3283_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Delay_N_3172(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin133_Delay_N_Fiss_3250_3284_join[0], pop_float(&SplitJoin133_Delay_N_Fiss_3250_3284_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_3173(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin133_Delay_N_Fiss_3250_3284_join[1], pop_float(&SplitJoin133_Delay_N_Fiss_3250_3284_split[1])) ; 
	}
	ENDFOR
}

void Delay_N_3174(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin133_Delay_N_Fiss_3250_3284_join[2], pop_float(&SplitJoin133_Delay_N_Fiss_3250_3284_split[2])) ; 
	}
	ENDFOR
}

void Delay_N_3175(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin133_Delay_N_Fiss_3250_3284_join[3], pop_float(&SplitJoin133_Delay_N_Fiss_3250_3284_split[3])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3170() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin133_Delay_N_Fiss_3250_3284_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[6]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3171() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3171DUPLICATE_Splitter_3176, pop_float(&SplitJoin133_Delay_N_Fiss_3250_3284_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3178(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin135_FirFilter_Fiss_3251_3285_split[0], i) * FirFilter_3178_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin135_FirFilter_Fiss_3251_3285_split[0]) ; 
		push_float(&SplitJoin135_FirFilter_Fiss_3251_3285_join[0], sum) ; 
 {
		FOR(int, streamItVar589, 0,  < , 3, streamItVar589++) {
			pop_void(&SplitJoin135_FirFilter_Fiss_3251_3285_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3179(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin135_FirFilter_Fiss_3251_3285_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin135_FirFilter_Fiss_3251_3285_split[1], i) * FirFilter_3179_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin135_FirFilter_Fiss_3251_3285_split[1]) ; 
		push_float(&SplitJoin135_FirFilter_Fiss_3251_3285_join[1], sum) ; 
 {
		FOR(int, streamItVar590, 0,  < , 2, streamItVar590++) {
			pop_void(&SplitJoin135_FirFilter_Fiss_3251_3285_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3180(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar591, 0,  < , 2, streamItVar591++) {
			pop_void(&SplitJoin135_FirFilter_Fiss_3251_3285_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin135_FirFilter_Fiss_3251_3285_split[2], i) * FirFilter_3180_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin135_FirFilter_Fiss_3251_3285_split[2]) ; 
		push_float(&SplitJoin135_FirFilter_Fiss_3251_3285_join[2], sum) ; 
 {
		pop_void(&SplitJoin135_FirFilter_Fiss_3251_3285_split[2]) ; 
	}
	}
	ENDFOR
}

void FirFilter_3181(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar592, 0,  < , 3, streamItVar592++) {
			pop_void(&SplitJoin135_FirFilter_Fiss_3251_3285_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin135_FirFilter_Fiss_3251_3285_split[3], i) * FirFilter_3181_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin135_FirFilter_Fiss_3251_3285_split[3]) ; 
		push_float(&SplitJoin135_FirFilter_Fiss_3251_3285_join[3], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_3176() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3171DUPLICATE_Splitter_3176);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin135_FirFilter_Fiss_3251_3285_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3177() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3177DownSamp_2993, pop_float(&SplitJoin135_FirFilter_Fiss_3251_3285_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp_2993() {
	push_float(&DownSamp_2993UpSamp_2994, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3177DownSamp_2993)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3177DownSamp_2993) ; 
	}
	ENDFOR
}


void UpSamp_2994() {
	push_float(&UpSamp_2994WEIGHTED_ROUND_ROBIN_Splitter_3182, pop_float(&DownSamp_2993UpSamp_2994)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		push_float(&UpSamp_2994WEIGHTED_ROUND_ROBIN_Splitter_3182, 0.0) ; 
	}
	ENDFOR
}


void Delay_N_3184(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin137_Delay_N_Fiss_3252_3286_join[0], pop_float(&SplitJoin137_Delay_N_Fiss_3252_3286_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_3185(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin137_Delay_N_Fiss_3252_3286_join[1], pop_float(&SplitJoin137_Delay_N_Fiss_3252_3286_split[1])) ; 
	}
	ENDFOR
}

void Delay_N_3186(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin137_Delay_N_Fiss_3252_3286_join[2], pop_float(&SplitJoin137_Delay_N_Fiss_3252_3286_split[2])) ; 
	}
	ENDFOR
}

void Delay_N_3187(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin137_Delay_N_Fiss_3252_3286_join[3], pop_float(&SplitJoin137_Delay_N_Fiss_3252_3286_split[3])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3182() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin137_Delay_N_Fiss_3252_3286_split[__iter_], pop_float(&UpSamp_2994WEIGHTED_ROUND_ROBIN_Splitter_3182));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3183() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3183DUPLICATE_Splitter_3188, pop_float(&SplitJoin137_Delay_N_Fiss_3252_3286_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3190(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin139_FirFilter_Fiss_3253_3287_split[0], i) * FirFilter_3190_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin139_FirFilter_Fiss_3253_3287_split[0]) ; 
		push_float(&SplitJoin139_FirFilter_Fiss_3253_3287_join[0], sum) ; 
 {
		FOR(int, streamItVar585, 0,  < , 3, streamItVar585++) {
			pop_void(&SplitJoin139_FirFilter_Fiss_3253_3287_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3191(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		pop_void(&SplitJoin139_FirFilter_Fiss_3253_3287_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin139_FirFilter_Fiss_3253_3287_split[1], i) * FirFilter_3191_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin139_FirFilter_Fiss_3253_3287_split[1]) ; 
		push_float(&SplitJoin139_FirFilter_Fiss_3253_3287_join[1], sum) ; 
 {
		FOR(int, streamItVar586, 0,  < , 2, streamItVar586++) {
			pop_void(&SplitJoin139_FirFilter_Fiss_3253_3287_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3192(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		FOR(int, streamItVar587, 0,  < , 2, streamItVar587++) {
			pop_void(&SplitJoin139_FirFilter_Fiss_3253_3287_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin139_FirFilter_Fiss_3253_3287_split[2], i) * FirFilter_3192_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin139_FirFilter_Fiss_3253_3287_split[2]) ; 
		push_float(&SplitJoin139_FirFilter_Fiss_3253_3287_join[2], sum) ; 
 {
		pop_void(&SplitJoin139_FirFilter_Fiss_3253_3287_split[2]) ; 
	}
	}
	ENDFOR
}

void FirFilter_3193(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		FOR(int, streamItVar588, 0,  < , 3, streamItVar588++) {
			pop_void(&SplitJoin139_FirFilter_Fiss_3253_3287_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin139_FirFilter_Fiss_3253_3287_split[3], i) * FirFilter_3193_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin139_FirFilter_Fiss_3253_3287_split[3]) ; 
		push_float(&SplitJoin139_FirFilter_Fiss_3253_3287_join[3], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_3188() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3183DUPLICATE_Splitter_3188);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin139_FirFilter_Fiss_3253_3287_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3189() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[6], pop_float(&SplitJoin139_FirFilter_Fiss_3253_3287_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Delay_N_3196(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin154_Delay_N_Fiss_3254_3288_join[0], pop_float(&SplitJoin154_Delay_N_Fiss_3254_3288_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_3197(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin154_Delay_N_Fiss_3254_3288_join[1], pop_float(&SplitJoin154_Delay_N_Fiss_3254_3288_split[1])) ; 
	}
	ENDFOR
}

void Delay_N_3198(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin154_Delay_N_Fiss_3254_3288_join[2], pop_float(&SplitJoin154_Delay_N_Fiss_3254_3288_split[2])) ; 
	}
	ENDFOR
}

void Delay_N_3199(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin154_Delay_N_Fiss_3254_3288_join[3], pop_float(&SplitJoin154_Delay_N_Fiss_3254_3288_split[3])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3194() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin154_Delay_N_Fiss_3254_3288_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[7]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3195() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3195DUPLICATE_Splitter_3200, pop_float(&SplitJoin154_Delay_N_Fiss_3254_3288_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3202(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin156_FirFilter_Fiss_3255_3289_split[0], i) * FirFilter_3202_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin156_FirFilter_Fiss_3255_3289_split[0]) ; 
		push_float(&SplitJoin156_FirFilter_Fiss_3255_3289_join[0], sum) ; 
 {
		FOR(int, streamItVar581, 0,  < , 3, streamItVar581++) {
			pop_void(&SplitJoin156_FirFilter_Fiss_3255_3289_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3203(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin156_FirFilter_Fiss_3255_3289_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin156_FirFilter_Fiss_3255_3289_split[1], i) * FirFilter_3203_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin156_FirFilter_Fiss_3255_3289_split[1]) ; 
		push_float(&SplitJoin156_FirFilter_Fiss_3255_3289_join[1], sum) ; 
 {
		FOR(int, streamItVar582, 0,  < , 2, streamItVar582++) {
			pop_void(&SplitJoin156_FirFilter_Fiss_3255_3289_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3204(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar583, 0,  < , 2, streamItVar583++) {
			pop_void(&SplitJoin156_FirFilter_Fiss_3255_3289_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin156_FirFilter_Fiss_3255_3289_split[2], i) * FirFilter_3204_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin156_FirFilter_Fiss_3255_3289_split[2]) ; 
		push_float(&SplitJoin156_FirFilter_Fiss_3255_3289_join[2], sum) ; 
 {
		pop_void(&SplitJoin156_FirFilter_Fiss_3255_3289_split[2]) ; 
	}
	}
	ENDFOR
}

void FirFilter_3205(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar584, 0,  < , 3, streamItVar584++) {
			pop_void(&SplitJoin156_FirFilter_Fiss_3255_3289_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin156_FirFilter_Fiss_3255_3289_split[3], i) * FirFilter_3205_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin156_FirFilter_Fiss_3255_3289_split[3]) ; 
		push_float(&SplitJoin156_FirFilter_Fiss_3255_3289_join[3], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_3200() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3195DUPLICATE_Splitter_3200);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin156_FirFilter_Fiss_3255_3289_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3201() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3201DownSamp_3000, pop_float(&SplitJoin156_FirFilter_Fiss_3255_3289_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DownSamp_3000() {
	push_float(&DownSamp_3000UpSamp_3001, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3201DownSamp_3000)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3201DownSamp_3000) ; 
	}
	ENDFOR
}


void UpSamp_3001() {
	push_float(&UpSamp_3001WEIGHTED_ROUND_ROBIN_Splitter_3206, pop_float(&DownSamp_3000UpSamp_3001)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		push_float(&UpSamp_3001WEIGHTED_ROUND_ROBIN_Splitter_3206, 0.0) ; 
	}
	ENDFOR
}


void Delay_N_3208(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin158_Delay_N_Fiss_3256_3290_join[0], pop_float(&SplitJoin158_Delay_N_Fiss_3256_3290_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_3209(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin158_Delay_N_Fiss_3256_3290_join[1], pop_float(&SplitJoin158_Delay_N_Fiss_3256_3290_split[1])) ; 
	}
	ENDFOR
}

void Delay_N_3210(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin158_Delay_N_Fiss_3256_3290_join[2], pop_float(&SplitJoin158_Delay_N_Fiss_3256_3290_split[2])) ; 
	}
	ENDFOR
}

void Delay_N_3211(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		push_float(&SplitJoin158_Delay_N_Fiss_3256_3290_join[3], pop_float(&SplitJoin158_Delay_N_Fiss_3256_3290_split[3])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3206() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin158_Delay_N_Fiss_3256_3290_split[__iter_], pop_float(&UpSamp_3001WEIGHTED_ROUND_ROBIN_Splitter_3206));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3207() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3207DUPLICATE_Splitter_3212, pop_float(&SplitJoin158_Delay_N_Fiss_3256_3290_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void FirFilter_3214(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin160_FirFilter_Fiss_3257_3291_split[0], i) * FirFilter_3214_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin160_FirFilter_Fiss_3257_3291_split[0]) ; 
		push_float(&SplitJoin160_FirFilter_Fiss_3257_3291_join[0], sum) ; 
 {
		FOR(int, streamItVar577, 0,  < , 3, streamItVar577++) {
			pop_void(&SplitJoin160_FirFilter_Fiss_3257_3291_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3215(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		pop_void(&SplitJoin160_FirFilter_Fiss_3257_3291_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin160_FirFilter_Fiss_3257_3291_split[1], i) * FirFilter_3215_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin160_FirFilter_Fiss_3257_3291_split[1]) ; 
		push_float(&SplitJoin160_FirFilter_Fiss_3257_3291_join[1], sum) ; 
 {
		FOR(int, streamItVar578, 0,  < , 2, streamItVar578++) {
			pop_void(&SplitJoin160_FirFilter_Fiss_3257_3291_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
}

void FirFilter_3216(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		FOR(int, streamItVar579, 0,  < , 2, streamItVar579++) {
			pop_void(&SplitJoin160_FirFilter_Fiss_3257_3291_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin160_FirFilter_Fiss_3257_3291_split[2], i) * FirFilter_3216_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin160_FirFilter_Fiss_3257_3291_split[2]) ; 
		push_float(&SplitJoin160_FirFilter_Fiss_3257_3291_join[2], sum) ; 
 {
		pop_void(&SplitJoin160_FirFilter_Fiss_3257_3291_split[2]) ; 
	}
	}
	ENDFOR
}

void FirFilter_3217(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		FOR(int, streamItVar580, 0,  < , 3, streamItVar580++) {
			pop_void(&SplitJoin160_FirFilter_Fiss_3257_3291_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin160_FirFilter_Fiss_3257_3291_split[3], i) * FirFilter_3217_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin160_FirFilter_Fiss_3257_3291_split[3]) ; 
		push_float(&SplitJoin160_FirFilter_Fiss_3257_3291_join[3], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_3212() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3207DUPLICATE_Splitter_3212);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin160_FirFilter_Fiss_3257_3291_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3213() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[7], pop_float(&SplitJoin160_FirFilter_Fiss_3257_3291_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void DUPLICATE_Splitter_3006() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&source_2946DUPLICATE_Splitter_3006);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3007() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3007WEIGHTED_ROUND_ROBIN_Splitter_3218, pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Combine_3220(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		FOR(int, i, 0,  < , 8, i++) {
			sum = (sum + pop_float(&SplitJoin10_Combine_Fiss_3229_3292_split[0])) ; 
		}
		ENDFOR
		push_float(&SplitJoin10_Combine_Fiss_3229_3292_join[0], sum) ; 
	}
	ENDFOR
}

void Combine_3221(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		FOR(int, i, 0,  < , 8, i++) {
			sum = (sum + pop_float(&SplitJoin10_Combine_Fiss_3229_3292_split[1])) ; 
		}
		ENDFOR
		push_float(&SplitJoin10_Combine_Fiss_3229_3292_join[1], sum) ; 
	}
	ENDFOR
}

void Combine_3222(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		FOR(int, i, 0,  < , 8, i++) {
			sum = (sum + pop_float(&SplitJoin10_Combine_Fiss_3229_3292_split[2])) ; 
		}
		ENDFOR
		push_float(&SplitJoin10_Combine_Fiss_3229_3292_join[2], sum) ; 
	}
	ENDFOR
}

void Combine_3223(){
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++) {
		float sum = 0.0;
		FOR(int, i, 0,  < , 8, i++) {
			sum = (sum + pop_float(&SplitJoin10_Combine_Fiss_3229_3292_split[3])) ; 
		}
		ENDFOR
		push_float(&SplitJoin10_Combine_Fiss_3229_3292_join[3], sum) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3218() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin10_Combine_Fiss_3229_3292_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3007WEIGHTED_ROUND_ROBIN_Splitter_3218));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3219() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3219sink_3005, pop_float(&SplitJoin10_Combine_Fiss_3229_3292_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void sink_3005(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		printf("%.10f", pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3219sink_3005));
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3207DUPLICATE_Splitter_3212);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3063DUPLICATE_Splitter_3068);
	FOR(int, __iter_init_0_, 0, <, 4, __iter_init_0_++)
		init_buffer_float(&SplitJoin139_FirFilter_Fiss_3253_3287_split[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&DownSamp_2951UpSamp_2952);
	FOR(int, __iter_init_1_, 0, <, 4, __iter_init_1_++)
		init_buffer_float(&SplitJoin4_FirFilter_Fiss_3226_3261_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 4, __iter_init_2_++)
		init_buffer_float(&SplitJoin55_FirFilter_Fiss_3237_3271_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 4, __iter_init_3_++)
		init_buffer_float(&SplitJoin6_Delay_N_Fiss_3227_3262_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 4, __iter_init_4_++)
		init_buffer_float(&SplitJoin51_FirFilter_Fiss_3235_3269_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 4, __iter_init_5_++)
		init_buffer_float(&SplitJoin74_Delay_N_Fiss_3240_3274_join[__iter_init_5_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3129DownSamp_2979);
	init_buffer_float(&UpSamp_2994WEIGHTED_ROUND_ROBIN_Splitter_3182);
	FOR(int, __iter_init_6_, 0, <, 4, __iter_init_6_++)
		init_buffer_float(&SplitJoin154_Delay_N_Fiss_3254_3288_join[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 4, __iter_init_7_++)
		init_buffer_float(&SplitJoin72_FirFilter_Fiss_3239_3273_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 4, __iter_init_8_++)
		init_buffer_float(&SplitJoin76_FirFilter_Fiss_3241_3275_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 4, __iter_init_9_++)
		init_buffer_float(&SplitJoin28_Delay_N_Fiss_3230_3264_split[__iter_init_9_]);
	ENDFOR
	init_buffer_float(&DownSamp_3000UpSamp_3001);
	FOR(int, __iter_init_10_, 0, <, 4, __iter_init_10_++)
		init_buffer_float(&SplitJoin93_FirFilter_Fiss_3243_3277_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 4, __iter_init_11_++)
		init_buffer_float(&SplitJoin160_FirFilter_Fiss_3257_3291_split[__iter_init_11_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3081DownSamp_2965);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3183DUPLICATE_Splitter_3188);
	FOR(int, __iter_init_12_, 0, <, 4, __iter_init_12_++)
		init_buffer_float(&SplitJoin8_FirFilter_Fiss_3228_3263_join[__iter_init_12_]);
	ENDFOR
	init_buffer_float(&source_2946DUPLICATE_Splitter_3006);
	FOR(int, __iter_init_13_, 0, <, 4, __iter_init_13_++)
		init_buffer_float(&SplitJoin10_Combine_Fiss_3229_3292_join[__iter_init_13_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3027DUPLICATE_Splitter_3032);
	init_buffer_float(&DownSamp_2993UpSamp_2994);
	FOR(int, __iter_init_14_, 0, <, 4, __iter_init_14_++)
		init_buffer_float(&SplitJoin74_Delay_N_Fiss_3240_3274_split[__iter_init_14_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3105DownSamp_2972);
	init_buffer_float(&UpSamp_2973WEIGHTED_ROUND_ROBIN_Splitter_3110);
	FOR(int, __iter_init_15_, 0, <, 4, __iter_init_15_++)
		init_buffer_float(&SplitJoin70_Delay_N_Fiss_3238_3272_split[__iter_init_15_]);
	ENDFOR
	FOR(int, __iter_init_16_, 0, <, 4, __iter_init_16_++)
		init_buffer_float(&SplitJoin95_Delay_N_Fiss_3244_3278_join[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 4, __iter_init_17_++)
		init_buffer_float(&SplitJoin156_FirFilter_Fiss_3255_3289_join[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 4, __iter_init_18_++)
		init_buffer_float(&SplitJoin112_Delay_N_Fiss_3246_3280_split[__iter_init_18_]);
	ENDFOR
	FOR(int, __iter_init_19_, 0, <, 4, __iter_init_19_++)
		init_buffer_float(&SplitJoin53_Delay_N_Fiss_3236_3270_join[__iter_init_19_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3075DUPLICATE_Splitter_3080);
	init_buffer_float(&UpSamp_2987WEIGHTED_ROUND_ROBIN_Splitter_3158);
	FOR(int, __iter_init_20_, 0, <, 4, __iter_init_20_++)
		init_buffer_float(&SplitJoin49_Delay_N_Fiss_3234_3268_join[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 4, __iter_init_21_++)
		init_buffer_float(&SplitJoin49_Delay_N_Fiss_3234_3268_split[__iter_init_21_]);
	ENDFOR
	FOR(int, __iter_init_22_, 0, <, 4, __iter_init_22_++)
		init_buffer_float(&SplitJoin158_Delay_N_Fiss_3256_3290_split[__iter_init_22_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3195DUPLICATE_Splitter_3200);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3219sink_3005);
	FOR(int, __iter_init_23_, 0, <, 4, __iter_init_23_++)
		init_buffer_float(&SplitJoin2_Delay_N_Fiss_3225_3260_join[__iter_init_23_]);
	ENDFOR
	init_buffer_float(&DownSamp_2986UpSamp_2987);
	FOR(int, __iter_init_24_, 0, <, 4, __iter_init_24_++)
		init_buffer_float(&SplitJoin30_FirFilter_Fiss_3231_3265_join[__iter_init_24_]);
	ENDFOR
	FOR(int, __iter_init_25_, 0, <, 4, __iter_init_25_++)
		init_buffer_float(&SplitJoin32_Delay_N_Fiss_3232_3266_join[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 4, __iter_init_26_++)
		init_buffer_float(&SplitJoin53_Delay_N_Fiss_3236_3270_split[__iter_init_26_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3171DUPLICATE_Splitter_3176);
	FOR(int, __iter_init_27_, 0, <, 4, __iter_init_27_++)
		init_buffer_float(&SplitJoin91_Delay_N_Fiss_3242_3276_join[__iter_init_27_]);
	ENDFOR
	init_buffer_float(&UpSamp_3001WEIGHTED_ROUND_ROBIN_Splitter_3206);
	FOR(int, __iter_init_28_, 0, <, 4, __iter_init_28_++)
		init_buffer_float(&SplitJoin10_Combine_Fiss_3229_3292_split[__iter_init_28_]);
	ENDFOR
	init_buffer_float(&UpSamp_2952WEIGHTED_ROUND_ROBIN_Splitter_3038);
	FOR(int, __iter_init_29_, 0, <, 4, __iter_init_29_++)
		init_buffer_float(&SplitJoin6_Delay_N_Fiss_3227_3262_join[__iter_init_29_]);
	ENDFOR
	FOR(int, __iter_init_30_, 0, <, 4, __iter_init_30_++)
		init_buffer_float(&SplitJoin135_FirFilter_Fiss_3251_3285_join[__iter_init_30_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3201DownSamp_3000);
	FOR(int, __iter_init_31_, 0, <, 4, __iter_init_31_++)
		init_buffer_float(&SplitJoin8_FirFilter_Fiss_3228_3263_split[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 4, __iter_init_32_++)
		init_buffer_float(&SplitJoin30_FirFilter_Fiss_3231_3265_split[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 4, __iter_init_33_++)
		init_buffer_float(&SplitJoin2_Delay_N_Fiss_3225_3260_split[__iter_init_33_]);
	ENDFOR
	init_buffer_float(&DownSamp_2979UpSamp_2980);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3039DUPLICATE_Splitter_3044);
	FOR(int, __iter_init_34_, 0, <, 4, __iter_init_34_++)
		init_buffer_float(&SplitJoin154_Delay_N_Fiss_3254_3288_split[__iter_init_34_]);
	ENDFOR
	FOR(int, __iter_init_35_, 0, <, 4, __iter_init_35_++)
		init_buffer_float(&SplitJoin112_Delay_N_Fiss_3246_3280_join[__iter_init_35_]);
	ENDFOR
	FOR(int, __iter_init_36_, 0, <, 4, __iter_init_36_++)
		init_buffer_float(&SplitJoin158_Delay_N_Fiss_3256_3290_join[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 4, __iter_init_37_++)
		init_buffer_float(&SplitJoin97_FirFilter_Fiss_3245_3279_split[__iter_init_37_]);
	ENDFOR
	FOR(int, __iter_init_38_, 0, <, 4, __iter_init_38_++)
		init_buffer_float(&SplitJoin114_FirFilter_Fiss_3247_3281_join[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 4, __iter_init_39_++)
		init_buffer_float(&SplitJoin118_FirFilter_Fiss_3249_3283_join[__iter_init_39_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3087DUPLICATE_Splitter_3092);
	FOR(int, __iter_init_40_, 0, <, 4, __iter_init_40_++)
		init_buffer_float(&SplitJoin4_FirFilter_Fiss_3226_3261_split[__iter_init_40_]);
	ENDFOR
	FOR(int, __iter_init_41_, 0, <, 4, __iter_init_41_++)
		init_buffer_float(&SplitJoin137_Delay_N_Fiss_3252_3286_split[__iter_init_41_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3159DUPLICATE_Splitter_3164);
	FOR(int, __iter_init_42_, 0, <, 8, __iter_init_42_++)
		init_buffer_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[__iter_init_42_]);
	ENDFOR
	FOR(int, __iter_init_43_, 0, <, 4, __iter_init_43_++)
		init_buffer_float(&SplitJoin70_Delay_N_Fiss_3238_3272_join[__iter_init_43_]);
	ENDFOR
	FOR(int, __iter_init_44_, 0, <, 4, __iter_init_44_++)
		init_buffer_float(&SplitJoin118_FirFilter_Fiss_3249_3283_split[__iter_init_44_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3099DUPLICATE_Splitter_3104);
	FOR(int, __iter_init_45_, 0, <, 4, __iter_init_45_++)
		init_buffer_float(&SplitJoin160_FirFilter_Fiss_3257_3291_join[__iter_init_45_]);
	ENDFOR
	FOR(int, __iter_init_46_, 0, <, 4, __iter_init_46_++)
		init_buffer_float(&SplitJoin32_Delay_N_Fiss_3232_3266_split[__iter_init_46_]);
	ENDFOR
	FOR(int, __iter_init_47_, 0, <, 4, __iter_init_47_++)
		init_buffer_float(&SplitJoin139_FirFilter_Fiss_3253_3287_join[__iter_init_47_]);
	ENDFOR
	init_buffer_float(&UpSamp_2980WEIGHTED_ROUND_ROBIN_Splitter_3134);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3177DownSamp_2993);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3051DUPLICATE_Splitter_3056);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3007WEIGHTED_ROUND_ROBIN_Splitter_3218);
	FOR(int, __iter_init_48_, 0, <, 4, __iter_init_48_++)
		init_buffer_float(&SplitJoin95_Delay_N_Fiss_3244_3278_split[__iter_init_48_]);
	ENDFOR
	FOR(int, __iter_init_49_, 0, <, 4, __iter_init_49_++)
		init_buffer_float(&SplitJoin156_FirFilter_Fiss_3255_3289_split[__iter_init_49_]);
	ENDFOR
	FOR(int, __iter_init_50_, 0, <, 4, __iter_init_50_++)
		init_buffer_float(&SplitJoin34_FirFilter_Fiss_3233_3267_split[__iter_init_50_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3111DUPLICATE_Splitter_3116);
	FOR(int, __iter_init_51_, 0, <, 4, __iter_init_51_++)
		init_buffer_float(&SplitJoin133_Delay_N_Fiss_3250_3284_split[__iter_init_51_]);
	ENDFOR
	FOR(int, __iter_init_52_, 0, <, 4, __iter_init_52_++)
		init_buffer_float(&SplitJoin133_Delay_N_Fiss_3250_3284_join[__iter_init_52_]);
	ENDFOR
	init_buffer_float(&DownSamp_2958UpSamp_2959);
	FOR(int, __iter_init_53_, 0, <, 4, __iter_init_53_++)
		init_buffer_float(&SplitJoin91_Delay_N_Fiss_3242_3276_split[__iter_init_53_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3153DownSamp_2986);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3033DownSamp_2951);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3147DUPLICATE_Splitter_3152);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3123DUPLICATE_Splitter_3128);
	FOR(int, __iter_init_54_, 0, <, 4, __iter_init_54_++)
		init_buffer_float(&SplitJoin51_FirFilter_Fiss_3235_3269_split[__iter_init_54_]);
	ENDFOR
	FOR(int, __iter_init_55_, 0, <, 4, __iter_init_55_++)
		init_buffer_float(&SplitJoin116_Delay_N_Fiss_3248_3282_join[__iter_init_55_]);
	ENDFOR
	FOR(int, __iter_init_56_, 0, <, 4, __iter_init_56_++)
		init_buffer_float(&SplitJoin135_FirFilter_Fiss_3251_3285_split[__iter_init_56_]);
	ENDFOR
	FOR(int, __iter_init_57_, 0, <, 4, __iter_init_57_++)
		init_buffer_float(&SplitJoin76_FirFilter_Fiss_3241_3275_split[__iter_init_57_]);
	ENDFOR
	FOR(int, __iter_init_58_, 0, <, 4, __iter_init_58_++)
		init_buffer_float(&SplitJoin114_FirFilter_Fiss_3247_3281_split[__iter_init_58_]);
	ENDFOR
	FOR(int, __iter_init_59_, 0, <, 8, __iter_init_59_++)
		init_buffer_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[__iter_init_59_]);
	ENDFOR
	FOR(int, __iter_init_60_, 0, <, 4, __iter_init_60_++)
		init_buffer_float(&SplitJoin97_FirFilter_Fiss_3245_3279_join[__iter_init_60_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3057DownSamp_2958);
	init_buffer_float(&DownSamp_2972UpSamp_2973);
	init_buffer_float(&DownSamp_2965UpSamp_2966);
	FOR(int, __iter_init_61_, 0, <, 4, __iter_init_61_++)
		init_buffer_float(&SplitJoin72_FirFilter_Fiss_3239_3273_split[__iter_init_61_]);
	ENDFOR
	FOR(int, __iter_init_62_, 0, <, 4, __iter_init_62_++)
		init_buffer_float(&SplitJoin55_FirFilter_Fiss_3237_3271_join[__iter_init_62_]);
	ENDFOR
	FOR(int, __iter_init_63_, 0, <, 4, __iter_init_63_++)
		init_buffer_float(&SplitJoin137_Delay_N_Fiss_3252_3286_join[__iter_init_63_]);
	ENDFOR
	init_buffer_float(&UpSamp_2966WEIGHTED_ROUND_ROBIN_Splitter_3086);
	init_buffer_float(&UpSamp_2959WEIGHTED_ROUND_ROBIN_Splitter_3062);
	FOR(int, __iter_init_64_, 0, <, 4, __iter_init_64_++)
		init_buffer_float(&SplitJoin93_FirFilter_Fiss_3243_3277_split[__iter_init_64_]);
	ENDFOR
	FOR(int, __iter_init_65_, 0, <, 4, __iter_init_65_++)
		init_buffer_float(&SplitJoin34_FirFilter_Fiss_3233_3267_join[__iter_init_65_]);
	ENDFOR
	FOR(int, __iter_init_66_, 0, <, 4, __iter_init_66_++)
		init_buffer_float(&SplitJoin28_Delay_N_Fiss_3230_3264_join[__iter_init_66_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3135DUPLICATE_Splitter_3140);
	FOR(int, __iter_init_67_, 0, <, 4, __iter_init_67_++)
		init_buffer_float(&SplitJoin116_Delay_N_Fiss_3248_3282_split[__iter_init_67_]);
	ENDFOR
// --- init: source_2946
	 {
	source_2946_s.current = 0.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 300, __iter_init_++) {
		push_float(&source_2946DUPLICATE_Splitter_3006, source_2946_s.current) ; 
		if((source_2946_s.current > 1000.0)) {
			source_2946_s.current = 0.0 ; 
		}
		else {
			source_2946_s.current = (source_2946_s.current + 1.0) ; 
		}
	}
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3006
	FOR(uint32_t, __iter_init_, 0, <, 300, __iter_init_++)
		float __token_ = pop_float(&source_2946DUPLICATE_Splitter_3006);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3026
	FOR(uint32_t, __iter_init_, 0, <, 75, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin2_Delay_N_Fiss_3225_3260_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[0]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3028
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin2_Delay_N_Fiss_3225_3260_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3029
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin2_Delay_N_Fiss_3225_3260_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3030
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin2_Delay_N_Fiss_3225_3260_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3031
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin2_Delay_N_Fiss_3225_3260_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3027
	FOR(uint32_t, __iter_init_, 0, <, 46, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3027DUPLICATE_Splitter_3032, pop_float(&SplitJoin2_Delay_N_Fiss_3225_3260_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3032
	FOR(uint32_t, __iter_init_, 0, <, 183, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3027DUPLICATE_Splitter_3032);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin4_FirFilter_Fiss_3226_3261_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_3034
	 {
	FirFilter_3034_s.COEFF[0] = 1.0 ; 
	FirFilter_3034_s.COEFF[1] = 34.0 ; 
	FirFilter_3034_s.COEFF[2] = 67.0 ; 
	FirFilter_3034_s.COEFF[3] = 100.0 ; 
	FirFilter_3034_s.COEFF[4] = 133.0 ; 
	FirFilter_3034_s.COEFF[5] = 166.0 ; 
	FirFilter_3034_s.COEFF[6] = 199.0 ; 
	FirFilter_3034_s.COEFF[7] = 232.0 ; 
	FirFilter_3034_s.COEFF[8] = 265.0 ; 
	FirFilter_3034_s.COEFF[9] = 298.0 ; 
	FirFilter_3034_s.COEFF[10] = 331.0 ; 
	FirFilter_3034_s.COEFF[11] = 364.0 ; 
	FirFilter_3034_s.COEFF[12] = 397.0 ; 
	FirFilter_3034_s.COEFF[13] = 430.0 ; 
	FirFilter_3034_s.COEFF[14] = 463.0 ; 
	FirFilter_3034_s.COEFF[15] = 496.0 ; 
	FirFilter_3034_s.COEFF[16] = 529.0 ; 
	FirFilter_3034_s.COEFF[17] = 562.0 ; 
	FirFilter_3034_s.COEFF[18] = 595.0 ; 
	FirFilter_3034_s.COEFF[19] = 628.0 ; 
	FirFilter_3034_s.COEFF[20] = 661.0 ; 
	FirFilter_3034_s.COEFF[21] = 694.0 ; 
	FirFilter_3034_s.COEFF[22] = 727.0 ; 
	FirFilter_3034_s.COEFF[23] = 760.0 ; 
	FirFilter_3034_s.COEFF[24] = 793.0 ; 
	FirFilter_3034_s.COEFF[25] = 826.0 ; 
	FirFilter_3034_s.COEFF[26] = 859.0 ; 
	FirFilter_3034_s.COEFF[27] = 892.0 ; 
	FirFilter_3034_s.COEFF[28] = 925.0 ; 
	FirFilter_3034_s.COEFF[29] = 958.0 ; 
	FirFilter_3034_s.COEFF[30] = 991.0 ; 
	FirFilter_3034_s.COEFF[31] = 1024.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin4_FirFilter_Fiss_3226_3261_split[0], i) * FirFilter_3034_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin4_FirFilter_Fiss_3226_3261_split[0]) ; 
		push_float(&SplitJoin4_FirFilter_Fiss_3226_3261_join[0], sum) ; 
 {
		FOR(int, streamItVar637, 0,  < , 3, streamItVar637++) {
			pop_void(&SplitJoin4_FirFilter_Fiss_3226_3261_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_3035
	 {
	FirFilter_3035_s.COEFF[0] = 1.0 ; 
	FirFilter_3035_s.COEFF[1] = 34.0 ; 
	FirFilter_3035_s.COEFF[2] = 67.0 ; 
	FirFilter_3035_s.COEFF[3] = 100.0 ; 
	FirFilter_3035_s.COEFF[4] = 133.0 ; 
	FirFilter_3035_s.COEFF[5] = 166.0 ; 
	FirFilter_3035_s.COEFF[6] = 199.0 ; 
	FirFilter_3035_s.COEFF[7] = 232.0 ; 
	FirFilter_3035_s.COEFF[8] = 265.0 ; 
	FirFilter_3035_s.COEFF[9] = 298.0 ; 
	FirFilter_3035_s.COEFF[10] = 331.0 ; 
	FirFilter_3035_s.COEFF[11] = 364.0 ; 
	FirFilter_3035_s.COEFF[12] = 397.0 ; 
	FirFilter_3035_s.COEFF[13] = 430.0 ; 
	FirFilter_3035_s.COEFF[14] = 463.0 ; 
	FirFilter_3035_s.COEFF[15] = 496.0 ; 
	FirFilter_3035_s.COEFF[16] = 529.0 ; 
	FirFilter_3035_s.COEFF[17] = 562.0 ; 
	FirFilter_3035_s.COEFF[18] = 595.0 ; 
	FirFilter_3035_s.COEFF[19] = 628.0 ; 
	FirFilter_3035_s.COEFF[20] = 661.0 ; 
	FirFilter_3035_s.COEFF[21] = 694.0 ; 
	FirFilter_3035_s.COEFF[22] = 727.0 ; 
	FirFilter_3035_s.COEFF[23] = 760.0 ; 
	FirFilter_3035_s.COEFF[24] = 793.0 ; 
	FirFilter_3035_s.COEFF[25] = 826.0 ; 
	FirFilter_3035_s.COEFF[26] = 859.0 ; 
	FirFilter_3035_s.COEFF[27] = 892.0 ; 
	FirFilter_3035_s.COEFF[28] = 925.0 ; 
	FirFilter_3035_s.COEFF[29] = 958.0 ; 
	FirFilter_3035_s.COEFF[30] = 991.0 ; 
	FirFilter_3035_s.COEFF[31] = 1024.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin4_FirFilter_Fiss_3226_3261_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin4_FirFilter_Fiss_3226_3261_split[1], i) * FirFilter_3035_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin4_FirFilter_Fiss_3226_3261_split[1]) ; 
		push_float(&SplitJoin4_FirFilter_Fiss_3226_3261_join[1], sum) ; 
 {
		FOR(int, streamItVar638, 0,  < , 2, streamItVar638++) {
			pop_void(&SplitJoin4_FirFilter_Fiss_3226_3261_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_3036
	 {
	FirFilter_3036_s.COEFF[0] = 1.0 ; 
	FirFilter_3036_s.COEFF[1] = 34.0 ; 
	FirFilter_3036_s.COEFF[2] = 67.0 ; 
	FirFilter_3036_s.COEFF[3] = 100.0 ; 
	FirFilter_3036_s.COEFF[4] = 133.0 ; 
	FirFilter_3036_s.COEFF[5] = 166.0 ; 
	FirFilter_3036_s.COEFF[6] = 199.0 ; 
	FirFilter_3036_s.COEFF[7] = 232.0 ; 
	FirFilter_3036_s.COEFF[8] = 265.0 ; 
	FirFilter_3036_s.COEFF[9] = 298.0 ; 
	FirFilter_3036_s.COEFF[10] = 331.0 ; 
	FirFilter_3036_s.COEFF[11] = 364.0 ; 
	FirFilter_3036_s.COEFF[12] = 397.0 ; 
	FirFilter_3036_s.COEFF[13] = 430.0 ; 
	FirFilter_3036_s.COEFF[14] = 463.0 ; 
	FirFilter_3036_s.COEFF[15] = 496.0 ; 
	FirFilter_3036_s.COEFF[16] = 529.0 ; 
	FirFilter_3036_s.COEFF[17] = 562.0 ; 
	FirFilter_3036_s.COEFF[18] = 595.0 ; 
	FirFilter_3036_s.COEFF[19] = 628.0 ; 
	FirFilter_3036_s.COEFF[20] = 661.0 ; 
	FirFilter_3036_s.COEFF[21] = 694.0 ; 
	FirFilter_3036_s.COEFF[22] = 727.0 ; 
	FirFilter_3036_s.COEFF[23] = 760.0 ; 
	FirFilter_3036_s.COEFF[24] = 793.0 ; 
	FirFilter_3036_s.COEFF[25] = 826.0 ; 
	FirFilter_3036_s.COEFF[26] = 859.0 ; 
	FirFilter_3036_s.COEFF[27] = 892.0 ; 
	FirFilter_3036_s.COEFF[28] = 925.0 ; 
	FirFilter_3036_s.COEFF[29] = 958.0 ; 
	FirFilter_3036_s.COEFF[30] = 991.0 ; 
	FirFilter_3036_s.COEFF[31] = 1024.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar639, 0,  < , 2, streamItVar639++) {
			pop_void(&SplitJoin4_FirFilter_Fiss_3226_3261_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin4_FirFilter_Fiss_3226_3261_split[2], i) * FirFilter_3036_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin4_FirFilter_Fiss_3226_3261_split[2]) ; 
		push_float(&SplitJoin4_FirFilter_Fiss_3226_3261_join[2], sum) ; 
 {
		pop_void(&SplitJoin4_FirFilter_Fiss_3226_3261_split[2]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_3037
	 {
	FirFilter_3037_s.COEFF[0] = 1.0 ; 
	FirFilter_3037_s.COEFF[1] = 34.0 ; 
	FirFilter_3037_s.COEFF[2] = 67.0 ; 
	FirFilter_3037_s.COEFF[3] = 100.0 ; 
	FirFilter_3037_s.COEFF[4] = 133.0 ; 
	FirFilter_3037_s.COEFF[5] = 166.0 ; 
	FirFilter_3037_s.COEFF[6] = 199.0 ; 
	FirFilter_3037_s.COEFF[7] = 232.0 ; 
	FirFilter_3037_s.COEFF[8] = 265.0 ; 
	FirFilter_3037_s.COEFF[9] = 298.0 ; 
	FirFilter_3037_s.COEFF[10] = 331.0 ; 
	FirFilter_3037_s.COEFF[11] = 364.0 ; 
	FirFilter_3037_s.COEFF[12] = 397.0 ; 
	FirFilter_3037_s.COEFF[13] = 430.0 ; 
	FirFilter_3037_s.COEFF[14] = 463.0 ; 
	FirFilter_3037_s.COEFF[15] = 496.0 ; 
	FirFilter_3037_s.COEFF[16] = 529.0 ; 
	FirFilter_3037_s.COEFF[17] = 562.0 ; 
	FirFilter_3037_s.COEFF[18] = 595.0 ; 
	FirFilter_3037_s.COEFF[19] = 628.0 ; 
	FirFilter_3037_s.COEFF[20] = 661.0 ; 
	FirFilter_3037_s.COEFF[21] = 694.0 ; 
	FirFilter_3037_s.COEFF[22] = 727.0 ; 
	FirFilter_3037_s.COEFF[23] = 760.0 ; 
	FirFilter_3037_s.COEFF[24] = 793.0 ; 
	FirFilter_3037_s.COEFF[25] = 826.0 ; 
	FirFilter_3037_s.COEFF[26] = 859.0 ; 
	FirFilter_3037_s.COEFF[27] = 892.0 ; 
	FirFilter_3037_s.COEFF[28] = 925.0 ; 
	FirFilter_3037_s.COEFF[29] = 958.0 ; 
	FirFilter_3037_s.COEFF[30] = 991.0 ; 
	FirFilter_3037_s.COEFF[31] = 1024.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar640, 0,  < , 3, streamItVar640++) {
			pop_void(&SplitJoin4_FirFilter_Fiss_3226_3261_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin4_FirFilter_Fiss_3226_3261_split[3], i) * FirFilter_3037_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin4_FirFilter_Fiss_3226_3261_split[3]) ; 
		push_float(&SplitJoin4_FirFilter_Fiss_3226_3261_join[3], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3033
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3033DownSamp_2951, pop_float(&SplitJoin4_FirFilter_Fiss_3226_3261_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DownSamp_2951
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++) {
		push_float(&DownSamp_2951UpSamp_2952, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3033DownSamp_2951)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3033DownSamp_2951) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: UpSamp_2952
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++) {
		push_float(&UpSamp_2952WEIGHTED_ROUND_ROBIN_Splitter_3038, pop_float(&DownSamp_2951UpSamp_2952)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			push_float(&UpSamp_2952WEIGHTED_ROUND_ROBIN_Splitter_3038, 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3038
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin6_Delay_N_Fiss_3227_3262_split[__iter_], pop_float(&UpSamp_2952WEIGHTED_ROUND_ROBIN_Splitter_3038));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3040
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin6_Delay_N_Fiss_3227_3262_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3041
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin6_Delay_N_Fiss_3227_3262_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3042
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin6_Delay_N_Fiss_3227_3262_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3043
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin6_Delay_N_Fiss_3227_3262_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3039
	FOR(uint32_t, __iter_init_, 0, <, 9, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3039DUPLICATE_Splitter_3044, pop_float(&SplitJoin6_Delay_N_Fiss_3227_3262_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3044
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3039DUPLICATE_Splitter_3044);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin8_FirFilter_Fiss_3228_3263_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_3046
	 {
	FirFilter_3046_s.COEFF[0] = 0.0 ; 
	FirFilter_3046_s.COEFF[1] = 1.0 ; 
	FirFilter_3046_s.COEFF[2] = 2.0 ; 
	FirFilter_3046_s.COEFF[3] = 3.0 ; 
	FirFilter_3046_s.COEFF[4] = 4.0 ; 
	FirFilter_3046_s.COEFF[5] = 5.0 ; 
	FirFilter_3046_s.COEFF[6] = 6.0 ; 
	FirFilter_3046_s.COEFF[7] = 7.0 ; 
	FirFilter_3046_s.COEFF[8] = 8.0 ; 
	FirFilter_3046_s.COEFF[9] = 9.0 ; 
	FirFilter_3046_s.COEFF[10] = 10.0 ; 
	FirFilter_3046_s.COEFF[11] = 11.0 ; 
	FirFilter_3046_s.COEFF[12] = 12.0 ; 
	FirFilter_3046_s.COEFF[13] = 13.0 ; 
	FirFilter_3046_s.COEFF[14] = 14.0 ; 
	FirFilter_3046_s.COEFF[15] = 15.0 ; 
	FirFilter_3046_s.COEFF[16] = 16.0 ; 
	FirFilter_3046_s.COEFF[17] = 17.0 ; 
	FirFilter_3046_s.COEFF[18] = 18.0 ; 
	FirFilter_3046_s.COEFF[19] = 19.0 ; 
	FirFilter_3046_s.COEFF[20] = 20.0 ; 
	FirFilter_3046_s.COEFF[21] = 21.0 ; 
	FirFilter_3046_s.COEFF[22] = 22.0 ; 
	FirFilter_3046_s.COEFF[23] = 23.0 ; 
	FirFilter_3046_s.COEFF[24] = 24.0 ; 
	FirFilter_3046_s.COEFF[25] = 25.0 ; 
	FirFilter_3046_s.COEFF[26] = 26.0 ; 
	FirFilter_3046_s.COEFF[27] = 27.0 ; 
	FirFilter_3046_s.COEFF[28] = 28.0 ; 
	FirFilter_3046_s.COEFF[29] = 29.0 ; 
	FirFilter_3046_s.COEFF[30] = 30.0 ; 
	FirFilter_3046_s.COEFF[31] = 31.0 ; 
}
	 {
	float sum = 0.0;
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin8_FirFilter_Fiss_3228_3263_split[0], i) * FirFilter_3046_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin8_FirFilter_Fiss_3228_3263_split[0]) ; 
	push_float(&SplitJoin8_FirFilter_Fiss_3228_3263_join[0], sum) ; 
 {
	FOR(int, streamItVar633, 0,  < , 3, streamItVar633++) {
		pop_void(&SplitJoin8_FirFilter_Fiss_3228_3263_split[0]) ; 
	}
	ENDFOR
}
}
//--------------------------------
// --- init: FirFilter_3047
	 {
	FirFilter_3047_s.COEFF[0] = 0.0 ; 
	FirFilter_3047_s.COEFF[1] = 1.0 ; 
	FirFilter_3047_s.COEFF[2] = 2.0 ; 
	FirFilter_3047_s.COEFF[3] = 3.0 ; 
	FirFilter_3047_s.COEFF[4] = 4.0 ; 
	FirFilter_3047_s.COEFF[5] = 5.0 ; 
	FirFilter_3047_s.COEFF[6] = 6.0 ; 
	FirFilter_3047_s.COEFF[7] = 7.0 ; 
	FirFilter_3047_s.COEFF[8] = 8.0 ; 
	FirFilter_3047_s.COEFF[9] = 9.0 ; 
	FirFilter_3047_s.COEFF[10] = 10.0 ; 
	FirFilter_3047_s.COEFF[11] = 11.0 ; 
	FirFilter_3047_s.COEFF[12] = 12.0 ; 
	FirFilter_3047_s.COEFF[13] = 13.0 ; 
	FirFilter_3047_s.COEFF[14] = 14.0 ; 
	FirFilter_3047_s.COEFF[15] = 15.0 ; 
	FirFilter_3047_s.COEFF[16] = 16.0 ; 
	FirFilter_3047_s.COEFF[17] = 17.0 ; 
	FirFilter_3047_s.COEFF[18] = 18.0 ; 
	FirFilter_3047_s.COEFF[19] = 19.0 ; 
	FirFilter_3047_s.COEFF[20] = 20.0 ; 
	FirFilter_3047_s.COEFF[21] = 21.0 ; 
	FirFilter_3047_s.COEFF[22] = 22.0 ; 
	FirFilter_3047_s.COEFF[23] = 23.0 ; 
	FirFilter_3047_s.COEFF[24] = 24.0 ; 
	FirFilter_3047_s.COEFF[25] = 25.0 ; 
	FirFilter_3047_s.COEFF[26] = 26.0 ; 
	FirFilter_3047_s.COEFF[27] = 27.0 ; 
	FirFilter_3047_s.COEFF[28] = 28.0 ; 
	FirFilter_3047_s.COEFF[29] = 29.0 ; 
	FirFilter_3047_s.COEFF[30] = 30.0 ; 
	FirFilter_3047_s.COEFF[31] = 31.0 ; 
}
	 {
	float sum = 0.0;
 {
	pop_void(&SplitJoin8_FirFilter_Fiss_3228_3263_split[1]) ; 
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin8_FirFilter_Fiss_3228_3263_split[1], i) * FirFilter_3047_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin8_FirFilter_Fiss_3228_3263_split[1]) ; 
	push_float(&SplitJoin8_FirFilter_Fiss_3228_3263_join[1], sum) ; 
 {
	FOR(int, streamItVar634, 0,  < , 2, streamItVar634++) {
		pop_void(&SplitJoin8_FirFilter_Fiss_3228_3263_split[1]) ; 
	}
	ENDFOR
}
}
//--------------------------------
// --- init: FirFilter_3048
	 {
	FirFilter_3048_s.COEFF[0] = 0.0 ; 
	FirFilter_3048_s.COEFF[1] = 1.0 ; 
	FirFilter_3048_s.COEFF[2] = 2.0 ; 
	FirFilter_3048_s.COEFF[3] = 3.0 ; 
	FirFilter_3048_s.COEFF[4] = 4.0 ; 
	FirFilter_3048_s.COEFF[5] = 5.0 ; 
	FirFilter_3048_s.COEFF[6] = 6.0 ; 
	FirFilter_3048_s.COEFF[7] = 7.0 ; 
	FirFilter_3048_s.COEFF[8] = 8.0 ; 
	FirFilter_3048_s.COEFF[9] = 9.0 ; 
	FirFilter_3048_s.COEFF[10] = 10.0 ; 
	FirFilter_3048_s.COEFF[11] = 11.0 ; 
	FirFilter_3048_s.COEFF[12] = 12.0 ; 
	FirFilter_3048_s.COEFF[13] = 13.0 ; 
	FirFilter_3048_s.COEFF[14] = 14.0 ; 
	FirFilter_3048_s.COEFF[15] = 15.0 ; 
	FirFilter_3048_s.COEFF[16] = 16.0 ; 
	FirFilter_3048_s.COEFF[17] = 17.0 ; 
	FirFilter_3048_s.COEFF[18] = 18.0 ; 
	FirFilter_3048_s.COEFF[19] = 19.0 ; 
	FirFilter_3048_s.COEFF[20] = 20.0 ; 
	FirFilter_3048_s.COEFF[21] = 21.0 ; 
	FirFilter_3048_s.COEFF[22] = 22.0 ; 
	FirFilter_3048_s.COEFF[23] = 23.0 ; 
	FirFilter_3048_s.COEFF[24] = 24.0 ; 
	FirFilter_3048_s.COEFF[25] = 25.0 ; 
	FirFilter_3048_s.COEFF[26] = 26.0 ; 
	FirFilter_3048_s.COEFF[27] = 27.0 ; 
	FirFilter_3048_s.COEFF[28] = 28.0 ; 
	FirFilter_3048_s.COEFF[29] = 29.0 ; 
	FirFilter_3048_s.COEFF[30] = 30.0 ; 
	FirFilter_3048_s.COEFF[31] = 31.0 ; 
}
	 {
	float sum = 0.0;
 {
	FOR(int, streamItVar635, 0,  < , 2, streamItVar635++) {
		pop_void(&SplitJoin8_FirFilter_Fiss_3228_3263_split[2]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin8_FirFilter_Fiss_3228_3263_split[2], i) * FirFilter_3048_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin8_FirFilter_Fiss_3228_3263_split[2]) ; 
	push_float(&SplitJoin8_FirFilter_Fiss_3228_3263_join[2], sum) ; 
 {
	pop_void(&SplitJoin8_FirFilter_Fiss_3228_3263_split[2]) ; 
}
}
//--------------------------------
// --- init: FirFilter_3049
	 {
	FirFilter_3049_s.COEFF[0] = 0.0 ; 
	FirFilter_3049_s.COEFF[1] = 1.0 ; 
	FirFilter_3049_s.COEFF[2] = 2.0 ; 
	FirFilter_3049_s.COEFF[3] = 3.0 ; 
	FirFilter_3049_s.COEFF[4] = 4.0 ; 
	FirFilter_3049_s.COEFF[5] = 5.0 ; 
	FirFilter_3049_s.COEFF[6] = 6.0 ; 
	FirFilter_3049_s.COEFF[7] = 7.0 ; 
	FirFilter_3049_s.COEFF[8] = 8.0 ; 
	FirFilter_3049_s.COEFF[9] = 9.0 ; 
	FirFilter_3049_s.COEFF[10] = 10.0 ; 
	FirFilter_3049_s.COEFF[11] = 11.0 ; 
	FirFilter_3049_s.COEFF[12] = 12.0 ; 
	FirFilter_3049_s.COEFF[13] = 13.0 ; 
	FirFilter_3049_s.COEFF[14] = 14.0 ; 
	FirFilter_3049_s.COEFF[15] = 15.0 ; 
	FirFilter_3049_s.COEFF[16] = 16.0 ; 
	FirFilter_3049_s.COEFF[17] = 17.0 ; 
	FirFilter_3049_s.COEFF[18] = 18.0 ; 
	FirFilter_3049_s.COEFF[19] = 19.0 ; 
	FirFilter_3049_s.COEFF[20] = 20.0 ; 
	FirFilter_3049_s.COEFF[21] = 21.0 ; 
	FirFilter_3049_s.COEFF[22] = 22.0 ; 
	FirFilter_3049_s.COEFF[23] = 23.0 ; 
	FirFilter_3049_s.COEFF[24] = 24.0 ; 
	FirFilter_3049_s.COEFF[25] = 25.0 ; 
	FirFilter_3049_s.COEFF[26] = 26.0 ; 
	FirFilter_3049_s.COEFF[27] = 27.0 ; 
	FirFilter_3049_s.COEFF[28] = 28.0 ; 
	FirFilter_3049_s.COEFF[29] = 29.0 ; 
	FirFilter_3049_s.COEFF[30] = 30.0 ; 
	FirFilter_3049_s.COEFF[31] = 31.0 ; 
}
	 {
	float sum = 0.0;
 {
	FOR(int, streamItVar636, 0,  < , 3, streamItVar636++) {
		pop_void(&SplitJoin8_FirFilter_Fiss_3228_3263_split[3]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin8_FirFilter_Fiss_3228_3263_split[3], i) * FirFilter_3049_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin8_FirFilter_Fiss_3228_3263_split[3]) ; 
	push_float(&SplitJoin8_FirFilter_Fiss_3228_3263_join[3], sum) ; 
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3045
	
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[0], pop_float(&SplitJoin8_FirFilter_Fiss_3228_3263_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3050
	FOR(uint32_t, __iter_init_, 0, <, 75, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin28_Delay_N_Fiss_3230_3264_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[1]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3052
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin28_Delay_N_Fiss_3230_3264_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3053
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin28_Delay_N_Fiss_3230_3264_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3054
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin28_Delay_N_Fiss_3230_3264_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3055
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin28_Delay_N_Fiss_3230_3264_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3051
	FOR(uint32_t, __iter_init_, 0, <, 46, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3051DUPLICATE_Splitter_3056, pop_float(&SplitJoin28_Delay_N_Fiss_3230_3264_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3056
	FOR(uint32_t, __iter_init_, 0, <, 183, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3051DUPLICATE_Splitter_3056);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin30_FirFilter_Fiss_3231_3265_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_3058
	 {
	FirFilter_3058_s.COEFF[0] = 11.0 ; 
	FirFilter_3058_s.COEFF[1] = 44.0 ; 
	FirFilter_3058_s.COEFF[2] = 77.0 ; 
	FirFilter_3058_s.COEFF[3] = 110.0 ; 
	FirFilter_3058_s.COEFF[4] = 143.0 ; 
	FirFilter_3058_s.COEFF[5] = 176.0 ; 
	FirFilter_3058_s.COEFF[6] = 209.0 ; 
	FirFilter_3058_s.COEFF[7] = 242.0 ; 
	FirFilter_3058_s.COEFF[8] = 275.0 ; 
	FirFilter_3058_s.COEFF[9] = 308.0 ; 
	FirFilter_3058_s.COEFF[10] = 341.0 ; 
	FirFilter_3058_s.COEFF[11] = 374.0 ; 
	FirFilter_3058_s.COEFF[12] = 407.0 ; 
	FirFilter_3058_s.COEFF[13] = 440.0 ; 
	FirFilter_3058_s.COEFF[14] = 473.0 ; 
	FirFilter_3058_s.COEFF[15] = 506.0 ; 
	FirFilter_3058_s.COEFF[16] = 539.0 ; 
	FirFilter_3058_s.COEFF[17] = 572.0 ; 
	FirFilter_3058_s.COEFF[18] = 605.0 ; 
	FirFilter_3058_s.COEFF[19] = 638.0 ; 
	FirFilter_3058_s.COEFF[20] = 671.0 ; 
	FirFilter_3058_s.COEFF[21] = 704.0 ; 
	FirFilter_3058_s.COEFF[22] = 737.0 ; 
	FirFilter_3058_s.COEFF[23] = 770.0 ; 
	FirFilter_3058_s.COEFF[24] = 803.0 ; 
	FirFilter_3058_s.COEFF[25] = 836.0 ; 
	FirFilter_3058_s.COEFF[26] = 869.0 ; 
	FirFilter_3058_s.COEFF[27] = 902.0 ; 
	FirFilter_3058_s.COEFF[28] = 935.0 ; 
	FirFilter_3058_s.COEFF[29] = 968.0 ; 
	FirFilter_3058_s.COEFF[30] = 1001.0 ; 
	FirFilter_3058_s.COEFF[31] = 1034.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin30_FirFilter_Fiss_3231_3265_split[0], i) * FirFilter_3058_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin30_FirFilter_Fiss_3231_3265_split[0]) ; 
		push_float(&SplitJoin30_FirFilter_Fiss_3231_3265_join[0], sum) ; 
 {
		FOR(int, streamItVar629, 0,  < , 3, streamItVar629++) {
			pop_void(&SplitJoin30_FirFilter_Fiss_3231_3265_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_3059
	 {
	FirFilter_3059_s.COEFF[0] = 11.0 ; 
	FirFilter_3059_s.COEFF[1] = 44.0 ; 
	FirFilter_3059_s.COEFF[2] = 77.0 ; 
	FirFilter_3059_s.COEFF[3] = 110.0 ; 
	FirFilter_3059_s.COEFF[4] = 143.0 ; 
	FirFilter_3059_s.COEFF[5] = 176.0 ; 
	FirFilter_3059_s.COEFF[6] = 209.0 ; 
	FirFilter_3059_s.COEFF[7] = 242.0 ; 
	FirFilter_3059_s.COEFF[8] = 275.0 ; 
	FirFilter_3059_s.COEFF[9] = 308.0 ; 
	FirFilter_3059_s.COEFF[10] = 341.0 ; 
	FirFilter_3059_s.COEFF[11] = 374.0 ; 
	FirFilter_3059_s.COEFF[12] = 407.0 ; 
	FirFilter_3059_s.COEFF[13] = 440.0 ; 
	FirFilter_3059_s.COEFF[14] = 473.0 ; 
	FirFilter_3059_s.COEFF[15] = 506.0 ; 
	FirFilter_3059_s.COEFF[16] = 539.0 ; 
	FirFilter_3059_s.COEFF[17] = 572.0 ; 
	FirFilter_3059_s.COEFF[18] = 605.0 ; 
	FirFilter_3059_s.COEFF[19] = 638.0 ; 
	FirFilter_3059_s.COEFF[20] = 671.0 ; 
	FirFilter_3059_s.COEFF[21] = 704.0 ; 
	FirFilter_3059_s.COEFF[22] = 737.0 ; 
	FirFilter_3059_s.COEFF[23] = 770.0 ; 
	FirFilter_3059_s.COEFF[24] = 803.0 ; 
	FirFilter_3059_s.COEFF[25] = 836.0 ; 
	FirFilter_3059_s.COEFF[26] = 869.0 ; 
	FirFilter_3059_s.COEFF[27] = 902.0 ; 
	FirFilter_3059_s.COEFF[28] = 935.0 ; 
	FirFilter_3059_s.COEFF[29] = 968.0 ; 
	FirFilter_3059_s.COEFF[30] = 1001.0 ; 
	FirFilter_3059_s.COEFF[31] = 1034.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin30_FirFilter_Fiss_3231_3265_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin30_FirFilter_Fiss_3231_3265_split[1], i) * FirFilter_3059_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin30_FirFilter_Fiss_3231_3265_split[1]) ; 
		push_float(&SplitJoin30_FirFilter_Fiss_3231_3265_join[1], sum) ; 
 {
		FOR(int, streamItVar630, 0,  < , 2, streamItVar630++) {
			pop_void(&SplitJoin30_FirFilter_Fiss_3231_3265_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_3060
	 {
	FirFilter_3060_s.COEFF[0] = 11.0 ; 
	FirFilter_3060_s.COEFF[1] = 44.0 ; 
	FirFilter_3060_s.COEFF[2] = 77.0 ; 
	FirFilter_3060_s.COEFF[3] = 110.0 ; 
	FirFilter_3060_s.COEFF[4] = 143.0 ; 
	FirFilter_3060_s.COEFF[5] = 176.0 ; 
	FirFilter_3060_s.COEFF[6] = 209.0 ; 
	FirFilter_3060_s.COEFF[7] = 242.0 ; 
	FirFilter_3060_s.COEFF[8] = 275.0 ; 
	FirFilter_3060_s.COEFF[9] = 308.0 ; 
	FirFilter_3060_s.COEFF[10] = 341.0 ; 
	FirFilter_3060_s.COEFF[11] = 374.0 ; 
	FirFilter_3060_s.COEFF[12] = 407.0 ; 
	FirFilter_3060_s.COEFF[13] = 440.0 ; 
	FirFilter_3060_s.COEFF[14] = 473.0 ; 
	FirFilter_3060_s.COEFF[15] = 506.0 ; 
	FirFilter_3060_s.COEFF[16] = 539.0 ; 
	FirFilter_3060_s.COEFF[17] = 572.0 ; 
	FirFilter_3060_s.COEFF[18] = 605.0 ; 
	FirFilter_3060_s.COEFF[19] = 638.0 ; 
	FirFilter_3060_s.COEFF[20] = 671.0 ; 
	FirFilter_3060_s.COEFF[21] = 704.0 ; 
	FirFilter_3060_s.COEFF[22] = 737.0 ; 
	FirFilter_3060_s.COEFF[23] = 770.0 ; 
	FirFilter_3060_s.COEFF[24] = 803.0 ; 
	FirFilter_3060_s.COEFF[25] = 836.0 ; 
	FirFilter_3060_s.COEFF[26] = 869.0 ; 
	FirFilter_3060_s.COEFF[27] = 902.0 ; 
	FirFilter_3060_s.COEFF[28] = 935.0 ; 
	FirFilter_3060_s.COEFF[29] = 968.0 ; 
	FirFilter_3060_s.COEFF[30] = 1001.0 ; 
	FirFilter_3060_s.COEFF[31] = 1034.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar631, 0,  < , 2, streamItVar631++) {
			pop_void(&SplitJoin30_FirFilter_Fiss_3231_3265_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin30_FirFilter_Fiss_3231_3265_split[2], i) * FirFilter_3060_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin30_FirFilter_Fiss_3231_3265_split[2]) ; 
		push_float(&SplitJoin30_FirFilter_Fiss_3231_3265_join[2], sum) ; 
 {
		pop_void(&SplitJoin30_FirFilter_Fiss_3231_3265_split[2]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_3061
	 {
	FirFilter_3061_s.COEFF[0] = 11.0 ; 
	FirFilter_3061_s.COEFF[1] = 44.0 ; 
	FirFilter_3061_s.COEFF[2] = 77.0 ; 
	FirFilter_3061_s.COEFF[3] = 110.0 ; 
	FirFilter_3061_s.COEFF[4] = 143.0 ; 
	FirFilter_3061_s.COEFF[5] = 176.0 ; 
	FirFilter_3061_s.COEFF[6] = 209.0 ; 
	FirFilter_3061_s.COEFF[7] = 242.0 ; 
	FirFilter_3061_s.COEFF[8] = 275.0 ; 
	FirFilter_3061_s.COEFF[9] = 308.0 ; 
	FirFilter_3061_s.COEFF[10] = 341.0 ; 
	FirFilter_3061_s.COEFF[11] = 374.0 ; 
	FirFilter_3061_s.COEFF[12] = 407.0 ; 
	FirFilter_3061_s.COEFF[13] = 440.0 ; 
	FirFilter_3061_s.COEFF[14] = 473.0 ; 
	FirFilter_3061_s.COEFF[15] = 506.0 ; 
	FirFilter_3061_s.COEFF[16] = 539.0 ; 
	FirFilter_3061_s.COEFF[17] = 572.0 ; 
	FirFilter_3061_s.COEFF[18] = 605.0 ; 
	FirFilter_3061_s.COEFF[19] = 638.0 ; 
	FirFilter_3061_s.COEFF[20] = 671.0 ; 
	FirFilter_3061_s.COEFF[21] = 704.0 ; 
	FirFilter_3061_s.COEFF[22] = 737.0 ; 
	FirFilter_3061_s.COEFF[23] = 770.0 ; 
	FirFilter_3061_s.COEFF[24] = 803.0 ; 
	FirFilter_3061_s.COEFF[25] = 836.0 ; 
	FirFilter_3061_s.COEFF[26] = 869.0 ; 
	FirFilter_3061_s.COEFF[27] = 902.0 ; 
	FirFilter_3061_s.COEFF[28] = 935.0 ; 
	FirFilter_3061_s.COEFF[29] = 968.0 ; 
	FirFilter_3061_s.COEFF[30] = 1001.0 ; 
	FirFilter_3061_s.COEFF[31] = 1034.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar632, 0,  < , 3, streamItVar632++) {
			pop_void(&SplitJoin30_FirFilter_Fiss_3231_3265_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin30_FirFilter_Fiss_3231_3265_split[3], i) * FirFilter_3061_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin30_FirFilter_Fiss_3231_3265_split[3]) ; 
		push_float(&SplitJoin30_FirFilter_Fiss_3231_3265_join[3], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3057
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3057DownSamp_2958, pop_float(&SplitJoin30_FirFilter_Fiss_3231_3265_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DownSamp_2958
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++) {
		push_float(&DownSamp_2958UpSamp_2959, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3057DownSamp_2958)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3057DownSamp_2958) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: UpSamp_2959
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++) {
		push_float(&UpSamp_2959WEIGHTED_ROUND_ROBIN_Splitter_3062, pop_float(&DownSamp_2958UpSamp_2959)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			push_float(&UpSamp_2959WEIGHTED_ROUND_ROBIN_Splitter_3062, 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3062
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin32_Delay_N_Fiss_3232_3266_split[__iter_], pop_float(&UpSamp_2959WEIGHTED_ROUND_ROBIN_Splitter_3062));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3064
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin32_Delay_N_Fiss_3232_3266_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3065
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin32_Delay_N_Fiss_3232_3266_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3066
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin32_Delay_N_Fiss_3232_3266_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3067
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin32_Delay_N_Fiss_3232_3266_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3063
	FOR(uint32_t, __iter_init_, 0, <, 9, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3063DUPLICATE_Splitter_3068, pop_float(&SplitJoin32_Delay_N_Fiss_3232_3266_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3068
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3063DUPLICATE_Splitter_3068);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin34_FirFilter_Fiss_3233_3267_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_3070
	 {
	FirFilter_3070_s.COEFF[0] = 2.0 ; 
	FirFilter_3070_s.COEFF[1] = 4.0 ; 
	FirFilter_3070_s.COEFF[2] = 6.0 ; 
	FirFilter_3070_s.COEFF[3] = 8.0 ; 
	FirFilter_3070_s.COEFF[4] = 10.0 ; 
	FirFilter_3070_s.COEFF[5] = 12.0 ; 
	FirFilter_3070_s.COEFF[6] = 14.0 ; 
	FirFilter_3070_s.COEFF[7] = 16.0 ; 
	FirFilter_3070_s.COEFF[8] = 18.0 ; 
	FirFilter_3070_s.COEFF[9] = 20.0 ; 
	FirFilter_3070_s.COEFF[10] = 22.0 ; 
	FirFilter_3070_s.COEFF[11] = 24.0 ; 
	FirFilter_3070_s.COEFF[12] = 26.0 ; 
	FirFilter_3070_s.COEFF[13] = 28.0 ; 
	FirFilter_3070_s.COEFF[14] = 30.0 ; 
	FirFilter_3070_s.COEFF[15] = 32.0 ; 
	FirFilter_3070_s.COEFF[16] = 34.0 ; 
	FirFilter_3070_s.COEFF[17] = 36.0 ; 
	FirFilter_3070_s.COEFF[18] = 38.0 ; 
	FirFilter_3070_s.COEFF[19] = 40.0 ; 
	FirFilter_3070_s.COEFF[20] = 42.0 ; 
	FirFilter_3070_s.COEFF[21] = 44.0 ; 
	FirFilter_3070_s.COEFF[22] = 46.0 ; 
	FirFilter_3070_s.COEFF[23] = 48.0 ; 
	FirFilter_3070_s.COEFF[24] = 50.0 ; 
	FirFilter_3070_s.COEFF[25] = 52.0 ; 
	FirFilter_3070_s.COEFF[26] = 54.0 ; 
	FirFilter_3070_s.COEFF[27] = 56.0 ; 
	FirFilter_3070_s.COEFF[28] = 58.0 ; 
	FirFilter_3070_s.COEFF[29] = 60.0 ; 
	FirFilter_3070_s.COEFF[30] = 62.0 ; 
	FirFilter_3070_s.COEFF[31] = 64.0 ; 
}
	 {
	float sum = 0.0;
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin34_FirFilter_Fiss_3233_3267_split[0], i) * FirFilter_3070_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin34_FirFilter_Fiss_3233_3267_split[0]) ; 
	push_float(&SplitJoin34_FirFilter_Fiss_3233_3267_join[0], sum) ; 
 {
	FOR(int, streamItVar625, 0,  < , 3, streamItVar625++) {
		pop_void(&SplitJoin34_FirFilter_Fiss_3233_3267_split[0]) ; 
	}
	ENDFOR
}
}
//--------------------------------
// --- init: FirFilter_3071
	 {
	FirFilter_3071_s.COEFF[0] = 2.0 ; 
	FirFilter_3071_s.COEFF[1] = 4.0 ; 
	FirFilter_3071_s.COEFF[2] = 6.0 ; 
	FirFilter_3071_s.COEFF[3] = 8.0 ; 
	FirFilter_3071_s.COEFF[4] = 10.0 ; 
	FirFilter_3071_s.COEFF[5] = 12.0 ; 
	FirFilter_3071_s.COEFF[6] = 14.0 ; 
	FirFilter_3071_s.COEFF[7] = 16.0 ; 
	FirFilter_3071_s.COEFF[8] = 18.0 ; 
	FirFilter_3071_s.COEFF[9] = 20.0 ; 
	FirFilter_3071_s.COEFF[10] = 22.0 ; 
	FirFilter_3071_s.COEFF[11] = 24.0 ; 
	FirFilter_3071_s.COEFF[12] = 26.0 ; 
	FirFilter_3071_s.COEFF[13] = 28.0 ; 
	FirFilter_3071_s.COEFF[14] = 30.0 ; 
	FirFilter_3071_s.COEFF[15] = 32.0 ; 
	FirFilter_3071_s.COEFF[16] = 34.0 ; 
	FirFilter_3071_s.COEFF[17] = 36.0 ; 
	FirFilter_3071_s.COEFF[18] = 38.0 ; 
	FirFilter_3071_s.COEFF[19] = 40.0 ; 
	FirFilter_3071_s.COEFF[20] = 42.0 ; 
	FirFilter_3071_s.COEFF[21] = 44.0 ; 
	FirFilter_3071_s.COEFF[22] = 46.0 ; 
	FirFilter_3071_s.COEFF[23] = 48.0 ; 
	FirFilter_3071_s.COEFF[24] = 50.0 ; 
	FirFilter_3071_s.COEFF[25] = 52.0 ; 
	FirFilter_3071_s.COEFF[26] = 54.0 ; 
	FirFilter_3071_s.COEFF[27] = 56.0 ; 
	FirFilter_3071_s.COEFF[28] = 58.0 ; 
	FirFilter_3071_s.COEFF[29] = 60.0 ; 
	FirFilter_3071_s.COEFF[30] = 62.0 ; 
	FirFilter_3071_s.COEFF[31] = 64.0 ; 
}
	 {
	float sum = 0.0;
 {
	pop_void(&SplitJoin34_FirFilter_Fiss_3233_3267_split[1]) ; 
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin34_FirFilter_Fiss_3233_3267_split[1], i) * FirFilter_3071_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin34_FirFilter_Fiss_3233_3267_split[1]) ; 
	push_float(&SplitJoin34_FirFilter_Fiss_3233_3267_join[1], sum) ; 
 {
	FOR(int, streamItVar626, 0,  < , 2, streamItVar626++) {
		pop_void(&SplitJoin34_FirFilter_Fiss_3233_3267_split[1]) ; 
	}
	ENDFOR
}
}
//--------------------------------
// --- init: FirFilter_3072
	 {
	FirFilter_3072_s.COEFF[0] = 2.0 ; 
	FirFilter_3072_s.COEFF[1] = 4.0 ; 
	FirFilter_3072_s.COEFF[2] = 6.0 ; 
	FirFilter_3072_s.COEFF[3] = 8.0 ; 
	FirFilter_3072_s.COEFF[4] = 10.0 ; 
	FirFilter_3072_s.COEFF[5] = 12.0 ; 
	FirFilter_3072_s.COEFF[6] = 14.0 ; 
	FirFilter_3072_s.COEFF[7] = 16.0 ; 
	FirFilter_3072_s.COEFF[8] = 18.0 ; 
	FirFilter_3072_s.COEFF[9] = 20.0 ; 
	FirFilter_3072_s.COEFF[10] = 22.0 ; 
	FirFilter_3072_s.COEFF[11] = 24.0 ; 
	FirFilter_3072_s.COEFF[12] = 26.0 ; 
	FirFilter_3072_s.COEFF[13] = 28.0 ; 
	FirFilter_3072_s.COEFF[14] = 30.0 ; 
	FirFilter_3072_s.COEFF[15] = 32.0 ; 
	FirFilter_3072_s.COEFF[16] = 34.0 ; 
	FirFilter_3072_s.COEFF[17] = 36.0 ; 
	FirFilter_3072_s.COEFF[18] = 38.0 ; 
	FirFilter_3072_s.COEFF[19] = 40.0 ; 
	FirFilter_3072_s.COEFF[20] = 42.0 ; 
	FirFilter_3072_s.COEFF[21] = 44.0 ; 
	FirFilter_3072_s.COEFF[22] = 46.0 ; 
	FirFilter_3072_s.COEFF[23] = 48.0 ; 
	FirFilter_3072_s.COEFF[24] = 50.0 ; 
	FirFilter_3072_s.COEFF[25] = 52.0 ; 
	FirFilter_3072_s.COEFF[26] = 54.0 ; 
	FirFilter_3072_s.COEFF[27] = 56.0 ; 
	FirFilter_3072_s.COEFF[28] = 58.0 ; 
	FirFilter_3072_s.COEFF[29] = 60.0 ; 
	FirFilter_3072_s.COEFF[30] = 62.0 ; 
	FirFilter_3072_s.COEFF[31] = 64.0 ; 
}
	 {
	float sum = 0.0;
 {
	FOR(int, streamItVar627, 0,  < , 2, streamItVar627++) {
		pop_void(&SplitJoin34_FirFilter_Fiss_3233_3267_split[2]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin34_FirFilter_Fiss_3233_3267_split[2], i) * FirFilter_3072_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin34_FirFilter_Fiss_3233_3267_split[2]) ; 
	push_float(&SplitJoin34_FirFilter_Fiss_3233_3267_join[2], sum) ; 
 {
	pop_void(&SplitJoin34_FirFilter_Fiss_3233_3267_split[2]) ; 
}
}
//--------------------------------
// --- init: FirFilter_3073
	 {
	FirFilter_3073_s.COEFF[0] = 2.0 ; 
	FirFilter_3073_s.COEFF[1] = 4.0 ; 
	FirFilter_3073_s.COEFF[2] = 6.0 ; 
	FirFilter_3073_s.COEFF[3] = 8.0 ; 
	FirFilter_3073_s.COEFF[4] = 10.0 ; 
	FirFilter_3073_s.COEFF[5] = 12.0 ; 
	FirFilter_3073_s.COEFF[6] = 14.0 ; 
	FirFilter_3073_s.COEFF[7] = 16.0 ; 
	FirFilter_3073_s.COEFF[8] = 18.0 ; 
	FirFilter_3073_s.COEFF[9] = 20.0 ; 
	FirFilter_3073_s.COEFF[10] = 22.0 ; 
	FirFilter_3073_s.COEFF[11] = 24.0 ; 
	FirFilter_3073_s.COEFF[12] = 26.0 ; 
	FirFilter_3073_s.COEFF[13] = 28.0 ; 
	FirFilter_3073_s.COEFF[14] = 30.0 ; 
	FirFilter_3073_s.COEFF[15] = 32.0 ; 
	FirFilter_3073_s.COEFF[16] = 34.0 ; 
	FirFilter_3073_s.COEFF[17] = 36.0 ; 
	FirFilter_3073_s.COEFF[18] = 38.0 ; 
	FirFilter_3073_s.COEFF[19] = 40.0 ; 
	FirFilter_3073_s.COEFF[20] = 42.0 ; 
	FirFilter_3073_s.COEFF[21] = 44.0 ; 
	FirFilter_3073_s.COEFF[22] = 46.0 ; 
	FirFilter_3073_s.COEFF[23] = 48.0 ; 
	FirFilter_3073_s.COEFF[24] = 50.0 ; 
	FirFilter_3073_s.COEFF[25] = 52.0 ; 
	FirFilter_3073_s.COEFF[26] = 54.0 ; 
	FirFilter_3073_s.COEFF[27] = 56.0 ; 
	FirFilter_3073_s.COEFF[28] = 58.0 ; 
	FirFilter_3073_s.COEFF[29] = 60.0 ; 
	FirFilter_3073_s.COEFF[30] = 62.0 ; 
	FirFilter_3073_s.COEFF[31] = 64.0 ; 
}
	 {
	float sum = 0.0;
 {
	FOR(int, streamItVar628, 0,  < , 3, streamItVar628++) {
		pop_void(&SplitJoin34_FirFilter_Fiss_3233_3267_split[3]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin34_FirFilter_Fiss_3233_3267_split[3], i) * FirFilter_3073_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin34_FirFilter_Fiss_3233_3267_split[3]) ; 
	push_float(&SplitJoin34_FirFilter_Fiss_3233_3267_join[3], sum) ; 
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3069
	
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[1], pop_float(&SplitJoin34_FirFilter_Fiss_3233_3267_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3074
	FOR(uint32_t, __iter_init_, 0, <, 75, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin49_Delay_N_Fiss_3234_3268_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[2]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3076
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin49_Delay_N_Fiss_3234_3268_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3077
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin49_Delay_N_Fiss_3234_3268_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3078
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin49_Delay_N_Fiss_3234_3268_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3079
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin49_Delay_N_Fiss_3234_3268_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3075
	FOR(uint32_t, __iter_init_, 0, <, 46, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3075DUPLICATE_Splitter_3080, pop_float(&SplitJoin49_Delay_N_Fiss_3234_3268_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3080
	FOR(uint32_t, __iter_init_, 0, <, 183, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3075DUPLICATE_Splitter_3080);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin51_FirFilter_Fiss_3235_3269_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_3082
	 {
	FirFilter_3082_s.COEFF[0] = 21.0 ; 
	FirFilter_3082_s.COEFF[1] = 54.0 ; 
	FirFilter_3082_s.COEFF[2] = 87.0 ; 
	FirFilter_3082_s.COEFF[3] = 120.0 ; 
	FirFilter_3082_s.COEFF[4] = 153.0 ; 
	FirFilter_3082_s.COEFF[5] = 186.0 ; 
	FirFilter_3082_s.COEFF[6] = 219.0 ; 
	FirFilter_3082_s.COEFF[7] = 252.0 ; 
	FirFilter_3082_s.COEFF[8] = 285.0 ; 
	FirFilter_3082_s.COEFF[9] = 318.0 ; 
	FirFilter_3082_s.COEFF[10] = 351.0 ; 
	FirFilter_3082_s.COEFF[11] = 384.0 ; 
	FirFilter_3082_s.COEFF[12] = 417.0 ; 
	FirFilter_3082_s.COEFF[13] = 450.0 ; 
	FirFilter_3082_s.COEFF[14] = 483.0 ; 
	FirFilter_3082_s.COEFF[15] = 516.0 ; 
	FirFilter_3082_s.COEFF[16] = 549.0 ; 
	FirFilter_3082_s.COEFF[17] = 582.0 ; 
	FirFilter_3082_s.COEFF[18] = 615.0 ; 
	FirFilter_3082_s.COEFF[19] = 648.0 ; 
	FirFilter_3082_s.COEFF[20] = 681.0 ; 
	FirFilter_3082_s.COEFF[21] = 714.0 ; 
	FirFilter_3082_s.COEFF[22] = 747.0 ; 
	FirFilter_3082_s.COEFF[23] = 780.0 ; 
	FirFilter_3082_s.COEFF[24] = 813.0 ; 
	FirFilter_3082_s.COEFF[25] = 846.0 ; 
	FirFilter_3082_s.COEFF[26] = 879.0 ; 
	FirFilter_3082_s.COEFF[27] = 912.0 ; 
	FirFilter_3082_s.COEFF[28] = 945.0 ; 
	FirFilter_3082_s.COEFF[29] = 978.0 ; 
	FirFilter_3082_s.COEFF[30] = 1011.0 ; 
	FirFilter_3082_s.COEFF[31] = 1044.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin51_FirFilter_Fiss_3235_3269_split[0], i) * FirFilter_3082_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin51_FirFilter_Fiss_3235_3269_split[0]) ; 
		push_float(&SplitJoin51_FirFilter_Fiss_3235_3269_join[0], sum) ; 
 {
		FOR(int, streamItVar621, 0,  < , 3, streamItVar621++) {
			pop_void(&SplitJoin51_FirFilter_Fiss_3235_3269_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_3083
	 {
	FirFilter_3083_s.COEFF[0] = 21.0 ; 
	FirFilter_3083_s.COEFF[1] = 54.0 ; 
	FirFilter_3083_s.COEFF[2] = 87.0 ; 
	FirFilter_3083_s.COEFF[3] = 120.0 ; 
	FirFilter_3083_s.COEFF[4] = 153.0 ; 
	FirFilter_3083_s.COEFF[5] = 186.0 ; 
	FirFilter_3083_s.COEFF[6] = 219.0 ; 
	FirFilter_3083_s.COEFF[7] = 252.0 ; 
	FirFilter_3083_s.COEFF[8] = 285.0 ; 
	FirFilter_3083_s.COEFF[9] = 318.0 ; 
	FirFilter_3083_s.COEFF[10] = 351.0 ; 
	FirFilter_3083_s.COEFF[11] = 384.0 ; 
	FirFilter_3083_s.COEFF[12] = 417.0 ; 
	FirFilter_3083_s.COEFF[13] = 450.0 ; 
	FirFilter_3083_s.COEFF[14] = 483.0 ; 
	FirFilter_3083_s.COEFF[15] = 516.0 ; 
	FirFilter_3083_s.COEFF[16] = 549.0 ; 
	FirFilter_3083_s.COEFF[17] = 582.0 ; 
	FirFilter_3083_s.COEFF[18] = 615.0 ; 
	FirFilter_3083_s.COEFF[19] = 648.0 ; 
	FirFilter_3083_s.COEFF[20] = 681.0 ; 
	FirFilter_3083_s.COEFF[21] = 714.0 ; 
	FirFilter_3083_s.COEFF[22] = 747.0 ; 
	FirFilter_3083_s.COEFF[23] = 780.0 ; 
	FirFilter_3083_s.COEFF[24] = 813.0 ; 
	FirFilter_3083_s.COEFF[25] = 846.0 ; 
	FirFilter_3083_s.COEFF[26] = 879.0 ; 
	FirFilter_3083_s.COEFF[27] = 912.0 ; 
	FirFilter_3083_s.COEFF[28] = 945.0 ; 
	FirFilter_3083_s.COEFF[29] = 978.0 ; 
	FirFilter_3083_s.COEFF[30] = 1011.0 ; 
	FirFilter_3083_s.COEFF[31] = 1044.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin51_FirFilter_Fiss_3235_3269_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin51_FirFilter_Fiss_3235_3269_split[1], i) * FirFilter_3083_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin51_FirFilter_Fiss_3235_3269_split[1]) ; 
		push_float(&SplitJoin51_FirFilter_Fiss_3235_3269_join[1], sum) ; 
 {
		FOR(int, streamItVar622, 0,  < , 2, streamItVar622++) {
			pop_void(&SplitJoin51_FirFilter_Fiss_3235_3269_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_3084
	 {
	FirFilter_3084_s.COEFF[0] = 21.0 ; 
	FirFilter_3084_s.COEFF[1] = 54.0 ; 
	FirFilter_3084_s.COEFF[2] = 87.0 ; 
	FirFilter_3084_s.COEFF[3] = 120.0 ; 
	FirFilter_3084_s.COEFF[4] = 153.0 ; 
	FirFilter_3084_s.COEFF[5] = 186.0 ; 
	FirFilter_3084_s.COEFF[6] = 219.0 ; 
	FirFilter_3084_s.COEFF[7] = 252.0 ; 
	FirFilter_3084_s.COEFF[8] = 285.0 ; 
	FirFilter_3084_s.COEFF[9] = 318.0 ; 
	FirFilter_3084_s.COEFF[10] = 351.0 ; 
	FirFilter_3084_s.COEFF[11] = 384.0 ; 
	FirFilter_3084_s.COEFF[12] = 417.0 ; 
	FirFilter_3084_s.COEFF[13] = 450.0 ; 
	FirFilter_3084_s.COEFF[14] = 483.0 ; 
	FirFilter_3084_s.COEFF[15] = 516.0 ; 
	FirFilter_3084_s.COEFF[16] = 549.0 ; 
	FirFilter_3084_s.COEFF[17] = 582.0 ; 
	FirFilter_3084_s.COEFF[18] = 615.0 ; 
	FirFilter_3084_s.COEFF[19] = 648.0 ; 
	FirFilter_3084_s.COEFF[20] = 681.0 ; 
	FirFilter_3084_s.COEFF[21] = 714.0 ; 
	FirFilter_3084_s.COEFF[22] = 747.0 ; 
	FirFilter_3084_s.COEFF[23] = 780.0 ; 
	FirFilter_3084_s.COEFF[24] = 813.0 ; 
	FirFilter_3084_s.COEFF[25] = 846.0 ; 
	FirFilter_3084_s.COEFF[26] = 879.0 ; 
	FirFilter_3084_s.COEFF[27] = 912.0 ; 
	FirFilter_3084_s.COEFF[28] = 945.0 ; 
	FirFilter_3084_s.COEFF[29] = 978.0 ; 
	FirFilter_3084_s.COEFF[30] = 1011.0 ; 
	FirFilter_3084_s.COEFF[31] = 1044.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar623, 0,  < , 2, streamItVar623++) {
			pop_void(&SplitJoin51_FirFilter_Fiss_3235_3269_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin51_FirFilter_Fiss_3235_3269_split[2], i) * FirFilter_3084_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin51_FirFilter_Fiss_3235_3269_split[2]) ; 
		push_float(&SplitJoin51_FirFilter_Fiss_3235_3269_join[2], sum) ; 
 {
		pop_void(&SplitJoin51_FirFilter_Fiss_3235_3269_split[2]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_3085
	 {
	FirFilter_3085_s.COEFF[0] = 21.0 ; 
	FirFilter_3085_s.COEFF[1] = 54.0 ; 
	FirFilter_3085_s.COEFF[2] = 87.0 ; 
	FirFilter_3085_s.COEFF[3] = 120.0 ; 
	FirFilter_3085_s.COEFF[4] = 153.0 ; 
	FirFilter_3085_s.COEFF[5] = 186.0 ; 
	FirFilter_3085_s.COEFF[6] = 219.0 ; 
	FirFilter_3085_s.COEFF[7] = 252.0 ; 
	FirFilter_3085_s.COEFF[8] = 285.0 ; 
	FirFilter_3085_s.COEFF[9] = 318.0 ; 
	FirFilter_3085_s.COEFF[10] = 351.0 ; 
	FirFilter_3085_s.COEFF[11] = 384.0 ; 
	FirFilter_3085_s.COEFF[12] = 417.0 ; 
	FirFilter_3085_s.COEFF[13] = 450.0 ; 
	FirFilter_3085_s.COEFF[14] = 483.0 ; 
	FirFilter_3085_s.COEFF[15] = 516.0 ; 
	FirFilter_3085_s.COEFF[16] = 549.0 ; 
	FirFilter_3085_s.COEFF[17] = 582.0 ; 
	FirFilter_3085_s.COEFF[18] = 615.0 ; 
	FirFilter_3085_s.COEFF[19] = 648.0 ; 
	FirFilter_3085_s.COEFF[20] = 681.0 ; 
	FirFilter_3085_s.COEFF[21] = 714.0 ; 
	FirFilter_3085_s.COEFF[22] = 747.0 ; 
	FirFilter_3085_s.COEFF[23] = 780.0 ; 
	FirFilter_3085_s.COEFF[24] = 813.0 ; 
	FirFilter_3085_s.COEFF[25] = 846.0 ; 
	FirFilter_3085_s.COEFF[26] = 879.0 ; 
	FirFilter_3085_s.COEFF[27] = 912.0 ; 
	FirFilter_3085_s.COEFF[28] = 945.0 ; 
	FirFilter_3085_s.COEFF[29] = 978.0 ; 
	FirFilter_3085_s.COEFF[30] = 1011.0 ; 
	FirFilter_3085_s.COEFF[31] = 1044.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar624, 0,  < , 3, streamItVar624++) {
			pop_void(&SplitJoin51_FirFilter_Fiss_3235_3269_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin51_FirFilter_Fiss_3235_3269_split[3], i) * FirFilter_3085_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin51_FirFilter_Fiss_3235_3269_split[3]) ; 
		push_float(&SplitJoin51_FirFilter_Fiss_3235_3269_join[3], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3081
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3081DownSamp_2965, pop_float(&SplitJoin51_FirFilter_Fiss_3235_3269_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DownSamp_2965
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++) {
		push_float(&DownSamp_2965UpSamp_2966, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3081DownSamp_2965)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3081DownSamp_2965) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: UpSamp_2966
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++) {
		push_float(&UpSamp_2966WEIGHTED_ROUND_ROBIN_Splitter_3086, pop_float(&DownSamp_2965UpSamp_2966)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			push_float(&UpSamp_2966WEIGHTED_ROUND_ROBIN_Splitter_3086, 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3086
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin53_Delay_N_Fiss_3236_3270_split[__iter_], pop_float(&UpSamp_2966WEIGHTED_ROUND_ROBIN_Splitter_3086));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3088
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin53_Delay_N_Fiss_3236_3270_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3089
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin53_Delay_N_Fiss_3236_3270_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3090
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin53_Delay_N_Fiss_3236_3270_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3091
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin53_Delay_N_Fiss_3236_3270_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3087
	FOR(uint32_t, __iter_init_, 0, <, 9, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3087DUPLICATE_Splitter_3092, pop_float(&SplitJoin53_Delay_N_Fiss_3236_3270_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3092
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3087DUPLICATE_Splitter_3092);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin55_FirFilter_Fiss_3237_3271_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_3094
	 {
	FirFilter_3094_s.COEFF[0] = 6.0 ; 
	FirFilter_3094_s.COEFF[1] = 9.0 ; 
	FirFilter_3094_s.COEFF[2] = 12.0 ; 
	FirFilter_3094_s.COEFF[3] = 15.0 ; 
	FirFilter_3094_s.COEFF[4] = 18.0 ; 
	FirFilter_3094_s.COEFF[5] = 21.0 ; 
	FirFilter_3094_s.COEFF[6] = 24.0 ; 
	FirFilter_3094_s.COEFF[7] = 27.0 ; 
	FirFilter_3094_s.COEFF[8] = 30.0 ; 
	FirFilter_3094_s.COEFF[9] = 33.0 ; 
	FirFilter_3094_s.COEFF[10] = 36.0 ; 
	FirFilter_3094_s.COEFF[11] = 39.0 ; 
	FirFilter_3094_s.COEFF[12] = 42.0 ; 
	FirFilter_3094_s.COEFF[13] = 45.0 ; 
	FirFilter_3094_s.COEFF[14] = 48.0 ; 
	FirFilter_3094_s.COEFF[15] = 51.0 ; 
	FirFilter_3094_s.COEFF[16] = 54.0 ; 
	FirFilter_3094_s.COEFF[17] = 57.0 ; 
	FirFilter_3094_s.COEFF[18] = 60.0 ; 
	FirFilter_3094_s.COEFF[19] = 63.0 ; 
	FirFilter_3094_s.COEFF[20] = 66.0 ; 
	FirFilter_3094_s.COEFF[21] = 69.0 ; 
	FirFilter_3094_s.COEFF[22] = 72.0 ; 
	FirFilter_3094_s.COEFF[23] = 75.0 ; 
	FirFilter_3094_s.COEFF[24] = 78.0 ; 
	FirFilter_3094_s.COEFF[25] = 81.0 ; 
	FirFilter_3094_s.COEFF[26] = 84.0 ; 
	FirFilter_3094_s.COEFF[27] = 87.0 ; 
	FirFilter_3094_s.COEFF[28] = 90.0 ; 
	FirFilter_3094_s.COEFF[29] = 93.0 ; 
	FirFilter_3094_s.COEFF[30] = 96.0 ; 
	FirFilter_3094_s.COEFF[31] = 99.0 ; 
}
	 {
	float sum = 0.0;
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin55_FirFilter_Fiss_3237_3271_split[0], i) * FirFilter_3094_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin55_FirFilter_Fiss_3237_3271_split[0]) ; 
	push_float(&SplitJoin55_FirFilter_Fiss_3237_3271_join[0], sum) ; 
 {
	FOR(int, streamItVar617, 0,  < , 3, streamItVar617++) {
		pop_void(&SplitJoin55_FirFilter_Fiss_3237_3271_split[0]) ; 
	}
	ENDFOR
}
}
//--------------------------------
// --- init: FirFilter_3095
	 {
	FirFilter_3095_s.COEFF[0] = 6.0 ; 
	FirFilter_3095_s.COEFF[1] = 9.0 ; 
	FirFilter_3095_s.COEFF[2] = 12.0 ; 
	FirFilter_3095_s.COEFF[3] = 15.0 ; 
	FirFilter_3095_s.COEFF[4] = 18.0 ; 
	FirFilter_3095_s.COEFF[5] = 21.0 ; 
	FirFilter_3095_s.COEFF[6] = 24.0 ; 
	FirFilter_3095_s.COEFF[7] = 27.0 ; 
	FirFilter_3095_s.COEFF[8] = 30.0 ; 
	FirFilter_3095_s.COEFF[9] = 33.0 ; 
	FirFilter_3095_s.COEFF[10] = 36.0 ; 
	FirFilter_3095_s.COEFF[11] = 39.0 ; 
	FirFilter_3095_s.COEFF[12] = 42.0 ; 
	FirFilter_3095_s.COEFF[13] = 45.0 ; 
	FirFilter_3095_s.COEFF[14] = 48.0 ; 
	FirFilter_3095_s.COEFF[15] = 51.0 ; 
	FirFilter_3095_s.COEFF[16] = 54.0 ; 
	FirFilter_3095_s.COEFF[17] = 57.0 ; 
	FirFilter_3095_s.COEFF[18] = 60.0 ; 
	FirFilter_3095_s.COEFF[19] = 63.0 ; 
	FirFilter_3095_s.COEFF[20] = 66.0 ; 
	FirFilter_3095_s.COEFF[21] = 69.0 ; 
	FirFilter_3095_s.COEFF[22] = 72.0 ; 
	FirFilter_3095_s.COEFF[23] = 75.0 ; 
	FirFilter_3095_s.COEFF[24] = 78.0 ; 
	FirFilter_3095_s.COEFF[25] = 81.0 ; 
	FirFilter_3095_s.COEFF[26] = 84.0 ; 
	FirFilter_3095_s.COEFF[27] = 87.0 ; 
	FirFilter_3095_s.COEFF[28] = 90.0 ; 
	FirFilter_3095_s.COEFF[29] = 93.0 ; 
	FirFilter_3095_s.COEFF[30] = 96.0 ; 
	FirFilter_3095_s.COEFF[31] = 99.0 ; 
}
	 {
	float sum = 0.0;
 {
	pop_void(&SplitJoin55_FirFilter_Fiss_3237_3271_split[1]) ; 
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin55_FirFilter_Fiss_3237_3271_split[1], i) * FirFilter_3095_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin55_FirFilter_Fiss_3237_3271_split[1]) ; 
	push_float(&SplitJoin55_FirFilter_Fiss_3237_3271_join[1], sum) ; 
 {
	FOR(int, streamItVar618, 0,  < , 2, streamItVar618++) {
		pop_void(&SplitJoin55_FirFilter_Fiss_3237_3271_split[1]) ; 
	}
	ENDFOR
}
}
//--------------------------------
// --- init: FirFilter_3096
	 {
	FirFilter_3096_s.COEFF[0] = 6.0 ; 
	FirFilter_3096_s.COEFF[1] = 9.0 ; 
	FirFilter_3096_s.COEFF[2] = 12.0 ; 
	FirFilter_3096_s.COEFF[3] = 15.0 ; 
	FirFilter_3096_s.COEFF[4] = 18.0 ; 
	FirFilter_3096_s.COEFF[5] = 21.0 ; 
	FirFilter_3096_s.COEFF[6] = 24.0 ; 
	FirFilter_3096_s.COEFF[7] = 27.0 ; 
	FirFilter_3096_s.COEFF[8] = 30.0 ; 
	FirFilter_3096_s.COEFF[9] = 33.0 ; 
	FirFilter_3096_s.COEFF[10] = 36.0 ; 
	FirFilter_3096_s.COEFF[11] = 39.0 ; 
	FirFilter_3096_s.COEFF[12] = 42.0 ; 
	FirFilter_3096_s.COEFF[13] = 45.0 ; 
	FirFilter_3096_s.COEFF[14] = 48.0 ; 
	FirFilter_3096_s.COEFF[15] = 51.0 ; 
	FirFilter_3096_s.COEFF[16] = 54.0 ; 
	FirFilter_3096_s.COEFF[17] = 57.0 ; 
	FirFilter_3096_s.COEFF[18] = 60.0 ; 
	FirFilter_3096_s.COEFF[19] = 63.0 ; 
	FirFilter_3096_s.COEFF[20] = 66.0 ; 
	FirFilter_3096_s.COEFF[21] = 69.0 ; 
	FirFilter_3096_s.COEFF[22] = 72.0 ; 
	FirFilter_3096_s.COEFF[23] = 75.0 ; 
	FirFilter_3096_s.COEFF[24] = 78.0 ; 
	FirFilter_3096_s.COEFF[25] = 81.0 ; 
	FirFilter_3096_s.COEFF[26] = 84.0 ; 
	FirFilter_3096_s.COEFF[27] = 87.0 ; 
	FirFilter_3096_s.COEFF[28] = 90.0 ; 
	FirFilter_3096_s.COEFF[29] = 93.0 ; 
	FirFilter_3096_s.COEFF[30] = 96.0 ; 
	FirFilter_3096_s.COEFF[31] = 99.0 ; 
}
	 {
	float sum = 0.0;
 {
	FOR(int, streamItVar619, 0,  < , 2, streamItVar619++) {
		pop_void(&SplitJoin55_FirFilter_Fiss_3237_3271_split[2]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin55_FirFilter_Fiss_3237_3271_split[2], i) * FirFilter_3096_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin55_FirFilter_Fiss_3237_3271_split[2]) ; 
	push_float(&SplitJoin55_FirFilter_Fiss_3237_3271_join[2], sum) ; 
 {
	pop_void(&SplitJoin55_FirFilter_Fiss_3237_3271_split[2]) ; 
}
}
//--------------------------------
// --- init: FirFilter_3097
	 {
	FirFilter_3097_s.COEFF[0] = 6.0 ; 
	FirFilter_3097_s.COEFF[1] = 9.0 ; 
	FirFilter_3097_s.COEFF[2] = 12.0 ; 
	FirFilter_3097_s.COEFF[3] = 15.0 ; 
	FirFilter_3097_s.COEFF[4] = 18.0 ; 
	FirFilter_3097_s.COEFF[5] = 21.0 ; 
	FirFilter_3097_s.COEFF[6] = 24.0 ; 
	FirFilter_3097_s.COEFF[7] = 27.0 ; 
	FirFilter_3097_s.COEFF[8] = 30.0 ; 
	FirFilter_3097_s.COEFF[9] = 33.0 ; 
	FirFilter_3097_s.COEFF[10] = 36.0 ; 
	FirFilter_3097_s.COEFF[11] = 39.0 ; 
	FirFilter_3097_s.COEFF[12] = 42.0 ; 
	FirFilter_3097_s.COEFF[13] = 45.0 ; 
	FirFilter_3097_s.COEFF[14] = 48.0 ; 
	FirFilter_3097_s.COEFF[15] = 51.0 ; 
	FirFilter_3097_s.COEFF[16] = 54.0 ; 
	FirFilter_3097_s.COEFF[17] = 57.0 ; 
	FirFilter_3097_s.COEFF[18] = 60.0 ; 
	FirFilter_3097_s.COEFF[19] = 63.0 ; 
	FirFilter_3097_s.COEFF[20] = 66.0 ; 
	FirFilter_3097_s.COEFF[21] = 69.0 ; 
	FirFilter_3097_s.COEFF[22] = 72.0 ; 
	FirFilter_3097_s.COEFF[23] = 75.0 ; 
	FirFilter_3097_s.COEFF[24] = 78.0 ; 
	FirFilter_3097_s.COEFF[25] = 81.0 ; 
	FirFilter_3097_s.COEFF[26] = 84.0 ; 
	FirFilter_3097_s.COEFF[27] = 87.0 ; 
	FirFilter_3097_s.COEFF[28] = 90.0 ; 
	FirFilter_3097_s.COEFF[29] = 93.0 ; 
	FirFilter_3097_s.COEFF[30] = 96.0 ; 
	FirFilter_3097_s.COEFF[31] = 99.0 ; 
}
	 {
	float sum = 0.0;
 {
	FOR(int, streamItVar620, 0,  < , 3, streamItVar620++) {
		pop_void(&SplitJoin55_FirFilter_Fiss_3237_3271_split[3]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin55_FirFilter_Fiss_3237_3271_split[3], i) * FirFilter_3097_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin55_FirFilter_Fiss_3237_3271_split[3]) ; 
	push_float(&SplitJoin55_FirFilter_Fiss_3237_3271_join[3], sum) ; 
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3093
	
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[2], pop_float(&SplitJoin55_FirFilter_Fiss_3237_3271_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3098
	FOR(uint32_t, __iter_init_, 0, <, 75, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin70_Delay_N_Fiss_3238_3272_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[3]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3100
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin70_Delay_N_Fiss_3238_3272_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3101
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin70_Delay_N_Fiss_3238_3272_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3102
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin70_Delay_N_Fiss_3238_3272_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3103
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin70_Delay_N_Fiss_3238_3272_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3099
	FOR(uint32_t, __iter_init_, 0, <, 46, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3099DUPLICATE_Splitter_3104, pop_float(&SplitJoin70_Delay_N_Fiss_3238_3272_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3104
	FOR(uint32_t, __iter_init_, 0, <, 183, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3099DUPLICATE_Splitter_3104);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin72_FirFilter_Fiss_3239_3273_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_3106
	 {
	FirFilter_3106_s.COEFF[0] = 31.0 ; 
	FirFilter_3106_s.COEFF[1] = 64.0 ; 
	FirFilter_3106_s.COEFF[2] = 97.0 ; 
	FirFilter_3106_s.COEFF[3] = 130.0 ; 
	FirFilter_3106_s.COEFF[4] = 163.0 ; 
	FirFilter_3106_s.COEFF[5] = 196.0 ; 
	FirFilter_3106_s.COEFF[6] = 229.0 ; 
	FirFilter_3106_s.COEFF[7] = 262.0 ; 
	FirFilter_3106_s.COEFF[8] = 295.0 ; 
	FirFilter_3106_s.COEFF[9] = 328.0 ; 
	FirFilter_3106_s.COEFF[10] = 361.0 ; 
	FirFilter_3106_s.COEFF[11] = 394.0 ; 
	FirFilter_3106_s.COEFF[12] = 427.0 ; 
	FirFilter_3106_s.COEFF[13] = 460.0 ; 
	FirFilter_3106_s.COEFF[14] = 493.0 ; 
	FirFilter_3106_s.COEFF[15] = 526.0 ; 
	FirFilter_3106_s.COEFF[16] = 559.0 ; 
	FirFilter_3106_s.COEFF[17] = 592.0 ; 
	FirFilter_3106_s.COEFF[18] = 625.0 ; 
	FirFilter_3106_s.COEFF[19] = 658.0 ; 
	FirFilter_3106_s.COEFF[20] = 691.0 ; 
	FirFilter_3106_s.COEFF[21] = 724.0 ; 
	FirFilter_3106_s.COEFF[22] = 757.0 ; 
	FirFilter_3106_s.COEFF[23] = 790.0 ; 
	FirFilter_3106_s.COEFF[24] = 823.0 ; 
	FirFilter_3106_s.COEFF[25] = 856.0 ; 
	FirFilter_3106_s.COEFF[26] = 889.0 ; 
	FirFilter_3106_s.COEFF[27] = 922.0 ; 
	FirFilter_3106_s.COEFF[28] = 955.0 ; 
	FirFilter_3106_s.COEFF[29] = 988.0 ; 
	FirFilter_3106_s.COEFF[30] = 1021.0 ; 
	FirFilter_3106_s.COEFF[31] = 1054.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin72_FirFilter_Fiss_3239_3273_split[0], i) * FirFilter_3106_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin72_FirFilter_Fiss_3239_3273_split[0]) ; 
		push_float(&SplitJoin72_FirFilter_Fiss_3239_3273_join[0], sum) ; 
 {
		FOR(int, streamItVar613, 0,  < , 3, streamItVar613++) {
			pop_void(&SplitJoin72_FirFilter_Fiss_3239_3273_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_3107
	 {
	FirFilter_3107_s.COEFF[0] = 31.0 ; 
	FirFilter_3107_s.COEFF[1] = 64.0 ; 
	FirFilter_3107_s.COEFF[2] = 97.0 ; 
	FirFilter_3107_s.COEFF[3] = 130.0 ; 
	FirFilter_3107_s.COEFF[4] = 163.0 ; 
	FirFilter_3107_s.COEFF[5] = 196.0 ; 
	FirFilter_3107_s.COEFF[6] = 229.0 ; 
	FirFilter_3107_s.COEFF[7] = 262.0 ; 
	FirFilter_3107_s.COEFF[8] = 295.0 ; 
	FirFilter_3107_s.COEFF[9] = 328.0 ; 
	FirFilter_3107_s.COEFF[10] = 361.0 ; 
	FirFilter_3107_s.COEFF[11] = 394.0 ; 
	FirFilter_3107_s.COEFF[12] = 427.0 ; 
	FirFilter_3107_s.COEFF[13] = 460.0 ; 
	FirFilter_3107_s.COEFF[14] = 493.0 ; 
	FirFilter_3107_s.COEFF[15] = 526.0 ; 
	FirFilter_3107_s.COEFF[16] = 559.0 ; 
	FirFilter_3107_s.COEFF[17] = 592.0 ; 
	FirFilter_3107_s.COEFF[18] = 625.0 ; 
	FirFilter_3107_s.COEFF[19] = 658.0 ; 
	FirFilter_3107_s.COEFF[20] = 691.0 ; 
	FirFilter_3107_s.COEFF[21] = 724.0 ; 
	FirFilter_3107_s.COEFF[22] = 757.0 ; 
	FirFilter_3107_s.COEFF[23] = 790.0 ; 
	FirFilter_3107_s.COEFF[24] = 823.0 ; 
	FirFilter_3107_s.COEFF[25] = 856.0 ; 
	FirFilter_3107_s.COEFF[26] = 889.0 ; 
	FirFilter_3107_s.COEFF[27] = 922.0 ; 
	FirFilter_3107_s.COEFF[28] = 955.0 ; 
	FirFilter_3107_s.COEFF[29] = 988.0 ; 
	FirFilter_3107_s.COEFF[30] = 1021.0 ; 
	FirFilter_3107_s.COEFF[31] = 1054.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin72_FirFilter_Fiss_3239_3273_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin72_FirFilter_Fiss_3239_3273_split[1], i) * FirFilter_3107_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin72_FirFilter_Fiss_3239_3273_split[1]) ; 
		push_float(&SplitJoin72_FirFilter_Fiss_3239_3273_join[1], sum) ; 
 {
		FOR(int, streamItVar614, 0,  < , 2, streamItVar614++) {
			pop_void(&SplitJoin72_FirFilter_Fiss_3239_3273_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_3108
	 {
	FirFilter_3108_s.COEFF[0] = 31.0 ; 
	FirFilter_3108_s.COEFF[1] = 64.0 ; 
	FirFilter_3108_s.COEFF[2] = 97.0 ; 
	FirFilter_3108_s.COEFF[3] = 130.0 ; 
	FirFilter_3108_s.COEFF[4] = 163.0 ; 
	FirFilter_3108_s.COEFF[5] = 196.0 ; 
	FirFilter_3108_s.COEFF[6] = 229.0 ; 
	FirFilter_3108_s.COEFF[7] = 262.0 ; 
	FirFilter_3108_s.COEFF[8] = 295.0 ; 
	FirFilter_3108_s.COEFF[9] = 328.0 ; 
	FirFilter_3108_s.COEFF[10] = 361.0 ; 
	FirFilter_3108_s.COEFF[11] = 394.0 ; 
	FirFilter_3108_s.COEFF[12] = 427.0 ; 
	FirFilter_3108_s.COEFF[13] = 460.0 ; 
	FirFilter_3108_s.COEFF[14] = 493.0 ; 
	FirFilter_3108_s.COEFF[15] = 526.0 ; 
	FirFilter_3108_s.COEFF[16] = 559.0 ; 
	FirFilter_3108_s.COEFF[17] = 592.0 ; 
	FirFilter_3108_s.COEFF[18] = 625.0 ; 
	FirFilter_3108_s.COEFF[19] = 658.0 ; 
	FirFilter_3108_s.COEFF[20] = 691.0 ; 
	FirFilter_3108_s.COEFF[21] = 724.0 ; 
	FirFilter_3108_s.COEFF[22] = 757.0 ; 
	FirFilter_3108_s.COEFF[23] = 790.0 ; 
	FirFilter_3108_s.COEFF[24] = 823.0 ; 
	FirFilter_3108_s.COEFF[25] = 856.0 ; 
	FirFilter_3108_s.COEFF[26] = 889.0 ; 
	FirFilter_3108_s.COEFF[27] = 922.0 ; 
	FirFilter_3108_s.COEFF[28] = 955.0 ; 
	FirFilter_3108_s.COEFF[29] = 988.0 ; 
	FirFilter_3108_s.COEFF[30] = 1021.0 ; 
	FirFilter_3108_s.COEFF[31] = 1054.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar615, 0,  < , 2, streamItVar615++) {
			pop_void(&SplitJoin72_FirFilter_Fiss_3239_3273_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin72_FirFilter_Fiss_3239_3273_split[2], i) * FirFilter_3108_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin72_FirFilter_Fiss_3239_3273_split[2]) ; 
		push_float(&SplitJoin72_FirFilter_Fiss_3239_3273_join[2], sum) ; 
 {
		pop_void(&SplitJoin72_FirFilter_Fiss_3239_3273_split[2]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_3109
	 {
	FirFilter_3109_s.COEFF[0] = 31.0 ; 
	FirFilter_3109_s.COEFF[1] = 64.0 ; 
	FirFilter_3109_s.COEFF[2] = 97.0 ; 
	FirFilter_3109_s.COEFF[3] = 130.0 ; 
	FirFilter_3109_s.COEFF[4] = 163.0 ; 
	FirFilter_3109_s.COEFF[5] = 196.0 ; 
	FirFilter_3109_s.COEFF[6] = 229.0 ; 
	FirFilter_3109_s.COEFF[7] = 262.0 ; 
	FirFilter_3109_s.COEFF[8] = 295.0 ; 
	FirFilter_3109_s.COEFF[9] = 328.0 ; 
	FirFilter_3109_s.COEFF[10] = 361.0 ; 
	FirFilter_3109_s.COEFF[11] = 394.0 ; 
	FirFilter_3109_s.COEFF[12] = 427.0 ; 
	FirFilter_3109_s.COEFF[13] = 460.0 ; 
	FirFilter_3109_s.COEFF[14] = 493.0 ; 
	FirFilter_3109_s.COEFF[15] = 526.0 ; 
	FirFilter_3109_s.COEFF[16] = 559.0 ; 
	FirFilter_3109_s.COEFF[17] = 592.0 ; 
	FirFilter_3109_s.COEFF[18] = 625.0 ; 
	FirFilter_3109_s.COEFF[19] = 658.0 ; 
	FirFilter_3109_s.COEFF[20] = 691.0 ; 
	FirFilter_3109_s.COEFF[21] = 724.0 ; 
	FirFilter_3109_s.COEFF[22] = 757.0 ; 
	FirFilter_3109_s.COEFF[23] = 790.0 ; 
	FirFilter_3109_s.COEFF[24] = 823.0 ; 
	FirFilter_3109_s.COEFF[25] = 856.0 ; 
	FirFilter_3109_s.COEFF[26] = 889.0 ; 
	FirFilter_3109_s.COEFF[27] = 922.0 ; 
	FirFilter_3109_s.COEFF[28] = 955.0 ; 
	FirFilter_3109_s.COEFF[29] = 988.0 ; 
	FirFilter_3109_s.COEFF[30] = 1021.0 ; 
	FirFilter_3109_s.COEFF[31] = 1054.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar616, 0,  < , 3, streamItVar616++) {
			pop_void(&SplitJoin72_FirFilter_Fiss_3239_3273_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin72_FirFilter_Fiss_3239_3273_split[3], i) * FirFilter_3109_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin72_FirFilter_Fiss_3239_3273_split[3]) ; 
		push_float(&SplitJoin72_FirFilter_Fiss_3239_3273_join[3], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3105
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3105DownSamp_2972, pop_float(&SplitJoin72_FirFilter_Fiss_3239_3273_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DownSamp_2972
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++) {
		push_float(&DownSamp_2972UpSamp_2973, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3105DownSamp_2972)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3105DownSamp_2972) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: UpSamp_2973
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++) {
		push_float(&UpSamp_2973WEIGHTED_ROUND_ROBIN_Splitter_3110, pop_float(&DownSamp_2972UpSamp_2973)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			push_float(&UpSamp_2973WEIGHTED_ROUND_ROBIN_Splitter_3110, 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3110
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin74_Delay_N_Fiss_3240_3274_split[__iter_], pop_float(&UpSamp_2973WEIGHTED_ROUND_ROBIN_Splitter_3110));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3112
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin74_Delay_N_Fiss_3240_3274_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3113
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin74_Delay_N_Fiss_3240_3274_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3114
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin74_Delay_N_Fiss_3240_3274_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3115
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin74_Delay_N_Fiss_3240_3274_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3111
	FOR(uint32_t, __iter_init_, 0, <, 9, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3111DUPLICATE_Splitter_3116, pop_float(&SplitJoin74_Delay_N_Fiss_3240_3274_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3116
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3111DUPLICATE_Splitter_3116);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin76_FirFilter_Fiss_3241_3275_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_3118
	 {
	FirFilter_3118_s.COEFF[0] = 12.0 ; 
	FirFilter_3118_s.COEFF[1] = 16.0 ; 
	FirFilter_3118_s.COEFF[2] = 20.0 ; 
	FirFilter_3118_s.COEFF[3] = 24.0 ; 
	FirFilter_3118_s.COEFF[4] = 28.0 ; 
	FirFilter_3118_s.COEFF[5] = 32.0 ; 
	FirFilter_3118_s.COEFF[6] = 36.0 ; 
	FirFilter_3118_s.COEFF[7] = 40.0 ; 
	FirFilter_3118_s.COEFF[8] = 44.0 ; 
	FirFilter_3118_s.COEFF[9] = 48.0 ; 
	FirFilter_3118_s.COEFF[10] = 52.0 ; 
	FirFilter_3118_s.COEFF[11] = 56.0 ; 
	FirFilter_3118_s.COEFF[12] = 60.0 ; 
	FirFilter_3118_s.COEFF[13] = 64.0 ; 
	FirFilter_3118_s.COEFF[14] = 68.0 ; 
	FirFilter_3118_s.COEFF[15] = 72.0 ; 
	FirFilter_3118_s.COEFF[16] = 76.0 ; 
	FirFilter_3118_s.COEFF[17] = 80.0 ; 
	FirFilter_3118_s.COEFF[18] = 84.0 ; 
	FirFilter_3118_s.COEFF[19] = 88.0 ; 
	FirFilter_3118_s.COEFF[20] = 92.0 ; 
	FirFilter_3118_s.COEFF[21] = 96.0 ; 
	FirFilter_3118_s.COEFF[22] = 100.0 ; 
	FirFilter_3118_s.COEFF[23] = 104.0 ; 
	FirFilter_3118_s.COEFF[24] = 108.0 ; 
	FirFilter_3118_s.COEFF[25] = 112.0 ; 
	FirFilter_3118_s.COEFF[26] = 116.0 ; 
	FirFilter_3118_s.COEFF[27] = 120.0 ; 
	FirFilter_3118_s.COEFF[28] = 124.0 ; 
	FirFilter_3118_s.COEFF[29] = 128.0 ; 
	FirFilter_3118_s.COEFF[30] = 132.0 ; 
	FirFilter_3118_s.COEFF[31] = 136.0 ; 
}
	 {
	float sum = 0.0;
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin76_FirFilter_Fiss_3241_3275_split[0], i) * FirFilter_3118_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin76_FirFilter_Fiss_3241_3275_split[0]) ; 
	push_float(&SplitJoin76_FirFilter_Fiss_3241_3275_join[0], sum) ; 
 {
	FOR(int, streamItVar609, 0,  < , 3, streamItVar609++) {
		pop_void(&SplitJoin76_FirFilter_Fiss_3241_3275_split[0]) ; 
	}
	ENDFOR
}
}
//--------------------------------
// --- init: FirFilter_3119
	 {
	FirFilter_3119_s.COEFF[0] = 12.0 ; 
	FirFilter_3119_s.COEFF[1] = 16.0 ; 
	FirFilter_3119_s.COEFF[2] = 20.0 ; 
	FirFilter_3119_s.COEFF[3] = 24.0 ; 
	FirFilter_3119_s.COEFF[4] = 28.0 ; 
	FirFilter_3119_s.COEFF[5] = 32.0 ; 
	FirFilter_3119_s.COEFF[6] = 36.0 ; 
	FirFilter_3119_s.COEFF[7] = 40.0 ; 
	FirFilter_3119_s.COEFF[8] = 44.0 ; 
	FirFilter_3119_s.COEFF[9] = 48.0 ; 
	FirFilter_3119_s.COEFF[10] = 52.0 ; 
	FirFilter_3119_s.COEFF[11] = 56.0 ; 
	FirFilter_3119_s.COEFF[12] = 60.0 ; 
	FirFilter_3119_s.COEFF[13] = 64.0 ; 
	FirFilter_3119_s.COEFF[14] = 68.0 ; 
	FirFilter_3119_s.COEFF[15] = 72.0 ; 
	FirFilter_3119_s.COEFF[16] = 76.0 ; 
	FirFilter_3119_s.COEFF[17] = 80.0 ; 
	FirFilter_3119_s.COEFF[18] = 84.0 ; 
	FirFilter_3119_s.COEFF[19] = 88.0 ; 
	FirFilter_3119_s.COEFF[20] = 92.0 ; 
	FirFilter_3119_s.COEFF[21] = 96.0 ; 
	FirFilter_3119_s.COEFF[22] = 100.0 ; 
	FirFilter_3119_s.COEFF[23] = 104.0 ; 
	FirFilter_3119_s.COEFF[24] = 108.0 ; 
	FirFilter_3119_s.COEFF[25] = 112.0 ; 
	FirFilter_3119_s.COEFF[26] = 116.0 ; 
	FirFilter_3119_s.COEFF[27] = 120.0 ; 
	FirFilter_3119_s.COEFF[28] = 124.0 ; 
	FirFilter_3119_s.COEFF[29] = 128.0 ; 
	FirFilter_3119_s.COEFF[30] = 132.0 ; 
	FirFilter_3119_s.COEFF[31] = 136.0 ; 
}
	 {
	float sum = 0.0;
 {
	pop_void(&SplitJoin76_FirFilter_Fiss_3241_3275_split[1]) ; 
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin76_FirFilter_Fiss_3241_3275_split[1], i) * FirFilter_3119_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin76_FirFilter_Fiss_3241_3275_split[1]) ; 
	push_float(&SplitJoin76_FirFilter_Fiss_3241_3275_join[1], sum) ; 
 {
	FOR(int, streamItVar610, 0,  < , 2, streamItVar610++) {
		pop_void(&SplitJoin76_FirFilter_Fiss_3241_3275_split[1]) ; 
	}
	ENDFOR
}
}
//--------------------------------
// --- init: FirFilter_3120
	 {
	FirFilter_3120_s.COEFF[0] = 12.0 ; 
	FirFilter_3120_s.COEFF[1] = 16.0 ; 
	FirFilter_3120_s.COEFF[2] = 20.0 ; 
	FirFilter_3120_s.COEFF[3] = 24.0 ; 
	FirFilter_3120_s.COEFF[4] = 28.0 ; 
	FirFilter_3120_s.COEFF[5] = 32.0 ; 
	FirFilter_3120_s.COEFF[6] = 36.0 ; 
	FirFilter_3120_s.COEFF[7] = 40.0 ; 
	FirFilter_3120_s.COEFF[8] = 44.0 ; 
	FirFilter_3120_s.COEFF[9] = 48.0 ; 
	FirFilter_3120_s.COEFF[10] = 52.0 ; 
	FirFilter_3120_s.COEFF[11] = 56.0 ; 
	FirFilter_3120_s.COEFF[12] = 60.0 ; 
	FirFilter_3120_s.COEFF[13] = 64.0 ; 
	FirFilter_3120_s.COEFF[14] = 68.0 ; 
	FirFilter_3120_s.COEFF[15] = 72.0 ; 
	FirFilter_3120_s.COEFF[16] = 76.0 ; 
	FirFilter_3120_s.COEFF[17] = 80.0 ; 
	FirFilter_3120_s.COEFF[18] = 84.0 ; 
	FirFilter_3120_s.COEFF[19] = 88.0 ; 
	FirFilter_3120_s.COEFF[20] = 92.0 ; 
	FirFilter_3120_s.COEFF[21] = 96.0 ; 
	FirFilter_3120_s.COEFF[22] = 100.0 ; 
	FirFilter_3120_s.COEFF[23] = 104.0 ; 
	FirFilter_3120_s.COEFF[24] = 108.0 ; 
	FirFilter_3120_s.COEFF[25] = 112.0 ; 
	FirFilter_3120_s.COEFF[26] = 116.0 ; 
	FirFilter_3120_s.COEFF[27] = 120.0 ; 
	FirFilter_3120_s.COEFF[28] = 124.0 ; 
	FirFilter_3120_s.COEFF[29] = 128.0 ; 
	FirFilter_3120_s.COEFF[30] = 132.0 ; 
	FirFilter_3120_s.COEFF[31] = 136.0 ; 
}
	 {
	float sum = 0.0;
 {
	FOR(int, streamItVar611, 0,  < , 2, streamItVar611++) {
		pop_void(&SplitJoin76_FirFilter_Fiss_3241_3275_split[2]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin76_FirFilter_Fiss_3241_3275_split[2], i) * FirFilter_3120_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin76_FirFilter_Fiss_3241_3275_split[2]) ; 
	push_float(&SplitJoin76_FirFilter_Fiss_3241_3275_join[2], sum) ; 
 {
	pop_void(&SplitJoin76_FirFilter_Fiss_3241_3275_split[2]) ; 
}
}
//--------------------------------
// --- init: FirFilter_3121
	 {
	FirFilter_3121_s.COEFF[0] = 12.0 ; 
	FirFilter_3121_s.COEFF[1] = 16.0 ; 
	FirFilter_3121_s.COEFF[2] = 20.0 ; 
	FirFilter_3121_s.COEFF[3] = 24.0 ; 
	FirFilter_3121_s.COEFF[4] = 28.0 ; 
	FirFilter_3121_s.COEFF[5] = 32.0 ; 
	FirFilter_3121_s.COEFF[6] = 36.0 ; 
	FirFilter_3121_s.COEFF[7] = 40.0 ; 
	FirFilter_3121_s.COEFF[8] = 44.0 ; 
	FirFilter_3121_s.COEFF[9] = 48.0 ; 
	FirFilter_3121_s.COEFF[10] = 52.0 ; 
	FirFilter_3121_s.COEFF[11] = 56.0 ; 
	FirFilter_3121_s.COEFF[12] = 60.0 ; 
	FirFilter_3121_s.COEFF[13] = 64.0 ; 
	FirFilter_3121_s.COEFF[14] = 68.0 ; 
	FirFilter_3121_s.COEFF[15] = 72.0 ; 
	FirFilter_3121_s.COEFF[16] = 76.0 ; 
	FirFilter_3121_s.COEFF[17] = 80.0 ; 
	FirFilter_3121_s.COEFF[18] = 84.0 ; 
	FirFilter_3121_s.COEFF[19] = 88.0 ; 
	FirFilter_3121_s.COEFF[20] = 92.0 ; 
	FirFilter_3121_s.COEFF[21] = 96.0 ; 
	FirFilter_3121_s.COEFF[22] = 100.0 ; 
	FirFilter_3121_s.COEFF[23] = 104.0 ; 
	FirFilter_3121_s.COEFF[24] = 108.0 ; 
	FirFilter_3121_s.COEFF[25] = 112.0 ; 
	FirFilter_3121_s.COEFF[26] = 116.0 ; 
	FirFilter_3121_s.COEFF[27] = 120.0 ; 
	FirFilter_3121_s.COEFF[28] = 124.0 ; 
	FirFilter_3121_s.COEFF[29] = 128.0 ; 
	FirFilter_3121_s.COEFF[30] = 132.0 ; 
	FirFilter_3121_s.COEFF[31] = 136.0 ; 
}
	 {
	float sum = 0.0;
 {
	FOR(int, streamItVar612, 0,  < , 3, streamItVar612++) {
		pop_void(&SplitJoin76_FirFilter_Fiss_3241_3275_split[3]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin76_FirFilter_Fiss_3241_3275_split[3], i) * FirFilter_3121_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin76_FirFilter_Fiss_3241_3275_split[3]) ; 
	push_float(&SplitJoin76_FirFilter_Fiss_3241_3275_join[3], sum) ; 
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3117
	
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[3], pop_float(&SplitJoin76_FirFilter_Fiss_3241_3275_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3122
	FOR(uint32_t, __iter_init_, 0, <, 75, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin91_Delay_N_Fiss_3242_3276_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[4]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3124
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin91_Delay_N_Fiss_3242_3276_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3125
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin91_Delay_N_Fiss_3242_3276_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3126
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin91_Delay_N_Fiss_3242_3276_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3127
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin91_Delay_N_Fiss_3242_3276_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3123
	FOR(uint32_t, __iter_init_, 0, <, 46, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3123DUPLICATE_Splitter_3128, pop_float(&SplitJoin91_Delay_N_Fiss_3242_3276_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3128
	FOR(uint32_t, __iter_init_, 0, <, 183, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3123DUPLICATE_Splitter_3128);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin93_FirFilter_Fiss_3243_3277_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_3130
	 {
	FirFilter_3130_s.COEFF[0] = 41.0 ; 
	FirFilter_3130_s.COEFF[1] = 74.0 ; 
	FirFilter_3130_s.COEFF[2] = 107.0 ; 
	FirFilter_3130_s.COEFF[3] = 140.0 ; 
	FirFilter_3130_s.COEFF[4] = 173.0 ; 
	FirFilter_3130_s.COEFF[5] = 206.0 ; 
	FirFilter_3130_s.COEFF[6] = 239.0 ; 
	FirFilter_3130_s.COEFF[7] = 272.0 ; 
	FirFilter_3130_s.COEFF[8] = 305.0 ; 
	FirFilter_3130_s.COEFF[9] = 338.0 ; 
	FirFilter_3130_s.COEFF[10] = 371.0 ; 
	FirFilter_3130_s.COEFF[11] = 404.0 ; 
	FirFilter_3130_s.COEFF[12] = 437.0 ; 
	FirFilter_3130_s.COEFF[13] = 470.0 ; 
	FirFilter_3130_s.COEFF[14] = 503.0 ; 
	FirFilter_3130_s.COEFF[15] = 536.0 ; 
	FirFilter_3130_s.COEFF[16] = 569.0 ; 
	FirFilter_3130_s.COEFF[17] = 602.0 ; 
	FirFilter_3130_s.COEFF[18] = 635.0 ; 
	FirFilter_3130_s.COEFF[19] = 668.0 ; 
	FirFilter_3130_s.COEFF[20] = 701.0 ; 
	FirFilter_3130_s.COEFF[21] = 734.0 ; 
	FirFilter_3130_s.COEFF[22] = 767.0 ; 
	FirFilter_3130_s.COEFF[23] = 800.0 ; 
	FirFilter_3130_s.COEFF[24] = 833.0 ; 
	FirFilter_3130_s.COEFF[25] = 866.0 ; 
	FirFilter_3130_s.COEFF[26] = 899.0 ; 
	FirFilter_3130_s.COEFF[27] = 932.0 ; 
	FirFilter_3130_s.COEFF[28] = 965.0 ; 
	FirFilter_3130_s.COEFF[29] = 998.0 ; 
	FirFilter_3130_s.COEFF[30] = 1031.0 ; 
	FirFilter_3130_s.COEFF[31] = 1064.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin93_FirFilter_Fiss_3243_3277_split[0], i) * FirFilter_3130_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin93_FirFilter_Fiss_3243_3277_split[0]) ; 
		push_float(&SplitJoin93_FirFilter_Fiss_3243_3277_join[0], sum) ; 
 {
		FOR(int, streamItVar605, 0,  < , 3, streamItVar605++) {
			pop_void(&SplitJoin93_FirFilter_Fiss_3243_3277_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_3131
	 {
	FirFilter_3131_s.COEFF[0] = 41.0 ; 
	FirFilter_3131_s.COEFF[1] = 74.0 ; 
	FirFilter_3131_s.COEFF[2] = 107.0 ; 
	FirFilter_3131_s.COEFF[3] = 140.0 ; 
	FirFilter_3131_s.COEFF[4] = 173.0 ; 
	FirFilter_3131_s.COEFF[5] = 206.0 ; 
	FirFilter_3131_s.COEFF[6] = 239.0 ; 
	FirFilter_3131_s.COEFF[7] = 272.0 ; 
	FirFilter_3131_s.COEFF[8] = 305.0 ; 
	FirFilter_3131_s.COEFF[9] = 338.0 ; 
	FirFilter_3131_s.COEFF[10] = 371.0 ; 
	FirFilter_3131_s.COEFF[11] = 404.0 ; 
	FirFilter_3131_s.COEFF[12] = 437.0 ; 
	FirFilter_3131_s.COEFF[13] = 470.0 ; 
	FirFilter_3131_s.COEFF[14] = 503.0 ; 
	FirFilter_3131_s.COEFF[15] = 536.0 ; 
	FirFilter_3131_s.COEFF[16] = 569.0 ; 
	FirFilter_3131_s.COEFF[17] = 602.0 ; 
	FirFilter_3131_s.COEFF[18] = 635.0 ; 
	FirFilter_3131_s.COEFF[19] = 668.0 ; 
	FirFilter_3131_s.COEFF[20] = 701.0 ; 
	FirFilter_3131_s.COEFF[21] = 734.0 ; 
	FirFilter_3131_s.COEFF[22] = 767.0 ; 
	FirFilter_3131_s.COEFF[23] = 800.0 ; 
	FirFilter_3131_s.COEFF[24] = 833.0 ; 
	FirFilter_3131_s.COEFF[25] = 866.0 ; 
	FirFilter_3131_s.COEFF[26] = 899.0 ; 
	FirFilter_3131_s.COEFF[27] = 932.0 ; 
	FirFilter_3131_s.COEFF[28] = 965.0 ; 
	FirFilter_3131_s.COEFF[29] = 998.0 ; 
	FirFilter_3131_s.COEFF[30] = 1031.0 ; 
	FirFilter_3131_s.COEFF[31] = 1064.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin93_FirFilter_Fiss_3243_3277_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin93_FirFilter_Fiss_3243_3277_split[1], i) * FirFilter_3131_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin93_FirFilter_Fiss_3243_3277_split[1]) ; 
		push_float(&SplitJoin93_FirFilter_Fiss_3243_3277_join[1], sum) ; 
 {
		FOR(int, streamItVar606, 0,  < , 2, streamItVar606++) {
			pop_void(&SplitJoin93_FirFilter_Fiss_3243_3277_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_3132
	 {
	FirFilter_3132_s.COEFF[0] = 41.0 ; 
	FirFilter_3132_s.COEFF[1] = 74.0 ; 
	FirFilter_3132_s.COEFF[2] = 107.0 ; 
	FirFilter_3132_s.COEFF[3] = 140.0 ; 
	FirFilter_3132_s.COEFF[4] = 173.0 ; 
	FirFilter_3132_s.COEFF[5] = 206.0 ; 
	FirFilter_3132_s.COEFF[6] = 239.0 ; 
	FirFilter_3132_s.COEFF[7] = 272.0 ; 
	FirFilter_3132_s.COEFF[8] = 305.0 ; 
	FirFilter_3132_s.COEFF[9] = 338.0 ; 
	FirFilter_3132_s.COEFF[10] = 371.0 ; 
	FirFilter_3132_s.COEFF[11] = 404.0 ; 
	FirFilter_3132_s.COEFF[12] = 437.0 ; 
	FirFilter_3132_s.COEFF[13] = 470.0 ; 
	FirFilter_3132_s.COEFF[14] = 503.0 ; 
	FirFilter_3132_s.COEFF[15] = 536.0 ; 
	FirFilter_3132_s.COEFF[16] = 569.0 ; 
	FirFilter_3132_s.COEFF[17] = 602.0 ; 
	FirFilter_3132_s.COEFF[18] = 635.0 ; 
	FirFilter_3132_s.COEFF[19] = 668.0 ; 
	FirFilter_3132_s.COEFF[20] = 701.0 ; 
	FirFilter_3132_s.COEFF[21] = 734.0 ; 
	FirFilter_3132_s.COEFF[22] = 767.0 ; 
	FirFilter_3132_s.COEFF[23] = 800.0 ; 
	FirFilter_3132_s.COEFF[24] = 833.0 ; 
	FirFilter_3132_s.COEFF[25] = 866.0 ; 
	FirFilter_3132_s.COEFF[26] = 899.0 ; 
	FirFilter_3132_s.COEFF[27] = 932.0 ; 
	FirFilter_3132_s.COEFF[28] = 965.0 ; 
	FirFilter_3132_s.COEFF[29] = 998.0 ; 
	FirFilter_3132_s.COEFF[30] = 1031.0 ; 
	FirFilter_3132_s.COEFF[31] = 1064.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar607, 0,  < , 2, streamItVar607++) {
			pop_void(&SplitJoin93_FirFilter_Fiss_3243_3277_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin93_FirFilter_Fiss_3243_3277_split[2], i) * FirFilter_3132_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin93_FirFilter_Fiss_3243_3277_split[2]) ; 
		push_float(&SplitJoin93_FirFilter_Fiss_3243_3277_join[2], sum) ; 
 {
		pop_void(&SplitJoin93_FirFilter_Fiss_3243_3277_split[2]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_3133
	 {
	FirFilter_3133_s.COEFF[0] = 41.0 ; 
	FirFilter_3133_s.COEFF[1] = 74.0 ; 
	FirFilter_3133_s.COEFF[2] = 107.0 ; 
	FirFilter_3133_s.COEFF[3] = 140.0 ; 
	FirFilter_3133_s.COEFF[4] = 173.0 ; 
	FirFilter_3133_s.COEFF[5] = 206.0 ; 
	FirFilter_3133_s.COEFF[6] = 239.0 ; 
	FirFilter_3133_s.COEFF[7] = 272.0 ; 
	FirFilter_3133_s.COEFF[8] = 305.0 ; 
	FirFilter_3133_s.COEFF[9] = 338.0 ; 
	FirFilter_3133_s.COEFF[10] = 371.0 ; 
	FirFilter_3133_s.COEFF[11] = 404.0 ; 
	FirFilter_3133_s.COEFF[12] = 437.0 ; 
	FirFilter_3133_s.COEFF[13] = 470.0 ; 
	FirFilter_3133_s.COEFF[14] = 503.0 ; 
	FirFilter_3133_s.COEFF[15] = 536.0 ; 
	FirFilter_3133_s.COEFF[16] = 569.0 ; 
	FirFilter_3133_s.COEFF[17] = 602.0 ; 
	FirFilter_3133_s.COEFF[18] = 635.0 ; 
	FirFilter_3133_s.COEFF[19] = 668.0 ; 
	FirFilter_3133_s.COEFF[20] = 701.0 ; 
	FirFilter_3133_s.COEFF[21] = 734.0 ; 
	FirFilter_3133_s.COEFF[22] = 767.0 ; 
	FirFilter_3133_s.COEFF[23] = 800.0 ; 
	FirFilter_3133_s.COEFF[24] = 833.0 ; 
	FirFilter_3133_s.COEFF[25] = 866.0 ; 
	FirFilter_3133_s.COEFF[26] = 899.0 ; 
	FirFilter_3133_s.COEFF[27] = 932.0 ; 
	FirFilter_3133_s.COEFF[28] = 965.0 ; 
	FirFilter_3133_s.COEFF[29] = 998.0 ; 
	FirFilter_3133_s.COEFF[30] = 1031.0 ; 
	FirFilter_3133_s.COEFF[31] = 1064.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar608, 0,  < , 3, streamItVar608++) {
			pop_void(&SplitJoin93_FirFilter_Fiss_3243_3277_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin93_FirFilter_Fiss_3243_3277_split[3], i) * FirFilter_3133_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin93_FirFilter_Fiss_3243_3277_split[3]) ; 
		push_float(&SplitJoin93_FirFilter_Fiss_3243_3277_join[3], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3129
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3129DownSamp_2979, pop_float(&SplitJoin93_FirFilter_Fiss_3243_3277_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DownSamp_2979
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++) {
		push_float(&DownSamp_2979UpSamp_2980, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3129DownSamp_2979)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3129DownSamp_2979) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: UpSamp_2980
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++) {
		push_float(&UpSamp_2980WEIGHTED_ROUND_ROBIN_Splitter_3134, pop_float(&DownSamp_2979UpSamp_2980)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			push_float(&UpSamp_2980WEIGHTED_ROUND_ROBIN_Splitter_3134, 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3134
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin95_Delay_N_Fiss_3244_3278_split[__iter_], pop_float(&UpSamp_2980WEIGHTED_ROUND_ROBIN_Splitter_3134));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3136
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin95_Delay_N_Fiss_3244_3278_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3137
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin95_Delay_N_Fiss_3244_3278_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3138
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin95_Delay_N_Fiss_3244_3278_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3139
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin95_Delay_N_Fiss_3244_3278_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3135
	FOR(uint32_t, __iter_init_, 0, <, 9, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3135DUPLICATE_Splitter_3140, pop_float(&SplitJoin95_Delay_N_Fiss_3244_3278_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3140
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3135DUPLICATE_Splitter_3140);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin97_FirFilter_Fiss_3245_3279_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_3142
	 {
	FirFilter_3142_s.COEFF[0] = 20.0 ; 
	FirFilter_3142_s.COEFF[1] = 25.0 ; 
	FirFilter_3142_s.COEFF[2] = 30.0 ; 
	FirFilter_3142_s.COEFF[3] = 35.0 ; 
	FirFilter_3142_s.COEFF[4] = 40.0 ; 
	FirFilter_3142_s.COEFF[5] = 45.0 ; 
	FirFilter_3142_s.COEFF[6] = 50.0 ; 
	FirFilter_3142_s.COEFF[7] = 55.0 ; 
	FirFilter_3142_s.COEFF[8] = 60.0 ; 
	FirFilter_3142_s.COEFF[9] = 65.0 ; 
	FirFilter_3142_s.COEFF[10] = 70.0 ; 
	FirFilter_3142_s.COEFF[11] = 75.0 ; 
	FirFilter_3142_s.COEFF[12] = 80.0 ; 
	FirFilter_3142_s.COEFF[13] = 85.0 ; 
	FirFilter_3142_s.COEFF[14] = 90.0 ; 
	FirFilter_3142_s.COEFF[15] = 95.0 ; 
	FirFilter_3142_s.COEFF[16] = 100.0 ; 
	FirFilter_3142_s.COEFF[17] = 105.0 ; 
	FirFilter_3142_s.COEFF[18] = 110.0 ; 
	FirFilter_3142_s.COEFF[19] = 115.0 ; 
	FirFilter_3142_s.COEFF[20] = 120.0 ; 
	FirFilter_3142_s.COEFF[21] = 125.0 ; 
	FirFilter_3142_s.COEFF[22] = 130.0 ; 
	FirFilter_3142_s.COEFF[23] = 135.0 ; 
	FirFilter_3142_s.COEFF[24] = 140.0 ; 
	FirFilter_3142_s.COEFF[25] = 145.0 ; 
	FirFilter_3142_s.COEFF[26] = 150.0 ; 
	FirFilter_3142_s.COEFF[27] = 155.0 ; 
	FirFilter_3142_s.COEFF[28] = 160.0 ; 
	FirFilter_3142_s.COEFF[29] = 165.0 ; 
	FirFilter_3142_s.COEFF[30] = 170.0 ; 
	FirFilter_3142_s.COEFF[31] = 175.0 ; 
}
	 {
	float sum = 0.0;
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin97_FirFilter_Fiss_3245_3279_split[0], i) * FirFilter_3142_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin97_FirFilter_Fiss_3245_3279_split[0]) ; 
	push_float(&SplitJoin97_FirFilter_Fiss_3245_3279_join[0], sum) ; 
 {
	FOR(int, streamItVar601, 0,  < , 3, streamItVar601++) {
		pop_void(&SplitJoin97_FirFilter_Fiss_3245_3279_split[0]) ; 
	}
	ENDFOR
}
}
//--------------------------------
// --- init: FirFilter_3143
	 {
	FirFilter_3143_s.COEFF[0] = 20.0 ; 
	FirFilter_3143_s.COEFF[1] = 25.0 ; 
	FirFilter_3143_s.COEFF[2] = 30.0 ; 
	FirFilter_3143_s.COEFF[3] = 35.0 ; 
	FirFilter_3143_s.COEFF[4] = 40.0 ; 
	FirFilter_3143_s.COEFF[5] = 45.0 ; 
	FirFilter_3143_s.COEFF[6] = 50.0 ; 
	FirFilter_3143_s.COEFF[7] = 55.0 ; 
	FirFilter_3143_s.COEFF[8] = 60.0 ; 
	FirFilter_3143_s.COEFF[9] = 65.0 ; 
	FirFilter_3143_s.COEFF[10] = 70.0 ; 
	FirFilter_3143_s.COEFF[11] = 75.0 ; 
	FirFilter_3143_s.COEFF[12] = 80.0 ; 
	FirFilter_3143_s.COEFF[13] = 85.0 ; 
	FirFilter_3143_s.COEFF[14] = 90.0 ; 
	FirFilter_3143_s.COEFF[15] = 95.0 ; 
	FirFilter_3143_s.COEFF[16] = 100.0 ; 
	FirFilter_3143_s.COEFF[17] = 105.0 ; 
	FirFilter_3143_s.COEFF[18] = 110.0 ; 
	FirFilter_3143_s.COEFF[19] = 115.0 ; 
	FirFilter_3143_s.COEFF[20] = 120.0 ; 
	FirFilter_3143_s.COEFF[21] = 125.0 ; 
	FirFilter_3143_s.COEFF[22] = 130.0 ; 
	FirFilter_3143_s.COEFF[23] = 135.0 ; 
	FirFilter_3143_s.COEFF[24] = 140.0 ; 
	FirFilter_3143_s.COEFF[25] = 145.0 ; 
	FirFilter_3143_s.COEFF[26] = 150.0 ; 
	FirFilter_3143_s.COEFF[27] = 155.0 ; 
	FirFilter_3143_s.COEFF[28] = 160.0 ; 
	FirFilter_3143_s.COEFF[29] = 165.0 ; 
	FirFilter_3143_s.COEFF[30] = 170.0 ; 
	FirFilter_3143_s.COEFF[31] = 175.0 ; 
}
	 {
	float sum = 0.0;
 {
	pop_void(&SplitJoin97_FirFilter_Fiss_3245_3279_split[1]) ; 
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin97_FirFilter_Fiss_3245_3279_split[1], i) * FirFilter_3143_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin97_FirFilter_Fiss_3245_3279_split[1]) ; 
	push_float(&SplitJoin97_FirFilter_Fiss_3245_3279_join[1], sum) ; 
 {
	FOR(int, streamItVar602, 0,  < , 2, streamItVar602++) {
		pop_void(&SplitJoin97_FirFilter_Fiss_3245_3279_split[1]) ; 
	}
	ENDFOR
}
}
//--------------------------------
// --- init: FirFilter_3144
	 {
	FirFilter_3144_s.COEFF[0] = 20.0 ; 
	FirFilter_3144_s.COEFF[1] = 25.0 ; 
	FirFilter_3144_s.COEFF[2] = 30.0 ; 
	FirFilter_3144_s.COEFF[3] = 35.0 ; 
	FirFilter_3144_s.COEFF[4] = 40.0 ; 
	FirFilter_3144_s.COEFF[5] = 45.0 ; 
	FirFilter_3144_s.COEFF[6] = 50.0 ; 
	FirFilter_3144_s.COEFF[7] = 55.0 ; 
	FirFilter_3144_s.COEFF[8] = 60.0 ; 
	FirFilter_3144_s.COEFF[9] = 65.0 ; 
	FirFilter_3144_s.COEFF[10] = 70.0 ; 
	FirFilter_3144_s.COEFF[11] = 75.0 ; 
	FirFilter_3144_s.COEFF[12] = 80.0 ; 
	FirFilter_3144_s.COEFF[13] = 85.0 ; 
	FirFilter_3144_s.COEFF[14] = 90.0 ; 
	FirFilter_3144_s.COEFF[15] = 95.0 ; 
	FirFilter_3144_s.COEFF[16] = 100.0 ; 
	FirFilter_3144_s.COEFF[17] = 105.0 ; 
	FirFilter_3144_s.COEFF[18] = 110.0 ; 
	FirFilter_3144_s.COEFF[19] = 115.0 ; 
	FirFilter_3144_s.COEFF[20] = 120.0 ; 
	FirFilter_3144_s.COEFF[21] = 125.0 ; 
	FirFilter_3144_s.COEFF[22] = 130.0 ; 
	FirFilter_3144_s.COEFF[23] = 135.0 ; 
	FirFilter_3144_s.COEFF[24] = 140.0 ; 
	FirFilter_3144_s.COEFF[25] = 145.0 ; 
	FirFilter_3144_s.COEFF[26] = 150.0 ; 
	FirFilter_3144_s.COEFF[27] = 155.0 ; 
	FirFilter_3144_s.COEFF[28] = 160.0 ; 
	FirFilter_3144_s.COEFF[29] = 165.0 ; 
	FirFilter_3144_s.COEFF[30] = 170.0 ; 
	FirFilter_3144_s.COEFF[31] = 175.0 ; 
}
	 {
	float sum = 0.0;
 {
	FOR(int, streamItVar603, 0,  < , 2, streamItVar603++) {
		pop_void(&SplitJoin97_FirFilter_Fiss_3245_3279_split[2]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin97_FirFilter_Fiss_3245_3279_split[2], i) * FirFilter_3144_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin97_FirFilter_Fiss_3245_3279_split[2]) ; 
	push_float(&SplitJoin97_FirFilter_Fiss_3245_3279_join[2], sum) ; 
 {
	pop_void(&SplitJoin97_FirFilter_Fiss_3245_3279_split[2]) ; 
}
}
//--------------------------------
// --- init: FirFilter_3145
	 {
	FirFilter_3145_s.COEFF[0] = 20.0 ; 
	FirFilter_3145_s.COEFF[1] = 25.0 ; 
	FirFilter_3145_s.COEFF[2] = 30.0 ; 
	FirFilter_3145_s.COEFF[3] = 35.0 ; 
	FirFilter_3145_s.COEFF[4] = 40.0 ; 
	FirFilter_3145_s.COEFF[5] = 45.0 ; 
	FirFilter_3145_s.COEFF[6] = 50.0 ; 
	FirFilter_3145_s.COEFF[7] = 55.0 ; 
	FirFilter_3145_s.COEFF[8] = 60.0 ; 
	FirFilter_3145_s.COEFF[9] = 65.0 ; 
	FirFilter_3145_s.COEFF[10] = 70.0 ; 
	FirFilter_3145_s.COEFF[11] = 75.0 ; 
	FirFilter_3145_s.COEFF[12] = 80.0 ; 
	FirFilter_3145_s.COEFF[13] = 85.0 ; 
	FirFilter_3145_s.COEFF[14] = 90.0 ; 
	FirFilter_3145_s.COEFF[15] = 95.0 ; 
	FirFilter_3145_s.COEFF[16] = 100.0 ; 
	FirFilter_3145_s.COEFF[17] = 105.0 ; 
	FirFilter_3145_s.COEFF[18] = 110.0 ; 
	FirFilter_3145_s.COEFF[19] = 115.0 ; 
	FirFilter_3145_s.COEFF[20] = 120.0 ; 
	FirFilter_3145_s.COEFF[21] = 125.0 ; 
	FirFilter_3145_s.COEFF[22] = 130.0 ; 
	FirFilter_3145_s.COEFF[23] = 135.0 ; 
	FirFilter_3145_s.COEFF[24] = 140.0 ; 
	FirFilter_3145_s.COEFF[25] = 145.0 ; 
	FirFilter_3145_s.COEFF[26] = 150.0 ; 
	FirFilter_3145_s.COEFF[27] = 155.0 ; 
	FirFilter_3145_s.COEFF[28] = 160.0 ; 
	FirFilter_3145_s.COEFF[29] = 165.0 ; 
	FirFilter_3145_s.COEFF[30] = 170.0 ; 
	FirFilter_3145_s.COEFF[31] = 175.0 ; 
}
	 {
	float sum = 0.0;
 {
	FOR(int, streamItVar604, 0,  < , 3, streamItVar604++) {
		pop_void(&SplitJoin97_FirFilter_Fiss_3245_3279_split[3]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin97_FirFilter_Fiss_3245_3279_split[3], i) * FirFilter_3145_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin97_FirFilter_Fiss_3245_3279_split[3]) ; 
	push_float(&SplitJoin97_FirFilter_Fiss_3245_3279_join[3], sum) ; 
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3141
	
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[4], pop_float(&SplitJoin97_FirFilter_Fiss_3245_3279_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3146
	FOR(uint32_t, __iter_init_, 0, <, 75, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin112_Delay_N_Fiss_3246_3280_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[5]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3148
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin112_Delay_N_Fiss_3246_3280_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3149
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin112_Delay_N_Fiss_3246_3280_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3150
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin112_Delay_N_Fiss_3246_3280_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3151
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin112_Delay_N_Fiss_3246_3280_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3147
	FOR(uint32_t, __iter_init_, 0, <, 46, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3147DUPLICATE_Splitter_3152, pop_float(&SplitJoin112_Delay_N_Fiss_3246_3280_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3152
	FOR(uint32_t, __iter_init_, 0, <, 183, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3147DUPLICATE_Splitter_3152);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin114_FirFilter_Fiss_3247_3281_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_3154
	 {
	FirFilter_3154_s.COEFF[0] = 51.0 ; 
	FirFilter_3154_s.COEFF[1] = 84.0 ; 
	FirFilter_3154_s.COEFF[2] = 117.0 ; 
	FirFilter_3154_s.COEFF[3] = 150.0 ; 
	FirFilter_3154_s.COEFF[4] = 183.0 ; 
	FirFilter_3154_s.COEFF[5] = 216.0 ; 
	FirFilter_3154_s.COEFF[6] = 249.0 ; 
	FirFilter_3154_s.COEFF[7] = 282.0 ; 
	FirFilter_3154_s.COEFF[8] = 315.0 ; 
	FirFilter_3154_s.COEFF[9] = 348.0 ; 
	FirFilter_3154_s.COEFF[10] = 381.0 ; 
	FirFilter_3154_s.COEFF[11] = 414.0 ; 
	FirFilter_3154_s.COEFF[12] = 447.0 ; 
	FirFilter_3154_s.COEFF[13] = 480.0 ; 
	FirFilter_3154_s.COEFF[14] = 513.0 ; 
	FirFilter_3154_s.COEFF[15] = 546.0 ; 
	FirFilter_3154_s.COEFF[16] = 579.0 ; 
	FirFilter_3154_s.COEFF[17] = 612.0 ; 
	FirFilter_3154_s.COEFF[18] = 645.0 ; 
	FirFilter_3154_s.COEFF[19] = 678.0 ; 
	FirFilter_3154_s.COEFF[20] = 711.0 ; 
	FirFilter_3154_s.COEFF[21] = 744.0 ; 
	FirFilter_3154_s.COEFF[22] = 777.0 ; 
	FirFilter_3154_s.COEFF[23] = 810.0 ; 
	FirFilter_3154_s.COEFF[24] = 843.0 ; 
	FirFilter_3154_s.COEFF[25] = 876.0 ; 
	FirFilter_3154_s.COEFF[26] = 909.0 ; 
	FirFilter_3154_s.COEFF[27] = 942.0 ; 
	FirFilter_3154_s.COEFF[28] = 975.0 ; 
	FirFilter_3154_s.COEFF[29] = 1008.0 ; 
	FirFilter_3154_s.COEFF[30] = 1041.0 ; 
	FirFilter_3154_s.COEFF[31] = 1074.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin114_FirFilter_Fiss_3247_3281_split[0], i) * FirFilter_3154_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin114_FirFilter_Fiss_3247_3281_split[0]) ; 
		push_float(&SplitJoin114_FirFilter_Fiss_3247_3281_join[0], sum) ; 
 {
		FOR(int, streamItVar597, 0,  < , 3, streamItVar597++) {
			pop_void(&SplitJoin114_FirFilter_Fiss_3247_3281_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_3155
	 {
	FirFilter_3155_s.COEFF[0] = 51.0 ; 
	FirFilter_3155_s.COEFF[1] = 84.0 ; 
	FirFilter_3155_s.COEFF[2] = 117.0 ; 
	FirFilter_3155_s.COEFF[3] = 150.0 ; 
	FirFilter_3155_s.COEFF[4] = 183.0 ; 
	FirFilter_3155_s.COEFF[5] = 216.0 ; 
	FirFilter_3155_s.COEFF[6] = 249.0 ; 
	FirFilter_3155_s.COEFF[7] = 282.0 ; 
	FirFilter_3155_s.COEFF[8] = 315.0 ; 
	FirFilter_3155_s.COEFF[9] = 348.0 ; 
	FirFilter_3155_s.COEFF[10] = 381.0 ; 
	FirFilter_3155_s.COEFF[11] = 414.0 ; 
	FirFilter_3155_s.COEFF[12] = 447.0 ; 
	FirFilter_3155_s.COEFF[13] = 480.0 ; 
	FirFilter_3155_s.COEFF[14] = 513.0 ; 
	FirFilter_3155_s.COEFF[15] = 546.0 ; 
	FirFilter_3155_s.COEFF[16] = 579.0 ; 
	FirFilter_3155_s.COEFF[17] = 612.0 ; 
	FirFilter_3155_s.COEFF[18] = 645.0 ; 
	FirFilter_3155_s.COEFF[19] = 678.0 ; 
	FirFilter_3155_s.COEFF[20] = 711.0 ; 
	FirFilter_3155_s.COEFF[21] = 744.0 ; 
	FirFilter_3155_s.COEFF[22] = 777.0 ; 
	FirFilter_3155_s.COEFF[23] = 810.0 ; 
	FirFilter_3155_s.COEFF[24] = 843.0 ; 
	FirFilter_3155_s.COEFF[25] = 876.0 ; 
	FirFilter_3155_s.COEFF[26] = 909.0 ; 
	FirFilter_3155_s.COEFF[27] = 942.0 ; 
	FirFilter_3155_s.COEFF[28] = 975.0 ; 
	FirFilter_3155_s.COEFF[29] = 1008.0 ; 
	FirFilter_3155_s.COEFF[30] = 1041.0 ; 
	FirFilter_3155_s.COEFF[31] = 1074.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin114_FirFilter_Fiss_3247_3281_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin114_FirFilter_Fiss_3247_3281_split[1], i) * FirFilter_3155_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin114_FirFilter_Fiss_3247_3281_split[1]) ; 
		push_float(&SplitJoin114_FirFilter_Fiss_3247_3281_join[1], sum) ; 
 {
		FOR(int, streamItVar598, 0,  < , 2, streamItVar598++) {
			pop_void(&SplitJoin114_FirFilter_Fiss_3247_3281_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_3156
	 {
	FirFilter_3156_s.COEFF[0] = 51.0 ; 
	FirFilter_3156_s.COEFF[1] = 84.0 ; 
	FirFilter_3156_s.COEFF[2] = 117.0 ; 
	FirFilter_3156_s.COEFF[3] = 150.0 ; 
	FirFilter_3156_s.COEFF[4] = 183.0 ; 
	FirFilter_3156_s.COEFF[5] = 216.0 ; 
	FirFilter_3156_s.COEFF[6] = 249.0 ; 
	FirFilter_3156_s.COEFF[7] = 282.0 ; 
	FirFilter_3156_s.COEFF[8] = 315.0 ; 
	FirFilter_3156_s.COEFF[9] = 348.0 ; 
	FirFilter_3156_s.COEFF[10] = 381.0 ; 
	FirFilter_3156_s.COEFF[11] = 414.0 ; 
	FirFilter_3156_s.COEFF[12] = 447.0 ; 
	FirFilter_3156_s.COEFF[13] = 480.0 ; 
	FirFilter_3156_s.COEFF[14] = 513.0 ; 
	FirFilter_3156_s.COEFF[15] = 546.0 ; 
	FirFilter_3156_s.COEFF[16] = 579.0 ; 
	FirFilter_3156_s.COEFF[17] = 612.0 ; 
	FirFilter_3156_s.COEFF[18] = 645.0 ; 
	FirFilter_3156_s.COEFF[19] = 678.0 ; 
	FirFilter_3156_s.COEFF[20] = 711.0 ; 
	FirFilter_3156_s.COEFF[21] = 744.0 ; 
	FirFilter_3156_s.COEFF[22] = 777.0 ; 
	FirFilter_3156_s.COEFF[23] = 810.0 ; 
	FirFilter_3156_s.COEFF[24] = 843.0 ; 
	FirFilter_3156_s.COEFF[25] = 876.0 ; 
	FirFilter_3156_s.COEFF[26] = 909.0 ; 
	FirFilter_3156_s.COEFF[27] = 942.0 ; 
	FirFilter_3156_s.COEFF[28] = 975.0 ; 
	FirFilter_3156_s.COEFF[29] = 1008.0 ; 
	FirFilter_3156_s.COEFF[30] = 1041.0 ; 
	FirFilter_3156_s.COEFF[31] = 1074.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar599, 0,  < , 2, streamItVar599++) {
			pop_void(&SplitJoin114_FirFilter_Fiss_3247_3281_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin114_FirFilter_Fiss_3247_3281_split[2], i) * FirFilter_3156_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin114_FirFilter_Fiss_3247_3281_split[2]) ; 
		push_float(&SplitJoin114_FirFilter_Fiss_3247_3281_join[2], sum) ; 
 {
		pop_void(&SplitJoin114_FirFilter_Fiss_3247_3281_split[2]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_3157
	 {
	FirFilter_3157_s.COEFF[0] = 51.0 ; 
	FirFilter_3157_s.COEFF[1] = 84.0 ; 
	FirFilter_3157_s.COEFF[2] = 117.0 ; 
	FirFilter_3157_s.COEFF[3] = 150.0 ; 
	FirFilter_3157_s.COEFF[4] = 183.0 ; 
	FirFilter_3157_s.COEFF[5] = 216.0 ; 
	FirFilter_3157_s.COEFF[6] = 249.0 ; 
	FirFilter_3157_s.COEFF[7] = 282.0 ; 
	FirFilter_3157_s.COEFF[8] = 315.0 ; 
	FirFilter_3157_s.COEFF[9] = 348.0 ; 
	FirFilter_3157_s.COEFF[10] = 381.0 ; 
	FirFilter_3157_s.COEFF[11] = 414.0 ; 
	FirFilter_3157_s.COEFF[12] = 447.0 ; 
	FirFilter_3157_s.COEFF[13] = 480.0 ; 
	FirFilter_3157_s.COEFF[14] = 513.0 ; 
	FirFilter_3157_s.COEFF[15] = 546.0 ; 
	FirFilter_3157_s.COEFF[16] = 579.0 ; 
	FirFilter_3157_s.COEFF[17] = 612.0 ; 
	FirFilter_3157_s.COEFF[18] = 645.0 ; 
	FirFilter_3157_s.COEFF[19] = 678.0 ; 
	FirFilter_3157_s.COEFF[20] = 711.0 ; 
	FirFilter_3157_s.COEFF[21] = 744.0 ; 
	FirFilter_3157_s.COEFF[22] = 777.0 ; 
	FirFilter_3157_s.COEFF[23] = 810.0 ; 
	FirFilter_3157_s.COEFF[24] = 843.0 ; 
	FirFilter_3157_s.COEFF[25] = 876.0 ; 
	FirFilter_3157_s.COEFF[26] = 909.0 ; 
	FirFilter_3157_s.COEFF[27] = 942.0 ; 
	FirFilter_3157_s.COEFF[28] = 975.0 ; 
	FirFilter_3157_s.COEFF[29] = 1008.0 ; 
	FirFilter_3157_s.COEFF[30] = 1041.0 ; 
	FirFilter_3157_s.COEFF[31] = 1074.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar600, 0,  < , 3, streamItVar600++) {
			pop_void(&SplitJoin114_FirFilter_Fiss_3247_3281_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin114_FirFilter_Fiss_3247_3281_split[3], i) * FirFilter_3157_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin114_FirFilter_Fiss_3247_3281_split[3]) ; 
		push_float(&SplitJoin114_FirFilter_Fiss_3247_3281_join[3], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3153
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3153DownSamp_2986, pop_float(&SplitJoin114_FirFilter_Fiss_3247_3281_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DownSamp_2986
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++) {
		push_float(&DownSamp_2986UpSamp_2987, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3153DownSamp_2986)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3153DownSamp_2986) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: UpSamp_2987
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++) {
		push_float(&UpSamp_2987WEIGHTED_ROUND_ROBIN_Splitter_3158, pop_float(&DownSamp_2986UpSamp_2987)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			push_float(&UpSamp_2987WEIGHTED_ROUND_ROBIN_Splitter_3158, 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3158
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin116_Delay_N_Fiss_3248_3282_split[__iter_], pop_float(&UpSamp_2987WEIGHTED_ROUND_ROBIN_Splitter_3158));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3160
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin116_Delay_N_Fiss_3248_3282_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3161
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin116_Delay_N_Fiss_3248_3282_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3162
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin116_Delay_N_Fiss_3248_3282_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3163
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin116_Delay_N_Fiss_3248_3282_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3159
	FOR(uint32_t, __iter_init_, 0, <, 9, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3159DUPLICATE_Splitter_3164, pop_float(&SplitJoin116_Delay_N_Fiss_3248_3282_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3164
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3159DUPLICATE_Splitter_3164);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin118_FirFilter_Fiss_3249_3283_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_3166
	 {
	FirFilter_3166_s.COEFF[0] = 30.0 ; 
	FirFilter_3166_s.COEFF[1] = 36.0 ; 
	FirFilter_3166_s.COEFF[2] = 42.0 ; 
	FirFilter_3166_s.COEFF[3] = 48.0 ; 
	FirFilter_3166_s.COEFF[4] = 54.0 ; 
	FirFilter_3166_s.COEFF[5] = 60.0 ; 
	FirFilter_3166_s.COEFF[6] = 66.0 ; 
	FirFilter_3166_s.COEFF[7] = 72.0 ; 
	FirFilter_3166_s.COEFF[8] = 78.0 ; 
	FirFilter_3166_s.COEFF[9] = 84.0 ; 
	FirFilter_3166_s.COEFF[10] = 90.0 ; 
	FirFilter_3166_s.COEFF[11] = 96.0 ; 
	FirFilter_3166_s.COEFF[12] = 102.0 ; 
	FirFilter_3166_s.COEFF[13] = 108.0 ; 
	FirFilter_3166_s.COEFF[14] = 114.0 ; 
	FirFilter_3166_s.COEFF[15] = 120.0 ; 
	FirFilter_3166_s.COEFF[16] = 126.0 ; 
	FirFilter_3166_s.COEFF[17] = 132.0 ; 
	FirFilter_3166_s.COEFF[18] = 138.0 ; 
	FirFilter_3166_s.COEFF[19] = 144.0 ; 
	FirFilter_3166_s.COEFF[20] = 150.0 ; 
	FirFilter_3166_s.COEFF[21] = 156.0 ; 
	FirFilter_3166_s.COEFF[22] = 162.0 ; 
	FirFilter_3166_s.COEFF[23] = 168.0 ; 
	FirFilter_3166_s.COEFF[24] = 174.0 ; 
	FirFilter_3166_s.COEFF[25] = 180.0 ; 
	FirFilter_3166_s.COEFF[26] = 186.0 ; 
	FirFilter_3166_s.COEFF[27] = 192.0 ; 
	FirFilter_3166_s.COEFF[28] = 198.0 ; 
	FirFilter_3166_s.COEFF[29] = 204.0 ; 
	FirFilter_3166_s.COEFF[30] = 210.0 ; 
	FirFilter_3166_s.COEFF[31] = 216.0 ; 
}
	 {
	float sum = 0.0;
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin118_FirFilter_Fiss_3249_3283_split[0], i) * FirFilter_3166_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin118_FirFilter_Fiss_3249_3283_split[0]) ; 
	push_float(&SplitJoin118_FirFilter_Fiss_3249_3283_join[0], sum) ; 
 {
	FOR(int, streamItVar593, 0,  < , 3, streamItVar593++) {
		pop_void(&SplitJoin118_FirFilter_Fiss_3249_3283_split[0]) ; 
	}
	ENDFOR
}
}
//--------------------------------
// --- init: FirFilter_3167
	 {
	FirFilter_3167_s.COEFF[0] = 30.0 ; 
	FirFilter_3167_s.COEFF[1] = 36.0 ; 
	FirFilter_3167_s.COEFF[2] = 42.0 ; 
	FirFilter_3167_s.COEFF[3] = 48.0 ; 
	FirFilter_3167_s.COEFF[4] = 54.0 ; 
	FirFilter_3167_s.COEFF[5] = 60.0 ; 
	FirFilter_3167_s.COEFF[6] = 66.0 ; 
	FirFilter_3167_s.COEFF[7] = 72.0 ; 
	FirFilter_3167_s.COEFF[8] = 78.0 ; 
	FirFilter_3167_s.COEFF[9] = 84.0 ; 
	FirFilter_3167_s.COEFF[10] = 90.0 ; 
	FirFilter_3167_s.COEFF[11] = 96.0 ; 
	FirFilter_3167_s.COEFF[12] = 102.0 ; 
	FirFilter_3167_s.COEFF[13] = 108.0 ; 
	FirFilter_3167_s.COEFF[14] = 114.0 ; 
	FirFilter_3167_s.COEFF[15] = 120.0 ; 
	FirFilter_3167_s.COEFF[16] = 126.0 ; 
	FirFilter_3167_s.COEFF[17] = 132.0 ; 
	FirFilter_3167_s.COEFF[18] = 138.0 ; 
	FirFilter_3167_s.COEFF[19] = 144.0 ; 
	FirFilter_3167_s.COEFF[20] = 150.0 ; 
	FirFilter_3167_s.COEFF[21] = 156.0 ; 
	FirFilter_3167_s.COEFF[22] = 162.0 ; 
	FirFilter_3167_s.COEFF[23] = 168.0 ; 
	FirFilter_3167_s.COEFF[24] = 174.0 ; 
	FirFilter_3167_s.COEFF[25] = 180.0 ; 
	FirFilter_3167_s.COEFF[26] = 186.0 ; 
	FirFilter_3167_s.COEFF[27] = 192.0 ; 
	FirFilter_3167_s.COEFF[28] = 198.0 ; 
	FirFilter_3167_s.COEFF[29] = 204.0 ; 
	FirFilter_3167_s.COEFF[30] = 210.0 ; 
	FirFilter_3167_s.COEFF[31] = 216.0 ; 
}
	 {
	float sum = 0.0;
 {
	pop_void(&SplitJoin118_FirFilter_Fiss_3249_3283_split[1]) ; 
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin118_FirFilter_Fiss_3249_3283_split[1], i) * FirFilter_3167_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin118_FirFilter_Fiss_3249_3283_split[1]) ; 
	push_float(&SplitJoin118_FirFilter_Fiss_3249_3283_join[1], sum) ; 
 {
	FOR(int, streamItVar594, 0,  < , 2, streamItVar594++) {
		pop_void(&SplitJoin118_FirFilter_Fiss_3249_3283_split[1]) ; 
	}
	ENDFOR
}
}
//--------------------------------
// --- init: FirFilter_3168
	 {
	FirFilter_3168_s.COEFF[0] = 30.0 ; 
	FirFilter_3168_s.COEFF[1] = 36.0 ; 
	FirFilter_3168_s.COEFF[2] = 42.0 ; 
	FirFilter_3168_s.COEFF[3] = 48.0 ; 
	FirFilter_3168_s.COEFF[4] = 54.0 ; 
	FirFilter_3168_s.COEFF[5] = 60.0 ; 
	FirFilter_3168_s.COEFF[6] = 66.0 ; 
	FirFilter_3168_s.COEFF[7] = 72.0 ; 
	FirFilter_3168_s.COEFF[8] = 78.0 ; 
	FirFilter_3168_s.COEFF[9] = 84.0 ; 
	FirFilter_3168_s.COEFF[10] = 90.0 ; 
	FirFilter_3168_s.COEFF[11] = 96.0 ; 
	FirFilter_3168_s.COEFF[12] = 102.0 ; 
	FirFilter_3168_s.COEFF[13] = 108.0 ; 
	FirFilter_3168_s.COEFF[14] = 114.0 ; 
	FirFilter_3168_s.COEFF[15] = 120.0 ; 
	FirFilter_3168_s.COEFF[16] = 126.0 ; 
	FirFilter_3168_s.COEFF[17] = 132.0 ; 
	FirFilter_3168_s.COEFF[18] = 138.0 ; 
	FirFilter_3168_s.COEFF[19] = 144.0 ; 
	FirFilter_3168_s.COEFF[20] = 150.0 ; 
	FirFilter_3168_s.COEFF[21] = 156.0 ; 
	FirFilter_3168_s.COEFF[22] = 162.0 ; 
	FirFilter_3168_s.COEFF[23] = 168.0 ; 
	FirFilter_3168_s.COEFF[24] = 174.0 ; 
	FirFilter_3168_s.COEFF[25] = 180.0 ; 
	FirFilter_3168_s.COEFF[26] = 186.0 ; 
	FirFilter_3168_s.COEFF[27] = 192.0 ; 
	FirFilter_3168_s.COEFF[28] = 198.0 ; 
	FirFilter_3168_s.COEFF[29] = 204.0 ; 
	FirFilter_3168_s.COEFF[30] = 210.0 ; 
	FirFilter_3168_s.COEFF[31] = 216.0 ; 
}
	 {
	float sum = 0.0;
 {
	FOR(int, streamItVar595, 0,  < , 2, streamItVar595++) {
		pop_void(&SplitJoin118_FirFilter_Fiss_3249_3283_split[2]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin118_FirFilter_Fiss_3249_3283_split[2], i) * FirFilter_3168_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin118_FirFilter_Fiss_3249_3283_split[2]) ; 
	push_float(&SplitJoin118_FirFilter_Fiss_3249_3283_join[2], sum) ; 
 {
	pop_void(&SplitJoin118_FirFilter_Fiss_3249_3283_split[2]) ; 
}
}
//--------------------------------
// --- init: FirFilter_3169
	 {
	FirFilter_3169_s.COEFF[0] = 30.0 ; 
	FirFilter_3169_s.COEFF[1] = 36.0 ; 
	FirFilter_3169_s.COEFF[2] = 42.0 ; 
	FirFilter_3169_s.COEFF[3] = 48.0 ; 
	FirFilter_3169_s.COEFF[4] = 54.0 ; 
	FirFilter_3169_s.COEFF[5] = 60.0 ; 
	FirFilter_3169_s.COEFF[6] = 66.0 ; 
	FirFilter_3169_s.COEFF[7] = 72.0 ; 
	FirFilter_3169_s.COEFF[8] = 78.0 ; 
	FirFilter_3169_s.COEFF[9] = 84.0 ; 
	FirFilter_3169_s.COEFF[10] = 90.0 ; 
	FirFilter_3169_s.COEFF[11] = 96.0 ; 
	FirFilter_3169_s.COEFF[12] = 102.0 ; 
	FirFilter_3169_s.COEFF[13] = 108.0 ; 
	FirFilter_3169_s.COEFF[14] = 114.0 ; 
	FirFilter_3169_s.COEFF[15] = 120.0 ; 
	FirFilter_3169_s.COEFF[16] = 126.0 ; 
	FirFilter_3169_s.COEFF[17] = 132.0 ; 
	FirFilter_3169_s.COEFF[18] = 138.0 ; 
	FirFilter_3169_s.COEFF[19] = 144.0 ; 
	FirFilter_3169_s.COEFF[20] = 150.0 ; 
	FirFilter_3169_s.COEFF[21] = 156.0 ; 
	FirFilter_3169_s.COEFF[22] = 162.0 ; 
	FirFilter_3169_s.COEFF[23] = 168.0 ; 
	FirFilter_3169_s.COEFF[24] = 174.0 ; 
	FirFilter_3169_s.COEFF[25] = 180.0 ; 
	FirFilter_3169_s.COEFF[26] = 186.0 ; 
	FirFilter_3169_s.COEFF[27] = 192.0 ; 
	FirFilter_3169_s.COEFF[28] = 198.0 ; 
	FirFilter_3169_s.COEFF[29] = 204.0 ; 
	FirFilter_3169_s.COEFF[30] = 210.0 ; 
	FirFilter_3169_s.COEFF[31] = 216.0 ; 
}
	 {
	float sum = 0.0;
 {
	FOR(int, streamItVar596, 0,  < , 3, streamItVar596++) {
		pop_void(&SplitJoin118_FirFilter_Fiss_3249_3283_split[3]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin118_FirFilter_Fiss_3249_3283_split[3], i) * FirFilter_3169_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin118_FirFilter_Fiss_3249_3283_split[3]) ; 
	push_float(&SplitJoin118_FirFilter_Fiss_3249_3283_join[3], sum) ; 
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3165
	
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[5], pop_float(&SplitJoin118_FirFilter_Fiss_3249_3283_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3170
	FOR(uint32_t, __iter_init_, 0, <, 75, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin133_Delay_N_Fiss_3250_3284_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[6]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3172
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin133_Delay_N_Fiss_3250_3284_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3173
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin133_Delay_N_Fiss_3250_3284_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3174
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin133_Delay_N_Fiss_3250_3284_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3175
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin133_Delay_N_Fiss_3250_3284_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3171
	FOR(uint32_t, __iter_init_, 0, <, 46, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3171DUPLICATE_Splitter_3176, pop_float(&SplitJoin133_Delay_N_Fiss_3250_3284_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3176
	FOR(uint32_t, __iter_init_, 0, <, 183, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3171DUPLICATE_Splitter_3176);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin135_FirFilter_Fiss_3251_3285_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_3178
	 {
	FirFilter_3178_s.COEFF[0] = 61.0 ; 
	FirFilter_3178_s.COEFF[1] = 94.0 ; 
	FirFilter_3178_s.COEFF[2] = 127.0 ; 
	FirFilter_3178_s.COEFF[3] = 160.0 ; 
	FirFilter_3178_s.COEFF[4] = 193.0 ; 
	FirFilter_3178_s.COEFF[5] = 226.0 ; 
	FirFilter_3178_s.COEFF[6] = 259.0 ; 
	FirFilter_3178_s.COEFF[7] = 292.0 ; 
	FirFilter_3178_s.COEFF[8] = 325.0 ; 
	FirFilter_3178_s.COEFF[9] = 358.0 ; 
	FirFilter_3178_s.COEFF[10] = 391.0 ; 
	FirFilter_3178_s.COEFF[11] = 424.0 ; 
	FirFilter_3178_s.COEFF[12] = 457.0 ; 
	FirFilter_3178_s.COEFF[13] = 490.0 ; 
	FirFilter_3178_s.COEFF[14] = 523.0 ; 
	FirFilter_3178_s.COEFF[15] = 556.0 ; 
	FirFilter_3178_s.COEFF[16] = 589.0 ; 
	FirFilter_3178_s.COEFF[17] = 622.0 ; 
	FirFilter_3178_s.COEFF[18] = 655.0 ; 
	FirFilter_3178_s.COEFF[19] = 688.0 ; 
	FirFilter_3178_s.COEFF[20] = 721.0 ; 
	FirFilter_3178_s.COEFF[21] = 754.0 ; 
	FirFilter_3178_s.COEFF[22] = 787.0 ; 
	FirFilter_3178_s.COEFF[23] = 820.0 ; 
	FirFilter_3178_s.COEFF[24] = 853.0 ; 
	FirFilter_3178_s.COEFF[25] = 886.0 ; 
	FirFilter_3178_s.COEFF[26] = 919.0 ; 
	FirFilter_3178_s.COEFF[27] = 952.0 ; 
	FirFilter_3178_s.COEFF[28] = 985.0 ; 
	FirFilter_3178_s.COEFF[29] = 1018.0 ; 
	FirFilter_3178_s.COEFF[30] = 1051.0 ; 
	FirFilter_3178_s.COEFF[31] = 1084.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin135_FirFilter_Fiss_3251_3285_split[0], i) * FirFilter_3178_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin135_FirFilter_Fiss_3251_3285_split[0]) ; 
		push_float(&SplitJoin135_FirFilter_Fiss_3251_3285_join[0], sum) ; 
 {
		FOR(int, streamItVar589, 0,  < , 3, streamItVar589++) {
			pop_void(&SplitJoin135_FirFilter_Fiss_3251_3285_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_3179
	 {
	FirFilter_3179_s.COEFF[0] = 61.0 ; 
	FirFilter_3179_s.COEFF[1] = 94.0 ; 
	FirFilter_3179_s.COEFF[2] = 127.0 ; 
	FirFilter_3179_s.COEFF[3] = 160.0 ; 
	FirFilter_3179_s.COEFF[4] = 193.0 ; 
	FirFilter_3179_s.COEFF[5] = 226.0 ; 
	FirFilter_3179_s.COEFF[6] = 259.0 ; 
	FirFilter_3179_s.COEFF[7] = 292.0 ; 
	FirFilter_3179_s.COEFF[8] = 325.0 ; 
	FirFilter_3179_s.COEFF[9] = 358.0 ; 
	FirFilter_3179_s.COEFF[10] = 391.0 ; 
	FirFilter_3179_s.COEFF[11] = 424.0 ; 
	FirFilter_3179_s.COEFF[12] = 457.0 ; 
	FirFilter_3179_s.COEFF[13] = 490.0 ; 
	FirFilter_3179_s.COEFF[14] = 523.0 ; 
	FirFilter_3179_s.COEFF[15] = 556.0 ; 
	FirFilter_3179_s.COEFF[16] = 589.0 ; 
	FirFilter_3179_s.COEFF[17] = 622.0 ; 
	FirFilter_3179_s.COEFF[18] = 655.0 ; 
	FirFilter_3179_s.COEFF[19] = 688.0 ; 
	FirFilter_3179_s.COEFF[20] = 721.0 ; 
	FirFilter_3179_s.COEFF[21] = 754.0 ; 
	FirFilter_3179_s.COEFF[22] = 787.0 ; 
	FirFilter_3179_s.COEFF[23] = 820.0 ; 
	FirFilter_3179_s.COEFF[24] = 853.0 ; 
	FirFilter_3179_s.COEFF[25] = 886.0 ; 
	FirFilter_3179_s.COEFF[26] = 919.0 ; 
	FirFilter_3179_s.COEFF[27] = 952.0 ; 
	FirFilter_3179_s.COEFF[28] = 985.0 ; 
	FirFilter_3179_s.COEFF[29] = 1018.0 ; 
	FirFilter_3179_s.COEFF[30] = 1051.0 ; 
	FirFilter_3179_s.COEFF[31] = 1084.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin135_FirFilter_Fiss_3251_3285_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin135_FirFilter_Fiss_3251_3285_split[1], i) * FirFilter_3179_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin135_FirFilter_Fiss_3251_3285_split[1]) ; 
		push_float(&SplitJoin135_FirFilter_Fiss_3251_3285_join[1], sum) ; 
 {
		FOR(int, streamItVar590, 0,  < , 2, streamItVar590++) {
			pop_void(&SplitJoin135_FirFilter_Fiss_3251_3285_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_3180
	 {
	FirFilter_3180_s.COEFF[0] = 61.0 ; 
	FirFilter_3180_s.COEFF[1] = 94.0 ; 
	FirFilter_3180_s.COEFF[2] = 127.0 ; 
	FirFilter_3180_s.COEFF[3] = 160.0 ; 
	FirFilter_3180_s.COEFF[4] = 193.0 ; 
	FirFilter_3180_s.COEFF[5] = 226.0 ; 
	FirFilter_3180_s.COEFF[6] = 259.0 ; 
	FirFilter_3180_s.COEFF[7] = 292.0 ; 
	FirFilter_3180_s.COEFF[8] = 325.0 ; 
	FirFilter_3180_s.COEFF[9] = 358.0 ; 
	FirFilter_3180_s.COEFF[10] = 391.0 ; 
	FirFilter_3180_s.COEFF[11] = 424.0 ; 
	FirFilter_3180_s.COEFF[12] = 457.0 ; 
	FirFilter_3180_s.COEFF[13] = 490.0 ; 
	FirFilter_3180_s.COEFF[14] = 523.0 ; 
	FirFilter_3180_s.COEFF[15] = 556.0 ; 
	FirFilter_3180_s.COEFF[16] = 589.0 ; 
	FirFilter_3180_s.COEFF[17] = 622.0 ; 
	FirFilter_3180_s.COEFF[18] = 655.0 ; 
	FirFilter_3180_s.COEFF[19] = 688.0 ; 
	FirFilter_3180_s.COEFF[20] = 721.0 ; 
	FirFilter_3180_s.COEFF[21] = 754.0 ; 
	FirFilter_3180_s.COEFF[22] = 787.0 ; 
	FirFilter_3180_s.COEFF[23] = 820.0 ; 
	FirFilter_3180_s.COEFF[24] = 853.0 ; 
	FirFilter_3180_s.COEFF[25] = 886.0 ; 
	FirFilter_3180_s.COEFF[26] = 919.0 ; 
	FirFilter_3180_s.COEFF[27] = 952.0 ; 
	FirFilter_3180_s.COEFF[28] = 985.0 ; 
	FirFilter_3180_s.COEFF[29] = 1018.0 ; 
	FirFilter_3180_s.COEFF[30] = 1051.0 ; 
	FirFilter_3180_s.COEFF[31] = 1084.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar591, 0,  < , 2, streamItVar591++) {
			pop_void(&SplitJoin135_FirFilter_Fiss_3251_3285_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin135_FirFilter_Fiss_3251_3285_split[2], i) * FirFilter_3180_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin135_FirFilter_Fiss_3251_3285_split[2]) ; 
		push_float(&SplitJoin135_FirFilter_Fiss_3251_3285_join[2], sum) ; 
 {
		pop_void(&SplitJoin135_FirFilter_Fiss_3251_3285_split[2]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_3181
	 {
	FirFilter_3181_s.COEFF[0] = 61.0 ; 
	FirFilter_3181_s.COEFF[1] = 94.0 ; 
	FirFilter_3181_s.COEFF[2] = 127.0 ; 
	FirFilter_3181_s.COEFF[3] = 160.0 ; 
	FirFilter_3181_s.COEFF[4] = 193.0 ; 
	FirFilter_3181_s.COEFF[5] = 226.0 ; 
	FirFilter_3181_s.COEFF[6] = 259.0 ; 
	FirFilter_3181_s.COEFF[7] = 292.0 ; 
	FirFilter_3181_s.COEFF[8] = 325.0 ; 
	FirFilter_3181_s.COEFF[9] = 358.0 ; 
	FirFilter_3181_s.COEFF[10] = 391.0 ; 
	FirFilter_3181_s.COEFF[11] = 424.0 ; 
	FirFilter_3181_s.COEFF[12] = 457.0 ; 
	FirFilter_3181_s.COEFF[13] = 490.0 ; 
	FirFilter_3181_s.COEFF[14] = 523.0 ; 
	FirFilter_3181_s.COEFF[15] = 556.0 ; 
	FirFilter_3181_s.COEFF[16] = 589.0 ; 
	FirFilter_3181_s.COEFF[17] = 622.0 ; 
	FirFilter_3181_s.COEFF[18] = 655.0 ; 
	FirFilter_3181_s.COEFF[19] = 688.0 ; 
	FirFilter_3181_s.COEFF[20] = 721.0 ; 
	FirFilter_3181_s.COEFF[21] = 754.0 ; 
	FirFilter_3181_s.COEFF[22] = 787.0 ; 
	FirFilter_3181_s.COEFF[23] = 820.0 ; 
	FirFilter_3181_s.COEFF[24] = 853.0 ; 
	FirFilter_3181_s.COEFF[25] = 886.0 ; 
	FirFilter_3181_s.COEFF[26] = 919.0 ; 
	FirFilter_3181_s.COEFF[27] = 952.0 ; 
	FirFilter_3181_s.COEFF[28] = 985.0 ; 
	FirFilter_3181_s.COEFF[29] = 1018.0 ; 
	FirFilter_3181_s.COEFF[30] = 1051.0 ; 
	FirFilter_3181_s.COEFF[31] = 1084.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar592, 0,  < , 3, streamItVar592++) {
			pop_void(&SplitJoin135_FirFilter_Fiss_3251_3285_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin135_FirFilter_Fiss_3251_3285_split[3], i) * FirFilter_3181_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin135_FirFilter_Fiss_3251_3285_split[3]) ; 
		push_float(&SplitJoin135_FirFilter_Fiss_3251_3285_join[3], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3177
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3177DownSamp_2993, pop_float(&SplitJoin135_FirFilter_Fiss_3251_3285_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DownSamp_2993
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++) {
		push_float(&DownSamp_2993UpSamp_2994, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3177DownSamp_2993)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3177DownSamp_2993) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: UpSamp_2994
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++) {
		push_float(&UpSamp_2994WEIGHTED_ROUND_ROBIN_Splitter_3182, pop_float(&DownSamp_2993UpSamp_2994)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			push_float(&UpSamp_2994WEIGHTED_ROUND_ROBIN_Splitter_3182, 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3182
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin137_Delay_N_Fiss_3252_3286_split[__iter_], pop_float(&UpSamp_2994WEIGHTED_ROUND_ROBIN_Splitter_3182));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3184
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin137_Delay_N_Fiss_3252_3286_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3185
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin137_Delay_N_Fiss_3252_3286_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3186
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin137_Delay_N_Fiss_3252_3286_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3187
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin137_Delay_N_Fiss_3252_3286_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3183
	FOR(uint32_t, __iter_init_, 0, <, 9, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3183DUPLICATE_Splitter_3188, pop_float(&SplitJoin137_Delay_N_Fiss_3252_3286_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3188
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3183DUPLICATE_Splitter_3188);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin139_FirFilter_Fiss_3253_3287_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_3190
	 {
	FirFilter_3190_s.COEFF[0] = 42.0 ; 
	FirFilter_3190_s.COEFF[1] = 49.0 ; 
	FirFilter_3190_s.COEFF[2] = 56.0 ; 
	FirFilter_3190_s.COEFF[3] = 63.0 ; 
	FirFilter_3190_s.COEFF[4] = 70.0 ; 
	FirFilter_3190_s.COEFF[5] = 77.0 ; 
	FirFilter_3190_s.COEFF[6] = 84.0 ; 
	FirFilter_3190_s.COEFF[7] = 91.0 ; 
	FirFilter_3190_s.COEFF[8] = 98.0 ; 
	FirFilter_3190_s.COEFF[9] = 105.0 ; 
	FirFilter_3190_s.COEFF[10] = 112.0 ; 
	FirFilter_3190_s.COEFF[11] = 119.0 ; 
	FirFilter_3190_s.COEFF[12] = 126.0 ; 
	FirFilter_3190_s.COEFF[13] = 133.0 ; 
	FirFilter_3190_s.COEFF[14] = 140.0 ; 
	FirFilter_3190_s.COEFF[15] = 147.0 ; 
	FirFilter_3190_s.COEFF[16] = 154.0 ; 
	FirFilter_3190_s.COEFF[17] = 161.0 ; 
	FirFilter_3190_s.COEFF[18] = 168.0 ; 
	FirFilter_3190_s.COEFF[19] = 175.0 ; 
	FirFilter_3190_s.COEFF[20] = 182.0 ; 
	FirFilter_3190_s.COEFF[21] = 189.0 ; 
	FirFilter_3190_s.COEFF[22] = 196.0 ; 
	FirFilter_3190_s.COEFF[23] = 203.0 ; 
	FirFilter_3190_s.COEFF[24] = 210.0 ; 
	FirFilter_3190_s.COEFF[25] = 217.0 ; 
	FirFilter_3190_s.COEFF[26] = 224.0 ; 
	FirFilter_3190_s.COEFF[27] = 231.0 ; 
	FirFilter_3190_s.COEFF[28] = 238.0 ; 
	FirFilter_3190_s.COEFF[29] = 245.0 ; 
	FirFilter_3190_s.COEFF[30] = 252.0 ; 
	FirFilter_3190_s.COEFF[31] = 259.0 ; 
}
	 {
	float sum = 0.0;
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin139_FirFilter_Fiss_3253_3287_split[0], i) * FirFilter_3190_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin139_FirFilter_Fiss_3253_3287_split[0]) ; 
	push_float(&SplitJoin139_FirFilter_Fiss_3253_3287_join[0], sum) ; 
 {
	FOR(int, streamItVar585, 0,  < , 3, streamItVar585++) {
		pop_void(&SplitJoin139_FirFilter_Fiss_3253_3287_split[0]) ; 
	}
	ENDFOR
}
}
//--------------------------------
// --- init: FirFilter_3191
	 {
	FirFilter_3191_s.COEFF[0] = 42.0 ; 
	FirFilter_3191_s.COEFF[1] = 49.0 ; 
	FirFilter_3191_s.COEFF[2] = 56.0 ; 
	FirFilter_3191_s.COEFF[3] = 63.0 ; 
	FirFilter_3191_s.COEFF[4] = 70.0 ; 
	FirFilter_3191_s.COEFF[5] = 77.0 ; 
	FirFilter_3191_s.COEFF[6] = 84.0 ; 
	FirFilter_3191_s.COEFF[7] = 91.0 ; 
	FirFilter_3191_s.COEFF[8] = 98.0 ; 
	FirFilter_3191_s.COEFF[9] = 105.0 ; 
	FirFilter_3191_s.COEFF[10] = 112.0 ; 
	FirFilter_3191_s.COEFF[11] = 119.0 ; 
	FirFilter_3191_s.COEFF[12] = 126.0 ; 
	FirFilter_3191_s.COEFF[13] = 133.0 ; 
	FirFilter_3191_s.COEFF[14] = 140.0 ; 
	FirFilter_3191_s.COEFF[15] = 147.0 ; 
	FirFilter_3191_s.COEFF[16] = 154.0 ; 
	FirFilter_3191_s.COEFF[17] = 161.0 ; 
	FirFilter_3191_s.COEFF[18] = 168.0 ; 
	FirFilter_3191_s.COEFF[19] = 175.0 ; 
	FirFilter_3191_s.COEFF[20] = 182.0 ; 
	FirFilter_3191_s.COEFF[21] = 189.0 ; 
	FirFilter_3191_s.COEFF[22] = 196.0 ; 
	FirFilter_3191_s.COEFF[23] = 203.0 ; 
	FirFilter_3191_s.COEFF[24] = 210.0 ; 
	FirFilter_3191_s.COEFF[25] = 217.0 ; 
	FirFilter_3191_s.COEFF[26] = 224.0 ; 
	FirFilter_3191_s.COEFF[27] = 231.0 ; 
	FirFilter_3191_s.COEFF[28] = 238.0 ; 
	FirFilter_3191_s.COEFF[29] = 245.0 ; 
	FirFilter_3191_s.COEFF[30] = 252.0 ; 
	FirFilter_3191_s.COEFF[31] = 259.0 ; 
}
	 {
	float sum = 0.0;
 {
	pop_void(&SplitJoin139_FirFilter_Fiss_3253_3287_split[1]) ; 
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin139_FirFilter_Fiss_3253_3287_split[1], i) * FirFilter_3191_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin139_FirFilter_Fiss_3253_3287_split[1]) ; 
	push_float(&SplitJoin139_FirFilter_Fiss_3253_3287_join[1], sum) ; 
 {
	FOR(int, streamItVar586, 0,  < , 2, streamItVar586++) {
		pop_void(&SplitJoin139_FirFilter_Fiss_3253_3287_split[1]) ; 
	}
	ENDFOR
}
}
//--------------------------------
// --- init: FirFilter_3192
	 {
	FirFilter_3192_s.COEFF[0] = 42.0 ; 
	FirFilter_3192_s.COEFF[1] = 49.0 ; 
	FirFilter_3192_s.COEFF[2] = 56.0 ; 
	FirFilter_3192_s.COEFF[3] = 63.0 ; 
	FirFilter_3192_s.COEFF[4] = 70.0 ; 
	FirFilter_3192_s.COEFF[5] = 77.0 ; 
	FirFilter_3192_s.COEFF[6] = 84.0 ; 
	FirFilter_3192_s.COEFF[7] = 91.0 ; 
	FirFilter_3192_s.COEFF[8] = 98.0 ; 
	FirFilter_3192_s.COEFF[9] = 105.0 ; 
	FirFilter_3192_s.COEFF[10] = 112.0 ; 
	FirFilter_3192_s.COEFF[11] = 119.0 ; 
	FirFilter_3192_s.COEFF[12] = 126.0 ; 
	FirFilter_3192_s.COEFF[13] = 133.0 ; 
	FirFilter_3192_s.COEFF[14] = 140.0 ; 
	FirFilter_3192_s.COEFF[15] = 147.0 ; 
	FirFilter_3192_s.COEFF[16] = 154.0 ; 
	FirFilter_3192_s.COEFF[17] = 161.0 ; 
	FirFilter_3192_s.COEFF[18] = 168.0 ; 
	FirFilter_3192_s.COEFF[19] = 175.0 ; 
	FirFilter_3192_s.COEFF[20] = 182.0 ; 
	FirFilter_3192_s.COEFF[21] = 189.0 ; 
	FirFilter_3192_s.COEFF[22] = 196.0 ; 
	FirFilter_3192_s.COEFF[23] = 203.0 ; 
	FirFilter_3192_s.COEFF[24] = 210.0 ; 
	FirFilter_3192_s.COEFF[25] = 217.0 ; 
	FirFilter_3192_s.COEFF[26] = 224.0 ; 
	FirFilter_3192_s.COEFF[27] = 231.0 ; 
	FirFilter_3192_s.COEFF[28] = 238.0 ; 
	FirFilter_3192_s.COEFF[29] = 245.0 ; 
	FirFilter_3192_s.COEFF[30] = 252.0 ; 
	FirFilter_3192_s.COEFF[31] = 259.0 ; 
}
	 {
	float sum = 0.0;
 {
	FOR(int, streamItVar587, 0,  < , 2, streamItVar587++) {
		pop_void(&SplitJoin139_FirFilter_Fiss_3253_3287_split[2]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin139_FirFilter_Fiss_3253_3287_split[2], i) * FirFilter_3192_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin139_FirFilter_Fiss_3253_3287_split[2]) ; 
	push_float(&SplitJoin139_FirFilter_Fiss_3253_3287_join[2], sum) ; 
 {
	pop_void(&SplitJoin139_FirFilter_Fiss_3253_3287_split[2]) ; 
}
}
//--------------------------------
// --- init: FirFilter_3193
	 {
	FirFilter_3193_s.COEFF[0] = 42.0 ; 
	FirFilter_3193_s.COEFF[1] = 49.0 ; 
	FirFilter_3193_s.COEFF[2] = 56.0 ; 
	FirFilter_3193_s.COEFF[3] = 63.0 ; 
	FirFilter_3193_s.COEFF[4] = 70.0 ; 
	FirFilter_3193_s.COEFF[5] = 77.0 ; 
	FirFilter_3193_s.COEFF[6] = 84.0 ; 
	FirFilter_3193_s.COEFF[7] = 91.0 ; 
	FirFilter_3193_s.COEFF[8] = 98.0 ; 
	FirFilter_3193_s.COEFF[9] = 105.0 ; 
	FirFilter_3193_s.COEFF[10] = 112.0 ; 
	FirFilter_3193_s.COEFF[11] = 119.0 ; 
	FirFilter_3193_s.COEFF[12] = 126.0 ; 
	FirFilter_3193_s.COEFF[13] = 133.0 ; 
	FirFilter_3193_s.COEFF[14] = 140.0 ; 
	FirFilter_3193_s.COEFF[15] = 147.0 ; 
	FirFilter_3193_s.COEFF[16] = 154.0 ; 
	FirFilter_3193_s.COEFF[17] = 161.0 ; 
	FirFilter_3193_s.COEFF[18] = 168.0 ; 
	FirFilter_3193_s.COEFF[19] = 175.0 ; 
	FirFilter_3193_s.COEFF[20] = 182.0 ; 
	FirFilter_3193_s.COEFF[21] = 189.0 ; 
	FirFilter_3193_s.COEFF[22] = 196.0 ; 
	FirFilter_3193_s.COEFF[23] = 203.0 ; 
	FirFilter_3193_s.COEFF[24] = 210.0 ; 
	FirFilter_3193_s.COEFF[25] = 217.0 ; 
	FirFilter_3193_s.COEFF[26] = 224.0 ; 
	FirFilter_3193_s.COEFF[27] = 231.0 ; 
	FirFilter_3193_s.COEFF[28] = 238.0 ; 
	FirFilter_3193_s.COEFF[29] = 245.0 ; 
	FirFilter_3193_s.COEFF[30] = 252.0 ; 
	FirFilter_3193_s.COEFF[31] = 259.0 ; 
}
	 {
	float sum = 0.0;
 {
	FOR(int, streamItVar588, 0,  < , 3, streamItVar588++) {
		pop_void(&SplitJoin139_FirFilter_Fiss_3253_3287_split[3]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin139_FirFilter_Fiss_3253_3287_split[3], i) * FirFilter_3193_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin139_FirFilter_Fiss_3253_3287_split[3]) ; 
	push_float(&SplitJoin139_FirFilter_Fiss_3253_3287_join[3], sum) ; 
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3189
	
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[6], pop_float(&SplitJoin139_FirFilter_Fiss_3253_3287_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3194
	FOR(uint32_t, __iter_init_, 0, <, 75, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin154_Delay_N_Fiss_3254_3288_split[__iter_], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_split[7]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3196
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin154_Delay_N_Fiss_3254_3288_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3197
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin154_Delay_N_Fiss_3254_3288_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3198
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin154_Delay_N_Fiss_3254_3288_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3199
	FOR(uint32_t, __iter_init_, 0, <, 47, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin154_Delay_N_Fiss_3254_3288_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3195
	FOR(uint32_t, __iter_init_, 0, <, 46, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3195DUPLICATE_Splitter_3200, pop_float(&SplitJoin154_Delay_N_Fiss_3254_3288_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3200
	FOR(uint32_t, __iter_init_, 0, <, 183, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3195DUPLICATE_Splitter_3200);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin156_FirFilter_Fiss_3255_3289_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_3202
	 {
	FirFilter_3202_s.COEFF[0] = 71.0 ; 
	FirFilter_3202_s.COEFF[1] = 104.0 ; 
	FirFilter_3202_s.COEFF[2] = 137.0 ; 
	FirFilter_3202_s.COEFF[3] = 170.0 ; 
	FirFilter_3202_s.COEFF[4] = 203.0 ; 
	FirFilter_3202_s.COEFF[5] = 236.0 ; 
	FirFilter_3202_s.COEFF[6] = 269.0 ; 
	FirFilter_3202_s.COEFF[7] = 302.0 ; 
	FirFilter_3202_s.COEFF[8] = 335.0 ; 
	FirFilter_3202_s.COEFF[9] = 368.0 ; 
	FirFilter_3202_s.COEFF[10] = 401.0 ; 
	FirFilter_3202_s.COEFF[11] = 434.0 ; 
	FirFilter_3202_s.COEFF[12] = 467.0 ; 
	FirFilter_3202_s.COEFF[13] = 500.0 ; 
	FirFilter_3202_s.COEFF[14] = 533.0 ; 
	FirFilter_3202_s.COEFF[15] = 566.0 ; 
	FirFilter_3202_s.COEFF[16] = 599.0 ; 
	FirFilter_3202_s.COEFF[17] = 632.0 ; 
	FirFilter_3202_s.COEFF[18] = 665.0 ; 
	FirFilter_3202_s.COEFF[19] = 698.0 ; 
	FirFilter_3202_s.COEFF[20] = 731.0 ; 
	FirFilter_3202_s.COEFF[21] = 764.0 ; 
	FirFilter_3202_s.COEFF[22] = 797.0 ; 
	FirFilter_3202_s.COEFF[23] = 830.0 ; 
	FirFilter_3202_s.COEFF[24] = 863.0 ; 
	FirFilter_3202_s.COEFF[25] = 896.0 ; 
	FirFilter_3202_s.COEFF[26] = 929.0 ; 
	FirFilter_3202_s.COEFF[27] = 962.0 ; 
	FirFilter_3202_s.COEFF[28] = 995.0 ; 
	FirFilter_3202_s.COEFF[29] = 1028.0 ; 
	FirFilter_3202_s.COEFF[30] = 1061.0 ; 
	FirFilter_3202_s.COEFF[31] = 1094.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin156_FirFilter_Fiss_3255_3289_split[0], i) * FirFilter_3202_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin156_FirFilter_Fiss_3255_3289_split[0]) ; 
		push_float(&SplitJoin156_FirFilter_Fiss_3255_3289_join[0], sum) ; 
 {
		FOR(int, streamItVar581, 0,  < , 3, streamItVar581++) {
			pop_void(&SplitJoin156_FirFilter_Fiss_3255_3289_split[0]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_3203
	 {
	FirFilter_3203_s.COEFF[0] = 71.0 ; 
	FirFilter_3203_s.COEFF[1] = 104.0 ; 
	FirFilter_3203_s.COEFF[2] = 137.0 ; 
	FirFilter_3203_s.COEFF[3] = 170.0 ; 
	FirFilter_3203_s.COEFF[4] = 203.0 ; 
	FirFilter_3203_s.COEFF[5] = 236.0 ; 
	FirFilter_3203_s.COEFF[6] = 269.0 ; 
	FirFilter_3203_s.COEFF[7] = 302.0 ; 
	FirFilter_3203_s.COEFF[8] = 335.0 ; 
	FirFilter_3203_s.COEFF[9] = 368.0 ; 
	FirFilter_3203_s.COEFF[10] = 401.0 ; 
	FirFilter_3203_s.COEFF[11] = 434.0 ; 
	FirFilter_3203_s.COEFF[12] = 467.0 ; 
	FirFilter_3203_s.COEFF[13] = 500.0 ; 
	FirFilter_3203_s.COEFF[14] = 533.0 ; 
	FirFilter_3203_s.COEFF[15] = 566.0 ; 
	FirFilter_3203_s.COEFF[16] = 599.0 ; 
	FirFilter_3203_s.COEFF[17] = 632.0 ; 
	FirFilter_3203_s.COEFF[18] = 665.0 ; 
	FirFilter_3203_s.COEFF[19] = 698.0 ; 
	FirFilter_3203_s.COEFF[20] = 731.0 ; 
	FirFilter_3203_s.COEFF[21] = 764.0 ; 
	FirFilter_3203_s.COEFF[22] = 797.0 ; 
	FirFilter_3203_s.COEFF[23] = 830.0 ; 
	FirFilter_3203_s.COEFF[24] = 863.0 ; 
	FirFilter_3203_s.COEFF[25] = 896.0 ; 
	FirFilter_3203_s.COEFF[26] = 929.0 ; 
	FirFilter_3203_s.COEFF[27] = 962.0 ; 
	FirFilter_3203_s.COEFF[28] = 995.0 ; 
	FirFilter_3203_s.COEFF[29] = 1028.0 ; 
	FirFilter_3203_s.COEFF[30] = 1061.0 ; 
	FirFilter_3203_s.COEFF[31] = 1094.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin156_FirFilter_Fiss_3255_3289_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin156_FirFilter_Fiss_3255_3289_split[1], i) * FirFilter_3203_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin156_FirFilter_Fiss_3255_3289_split[1]) ; 
		push_float(&SplitJoin156_FirFilter_Fiss_3255_3289_join[1], sum) ; 
 {
		FOR(int, streamItVar582, 0,  < , 2, streamItVar582++) {
			pop_void(&SplitJoin156_FirFilter_Fiss_3255_3289_split[1]) ; 
		}
		ENDFOR
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_3204
	 {
	FirFilter_3204_s.COEFF[0] = 71.0 ; 
	FirFilter_3204_s.COEFF[1] = 104.0 ; 
	FirFilter_3204_s.COEFF[2] = 137.0 ; 
	FirFilter_3204_s.COEFF[3] = 170.0 ; 
	FirFilter_3204_s.COEFF[4] = 203.0 ; 
	FirFilter_3204_s.COEFF[5] = 236.0 ; 
	FirFilter_3204_s.COEFF[6] = 269.0 ; 
	FirFilter_3204_s.COEFF[7] = 302.0 ; 
	FirFilter_3204_s.COEFF[8] = 335.0 ; 
	FirFilter_3204_s.COEFF[9] = 368.0 ; 
	FirFilter_3204_s.COEFF[10] = 401.0 ; 
	FirFilter_3204_s.COEFF[11] = 434.0 ; 
	FirFilter_3204_s.COEFF[12] = 467.0 ; 
	FirFilter_3204_s.COEFF[13] = 500.0 ; 
	FirFilter_3204_s.COEFF[14] = 533.0 ; 
	FirFilter_3204_s.COEFF[15] = 566.0 ; 
	FirFilter_3204_s.COEFF[16] = 599.0 ; 
	FirFilter_3204_s.COEFF[17] = 632.0 ; 
	FirFilter_3204_s.COEFF[18] = 665.0 ; 
	FirFilter_3204_s.COEFF[19] = 698.0 ; 
	FirFilter_3204_s.COEFF[20] = 731.0 ; 
	FirFilter_3204_s.COEFF[21] = 764.0 ; 
	FirFilter_3204_s.COEFF[22] = 797.0 ; 
	FirFilter_3204_s.COEFF[23] = 830.0 ; 
	FirFilter_3204_s.COEFF[24] = 863.0 ; 
	FirFilter_3204_s.COEFF[25] = 896.0 ; 
	FirFilter_3204_s.COEFF[26] = 929.0 ; 
	FirFilter_3204_s.COEFF[27] = 962.0 ; 
	FirFilter_3204_s.COEFF[28] = 995.0 ; 
	FirFilter_3204_s.COEFF[29] = 1028.0 ; 
	FirFilter_3204_s.COEFF[30] = 1061.0 ; 
	FirFilter_3204_s.COEFF[31] = 1094.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar583, 0,  < , 2, streamItVar583++) {
			pop_void(&SplitJoin156_FirFilter_Fiss_3255_3289_split[2]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin156_FirFilter_Fiss_3255_3289_split[2], i) * FirFilter_3204_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin156_FirFilter_Fiss_3255_3289_split[2]) ; 
		push_float(&SplitJoin156_FirFilter_Fiss_3255_3289_join[2], sum) ; 
 {
		pop_void(&SplitJoin156_FirFilter_Fiss_3255_3289_split[2]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_3205
	 {
	FirFilter_3205_s.COEFF[0] = 71.0 ; 
	FirFilter_3205_s.COEFF[1] = 104.0 ; 
	FirFilter_3205_s.COEFF[2] = 137.0 ; 
	FirFilter_3205_s.COEFF[3] = 170.0 ; 
	FirFilter_3205_s.COEFF[4] = 203.0 ; 
	FirFilter_3205_s.COEFF[5] = 236.0 ; 
	FirFilter_3205_s.COEFF[6] = 269.0 ; 
	FirFilter_3205_s.COEFF[7] = 302.0 ; 
	FirFilter_3205_s.COEFF[8] = 335.0 ; 
	FirFilter_3205_s.COEFF[9] = 368.0 ; 
	FirFilter_3205_s.COEFF[10] = 401.0 ; 
	FirFilter_3205_s.COEFF[11] = 434.0 ; 
	FirFilter_3205_s.COEFF[12] = 467.0 ; 
	FirFilter_3205_s.COEFF[13] = 500.0 ; 
	FirFilter_3205_s.COEFF[14] = 533.0 ; 
	FirFilter_3205_s.COEFF[15] = 566.0 ; 
	FirFilter_3205_s.COEFF[16] = 599.0 ; 
	FirFilter_3205_s.COEFF[17] = 632.0 ; 
	FirFilter_3205_s.COEFF[18] = 665.0 ; 
	FirFilter_3205_s.COEFF[19] = 698.0 ; 
	FirFilter_3205_s.COEFF[20] = 731.0 ; 
	FirFilter_3205_s.COEFF[21] = 764.0 ; 
	FirFilter_3205_s.COEFF[22] = 797.0 ; 
	FirFilter_3205_s.COEFF[23] = 830.0 ; 
	FirFilter_3205_s.COEFF[24] = 863.0 ; 
	FirFilter_3205_s.COEFF[25] = 896.0 ; 
	FirFilter_3205_s.COEFF[26] = 929.0 ; 
	FirFilter_3205_s.COEFF[27] = 962.0 ; 
	FirFilter_3205_s.COEFF[28] = 995.0 ; 
	FirFilter_3205_s.COEFF[29] = 1028.0 ; 
	FirFilter_3205_s.COEFF[30] = 1061.0 ; 
	FirFilter_3205_s.COEFF[31] = 1094.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++) {
		float sum = 0.0;
 {
		FOR(int, streamItVar584, 0,  < , 3, streamItVar584++) {
			pop_void(&SplitJoin156_FirFilter_Fiss_3255_3289_split[3]) ; 
		}
		ENDFOR
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin156_FirFilter_Fiss_3255_3289_split[3], i) * FirFilter_3205_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin156_FirFilter_Fiss_3255_3289_split[3]) ; 
		push_float(&SplitJoin156_FirFilter_Fiss_3255_3289_join[3], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3201
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3201DownSamp_3000, pop_float(&SplitJoin156_FirFilter_Fiss_3255_3289_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DownSamp_3000
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++) {
		push_float(&DownSamp_3000UpSamp_3001, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3201DownSamp_3000)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3201DownSamp_3000) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: UpSamp_3001
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++) {
		push_float(&UpSamp_3001WEIGHTED_ROUND_ROBIN_Splitter_3206, pop_float(&DownSamp_3000UpSamp_3001)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			push_float(&UpSamp_3001WEIGHTED_ROUND_ROBIN_Splitter_3206, 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3206
	FOR(uint32_t, __iter_init_, 0, <, 38, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&SplitJoin158_Delay_N_Fiss_3256_3290_split[__iter_], pop_float(&UpSamp_3001WEIGHTED_ROUND_ROBIN_Splitter_3206));
		ENDFOR
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3208
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin158_Delay_N_Fiss_3256_3290_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3209
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin158_Delay_N_Fiss_3256_3290_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3210
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin158_Delay_N_Fiss_3256_3290_join[2], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3211
	FOR(uint32_t, __iter_init_, 0, <, 10, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin158_Delay_N_Fiss_3256_3290_join[3], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3207
	FOR(uint32_t, __iter_init_, 0, <, 9, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3207DUPLICATE_Splitter_3212, pop_float(&SplitJoin158_Delay_N_Fiss_3256_3290_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3212
	FOR(uint32_t, __iter_init_, 0, <, 35, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3207DUPLICATE_Splitter_3212);
		FOR(uint32_t, __iter_dup_, 0, <, 4, __iter_dup_++)
			push_float(&SplitJoin160_FirFilter_Fiss_3257_3291_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_3214
	 {
	FirFilter_3214_s.COEFF[0] = 56.0 ; 
	FirFilter_3214_s.COEFF[1] = 64.0 ; 
	FirFilter_3214_s.COEFF[2] = 72.0 ; 
	FirFilter_3214_s.COEFF[3] = 80.0 ; 
	FirFilter_3214_s.COEFF[4] = 88.0 ; 
	FirFilter_3214_s.COEFF[5] = 96.0 ; 
	FirFilter_3214_s.COEFF[6] = 104.0 ; 
	FirFilter_3214_s.COEFF[7] = 112.0 ; 
	FirFilter_3214_s.COEFF[8] = 120.0 ; 
	FirFilter_3214_s.COEFF[9] = 128.0 ; 
	FirFilter_3214_s.COEFF[10] = 136.0 ; 
	FirFilter_3214_s.COEFF[11] = 144.0 ; 
	FirFilter_3214_s.COEFF[12] = 152.0 ; 
	FirFilter_3214_s.COEFF[13] = 160.0 ; 
	FirFilter_3214_s.COEFF[14] = 168.0 ; 
	FirFilter_3214_s.COEFF[15] = 176.0 ; 
	FirFilter_3214_s.COEFF[16] = 184.0 ; 
	FirFilter_3214_s.COEFF[17] = 192.0 ; 
	FirFilter_3214_s.COEFF[18] = 200.0 ; 
	FirFilter_3214_s.COEFF[19] = 208.0 ; 
	FirFilter_3214_s.COEFF[20] = 216.0 ; 
	FirFilter_3214_s.COEFF[21] = 224.0 ; 
	FirFilter_3214_s.COEFF[22] = 232.0 ; 
	FirFilter_3214_s.COEFF[23] = 240.0 ; 
	FirFilter_3214_s.COEFF[24] = 248.0 ; 
	FirFilter_3214_s.COEFF[25] = 256.0 ; 
	FirFilter_3214_s.COEFF[26] = 264.0 ; 
	FirFilter_3214_s.COEFF[27] = 272.0 ; 
	FirFilter_3214_s.COEFF[28] = 280.0 ; 
	FirFilter_3214_s.COEFF[29] = 288.0 ; 
	FirFilter_3214_s.COEFF[30] = 296.0 ; 
	FirFilter_3214_s.COEFF[31] = 304.0 ; 
}
	 {
	float sum = 0.0;
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin160_FirFilter_Fiss_3257_3291_split[0], i) * FirFilter_3214_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin160_FirFilter_Fiss_3257_3291_split[0]) ; 
	push_float(&SplitJoin160_FirFilter_Fiss_3257_3291_join[0], sum) ; 
 {
	FOR(int, streamItVar577, 0,  < , 3, streamItVar577++) {
		pop_void(&SplitJoin160_FirFilter_Fiss_3257_3291_split[0]) ; 
	}
	ENDFOR
}
}
//--------------------------------
// --- init: FirFilter_3215
	 {
	FirFilter_3215_s.COEFF[0] = 56.0 ; 
	FirFilter_3215_s.COEFF[1] = 64.0 ; 
	FirFilter_3215_s.COEFF[2] = 72.0 ; 
	FirFilter_3215_s.COEFF[3] = 80.0 ; 
	FirFilter_3215_s.COEFF[4] = 88.0 ; 
	FirFilter_3215_s.COEFF[5] = 96.0 ; 
	FirFilter_3215_s.COEFF[6] = 104.0 ; 
	FirFilter_3215_s.COEFF[7] = 112.0 ; 
	FirFilter_3215_s.COEFF[8] = 120.0 ; 
	FirFilter_3215_s.COEFF[9] = 128.0 ; 
	FirFilter_3215_s.COEFF[10] = 136.0 ; 
	FirFilter_3215_s.COEFF[11] = 144.0 ; 
	FirFilter_3215_s.COEFF[12] = 152.0 ; 
	FirFilter_3215_s.COEFF[13] = 160.0 ; 
	FirFilter_3215_s.COEFF[14] = 168.0 ; 
	FirFilter_3215_s.COEFF[15] = 176.0 ; 
	FirFilter_3215_s.COEFF[16] = 184.0 ; 
	FirFilter_3215_s.COEFF[17] = 192.0 ; 
	FirFilter_3215_s.COEFF[18] = 200.0 ; 
	FirFilter_3215_s.COEFF[19] = 208.0 ; 
	FirFilter_3215_s.COEFF[20] = 216.0 ; 
	FirFilter_3215_s.COEFF[21] = 224.0 ; 
	FirFilter_3215_s.COEFF[22] = 232.0 ; 
	FirFilter_3215_s.COEFF[23] = 240.0 ; 
	FirFilter_3215_s.COEFF[24] = 248.0 ; 
	FirFilter_3215_s.COEFF[25] = 256.0 ; 
	FirFilter_3215_s.COEFF[26] = 264.0 ; 
	FirFilter_3215_s.COEFF[27] = 272.0 ; 
	FirFilter_3215_s.COEFF[28] = 280.0 ; 
	FirFilter_3215_s.COEFF[29] = 288.0 ; 
	FirFilter_3215_s.COEFF[30] = 296.0 ; 
	FirFilter_3215_s.COEFF[31] = 304.0 ; 
}
	 {
	float sum = 0.0;
 {
	pop_void(&SplitJoin160_FirFilter_Fiss_3257_3291_split[1]) ; 
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin160_FirFilter_Fiss_3257_3291_split[1], i) * FirFilter_3215_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin160_FirFilter_Fiss_3257_3291_split[1]) ; 
	push_float(&SplitJoin160_FirFilter_Fiss_3257_3291_join[1], sum) ; 
 {
	FOR(int, streamItVar578, 0,  < , 2, streamItVar578++) {
		pop_void(&SplitJoin160_FirFilter_Fiss_3257_3291_split[1]) ; 
	}
	ENDFOR
}
}
//--------------------------------
// --- init: FirFilter_3216
	 {
	FirFilter_3216_s.COEFF[0] = 56.0 ; 
	FirFilter_3216_s.COEFF[1] = 64.0 ; 
	FirFilter_3216_s.COEFF[2] = 72.0 ; 
	FirFilter_3216_s.COEFF[3] = 80.0 ; 
	FirFilter_3216_s.COEFF[4] = 88.0 ; 
	FirFilter_3216_s.COEFF[5] = 96.0 ; 
	FirFilter_3216_s.COEFF[6] = 104.0 ; 
	FirFilter_3216_s.COEFF[7] = 112.0 ; 
	FirFilter_3216_s.COEFF[8] = 120.0 ; 
	FirFilter_3216_s.COEFF[9] = 128.0 ; 
	FirFilter_3216_s.COEFF[10] = 136.0 ; 
	FirFilter_3216_s.COEFF[11] = 144.0 ; 
	FirFilter_3216_s.COEFF[12] = 152.0 ; 
	FirFilter_3216_s.COEFF[13] = 160.0 ; 
	FirFilter_3216_s.COEFF[14] = 168.0 ; 
	FirFilter_3216_s.COEFF[15] = 176.0 ; 
	FirFilter_3216_s.COEFF[16] = 184.0 ; 
	FirFilter_3216_s.COEFF[17] = 192.0 ; 
	FirFilter_3216_s.COEFF[18] = 200.0 ; 
	FirFilter_3216_s.COEFF[19] = 208.0 ; 
	FirFilter_3216_s.COEFF[20] = 216.0 ; 
	FirFilter_3216_s.COEFF[21] = 224.0 ; 
	FirFilter_3216_s.COEFF[22] = 232.0 ; 
	FirFilter_3216_s.COEFF[23] = 240.0 ; 
	FirFilter_3216_s.COEFF[24] = 248.0 ; 
	FirFilter_3216_s.COEFF[25] = 256.0 ; 
	FirFilter_3216_s.COEFF[26] = 264.0 ; 
	FirFilter_3216_s.COEFF[27] = 272.0 ; 
	FirFilter_3216_s.COEFF[28] = 280.0 ; 
	FirFilter_3216_s.COEFF[29] = 288.0 ; 
	FirFilter_3216_s.COEFF[30] = 296.0 ; 
	FirFilter_3216_s.COEFF[31] = 304.0 ; 
}
	 {
	float sum = 0.0;
 {
	FOR(int, streamItVar579, 0,  < , 2, streamItVar579++) {
		pop_void(&SplitJoin160_FirFilter_Fiss_3257_3291_split[2]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin160_FirFilter_Fiss_3257_3291_split[2], i) * FirFilter_3216_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin160_FirFilter_Fiss_3257_3291_split[2]) ; 
	push_float(&SplitJoin160_FirFilter_Fiss_3257_3291_join[2], sum) ; 
 {
	pop_void(&SplitJoin160_FirFilter_Fiss_3257_3291_split[2]) ; 
}
}
//--------------------------------
// --- init: FirFilter_3217
	 {
	FirFilter_3217_s.COEFF[0] = 56.0 ; 
	FirFilter_3217_s.COEFF[1] = 64.0 ; 
	FirFilter_3217_s.COEFF[2] = 72.0 ; 
	FirFilter_3217_s.COEFF[3] = 80.0 ; 
	FirFilter_3217_s.COEFF[4] = 88.0 ; 
	FirFilter_3217_s.COEFF[5] = 96.0 ; 
	FirFilter_3217_s.COEFF[6] = 104.0 ; 
	FirFilter_3217_s.COEFF[7] = 112.0 ; 
	FirFilter_3217_s.COEFF[8] = 120.0 ; 
	FirFilter_3217_s.COEFF[9] = 128.0 ; 
	FirFilter_3217_s.COEFF[10] = 136.0 ; 
	FirFilter_3217_s.COEFF[11] = 144.0 ; 
	FirFilter_3217_s.COEFF[12] = 152.0 ; 
	FirFilter_3217_s.COEFF[13] = 160.0 ; 
	FirFilter_3217_s.COEFF[14] = 168.0 ; 
	FirFilter_3217_s.COEFF[15] = 176.0 ; 
	FirFilter_3217_s.COEFF[16] = 184.0 ; 
	FirFilter_3217_s.COEFF[17] = 192.0 ; 
	FirFilter_3217_s.COEFF[18] = 200.0 ; 
	FirFilter_3217_s.COEFF[19] = 208.0 ; 
	FirFilter_3217_s.COEFF[20] = 216.0 ; 
	FirFilter_3217_s.COEFF[21] = 224.0 ; 
	FirFilter_3217_s.COEFF[22] = 232.0 ; 
	FirFilter_3217_s.COEFF[23] = 240.0 ; 
	FirFilter_3217_s.COEFF[24] = 248.0 ; 
	FirFilter_3217_s.COEFF[25] = 256.0 ; 
	FirFilter_3217_s.COEFF[26] = 264.0 ; 
	FirFilter_3217_s.COEFF[27] = 272.0 ; 
	FirFilter_3217_s.COEFF[28] = 280.0 ; 
	FirFilter_3217_s.COEFF[29] = 288.0 ; 
	FirFilter_3217_s.COEFF[30] = 296.0 ; 
	FirFilter_3217_s.COEFF[31] = 304.0 ; 
}
	 {
	float sum = 0.0;
 {
	FOR(int, streamItVar580, 0,  < , 3, streamItVar580++) {
		pop_void(&SplitJoin160_FirFilter_Fiss_3257_3291_split[3]) ; 
	}
	ENDFOR
}
	sum = 0.0 ; 
	sum = 0.0 ; 
	FOR(int, i, 0,  < , 32, i++) {
		sum = (sum + (peek_float(&SplitJoin160_FirFilter_Fiss_3257_3291_split[3], i) * FirFilter_3217_s.COEFF[(31 - i)])) ; 
	}
	ENDFOR
	pop_float(&SplitJoin160_FirFilter_Fiss_3257_3291_split[3]) ; 
	push_float(&SplitJoin160_FirFilter_Fiss_3257_3291_join[3], sum) ; 
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3213
	
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[7], pop_float(&SplitJoin160_FirFilter_Fiss_3257_3291_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3007
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3007WEIGHTED_ROUND_ROBIN_Splitter_3218, pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_2944_3008_3224_3259_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3218
	
	FOR(uint32_t, __iter_dec_, 0, <, 4, __iter_dec_++)
		FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
			push_float(&SplitJoin10_Combine_Fiss_3229_3292_split[__iter_dec_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3007WEIGHTED_ROUND_ROBIN_Splitter_3218));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: Combine_3220
	 {
	float sum = 0.0;
	FOR(int, i, 0,  < , 8, i++) {
		sum = (sum + pop_float(&SplitJoin10_Combine_Fiss_3229_3292_split[0])) ; 
	}
	ENDFOR
	push_float(&SplitJoin10_Combine_Fiss_3229_3292_join[0], sum) ; 
}
//--------------------------------
// --- init: Combine_3221
	 {
	float sum = 0.0;
	FOR(int, i, 0,  < , 8, i++) {
		sum = (sum + pop_float(&SplitJoin10_Combine_Fiss_3229_3292_split[1])) ; 
	}
	ENDFOR
	push_float(&SplitJoin10_Combine_Fiss_3229_3292_join[1], sum) ; 
}
//--------------------------------
// --- init: Combine_3222
	 {
	float sum = 0.0;
	FOR(int, i, 0,  < , 8, i++) {
		sum = (sum + pop_float(&SplitJoin10_Combine_Fiss_3229_3292_split[2])) ; 
	}
	ENDFOR
	push_float(&SplitJoin10_Combine_Fiss_3229_3292_join[2], sum) ; 
}
//--------------------------------
// --- init: Combine_3223
	 {
	float sum = 0.0;
	FOR(int, i, 0,  < , 8, i++) {
		sum = (sum + pop_float(&SplitJoin10_Combine_Fiss_3229_3292_split[3])) ; 
	}
	ENDFOR
	push_float(&SplitJoin10_Combine_Fiss_3229_3292_join[3], sum) ; 
}
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3219
	
	FOR(uint32_t, __iter_, 0, <, 4, __iter_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3219sink_3005, pop_float(&SplitJoin10_Combine_Fiss_3229_3292_join[__iter_]));
	ENDFOR
//--------------------------------
// --- init: sink_3005
	FOR(uint32_t, __iter_init_, 0, <, 4, __iter_init_++) {
		printf("%.10f", pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3219sink_3005));
		printf("\n");
	}
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		source_2946();
		DUPLICATE_Splitter_3006();
			WEIGHTED_ROUND_ROBIN_Splitter_3026();
				Delay_N_3028();
				Delay_N_3029();
				Delay_N_3030();
				Delay_N_3031();
			WEIGHTED_ROUND_ROBIN_Joiner_3027();
			DUPLICATE_Splitter_3032();
				FirFilter_3034();
				FirFilter_3035();
				FirFilter_3036();
				FirFilter_3037();
			WEIGHTED_ROUND_ROBIN_Joiner_3033();
			DownSamp_2951();
			UpSamp_2952();
			WEIGHTED_ROUND_ROBIN_Splitter_3038();
				Delay_N_3040();
				Delay_N_3041();
				Delay_N_3042();
				Delay_N_3043();
			WEIGHTED_ROUND_ROBIN_Joiner_3039();
			DUPLICATE_Splitter_3044();
				FirFilter_3046();
				FirFilter_3047();
				FirFilter_3048();
				FirFilter_3049();
			WEIGHTED_ROUND_ROBIN_Joiner_3045();
			WEIGHTED_ROUND_ROBIN_Splitter_3050();
				Delay_N_3052();
				Delay_N_3053();
				Delay_N_3054();
				Delay_N_3055();
			WEIGHTED_ROUND_ROBIN_Joiner_3051();
			DUPLICATE_Splitter_3056();
				FirFilter_3058();
				FirFilter_3059();
				FirFilter_3060();
				FirFilter_3061();
			WEIGHTED_ROUND_ROBIN_Joiner_3057();
			DownSamp_2958();
			UpSamp_2959();
			WEIGHTED_ROUND_ROBIN_Splitter_3062();
				Delay_N_3064();
				Delay_N_3065();
				Delay_N_3066();
				Delay_N_3067();
			WEIGHTED_ROUND_ROBIN_Joiner_3063();
			DUPLICATE_Splitter_3068();
				FirFilter_3070();
				FirFilter_3071();
				FirFilter_3072();
				FirFilter_3073();
			WEIGHTED_ROUND_ROBIN_Joiner_3069();
			WEIGHTED_ROUND_ROBIN_Splitter_3074();
				Delay_N_3076();
				Delay_N_3077();
				Delay_N_3078();
				Delay_N_3079();
			WEIGHTED_ROUND_ROBIN_Joiner_3075();
			DUPLICATE_Splitter_3080();
				FirFilter_3082();
				FirFilter_3083();
				FirFilter_3084();
				FirFilter_3085();
			WEIGHTED_ROUND_ROBIN_Joiner_3081();
			DownSamp_2965();
			UpSamp_2966();
			WEIGHTED_ROUND_ROBIN_Splitter_3086();
				Delay_N_3088();
				Delay_N_3089();
				Delay_N_3090();
				Delay_N_3091();
			WEIGHTED_ROUND_ROBIN_Joiner_3087();
			DUPLICATE_Splitter_3092();
				FirFilter_3094();
				FirFilter_3095();
				FirFilter_3096();
				FirFilter_3097();
			WEIGHTED_ROUND_ROBIN_Joiner_3093();
			WEIGHTED_ROUND_ROBIN_Splitter_3098();
				Delay_N_3100();
				Delay_N_3101();
				Delay_N_3102();
				Delay_N_3103();
			WEIGHTED_ROUND_ROBIN_Joiner_3099();
			DUPLICATE_Splitter_3104();
				FirFilter_3106();
				FirFilter_3107();
				FirFilter_3108();
				FirFilter_3109();
			WEIGHTED_ROUND_ROBIN_Joiner_3105();
			DownSamp_2972();
			UpSamp_2973();
			WEIGHTED_ROUND_ROBIN_Splitter_3110();
				Delay_N_3112();
				Delay_N_3113();
				Delay_N_3114();
				Delay_N_3115();
			WEIGHTED_ROUND_ROBIN_Joiner_3111();
			DUPLICATE_Splitter_3116();
				FirFilter_3118();
				FirFilter_3119();
				FirFilter_3120();
				FirFilter_3121();
			WEIGHTED_ROUND_ROBIN_Joiner_3117();
			WEIGHTED_ROUND_ROBIN_Splitter_3122();
				Delay_N_3124();
				Delay_N_3125();
				Delay_N_3126();
				Delay_N_3127();
			WEIGHTED_ROUND_ROBIN_Joiner_3123();
			DUPLICATE_Splitter_3128();
				FirFilter_3130();
				FirFilter_3131();
				FirFilter_3132();
				FirFilter_3133();
			WEIGHTED_ROUND_ROBIN_Joiner_3129();
			DownSamp_2979();
			UpSamp_2980();
			WEIGHTED_ROUND_ROBIN_Splitter_3134();
				Delay_N_3136();
				Delay_N_3137();
				Delay_N_3138();
				Delay_N_3139();
			WEIGHTED_ROUND_ROBIN_Joiner_3135();
			DUPLICATE_Splitter_3140();
				FirFilter_3142();
				FirFilter_3143();
				FirFilter_3144();
				FirFilter_3145();
			WEIGHTED_ROUND_ROBIN_Joiner_3141();
			WEIGHTED_ROUND_ROBIN_Splitter_3146();
				Delay_N_3148();
				Delay_N_3149();
				Delay_N_3150();
				Delay_N_3151();
			WEIGHTED_ROUND_ROBIN_Joiner_3147();
			DUPLICATE_Splitter_3152();
				FirFilter_3154();
				FirFilter_3155();
				FirFilter_3156();
				FirFilter_3157();
			WEIGHTED_ROUND_ROBIN_Joiner_3153();
			DownSamp_2986();
			UpSamp_2987();
			WEIGHTED_ROUND_ROBIN_Splitter_3158();
				Delay_N_3160();
				Delay_N_3161();
				Delay_N_3162();
				Delay_N_3163();
			WEIGHTED_ROUND_ROBIN_Joiner_3159();
			DUPLICATE_Splitter_3164();
				FirFilter_3166();
				FirFilter_3167();
				FirFilter_3168();
				FirFilter_3169();
			WEIGHTED_ROUND_ROBIN_Joiner_3165();
			WEIGHTED_ROUND_ROBIN_Splitter_3170();
				Delay_N_3172();
				Delay_N_3173();
				Delay_N_3174();
				Delay_N_3175();
			WEIGHTED_ROUND_ROBIN_Joiner_3171();
			DUPLICATE_Splitter_3176();
				FirFilter_3178();
				FirFilter_3179();
				FirFilter_3180();
				FirFilter_3181();
			WEIGHTED_ROUND_ROBIN_Joiner_3177();
			DownSamp_2993();
			UpSamp_2994();
			WEIGHTED_ROUND_ROBIN_Splitter_3182();
				Delay_N_3184();
				Delay_N_3185();
				Delay_N_3186();
				Delay_N_3187();
			WEIGHTED_ROUND_ROBIN_Joiner_3183();
			DUPLICATE_Splitter_3188();
				FirFilter_3190();
				FirFilter_3191();
				FirFilter_3192();
				FirFilter_3193();
			WEIGHTED_ROUND_ROBIN_Joiner_3189();
			WEIGHTED_ROUND_ROBIN_Splitter_3194();
				Delay_N_3196();
				Delay_N_3197();
				Delay_N_3198();
				Delay_N_3199();
			WEIGHTED_ROUND_ROBIN_Joiner_3195();
			DUPLICATE_Splitter_3200();
				FirFilter_3202();
				FirFilter_3203();
				FirFilter_3204();
				FirFilter_3205();
			WEIGHTED_ROUND_ROBIN_Joiner_3201();
			DownSamp_3000();
			UpSamp_3001();
			WEIGHTED_ROUND_ROBIN_Splitter_3206();
				Delay_N_3208();
				Delay_N_3209();
				Delay_N_3210();
				Delay_N_3211();
			WEIGHTED_ROUND_ROBIN_Joiner_3207();
			DUPLICATE_Splitter_3212();
				FirFilter_3214();
				FirFilter_3215();
				FirFilter_3216();
				FirFilter_3217();
			WEIGHTED_ROUND_ROBIN_Joiner_3213();
		WEIGHTED_ROUND_ROBIN_Joiner_3007();
		WEIGHTED_ROUND_ROBIN_Splitter_3218();
			Combine_3220();
			Combine_3221();
			Combine_3222();
			Combine_3223();
		WEIGHTED_ROUND_ROBIN_Joiner_3219();
		sink_3005();
	ENDFOR
	return EXIT_SUCCESS;
}
