#include "PEG2-FilterBankNew_nocache.h"

buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4033DUPLICATE_Splitter_4036;
buffer_float_t SplitJoin50_FirFilter_Fiss_4101_4135_join[2];
buffer_float_t SplitJoin61_Delay_N_Fiss_4104_4138_join[2];
buffer_float_t SplitJoin83_Delay_N_Fiss_4110_4144_split[2];
buffer_float_t SplitJoin6_Delay_N_Fiss_4087_4122_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3993DUPLICATE_Splitter_3996;
buffer_float_t SplitJoin72_FirFilter_Fiss_4107_4141_split[2];
buffer_float_t UpSamp_3920WEIGHTED_ROUND_ROBIN_Splitter_4056;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4005DownSamp_3898;
buffer_float_t DownSamp_3912UpSamp_3913;
buffer_float_t SplitJoin63_FirFilter_Fiss_4105_4139_split[2];
buffer_float_t SplitJoin102_FirFilter_Fiss_4117_4151_split[2];
buffer_float_t SplitJoin37_FirFilter_Fiss_4097_4131_split[2];
buffer_float_t SplitJoin4_FirFilter_Fiss_4086_4121_join[2];
buffer_float_t DownSamp_3905UpSamp_3906;
buffer_float_t SplitJoin2_Delay_N_Fiss_4085_4120_split[2];
buffer_float_t SplitJoin59_FirFilter_Fiss_4103_4137_split[2];
buffer_float_t SplitJoin87_Delay_N_Fiss_4112_4146_split[2];
buffer_float_t SplitJoin74_Delay_N_Fiss_4108_4142_join[2];
buffer_float_t SplitJoin83_Delay_N_Fiss_4110_4144_join[2];
buffer_float_t SplitJoin63_FirFilter_Fiss_4105_4139_join[2];
buffer_float_t SplitJoin98_FirFilter_Fiss_4115_4149_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3985DUPLICATE_Splitter_3988;
buffer_float_t SplitJoin76_FirFilter_Fiss_4109_4143_split[2];
buffer_float_t SplitJoin24_FirFilter_Fiss_4093_4127_split[2];
buffer_float_t SplitJoin31_Delay_N_Fiss_4094_4128_join[2];
buffer_float_t SplitJoin6_Delay_N_Fiss_4087_4122_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4049DUPLICATE_Splitter_4052;
buffer_float_t SplitJoin89_FirFilter_Fiss_4113_4147_join[2];
buffer_float_t SplitJoin18_Delay_N_Fiss_4090_4124_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3953DUPLICATE_Splitter_3956;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4025DUPLICATE_Splitter_4028;
buffer_float_t DownSamp_3891UpSamp_3892;
buffer_float_t SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4041DUPLICATE_Splitter_4044;
buffer_float_t SplitJoin76_FirFilter_Fiss_4109_4143_join[2];
buffer_float_t SplitJoin98_FirFilter_Fiss_4115_4149_join[2];
buffer_float_t DownSamp_3884UpSamp_3885;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3989DownSamp_3891;
buffer_float_t DownSamp_3877UpSamp_3878;
buffer_float_t SplitJoin10_Combine_Fiss_4089_4152_join[2];
buffer_float_t SplitJoin96_Delay_N_Fiss_4114_4148_split[2];
buffer_float_t SplitJoin70_Delay_N_Fiss_4106_4140_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4021DownSamp_3905;
buffer_float_t SplitJoin87_Delay_N_Fiss_4112_4146_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3961DUPLICATE_Splitter_3964;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4009DUPLICATE_Splitter_4012;
buffer_float_t SplitJoin70_Delay_N_Fiss_4106_4140_join[2];
buffer_float_t UpSamp_3913WEIGHTED_ROUND_ROBIN_Splitter_4040;
buffer_float_t SplitJoin33_FirFilter_Fiss_4095_4129_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4037DownSamp_3912;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4065DUPLICATE_Splitter_4068;
buffer_float_t SplitJoin44_Delay_N_Fiss_4098_4132_split[2];
buffer_float_t SplitJoin2_Delay_N_Fiss_4085_4120_join[2];
buffer_float_t SplitJoin46_FirFilter_Fiss_4099_4133_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4001DUPLICATE_Splitter_4004;
buffer_float_t SplitJoin10_Combine_Fiss_4089_4152_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4057DUPLICATE_Splitter_4060;
buffer_float_t SplitJoin46_FirFilter_Fiss_4099_4133_split[2];
buffer_float_t DownSamp_3898UpSamp_3899;
buffer_float_t SplitJoin48_Delay_N_Fiss_4100_4134_split[2];
buffer_float_t SplitJoin57_Delay_N_Fiss_4102_4136_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4069DownSamp_3926;
buffer_float_t UpSamp_3878WEIGHTED_ROUND_ROBIN_Splitter_3960;
buffer_float_t UpSamp_3927WEIGHTED_ROUND_ROBIN_Splitter_4072;
buffer_float_t SplitJoin35_Delay_N_Fiss_4096_4130_join[2];
buffer_float_t SplitJoin85_FirFilter_Fiss_4111_4145_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3933WEIGHTED_ROUND_ROBIN_Splitter_4080;
buffer_float_t SplitJoin22_Delay_N_Fiss_4092_4126_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3973DownSamp_3884;
buffer_float_t SplitJoin24_FirFilter_Fiss_4093_4127_join[2];
buffer_float_t SplitJoin100_Delay_N_Fiss_4116_4150_join[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4081sink_3931;
buffer_float_t SplitJoin4_FirFilter_Fiss_4086_4121_split[2];
buffer_float_t SplitJoin50_FirFilter_Fiss_4101_4135_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4053DownSamp_3919;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3969DUPLICATE_Splitter_3972;
buffer_float_t SplitJoin61_Delay_N_Fiss_4104_4138_split[2];
buffer_float_t SplitJoin44_Delay_N_Fiss_4098_4132_join[2];
buffer_float_t SplitJoin59_FirFilter_Fiss_4103_4137_join[2];
buffer_float_t SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[8];
buffer_float_t SplitJoin102_FirFilter_Fiss_4117_4151_join[2];
buffer_float_t SplitJoin33_FirFilter_Fiss_4095_4129_split[2];
buffer_float_t SplitJoin31_Delay_N_Fiss_4094_4128_split[2];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4073DUPLICATE_Splitter_4076;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3977DUPLICATE_Splitter_3980;
buffer_float_t UpSamp_3892WEIGHTED_ROUND_ROBIN_Splitter_3992;
buffer_float_t source_3872DUPLICATE_Splitter_3932;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3957DownSamp_3877;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_4017DUPLICATE_Splitter_4020;
buffer_float_t SplitJoin20_FirFilter_Fiss_4091_4125_join[2];
buffer_float_t DownSamp_3919UpSamp_3920;
buffer_float_t SplitJoin96_Delay_N_Fiss_4114_4148_join[2];
buffer_float_t SplitJoin100_Delay_N_Fiss_4116_4150_split[2];
buffer_float_t UpSamp_3885WEIGHTED_ROUND_ROBIN_Splitter_3976;
buffer_float_t SplitJoin22_Delay_N_Fiss_4092_4126_split[2];
buffer_float_t UpSamp_3899WEIGHTED_ROUND_ROBIN_Splitter_4008;
buffer_float_t SplitJoin74_Delay_N_Fiss_4108_4142_split[2];
buffer_float_t UpSamp_3906WEIGHTED_ROUND_ROBIN_Splitter_4024;
buffer_float_t SplitJoin20_FirFilter_Fiss_4091_4125_split[2];
buffer_float_t SplitJoin8_FirFilter_Fiss_4088_4123_split[2];
buffer_float_t SplitJoin57_Delay_N_Fiss_4102_4136_split[2];
buffer_float_t SplitJoin48_Delay_N_Fiss_4100_4134_join[2];
buffer_float_t SplitJoin8_FirFilter_Fiss_4088_4123_join[2];
buffer_float_t SplitJoin85_FirFilter_Fiss_4111_4145_join[2];
buffer_float_t DownSamp_3926UpSamp_3927;
buffer_float_t SplitJoin72_FirFilter_Fiss_4107_4141_join[2];
buffer_float_t SplitJoin18_Delay_N_Fiss_4090_4124_split[2];
buffer_float_t SplitJoin35_Delay_N_Fiss_4096_4130_split[2];
buffer_float_t SplitJoin37_FirFilter_Fiss_4097_4131_join[2];
buffer_float_t SplitJoin89_FirFilter_Fiss_4113_4147_split[2];


source_3872_t source_3872_s;
FirFilter_3958_t FirFilter_3958_s;
FirFilter_3958_t FirFilter_3959_s;
FirFilter_3958_t FirFilter_3966_s;
FirFilter_3958_t FirFilter_3967_s;
FirFilter_3958_t FirFilter_3974_s;
FirFilter_3958_t FirFilter_3975_s;
FirFilter_3958_t FirFilter_3982_s;
FirFilter_3958_t FirFilter_3983_s;
FirFilter_3958_t FirFilter_3990_s;
FirFilter_3958_t FirFilter_3991_s;
FirFilter_3958_t FirFilter_3998_s;
FirFilter_3958_t FirFilter_3999_s;
FirFilter_3958_t FirFilter_4006_s;
FirFilter_3958_t FirFilter_4007_s;
FirFilter_3958_t FirFilter_4014_s;
FirFilter_3958_t FirFilter_4015_s;
FirFilter_3958_t FirFilter_4022_s;
FirFilter_3958_t FirFilter_4023_s;
FirFilter_3958_t FirFilter_4030_s;
FirFilter_3958_t FirFilter_4031_s;
FirFilter_3958_t FirFilter_4038_s;
FirFilter_3958_t FirFilter_4039_s;
FirFilter_3958_t FirFilter_4046_s;
FirFilter_3958_t FirFilter_4047_s;
FirFilter_3958_t FirFilter_4054_s;
FirFilter_3958_t FirFilter_4055_s;
FirFilter_3958_t FirFilter_4062_s;
FirFilter_3958_t FirFilter_4063_s;
FirFilter_3958_t FirFilter_4070_s;
FirFilter_3958_t FirFilter_4071_s;
FirFilter_3958_t FirFilter_4078_s;
FirFilter_3958_t FirFilter_4079_s;

void source_3872(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		push_float(&source_3872DUPLICATE_Splitter_3932, source_3872_s.current) ; 
		if((source_3872_s.current > 1000.0)) {
			source_3872_s.current = 0.0 ; 
		}
		else {
			source_3872_s.current = (source_3872_s.current + 1.0) ; 
		}
	}
	ENDFOR
}

void Delay_N_3954(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin2_Delay_N_Fiss_4085_4120_join[0], pop_float(&SplitJoin2_Delay_N_Fiss_4085_4120_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_3955(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin2_Delay_N_Fiss_4085_4120_join[1], pop_float(&SplitJoin2_Delay_N_Fiss_4085_4120_split[1])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3952() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&SplitJoin2_Delay_N_Fiss_4085_4120_split[0], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[0]));
		push_float(&SplitJoin2_Delay_N_Fiss_4085_4120_split[1], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[0]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3953() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3953DUPLICATE_Splitter_3956, pop_float(&SplitJoin2_Delay_N_Fiss_4085_4120_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3953DUPLICATE_Splitter_3956, pop_float(&SplitJoin2_Delay_N_Fiss_4085_4120_join[1]));
	ENDFOR
}}

void FirFilter_3958(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin4_FirFilter_Fiss_4086_4121_split[0], i) * FirFilter_3958_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin4_FirFilter_Fiss_4086_4121_split[0]) ; 
		push_float(&SplitJoin4_FirFilter_Fiss_4086_4121_join[0], sum) ; 
 {
		pop_void(&SplitJoin4_FirFilter_Fiss_4086_4121_split[0]) ; 
	}
	}
	ENDFOR
}

void FirFilter_3959(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin4_FirFilter_Fiss_4086_4121_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin4_FirFilter_Fiss_4086_4121_split[1], i) * FirFilter_3959_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin4_FirFilter_Fiss_4086_4121_split[1]) ; 
		push_float(&SplitJoin4_FirFilter_Fiss_4086_4121_join[1], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_3956() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3953DUPLICATE_Splitter_3956);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin4_FirFilter_Fiss_4086_4121_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3957() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3957DownSamp_3877, pop_float(&SplitJoin4_FirFilter_Fiss_4086_4121_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3957DownSamp_3877, pop_float(&SplitJoin4_FirFilter_Fiss_4086_4121_join[1]));
	ENDFOR
}}

void DownSamp_3877() {
	push_float(&DownSamp_3877UpSamp_3878, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3957DownSamp_3877)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3957DownSamp_3877) ; 
	}
	ENDFOR
}


void UpSamp_3878() {
	push_float(&UpSamp_3878WEIGHTED_ROUND_ROBIN_Splitter_3960, pop_float(&DownSamp_3877UpSamp_3878)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		push_float(&UpSamp_3878WEIGHTED_ROUND_ROBIN_Splitter_3960, 0.0) ; 
	}
	ENDFOR
}


void Delay_N_3962(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin6_Delay_N_Fiss_4087_4122_join[0], pop_float(&SplitJoin6_Delay_N_Fiss_4087_4122_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_3963(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin6_Delay_N_Fiss_4087_4122_join[1], pop_float(&SplitJoin6_Delay_N_Fiss_4087_4122_split[1])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3960() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&SplitJoin6_Delay_N_Fiss_4087_4122_split[0], pop_float(&UpSamp_3878WEIGHTED_ROUND_ROBIN_Splitter_3960));
		push_float(&SplitJoin6_Delay_N_Fiss_4087_4122_split[1], pop_float(&UpSamp_3878WEIGHTED_ROUND_ROBIN_Splitter_3960));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3961() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3961DUPLICATE_Splitter_3964, pop_float(&SplitJoin6_Delay_N_Fiss_4087_4122_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3961DUPLICATE_Splitter_3964, pop_float(&SplitJoin6_Delay_N_Fiss_4087_4122_join[1]));
	ENDFOR
}}

void FirFilter_3966(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin8_FirFilter_Fiss_4088_4123_split[0], i) * FirFilter_3966_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin8_FirFilter_Fiss_4088_4123_split[0]) ; 
		push_float(&SplitJoin8_FirFilter_Fiss_4088_4123_join[0], sum) ; 
 {
		pop_void(&SplitJoin8_FirFilter_Fiss_4088_4123_split[0]) ; 
	}
	}
	ENDFOR
}

void FirFilter_3967(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		pop_void(&SplitJoin8_FirFilter_Fiss_4088_4123_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin8_FirFilter_Fiss_4088_4123_split[1], i) * FirFilter_3967_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin8_FirFilter_Fiss_4088_4123_split[1]) ; 
		push_float(&SplitJoin8_FirFilter_Fiss_4088_4123_join[1], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_3964() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3961DUPLICATE_Splitter_3964);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin8_FirFilter_Fiss_4088_4123_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3965() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[0], pop_float(&SplitJoin8_FirFilter_Fiss_4088_4123_join[0]));
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[0], pop_float(&SplitJoin8_FirFilter_Fiss_4088_4123_join[1]));
	ENDFOR
}}

void Delay_N_3970(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin18_Delay_N_Fiss_4090_4124_join[0], pop_float(&SplitJoin18_Delay_N_Fiss_4090_4124_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_3971(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin18_Delay_N_Fiss_4090_4124_join[1], pop_float(&SplitJoin18_Delay_N_Fiss_4090_4124_split[1])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3968() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&SplitJoin18_Delay_N_Fiss_4090_4124_split[0], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[1]));
		push_float(&SplitJoin18_Delay_N_Fiss_4090_4124_split[1], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[1]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3969() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3969DUPLICATE_Splitter_3972, pop_float(&SplitJoin18_Delay_N_Fiss_4090_4124_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3969DUPLICATE_Splitter_3972, pop_float(&SplitJoin18_Delay_N_Fiss_4090_4124_join[1]));
	ENDFOR
}}

void FirFilter_3974(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin20_FirFilter_Fiss_4091_4125_split[0], i) * FirFilter_3974_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin20_FirFilter_Fiss_4091_4125_split[0]) ; 
		push_float(&SplitJoin20_FirFilter_Fiss_4091_4125_join[0], sum) ; 
 {
		pop_void(&SplitJoin20_FirFilter_Fiss_4091_4125_split[0]) ; 
	}
	}
	ENDFOR
}

void FirFilter_3975(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin20_FirFilter_Fiss_4091_4125_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin20_FirFilter_Fiss_4091_4125_split[1], i) * FirFilter_3975_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin20_FirFilter_Fiss_4091_4125_split[1]) ; 
		push_float(&SplitJoin20_FirFilter_Fiss_4091_4125_join[1], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_3972() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3969DUPLICATE_Splitter_3972);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin20_FirFilter_Fiss_4091_4125_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3973() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3973DownSamp_3884, pop_float(&SplitJoin20_FirFilter_Fiss_4091_4125_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3973DownSamp_3884, pop_float(&SplitJoin20_FirFilter_Fiss_4091_4125_join[1]));
	ENDFOR
}}

void DownSamp_3884() {
	push_float(&DownSamp_3884UpSamp_3885, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3973DownSamp_3884)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3973DownSamp_3884) ; 
	}
	ENDFOR
}


void UpSamp_3885() {
	push_float(&UpSamp_3885WEIGHTED_ROUND_ROBIN_Splitter_3976, pop_float(&DownSamp_3884UpSamp_3885)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		push_float(&UpSamp_3885WEIGHTED_ROUND_ROBIN_Splitter_3976, 0.0) ; 
	}
	ENDFOR
}


void Delay_N_3978(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin22_Delay_N_Fiss_4092_4126_join[0], pop_float(&SplitJoin22_Delay_N_Fiss_4092_4126_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_3979(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin22_Delay_N_Fiss_4092_4126_join[1], pop_float(&SplitJoin22_Delay_N_Fiss_4092_4126_split[1])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3976() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&SplitJoin22_Delay_N_Fiss_4092_4126_split[0], pop_float(&UpSamp_3885WEIGHTED_ROUND_ROBIN_Splitter_3976));
		push_float(&SplitJoin22_Delay_N_Fiss_4092_4126_split[1], pop_float(&UpSamp_3885WEIGHTED_ROUND_ROBIN_Splitter_3976));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3977() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3977DUPLICATE_Splitter_3980, pop_float(&SplitJoin22_Delay_N_Fiss_4092_4126_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3977DUPLICATE_Splitter_3980, pop_float(&SplitJoin22_Delay_N_Fiss_4092_4126_join[1]));
	ENDFOR
}}

void FirFilter_3982(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin24_FirFilter_Fiss_4093_4127_split[0], i) * FirFilter_3982_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin24_FirFilter_Fiss_4093_4127_split[0]) ; 
		push_float(&SplitJoin24_FirFilter_Fiss_4093_4127_join[0], sum) ; 
 {
		pop_void(&SplitJoin24_FirFilter_Fiss_4093_4127_split[0]) ; 
	}
	}
	ENDFOR
}

void FirFilter_3983(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		pop_void(&SplitJoin24_FirFilter_Fiss_4093_4127_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin24_FirFilter_Fiss_4093_4127_split[1], i) * FirFilter_3983_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin24_FirFilter_Fiss_4093_4127_split[1]) ; 
		push_float(&SplitJoin24_FirFilter_Fiss_4093_4127_join[1], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_3980() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3977DUPLICATE_Splitter_3980);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin24_FirFilter_Fiss_4093_4127_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3981() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[1], pop_float(&SplitJoin24_FirFilter_Fiss_4093_4127_join[0]));
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[1], pop_float(&SplitJoin24_FirFilter_Fiss_4093_4127_join[1]));
	ENDFOR
}}

void Delay_N_3986(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin31_Delay_N_Fiss_4094_4128_join[0], pop_float(&SplitJoin31_Delay_N_Fiss_4094_4128_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_3987(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin31_Delay_N_Fiss_4094_4128_join[1], pop_float(&SplitJoin31_Delay_N_Fiss_4094_4128_split[1])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3984() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&SplitJoin31_Delay_N_Fiss_4094_4128_split[0], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[2]));
		push_float(&SplitJoin31_Delay_N_Fiss_4094_4128_split[1], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[2]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3985() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3985DUPLICATE_Splitter_3988, pop_float(&SplitJoin31_Delay_N_Fiss_4094_4128_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3985DUPLICATE_Splitter_3988, pop_float(&SplitJoin31_Delay_N_Fiss_4094_4128_join[1]));
	ENDFOR
}}

void FirFilter_3990(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin33_FirFilter_Fiss_4095_4129_split[0], i) * FirFilter_3990_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin33_FirFilter_Fiss_4095_4129_split[0]) ; 
		push_float(&SplitJoin33_FirFilter_Fiss_4095_4129_join[0], sum) ; 
 {
		pop_void(&SplitJoin33_FirFilter_Fiss_4095_4129_split[0]) ; 
	}
	}
	ENDFOR
}

void FirFilter_3991(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin33_FirFilter_Fiss_4095_4129_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin33_FirFilter_Fiss_4095_4129_split[1], i) * FirFilter_3991_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin33_FirFilter_Fiss_4095_4129_split[1]) ; 
		push_float(&SplitJoin33_FirFilter_Fiss_4095_4129_join[1], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_3988() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3985DUPLICATE_Splitter_3988);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin33_FirFilter_Fiss_4095_4129_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3989() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3989DownSamp_3891, pop_float(&SplitJoin33_FirFilter_Fiss_4095_4129_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3989DownSamp_3891, pop_float(&SplitJoin33_FirFilter_Fiss_4095_4129_join[1]));
	ENDFOR
}}

void DownSamp_3891() {
	push_float(&DownSamp_3891UpSamp_3892, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3989DownSamp_3891)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3989DownSamp_3891) ; 
	}
	ENDFOR
}


void UpSamp_3892() {
	push_float(&UpSamp_3892WEIGHTED_ROUND_ROBIN_Splitter_3992, pop_float(&DownSamp_3891UpSamp_3892)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		push_float(&UpSamp_3892WEIGHTED_ROUND_ROBIN_Splitter_3992, 0.0) ; 
	}
	ENDFOR
}


void Delay_N_3994(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin35_Delay_N_Fiss_4096_4130_join[0], pop_float(&SplitJoin35_Delay_N_Fiss_4096_4130_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_3995(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin35_Delay_N_Fiss_4096_4130_join[1], pop_float(&SplitJoin35_Delay_N_Fiss_4096_4130_split[1])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3992() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&SplitJoin35_Delay_N_Fiss_4096_4130_split[0], pop_float(&UpSamp_3892WEIGHTED_ROUND_ROBIN_Splitter_3992));
		push_float(&SplitJoin35_Delay_N_Fiss_4096_4130_split[1], pop_float(&UpSamp_3892WEIGHTED_ROUND_ROBIN_Splitter_3992));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3993() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3993DUPLICATE_Splitter_3996, pop_float(&SplitJoin35_Delay_N_Fiss_4096_4130_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3993DUPLICATE_Splitter_3996, pop_float(&SplitJoin35_Delay_N_Fiss_4096_4130_join[1]));
	ENDFOR
}}

void FirFilter_3998(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin37_FirFilter_Fiss_4097_4131_split[0], i) * FirFilter_3998_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin37_FirFilter_Fiss_4097_4131_split[0]) ; 
		push_float(&SplitJoin37_FirFilter_Fiss_4097_4131_join[0], sum) ; 
 {
		pop_void(&SplitJoin37_FirFilter_Fiss_4097_4131_split[0]) ; 
	}
	}
	ENDFOR
}

void FirFilter_3999(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		pop_void(&SplitJoin37_FirFilter_Fiss_4097_4131_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin37_FirFilter_Fiss_4097_4131_split[1], i) * FirFilter_3999_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin37_FirFilter_Fiss_4097_4131_split[1]) ; 
		push_float(&SplitJoin37_FirFilter_Fiss_4097_4131_join[1], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_3996() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3993DUPLICATE_Splitter_3996);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin37_FirFilter_Fiss_4097_4131_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3997() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[2], pop_float(&SplitJoin37_FirFilter_Fiss_4097_4131_join[0]));
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[2], pop_float(&SplitJoin37_FirFilter_Fiss_4097_4131_join[1]));
	ENDFOR
}}

void Delay_N_4002(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin44_Delay_N_Fiss_4098_4132_join[0], pop_float(&SplitJoin44_Delay_N_Fiss_4098_4132_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_4003(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin44_Delay_N_Fiss_4098_4132_join[1], pop_float(&SplitJoin44_Delay_N_Fiss_4098_4132_split[1])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4000() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&SplitJoin44_Delay_N_Fiss_4098_4132_split[0], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[3]));
		push_float(&SplitJoin44_Delay_N_Fiss_4098_4132_split[1], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[3]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4001() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4001DUPLICATE_Splitter_4004, pop_float(&SplitJoin44_Delay_N_Fiss_4098_4132_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4001DUPLICATE_Splitter_4004, pop_float(&SplitJoin44_Delay_N_Fiss_4098_4132_join[1]));
	ENDFOR
}}

void FirFilter_4006(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin46_FirFilter_Fiss_4099_4133_split[0], i) * FirFilter_4006_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin46_FirFilter_Fiss_4099_4133_split[0]) ; 
		push_float(&SplitJoin46_FirFilter_Fiss_4099_4133_join[0], sum) ; 
 {
		pop_void(&SplitJoin46_FirFilter_Fiss_4099_4133_split[0]) ; 
	}
	}
	ENDFOR
}

void FirFilter_4007(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin46_FirFilter_Fiss_4099_4133_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin46_FirFilter_Fiss_4099_4133_split[1], i) * FirFilter_4007_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin46_FirFilter_Fiss_4099_4133_split[1]) ; 
		push_float(&SplitJoin46_FirFilter_Fiss_4099_4133_join[1], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_4004() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4001DUPLICATE_Splitter_4004);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin46_FirFilter_Fiss_4099_4133_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4005() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4005DownSamp_3898, pop_float(&SplitJoin46_FirFilter_Fiss_4099_4133_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4005DownSamp_3898, pop_float(&SplitJoin46_FirFilter_Fiss_4099_4133_join[1]));
	ENDFOR
}}

void DownSamp_3898() {
	push_float(&DownSamp_3898UpSamp_3899, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4005DownSamp_3898)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4005DownSamp_3898) ; 
	}
	ENDFOR
}


void UpSamp_3899() {
	push_float(&UpSamp_3899WEIGHTED_ROUND_ROBIN_Splitter_4008, pop_float(&DownSamp_3898UpSamp_3899)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		push_float(&UpSamp_3899WEIGHTED_ROUND_ROBIN_Splitter_4008, 0.0) ; 
	}
	ENDFOR
}


void Delay_N_4010(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin48_Delay_N_Fiss_4100_4134_join[0], pop_float(&SplitJoin48_Delay_N_Fiss_4100_4134_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_4011(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin48_Delay_N_Fiss_4100_4134_join[1], pop_float(&SplitJoin48_Delay_N_Fiss_4100_4134_split[1])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4008() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&SplitJoin48_Delay_N_Fiss_4100_4134_split[0], pop_float(&UpSamp_3899WEIGHTED_ROUND_ROBIN_Splitter_4008));
		push_float(&SplitJoin48_Delay_N_Fiss_4100_4134_split[1], pop_float(&UpSamp_3899WEIGHTED_ROUND_ROBIN_Splitter_4008));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4009() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4009DUPLICATE_Splitter_4012, pop_float(&SplitJoin48_Delay_N_Fiss_4100_4134_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4009DUPLICATE_Splitter_4012, pop_float(&SplitJoin48_Delay_N_Fiss_4100_4134_join[1]));
	ENDFOR
}}

void FirFilter_4014(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin50_FirFilter_Fiss_4101_4135_split[0], i) * FirFilter_4014_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin50_FirFilter_Fiss_4101_4135_split[0]) ; 
		push_float(&SplitJoin50_FirFilter_Fiss_4101_4135_join[0], sum) ; 
 {
		pop_void(&SplitJoin50_FirFilter_Fiss_4101_4135_split[0]) ; 
	}
	}
	ENDFOR
}

void FirFilter_4015(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		pop_void(&SplitJoin50_FirFilter_Fiss_4101_4135_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin50_FirFilter_Fiss_4101_4135_split[1], i) * FirFilter_4015_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin50_FirFilter_Fiss_4101_4135_split[1]) ; 
		push_float(&SplitJoin50_FirFilter_Fiss_4101_4135_join[1], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_4012() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4009DUPLICATE_Splitter_4012);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin50_FirFilter_Fiss_4101_4135_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4013() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[3], pop_float(&SplitJoin50_FirFilter_Fiss_4101_4135_join[0]));
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[3], pop_float(&SplitJoin50_FirFilter_Fiss_4101_4135_join[1]));
	ENDFOR
}}

void Delay_N_4018(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin57_Delay_N_Fiss_4102_4136_join[0], pop_float(&SplitJoin57_Delay_N_Fiss_4102_4136_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_4019(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin57_Delay_N_Fiss_4102_4136_join[1], pop_float(&SplitJoin57_Delay_N_Fiss_4102_4136_split[1])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4016() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&SplitJoin57_Delay_N_Fiss_4102_4136_split[0], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[4]));
		push_float(&SplitJoin57_Delay_N_Fiss_4102_4136_split[1], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[4]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4017() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4017DUPLICATE_Splitter_4020, pop_float(&SplitJoin57_Delay_N_Fiss_4102_4136_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4017DUPLICATE_Splitter_4020, pop_float(&SplitJoin57_Delay_N_Fiss_4102_4136_join[1]));
	ENDFOR
}}

void FirFilter_4022(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin59_FirFilter_Fiss_4103_4137_split[0], i) * FirFilter_4022_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin59_FirFilter_Fiss_4103_4137_split[0]) ; 
		push_float(&SplitJoin59_FirFilter_Fiss_4103_4137_join[0], sum) ; 
 {
		pop_void(&SplitJoin59_FirFilter_Fiss_4103_4137_split[0]) ; 
	}
	}
	ENDFOR
}

void FirFilter_4023(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin59_FirFilter_Fiss_4103_4137_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin59_FirFilter_Fiss_4103_4137_split[1], i) * FirFilter_4023_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin59_FirFilter_Fiss_4103_4137_split[1]) ; 
		push_float(&SplitJoin59_FirFilter_Fiss_4103_4137_join[1], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_4020() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4017DUPLICATE_Splitter_4020);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin59_FirFilter_Fiss_4103_4137_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4021() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4021DownSamp_3905, pop_float(&SplitJoin59_FirFilter_Fiss_4103_4137_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4021DownSamp_3905, pop_float(&SplitJoin59_FirFilter_Fiss_4103_4137_join[1]));
	ENDFOR
}}

void DownSamp_3905() {
	push_float(&DownSamp_3905UpSamp_3906, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4021DownSamp_3905)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4021DownSamp_3905) ; 
	}
	ENDFOR
}


void UpSamp_3906() {
	push_float(&UpSamp_3906WEIGHTED_ROUND_ROBIN_Splitter_4024, pop_float(&DownSamp_3905UpSamp_3906)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		push_float(&UpSamp_3906WEIGHTED_ROUND_ROBIN_Splitter_4024, 0.0) ; 
	}
	ENDFOR
}


void Delay_N_4026(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin61_Delay_N_Fiss_4104_4138_join[0], pop_float(&SplitJoin61_Delay_N_Fiss_4104_4138_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_4027(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin61_Delay_N_Fiss_4104_4138_join[1], pop_float(&SplitJoin61_Delay_N_Fiss_4104_4138_split[1])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4024() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&SplitJoin61_Delay_N_Fiss_4104_4138_split[0], pop_float(&UpSamp_3906WEIGHTED_ROUND_ROBIN_Splitter_4024));
		push_float(&SplitJoin61_Delay_N_Fiss_4104_4138_split[1], pop_float(&UpSamp_3906WEIGHTED_ROUND_ROBIN_Splitter_4024));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4025() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4025DUPLICATE_Splitter_4028, pop_float(&SplitJoin61_Delay_N_Fiss_4104_4138_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4025DUPLICATE_Splitter_4028, pop_float(&SplitJoin61_Delay_N_Fiss_4104_4138_join[1]));
	ENDFOR
}}

void FirFilter_4030(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin63_FirFilter_Fiss_4105_4139_split[0], i) * FirFilter_4030_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin63_FirFilter_Fiss_4105_4139_split[0]) ; 
		push_float(&SplitJoin63_FirFilter_Fiss_4105_4139_join[0], sum) ; 
 {
		pop_void(&SplitJoin63_FirFilter_Fiss_4105_4139_split[0]) ; 
	}
	}
	ENDFOR
}

void FirFilter_4031(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		pop_void(&SplitJoin63_FirFilter_Fiss_4105_4139_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin63_FirFilter_Fiss_4105_4139_split[1], i) * FirFilter_4031_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin63_FirFilter_Fiss_4105_4139_split[1]) ; 
		push_float(&SplitJoin63_FirFilter_Fiss_4105_4139_join[1], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_4028() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4025DUPLICATE_Splitter_4028);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin63_FirFilter_Fiss_4105_4139_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4029() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[4], pop_float(&SplitJoin63_FirFilter_Fiss_4105_4139_join[0]));
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[4], pop_float(&SplitJoin63_FirFilter_Fiss_4105_4139_join[1]));
	ENDFOR
}}

void Delay_N_4034(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin70_Delay_N_Fiss_4106_4140_join[0], pop_float(&SplitJoin70_Delay_N_Fiss_4106_4140_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_4035(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin70_Delay_N_Fiss_4106_4140_join[1], pop_float(&SplitJoin70_Delay_N_Fiss_4106_4140_split[1])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4032() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&SplitJoin70_Delay_N_Fiss_4106_4140_split[0], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[5]));
		push_float(&SplitJoin70_Delay_N_Fiss_4106_4140_split[1], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[5]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4033() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4033DUPLICATE_Splitter_4036, pop_float(&SplitJoin70_Delay_N_Fiss_4106_4140_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4033DUPLICATE_Splitter_4036, pop_float(&SplitJoin70_Delay_N_Fiss_4106_4140_join[1]));
	ENDFOR
}}

void FirFilter_4038(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin72_FirFilter_Fiss_4107_4141_split[0], i) * FirFilter_4038_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin72_FirFilter_Fiss_4107_4141_split[0]) ; 
		push_float(&SplitJoin72_FirFilter_Fiss_4107_4141_join[0], sum) ; 
 {
		pop_void(&SplitJoin72_FirFilter_Fiss_4107_4141_split[0]) ; 
	}
	}
	ENDFOR
}

void FirFilter_4039(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin72_FirFilter_Fiss_4107_4141_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin72_FirFilter_Fiss_4107_4141_split[1], i) * FirFilter_4039_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin72_FirFilter_Fiss_4107_4141_split[1]) ; 
		push_float(&SplitJoin72_FirFilter_Fiss_4107_4141_join[1], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_4036() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4033DUPLICATE_Splitter_4036);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin72_FirFilter_Fiss_4107_4141_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4037() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4037DownSamp_3912, pop_float(&SplitJoin72_FirFilter_Fiss_4107_4141_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4037DownSamp_3912, pop_float(&SplitJoin72_FirFilter_Fiss_4107_4141_join[1]));
	ENDFOR
}}

void DownSamp_3912() {
	push_float(&DownSamp_3912UpSamp_3913, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4037DownSamp_3912)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4037DownSamp_3912) ; 
	}
	ENDFOR
}


void UpSamp_3913() {
	push_float(&UpSamp_3913WEIGHTED_ROUND_ROBIN_Splitter_4040, pop_float(&DownSamp_3912UpSamp_3913)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		push_float(&UpSamp_3913WEIGHTED_ROUND_ROBIN_Splitter_4040, 0.0) ; 
	}
	ENDFOR
}


void Delay_N_4042(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin74_Delay_N_Fiss_4108_4142_join[0], pop_float(&SplitJoin74_Delay_N_Fiss_4108_4142_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_4043(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin74_Delay_N_Fiss_4108_4142_join[1], pop_float(&SplitJoin74_Delay_N_Fiss_4108_4142_split[1])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4040() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&SplitJoin74_Delay_N_Fiss_4108_4142_split[0], pop_float(&UpSamp_3913WEIGHTED_ROUND_ROBIN_Splitter_4040));
		push_float(&SplitJoin74_Delay_N_Fiss_4108_4142_split[1], pop_float(&UpSamp_3913WEIGHTED_ROUND_ROBIN_Splitter_4040));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4041() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4041DUPLICATE_Splitter_4044, pop_float(&SplitJoin74_Delay_N_Fiss_4108_4142_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4041DUPLICATE_Splitter_4044, pop_float(&SplitJoin74_Delay_N_Fiss_4108_4142_join[1]));
	ENDFOR
}}

void FirFilter_4046(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin76_FirFilter_Fiss_4109_4143_split[0], i) * FirFilter_4046_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin76_FirFilter_Fiss_4109_4143_split[0]) ; 
		push_float(&SplitJoin76_FirFilter_Fiss_4109_4143_join[0], sum) ; 
 {
		pop_void(&SplitJoin76_FirFilter_Fiss_4109_4143_split[0]) ; 
	}
	}
	ENDFOR
}

void FirFilter_4047(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		pop_void(&SplitJoin76_FirFilter_Fiss_4109_4143_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin76_FirFilter_Fiss_4109_4143_split[1], i) * FirFilter_4047_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin76_FirFilter_Fiss_4109_4143_split[1]) ; 
		push_float(&SplitJoin76_FirFilter_Fiss_4109_4143_join[1], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_4044() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4041DUPLICATE_Splitter_4044);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin76_FirFilter_Fiss_4109_4143_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4045() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[5], pop_float(&SplitJoin76_FirFilter_Fiss_4109_4143_join[0]));
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[5], pop_float(&SplitJoin76_FirFilter_Fiss_4109_4143_join[1]));
	ENDFOR
}}

void Delay_N_4050(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin83_Delay_N_Fiss_4110_4144_join[0], pop_float(&SplitJoin83_Delay_N_Fiss_4110_4144_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_4051(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin83_Delay_N_Fiss_4110_4144_join[1], pop_float(&SplitJoin83_Delay_N_Fiss_4110_4144_split[1])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4048() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&SplitJoin83_Delay_N_Fiss_4110_4144_split[0], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[6]));
		push_float(&SplitJoin83_Delay_N_Fiss_4110_4144_split[1], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[6]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4049() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4049DUPLICATE_Splitter_4052, pop_float(&SplitJoin83_Delay_N_Fiss_4110_4144_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4049DUPLICATE_Splitter_4052, pop_float(&SplitJoin83_Delay_N_Fiss_4110_4144_join[1]));
	ENDFOR
}}

void FirFilter_4054(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin85_FirFilter_Fiss_4111_4145_split[0], i) * FirFilter_4054_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin85_FirFilter_Fiss_4111_4145_split[0]) ; 
		push_float(&SplitJoin85_FirFilter_Fiss_4111_4145_join[0], sum) ; 
 {
		pop_void(&SplitJoin85_FirFilter_Fiss_4111_4145_split[0]) ; 
	}
	}
	ENDFOR
}

void FirFilter_4055(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin85_FirFilter_Fiss_4111_4145_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin85_FirFilter_Fiss_4111_4145_split[1], i) * FirFilter_4055_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin85_FirFilter_Fiss_4111_4145_split[1]) ; 
		push_float(&SplitJoin85_FirFilter_Fiss_4111_4145_join[1], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_4052() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4049DUPLICATE_Splitter_4052);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin85_FirFilter_Fiss_4111_4145_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4053() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4053DownSamp_3919, pop_float(&SplitJoin85_FirFilter_Fiss_4111_4145_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4053DownSamp_3919, pop_float(&SplitJoin85_FirFilter_Fiss_4111_4145_join[1]));
	ENDFOR
}}

void DownSamp_3919() {
	push_float(&DownSamp_3919UpSamp_3920, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4053DownSamp_3919)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4053DownSamp_3919) ; 
	}
	ENDFOR
}


void UpSamp_3920() {
	push_float(&UpSamp_3920WEIGHTED_ROUND_ROBIN_Splitter_4056, pop_float(&DownSamp_3919UpSamp_3920)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		push_float(&UpSamp_3920WEIGHTED_ROUND_ROBIN_Splitter_4056, 0.0) ; 
	}
	ENDFOR
}


void Delay_N_4058(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin87_Delay_N_Fiss_4112_4146_join[0], pop_float(&SplitJoin87_Delay_N_Fiss_4112_4146_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_4059(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin87_Delay_N_Fiss_4112_4146_join[1], pop_float(&SplitJoin87_Delay_N_Fiss_4112_4146_split[1])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4056() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&SplitJoin87_Delay_N_Fiss_4112_4146_split[0], pop_float(&UpSamp_3920WEIGHTED_ROUND_ROBIN_Splitter_4056));
		push_float(&SplitJoin87_Delay_N_Fiss_4112_4146_split[1], pop_float(&UpSamp_3920WEIGHTED_ROUND_ROBIN_Splitter_4056));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4057() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4057DUPLICATE_Splitter_4060, pop_float(&SplitJoin87_Delay_N_Fiss_4112_4146_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4057DUPLICATE_Splitter_4060, pop_float(&SplitJoin87_Delay_N_Fiss_4112_4146_join[1]));
	ENDFOR
}}

void FirFilter_4062(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin89_FirFilter_Fiss_4113_4147_split[0], i) * FirFilter_4062_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin89_FirFilter_Fiss_4113_4147_split[0]) ; 
		push_float(&SplitJoin89_FirFilter_Fiss_4113_4147_join[0], sum) ; 
 {
		pop_void(&SplitJoin89_FirFilter_Fiss_4113_4147_split[0]) ; 
	}
	}
	ENDFOR
}

void FirFilter_4063(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		pop_void(&SplitJoin89_FirFilter_Fiss_4113_4147_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin89_FirFilter_Fiss_4113_4147_split[1], i) * FirFilter_4063_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin89_FirFilter_Fiss_4113_4147_split[1]) ; 
		push_float(&SplitJoin89_FirFilter_Fiss_4113_4147_join[1], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_4060() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4057DUPLICATE_Splitter_4060);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin89_FirFilter_Fiss_4113_4147_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4061() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[6], pop_float(&SplitJoin89_FirFilter_Fiss_4113_4147_join[0]));
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[6], pop_float(&SplitJoin89_FirFilter_Fiss_4113_4147_join[1]));
	ENDFOR
}}

void Delay_N_4066(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin96_Delay_N_Fiss_4114_4148_join[0], pop_float(&SplitJoin96_Delay_N_Fiss_4114_4148_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_4067(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin96_Delay_N_Fiss_4114_4148_join[1], pop_float(&SplitJoin96_Delay_N_Fiss_4114_4148_split[1])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4064() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&SplitJoin96_Delay_N_Fiss_4114_4148_split[0], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[7]));
		push_float(&SplitJoin96_Delay_N_Fiss_4114_4148_split[1], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[7]));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4065() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4065DUPLICATE_Splitter_4068, pop_float(&SplitJoin96_Delay_N_Fiss_4114_4148_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4065DUPLICATE_Splitter_4068, pop_float(&SplitJoin96_Delay_N_Fiss_4114_4148_join[1]));
	ENDFOR
}}

void FirFilter_4070(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin98_FirFilter_Fiss_4115_4149_split[0], i) * FirFilter_4070_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin98_FirFilter_Fiss_4115_4149_split[0]) ; 
		push_float(&SplitJoin98_FirFilter_Fiss_4115_4149_join[0], sum) ; 
 {
		pop_void(&SplitJoin98_FirFilter_Fiss_4115_4149_split[0]) ; 
	}
	}
	ENDFOR
}

void FirFilter_4071(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin98_FirFilter_Fiss_4115_4149_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin98_FirFilter_Fiss_4115_4149_split[1], i) * FirFilter_4071_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin98_FirFilter_Fiss_4115_4149_split[1]) ; 
		push_float(&SplitJoin98_FirFilter_Fiss_4115_4149_join[1], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_4068() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4065DUPLICATE_Splitter_4068);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin98_FirFilter_Fiss_4115_4149_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4069() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4069DownSamp_3926, pop_float(&SplitJoin98_FirFilter_Fiss_4115_4149_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4069DownSamp_3926, pop_float(&SplitJoin98_FirFilter_Fiss_4115_4149_join[1]));
	ENDFOR
}}

void DownSamp_3926() {
	push_float(&DownSamp_3926UpSamp_3927, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4069DownSamp_3926)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4069DownSamp_3926) ; 
	}
	ENDFOR
}


void UpSamp_3927() {
	push_float(&UpSamp_3927WEIGHTED_ROUND_ROBIN_Splitter_4072, pop_float(&DownSamp_3926UpSamp_3927)) ; 
	FOR(int, i, 0,  < , 7, i++) {
		push_float(&UpSamp_3927WEIGHTED_ROUND_ROBIN_Splitter_4072, 0.0) ; 
	}
	ENDFOR
}


void Delay_N_4074(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin100_Delay_N_Fiss_4116_4150_join[0], pop_float(&SplitJoin100_Delay_N_Fiss_4116_4150_split[0])) ; 
	}
	ENDFOR
}

void Delay_N_4075(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin100_Delay_N_Fiss_4116_4150_join[1], pop_float(&SplitJoin100_Delay_N_Fiss_4116_4150_split[1])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4072() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&SplitJoin100_Delay_N_Fiss_4116_4150_split[0], pop_float(&UpSamp_3927WEIGHTED_ROUND_ROBIN_Splitter_4072));
		push_float(&SplitJoin100_Delay_N_Fiss_4116_4150_split[1], pop_float(&UpSamp_3927WEIGHTED_ROUND_ROBIN_Splitter_4072));
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4073() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4073DUPLICATE_Splitter_4076, pop_float(&SplitJoin100_Delay_N_Fiss_4116_4150_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4073DUPLICATE_Splitter_4076, pop_float(&SplitJoin100_Delay_N_Fiss_4116_4150_join[1]));
	ENDFOR
}}

void FirFilter_4078(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin102_FirFilter_Fiss_4117_4151_split[0], i) * FirFilter_4078_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin102_FirFilter_Fiss_4117_4151_split[0]) ; 
		push_float(&SplitJoin102_FirFilter_Fiss_4117_4151_join[0], sum) ; 
 {
		pop_void(&SplitJoin102_FirFilter_Fiss_4117_4151_split[0]) ; 
	}
	}
	ENDFOR
}

void FirFilter_4079(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
		sum = 0.0 ; 
 {
		pop_void(&SplitJoin102_FirFilter_Fiss_4117_4151_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin102_FirFilter_Fiss_4117_4151_split[1], i) * FirFilter_4079_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin102_FirFilter_Fiss_4117_4151_split[1]) ; 
		push_float(&SplitJoin102_FirFilter_Fiss_4117_4151_join[1], sum) ; 
	}
	ENDFOR
}

void DUPLICATE_Splitter_4076() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4073DUPLICATE_Splitter_4076);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin102_FirFilter_Fiss_4117_4151_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4077() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[7], pop_float(&SplitJoin102_FirFilter_Fiss_4117_4151_join[0]));
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[7], pop_float(&SplitJoin102_FirFilter_Fiss_4117_4151_join[1]));
	ENDFOR
}}

void DUPLICATE_Splitter_3932() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		float __token_ = pop_float(&source_3872DUPLICATE_Splitter_3932);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3933() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3933WEIGHTED_ROUND_ROBIN_Splitter_4080, pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Combine_4082(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
		FOR(int, i, 0,  < , 8, i++) {
			sum = (sum + pop_float(&SplitJoin10_Combine_Fiss_4089_4152_split[0])) ; 
		}
		ENDFOR
		push_float(&SplitJoin10_Combine_Fiss_4089_4152_join[0], sum) ; 
	}
	ENDFOR
}

void Combine_4083(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		float sum = 0.0;
		FOR(int, i, 0,  < , 8, i++) {
			sum = (sum + pop_float(&SplitJoin10_Combine_Fiss_4089_4152_split[1])) ; 
		}
		ENDFOR
		push_float(&SplitJoin10_Combine_Fiss_4089_4152_join[1], sum) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_4080() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&SplitJoin10_Combine_Fiss_4089_4152_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3933WEIGHTED_ROUND_ROBIN_Splitter_4080));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&SplitJoin10_Combine_Fiss_4089_4152_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3933WEIGHTED_ROUND_ROBIN_Splitter_4080));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_4081() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4081sink_3931, pop_float(&SplitJoin10_Combine_Fiss_4089_4152_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4081sink_3931, pop_float(&SplitJoin10_Combine_Fiss_4089_4152_join[1]));
	ENDFOR
}}

void sink_3931(){
	FOR(uint32_t, __iter_steady_, 0, <, 8, __iter_steady_++) {
		printf("%.10f", pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4081sink_3931));
		printf("\n");
	}
	ENDFOR
}

void __stream_init__() {
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4033DUPLICATE_Splitter_4036);
	FOR(int, __iter_init_0_, 0, <, 2, __iter_init_0_++)
		init_buffer_float(&SplitJoin50_FirFilter_Fiss_4101_4135_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 2, __iter_init_1_++)
		init_buffer_float(&SplitJoin61_Delay_N_Fiss_4104_4138_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 2, __iter_init_2_++)
		init_buffer_float(&SplitJoin83_Delay_N_Fiss_4110_4144_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 2, __iter_init_3_++)
		init_buffer_float(&SplitJoin6_Delay_N_Fiss_4087_4122_split[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3993DUPLICATE_Splitter_3996);
	FOR(int, __iter_init_4_, 0, <, 2, __iter_init_4_++)
		init_buffer_float(&SplitJoin72_FirFilter_Fiss_4107_4141_split[__iter_init_4_]);
	ENDFOR
	init_buffer_float(&UpSamp_3920WEIGHTED_ROUND_ROBIN_Splitter_4056);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4005DownSamp_3898);
	init_buffer_float(&DownSamp_3912UpSamp_3913);
	FOR(int, __iter_init_5_, 0, <, 2, __iter_init_5_++)
		init_buffer_float(&SplitJoin63_FirFilter_Fiss_4105_4139_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 2, __iter_init_6_++)
		init_buffer_float(&SplitJoin102_FirFilter_Fiss_4117_4151_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 2, __iter_init_7_++)
		init_buffer_float(&SplitJoin37_FirFilter_Fiss_4097_4131_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 2, __iter_init_8_++)
		init_buffer_float(&SplitJoin4_FirFilter_Fiss_4086_4121_join[__iter_init_8_]);
	ENDFOR
	init_buffer_float(&DownSamp_3905UpSamp_3906);
	FOR(int, __iter_init_9_, 0, <, 2, __iter_init_9_++)
		init_buffer_float(&SplitJoin2_Delay_N_Fiss_4085_4120_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 2, __iter_init_10_++)
		init_buffer_float(&SplitJoin59_FirFilter_Fiss_4103_4137_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 2, __iter_init_11_++)
		init_buffer_float(&SplitJoin87_Delay_N_Fiss_4112_4146_split[__iter_init_11_]);
	ENDFOR
	FOR(int, __iter_init_12_, 0, <, 2, __iter_init_12_++)
		init_buffer_float(&SplitJoin74_Delay_N_Fiss_4108_4142_join[__iter_init_12_]);
	ENDFOR
	FOR(int, __iter_init_13_, 0, <, 2, __iter_init_13_++)
		init_buffer_float(&SplitJoin83_Delay_N_Fiss_4110_4144_join[__iter_init_13_]);
	ENDFOR
	FOR(int, __iter_init_14_, 0, <, 2, __iter_init_14_++)
		init_buffer_float(&SplitJoin63_FirFilter_Fiss_4105_4139_join[__iter_init_14_]);
	ENDFOR
	FOR(int, __iter_init_15_, 0, <, 2, __iter_init_15_++)
		init_buffer_float(&SplitJoin98_FirFilter_Fiss_4115_4149_split[__iter_init_15_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3985DUPLICATE_Splitter_3988);
	FOR(int, __iter_init_16_, 0, <, 2, __iter_init_16_++)
		init_buffer_float(&SplitJoin76_FirFilter_Fiss_4109_4143_split[__iter_init_16_]);
	ENDFOR
	FOR(int, __iter_init_17_, 0, <, 2, __iter_init_17_++)
		init_buffer_float(&SplitJoin24_FirFilter_Fiss_4093_4127_split[__iter_init_17_]);
	ENDFOR
	FOR(int, __iter_init_18_, 0, <, 2, __iter_init_18_++)
		init_buffer_float(&SplitJoin31_Delay_N_Fiss_4094_4128_join[__iter_init_18_]);
	ENDFOR
	FOR(int, __iter_init_19_, 0, <, 2, __iter_init_19_++)
		init_buffer_float(&SplitJoin6_Delay_N_Fiss_4087_4122_join[__iter_init_19_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4049DUPLICATE_Splitter_4052);
	FOR(int, __iter_init_20_, 0, <, 2, __iter_init_20_++)
		init_buffer_float(&SplitJoin89_FirFilter_Fiss_4113_4147_join[__iter_init_20_]);
	ENDFOR
	FOR(int, __iter_init_21_, 0, <, 2, __iter_init_21_++)
		init_buffer_float(&SplitJoin18_Delay_N_Fiss_4090_4124_join[__iter_init_21_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3953DUPLICATE_Splitter_3956);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4025DUPLICATE_Splitter_4028);
	init_buffer_float(&DownSamp_3891UpSamp_3892);
	FOR(int, __iter_init_22_, 0, <, 8, __iter_init_22_++)
		init_buffer_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[__iter_init_22_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4041DUPLICATE_Splitter_4044);
	FOR(int, __iter_init_23_, 0, <, 2, __iter_init_23_++)
		init_buffer_float(&SplitJoin76_FirFilter_Fiss_4109_4143_join[__iter_init_23_]);
	ENDFOR
	FOR(int, __iter_init_24_, 0, <, 2, __iter_init_24_++)
		init_buffer_float(&SplitJoin98_FirFilter_Fiss_4115_4149_join[__iter_init_24_]);
	ENDFOR
	init_buffer_float(&DownSamp_3884UpSamp_3885);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3989DownSamp_3891);
	init_buffer_float(&DownSamp_3877UpSamp_3878);
	FOR(int, __iter_init_25_, 0, <, 2, __iter_init_25_++)
		init_buffer_float(&SplitJoin10_Combine_Fiss_4089_4152_join[__iter_init_25_]);
	ENDFOR
	FOR(int, __iter_init_26_, 0, <, 2, __iter_init_26_++)
		init_buffer_float(&SplitJoin96_Delay_N_Fiss_4114_4148_split[__iter_init_26_]);
	ENDFOR
	FOR(int, __iter_init_27_, 0, <, 2, __iter_init_27_++)
		init_buffer_float(&SplitJoin70_Delay_N_Fiss_4106_4140_split[__iter_init_27_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4021DownSamp_3905);
	FOR(int, __iter_init_28_, 0, <, 2, __iter_init_28_++)
		init_buffer_float(&SplitJoin87_Delay_N_Fiss_4112_4146_join[__iter_init_28_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3961DUPLICATE_Splitter_3964);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4009DUPLICATE_Splitter_4012);
	FOR(int, __iter_init_29_, 0, <, 2, __iter_init_29_++)
		init_buffer_float(&SplitJoin70_Delay_N_Fiss_4106_4140_join[__iter_init_29_]);
	ENDFOR
	init_buffer_float(&UpSamp_3913WEIGHTED_ROUND_ROBIN_Splitter_4040);
	FOR(int, __iter_init_30_, 0, <, 2, __iter_init_30_++)
		init_buffer_float(&SplitJoin33_FirFilter_Fiss_4095_4129_join[__iter_init_30_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4037DownSamp_3912);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4065DUPLICATE_Splitter_4068);
	FOR(int, __iter_init_31_, 0, <, 2, __iter_init_31_++)
		init_buffer_float(&SplitJoin44_Delay_N_Fiss_4098_4132_split[__iter_init_31_]);
	ENDFOR
	FOR(int, __iter_init_32_, 0, <, 2, __iter_init_32_++)
		init_buffer_float(&SplitJoin2_Delay_N_Fiss_4085_4120_join[__iter_init_32_]);
	ENDFOR
	FOR(int, __iter_init_33_, 0, <, 2, __iter_init_33_++)
		init_buffer_float(&SplitJoin46_FirFilter_Fiss_4099_4133_join[__iter_init_33_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4001DUPLICATE_Splitter_4004);
	FOR(int, __iter_init_34_, 0, <, 2, __iter_init_34_++)
		init_buffer_float(&SplitJoin10_Combine_Fiss_4089_4152_split[__iter_init_34_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4057DUPLICATE_Splitter_4060);
	FOR(int, __iter_init_35_, 0, <, 2, __iter_init_35_++)
		init_buffer_float(&SplitJoin46_FirFilter_Fiss_4099_4133_split[__iter_init_35_]);
	ENDFOR
	init_buffer_float(&DownSamp_3898UpSamp_3899);
	FOR(int, __iter_init_36_, 0, <, 2, __iter_init_36_++)
		init_buffer_float(&SplitJoin48_Delay_N_Fiss_4100_4134_split[__iter_init_36_]);
	ENDFOR
	FOR(int, __iter_init_37_, 0, <, 2, __iter_init_37_++)
		init_buffer_float(&SplitJoin57_Delay_N_Fiss_4102_4136_join[__iter_init_37_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4069DownSamp_3926);
	init_buffer_float(&UpSamp_3878WEIGHTED_ROUND_ROBIN_Splitter_3960);
	init_buffer_float(&UpSamp_3927WEIGHTED_ROUND_ROBIN_Splitter_4072);
	FOR(int, __iter_init_38_, 0, <, 2, __iter_init_38_++)
		init_buffer_float(&SplitJoin35_Delay_N_Fiss_4096_4130_join[__iter_init_38_]);
	ENDFOR
	FOR(int, __iter_init_39_, 0, <, 2, __iter_init_39_++)
		init_buffer_float(&SplitJoin85_FirFilter_Fiss_4111_4145_split[__iter_init_39_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3933WEIGHTED_ROUND_ROBIN_Splitter_4080);
	FOR(int, __iter_init_40_, 0, <, 2, __iter_init_40_++)
		init_buffer_float(&SplitJoin22_Delay_N_Fiss_4092_4126_join[__iter_init_40_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3973DownSamp_3884);
	FOR(int, __iter_init_41_, 0, <, 2, __iter_init_41_++)
		init_buffer_float(&SplitJoin24_FirFilter_Fiss_4093_4127_join[__iter_init_41_]);
	ENDFOR
	FOR(int, __iter_init_42_, 0, <, 2, __iter_init_42_++)
		init_buffer_float(&SplitJoin100_Delay_N_Fiss_4116_4150_join[__iter_init_42_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4081sink_3931);
	FOR(int, __iter_init_43_, 0, <, 2, __iter_init_43_++)
		init_buffer_float(&SplitJoin4_FirFilter_Fiss_4086_4121_split[__iter_init_43_]);
	ENDFOR
	FOR(int, __iter_init_44_, 0, <, 2, __iter_init_44_++)
		init_buffer_float(&SplitJoin50_FirFilter_Fiss_4101_4135_split[__iter_init_44_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4053DownSamp_3919);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3969DUPLICATE_Splitter_3972);
	FOR(int, __iter_init_45_, 0, <, 2, __iter_init_45_++)
		init_buffer_float(&SplitJoin61_Delay_N_Fiss_4104_4138_split[__iter_init_45_]);
	ENDFOR
	FOR(int, __iter_init_46_, 0, <, 2, __iter_init_46_++)
		init_buffer_float(&SplitJoin44_Delay_N_Fiss_4098_4132_join[__iter_init_46_]);
	ENDFOR
	FOR(int, __iter_init_47_, 0, <, 2, __iter_init_47_++)
		init_buffer_float(&SplitJoin59_FirFilter_Fiss_4103_4137_join[__iter_init_47_]);
	ENDFOR
	FOR(int, __iter_init_48_, 0, <, 8, __iter_init_48_++)
		init_buffer_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[__iter_init_48_]);
	ENDFOR
	FOR(int, __iter_init_49_, 0, <, 2, __iter_init_49_++)
		init_buffer_float(&SplitJoin102_FirFilter_Fiss_4117_4151_join[__iter_init_49_]);
	ENDFOR
	FOR(int, __iter_init_50_, 0, <, 2, __iter_init_50_++)
		init_buffer_float(&SplitJoin33_FirFilter_Fiss_4095_4129_split[__iter_init_50_]);
	ENDFOR
	FOR(int, __iter_init_51_, 0, <, 2, __iter_init_51_++)
		init_buffer_float(&SplitJoin31_Delay_N_Fiss_4094_4128_split[__iter_init_51_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4073DUPLICATE_Splitter_4076);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3977DUPLICATE_Splitter_3980);
	init_buffer_float(&UpSamp_3892WEIGHTED_ROUND_ROBIN_Splitter_3992);
	init_buffer_float(&source_3872DUPLICATE_Splitter_3932);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3957DownSamp_3877);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_4017DUPLICATE_Splitter_4020);
	FOR(int, __iter_init_52_, 0, <, 2, __iter_init_52_++)
		init_buffer_float(&SplitJoin20_FirFilter_Fiss_4091_4125_join[__iter_init_52_]);
	ENDFOR
	init_buffer_float(&DownSamp_3919UpSamp_3920);
	FOR(int, __iter_init_53_, 0, <, 2, __iter_init_53_++)
		init_buffer_float(&SplitJoin96_Delay_N_Fiss_4114_4148_join[__iter_init_53_]);
	ENDFOR
	FOR(int, __iter_init_54_, 0, <, 2, __iter_init_54_++)
		init_buffer_float(&SplitJoin100_Delay_N_Fiss_4116_4150_split[__iter_init_54_]);
	ENDFOR
	init_buffer_float(&UpSamp_3885WEIGHTED_ROUND_ROBIN_Splitter_3976);
	FOR(int, __iter_init_55_, 0, <, 2, __iter_init_55_++)
		init_buffer_float(&SplitJoin22_Delay_N_Fiss_4092_4126_split[__iter_init_55_]);
	ENDFOR
	init_buffer_float(&UpSamp_3899WEIGHTED_ROUND_ROBIN_Splitter_4008);
	FOR(int, __iter_init_56_, 0, <, 2, __iter_init_56_++)
		init_buffer_float(&SplitJoin74_Delay_N_Fiss_4108_4142_split[__iter_init_56_]);
	ENDFOR
	init_buffer_float(&UpSamp_3906WEIGHTED_ROUND_ROBIN_Splitter_4024);
	FOR(int, __iter_init_57_, 0, <, 2, __iter_init_57_++)
		init_buffer_float(&SplitJoin20_FirFilter_Fiss_4091_4125_split[__iter_init_57_]);
	ENDFOR
	FOR(int, __iter_init_58_, 0, <, 2, __iter_init_58_++)
		init_buffer_float(&SplitJoin8_FirFilter_Fiss_4088_4123_split[__iter_init_58_]);
	ENDFOR
	FOR(int, __iter_init_59_, 0, <, 2, __iter_init_59_++)
		init_buffer_float(&SplitJoin57_Delay_N_Fiss_4102_4136_split[__iter_init_59_]);
	ENDFOR
	FOR(int, __iter_init_60_, 0, <, 2, __iter_init_60_++)
		init_buffer_float(&SplitJoin48_Delay_N_Fiss_4100_4134_join[__iter_init_60_]);
	ENDFOR
	FOR(int, __iter_init_61_, 0, <, 2, __iter_init_61_++)
		init_buffer_float(&SplitJoin8_FirFilter_Fiss_4088_4123_join[__iter_init_61_]);
	ENDFOR
	FOR(int, __iter_init_62_, 0, <, 2, __iter_init_62_++)
		init_buffer_float(&SplitJoin85_FirFilter_Fiss_4111_4145_join[__iter_init_62_]);
	ENDFOR
	init_buffer_float(&DownSamp_3926UpSamp_3927);
	FOR(int, __iter_init_63_, 0, <, 2, __iter_init_63_++)
		init_buffer_float(&SplitJoin72_FirFilter_Fiss_4107_4141_join[__iter_init_63_]);
	ENDFOR
	FOR(int, __iter_init_64_, 0, <, 2, __iter_init_64_++)
		init_buffer_float(&SplitJoin18_Delay_N_Fiss_4090_4124_split[__iter_init_64_]);
	ENDFOR
	FOR(int, __iter_init_65_, 0, <, 2, __iter_init_65_++)
		init_buffer_float(&SplitJoin35_Delay_N_Fiss_4096_4130_split[__iter_init_65_]);
	ENDFOR
	FOR(int, __iter_init_66_, 0, <, 2, __iter_init_66_++)
		init_buffer_float(&SplitJoin37_FirFilter_Fiss_4097_4131_join[__iter_init_66_]);
	ENDFOR
	FOR(int, __iter_init_67_, 0, <, 2, __iter_init_67_++)
		init_buffer_float(&SplitJoin89_FirFilter_Fiss_4113_4147_split[__iter_init_67_]);
	ENDFOR
// --- init: source_3872
	 {
	source_3872_s.current = 0.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 186, __iter_init_++) {
		push_float(&source_3872DUPLICATE_Splitter_3932, source_3872_s.current) ; 
		if((source_3872_s.current > 1000.0)) {
			source_3872_s.current = 0.0 ; 
		}
		else {
			source_3872_s.current = (source_3872_s.current + 1.0) ; 
		}
	}
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3932
	FOR(uint32_t, __iter_init_, 0, <, 186, __iter_init_++)
		float __token_ = pop_float(&source_3872DUPLICATE_Splitter_3932);
		FOR(uint32_t, __iter_dup_, 0, <, 8, __iter_dup_++)
			push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3952
	FOR(uint32_t, __iter_init_, 0, <, 93, __iter_init_++)
		push_float(&SplitJoin2_Delay_N_Fiss_4085_4120_split[0], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[0]));
		push_float(&SplitJoin2_Delay_N_Fiss_4085_4120_split[1], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[0]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3954
	FOR(uint32_t, __iter_init_, 0, <, 65, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin2_Delay_N_Fiss_4085_4120_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3955
	FOR(uint32_t, __iter_init_, 0, <, 65, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin2_Delay_N_Fiss_4085_4120_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3953
	FOR(uint32_t, __iter_init_, 0, <, 64, __iter_init_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3953DUPLICATE_Splitter_3956, pop_float(&SplitJoin2_Delay_N_Fiss_4085_4120_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3953DUPLICATE_Splitter_3956, pop_float(&SplitJoin2_Delay_N_Fiss_4085_4120_join[1]));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3956
	FOR(uint32_t, __iter_init_, 0, <, 127, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3953DUPLICATE_Splitter_3956);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin4_FirFilter_Fiss_4086_4121_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_3958
	 {
	FirFilter_3958_s.COEFF[0] = 1.0 ; 
	FirFilter_3958_s.COEFF[1] = 34.0 ; 
	FirFilter_3958_s.COEFF[2] = 67.0 ; 
	FirFilter_3958_s.COEFF[3] = 100.0 ; 
	FirFilter_3958_s.COEFF[4] = 133.0 ; 
	FirFilter_3958_s.COEFF[5] = 166.0 ; 
	FirFilter_3958_s.COEFF[6] = 199.0 ; 
	FirFilter_3958_s.COEFF[7] = 232.0 ; 
	FirFilter_3958_s.COEFF[8] = 265.0 ; 
	FirFilter_3958_s.COEFF[9] = 298.0 ; 
	FirFilter_3958_s.COEFF[10] = 331.0 ; 
	FirFilter_3958_s.COEFF[11] = 364.0 ; 
	FirFilter_3958_s.COEFF[12] = 397.0 ; 
	FirFilter_3958_s.COEFF[13] = 430.0 ; 
	FirFilter_3958_s.COEFF[14] = 463.0 ; 
	FirFilter_3958_s.COEFF[15] = 496.0 ; 
	FirFilter_3958_s.COEFF[16] = 529.0 ; 
	FirFilter_3958_s.COEFF[17] = 562.0 ; 
	FirFilter_3958_s.COEFF[18] = 595.0 ; 
	FirFilter_3958_s.COEFF[19] = 628.0 ; 
	FirFilter_3958_s.COEFF[20] = 661.0 ; 
	FirFilter_3958_s.COEFF[21] = 694.0 ; 
	FirFilter_3958_s.COEFF[22] = 727.0 ; 
	FirFilter_3958_s.COEFF[23] = 760.0 ; 
	FirFilter_3958_s.COEFF[24] = 793.0 ; 
	FirFilter_3958_s.COEFF[25] = 826.0 ; 
	FirFilter_3958_s.COEFF[26] = 859.0 ; 
	FirFilter_3958_s.COEFF[27] = 892.0 ; 
	FirFilter_3958_s.COEFF[28] = 925.0 ; 
	FirFilter_3958_s.COEFF[29] = 958.0 ; 
	FirFilter_3958_s.COEFF[30] = 991.0 ; 
	FirFilter_3958_s.COEFF[31] = 1024.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin4_FirFilter_Fiss_4086_4121_split[0], i) * FirFilter_3958_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin4_FirFilter_Fiss_4086_4121_split[0]) ; 
		push_float(&SplitJoin4_FirFilter_Fiss_4086_4121_join[0], sum) ; 
 {
		pop_void(&SplitJoin4_FirFilter_Fiss_4086_4121_split[0]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_3959
	 {
	FirFilter_3959_s.COEFF[0] = 1.0 ; 
	FirFilter_3959_s.COEFF[1] = 34.0 ; 
	FirFilter_3959_s.COEFF[2] = 67.0 ; 
	FirFilter_3959_s.COEFF[3] = 100.0 ; 
	FirFilter_3959_s.COEFF[4] = 133.0 ; 
	FirFilter_3959_s.COEFF[5] = 166.0 ; 
	FirFilter_3959_s.COEFF[6] = 199.0 ; 
	FirFilter_3959_s.COEFF[7] = 232.0 ; 
	FirFilter_3959_s.COEFF[8] = 265.0 ; 
	FirFilter_3959_s.COEFF[9] = 298.0 ; 
	FirFilter_3959_s.COEFF[10] = 331.0 ; 
	FirFilter_3959_s.COEFF[11] = 364.0 ; 
	FirFilter_3959_s.COEFF[12] = 397.0 ; 
	FirFilter_3959_s.COEFF[13] = 430.0 ; 
	FirFilter_3959_s.COEFF[14] = 463.0 ; 
	FirFilter_3959_s.COEFF[15] = 496.0 ; 
	FirFilter_3959_s.COEFF[16] = 529.0 ; 
	FirFilter_3959_s.COEFF[17] = 562.0 ; 
	FirFilter_3959_s.COEFF[18] = 595.0 ; 
	FirFilter_3959_s.COEFF[19] = 628.0 ; 
	FirFilter_3959_s.COEFF[20] = 661.0 ; 
	FirFilter_3959_s.COEFF[21] = 694.0 ; 
	FirFilter_3959_s.COEFF[22] = 727.0 ; 
	FirFilter_3959_s.COEFF[23] = 760.0 ; 
	FirFilter_3959_s.COEFF[24] = 793.0 ; 
	FirFilter_3959_s.COEFF[25] = 826.0 ; 
	FirFilter_3959_s.COEFF[26] = 859.0 ; 
	FirFilter_3959_s.COEFF[27] = 892.0 ; 
	FirFilter_3959_s.COEFF[28] = 925.0 ; 
	FirFilter_3959_s.COEFF[29] = 958.0 ; 
	FirFilter_3959_s.COEFF[30] = 991.0 ; 
	FirFilter_3959_s.COEFF[31] = 1024.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin4_FirFilter_Fiss_4086_4121_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin4_FirFilter_Fiss_4086_4121_split[1], i) * FirFilter_3959_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin4_FirFilter_Fiss_4086_4121_split[1]) ; 
		push_float(&SplitJoin4_FirFilter_Fiss_4086_4121_join[1], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3957
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3957DownSamp_3877, pop_float(&SplitJoin4_FirFilter_Fiss_4086_4121_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3957DownSamp_3877, pop_float(&SplitJoin4_FirFilter_Fiss_4086_4121_join[1]));
	ENDFOR
//--------------------------------
// --- init: DownSamp_3877
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		push_float(&DownSamp_3877UpSamp_3878, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3957DownSamp_3877)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3957DownSamp_3877) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: UpSamp_3878
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		push_float(&UpSamp_3878WEIGHTED_ROUND_ROBIN_Splitter_3960, pop_float(&DownSamp_3877UpSamp_3878)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			push_float(&UpSamp_3878WEIGHTED_ROUND_ROBIN_Splitter_3960, 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3960
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++)
		push_float(&SplitJoin6_Delay_N_Fiss_4087_4122_split[0], pop_float(&UpSamp_3878WEIGHTED_ROUND_ROBIN_Splitter_3960));
		push_float(&SplitJoin6_Delay_N_Fiss_4087_4122_split[1], pop_float(&UpSamp_3878WEIGHTED_ROUND_ROBIN_Splitter_3960));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3962
	FOR(uint32_t, __iter_init_, 0, <, 20, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin6_Delay_N_Fiss_4087_4122_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3963
	FOR(uint32_t, __iter_init_, 0, <, 20, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin6_Delay_N_Fiss_4087_4122_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3961
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3961DUPLICATE_Splitter_3964, pop_float(&SplitJoin6_Delay_N_Fiss_4087_4122_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3961DUPLICATE_Splitter_3964, pop_float(&SplitJoin6_Delay_N_Fiss_4087_4122_join[1]));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3964
	FOR(uint32_t, __iter_init_, 0, <, 37, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3961DUPLICATE_Splitter_3964);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin8_FirFilter_Fiss_4088_4123_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_3966
	 {
	FirFilter_3966_s.COEFF[0] = 0.0 ; 
	FirFilter_3966_s.COEFF[1] = 1.0 ; 
	FirFilter_3966_s.COEFF[2] = 2.0 ; 
	FirFilter_3966_s.COEFF[3] = 3.0 ; 
	FirFilter_3966_s.COEFF[4] = 4.0 ; 
	FirFilter_3966_s.COEFF[5] = 5.0 ; 
	FirFilter_3966_s.COEFF[6] = 6.0 ; 
	FirFilter_3966_s.COEFF[7] = 7.0 ; 
	FirFilter_3966_s.COEFF[8] = 8.0 ; 
	FirFilter_3966_s.COEFF[9] = 9.0 ; 
	FirFilter_3966_s.COEFF[10] = 10.0 ; 
	FirFilter_3966_s.COEFF[11] = 11.0 ; 
	FirFilter_3966_s.COEFF[12] = 12.0 ; 
	FirFilter_3966_s.COEFF[13] = 13.0 ; 
	FirFilter_3966_s.COEFF[14] = 14.0 ; 
	FirFilter_3966_s.COEFF[15] = 15.0 ; 
	FirFilter_3966_s.COEFF[16] = 16.0 ; 
	FirFilter_3966_s.COEFF[17] = 17.0 ; 
	FirFilter_3966_s.COEFF[18] = 18.0 ; 
	FirFilter_3966_s.COEFF[19] = 19.0 ; 
	FirFilter_3966_s.COEFF[20] = 20.0 ; 
	FirFilter_3966_s.COEFF[21] = 21.0 ; 
	FirFilter_3966_s.COEFF[22] = 22.0 ; 
	FirFilter_3966_s.COEFF[23] = 23.0 ; 
	FirFilter_3966_s.COEFF[24] = 24.0 ; 
	FirFilter_3966_s.COEFF[25] = 25.0 ; 
	FirFilter_3966_s.COEFF[26] = 26.0 ; 
	FirFilter_3966_s.COEFF[27] = 27.0 ; 
	FirFilter_3966_s.COEFF[28] = 28.0 ; 
	FirFilter_3966_s.COEFF[29] = 29.0 ; 
	FirFilter_3966_s.COEFF[30] = 30.0 ; 
	FirFilter_3966_s.COEFF[31] = 31.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin8_FirFilter_Fiss_4088_4123_split[0], i) * FirFilter_3966_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin8_FirFilter_Fiss_4088_4123_split[0]) ; 
		push_float(&SplitJoin8_FirFilter_Fiss_4088_4123_join[0], sum) ; 
 {
		pop_void(&SplitJoin8_FirFilter_Fiss_4088_4123_split[0]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_3967
	 {
	FirFilter_3967_s.COEFF[0] = 0.0 ; 
	FirFilter_3967_s.COEFF[1] = 1.0 ; 
	FirFilter_3967_s.COEFF[2] = 2.0 ; 
	FirFilter_3967_s.COEFF[3] = 3.0 ; 
	FirFilter_3967_s.COEFF[4] = 4.0 ; 
	FirFilter_3967_s.COEFF[5] = 5.0 ; 
	FirFilter_3967_s.COEFF[6] = 6.0 ; 
	FirFilter_3967_s.COEFF[7] = 7.0 ; 
	FirFilter_3967_s.COEFF[8] = 8.0 ; 
	FirFilter_3967_s.COEFF[9] = 9.0 ; 
	FirFilter_3967_s.COEFF[10] = 10.0 ; 
	FirFilter_3967_s.COEFF[11] = 11.0 ; 
	FirFilter_3967_s.COEFF[12] = 12.0 ; 
	FirFilter_3967_s.COEFF[13] = 13.0 ; 
	FirFilter_3967_s.COEFF[14] = 14.0 ; 
	FirFilter_3967_s.COEFF[15] = 15.0 ; 
	FirFilter_3967_s.COEFF[16] = 16.0 ; 
	FirFilter_3967_s.COEFF[17] = 17.0 ; 
	FirFilter_3967_s.COEFF[18] = 18.0 ; 
	FirFilter_3967_s.COEFF[19] = 19.0 ; 
	FirFilter_3967_s.COEFF[20] = 20.0 ; 
	FirFilter_3967_s.COEFF[21] = 21.0 ; 
	FirFilter_3967_s.COEFF[22] = 22.0 ; 
	FirFilter_3967_s.COEFF[23] = 23.0 ; 
	FirFilter_3967_s.COEFF[24] = 24.0 ; 
	FirFilter_3967_s.COEFF[25] = 25.0 ; 
	FirFilter_3967_s.COEFF[26] = 26.0 ; 
	FirFilter_3967_s.COEFF[27] = 27.0 ; 
	FirFilter_3967_s.COEFF[28] = 28.0 ; 
	FirFilter_3967_s.COEFF[29] = 29.0 ; 
	FirFilter_3967_s.COEFF[30] = 30.0 ; 
	FirFilter_3967_s.COEFF[31] = 31.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin8_FirFilter_Fiss_4088_4123_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin8_FirFilter_Fiss_4088_4123_split[1], i) * FirFilter_3967_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin8_FirFilter_Fiss_4088_4123_split[1]) ; 
		push_float(&SplitJoin8_FirFilter_Fiss_4088_4123_join[1], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3965
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[0], pop_float(&SplitJoin8_FirFilter_Fiss_4088_4123_join[0]));
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[0], pop_float(&SplitJoin8_FirFilter_Fiss_4088_4123_join[1]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3968
	FOR(uint32_t, __iter_init_, 0, <, 93, __iter_init_++)
		push_float(&SplitJoin18_Delay_N_Fiss_4090_4124_split[0], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[1]));
		push_float(&SplitJoin18_Delay_N_Fiss_4090_4124_split[1], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[1]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3970
	FOR(uint32_t, __iter_init_, 0, <, 65, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin18_Delay_N_Fiss_4090_4124_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3971
	FOR(uint32_t, __iter_init_, 0, <, 65, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin18_Delay_N_Fiss_4090_4124_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3969
	FOR(uint32_t, __iter_init_, 0, <, 64, __iter_init_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3969DUPLICATE_Splitter_3972, pop_float(&SplitJoin18_Delay_N_Fiss_4090_4124_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3969DUPLICATE_Splitter_3972, pop_float(&SplitJoin18_Delay_N_Fiss_4090_4124_join[1]));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3972
	FOR(uint32_t, __iter_init_, 0, <, 127, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3969DUPLICATE_Splitter_3972);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin20_FirFilter_Fiss_4091_4125_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_3974
	 {
	FirFilter_3974_s.COEFF[0] = 11.0 ; 
	FirFilter_3974_s.COEFF[1] = 44.0 ; 
	FirFilter_3974_s.COEFF[2] = 77.0 ; 
	FirFilter_3974_s.COEFF[3] = 110.0 ; 
	FirFilter_3974_s.COEFF[4] = 143.0 ; 
	FirFilter_3974_s.COEFF[5] = 176.0 ; 
	FirFilter_3974_s.COEFF[6] = 209.0 ; 
	FirFilter_3974_s.COEFF[7] = 242.0 ; 
	FirFilter_3974_s.COEFF[8] = 275.0 ; 
	FirFilter_3974_s.COEFF[9] = 308.0 ; 
	FirFilter_3974_s.COEFF[10] = 341.0 ; 
	FirFilter_3974_s.COEFF[11] = 374.0 ; 
	FirFilter_3974_s.COEFF[12] = 407.0 ; 
	FirFilter_3974_s.COEFF[13] = 440.0 ; 
	FirFilter_3974_s.COEFF[14] = 473.0 ; 
	FirFilter_3974_s.COEFF[15] = 506.0 ; 
	FirFilter_3974_s.COEFF[16] = 539.0 ; 
	FirFilter_3974_s.COEFF[17] = 572.0 ; 
	FirFilter_3974_s.COEFF[18] = 605.0 ; 
	FirFilter_3974_s.COEFF[19] = 638.0 ; 
	FirFilter_3974_s.COEFF[20] = 671.0 ; 
	FirFilter_3974_s.COEFF[21] = 704.0 ; 
	FirFilter_3974_s.COEFF[22] = 737.0 ; 
	FirFilter_3974_s.COEFF[23] = 770.0 ; 
	FirFilter_3974_s.COEFF[24] = 803.0 ; 
	FirFilter_3974_s.COEFF[25] = 836.0 ; 
	FirFilter_3974_s.COEFF[26] = 869.0 ; 
	FirFilter_3974_s.COEFF[27] = 902.0 ; 
	FirFilter_3974_s.COEFF[28] = 935.0 ; 
	FirFilter_3974_s.COEFF[29] = 968.0 ; 
	FirFilter_3974_s.COEFF[30] = 1001.0 ; 
	FirFilter_3974_s.COEFF[31] = 1034.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin20_FirFilter_Fiss_4091_4125_split[0], i) * FirFilter_3974_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin20_FirFilter_Fiss_4091_4125_split[0]) ; 
		push_float(&SplitJoin20_FirFilter_Fiss_4091_4125_join[0], sum) ; 
 {
		pop_void(&SplitJoin20_FirFilter_Fiss_4091_4125_split[0]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_3975
	 {
	FirFilter_3975_s.COEFF[0] = 11.0 ; 
	FirFilter_3975_s.COEFF[1] = 44.0 ; 
	FirFilter_3975_s.COEFF[2] = 77.0 ; 
	FirFilter_3975_s.COEFF[3] = 110.0 ; 
	FirFilter_3975_s.COEFF[4] = 143.0 ; 
	FirFilter_3975_s.COEFF[5] = 176.0 ; 
	FirFilter_3975_s.COEFF[6] = 209.0 ; 
	FirFilter_3975_s.COEFF[7] = 242.0 ; 
	FirFilter_3975_s.COEFF[8] = 275.0 ; 
	FirFilter_3975_s.COEFF[9] = 308.0 ; 
	FirFilter_3975_s.COEFF[10] = 341.0 ; 
	FirFilter_3975_s.COEFF[11] = 374.0 ; 
	FirFilter_3975_s.COEFF[12] = 407.0 ; 
	FirFilter_3975_s.COEFF[13] = 440.0 ; 
	FirFilter_3975_s.COEFF[14] = 473.0 ; 
	FirFilter_3975_s.COEFF[15] = 506.0 ; 
	FirFilter_3975_s.COEFF[16] = 539.0 ; 
	FirFilter_3975_s.COEFF[17] = 572.0 ; 
	FirFilter_3975_s.COEFF[18] = 605.0 ; 
	FirFilter_3975_s.COEFF[19] = 638.0 ; 
	FirFilter_3975_s.COEFF[20] = 671.0 ; 
	FirFilter_3975_s.COEFF[21] = 704.0 ; 
	FirFilter_3975_s.COEFF[22] = 737.0 ; 
	FirFilter_3975_s.COEFF[23] = 770.0 ; 
	FirFilter_3975_s.COEFF[24] = 803.0 ; 
	FirFilter_3975_s.COEFF[25] = 836.0 ; 
	FirFilter_3975_s.COEFF[26] = 869.0 ; 
	FirFilter_3975_s.COEFF[27] = 902.0 ; 
	FirFilter_3975_s.COEFF[28] = 935.0 ; 
	FirFilter_3975_s.COEFF[29] = 968.0 ; 
	FirFilter_3975_s.COEFF[30] = 1001.0 ; 
	FirFilter_3975_s.COEFF[31] = 1034.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin20_FirFilter_Fiss_4091_4125_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin20_FirFilter_Fiss_4091_4125_split[1], i) * FirFilter_3975_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin20_FirFilter_Fiss_4091_4125_split[1]) ; 
		push_float(&SplitJoin20_FirFilter_Fiss_4091_4125_join[1], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3973
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3973DownSamp_3884, pop_float(&SplitJoin20_FirFilter_Fiss_4091_4125_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3973DownSamp_3884, pop_float(&SplitJoin20_FirFilter_Fiss_4091_4125_join[1]));
	ENDFOR
//--------------------------------
// --- init: DownSamp_3884
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		push_float(&DownSamp_3884UpSamp_3885, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3973DownSamp_3884)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3973DownSamp_3884) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: UpSamp_3885
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		push_float(&UpSamp_3885WEIGHTED_ROUND_ROBIN_Splitter_3976, pop_float(&DownSamp_3884UpSamp_3885)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			push_float(&UpSamp_3885WEIGHTED_ROUND_ROBIN_Splitter_3976, 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3976
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++)
		push_float(&SplitJoin22_Delay_N_Fiss_4092_4126_split[0], pop_float(&UpSamp_3885WEIGHTED_ROUND_ROBIN_Splitter_3976));
		push_float(&SplitJoin22_Delay_N_Fiss_4092_4126_split[1], pop_float(&UpSamp_3885WEIGHTED_ROUND_ROBIN_Splitter_3976));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3978
	FOR(uint32_t, __iter_init_, 0, <, 20, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin22_Delay_N_Fiss_4092_4126_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3979
	FOR(uint32_t, __iter_init_, 0, <, 20, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin22_Delay_N_Fiss_4092_4126_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3977
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3977DUPLICATE_Splitter_3980, pop_float(&SplitJoin22_Delay_N_Fiss_4092_4126_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3977DUPLICATE_Splitter_3980, pop_float(&SplitJoin22_Delay_N_Fiss_4092_4126_join[1]));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3980
	FOR(uint32_t, __iter_init_, 0, <, 37, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3977DUPLICATE_Splitter_3980);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin24_FirFilter_Fiss_4093_4127_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_3982
	 {
	FirFilter_3982_s.COEFF[0] = 2.0 ; 
	FirFilter_3982_s.COEFF[1] = 4.0 ; 
	FirFilter_3982_s.COEFF[2] = 6.0 ; 
	FirFilter_3982_s.COEFF[3] = 8.0 ; 
	FirFilter_3982_s.COEFF[4] = 10.0 ; 
	FirFilter_3982_s.COEFF[5] = 12.0 ; 
	FirFilter_3982_s.COEFF[6] = 14.0 ; 
	FirFilter_3982_s.COEFF[7] = 16.0 ; 
	FirFilter_3982_s.COEFF[8] = 18.0 ; 
	FirFilter_3982_s.COEFF[9] = 20.0 ; 
	FirFilter_3982_s.COEFF[10] = 22.0 ; 
	FirFilter_3982_s.COEFF[11] = 24.0 ; 
	FirFilter_3982_s.COEFF[12] = 26.0 ; 
	FirFilter_3982_s.COEFF[13] = 28.0 ; 
	FirFilter_3982_s.COEFF[14] = 30.0 ; 
	FirFilter_3982_s.COEFF[15] = 32.0 ; 
	FirFilter_3982_s.COEFF[16] = 34.0 ; 
	FirFilter_3982_s.COEFF[17] = 36.0 ; 
	FirFilter_3982_s.COEFF[18] = 38.0 ; 
	FirFilter_3982_s.COEFF[19] = 40.0 ; 
	FirFilter_3982_s.COEFF[20] = 42.0 ; 
	FirFilter_3982_s.COEFF[21] = 44.0 ; 
	FirFilter_3982_s.COEFF[22] = 46.0 ; 
	FirFilter_3982_s.COEFF[23] = 48.0 ; 
	FirFilter_3982_s.COEFF[24] = 50.0 ; 
	FirFilter_3982_s.COEFF[25] = 52.0 ; 
	FirFilter_3982_s.COEFF[26] = 54.0 ; 
	FirFilter_3982_s.COEFF[27] = 56.0 ; 
	FirFilter_3982_s.COEFF[28] = 58.0 ; 
	FirFilter_3982_s.COEFF[29] = 60.0 ; 
	FirFilter_3982_s.COEFF[30] = 62.0 ; 
	FirFilter_3982_s.COEFF[31] = 64.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin24_FirFilter_Fiss_4093_4127_split[0], i) * FirFilter_3982_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin24_FirFilter_Fiss_4093_4127_split[0]) ; 
		push_float(&SplitJoin24_FirFilter_Fiss_4093_4127_join[0], sum) ; 
 {
		pop_void(&SplitJoin24_FirFilter_Fiss_4093_4127_split[0]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_3983
	 {
	FirFilter_3983_s.COEFF[0] = 2.0 ; 
	FirFilter_3983_s.COEFF[1] = 4.0 ; 
	FirFilter_3983_s.COEFF[2] = 6.0 ; 
	FirFilter_3983_s.COEFF[3] = 8.0 ; 
	FirFilter_3983_s.COEFF[4] = 10.0 ; 
	FirFilter_3983_s.COEFF[5] = 12.0 ; 
	FirFilter_3983_s.COEFF[6] = 14.0 ; 
	FirFilter_3983_s.COEFF[7] = 16.0 ; 
	FirFilter_3983_s.COEFF[8] = 18.0 ; 
	FirFilter_3983_s.COEFF[9] = 20.0 ; 
	FirFilter_3983_s.COEFF[10] = 22.0 ; 
	FirFilter_3983_s.COEFF[11] = 24.0 ; 
	FirFilter_3983_s.COEFF[12] = 26.0 ; 
	FirFilter_3983_s.COEFF[13] = 28.0 ; 
	FirFilter_3983_s.COEFF[14] = 30.0 ; 
	FirFilter_3983_s.COEFF[15] = 32.0 ; 
	FirFilter_3983_s.COEFF[16] = 34.0 ; 
	FirFilter_3983_s.COEFF[17] = 36.0 ; 
	FirFilter_3983_s.COEFF[18] = 38.0 ; 
	FirFilter_3983_s.COEFF[19] = 40.0 ; 
	FirFilter_3983_s.COEFF[20] = 42.0 ; 
	FirFilter_3983_s.COEFF[21] = 44.0 ; 
	FirFilter_3983_s.COEFF[22] = 46.0 ; 
	FirFilter_3983_s.COEFF[23] = 48.0 ; 
	FirFilter_3983_s.COEFF[24] = 50.0 ; 
	FirFilter_3983_s.COEFF[25] = 52.0 ; 
	FirFilter_3983_s.COEFF[26] = 54.0 ; 
	FirFilter_3983_s.COEFF[27] = 56.0 ; 
	FirFilter_3983_s.COEFF[28] = 58.0 ; 
	FirFilter_3983_s.COEFF[29] = 60.0 ; 
	FirFilter_3983_s.COEFF[30] = 62.0 ; 
	FirFilter_3983_s.COEFF[31] = 64.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin24_FirFilter_Fiss_4093_4127_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin24_FirFilter_Fiss_4093_4127_split[1], i) * FirFilter_3983_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin24_FirFilter_Fiss_4093_4127_split[1]) ; 
		push_float(&SplitJoin24_FirFilter_Fiss_4093_4127_join[1], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3981
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[1], pop_float(&SplitJoin24_FirFilter_Fiss_4093_4127_join[0]));
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[1], pop_float(&SplitJoin24_FirFilter_Fiss_4093_4127_join[1]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3984
	FOR(uint32_t, __iter_init_, 0, <, 93, __iter_init_++)
		push_float(&SplitJoin31_Delay_N_Fiss_4094_4128_split[0], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[2]));
		push_float(&SplitJoin31_Delay_N_Fiss_4094_4128_split[1], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[2]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3986
	FOR(uint32_t, __iter_init_, 0, <, 65, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin31_Delay_N_Fiss_4094_4128_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3987
	FOR(uint32_t, __iter_init_, 0, <, 65, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin31_Delay_N_Fiss_4094_4128_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3985
	FOR(uint32_t, __iter_init_, 0, <, 64, __iter_init_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3985DUPLICATE_Splitter_3988, pop_float(&SplitJoin31_Delay_N_Fiss_4094_4128_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3985DUPLICATE_Splitter_3988, pop_float(&SplitJoin31_Delay_N_Fiss_4094_4128_join[1]));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3988
	FOR(uint32_t, __iter_init_, 0, <, 127, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3985DUPLICATE_Splitter_3988);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin33_FirFilter_Fiss_4095_4129_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_3990
	 {
	FirFilter_3990_s.COEFF[0] = 21.0 ; 
	FirFilter_3990_s.COEFF[1] = 54.0 ; 
	FirFilter_3990_s.COEFF[2] = 87.0 ; 
	FirFilter_3990_s.COEFF[3] = 120.0 ; 
	FirFilter_3990_s.COEFF[4] = 153.0 ; 
	FirFilter_3990_s.COEFF[5] = 186.0 ; 
	FirFilter_3990_s.COEFF[6] = 219.0 ; 
	FirFilter_3990_s.COEFF[7] = 252.0 ; 
	FirFilter_3990_s.COEFF[8] = 285.0 ; 
	FirFilter_3990_s.COEFF[9] = 318.0 ; 
	FirFilter_3990_s.COEFF[10] = 351.0 ; 
	FirFilter_3990_s.COEFF[11] = 384.0 ; 
	FirFilter_3990_s.COEFF[12] = 417.0 ; 
	FirFilter_3990_s.COEFF[13] = 450.0 ; 
	FirFilter_3990_s.COEFF[14] = 483.0 ; 
	FirFilter_3990_s.COEFF[15] = 516.0 ; 
	FirFilter_3990_s.COEFF[16] = 549.0 ; 
	FirFilter_3990_s.COEFF[17] = 582.0 ; 
	FirFilter_3990_s.COEFF[18] = 615.0 ; 
	FirFilter_3990_s.COEFF[19] = 648.0 ; 
	FirFilter_3990_s.COEFF[20] = 681.0 ; 
	FirFilter_3990_s.COEFF[21] = 714.0 ; 
	FirFilter_3990_s.COEFF[22] = 747.0 ; 
	FirFilter_3990_s.COEFF[23] = 780.0 ; 
	FirFilter_3990_s.COEFF[24] = 813.0 ; 
	FirFilter_3990_s.COEFF[25] = 846.0 ; 
	FirFilter_3990_s.COEFF[26] = 879.0 ; 
	FirFilter_3990_s.COEFF[27] = 912.0 ; 
	FirFilter_3990_s.COEFF[28] = 945.0 ; 
	FirFilter_3990_s.COEFF[29] = 978.0 ; 
	FirFilter_3990_s.COEFF[30] = 1011.0 ; 
	FirFilter_3990_s.COEFF[31] = 1044.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin33_FirFilter_Fiss_4095_4129_split[0], i) * FirFilter_3990_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin33_FirFilter_Fiss_4095_4129_split[0]) ; 
		push_float(&SplitJoin33_FirFilter_Fiss_4095_4129_join[0], sum) ; 
 {
		pop_void(&SplitJoin33_FirFilter_Fiss_4095_4129_split[0]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_3991
	 {
	FirFilter_3991_s.COEFF[0] = 21.0 ; 
	FirFilter_3991_s.COEFF[1] = 54.0 ; 
	FirFilter_3991_s.COEFF[2] = 87.0 ; 
	FirFilter_3991_s.COEFF[3] = 120.0 ; 
	FirFilter_3991_s.COEFF[4] = 153.0 ; 
	FirFilter_3991_s.COEFF[5] = 186.0 ; 
	FirFilter_3991_s.COEFF[6] = 219.0 ; 
	FirFilter_3991_s.COEFF[7] = 252.0 ; 
	FirFilter_3991_s.COEFF[8] = 285.0 ; 
	FirFilter_3991_s.COEFF[9] = 318.0 ; 
	FirFilter_3991_s.COEFF[10] = 351.0 ; 
	FirFilter_3991_s.COEFF[11] = 384.0 ; 
	FirFilter_3991_s.COEFF[12] = 417.0 ; 
	FirFilter_3991_s.COEFF[13] = 450.0 ; 
	FirFilter_3991_s.COEFF[14] = 483.0 ; 
	FirFilter_3991_s.COEFF[15] = 516.0 ; 
	FirFilter_3991_s.COEFF[16] = 549.0 ; 
	FirFilter_3991_s.COEFF[17] = 582.0 ; 
	FirFilter_3991_s.COEFF[18] = 615.0 ; 
	FirFilter_3991_s.COEFF[19] = 648.0 ; 
	FirFilter_3991_s.COEFF[20] = 681.0 ; 
	FirFilter_3991_s.COEFF[21] = 714.0 ; 
	FirFilter_3991_s.COEFF[22] = 747.0 ; 
	FirFilter_3991_s.COEFF[23] = 780.0 ; 
	FirFilter_3991_s.COEFF[24] = 813.0 ; 
	FirFilter_3991_s.COEFF[25] = 846.0 ; 
	FirFilter_3991_s.COEFF[26] = 879.0 ; 
	FirFilter_3991_s.COEFF[27] = 912.0 ; 
	FirFilter_3991_s.COEFF[28] = 945.0 ; 
	FirFilter_3991_s.COEFF[29] = 978.0 ; 
	FirFilter_3991_s.COEFF[30] = 1011.0 ; 
	FirFilter_3991_s.COEFF[31] = 1044.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin33_FirFilter_Fiss_4095_4129_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin33_FirFilter_Fiss_4095_4129_split[1], i) * FirFilter_3991_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin33_FirFilter_Fiss_4095_4129_split[1]) ; 
		push_float(&SplitJoin33_FirFilter_Fiss_4095_4129_join[1], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3989
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3989DownSamp_3891, pop_float(&SplitJoin33_FirFilter_Fiss_4095_4129_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3989DownSamp_3891, pop_float(&SplitJoin33_FirFilter_Fiss_4095_4129_join[1]));
	ENDFOR
//--------------------------------
// --- init: DownSamp_3891
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		push_float(&DownSamp_3891UpSamp_3892, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3989DownSamp_3891)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3989DownSamp_3891) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: UpSamp_3892
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		push_float(&UpSamp_3892WEIGHTED_ROUND_ROBIN_Splitter_3992, pop_float(&DownSamp_3891UpSamp_3892)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			push_float(&UpSamp_3892WEIGHTED_ROUND_ROBIN_Splitter_3992, 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_3992
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++)
		push_float(&SplitJoin35_Delay_N_Fiss_4096_4130_split[0], pop_float(&UpSamp_3892WEIGHTED_ROUND_ROBIN_Splitter_3992));
		push_float(&SplitJoin35_Delay_N_Fiss_4096_4130_split[1], pop_float(&UpSamp_3892WEIGHTED_ROUND_ROBIN_Splitter_3992));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3994
	FOR(uint32_t, __iter_init_, 0, <, 20, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin35_Delay_N_Fiss_4096_4130_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_3995
	FOR(uint32_t, __iter_init_, 0, <, 20, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin35_Delay_N_Fiss_4096_4130_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3993
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3993DUPLICATE_Splitter_3996, pop_float(&SplitJoin35_Delay_N_Fiss_4096_4130_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3993DUPLICATE_Splitter_3996, pop_float(&SplitJoin35_Delay_N_Fiss_4096_4130_join[1]));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_3996
	FOR(uint32_t, __iter_init_, 0, <, 37, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3993DUPLICATE_Splitter_3996);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin37_FirFilter_Fiss_4097_4131_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_3998
	 {
	FirFilter_3998_s.COEFF[0] = 6.0 ; 
	FirFilter_3998_s.COEFF[1] = 9.0 ; 
	FirFilter_3998_s.COEFF[2] = 12.0 ; 
	FirFilter_3998_s.COEFF[3] = 15.0 ; 
	FirFilter_3998_s.COEFF[4] = 18.0 ; 
	FirFilter_3998_s.COEFF[5] = 21.0 ; 
	FirFilter_3998_s.COEFF[6] = 24.0 ; 
	FirFilter_3998_s.COEFF[7] = 27.0 ; 
	FirFilter_3998_s.COEFF[8] = 30.0 ; 
	FirFilter_3998_s.COEFF[9] = 33.0 ; 
	FirFilter_3998_s.COEFF[10] = 36.0 ; 
	FirFilter_3998_s.COEFF[11] = 39.0 ; 
	FirFilter_3998_s.COEFF[12] = 42.0 ; 
	FirFilter_3998_s.COEFF[13] = 45.0 ; 
	FirFilter_3998_s.COEFF[14] = 48.0 ; 
	FirFilter_3998_s.COEFF[15] = 51.0 ; 
	FirFilter_3998_s.COEFF[16] = 54.0 ; 
	FirFilter_3998_s.COEFF[17] = 57.0 ; 
	FirFilter_3998_s.COEFF[18] = 60.0 ; 
	FirFilter_3998_s.COEFF[19] = 63.0 ; 
	FirFilter_3998_s.COEFF[20] = 66.0 ; 
	FirFilter_3998_s.COEFF[21] = 69.0 ; 
	FirFilter_3998_s.COEFF[22] = 72.0 ; 
	FirFilter_3998_s.COEFF[23] = 75.0 ; 
	FirFilter_3998_s.COEFF[24] = 78.0 ; 
	FirFilter_3998_s.COEFF[25] = 81.0 ; 
	FirFilter_3998_s.COEFF[26] = 84.0 ; 
	FirFilter_3998_s.COEFF[27] = 87.0 ; 
	FirFilter_3998_s.COEFF[28] = 90.0 ; 
	FirFilter_3998_s.COEFF[29] = 93.0 ; 
	FirFilter_3998_s.COEFF[30] = 96.0 ; 
	FirFilter_3998_s.COEFF[31] = 99.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin37_FirFilter_Fiss_4097_4131_split[0], i) * FirFilter_3998_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin37_FirFilter_Fiss_4097_4131_split[0]) ; 
		push_float(&SplitJoin37_FirFilter_Fiss_4097_4131_join[0], sum) ; 
 {
		pop_void(&SplitJoin37_FirFilter_Fiss_4097_4131_split[0]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_3999
	 {
	FirFilter_3999_s.COEFF[0] = 6.0 ; 
	FirFilter_3999_s.COEFF[1] = 9.0 ; 
	FirFilter_3999_s.COEFF[2] = 12.0 ; 
	FirFilter_3999_s.COEFF[3] = 15.0 ; 
	FirFilter_3999_s.COEFF[4] = 18.0 ; 
	FirFilter_3999_s.COEFF[5] = 21.0 ; 
	FirFilter_3999_s.COEFF[6] = 24.0 ; 
	FirFilter_3999_s.COEFF[7] = 27.0 ; 
	FirFilter_3999_s.COEFF[8] = 30.0 ; 
	FirFilter_3999_s.COEFF[9] = 33.0 ; 
	FirFilter_3999_s.COEFF[10] = 36.0 ; 
	FirFilter_3999_s.COEFF[11] = 39.0 ; 
	FirFilter_3999_s.COEFF[12] = 42.0 ; 
	FirFilter_3999_s.COEFF[13] = 45.0 ; 
	FirFilter_3999_s.COEFF[14] = 48.0 ; 
	FirFilter_3999_s.COEFF[15] = 51.0 ; 
	FirFilter_3999_s.COEFF[16] = 54.0 ; 
	FirFilter_3999_s.COEFF[17] = 57.0 ; 
	FirFilter_3999_s.COEFF[18] = 60.0 ; 
	FirFilter_3999_s.COEFF[19] = 63.0 ; 
	FirFilter_3999_s.COEFF[20] = 66.0 ; 
	FirFilter_3999_s.COEFF[21] = 69.0 ; 
	FirFilter_3999_s.COEFF[22] = 72.0 ; 
	FirFilter_3999_s.COEFF[23] = 75.0 ; 
	FirFilter_3999_s.COEFF[24] = 78.0 ; 
	FirFilter_3999_s.COEFF[25] = 81.0 ; 
	FirFilter_3999_s.COEFF[26] = 84.0 ; 
	FirFilter_3999_s.COEFF[27] = 87.0 ; 
	FirFilter_3999_s.COEFF[28] = 90.0 ; 
	FirFilter_3999_s.COEFF[29] = 93.0 ; 
	FirFilter_3999_s.COEFF[30] = 96.0 ; 
	FirFilter_3999_s.COEFF[31] = 99.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin37_FirFilter_Fiss_4097_4131_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin37_FirFilter_Fiss_4097_4131_split[1], i) * FirFilter_3999_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin37_FirFilter_Fiss_4097_4131_split[1]) ; 
		push_float(&SplitJoin37_FirFilter_Fiss_4097_4131_join[1], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3997
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[2], pop_float(&SplitJoin37_FirFilter_Fiss_4097_4131_join[0]));
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[2], pop_float(&SplitJoin37_FirFilter_Fiss_4097_4131_join[1]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4000
	FOR(uint32_t, __iter_init_, 0, <, 93, __iter_init_++)
		push_float(&SplitJoin44_Delay_N_Fiss_4098_4132_split[0], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[3]));
		push_float(&SplitJoin44_Delay_N_Fiss_4098_4132_split[1], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[3]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_4002
	FOR(uint32_t, __iter_init_, 0, <, 65, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin44_Delay_N_Fiss_4098_4132_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_4003
	FOR(uint32_t, __iter_init_, 0, <, 65, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin44_Delay_N_Fiss_4098_4132_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4001
	FOR(uint32_t, __iter_init_, 0, <, 64, __iter_init_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4001DUPLICATE_Splitter_4004, pop_float(&SplitJoin44_Delay_N_Fiss_4098_4132_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4001DUPLICATE_Splitter_4004, pop_float(&SplitJoin44_Delay_N_Fiss_4098_4132_join[1]));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_4004
	FOR(uint32_t, __iter_init_, 0, <, 127, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4001DUPLICATE_Splitter_4004);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin46_FirFilter_Fiss_4099_4133_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_4006
	 {
	FirFilter_4006_s.COEFF[0] = 31.0 ; 
	FirFilter_4006_s.COEFF[1] = 64.0 ; 
	FirFilter_4006_s.COEFF[2] = 97.0 ; 
	FirFilter_4006_s.COEFF[3] = 130.0 ; 
	FirFilter_4006_s.COEFF[4] = 163.0 ; 
	FirFilter_4006_s.COEFF[5] = 196.0 ; 
	FirFilter_4006_s.COEFF[6] = 229.0 ; 
	FirFilter_4006_s.COEFF[7] = 262.0 ; 
	FirFilter_4006_s.COEFF[8] = 295.0 ; 
	FirFilter_4006_s.COEFF[9] = 328.0 ; 
	FirFilter_4006_s.COEFF[10] = 361.0 ; 
	FirFilter_4006_s.COEFF[11] = 394.0 ; 
	FirFilter_4006_s.COEFF[12] = 427.0 ; 
	FirFilter_4006_s.COEFF[13] = 460.0 ; 
	FirFilter_4006_s.COEFF[14] = 493.0 ; 
	FirFilter_4006_s.COEFF[15] = 526.0 ; 
	FirFilter_4006_s.COEFF[16] = 559.0 ; 
	FirFilter_4006_s.COEFF[17] = 592.0 ; 
	FirFilter_4006_s.COEFF[18] = 625.0 ; 
	FirFilter_4006_s.COEFF[19] = 658.0 ; 
	FirFilter_4006_s.COEFF[20] = 691.0 ; 
	FirFilter_4006_s.COEFF[21] = 724.0 ; 
	FirFilter_4006_s.COEFF[22] = 757.0 ; 
	FirFilter_4006_s.COEFF[23] = 790.0 ; 
	FirFilter_4006_s.COEFF[24] = 823.0 ; 
	FirFilter_4006_s.COEFF[25] = 856.0 ; 
	FirFilter_4006_s.COEFF[26] = 889.0 ; 
	FirFilter_4006_s.COEFF[27] = 922.0 ; 
	FirFilter_4006_s.COEFF[28] = 955.0 ; 
	FirFilter_4006_s.COEFF[29] = 988.0 ; 
	FirFilter_4006_s.COEFF[30] = 1021.0 ; 
	FirFilter_4006_s.COEFF[31] = 1054.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin46_FirFilter_Fiss_4099_4133_split[0], i) * FirFilter_4006_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin46_FirFilter_Fiss_4099_4133_split[0]) ; 
		push_float(&SplitJoin46_FirFilter_Fiss_4099_4133_join[0], sum) ; 
 {
		pop_void(&SplitJoin46_FirFilter_Fiss_4099_4133_split[0]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_4007
	 {
	FirFilter_4007_s.COEFF[0] = 31.0 ; 
	FirFilter_4007_s.COEFF[1] = 64.0 ; 
	FirFilter_4007_s.COEFF[2] = 97.0 ; 
	FirFilter_4007_s.COEFF[3] = 130.0 ; 
	FirFilter_4007_s.COEFF[4] = 163.0 ; 
	FirFilter_4007_s.COEFF[5] = 196.0 ; 
	FirFilter_4007_s.COEFF[6] = 229.0 ; 
	FirFilter_4007_s.COEFF[7] = 262.0 ; 
	FirFilter_4007_s.COEFF[8] = 295.0 ; 
	FirFilter_4007_s.COEFF[9] = 328.0 ; 
	FirFilter_4007_s.COEFF[10] = 361.0 ; 
	FirFilter_4007_s.COEFF[11] = 394.0 ; 
	FirFilter_4007_s.COEFF[12] = 427.0 ; 
	FirFilter_4007_s.COEFF[13] = 460.0 ; 
	FirFilter_4007_s.COEFF[14] = 493.0 ; 
	FirFilter_4007_s.COEFF[15] = 526.0 ; 
	FirFilter_4007_s.COEFF[16] = 559.0 ; 
	FirFilter_4007_s.COEFF[17] = 592.0 ; 
	FirFilter_4007_s.COEFF[18] = 625.0 ; 
	FirFilter_4007_s.COEFF[19] = 658.0 ; 
	FirFilter_4007_s.COEFF[20] = 691.0 ; 
	FirFilter_4007_s.COEFF[21] = 724.0 ; 
	FirFilter_4007_s.COEFF[22] = 757.0 ; 
	FirFilter_4007_s.COEFF[23] = 790.0 ; 
	FirFilter_4007_s.COEFF[24] = 823.0 ; 
	FirFilter_4007_s.COEFF[25] = 856.0 ; 
	FirFilter_4007_s.COEFF[26] = 889.0 ; 
	FirFilter_4007_s.COEFF[27] = 922.0 ; 
	FirFilter_4007_s.COEFF[28] = 955.0 ; 
	FirFilter_4007_s.COEFF[29] = 988.0 ; 
	FirFilter_4007_s.COEFF[30] = 1021.0 ; 
	FirFilter_4007_s.COEFF[31] = 1054.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin46_FirFilter_Fiss_4099_4133_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin46_FirFilter_Fiss_4099_4133_split[1], i) * FirFilter_4007_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin46_FirFilter_Fiss_4099_4133_split[1]) ; 
		push_float(&SplitJoin46_FirFilter_Fiss_4099_4133_join[1], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4005
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4005DownSamp_3898, pop_float(&SplitJoin46_FirFilter_Fiss_4099_4133_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4005DownSamp_3898, pop_float(&SplitJoin46_FirFilter_Fiss_4099_4133_join[1]));
	ENDFOR
//--------------------------------
// --- init: DownSamp_3898
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		push_float(&DownSamp_3898UpSamp_3899, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4005DownSamp_3898)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4005DownSamp_3898) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: UpSamp_3899
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		push_float(&UpSamp_3899WEIGHTED_ROUND_ROBIN_Splitter_4008, pop_float(&DownSamp_3898UpSamp_3899)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			push_float(&UpSamp_3899WEIGHTED_ROUND_ROBIN_Splitter_4008, 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4008
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++)
		push_float(&SplitJoin48_Delay_N_Fiss_4100_4134_split[0], pop_float(&UpSamp_3899WEIGHTED_ROUND_ROBIN_Splitter_4008));
		push_float(&SplitJoin48_Delay_N_Fiss_4100_4134_split[1], pop_float(&UpSamp_3899WEIGHTED_ROUND_ROBIN_Splitter_4008));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_4010
	FOR(uint32_t, __iter_init_, 0, <, 20, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin48_Delay_N_Fiss_4100_4134_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_4011
	FOR(uint32_t, __iter_init_, 0, <, 20, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin48_Delay_N_Fiss_4100_4134_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4009
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4009DUPLICATE_Splitter_4012, pop_float(&SplitJoin48_Delay_N_Fiss_4100_4134_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4009DUPLICATE_Splitter_4012, pop_float(&SplitJoin48_Delay_N_Fiss_4100_4134_join[1]));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_4012
	FOR(uint32_t, __iter_init_, 0, <, 37, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4009DUPLICATE_Splitter_4012);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin50_FirFilter_Fiss_4101_4135_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_4014
	 {
	FirFilter_4014_s.COEFF[0] = 12.0 ; 
	FirFilter_4014_s.COEFF[1] = 16.0 ; 
	FirFilter_4014_s.COEFF[2] = 20.0 ; 
	FirFilter_4014_s.COEFF[3] = 24.0 ; 
	FirFilter_4014_s.COEFF[4] = 28.0 ; 
	FirFilter_4014_s.COEFF[5] = 32.0 ; 
	FirFilter_4014_s.COEFF[6] = 36.0 ; 
	FirFilter_4014_s.COEFF[7] = 40.0 ; 
	FirFilter_4014_s.COEFF[8] = 44.0 ; 
	FirFilter_4014_s.COEFF[9] = 48.0 ; 
	FirFilter_4014_s.COEFF[10] = 52.0 ; 
	FirFilter_4014_s.COEFF[11] = 56.0 ; 
	FirFilter_4014_s.COEFF[12] = 60.0 ; 
	FirFilter_4014_s.COEFF[13] = 64.0 ; 
	FirFilter_4014_s.COEFF[14] = 68.0 ; 
	FirFilter_4014_s.COEFF[15] = 72.0 ; 
	FirFilter_4014_s.COEFF[16] = 76.0 ; 
	FirFilter_4014_s.COEFF[17] = 80.0 ; 
	FirFilter_4014_s.COEFF[18] = 84.0 ; 
	FirFilter_4014_s.COEFF[19] = 88.0 ; 
	FirFilter_4014_s.COEFF[20] = 92.0 ; 
	FirFilter_4014_s.COEFF[21] = 96.0 ; 
	FirFilter_4014_s.COEFF[22] = 100.0 ; 
	FirFilter_4014_s.COEFF[23] = 104.0 ; 
	FirFilter_4014_s.COEFF[24] = 108.0 ; 
	FirFilter_4014_s.COEFF[25] = 112.0 ; 
	FirFilter_4014_s.COEFF[26] = 116.0 ; 
	FirFilter_4014_s.COEFF[27] = 120.0 ; 
	FirFilter_4014_s.COEFF[28] = 124.0 ; 
	FirFilter_4014_s.COEFF[29] = 128.0 ; 
	FirFilter_4014_s.COEFF[30] = 132.0 ; 
	FirFilter_4014_s.COEFF[31] = 136.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin50_FirFilter_Fiss_4101_4135_split[0], i) * FirFilter_4014_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin50_FirFilter_Fiss_4101_4135_split[0]) ; 
		push_float(&SplitJoin50_FirFilter_Fiss_4101_4135_join[0], sum) ; 
 {
		pop_void(&SplitJoin50_FirFilter_Fiss_4101_4135_split[0]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_4015
	 {
	FirFilter_4015_s.COEFF[0] = 12.0 ; 
	FirFilter_4015_s.COEFF[1] = 16.0 ; 
	FirFilter_4015_s.COEFF[2] = 20.0 ; 
	FirFilter_4015_s.COEFF[3] = 24.0 ; 
	FirFilter_4015_s.COEFF[4] = 28.0 ; 
	FirFilter_4015_s.COEFF[5] = 32.0 ; 
	FirFilter_4015_s.COEFF[6] = 36.0 ; 
	FirFilter_4015_s.COEFF[7] = 40.0 ; 
	FirFilter_4015_s.COEFF[8] = 44.0 ; 
	FirFilter_4015_s.COEFF[9] = 48.0 ; 
	FirFilter_4015_s.COEFF[10] = 52.0 ; 
	FirFilter_4015_s.COEFF[11] = 56.0 ; 
	FirFilter_4015_s.COEFF[12] = 60.0 ; 
	FirFilter_4015_s.COEFF[13] = 64.0 ; 
	FirFilter_4015_s.COEFF[14] = 68.0 ; 
	FirFilter_4015_s.COEFF[15] = 72.0 ; 
	FirFilter_4015_s.COEFF[16] = 76.0 ; 
	FirFilter_4015_s.COEFF[17] = 80.0 ; 
	FirFilter_4015_s.COEFF[18] = 84.0 ; 
	FirFilter_4015_s.COEFF[19] = 88.0 ; 
	FirFilter_4015_s.COEFF[20] = 92.0 ; 
	FirFilter_4015_s.COEFF[21] = 96.0 ; 
	FirFilter_4015_s.COEFF[22] = 100.0 ; 
	FirFilter_4015_s.COEFF[23] = 104.0 ; 
	FirFilter_4015_s.COEFF[24] = 108.0 ; 
	FirFilter_4015_s.COEFF[25] = 112.0 ; 
	FirFilter_4015_s.COEFF[26] = 116.0 ; 
	FirFilter_4015_s.COEFF[27] = 120.0 ; 
	FirFilter_4015_s.COEFF[28] = 124.0 ; 
	FirFilter_4015_s.COEFF[29] = 128.0 ; 
	FirFilter_4015_s.COEFF[30] = 132.0 ; 
	FirFilter_4015_s.COEFF[31] = 136.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin50_FirFilter_Fiss_4101_4135_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin50_FirFilter_Fiss_4101_4135_split[1], i) * FirFilter_4015_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin50_FirFilter_Fiss_4101_4135_split[1]) ; 
		push_float(&SplitJoin50_FirFilter_Fiss_4101_4135_join[1], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4013
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[3], pop_float(&SplitJoin50_FirFilter_Fiss_4101_4135_join[0]));
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[3], pop_float(&SplitJoin50_FirFilter_Fiss_4101_4135_join[1]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4016
	FOR(uint32_t, __iter_init_, 0, <, 93, __iter_init_++)
		push_float(&SplitJoin57_Delay_N_Fiss_4102_4136_split[0], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[4]));
		push_float(&SplitJoin57_Delay_N_Fiss_4102_4136_split[1], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[4]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_4018
	FOR(uint32_t, __iter_init_, 0, <, 65, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin57_Delay_N_Fiss_4102_4136_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_4019
	FOR(uint32_t, __iter_init_, 0, <, 65, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin57_Delay_N_Fiss_4102_4136_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4017
	FOR(uint32_t, __iter_init_, 0, <, 64, __iter_init_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4017DUPLICATE_Splitter_4020, pop_float(&SplitJoin57_Delay_N_Fiss_4102_4136_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4017DUPLICATE_Splitter_4020, pop_float(&SplitJoin57_Delay_N_Fiss_4102_4136_join[1]));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_4020
	FOR(uint32_t, __iter_init_, 0, <, 127, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4017DUPLICATE_Splitter_4020);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin59_FirFilter_Fiss_4103_4137_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_4022
	 {
	FirFilter_4022_s.COEFF[0] = 41.0 ; 
	FirFilter_4022_s.COEFF[1] = 74.0 ; 
	FirFilter_4022_s.COEFF[2] = 107.0 ; 
	FirFilter_4022_s.COEFF[3] = 140.0 ; 
	FirFilter_4022_s.COEFF[4] = 173.0 ; 
	FirFilter_4022_s.COEFF[5] = 206.0 ; 
	FirFilter_4022_s.COEFF[6] = 239.0 ; 
	FirFilter_4022_s.COEFF[7] = 272.0 ; 
	FirFilter_4022_s.COEFF[8] = 305.0 ; 
	FirFilter_4022_s.COEFF[9] = 338.0 ; 
	FirFilter_4022_s.COEFF[10] = 371.0 ; 
	FirFilter_4022_s.COEFF[11] = 404.0 ; 
	FirFilter_4022_s.COEFF[12] = 437.0 ; 
	FirFilter_4022_s.COEFF[13] = 470.0 ; 
	FirFilter_4022_s.COEFF[14] = 503.0 ; 
	FirFilter_4022_s.COEFF[15] = 536.0 ; 
	FirFilter_4022_s.COEFF[16] = 569.0 ; 
	FirFilter_4022_s.COEFF[17] = 602.0 ; 
	FirFilter_4022_s.COEFF[18] = 635.0 ; 
	FirFilter_4022_s.COEFF[19] = 668.0 ; 
	FirFilter_4022_s.COEFF[20] = 701.0 ; 
	FirFilter_4022_s.COEFF[21] = 734.0 ; 
	FirFilter_4022_s.COEFF[22] = 767.0 ; 
	FirFilter_4022_s.COEFF[23] = 800.0 ; 
	FirFilter_4022_s.COEFF[24] = 833.0 ; 
	FirFilter_4022_s.COEFF[25] = 866.0 ; 
	FirFilter_4022_s.COEFF[26] = 899.0 ; 
	FirFilter_4022_s.COEFF[27] = 932.0 ; 
	FirFilter_4022_s.COEFF[28] = 965.0 ; 
	FirFilter_4022_s.COEFF[29] = 998.0 ; 
	FirFilter_4022_s.COEFF[30] = 1031.0 ; 
	FirFilter_4022_s.COEFF[31] = 1064.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin59_FirFilter_Fiss_4103_4137_split[0], i) * FirFilter_4022_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin59_FirFilter_Fiss_4103_4137_split[0]) ; 
		push_float(&SplitJoin59_FirFilter_Fiss_4103_4137_join[0], sum) ; 
 {
		pop_void(&SplitJoin59_FirFilter_Fiss_4103_4137_split[0]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_4023
	 {
	FirFilter_4023_s.COEFF[0] = 41.0 ; 
	FirFilter_4023_s.COEFF[1] = 74.0 ; 
	FirFilter_4023_s.COEFF[2] = 107.0 ; 
	FirFilter_4023_s.COEFF[3] = 140.0 ; 
	FirFilter_4023_s.COEFF[4] = 173.0 ; 
	FirFilter_4023_s.COEFF[5] = 206.0 ; 
	FirFilter_4023_s.COEFF[6] = 239.0 ; 
	FirFilter_4023_s.COEFF[7] = 272.0 ; 
	FirFilter_4023_s.COEFF[8] = 305.0 ; 
	FirFilter_4023_s.COEFF[9] = 338.0 ; 
	FirFilter_4023_s.COEFF[10] = 371.0 ; 
	FirFilter_4023_s.COEFF[11] = 404.0 ; 
	FirFilter_4023_s.COEFF[12] = 437.0 ; 
	FirFilter_4023_s.COEFF[13] = 470.0 ; 
	FirFilter_4023_s.COEFF[14] = 503.0 ; 
	FirFilter_4023_s.COEFF[15] = 536.0 ; 
	FirFilter_4023_s.COEFF[16] = 569.0 ; 
	FirFilter_4023_s.COEFF[17] = 602.0 ; 
	FirFilter_4023_s.COEFF[18] = 635.0 ; 
	FirFilter_4023_s.COEFF[19] = 668.0 ; 
	FirFilter_4023_s.COEFF[20] = 701.0 ; 
	FirFilter_4023_s.COEFF[21] = 734.0 ; 
	FirFilter_4023_s.COEFF[22] = 767.0 ; 
	FirFilter_4023_s.COEFF[23] = 800.0 ; 
	FirFilter_4023_s.COEFF[24] = 833.0 ; 
	FirFilter_4023_s.COEFF[25] = 866.0 ; 
	FirFilter_4023_s.COEFF[26] = 899.0 ; 
	FirFilter_4023_s.COEFF[27] = 932.0 ; 
	FirFilter_4023_s.COEFF[28] = 965.0 ; 
	FirFilter_4023_s.COEFF[29] = 998.0 ; 
	FirFilter_4023_s.COEFF[30] = 1031.0 ; 
	FirFilter_4023_s.COEFF[31] = 1064.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin59_FirFilter_Fiss_4103_4137_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin59_FirFilter_Fiss_4103_4137_split[1], i) * FirFilter_4023_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin59_FirFilter_Fiss_4103_4137_split[1]) ; 
		push_float(&SplitJoin59_FirFilter_Fiss_4103_4137_join[1], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4021
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4021DownSamp_3905, pop_float(&SplitJoin59_FirFilter_Fiss_4103_4137_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4021DownSamp_3905, pop_float(&SplitJoin59_FirFilter_Fiss_4103_4137_join[1]));
	ENDFOR
//--------------------------------
// --- init: DownSamp_3905
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		push_float(&DownSamp_3905UpSamp_3906, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4021DownSamp_3905)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4021DownSamp_3905) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: UpSamp_3906
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		push_float(&UpSamp_3906WEIGHTED_ROUND_ROBIN_Splitter_4024, pop_float(&DownSamp_3905UpSamp_3906)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			push_float(&UpSamp_3906WEIGHTED_ROUND_ROBIN_Splitter_4024, 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4024
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++)
		push_float(&SplitJoin61_Delay_N_Fiss_4104_4138_split[0], pop_float(&UpSamp_3906WEIGHTED_ROUND_ROBIN_Splitter_4024));
		push_float(&SplitJoin61_Delay_N_Fiss_4104_4138_split[1], pop_float(&UpSamp_3906WEIGHTED_ROUND_ROBIN_Splitter_4024));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_4026
	FOR(uint32_t, __iter_init_, 0, <, 20, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin61_Delay_N_Fiss_4104_4138_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_4027
	FOR(uint32_t, __iter_init_, 0, <, 20, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin61_Delay_N_Fiss_4104_4138_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4025
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4025DUPLICATE_Splitter_4028, pop_float(&SplitJoin61_Delay_N_Fiss_4104_4138_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4025DUPLICATE_Splitter_4028, pop_float(&SplitJoin61_Delay_N_Fiss_4104_4138_join[1]));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_4028
	FOR(uint32_t, __iter_init_, 0, <, 37, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4025DUPLICATE_Splitter_4028);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin63_FirFilter_Fiss_4105_4139_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_4030
	 {
	FirFilter_4030_s.COEFF[0] = 20.0 ; 
	FirFilter_4030_s.COEFF[1] = 25.0 ; 
	FirFilter_4030_s.COEFF[2] = 30.0 ; 
	FirFilter_4030_s.COEFF[3] = 35.0 ; 
	FirFilter_4030_s.COEFF[4] = 40.0 ; 
	FirFilter_4030_s.COEFF[5] = 45.0 ; 
	FirFilter_4030_s.COEFF[6] = 50.0 ; 
	FirFilter_4030_s.COEFF[7] = 55.0 ; 
	FirFilter_4030_s.COEFF[8] = 60.0 ; 
	FirFilter_4030_s.COEFF[9] = 65.0 ; 
	FirFilter_4030_s.COEFF[10] = 70.0 ; 
	FirFilter_4030_s.COEFF[11] = 75.0 ; 
	FirFilter_4030_s.COEFF[12] = 80.0 ; 
	FirFilter_4030_s.COEFF[13] = 85.0 ; 
	FirFilter_4030_s.COEFF[14] = 90.0 ; 
	FirFilter_4030_s.COEFF[15] = 95.0 ; 
	FirFilter_4030_s.COEFF[16] = 100.0 ; 
	FirFilter_4030_s.COEFF[17] = 105.0 ; 
	FirFilter_4030_s.COEFF[18] = 110.0 ; 
	FirFilter_4030_s.COEFF[19] = 115.0 ; 
	FirFilter_4030_s.COEFF[20] = 120.0 ; 
	FirFilter_4030_s.COEFF[21] = 125.0 ; 
	FirFilter_4030_s.COEFF[22] = 130.0 ; 
	FirFilter_4030_s.COEFF[23] = 135.0 ; 
	FirFilter_4030_s.COEFF[24] = 140.0 ; 
	FirFilter_4030_s.COEFF[25] = 145.0 ; 
	FirFilter_4030_s.COEFF[26] = 150.0 ; 
	FirFilter_4030_s.COEFF[27] = 155.0 ; 
	FirFilter_4030_s.COEFF[28] = 160.0 ; 
	FirFilter_4030_s.COEFF[29] = 165.0 ; 
	FirFilter_4030_s.COEFF[30] = 170.0 ; 
	FirFilter_4030_s.COEFF[31] = 175.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin63_FirFilter_Fiss_4105_4139_split[0], i) * FirFilter_4030_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin63_FirFilter_Fiss_4105_4139_split[0]) ; 
		push_float(&SplitJoin63_FirFilter_Fiss_4105_4139_join[0], sum) ; 
 {
		pop_void(&SplitJoin63_FirFilter_Fiss_4105_4139_split[0]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_4031
	 {
	FirFilter_4031_s.COEFF[0] = 20.0 ; 
	FirFilter_4031_s.COEFF[1] = 25.0 ; 
	FirFilter_4031_s.COEFF[2] = 30.0 ; 
	FirFilter_4031_s.COEFF[3] = 35.0 ; 
	FirFilter_4031_s.COEFF[4] = 40.0 ; 
	FirFilter_4031_s.COEFF[5] = 45.0 ; 
	FirFilter_4031_s.COEFF[6] = 50.0 ; 
	FirFilter_4031_s.COEFF[7] = 55.0 ; 
	FirFilter_4031_s.COEFF[8] = 60.0 ; 
	FirFilter_4031_s.COEFF[9] = 65.0 ; 
	FirFilter_4031_s.COEFF[10] = 70.0 ; 
	FirFilter_4031_s.COEFF[11] = 75.0 ; 
	FirFilter_4031_s.COEFF[12] = 80.0 ; 
	FirFilter_4031_s.COEFF[13] = 85.0 ; 
	FirFilter_4031_s.COEFF[14] = 90.0 ; 
	FirFilter_4031_s.COEFF[15] = 95.0 ; 
	FirFilter_4031_s.COEFF[16] = 100.0 ; 
	FirFilter_4031_s.COEFF[17] = 105.0 ; 
	FirFilter_4031_s.COEFF[18] = 110.0 ; 
	FirFilter_4031_s.COEFF[19] = 115.0 ; 
	FirFilter_4031_s.COEFF[20] = 120.0 ; 
	FirFilter_4031_s.COEFF[21] = 125.0 ; 
	FirFilter_4031_s.COEFF[22] = 130.0 ; 
	FirFilter_4031_s.COEFF[23] = 135.0 ; 
	FirFilter_4031_s.COEFF[24] = 140.0 ; 
	FirFilter_4031_s.COEFF[25] = 145.0 ; 
	FirFilter_4031_s.COEFF[26] = 150.0 ; 
	FirFilter_4031_s.COEFF[27] = 155.0 ; 
	FirFilter_4031_s.COEFF[28] = 160.0 ; 
	FirFilter_4031_s.COEFF[29] = 165.0 ; 
	FirFilter_4031_s.COEFF[30] = 170.0 ; 
	FirFilter_4031_s.COEFF[31] = 175.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin63_FirFilter_Fiss_4105_4139_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin63_FirFilter_Fiss_4105_4139_split[1], i) * FirFilter_4031_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin63_FirFilter_Fiss_4105_4139_split[1]) ; 
		push_float(&SplitJoin63_FirFilter_Fiss_4105_4139_join[1], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4029
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[4], pop_float(&SplitJoin63_FirFilter_Fiss_4105_4139_join[0]));
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[4], pop_float(&SplitJoin63_FirFilter_Fiss_4105_4139_join[1]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4032
	FOR(uint32_t, __iter_init_, 0, <, 93, __iter_init_++)
		push_float(&SplitJoin70_Delay_N_Fiss_4106_4140_split[0], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[5]));
		push_float(&SplitJoin70_Delay_N_Fiss_4106_4140_split[1], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[5]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_4034
	FOR(uint32_t, __iter_init_, 0, <, 65, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin70_Delay_N_Fiss_4106_4140_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_4035
	FOR(uint32_t, __iter_init_, 0, <, 65, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin70_Delay_N_Fiss_4106_4140_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4033
	FOR(uint32_t, __iter_init_, 0, <, 64, __iter_init_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4033DUPLICATE_Splitter_4036, pop_float(&SplitJoin70_Delay_N_Fiss_4106_4140_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4033DUPLICATE_Splitter_4036, pop_float(&SplitJoin70_Delay_N_Fiss_4106_4140_join[1]));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_4036
	FOR(uint32_t, __iter_init_, 0, <, 127, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4033DUPLICATE_Splitter_4036);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin72_FirFilter_Fiss_4107_4141_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_4038
	 {
	FirFilter_4038_s.COEFF[0] = 51.0 ; 
	FirFilter_4038_s.COEFF[1] = 84.0 ; 
	FirFilter_4038_s.COEFF[2] = 117.0 ; 
	FirFilter_4038_s.COEFF[3] = 150.0 ; 
	FirFilter_4038_s.COEFF[4] = 183.0 ; 
	FirFilter_4038_s.COEFF[5] = 216.0 ; 
	FirFilter_4038_s.COEFF[6] = 249.0 ; 
	FirFilter_4038_s.COEFF[7] = 282.0 ; 
	FirFilter_4038_s.COEFF[8] = 315.0 ; 
	FirFilter_4038_s.COEFF[9] = 348.0 ; 
	FirFilter_4038_s.COEFF[10] = 381.0 ; 
	FirFilter_4038_s.COEFF[11] = 414.0 ; 
	FirFilter_4038_s.COEFF[12] = 447.0 ; 
	FirFilter_4038_s.COEFF[13] = 480.0 ; 
	FirFilter_4038_s.COEFF[14] = 513.0 ; 
	FirFilter_4038_s.COEFF[15] = 546.0 ; 
	FirFilter_4038_s.COEFF[16] = 579.0 ; 
	FirFilter_4038_s.COEFF[17] = 612.0 ; 
	FirFilter_4038_s.COEFF[18] = 645.0 ; 
	FirFilter_4038_s.COEFF[19] = 678.0 ; 
	FirFilter_4038_s.COEFF[20] = 711.0 ; 
	FirFilter_4038_s.COEFF[21] = 744.0 ; 
	FirFilter_4038_s.COEFF[22] = 777.0 ; 
	FirFilter_4038_s.COEFF[23] = 810.0 ; 
	FirFilter_4038_s.COEFF[24] = 843.0 ; 
	FirFilter_4038_s.COEFF[25] = 876.0 ; 
	FirFilter_4038_s.COEFF[26] = 909.0 ; 
	FirFilter_4038_s.COEFF[27] = 942.0 ; 
	FirFilter_4038_s.COEFF[28] = 975.0 ; 
	FirFilter_4038_s.COEFF[29] = 1008.0 ; 
	FirFilter_4038_s.COEFF[30] = 1041.0 ; 
	FirFilter_4038_s.COEFF[31] = 1074.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin72_FirFilter_Fiss_4107_4141_split[0], i) * FirFilter_4038_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin72_FirFilter_Fiss_4107_4141_split[0]) ; 
		push_float(&SplitJoin72_FirFilter_Fiss_4107_4141_join[0], sum) ; 
 {
		pop_void(&SplitJoin72_FirFilter_Fiss_4107_4141_split[0]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_4039
	 {
	FirFilter_4039_s.COEFF[0] = 51.0 ; 
	FirFilter_4039_s.COEFF[1] = 84.0 ; 
	FirFilter_4039_s.COEFF[2] = 117.0 ; 
	FirFilter_4039_s.COEFF[3] = 150.0 ; 
	FirFilter_4039_s.COEFF[4] = 183.0 ; 
	FirFilter_4039_s.COEFF[5] = 216.0 ; 
	FirFilter_4039_s.COEFF[6] = 249.0 ; 
	FirFilter_4039_s.COEFF[7] = 282.0 ; 
	FirFilter_4039_s.COEFF[8] = 315.0 ; 
	FirFilter_4039_s.COEFF[9] = 348.0 ; 
	FirFilter_4039_s.COEFF[10] = 381.0 ; 
	FirFilter_4039_s.COEFF[11] = 414.0 ; 
	FirFilter_4039_s.COEFF[12] = 447.0 ; 
	FirFilter_4039_s.COEFF[13] = 480.0 ; 
	FirFilter_4039_s.COEFF[14] = 513.0 ; 
	FirFilter_4039_s.COEFF[15] = 546.0 ; 
	FirFilter_4039_s.COEFF[16] = 579.0 ; 
	FirFilter_4039_s.COEFF[17] = 612.0 ; 
	FirFilter_4039_s.COEFF[18] = 645.0 ; 
	FirFilter_4039_s.COEFF[19] = 678.0 ; 
	FirFilter_4039_s.COEFF[20] = 711.0 ; 
	FirFilter_4039_s.COEFF[21] = 744.0 ; 
	FirFilter_4039_s.COEFF[22] = 777.0 ; 
	FirFilter_4039_s.COEFF[23] = 810.0 ; 
	FirFilter_4039_s.COEFF[24] = 843.0 ; 
	FirFilter_4039_s.COEFF[25] = 876.0 ; 
	FirFilter_4039_s.COEFF[26] = 909.0 ; 
	FirFilter_4039_s.COEFF[27] = 942.0 ; 
	FirFilter_4039_s.COEFF[28] = 975.0 ; 
	FirFilter_4039_s.COEFF[29] = 1008.0 ; 
	FirFilter_4039_s.COEFF[30] = 1041.0 ; 
	FirFilter_4039_s.COEFF[31] = 1074.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin72_FirFilter_Fiss_4107_4141_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin72_FirFilter_Fiss_4107_4141_split[1], i) * FirFilter_4039_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin72_FirFilter_Fiss_4107_4141_split[1]) ; 
		push_float(&SplitJoin72_FirFilter_Fiss_4107_4141_join[1], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4037
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4037DownSamp_3912, pop_float(&SplitJoin72_FirFilter_Fiss_4107_4141_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4037DownSamp_3912, pop_float(&SplitJoin72_FirFilter_Fiss_4107_4141_join[1]));
	ENDFOR
//--------------------------------
// --- init: DownSamp_3912
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		push_float(&DownSamp_3912UpSamp_3913, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4037DownSamp_3912)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4037DownSamp_3912) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: UpSamp_3913
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		push_float(&UpSamp_3913WEIGHTED_ROUND_ROBIN_Splitter_4040, pop_float(&DownSamp_3912UpSamp_3913)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			push_float(&UpSamp_3913WEIGHTED_ROUND_ROBIN_Splitter_4040, 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4040
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++)
		push_float(&SplitJoin74_Delay_N_Fiss_4108_4142_split[0], pop_float(&UpSamp_3913WEIGHTED_ROUND_ROBIN_Splitter_4040));
		push_float(&SplitJoin74_Delay_N_Fiss_4108_4142_split[1], pop_float(&UpSamp_3913WEIGHTED_ROUND_ROBIN_Splitter_4040));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_4042
	FOR(uint32_t, __iter_init_, 0, <, 20, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin74_Delay_N_Fiss_4108_4142_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_4043
	FOR(uint32_t, __iter_init_, 0, <, 20, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin74_Delay_N_Fiss_4108_4142_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4041
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4041DUPLICATE_Splitter_4044, pop_float(&SplitJoin74_Delay_N_Fiss_4108_4142_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4041DUPLICATE_Splitter_4044, pop_float(&SplitJoin74_Delay_N_Fiss_4108_4142_join[1]));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_4044
	FOR(uint32_t, __iter_init_, 0, <, 37, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4041DUPLICATE_Splitter_4044);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin76_FirFilter_Fiss_4109_4143_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_4046
	 {
	FirFilter_4046_s.COEFF[0] = 30.0 ; 
	FirFilter_4046_s.COEFF[1] = 36.0 ; 
	FirFilter_4046_s.COEFF[2] = 42.0 ; 
	FirFilter_4046_s.COEFF[3] = 48.0 ; 
	FirFilter_4046_s.COEFF[4] = 54.0 ; 
	FirFilter_4046_s.COEFF[5] = 60.0 ; 
	FirFilter_4046_s.COEFF[6] = 66.0 ; 
	FirFilter_4046_s.COEFF[7] = 72.0 ; 
	FirFilter_4046_s.COEFF[8] = 78.0 ; 
	FirFilter_4046_s.COEFF[9] = 84.0 ; 
	FirFilter_4046_s.COEFF[10] = 90.0 ; 
	FirFilter_4046_s.COEFF[11] = 96.0 ; 
	FirFilter_4046_s.COEFF[12] = 102.0 ; 
	FirFilter_4046_s.COEFF[13] = 108.0 ; 
	FirFilter_4046_s.COEFF[14] = 114.0 ; 
	FirFilter_4046_s.COEFF[15] = 120.0 ; 
	FirFilter_4046_s.COEFF[16] = 126.0 ; 
	FirFilter_4046_s.COEFF[17] = 132.0 ; 
	FirFilter_4046_s.COEFF[18] = 138.0 ; 
	FirFilter_4046_s.COEFF[19] = 144.0 ; 
	FirFilter_4046_s.COEFF[20] = 150.0 ; 
	FirFilter_4046_s.COEFF[21] = 156.0 ; 
	FirFilter_4046_s.COEFF[22] = 162.0 ; 
	FirFilter_4046_s.COEFF[23] = 168.0 ; 
	FirFilter_4046_s.COEFF[24] = 174.0 ; 
	FirFilter_4046_s.COEFF[25] = 180.0 ; 
	FirFilter_4046_s.COEFF[26] = 186.0 ; 
	FirFilter_4046_s.COEFF[27] = 192.0 ; 
	FirFilter_4046_s.COEFF[28] = 198.0 ; 
	FirFilter_4046_s.COEFF[29] = 204.0 ; 
	FirFilter_4046_s.COEFF[30] = 210.0 ; 
	FirFilter_4046_s.COEFF[31] = 216.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin76_FirFilter_Fiss_4109_4143_split[0], i) * FirFilter_4046_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin76_FirFilter_Fiss_4109_4143_split[0]) ; 
		push_float(&SplitJoin76_FirFilter_Fiss_4109_4143_join[0], sum) ; 
 {
		pop_void(&SplitJoin76_FirFilter_Fiss_4109_4143_split[0]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_4047
	 {
	FirFilter_4047_s.COEFF[0] = 30.0 ; 
	FirFilter_4047_s.COEFF[1] = 36.0 ; 
	FirFilter_4047_s.COEFF[2] = 42.0 ; 
	FirFilter_4047_s.COEFF[3] = 48.0 ; 
	FirFilter_4047_s.COEFF[4] = 54.0 ; 
	FirFilter_4047_s.COEFF[5] = 60.0 ; 
	FirFilter_4047_s.COEFF[6] = 66.0 ; 
	FirFilter_4047_s.COEFF[7] = 72.0 ; 
	FirFilter_4047_s.COEFF[8] = 78.0 ; 
	FirFilter_4047_s.COEFF[9] = 84.0 ; 
	FirFilter_4047_s.COEFF[10] = 90.0 ; 
	FirFilter_4047_s.COEFF[11] = 96.0 ; 
	FirFilter_4047_s.COEFF[12] = 102.0 ; 
	FirFilter_4047_s.COEFF[13] = 108.0 ; 
	FirFilter_4047_s.COEFF[14] = 114.0 ; 
	FirFilter_4047_s.COEFF[15] = 120.0 ; 
	FirFilter_4047_s.COEFF[16] = 126.0 ; 
	FirFilter_4047_s.COEFF[17] = 132.0 ; 
	FirFilter_4047_s.COEFF[18] = 138.0 ; 
	FirFilter_4047_s.COEFF[19] = 144.0 ; 
	FirFilter_4047_s.COEFF[20] = 150.0 ; 
	FirFilter_4047_s.COEFF[21] = 156.0 ; 
	FirFilter_4047_s.COEFF[22] = 162.0 ; 
	FirFilter_4047_s.COEFF[23] = 168.0 ; 
	FirFilter_4047_s.COEFF[24] = 174.0 ; 
	FirFilter_4047_s.COEFF[25] = 180.0 ; 
	FirFilter_4047_s.COEFF[26] = 186.0 ; 
	FirFilter_4047_s.COEFF[27] = 192.0 ; 
	FirFilter_4047_s.COEFF[28] = 198.0 ; 
	FirFilter_4047_s.COEFF[29] = 204.0 ; 
	FirFilter_4047_s.COEFF[30] = 210.0 ; 
	FirFilter_4047_s.COEFF[31] = 216.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin76_FirFilter_Fiss_4109_4143_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin76_FirFilter_Fiss_4109_4143_split[1], i) * FirFilter_4047_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin76_FirFilter_Fiss_4109_4143_split[1]) ; 
		push_float(&SplitJoin76_FirFilter_Fiss_4109_4143_join[1], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4045
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[5], pop_float(&SplitJoin76_FirFilter_Fiss_4109_4143_join[0]));
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[5], pop_float(&SplitJoin76_FirFilter_Fiss_4109_4143_join[1]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4048
	FOR(uint32_t, __iter_init_, 0, <, 93, __iter_init_++)
		push_float(&SplitJoin83_Delay_N_Fiss_4110_4144_split[0], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[6]));
		push_float(&SplitJoin83_Delay_N_Fiss_4110_4144_split[1], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[6]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_4050
	FOR(uint32_t, __iter_init_, 0, <, 65, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin83_Delay_N_Fiss_4110_4144_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_4051
	FOR(uint32_t, __iter_init_, 0, <, 65, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin83_Delay_N_Fiss_4110_4144_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4049
	FOR(uint32_t, __iter_init_, 0, <, 64, __iter_init_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4049DUPLICATE_Splitter_4052, pop_float(&SplitJoin83_Delay_N_Fiss_4110_4144_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4049DUPLICATE_Splitter_4052, pop_float(&SplitJoin83_Delay_N_Fiss_4110_4144_join[1]));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_4052
	FOR(uint32_t, __iter_init_, 0, <, 127, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4049DUPLICATE_Splitter_4052);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin85_FirFilter_Fiss_4111_4145_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_4054
	 {
	FirFilter_4054_s.COEFF[0] = 61.0 ; 
	FirFilter_4054_s.COEFF[1] = 94.0 ; 
	FirFilter_4054_s.COEFF[2] = 127.0 ; 
	FirFilter_4054_s.COEFF[3] = 160.0 ; 
	FirFilter_4054_s.COEFF[4] = 193.0 ; 
	FirFilter_4054_s.COEFF[5] = 226.0 ; 
	FirFilter_4054_s.COEFF[6] = 259.0 ; 
	FirFilter_4054_s.COEFF[7] = 292.0 ; 
	FirFilter_4054_s.COEFF[8] = 325.0 ; 
	FirFilter_4054_s.COEFF[9] = 358.0 ; 
	FirFilter_4054_s.COEFF[10] = 391.0 ; 
	FirFilter_4054_s.COEFF[11] = 424.0 ; 
	FirFilter_4054_s.COEFF[12] = 457.0 ; 
	FirFilter_4054_s.COEFF[13] = 490.0 ; 
	FirFilter_4054_s.COEFF[14] = 523.0 ; 
	FirFilter_4054_s.COEFF[15] = 556.0 ; 
	FirFilter_4054_s.COEFF[16] = 589.0 ; 
	FirFilter_4054_s.COEFF[17] = 622.0 ; 
	FirFilter_4054_s.COEFF[18] = 655.0 ; 
	FirFilter_4054_s.COEFF[19] = 688.0 ; 
	FirFilter_4054_s.COEFF[20] = 721.0 ; 
	FirFilter_4054_s.COEFF[21] = 754.0 ; 
	FirFilter_4054_s.COEFF[22] = 787.0 ; 
	FirFilter_4054_s.COEFF[23] = 820.0 ; 
	FirFilter_4054_s.COEFF[24] = 853.0 ; 
	FirFilter_4054_s.COEFF[25] = 886.0 ; 
	FirFilter_4054_s.COEFF[26] = 919.0 ; 
	FirFilter_4054_s.COEFF[27] = 952.0 ; 
	FirFilter_4054_s.COEFF[28] = 985.0 ; 
	FirFilter_4054_s.COEFF[29] = 1018.0 ; 
	FirFilter_4054_s.COEFF[30] = 1051.0 ; 
	FirFilter_4054_s.COEFF[31] = 1084.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin85_FirFilter_Fiss_4111_4145_split[0], i) * FirFilter_4054_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin85_FirFilter_Fiss_4111_4145_split[0]) ; 
		push_float(&SplitJoin85_FirFilter_Fiss_4111_4145_join[0], sum) ; 
 {
		pop_void(&SplitJoin85_FirFilter_Fiss_4111_4145_split[0]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_4055
	 {
	FirFilter_4055_s.COEFF[0] = 61.0 ; 
	FirFilter_4055_s.COEFF[1] = 94.0 ; 
	FirFilter_4055_s.COEFF[2] = 127.0 ; 
	FirFilter_4055_s.COEFF[3] = 160.0 ; 
	FirFilter_4055_s.COEFF[4] = 193.0 ; 
	FirFilter_4055_s.COEFF[5] = 226.0 ; 
	FirFilter_4055_s.COEFF[6] = 259.0 ; 
	FirFilter_4055_s.COEFF[7] = 292.0 ; 
	FirFilter_4055_s.COEFF[8] = 325.0 ; 
	FirFilter_4055_s.COEFF[9] = 358.0 ; 
	FirFilter_4055_s.COEFF[10] = 391.0 ; 
	FirFilter_4055_s.COEFF[11] = 424.0 ; 
	FirFilter_4055_s.COEFF[12] = 457.0 ; 
	FirFilter_4055_s.COEFF[13] = 490.0 ; 
	FirFilter_4055_s.COEFF[14] = 523.0 ; 
	FirFilter_4055_s.COEFF[15] = 556.0 ; 
	FirFilter_4055_s.COEFF[16] = 589.0 ; 
	FirFilter_4055_s.COEFF[17] = 622.0 ; 
	FirFilter_4055_s.COEFF[18] = 655.0 ; 
	FirFilter_4055_s.COEFF[19] = 688.0 ; 
	FirFilter_4055_s.COEFF[20] = 721.0 ; 
	FirFilter_4055_s.COEFF[21] = 754.0 ; 
	FirFilter_4055_s.COEFF[22] = 787.0 ; 
	FirFilter_4055_s.COEFF[23] = 820.0 ; 
	FirFilter_4055_s.COEFF[24] = 853.0 ; 
	FirFilter_4055_s.COEFF[25] = 886.0 ; 
	FirFilter_4055_s.COEFF[26] = 919.0 ; 
	FirFilter_4055_s.COEFF[27] = 952.0 ; 
	FirFilter_4055_s.COEFF[28] = 985.0 ; 
	FirFilter_4055_s.COEFF[29] = 1018.0 ; 
	FirFilter_4055_s.COEFF[30] = 1051.0 ; 
	FirFilter_4055_s.COEFF[31] = 1084.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin85_FirFilter_Fiss_4111_4145_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin85_FirFilter_Fiss_4111_4145_split[1], i) * FirFilter_4055_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin85_FirFilter_Fiss_4111_4145_split[1]) ; 
		push_float(&SplitJoin85_FirFilter_Fiss_4111_4145_join[1], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4053
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4053DownSamp_3919, pop_float(&SplitJoin85_FirFilter_Fiss_4111_4145_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4053DownSamp_3919, pop_float(&SplitJoin85_FirFilter_Fiss_4111_4145_join[1]));
	ENDFOR
//--------------------------------
// --- init: DownSamp_3919
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		push_float(&DownSamp_3919UpSamp_3920, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4053DownSamp_3919)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4053DownSamp_3919) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: UpSamp_3920
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		push_float(&UpSamp_3920WEIGHTED_ROUND_ROBIN_Splitter_4056, pop_float(&DownSamp_3919UpSamp_3920)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			push_float(&UpSamp_3920WEIGHTED_ROUND_ROBIN_Splitter_4056, 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4056
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++)
		push_float(&SplitJoin87_Delay_N_Fiss_4112_4146_split[0], pop_float(&UpSamp_3920WEIGHTED_ROUND_ROBIN_Splitter_4056));
		push_float(&SplitJoin87_Delay_N_Fiss_4112_4146_split[1], pop_float(&UpSamp_3920WEIGHTED_ROUND_ROBIN_Splitter_4056));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_4058
	FOR(uint32_t, __iter_init_, 0, <, 20, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin87_Delay_N_Fiss_4112_4146_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_4059
	FOR(uint32_t, __iter_init_, 0, <, 20, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin87_Delay_N_Fiss_4112_4146_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4057
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4057DUPLICATE_Splitter_4060, pop_float(&SplitJoin87_Delay_N_Fiss_4112_4146_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4057DUPLICATE_Splitter_4060, pop_float(&SplitJoin87_Delay_N_Fiss_4112_4146_join[1]));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_4060
	FOR(uint32_t, __iter_init_, 0, <, 37, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4057DUPLICATE_Splitter_4060);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin89_FirFilter_Fiss_4113_4147_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_4062
	 {
	FirFilter_4062_s.COEFF[0] = 42.0 ; 
	FirFilter_4062_s.COEFF[1] = 49.0 ; 
	FirFilter_4062_s.COEFF[2] = 56.0 ; 
	FirFilter_4062_s.COEFF[3] = 63.0 ; 
	FirFilter_4062_s.COEFF[4] = 70.0 ; 
	FirFilter_4062_s.COEFF[5] = 77.0 ; 
	FirFilter_4062_s.COEFF[6] = 84.0 ; 
	FirFilter_4062_s.COEFF[7] = 91.0 ; 
	FirFilter_4062_s.COEFF[8] = 98.0 ; 
	FirFilter_4062_s.COEFF[9] = 105.0 ; 
	FirFilter_4062_s.COEFF[10] = 112.0 ; 
	FirFilter_4062_s.COEFF[11] = 119.0 ; 
	FirFilter_4062_s.COEFF[12] = 126.0 ; 
	FirFilter_4062_s.COEFF[13] = 133.0 ; 
	FirFilter_4062_s.COEFF[14] = 140.0 ; 
	FirFilter_4062_s.COEFF[15] = 147.0 ; 
	FirFilter_4062_s.COEFF[16] = 154.0 ; 
	FirFilter_4062_s.COEFF[17] = 161.0 ; 
	FirFilter_4062_s.COEFF[18] = 168.0 ; 
	FirFilter_4062_s.COEFF[19] = 175.0 ; 
	FirFilter_4062_s.COEFF[20] = 182.0 ; 
	FirFilter_4062_s.COEFF[21] = 189.0 ; 
	FirFilter_4062_s.COEFF[22] = 196.0 ; 
	FirFilter_4062_s.COEFF[23] = 203.0 ; 
	FirFilter_4062_s.COEFF[24] = 210.0 ; 
	FirFilter_4062_s.COEFF[25] = 217.0 ; 
	FirFilter_4062_s.COEFF[26] = 224.0 ; 
	FirFilter_4062_s.COEFF[27] = 231.0 ; 
	FirFilter_4062_s.COEFF[28] = 238.0 ; 
	FirFilter_4062_s.COEFF[29] = 245.0 ; 
	FirFilter_4062_s.COEFF[30] = 252.0 ; 
	FirFilter_4062_s.COEFF[31] = 259.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin89_FirFilter_Fiss_4113_4147_split[0], i) * FirFilter_4062_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin89_FirFilter_Fiss_4113_4147_split[0]) ; 
		push_float(&SplitJoin89_FirFilter_Fiss_4113_4147_join[0], sum) ; 
 {
		pop_void(&SplitJoin89_FirFilter_Fiss_4113_4147_split[0]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_4063
	 {
	FirFilter_4063_s.COEFF[0] = 42.0 ; 
	FirFilter_4063_s.COEFF[1] = 49.0 ; 
	FirFilter_4063_s.COEFF[2] = 56.0 ; 
	FirFilter_4063_s.COEFF[3] = 63.0 ; 
	FirFilter_4063_s.COEFF[4] = 70.0 ; 
	FirFilter_4063_s.COEFF[5] = 77.0 ; 
	FirFilter_4063_s.COEFF[6] = 84.0 ; 
	FirFilter_4063_s.COEFF[7] = 91.0 ; 
	FirFilter_4063_s.COEFF[8] = 98.0 ; 
	FirFilter_4063_s.COEFF[9] = 105.0 ; 
	FirFilter_4063_s.COEFF[10] = 112.0 ; 
	FirFilter_4063_s.COEFF[11] = 119.0 ; 
	FirFilter_4063_s.COEFF[12] = 126.0 ; 
	FirFilter_4063_s.COEFF[13] = 133.0 ; 
	FirFilter_4063_s.COEFF[14] = 140.0 ; 
	FirFilter_4063_s.COEFF[15] = 147.0 ; 
	FirFilter_4063_s.COEFF[16] = 154.0 ; 
	FirFilter_4063_s.COEFF[17] = 161.0 ; 
	FirFilter_4063_s.COEFF[18] = 168.0 ; 
	FirFilter_4063_s.COEFF[19] = 175.0 ; 
	FirFilter_4063_s.COEFF[20] = 182.0 ; 
	FirFilter_4063_s.COEFF[21] = 189.0 ; 
	FirFilter_4063_s.COEFF[22] = 196.0 ; 
	FirFilter_4063_s.COEFF[23] = 203.0 ; 
	FirFilter_4063_s.COEFF[24] = 210.0 ; 
	FirFilter_4063_s.COEFF[25] = 217.0 ; 
	FirFilter_4063_s.COEFF[26] = 224.0 ; 
	FirFilter_4063_s.COEFF[27] = 231.0 ; 
	FirFilter_4063_s.COEFF[28] = 238.0 ; 
	FirFilter_4063_s.COEFF[29] = 245.0 ; 
	FirFilter_4063_s.COEFF[30] = 252.0 ; 
	FirFilter_4063_s.COEFF[31] = 259.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin89_FirFilter_Fiss_4113_4147_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin89_FirFilter_Fiss_4113_4147_split[1], i) * FirFilter_4063_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin89_FirFilter_Fiss_4113_4147_split[1]) ; 
		push_float(&SplitJoin89_FirFilter_Fiss_4113_4147_join[1], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4061
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[6], pop_float(&SplitJoin89_FirFilter_Fiss_4113_4147_join[0]));
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[6], pop_float(&SplitJoin89_FirFilter_Fiss_4113_4147_join[1]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4064
	FOR(uint32_t, __iter_init_, 0, <, 93, __iter_init_++)
		push_float(&SplitJoin96_Delay_N_Fiss_4114_4148_split[0], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[7]));
		push_float(&SplitJoin96_Delay_N_Fiss_4114_4148_split[1], pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_split[7]));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_4066
	FOR(uint32_t, __iter_init_, 0, <, 65, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin96_Delay_N_Fiss_4114_4148_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_4067
	FOR(uint32_t, __iter_init_, 0, <, 65, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin96_Delay_N_Fiss_4114_4148_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4065
	FOR(uint32_t, __iter_init_, 0, <, 64, __iter_init_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4065DUPLICATE_Splitter_4068, pop_float(&SplitJoin96_Delay_N_Fiss_4114_4148_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4065DUPLICATE_Splitter_4068, pop_float(&SplitJoin96_Delay_N_Fiss_4114_4148_join[1]));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_4068
	FOR(uint32_t, __iter_init_, 0, <, 127, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4065DUPLICATE_Splitter_4068);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin98_FirFilter_Fiss_4115_4149_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_4070
	 {
	FirFilter_4070_s.COEFF[0] = 71.0 ; 
	FirFilter_4070_s.COEFF[1] = 104.0 ; 
	FirFilter_4070_s.COEFF[2] = 137.0 ; 
	FirFilter_4070_s.COEFF[3] = 170.0 ; 
	FirFilter_4070_s.COEFF[4] = 203.0 ; 
	FirFilter_4070_s.COEFF[5] = 236.0 ; 
	FirFilter_4070_s.COEFF[6] = 269.0 ; 
	FirFilter_4070_s.COEFF[7] = 302.0 ; 
	FirFilter_4070_s.COEFF[8] = 335.0 ; 
	FirFilter_4070_s.COEFF[9] = 368.0 ; 
	FirFilter_4070_s.COEFF[10] = 401.0 ; 
	FirFilter_4070_s.COEFF[11] = 434.0 ; 
	FirFilter_4070_s.COEFF[12] = 467.0 ; 
	FirFilter_4070_s.COEFF[13] = 500.0 ; 
	FirFilter_4070_s.COEFF[14] = 533.0 ; 
	FirFilter_4070_s.COEFF[15] = 566.0 ; 
	FirFilter_4070_s.COEFF[16] = 599.0 ; 
	FirFilter_4070_s.COEFF[17] = 632.0 ; 
	FirFilter_4070_s.COEFF[18] = 665.0 ; 
	FirFilter_4070_s.COEFF[19] = 698.0 ; 
	FirFilter_4070_s.COEFF[20] = 731.0 ; 
	FirFilter_4070_s.COEFF[21] = 764.0 ; 
	FirFilter_4070_s.COEFF[22] = 797.0 ; 
	FirFilter_4070_s.COEFF[23] = 830.0 ; 
	FirFilter_4070_s.COEFF[24] = 863.0 ; 
	FirFilter_4070_s.COEFF[25] = 896.0 ; 
	FirFilter_4070_s.COEFF[26] = 929.0 ; 
	FirFilter_4070_s.COEFF[27] = 962.0 ; 
	FirFilter_4070_s.COEFF[28] = 995.0 ; 
	FirFilter_4070_s.COEFF[29] = 1028.0 ; 
	FirFilter_4070_s.COEFF[30] = 1061.0 ; 
	FirFilter_4070_s.COEFF[31] = 1094.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin98_FirFilter_Fiss_4115_4149_split[0], i) * FirFilter_4070_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin98_FirFilter_Fiss_4115_4149_split[0]) ; 
		push_float(&SplitJoin98_FirFilter_Fiss_4115_4149_join[0], sum) ; 
 {
		pop_void(&SplitJoin98_FirFilter_Fiss_4115_4149_split[0]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_4071
	 {
	FirFilter_4071_s.COEFF[0] = 71.0 ; 
	FirFilter_4071_s.COEFF[1] = 104.0 ; 
	FirFilter_4071_s.COEFF[2] = 137.0 ; 
	FirFilter_4071_s.COEFF[3] = 170.0 ; 
	FirFilter_4071_s.COEFF[4] = 203.0 ; 
	FirFilter_4071_s.COEFF[5] = 236.0 ; 
	FirFilter_4071_s.COEFF[6] = 269.0 ; 
	FirFilter_4071_s.COEFF[7] = 302.0 ; 
	FirFilter_4071_s.COEFF[8] = 335.0 ; 
	FirFilter_4071_s.COEFF[9] = 368.0 ; 
	FirFilter_4071_s.COEFF[10] = 401.0 ; 
	FirFilter_4071_s.COEFF[11] = 434.0 ; 
	FirFilter_4071_s.COEFF[12] = 467.0 ; 
	FirFilter_4071_s.COEFF[13] = 500.0 ; 
	FirFilter_4071_s.COEFF[14] = 533.0 ; 
	FirFilter_4071_s.COEFF[15] = 566.0 ; 
	FirFilter_4071_s.COEFF[16] = 599.0 ; 
	FirFilter_4071_s.COEFF[17] = 632.0 ; 
	FirFilter_4071_s.COEFF[18] = 665.0 ; 
	FirFilter_4071_s.COEFF[19] = 698.0 ; 
	FirFilter_4071_s.COEFF[20] = 731.0 ; 
	FirFilter_4071_s.COEFF[21] = 764.0 ; 
	FirFilter_4071_s.COEFF[22] = 797.0 ; 
	FirFilter_4071_s.COEFF[23] = 830.0 ; 
	FirFilter_4071_s.COEFF[24] = 863.0 ; 
	FirFilter_4071_s.COEFF[25] = 896.0 ; 
	FirFilter_4071_s.COEFF[26] = 929.0 ; 
	FirFilter_4071_s.COEFF[27] = 962.0 ; 
	FirFilter_4071_s.COEFF[28] = 995.0 ; 
	FirFilter_4071_s.COEFF[29] = 1028.0 ; 
	FirFilter_4071_s.COEFF[30] = 1061.0 ; 
	FirFilter_4071_s.COEFF[31] = 1094.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin98_FirFilter_Fiss_4115_4149_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin98_FirFilter_Fiss_4115_4149_split[1], i) * FirFilter_4071_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin98_FirFilter_Fiss_4115_4149_split[1]) ; 
		push_float(&SplitJoin98_FirFilter_Fiss_4115_4149_join[1], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4069
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4069DownSamp_3926, pop_float(&SplitJoin98_FirFilter_Fiss_4115_4149_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4069DownSamp_3926, pop_float(&SplitJoin98_FirFilter_Fiss_4115_4149_join[1]));
	ENDFOR
//--------------------------------
// --- init: DownSamp_3926
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		push_float(&DownSamp_3926UpSamp_3927, pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4069DownSamp_3926)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4069DownSamp_3926) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: UpSamp_3927
	FOR(uint32_t, __iter_init_, 0, <, 12, __iter_init_++) {
		push_float(&UpSamp_3927WEIGHTED_ROUND_ROBIN_Splitter_4072, pop_float(&DownSamp_3926UpSamp_3927)) ; 
		FOR(int, i, 0,  < , 7, i++) {
			push_float(&UpSamp_3927WEIGHTED_ROUND_ROBIN_Splitter_4072, 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4072
	FOR(uint32_t, __iter_init_, 0, <, 48, __iter_init_++)
		push_float(&SplitJoin100_Delay_N_Fiss_4116_4150_split[0], pop_float(&UpSamp_3927WEIGHTED_ROUND_ROBIN_Splitter_4072));
		push_float(&SplitJoin100_Delay_N_Fiss_4116_4150_split[1], pop_float(&UpSamp_3927WEIGHTED_ROUND_ROBIN_Splitter_4072));
	ENDFOR
//--------------------------------
// --- prework: Delay_N_4074
	FOR(uint32_t, __iter_init_, 0, <, 20, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin100_Delay_N_Fiss_4116_4150_join[0], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- prework: Delay_N_4075
	FOR(uint32_t, __iter_init_, 0, <, 20, __iter_init_++) {
		FOR(int, i, 0,  < , 31, i++) {
			push_float(&SplitJoin100_Delay_N_Fiss_4116_4150_join[1], 0.0) ; 
		}
		ENDFOR
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4073
	FOR(uint32_t, __iter_init_, 0, <, 19, __iter_init_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4073DUPLICATE_Splitter_4076, pop_float(&SplitJoin100_Delay_N_Fiss_4116_4150_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4073DUPLICATE_Splitter_4076, pop_float(&SplitJoin100_Delay_N_Fiss_4116_4150_join[1]));
	ENDFOR
//--------------------------------
// --- init: DUPLICATE_Splitter_4076
	FOR(uint32_t, __iter_init_, 0, <, 37, __iter_init_++)
		float __token_ = pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4073DUPLICATE_Splitter_4076);
		FOR(uint32_t, __iter_dup_, 0, <, 2, __iter_dup_++)
			push_float(&SplitJoin102_FirFilter_Fiss_4117_4151_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: FirFilter_4078
	 {
	FirFilter_4078_s.COEFF[0] = 56.0 ; 
	FirFilter_4078_s.COEFF[1] = 64.0 ; 
	FirFilter_4078_s.COEFF[2] = 72.0 ; 
	FirFilter_4078_s.COEFF[3] = 80.0 ; 
	FirFilter_4078_s.COEFF[4] = 88.0 ; 
	FirFilter_4078_s.COEFF[5] = 96.0 ; 
	FirFilter_4078_s.COEFF[6] = 104.0 ; 
	FirFilter_4078_s.COEFF[7] = 112.0 ; 
	FirFilter_4078_s.COEFF[8] = 120.0 ; 
	FirFilter_4078_s.COEFF[9] = 128.0 ; 
	FirFilter_4078_s.COEFF[10] = 136.0 ; 
	FirFilter_4078_s.COEFF[11] = 144.0 ; 
	FirFilter_4078_s.COEFF[12] = 152.0 ; 
	FirFilter_4078_s.COEFF[13] = 160.0 ; 
	FirFilter_4078_s.COEFF[14] = 168.0 ; 
	FirFilter_4078_s.COEFF[15] = 176.0 ; 
	FirFilter_4078_s.COEFF[16] = 184.0 ; 
	FirFilter_4078_s.COEFF[17] = 192.0 ; 
	FirFilter_4078_s.COEFF[18] = 200.0 ; 
	FirFilter_4078_s.COEFF[19] = 208.0 ; 
	FirFilter_4078_s.COEFF[20] = 216.0 ; 
	FirFilter_4078_s.COEFF[21] = 224.0 ; 
	FirFilter_4078_s.COEFF[22] = 232.0 ; 
	FirFilter_4078_s.COEFF[23] = 240.0 ; 
	FirFilter_4078_s.COEFF[24] = 248.0 ; 
	FirFilter_4078_s.COEFF[25] = 256.0 ; 
	FirFilter_4078_s.COEFF[26] = 264.0 ; 
	FirFilter_4078_s.COEFF[27] = 272.0 ; 
	FirFilter_4078_s.COEFF[28] = 280.0 ; 
	FirFilter_4078_s.COEFF[29] = 288.0 ; 
	FirFilter_4078_s.COEFF[30] = 296.0 ; 
	FirFilter_4078_s.COEFF[31] = 304.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		float sum = 0.0;
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin102_FirFilter_Fiss_4117_4151_split[0], i) * FirFilter_4078_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin102_FirFilter_Fiss_4117_4151_split[0]) ; 
		push_float(&SplitJoin102_FirFilter_Fiss_4117_4151_join[0], sum) ; 
 {
		pop_void(&SplitJoin102_FirFilter_Fiss_4117_4151_split[0]) ; 
	}
	}
	ENDFOR
//--------------------------------
// --- init: FirFilter_4079
	 {
	FirFilter_4079_s.COEFF[0] = 56.0 ; 
	FirFilter_4079_s.COEFF[1] = 64.0 ; 
	FirFilter_4079_s.COEFF[2] = 72.0 ; 
	FirFilter_4079_s.COEFF[3] = 80.0 ; 
	FirFilter_4079_s.COEFF[4] = 88.0 ; 
	FirFilter_4079_s.COEFF[5] = 96.0 ; 
	FirFilter_4079_s.COEFF[6] = 104.0 ; 
	FirFilter_4079_s.COEFF[7] = 112.0 ; 
	FirFilter_4079_s.COEFF[8] = 120.0 ; 
	FirFilter_4079_s.COEFF[9] = 128.0 ; 
	FirFilter_4079_s.COEFF[10] = 136.0 ; 
	FirFilter_4079_s.COEFF[11] = 144.0 ; 
	FirFilter_4079_s.COEFF[12] = 152.0 ; 
	FirFilter_4079_s.COEFF[13] = 160.0 ; 
	FirFilter_4079_s.COEFF[14] = 168.0 ; 
	FirFilter_4079_s.COEFF[15] = 176.0 ; 
	FirFilter_4079_s.COEFF[16] = 184.0 ; 
	FirFilter_4079_s.COEFF[17] = 192.0 ; 
	FirFilter_4079_s.COEFF[18] = 200.0 ; 
	FirFilter_4079_s.COEFF[19] = 208.0 ; 
	FirFilter_4079_s.COEFF[20] = 216.0 ; 
	FirFilter_4079_s.COEFF[21] = 224.0 ; 
	FirFilter_4079_s.COEFF[22] = 232.0 ; 
	FirFilter_4079_s.COEFF[23] = 240.0 ; 
	FirFilter_4079_s.COEFF[24] = 248.0 ; 
	FirFilter_4079_s.COEFF[25] = 256.0 ; 
	FirFilter_4079_s.COEFF[26] = 264.0 ; 
	FirFilter_4079_s.COEFF[27] = 272.0 ; 
	FirFilter_4079_s.COEFF[28] = 280.0 ; 
	FirFilter_4079_s.COEFF[29] = 288.0 ; 
	FirFilter_4079_s.COEFF[30] = 296.0 ; 
	FirFilter_4079_s.COEFF[31] = 304.0 ; 
}
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		float sum = 0.0;
 {
		pop_void(&SplitJoin102_FirFilter_Fiss_4117_4151_split[1]) ; 
	}
		sum = 0.0 ; 
		sum = 0.0 ; 
		FOR(int, i, 0,  < , 32, i++) {
			sum = (sum + (peek_float(&SplitJoin102_FirFilter_Fiss_4117_4151_split[1], i) * FirFilter_4079_s.COEFF[(31 - i)])) ; 
		}
		ENDFOR
		pop_float(&SplitJoin102_FirFilter_Fiss_4117_4151_split[1]) ; 
		push_float(&SplitJoin102_FirFilter_Fiss_4117_4151_join[1], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4077
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[7], pop_float(&SplitJoin102_FirFilter_Fiss_4117_4151_join[0]));
		push_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[7], pop_float(&SplitJoin102_FirFilter_Fiss_4117_4151_join[1]));
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_3933
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3933WEIGHTED_ROUND_ROBIN_Splitter_4080, pop_float(&SplitJoin0_SplitJoin0_SplitJoin0_Branches_3870_3934_4084_4119_join[__iter_]));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Splitter_4080
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&SplitJoin10_Combine_Fiss_4089_4152_split[0], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3933WEIGHTED_ROUND_ROBIN_Splitter_4080));
		ENDFOR
		FOR(uint32_t, __iter_, 0, <, 8, __iter_++)
			push_float(&SplitJoin10_Combine_Fiss_4089_4152_split[1], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3933WEIGHTED_ROUND_ROBIN_Splitter_4080));
		ENDFOR
	ENDFOR
//--------------------------------
// --- init: Combine_4082
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		float sum = 0.0;
		FOR(int, i, 0,  < , 8, i++) {
			sum = (sum + pop_float(&SplitJoin10_Combine_Fiss_4089_4152_split[0])) ; 
		}
		ENDFOR
		push_float(&SplitJoin10_Combine_Fiss_4089_4152_join[0], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: Combine_4083
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++) {
		float sum = 0.0;
		FOR(int, i, 0,  < , 8, i++) {
			sum = (sum + pop_float(&SplitJoin10_Combine_Fiss_4089_4152_split[1])) ; 
		}
		ENDFOR
		push_float(&SplitJoin10_Combine_Fiss_4089_4152_join[1], sum) ; 
	}
	ENDFOR
//--------------------------------
// --- init: WEIGHTED_ROUND_ROBIN_Joiner_4081
	FOR(uint32_t, __iter_init_, 0, <, 3, __iter_init_++)
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4081sink_3931, pop_float(&SplitJoin10_Combine_Fiss_4089_4152_join[0]));
		push_float(&WEIGHTED_ROUND_ROBIN_Joiner_4081sink_3931, pop_float(&SplitJoin10_Combine_Fiss_4089_4152_join[1]));
	ENDFOR
//--------------------------------
// --- init: sink_3931
	FOR(uint32_t, __iter_init_, 0, <, 6, __iter_init_++) {
		printf("%.10f", pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_4081sink_3931));
		printf("\n");
	}
	ENDFOR
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		source_3872();
		DUPLICATE_Splitter_3932();
			WEIGHTED_ROUND_ROBIN_Splitter_3952();
				Delay_N_3954();
				Delay_N_3955();
			WEIGHTED_ROUND_ROBIN_Joiner_3953();
			DUPLICATE_Splitter_3956();
				FirFilter_3958();
				FirFilter_3959();
			WEIGHTED_ROUND_ROBIN_Joiner_3957();
			DownSamp_3877();
			UpSamp_3878();
			WEIGHTED_ROUND_ROBIN_Splitter_3960();
				Delay_N_3962();
				Delay_N_3963();
			WEIGHTED_ROUND_ROBIN_Joiner_3961();
			DUPLICATE_Splitter_3964();
				FirFilter_3966();
				FirFilter_3967();
			WEIGHTED_ROUND_ROBIN_Joiner_3965();
			WEIGHTED_ROUND_ROBIN_Splitter_3968();
				Delay_N_3970();
				Delay_N_3971();
			WEIGHTED_ROUND_ROBIN_Joiner_3969();
			DUPLICATE_Splitter_3972();
				FirFilter_3974();
				FirFilter_3975();
			WEIGHTED_ROUND_ROBIN_Joiner_3973();
			DownSamp_3884();
			UpSamp_3885();
			WEIGHTED_ROUND_ROBIN_Splitter_3976();
				Delay_N_3978();
				Delay_N_3979();
			WEIGHTED_ROUND_ROBIN_Joiner_3977();
			DUPLICATE_Splitter_3980();
				FirFilter_3982();
				FirFilter_3983();
			WEIGHTED_ROUND_ROBIN_Joiner_3981();
			WEIGHTED_ROUND_ROBIN_Splitter_3984();
				Delay_N_3986();
				Delay_N_3987();
			WEIGHTED_ROUND_ROBIN_Joiner_3985();
			DUPLICATE_Splitter_3988();
				FirFilter_3990();
				FirFilter_3991();
			WEIGHTED_ROUND_ROBIN_Joiner_3989();
			DownSamp_3891();
			UpSamp_3892();
			WEIGHTED_ROUND_ROBIN_Splitter_3992();
				Delay_N_3994();
				Delay_N_3995();
			WEIGHTED_ROUND_ROBIN_Joiner_3993();
			DUPLICATE_Splitter_3996();
				FirFilter_3998();
				FirFilter_3999();
			WEIGHTED_ROUND_ROBIN_Joiner_3997();
			WEIGHTED_ROUND_ROBIN_Splitter_4000();
				Delay_N_4002();
				Delay_N_4003();
			WEIGHTED_ROUND_ROBIN_Joiner_4001();
			DUPLICATE_Splitter_4004();
				FirFilter_4006();
				FirFilter_4007();
			WEIGHTED_ROUND_ROBIN_Joiner_4005();
			DownSamp_3898();
			UpSamp_3899();
			WEIGHTED_ROUND_ROBIN_Splitter_4008();
				Delay_N_4010();
				Delay_N_4011();
			WEIGHTED_ROUND_ROBIN_Joiner_4009();
			DUPLICATE_Splitter_4012();
				FirFilter_4014();
				FirFilter_4015();
			WEIGHTED_ROUND_ROBIN_Joiner_4013();
			WEIGHTED_ROUND_ROBIN_Splitter_4016();
				Delay_N_4018();
				Delay_N_4019();
			WEIGHTED_ROUND_ROBIN_Joiner_4017();
			DUPLICATE_Splitter_4020();
				FirFilter_4022();
				FirFilter_4023();
			WEIGHTED_ROUND_ROBIN_Joiner_4021();
			DownSamp_3905();
			UpSamp_3906();
			WEIGHTED_ROUND_ROBIN_Splitter_4024();
				Delay_N_4026();
				Delay_N_4027();
			WEIGHTED_ROUND_ROBIN_Joiner_4025();
			DUPLICATE_Splitter_4028();
				FirFilter_4030();
				FirFilter_4031();
			WEIGHTED_ROUND_ROBIN_Joiner_4029();
			WEIGHTED_ROUND_ROBIN_Splitter_4032();
				Delay_N_4034();
				Delay_N_4035();
			WEIGHTED_ROUND_ROBIN_Joiner_4033();
			DUPLICATE_Splitter_4036();
				FirFilter_4038();
				FirFilter_4039();
			WEIGHTED_ROUND_ROBIN_Joiner_4037();
			DownSamp_3912();
			UpSamp_3913();
			WEIGHTED_ROUND_ROBIN_Splitter_4040();
				Delay_N_4042();
				Delay_N_4043();
			WEIGHTED_ROUND_ROBIN_Joiner_4041();
			DUPLICATE_Splitter_4044();
				FirFilter_4046();
				FirFilter_4047();
			WEIGHTED_ROUND_ROBIN_Joiner_4045();
			WEIGHTED_ROUND_ROBIN_Splitter_4048();
				Delay_N_4050();
				Delay_N_4051();
			WEIGHTED_ROUND_ROBIN_Joiner_4049();
			DUPLICATE_Splitter_4052();
				FirFilter_4054();
				FirFilter_4055();
			WEIGHTED_ROUND_ROBIN_Joiner_4053();
			DownSamp_3919();
			UpSamp_3920();
			WEIGHTED_ROUND_ROBIN_Splitter_4056();
				Delay_N_4058();
				Delay_N_4059();
			WEIGHTED_ROUND_ROBIN_Joiner_4057();
			DUPLICATE_Splitter_4060();
				FirFilter_4062();
				FirFilter_4063();
			WEIGHTED_ROUND_ROBIN_Joiner_4061();
			WEIGHTED_ROUND_ROBIN_Splitter_4064();
				Delay_N_4066();
				Delay_N_4067();
			WEIGHTED_ROUND_ROBIN_Joiner_4065();
			DUPLICATE_Splitter_4068();
				FirFilter_4070();
				FirFilter_4071();
			WEIGHTED_ROUND_ROBIN_Joiner_4069();
			DownSamp_3926();
			UpSamp_3927();
			WEIGHTED_ROUND_ROBIN_Splitter_4072();
				Delay_N_4074();
				Delay_N_4075();
			WEIGHTED_ROUND_ROBIN_Joiner_4073();
			DUPLICATE_Splitter_4076();
				FirFilter_4078();
				FirFilter_4079();
			WEIGHTED_ROUND_ROBIN_Joiner_4077();
		WEIGHTED_ROUND_ROBIN_Joiner_3933();
		WEIGHTED_ROUND_ROBIN_Splitter_4080();
			Combine_4082();
			Combine_4083();
		WEIGHTED_ROUND_ROBIN_Joiner_4081();
		sink_3931();
	ENDFOR
	return EXIT_SUCCESS;
}
