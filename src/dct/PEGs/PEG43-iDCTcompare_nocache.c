#include "PEG43-iDCTcompare_nocache.h"

buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8350_8426_8552_8559_join[3];
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[43];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[43];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_8555_8562_join[8];
buffer_int_t AnonFilter_a0_8349DUPLICATE_Splitter_8424;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8350_8426_8552_8559_split[3];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_8425AnonFilter_a2_8377;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_8554_8561_join[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_8555_8562_split[8];
buffer_float_t Post_CollapsedDataParallel_2_8423WEIGHTED_ROUND_ROBIN_Splitter_8487;
buffer_float_t Pre_CollapsedDataParallel_1_8422WEIGHTED_ROUND_ROBIN_Splitter_8477;
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[43];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_8554_8561_split[8];
buffer_int_t SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8478Post_CollapsedDataParallel_2_8423;
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[43];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8488WEIGHTED_ROUND_ROBIN_Splitter_8497;
buffer_int_t SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_8543iDCT8x8_1D_col_fast_8376;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_8433Pre_CollapsedDataParallel_1_8422;


iDCT_2D_reference_coarse_8352_t iDCT_2D_reference_coarse_8352_s;
iDCT_2D_reference_coarse_8352_t iDCT_1D_reference_fine_8479_s;
iDCT_2D_reference_coarse_8352_t iDCT_1D_reference_fine_8480_s;
iDCT_2D_reference_coarse_8352_t iDCT_1D_reference_fine_8481_s;
iDCT_2D_reference_coarse_8352_t iDCT_1D_reference_fine_8482_s;
iDCT_2D_reference_coarse_8352_t iDCT_1D_reference_fine_8483_s;
iDCT_2D_reference_coarse_8352_t iDCT_1D_reference_fine_8484_s;
iDCT_2D_reference_coarse_8352_t iDCT_1D_reference_fine_8485_s;
iDCT_2D_reference_coarse_8352_t iDCT_1D_reference_fine_8486_s;
iDCT_2D_reference_coarse_8352_t iDCT_1D_reference_fine_8489_s;
iDCT_2D_reference_coarse_8352_t iDCT_1D_reference_fine_8490_s;
iDCT_2D_reference_coarse_8352_t iDCT_1D_reference_fine_8491_s;
iDCT_2D_reference_coarse_8352_t iDCT_1D_reference_fine_8492_s;
iDCT_2D_reference_coarse_8352_t iDCT_1D_reference_fine_8493_s;
iDCT_2D_reference_coarse_8352_t iDCT_1D_reference_fine_8494_s;
iDCT_2D_reference_coarse_8352_t iDCT_1D_reference_fine_8495_s;
iDCT_2D_reference_coarse_8352_t iDCT_1D_reference_fine_8496_s;
iDCT8x8_1D_col_fast_8376_t iDCT8x8_1D_col_fast_8376_s;
AnonFilter_a2_8377_t AnonFilter_a2_8377_s;

void AnonFilter_a0_8349(){
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&AnonFilter_a0_8349DUPLICATE_Splitter_8424, (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_2D_reference_coarse_8352(){
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_8352_s.coeff[k][j] * peek_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8350_8426_8552_8559_split[0], ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_8352_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8350_8426_8552_8559_join[0], ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8350_8426_8552_8559_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void AnonFilter_a3_8434(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[0], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[0])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8435(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[1], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[1])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8436(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[2], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[2])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8437(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[3], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[3])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8438(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[4], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[4])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8439(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[5], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[5])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8440(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[6], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[6])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8441(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[7], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[7])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8442(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[8], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[8])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8443(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[9], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[9])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8444(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[10], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[10])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8445(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[11], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[11])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8446(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[12], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[12])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8447(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[13], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[13])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8448(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[14], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[14])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8449(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[15], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[15])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8450(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[16], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[16])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8451(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[17], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[17])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8452(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[18], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[18])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8453(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[19], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[19])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8454(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[20], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[20])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8455(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[21], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[21])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8456(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[22], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[22])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8457(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[23], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[23])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8458(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[24], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[24])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8459(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[25], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[25])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8460(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[26], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[26])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8461(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[27], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[27])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8462(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[28], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[28])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8463(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[29], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[29])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8464(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[30], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[30])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8465(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[31], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[31])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8466(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[32], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[32])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8467(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[33], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[33])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8468(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[34], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[34])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8469(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[35], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[35])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8470(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[36], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[36])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8471(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[37], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[37])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8472(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[38], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[38])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8473(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[39], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[39])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8474(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[40], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[40])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8475(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[41], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[41])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_8476(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[42], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[42])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8432() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 43, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8350_8426_8552_8559_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8433() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 43, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8433Pre_CollapsedDataParallel_1_8422, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_8422(){
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = 0 ; 
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Pre_CollapsedDataParallel_1_8422WEIGHTED_ROUND_ROBIN_Splitter_8477, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_8433Pre_CollapsedDataParallel_1_8422, (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8433Pre_CollapsedDataParallel_1_8422) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8479(){
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8479_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8554_8561_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8554_8561_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8554_8561_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8480(){
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8480_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8554_8561_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8554_8561_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8554_8561_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8481(){
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8481_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8554_8561_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8554_8561_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8554_8561_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8482(){
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8482_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8554_8561_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8554_8561_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8554_8561_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8483(){
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8483_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8554_8561_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8554_8561_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8554_8561_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8484(){
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8484_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8554_8561_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8554_8561_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8554_8561_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8485(){
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8485_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8554_8561_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8554_8561_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8554_8561_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8486(){
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8486_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8554_8561_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8554_8561_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8554_8561_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8477() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8554_8561_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_8422WEIGHTED_ROUND_ROBIN_Splitter_8477));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8478() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8478Post_CollapsedDataParallel_2_8423, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8554_8561_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_8423(){
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Post_CollapsedDataParallel_2_8423WEIGHTED_ROUND_ROBIN_Splitter_8487, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_8478Post_CollapsedDataParallel_2_8423, (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8478Post_CollapsedDataParallel_2_8423) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8489(){
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8489_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8555_8562_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8555_8562_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8555_8562_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8490(){
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8490_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8555_8562_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8555_8562_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8555_8562_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8491(){
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8491_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8555_8562_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8555_8562_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8555_8562_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8492(){
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8492_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8555_8562_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8555_8562_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8555_8562_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8493(){
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8493_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8555_8562_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8555_8562_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8555_8562_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8494(){
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8494_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8555_8562_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8555_8562_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8555_8562_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8495(){
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8495_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8555_8562_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8555_8562_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8555_8562_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_8496(){
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_8496_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8555_8562_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8555_8562_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8555_8562_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8487() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8555_8562_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_8423WEIGHTED_ROUND_ROBIN_Splitter_8487));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8488() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_8488WEIGHTED_ROUND_ROBIN_Splitter_8497, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8555_8562_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4_8499(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[0], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[0]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8500(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[1], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[1]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8501(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[2], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[2]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8502(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[3], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[3]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8503(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[4], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[4]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8504(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[5], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[5]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8505(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[6], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[6]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8506(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[7], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[7]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8507(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[8], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[8]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8508(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[9], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[9]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8509(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[10], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[10]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8510(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[11], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[11]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8511(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[12], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[12]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8512(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[13], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[13]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8513(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[14], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[14]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8514(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[15], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[15]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8515(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[16], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[16]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8516(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[17], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[17]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8517(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[18], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[18]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8518(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[19], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[19]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8519(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[20], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[20]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8520(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[21], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[21]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8521(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[22], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[22]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8522(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[23], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[23]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8523(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[24], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[24]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8524(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[25], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[25]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8525(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[26], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[26]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8526(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[27], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[27]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8527(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[28], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[28]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8528(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[29], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[29]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8529(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[30], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[30]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8530(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[31], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[31]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8531(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[32], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[32]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8532(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[33], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[33]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8533(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[34], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[34]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8534(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[35], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[35]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8535(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[36], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[36]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8536(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[37], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[37]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8537(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[38], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[38]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8538(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[39], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[39]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8539(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[40], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[40]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8540(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[41], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[41]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_8541(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[42], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[42]) + 0.5)))) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8497() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 43, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_8488WEIGHTED_ROUND_ROBIN_Splitter_8497));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8498() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 43, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8350_8426_8552_8559_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast_8544(){
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[0], 0) ; 
		x1 = (peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[0], 4) << 11) ; 
		x2 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[0], 6) ; 
		x3 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[0], 2) ; 
		x4 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[0], 1) ; 
		x5 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[0], 7) ; 
		x6 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[0], 5) ; 
		x7 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[0], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[0], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[0], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[0], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[0], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[0], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[0], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[0], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[0], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[0], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_8545(){
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[1], 0) ; 
		x1 = (peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[1], 4) << 11) ; 
		x2 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[1], 6) ; 
		x3 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[1], 2) ; 
		x4 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[1], 1) ; 
		x5 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[1], 7) ; 
		x6 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[1], 5) ; 
		x7 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[1], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[1], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[1], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[1], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[1], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[1], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[1], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[1], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[1], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[1], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_8546(){
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[2], 0) ; 
		x1 = (peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[2], 4) << 11) ; 
		x2 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[2], 6) ; 
		x3 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[2], 2) ; 
		x4 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[2], 1) ; 
		x5 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[2], 7) ; 
		x6 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[2], 5) ; 
		x7 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[2], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[2], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[2], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[2], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[2], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[2], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[2], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[2], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[2], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[2], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_8547(){
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[3], 0) ; 
		x1 = (peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[3], 4) << 11) ; 
		x2 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[3], 6) ; 
		x3 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[3], 2) ; 
		x4 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[3], 1) ; 
		x5 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[3], 7) ; 
		x6 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[3], 5) ; 
		x7 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[3], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[3], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[3], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[3], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[3], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[3], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[3], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[3], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[3], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[3], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_8548(){
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[4], 0) ; 
		x1 = (peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[4], 4) << 11) ; 
		x2 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[4], 6) ; 
		x3 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[4], 2) ; 
		x4 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[4], 1) ; 
		x5 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[4], 7) ; 
		x6 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[4], 5) ; 
		x7 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[4], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[4], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[4], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[4], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[4], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[4], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[4], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[4], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[4], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[4], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_8549(){
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[5], 0) ; 
		x1 = (peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[5], 4) << 11) ; 
		x2 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[5], 6) ; 
		x3 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[5], 2) ; 
		x4 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[5], 1) ; 
		x5 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[5], 7) ; 
		x6 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[5], 5) ; 
		x7 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[5], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[5], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[5], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[5], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[5], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[5], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[5], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[5], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[5], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[5], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_8550(){
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[6], 0) ; 
		x1 = (peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[6], 4) << 11) ; 
		x2 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[6], 6) ; 
		x3 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[6], 2) ; 
		x4 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[6], 1) ; 
		x5 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[6], 7) ; 
		x6 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[6], 5) ; 
		x7 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[6], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[6], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[6], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[6], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[6], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[6], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[6], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[6], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[6], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[6], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_8551(){
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[7], 0) ; 
		x1 = (peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[7], 4) << 11) ; 
		x2 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[7], 6) ; 
		x3 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[7], 2) ; 
		x4 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[7], 1) ; 
		x5 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[7], 7) ; 
		x6 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[7], 5) ; 
		x7 = peek_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[7], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[7], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[7], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[7], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[7], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[7], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[7], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[7], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[7], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[7], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_8542() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8350_8426_8552_8559_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8543() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_8543iDCT8x8_1D_col_fast_8376, pop_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast_8376(){
	FOR(uint32_t, __iter_steady_, 0, <, 43, __iter_steady_++) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_8543iDCT8x8_1D_col_fast_8376, (c + 0)) ; 
			x1 = (peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_8543iDCT8x8_1D_col_fast_8376, (c + 32)) << 8) ; 
			x2 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_8543iDCT8x8_1D_col_fast_8376, (c + 48)) ; 
			x3 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_8543iDCT8x8_1D_col_fast_8376, (c + 16)) ; 
			x4 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_8543iDCT8x8_1D_col_fast_8376, (c + 8)) ; 
			x5 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_8543iDCT8x8_1D_col_fast_8376, (c + 56)) ; 
			x6 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_8543iDCT8x8_1D_col_fast_8376, (c + 40)) ; 
			x7 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_8543iDCT8x8_1D_col_fast_8376, (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_8376_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_8376_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_8376_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_8376_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_8376_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_8376_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_8376_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_8376_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_8376_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_8543iDCT8x8_1D_col_fast_8376) ; 
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8350_8426_8552_8559_join[2], iDCT8x8_1D_col_fast_8376_s.buffer[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void DUPLICATE_Splitter_8424() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2752, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_8349DUPLICATE_Splitter_8424);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8350_8426_8552_8559_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_8425() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 2752, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_8425AnonFilter_a2_8377, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8350_8426_8552_8559_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2_8377(){
	FOR(uint32_t, __iter_steady_, 0, <, 2752, __iter_steady_++) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_8425AnonFilter_a2_8377) ; 
		reffine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_8425AnonFilter_a2_8377) ; 
		fastfine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_8425AnonFilter_a2_8377) ; 
		AnonFilter_a2_8377_s.count = (AnonFilter_a2_8377_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_8377_s.errors = (AnonFilter_a2_8377_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_8377_s.errors / AnonFilter_a2_8377_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_8377_s.errors = (AnonFilter_a2_8377_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_8377_s.errors / AnonFilter_a2_8377_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 3, __iter_init_0_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8350_8426_8552_8559_join[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 43, __iter_init_1_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 43, __iter_init_2_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_8556_8563_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8555_8562_join[__iter_init_3_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_8349DUPLICATE_Splitter_8424);
	FOR(int, __iter_init_4_, 0, <, 3, __iter_init_4_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_8350_8426_8552_8559_split[__iter_init_4_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_8425AnonFilter_a2_8377);
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8554_8561_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_8555_8562_split[__iter_init_6_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_8423WEIGHTED_ROUND_ROBIN_Splitter_8487);
	init_buffer_float(&Pre_CollapsedDataParallel_1_8422WEIGHTED_ROUND_ROBIN_Splitter_8477);
	FOR(int, __iter_init_7_, 0, <, 43, __iter_init_7_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_8554_8561_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_split[__iter_init_9_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8478Post_CollapsedDataParallel_2_8423);
	FOR(int, __iter_init_10_, 0, <, 43, __iter_init_10_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_8553_8560_join[__iter_init_10_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8488WEIGHTED_ROUND_ROBIN_Splitter_8497);
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_int(&SplitJoin110_iDCT8x8_1D_row_fast_Fiss_8557_8564_join[__iter_init_11_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_8543iDCT8x8_1D_col_fast_8376);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_8433Pre_CollapsedDataParallel_1_8422);
// --- init: iDCT_2D_reference_coarse_8352
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_8352_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8479
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8479_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8480
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8480_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8481
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8481_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8482
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8482_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8483
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8483_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8484
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8484_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8485
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8485_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8486
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8486_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8489
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8489_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8490
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8490_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8491
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8491_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8492
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8492_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8493
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8493_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8494
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8494_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8495
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8495_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_8496
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_8496_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_8377
	 {
	AnonFilter_a2_8377_s.count = 0.0 ; 
	AnonFilter_a2_8377_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_8349();
		DUPLICATE_Splitter_8424();
			iDCT_2D_reference_coarse_8352();
			WEIGHTED_ROUND_ROBIN_Splitter_8432();
				AnonFilter_a3_8434();
				AnonFilter_a3_8435();
				AnonFilter_a3_8436();
				AnonFilter_a3_8437();
				AnonFilter_a3_8438();
				AnonFilter_a3_8439();
				AnonFilter_a3_8440();
				AnonFilter_a3_8441();
				AnonFilter_a3_8442();
				AnonFilter_a3_8443();
				AnonFilter_a3_8444();
				AnonFilter_a3_8445();
				AnonFilter_a3_8446();
				AnonFilter_a3_8447();
				AnonFilter_a3_8448();
				AnonFilter_a3_8449();
				AnonFilter_a3_8450();
				AnonFilter_a3_8451();
				AnonFilter_a3_8452();
				AnonFilter_a3_8453();
				AnonFilter_a3_8454();
				AnonFilter_a3_8455();
				AnonFilter_a3_8456();
				AnonFilter_a3_8457();
				AnonFilter_a3_8458();
				AnonFilter_a3_8459();
				AnonFilter_a3_8460();
				AnonFilter_a3_8461();
				AnonFilter_a3_8462();
				AnonFilter_a3_8463();
				AnonFilter_a3_8464();
				AnonFilter_a3_8465();
				AnonFilter_a3_8466();
				AnonFilter_a3_8467();
				AnonFilter_a3_8468();
				AnonFilter_a3_8469();
				AnonFilter_a3_8470();
				AnonFilter_a3_8471();
				AnonFilter_a3_8472();
				AnonFilter_a3_8473();
				AnonFilter_a3_8474();
				AnonFilter_a3_8475();
				AnonFilter_a3_8476();
			WEIGHTED_ROUND_ROBIN_Joiner_8433();
			Pre_CollapsedDataParallel_1_8422();
			WEIGHTED_ROUND_ROBIN_Splitter_8477();
				iDCT_1D_reference_fine_8479();
				iDCT_1D_reference_fine_8480();
				iDCT_1D_reference_fine_8481();
				iDCT_1D_reference_fine_8482();
				iDCT_1D_reference_fine_8483();
				iDCT_1D_reference_fine_8484();
				iDCT_1D_reference_fine_8485();
				iDCT_1D_reference_fine_8486();
			WEIGHTED_ROUND_ROBIN_Joiner_8478();
			Post_CollapsedDataParallel_2_8423();
			WEIGHTED_ROUND_ROBIN_Splitter_8487();
				iDCT_1D_reference_fine_8489();
				iDCT_1D_reference_fine_8490();
				iDCT_1D_reference_fine_8491();
				iDCT_1D_reference_fine_8492();
				iDCT_1D_reference_fine_8493();
				iDCT_1D_reference_fine_8494();
				iDCT_1D_reference_fine_8495();
				iDCT_1D_reference_fine_8496();
			WEIGHTED_ROUND_ROBIN_Joiner_8488();
			WEIGHTED_ROUND_ROBIN_Splitter_8497();
				AnonFilter_a4_8499();
				AnonFilter_a4_8500();
				AnonFilter_a4_8501();
				AnonFilter_a4_8502();
				AnonFilter_a4_8503();
				AnonFilter_a4_8504();
				AnonFilter_a4_8505();
				AnonFilter_a4_8506();
				AnonFilter_a4_8507();
				AnonFilter_a4_8508();
				AnonFilter_a4_8509();
				AnonFilter_a4_8510();
				AnonFilter_a4_8511();
				AnonFilter_a4_8512();
				AnonFilter_a4_8513();
				AnonFilter_a4_8514();
				AnonFilter_a4_8515();
				AnonFilter_a4_8516();
				AnonFilter_a4_8517();
				AnonFilter_a4_8518();
				AnonFilter_a4_8519();
				AnonFilter_a4_8520();
				AnonFilter_a4_8521();
				AnonFilter_a4_8522();
				AnonFilter_a4_8523();
				AnonFilter_a4_8524();
				AnonFilter_a4_8525();
				AnonFilter_a4_8526();
				AnonFilter_a4_8527();
				AnonFilter_a4_8528();
				AnonFilter_a4_8529();
				AnonFilter_a4_8530();
				AnonFilter_a4_8531();
				AnonFilter_a4_8532();
				AnonFilter_a4_8533();
				AnonFilter_a4_8534();
				AnonFilter_a4_8535();
				AnonFilter_a4_8536();
				AnonFilter_a4_8537();
				AnonFilter_a4_8538();
				AnonFilter_a4_8539();
				AnonFilter_a4_8540();
				AnonFilter_a4_8541();
			WEIGHTED_ROUND_ROBIN_Joiner_8498();
			WEIGHTED_ROUND_ROBIN_Splitter_8542();
				iDCT8x8_1D_row_fast_8544();
				iDCT8x8_1D_row_fast_8545();
				iDCT8x8_1D_row_fast_8546();
				iDCT8x8_1D_row_fast_8547();
				iDCT8x8_1D_row_fast_8548();
				iDCT8x8_1D_row_fast_8549();
				iDCT8x8_1D_row_fast_8550();
				iDCT8x8_1D_row_fast_8551();
			WEIGHTED_ROUND_ROBIN_Joiner_8543();
			iDCT8x8_1D_col_fast_8376();
		WEIGHTED_ROUND_ROBIN_Joiner_8425();
		AnonFilter_a2_8377();
	ENDFOR
	return EXIT_SUCCESS;
}
