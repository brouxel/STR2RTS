#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=384 on the compile command line
#else
#if BUF_SIZEMAX < 384
#error BUF_SIZEMAX too small, it must be at least 384
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_18378_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_18402_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_18403_t;
void AnonFilter_a0_18375();
void DUPLICATE_Splitter_18450();
void iDCT_2D_reference_coarse_18378();
void WEIGHTED_ROUND_ROBIN_Splitter_18458();
void AnonFilter_a3_18460();
void AnonFilter_a3_18461();
void AnonFilter_a3_18462();
void AnonFilter_a3_18463();
void WEIGHTED_ROUND_ROBIN_Joiner_18459();
void Pre_CollapsedDataParallel_1_18448();
void WEIGHTED_ROUND_ROBIN_Splitter_18464();
void iDCT_1D_reference_fine_18466();
void iDCT_1D_reference_fine_18467();
void iDCT_1D_reference_fine_18468();
void iDCT_1D_reference_fine_18469();
void WEIGHTED_ROUND_ROBIN_Joiner_18465();
void Post_CollapsedDataParallel_2_18449();
void WEIGHTED_ROUND_ROBIN_Splitter_18470();
void iDCT_1D_reference_fine_18472();
void iDCT_1D_reference_fine_18473();
void iDCT_1D_reference_fine_18474();
void iDCT_1D_reference_fine_18475();
void WEIGHTED_ROUND_ROBIN_Joiner_18471();
void WEIGHTED_ROUND_ROBIN_Splitter_18476();
void AnonFilter_a4_18478();
void AnonFilter_a4_18479();
void AnonFilter_a4_18480();
void AnonFilter_a4_18481();
void WEIGHTED_ROUND_ROBIN_Joiner_18477();
void WEIGHTED_ROUND_ROBIN_Splitter_18482();
void iDCT8x8_1D_row_fast_18484();
void iDCT8x8_1D_row_fast_18485();
void iDCT8x8_1D_row_fast_18486();
void iDCT8x8_1D_row_fast_18487();
void WEIGHTED_ROUND_ROBIN_Joiner_18483();
void iDCT8x8_1D_col_fast_18402();
void WEIGHTED_ROUND_ROBIN_Joiner_18451();
void AnonFilter_a2_18403();

#ifdef __cplusplus
}
#endif
#endif
