#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=3456 on the compile command line
#else
#if BUF_SIZEMAX < 3456
#error BUF_SIZEMAX too small, it must be at least 3456
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_15502_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_15526_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_15527_t;
void AnonFilter_a0_15499();
void DUPLICATE_Splitter_15574();
void iDCT_2D_reference_coarse_15502();
void WEIGHTED_ROUND_ROBIN_Splitter_15582();
void AnonFilter_a3_15584();
void AnonFilter_a3_15585();
void AnonFilter_a3_15586();
void AnonFilter_a3_15587();
void AnonFilter_a3_15588();
void AnonFilter_a3_15589();
void AnonFilter_a3_15590();
void AnonFilter_a3_15591();
void AnonFilter_a3_15592();
void AnonFilter_a3_15593();
void AnonFilter_a3_15594();
void AnonFilter_a3_15595();
void AnonFilter_a3_15596();
void AnonFilter_a3_15597();
void AnonFilter_a3_15598();
void AnonFilter_a3_15599();
void AnonFilter_a3_15600();
void AnonFilter_a3_15601();
void WEIGHTED_ROUND_ROBIN_Joiner_15583();
void Pre_CollapsedDataParallel_1_15572();
void WEIGHTED_ROUND_ROBIN_Splitter_15602();
void iDCT_1D_reference_fine_15604();
void iDCT_1D_reference_fine_15605();
void iDCT_1D_reference_fine_15606();
void iDCT_1D_reference_fine_15607();
void iDCT_1D_reference_fine_15608();
void iDCT_1D_reference_fine_15609();
void iDCT_1D_reference_fine_15610();
void iDCT_1D_reference_fine_15611();
void WEIGHTED_ROUND_ROBIN_Joiner_15603();
void Post_CollapsedDataParallel_2_15573();
void WEIGHTED_ROUND_ROBIN_Splitter_15612();
void iDCT_1D_reference_fine_15614();
void iDCT_1D_reference_fine_15615();
void iDCT_1D_reference_fine_15616();
void iDCT_1D_reference_fine_15617();
void iDCT_1D_reference_fine_15618();
void iDCT_1D_reference_fine_15619();
void iDCT_1D_reference_fine_15620();
void iDCT_1D_reference_fine_15621();
void WEIGHTED_ROUND_ROBIN_Joiner_15613();
void WEIGHTED_ROUND_ROBIN_Splitter_15622();
void AnonFilter_a4_15624();
void AnonFilter_a4_15625();
void AnonFilter_a4_15626();
void AnonFilter_a4_15627();
void AnonFilter_a4_15628();
void AnonFilter_a4_15629();
void AnonFilter_a4_15630();
void AnonFilter_a4_15631();
void AnonFilter_a4_15632();
void AnonFilter_a4_15633();
void AnonFilter_a4_15634();
void AnonFilter_a4_15635();
void AnonFilter_a4_15636();
void AnonFilter_a4_15637();
void AnonFilter_a4_15638();
void AnonFilter_a4_15639();
void AnonFilter_a4_15640();
void AnonFilter_a4_15641();
void WEIGHTED_ROUND_ROBIN_Joiner_15623();
void WEIGHTED_ROUND_ROBIN_Splitter_15642();
void iDCT8x8_1D_row_fast_15644();
void iDCT8x8_1D_row_fast_15645();
void iDCT8x8_1D_row_fast_15646();
void iDCT8x8_1D_row_fast_15647();
void iDCT8x8_1D_row_fast_15648();
void iDCT8x8_1D_row_fast_15649();
void iDCT8x8_1D_row_fast_15650();
void iDCT8x8_1D_row_fast_15651();
void WEIGHTED_ROUND_ROBIN_Joiner_15643();
void iDCT8x8_1D_col_fast_15526();
void WEIGHTED_ROUND_ROBIN_Joiner_15575();
void AnonFilter_a2_15527();

#ifdef __cplusplus
}
#endif
#endif
