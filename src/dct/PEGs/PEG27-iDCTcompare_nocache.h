#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=10368 on the compile command line
#else
#if BUF_SIZEMAX < 10368
#error BUF_SIZEMAX too small, it must be at least 10368
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_13216_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_13240_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_13241_t;
void AnonFilter_a0_13213();
void DUPLICATE_Splitter_13288();
void iDCT_2D_reference_coarse_13216();
void WEIGHTED_ROUND_ROBIN_Splitter_13296();
void AnonFilter_a3_13298();
void AnonFilter_a3_13299();
void AnonFilter_a3_13300();
void AnonFilter_a3_13301();
void AnonFilter_a3_13302();
void AnonFilter_a3_13303();
void AnonFilter_a3_13304();
void AnonFilter_a3_13305();
void AnonFilter_a3_13306();
void AnonFilter_a3_13307();
void AnonFilter_a3_13308();
void AnonFilter_a3_13309();
void AnonFilter_a3_13310();
void AnonFilter_a3_13311();
void AnonFilter_a3_13312();
void AnonFilter_a3_13313();
void AnonFilter_a3_13314();
void AnonFilter_a3_13315();
void AnonFilter_a3_13316();
void AnonFilter_a3_13317();
void AnonFilter_a3_13318();
void AnonFilter_a3_13319();
void AnonFilter_a3_13320();
void AnonFilter_a3_13321();
void AnonFilter_a3_13322();
void AnonFilter_a3_13323();
void AnonFilter_a3_13324();
void WEIGHTED_ROUND_ROBIN_Joiner_13297();
void Pre_CollapsedDataParallel_1_13286();
void WEIGHTED_ROUND_ROBIN_Splitter_13325();
void iDCT_1D_reference_fine_13327();
void iDCT_1D_reference_fine_13328();
void iDCT_1D_reference_fine_13329();
void iDCT_1D_reference_fine_13330();
void iDCT_1D_reference_fine_13331();
void iDCT_1D_reference_fine_13332();
void iDCT_1D_reference_fine_13333();
void iDCT_1D_reference_fine_13334();
void WEIGHTED_ROUND_ROBIN_Joiner_13326();
void Post_CollapsedDataParallel_2_13287();
void WEIGHTED_ROUND_ROBIN_Splitter_13335();
void iDCT_1D_reference_fine_13337();
void iDCT_1D_reference_fine_13338();
void iDCT_1D_reference_fine_13339();
void iDCT_1D_reference_fine_13340();
void iDCT_1D_reference_fine_13341();
void iDCT_1D_reference_fine_13342();
void iDCT_1D_reference_fine_13343();
void iDCT_1D_reference_fine_13344();
void WEIGHTED_ROUND_ROBIN_Joiner_13336();
void WEIGHTED_ROUND_ROBIN_Splitter_13345();
void AnonFilter_a4_13347();
void AnonFilter_a4_13348();
void AnonFilter_a4_13349();
void AnonFilter_a4_13350();
void AnonFilter_a4_13351();
void AnonFilter_a4_13352();
void AnonFilter_a4_13353();
void AnonFilter_a4_13354();
void AnonFilter_a4_13355();
void AnonFilter_a4_13356();
void AnonFilter_a4_13357();
void AnonFilter_a4_13358();
void AnonFilter_a4_13359();
void AnonFilter_a4_13360();
void AnonFilter_a4_13361();
void AnonFilter_a4_13362();
void AnonFilter_a4_13363();
void AnonFilter_a4_13364();
void AnonFilter_a4_13365();
void AnonFilter_a4_13366();
void AnonFilter_a4_13367();
void AnonFilter_a4_13368();
void AnonFilter_a4_13369();
void AnonFilter_a4_13370();
void AnonFilter_a4_13371();
void AnonFilter_a4_13372();
void AnonFilter_a4_13373();
void WEIGHTED_ROUND_ROBIN_Joiner_13346();
void WEIGHTED_ROUND_ROBIN_Splitter_13374();
void iDCT8x8_1D_row_fast_13376();
void iDCT8x8_1D_row_fast_13377();
void iDCT8x8_1D_row_fast_13378();
void iDCT8x8_1D_row_fast_13379();
void iDCT8x8_1D_row_fast_13380();
void iDCT8x8_1D_row_fast_13381();
void iDCT8x8_1D_row_fast_13382();
void iDCT8x8_1D_row_fast_13383();
void WEIGHTED_ROUND_ROBIN_Joiner_13375();
void iDCT8x8_1D_col_fast_13240();
void WEIGHTED_ROUND_ROBIN_Joiner_13289();
void AnonFilter_a2_13241();

#ifdef __cplusplus
}
#endif
#endif
