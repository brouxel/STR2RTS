#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=1152 on the compile command line
#else
#if BUF_SIZEMAX < 1152
#error BUF_SIZEMAX too small, it must be at least 1152
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_6622_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_6646_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_6647_t;
void AnonFilter_a0_6619();
void DUPLICATE_Splitter_6694();
void iDCT_2D_reference_coarse_6622();
void WEIGHTED_ROUND_ROBIN_Splitter_6702();
void AnonFilter_a3_6704();
void AnonFilter_a3_6705();
void AnonFilter_a3_6706();
void AnonFilter_a3_6707();
void AnonFilter_a3_6708();
void AnonFilter_a3_6709();
void AnonFilter_a3_6710();
void AnonFilter_a3_6711();
void AnonFilter_a3_6712();
void AnonFilter_a3_6713();
void AnonFilter_a3_6714();
void AnonFilter_a3_6715();
void AnonFilter_a3_6716();
void AnonFilter_a3_6717();
void AnonFilter_a3_6718();
void AnonFilter_a3_6719();
void AnonFilter_a3_6720();
void AnonFilter_a3_6721();
void AnonFilter_a3_6722();
void AnonFilter_a3_6723();
void AnonFilter_a3_6724();
void AnonFilter_a3_6725();
void AnonFilter_a3_6726();
void AnonFilter_a3_6727();
void AnonFilter_a3_6728();
void AnonFilter_a3_6729();
void AnonFilter_a3_6730();
void AnonFilter_a3_6731();
void AnonFilter_a3_6732();
void AnonFilter_a3_6733();
void AnonFilter_a3_6734();
void AnonFilter_a3_6735();
void AnonFilter_a3_6736();
void AnonFilter_a3_6737();
void AnonFilter_a3_6738();
void AnonFilter_a3_6739();
void AnonFilter_a3_6740();
void AnonFilter_a3_6741();
void AnonFilter_a3_6742();
void AnonFilter_a3_6743();
void AnonFilter_a3_6744();
void AnonFilter_a3_6745();
void AnonFilter_a3_6746();
void AnonFilter_a3_6747();
void AnonFilter_a3_6748();
void AnonFilter_a3_6749();
void AnonFilter_a3_6750();
void AnonFilter_a3_6751();
void WEIGHTED_ROUND_ROBIN_Joiner_6703();
void Pre_CollapsedDataParallel_1_6692();
void WEIGHTED_ROUND_ROBIN_Splitter_6752();
void iDCT_1D_reference_fine_6754();
void iDCT_1D_reference_fine_6755();
void iDCT_1D_reference_fine_6756();
void iDCT_1D_reference_fine_6757();
void iDCT_1D_reference_fine_6758();
void iDCT_1D_reference_fine_6759();
void iDCT_1D_reference_fine_6760();
void iDCT_1D_reference_fine_6761();
void WEIGHTED_ROUND_ROBIN_Joiner_6753();
void Post_CollapsedDataParallel_2_6693();
void WEIGHTED_ROUND_ROBIN_Splitter_6762();
void iDCT_1D_reference_fine_6764();
void iDCT_1D_reference_fine_6765();
void iDCT_1D_reference_fine_6766();
void iDCT_1D_reference_fine_6767();
void iDCT_1D_reference_fine_6768();
void iDCT_1D_reference_fine_6769();
void iDCT_1D_reference_fine_6770();
void iDCT_1D_reference_fine_6771();
void WEIGHTED_ROUND_ROBIN_Joiner_6763();
void WEIGHTED_ROUND_ROBIN_Splitter_6772();
void AnonFilter_a4_6774();
void AnonFilter_a4_6775();
void AnonFilter_a4_6776();
void AnonFilter_a4_6777();
void AnonFilter_a4_6778();
void AnonFilter_a4_6779();
void AnonFilter_a4_6780();
void AnonFilter_a4_6781();
void AnonFilter_a4_6782();
void AnonFilter_a4_6783();
void AnonFilter_a4_6784();
void AnonFilter_a4_6785();
void AnonFilter_a4_6786();
void AnonFilter_a4_6787();
void AnonFilter_a4_6788();
void AnonFilter_a4_6789();
void AnonFilter_a4_6790();
void AnonFilter_a4_6791();
void AnonFilter_a4_6792();
void AnonFilter_a4_6793();
void AnonFilter_a4_6794();
void AnonFilter_a4_6795();
void AnonFilter_a4_6796();
void AnonFilter_a4_6797();
void AnonFilter_a4_6798();
void AnonFilter_a4_6799();
void AnonFilter_a4_6800();
void AnonFilter_a4_6801();
void AnonFilter_a4_6802();
void AnonFilter_a4_6803();
void AnonFilter_a4_6804();
void AnonFilter_a4_6805();
void AnonFilter_a4_6806();
void AnonFilter_a4_6807();
void AnonFilter_a4_6808();
void AnonFilter_a4_6809();
void AnonFilter_a4_6810();
void AnonFilter_a4_6811();
void AnonFilter_a4_6812();
void AnonFilter_a4_6813();
void AnonFilter_a4_6814();
void AnonFilter_a4_6815();
void AnonFilter_a4_6816();
void AnonFilter_a4_6817();
void AnonFilter_a4_6818();
void AnonFilter_a4_6819();
void AnonFilter_a4_6820();
void AnonFilter_a4_6821();
void WEIGHTED_ROUND_ROBIN_Joiner_6773();
void WEIGHTED_ROUND_ROBIN_Splitter_6822();
void iDCT8x8_1D_row_fast_6824();
void iDCT8x8_1D_row_fast_6825();
void iDCT8x8_1D_row_fast_6826();
void iDCT8x8_1D_row_fast_6827();
void iDCT8x8_1D_row_fast_6828();
void iDCT8x8_1D_row_fast_6829();
void iDCT8x8_1D_row_fast_6830();
void iDCT8x8_1D_row_fast_6831();
void WEIGHTED_ROUND_ROBIN_Joiner_6823();
void iDCT8x8_1D_col_fast_6646();
void WEIGHTED_ROUND_ROBIN_Joiner_6695();
void AnonFilter_a2_6647();

#ifdef __cplusplus
}
#endif
#endif
