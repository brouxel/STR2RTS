#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=1152 on the compile command line
#else
#if BUF_SIZEMAX < 1152
#error BUF_SIZEMAX too small, it must be at least 1152
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_14014_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_14038_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_14039_t;
void AnonFilter_a0_14011();
void DUPLICATE_Splitter_14086();
void iDCT_2D_reference_coarse_14014();
void WEIGHTED_ROUND_ROBIN_Splitter_14094();
void AnonFilter_a3_14096();
void AnonFilter_a3_14097();
void AnonFilter_a3_14098();
void AnonFilter_a3_14099();
void AnonFilter_a3_14100();
void AnonFilter_a3_14101();
void AnonFilter_a3_14102();
void AnonFilter_a3_14103();
void AnonFilter_a3_14104();
void AnonFilter_a3_14105();
void AnonFilter_a3_14106();
void AnonFilter_a3_14107();
void AnonFilter_a3_14108();
void AnonFilter_a3_14109();
void AnonFilter_a3_14110();
void AnonFilter_a3_14111();
void AnonFilter_a3_14112();
void AnonFilter_a3_14113();
void AnonFilter_a3_14114();
void AnonFilter_a3_14115();
void AnonFilter_a3_14116();
void AnonFilter_a3_14117();
void AnonFilter_a3_14118();
void AnonFilter_a3_14119();
void WEIGHTED_ROUND_ROBIN_Joiner_14095();
void Pre_CollapsedDataParallel_1_14084();
void WEIGHTED_ROUND_ROBIN_Splitter_14120();
void iDCT_1D_reference_fine_14122();
void iDCT_1D_reference_fine_14123();
void iDCT_1D_reference_fine_14124();
void iDCT_1D_reference_fine_14125();
void iDCT_1D_reference_fine_14126();
void iDCT_1D_reference_fine_14127();
void iDCT_1D_reference_fine_14128();
void iDCT_1D_reference_fine_14129();
void WEIGHTED_ROUND_ROBIN_Joiner_14121();
void Post_CollapsedDataParallel_2_14085();
void WEIGHTED_ROUND_ROBIN_Splitter_14130();
void iDCT_1D_reference_fine_14132();
void iDCT_1D_reference_fine_14133();
void iDCT_1D_reference_fine_14134();
void iDCT_1D_reference_fine_14135();
void iDCT_1D_reference_fine_14136();
void iDCT_1D_reference_fine_14137();
void iDCT_1D_reference_fine_14138();
void iDCT_1D_reference_fine_14139();
void WEIGHTED_ROUND_ROBIN_Joiner_14131();
void WEIGHTED_ROUND_ROBIN_Splitter_14140();
void AnonFilter_a4_14142();
void AnonFilter_a4_14143();
void AnonFilter_a4_14144();
void AnonFilter_a4_14145();
void AnonFilter_a4_14146();
void AnonFilter_a4_14147();
void AnonFilter_a4_14148();
void AnonFilter_a4_14149();
void AnonFilter_a4_14150();
void AnonFilter_a4_14151();
void AnonFilter_a4_14152();
void AnonFilter_a4_14153();
void AnonFilter_a4_14154();
void AnonFilter_a4_14155();
void AnonFilter_a4_14156();
void AnonFilter_a4_14157();
void AnonFilter_a4_14158();
void AnonFilter_a4_14159();
void AnonFilter_a4_14160();
void AnonFilter_a4_14161();
void AnonFilter_a4_14162();
void AnonFilter_a4_14163();
void AnonFilter_a4_14164();
void AnonFilter_a4_14165();
void WEIGHTED_ROUND_ROBIN_Joiner_14141();
void WEIGHTED_ROUND_ROBIN_Splitter_14166();
void iDCT8x8_1D_row_fast_14168();
void iDCT8x8_1D_row_fast_14169();
void iDCT8x8_1D_row_fast_14170();
void iDCT8x8_1D_row_fast_14171();
void iDCT8x8_1D_row_fast_14172();
void iDCT8x8_1D_row_fast_14173();
void iDCT8x8_1D_row_fast_14174();
void iDCT8x8_1D_row_fast_14175();
void WEIGHTED_ROUND_ROBIN_Joiner_14167();
void iDCT8x8_1D_col_fast_14038();
void WEIGHTED_ROUND_ROBIN_Joiner_14087();
void AnonFilter_a2_14039();

#ifdef __cplusplus
}
#endif
#endif
