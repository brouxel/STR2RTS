#include "PEG48-iDCTcompare_nocache.h"

buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_6834_6841_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6763WEIGHTED_ROUND_ROBIN_Splitter_6772;
buffer_int_t AnonFilter_a0_6619DUPLICATE_Splitter_6694;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6703Pre_CollapsedDataParallel_1_6692;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_6835_6842_split[8];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[48];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6620_6696_6832_6839_join[3];
buffer_float_t Post_CollapsedDataParallel_2_6693WEIGHTED_ROUND_ROBIN_Splitter_6762;
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[48];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_6835_6842_join[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6620_6696_6832_6839_split[3];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_6834_6841_split[8];
buffer_int_t SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_6823iDCT8x8_1D_col_fast_6646;
buffer_float_t Pre_CollapsedDataParallel_1_6692WEIGHTED_ROUND_ROBIN_Splitter_6752;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_6753Post_CollapsedDataParallel_2_6693;
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[48];
buffer_int_t SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_6695AnonFilter_a2_6647;
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[48];


iDCT_2D_reference_coarse_6622_t iDCT_2D_reference_coarse_6622_s;
iDCT_2D_reference_coarse_6622_t iDCT_1D_reference_fine_6754_s;
iDCT_2D_reference_coarse_6622_t iDCT_1D_reference_fine_6755_s;
iDCT_2D_reference_coarse_6622_t iDCT_1D_reference_fine_6756_s;
iDCT_2D_reference_coarse_6622_t iDCT_1D_reference_fine_6757_s;
iDCT_2D_reference_coarse_6622_t iDCT_1D_reference_fine_6758_s;
iDCT_2D_reference_coarse_6622_t iDCT_1D_reference_fine_6759_s;
iDCT_2D_reference_coarse_6622_t iDCT_1D_reference_fine_6760_s;
iDCT_2D_reference_coarse_6622_t iDCT_1D_reference_fine_6761_s;
iDCT_2D_reference_coarse_6622_t iDCT_1D_reference_fine_6764_s;
iDCT_2D_reference_coarse_6622_t iDCT_1D_reference_fine_6765_s;
iDCT_2D_reference_coarse_6622_t iDCT_1D_reference_fine_6766_s;
iDCT_2D_reference_coarse_6622_t iDCT_1D_reference_fine_6767_s;
iDCT_2D_reference_coarse_6622_t iDCT_1D_reference_fine_6768_s;
iDCT_2D_reference_coarse_6622_t iDCT_1D_reference_fine_6769_s;
iDCT_2D_reference_coarse_6622_t iDCT_1D_reference_fine_6770_s;
iDCT_2D_reference_coarse_6622_t iDCT_1D_reference_fine_6771_s;
iDCT8x8_1D_col_fast_6646_t iDCT8x8_1D_col_fast_6646_s;
AnonFilter_a2_6647_t AnonFilter_a2_6647_s;

void AnonFilter_a0_6619(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&AnonFilter_a0_6619DUPLICATE_Splitter_6694, (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_2D_reference_coarse_6622(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_6622_s.coeff[k][j] * peek_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6620_6696_6832_6839_split[0], ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_6622_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6620_6696_6832_6839_join[0], ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6620_6696_6832_6839_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void AnonFilter_a3_6704(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[0], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[0])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6705(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[1], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[1])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6706(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[2], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[2])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6707(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[3], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[3])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6708(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[4], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[4])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6709(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[5], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[5])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6710(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[6], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[6])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6711(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[7], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[7])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6712(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[8], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[8])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6713(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[9], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[9])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6714(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[10], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[10])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6715(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[11], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[11])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6716(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[12], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[12])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6717(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[13], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[13])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6718(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[14], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[14])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6719(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[15], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[15])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6720(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[16], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[16])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6721(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[17], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[17])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6722(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[18], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[18])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6723(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[19], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[19])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6724(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[20], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[20])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6725(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[21], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[21])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6726(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[22], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[22])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6727(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[23], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[23])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6728(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[24], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[24])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6729(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[25], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[25])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6730(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[26], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[26])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6731(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[27], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[27])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6732(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[28], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[28])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6733(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[29], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[29])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6734(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[30], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[30])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6735(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[31], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[31])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6736(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[32], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[32])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6737(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[33], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[33])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6738(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[34], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[34])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6739(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[35], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[35])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6740(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[36], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[36])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6741(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[37], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[37])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6742(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[38], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[38])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6743(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[39], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[39])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6744(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[40], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[40])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6745(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[41], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[41])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6746(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[42], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[42])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6747(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[43], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[43])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6748(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[44], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[44])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6749(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[45], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[45])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6750(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[46], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[46])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_6751(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[47], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[47])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6702() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 48, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6620_6696_6832_6839_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6703() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 48, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6703Pre_CollapsedDataParallel_1_6692, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_6692(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = 0 ; 
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Pre_CollapsedDataParallel_1_6692WEIGHTED_ROUND_ROBIN_Splitter_6752, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_6703Pre_CollapsedDataParallel_1_6692, (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6703Pre_CollapsedDataParallel_1_6692) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6754(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6754_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6834_6841_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6834_6841_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6834_6841_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6755(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6755_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6834_6841_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6834_6841_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6834_6841_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6756(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6756_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6834_6841_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6834_6841_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6834_6841_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6757(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6757_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6834_6841_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6834_6841_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6834_6841_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6758(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6758_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6834_6841_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6834_6841_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6834_6841_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6759(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6759_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6834_6841_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6834_6841_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6834_6841_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6760(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6760_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6834_6841_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6834_6841_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6834_6841_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6761(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6761_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6834_6841_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6834_6841_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6834_6841_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6752() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6834_6841_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_6692WEIGHTED_ROUND_ROBIN_Splitter_6752));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6753() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6753Post_CollapsedDataParallel_2_6693, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6834_6841_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_6693(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Post_CollapsedDataParallel_2_6693WEIGHTED_ROUND_ROBIN_Splitter_6762, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_6753Post_CollapsedDataParallel_2_6693, (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6753Post_CollapsedDataParallel_2_6693) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6764(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6764_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6835_6842_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6835_6842_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6835_6842_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6765(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6765_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6835_6842_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6835_6842_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6835_6842_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6766(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6766_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6835_6842_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6835_6842_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6835_6842_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6767(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6767_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6835_6842_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6835_6842_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6835_6842_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6768(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6768_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6835_6842_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6835_6842_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6835_6842_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6769(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6769_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6835_6842_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6835_6842_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6835_6842_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6770(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6770_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6835_6842_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6835_6842_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6835_6842_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_6771(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_6771_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6835_6842_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6835_6842_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6835_6842_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6762() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6835_6842_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_6693WEIGHTED_ROUND_ROBIN_Splitter_6762));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6763() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_6763WEIGHTED_ROUND_ROBIN_Splitter_6772, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6835_6842_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4_6774(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[0], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[0]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6775(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[1], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[1]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6776(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[2], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[2]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6777(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[3], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[3]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6778(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[4], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[4]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6779(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[5], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[5]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6780(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[6], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[6]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6781(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[7], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[7]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6782(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[8], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[8]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6783(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[9], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[9]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6784(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[10], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[10]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6785(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[11], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[11]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6786(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[12], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[12]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6787(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[13], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[13]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6788(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[14], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[14]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6789(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[15], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[15]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6790(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[16], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[16]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6791(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[17], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[17]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6792(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[18], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[18]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6793(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[19], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[19]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6794(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[20], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[20]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6795(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[21], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[21]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6796(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[22], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[22]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6797(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[23], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[23]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6798(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[24], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[24]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6799(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[25], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[25]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6800(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[26], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[26]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6801(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[27], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[27]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6802(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[28], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[28]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6803(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[29], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[29]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6804(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[30], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[30]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6805(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[31], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[31]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6806(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[32], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[32]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6807(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[33], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[33]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6808(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[34], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[34]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6809(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[35], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[35]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6810(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[36], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[36]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6811(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[37], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[37]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6812(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[38], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[38]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6813(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[39], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[39]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6814(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[40], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[40]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6815(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[41], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[41]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6816(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[42], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[42]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6817(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[43], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[43]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6818(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[44], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[44]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6819(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[45], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[45]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6820(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[46], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[46]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_6821(){
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[47], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[47]) + 0.5)))) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6772() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 48, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_6763WEIGHTED_ROUND_ROBIN_Splitter_6772));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6773() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 4, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 48, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6620_6696_6832_6839_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast_6824(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[0], 0) ; 
		x1 = (peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[0], 4) << 11) ; 
		x2 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[0], 6) ; 
		x3 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[0], 2) ; 
		x4 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[0], 1) ; 
		x5 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[0], 7) ; 
		x6 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[0], 5) ; 
		x7 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[0], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[0], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[0], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[0], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[0], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[0], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[0], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[0], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[0], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[0], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_6825(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[1], 0) ; 
		x1 = (peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[1], 4) << 11) ; 
		x2 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[1], 6) ; 
		x3 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[1], 2) ; 
		x4 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[1], 1) ; 
		x5 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[1], 7) ; 
		x6 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[1], 5) ; 
		x7 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[1], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[1], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[1], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[1], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[1], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[1], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[1], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[1], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[1], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[1], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_6826(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[2], 0) ; 
		x1 = (peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[2], 4) << 11) ; 
		x2 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[2], 6) ; 
		x3 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[2], 2) ; 
		x4 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[2], 1) ; 
		x5 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[2], 7) ; 
		x6 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[2], 5) ; 
		x7 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[2], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[2], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[2], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[2], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[2], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[2], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[2], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[2], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[2], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[2], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_6827(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[3], 0) ; 
		x1 = (peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[3], 4) << 11) ; 
		x2 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[3], 6) ; 
		x3 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[3], 2) ; 
		x4 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[3], 1) ; 
		x5 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[3], 7) ; 
		x6 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[3], 5) ; 
		x7 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[3], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[3], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[3], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[3], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[3], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[3], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[3], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[3], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[3], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[3], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_6828(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[4], 0) ; 
		x1 = (peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[4], 4) << 11) ; 
		x2 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[4], 6) ; 
		x3 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[4], 2) ; 
		x4 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[4], 1) ; 
		x5 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[4], 7) ; 
		x6 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[4], 5) ; 
		x7 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[4], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[4], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[4], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[4], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[4], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[4], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[4], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[4], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[4], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[4], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_6829(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[5], 0) ; 
		x1 = (peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[5], 4) << 11) ; 
		x2 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[5], 6) ; 
		x3 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[5], 2) ; 
		x4 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[5], 1) ; 
		x5 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[5], 7) ; 
		x6 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[5], 5) ; 
		x7 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[5], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[5], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[5], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[5], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[5], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[5], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[5], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[5], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[5], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[5], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_6830(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[6], 0) ; 
		x1 = (peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[6], 4) << 11) ; 
		x2 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[6], 6) ; 
		x3 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[6], 2) ; 
		x4 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[6], 1) ; 
		x5 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[6], 7) ; 
		x6 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[6], 5) ; 
		x7 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[6], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[6], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[6], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[6], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[6], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[6], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[6], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[6], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[6], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[6], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_6831(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[7], 0) ; 
		x1 = (peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[7], 4) << 11) ; 
		x2 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[7], 6) ; 
		x3 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[7], 2) ; 
		x4 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[7], 1) ; 
		x5 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[7], 7) ; 
		x6 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[7], 5) ; 
		x7 = peek_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[7], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[7], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[7], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[7], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[7], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[7], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[7], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[7], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[7], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[7], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_6822() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6620_6696_6832_6839_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6823() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_6823iDCT8x8_1D_col_fast_6646, pop_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast_6646(){
	FOR(uint32_t, __iter_steady_, 0, <, 3, __iter_steady_++) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_6823iDCT8x8_1D_col_fast_6646, (c + 0)) ; 
			x1 = (peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_6823iDCT8x8_1D_col_fast_6646, (c + 32)) << 8) ; 
			x2 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_6823iDCT8x8_1D_col_fast_6646, (c + 48)) ; 
			x3 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_6823iDCT8x8_1D_col_fast_6646, (c + 16)) ; 
			x4 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_6823iDCT8x8_1D_col_fast_6646, (c + 8)) ; 
			x5 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_6823iDCT8x8_1D_col_fast_6646, (c + 56)) ; 
			x6 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_6823iDCT8x8_1D_col_fast_6646, (c + 40)) ; 
			x7 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_6823iDCT8x8_1D_col_fast_6646, (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_6646_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_6646_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_6646_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_6646_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_6646_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_6646_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_6646_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_6646_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_6646_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_6823iDCT8x8_1D_col_fast_6646) ; 
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6620_6696_6832_6839_join[2], iDCT8x8_1D_col_fast_6646_s.buffer[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void DUPLICATE_Splitter_6694() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 192, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_6619DUPLICATE_Splitter_6694);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6620_6696_6832_6839_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_6695() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 192, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_6695AnonFilter_a2_6647, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6620_6696_6832_6839_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2_6647(){
	FOR(uint32_t, __iter_steady_, 0, <, 192, __iter_steady_++) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_6695AnonFilter_a2_6647) ; 
		reffine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_6695AnonFilter_a2_6647) ; 
		fastfine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_6695AnonFilter_a2_6647) ; 
		AnonFilter_a2_6647_s.count = (AnonFilter_a2_6647_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_6647_s.errors = (AnonFilter_a2_6647_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_6647_s.errors / AnonFilter_a2_6647_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_6647_s.errors = (AnonFilter_a2_6647_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_6647_s.errors / AnonFilter_a2_6647_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6834_6841_join[__iter_init_0_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6763WEIGHTED_ROUND_ROBIN_Splitter_6772);
	init_buffer_int(&AnonFilter_a0_6619DUPLICATE_Splitter_6694);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6703Pre_CollapsedDataParallel_1_6692);
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6835_6842_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 48, __iter_init_2_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 3, __iter_init_3_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6620_6696_6832_6839_join[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_6693WEIGHTED_ROUND_ROBIN_Splitter_6762);
	FOR(int, __iter_init_4_, 0, <, 48, __iter_init_4_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_join[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_6835_6842_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 3, __iter_init_6_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_6620_6696_6832_6839_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_6834_6841_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_split[__iter_init_8_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_6823iDCT8x8_1D_col_fast_6646);
	init_buffer_float(&Pre_CollapsedDataParallel_1_6692WEIGHTED_ROUND_ROBIN_Splitter_6752);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_6753Post_CollapsedDataParallel_2_6693);
	FOR(int, __iter_init_9_, 0, <, 48, __iter_init_9_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_6833_6840_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_int(&SplitJoin120_iDCT8x8_1D_row_fast_Fiss_6837_6844_join[__iter_init_10_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_6695AnonFilter_a2_6647);
	FOR(int, __iter_init_11_, 0, <, 48, __iter_init_11_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_6836_6843_split[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_6622
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_6622_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6754
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6754_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6755
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6755_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6756
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6756_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6757
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6757_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6758
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6758_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6759
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6759_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6760
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6760_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6761
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6761_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6764
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6764_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6765
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6765_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6766
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6766_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6767
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6767_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6768
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6768_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6769
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6769_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6770
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6770_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_6771
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_6771_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_6647
	 {
	AnonFilter_a2_6647_s.count = 0.0 ; 
	AnonFilter_a2_6647_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_6619();
		DUPLICATE_Splitter_6694();
			iDCT_2D_reference_coarse_6622();
			WEIGHTED_ROUND_ROBIN_Splitter_6702();
				AnonFilter_a3_6704();
				AnonFilter_a3_6705();
				AnonFilter_a3_6706();
				AnonFilter_a3_6707();
				AnonFilter_a3_6708();
				AnonFilter_a3_6709();
				AnonFilter_a3_6710();
				AnonFilter_a3_6711();
				AnonFilter_a3_6712();
				AnonFilter_a3_6713();
				AnonFilter_a3_6714();
				AnonFilter_a3_6715();
				AnonFilter_a3_6716();
				AnonFilter_a3_6717();
				AnonFilter_a3_6718();
				AnonFilter_a3_6719();
				AnonFilter_a3_6720();
				AnonFilter_a3_6721();
				AnonFilter_a3_6722();
				AnonFilter_a3_6723();
				AnonFilter_a3_6724();
				AnonFilter_a3_6725();
				AnonFilter_a3_6726();
				AnonFilter_a3_6727();
				AnonFilter_a3_6728();
				AnonFilter_a3_6729();
				AnonFilter_a3_6730();
				AnonFilter_a3_6731();
				AnonFilter_a3_6732();
				AnonFilter_a3_6733();
				AnonFilter_a3_6734();
				AnonFilter_a3_6735();
				AnonFilter_a3_6736();
				AnonFilter_a3_6737();
				AnonFilter_a3_6738();
				AnonFilter_a3_6739();
				AnonFilter_a3_6740();
				AnonFilter_a3_6741();
				AnonFilter_a3_6742();
				AnonFilter_a3_6743();
				AnonFilter_a3_6744();
				AnonFilter_a3_6745();
				AnonFilter_a3_6746();
				AnonFilter_a3_6747();
				AnonFilter_a3_6748();
				AnonFilter_a3_6749();
				AnonFilter_a3_6750();
				AnonFilter_a3_6751();
			WEIGHTED_ROUND_ROBIN_Joiner_6703();
			Pre_CollapsedDataParallel_1_6692();
			WEIGHTED_ROUND_ROBIN_Splitter_6752();
				iDCT_1D_reference_fine_6754();
				iDCT_1D_reference_fine_6755();
				iDCT_1D_reference_fine_6756();
				iDCT_1D_reference_fine_6757();
				iDCT_1D_reference_fine_6758();
				iDCT_1D_reference_fine_6759();
				iDCT_1D_reference_fine_6760();
				iDCT_1D_reference_fine_6761();
			WEIGHTED_ROUND_ROBIN_Joiner_6753();
			Post_CollapsedDataParallel_2_6693();
			WEIGHTED_ROUND_ROBIN_Splitter_6762();
				iDCT_1D_reference_fine_6764();
				iDCT_1D_reference_fine_6765();
				iDCT_1D_reference_fine_6766();
				iDCT_1D_reference_fine_6767();
				iDCT_1D_reference_fine_6768();
				iDCT_1D_reference_fine_6769();
				iDCT_1D_reference_fine_6770();
				iDCT_1D_reference_fine_6771();
			WEIGHTED_ROUND_ROBIN_Joiner_6763();
			WEIGHTED_ROUND_ROBIN_Splitter_6772();
				AnonFilter_a4_6774();
				AnonFilter_a4_6775();
				AnonFilter_a4_6776();
				AnonFilter_a4_6777();
				AnonFilter_a4_6778();
				AnonFilter_a4_6779();
				AnonFilter_a4_6780();
				AnonFilter_a4_6781();
				AnonFilter_a4_6782();
				AnonFilter_a4_6783();
				AnonFilter_a4_6784();
				AnonFilter_a4_6785();
				AnonFilter_a4_6786();
				AnonFilter_a4_6787();
				AnonFilter_a4_6788();
				AnonFilter_a4_6789();
				AnonFilter_a4_6790();
				AnonFilter_a4_6791();
				AnonFilter_a4_6792();
				AnonFilter_a4_6793();
				AnonFilter_a4_6794();
				AnonFilter_a4_6795();
				AnonFilter_a4_6796();
				AnonFilter_a4_6797();
				AnonFilter_a4_6798();
				AnonFilter_a4_6799();
				AnonFilter_a4_6800();
				AnonFilter_a4_6801();
				AnonFilter_a4_6802();
				AnonFilter_a4_6803();
				AnonFilter_a4_6804();
				AnonFilter_a4_6805();
				AnonFilter_a4_6806();
				AnonFilter_a4_6807();
				AnonFilter_a4_6808();
				AnonFilter_a4_6809();
				AnonFilter_a4_6810();
				AnonFilter_a4_6811();
				AnonFilter_a4_6812();
				AnonFilter_a4_6813();
				AnonFilter_a4_6814();
				AnonFilter_a4_6815();
				AnonFilter_a4_6816();
				AnonFilter_a4_6817();
				AnonFilter_a4_6818();
				AnonFilter_a4_6819();
				AnonFilter_a4_6820();
				AnonFilter_a4_6821();
			WEIGHTED_ROUND_ROBIN_Joiner_6773();
			WEIGHTED_ROUND_ROBIN_Splitter_6822();
				iDCT8x8_1D_row_fast_6824();
				iDCT8x8_1D_row_fast_6825();
				iDCT8x8_1D_row_fast_6826();
				iDCT8x8_1D_row_fast_6827();
				iDCT8x8_1D_row_fast_6828();
				iDCT8x8_1D_row_fast_6829();
				iDCT8x8_1D_row_fast_6830();
				iDCT8x8_1D_row_fast_6831();
			WEIGHTED_ROUND_ROBIN_Joiner_6823();
			iDCT8x8_1D_col_fast_6646();
		WEIGHTED_ROUND_ROBIN_Joiner_6695();
		AnonFilter_a2_6647();
	ENDFOR
	return EXIT_SUCCESS;
}
