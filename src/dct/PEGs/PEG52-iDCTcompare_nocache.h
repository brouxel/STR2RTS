#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=4992 on the compile command line
#else
#if BUF_SIZEMAX < 4992
#error BUF_SIZEMAX too small, it must be at least 4992
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_5166_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_5190_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_5191_t;
void AnonFilter_a0_5163();
void DUPLICATE_Splitter_5238();
void iDCT_2D_reference_coarse_5166();
void WEIGHTED_ROUND_ROBIN_Splitter_5246();
void AnonFilter_a3_5248();
void AnonFilter_a3_5249();
void AnonFilter_a3_5250();
void AnonFilter_a3_5251();
void AnonFilter_a3_5252();
void AnonFilter_a3_5253();
void AnonFilter_a3_5254();
void AnonFilter_a3_5255();
void AnonFilter_a3_5256();
void AnonFilter_a3_5257();
void AnonFilter_a3_5258();
void AnonFilter_a3_5259();
void AnonFilter_a3_5260();
void AnonFilter_a3_5261();
void AnonFilter_a3_5262();
void AnonFilter_a3_5263();
void AnonFilter_a3_5264();
void AnonFilter_a3_5265();
void AnonFilter_a3_5266();
void AnonFilter_a3_5267();
void AnonFilter_a3_5268();
void AnonFilter_a3_5269();
void AnonFilter_a3_5270();
void AnonFilter_a3_5271();
void AnonFilter_a3_5272();
void AnonFilter_a3_5273();
void AnonFilter_a3_5274();
void AnonFilter_a3_5275();
void AnonFilter_a3_5276();
void AnonFilter_a3_5277();
void AnonFilter_a3_5278();
void AnonFilter_a3_5279();
void AnonFilter_a3_5280();
void AnonFilter_a3_5281();
void AnonFilter_a3_5282();
void AnonFilter_a3_5283();
void AnonFilter_a3_5284();
void AnonFilter_a3_5285();
void AnonFilter_a3_5286();
void AnonFilter_a3_5287();
void AnonFilter_a3_5288();
void AnonFilter_a3_5289();
void AnonFilter_a3_5290();
void AnonFilter_a3_5291();
void AnonFilter_a3_5292();
void AnonFilter_a3_5293();
void AnonFilter_a3_5294();
void AnonFilter_a3_5295();
void AnonFilter_a3_5296();
void AnonFilter_a3_5297();
void AnonFilter_a3_5298();
void AnonFilter_a3_5299();
void WEIGHTED_ROUND_ROBIN_Joiner_5247();
void Pre_CollapsedDataParallel_1_5236();
void WEIGHTED_ROUND_ROBIN_Splitter_5300();
void iDCT_1D_reference_fine_5302();
void iDCT_1D_reference_fine_5303();
void iDCT_1D_reference_fine_5304();
void iDCT_1D_reference_fine_5305();
void iDCT_1D_reference_fine_5306();
void iDCT_1D_reference_fine_5307();
void iDCT_1D_reference_fine_5308();
void iDCT_1D_reference_fine_5309();
void WEIGHTED_ROUND_ROBIN_Joiner_5301();
void Post_CollapsedDataParallel_2_5237();
void WEIGHTED_ROUND_ROBIN_Splitter_5310();
void iDCT_1D_reference_fine_5312();
void iDCT_1D_reference_fine_5313();
void iDCT_1D_reference_fine_5314();
void iDCT_1D_reference_fine_5315();
void iDCT_1D_reference_fine_5316();
void iDCT_1D_reference_fine_5317();
void iDCT_1D_reference_fine_5318();
void iDCT_1D_reference_fine_5319();
void WEIGHTED_ROUND_ROBIN_Joiner_5311();
void WEIGHTED_ROUND_ROBIN_Splitter_5320();
void AnonFilter_a4_5322();
void AnonFilter_a4_5323();
void AnonFilter_a4_5324();
void AnonFilter_a4_5325();
void AnonFilter_a4_5326();
void AnonFilter_a4_5327();
void AnonFilter_a4_5328();
void AnonFilter_a4_5329();
void AnonFilter_a4_5330();
void AnonFilter_a4_5331();
void AnonFilter_a4_5332();
void AnonFilter_a4_5333();
void AnonFilter_a4_5334();
void AnonFilter_a4_5335();
void AnonFilter_a4_5336();
void AnonFilter_a4_5337();
void AnonFilter_a4_5338();
void AnonFilter_a4_5339();
void AnonFilter_a4_5340();
void AnonFilter_a4_5341();
void AnonFilter_a4_5342();
void AnonFilter_a4_5343();
void AnonFilter_a4_5344();
void AnonFilter_a4_5345();
void AnonFilter_a4_5346();
void AnonFilter_a4_5347();
void AnonFilter_a4_5348();
void AnonFilter_a4_5349();
void AnonFilter_a4_5350();
void AnonFilter_a4_5351();
void AnonFilter_a4_5352();
void AnonFilter_a4_5353();
void AnonFilter_a4_5354();
void AnonFilter_a4_5355();
void AnonFilter_a4_5356();
void AnonFilter_a4_5357();
void AnonFilter_a4_5358();
void AnonFilter_a4_5359();
void AnonFilter_a4_5360();
void AnonFilter_a4_5361();
void AnonFilter_a4_5362();
void AnonFilter_a4_5363();
void AnonFilter_a4_5364();
void AnonFilter_a4_5365();
void AnonFilter_a4_5366();
void AnonFilter_a4_5367();
void AnonFilter_a4_5368();
void AnonFilter_a4_5369();
void AnonFilter_a4_5370();
void AnonFilter_a4_5371();
void AnonFilter_a4_5372();
void AnonFilter_a4_5373();
void WEIGHTED_ROUND_ROBIN_Joiner_5321();
void WEIGHTED_ROUND_ROBIN_Splitter_5374();
void iDCT8x8_1D_row_fast_5376();
void iDCT8x8_1D_row_fast_5377();
void iDCT8x8_1D_row_fast_5378();
void iDCT8x8_1D_row_fast_5379();
void iDCT8x8_1D_row_fast_5380();
void iDCT8x8_1D_row_fast_5381();
void iDCT8x8_1D_row_fast_5382();
void iDCT8x8_1D_row_fast_5383();
void WEIGHTED_ROUND_ROBIN_Joiner_5375();
void iDCT8x8_1D_col_fast_5190();
void WEIGHTED_ROUND_ROBIN_Joiner_5239();
void AnonFilter_a2_5191();

#ifdef __cplusplus
}
#endif
#endif
