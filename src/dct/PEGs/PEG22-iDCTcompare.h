#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=4224 on the compile command line
#else
#if BUF_SIZEMAX < 4224
#error BUF_SIZEMAX too small, it must be at least 4224
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_14526_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_14550_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_14551_t;
void AnonFilter_a0(buffer_int_t *chanout);
void AnonFilter_a0_14523();
void DUPLICATE_Splitter_14598();
void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT_2D_reference_coarse_14526();
void WEIGHTED_ROUND_ROBIN_Splitter_14606();
void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout);
void AnonFilter_a3_14608();
void AnonFilter_a3_14609();
void AnonFilter_a3_14610();
void AnonFilter_a3_14611();
void AnonFilter_a3_14612();
void AnonFilter_a3_14613();
void AnonFilter_a3_14614();
void AnonFilter_a3_14615();
void AnonFilter_a3_14616();
void AnonFilter_a3_14617();
void AnonFilter_a3_14618();
void AnonFilter_a3_14619();
void AnonFilter_a3_14620();
void AnonFilter_a3_14621();
void AnonFilter_a3_14622();
void AnonFilter_a3_14623();
void AnonFilter_a3_14624();
void AnonFilter_a3_14625();
void AnonFilter_a3_14626();
void AnonFilter_a3_14627();
void AnonFilter_a3_14628();
void AnonFilter_a3_14629();
void WEIGHTED_ROUND_ROBIN_Joiner_14607();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_14596();
void WEIGHTED_ROUND_ROBIN_Splitter_14630();
void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout);
void iDCT_1D_reference_fine_14632();
void iDCT_1D_reference_fine_14633();
void iDCT_1D_reference_fine_14634();
void iDCT_1D_reference_fine_14635();
void iDCT_1D_reference_fine_14636();
void iDCT_1D_reference_fine_14637();
void iDCT_1D_reference_fine_14638();
void iDCT_1D_reference_fine_14639();
void WEIGHTED_ROUND_ROBIN_Joiner_14631();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_14597();
void WEIGHTED_ROUND_ROBIN_Splitter_14640();
void iDCT_1D_reference_fine_14642();
void iDCT_1D_reference_fine_14643();
void iDCT_1D_reference_fine_14644();
void iDCT_1D_reference_fine_14645();
void iDCT_1D_reference_fine_14646();
void iDCT_1D_reference_fine_14647();
void iDCT_1D_reference_fine_14648();
void iDCT_1D_reference_fine_14649();
void WEIGHTED_ROUND_ROBIN_Joiner_14641();
void WEIGHTED_ROUND_ROBIN_Splitter_14650();
void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout);
void AnonFilter_a4_14652();
void AnonFilter_a4_14653();
void AnonFilter_a4_14654();
void AnonFilter_a4_14655();
void AnonFilter_a4_14656();
void AnonFilter_a4_14657();
void AnonFilter_a4_14658();
void AnonFilter_a4_14659();
void AnonFilter_a4_14660();
void AnonFilter_a4_14661();
void AnonFilter_a4_14662();
void AnonFilter_a4_14663();
void AnonFilter_a4_14664();
void AnonFilter_a4_14665();
void AnonFilter_a4_14666();
void AnonFilter_a4_14667();
void AnonFilter_a4_14668();
void AnonFilter_a4_14669();
void AnonFilter_a4_14670();
void AnonFilter_a4_14671();
void AnonFilter_a4_14672();
void AnonFilter_a4_14673();
void WEIGHTED_ROUND_ROBIN_Joiner_14651();
void WEIGHTED_ROUND_ROBIN_Splitter_14674();
void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_row_fast_14676();
void iDCT8x8_1D_row_fast_14677();
void iDCT8x8_1D_row_fast_14678();
void iDCT8x8_1D_row_fast_14679();
void iDCT8x8_1D_row_fast_14680();
void iDCT8x8_1D_row_fast_14681();
void iDCT8x8_1D_row_fast_14682();
void iDCT8x8_1D_row_fast_14683();
void WEIGHTED_ROUND_ROBIN_Joiner_14675();
void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_col_fast_14550();
void WEIGHTED_ROUND_ROBIN_Joiner_14599();
void AnonFilter_a2(buffer_int_t *chanin);
void AnonFilter_a2_14551();

#ifdef __cplusplus
}
#endif
#endif
