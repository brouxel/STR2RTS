#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=1920 on the compile command line
#else
#if BUF_SIZEMAX < 1920
#error BUF_SIZEMAX too small, it must be at least 1920
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_15022_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_15046_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_15047_t;
void AnonFilter_a0_15019();
void DUPLICATE_Splitter_15094();
void iDCT_2D_reference_coarse_15022();
void WEIGHTED_ROUND_ROBIN_Splitter_15102();
void AnonFilter_a3_15104();
void AnonFilter_a3_15105();
void AnonFilter_a3_15106();
void AnonFilter_a3_15107();
void AnonFilter_a3_15108();
void AnonFilter_a3_15109();
void AnonFilter_a3_15110();
void AnonFilter_a3_15111();
void AnonFilter_a3_15112();
void AnonFilter_a3_15113();
void AnonFilter_a3_15114();
void AnonFilter_a3_15115();
void AnonFilter_a3_15116();
void AnonFilter_a3_15117();
void AnonFilter_a3_15118();
void AnonFilter_a3_15119();
void AnonFilter_a3_15120();
void AnonFilter_a3_15121();
void AnonFilter_a3_15122();
void AnonFilter_a3_15123();
void WEIGHTED_ROUND_ROBIN_Joiner_15103();
void Pre_CollapsedDataParallel_1_15092();
void WEIGHTED_ROUND_ROBIN_Splitter_15124();
void iDCT_1D_reference_fine_15126();
void iDCT_1D_reference_fine_15127();
void iDCT_1D_reference_fine_15128();
void iDCT_1D_reference_fine_15129();
void iDCT_1D_reference_fine_15130();
void iDCT_1D_reference_fine_15131();
void iDCT_1D_reference_fine_15132();
void iDCT_1D_reference_fine_15133();
void WEIGHTED_ROUND_ROBIN_Joiner_15125();
void Post_CollapsedDataParallel_2_15093();
void WEIGHTED_ROUND_ROBIN_Splitter_15134();
void iDCT_1D_reference_fine_15136();
void iDCT_1D_reference_fine_15137();
void iDCT_1D_reference_fine_15138();
void iDCT_1D_reference_fine_15139();
void iDCT_1D_reference_fine_15140();
void iDCT_1D_reference_fine_15141();
void iDCT_1D_reference_fine_15142();
void iDCT_1D_reference_fine_15143();
void WEIGHTED_ROUND_ROBIN_Joiner_15135();
void WEIGHTED_ROUND_ROBIN_Splitter_15144();
void AnonFilter_a4_15146();
void AnonFilter_a4_15147();
void AnonFilter_a4_15148();
void AnonFilter_a4_15149();
void AnonFilter_a4_15150();
void AnonFilter_a4_15151();
void AnonFilter_a4_15152();
void AnonFilter_a4_15153();
void AnonFilter_a4_15154();
void AnonFilter_a4_15155();
void AnonFilter_a4_15156();
void AnonFilter_a4_15157();
void AnonFilter_a4_15158();
void AnonFilter_a4_15159();
void AnonFilter_a4_15160();
void AnonFilter_a4_15161();
void AnonFilter_a4_15162();
void AnonFilter_a4_15163();
void AnonFilter_a4_15164();
void AnonFilter_a4_15165();
void WEIGHTED_ROUND_ROBIN_Joiner_15145();
void WEIGHTED_ROUND_ROBIN_Splitter_15166();
void iDCT8x8_1D_row_fast_15168();
void iDCT8x8_1D_row_fast_15169();
void iDCT8x8_1D_row_fast_15170();
void iDCT8x8_1D_row_fast_15171();
void iDCT8x8_1D_row_fast_15172();
void iDCT8x8_1D_row_fast_15173();
void iDCT8x8_1D_row_fast_15174();
void iDCT8x8_1D_row_fast_15175();
void WEIGHTED_ROUND_ROBIN_Joiner_15167();
void iDCT8x8_1D_col_fast_15046();
void WEIGHTED_ROUND_ROBIN_Joiner_15095();
void AnonFilter_a2_15047();

#ifdef __cplusplus
}
#endif
#endif
