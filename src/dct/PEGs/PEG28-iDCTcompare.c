#include "PEG28-iDCTcompare.h"

buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12940_13016_13112_13119_split[3];
buffer_int_t AnonFilter_a0_12939DUPLICATE_Splitter_13014;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_13053Post_CollapsedDataParallel_2_13013;
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_13116_13123_join[28];
buffer_float_t Pre_CollapsedDataParallel_1_13012WEIGHTED_ROUND_ROBIN_Splitter_13052;
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_13113_13120_join[28];
buffer_float_t Post_CollapsedDataParallel_2_13013WEIGHTED_ROUND_ROBIN_Splitter_13062;
buffer_int_t SplitJoin80_iDCT8x8_1D_row_fast_Fiss_13117_13124_split[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_13115_13122_join[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_13103iDCT8x8_1D_col_fast_12966;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12940_13016_13112_13119_join[3];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_13015AnonFilter_a2_12967;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_13115_13122_split[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_13114_13121_split[8];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_13116_13123_split[28];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_13023Pre_CollapsedDataParallel_1_13012;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_13063WEIGHTED_ROUND_ROBIN_Splitter_13072;
buffer_int_t SplitJoin80_iDCT8x8_1D_row_fast_Fiss_13117_13124_join[8];
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_13113_13120_split[28];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_13114_13121_join[8];


iDCT_2D_reference_coarse_12942_t iDCT_2D_reference_coarse_12942_s;
iDCT_2D_reference_coarse_12942_t iDCT_1D_reference_fine_13054_s;
iDCT_2D_reference_coarse_12942_t iDCT_1D_reference_fine_13055_s;
iDCT_2D_reference_coarse_12942_t iDCT_1D_reference_fine_13056_s;
iDCT_2D_reference_coarse_12942_t iDCT_1D_reference_fine_13057_s;
iDCT_2D_reference_coarse_12942_t iDCT_1D_reference_fine_13058_s;
iDCT_2D_reference_coarse_12942_t iDCT_1D_reference_fine_13059_s;
iDCT_2D_reference_coarse_12942_t iDCT_1D_reference_fine_13060_s;
iDCT_2D_reference_coarse_12942_t iDCT_1D_reference_fine_13061_s;
iDCT_2D_reference_coarse_12942_t iDCT_1D_reference_fine_13064_s;
iDCT_2D_reference_coarse_12942_t iDCT_1D_reference_fine_13065_s;
iDCT_2D_reference_coarse_12942_t iDCT_1D_reference_fine_13066_s;
iDCT_2D_reference_coarse_12942_t iDCT_1D_reference_fine_13067_s;
iDCT_2D_reference_coarse_12942_t iDCT_1D_reference_fine_13068_s;
iDCT_2D_reference_coarse_12942_t iDCT_1D_reference_fine_13069_s;
iDCT_2D_reference_coarse_12942_t iDCT_1D_reference_fine_13070_s;
iDCT_2D_reference_coarse_12942_t iDCT_1D_reference_fine_13071_s;
iDCT8x8_1D_col_fast_12966_t iDCT8x8_1D_col_fast_12966_s;
AnonFilter_a2_12967_t AnonFilter_a2_12967_s;

void AnonFilter_a0(buffer_int_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}


void AnonFilter_a0_12939() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		AnonFilter_a0(&(AnonFilter_a0_12939DUPLICATE_Splitter_13014));
	ENDFOR
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_12942_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_12942_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&(*chanout), ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_2D_reference_coarse_12942() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12940_13016_13112_13119_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12940_13016_13112_13119_join[0]));
	ENDFOR
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_13024() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_join[0]));
	ENDFOR
}

void AnonFilter_a3_13025() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_join[1]));
	ENDFOR
}

void AnonFilter_a3_13026() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_join[2]));
	ENDFOR
}

void AnonFilter_a3_13027() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_join[3]));
	ENDFOR
}

void AnonFilter_a3_13028() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_join[4]));
	ENDFOR
}

void AnonFilter_a3_13029() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_join[5]));
	ENDFOR
}

void AnonFilter_a3_13030() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_join[6]));
	ENDFOR
}

void AnonFilter_a3_13031() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_join[7]));
	ENDFOR
}

void AnonFilter_a3_13032() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_split[8]), &(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_join[8]));
	ENDFOR
}

void AnonFilter_a3_13033() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_split[9]), &(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_join[9]));
	ENDFOR
}

void AnonFilter_a3_13034() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_split[10]), &(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_join[10]));
	ENDFOR
}

void AnonFilter_a3_13035() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_split[11]), &(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_join[11]));
	ENDFOR
}

void AnonFilter_a3_13036() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_split[12]), &(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_join[12]));
	ENDFOR
}

void AnonFilter_a3_13037() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_split[13]), &(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_join[13]));
	ENDFOR
}

void AnonFilter_a3_13038() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_split[14]), &(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_join[14]));
	ENDFOR
}

void AnonFilter_a3_13039() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_split[15]), &(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_join[15]));
	ENDFOR
}

void AnonFilter_a3_13040() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_split[16]), &(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_join[16]));
	ENDFOR
}

void AnonFilter_a3_13041() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_split[17]), &(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_join[17]));
	ENDFOR
}

void AnonFilter_a3_13042() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_split[18]), &(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_join[18]));
	ENDFOR
}

void AnonFilter_a3_13043() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_split[19]), &(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_join[19]));
	ENDFOR
}

void AnonFilter_a3_13044() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_split[20]), &(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_join[20]));
	ENDFOR
}

void AnonFilter_a3_13045() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_split[21]), &(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_join[21]));
	ENDFOR
}

void AnonFilter_a3_13046() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_split[22]), &(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_join[22]));
	ENDFOR
}

void AnonFilter_a3_13047() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_split[23]), &(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_join[23]));
	ENDFOR
}

void AnonFilter_a3_13048() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_split[24]), &(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_join[24]));
	ENDFOR
}

void AnonFilter_a3_13049() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_split[25]), &(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_join[25]));
	ENDFOR
}

void AnonFilter_a3_13050() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_split[26]), &(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_join[26]));
	ENDFOR
}

void AnonFilter_a3_13051() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_split[27]), &(SplitJoin3_AnonFilter_a3_Fiss_13113_13120_join[27]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_13022() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 28, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_13113_13120_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12940_13016_13112_13119_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_13023() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 28, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_13023Pre_CollapsedDataParallel_1_13012, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_13113_13120_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_13012() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_13023Pre_CollapsedDataParallel_1_13012), &(Pre_CollapsedDataParallel_1_13012WEIGHTED_ROUND_ROBIN_Splitter_13052));
	ENDFOR
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_13054_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
			}
			ENDFOR
			push_float(&(*chanout), tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_1D_reference_fine_13054() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_13114_13121_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_13114_13121_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_13055() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_13114_13121_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_13114_13121_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_13056() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_13114_13121_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_13114_13121_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_13057() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_13114_13121_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_13114_13121_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_13058() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_13114_13121_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_13114_13121_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_13059() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_13114_13121_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_13114_13121_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_13060() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_13114_13121_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_13114_13121_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_13061() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_13114_13121_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_13114_13121_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_13052() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13114_13121_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_13012WEIGHTED_ROUND_ROBIN_Splitter_13052));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_13053() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_13053Post_CollapsedDataParallel_2_13013, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13114_13121_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_13013() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_13053Post_CollapsedDataParallel_2_13013), &(Post_CollapsedDataParallel_2_13013WEIGHTED_ROUND_ROBIN_Splitter_13062));
	ENDFOR
}

void iDCT_1D_reference_fine_13064() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_13115_13122_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_13115_13122_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_13065() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_13115_13122_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_13115_13122_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_13066() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_13115_13122_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_13115_13122_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_13067() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_13115_13122_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_13115_13122_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_13068() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_13115_13122_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_13115_13122_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_13069() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_13115_13122_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_13115_13122_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_13070() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_13115_13122_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_13115_13122_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_13071() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_13115_13122_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_13115_13122_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_13062() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13115_13122_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_13013WEIGHTED_ROUND_ROBIN_Splitter_13062));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_13063() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_13063WEIGHTED_ROUND_ROBIN_Splitter_13072, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13115_13122_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_13074() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_join[0]));
	ENDFOR
}

void AnonFilter_a4_13075() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_join[1]));
	ENDFOR
}

void AnonFilter_a4_13076() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_join[2]));
	ENDFOR
}

void AnonFilter_a4_13077() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_join[3]));
	ENDFOR
}

void AnonFilter_a4_13078() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_join[4]));
	ENDFOR
}

void AnonFilter_a4_13079() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_join[5]));
	ENDFOR
}

void AnonFilter_a4_13080() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_join[6]));
	ENDFOR
}

void AnonFilter_a4_13081() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_join[7]));
	ENDFOR
}

void AnonFilter_a4_13082() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_split[8]), &(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_join[8]));
	ENDFOR
}

void AnonFilter_a4_13083() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_split[9]), &(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_join[9]));
	ENDFOR
}

void AnonFilter_a4_13084() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_split[10]), &(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_join[10]));
	ENDFOR
}

void AnonFilter_a4_13085() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_split[11]), &(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_join[11]));
	ENDFOR
}

void AnonFilter_a4_13086() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_split[12]), &(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_join[12]));
	ENDFOR
}

void AnonFilter_a4_13087() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_split[13]), &(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_join[13]));
	ENDFOR
}

void AnonFilter_a4_13088() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_split[14]), &(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_join[14]));
	ENDFOR
}

void AnonFilter_a4_13089() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_split[15]), &(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_join[15]));
	ENDFOR
}

void AnonFilter_a4_13090() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_split[16]), &(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_join[16]));
	ENDFOR
}

void AnonFilter_a4_13091() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_split[17]), &(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_join[17]));
	ENDFOR
}

void AnonFilter_a4_13092() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_split[18]), &(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_join[18]));
	ENDFOR
}

void AnonFilter_a4_13093() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_split[19]), &(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_join[19]));
	ENDFOR
}

void AnonFilter_a4_13094() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_split[20]), &(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_join[20]));
	ENDFOR
}

void AnonFilter_a4_13095() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_split[21]), &(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_join[21]));
	ENDFOR
}

void AnonFilter_a4_13096() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_split[22]), &(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_join[22]));
	ENDFOR
}

void AnonFilter_a4_13097() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_split[23]), &(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_join[23]));
	ENDFOR
}

void AnonFilter_a4_13098() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_split[24]), &(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_join[24]));
	ENDFOR
}

void AnonFilter_a4_13099() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_split[25]), &(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_join[25]));
	ENDFOR
}

void AnonFilter_a4_13100() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_split[26]), &(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_join[26]));
	ENDFOR
}

void AnonFilter_a4_13101() {
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_split[27]), &(SplitJoin9_AnonFilter_a4_Fiss_13116_13123_join[27]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_13072() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 28, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_13116_13123_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_13063WEIGHTED_ROUND_ROBIN_Splitter_13072));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_13073() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 16, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 28, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12940_13016_13112_13119_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_13116_13123_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), 0) ; 
		x1 = (peek_int(&(*chanin), 4) << 11) ; 
		x2 = peek_int(&(*chanin), 6) ; 
		x3 = peek_int(&(*chanin), 2) ; 
		x4 = peek_int(&(*chanin), 1) ; 
		x5 = peek_int(&(*chanin), 7) ; 
		x6 = peek_int(&(*chanin), 5) ; 
		x7 = peek_int(&(*chanin), 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&(*chanout), x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
			push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
			push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
			push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
			push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
			push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
			push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
			push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_row_fast_13104() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin80_iDCT8x8_1D_row_fast_Fiss_13117_13124_split[0]), &(SplitJoin80_iDCT8x8_1D_row_fast_Fiss_13117_13124_join[0]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_13105() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin80_iDCT8x8_1D_row_fast_Fiss_13117_13124_split[1]), &(SplitJoin80_iDCT8x8_1D_row_fast_Fiss_13117_13124_join[1]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_13106() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin80_iDCT8x8_1D_row_fast_Fiss_13117_13124_split[2]), &(SplitJoin80_iDCT8x8_1D_row_fast_Fiss_13117_13124_join[2]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_13107() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin80_iDCT8x8_1D_row_fast_Fiss_13117_13124_split[3]), &(SplitJoin80_iDCT8x8_1D_row_fast_Fiss_13117_13124_join[3]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_13108() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin80_iDCT8x8_1D_row_fast_Fiss_13117_13124_split[4]), &(SplitJoin80_iDCT8x8_1D_row_fast_Fiss_13117_13124_join[4]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_13109() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin80_iDCT8x8_1D_row_fast_Fiss_13117_13124_split[5]), &(SplitJoin80_iDCT8x8_1D_row_fast_Fiss_13117_13124_join[5]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_13110() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin80_iDCT8x8_1D_row_fast_Fiss_13117_13124_split[6]), &(SplitJoin80_iDCT8x8_1D_row_fast_Fiss_13117_13124_join[6]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_13111() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin80_iDCT8x8_1D_row_fast_Fiss_13117_13124_split[7]), &(SplitJoin80_iDCT8x8_1D_row_fast_Fiss_13117_13124_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_13102() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin80_iDCT8x8_1D_row_fast_Fiss_13117_13124_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12940_13016_13112_13119_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_13103() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_13103iDCT8x8_1D_col_fast_12966, pop_int(&SplitJoin80_iDCT8x8_1D_row_fast_Fiss_13117_13124_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&(*chanin), (c + 0)) ; 
			x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
			x2 = peek_int(&(*chanin), (c + 48)) ; 
			x3 = peek_int(&(*chanin), (c + 16)) ; 
			x4 = peek_int(&(*chanin), (c + 8)) ; 
			x5 = peek_int(&(*chanin), (c + 56)) ; 
			x6 = peek_int(&(*chanin), (c + 40)) ; 
			x7 = peek_int(&(*chanin), (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_12966_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_12966_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_12966_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_12966_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_12966_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_12966_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_12966_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_12966_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_12966_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
			push_int(&(*chanout), iDCT8x8_1D_col_fast_12966_s.buffer[i]) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_col_fast_12966() {
	FOR(uint32_t, __iter_steady_, 0, <, 7, __iter_steady_++)
		iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_13103iDCT8x8_1D_col_fast_12966), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12940_13016_13112_13119_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_13014() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 448, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_12939DUPLICATE_Splitter_13014);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12940_13016_13112_13119_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_13015() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 448, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_13015AnonFilter_a2_12967, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12940_13016_13112_13119_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_12967_s.count = (AnonFilter_a2_12967_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_12967_s.errors = (AnonFilter_a2_12967_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_12967_s.errors / AnonFilter_a2_12967_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_12967_s.errors = (AnonFilter_a2_12967_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_12967_s.errors / AnonFilter_a2_12967_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_12967() {
	FOR(uint32_t, __iter_steady_, 0, <, 448, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_13015AnonFilter_a2_12967));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 3, __iter_init_0_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12940_13016_13112_13119_split[__iter_init_0_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_12939DUPLICATE_Splitter_13014);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_13053Post_CollapsedDataParallel_2_13013);
	FOR(int, __iter_init_1_, 0, <, 28, __iter_init_1_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_13116_13123_join[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_13012WEIGHTED_ROUND_ROBIN_Splitter_13052);
	FOR(int, __iter_init_2_, 0, <, 28, __iter_init_2_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_13113_13120_join[__iter_init_2_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_13013WEIGHTED_ROUND_ROBIN_Splitter_13062);
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_int(&SplitJoin80_iDCT8x8_1D_row_fast_Fiss_13117_13124_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13115_13122_join[__iter_init_4_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_13103iDCT8x8_1D_col_fast_12966);
	FOR(int, __iter_init_5_, 0, <, 3, __iter_init_5_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_12940_13016_13112_13119_join[__iter_init_5_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_13015AnonFilter_a2_12967);
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_13115_13122_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 8, __iter_init_7_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13114_13121_split[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 28, __iter_init_8_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_13116_13123_split[__iter_init_8_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_13023Pre_CollapsedDataParallel_1_13012);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_13063WEIGHTED_ROUND_ROBIN_Splitter_13072);
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_int(&SplitJoin80_iDCT8x8_1D_row_fast_Fiss_13117_13124_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 28, __iter_init_10_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_13113_13120_split[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_13114_13121_join[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_12942
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_12942_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13054
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13054_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13055
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13055_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13056
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13056_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13057
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13057_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13058
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13058_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13059
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13059_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13060
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13060_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13061
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13061_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13064
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13064_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13065
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13065_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13066
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13066_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13067
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13067_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13068
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13068_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13069
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13069_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13070
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13070_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_13071
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_13071_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_12967
	 {
	AnonFilter_a2_12967_s.count = 0.0 ; 
	AnonFilter_a2_12967_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_12939();
		DUPLICATE_Splitter_13014();
			iDCT_2D_reference_coarse_12942();
			WEIGHTED_ROUND_ROBIN_Splitter_13022();
				AnonFilter_a3_13024();
				AnonFilter_a3_13025();
				AnonFilter_a3_13026();
				AnonFilter_a3_13027();
				AnonFilter_a3_13028();
				AnonFilter_a3_13029();
				AnonFilter_a3_13030();
				AnonFilter_a3_13031();
				AnonFilter_a3_13032();
				AnonFilter_a3_13033();
				AnonFilter_a3_13034();
				AnonFilter_a3_13035();
				AnonFilter_a3_13036();
				AnonFilter_a3_13037();
				AnonFilter_a3_13038();
				AnonFilter_a3_13039();
				AnonFilter_a3_13040();
				AnonFilter_a3_13041();
				AnonFilter_a3_13042();
				AnonFilter_a3_13043();
				AnonFilter_a3_13044();
				AnonFilter_a3_13045();
				AnonFilter_a3_13046();
				AnonFilter_a3_13047();
				AnonFilter_a3_13048();
				AnonFilter_a3_13049();
				AnonFilter_a3_13050();
				AnonFilter_a3_13051();
			WEIGHTED_ROUND_ROBIN_Joiner_13023();
			Pre_CollapsedDataParallel_1_13012();
			WEIGHTED_ROUND_ROBIN_Splitter_13052();
				iDCT_1D_reference_fine_13054();
				iDCT_1D_reference_fine_13055();
				iDCT_1D_reference_fine_13056();
				iDCT_1D_reference_fine_13057();
				iDCT_1D_reference_fine_13058();
				iDCT_1D_reference_fine_13059();
				iDCT_1D_reference_fine_13060();
				iDCT_1D_reference_fine_13061();
			WEIGHTED_ROUND_ROBIN_Joiner_13053();
			Post_CollapsedDataParallel_2_13013();
			WEIGHTED_ROUND_ROBIN_Splitter_13062();
				iDCT_1D_reference_fine_13064();
				iDCT_1D_reference_fine_13065();
				iDCT_1D_reference_fine_13066();
				iDCT_1D_reference_fine_13067();
				iDCT_1D_reference_fine_13068();
				iDCT_1D_reference_fine_13069();
				iDCT_1D_reference_fine_13070();
				iDCT_1D_reference_fine_13071();
			WEIGHTED_ROUND_ROBIN_Joiner_13063();
			WEIGHTED_ROUND_ROBIN_Splitter_13072();
				AnonFilter_a4_13074();
				AnonFilter_a4_13075();
				AnonFilter_a4_13076();
				AnonFilter_a4_13077();
				AnonFilter_a4_13078();
				AnonFilter_a4_13079();
				AnonFilter_a4_13080();
				AnonFilter_a4_13081();
				AnonFilter_a4_13082();
				AnonFilter_a4_13083();
				AnonFilter_a4_13084();
				AnonFilter_a4_13085();
				AnonFilter_a4_13086();
				AnonFilter_a4_13087();
				AnonFilter_a4_13088();
				AnonFilter_a4_13089();
				AnonFilter_a4_13090();
				AnonFilter_a4_13091();
				AnonFilter_a4_13092();
				AnonFilter_a4_13093();
				AnonFilter_a4_13094();
				AnonFilter_a4_13095();
				AnonFilter_a4_13096();
				AnonFilter_a4_13097();
				AnonFilter_a4_13098();
				AnonFilter_a4_13099();
				AnonFilter_a4_13100();
				AnonFilter_a4_13101();
			WEIGHTED_ROUND_ROBIN_Joiner_13073();
			WEIGHTED_ROUND_ROBIN_Splitter_13102();
				iDCT8x8_1D_row_fast_13104();
				iDCT8x8_1D_row_fast_13105();
				iDCT8x8_1D_row_fast_13106();
				iDCT8x8_1D_row_fast_13107();
				iDCT8x8_1D_row_fast_13108();
				iDCT8x8_1D_row_fast_13109();
				iDCT8x8_1D_row_fast_13110();
				iDCT8x8_1D_row_fast_13111();
			WEIGHTED_ROUND_ROBIN_Joiner_13103();
			iDCT8x8_1D_col_fast_12966();
		WEIGHTED_ROUND_ROBIN_Joiner_13015();
		AnonFilter_a2_12967();
	ENDFOR
	return EXIT_SUCCESS;
}
