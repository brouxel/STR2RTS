#include "PEG23-iDCTcompare_nocache.h"

buffer_int_t AnonFilter_a0_14269DUPLICATE_Splitter_14344;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_14423iDCT8x8_1D_col_fast_14296;
buffer_int_t SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_14345AnonFilter_a2_14297;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_14353Pre_CollapsedDataParallel_1_14342;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_14434_14441_join[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14270_14346_14432_14439_join[3];
buffer_float_t Post_CollapsedDataParallel_2_14343WEIGHTED_ROUND_ROBIN_Splitter_14387;
buffer_int_t SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_14434_14441_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_14388WEIGHTED_ROUND_ROBIN_Splitter_14397;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14270_14346_14432_14439_split[3];
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_14433_14440_split[23];
buffer_float_t Pre_CollapsedDataParallel_1_14342WEIGHTED_ROUND_ROBIN_Splitter_14377;
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_14436_14443_join[23];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_14433_14440_join[23];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_14435_14442_split[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_14378Post_CollapsedDataParallel_2_14343;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_14435_14442_join[8];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_14436_14443_split[23];


iDCT_2D_reference_coarse_14272_t iDCT_2D_reference_coarse_14272_s;
iDCT_2D_reference_coarse_14272_t iDCT_1D_reference_fine_14379_s;
iDCT_2D_reference_coarse_14272_t iDCT_1D_reference_fine_14380_s;
iDCT_2D_reference_coarse_14272_t iDCT_1D_reference_fine_14381_s;
iDCT_2D_reference_coarse_14272_t iDCT_1D_reference_fine_14382_s;
iDCT_2D_reference_coarse_14272_t iDCT_1D_reference_fine_14383_s;
iDCT_2D_reference_coarse_14272_t iDCT_1D_reference_fine_14384_s;
iDCT_2D_reference_coarse_14272_t iDCT_1D_reference_fine_14385_s;
iDCT_2D_reference_coarse_14272_t iDCT_1D_reference_fine_14386_s;
iDCT_2D_reference_coarse_14272_t iDCT_1D_reference_fine_14389_s;
iDCT_2D_reference_coarse_14272_t iDCT_1D_reference_fine_14390_s;
iDCT_2D_reference_coarse_14272_t iDCT_1D_reference_fine_14391_s;
iDCT_2D_reference_coarse_14272_t iDCT_1D_reference_fine_14392_s;
iDCT_2D_reference_coarse_14272_t iDCT_1D_reference_fine_14393_s;
iDCT_2D_reference_coarse_14272_t iDCT_1D_reference_fine_14394_s;
iDCT_2D_reference_coarse_14272_t iDCT_1D_reference_fine_14395_s;
iDCT_2D_reference_coarse_14272_t iDCT_1D_reference_fine_14396_s;
iDCT8x8_1D_col_fast_14296_t iDCT8x8_1D_col_fast_14296_s;
AnonFilter_a2_14297_t AnonFilter_a2_14297_s;

void AnonFilter_a0_14269(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&AnonFilter_a0_14269DUPLICATE_Splitter_14344, (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_2D_reference_coarse_14272(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_14272_s.coeff[k][j] * peek_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14270_14346_14432_14439_split[0], ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_14272_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14270_14346_14432_14439_join[0], ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14270_14346_14432_14439_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void AnonFilter_a3_14354(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_join[0], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_split[0])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14355(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_join[1], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_split[1])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14356(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_join[2], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_split[2])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14357(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_join[3], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_split[3])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14358(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_join[4], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_split[4])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14359(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_join[5], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_split[5])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14360(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_join[6], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_split[6])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14361(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_join[7], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_split[7])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14362(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_join[8], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_split[8])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14363(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_join[9], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_split[9])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14364(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_join[10], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_split[10])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14365(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_join[11], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_split[11])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14366(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_join[12], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_split[12])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14367(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_join[13], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_split[13])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14368(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_join[14], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_split[14])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14369(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_join[15], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_split[15])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14370(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_join[16], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_split[16])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14371(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_join[17], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_split[17])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14372(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_join[18], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_split[18])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14373(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_join[19], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_split[19])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14374(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_join[20], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_split[20])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14375(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_join[21], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_split[21])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_14376(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_join[22], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_split[22])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_14352() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 23, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14270_14346_14432_14439_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_14353() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 23, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_14353Pre_CollapsedDataParallel_1_14342, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_14342(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = 0 ; 
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Pre_CollapsedDataParallel_1_14342WEIGHTED_ROUND_ROBIN_Splitter_14377, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_14353Pre_CollapsedDataParallel_1_14342, (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_14353Pre_CollapsedDataParallel_1_14342) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14379(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14379_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14434_14441_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14434_14441_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14434_14441_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14380(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14380_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14434_14441_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14434_14441_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14434_14441_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14381(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14381_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14434_14441_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14434_14441_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14434_14441_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14382(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14382_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14434_14441_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14434_14441_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14434_14441_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14383(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14383_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14434_14441_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14434_14441_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14434_14441_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14384(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14384_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14434_14441_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14434_14441_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14434_14441_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14385(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14385_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14434_14441_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14434_14441_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14434_14441_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14386(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14386_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14434_14441_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14434_14441_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14434_14441_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_14377() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14434_14441_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_14342WEIGHTED_ROUND_ROBIN_Splitter_14377));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_14378() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_14378Post_CollapsedDataParallel_2_14343, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14434_14441_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_14343(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Post_CollapsedDataParallel_2_14343WEIGHTED_ROUND_ROBIN_Splitter_14387, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_14378Post_CollapsedDataParallel_2_14343, (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_14378Post_CollapsedDataParallel_2_14343) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14389(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14389_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14435_14442_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14435_14442_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14435_14442_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14390(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14390_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14435_14442_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14435_14442_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14435_14442_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14391(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14391_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14435_14442_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14435_14442_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14435_14442_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14392(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14392_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14435_14442_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14435_14442_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14435_14442_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14393(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14393_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14435_14442_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14435_14442_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14435_14442_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14394(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14394_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14435_14442_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14435_14442_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14435_14442_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14395(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14395_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14435_14442_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14435_14442_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14435_14442_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_14396(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_14396_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14435_14442_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14435_14442_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14435_14442_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_14387() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14435_14442_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_14343WEIGHTED_ROUND_ROBIN_Splitter_14387));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_14388() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_14388WEIGHTED_ROUND_ROBIN_Splitter_14397, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14435_14442_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4_14399(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_join[0], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_split[0]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14400(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_join[1], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_split[1]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14401(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_join[2], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_split[2]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14402(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_join[3], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_split[3]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14403(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_join[4], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_split[4]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14404(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_join[5], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_split[5]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14405(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_join[6], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_split[6]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14406(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_join[7], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_split[7]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14407(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_join[8], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_split[8]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14408(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_join[9], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_split[9]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14409(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_join[10], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_split[10]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14410(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_join[11], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_split[11]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14411(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_join[12], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_split[12]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14412(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_join[13], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_split[13]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14413(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_join[14], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_split[14]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14414(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_join[15], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_split[15]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14415(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_join[16], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_split[16]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14416(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_join[17], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_split[17]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14417(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_join[18], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_split[18]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14418(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_join[19], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_split[19]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14419(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_join[20], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_split[20]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14420(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_join[21], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_split[21]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_14421(){
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_join[22], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_split[22]) + 0.5)))) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_14397() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 23, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_14388WEIGHTED_ROUND_ROBIN_Splitter_14397));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_14398() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 23, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14270_14346_14432_14439_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast_14424(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[0], 0) ; 
		x1 = (peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[0], 4) << 11) ; 
		x2 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[0], 6) ; 
		x3 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[0], 2) ; 
		x4 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[0], 1) ; 
		x5 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[0], 7) ; 
		x6 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[0], 5) ; 
		x7 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[0], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[0], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[0], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[0], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[0], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[0], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[0], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[0], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[0], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[0], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_14425(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[1], 0) ; 
		x1 = (peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[1], 4) << 11) ; 
		x2 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[1], 6) ; 
		x3 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[1], 2) ; 
		x4 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[1], 1) ; 
		x5 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[1], 7) ; 
		x6 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[1], 5) ; 
		x7 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[1], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[1], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[1], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[1], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[1], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[1], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[1], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[1], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[1], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[1], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_14426(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[2], 0) ; 
		x1 = (peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[2], 4) << 11) ; 
		x2 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[2], 6) ; 
		x3 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[2], 2) ; 
		x4 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[2], 1) ; 
		x5 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[2], 7) ; 
		x6 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[2], 5) ; 
		x7 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[2], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[2], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[2], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[2], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[2], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[2], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[2], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[2], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[2], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[2], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_14427(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[3], 0) ; 
		x1 = (peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[3], 4) << 11) ; 
		x2 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[3], 6) ; 
		x3 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[3], 2) ; 
		x4 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[3], 1) ; 
		x5 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[3], 7) ; 
		x6 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[3], 5) ; 
		x7 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[3], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[3], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[3], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[3], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[3], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[3], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[3], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[3], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[3], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[3], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_14428(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[4], 0) ; 
		x1 = (peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[4], 4) << 11) ; 
		x2 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[4], 6) ; 
		x3 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[4], 2) ; 
		x4 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[4], 1) ; 
		x5 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[4], 7) ; 
		x6 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[4], 5) ; 
		x7 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[4], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[4], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[4], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[4], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[4], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[4], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[4], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[4], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[4], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[4], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_14429(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[5], 0) ; 
		x1 = (peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[5], 4) << 11) ; 
		x2 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[5], 6) ; 
		x3 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[5], 2) ; 
		x4 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[5], 1) ; 
		x5 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[5], 7) ; 
		x6 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[5], 5) ; 
		x7 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[5], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[5], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[5], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[5], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[5], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[5], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[5], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[5], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[5], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[5], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_14430(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[6], 0) ; 
		x1 = (peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[6], 4) << 11) ; 
		x2 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[6], 6) ; 
		x3 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[6], 2) ; 
		x4 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[6], 1) ; 
		x5 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[6], 7) ; 
		x6 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[6], 5) ; 
		x7 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[6], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[6], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[6], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[6], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[6], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[6], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[6], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[6], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[6], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[6], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_14431(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[7], 0) ; 
		x1 = (peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[7], 4) << 11) ; 
		x2 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[7], 6) ; 
		x3 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[7], 2) ; 
		x4 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[7], 1) ; 
		x5 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[7], 7) ; 
		x6 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[7], 5) ; 
		x7 = peek_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[7], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[7], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[7], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[7], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[7], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[7], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[7], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[7], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[7], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[7], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_14422() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14270_14346_14432_14439_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_14423() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_14423iDCT8x8_1D_col_fast_14296, pop_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast_14296(){
	FOR(uint32_t, __iter_steady_, 0, <, 23, __iter_steady_++) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_14423iDCT8x8_1D_col_fast_14296, (c + 0)) ; 
			x1 = (peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_14423iDCT8x8_1D_col_fast_14296, (c + 32)) << 8) ; 
			x2 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_14423iDCT8x8_1D_col_fast_14296, (c + 48)) ; 
			x3 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_14423iDCT8x8_1D_col_fast_14296, (c + 16)) ; 
			x4 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_14423iDCT8x8_1D_col_fast_14296, (c + 8)) ; 
			x5 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_14423iDCT8x8_1D_col_fast_14296, (c + 56)) ; 
			x6 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_14423iDCT8x8_1D_col_fast_14296, (c + 40)) ; 
			x7 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_14423iDCT8x8_1D_col_fast_14296, (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_14296_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_14296_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_14296_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_14296_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_14296_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_14296_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_14296_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_14296_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_14296_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_14423iDCT8x8_1D_col_fast_14296) ; 
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14270_14346_14432_14439_join[2], iDCT8x8_1D_col_fast_14296_s.buffer[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void DUPLICATE_Splitter_14344() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1472, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_14269DUPLICATE_Splitter_14344);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14270_14346_14432_14439_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_14345() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1472, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_14345AnonFilter_a2_14297, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14270_14346_14432_14439_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2_14297(){
	FOR(uint32_t, __iter_steady_, 0, <, 1472, __iter_steady_++) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_14345AnonFilter_a2_14297) ; 
		reffine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_14345AnonFilter_a2_14297) ; 
		fastfine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_14345AnonFilter_a2_14297) ; 
		AnonFilter_a2_14297_s.count = (AnonFilter_a2_14297_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_14297_s.errors = (AnonFilter_a2_14297_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_14297_s.errors / AnonFilter_a2_14297_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_14297_s.errors = (AnonFilter_a2_14297_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_14297_s.errors / AnonFilter_a2_14297_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}
	ENDFOR
}

void __stream_init__() {
	init_buffer_int(&AnonFilter_a0_14269DUPLICATE_Splitter_14344);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_14423iDCT8x8_1D_col_fast_14296);
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_join[__iter_init_0_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_14345AnonFilter_a2_14297);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_14353Pre_CollapsedDataParallel_1_14342);
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14434_14441_join[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 3, __iter_init_2_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14270_14346_14432_14439_join[__iter_init_2_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_14343WEIGHTED_ROUND_ROBIN_Splitter_14387);
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_int(&SplitJoin70_iDCT8x8_1D_row_fast_Fiss_14437_14444_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_14434_14441_split[__iter_init_4_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_14388WEIGHTED_ROUND_ROBIN_Splitter_14397);
	FOR(int, __iter_init_5_, 0, <, 3, __iter_init_5_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_14270_14346_14432_14439_split[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 23, __iter_init_6_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_split[__iter_init_6_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_14342WEIGHTED_ROUND_ROBIN_Splitter_14377);
	FOR(int, __iter_init_7_, 0, <, 23, __iter_init_7_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 23, __iter_init_8_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_14433_14440_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14435_14442_split[__iter_init_9_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_14378Post_CollapsedDataParallel_2_14343);
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_14435_14442_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 23, __iter_init_11_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_14436_14443_split[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_14272
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_14272_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14379
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14379_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14380
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14380_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14381
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14381_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14382
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14382_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14383
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14383_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14384
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14384_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14385
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14385_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14386
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14386_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14389
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14389_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14390
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14390_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14391
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14391_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14392
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14392_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14393
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14393_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14394
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14394_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14395
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14395_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_14396
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_14396_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_14297
	 {
	AnonFilter_a2_14297_s.count = 0.0 ; 
	AnonFilter_a2_14297_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_14269();
		DUPLICATE_Splitter_14344();
			iDCT_2D_reference_coarse_14272();
			WEIGHTED_ROUND_ROBIN_Splitter_14352();
				AnonFilter_a3_14354();
				AnonFilter_a3_14355();
				AnonFilter_a3_14356();
				AnonFilter_a3_14357();
				AnonFilter_a3_14358();
				AnonFilter_a3_14359();
				AnonFilter_a3_14360();
				AnonFilter_a3_14361();
				AnonFilter_a3_14362();
				AnonFilter_a3_14363();
				AnonFilter_a3_14364();
				AnonFilter_a3_14365();
				AnonFilter_a3_14366();
				AnonFilter_a3_14367();
				AnonFilter_a3_14368();
				AnonFilter_a3_14369();
				AnonFilter_a3_14370();
				AnonFilter_a3_14371();
				AnonFilter_a3_14372();
				AnonFilter_a3_14373();
				AnonFilter_a3_14374();
				AnonFilter_a3_14375();
				AnonFilter_a3_14376();
			WEIGHTED_ROUND_ROBIN_Joiner_14353();
			Pre_CollapsedDataParallel_1_14342();
			WEIGHTED_ROUND_ROBIN_Splitter_14377();
				iDCT_1D_reference_fine_14379();
				iDCT_1D_reference_fine_14380();
				iDCT_1D_reference_fine_14381();
				iDCT_1D_reference_fine_14382();
				iDCT_1D_reference_fine_14383();
				iDCT_1D_reference_fine_14384();
				iDCT_1D_reference_fine_14385();
				iDCT_1D_reference_fine_14386();
			WEIGHTED_ROUND_ROBIN_Joiner_14378();
			Post_CollapsedDataParallel_2_14343();
			WEIGHTED_ROUND_ROBIN_Splitter_14387();
				iDCT_1D_reference_fine_14389();
				iDCT_1D_reference_fine_14390();
				iDCT_1D_reference_fine_14391();
				iDCT_1D_reference_fine_14392();
				iDCT_1D_reference_fine_14393();
				iDCT_1D_reference_fine_14394();
				iDCT_1D_reference_fine_14395();
				iDCT_1D_reference_fine_14396();
			WEIGHTED_ROUND_ROBIN_Joiner_14388();
			WEIGHTED_ROUND_ROBIN_Splitter_14397();
				AnonFilter_a4_14399();
				AnonFilter_a4_14400();
				AnonFilter_a4_14401();
				AnonFilter_a4_14402();
				AnonFilter_a4_14403();
				AnonFilter_a4_14404();
				AnonFilter_a4_14405();
				AnonFilter_a4_14406();
				AnonFilter_a4_14407();
				AnonFilter_a4_14408();
				AnonFilter_a4_14409();
				AnonFilter_a4_14410();
				AnonFilter_a4_14411();
				AnonFilter_a4_14412();
				AnonFilter_a4_14413();
				AnonFilter_a4_14414();
				AnonFilter_a4_14415();
				AnonFilter_a4_14416();
				AnonFilter_a4_14417();
				AnonFilter_a4_14418();
				AnonFilter_a4_14419();
				AnonFilter_a4_14420();
				AnonFilter_a4_14421();
			WEIGHTED_ROUND_ROBIN_Joiner_14398();
			WEIGHTED_ROUND_ROBIN_Splitter_14422();
				iDCT8x8_1D_row_fast_14424();
				iDCT8x8_1D_row_fast_14425();
				iDCT8x8_1D_row_fast_14426();
				iDCT8x8_1D_row_fast_14427();
				iDCT8x8_1D_row_fast_14428();
				iDCT8x8_1D_row_fast_14429();
				iDCT8x8_1D_row_fast_14430();
				iDCT8x8_1D_row_fast_14431();
			WEIGHTED_ROUND_ROBIN_Joiner_14423();
			iDCT8x8_1D_col_fast_14296();
		WEIGHTED_ROUND_ROBIN_Joiner_14345();
		AnonFilter_a2_14297();
	ENDFOR
	return EXIT_SUCCESS;
}
