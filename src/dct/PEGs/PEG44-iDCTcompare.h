#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=4224 on the compile command line
#else
#if BUF_SIZEMAX < 4224
#error BUF_SIZEMAX too small, it must be at least 4224
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_8014_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_8038_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_8039_t;
void AnonFilter_a0(buffer_int_t *chanout);
void AnonFilter_a0_8011();
void DUPLICATE_Splitter_8086();
void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT_2D_reference_coarse_8014();
void WEIGHTED_ROUND_ROBIN_Splitter_8094();
void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout);
void AnonFilter_a3_8096();
void AnonFilter_a3_8097();
void AnonFilter_a3_8098();
void AnonFilter_a3_8099();
void AnonFilter_a3_8100();
void AnonFilter_a3_8101();
void AnonFilter_a3_8102();
void AnonFilter_a3_8103();
void AnonFilter_a3_8104();
void AnonFilter_a3_8105();
void AnonFilter_a3_8106();
void AnonFilter_a3_8107();
void AnonFilter_a3_8108();
void AnonFilter_a3_8109();
void AnonFilter_a3_8110();
void AnonFilter_a3_8111();
void AnonFilter_a3_8112();
void AnonFilter_a3_8113();
void AnonFilter_a3_8114();
void AnonFilter_a3_8115();
void AnonFilter_a3_8116();
void AnonFilter_a3_8117();
void AnonFilter_a3_8118();
void AnonFilter_a3_8119();
void AnonFilter_a3_8120();
void AnonFilter_a3_8121();
void AnonFilter_a3_8122();
void AnonFilter_a3_8123();
void AnonFilter_a3_8124();
void AnonFilter_a3_8125();
void AnonFilter_a3_8126();
void AnonFilter_a3_8127();
void AnonFilter_a3_8128();
void AnonFilter_a3_8129();
void AnonFilter_a3_8130();
void AnonFilter_a3_8131();
void AnonFilter_a3_8132();
void AnonFilter_a3_8133();
void AnonFilter_a3_8134();
void AnonFilter_a3_8135();
void AnonFilter_a3_8136();
void AnonFilter_a3_8137();
void AnonFilter_a3_8138();
void AnonFilter_a3_8139();
void WEIGHTED_ROUND_ROBIN_Joiner_8095();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_8084();
void WEIGHTED_ROUND_ROBIN_Splitter_8140();
void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout);
void iDCT_1D_reference_fine_8142();
void iDCT_1D_reference_fine_8143();
void iDCT_1D_reference_fine_8144();
void iDCT_1D_reference_fine_8145();
void iDCT_1D_reference_fine_8146();
void iDCT_1D_reference_fine_8147();
void iDCT_1D_reference_fine_8148();
void iDCT_1D_reference_fine_8149();
void WEIGHTED_ROUND_ROBIN_Joiner_8141();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_8085();
void WEIGHTED_ROUND_ROBIN_Splitter_8150();
void iDCT_1D_reference_fine_8152();
void iDCT_1D_reference_fine_8153();
void iDCT_1D_reference_fine_8154();
void iDCT_1D_reference_fine_8155();
void iDCT_1D_reference_fine_8156();
void iDCT_1D_reference_fine_8157();
void iDCT_1D_reference_fine_8158();
void iDCT_1D_reference_fine_8159();
void WEIGHTED_ROUND_ROBIN_Joiner_8151();
void WEIGHTED_ROUND_ROBIN_Splitter_8160();
void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout);
void AnonFilter_a4_8162();
void AnonFilter_a4_8163();
void AnonFilter_a4_8164();
void AnonFilter_a4_8165();
void AnonFilter_a4_8166();
void AnonFilter_a4_8167();
void AnonFilter_a4_8168();
void AnonFilter_a4_8169();
void AnonFilter_a4_8170();
void AnonFilter_a4_8171();
void AnonFilter_a4_8172();
void AnonFilter_a4_8173();
void AnonFilter_a4_8174();
void AnonFilter_a4_8175();
void AnonFilter_a4_8176();
void AnonFilter_a4_8177();
void AnonFilter_a4_8178();
void AnonFilter_a4_8179();
void AnonFilter_a4_8180();
void AnonFilter_a4_8181();
void AnonFilter_a4_8182();
void AnonFilter_a4_8183();
void AnonFilter_a4_8184();
void AnonFilter_a4_8185();
void AnonFilter_a4_8186();
void AnonFilter_a4_8187();
void AnonFilter_a4_8188();
void AnonFilter_a4_8189();
void AnonFilter_a4_8190();
void AnonFilter_a4_8191();
void AnonFilter_a4_8192();
void AnonFilter_a4_8193();
void AnonFilter_a4_8194();
void AnonFilter_a4_8195();
void AnonFilter_a4_8196();
void AnonFilter_a4_8197();
void AnonFilter_a4_8198();
void AnonFilter_a4_8199();
void AnonFilter_a4_8200();
void AnonFilter_a4_8201();
void AnonFilter_a4_8202();
void AnonFilter_a4_8203();
void AnonFilter_a4_8204();
void AnonFilter_a4_8205();
void WEIGHTED_ROUND_ROBIN_Joiner_8161();
void WEIGHTED_ROUND_ROBIN_Splitter_8206();
void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_row_fast_8208();
void iDCT8x8_1D_row_fast_8209();
void iDCT8x8_1D_row_fast_8210();
void iDCT8x8_1D_row_fast_8211();
void iDCT8x8_1D_row_fast_8212();
void iDCT8x8_1D_row_fast_8213();
void iDCT8x8_1D_row_fast_8214();
void iDCT8x8_1D_row_fast_8215();
void WEIGHTED_ROUND_ROBIN_Joiner_8207();
void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_col_fast_8038();
void WEIGHTED_ROUND_ROBIN_Joiner_8087();
void AnonFilter_a2(buffer_int_t *chanin);
void AnonFilter_a2_8039();

#ifdef __cplusplus
}
#endif
#endif
