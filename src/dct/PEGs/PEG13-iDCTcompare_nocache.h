#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=4992 on the compile command line
#else
#if BUF_SIZEMAX < 4992
#error BUF_SIZEMAX too small, it must be at least 4992
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_16632_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_16656_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_16657_t;
void AnonFilter_a0_16629();
void DUPLICATE_Splitter_16704();
void iDCT_2D_reference_coarse_16632();
void WEIGHTED_ROUND_ROBIN_Splitter_16712();
void AnonFilter_a3_16714();
void AnonFilter_a3_16715();
void AnonFilter_a3_16716();
void AnonFilter_a3_16717();
void AnonFilter_a3_16718();
void AnonFilter_a3_16719();
void AnonFilter_a3_16720();
void AnonFilter_a3_16721();
void AnonFilter_a3_16722();
void AnonFilter_a3_16723();
void AnonFilter_a3_16724();
void AnonFilter_a3_16725();
void AnonFilter_a3_16726();
void WEIGHTED_ROUND_ROBIN_Joiner_16713();
void Pre_CollapsedDataParallel_1_16702();
void WEIGHTED_ROUND_ROBIN_Splitter_16727();
void iDCT_1D_reference_fine_16729();
void iDCT_1D_reference_fine_16730();
void iDCT_1D_reference_fine_16731();
void iDCT_1D_reference_fine_16732();
void iDCT_1D_reference_fine_16733();
void iDCT_1D_reference_fine_16734();
void iDCT_1D_reference_fine_16735();
void iDCT_1D_reference_fine_16736();
void WEIGHTED_ROUND_ROBIN_Joiner_16728();
void Post_CollapsedDataParallel_2_16703();
void WEIGHTED_ROUND_ROBIN_Splitter_16737();
void iDCT_1D_reference_fine_16739();
void iDCT_1D_reference_fine_16740();
void iDCT_1D_reference_fine_16741();
void iDCT_1D_reference_fine_16742();
void iDCT_1D_reference_fine_16743();
void iDCT_1D_reference_fine_16744();
void iDCT_1D_reference_fine_16745();
void iDCT_1D_reference_fine_16746();
void WEIGHTED_ROUND_ROBIN_Joiner_16738();
void WEIGHTED_ROUND_ROBIN_Splitter_16747();
void AnonFilter_a4_16749();
void AnonFilter_a4_16750();
void AnonFilter_a4_16751();
void AnonFilter_a4_16752();
void AnonFilter_a4_16753();
void AnonFilter_a4_16754();
void AnonFilter_a4_16755();
void AnonFilter_a4_16756();
void AnonFilter_a4_16757();
void AnonFilter_a4_16758();
void AnonFilter_a4_16759();
void AnonFilter_a4_16760();
void AnonFilter_a4_16761();
void WEIGHTED_ROUND_ROBIN_Joiner_16748();
void WEIGHTED_ROUND_ROBIN_Splitter_16762();
void iDCT8x8_1D_row_fast_16764();
void iDCT8x8_1D_row_fast_16765();
void iDCT8x8_1D_row_fast_16766();
void iDCT8x8_1D_row_fast_16767();
void iDCT8x8_1D_row_fast_16768();
void iDCT8x8_1D_row_fast_16769();
void iDCT8x8_1D_row_fast_16770();
void iDCT8x8_1D_row_fast_16771();
void WEIGHTED_ROUND_ROBIN_Joiner_16763();
void iDCT8x8_1D_col_fast_16656();
void WEIGHTED_ROUND_ROBIN_Joiner_16705();
void AnonFilter_a2_16657();

#ifdef __cplusplus
}
#endif
#endif
