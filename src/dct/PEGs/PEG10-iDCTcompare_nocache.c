#include "PEG10-iDCTcompare_nocache.h"

buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_17397_17404_split[10];
buffer_int_t AnonFilter_a0_17259DUPLICATE_Splitter_17334;
buffer_int_t SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[8];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_17400_17407_split[10];
buffer_int_t SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_17343Pre_CollapsedDataParallel_1_17332;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_17398_17405_split[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_17399_17406_join[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17260_17336_17396_17403_split[3];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_17335AnonFilter_a2_17287;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_17355Post_CollapsedDataParallel_2_17333;
buffer_float_t Post_CollapsedDataParallel_2_17333WEIGHTED_ROUND_ROBIN_Splitter_17364;
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_17400_17407_join[10];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_17398_17405_join[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_17399_17406_split[8];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_17397_17404_join[10];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17260_17336_17396_17403_join[3];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_17387iDCT8x8_1D_col_fast_17286;
buffer_float_t Pre_CollapsedDataParallel_1_17332WEIGHTED_ROUND_ROBIN_Splitter_17354;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_17365WEIGHTED_ROUND_ROBIN_Splitter_17374;


iDCT_2D_reference_coarse_17262_t iDCT_2D_reference_coarse_17262_s;
iDCT_2D_reference_coarse_17262_t iDCT_1D_reference_fine_17356_s;
iDCT_2D_reference_coarse_17262_t iDCT_1D_reference_fine_17357_s;
iDCT_2D_reference_coarse_17262_t iDCT_1D_reference_fine_17358_s;
iDCT_2D_reference_coarse_17262_t iDCT_1D_reference_fine_17359_s;
iDCT_2D_reference_coarse_17262_t iDCT_1D_reference_fine_17360_s;
iDCT_2D_reference_coarse_17262_t iDCT_1D_reference_fine_17361_s;
iDCT_2D_reference_coarse_17262_t iDCT_1D_reference_fine_17362_s;
iDCT_2D_reference_coarse_17262_t iDCT_1D_reference_fine_17363_s;
iDCT_2D_reference_coarse_17262_t iDCT_1D_reference_fine_17366_s;
iDCT_2D_reference_coarse_17262_t iDCT_1D_reference_fine_17367_s;
iDCT_2D_reference_coarse_17262_t iDCT_1D_reference_fine_17368_s;
iDCT_2D_reference_coarse_17262_t iDCT_1D_reference_fine_17369_s;
iDCT_2D_reference_coarse_17262_t iDCT_1D_reference_fine_17370_s;
iDCT_2D_reference_coarse_17262_t iDCT_1D_reference_fine_17371_s;
iDCT_2D_reference_coarse_17262_t iDCT_1D_reference_fine_17372_s;
iDCT_2D_reference_coarse_17262_t iDCT_1D_reference_fine_17373_s;
iDCT8x8_1D_col_fast_17286_t iDCT8x8_1D_col_fast_17286_s;
AnonFilter_a2_17287_t AnonFilter_a2_17287_s;

void AnonFilter_a0_17259(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&AnonFilter_a0_17259DUPLICATE_Splitter_17334, (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_2D_reference_coarse_17262(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_17262_s.coeff[k][j] * peek_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17260_17336_17396_17403_split[0], ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_17262_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17260_17336_17396_17403_join[0], ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17260_17336_17396_17403_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void AnonFilter_a3_17344(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_17397_17404_join[0], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_17397_17404_split[0])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_17345(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_17397_17404_join[1], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_17397_17404_split[1])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_17346(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_17397_17404_join[2], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_17397_17404_split[2])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_17347(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_17397_17404_join[3], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_17397_17404_split[3])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_17348(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_17397_17404_join[4], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_17397_17404_split[4])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_17349(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_17397_17404_join[5], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_17397_17404_split[5])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_17350(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_17397_17404_join[6], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_17397_17404_split[6])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_17351(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_17397_17404_join[7], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_17397_17404_split[7])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_17352(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_17397_17404_join[8], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_17397_17404_split[8])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_17353(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_17397_17404_join[9], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_17397_17404_split[9])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_17342() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 10, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_17397_17404_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17260_17336_17396_17403_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_17343() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 10, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_17343Pre_CollapsedDataParallel_1_17332, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_17397_17404_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_17332(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = 0 ; 
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Pre_CollapsedDataParallel_1_17332WEIGHTED_ROUND_ROBIN_Splitter_17354, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_17343Pre_CollapsedDataParallel_1_17332, (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_17343Pre_CollapsedDataParallel_1_17332) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_17356(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_17356_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17398_17405_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17398_17405_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17398_17405_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_17357(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_17357_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17398_17405_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17398_17405_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17398_17405_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_17358(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_17358_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17398_17405_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17398_17405_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17398_17405_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_17359(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_17359_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17398_17405_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17398_17405_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17398_17405_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_17360(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_17360_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17398_17405_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17398_17405_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17398_17405_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_17361(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_17361_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17398_17405_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17398_17405_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17398_17405_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_17362(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_17362_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17398_17405_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17398_17405_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17398_17405_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_17363(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_17363_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17398_17405_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17398_17405_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17398_17405_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_17354() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17398_17405_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_17332WEIGHTED_ROUND_ROBIN_Splitter_17354));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_17355() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_17355Post_CollapsedDataParallel_2_17333, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17398_17405_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_17333(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Post_CollapsedDataParallel_2_17333WEIGHTED_ROUND_ROBIN_Splitter_17364, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_17355Post_CollapsedDataParallel_2_17333, (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_17355Post_CollapsedDataParallel_2_17333) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_17366(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_17366_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17399_17406_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17399_17406_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17399_17406_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_17367(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_17367_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17399_17406_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17399_17406_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17399_17406_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_17368(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_17368_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17399_17406_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17399_17406_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17399_17406_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_17369(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_17369_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17399_17406_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17399_17406_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17399_17406_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_17370(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_17370_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17399_17406_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17399_17406_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17399_17406_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_17371(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_17371_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17399_17406_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17399_17406_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17399_17406_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_17372(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_17372_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17399_17406_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17399_17406_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17399_17406_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_17373(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_17373_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17399_17406_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17399_17406_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17399_17406_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_17364() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17399_17406_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_17333WEIGHTED_ROUND_ROBIN_Splitter_17364));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_17365() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_17365WEIGHTED_ROUND_ROBIN_Splitter_17374, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17399_17406_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4_17376(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_17400_17407_join[0], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_17400_17407_split[0]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_17377(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_17400_17407_join[1], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_17400_17407_split[1]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_17378(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_17400_17407_join[2], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_17400_17407_split[2]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_17379(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_17400_17407_join[3], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_17400_17407_split[3]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_17380(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_17400_17407_join[4], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_17400_17407_split[4]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_17381(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_17400_17407_join[5], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_17400_17407_split[5]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_17382(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_17400_17407_join[6], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_17400_17407_split[6]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_17383(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_17400_17407_join[7], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_17400_17407_split[7]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_17384(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_17400_17407_join[8], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_17400_17407_split[8]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_17385(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_17400_17407_join[9], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_17400_17407_split[9]) + 0.5)))) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_17374() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 10, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_17400_17407_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_17365WEIGHTED_ROUND_ROBIN_Splitter_17374));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_17375() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 10, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17260_17336_17396_17403_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_17400_17407_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast_17388(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[0], 0) ; 
		x1 = (peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[0], 4) << 11) ; 
		x2 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[0], 6) ; 
		x3 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[0], 2) ; 
		x4 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[0], 1) ; 
		x5 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[0], 7) ; 
		x6 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[0], 5) ; 
		x7 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[0], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[0], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[0], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[0], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[0], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[0], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[0], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[0], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[0], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[0], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_17389(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[1], 0) ; 
		x1 = (peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[1], 4) << 11) ; 
		x2 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[1], 6) ; 
		x3 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[1], 2) ; 
		x4 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[1], 1) ; 
		x5 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[1], 7) ; 
		x6 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[1], 5) ; 
		x7 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[1], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[1], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[1], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[1], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[1], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[1], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[1], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[1], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[1], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[1], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_17390(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[2], 0) ; 
		x1 = (peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[2], 4) << 11) ; 
		x2 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[2], 6) ; 
		x3 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[2], 2) ; 
		x4 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[2], 1) ; 
		x5 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[2], 7) ; 
		x6 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[2], 5) ; 
		x7 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[2], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[2], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[2], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[2], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[2], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[2], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[2], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[2], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[2], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[2], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_17391(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[3], 0) ; 
		x1 = (peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[3], 4) << 11) ; 
		x2 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[3], 6) ; 
		x3 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[3], 2) ; 
		x4 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[3], 1) ; 
		x5 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[3], 7) ; 
		x6 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[3], 5) ; 
		x7 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[3], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[3], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[3], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[3], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[3], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[3], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[3], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[3], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[3], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[3], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_17392(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[4], 0) ; 
		x1 = (peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[4], 4) << 11) ; 
		x2 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[4], 6) ; 
		x3 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[4], 2) ; 
		x4 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[4], 1) ; 
		x5 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[4], 7) ; 
		x6 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[4], 5) ; 
		x7 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[4], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[4], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[4], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[4], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[4], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[4], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[4], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[4], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[4], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[4], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_17393(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[5], 0) ; 
		x1 = (peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[5], 4) << 11) ; 
		x2 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[5], 6) ; 
		x3 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[5], 2) ; 
		x4 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[5], 1) ; 
		x5 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[5], 7) ; 
		x6 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[5], 5) ; 
		x7 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[5], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[5], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[5], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[5], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[5], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[5], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[5], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[5], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[5], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[5], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_17394(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[6], 0) ; 
		x1 = (peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[6], 4) << 11) ; 
		x2 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[6], 6) ; 
		x3 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[6], 2) ; 
		x4 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[6], 1) ; 
		x5 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[6], 7) ; 
		x6 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[6], 5) ; 
		x7 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[6], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[6], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[6], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[6], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[6], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[6], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[6], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[6], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[6], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[6], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_17395(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[7], 0) ; 
		x1 = (peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[7], 4) << 11) ; 
		x2 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[7], 6) ; 
		x3 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[7], 2) ; 
		x4 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[7], 1) ; 
		x5 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[7], 7) ; 
		x6 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[7], 5) ; 
		x7 = peek_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[7], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[7], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[7], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[7], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[7], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[7], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[7], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[7], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[7], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[7], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_17386() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17260_17336_17396_17403_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_17387() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_17387iDCT8x8_1D_col_fast_17286, pop_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast_17286(){
	FOR(uint32_t, __iter_steady_, 0, <, 5, __iter_steady_++) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_17387iDCT8x8_1D_col_fast_17286, (c + 0)) ; 
			x1 = (peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_17387iDCT8x8_1D_col_fast_17286, (c + 32)) << 8) ; 
			x2 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_17387iDCT8x8_1D_col_fast_17286, (c + 48)) ; 
			x3 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_17387iDCT8x8_1D_col_fast_17286, (c + 16)) ; 
			x4 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_17387iDCT8x8_1D_col_fast_17286, (c + 8)) ; 
			x5 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_17387iDCT8x8_1D_col_fast_17286, (c + 56)) ; 
			x6 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_17387iDCT8x8_1D_col_fast_17286, (c + 40)) ; 
			x7 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_17387iDCT8x8_1D_col_fast_17286, (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_17286_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_17286_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_17286_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_17286_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_17286_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_17286_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_17286_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_17286_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_17286_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_17387iDCT8x8_1D_col_fast_17286) ; 
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17260_17336_17396_17403_join[2], iDCT8x8_1D_col_fast_17286_s.buffer[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void DUPLICATE_Splitter_17334() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 320, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_17259DUPLICATE_Splitter_17334);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17260_17336_17396_17403_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_17335() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 320, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_17335AnonFilter_a2_17287, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17260_17336_17396_17403_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2_17287(){
	FOR(uint32_t, __iter_steady_, 0, <, 320, __iter_steady_++) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_17335AnonFilter_a2_17287) ; 
		reffine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_17335AnonFilter_a2_17287) ; 
		fastfine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_17335AnonFilter_a2_17287) ; 
		AnonFilter_a2_17287_s.count = (AnonFilter_a2_17287_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_17287_s.errors = (AnonFilter_a2_17287_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_17287_s.errors / AnonFilter_a2_17287_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_17287_s.errors = (AnonFilter_a2_17287_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_17287_s.errors / AnonFilter_a2_17287_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 10, __iter_init_0_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_17397_17404_split[__iter_init_0_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_17259DUPLICATE_Splitter_17334);
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_split[__iter_init_1_]);
	ENDFOR
	FOR(int, __iter_init_2_, 0, <, 10, __iter_init_2_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_17400_17407_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 8, __iter_init_3_++)
		init_buffer_int(&SplitJoin44_iDCT8x8_1D_row_fast_Fiss_17401_17408_join[__iter_init_3_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_17343Pre_CollapsedDataParallel_1_17332);
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17398_17405_split[__iter_init_4_]);
	ENDFOR
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17399_17406_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 3, __iter_init_6_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17260_17336_17396_17403_split[__iter_init_6_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_17335AnonFilter_a2_17287);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_17355Post_CollapsedDataParallel_2_17333);
	init_buffer_float(&Post_CollapsedDataParallel_2_17333WEIGHTED_ROUND_ROBIN_Splitter_17364);
	FOR(int, __iter_init_7_, 0, <, 10, __iter_init_7_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_17400_17407_join[__iter_init_7_]);
	ENDFOR
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_17398_17405_join[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_17399_17406_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 10, __iter_init_10_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_17397_17404_join[__iter_init_10_]);
	ENDFOR
	FOR(int, __iter_init_11_, 0, <, 3, __iter_init_11_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_17260_17336_17396_17403_join[__iter_init_11_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_17387iDCT8x8_1D_col_fast_17286);
	init_buffer_float(&Pre_CollapsedDataParallel_1_17332WEIGHTED_ROUND_ROBIN_Splitter_17354);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_17365WEIGHTED_ROUND_ROBIN_Splitter_17374);
// --- init: iDCT_2D_reference_coarse_17262
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_17262_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17356
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17356_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17357
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17357_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17358
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17358_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17359
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17359_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17360
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17360_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17361
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17361_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17362
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17362_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17363
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17363_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17366
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17366_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17367
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17367_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17368
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17368_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17369
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17369_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17370
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17370_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17371
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17371_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17372
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17372_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_17373
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_17373_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_17287
	 {
	AnonFilter_a2_17287_s.count = 0.0 ; 
	AnonFilter_a2_17287_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_17259();
		DUPLICATE_Splitter_17334();
			iDCT_2D_reference_coarse_17262();
			WEIGHTED_ROUND_ROBIN_Splitter_17342();
				AnonFilter_a3_17344();
				AnonFilter_a3_17345();
				AnonFilter_a3_17346();
				AnonFilter_a3_17347();
				AnonFilter_a3_17348();
				AnonFilter_a3_17349();
				AnonFilter_a3_17350();
				AnonFilter_a3_17351();
				AnonFilter_a3_17352();
				AnonFilter_a3_17353();
			WEIGHTED_ROUND_ROBIN_Joiner_17343();
			Pre_CollapsedDataParallel_1_17332();
			WEIGHTED_ROUND_ROBIN_Splitter_17354();
				iDCT_1D_reference_fine_17356();
				iDCT_1D_reference_fine_17357();
				iDCT_1D_reference_fine_17358();
				iDCT_1D_reference_fine_17359();
				iDCT_1D_reference_fine_17360();
				iDCT_1D_reference_fine_17361();
				iDCT_1D_reference_fine_17362();
				iDCT_1D_reference_fine_17363();
			WEIGHTED_ROUND_ROBIN_Joiner_17355();
			Post_CollapsedDataParallel_2_17333();
			WEIGHTED_ROUND_ROBIN_Splitter_17364();
				iDCT_1D_reference_fine_17366();
				iDCT_1D_reference_fine_17367();
				iDCT_1D_reference_fine_17368();
				iDCT_1D_reference_fine_17369();
				iDCT_1D_reference_fine_17370();
				iDCT_1D_reference_fine_17371();
				iDCT_1D_reference_fine_17372();
				iDCT_1D_reference_fine_17373();
			WEIGHTED_ROUND_ROBIN_Joiner_17365();
			WEIGHTED_ROUND_ROBIN_Splitter_17374();
				AnonFilter_a4_17376();
				AnonFilter_a4_17377();
				AnonFilter_a4_17378();
				AnonFilter_a4_17379();
				AnonFilter_a4_17380();
				AnonFilter_a4_17381();
				AnonFilter_a4_17382();
				AnonFilter_a4_17383();
				AnonFilter_a4_17384();
				AnonFilter_a4_17385();
			WEIGHTED_ROUND_ROBIN_Joiner_17375();
			WEIGHTED_ROUND_ROBIN_Splitter_17386();
				iDCT8x8_1D_row_fast_17388();
				iDCT8x8_1D_row_fast_17389();
				iDCT8x8_1D_row_fast_17390();
				iDCT8x8_1D_row_fast_17391();
				iDCT8x8_1D_row_fast_17392();
				iDCT8x8_1D_row_fast_17393();
				iDCT8x8_1D_row_fast_17394();
				iDCT8x8_1D_row_fast_17395();
			WEIGHTED_ROUND_ROBIN_Joiner_17387();
			iDCT8x8_1D_col_fast_17286();
		WEIGHTED_ROUND_ROBIN_Joiner_17335();
		AnonFilter_a2_17287();
	ENDFOR
	return EXIT_SUCCESS;
}
