#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=4224 on the compile command line
#else
#if BUF_SIZEMAX < 4224
#error BUF_SIZEMAX too small, it must be at least 4224
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_17056_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_17080_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_17081_t;
void AnonFilter_a0_17053();
void DUPLICATE_Splitter_17128();
void iDCT_2D_reference_coarse_17056();
void WEIGHTED_ROUND_ROBIN_Splitter_17136();
void AnonFilter_a3_17138();
void AnonFilter_a3_17139();
void AnonFilter_a3_17140();
void AnonFilter_a3_17141();
void AnonFilter_a3_17142();
void AnonFilter_a3_17143();
void AnonFilter_a3_17144();
void AnonFilter_a3_17145();
void AnonFilter_a3_17146();
void AnonFilter_a3_17147();
void AnonFilter_a3_17148();
void WEIGHTED_ROUND_ROBIN_Joiner_17137();
void Pre_CollapsedDataParallel_1_17126();
void WEIGHTED_ROUND_ROBIN_Splitter_17149();
void iDCT_1D_reference_fine_17151();
void iDCT_1D_reference_fine_17152();
void iDCT_1D_reference_fine_17153();
void iDCT_1D_reference_fine_17154();
void iDCT_1D_reference_fine_17155();
void iDCT_1D_reference_fine_17156();
void iDCT_1D_reference_fine_17157();
void iDCT_1D_reference_fine_17158();
void WEIGHTED_ROUND_ROBIN_Joiner_17150();
void Post_CollapsedDataParallel_2_17127();
void WEIGHTED_ROUND_ROBIN_Splitter_17159();
void iDCT_1D_reference_fine_17161();
void iDCT_1D_reference_fine_17162();
void iDCT_1D_reference_fine_17163();
void iDCT_1D_reference_fine_17164();
void iDCT_1D_reference_fine_17165();
void iDCT_1D_reference_fine_17166();
void iDCT_1D_reference_fine_17167();
void iDCT_1D_reference_fine_17168();
void WEIGHTED_ROUND_ROBIN_Joiner_17160();
void WEIGHTED_ROUND_ROBIN_Splitter_17169();
void AnonFilter_a4_17171();
void AnonFilter_a4_17172();
void AnonFilter_a4_17173();
void AnonFilter_a4_17174();
void AnonFilter_a4_17175();
void AnonFilter_a4_17176();
void AnonFilter_a4_17177();
void AnonFilter_a4_17178();
void AnonFilter_a4_17179();
void AnonFilter_a4_17180();
void AnonFilter_a4_17181();
void WEIGHTED_ROUND_ROBIN_Joiner_17170();
void WEIGHTED_ROUND_ROBIN_Splitter_17182();
void iDCT8x8_1D_row_fast_17184();
void iDCT8x8_1D_row_fast_17185();
void iDCT8x8_1D_row_fast_17186();
void iDCT8x8_1D_row_fast_17187();
void iDCT8x8_1D_row_fast_17188();
void iDCT8x8_1D_row_fast_17189();
void iDCT8x8_1D_row_fast_17190();
void iDCT8x8_1D_row_fast_17191();
void WEIGHTED_ROUND_ROBIN_Joiner_17183();
void iDCT8x8_1D_col_fast_17080();
void WEIGHTED_ROUND_ROBIN_Joiner_17129();
void AnonFilter_a2_17081();

#ifdef __cplusplus
}
#endif
#endif
