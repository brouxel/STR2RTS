#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=11904 on the compile command line
#else
#if BUF_SIZEMAX < 11904
#error BUF_SIZEMAX too small, it must be at least 11904
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_12096_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_12120_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_12121_t;
void AnonFilter_a0(buffer_int_t *chanout);
void AnonFilter_a0_12093();
void DUPLICATE_Splitter_12168();
void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT_2D_reference_coarse_12096();
void WEIGHTED_ROUND_ROBIN_Splitter_12176();
void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout);
void AnonFilter_a3_12178();
void AnonFilter_a3_12179();
void AnonFilter_a3_12180();
void AnonFilter_a3_12181();
void AnonFilter_a3_12182();
void AnonFilter_a3_12183();
void AnonFilter_a3_12184();
void AnonFilter_a3_12185();
void AnonFilter_a3_12186();
void AnonFilter_a3_12187();
void AnonFilter_a3_12188();
void AnonFilter_a3_12189();
void AnonFilter_a3_12190();
void AnonFilter_a3_12191();
void AnonFilter_a3_12192();
void AnonFilter_a3_12193();
void AnonFilter_a3_12194();
void AnonFilter_a3_12195();
void AnonFilter_a3_12196();
void AnonFilter_a3_12197();
void AnonFilter_a3_12198();
void AnonFilter_a3_12199();
void AnonFilter_a3_12200();
void AnonFilter_a3_12201();
void AnonFilter_a3_12202();
void AnonFilter_a3_12203();
void AnonFilter_a3_12204();
void AnonFilter_a3_12205();
void AnonFilter_a3_12206();
void AnonFilter_a3_12207();
void AnonFilter_a3_12208();
void WEIGHTED_ROUND_ROBIN_Joiner_12177();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_12166();
void WEIGHTED_ROUND_ROBIN_Splitter_12209();
void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout);
void iDCT_1D_reference_fine_12211();
void iDCT_1D_reference_fine_12212();
void iDCT_1D_reference_fine_12213();
void iDCT_1D_reference_fine_12214();
void iDCT_1D_reference_fine_12215();
void iDCT_1D_reference_fine_12216();
void iDCT_1D_reference_fine_12217();
void iDCT_1D_reference_fine_12218();
void WEIGHTED_ROUND_ROBIN_Joiner_12210();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_12167();
void WEIGHTED_ROUND_ROBIN_Splitter_12219();
void iDCT_1D_reference_fine_12221();
void iDCT_1D_reference_fine_12222();
void iDCT_1D_reference_fine_12223();
void iDCT_1D_reference_fine_12224();
void iDCT_1D_reference_fine_12225();
void iDCT_1D_reference_fine_12226();
void iDCT_1D_reference_fine_12227();
void iDCT_1D_reference_fine_12228();
void WEIGHTED_ROUND_ROBIN_Joiner_12220();
void WEIGHTED_ROUND_ROBIN_Splitter_12229();
void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout);
void AnonFilter_a4_12231();
void AnonFilter_a4_12232();
void AnonFilter_a4_12233();
void AnonFilter_a4_12234();
void AnonFilter_a4_12235();
void AnonFilter_a4_12236();
void AnonFilter_a4_12237();
void AnonFilter_a4_12238();
void AnonFilter_a4_12239();
void AnonFilter_a4_12240();
void AnonFilter_a4_12241();
void AnonFilter_a4_12242();
void AnonFilter_a4_12243();
void AnonFilter_a4_12244();
void AnonFilter_a4_12245();
void AnonFilter_a4_12246();
void AnonFilter_a4_12247();
void AnonFilter_a4_12248();
void AnonFilter_a4_12249();
void AnonFilter_a4_12250();
void AnonFilter_a4_12251();
void AnonFilter_a4_12252();
void AnonFilter_a4_12253();
void AnonFilter_a4_12254();
void AnonFilter_a4_12255();
void AnonFilter_a4_12256();
void AnonFilter_a4_12257();
void AnonFilter_a4_12258();
void AnonFilter_a4_12259();
void AnonFilter_a4_12260();
void AnonFilter_a4_12261();
void WEIGHTED_ROUND_ROBIN_Joiner_12230();
void WEIGHTED_ROUND_ROBIN_Splitter_12262();
void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_row_fast_12264();
void iDCT8x8_1D_row_fast_12265();
void iDCT8x8_1D_row_fast_12266();
void iDCT8x8_1D_row_fast_12267();
void iDCT8x8_1D_row_fast_12268();
void iDCT8x8_1D_row_fast_12269();
void iDCT8x8_1D_row_fast_12270();
void iDCT8x8_1D_row_fast_12271();
void WEIGHTED_ROUND_ROBIN_Joiner_12263();
void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_col_fast_12120();
void WEIGHTED_ROUND_ROBIN_Joiner_12169();
void AnonFilter_a2(buffer_int_t *chanin);
void AnonFilter_a2_12121();

#ifdef __cplusplus
}
#endif
#endif
