#include "PEG62-iDCTcompare_nocache.h"

buffer_int_t SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_1487_1494_split[8];
buffer_float_t Pre_CollapsedDataParallel_1_1316WEIGHTED_ROUND_ROBIN_Splitter_1390;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_1475iDCT8x8_1D_col_fast_1270;
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_1486_1493_split[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_1244_1320_1484_1491_join[3];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_1486_1493_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1391Post_CollapsedDataParallel_2_1317;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1401WEIGHTED_ROUND_ROBIN_Splitter_1410;
buffer_int_t SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[8];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_1244_1320_1484_1491_split[3];
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[62];
buffer_int_t AnonFilter_a0_1243DUPLICATE_Splitter_1318;
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_1319AnonFilter_a2_1271;
buffer_float_t Post_CollapsedDataParallel_2_1317WEIGHTED_ROUND_ROBIN_Splitter_1400;
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[62];
buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[62];
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[62];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_1327Pre_CollapsedDataParallel_1_1316;
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_1487_1494_join[8];


iDCT_2D_reference_coarse_1246_t iDCT_2D_reference_coarse_1246_s;
iDCT_2D_reference_coarse_1246_t iDCT_1D_reference_fine_1392_s;
iDCT_2D_reference_coarse_1246_t iDCT_1D_reference_fine_1393_s;
iDCT_2D_reference_coarse_1246_t iDCT_1D_reference_fine_1394_s;
iDCT_2D_reference_coarse_1246_t iDCT_1D_reference_fine_1395_s;
iDCT_2D_reference_coarse_1246_t iDCT_1D_reference_fine_1396_s;
iDCT_2D_reference_coarse_1246_t iDCT_1D_reference_fine_1397_s;
iDCT_2D_reference_coarse_1246_t iDCT_1D_reference_fine_1398_s;
iDCT_2D_reference_coarse_1246_t iDCT_1D_reference_fine_1399_s;
iDCT_2D_reference_coarse_1246_t iDCT_1D_reference_fine_1402_s;
iDCT_2D_reference_coarse_1246_t iDCT_1D_reference_fine_1403_s;
iDCT_2D_reference_coarse_1246_t iDCT_1D_reference_fine_1404_s;
iDCT_2D_reference_coarse_1246_t iDCT_1D_reference_fine_1405_s;
iDCT_2D_reference_coarse_1246_t iDCT_1D_reference_fine_1406_s;
iDCT_2D_reference_coarse_1246_t iDCT_1D_reference_fine_1407_s;
iDCT_2D_reference_coarse_1246_t iDCT_1D_reference_fine_1408_s;
iDCT_2D_reference_coarse_1246_t iDCT_1D_reference_fine_1409_s;
iDCT8x8_1D_col_fast_1270_t iDCT8x8_1D_col_fast_1270_s;
AnonFilter_a2_1271_t AnonFilter_a2_1271_s;

void AnonFilter_a0_1243(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&AnonFilter_a0_1243DUPLICATE_Splitter_1318, (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_2D_reference_coarse_1246(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_1246_s.coeff[k][j] * peek_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_1244_1320_1484_1491_split[0], ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_1246_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_1244_1320_1484_1491_join[0], ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_1244_1320_1484_1491_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void AnonFilter_a3_1328(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[0], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[0])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1329(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[1], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[1])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1330(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[2], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[2])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1331(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[3], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[3])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1332(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[4], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[4])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1333(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[5], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[5])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1334(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[6], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[6])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1335(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[7], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[7])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1336(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[8], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[8])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1337(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[9], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[9])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1338(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[10], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[10])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1339(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[11], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[11])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1340(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[12], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[12])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1341(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[13], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[13])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1342(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[14], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[14])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1343(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[15], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[15])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1344(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[16], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[16])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1345(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[17], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[17])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1346(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[18], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[18])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1347(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[19], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[19])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1348(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[20], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[20])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1349(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[21], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[21])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1350(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[22], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[22])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1351(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[23], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[23])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1352(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[24], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[24])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1353(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[25], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[25])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1354(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[26], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[26])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1355(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[27], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[27])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1356(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[28], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[28])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1357(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[29], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[29])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1358(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[30], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[30])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1359(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[31], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[31])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1360(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[32], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[32])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1361(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[33], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[33])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1362(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[34], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[34])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1363(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[35], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[35])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1364(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[36], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[36])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1365(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[37], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[37])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1366(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[38], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[38])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1367(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[39], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[39])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1368(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[40], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[40])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1369(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[41], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[41])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1370(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[42], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[42])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1371(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[43], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[43])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1372(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[44], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[44])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1373(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[45], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[45])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1374(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[46], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[46])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1375(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[47], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[47])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1376(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[48], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[48])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1377(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[49], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[49])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1378(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[50], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[50])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1379(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[51], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[51])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1380(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[52], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[52])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1381(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[53], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[53])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1382(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[54], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[54])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1383(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[55], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[55])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1384(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[56], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[56])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1385(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[57], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[57])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1386(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[58], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[58])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1387(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[59], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[59])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1388(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[60], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[60])) ; 
	}
	ENDFOR
}

void AnonFilter_a3_1389(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[61], pop_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[61])) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1326() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 62, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_1244_1320_1484_1491_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1327() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 62, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1327Pre_CollapsedDataParallel_1_1316, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1_1316(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = 0 ; 
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Pre_CollapsedDataParallel_1_1316WEIGHTED_ROUND_ROBIN_Splitter_1390, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_1327Pre_CollapsedDataParallel_1_1316, (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1327Pre_CollapsedDataParallel_1_1316) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_1392(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_1392_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1486_1493_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1486_1493_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1486_1493_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_1393(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_1393_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1486_1493_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1486_1493_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1486_1493_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_1394(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_1394_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1486_1493_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1486_1493_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1486_1493_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_1395(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_1395_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1486_1493_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1486_1493_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1486_1493_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_1396(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_1396_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1486_1493_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1486_1493_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1486_1493_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_1397(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_1397_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1486_1493_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1486_1493_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1486_1493_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_1398(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_1398_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1486_1493_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1486_1493_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1486_1493_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_1399(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_1399_s.coeff[x][u] * peek_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1486_1493_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1486_1493_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1486_1493_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1390() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1486_1493_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_1316WEIGHTED_ROUND_ROBIN_Splitter_1390));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1391() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1391Post_CollapsedDataParallel_2_1317, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1486_1493_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2_1317(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
			partialSum_i = 0 ; 
			partialSum_i = 0 ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&Post_CollapsedDataParallel_2_1317WEIGHTED_ROUND_ROBIN_Splitter_1400, peek_float(&WEIGHTED_ROUND_ROBIN_Joiner_1391Post_CollapsedDataParallel_2_1317, (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1391Post_CollapsedDataParallel_2_1317) ; 
	}
	ENDFOR
}

void iDCT_1D_reference_fine_1402(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_1402_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1487_1494_split[0], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1487_1494_join[0], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1487_1494_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_1403(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_1403_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1487_1494_split[1], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1487_1494_join[1], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1487_1494_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_1404(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_1404_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1487_1494_split[2], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1487_1494_join[2], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1487_1494_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_1405(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_1405_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1487_1494_split[3], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1487_1494_join[3], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1487_1494_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_1406(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_1406_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1487_1494_split[4], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1487_1494_join[4], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1487_1494_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_1407(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_1407_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1487_1494_split[5], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1487_1494_join[5], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1487_1494_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_1408(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_1408_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1487_1494_split[6], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1487_1494_join[6], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1487_1494_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT_1D_reference_fine_1409(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			tempsum = 0.0 ; 
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_1409_s.coeff[x][u] * peek_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1487_1494_split[7], u))) ; 
			}
			ENDFOR
			push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1487_1494_join[7], tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1487_1494_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1400() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1487_1494_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_1317WEIGHTED_ROUND_ROBIN_Splitter_1400));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1401() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_1401WEIGHTED_ROUND_ROBIN_Splitter_1410, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1487_1494_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4_1412(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[0], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[0]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1413(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[1], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[1]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1414(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[2], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[2]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1415(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[3], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[3]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1416(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[4], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[4]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1417(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[5], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[5]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1418(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[6], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[6]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1419(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[7], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[7]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1420(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[8], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[8]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1421(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[9], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[9]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1422(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[10], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[10]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1423(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[11], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[11]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1424(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[12], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[12]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1425(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[13], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[13]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1426(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[14], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[14]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1427(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[15], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[15]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1428(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[16], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[16]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1429(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[17], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[17]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1430(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[18], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[18]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1431(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[19], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[19]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1432(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[20], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[20]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1433(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[21], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[21]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1434(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[22], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[22]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1435(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[23], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[23]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1436(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[24], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[24]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1437(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[25], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[25]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1438(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[26], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[26]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1439(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[27], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[27]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1440(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[28], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[28]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1441(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[29], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[29]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1442(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[30], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[30]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1443(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[31], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[31]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1444(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[32], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[32]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1445(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[33], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[33]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1446(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[34], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[34]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1447(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[35], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[35]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1448(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[36], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[36]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1449(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[37], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[37]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1450(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[38], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[38]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1451(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[39], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[39]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1452(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[40], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[40]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1453(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[41], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[41]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1454(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[42], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[42]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1455(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[43], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[43]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1456(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[44], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[44]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1457(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[45], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[45]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1458(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[46], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[46]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1459(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[47], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[47]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1460(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[48], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[48]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1461(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[49], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[49]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1462(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[50], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[50]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1463(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[51], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[51]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1464(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[52], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[52]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1465(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[53], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[53]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1466(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[54], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[54]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1467(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[55], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[55]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1468(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[56], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[56]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1469(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[57], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[57]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1470(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[58], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[58]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1471(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[59], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[59]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1472(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[60], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[60]) + 0.5)))) ; 
	}
	ENDFOR
}

void AnonFilter_a4_1473(){
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++) {
		push_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[61], ((int) floor((pop_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[61]) + 0.5)))) ; 
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1410() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 62, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_1401WEIGHTED_ROUND_ROBIN_Splitter_1410));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1411() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 32, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 62, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_1244_1320_1484_1491_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast_1476(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[0], 0) ; 
		x1 = (peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[0], 4) << 11) ; 
		x2 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[0], 6) ; 
		x3 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[0], 2) ; 
		x4 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[0], 1) ; 
		x5 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[0], 7) ; 
		x6 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[0], 5) ; 
		x7 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[0], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[0], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[0], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[0], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[0], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[0], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[0], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[0], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[0], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[0], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[0]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_1477(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[1], 0) ; 
		x1 = (peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[1], 4) << 11) ; 
		x2 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[1], 6) ; 
		x3 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[1], 2) ; 
		x4 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[1], 1) ; 
		x5 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[1], 7) ; 
		x6 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[1], 5) ; 
		x7 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[1], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[1], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[1], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[1], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[1], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[1], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[1], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[1], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[1], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[1], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[1]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_1478(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[2], 0) ; 
		x1 = (peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[2], 4) << 11) ; 
		x2 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[2], 6) ; 
		x3 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[2], 2) ; 
		x4 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[2], 1) ; 
		x5 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[2], 7) ; 
		x6 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[2], 5) ; 
		x7 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[2], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[2], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[2], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[2], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[2], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[2], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[2], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[2], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[2], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[2], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[2]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_1479(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[3], 0) ; 
		x1 = (peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[3], 4) << 11) ; 
		x2 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[3], 6) ; 
		x3 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[3], 2) ; 
		x4 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[3], 1) ; 
		x5 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[3], 7) ; 
		x6 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[3], 5) ; 
		x7 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[3], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[3], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[3], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[3], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[3], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[3], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[3], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[3], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[3], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[3], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[3]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_1480(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[4], 0) ; 
		x1 = (peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[4], 4) << 11) ; 
		x2 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[4], 6) ; 
		x3 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[4], 2) ; 
		x4 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[4], 1) ; 
		x5 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[4], 7) ; 
		x6 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[4], 5) ; 
		x7 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[4], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[4], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[4], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[4], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[4], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[4], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[4], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[4], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[4], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[4], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[4]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_1481(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[5], 0) ; 
		x1 = (peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[5], 4) << 11) ; 
		x2 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[5], 6) ; 
		x3 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[5], 2) ; 
		x4 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[5], 1) ; 
		x5 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[5], 7) ; 
		x6 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[5], 5) ; 
		x7 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[5], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[5], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[5], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[5], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[5], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[5], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[5], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[5], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[5], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[5], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[5]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_1482(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[6], 0) ; 
		x1 = (peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[6], 4) << 11) ; 
		x2 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[6], 6) ; 
		x3 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[6], 2) ; 
		x4 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[6], 1) ; 
		x5 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[6], 7) ; 
		x6 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[6], 5) ; 
		x7 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[6], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[6], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[6], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[6], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[6], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[6], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[6], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[6], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[6], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[6], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[6]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void iDCT8x8_1D_row_fast_1483(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[7], 0) ; 
		x1 = (peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[7], 4) << 11) ; 
		x2 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[7], 6) ; 
		x3 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[7], 2) ; 
		x4 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[7], 1) ; 
		x5 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[7], 7) ; 
		x6 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[7], 5) ; 
		x7 = peek_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[7], 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[7], x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[7], ((x7 + x1) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[7], ((x3 + x2) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[7], ((x0 + x4) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[7], ((x8 + x6) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[7], ((x8 - x6) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[7], ((x0 - x4) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[7], ((x3 - x2) >> 8)) ; 
			push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[7], ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[7]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_1474() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_1244_1320_1484_1491_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1475() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_1475iDCT8x8_1D_col_fast_1270, pop_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast_1270(){
	FOR(uint32_t, __iter_steady_, 0, <, 31, __iter_steady_++) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_1475iDCT8x8_1D_col_fast_1270, (c + 0)) ; 
			x1 = (peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_1475iDCT8x8_1D_col_fast_1270, (c + 32)) << 8) ; 
			x2 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_1475iDCT8x8_1D_col_fast_1270, (c + 48)) ; 
			x3 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_1475iDCT8x8_1D_col_fast_1270, (c + 16)) ; 
			x4 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_1475iDCT8x8_1D_col_fast_1270, (c + 8)) ; 
			x5 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_1475iDCT8x8_1D_col_fast_1270, (c + 56)) ; 
			x6 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_1475iDCT8x8_1D_col_fast_1270, (c + 40)) ; 
			x7 = peek_int(&WEIGHTED_ROUND_ROBIN_Joiner_1475iDCT8x8_1D_col_fast_1270, (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_1270_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_1270_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_1270_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_1270_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_1270_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_1270_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_1270_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_1270_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_1270_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_1475iDCT8x8_1D_col_fast_1270) ; 
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_1244_1320_1484_1491_join[2], iDCT8x8_1D_col_fast_1270_s.buffer[i]) ; 
		}
		ENDFOR
	}
	ENDFOR
}

void DUPLICATE_Splitter_1318() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1984, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_1243DUPLICATE_Splitter_1318);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_1244_1320_1484_1491_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_1319() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 1984, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_1319AnonFilter_a2_1271, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_1244_1320_1484_1491_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2_1271(){
	FOR(uint32_t, __iter_steady_, 0, <, 1984, __iter_steady_++) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_1319AnonFilter_a2_1271) ; 
		reffine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_1319AnonFilter_a2_1271) ; 
		fastfine = pop_int(&WEIGHTED_ROUND_ROBIN_Joiner_1319AnonFilter_a2_1271) ; 
		AnonFilter_a2_1271_s.count = (AnonFilter_a2_1271_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_1271_s.errors = (AnonFilter_a2_1271_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_1271_s.errors / AnonFilter_a2_1271_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_1271_s.errors = (AnonFilter_a2_1271_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_1271_s.errors / AnonFilter_a2_1271_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 8, __iter_init_0_++)
		init_buffer_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 8, __iter_init_1_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1487_1494_split[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_1316WEIGHTED_ROUND_ROBIN_Splitter_1390);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_1475iDCT8x8_1D_col_fast_1270);
	FOR(int, __iter_init_2_, 0, <, 8, __iter_init_2_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1486_1493_split[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 3, __iter_init_3_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_1244_1320_1484_1491_join[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_1486_1493_join[__iter_init_4_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1391Post_CollapsedDataParallel_2_1317);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1401WEIGHTED_ROUND_ROBIN_Splitter_1410);
	FOR(int, __iter_init_5_, 0, <, 8, __iter_init_5_++)
		init_buffer_int(&SplitJoin148_iDCT8x8_1D_row_fast_Fiss_1489_1496_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 3, __iter_init_6_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_1244_1320_1484_1491_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 62, __iter_init_7_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_join[__iter_init_7_]);
	ENDFOR
	init_buffer_int(&AnonFilter_a0_1243DUPLICATE_Splitter_1318);
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_1319AnonFilter_a2_1271);
	init_buffer_float(&Post_CollapsedDataParallel_2_1317WEIGHTED_ROUND_ROBIN_Splitter_1400);
	FOR(int, __iter_init_8_, 0, <, 62, __iter_init_8_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_1485_1492_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 62, __iter_init_9_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_split[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 62, __iter_init_10_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_1488_1495_join[__iter_init_10_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_1327Pre_CollapsedDataParallel_1_1316);
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_1487_1494_join[__iter_init_11_]);
	ENDFOR
// --- init: iDCT_2D_reference_coarse_1246
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_1246_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1392
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1392_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1393
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1393_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1394
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1394_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1395
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1395_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1396
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1396_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1397
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1397_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1398
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1398_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1399
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1399_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1402
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1402_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1403
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1403_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1404
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1404_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1405
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1405_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1406
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1406_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1407
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1407_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1408
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1408_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_1409
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 0.0 ; 
			Cu = 0.0 ; 
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_1409_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_1271
	 {
	AnonFilter_a2_1271_s.count = 0.0 ; 
	AnonFilter_a2_1271_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_1243();
		DUPLICATE_Splitter_1318();
			iDCT_2D_reference_coarse_1246();
			WEIGHTED_ROUND_ROBIN_Splitter_1326();
				AnonFilter_a3_1328();
				AnonFilter_a3_1329();
				AnonFilter_a3_1330();
				AnonFilter_a3_1331();
				AnonFilter_a3_1332();
				AnonFilter_a3_1333();
				AnonFilter_a3_1334();
				AnonFilter_a3_1335();
				AnonFilter_a3_1336();
				AnonFilter_a3_1337();
				AnonFilter_a3_1338();
				AnonFilter_a3_1339();
				AnonFilter_a3_1340();
				AnonFilter_a3_1341();
				AnonFilter_a3_1342();
				AnonFilter_a3_1343();
				AnonFilter_a3_1344();
				AnonFilter_a3_1345();
				AnonFilter_a3_1346();
				AnonFilter_a3_1347();
				AnonFilter_a3_1348();
				AnonFilter_a3_1349();
				AnonFilter_a3_1350();
				AnonFilter_a3_1351();
				AnonFilter_a3_1352();
				AnonFilter_a3_1353();
				AnonFilter_a3_1354();
				AnonFilter_a3_1355();
				AnonFilter_a3_1356();
				AnonFilter_a3_1357();
				AnonFilter_a3_1358();
				AnonFilter_a3_1359();
				AnonFilter_a3_1360();
				AnonFilter_a3_1361();
				AnonFilter_a3_1362();
				AnonFilter_a3_1363();
				AnonFilter_a3_1364();
				AnonFilter_a3_1365();
				AnonFilter_a3_1366();
				AnonFilter_a3_1367();
				AnonFilter_a3_1368();
				AnonFilter_a3_1369();
				AnonFilter_a3_1370();
				AnonFilter_a3_1371();
				AnonFilter_a3_1372();
				AnonFilter_a3_1373();
				AnonFilter_a3_1374();
				AnonFilter_a3_1375();
				AnonFilter_a3_1376();
				AnonFilter_a3_1377();
				AnonFilter_a3_1378();
				AnonFilter_a3_1379();
				AnonFilter_a3_1380();
				AnonFilter_a3_1381();
				AnonFilter_a3_1382();
				AnonFilter_a3_1383();
				AnonFilter_a3_1384();
				AnonFilter_a3_1385();
				AnonFilter_a3_1386();
				AnonFilter_a3_1387();
				AnonFilter_a3_1388();
				AnonFilter_a3_1389();
			WEIGHTED_ROUND_ROBIN_Joiner_1327();
			Pre_CollapsedDataParallel_1_1316();
			WEIGHTED_ROUND_ROBIN_Splitter_1390();
				iDCT_1D_reference_fine_1392();
				iDCT_1D_reference_fine_1393();
				iDCT_1D_reference_fine_1394();
				iDCT_1D_reference_fine_1395();
				iDCT_1D_reference_fine_1396();
				iDCT_1D_reference_fine_1397();
				iDCT_1D_reference_fine_1398();
				iDCT_1D_reference_fine_1399();
			WEIGHTED_ROUND_ROBIN_Joiner_1391();
			Post_CollapsedDataParallel_2_1317();
			WEIGHTED_ROUND_ROBIN_Splitter_1400();
				iDCT_1D_reference_fine_1402();
				iDCT_1D_reference_fine_1403();
				iDCT_1D_reference_fine_1404();
				iDCT_1D_reference_fine_1405();
				iDCT_1D_reference_fine_1406();
				iDCT_1D_reference_fine_1407();
				iDCT_1D_reference_fine_1408();
				iDCT_1D_reference_fine_1409();
			WEIGHTED_ROUND_ROBIN_Joiner_1401();
			WEIGHTED_ROUND_ROBIN_Splitter_1410();
				AnonFilter_a4_1412();
				AnonFilter_a4_1413();
				AnonFilter_a4_1414();
				AnonFilter_a4_1415();
				AnonFilter_a4_1416();
				AnonFilter_a4_1417();
				AnonFilter_a4_1418();
				AnonFilter_a4_1419();
				AnonFilter_a4_1420();
				AnonFilter_a4_1421();
				AnonFilter_a4_1422();
				AnonFilter_a4_1423();
				AnonFilter_a4_1424();
				AnonFilter_a4_1425();
				AnonFilter_a4_1426();
				AnonFilter_a4_1427();
				AnonFilter_a4_1428();
				AnonFilter_a4_1429();
				AnonFilter_a4_1430();
				AnonFilter_a4_1431();
				AnonFilter_a4_1432();
				AnonFilter_a4_1433();
				AnonFilter_a4_1434();
				AnonFilter_a4_1435();
				AnonFilter_a4_1436();
				AnonFilter_a4_1437();
				AnonFilter_a4_1438();
				AnonFilter_a4_1439();
				AnonFilter_a4_1440();
				AnonFilter_a4_1441();
				AnonFilter_a4_1442();
				AnonFilter_a4_1443();
				AnonFilter_a4_1444();
				AnonFilter_a4_1445();
				AnonFilter_a4_1446();
				AnonFilter_a4_1447();
				AnonFilter_a4_1448();
				AnonFilter_a4_1449();
				AnonFilter_a4_1450();
				AnonFilter_a4_1451();
				AnonFilter_a4_1452();
				AnonFilter_a4_1453();
				AnonFilter_a4_1454();
				AnonFilter_a4_1455();
				AnonFilter_a4_1456();
				AnonFilter_a4_1457();
				AnonFilter_a4_1458();
				AnonFilter_a4_1459();
				AnonFilter_a4_1460();
				AnonFilter_a4_1461();
				AnonFilter_a4_1462();
				AnonFilter_a4_1463();
				AnonFilter_a4_1464();
				AnonFilter_a4_1465();
				AnonFilter_a4_1466();
				AnonFilter_a4_1467();
				AnonFilter_a4_1468();
				AnonFilter_a4_1469();
				AnonFilter_a4_1470();
				AnonFilter_a4_1471();
				AnonFilter_a4_1472();
				AnonFilter_a4_1473();
			WEIGHTED_ROUND_ROBIN_Joiner_1411();
			WEIGHTED_ROUND_ROBIN_Splitter_1474();
				iDCT8x8_1D_row_fast_1476();
				iDCT8x8_1D_row_fast_1477();
				iDCT8x8_1D_row_fast_1478();
				iDCT8x8_1D_row_fast_1479();
				iDCT8x8_1D_row_fast_1480();
				iDCT8x8_1D_row_fast_1481();
				iDCT8x8_1D_row_fast_1482();
				iDCT8x8_1D_row_fast_1483();
			WEIGHTED_ROUND_ROBIN_Joiner_1475();
			iDCT8x8_1D_col_fast_1270();
		WEIGHTED_ROUND_ROBIN_Joiner_1319();
		AnonFilter_a2_1271();
	ENDFOR
	return EXIT_SUCCESS;
}
