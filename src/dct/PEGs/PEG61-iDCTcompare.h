#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=23424 on the compile command line
#else
#if BUF_SIZEMAX < 23424
#error BUF_SIZEMAX too small, it must be at least 23424
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_1656_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_1680_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_1681_t;
void AnonFilter_a0(buffer_int_t *chanout);
void AnonFilter_a0_1653();
void DUPLICATE_Splitter_1728();
void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT_2D_reference_coarse_1656();
void WEIGHTED_ROUND_ROBIN_Splitter_1736();
void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout);
void AnonFilter_a3_1738();
void AnonFilter_a3_1739();
void AnonFilter_a3_1740();
void AnonFilter_a3_1741();
void AnonFilter_a3_1742();
void AnonFilter_a3_1743();
void AnonFilter_a3_1744();
void AnonFilter_a3_1745();
void AnonFilter_a3_1746();
void AnonFilter_a3_1747();
void AnonFilter_a3_1748();
void AnonFilter_a3_1749();
void AnonFilter_a3_1750();
void AnonFilter_a3_1751();
void AnonFilter_a3_1752();
void AnonFilter_a3_1753();
void AnonFilter_a3_1754();
void AnonFilter_a3_1755();
void AnonFilter_a3_1756();
void AnonFilter_a3_1757();
void AnonFilter_a3_1758();
void AnonFilter_a3_1759();
void AnonFilter_a3_1760();
void AnonFilter_a3_1761();
void AnonFilter_a3_1762();
void AnonFilter_a3_1763();
void AnonFilter_a3_1764();
void AnonFilter_a3_1765();
void AnonFilter_a3_1766();
void AnonFilter_a3_1767();
void AnonFilter_a3_1768();
void AnonFilter_a3_1769();
void AnonFilter_a3_1770();
void AnonFilter_a3_1771();
void AnonFilter_a3_1772();
void AnonFilter_a3_1773();
void AnonFilter_a3_1774();
void AnonFilter_a3_1775();
void AnonFilter_a3_1776();
void AnonFilter_a3_1777();
void AnonFilter_a3_1778();
void AnonFilter_a3_1779();
void AnonFilter_a3_1780();
void AnonFilter_a3_1781();
void AnonFilter_a3_1782();
void AnonFilter_a3_1783();
void AnonFilter_a3_1784();
void AnonFilter_a3_1785();
void AnonFilter_a3_1786();
void AnonFilter_a3_1787();
void AnonFilter_a3_1788();
void AnonFilter_a3_1789();
void AnonFilter_a3_1790();
void AnonFilter_a3_1791();
void AnonFilter_a3_1792();
void AnonFilter_a3_1793();
void AnonFilter_a3_1794();
void AnonFilter_a3_1795();
void AnonFilter_a3_1796();
void AnonFilter_a3_1797();
void AnonFilter_a3_1798();
void WEIGHTED_ROUND_ROBIN_Joiner_1737();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_1726();
void WEIGHTED_ROUND_ROBIN_Splitter_1799();
void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout);
void iDCT_1D_reference_fine_1801();
void iDCT_1D_reference_fine_1802();
void iDCT_1D_reference_fine_1803();
void iDCT_1D_reference_fine_1804();
void iDCT_1D_reference_fine_1805();
void iDCT_1D_reference_fine_1806();
void iDCT_1D_reference_fine_1807();
void iDCT_1D_reference_fine_1808();
void WEIGHTED_ROUND_ROBIN_Joiner_1800();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_1727();
void WEIGHTED_ROUND_ROBIN_Splitter_1809();
void iDCT_1D_reference_fine_1811();
void iDCT_1D_reference_fine_1812();
void iDCT_1D_reference_fine_1813();
void iDCT_1D_reference_fine_1814();
void iDCT_1D_reference_fine_1815();
void iDCT_1D_reference_fine_1816();
void iDCT_1D_reference_fine_1817();
void iDCT_1D_reference_fine_1818();
void WEIGHTED_ROUND_ROBIN_Joiner_1810();
void WEIGHTED_ROUND_ROBIN_Splitter_1819();
void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout);
void AnonFilter_a4_1821();
void AnonFilter_a4_1822();
void AnonFilter_a4_1823();
void AnonFilter_a4_1824();
void AnonFilter_a4_1825();
void AnonFilter_a4_1826();
void AnonFilter_a4_1827();
void AnonFilter_a4_1828();
void AnonFilter_a4_1829();
void AnonFilter_a4_1830();
void AnonFilter_a4_1831();
void AnonFilter_a4_1832();
void AnonFilter_a4_1833();
void AnonFilter_a4_1834();
void AnonFilter_a4_1835();
void AnonFilter_a4_1836();
void AnonFilter_a4_1837();
void AnonFilter_a4_1838();
void AnonFilter_a4_1839();
void AnonFilter_a4_1840();
void AnonFilter_a4_1841();
void AnonFilter_a4_1842();
void AnonFilter_a4_1843();
void AnonFilter_a4_1844();
void AnonFilter_a4_1845();
void AnonFilter_a4_1846();
void AnonFilter_a4_1847();
void AnonFilter_a4_1848();
void AnonFilter_a4_1849();
void AnonFilter_a4_1850();
void AnonFilter_a4_1851();
void AnonFilter_a4_1852();
void AnonFilter_a4_1853();
void AnonFilter_a4_1854();
void AnonFilter_a4_1855();
void AnonFilter_a4_1856();
void AnonFilter_a4_1857();
void AnonFilter_a4_1858();
void AnonFilter_a4_1859();
void AnonFilter_a4_1860();
void AnonFilter_a4_1861();
void AnonFilter_a4_1862();
void AnonFilter_a4_1863();
void AnonFilter_a4_1864();
void AnonFilter_a4_1865();
void AnonFilter_a4_1866();
void AnonFilter_a4_1867();
void AnonFilter_a4_1868();
void AnonFilter_a4_1869();
void AnonFilter_a4_1870();
void AnonFilter_a4_1871();
void AnonFilter_a4_1872();
void AnonFilter_a4_1873();
void AnonFilter_a4_1874();
void AnonFilter_a4_1875();
void AnonFilter_a4_1876();
void AnonFilter_a4_1877();
void AnonFilter_a4_1878();
void AnonFilter_a4_1879();
void AnonFilter_a4_1880();
void AnonFilter_a4_1881();
void WEIGHTED_ROUND_ROBIN_Joiner_1820();
void WEIGHTED_ROUND_ROBIN_Splitter_1882();
void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_row_fast_1884();
void iDCT8x8_1D_row_fast_1885();
void iDCT8x8_1D_row_fast_1886();
void iDCT8x8_1D_row_fast_1887();
void iDCT8x8_1D_row_fast_1888();
void iDCT8x8_1D_row_fast_1889();
void iDCT8x8_1D_row_fast_1890();
void iDCT8x8_1D_row_fast_1891();
void WEIGHTED_ROUND_ROBIN_Joiner_1883();
void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_col_fast_1680();
void WEIGHTED_ROUND_ROBIN_Joiner_1729();
void AnonFilter_a2(buffer_int_t *chanin);
void AnonFilter_a2_1681();

#ifdef __cplusplus
}
#endif
#endif
