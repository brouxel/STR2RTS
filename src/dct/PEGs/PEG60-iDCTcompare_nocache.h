#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=5760 on the compile command line
#else
#if BUF_SIZEMAX < 5760
#error BUF_SIZEMAX too small, it must be at least 5760
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_2062_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_2086_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_2087_t;
void AnonFilter_a0_2059();
void DUPLICATE_Splitter_2134();
void iDCT_2D_reference_coarse_2062();
void WEIGHTED_ROUND_ROBIN_Splitter_2142();
void AnonFilter_a3_2144();
void AnonFilter_a3_2145();
void AnonFilter_a3_2146();
void AnonFilter_a3_2147();
void AnonFilter_a3_2148();
void AnonFilter_a3_2149();
void AnonFilter_a3_2150();
void AnonFilter_a3_2151();
void AnonFilter_a3_2152();
void AnonFilter_a3_2153();
void AnonFilter_a3_2154();
void AnonFilter_a3_2155();
void AnonFilter_a3_2156();
void AnonFilter_a3_2157();
void AnonFilter_a3_2158();
void AnonFilter_a3_2159();
void AnonFilter_a3_2160();
void AnonFilter_a3_2161();
void AnonFilter_a3_2162();
void AnonFilter_a3_2163();
void AnonFilter_a3_2164();
void AnonFilter_a3_2165();
void AnonFilter_a3_2166();
void AnonFilter_a3_2167();
void AnonFilter_a3_2168();
void AnonFilter_a3_2169();
void AnonFilter_a3_2170();
void AnonFilter_a3_2171();
void AnonFilter_a3_2172();
void AnonFilter_a3_2173();
void AnonFilter_a3_2174();
void AnonFilter_a3_2175();
void AnonFilter_a3_2176();
void AnonFilter_a3_2177();
void AnonFilter_a3_2178();
void AnonFilter_a3_2179();
void AnonFilter_a3_2180();
void AnonFilter_a3_2181();
void AnonFilter_a3_2182();
void AnonFilter_a3_2183();
void AnonFilter_a3_2184();
void AnonFilter_a3_2185();
void AnonFilter_a3_2186();
void AnonFilter_a3_2187();
void AnonFilter_a3_2188();
void AnonFilter_a3_2189();
void AnonFilter_a3_2190();
void AnonFilter_a3_2191();
void AnonFilter_a3_2192();
void AnonFilter_a3_2193();
void AnonFilter_a3_2194();
void AnonFilter_a3_2195();
void AnonFilter_a3_2196();
void AnonFilter_a3_2197();
void AnonFilter_a3_2198();
void AnonFilter_a3_2199();
void AnonFilter_a3_2200();
void AnonFilter_a3_2201();
void AnonFilter_a3_2202();
void AnonFilter_a3_2203();
void WEIGHTED_ROUND_ROBIN_Joiner_2143();
void Pre_CollapsedDataParallel_1_2132();
void WEIGHTED_ROUND_ROBIN_Splitter_2204();
void iDCT_1D_reference_fine_2206();
void iDCT_1D_reference_fine_2207();
void iDCT_1D_reference_fine_2208();
void iDCT_1D_reference_fine_2209();
void iDCT_1D_reference_fine_2210();
void iDCT_1D_reference_fine_2211();
void iDCT_1D_reference_fine_2212();
void iDCT_1D_reference_fine_2213();
void WEIGHTED_ROUND_ROBIN_Joiner_2205();
void Post_CollapsedDataParallel_2_2133();
void WEIGHTED_ROUND_ROBIN_Splitter_2214();
void iDCT_1D_reference_fine_2216();
void iDCT_1D_reference_fine_2217();
void iDCT_1D_reference_fine_2218();
void iDCT_1D_reference_fine_2219();
void iDCT_1D_reference_fine_2220();
void iDCT_1D_reference_fine_2221();
void iDCT_1D_reference_fine_2222();
void iDCT_1D_reference_fine_2223();
void WEIGHTED_ROUND_ROBIN_Joiner_2215();
void WEIGHTED_ROUND_ROBIN_Splitter_2224();
void AnonFilter_a4_2226();
void AnonFilter_a4_2227();
void AnonFilter_a4_2228();
void AnonFilter_a4_2229();
void AnonFilter_a4_2230();
void AnonFilter_a4_2231();
void AnonFilter_a4_2232();
void AnonFilter_a4_2233();
void AnonFilter_a4_2234();
void AnonFilter_a4_2235();
void AnonFilter_a4_2236();
void AnonFilter_a4_2237();
void AnonFilter_a4_2238();
void AnonFilter_a4_2239();
void AnonFilter_a4_2240();
void AnonFilter_a4_2241();
void AnonFilter_a4_2242();
void AnonFilter_a4_2243();
void AnonFilter_a4_2244();
void AnonFilter_a4_2245();
void AnonFilter_a4_2246();
void AnonFilter_a4_2247();
void AnonFilter_a4_2248();
void AnonFilter_a4_2249();
void AnonFilter_a4_2250();
void AnonFilter_a4_2251();
void AnonFilter_a4_2252();
void AnonFilter_a4_2253();
void AnonFilter_a4_2254();
void AnonFilter_a4_2255();
void AnonFilter_a4_2256();
void AnonFilter_a4_2257();
void AnonFilter_a4_2258();
void AnonFilter_a4_2259();
void AnonFilter_a4_2260();
void AnonFilter_a4_2261();
void AnonFilter_a4_2262();
void AnonFilter_a4_2263();
void AnonFilter_a4_2264();
void AnonFilter_a4_2265();
void AnonFilter_a4_2266();
void AnonFilter_a4_2267();
void AnonFilter_a4_2268();
void AnonFilter_a4_2269();
void AnonFilter_a4_2270();
void AnonFilter_a4_2271();
void AnonFilter_a4_2272();
void AnonFilter_a4_2273();
void AnonFilter_a4_2274();
void AnonFilter_a4_2275();
void AnonFilter_a4_2276();
void AnonFilter_a4_2277();
void AnonFilter_a4_2278();
void AnonFilter_a4_2279();
void AnonFilter_a4_2280();
void AnonFilter_a4_2281();
void AnonFilter_a4_2282();
void AnonFilter_a4_2283();
void AnonFilter_a4_2284();
void AnonFilter_a4_2285();
void WEIGHTED_ROUND_ROBIN_Joiner_2225();
void WEIGHTED_ROUND_ROBIN_Splitter_2286();
void iDCT8x8_1D_row_fast_2288();
void iDCT8x8_1D_row_fast_2289();
void iDCT8x8_1D_row_fast_2290();
void iDCT8x8_1D_row_fast_2291();
void iDCT8x8_1D_row_fast_2292();
void iDCT8x8_1D_row_fast_2293();
void iDCT8x8_1D_row_fast_2294();
void iDCT8x8_1D_row_fast_2295();
void WEIGHTED_ROUND_ROBIN_Joiner_2287();
void iDCT8x8_1D_col_fast_2086();
void WEIGHTED_ROUND_ROBIN_Joiner_2135();
void AnonFilter_a2_2087();

#ifdef __cplusplus
}
#endif
#endif
