#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=384 on the compile command line
#else
#if BUF_SIZEMAX < 384
#error BUF_SIZEMAX too small, it must be at least 384
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_17662_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_17686_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_17687_t;
void AnonFilter_a0(buffer_int_t *chanout);
void AnonFilter_a0_17659();
void DUPLICATE_Splitter_17734();
void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT_2D_reference_coarse_17662();
void WEIGHTED_ROUND_ROBIN_Splitter_17742();
void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout);
void AnonFilter_a3_17744();
void AnonFilter_a3_17745();
void AnonFilter_a3_17746();
void AnonFilter_a3_17747();
void AnonFilter_a3_17748();
void AnonFilter_a3_17749();
void AnonFilter_a3_17750();
void AnonFilter_a3_17751();
void WEIGHTED_ROUND_ROBIN_Joiner_17743();
void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout);
void Pre_CollapsedDataParallel_1_17732();
void WEIGHTED_ROUND_ROBIN_Splitter_17752();
void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout);
void iDCT_1D_reference_fine_17754();
void iDCT_1D_reference_fine_17755();
void iDCT_1D_reference_fine_17756();
void iDCT_1D_reference_fine_17757();
void iDCT_1D_reference_fine_17758();
void iDCT_1D_reference_fine_17759();
void iDCT_1D_reference_fine_17760();
void iDCT_1D_reference_fine_17761();
void WEIGHTED_ROUND_ROBIN_Joiner_17753();
void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout);
void Post_CollapsedDataParallel_2_17733();
void WEIGHTED_ROUND_ROBIN_Splitter_17762();
void iDCT_1D_reference_fine_17764();
void iDCT_1D_reference_fine_17765();
void iDCT_1D_reference_fine_17766();
void iDCT_1D_reference_fine_17767();
void iDCT_1D_reference_fine_17768();
void iDCT_1D_reference_fine_17769();
void iDCT_1D_reference_fine_17770();
void iDCT_1D_reference_fine_17771();
void WEIGHTED_ROUND_ROBIN_Joiner_17763();
void WEIGHTED_ROUND_ROBIN_Splitter_17772();
void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout);
void AnonFilter_a4_17774();
void AnonFilter_a4_17775();
void AnonFilter_a4_17776();
void AnonFilter_a4_17777();
void AnonFilter_a4_17778();
void AnonFilter_a4_17779();
void AnonFilter_a4_17780();
void AnonFilter_a4_17781();
void WEIGHTED_ROUND_ROBIN_Joiner_17773();
void WEIGHTED_ROUND_ROBIN_Splitter_17782();
void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_row_fast_17784();
void iDCT8x8_1D_row_fast_17785();
void iDCT8x8_1D_row_fast_17786();
void iDCT8x8_1D_row_fast_17787();
void iDCT8x8_1D_row_fast_17788();
void iDCT8x8_1D_row_fast_17789();
void iDCT8x8_1D_row_fast_17790();
void iDCT8x8_1D_row_fast_17791();
void WEIGHTED_ROUND_ROBIN_Joiner_17783();
void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout);
void iDCT8x8_1D_col_fast_17686();
void WEIGHTED_ROUND_ROBIN_Joiner_17735();
void AnonFilter_a2(buffer_int_t *chanin);
void AnonFilter_a2_17687();

#ifdef __cplusplus
}
#endif
#endif
