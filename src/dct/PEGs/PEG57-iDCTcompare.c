#include "PEG57-iDCTcompare.h"

buffer_float_t SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[57];
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_3254_3330_3484_3491_split[3];
buffer_float_t Pre_CollapsedDataParallel_1_3326WEIGHTED_ROUND_ROBIN_Splitter_3395;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3406WEIGHTED_ROUND_ROBIN_Splitter_3415;
buffer_int_t SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_3254_3330_3484_3491_join[3];
buffer_int_t SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[57];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_3486_3493_join[8];
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3337Pre_CollapsedDataParallel_1_3326;
buffer_float_t SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[57];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_3487_3494_split[8];
buffer_int_t SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[57];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_3475iDCT8x8_1D_col_fast_3280;
buffer_int_t AnonFilter_a0_3253DUPLICATE_Splitter_3328;
buffer_float_t WEIGHTED_ROUND_ROBIN_Joiner_3396Post_CollapsedDataParallel_2_3327;
buffer_int_t SplitJoin138_iDCT8x8_1D_row_fast_Fiss_3489_3496_split[8];
buffer_float_t SplitJoin7_iDCT_1D_reference_fine_Fiss_3487_3494_join[8];
buffer_float_t SplitJoin5_iDCT_1D_reference_fine_Fiss_3486_3493_split[8];
buffer_float_t Post_CollapsedDataParallel_2_3327WEIGHTED_ROUND_ROBIN_Splitter_3405;
buffer_int_t SplitJoin138_iDCT8x8_1D_row_fast_Fiss_3489_3496_join[8];
buffer_int_t WEIGHTED_ROUND_ROBIN_Joiner_3329AnonFilter_a2_3281;


iDCT_2D_reference_coarse_3256_t iDCT_2D_reference_coarse_3256_s;
iDCT_2D_reference_coarse_3256_t iDCT_1D_reference_fine_3397_s;
iDCT_2D_reference_coarse_3256_t iDCT_1D_reference_fine_3398_s;
iDCT_2D_reference_coarse_3256_t iDCT_1D_reference_fine_3399_s;
iDCT_2D_reference_coarse_3256_t iDCT_1D_reference_fine_3400_s;
iDCT_2D_reference_coarse_3256_t iDCT_1D_reference_fine_3401_s;
iDCT_2D_reference_coarse_3256_t iDCT_1D_reference_fine_3402_s;
iDCT_2D_reference_coarse_3256_t iDCT_1D_reference_fine_3403_s;
iDCT_2D_reference_coarse_3256_t iDCT_1D_reference_fine_3404_s;
iDCT_2D_reference_coarse_3256_t iDCT_1D_reference_fine_3407_s;
iDCT_2D_reference_coarse_3256_t iDCT_1D_reference_fine_3408_s;
iDCT_2D_reference_coarse_3256_t iDCT_1D_reference_fine_3409_s;
iDCT_2D_reference_coarse_3256_t iDCT_1D_reference_fine_3410_s;
iDCT_2D_reference_coarse_3256_t iDCT_1D_reference_fine_3411_s;
iDCT_2D_reference_coarse_3256_t iDCT_1D_reference_fine_3412_s;
iDCT_2D_reference_coarse_3256_t iDCT_1D_reference_fine_3413_s;
iDCT_2D_reference_coarse_3256_t iDCT_1D_reference_fine_3414_s;
iDCT8x8_1D_col_fast_3280_t iDCT8x8_1D_col_fast_3280_s;
AnonFilter_a2_3281_t AnonFilter_a2_3281_s;

void AnonFilter_a0(buffer_int_t *chanout) {
		FOR(int, i, 0,  < , 64, i++) {
			push_int(&(*chanout), (((int) pow(3.0, i)) % 75)) ; 
		}
		ENDFOR
	}


void AnonFilter_a0_3253() {
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		AnonFilter_a0(&(AnonFilter_a0_3253DUPLICATE_Splitter_3328));
	ENDFOR
}

void iDCT_2D_reference_coarse(buffer_int_t *chanin, buffer_int_t *chanout) {
		float block_x[8][8];
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				block_x[i][j] = 0.0 ; 
				FOR(int, k, 0,  < , 8, k++) {
					block_x[i][j] = (block_x[i][j] + (iDCT_2D_reference_coarse_3256_s.coeff[k][j] * peek_int(&(*chanin), ((8 * i) + k)))) ; 
				}
				ENDFOR
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 8, i++) {
			FOR(int, j, 0,  < , 8, j++) {
				float block_y = 0.0;
				FOR(int, k, 0,  < , 8, k++) {
					block_y = (block_y + (iDCT_2D_reference_coarse_3256_s.coeff[k][i] * block_x[k][j])) ; 
				}
				ENDFOR
				block_y = ((float) floor((block_y + 0.5))) ; 
				push_int(&(*chanout), ((int) block_y)) ; 
			}
			ENDFOR
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_2D_reference_coarse_3256() {
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		iDCT_2D_reference_coarse(&(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_3254_3330_3484_3491_split[0]), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_3254_3330_3484_3491_join[0]));
	ENDFOR
}

void AnonFilter_a3(buffer_int_t *chanin, buffer_float_t *chanout) {
		push_float(&(*chanout), pop_int(&(*chanin))) ; 
	}


void AnonFilter_a3_3338() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[0]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[0]));
	ENDFOR
}

void AnonFilter_a3_3339() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[1]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[1]));
	ENDFOR
}

void AnonFilter_a3_3340() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[2]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[2]));
	ENDFOR
}

void AnonFilter_a3_3341() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[3]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[3]));
	ENDFOR
}

void AnonFilter_a3_3342() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[4]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[4]));
	ENDFOR
}

void AnonFilter_a3_3343() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[5]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[5]));
	ENDFOR
}

void AnonFilter_a3_3344() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[6]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[6]));
	ENDFOR
}

void AnonFilter_a3_3345() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[7]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[7]));
	ENDFOR
}

void AnonFilter_a3_3346() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[8]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[8]));
	ENDFOR
}

void AnonFilter_a3_3347() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[9]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[9]));
	ENDFOR
}

void AnonFilter_a3_3348() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[10]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[10]));
	ENDFOR
}

void AnonFilter_a3_3349() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[11]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[11]));
	ENDFOR
}

void AnonFilter_a3_3350() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[12]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[12]));
	ENDFOR
}

void AnonFilter_a3_3351() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[13]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[13]));
	ENDFOR
}

void AnonFilter_a3_3352() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[14]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[14]));
	ENDFOR
}

void AnonFilter_a3_3353() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[15]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[15]));
	ENDFOR
}

void AnonFilter_a3_3354() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[16]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[16]));
	ENDFOR
}

void AnonFilter_a3_3355() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[17]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[17]));
	ENDFOR
}

void AnonFilter_a3_3356() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[18]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[18]));
	ENDFOR
}

void AnonFilter_a3_3357() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[19]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[19]));
	ENDFOR
}

void AnonFilter_a3_3358() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[20]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[20]));
	ENDFOR
}

void AnonFilter_a3_3359() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[21]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[21]));
	ENDFOR
}

void AnonFilter_a3_3360() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[22]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[22]));
	ENDFOR
}

void AnonFilter_a3_3361() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[23]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[23]));
	ENDFOR
}

void AnonFilter_a3_3362() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[24]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[24]));
	ENDFOR
}

void AnonFilter_a3_3363() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[25]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[25]));
	ENDFOR
}

void AnonFilter_a3_3364() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[26]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[26]));
	ENDFOR
}

void AnonFilter_a3_3365() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[27]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[27]));
	ENDFOR
}

void AnonFilter_a3_3366() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[28]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[28]));
	ENDFOR
}

void AnonFilter_a3_3367() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[29]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[29]));
	ENDFOR
}

void AnonFilter_a3_3368() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[30]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[30]));
	ENDFOR
}

void AnonFilter_a3_3369() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[31]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[31]));
	ENDFOR
}

void AnonFilter_a3_3370() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[32]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[32]));
	ENDFOR
}

void AnonFilter_a3_3371() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[33]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[33]));
	ENDFOR
}

void AnonFilter_a3_3372() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[34]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[34]));
	ENDFOR
}

void AnonFilter_a3_3373() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[35]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[35]));
	ENDFOR
}

void AnonFilter_a3_3374() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[36]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[36]));
	ENDFOR
}

void AnonFilter_a3_3375() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[37]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[37]));
	ENDFOR
}

void AnonFilter_a3_3376() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[38]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[38]));
	ENDFOR
}

void AnonFilter_a3_3377() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[39]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[39]));
	ENDFOR
}

void AnonFilter_a3_3378() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[40]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[40]));
	ENDFOR
}

void AnonFilter_a3_3379() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[41]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[41]));
	ENDFOR
}

void AnonFilter_a3_3380() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[42]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[42]));
	ENDFOR
}

void AnonFilter_a3_3381() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[43]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[43]));
	ENDFOR
}

void AnonFilter_a3_3382() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[44]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[44]));
	ENDFOR
}

void AnonFilter_a3_3383() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[45]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[45]));
	ENDFOR
}

void AnonFilter_a3_3384() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[46]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[46]));
	ENDFOR
}

void AnonFilter_a3_3385() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[47]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[47]));
	ENDFOR
}

void AnonFilter_a3_3386() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[48]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[48]));
	ENDFOR
}

void AnonFilter_a3_3387() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[49]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[49]));
	ENDFOR
}

void AnonFilter_a3_3388() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[50]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[50]));
	ENDFOR
}

void AnonFilter_a3_3389() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[51]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[51]));
	ENDFOR
}

void AnonFilter_a3_3390() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[52]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[52]));
	ENDFOR
}

void AnonFilter_a3_3391() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[53]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[53]));
	ENDFOR
}

void AnonFilter_a3_3392() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[54]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[54]));
	ENDFOR
}

void AnonFilter_a3_3393() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[55]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[55]));
	ENDFOR
}

void AnonFilter_a3_3394() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a3(&(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[56]), &(SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[56]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3336() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 57, __iter_++)
			push_int(&SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[__iter_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_3254_3330_3484_3491_split[1]));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3337() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 57, __iter_++)
			push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3337Pre_CollapsedDataParallel_1_3326, pop_float(&SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void Pre_CollapsedDataParallel_1(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int iTimesSumOfWeights_Plus_PartialSum_k = 0;
			iTimesSumOfWeights_Plus_PartialSum_k = _k ; 
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (iTimesSumOfWeights_Plus_PartialSum_k + 0))) ; 
				iTimesSumOfWeights_Plus_PartialSum_k = (iTimesSumOfWeights_Plus_PartialSum_k + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Pre_CollapsedDataParallel_1_3326() {
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		Pre_CollapsedDataParallel_1(&(WEIGHTED_ROUND_ROBIN_Joiner_3337Pre_CollapsedDataParallel_1_3326), &(Pre_CollapsedDataParallel_1_3326WEIGHTED_ROUND_ROBIN_Splitter_3395));
	ENDFOR
}

void iDCT_1D_reference_fine(buffer_float_t *chanin, buffer_float_t *chanout) {
		FOR(int, x, 0,  < , 8, x++) {
			float tempsum = 0.0;
			FOR(int, u, 0,  < , 8, u++) {
				tempsum = (tempsum + (iDCT_1D_reference_fine_3397_s.coeff[x][u] * peek_float(&(*chanin), u))) ; 
			}
			ENDFOR
			push_float(&(*chanout), tempsum) ; 
		}
		ENDFOR
		FOR(int, u, 0,  < , 8, u++) {
			pop_float(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT_1D_reference_fine_3397() {
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_3486_3493_split[0]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_3486_3493_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_3398() {
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_3486_3493_split[1]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_3486_3493_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_3399() {
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_3486_3493_split[2]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_3486_3493_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_3400() {
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_3486_3493_split[3]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_3486_3493_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_3401() {
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_3486_3493_split[4]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_3486_3493_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_3402() {
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_3486_3493_split[5]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_3486_3493_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_3403() {
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_3486_3493_split[6]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_3486_3493_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_3404() {
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin5_iDCT_1D_reference_fine_Fiss_3486_3493_split[7]), &(SplitJoin5_iDCT_1D_reference_fine_Fiss_3486_3493_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3395() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3486_3493_split[__iter_dec_], pop_float(&Pre_CollapsedDataParallel_1_3326WEIGHTED_ROUND_ROBIN_Splitter_3395));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3396() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3396Post_CollapsedDataParallel_2_3327, pop_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3486_3493_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void Post_CollapsedDataParallel_2(buffer_float_t *chanin, buffer_float_t *chanout) {
 {
 {
		FOR(int, _k, 0,  < , 8, _k++) {
			int partialSum_i = 0;
 {
			FOR(int, _i, 0,  < , 8, _i++) {
				push_float(&(*chanout), peek_float(&(*chanin), (_k + (partialSum_i + 0)))) ; 
				partialSum_i = (partialSum_i + 8) ; 
			}
			ENDFOR
		}
		}
		ENDFOR
	}
	}
		pop_float(&(*chanin)) ; 
	}


void Post_CollapsedDataParallel_2_3327() {
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		Post_CollapsedDataParallel_2(&(WEIGHTED_ROUND_ROBIN_Joiner_3396Post_CollapsedDataParallel_2_3327), &(Post_CollapsedDataParallel_2_3327WEIGHTED_ROUND_ROBIN_Splitter_3405));
	ENDFOR
}

void iDCT_1D_reference_fine_3407() {
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_3487_3494_split[0]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_3487_3494_join[0]));
	ENDFOR
}

void iDCT_1D_reference_fine_3408() {
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_3487_3494_split[1]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_3487_3494_join[1]));
	ENDFOR
}

void iDCT_1D_reference_fine_3409() {
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_3487_3494_split[2]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_3487_3494_join[2]));
	ENDFOR
}

void iDCT_1D_reference_fine_3410() {
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_3487_3494_split[3]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_3487_3494_join[3]));
	ENDFOR
}

void iDCT_1D_reference_fine_3411() {
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_3487_3494_split[4]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_3487_3494_join[4]));
	ENDFOR
}

void iDCT_1D_reference_fine_3412() {
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_3487_3494_split[5]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_3487_3494_join[5]));
	ENDFOR
}

void iDCT_1D_reference_fine_3413() {
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_3487_3494_split[6]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_3487_3494_join[6]));
	ENDFOR
}

void iDCT_1D_reference_fine_3414() {
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		iDCT_1D_reference_fine(&(SplitJoin7_iDCT_1D_reference_fine_Fiss_3487_3494_split[7]), &(SplitJoin7_iDCT_1D_reference_fine_Fiss_3487_3494_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3405() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3487_3494_split[__iter_dec_], pop_float(&Post_CollapsedDataParallel_2_3327WEIGHTED_ROUND_ROBIN_Splitter_3405));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3406() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_float(&WEIGHTED_ROUND_ROBIN_Joiner_3406WEIGHTED_ROUND_ROBIN_Splitter_3415, pop_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3487_3494_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void AnonFilter_a4(buffer_float_t *chanin, buffer_int_t *chanout) {
		push_int(&(*chanout), ((int) floor((pop_float(&(*chanin)) + 0.5)))) ; 
	}


void AnonFilter_a4_3417() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[0]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[0]));
	ENDFOR
}

void AnonFilter_a4_3418() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[1]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[1]));
	ENDFOR
}

void AnonFilter_a4_3419() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[2]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[2]));
	ENDFOR
}

void AnonFilter_a4_3420() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[3]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[3]));
	ENDFOR
}

void AnonFilter_a4_3421() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[4]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[4]));
	ENDFOR
}

void AnonFilter_a4_3422() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[5]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[5]));
	ENDFOR
}

void AnonFilter_a4_3423() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[6]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[6]));
	ENDFOR
}

void AnonFilter_a4_3424() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[7]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[7]));
	ENDFOR
}

void AnonFilter_a4_3425() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[8]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[8]));
	ENDFOR
}

void AnonFilter_a4_3426() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[9]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[9]));
	ENDFOR
}

void AnonFilter_a4_3427() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[10]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[10]));
	ENDFOR
}

void AnonFilter_a4_3428() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[11]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[11]));
	ENDFOR
}

void AnonFilter_a4_3429() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[12]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[12]));
	ENDFOR
}

void AnonFilter_a4_3430() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[13]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[13]));
	ENDFOR
}

void AnonFilter_a4_3431() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[14]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[14]));
	ENDFOR
}

void AnonFilter_a4_3432() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[15]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[15]));
	ENDFOR
}

void AnonFilter_a4_3433() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[16]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[16]));
	ENDFOR
}

void AnonFilter_a4_3434() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[17]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[17]));
	ENDFOR
}

void AnonFilter_a4_3435() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[18]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[18]));
	ENDFOR
}

void AnonFilter_a4_3436() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[19]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[19]));
	ENDFOR
}

void AnonFilter_a4_3437() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[20]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[20]));
	ENDFOR
}

void AnonFilter_a4_3438() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[21]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[21]));
	ENDFOR
}

void AnonFilter_a4_3439() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[22]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[22]));
	ENDFOR
}

void AnonFilter_a4_3440() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[23]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[23]));
	ENDFOR
}

void AnonFilter_a4_3441() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[24]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[24]));
	ENDFOR
}

void AnonFilter_a4_3442() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[25]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[25]));
	ENDFOR
}

void AnonFilter_a4_3443() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[26]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[26]));
	ENDFOR
}

void AnonFilter_a4_3444() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[27]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[27]));
	ENDFOR
}

void AnonFilter_a4_3445() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[28]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[28]));
	ENDFOR
}

void AnonFilter_a4_3446() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[29]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[29]));
	ENDFOR
}

void AnonFilter_a4_3447() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[30]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[30]));
	ENDFOR
}

void AnonFilter_a4_3448() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[31]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[31]));
	ENDFOR
}

void AnonFilter_a4_3449() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[32]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[32]));
	ENDFOR
}

void AnonFilter_a4_3450() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[33]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[33]));
	ENDFOR
}

void AnonFilter_a4_3451() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[34]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[34]));
	ENDFOR
}

void AnonFilter_a4_3452() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[35]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[35]));
	ENDFOR
}

void AnonFilter_a4_3453() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[36]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[36]));
	ENDFOR
}

void AnonFilter_a4_3454() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[37]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[37]));
	ENDFOR
}

void AnonFilter_a4_3455() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[38]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[38]));
	ENDFOR
}

void AnonFilter_a4_3456() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[39]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[39]));
	ENDFOR
}

void AnonFilter_a4_3457() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[40]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[40]));
	ENDFOR
}

void AnonFilter_a4_3458() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[41]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[41]));
	ENDFOR
}

void AnonFilter_a4_3459() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[42]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[42]));
	ENDFOR
}

void AnonFilter_a4_3460() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[43]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[43]));
	ENDFOR
}

void AnonFilter_a4_3461() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[44]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[44]));
	ENDFOR
}

void AnonFilter_a4_3462() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[45]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[45]));
	ENDFOR
}

void AnonFilter_a4_3463() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[46]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[46]));
	ENDFOR
}

void AnonFilter_a4_3464() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[47]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[47]));
	ENDFOR
}

void AnonFilter_a4_3465() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[48]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[48]));
	ENDFOR
}

void AnonFilter_a4_3466() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[49]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[49]));
	ENDFOR
}

void AnonFilter_a4_3467() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[50]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[50]));
	ENDFOR
}

void AnonFilter_a4_3468() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[51]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[51]));
	ENDFOR
}

void AnonFilter_a4_3469() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[52]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[52]));
	ENDFOR
}

void AnonFilter_a4_3470() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[53]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[53]));
	ENDFOR
}

void AnonFilter_a4_3471() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[54]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[54]));
	ENDFOR
}

void AnonFilter_a4_3472() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[55]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[55]));
	ENDFOR
}

void AnonFilter_a4_3473() {
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		AnonFilter_a4(&(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[56]), &(SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[56]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3415() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 57, __iter_++)
			push_float(&SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[__iter_], pop_float(&WEIGHTED_ROUND_ROBIN_Joiner_3406WEIGHTED_ROUND_ROBIN_Splitter_3415));
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3416() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 64, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 57, __iter_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_3254_3330_3484_3491_join[1], pop_int(&SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_row_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		int x0 = 0;
		int x1 = 0;
		int x2 = 0;
		int x3 = 0;
		int x4 = 0;
		int x5 = 0;
		int x6 = 0;
		int x7 = 0;
		int x8 = 0;
		x0 = peek_int(&(*chanin), 0) ; 
		x1 = (peek_int(&(*chanin), 4) << 11) ; 
		x2 = peek_int(&(*chanin), 6) ; 
		x3 = peek_int(&(*chanin), 2) ; 
		x4 = peek_int(&(*chanin), 1) ; 
		x5 = peek_int(&(*chanin), 7) ; 
		x6 = peek_int(&(*chanin), 5) ; 
		x7 = peek_int(&(*chanin), 3) ; 
		if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
			x0 = (x0 << 3) ; 
			FOR(int, i, 0,  < , 8, i++) {
				push_int(&(*chanout), x0) ; 
			}
			ENDFOR
		}
		else {
			x0 = ((x0 << 11) + 128) ; 
			x8 = (565 * (x4 + x5)) ; 
			x4 = (x8 + (2276 * x4)) ; 
			x5 = (x8 - (3406 * x5)) ; 
			x8 = (2408 * (x6 + x7)) ; 
			x6 = (x8 - (799 * x6)) ; 
			x7 = (x8 - (4017 * x7)) ; 
			x8 = (x0 + x1) ; 
			x0 = (x0 - x1) ; 
			x1 = (1108 * (x3 + x2)) ; 
			x2 = (x1 - (3784 * x2)) ; 
			x3 = (x1 + (1568 * x3)) ; 
			x1 = (x4 + x6) ; 
			x4 = (x4 - x6) ; 
			x6 = (x5 + x7) ; 
			x5 = (x5 - x7) ; 
			x7 = (x8 + x3) ; 
			x8 = (x8 - x3) ; 
			x3 = (x0 + x2) ; 
			x0 = (x0 - x2) ; 
			x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
			x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
			push_int(&(*chanout), ((x7 + x1) >> 8)) ; 
			push_int(&(*chanout), ((x3 + x2) >> 8)) ; 
			push_int(&(*chanout), ((x0 + x4) >> 8)) ; 
			push_int(&(*chanout), ((x8 + x6) >> 8)) ; 
			push_int(&(*chanout), ((x8 - x6) >> 8)) ; 
			push_int(&(*chanout), ((x0 - x4) >> 8)) ; 
			push_int(&(*chanout), ((x3 - x2) >> 8)) ; 
			push_int(&(*chanout), ((x7 - x1) >> 8)) ; 
		}
		FOR(int, i, 0,  < , 8, i++) {
			pop_int(&(*chanin)) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_row_fast_3476() {
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin138_iDCT8x8_1D_row_fast_Fiss_3489_3496_split[0]), &(SplitJoin138_iDCT8x8_1D_row_fast_Fiss_3489_3496_join[0]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_3477() {
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin138_iDCT8x8_1D_row_fast_Fiss_3489_3496_split[1]), &(SplitJoin138_iDCT8x8_1D_row_fast_Fiss_3489_3496_join[1]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_3478() {
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin138_iDCT8x8_1D_row_fast_Fiss_3489_3496_split[2]), &(SplitJoin138_iDCT8x8_1D_row_fast_Fiss_3489_3496_join[2]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_3479() {
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin138_iDCT8x8_1D_row_fast_Fiss_3489_3496_split[3]), &(SplitJoin138_iDCT8x8_1D_row_fast_Fiss_3489_3496_join[3]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_3480() {
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin138_iDCT8x8_1D_row_fast_Fiss_3489_3496_split[4]), &(SplitJoin138_iDCT8x8_1D_row_fast_Fiss_3489_3496_join[4]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_3481() {
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin138_iDCT8x8_1D_row_fast_Fiss_3489_3496_split[5]), &(SplitJoin138_iDCT8x8_1D_row_fast_Fiss_3489_3496_join[5]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_3482() {
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin138_iDCT8x8_1D_row_fast_Fiss_3489_3496_split[6]), &(SplitJoin138_iDCT8x8_1D_row_fast_Fiss_3489_3496_join[6]));
	ENDFOR
}

void iDCT8x8_1D_row_fast_3483() {
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		iDCT8x8_1D_row_fast(&(SplitJoin138_iDCT8x8_1D_row_fast_Fiss_3489_3496_split[7]), &(SplitJoin138_iDCT8x8_1D_row_fast_Fiss_3489_3496_join[7]));
	ENDFOR
}

void WEIGHTED_ROUND_ROBIN_Splitter_3474() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&SplitJoin138_iDCT8x8_1D_row_fast_Fiss_3489_3496_split[__iter_dec_], pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_3254_3330_3484_3491_split[2]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3475() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		FOR(uint32_t, __iter_dec_, 0, <, 8, __iter_dec_++)
			FOR(uint32_t, __iter_tok_, 0, <, 8, __iter_tok_++)
				push_int(&WEIGHTED_ROUND_ROBIN_Joiner_3475iDCT8x8_1D_col_fast_3280, pop_int(&SplitJoin138_iDCT8x8_1D_row_fast_Fiss_3489_3496_join[__iter_dec_]));
			ENDFOR
		ENDFOR
	ENDFOR
}}

void iDCT8x8_1D_col_fast(buffer_int_t *chanin, buffer_int_t *chanout) {
		FOR(int, c, 0,  < , 8, c++) {
			int x0 = 0;
			int x1 = 0;
			int x2 = 0;
			int x3 = 0;
			int x4 = 0;
			int x5 = 0;
			int x6 = 0;
			int x7 = 0;
			int x8 = 0;
			x0 = peek_int(&(*chanin), (c + 0)) ; 
			x1 = (peek_int(&(*chanin), (c + 32)) << 8) ; 
			x2 = peek_int(&(*chanin), (c + 48)) ; 
			x3 = peek_int(&(*chanin), (c + 16)) ; 
			x4 = peek_int(&(*chanin), (c + 8)) ; 
			x5 = peek_int(&(*chanin), (c + 56)) ; 
			x6 = peek_int(&(*chanin), (c + 40)) ; 
			x7 = peek_int(&(*chanin), (c + 24)) ; 
			if(((((((x1 == 0 && x2 == 0) && x3 == 0) && x4 == 0) && x5 == 0) && x6 == 0) && x7 == 0)) {
				x0 = ((x0 + 32) >> 6) ; 
				FOR(int, i, 0,  < , 8, i++) {
					iDCT8x8_1D_col_fast_3280_s.buffer[(c + (8 * i))] = x0 ; 
				}
				ENDFOR
			}
			else {
				x0 = ((x0 << 8) + 8192) ; 
				x8 = ((565 * (x4 + x5)) + 4) ; 
				x4 = ((x8 + (2276 * x4)) >> 3) ; 
				x5 = ((x8 - (3406 * x5)) >> 3) ; 
				x8 = ((2408 * (x6 + x7)) + 4) ; 
				x6 = ((x8 - (799 * x6)) >> 3) ; 
				x7 = ((x8 - (4017 * x7)) >> 3) ; 
				x8 = (x0 + x1) ; 
				x0 = (x0 - x1) ; 
				x1 = ((1108 * (x3 + x2)) + 4) ; 
				x2 = ((x1 - (3784 * x2)) >> 3) ; 
				x3 = ((x1 + (1568 * x3)) >> 3) ; 
				x1 = (x4 + x6) ; 
				x4 = (x4 - x6) ; 
				x6 = (x5 + x7) ; 
				x5 = (x5 - x7) ; 
				x7 = (x8 + x3) ; 
				x8 = (x8 - x3) ; 
				x3 = (x0 + x2) ; 
				x0 = (x0 - x2) ; 
				x2 = (((181 * (x4 + x5)) + 128) >> 8) ; 
				x4 = (((181 * (x4 - x5)) + 128) >> 8) ; 
				iDCT8x8_1D_col_fast_3280_s.buffer[(c + 0)] = ((x7 + x1) >> 14) ; 
				iDCT8x8_1D_col_fast_3280_s.buffer[(c + 8)] = ((x3 + x2) >> 14) ; 
				iDCT8x8_1D_col_fast_3280_s.buffer[(c + 16)] = ((x0 + x4) >> 14) ; 
				iDCT8x8_1D_col_fast_3280_s.buffer[(c + 24)] = ((x8 + x6) >> 14) ; 
				iDCT8x8_1D_col_fast_3280_s.buffer[(c + 32)] = ((x8 - x6) >> 14) ; 
				iDCT8x8_1D_col_fast_3280_s.buffer[(c + 40)] = ((x0 - x4) >> 14) ; 
				iDCT8x8_1D_col_fast_3280_s.buffer[(c + 48)] = ((x3 - x2) >> 14) ; 
				iDCT8x8_1D_col_fast_3280_s.buffer[(c + 56)] = ((x7 - x1) >> 14) ; 
			}
		}
		ENDFOR
		FOR(int, i, 0,  < , 64, i++) {
			pop_int(&(*chanin)) ; 
			push_int(&(*chanout), iDCT8x8_1D_col_fast_3280_s.buffer[i]) ; 
		}
		ENDFOR
	}


void iDCT8x8_1D_col_fast_3280() {
	FOR(uint32_t, __iter_steady_, 0, <, 57, __iter_steady_++)
		iDCT8x8_1D_col_fast(&(WEIGHTED_ROUND_ROBIN_Joiner_3475iDCT8x8_1D_col_fast_3280), &(SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_3254_3330_3484_3491_join[2]));
	ENDFOR
}

void DUPLICATE_Splitter_3328() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3648, __iter_steady_++)
		int __token_ = pop_int(&AnonFilter_a0_3253DUPLICATE_Splitter_3328);
		FOR(uint32_t, __iter_dup_, 0, <, 3, __iter_dup_++)
			push_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_3254_3330_3484_3491_split[__iter_dup_], __token_);
		ENDFOR
	ENDFOR
}}

void WEIGHTED_ROUND_ROBIN_Joiner_3329() {
{
	FOR(uint32_t, __iter_steady_, 0, <, 3648, __iter_steady_++)
		FOR(uint32_t, __iter_, 0, <, 3, __iter_++)
			push_int(&WEIGHTED_ROUND_ROBIN_Joiner_3329AnonFilter_a2_3281, pop_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_3254_3330_3484_3491_join[__iter_]));
		ENDFOR
	ENDFOR
}}

void AnonFilter_a2(buffer_int_t *chanin) {
		int refcoarse = 0;
		int reffine = 0;
		int fastfine = 0;
		refcoarse = pop_int(&(*chanin)) ; 
		reffine = pop_int(&(*chanin)) ; 
		fastfine = pop_int(&(*chanin)) ; 
		AnonFilter_a2_3281_s.count = (AnonFilter_a2_3281_s.count + 1.0) ; 
		if(refcoarse != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT coarse and fine");
			printf("\n");
			printf("%s", "        coarse iDCT gives ");
			printf("%d", refcoarse);
			printf("%s", " and fine iDCT gives ");
			printf("%d", reffine);
			printf("\n");
			AnonFilter_a2_3281_s.errors = (AnonFilter_a2_3281_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_3281_s.errors / AnonFilter_a2_3281_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
		if(fastfine != reffine) {
			float errorRate = 0.0;
			printf("%s", "Error - Discrepancy between reference iDCT and fast iDCT");
			printf("\n");
			printf("%s", "        reference iDCT gives ");
			printf("%d", reffine);
			printf("%s", " and fast iDCT gives ");
			printf("%d", fastfine);
			printf("\n");
			AnonFilter_a2_3281_s.errors = (AnonFilter_a2_3281_s.errors + 1.0) ; 
			errorRate = (AnonFilter_a2_3281_s.errors / AnonFilter_a2_3281_s.count) ; 
			printf("%s", "        Error Rate is ");
			printf("%.10f", errorRate);
			printf("\n");
		}
	}


void AnonFilter_a2_3281() {
	FOR(uint32_t, __iter_steady_, 0, <, 3648, __iter_steady_++)
		AnonFilter_a2(&(WEIGHTED_ROUND_ROBIN_Joiner_3329AnonFilter_a2_3281));
	ENDFOR
}

void __stream_init__() {
	FOR(int, __iter_init_0_, 0, <, 57, __iter_init_0_++)
		init_buffer_float(&SplitJoin9_AnonFilter_a4_Fiss_3488_3495_split[__iter_init_0_]);
	ENDFOR
	FOR(int, __iter_init_1_, 0, <, 3, __iter_init_1_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_3254_3330_3484_3491_split[__iter_init_1_]);
	ENDFOR
	init_buffer_float(&Pre_CollapsedDataParallel_1_3326WEIGHTED_ROUND_ROBIN_Splitter_3395);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3406WEIGHTED_ROUND_ROBIN_Splitter_3415);
	FOR(int, __iter_init_2_, 0, <, 3, __iter_init_2_++)
		init_buffer_int(&SplitJoin0_SplitJoin0_SplitJoin0_AnonFilter_a1_3254_3330_3484_3491_join[__iter_init_2_]);
	ENDFOR
	FOR(int, __iter_init_3_, 0, <, 57, __iter_init_3_++)
		init_buffer_int(&SplitJoin3_AnonFilter_a3_Fiss_3485_3492_split[__iter_init_3_]);
	ENDFOR
	FOR(int, __iter_init_4_, 0, <, 8, __iter_init_4_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3486_3493_join[__iter_init_4_]);
	ENDFOR
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3337Pre_CollapsedDataParallel_1_3326);
	FOR(int, __iter_init_5_, 0, <, 57, __iter_init_5_++)
		init_buffer_float(&SplitJoin3_AnonFilter_a3_Fiss_3485_3492_join[__iter_init_5_]);
	ENDFOR
	FOR(int, __iter_init_6_, 0, <, 8, __iter_init_6_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3487_3494_split[__iter_init_6_]);
	ENDFOR
	FOR(int, __iter_init_7_, 0, <, 57, __iter_init_7_++)
		init_buffer_int(&SplitJoin9_AnonFilter_a4_Fiss_3488_3495_join[__iter_init_7_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_3475iDCT8x8_1D_col_fast_3280);
	init_buffer_int(&AnonFilter_a0_3253DUPLICATE_Splitter_3328);
	init_buffer_float(&WEIGHTED_ROUND_ROBIN_Joiner_3396Post_CollapsedDataParallel_2_3327);
	FOR(int, __iter_init_8_, 0, <, 8, __iter_init_8_++)
		init_buffer_int(&SplitJoin138_iDCT8x8_1D_row_fast_Fiss_3489_3496_split[__iter_init_8_]);
	ENDFOR
	FOR(int, __iter_init_9_, 0, <, 8, __iter_init_9_++)
		init_buffer_float(&SplitJoin7_iDCT_1D_reference_fine_Fiss_3487_3494_join[__iter_init_9_]);
	ENDFOR
	FOR(int, __iter_init_10_, 0, <, 8, __iter_init_10_++)
		init_buffer_float(&SplitJoin5_iDCT_1D_reference_fine_Fiss_3486_3493_split[__iter_init_10_]);
	ENDFOR
	init_buffer_float(&Post_CollapsedDataParallel_2_3327WEIGHTED_ROUND_ROBIN_Splitter_3405);
	FOR(int, __iter_init_11_, 0, <, 8, __iter_init_11_++)
		init_buffer_int(&SplitJoin138_iDCT8x8_1D_row_fast_Fiss_3489_3496_join[__iter_init_11_]);
	ENDFOR
	init_buffer_int(&WEIGHTED_ROUND_ROBIN_Joiner_3329AnonFilter_a2_3281);
// --- init: iDCT_2D_reference_coarse_3256
	 {
	FOR(int, freq, 0,  < , 8, freq++) {
		float scale = 0.0;
		scale = freq == 0 ? (0.35355338) : (0.5) ; 
		FOR(int, time, 0,  < , 8, time++) {
			iDCT_2D_reference_coarse_3256_s.coeff[freq][time] = (scale * ((float) cos(((0.3926991 * freq) * (time + 0.5))))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3397
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3397_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3398
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3398_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3399
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3399_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3400
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3400_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3401
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3401_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3402
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3402_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3403
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3403_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3404
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3404_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3407
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3407_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3408
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3408_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3409
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3409_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3410
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3410_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3411
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3411_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3412
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3412_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3413
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3413_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: iDCT_1D_reference_fine_3414
	 {
	FOR(int, x, 0,  < , 8, x++) {
		FOR(int, u, 0,  < , 8, u++) {
			float Cu = 0.0;
			Cu = 1.0 ; 
			if(u == 0) {
				Cu = 0.70710677 ; 
			}
			iDCT_1D_reference_fine_3414_s.coeff[x][u] = ((0.5 * Cu) * ((float) cos((((u * 3.1415927) * ((2.0 * x) + 1.0)) / 16.0)))) ; 
		}
		ENDFOR
	}
	ENDFOR
}
//--------------------------------
// --- init: AnonFilter_a2_3281
	 {
	AnonFilter_a2_3281_s.count = 0.0 ; 
	AnonFilter_a2_3281_s.errors = 0.0 ; 
}
//--------------------------------
}
int main(int argv, char** argc) {
	__stream_init__();

	FOR(uint32_t, iter, 0, <, MAX_ITERATION, iter++)
		AnonFilter_a0_3253();
		DUPLICATE_Splitter_3328();
			iDCT_2D_reference_coarse_3256();
			WEIGHTED_ROUND_ROBIN_Splitter_3336();
				AnonFilter_a3_3338();
				AnonFilter_a3_3339();
				AnonFilter_a3_3340();
				AnonFilter_a3_3341();
				AnonFilter_a3_3342();
				AnonFilter_a3_3343();
				AnonFilter_a3_3344();
				AnonFilter_a3_3345();
				AnonFilter_a3_3346();
				AnonFilter_a3_3347();
				AnonFilter_a3_3348();
				AnonFilter_a3_3349();
				AnonFilter_a3_3350();
				AnonFilter_a3_3351();
				AnonFilter_a3_3352();
				AnonFilter_a3_3353();
				AnonFilter_a3_3354();
				AnonFilter_a3_3355();
				AnonFilter_a3_3356();
				AnonFilter_a3_3357();
				AnonFilter_a3_3358();
				AnonFilter_a3_3359();
				AnonFilter_a3_3360();
				AnonFilter_a3_3361();
				AnonFilter_a3_3362();
				AnonFilter_a3_3363();
				AnonFilter_a3_3364();
				AnonFilter_a3_3365();
				AnonFilter_a3_3366();
				AnonFilter_a3_3367();
				AnonFilter_a3_3368();
				AnonFilter_a3_3369();
				AnonFilter_a3_3370();
				AnonFilter_a3_3371();
				AnonFilter_a3_3372();
				AnonFilter_a3_3373();
				AnonFilter_a3_3374();
				AnonFilter_a3_3375();
				AnonFilter_a3_3376();
				AnonFilter_a3_3377();
				AnonFilter_a3_3378();
				AnonFilter_a3_3379();
				AnonFilter_a3_3380();
				AnonFilter_a3_3381();
				AnonFilter_a3_3382();
				AnonFilter_a3_3383();
				AnonFilter_a3_3384();
				AnonFilter_a3_3385();
				AnonFilter_a3_3386();
				AnonFilter_a3_3387();
				AnonFilter_a3_3388();
				AnonFilter_a3_3389();
				AnonFilter_a3_3390();
				AnonFilter_a3_3391();
				AnonFilter_a3_3392();
				AnonFilter_a3_3393();
				AnonFilter_a3_3394();
			WEIGHTED_ROUND_ROBIN_Joiner_3337();
			Pre_CollapsedDataParallel_1_3326();
			WEIGHTED_ROUND_ROBIN_Splitter_3395();
				iDCT_1D_reference_fine_3397();
				iDCT_1D_reference_fine_3398();
				iDCT_1D_reference_fine_3399();
				iDCT_1D_reference_fine_3400();
				iDCT_1D_reference_fine_3401();
				iDCT_1D_reference_fine_3402();
				iDCT_1D_reference_fine_3403();
				iDCT_1D_reference_fine_3404();
			WEIGHTED_ROUND_ROBIN_Joiner_3396();
			Post_CollapsedDataParallel_2_3327();
			WEIGHTED_ROUND_ROBIN_Splitter_3405();
				iDCT_1D_reference_fine_3407();
				iDCT_1D_reference_fine_3408();
				iDCT_1D_reference_fine_3409();
				iDCT_1D_reference_fine_3410();
				iDCT_1D_reference_fine_3411();
				iDCT_1D_reference_fine_3412();
				iDCT_1D_reference_fine_3413();
				iDCT_1D_reference_fine_3414();
			WEIGHTED_ROUND_ROBIN_Joiner_3406();
			WEIGHTED_ROUND_ROBIN_Splitter_3415();
				AnonFilter_a4_3417();
				AnonFilter_a4_3418();
				AnonFilter_a4_3419();
				AnonFilter_a4_3420();
				AnonFilter_a4_3421();
				AnonFilter_a4_3422();
				AnonFilter_a4_3423();
				AnonFilter_a4_3424();
				AnonFilter_a4_3425();
				AnonFilter_a4_3426();
				AnonFilter_a4_3427();
				AnonFilter_a4_3428();
				AnonFilter_a4_3429();
				AnonFilter_a4_3430();
				AnonFilter_a4_3431();
				AnonFilter_a4_3432();
				AnonFilter_a4_3433();
				AnonFilter_a4_3434();
				AnonFilter_a4_3435();
				AnonFilter_a4_3436();
				AnonFilter_a4_3437();
				AnonFilter_a4_3438();
				AnonFilter_a4_3439();
				AnonFilter_a4_3440();
				AnonFilter_a4_3441();
				AnonFilter_a4_3442();
				AnonFilter_a4_3443();
				AnonFilter_a4_3444();
				AnonFilter_a4_3445();
				AnonFilter_a4_3446();
				AnonFilter_a4_3447();
				AnonFilter_a4_3448();
				AnonFilter_a4_3449();
				AnonFilter_a4_3450();
				AnonFilter_a4_3451();
				AnonFilter_a4_3452();
				AnonFilter_a4_3453();
				AnonFilter_a4_3454();
				AnonFilter_a4_3455();
				AnonFilter_a4_3456();
				AnonFilter_a4_3457();
				AnonFilter_a4_3458();
				AnonFilter_a4_3459();
				AnonFilter_a4_3460();
				AnonFilter_a4_3461();
				AnonFilter_a4_3462();
				AnonFilter_a4_3463();
				AnonFilter_a4_3464();
				AnonFilter_a4_3465();
				AnonFilter_a4_3466();
				AnonFilter_a4_3467();
				AnonFilter_a4_3468();
				AnonFilter_a4_3469();
				AnonFilter_a4_3470();
				AnonFilter_a4_3471();
				AnonFilter_a4_3472();
				AnonFilter_a4_3473();
			WEIGHTED_ROUND_ROBIN_Joiner_3416();
			WEIGHTED_ROUND_ROBIN_Splitter_3474();
				iDCT8x8_1D_row_fast_3476();
				iDCT8x8_1D_row_fast_3477();
				iDCT8x8_1D_row_fast_3478();
				iDCT8x8_1D_row_fast_3479();
				iDCT8x8_1D_row_fast_3480();
				iDCT8x8_1D_row_fast_3481();
				iDCT8x8_1D_row_fast_3482();
				iDCT8x8_1D_row_fast_3483();
			WEIGHTED_ROUND_ROBIN_Joiner_3475();
			iDCT8x8_1D_col_fast_3280();
		WEIGHTED_ROUND_ROBIN_Joiner_3329();
		AnonFilter_a2_3281();
	ENDFOR
	return EXIT_SUCCESS;
}
