#ifndef BUF_SIZEMAX 
#error add -DBUF_SIZEMAX=14976 on the compile command line
#else
#if BUF_SIZEMAX < 14976
#error BUF_SIZEMAX too small, it must be at least 14976
#endif
#endif

#include "globals.h"

#ifndef _H
#define _H
#ifdef __cplusplus
extern "C" {
#endif



typedef struct {
	float coeff[8][8];
} iDCT_2D_reference_coarse_9664_t;

typedef struct {
	int buffer[64];
} iDCT8x8_1D_col_fast_9688_t;

typedef struct {
	float count;
	float errors;
} AnonFilter_a2_9689_t;
void AnonFilter_a0_9661();
void DUPLICATE_Splitter_9736();
void iDCT_2D_reference_coarse_9664();
void WEIGHTED_ROUND_ROBIN_Splitter_9744();
void AnonFilter_a3_9746();
void AnonFilter_a3_9747();
void AnonFilter_a3_9748();
void AnonFilter_a3_9749();
void AnonFilter_a3_9750();
void AnonFilter_a3_9751();
void AnonFilter_a3_9752();
void AnonFilter_a3_9753();
void AnonFilter_a3_9754();
void AnonFilter_a3_9755();
void AnonFilter_a3_9756();
void AnonFilter_a3_9757();
void AnonFilter_a3_9758();
void AnonFilter_a3_9759();
void AnonFilter_a3_9760();
void AnonFilter_a3_9761();
void AnonFilter_a3_9762();
void AnonFilter_a3_9763();
void AnonFilter_a3_9764();
void AnonFilter_a3_9765();
void AnonFilter_a3_9766();
void AnonFilter_a3_9767();
void AnonFilter_a3_9768();
void AnonFilter_a3_9769();
void AnonFilter_a3_9770();
void AnonFilter_a3_9771();
void AnonFilter_a3_9772();
void AnonFilter_a3_9773();
void AnonFilter_a3_9774();
void AnonFilter_a3_9775();
void AnonFilter_a3_9776();
void AnonFilter_a3_9777();
void AnonFilter_a3_9778();
void AnonFilter_a3_9779();
void AnonFilter_a3_9780();
void AnonFilter_a3_9781();
void AnonFilter_a3_9782();
void AnonFilter_a3_9783();
void AnonFilter_a3_9784();
void WEIGHTED_ROUND_ROBIN_Joiner_9745();
void Pre_CollapsedDataParallel_1_9734();
void WEIGHTED_ROUND_ROBIN_Splitter_9785();
void iDCT_1D_reference_fine_9787();
void iDCT_1D_reference_fine_9788();
void iDCT_1D_reference_fine_9789();
void iDCT_1D_reference_fine_9790();
void iDCT_1D_reference_fine_9791();
void iDCT_1D_reference_fine_9792();
void iDCT_1D_reference_fine_9793();
void iDCT_1D_reference_fine_9794();
void WEIGHTED_ROUND_ROBIN_Joiner_9786();
void Post_CollapsedDataParallel_2_9735();
void WEIGHTED_ROUND_ROBIN_Splitter_9795();
void iDCT_1D_reference_fine_9797();
void iDCT_1D_reference_fine_9798();
void iDCT_1D_reference_fine_9799();
void iDCT_1D_reference_fine_9800();
void iDCT_1D_reference_fine_9801();
void iDCT_1D_reference_fine_9802();
void iDCT_1D_reference_fine_9803();
void iDCT_1D_reference_fine_9804();
void WEIGHTED_ROUND_ROBIN_Joiner_9796();
void WEIGHTED_ROUND_ROBIN_Splitter_9805();
void AnonFilter_a4_9807();
void AnonFilter_a4_9808();
void AnonFilter_a4_9809();
void AnonFilter_a4_9810();
void AnonFilter_a4_9811();
void AnonFilter_a4_9812();
void AnonFilter_a4_9813();
void AnonFilter_a4_9814();
void AnonFilter_a4_9815();
void AnonFilter_a4_9816();
void AnonFilter_a4_9817();
void AnonFilter_a4_9818();
void AnonFilter_a4_9819();
void AnonFilter_a4_9820();
void AnonFilter_a4_9821();
void AnonFilter_a4_9822();
void AnonFilter_a4_9823();
void AnonFilter_a4_9824();
void AnonFilter_a4_9825();
void AnonFilter_a4_9826();
void AnonFilter_a4_9827();
void AnonFilter_a4_9828();
void AnonFilter_a4_9829();
void AnonFilter_a4_9830();
void AnonFilter_a4_9831();
void AnonFilter_a4_9832();
void AnonFilter_a4_9833();
void AnonFilter_a4_9834();
void AnonFilter_a4_9835();
void AnonFilter_a4_9836();
void AnonFilter_a4_9837();
void AnonFilter_a4_9838();
void AnonFilter_a4_9839();
void AnonFilter_a4_9840();
void AnonFilter_a4_9841();
void AnonFilter_a4_9842();
void AnonFilter_a4_9843();
void AnonFilter_a4_9844();
void AnonFilter_a4_9845();
void WEIGHTED_ROUND_ROBIN_Joiner_9806();
void WEIGHTED_ROUND_ROBIN_Splitter_9846();
void iDCT8x8_1D_row_fast_9848();
void iDCT8x8_1D_row_fast_9849();
void iDCT8x8_1D_row_fast_9850();
void iDCT8x8_1D_row_fast_9851();
void iDCT8x8_1D_row_fast_9852();
void iDCT8x8_1D_row_fast_9853();
void iDCT8x8_1D_row_fast_9854();
void iDCT8x8_1D_row_fast_9855();
void WEIGHTED_ROUND_ROBIN_Joiner_9847();
void iDCT8x8_1D_col_fast_9688();
void WEIGHTED_ROUND_ROBIN_Joiner_9737();
void AnonFilter_a2_9689();

#ifdef __cplusplus
}
#endif
#endif
